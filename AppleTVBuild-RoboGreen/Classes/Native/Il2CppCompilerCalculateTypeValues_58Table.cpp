﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DG.Tweening.Sequence
struct Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2;
// DG.Tweening.Tween
struct Tween_t119487E0AB84EF563521F1043116BDBAE68AC437;
// Doozy.Engine.Nody.GraphController
struct GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8;
// Doozy.Engine.Nody.Models.Graph
struct Graph_t26D9C31435F4455B6EF025E506031462302E7AD5;
// Doozy.Engine.Nody.Models.Node
struct Node_t06F79187A31E6958DED19533B1A8783AD1633439;
// Doozy.Engine.Nody.Nodes.SubGraphNode
struct SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B;
// Doozy.Engine.Orientation.OrientationEvent
struct OrientationEvent_tCC6D3CFE9251CC74E8F13085EE96BF9A1B2B267B;
// Doozy.Engine.Progress.ProgressEvent
struct ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB;
// Doozy.Engine.Progress.Progressor
struct Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A;
// Doozy.Engine.SceneManagement.ActiveSceneChangedEvent
struct ActiveSceneChangedEvent_t0AB76BCA01E7396F308C3216662C1A70015DD7D4;
// Doozy.Engine.SceneManagement.SceneLoadBehavior
struct SceneLoadBehavior_t09385F7DAC85769326248B04A71A5434C14D45AF;
// Doozy.Engine.SceneManagement.SceneLoadedEvent
struct SceneLoadedEvent_t9EED90839B7F10C9CF694B1DBFC750B7E9632E82;
// Doozy.Engine.SceneManagement.SceneLoader
struct SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438;
// Doozy.Engine.SceneManagement.SceneUnloadedEvent
struct SceneUnloadedEvent_tF6E196C99EC905E8F0AA9DE9F5246F3448B41471;
// Doozy.Engine.Soundy.SoundyController
struct SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55;
// Doozy.Engine.Soundy.SoundyDatabase
struct SoundyDatabase_t053E1E9D0784E5A630FB918EFC5A7ACAA1D45656;
// Doozy.Engine.Soundy.SoundyPooler
struct SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282;
// Doozy.Engine.UI.Base.UIAction
struct UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<Doozy.Engine.Nody.GraphController>
struct List_1_t31A111DAFD6F46A820A121BA6E1C065DC3352DCB;
// System.Collections.Generic.List`1<Doozy.Engine.Nody.Models.Connection>
struct List_1_t61DEAC4B06FEC937FDCA0FF04B7C2798A15F1B26;
// System.Collections.Generic.List`1<Doozy.Engine.Nody.Models.Node>
struct List_1_t11A6410B9139871ED4209E0644694FDA98C325D1;
// System.Collections.Generic.List`1<Doozy.Engine.Nody.Models.Socket>
struct List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B;
// System.Collections.Generic.List`1<Doozy.Engine.Nody.Nodes.SwitchBackNode/SourceInfo>
struct List_1_t33387D4A0B481B002D4EAF57889F1CB4358AF7F1;
// System.Collections.Generic.List`1<Doozy.Engine.Progress.ProgressTarget>
struct List_1_t2A31FDCF6ACB40936AC743A2EFAF773FF65FE3F9;
// System.Collections.Generic.List`1<Doozy.Engine.Progress.Progressor>
struct List_1_tBB346642EB44B9D182B9B1708BF9EE34A696E946;
// System.Collections.Generic.List`1<Doozy.Engine.SceneManagement.SceneLoader>
struct List_1_tA9542A65BB25F900DB32842C29255A9A48523618;
// System.Collections.Generic.List`1<Doozy.Engine.Soundy.SoundyController>
struct List_1_t9BE00C8B071DC24C9723A6663A60205AB423B921;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Func`2<Doozy.Engine.Progress.Progressor,System.Boolean>
struct Func_2_t6F58E1CB2804FA531F97321E15F0E4A6015D7061;
// System.Func`2<Doozy.Engine.Soundy.SoundyController,System.Boolean>
struct Func_2_t937B00FFBED8AFA80585DA4C4776787F1DEF7E72;
// System.Func`2<UnityEngine.Rect,System.Single>
struct Func_2_tB2BBA2235D0762C1D488C18DDDB7D454EC1D9380;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TMP_Text
struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AsyncOperation
struct AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F;
// UnityEngine.Audio.AudioMixerGroup
struct AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.WaitForSecondsRealtime
struct WaitForSecondsRealtime_t0CF361107C4A9E25E0D4CF2F37732CE785235739;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DOTWEENMODULEAUDIO_T769A1BF9E3697BFACC9815179E7A39085E56CE4D_H
#define DOTWEENMODULEAUDIO_T769A1BF9E3697BFACC9815179E7A39085E56CE4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleAudio
struct  DOTweenModuleAudio_t769A1BF9E3697BFACC9815179E7A39085E56CE4D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULEAUDIO_T769A1BF9E3697BFACC9815179E7A39085E56CE4D_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T72C207EC4FC022EB09F6F8D33004BEC6FBC66E6F_H
#define U3CU3EC__DISPLAYCLASS0_0_T72C207EC4FC022EB09F6F8D33004BEC6FBC66E6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t72C207EC4FC022EB09F6F8D33004BEC6FBC66E6F  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::target
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t72C207EC4FC022EB09F6F8D33004BEC6FBC66E6F, ___target_0)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_target_0() const { return ___target_0; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T72C207EC4FC022EB09F6F8D33004BEC6FBC66E6F_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T4A743D54E80E5072E25F38A09BE27414B88BC2DD_H
#define U3CU3EC__DISPLAYCLASS1_0_T4A743D54E80E5072E25F38A09BE27414B88BC2DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t4A743D54E80E5072E25F38A09BE27414B88BC2DD  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::target
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t4A743D54E80E5072E25F38A09BE27414B88BC2DD, ___target_0)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_target_0() const { return ___target_0; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T4A743D54E80E5072E25F38A09BE27414B88BC2DD_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T9C60BFCCE659FB7698FF542E41008F990B147ACC_H
#define U3CU3EC__DISPLAYCLASS2_0_T9C60BFCCE659FB7698FF542E41008F990B147ACC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t9C60BFCCE659FB7698FF542E41008F990B147ACC  : public RuntimeObject
{
public:
	// UnityEngine.Audio.AudioMixer DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::target
	AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * ___target_0;
	// System.String DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::floatName
	String_t* ___floatName_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t9C60BFCCE659FB7698FF542E41008F990B147ACC, ___target_0)); }
	inline AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * get_target_0() const { return ___target_0; }
	inline AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_floatName_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t9C60BFCCE659FB7698FF542E41008F990B147ACC, ___floatName_1)); }
	inline String_t* get_floatName_1() const { return ___floatName_1; }
	inline String_t** get_address_of_floatName_1() { return &___floatName_1; }
	inline void set_floatName_1(String_t* value)
	{
		___floatName_1 = value;
		Il2CppCodeGenWriteBarrier((&___floatName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T9C60BFCCE659FB7698FF542E41008F990B147ACC_H
#ifndef DOTWEENMODULEPHYSICS_T54AF484E9A4CEC236EE03303DDE8634FC383938E_H
#define DOTWEENMODULEPHYSICS_T54AF484E9A4CEC236EE03303DDE8634FC383938E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics
struct  DOTweenModulePhysics_t54AF484E9A4CEC236EE03303DDE8634FC383938E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULEPHYSICS_T54AF484E9A4CEC236EE03303DDE8634FC383938E_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T5C64C7513BD29248DFA2579BC02E0EB936F7BFE3_H
#define U3CU3EC__DISPLAYCLASS0_0_T5C64C7513BD29248DFA2579BC02E0EB936F7BFE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t5C64C7513BD29248DFA2579BC02E0EB936F7BFE3  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass0_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t5C64C7513BD29248DFA2579BC02E0EB936F7BFE3, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T5C64C7513BD29248DFA2579BC02E0EB936F7BFE3_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T41907D10A4F8492809D26E4C4B12EEF791CD791F_H
#define U3CU3EC__DISPLAYCLASS10_0_T41907D10A4F8492809D26E4C4B12EEF791CD791F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t41907D10A4F8492809D26E4C4B12EEF791CD791F  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::trans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t41907D10A4F8492809D26E4C4B12EEF791CD791F, ___trans_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_trans_0() const { return ___trans_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((&___trans_0), value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t41907D10A4F8492809D26E4C4B12EEF791CD791F, ___target_1)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_1() const { return ___target_1; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T41907D10A4F8492809D26E4C4B12EEF791CD791F_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_TA56D94394389743477D8477AC85120BFC5536CC3_H
#define U3CU3EC__DISPLAYCLASS1_0_TA56D94394389743477D8477AC85120BFC5536CC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tA56D94394389743477D8477AC85120BFC5536CC3  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass1_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tA56D94394389743477D8477AC85120BFC5536CC3, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_TA56D94394389743477D8477AC85120BFC5536CC3_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T0C665D49874F06B1EA3D9C3145443B48B75733FF_H
#define U3CU3EC__DISPLAYCLASS2_0_T0C665D49874F06B1EA3D9C3145443B48B75733FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t0C665D49874F06B1EA3D9C3145443B48B75733FF  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass2_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t0C665D49874F06B1EA3D9C3145443B48B75733FF, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T0C665D49874F06B1EA3D9C3145443B48B75733FF_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_TFD9A3293995CC5CFCD9996BB2B9DBECE1F2853EB_H
#define U3CU3EC__DISPLAYCLASS3_0_TFD9A3293995CC5CFCD9996BB2B9DBECE1F2853EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_tFD9A3293995CC5CFCD9996BB2B9DBECE1F2853EB  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass3_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tFD9A3293995CC5CFCD9996BB2B9DBECE1F2853EB, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_TFD9A3293995CC5CFCD9996BB2B9DBECE1F2853EB_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T95C132B298C8FED300DCB1A2CF98CCC5CFBA9AE3_H
#define U3CU3EC__DISPLAYCLASS4_0_T95C132B298C8FED300DCB1A2CF98CCC5CFBA9AE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t95C132B298C8FED300DCB1A2CF98CCC5CFBA9AE3  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass4_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t95C132B298C8FED300DCB1A2CF98CCC5CFBA9AE3, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T95C132B298C8FED300DCB1A2CF98CCC5CFBA9AE3_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T41A0920837839D6F1EBED888FCA9A88B12AC0F18_H
#define U3CU3EC__DISPLAYCLASS5_0_T41A0920837839D6F1EBED888FCA9A88B12AC0F18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t41A0920837839D6F1EBED888FCA9A88B12AC0F18  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass5_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t41A0920837839D6F1EBED888FCA9A88B12AC0F18, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T41A0920837839D6F1EBED888FCA9A88B12AC0F18_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T08A1ECA62C440E40725FC57E64C7EB336CECCC84_H
#define U3CU3EC__DISPLAYCLASS7_0_T08A1ECA62C440E40725FC57E64C7EB336CECCC84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t08A1ECA62C440E40725FC57E64C7EB336CECCC84  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass7_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t08A1ECA62C440E40725FC57E64C7EB336CECCC84, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T08A1ECA62C440E40725FC57E64C7EB336CECCC84_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_TB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1_H
#define U3CU3EC__DISPLAYCLASS8_0_TB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::trans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1, ___trans_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_trans_0() const { return ___trans_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((&___trans_0), value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1, ___target_1)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_1() const { return ___target_1; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_TB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T96637B7BE8971D6AE57F08CD85C9BFF8E11C2B6F_H
#define U3CU3EC__DISPLAYCLASS9_0_T96637B7BE8971D6AE57F08CD85C9BFF8E11C2B6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t96637B7BE8971D6AE57F08CD85C9BFF8E11C2B6F  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass9_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t96637B7BE8971D6AE57F08CD85C9BFF8E11C2B6F, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T96637B7BE8971D6AE57F08CD85C9BFF8E11C2B6F_H
#ifndef DOTWEENMODULEPHYSICS2D_T31D337CBA76C3DFD0CF747008022FBBAB46343BF_H
#define DOTWEENMODULEPHYSICS2D_T31D337CBA76C3DFD0CF747008022FBBAB46343BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics2D
struct  DOTweenModulePhysics2D_t31D337CBA76C3DFD0CF747008022FBBAB46343BF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULEPHYSICS2D_T31D337CBA76C3DFD0CF747008022FBBAB46343BF_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_TAE598412087F01228B45541B5C42672EA61EC671_H
#define U3CU3EC__DISPLAYCLASS0_0_TAE598412087F01228B45541B5C42672EA61EC671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_tAE598412087F01228B45541B5C42672EA61EC671  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass0_0::target
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tAE598412087F01228B45541B5C42672EA61EC671, ___target_0)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_TAE598412087F01228B45541B5C42672EA61EC671_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T4A0577007A5B036CAFBBD362531FDA652965EF09_H
#define U3CU3EC__DISPLAYCLASS1_0_T4A0577007A5B036CAFBBD362531FDA652965EF09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t4A0577007A5B036CAFBBD362531FDA652965EF09  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass1_0::target
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t4A0577007A5B036CAFBBD362531FDA652965EF09, ___target_0)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T4A0577007A5B036CAFBBD362531FDA652965EF09_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T2674AB8293A4F3119319017A1FFF86A555F234A7_H
#define U3CU3EC__DISPLAYCLASS2_0_T2674AB8293A4F3119319017A1FFF86A555F234A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t2674AB8293A4F3119319017A1FFF86A555F234A7  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass2_0::target
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t2674AB8293A4F3119319017A1FFF86A555F234A7, ___target_0)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T2674AB8293A4F3119319017A1FFF86A555F234A7_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T6012E00DCCD7C59473E11E70423E98FFAEAB126F_H
#define U3CU3EC__DISPLAYCLASS3_0_T6012E00DCCD7C59473E11E70423E98FFAEAB126F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t6012E00DCCD7C59473E11E70423E98FFAEAB126F  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass3_0::target
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t6012E00DCCD7C59473E11E70423E98FFAEAB126F, ___target_0)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T6012E00DCCD7C59473E11E70423E98FFAEAB126F_H
#ifndef DOTWEENMODULESPRITE_TA07D646AA9234481CF1BA55D046BC7F824A818F2_H
#define DOTWEENMODULESPRITE_TA07D646AA9234481CF1BA55D046BC7F824A818F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleSprite
struct  DOTweenModuleSprite_tA07D646AA9234481CF1BA55D046BC7F824A818F2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULESPRITE_TA07D646AA9234481CF1BA55D046BC7F824A818F2_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_TF979E38F1CE3544D9E454AE0AF7ECB621C359998_H
#define U3CU3EC__DISPLAYCLASS0_0_TF979E38F1CE3544D9E454AE0AF7ECB621C359998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_tF979E38F1CE3544D9E454AE0AF7ECB621C359998  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::target
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tF979E38F1CE3544D9E454AE0AF7ECB621C359998, ___target_0)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_TF979E38F1CE3544D9E454AE0AF7ECB621C359998_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T3C1F8A4FCB252FA03523157E817DEDAD87EBA1C7_H
#define U3CU3EC__DISPLAYCLASS1_0_T3C1F8A4FCB252FA03523157E817DEDAD87EBA1C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t3C1F8A4FCB252FA03523157E817DEDAD87EBA1C7  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::target
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t3C1F8A4FCB252FA03523157E817DEDAD87EBA1C7, ___target_0)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T3C1F8A4FCB252FA03523157E817DEDAD87EBA1C7_H
#ifndef DOTWEENMODULEUI_T39E58130742D7CFD84DE6B0EA1DC7ED3C0A839D0_H
#define DOTWEENMODULEUI_T39E58130742D7CFD84DE6B0EA1DC7ED3C0A839D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI
struct  DOTweenModuleUI_t39E58130742D7CFD84DE6B0EA1DC7ED3C0A839D0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULEUI_T39E58130742D7CFD84DE6B0EA1DC7ED3C0A839D0_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T1CD861E6D228C2C19A057642CB25BC00977708E7_H
#define U3CU3EC__DISPLAYCLASS0_0_T1CD861E6D228C2C19A057642CB25BC00977708E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t1CD861E6D228C2C19A057642CB25BC00977708E7  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::target
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t1CD861E6D228C2C19A057642CB25BC00977708E7, ___target_0)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_target_0() const { return ___target_0; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T1CD861E6D228C2C19A057642CB25BC00977708E7_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T551B53D6928422D334300FB5FE36087E17A7199A_H
#define U3CU3EC__DISPLAYCLASS1_0_T551B53D6928422D334300FB5FE36087E17A7199A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t551B53D6928422D334300FB5FE36087E17A7199A  : public RuntimeObject
{
public:
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::target
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t551B53D6928422D334300FB5FE36087E17A7199A, ___target_0)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_target_0() const { return ___target_0; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T551B53D6928422D334300FB5FE36087E17A7199A_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TE5F3CF3169FC68264B35D20D81204FFF0F52D2CF_H
#define U3CU3EC__DISPLAYCLASS2_0_TE5F3CF3169FC68264B35D20D81204FFF0F52D2CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tE5F3CF3169FC68264B35D20D81204FFF0F52D2CF  : public RuntimeObject
{
public:
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::target
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tE5F3CF3169FC68264B35D20D81204FFF0F52D2CF, ___target_0)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_target_0() const { return ___target_0; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TE5F3CF3169FC68264B35D20D81204FFF0F52D2CF_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T8723E84900FA09817B34237CBECC97C7A463F288_H
#define U3CU3EC__DISPLAYCLASS3_0_T8723E84900FA09817B34237CBECC97C7A463F288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t8723E84900FA09817B34237CBECC97C7A463F288  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::target
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t8723E84900FA09817B34237CBECC97C7A463F288, ___target_0)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_target_0() const { return ___target_0; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T8723E84900FA09817B34237CBECC97C7A463F288_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T3D7A8AD757769FD8E3E12FE825921D885FBAC98E_H
#define U3CU3EC__DISPLAYCLASS4_0_T3D7A8AD757769FD8E3E12FE825921D885FBAC98E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t3D7A8AD757769FD8E3E12FE825921D885FBAC98E  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::target
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t3D7A8AD757769FD8E3E12FE825921D885FBAC98E, ___target_0)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_target_0() const { return ___target_0; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T3D7A8AD757769FD8E3E12FE825921D885FBAC98E_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T89CE27455F60C162B98656F385CDF2F8C83B3813_H
#define U3CU3EC__DISPLAYCLASS5_0_T89CE27455F60C162B98656F385CDF2F8C83B3813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t89CE27455F60C162B98656F385CDF2F8C83B3813  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::target
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t89CE27455F60C162B98656F385CDF2F8C83B3813, ___target_0)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_target_0() const { return ___target_0; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T89CE27455F60C162B98656F385CDF2F8C83B3813_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T1997C2BD49954A2A3D92FD767A822B8F1B4B95DB_H
#define U3CU3EC__DISPLAYCLASS7_0_T1997C2BD49954A2A3D92FD767A822B8F1B4B95DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t1997C2BD49954A2A3D92FD767A822B8F1B4B95DB  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::target
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t1997C2BD49954A2A3D92FD767A822B8F1B4B95DB, ___target_0)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_target_0() const { return ___target_0; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T1997C2BD49954A2A3D92FD767A822B8F1B4B95DB_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05_H
#define U3CU3EC__DISPLAYCLASS8_0_T4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::target
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05, ___target_0)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_target_0() const { return ___target_0; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05_H
#ifndef UTILS_TF7D730835163762D9317B6FB65E30C704BD3BF7F_H
#define UTILS_TF7D730835163762D9317B6FB65E30C704BD3BF7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI_Utils
struct  Utils_tF7D730835163762D9317B6FB65E30C704BD3BF7F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_TF7D730835163762D9317B6FB65E30C704BD3BF7F_H
#ifndef CLASSUTILS_T379F9EC7BB5DAFF4671B33798096B694B0105537_H
#define CLASSUTILS_T379F9EC7BB5DAFF4671B33798096B694B0105537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Extensions.ClassUtils
struct  ClassUtils_t379F9EC7BB5DAFF4671B33798096B694B0105537  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSUTILS_T379F9EC7BB5DAFF4671B33798096B694B0105537_H
#ifndef COLOREXTENSIONS_T9ECD992F74C8A039DBC62CD2E384DAD3164DF4FC_H
#define COLOREXTENSIONS_T9ECD992F74C8A039DBC62CD2E384DAD3164DF4FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Extensions.ColorExtensions
struct  ColorExtensions_t9ECD992F74C8A039DBC62CD2E384DAD3164DF4FC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOREXTENSIONS_T9ECD992F74C8A039DBC62CD2E384DAD3164DF4FC_H
#ifndef U3CU3EC_T8D9C2D68963FDD49864A9E337708F024EF48F38E_H
#define U3CU3EC_T8D9C2D68963FDD49864A9E337708F024EF48F38E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Extensions.RectExtensions_<>c
struct  U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E_StaticFields
{
public:
	// Doozy.Engine.Extensions.RectExtensions_<>c Doozy.Engine.Extensions.RectExtensions_<>c::<>9
	U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Rect,System.Single> Doozy.Engine.Extensions.RectExtensions_<>c::<>9__37_0
	Func_2_tB2BBA2235D0762C1D488C18DDDB7D454EC1D9380 * ___U3CU3E9__37_0_1;
	// System.Func`2<UnityEngine.Rect,System.Single> Doozy.Engine.Extensions.RectExtensions_<>c::<>9__37_1
	Func_2_tB2BBA2235D0762C1D488C18DDDB7D454EC1D9380 * ___U3CU3E9__37_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E_StaticFields, ___U3CU3E9__37_0_1)); }
	inline Func_2_tB2BBA2235D0762C1D488C18DDDB7D454EC1D9380 * get_U3CU3E9__37_0_1() const { return ___U3CU3E9__37_0_1; }
	inline Func_2_tB2BBA2235D0762C1D488C18DDDB7D454EC1D9380 ** get_address_of_U3CU3E9__37_0_1() { return &___U3CU3E9__37_0_1; }
	inline void set_U3CU3E9__37_0_1(Func_2_tB2BBA2235D0762C1D488C18DDDB7D454EC1D9380 * value)
	{
		___U3CU3E9__37_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E_StaticFields, ___U3CU3E9__37_1_2)); }
	inline Func_2_tB2BBA2235D0762C1D488C18DDDB7D454EC1D9380 * get_U3CU3E9__37_1_2() const { return ___U3CU3E9__37_1_2; }
	inline Func_2_tB2BBA2235D0762C1D488C18DDDB7D454EC1D9380 ** get_address_of_U3CU3E9__37_1_2() { return &___U3CU3E9__37_1_2; }
	inline void set_U3CU3E9__37_1_2(Func_2_tB2BBA2235D0762C1D488C18DDDB7D454EC1D9380 * value)
	{
		___U3CU3E9__37_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T8D9C2D68963FDD49864A9E337708F024EF48F38E_H
#ifndef U3CU3EC__DISPLAYCLASS37_0_T5A2E68FA6B3433A283A0D75E6763FE3075C72C4D_H
#define U3CU3EC__DISPLAYCLASS37_0_T5A2E68FA6B3433A283A0D75E6763FE3075C72C4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Extensions.RectExtensions_<>c__DisplayClass37_0
struct  U3CU3Ec__DisplayClass37_0_t5A2E68FA6B3433A283A0D75E6763FE3075C72C4D  : public RuntimeObject
{
public:
	// System.Single Doozy.Engine.Extensions.RectExtensions_<>c__DisplayClass37_0::x
	float ___x_0;
	// System.Single Doozy.Engine.Extensions.RectExtensions_<>c__DisplayClass37_0::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t5A2E68FA6B3433A283A0D75E6763FE3075C72C4D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t5A2E68FA6B3433A283A0D75E6763FE3075C72C4D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS37_0_T5A2E68FA6B3433A283A0D75E6763FE3075C72C4D_H
#ifndef RECTTRANSFORMEXTENSIONS_TD52A31E4FDFCF5BD5727BE8110099C6FD967F811_H
#define RECTTRANSFORMEXTENSIONS_TD52A31E4FDFCF5BD5727BE8110099C6FD967F811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Extensions.RectTransformExtensions
struct  RectTransformExtensions_tD52A31E4FDFCF5BD5727BE8110099C6FD967F811  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMEXTENSIONS_TD52A31E4FDFCF5BD5727BE8110099C6FD967F811_H
#ifndef PASSTHROUGHCONNECTION_TF2D74D39C4C4A2639441B6C52F7C6EF7EB3AB03A_H
#define PASSTHROUGHCONNECTION_TF2D74D39C4C4A2639441B6C52F7C6EF7EB3AB03A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Connections.PassthroughConnection
struct  PassthroughConnection_tF2D74D39C4C4A2639441B6C52F7C6EF7EB3AB03A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSTHROUGHCONNECTION_TF2D74D39C4C4A2639441B6C52F7C6EF7EB3AB03A_H
#ifndef U3CINITIALIZEGRAPHENUMERATORU3ED__32_T0097E6EE24AD048D8871F3B8CB08A92D033FCA46_H
#define U3CINITIALIZEGRAPHENUMERATORU3ED__32_T0097E6EE24AD048D8871F3B8CB08A92D033FCA46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.GraphController_<InitializeGraphEnumerator>d__32
struct  U3CInitializeGraphEnumeratorU3Ed__32_t0097E6EE24AD048D8871F3B8CB08A92D033FCA46  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.Nody.GraphController_<InitializeGraphEnumerator>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.Nody.GraphController_<InitializeGraphEnumerator>d__32::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.Nody.GraphController Doozy.Engine.Nody.GraphController_<InitializeGraphEnumerator>d__32::<>4__this
	GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeGraphEnumeratorU3Ed__32_t0097E6EE24AD048D8871F3B8CB08A92D033FCA46, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeGraphEnumeratorU3Ed__32_t0097E6EE24AD048D8871F3B8CB08A92D033FCA46, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeGraphEnumeratorU3Ed__32_t0097E6EE24AD048D8871F3B8CB08A92D033FCA46, ___U3CU3E4__this_2)); }
	inline GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZEGRAPHENUMERATORU3ED__32_T0097E6EE24AD048D8871F3B8CB08A92D033FCA46_H
#ifndef SOURCEINFO_TE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2_H
#define SOURCEINFO_TE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Nodes.SwitchBackNode_SourceInfo
struct  SourceInfo_tE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2  : public RuntimeObject
{
public:
	// System.String Doozy.Engine.Nody.Nodes.SwitchBackNode_SourceInfo::SourceName
	String_t* ___SourceName_0;
	// System.String Doozy.Engine.Nody.Nodes.SwitchBackNode_SourceInfo::InputSocketId
	String_t* ___InputSocketId_1;
	// System.String Doozy.Engine.Nody.Nodes.SwitchBackNode_SourceInfo::OutputSocketId
	String_t* ___OutputSocketId_2;
	// System.Boolean Doozy.Engine.Nody.Nodes.SwitchBackNode_SourceInfo::InputSocketIsConnected
	bool ___InputSocketIsConnected_3;
	// System.Boolean Doozy.Engine.Nody.Nodes.SwitchBackNode_SourceInfo::OutputSocketIsConnected
	bool ___OutputSocketIsConnected_4;

public:
	inline static int32_t get_offset_of_SourceName_0() { return static_cast<int32_t>(offsetof(SourceInfo_tE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2, ___SourceName_0)); }
	inline String_t* get_SourceName_0() const { return ___SourceName_0; }
	inline String_t** get_address_of_SourceName_0() { return &___SourceName_0; }
	inline void set_SourceName_0(String_t* value)
	{
		___SourceName_0 = value;
		Il2CppCodeGenWriteBarrier((&___SourceName_0), value);
	}

	inline static int32_t get_offset_of_InputSocketId_1() { return static_cast<int32_t>(offsetof(SourceInfo_tE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2, ___InputSocketId_1)); }
	inline String_t* get_InputSocketId_1() const { return ___InputSocketId_1; }
	inline String_t** get_address_of_InputSocketId_1() { return &___InputSocketId_1; }
	inline void set_InputSocketId_1(String_t* value)
	{
		___InputSocketId_1 = value;
		Il2CppCodeGenWriteBarrier((&___InputSocketId_1), value);
	}

	inline static int32_t get_offset_of_OutputSocketId_2() { return static_cast<int32_t>(offsetof(SourceInfo_tE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2, ___OutputSocketId_2)); }
	inline String_t* get_OutputSocketId_2() const { return ___OutputSocketId_2; }
	inline String_t** get_address_of_OutputSocketId_2() { return &___OutputSocketId_2; }
	inline void set_OutputSocketId_2(String_t* value)
	{
		___OutputSocketId_2 = value;
		Il2CppCodeGenWriteBarrier((&___OutputSocketId_2), value);
	}

	inline static int32_t get_offset_of_InputSocketIsConnected_3() { return static_cast<int32_t>(offsetof(SourceInfo_tE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2, ___InputSocketIsConnected_3)); }
	inline bool get_InputSocketIsConnected_3() const { return ___InputSocketIsConnected_3; }
	inline bool* get_address_of_InputSocketIsConnected_3() { return &___InputSocketIsConnected_3; }
	inline void set_InputSocketIsConnected_3(bool value)
	{
		___InputSocketIsConnected_3 = value;
	}

	inline static int32_t get_offset_of_OutputSocketIsConnected_4() { return static_cast<int32_t>(offsetof(SourceInfo_tE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2, ___OutputSocketIsConnected_4)); }
	inline bool get_OutputSocketIsConnected_4() const { return ___OutputSocketIsConnected_4; }
	inline bool* get_address_of_OutputSocketIsConnected_4() { return &___OutputSocketIsConnected_4; }
	inline void set_OutputSocketIsConnected_4(bool value)
	{
		___OutputSocketIsConnected_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOURCEINFO_TE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2_H
#ifndef U3CU3EC_T25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E_H
#define U3CU3EC_T25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.ProgressorGroup_<>c
struct  U3CU3Ec_t25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E_StaticFields
{
public:
	// Doozy.Engine.Progress.ProgressorGroup_<>c Doozy.Engine.Progress.ProgressorGroup_<>c::<>9
	U3CU3Ec_t25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E * ___U3CU3E9_0;
	// System.Func`2<Doozy.Engine.Progress.Progressor,System.Boolean> Doozy.Engine.Progress.ProgressorGroup_<>c::<>9__20_0
	Func_2_t6F58E1CB2804FA531F97321E15F0E4A6015D7061 * ___U3CU3E9__20_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__20_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E_StaticFields, ___U3CU3E9__20_0_1)); }
	inline Func_2_t6F58E1CB2804FA531F97321E15F0E4A6015D7061 * get_U3CU3E9__20_0_1() const { return ___U3CU3E9__20_0_1; }
	inline Func_2_t6F58E1CB2804FA531F97321E15F0E4A6015D7061 ** get_address_of_U3CU3E9__20_0_1() { return &___U3CU3E9__20_0_1; }
	inline void set_U3CU3E9__20_0_1(Func_2_t6F58E1CB2804FA531F97321E15F0E4A6015D7061 * value)
	{
		___U3CU3E9__20_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__20_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E_H
#ifndef SCENELOADBEHAVIOR_T09385F7DAC85769326248B04A71A5434C14D45AF_H
#define SCENELOADBEHAVIOR_T09385F7DAC85769326248B04A71A5434C14D45AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SceneManagement.SceneLoadBehavior
struct  SceneLoadBehavior_t09385F7DAC85769326248B04A71A5434C14D45AF  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.Base.UIAction Doozy.Engine.SceneManagement.SceneLoadBehavior::OnLoadScene
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * ___OnLoadScene_0;
	// Doozy.Engine.UI.Base.UIAction Doozy.Engine.SceneManagement.SceneLoadBehavior::OnSceneLoaded
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * ___OnSceneLoaded_1;

public:
	inline static int32_t get_offset_of_OnLoadScene_0() { return static_cast<int32_t>(offsetof(SceneLoadBehavior_t09385F7DAC85769326248B04A71A5434C14D45AF, ___OnLoadScene_0)); }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * get_OnLoadScene_0() const { return ___OnLoadScene_0; }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 ** get_address_of_OnLoadScene_0() { return &___OnLoadScene_0; }
	inline void set_OnLoadScene_0(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * value)
	{
		___OnLoadScene_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnLoadScene_0), value);
	}

	inline static int32_t get_offset_of_OnSceneLoaded_1() { return static_cast<int32_t>(offsetof(SceneLoadBehavior_t09385F7DAC85769326248B04A71A5434C14D45AF, ___OnSceneLoaded_1)); }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * get_OnSceneLoaded_1() const { return ___OnSceneLoaded_1; }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 ** get_address_of_OnSceneLoaded_1() { return &___OnSceneLoaded_1; }
	inline void set_OnSceneLoaded_1(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * value)
	{
		___OnSceneLoaded_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnSceneLoaded_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENELOADBEHAVIOR_T09385F7DAC85769326248B04A71A5434C14D45AF_H
#ifndef U3CSELFDESTRUCTU3ED__61_T16E49D474D5F94504CCFB97853EA0E6841DEA028_H
#define U3CSELFDESTRUCTU3ED__61_T16E49D474D5F94504CCFB97853EA0E6841DEA028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SceneManagement.SceneLoader_<SelfDestruct>d__61
struct  U3CSelfDestructU3Ed__61_t16E49D474D5F94504CCFB97853EA0E6841DEA028  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.SceneManagement.SceneLoader_<SelfDestruct>d__61::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.SceneManagement.SceneLoader_<SelfDestruct>d__61::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.SceneManagement.SceneLoader Doozy.Engine.SceneManagement.SceneLoader_<SelfDestruct>d__61::<>4__this
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSelfDestructU3Ed__61_t16E49D474D5F94504CCFB97853EA0E6841DEA028, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSelfDestructU3Ed__61_t16E49D474D5F94504CCFB97853EA0E6841DEA028, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSelfDestructU3Ed__61_t16E49D474D5F94504CCFB97853EA0E6841DEA028, ___U3CU3E4__this_2)); }
	inline SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSELFDESTRUCTU3ED__61_T16E49D474D5F94504CCFB97853EA0E6841DEA028_H
#ifndef U3CU3EC_TB47CCCB4810E026898C7B7306A51ED6D85E61E1D_H
#define U3CU3EC_TB47CCCB4810E026898C7B7306A51ED6D85E61E1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundyController_<>c
struct  U3CU3Ec_tB47CCCB4810E026898C7B7306A51ED6D85E61E1D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB47CCCB4810E026898C7B7306A51ED6D85E61E1D_StaticFields
{
public:
	// Doozy.Engine.Soundy.SoundyController_<>c Doozy.Engine.Soundy.SoundyController_<>c::<>9
	U3CU3Ec_tB47CCCB4810E026898C7B7306A51ED6D85E61E1D * ___U3CU3E9_0;
	// System.Func`2<Doozy.Engine.Soundy.SoundyController,System.Boolean> Doozy.Engine.Soundy.SoundyController_<>c::<>9__56_0
	Func_2_t937B00FFBED8AFA80585DA4C4776787F1DEF7E72 * ___U3CU3E9__56_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB47CCCB4810E026898C7B7306A51ED6D85E61E1D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB47CCCB4810E026898C7B7306A51ED6D85E61E1D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB47CCCB4810E026898C7B7306A51ED6D85E61E1D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB47CCCB4810E026898C7B7306A51ED6D85E61E1D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__56_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB47CCCB4810E026898C7B7306A51ED6D85E61E1D_StaticFields, ___U3CU3E9__56_0_1)); }
	inline Func_2_t937B00FFBED8AFA80585DA4C4776787F1DEF7E72 * get_U3CU3E9__56_0_1() const { return ___U3CU3E9__56_0_1; }
	inline Func_2_t937B00FFBED8AFA80585DA4C4776787F1DEF7E72 ** get_address_of_U3CU3E9__56_0_1() { return &___U3CU3E9__56_0_1; }
	inline void set_U3CU3E9__56_0_1(Func_2_t937B00FFBED8AFA80585DA4C4776787F1DEF7E72 * value)
	{
		___U3CU3E9__56_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__56_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB47CCCB4810E026898C7B7306A51ED6D85E61E1D_H
#ifndef U3CU3EC_TEAFC2663C9C29EC1AE07289F84C469987E3C9535_H
#define U3CU3EC_TEAFC2663C9C29EC1AE07289F84C469987E3C9535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundyPooler_<>c
struct  U3CU3Ec_tEAFC2663C9C29EC1AE07289F84C469987E3C9535  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tEAFC2663C9C29EC1AE07289F84C469987E3C9535_StaticFields
{
public:
	// Doozy.Engine.Soundy.SoundyPooler_<>c Doozy.Engine.Soundy.SoundyPooler_<>c::<>9
	U3CU3Ec_tEAFC2663C9C29EC1AE07289F84C469987E3C9535 * ___U3CU3E9_0;
	// System.Func`2<Doozy.Engine.Soundy.SoundyController,System.Boolean> Doozy.Engine.Soundy.SoundyPooler_<>c::<>9__28_0
	Func_2_t937B00FFBED8AFA80585DA4C4776787F1DEF7E72 * ___U3CU3E9__28_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEAFC2663C9C29EC1AE07289F84C469987E3C9535_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tEAFC2663C9C29EC1AE07289F84C469987E3C9535 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tEAFC2663C9C29EC1AE07289F84C469987E3C9535 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tEAFC2663C9C29EC1AE07289F84C469987E3C9535 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__28_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEAFC2663C9C29EC1AE07289F84C469987E3C9535_StaticFields, ___U3CU3E9__28_0_1)); }
	inline Func_2_t937B00FFBED8AFA80585DA4C4776787F1DEF7E72 * get_U3CU3E9__28_0_1() const { return ___U3CU3E9__28_0_1; }
	inline Func_2_t937B00FFBED8AFA80585DA4C4776787F1DEF7E72 ** get_address_of_U3CU3E9__28_0_1() { return &___U3CU3E9__28_0_1; }
	inline void set_U3CU3E9__28_0_1(Func_2_t937B00FFBED8AFA80585DA4C4776787F1DEF7E72 * value)
	{
		___U3CU3E9__28_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__28_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TEAFC2663C9C29EC1AE07289F84C469987E3C9535_H
#ifndef U3CKILLIDLECONTROLLERSENUMERATORU3ED__29_T2905789CD4F4443550082A565343E00767D3D558_H
#define U3CKILLIDLECONTROLLERSENUMERATORU3ED__29_T2905789CD4F4443550082A565343E00767D3D558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundyPooler_<KillIdleControllersEnumerator>d__29
struct  U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.Soundy.SoundyPooler_<KillIdleControllersEnumerator>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.Soundy.SoundyPooler_<KillIdleControllersEnumerator>d__29::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.Soundy.SoundyPooler Doozy.Engine.Soundy.SoundyPooler_<KillIdleControllersEnumerator>d__29::<>4__this
	SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282 * ___U3CU3E4__this_2;
	// System.Int32 Doozy.Engine.Soundy.SoundyPooler_<KillIdleControllersEnumerator>d__29::<minimumNumberOfControllers>5__1
	int32_t ___U3CminimumNumberOfControllersU3E5__1_3;
	// System.Single Doozy.Engine.Soundy.SoundyPooler_<KillIdleControllersEnumerator>d__29::<controllerIdleKillDuration>5__2
	float ___U3CcontrollerIdleKillDurationU3E5__2_4;
	// System.Int32 Doozy.Engine.Soundy.SoundyPooler_<KillIdleControllersEnumerator>d__29::<i>5__3
	int32_t ___U3CiU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558, ___U3CU3E4__this_2)); }
	inline SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CminimumNumberOfControllersU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558, ___U3CminimumNumberOfControllersU3E5__1_3)); }
	inline int32_t get_U3CminimumNumberOfControllersU3E5__1_3() const { return ___U3CminimumNumberOfControllersU3E5__1_3; }
	inline int32_t* get_address_of_U3CminimumNumberOfControllersU3E5__1_3() { return &___U3CminimumNumberOfControllersU3E5__1_3; }
	inline void set_U3CminimumNumberOfControllersU3E5__1_3(int32_t value)
	{
		___U3CminimumNumberOfControllersU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CcontrollerIdleKillDurationU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558, ___U3CcontrollerIdleKillDurationU3E5__2_4)); }
	inline float get_U3CcontrollerIdleKillDurationU3E5__2_4() const { return ___U3CcontrollerIdleKillDurationU3E5__2_4; }
	inline float* get_address_of_U3CcontrollerIdleKillDurationU3E5__2_4() { return &___U3CcontrollerIdleKillDurationU3E5__2_4; }
	inline void set_U3CcontrollerIdleKillDurationU3E5__2_4(float value)
	{
		___U3CcontrollerIdleKillDurationU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558, ___U3CiU3E5__3_5)); }
	inline int32_t get_U3CiU3E5__3_5() const { return ___U3CiU3E5__3_5; }
	inline int32_t* get_address_of_U3CiU3E5__3_5() { return &___U3CiU3E5__3_5; }
	inline void set_U3CiU3E5__3_5(int32_t value)
	{
		___U3CiU3E5__3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CKILLIDLECONTROLLERSENUMERATORU3ED__29_T2905789CD4F4443550082A565343E00767D3D558_H
#ifndef SOUNDYUTILS_T9EE5EFFD61B9D1B30391E6E0093714AE15664040_H
#define SOUNDYUTILS_T9EE5EFFD61B9D1B30391E6E0093714AE15664040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundyUtils
struct  SoundyUtils_t9EE5EFFD61B9D1B30391E6E0093714AE15664040  : public RuntimeObject
{
public:

public:
};

struct SoundyUtils_t9EE5EFFD61B9D1B30391E6E0093714AE15664040_StaticFields
{
public:
	// System.Single Doozy.Engine.Soundy.SoundyUtils::TwelfthRootOfTwo
	float ___TwelfthRootOfTwo_0;

public:
	inline static int32_t get_offset_of_TwelfthRootOfTwo_0() { return static_cast<int32_t>(offsetof(SoundyUtils_t9EE5EFFD61B9D1B30391E6E0093714AE15664040_StaticFields, ___TwelfthRootOfTwo_0)); }
	inline float get_TwelfthRootOfTwo_0() const { return ___TwelfthRootOfTwo_0; }
	inline float* get_address_of_TwelfthRootOfTwo_0() { return &___TwelfthRootOfTwo_0; }
	inline void set_TwelfthRootOfTwo_0(float value)
	{
		___TwelfthRootOfTwo_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDYUTILS_T9EE5EFFD61B9D1B30391E6E0093714AE15664040_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef MINMAXRANGEATTRIBUTE_T0403C3BF156C9F9122007D03EE93B8F74FD1D025_H
#define MINMAXRANGEATTRIBUTE_T0403C3BF156C9F9122007D03EE93B8F74FD1D025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Attributes.MinMaxRangeAttribute
struct  MinMaxRangeAttribute_t0403C3BF156C9F9122007D03EE93B8F74FD1D025  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Single Doozy.Engine.Attributes.MinMaxRangeAttribute::<Min>k__BackingField
	float ___U3CMinU3Ek__BackingField_0;
	// System.Single Doozy.Engine.Attributes.MinMaxRangeAttribute::<Max>k__BackingField
	float ___U3CMaxU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMinU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MinMaxRangeAttribute_t0403C3BF156C9F9122007D03EE93B8F74FD1D025, ___U3CMinU3Ek__BackingField_0)); }
	inline float get_U3CMinU3Ek__BackingField_0() const { return ___U3CMinU3Ek__BackingField_0; }
	inline float* get_address_of_U3CMinU3Ek__BackingField_0() { return &___U3CMinU3Ek__BackingField_0; }
	inline void set_U3CMinU3Ek__BackingField_0(float value)
	{
		___U3CMinU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaxU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MinMaxRangeAttribute_t0403C3BF156C9F9122007D03EE93B8F74FD1D025, ___U3CMaxU3Ek__BackingField_1)); }
	inline float get_U3CMaxU3Ek__BackingField_1() const { return ___U3CMaxU3Ek__BackingField_1; }
	inline float* get_address_of_U3CMaxU3Ek__BackingField_1() { return &___U3CMaxU3Ek__BackingField_1; }
	inline void set_U3CMaxU3Ek__BackingField_1(float value)
	{
		___U3CMaxU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINMAXRANGEATTRIBUTE_T0403C3BF156C9F9122007D03EE93B8F74FD1D025_H
#ifndef NODEMENU_T78619524EE5A6B30E1DC06320AB591EC49A3AA52_H
#define NODEMENU_T78619524EE5A6B30E1DC06320AB591EC49A3AA52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Attributes.NodeMenu
struct  NodeMenu_t78619524EE5A6B30E1DC06320AB591EC49A3AA52  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Doozy.Engine.Nody.Attributes.NodeMenu::MenuName
	String_t* ___MenuName_0;
	// System.Int32 Doozy.Engine.Nody.Attributes.NodeMenu::Order
	int32_t ___Order_1;
	// System.Boolean Doozy.Engine.Nody.Attributes.NodeMenu::AddSeparatorAfter
	bool ___AddSeparatorAfter_2;
	// System.Boolean Doozy.Engine.Nody.Attributes.NodeMenu::AddSeparatorBefore
	bool ___AddSeparatorBefore_3;

public:
	inline static int32_t get_offset_of_MenuName_0() { return static_cast<int32_t>(offsetof(NodeMenu_t78619524EE5A6B30E1DC06320AB591EC49A3AA52, ___MenuName_0)); }
	inline String_t* get_MenuName_0() const { return ___MenuName_0; }
	inline String_t** get_address_of_MenuName_0() { return &___MenuName_0; }
	inline void set_MenuName_0(String_t* value)
	{
		___MenuName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MenuName_0), value);
	}

	inline static int32_t get_offset_of_Order_1() { return static_cast<int32_t>(offsetof(NodeMenu_t78619524EE5A6B30E1DC06320AB591EC49A3AA52, ___Order_1)); }
	inline int32_t get_Order_1() const { return ___Order_1; }
	inline int32_t* get_address_of_Order_1() { return &___Order_1; }
	inline void set_Order_1(int32_t value)
	{
		___Order_1 = value;
	}

	inline static int32_t get_offset_of_AddSeparatorAfter_2() { return static_cast<int32_t>(offsetof(NodeMenu_t78619524EE5A6B30E1DC06320AB591EC49A3AA52, ___AddSeparatorAfter_2)); }
	inline bool get_AddSeparatorAfter_2() const { return ___AddSeparatorAfter_2; }
	inline bool* get_address_of_AddSeparatorAfter_2() { return &___AddSeparatorAfter_2; }
	inline void set_AddSeparatorAfter_2(bool value)
	{
		___AddSeparatorAfter_2 = value;
	}

	inline static int32_t get_offset_of_AddSeparatorBefore_3() { return static_cast<int32_t>(offsetof(NodeMenu_t78619524EE5A6B30E1DC06320AB591EC49A3AA52, ___AddSeparatorBefore_3)); }
	inline bool get_AddSeparatorBefore_3() const { return ___AddSeparatorBefore_3; }
	inline bool* get_address_of_AddSeparatorBefore_3() { return &___AddSeparatorBefore_3; }
	inline void set_AddSeparatorBefore_3(bool value)
	{
		___AddSeparatorBefore_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEMENU_T78619524EE5A6B30E1DC06320AB591EC49A3AA52_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef UNITYEVENT_1_T76E58B9E749D9FE71072475712C59DD401882082_H
#define UNITYEVENT_1_T76E58B9E749D9FE71072475712C59DD401882082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Doozy.Engine.Orientation.DetectedOrientation>
struct  UnityEvent_1_t76E58B9E749D9FE71072475712C59DD401882082  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t76E58B9E749D9FE71072475712C59DD401882082, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T76E58B9E749D9FE71072475712C59DD401882082_H
#ifndef UNITYEVENT_1_T6FE5C79FD433599728A9AA732E588823AB88FDB5_H
#define UNITYEVENT_1_T6FE5C79FD433599728A9AA732E588823AB88FDB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T6FE5C79FD433599728A9AA732E588823AB88FDB5_H
#ifndef UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#define UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifndef UNITYEVENT_1_TACA444CD8B2CBDCD9393629F06117A47C27A8F15_H
#define UNITYEVENT_1_TACA444CD8B2CBDCD9393629F06117A47C27A8F15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_tACA444CD8B2CBDCD9393629F06117A47C27A8F15  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_tACA444CD8B2CBDCD9393629F06117A47C27A8F15, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TACA444CD8B2CBDCD9393629F06117A47C27A8F15_H
#ifndef UNITYEVENT_1_TDBAAAC82108B473A2F8F69586B33ED12EA5D86CE_H
#define UNITYEVENT_1_TDBAAAC82108B473A2F8F69586B33ED12EA5D86CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>
struct  UnityEvent_1_tDBAAAC82108B473A2F8F69586B33ED12EA5D86CE  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_tDBAAAC82108B473A2F8F69586B33ED12EA5D86CE, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TDBAAAC82108B473A2F8F69586B33ED12EA5D86CE_H
#ifndef UNITYEVENT_1_T33DD8AB59BBD10CD0C6BCA30D04B19934D43AEF5_H
#define UNITYEVENT_1_T33DD8AB59BBD10CD0C6BCA30D04B19934D43AEF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.SceneManagement.Scene>
struct  UnityEvent_1_t33DD8AB59BBD10CD0C6BCA30D04B19934D43AEF5  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t33DD8AB59BBD10CD0C6BCA30D04B19934D43AEF5, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T33DD8AB59BBD10CD0C6BCA30D04B19934D43AEF5_H
#ifndef UNITYEVENT_2_TF1F8B1C816B050524CB9BFBC4DD15218881397D1_H
#define UNITYEVENT_2_TF1F8B1C816B050524CB9BFBC4DD15218881397D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct  UnityEvent_2_tF1F8B1C816B050524CB9BFBC4DD15218881397D1  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_tF1F8B1C816B050524CB9BFBC4DD15218881397D1, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_TF1F8B1C816B050524CB9BFBC4DD15218881397D1_H
#ifndef UNITYEVENT_2_T01112610AD6911857DFEC961B0DEFC076775875F_H
#define UNITYEVENT_2_T01112610AD6911857DFEC961B0DEFC076775875F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct  UnityEvent_2_t01112610AD6911857DFEC961B0DEFC076775875F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t01112610AD6911857DFEC961B0DEFC076775875F, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T01112610AD6911857DFEC961B0DEFC076775875F_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T43377B7F5E091119F288FC2F606BD0CCF9764CD5_H
#define U3CU3EC__DISPLAYCLASS6_0_T43377B7F5E091119F288FC2F606BD0CCF9764CD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;
	// System.Single DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::s
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___s_4;
	// UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::endValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___endValue_5;
	// DG.Tweening.Tween DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::yTween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___yTween_6;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_startPosY_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___startPosY_1)); }
	inline float get_startPosY_1() const { return ___startPosY_1; }
	inline float* get_address_of_startPosY_1() { return &___startPosY_1; }
	inline void set_startPosY_1(float value)
	{
		___startPosY_1 = value;
	}

	inline static int32_t get_offset_of_offsetYSet_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___offsetYSet_2)); }
	inline bool get_offsetYSet_2() const { return ___offsetYSet_2; }
	inline bool* get_address_of_offsetYSet_2() { return &___offsetYSet_2; }
	inline void set_offsetYSet_2(bool value)
	{
		___offsetYSet_2 = value;
	}

	inline static int32_t get_offset_of_offsetY_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___offsetY_3)); }
	inline float get_offsetY_3() const { return ___offsetY_3; }
	inline float* get_address_of_offsetY_3() { return &___offsetY_3; }
	inline void set_offsetY_3(float value)
	{
		___offsetY_3 = value;
	}

	inline static int32_t get_offset_of_s_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___s_4)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_s_4() const { return ___s_4; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_s_4() { return &___s_4; }
	inline void set_s_4(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___s_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_4), value);
	}

	inline static int32_t get_offset_of_endValue_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___endValue_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_endValue_5() const { return ___endValue_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_endValue_5() { return &___endValue_5; }
	inline void set_endValue_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___endValue_5 = value;
	}

	inline static int32_t get_offset_of_yTween_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___yTween_6)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_yTween_6() const { return ___yTween_6; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_yTween_6() { return &___yTween_6; }
	inline void set_yTween_6(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___yTween_6 = value;
		Il2CppCodeGenWriteBarrier((&___yTween_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T43377B7F5E091119F288FC2F606BD0CCF9764CD5_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T7F406E4E06939C6DCA0E180DA0A06F4D928893A7_H
#define U3CU3EC__DISPLAYCLASS4_0_T7F406E4E06939C6DCA0E180DA0A06F4D928893A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::target
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___target_0;
	// System.Single DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::s
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___s_4;
	// UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::endValue
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___endValue_5;
	// DG.Tweening.Tween DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::yTween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___yTween_6;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___target_0)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_startPosY_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___startPosY_1)); }
	inline float get_startPosY_1() const { return ___startPosY_1; }
	inline float* get_address_of_startPosY_1() { return &___startPosY_1; }
	inline void set_startPosY_1(float value)
	{
		___startPosY_1 = value;
	}

	inline static int32_t get_offset_of_offsetYSet_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___offsetYSet_2)); }
	inline bool get_offsetYSet_2() const { return ___offsetYSet_2; }
	inline bool* get_address_of_offsetYSet_2() { return &___offsetYSet_2; }
	inline void set_offsetYSet_2(bool value)
	{
		___offsetYSet_2 = value;
	}

	inline static int32_t get_offset_of_offsetY_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___offsetY_3)); }
	inline float get_offsetY_3() const { return ___offsetY_3; }
	inline float* get_address_of_offsetY_3() { return &___offsetY_3; }
	inline void set_offsetY_3(float value)
	{
		___offsetY_3 = value;
	}

	inline static int32_t get_offset_of_s_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___s_4)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_s_4() const { return ___s_4; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_s_4() { return &___s_4; }
	inline void set_s_4(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___s_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_4), value);
	}

	inline static int32_t get_offset_of_endValue_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___endValue_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_endValue_5() const { return ___endValue_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_endValue_5() { return &___endValue_5; }
	inline void set_endValue_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___endValue_5 = value;
	}

	inline static int32_t get_offset_of_yTween_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___yTween_6)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_yTween_6() const { return ___yTween_6; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_yTween_6() { return &___yTween_6; }
	inline void set_yTween_6(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___yTween_6 = value;
		Il2CppCodeGenWriteBarrier((&___yTween_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T7F406E4E06939C6DCA0E180DA0A06F4D928893A7_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B_H
#define U3CU3EC__DISPLAYCLASS3_0_T23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::to
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___to_0;
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::target
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B, ___to_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_to_0() const { return ___to_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B, ___target_1)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_target_1() const { return ___target_1; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B_H
#ifndef EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#define EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifndef PARAMETERTYPE_T8D9374EA942C50D46303896C40AB7D37917E74CF_H
#define PARAMETERTYPE_T8D9374EA942C50D46303896C40AB7D37917E74CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Events.AnimatorEvent_ParameterType
struct  ParameterType_t8D9374EA942C50D46303896C40AB7D37917E74CF 
{
public:
	// System.Int32 Doozy.Engine.Events.AnimatorEvent_ParameterType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParameterType_t8D9374EA942C50D46303896C40AB7D37917E74CF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERTYPE_T8D9374EA942C50D46303896C40AB7D37917E74CF_H
#ifndef BOOLEVENT_TC2164AC1844ACBA9870E7D339F6FD57462A40D3B_H
#define BOOLEVENT_TC2164AC1844ACBA9870E7D339F6FD57462A40D3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Events.BoolEvent
struct  BoolEvent_tC2164AC1844ACBA9870E7D339F6FD57462A40D3B  : public UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEVENT_TC2164AC1844ACBA9870E7D339F6FD57462A40D3B_H
#ifndef GAMEOBJECTEVENT_TAD0B88F693C994943924525B6521DE200B874CCE_H
#define GAMEOBJECTEVENT_TAD0B88F693C994943924525B6521DE200B874CCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Events.GameObjectEvent
struct  GameObjectEvent_tAD0B88F693C994943924525B6521DE200B874CCE  : public UnityEvent_1_tDBAAAC82108B473A2F8F69586B33ED12EA5D86CE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEVENT_TAD0B88F693C994943924525B6521DE200B874CCE_H
#ifndef STRINGEVENT_T03BF969408548F9F12B69569AAED599BE5BC9FB6_H
#define STRINGEVENT_T03BF969408548F9F12B69569AAED599BE5BC9FB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Events.StringEvent
struct  StringEvent_t03BF969408548F9F12B69569AAED599BE5BC9FB6  : public UnityEvent_1_tACA444CD8B2CBDCD9393629F06117A47C27A8F15
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGEVENT_T03BF969408548F9F12B69569AAED599BE5BC9FB6_H
#ifndef RECTEXTENSIONS_T7FB4B606FB0775D16CA27A6BE2B71AB0CAAAF3AA_H
#define RECTEXTENSIONS_T7FB4B606FB0775D16CA27A6BE2B71AB0CAAAF3AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Extensions.RectExtensions
struct  RectExtensions_t7FB4B606FB0775D16CA27A6BE2B71AB0CAAAF3AA  : public RuntimeObject
{
public:

public:
};

struct RectExtensions_t7FB4B606FB0775D16CA27A6BE2B71AB0CAAAF3AA_StaticFields
{
public:
	// UnityEngine.Vector2 Doozy.Engine.Extensions.RectExtensions::s_tmpTopLeft
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___s_tmpTopLeft_0;

public:
	inline static int32_t get_offset_of_s_tmpTopLeft_0() { return static_cast<int32_t>(offsetof(RectExtensions_t7FB4B606FB0775D16CA27A6BE2B71AB0CAAAF3AA_StaticFields, ___s_tmpTopLeft_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_s_tmpTopLeft_0() const { return ___s_tmpTopLeft_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_s_tmpTopLeft_0() { return &___s_tmpTopLeft_0; }
	inline void set_s_tmpTopLeft_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___s_tmpTopLeft_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTEXTENSIONS_T7FB4B606FB0775D16CA27A6BE2B71AB0CAAAF3AA_H
#ifndef LANGUAGE_TF0C90ACCEE1E928D8C7CCF1DCB469095E48EC874_H
#define LANGUAGE_TF0C90ACCEE1E928D8C7CCF1DCB469095E48EC874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Language
struct  Language_tF0C90ACCEE1E928D8C7CCF1DCB469095E48EC874 
{
public:
	// System.Int32 Doozy.Engine.Language::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Language_tF0C90ACCEE1E928D8C7CCF1DCB469095E48EC874, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGE_TF0C90ACCEE1E928D8C7CCF1DCB469095E48EC874_H
#ifndef CONNECTION_T245E995D76298FBABC194CFFAFF0FDFEF8030EA6_H
#define CONNECTION_T245E995D76298FBABC194CFFAFF0FDFEF8030EA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Models.Connection
struct  Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6  : public RuntimeObject
{
public:
	// System.Boolean Doozy.Engine.Nody.Models.Connection::<Ping>k__BackingField
	bool ___U3CPingU3Ek__BackingField_0;
	// UnityEngine.Vector2 Doozy.Engine.Nody.Models.Connection::m_inputConnectionPoint
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_inputConnectionPoint_1;
	// UnityEngine.Vector2 Doozy.Engine.Nody.Models.Connection::m_outputConnectionPoint
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_outputConnectionPoint_2;
	// System.String Doozy.Engine.Nody.Models.Connection::m_id
	String_t* ___m_id_3;
	// System.String Doozy.Engine.Nody.Models.Connection::m_inputNodeId
	String_t* ___m_inputNodeId_4;
	// System.String Doozy.Engine.Nody.Models.Connection::m_inputSocketId
	String_t* ___m_inputSocketId_5;
	// System.String Doozy.Engine.Nody.Models.Connection::m_outputNodeId
	String_t* ___m_outputNodeId_6;
	// System.String Doozy.Engine.Nody.Models.Connection::m_outputSocketId
	String_t* ___m_outputSocketId_7;

public:
	inline static int32_t get_offset_of_U3CPingU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6, ___U3CPingU3Ek__BackingField_0)); }
	inline bool get_U3CPingU3Ek__BackingField_0() const { return ___U3CPingU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CPingU3Ek__BackingField_0() { return &___U3CPingU3Ek__BackingField_0; }
	inline void set_U3CPingU3Ek__BackingField_0(bool value)
	{
		___U3CPingU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_inputConnectionPoint_1() { return static_cast<int32_t>(offsetof(Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6, ___m_inputConnectionPoint_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_inputConnectionPoint_1() const { return ___m_inputConnectionPoint_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_inputConnectionPoint_1() { return &___m_inputConnectionPoint_1; }
	inline void set_m_inputConnectionPoint_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_inputConnectionPoint_1 = value;
	}

	inline static int32_t get_offset_of_m_outputConnectionPoint_2() { return static_cast<int32_t>(offsetof(Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6, ___m_outputConnectionPoint_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_outputConnectionPoint_2() const { return ___m_outputConnectionPoint_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_outputConnectionPoint_2() { return &___m_outputConnectionPoint_2; }
	inline void set_m_outputConnectionPoint_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_outputConnectionPoint_2 = value;
	}

	inline static int32_t get_offset_of_m_id_3() { return static_cast<int32_t>(offsetof(Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6, ___m_id_3)); }
	inline String_t* get_m_id_3() const { return ___m_id_3; }
	inline String_t** get_address_of_m_id_3() { return &___m_id_3; }
	inline void set_m_id_3(String_t* value)
	{
		___m_id_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_id_3), value);
	}

	inline static int32_t get_offset_of_m_inputNodeId_4() { return static_cast<int32_t>(offsetof(Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6, ___m_inputNodeId_4)); }
	inline String_t* get_m_inputNodeId_4() const { return ___m_inputNodeId_4; }
	inline String_t** get_address_of_m_inputNodeId_4() { return &___m_inputNodeId_4; }
	inline void set_m_inputNodeId_4(String_t* value)
	{
		___m_inputNodeId_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputNodeId_4), value);
	}

	inline static int32_t get_offset_of_m_inputSocketId_5() { return static_cast<int32_t>(offsetof(Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6, ___m_inputSocketId_5)); }
	inline String_t* get_m_inputSocketId_5() const { return ___m_inputSocketId_5; }
	inline String_t** get_address_of_m_inputSocketId_5() { return &___m_inputSocketId_5; }
	inline void set_m_inputSocketId_5(String_t* value)
	{
		___m_inputSocketId_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputSocketId_5), value);
	}

	inline static int32_t get_offset_of_m_outputNodeId_6() { return static_cast<int32_t>(offsetof(Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6, ___m_outputNodeId_6)); }
	inline String_t* get_m_outputNodeId_6() const { return ___m_outputNodeId_6; }
	inline String_t** get_address_of_m_outputNodeId_6() { return &___m_outputNodeId_6; }
	inline void set_m_outputNodeId_6(String_t* value)
	{
		___m_outputNodeId_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_outputNodeId_6), value);
	}

	inline static int32_t get_offset_of_m_outputSocketId_7() { return static_cast<int32_t>(offsetof(Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6, ___m_outputSocketId_7)); }
	inline String_t* get_m_outputSocketId_7() const { return ___m_outputSocketId_7; }
	inline String_t** get_address_of_m_outputSocketId_7() { return &___m_outputSocketId_7; }
	inline void set_m_outputSocketId_7(String_t* value)
	{
		___m_outputSocketId_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_outputSocketId_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTION_T245E995D76298FBABC194CFFAFF0FDFEF8030EA6_H
#ifndef CONNECTIONMODE_T02A78013BE4577E4FE874D973349D38F1CD46CB9_H
#define CONNECTIONMODE_T02A78013BE4577E4FE874D973349D38F1CD46CB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Models.ConnectionMode
struct  ConnectionMode_t02A78013BE4577E4FE874D973349D38F1CD46CB9 
{
public:
	// System.Int32 Doozy.Engine.Nody.Models.ConnectionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectionMode_t02A78013BE4577E4FE874D973349D38F1CD46CB9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONMODE_T02A78013BE4577E4FE874D973349D38F1CD46CB9_H
#ifndef NODETYPE_TDA55D450054F4809E7380A0CFE79C0AE83045BAF_H
#define NODETYPE_TDA55D450054F4809E7380A0CFE79C0AE83045BAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Models.NodeType
struct  NodeType_tDA55D450054F4809E7380A0CFE79C0AE83045BAF 
{
public:
	// System.Int32 Doozy.Engine.Nody.Models.NodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NodeType_tDA55D450054F4809E7380A0CFE79C0AE83045BAF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODETYPE_TDA55D450054F4809E7380A0CFE79C0AE83045BAF_H
#ifndef SOCKETDIRECTION_TA05CAE38A57F695A9EACFF2522207DEF430E5C42_H
#define SOCKETDIRECTION_TA05CAE38A57F695A9EACFF2522207DEF430E5C42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Models.SocketDirection
struct  SocketDirection_tA05CAE38A57F695A9EACFF2522207DEF430E5C42 
{
public:
	// System.Int32 Doozy.Engine.Nody.Models.SocketDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketDirection_tA05CAE38A57F695A9EACFF2522207DEF430E5C42, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETDIRECTION_TA05CAE38A57F695A9EACFF2522207DEF430E5C42_H
#ifndef DETECTEDORIENTATION_TDB199EB6BDD094C3916416E502ABF997746D6ECA_H
#define DETECTEDORIENTATION_TDB199EB6BDD094C3916416E502ABF997746D6ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Orientation.DetectedOrientation
struct  DetectedOrientation_tDB199EB6BDD094C3916416E502ABF997746D6ECA 
{
public:
	// System.Int32 Doozy.Engine.Orientation.DetectedOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DetectedOrientation_tDB199EB6BDD094C3916416E502ABF997746D6ECA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTEDORIENTATION_TDB199EB6BDD094C3916416E502ABF997746D6ECA_H
#ifndef ORIENTATIONEVENT_TCC6D3CFE9251CC74E8F13085EE96BF9A1B2B267B_H
#define ORIENTATIONEVENT_TCC6D3CFE9251CC74E8F13085EE96BF9A1B2B267B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Orientation.OrientationEvent
struct  OrientationEvent_tCC6D3CFE9251CC74E8F13085EE96BF9A1B2B267B  : public UnityEvent_1_t76E58B9E749D9FE71072475712C59DD401882082
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATIONEVENT_TCC6D3CFE9251CC74E8F13085EE96BF9A1B2B267B_H
#ifndef PROGRESSEVENT_T029EF1ED75264735DFB03CFABD83B7D07DD8C3BB_H
#define PROGRESSEVENT_T029EF1ED75264735DFB03CFABD83B7D07DD8C3BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.ProgressEvent
struct  ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB  : public UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSEVENT_T029EF1ED75264735DFB03CFABD83B7D07DD8C3BB_H
#ifndef RESETVALUE_T30211A108252757290364E5762AB7B860211DA12_H
#define RESETVALUE_T30211A108252757290364E5762AB7B860211DA12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.ResetValue
struct  ResetValue_t30211A108252757290364E5762AB7B860211DA12 
{
public:
	// System.Int32 Doozy.Engine.Progress.ResetValue::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ResetValue_t30211A108252757290364E5762AB7B860211DA12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESETVALUE_T30211A108252757290364E5762AB7B860211DA12_H
#ifndef TARGETPROGRESS_TDEED5DA2E9A8C4C19A1ACECE025915AA7D35C5C9_H
#define TARGETPROGRESS_TDEED5DA2E9A8C4C19A1ACECE025915AA7D35C5C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.TargetProgress
struct  TargetProgress_tDEED5DA2E9A8C4C19A1ACECE025915AA7D35C5C9 
{
public:
	// System.Int32 Doozy.Engine.Progress.TargetProgress::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TargetProgress_tDEED5DA2E9A8C4C19A1ACECE025915AA7D35C5C9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETPROGRESS_TDEED5DA2E9A8C4C19A1ACECE025915AA7D35C5C9_H
#ifndef TARGETVARIABLE_TA72CCEF209B5E96B94AC7906D4089D70A57030FF_H
#define TARGETVARIABLE_TA72CCEF209B5E96B94AC7906D4089D70A57030FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.TargetVariable
struct  TargetVariable_tA72CCEF209B5E96B94AC7906D4089D70A57030FF 
{
public:
	// System.Int32 Doozy.Engine.Progress.TargetVariable::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TargetVariable_tA72CCEF209B5E96B94AC7906D4089D70A57030FF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETVARIABLE_TA72CCEF209B5E96B94AC7906D4089D70A57030FF_H
#ifndef ACTIVESCENECHANGEDEVENT_T0AB76BCA01E7396F308C3216662C1A70015DD7D4_H
#define ACTIVESCENECHANGEDEVENT_T0AB76BCA01E7396F308C3216662C1A70015DD7D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SceneManagement.ActiveSceneChangedEvent
struct  ActiveSceneChangedEvent_t0AB76BCA01E7396F308C3216662C1A70015DD7D4  : public UnityEvent_2_t01112610AD6911857DFEC961B0DEFC076775875F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVESCENECHANGEDEVENT_T0AB76BCA01E7396F308C3216662C1A70015DD7D4_H
#ifndef GETSCENEBY_T7E8029F50A80BF74544C7791080F0A2205968CE9_H
#define GETSCENEBY_T7E8029F50A80BF74544C7791080F0A2205968CE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SceneManagement.GetSceneBy
struct  GetSceneBy_t7E8029F50A80BF74544C7791080F0A2205968CE9 
{
public:
	// System.Int32 Doozy.Engine.SceneManagement.GetSceneBy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GetSceneBy_t7E8029F50A80BF74544C7791080F0A2205968CE9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEBY_T7E8029F50A80BF74544C7791080F0A2205968CE9_H
#ifndef SCENELOADEDEVENT_T9EED90839B7F10C9CF694B1DBFC750B7E9632E82_H
#define SCENELOADEDEVENT_T9EED90839B7F10C9CF694B1DBFC750B7E9632E82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SceneManagement.SceneLoadedEvent
struct  SceneLoadedEvent_t9EED90839B7F10C9CF694B1DBFC750B7E9632E82  : public UnityEvent_2_tF1F8B1C816B050524CB9BFBC4DD15218881397D1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENELOADEDEVENT_T9EED90839B7F10C9CF694B1DBFC750B7E9632E82_H
#ifndef SCENEUNLOADEDEVENT_TF6E196C99EC905E8F0AA9DE9F5246F3448B41471_H
#define SCENEUNLOADEDEVENT_TF6E196C99EC905E8F0AA9DE9F5246F3448B41471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SceneManagement.SceneUnloadedEvent
struct  SceneUnloadedEvent_tF6E196C99EC905E8F0AA9DE9F5246F3448B41471  : public UnityEvent_1_t33DD8AB59BBD10CD0C6BCA30D04B19934D43AEF5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEUNLOADEDEVENT_TF6E196C99EC905E8F0AA9DE9F5246F3448B41471_H
#ifndef SOUNDSOURCE_T7FA55BAC1FEA972D82594371862BBA8698B3D3E9_H
#define SOUNDSOURCE_T7FA55BAC1FEA972D82594371862BBA8698B3D3E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundSource
struct  SoundSource_t7FA55BAC1FEA972D82594371862BBA8698B3D3E9 
{
public:
	// System.Int32 Doozy.Engine.Soundy.SoundSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SoundSource_t7FA55BAC1FEA972D82594371862BBA8698B3D3E9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDSOURCE_T7FA55BAC1FEA972D82594371862BBA8698B3D3E9_H
#ifndef HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#define HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef LOADSCENEMODE_T75F0B96794398942671B8315D2A9AC25C40A22D5_H
#define LOADSCENEMODE_T75F0B96794398942671B8315D2A9AC25C40A22D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t75F0B96794398942671B8315D2A9AC25C40A22D5 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadSceneMode_t75F0B96794398942671B8315D2A9AC25C40A22D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEMODE_T75F0B96794398942671B8315D2A9AC25C40A22D5_H
#ifndef ANIMATOREVENT_T0342FEEFA357FBFBA4099D8D6B09DB667F448621_H
#define ANIMATOREVENT_T0342FEEFA357FBFBA4099D8D6B09DB667F448621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Events.AnimatorEvent
struct  AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621  : public RuntimeObject
{
public:
	// UnityEngine.Animator Doozy.Engine.Events.AnimatorEvent::Animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___Animator_0;
	// System.Boolean Doozy.Engine.Events.AnimatorEvent::BoolValue
	bool ___BoolValue_1;
	// System.Single Doozy.Engine.Events.AnimatorEvent::FloatValue
	float ___FloatValue_2;
	// System.Int32 Doozy.Engine.Events.AnimatorEvent::IntValue
	int32_t ___IntValue_3;
	// System.String Doozy.Engine.Events.AnimatorEvent::ParameterName
	String_t* ___ParameterName_4;
	// System.Boolean Doozy.Engine.Events.AnimatorEvent::ResetTrigger
	bool ___ResetTrigger_5;
	// Doozy.Engine.Events.AnimatorEvent_ParameterType Doozy.Engine.Events.AnimatorEvent::TargetParameterType
	int32_t ___TargetParameterType_6;

public:
	inline static int32_t get_offset_of_Animator_0() { return static_cast<int32_t>(offsetof(AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621, ___Animator_0)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_Animator_0() const { return ___Animator_0; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_Animator_0() { return &___Animator_0; }
	inline void set_Animator_0(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___Animator_0 = value;
		Il2CppCodeGenWriteBarrier((&___Animator_0), value);
	}

	inline static int32_t get_offset_of_BoolValue_1() { return static_cast<int32_t>(offsetof(AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621, ___BoolValue_1)); }
	inline bool get_BoolValue_1() const { return ___BoolValue_1; }
	inline bool* get_address_of_BoolValue_1() { return &___BoolValue_1; }
	inline void set_BoolValue_1(bool value)
	{
		___BoolValue_1 = value;
	}

	inline static int32_t get_offset_of_FloatValue_2() { return static_cast<int32_t>(offsetof(AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621, ___FloatValue_2)); }
	inline float get_FloatValue_2() const { return ___FloatValue_2; }
	inline float* get_address_of_FloatValue_2() { return &___FloatValue_2; }
	inline void set_FloatValue_2(float value)
	{
		___FloatValue_2 = value;
	}

	inline static int32_t get_offset_of_IntValue_3() { return static_cast<int32_t>(offsetof(AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621, ___IntValue_3)); }
	inline int32_t get_IntValue_3() const { return ___IntValue_3; }
	inline int32_t* get_address_of_IntValue_3() { return &___IntValue_3; }
	inline void set_IntValue_3(int32_t value)
	{
		___IntValue_3 = value;
	}

	inline static int32_t get_offset_of_ParameterName_4() { return static_cast<int32_t>(offsetof(AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621, ___ParameterName_4)); }
	inline String_t* get_ParameterName_4() const { return ___ParameterName_4; }
	inline String_t** get_address_of_ParameterName_4() { return &___ParameterName_4; }
	inline void set_ParameterName_4(String_t* value)
	{
		___ParameterName_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParameterName_4), value);
	}

	inline static int32_t get_offset_of_ResetTrigger_5() { return static_cast<int32_t>(offsetof(AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621, ___ResetTrigger_5)); }
	inline bool get_ResetTrigger_5() const { return ___ResetTrigger_5; }
	inline bool* get_address_of_ResetTrigger_5() { return &___ResetTrigger_5; }
	inline void set_ResetTrigger_5(bool value)
	{
		___ResetTrigger_5 = value;
	}

	inline static int32_t get_offset_of_TargetParameterType_6() { return static_cast<int32_t>(offsetof(AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621, ___TargetParameterType_6)); }
	inline int32_t get_TargetParameterType_6() const { return ___TargetParameterType_6; }
	inline int32_t* get_address_of_TargetParameterType_6() { return &___TargetParameterType_6; }
	inline void set_TargetParameterType_6(int32_t value)
	{
		___TargetParameterType_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOREVENT_T0342FEEFA357FBFBA4099D8D6B09DB667F448621_H
#ifndef SOCKET_T0285F9E2EE130163067662CA798F6DEB4559E1E8_H
#define SOCKET_T0285F9E2EE130163067662CA798F6DEB4559E1E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Models.Socket
struct  Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8  : public RuntimeObject
{
public:
	// Doozy.Engine.Nody.Models.ConnectionMode Doozy.Engine.Nody.Models.Socket::m_connectionMode
	int32_t ___m_connectionMode_2;
	// System.Collections.Generic.List`1<Doozy.Engine.Nody.Models.Connection> Doozy.Engine.Nody.Models.Socket::m_connections
	List_1_t61DEAC4B06FEC937FDCA0FF04B7C2798A15F1B26 * ___m_connections_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Doozy.Engine.Nody.Models.Socket::m_connectionPoints
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_connectionPoints_4;
	// Doozy.Engine.Nody.Models.SocketDirection Doozy.Engine.Nody.Models.Socket::m_direction
	int32_t ___m_direction_5;
	// System.Type Doozy.Engine.Nody.Models.Socket::m_valueType
	Type_t * ___m_valueType_6;
	// System.Boolean Doozy.Engine.Nody.Models.Socket::m_canBeDeleted
	bool ___m_canBeDeleted_7;
	// System.Boolean Doozy.Engine.Nody.Models.Socket::m_canBeReordered
	bool ___m_canBeReordered_8;
	// System.Single Doozy.Engine.Nody.Models.Socket::m_curveModifier
	float ___m_curveModifier_9;
	// System.Single Doozy.Engine.Nody.Models.Socket::m_height
	float ___m_height_10;
	// System.Single Doozy.Engine.Nody.Models.Socket::m_width
	float ___m_width_11;
	// System.Single Doozy.Engine.Nody.Models.Socket::m_x
	float ___m_x_12;
	// System.Single Doozy.Engine.Nody.Models.Socket::m_y
	float ___m_y_13;
	// System.String Doozy.Engine.Nody.Models.Socket::m_id
	String_t* ___m_id_14;
	// System.String Doozy.Engine.Nody.Models.Socket::m_nodeId
	String_t* ___m_nodeId_15;
	// System.String Doozy.Engine.Nody.Models.Socket::m_socketName
	String_t* ___m_socketName_16;
	// System.String Doozy.Engine.Nody.Models.Socket::m_value
	String_t* ___m_value_17;
	// System.String Doozy.Engine.Nody.Models.Socket::m_valueTypeQualifiedName
	String_t* ___m_valueTypeQualifiedName_18;
	// UnityEngine.Rect Doozy.Engine.Nody.Models.Socket::m_hoverRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___m_hoverRect_19;

public:
	inline static int32_t get_offset_of_m_connectionMode_2() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_connectionMode_2)); }
	inline int32_t get_m_connectionMode_2() const { return ___m_connectionMode_2; }
	inline int32_t* get_address_of_m_connectionMode_2() { return &___m_connectionMode_2; }
	inline void set_m_connectionMode_2(int32_t value)
	{
		___m_connectionMode_2 = value;
	}

	inline static int32_t get_offset_of_m_connections_3() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_connections_3)); }
	inline List_1_t61DEAC4B06FEC937FDCA0FF04B7C2798A15F1B26 * get_m_connections_3() const { return ___m_connections_3; }
	inline List_1_t61DEAC4B06FEC937FDCA0FF04B7C2798A15F1B26 ** get_address_of_m_connections_3() { return &___m_connections_3; }
	inline void set_m_connections_3(List_1_t61DEAC4B06FEC937FDCA0FF04B7C2798A15F1B26 * value)
	{
		___m_connections_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_connections_3), value);
	}

	inline static int32_t get_offset_of_m_connectionPoints_4() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_connectionPoints_4)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_connectionPoints_4() const { return ___m_connectionPoints_4; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_connectionPoints_4() { return &___m_connectionPoints_4; }
	inline void set_m_connectionPoints_4(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_connectionPoints_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_connectionPoints_4), value);
	}

	inline static int32_t get_offset_of_m_direction_5() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_direction_5)); }
	inline int32_t get_m_direction_5() const { return ___m_direction_5; }
	inline int32_t* get_address_of_m_direction_5() { return &___m_direction_5; }
	inline void set_m_direction_5(int32_t value)
	{
		___m_direction_5 = value;
	}

	inline static int32_t get_offset_of_m_valueType_6() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_valueType_6)); }
	inline Type_t * get_m_valueType_6() const { return ___m_valueType_6; }
	inline Type_t ** get_address_of_m_valueType_6() { return &___m_valueType_6; }
	inline void set_m_valueType_6(Type_t * value)
	{
		___m_valueType_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_valueType_6), value);
	}

	inline static int32_t get_offset_of_m_canBeDeleted_7() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_canBeDeleted_7)); }
	inline bool get_m_canBeDeleted_7() const { return ___m_canBeDeleted_7; }
	inline bool* get_address_of_m_canBeDeleted_7() { return &___m_canBeDeleted_7; }
	inline void set_m_canBeDeleted_7(bool value)
	{
		___m_canBeDeleted_7 = value;
	}

	inline static int32_t get_offset_of_m_canBeReordered_8() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_canBeReordered_8)); }
	inline bool get_m_canBeReordered_8() const { return ___m_canBeReordered_8; }
	inline bool* get_address_of_m_canBeReordered_8() { return &___m_canBeReordered_8; }
	inline void set_m_canBeReordered_8(bool value)
	{
		___m_canBeReordered_8 = value;
	}

	inline static int32_t get_offset_of_m_curveModifier_9() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_curveModifier_9)); }
	inline float get_m_curveModifier_9() const { return ___m_curveModifier_9; }
	inline float* get_address_of_m_curveModifier_9() { return &___m_curveModifier_9; }
	inline void set_m_curveModifier_9(float value)
	{
		___m_curveModifier_9 = value;
	}

	inline static int32_t get_offset_of_m_height_10() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_height_10)); }
	inline float get_m_height_10() const { return ___m_height_10; }
	inline float* get_address_of_m_height_10() { return &___m_height_10; }
	inline void set_m_height_10(float value)
	{
		___m_height_10 = value;
	}

	inline static int32_t get_offset_of_m_width_11() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_width_11)); }
	inline float get_m_width_11() const { return ___m_width_11; }
	inline float* get_address_of_m_width_11() { return &___m_width_11; }
	inline void set_m_width_11(float value)
	{
		___m_width_11 = value;
	}

	inline static int32_t get_offset_of_m_x_12() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_x_12)); }
	inline float get_m_x_12() const { return ___m_x_12; }
	inline float* get_address_of_m_x_12() { return &___m_x_12; }
	inline void set_m_x_12(float value)
	{
		___m_x_12 = value;
	}

	inline static int32_t get_offset_of_m_y_13() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_y_13)); }
	inline float get_m_y_13() const { return ___m_y_13; }
	inline float* get_address_of_m_y_13() { return &___m_y_13; }
	inline void set_m_y_13(float value)
	{
		___m_y_13 = value;
	}

	inline static int32_t get_offset_of_m_id_14() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_id_14)); }
	inline String_t* get_m_id_14() const { return ___m_id_14; }
	inline String_t** get_address_of_m_id_14() { return &___m_id_14; }
	inline void set_m_id_14(String_t* value)
	{
		___m_id_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_id_14), value);
	}

	inline static int32_t get_offset_of_m_nodeId_15() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_nodeId_15)); }
	inline String_t* get_m_nodeId_15() const { return ___m_nodeId_15; }
	inline String_t** get_address_of_m_nodeId_15() { return &___m_nodeId_15; }
	inline void set_m_nodeId_15(String_t* value)
	{
		___m_nodeId_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_nodeId_15), value);
	}

	inline static int32_t get_offset_of_m_socketName_16() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_socketName_16)); }
	inline String_t* get_m_socketName_16() const { return ___m_socketName_16; }
	inline String_t** get_address_of_m_socketName_16() { return &___m_socketName_16; }
	inline void set_m_socketName_16(String_t* value)
	{
		___m_socketName_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_socketName_16), value);
	}

	inline static int32_t get_offset_of_m_value_17() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_value_17)); }
	inline String_t* get_m_value_17() const { return ___m_value_17; }
	inline String_t** get_address_of_m_value_17() { return &___m_value_17; }
	inline void set_m_value_17(String_t* value)
	{
		___m_value_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_17), value);
	}

	inline static int32_t get_offset_of_m_valueTypeQualifiedName_18() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_valueTypeQualifiedName_18)); }
	inline String_t* get_m_valueTypeQualifiedName_18() const { return ___m_valueTypeQualifiedName_18; }
	inline String_t** get_address_of_m_valueTypeQualifiedName_18() { return &___m_valueTypeQualifiedName_18; }
	inline void set_m_valueTypeQualifiedName_18(String_t* value)
	{
		___m_valueTypeQualifiedName_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_valueTypeQualifiedName_18), value);
	}

	inline static int32_t get_offset_of_m_hoverRect_19() { return static_cast<int32_t>(offsetof(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8, ___m_hoverRect_19)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_m_hoverRect_19() const { return ___m_hoverRect_19; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_m_hoverRect_19() { return &___m_hoverRect_19; }
	inline void set_m_hoverRect_19(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___m_hoverRect_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKET_T0285F9E2EE130163067662CA798F6DEB4559E1E8_H
#ifndef U3CASYNCHRONOUSLOADU3ED__59_T48E51BB1B1B1C9604E9A7F967A7C690B33D2301B_H
#define U3CASYNCHRONOUSLOADU3ED__59_T48E51BB1B1B1C9604E9A7F967A7C690B33D2301B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__59
struct  U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__59::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__59::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__59::sceneName
	String_t* ___sceneName_2;
	// UnityEngine.SceneManagement.LoadSceneMode Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__59::mode
	int32_t ___mode_3;
	// Doozy.Engine.SceneManagement.SceneLoader Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__59::<>4__this
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438 * ___U3CU3E4__this_4;
	// System.Boolean Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__59::<sceneLoadedAndReady>5__1
	bool ___U3CsceneLoadedAndReadyU3E5__1_5;
	// System.Boolean Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__59::<activatingScene>5__2
	bool ___U3CactivatingSceneU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_sceneName_2() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B, ___sceneName_2)); }
	inline String_t* get_sceneName_2() const { return ___sceneName_2; }
	inline String_t** get_address_of_sceneName_2() { return &___sceneName_2; }
	inline void set_sceneName_2(String_t* value)
	{
		___sceneName_2 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_2), value);
	}

	inline static int32_t get_offset_of_mode_3() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B, ___mode_3)); }
	inline int32_t get_mode_3() const { return ___mode_3; }
	inline int32_t* get_address_of_mode_3() { return &___mode_3; }
	inline void set_mode_3(int32_t value)
	{
		___mode_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B, ___U3CU3E4__this_4)); }
	inline SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_U3CsceneLoadedAndReadyU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B, ___U3CsceneLoadedAndReadyU3E5__1_5)); }
	inline bool get_U3CsceneLoadedAndReadyU3E5__1_5() const { return ___U3CsceneLoadedAndReadyU3E5__1_5; }
	inline bool* get_address_of_U3CsceneLoadedAndReadyU3E5__1_5() { return &___U3CsceneLoadedAndReadyU3E5__1_5; }
	inline void set_U3CsceneLoadedAndReadyU3E5__1_5(bool value)
	{
		___U3CsceneLoadedAndReadyU3E5__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CactivatingSceneU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B, ___U3CactivatingSceneU3E5__2_6)); }
	inline bool get_U3CactivatingSceneU3E5__2_6() const { return ___U3CactivatingSceneU3E5__2_6; }
	inline bool* get_address_of_U3CactivatingSceneU3E5__2_6() { return &___U3CactivatingSceneU3E5__2_6; }
	inline void set_U3CactivatingSceneU3E5__2_6(bool value)
	{
		___U3CactivatingSceneU3E5__2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASYNCHRONOUSLOADU3ED__59_T48E51BB1B1B1C9604E9A7F967A7C690B33D2301B_H
#ifndef U3CASYNCHRONOUSLOADU3ED__60_T5B726064BF29675FC3B03046A8B4D70FFF140993_H
#define U3CASYNCHRONOUSLOADU3ED__60_T5B726064BF29675FC3B03046A8B4D70FFF140993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__60
struct  U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__60::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__60::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__60::sceneBuildIndex
	int32_t ___sceneBuildIndex_2;
	// UnityEngine.SceneManagement.LoadSceneMode Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__60::mode
	int32_t ___mode_3;
	// Doozy.Engine.SceneManagement.SceneLoader Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__60::<>4__this
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438 * ___U3CU3E4__this_4;
	// System.Boolean Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__60::<sceneLoadedAndReady>5__1
	bool ___U3CsceneLoadedAndReadyU3E5__1_5;
	// System.Boolean Doozy.Engine.SceneManagement.SceneLoader_<AsynchronousLoad>d__60::<activatingScene>5__2
	bool ___U3CactivatingSceneU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_sceneBuildIndex_2() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993, ___sceneBuildIndex_2)); }
	inline int32_t get_sceneBuildIndex_2() const { return ___sceneBuildIndex_2; }
	inline int32_t* get_address_of_sceneBuildIndex_2() { return &___sceneBuildIndex_2; }
	inline void set_sceneBuildIndex_2(int32_t value)
	{
		___sceneBuildIndex_2 = value;
	}

	inline static int32_t get_offset_of_mode_3() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993, ___mode_3)); }
	inline int32_t get_mode_3() const { return ___mode_3; }
	inline int32_t* get_address_of_mode_3() { return &___mode_3; }
	inline void set_mode_3(int32_t value)
	{
		___mode_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993, ___U3CU3E4__this_4)); }
	inline SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_U3CsceneLoadedAndReadyU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993, ___U3CsceneLoadedAndReadyU3E5__1_5)); }
	inline bool get_U3CsceneLoadedAndReadyU3E5__1_5() const { return ___U3CsceneLoadedAndReadyU3E5__1_5; }
	inline bool* get_address_of_U3CsceneLoadedAndReadyU3E5__1_5() { return &___U3CsceneLoadedAndReadyU3E5__1_5; }
	inline void set_U3CsceneLoadedAndReadyU3E5__1_5(bool value)
	{
		___U3CsceneLoadedAndReadyU3E5__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CactivatingSceneU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993, ___U3CactivatingSceneU3E5__2_6)); }
	inline bool get_U3CactivatingSceneU3E5__2_6() const { return ___U3CactivatingSceneU3E5__2_6; }
	inline bool* get_address_of_U3CactivatingSceneU3E5__2_6() { return &___U3CactivatingSceneU3E5__2_6; }
	inline void set_U3CactivatingSceneU3E5__2_6(bool value)
	{
		___U3CactivatingSceneU3E5__2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASYNCHRONOUSLOADU3ED__60_T5B726064BF29675FC3B03046A8B4D70FFF140993_H
#ifndef SOUNDYDATA_T1265C32F2FF565EF7D276CCAF3027B1BF7974720_H
#define SOUNDYDATA_T1265C32F2FF565EF7D276CCAF3027B1BF7974720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundyData
struct  SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720  : public RuntimeObject
{
public:
	// Doozy.Engine.Soundy.SoundSource Doozy.Engine.Soundy.SoundyData::SoundSource
	int32_t ___SoundSource_0;
	// System.String Doozy.Engine.Soundy.SoundyData::DatabaseName
	String_t* ___DatabaseName_1;
	// System.String Doozy.Engine.Soundy.SoundyData::SoundName
	String_t* ___SoundName_2;
	// UnityEngine.AudioClip Doozy.Engine.Soundy.SoundyData::AudioClip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___AudioClip_3;
	// UnityEngine.Audio.AudioMixerGroup Doozy.Engine.Soundy.SoundyData::OutputAudioMixerGroup
	AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 * ___OutputAudioMixerGroup_4;

public:
	inline static int32_t get_offset_of_SoundSource_0() { return static_cast<int32_t>(offsetof(SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720, ___SoundSource_0)); }
	inline int32_t get_SoundSource_0() const { return ___SoundSource_0; }
	inline int32_t* get_address_of_SoundSource_0() { return &___SoundSource_0; }
	inline void set_SoundSource_0(int32_t value)
	{
		___SoundSource_0 = value;
	}

	inline static int32_t get_offset_of_DatabaseName_1() { return static_cast<int32_t>(offsetof(SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720, ___DatabaseName_1)); }
	inline String_t* get_DatabaseName_1() const { return ___DatabaseName_1; }
	inline String_t** get_address_of_DatabaseName_1() { return &___DatabaseName_1; }
	inline void set_DatabaseName_1(String_t* value)
	{
		___DatabaseName_1 = value;
		Il2CppCodeGenWriteBarrier((&___DatabaseName_1), value);
	}

	inline static int32_t get_offset_of_SoundName_2() { return static_cast<int32_t>(offsetof(SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720, ___SoundName_2)); }
	inline String_t* get_SoundName_2() const { return ___SoundName_2; }
	inline String_t** get_address_of_SoundName_2() { return &___SoundName_2; }
	inline void set_SoundName_2(String_t* value)
	{
		___SoundName_2 = value;
		Il2CppCodeGenWriteBarrier((&___SoundName_2), value);
	}

	inline static int32_t get_offset_of_AudioClip_3() { return static_cast<int32_t>(offsetof(SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720, ___AudioClip_3)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_AudioClip_3() const { return ___AudioClip_3; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_AudioClip_3() { return &___AudioClip_3; }
	inline void set_AudioClip_3(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___AudioClip_3 = value;
		Il2CppCodeGenWriteBarrier((&___AudioClip_3), value);
	}

	inline static int32_t get_offset_of_OutputAudioMixerGroup_4() { return static_cast<int32_t>(offsetof(SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720, ___OutputAudioMixerGroup_4)); }
	inline AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 * get_OutputAudioMixerGroup_4() const { return ___OutputAudioMixerGroup_4; }
	inline AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 ** get_address_of_OutputAudioMixerGroup_4() { return &___OutputAudioMixerGroup_4; }
	inline void set_OutputAudioMixerGroup_4(AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 * value)
	{
		___OutputAudioMixerGroup_4 = value;
		Il2CppCodeGenWriteBarrier((&___OutputAudioMixerGroup_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDYDATA_T1265C32F2FF565EF7D276CCAF3027B1BF7974720_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef GRAPH_T26D9C31435F4455B6EF025E506031462302E7AD5_H
#define GRAPH_T26D9C31435F4455B6EF025E506031462302E7AD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Models.Graph
struct  Graph_t26D9C31435F4455B6EF025E506031462302E7AD5  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Doozy.Engine.Nody.Models.Graph Doozy.Engine.Nody.Models.Graph::<ActiveSubGraph>k__BackingField
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * ___U3CActiveSubGraphU3Ek__BackingField_6;
	// Doozy.Engine.Nody.Models.Node Doozy.Engine.Nody.Models.Graph::<ActiveNode>k__BackingField
	Node_t06F79187A31E6958DED19533B1A8783AD1633439 * ___U3CActiveNodeU3Ek__BackingField_7;
	// Doozy.Engine.Nody.Models.Node Doozy.Engine.Nody.Models.Graph::<PreviousActiveNode>k__BackingField
	Node_t06F79187A31E6958DED19533B1A8783AD1633439 * ___U3CPreviousActiveNodeU3Ek__BackingField_8;
	// System.Boolean Doozy.Engine.Nody.Models.Graph::DebugMode
	bool ___DebugMode_9;
	// Doozy.Engine.Nody.Models.Graph Doozy.Engine.Nody.Models.Graph::ParentGraph
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * ___ParentGraph_10;
	// Doozy.Engine.Nody.Nodes.SubGraphNode Doozy.Engine.Nody.Models.Graph::ParentSubGraphNode
	SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B * ___ParentSubGraphNode_11;
	// System.Collections.Generic.List`1<Doozy.Engine.Nody.Models.Node> Doozy.Engine.Nody.Models.Graph::m_activatedNodesHistory
	List_1_t11A6410B9139871ED4209E0644694FDA98C325D1 * ___m_activatedNodesHistory_12;
	// System.Collections.Generic.List`1<Doozy.Engine.Nody.Models.Node> Doozy.Engine.Nody.Models.Graph::m_globalNodes
	List_1_t11A6410B9139871ED4209E0644694FDA98C325D1 * ___m_globalNodes_13;
	// Doozy.Engine.Nody.Models.Node Doozy.Engine.Nody.Models.Graph::m_enterNode
	Node_t06F79187A31E6958DED19533B1A8783AD1633439 * ___m_enterNode_14;
	// Doozy.Engine.Nody.Models.Node Doozy.Engine.Nody.Models.Graph::m_exitNode
	Node_t06F79187A31E6958DED19533B1A8783AD1633439 * ___m_exitNode_15;
	// Doozy.Engine.Nody.Models.Node Doozy.Engine.Nody.Models.Graph::m_startNode
	Node_t06F79187A31E6958DED19533B1A8783AD1633439 * ___m_startNode_16;
	// System.Boolean Doozy.Engine.Nody.Models.Graph::m_isDirty
	bool ___m_isDirty_17;
	// System.Double Doozy.Engine.Nody.Models.Graph::m_infiniteLoopTimerStart
	double ___m_infiniteLoopTimerStart_18;
	// System.Single Doozy.Engine.Nody.Models.Graph::m_infiniteLoopTimeBreak
	float ___m_infiniteLoopTimeBreak_19;
	// System.Collections.Generic.List`1<Doozy.Engine.Nody.Models.Node> Doozy.Engine.Nody.Models.Graph::m_nodes
	List_1_t11A6410B9139871ED4209E0644694FDA98C325D1 * ___m_nodes_20;
	// System.Boolean Doozy.Engine.Nody.Models.Graph::m_isSubGraph
	bool ___m_isSubGraph_21;
	// System.Int32 Doozy.Engine.Nody.Models.Graph::m_version
	int32_t ___m_version_22;
	// System.String Doozy.Engine.Nody.Models.Graph::m_description
	String_t* ___m_description_23;
	// System.String Doozy.Engine.Nody.Models.Graph::m_id
	String_t* ___m_id_24;
	// System.String Doozy.Engine.Nody.Models.Graph::m_lastModified
	String_t* ___m_lastModified_25;

public:
	inline static int32_t get_offset_of_U3CActiveSubGraphU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___U3CActiveSubGraphU3Ek__BackingField_6)); }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * get_U3CActiveSubGraphU3Ek__BackingField_6() const { return ___U3CActiveSubGraphU3Ek__BackingField_6; }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 ** get_address_of_U3CActiveSubGraphU3Ek__BackingField_6() { return &___U3CActiveSubGraphU3Ek__BackingField_6; }
	inline void set_U3CActiveSubGraphU3Ek__BackingField_6(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * value)
	{
		___U3CActiveSubGraphU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActiveSubGraphU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CActiveNodeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___U3CActiveNodeU3Ek__BackingField_7)); }
	inline Node_t06F79187A31E6958DED19533B1A8783AD1633439 * get_U3CActiveNodeU3Ek__BackingField_7() const { return ___U3CActiveNodeU3Ek__BackingField_7; }
	inline Node_t06F79187A31E6958DED19533B1A8783AD1633439 ** get_address_of_U3CActiveNodeU3Ek__BackingField_7() { return &___U3CActiveNodeU3Ek__BackingField_7; }
	inline void set_U3CActiveNodeU3Ek__BackingField_7(Node_t06F79187A31E6958DED19533B1A8783AD1633439 * value)
	{
		___U3CActiveNodeU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActiveNodeU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CPreviousActiveNodeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___U3CPreviousActiveNodeU3Ek__BackingField_8)); }
	inline Node_t06F79187A31E6958DED19533B1A8783AD1633439 * get_U3CPreviousActiveNodeU3Ek__BackingField_8() const { return ___U3CPreviousActiveNodeU3Ek__BackingField_8; }
	inline Node_t06F79187A31E6958DED19533B1A8783AD1633439 ** get_address_of_U3CPreviousActiveNodeU3Ek__BackingField_8() { return &___U3CPreviousActiveNodeU3Ek__BackingField_8; }
	inline void set_U3CPreviousActiveNodeU3Ek__BackingField_8(Node_t06F79187A31E6958DED19533B1A8783AD1633439 * value)
	{
		___U3CPreviousActiveNodeU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPreviousActiveNodeU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_DebugMode_9() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___DebugMode_9)); }
	inline bool get_DebugMode_9() const { return ___DebugMode_9; }
	inline bool* get_address_of_DebugMode_9() { return &___DebugMode_9; }
	inline void set_DebugMode_9(bool value)
	{
		___DebugMode_9 = value;
	}

	inline static int32_t get_offset_of_ParentGraph_10() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___ParentGraph_10)); }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * get_ParentGraph_10() const { return ___ParentGraph_10; }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 ** get_address_of_ParentGraph_10() { return &___ParentGraph_10; }
	inline void set_ParentGraph_10(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * value)
	{
		___ParentGraph_10 = value;
		Il2CppCodeGenWriteBarrier((&___ParentGraph_10), value);
	}

	inline static int32_t get_offset_of_ParentSubGraphNode_11() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___ParentSubGraphNode_11)); }
	inline SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B * get_ParentSubGraphNode_11() const { return ___ParentSubGraphNode_11; }
	inline SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B ** get_address_of_ParentSubGraphNode_11() { return &___ParentSubGraphNode_11; }
	inline void set_ParentSubGraphNode_11(SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B * value)
	{
		___ParentSubGraphNode_11 = value;
		Il2CppCodeGenWriteBarrier((&___ParentSubGraphNode_11), value);
	}

	inline static int32_t get_offset_of_m_activatedNodesHistory_12() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_activatedNodesHistory_12)); }
	inline List_1_t11A6410B9139871ED4209E0644694FDA98C325D1 * get_m_activatedNodesHistory_12() const { return ___m_activatedNodesHistory_12; }
	inline List_1_t11A6410B9139871ED4209E0644694FDA98C325D1 ** get_address_of_m_activatedNodesHistory_12() { return &___m_activatedNodesHistory_12; }
	inline void set_m_activatedNodesHistory_12(List_1_t11A6410B9139871ED4209E0644694FDA98C325D1 * value)
	{
		___m_activatedNodesHistory_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_activatedNodesHistory_12), value);
	}

	inline static int32_t get_offset_of_m_globalNodes_13() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_globalNodes_13)); }
	inline List_1_t11A6410B9139871ED4209E0644694FDA98C325D1 * get_m_globalNodes_13() const { return ___m_globalNodes_13; }
	inline List_1_t11A6410B9139871ED4209E0644694FDA98C325D1 ** get_address_of_m_globalNodes_13() { return &___m_globalNodes_13; }
	inline void set_m_globalNodes_13(List_1_t11A6410B9139871ED4209E0644694FDA98C325D1 * value)
	{
		___m_globalNodes_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_globalNodes_13), value);
	}

	inline static int32_t get_offset_of_m_enterNode_14() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_enterNode_14)); }
	inline Node_t06F79187A31E6958DED19533B1A8783AD1633439 * get_m_enterNode_14() const { return ___m_enterNode_14; }
	inline Node_t06F79187A31E6958DED19533B1A8783AD1633439 ** get_address_of_m_enterNode_14() { return &___m_enterNode_14; }
	inline void set_m_enterNode_14(Node_t06F79187A31E6958DED19533B1A8783AD1633439 * value)
	{
		___m_enterNode_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_enterNode_14), value);
	}

	inline static int32_t get_offset_of_m_exitNode_15() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_exitNode_15)); }
	inline Node_t06F79187A31E6958DED19533B1A8783AD1633439 * get_m_exitNode_15() const { return ___m_exitNode_15; }
	inline Node_t06F79187A31E6958DED19533B1A8783AD1633439 ** get_address_of_m_exitNode_15() { return &___m_exitNode_15; }
	inline void set_m_exitNode_15(Node_t06F79187A31E6958DED19533B1A8783AD1633439 * value)
	{
		___m_exitNode_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_exitNode_15), value);
	}

	inline static int32_t get_offset_of_m_startNode_16() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_startNode_16)); }
	inline Node_t06F79187A31E6958DED19533B1A8783AD1633439 * get_m_startNode_16() const { return ___m_startNode_16; }
	inline Node_t06F79187A31E6958DED19533B1A8783AD1633439 ** get_address_of_m_startNode_16() { return &___m_startNode_16; }
	inline void set_m_startNode_16(Node_t06F79187A31E6958DED19533B1A8783AD1633439 * value)
	{
		___m_startNode_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_startNode_16), value);
	}

	inline static int32_t get_offset_of_m_isDirty_17() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_isDirty_17)); }
	inline bool get_m_isDirty_17() const { return ___m_isDirty_17; }
	inline bool* get_address_of_m_isDirty_17() { return &___m_isDirty_17; }
	inline void set_m_isDirty_17(bool value)
	{
		___m_isDirty_17 = value;
	}

	inline static int32_t get_offset_of_m_infiniteLoopTimerStart_18() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_infiniteLoopTimerStart_18)); }
	inline double get_m_infiniteLoopTimerStart_18() const { return ___m_infiniteLoopTimerStart_18; }
	inline double* get_address_of_m_infiniteLoopTimerStart_18() { return &___m_infiniteLoopTimerStart_18; }
	inline void set_m_infiniteLoopTimerStart_18(double value)
	{
		___m_infiniteLoopTimerStart_18 = value;
	}

	inline static int32_t get_offset_of_m_infiniteLoopTimeBreak_19() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_infiniteLoopTimeBreak_19)); }
	inline float get_m_infiniteLoopTimeBreak_19() const { return ___m_infiniteLoopTimeBreak_19; }
	inline float* get_address_of_m_infiniteLoopTimeBreak_19() { return &___m_infiniteLoopTimeBreak_19; }
	inline void set_m_infiniteLoopTimeBreak_19(float value)
	{
		___m_infiniteLoopTimeBreak_19 = value;
	}

	inline static int32_t get_offset_of_m_nodes_20() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_nodes_20)); }
	inline List_1_t11A6410B9139871ED4209E0644694FDA98C325D1 * get_m_nodes_20() const { return ___m_nodes_20; }
	inline List_1_t11A6410B9139871ED4209E0644694FDA98C325D1 ** get_address_of_m_nodes_20() { return &___m_nodes_20; }
	inline void set_m_nodes_20(List_1_t11A6410B9139871ED4209E0644694FDA98C325D1 * value)
	{
		___m_nodes_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_nodes_20), value);
	}

	inline static int32_t get_offset_of_m_isSubGraph_21() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_isSubGraph_21)); }
	inline bool get_m_isSubGraph_21() const { return ___m_isSubGraph_21; }
	inline bool* get_address_of_m_isSubGraph_21() { return &___m_isSubGraph_21; }
	inline void set_m_isSubGraph_21(bool value)
	{
		___m_isSubGraph_21 = value;
	}

	inline static int32_t get_offset_of_m_version_22() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_version_22)); }
	inline int32_t get_m_version_22() const { return ___m_version_22; }
	inline int32_t* get_address_of_m_version_22() { return &___m_version_22; }
	inline void set_m_version_22(int32_t value)
	{
		___m_version_22 = value;
	}

	inline static int32_t get_offset_of_m_description_23() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_description_23)); }
	inline String_t* get_m_description_23() const { return ___m_description_23; }
	inline String_t** get_address_of_m_description_23() { return &___m_description_23; }
	inline void set_m_description_23(String_t* value)
	{
		___m_description_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_description_23), value);
	}

	inline static int32_t get_offset_of_m_id_24() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_id_24)); }
	inline String_t* get_m_id_24() const { return ___m_id_24; }
	inline String_t** get_address_of_m_id_24() { return &___m_id_24; }
	inline void set_m_id_24(String_t* value)
	{
		___m_id_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_id_24), value);
	}

	inline static int32_t get_offset_of_m_lastModified_25() { return static_cast<int32_t>(offsetof(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5, ___m_lastModified_25)); }
	inline String_t* get_m_lastModified_25() const { return ___m_lastModified_25; }
	inline String_t** get_address_of_m_lastModified_25() { return &___m_lastModified_25; }
	inline void set_m_lastModified_25(String_t* value)
	{
		___m_lastModified_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_lastModified_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPH_T26D9C31435F4455B6EF025E506031462302E7AD5_H
#ifndef NODE_T06F79187A31E6958DED19533B1A8783AD1633439_H
#define NODE_T06F79187A31E6958DED19533B1A8783AD1633439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Models.Node
struct  Node_t06F79187A31E6958DED19533B1A8783AD1633439  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<Doozy.Engine.Nody.Models.Socket> Doozy.Engine.Nody.Models.Node::m_inputSockets
	List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B * ___m_inputSockets_4;
	// System.Collections.Generic.List`1<Doozy.Engine.Nody.Models.Socket> Doozy.Engine.Nody.Models.Node::m_outputSockets
	List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B * ___m_outputSockets_5;
	// Doozy.Engine.Nody.Models.NodeType Doozy.Engine.Nody.Models.Node::m_nodeType
	int32_t ___m_nodeType_6;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_allowDuplicateNodeName
	bool ___m_allowDuplicateNodeName_7;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_allowEmptyNodeName
	bool ___m_allowEmptyNodeName_8;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_canBeDeleted
	bool ___m_canBeDeleted_9;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_debugMode
	bool ___m_debugMode_10;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_useFixedUpdate
	bool ___m_useFixedUpdate_11;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_useLateUpdate
	bool ___m_useLateUpdate_12;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_useUpdate
	bool ___m_useUpdate_13;
	// System.Single Doozy.Engine.Nody.Models.Node::m_height
	float ___m_height_14;
	// System.Single Doozy.Engine.Nody.Models.Node::m_width
	float ___m_width_15;
	// System.Single Doozy.Engine.Nody.Models.Node::m_x
	float ___m_x_16;
	// System.Single Doozy.Engine.Nody.Models.Node::m_y
	float ___m_y_17;
	// System.Int32 Doozy.Engine.Nody.Models.Node::m_minimumInputSocketsCount
	int32_t ___m_minimumInputSocketsCount_18;
	// System.Int32 Doozy.Engine.Nody.Models.Node::m_minimumOutputSocketsCount
	int32_t ___m_minimumOutputSocketsCount_19;
	// System.String Doozy.Engine.Nody.Models.Node::m_graphId
	String_t* ___m_graphId_20;
	// System.String Doozy.Engine.Nody.Models.Node::m_id
	String_t* ___m_id_21;
	// System.String Doozy.Engine.Nody.Models.Node::m_name
	String_t* ___m_name_22;
	// Doozy.Engine.Nody.Models.Graph Doozy.Engine.Nody.Models.Node::m_activeGraph
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * ___m_activeGraph_23;
	// System.Boolean Doozy.Engine.Nody.Models.Node::<Ping>k__BackingField
	bool ___U3CPingU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_m_inputSockets_4() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_inputSockets_4)); }
	inline List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B * get_m_inputSockets_4() const { return ___m_inputSockets_4; }
	inline List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B ** get_address_of_m_inputSockets_4() { return &___m_inputSockets_4; }
	inline void set_m_inputSockets_4(List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B * value)
	{
		___m_inputSockets_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputSockets_4), value);
	}

	inline static int32_t get_offset_of_m_outputSockets_5() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_outputSockets_5)); }
	inline List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B * get_m_outputSockets_5() const { return ___m_outputSockets_5; }
	inline List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B ** get_address_of_m_outputSockets_5() { return &___m_outputSockets_5; }
	inline void set_m_outputSockets_5(List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B * value)
	{
		___m_outputSockets_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_outputSockets_5), value);
	}

	inline static int32_t get_offset_of_m_nodeType_6() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_nodeType_6)); }
	inline int32_t get_m_nodeType_6() const { return ___m_nodeType_6; }
	inline int32_t* get_address_of_m_nodeType_6() { return &___m_nodeType_6; }
	inline void set_m_nodeType_6(int32_t value)
	{
		___m_nodeType_6 = value;
	}

	inline static int32_t get_offset_of_m_allowDuplicateNodeName_7() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_allowDuplicateNodeName_7)); }
	inline bool get_m_allowDuplicateNodeName_7() const { return ___m_allowDuplicateNodeName_7; }
	inline bool* get_address_of_m_allowDuplicateNodeName_7() { return &___m_allowDuplicateNodeName_7; }
	inline void set_m_allowDuplicateNodeName_7(bool value)
	{
		___m_allowDuplicateNodeName_7 = value;
	}

	inline static int32_t get_offset_of_m_allowEmptyNodeName_8() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_allowEmptyNodeName_8)); }
	inline bool get_m_allowEmptyNodeName_8() const { return ___m_allowEmptyNodeName_8; }
	inline bool* get_address_of_m_allowEmptyNodeName_8() { return &___m_allowEmptyNodeName_8; }
	inline void set_m_allowEmptyNodeName_8(bool value)
	{
		___m_allowEmptyNodeName_8 = value;
	}

	inline static int32_t get_offset_of_m_canBeDeleted_9() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_canBeDeleted_9)); }
	inline bool get_m_canBeDeleted_9() const { return ___m_canBeDeleted_9; }
	inline bool* get_address_of_m_canBeDeleted_9() { return &___m_canBeDeleted_9; }
	inline void set_m_canBeDeleted_9(bool value)
	{
		___m_canBeDeleted_9 = value;
	}

	inline static int32_t get_offset_of_m_debugMode_10() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_debugMode_10)); }
	inline bool get_m_debugMode_10() const { return ___m_debugMode_10; }
	inline bool* get_address_of_m_debugMode_10() { return &___m_debugMode_10; }
	inline void set_m_debugMode_10(bool value)
	{
		___m_debugMode_10 = value;
	}

	inline static int32_t get_offset_of_m_useFixedUpdate_11() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_useFixedUpdate_11)); }
	inline bool get_m_useFixedUpdate_11() const { return ___m_useFixedUpdate_11; }
	inline bool* get_address_of_m_useFixedUpdate_11() { return &___m_useFixedUpdate_11; }
	inline void set_m_useFixedUpdate_11(bool value)
	{
		___m_useFixedUpdate_11 = value;
	}

	inline static int32_t get_offset_of_m_useLateUpdate_12() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_useLateUpdate_12)); }
	inline bool get_m_useLateUpdate_12() const { return ___m_useLateUpdate_12; }
	inline bool* get_address_of_m_useLateUpdate_12() { return &___m_useLateUpdate_12; }
	inline void set_m_useLateUpdate_12(bool value)
	{
		___m_useLateUpdate_12 = value;
	}

	inline static int32_t get_offset_of_m_useUpdate_13() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_useUpdate_13)); }
	inline bool get_m_useUpdate_13() const { return ___m_useUpdate_13; }
	inline bool* get_address_of_m_useUpdate_13() { return &___m_useUpdate_13; }
	inline void set_m_useUpdate_13(bool value)
	{
		___m_useUpdate_13 = value;
	}

	inline static int32_t get_offset_of_m_height_14() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_height_14)); }
	inline float get_m_height_14() const { return ___m_height_14; }
	inline float* get_address_of_m_height_14() { return &___m_height_14; }
	inline void set_m_height_14(float value)
	{
		___m_height_14 = value;
	}

	inline static int32_t get_offset_of_m_width_15() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_width_15)); }
	inline float get_m_width_15() const { return ___m_width_15; }
	inline float* get_address_of_m_width_15() { return &___m_width_15; }
	inline void set_m_width_15(float value)
	{
		___m_width_15 = value;
	}

	inline static int32_t get_offset_of_m_x_16() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_x_16)); }
	inline float get_m_x_16() const { return ___m_x_16; }
	inline float* get_address_of_m_x_16() { return &___m_x_16; }
	inline void set_m_x_16(float value)
	{
		___m_x_16 = value;
	}

	inline static int32_t get_offset_of_m_y_17() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_y_17)); }
	inline float get_m_y_17() const { return ___m_y_17; }
	inline float* get_address_of_m_y_17() { return &___m_y_17; }
	inline void set_m_y_17(float value)
	{
		___m_y_17 = value;
	}

	inline static int32_t get_offset_of_m_minimumInputSocketsCount_18() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_minimumInputSocketsCount_18)); }
	inline int32_t get_m_minimumInputSocketsCount_18() const { return ___m_minimumInputSocketsCount_18; }
	inline int32_t* get_address_of_m_minimumInputSocketsCount_18() { return &___m_minimumInputSocketsCount_18; }
	inline void set_m_minimumInputSocketsCount_18(int32_t value)
	{
		___m_minimumInputSocketsCount_18 = value;
	}

	inline static int32_t get_offset_of_m_minimumOutputSocketsCount_19() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_minimumOutputSocketsCount_19)); }
	inline int32_t get_m_minimumOutputSocketsCount_19() const { return ___m_minimumOutputSocketsCount_19; }
	inline int32_t* get_address_of_m_minimumOutputSocketsCount_19() { return &___m_minimumOutputSocketsCount_19; }
	inline void set_m_minimumOutputSocketsCount_19(int32_t value)
	{
		___m_minimumOutputSocketsCount_19 = value;
	}

	inline static int32_t get_offset_of_m_graphId_20() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_graphId_20)); }
	inline String_t* get_m_graphId_20() const { return ___m_graphId_20; }
	inline String_t** get_address_of_m_graphId_20() { return &___m_graphId_20; }
	inline void set_m_graphId_20(String_t* value)
	{
		___m_graphId_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphId_20), value);
	}

	inline static int32_t get_offset_of_m_id_21() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_id_21)); }
	inline String_t* get_m_id_21() const { return ___m_id_21; }
	inline String_t** get_address_of_m_id_21() { return &___m_id_21; }
	inline void set_m_id_21(String_t* value)
	{
		___m_id_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_id_21), value);
	}

	inline static int32_t get_offset_of_m_name_22() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_name_22)); }
	inline String_t* get_m_name_22() const { return ___m_name_22; }
	inline String_t** get_address_of_m_name_22() { return &___m_name_22; }
	inline void set_m_name_22(String_t* value)
	{
		___m_name_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_22), value);
	}

	inline static int32_t get_offset_of_m_activeGraph_23() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_activeGraph_23)); }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * get_m_activeGraph_23() const { return ___m_activeGraph_23; }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 ** get_address_of_m_activeGraph_23() { return &___m_activeGraph_23; }
	inline void set_m_activeGraph_23(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * value)
	{
		___m_activeGraph_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_activeGraph_23), value);
	}

	inline static int32_t get_offset_of_U3CPingU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___U3CPingU3Ek__BackingField_24)); }
	inline bool get_U3CPingU3Ek__BackingField_24() const { return ___U3CPingU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CPingU3Ek__BackingField_24() { return &___U3CPingU3Ek__BackingField_24; }
	inline void set_U3CPingU3Ek__BackingField_24(bool value)
	{
		___U3CPingU3Ek__BackingField_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_T06F79187A31E6958DED19533B1A8783AD1633439_H
#ifndef NODYSETTINGS_T1603F6FD41B39178DA2C000D8ACA773F4AAFA795_H
#define NODYSETTINGS_T1603F6FD41B39178DA2C000D8ACA773F4AAFA795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.NodySettings
struct  NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Single Doozy.Engine.Nody.NodySettings::NormalOpacity
	float ___NormalOpacity_6;
	// System.Single Doozy.Engine.Nody.NodySettings::ActiveOpacity
	float ___ActiveOpacity_7;
	// System.Single Doozy.Engine.Nody.NodySettings::HoverOpacity
	float ___HoverOpacity_8;
	// System.Single Doozy.Engine.Nody.NodySettings::FooterHeight
	float ___FooterHeight_9;
	// System.Single Doozy.Engine.Nody.NodySettings::MaxNodeWidth
	float ___MaxNodeWidth_10;
	// System.Single Doozy.Engine.Nody.NodySettings::MinNodeWidth
	float ___MinNodeWidth_11;
	// System.Single Doozy.Engine.Nody.NodySettings::NodeAccentLineHeight
	float ___NodeAccentLineHeight_12;
	// System.Single Doozy.Engine.Nody.NodySettings::NodeAddSocketButtonSize
	float ___NodeAddSocketButtonSize_13;
	// System.Single Doozy.Engine.Nody.NodySettings::NodeBodyOpacity
	float ___NodeBodyOpacity_14;
	// System.Single Doozy.Engine.Nody.NodySettings::NodeDeleteButtonSize
	float ___NodeDeleteButtonSize_15;
	// System.Single Doozy.Engine.Nody.NodySettings::NodeGlowOpacity
	float ___NodeGlowOpacity_16;
	// System.Single Doozy.Engine.Nody.NodySettings::NodeHeaderHeight
	float ___NodeHeaderHeight_17;
	// System.Single Doozy.Engine.Nody.NodySettings::NodeHeaderIconSize
	float ___NodeHeaderIconSize_18;
	// System.Single Doozy.Engine.Nody.NodySettings::PingColorChangeSpeed
	float ___PingColorChangeSpeed_19;
	// System.Single Doozy.Engine.Nody.NodySettings::SocketConnectedOpacity
	float ___SocketConnectedOpacity_20;
	// System.Single Doozy.Engine.Nody.NodySettings::SocketCurveModifierMaxValue
	float ___SocketCurveModifierMaxValue_21;
	// System.Single Doozy.Engine.Nody.NodySettings::SocketCurveModifierMinValue
	float ___SocketCurveModifierMinValue_22;
	// System.Single Doozy.Engine.Nody.NodySettings::SocketDividerHeight
	float ___SocketDividerHeight_23;
	// System.Single Doozy.Engine.Nody.NodySettings::SocketDividerOpacity
	float ___SocketDividerOpacity_24;
	// System.Single Doozy.Engine.Nody.NodySettings::SocketHeight
	float ___SocketHeight_25;
	// System.Single Doozy.Engine.Nody.NodySettings::SocketNotConnectedOpacity
	float ___SocketNotConnectedOpacity_26;
	// System.Single Doozy.Engine.Nody.NodySettings::SocketVerticalSpacing
	float ___SocketVerticalSpacing_27;
	// System.Single Doozy.Engine.Nody.NodySettings::ConnectionPointHeight
	float ___ConnectionPointHeight_28;
	// System.Single Doozy.Engine.Nody.NodySettings::ConnectionPointOffsetFromLeftMargin
	float ___ConnectionPointOffsetFromLeftMargin_29;
	// System.Single Doozy.Engine.Nody.NodySettings::ConnectionPointOffsetFromRightMargin
	float ___ConnectionPointOffsetFromRightMargin_30;
	// System.Single Doozy.Engine.Nody.NodySettings::ConnectionPointWidth
	float ___ConnectionPointWidth_31;
	// System.Single Doozy.Engine.Nody.NodySettings::CurvePointsMultiplier
	float ___CurvePointsMultiplier_32;
	// System.Single Doozy.Engine.Nody.NodySettings::CurveStrengthModifier
	float ___CurveStrengthModifier_33;
	// System.Single Doozy.Engine.Nody.NodySettings::CurveWidth
	float ___CurveWidth_34;
	// System.Single Doozy.Engine.Nody.NodySettings::DefaultCurveModifier
	float ___DefaultCurveModifier_35;
	// System.Single Doozy.Engine.Nody.NodySettings::MaxCurveModifier
	float ___MaxCurveModifier_36;
	// System.Single Doozy.Engine.Nody.NodySettings::MinCurveModifier
	float ___MinCurveModifier_37;
	// System.Single Doozy.Engine.Nody.NodySettings::GraphTabDividerWidth
	float ___GraphTabDividerWidth_38;
	// System.Single Doozy.Engine.Nody.NodySettings::GraphTabElementSpacing
	float ___GraphTabElementSpacing_39;
	// System.Single Doozy.Engine.Nody.NodySettings::GraphTabMaximumWidth
	float ___GraphTabMaximumWidth_40;
	// System.Single Doozy.Engine.Nody.NodySettings::GraphTabMinimumWidth
	float ___GraphTabMinimumWidth_41;
	// System.Single Doozy.Engine.Nody.NodySettings::GraphTabsAreaHeight
	float ___GraphTabsAreaHeight_42;
	// System.Single Doozy.Engine.Nody.NodySettings::GraphTabsBackgroundOpacity
	float ___GraphTabsBackgroundOpacity_43;
	// System.Double Doozy.Engine.Nody.NodySettings::RepaintIntervalDuringPlayMode
	double ___RepaintIntervalDuringPlayMode_44;
	// System.Double Doozy.Engine.Nody.NodySettings::RepaintIntervalWhileIdle
	double ___RepaintIntervalWhileIdle_45;
	// System.String Doozy.Engine.Nody.NodySettings::EditorPrefsKeyWindowToolbar
	String_t* ___EditorPrefsKeyWindowToolbar_46;
	// System.String Doozy.Engine.Nody.NodySettings::EditorPrefsKeyDotAnimationSpeed
	String_t* ___EditorPrefsKeyDotAnimationSpeed_47;
	// System.Single Doozy.Engine.Nody.NodySettings::DefaultNodeHeight
	float ___DefaultNodeHeight_48;
	// System.Single Doozy.Engine.Nody.NodySettings::DefaultNodeWidth
	float ___DefaultNodeWidth_49;
	// System.Single Doozy.Engine.Nody.NodySettings::EnterNodeWidth
	float ___EnterNodeWidth_50;
	// System.Single Doozy.Engine.Nody.NodySettings::ExitNodeWidth
	float ___ExitNodeWidth_51;
	// System.Single Doozy.Engine.Nody.NodySettings::StartNodeWidth
	float ___StartNodeWidth_52;
	// System.Single Doozy.Engine.Nody.NodySettings::SubGraphNodeWidth
	float ___SubGraphNodeWidth_53;
	// System.Single Doozy.Engine.Nody.NodySettings::SwitchBackNodeWidth
	float ___SwitchBackNodeWidth_54;
	// UnityEngine.HideFlags Doozy.Engine.Nody.NodySettings::DefaultHideFlagsForNodes
	int32_t ___DefaultHideFlagsForNodes_55;

public:
	inline static int32_t get_offset_of_NormalOpacity_6() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___NormalOpacity_6)); }
	inline float get_NormalOpacity_6() const { return ___NormalOpacity_6; }
	inline float* get_address_of_NormalOpacity_6() { return &___NormalOpacity_6; }
	inline void set_NormalOpacity_6(float value)
	{
		___NormalOpacity_6 = value;
	}

	inline static int32_t get_offset_of_ActiveOpacity_7() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___ActiveOpacity_7)); }
	inline float get_ActiveOpacity_7() const { return ___ActiveOpacity_7; }
	inline float* get_address_of_ActiveOpacity_7() { return &___ActiveOpacity_7; }
	inline void set_ActiveOpacity_7(float value)
	{
		___ActiveOpacity_7 = value;
	}

	inline static int32_t get_offset_of_HoverOpacity_8() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___HoverOpacity_8)); }
	inline float get_HoverOpacity_8() const { return ___HoverOpacity_8; }
	inline float* get_address_of_HoverOpacity_8() { return &___HoverOpacity_8; }
	inline void set_HoverOpacity_8(float value)
	{
		___HoverOpacity_8 = value;
	}

	inline static int32_t get_offset_of_FooterHeight_9() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___FooterHeight_9)); }
	inline float get_FooterHeight_9() const { return ___FooterHeight_9; }
	inline float* get_address_of_FooterHeight_9() { return &___FooterHeight_9; }
	inline void set_FooterHeight_9(float value)
	{
		___FooterHeight_9 = value;
	}

	inline static int32_t get_offset_of_MaxNodeWidth_10() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___MaxNodeWidth_10)); }
	inline float get_MaxNodeWidth_10() const { return ___MaxNodeWidth_10; }
	inline float* get_address_of_MaxNodeWidth_10() { return &___MaxNodeWidth_10; }
	inline void set_MaxNodeWidth_10(float value)
	{
		___MaxNodeWidth_10 = value;
	}

	inline static int32_t get_offset_of_MinNodeWidth_11() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___MinNodeWidth_11)); }
	inline float get_MinNodeWidth_11() const { return ___MinNodeWidth_11; }
	inline float* get_address_of_MinNodeWidth_11() { return &___MinNodeWidth_11; }
	inline void set_MinNodeWidth_11(float value)
	{
		___MinNodeWidth_11 = value;
	}

	inline static int32_t get_offset_of_NodeAccentLineHeight_12() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___NodeAccentLineHeight_12)); }
	inline float get_NodeAccentLineHeight_12() const { return ___NodeAccentLineHeight_12; }
	inline float* get_address_of_NodeAccentLineHeight_12() { return &___NodeAccentLineHeight_12; }
	inline void set_NodeAccentLineHeight_12(float value)
	{
		___NodeAccentLineHeight_12 = value;
	}

	inline static int32_t get_offset_of_NodeAddSocketButtonSize_13() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___NodeAddSocketButtonSize_13)); }
	inline float get_NodeAddSocketButtonSize_13() const { return ___NodeAddSocketButtonSize_13; }
	inline float* get_address_of_NodeAddSocketButtonSize_13() { return &___NodeAddSocketButtonSize_13; }
	inline void set_NodeAddSocketButtonSize_13(float value)
	{
		___NodeAddSocketButtonSize_13 = value;
	}

	inline static int32_t get_offset_of_NodeBodyOpacity_14() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___NodeBodyOpacity_14)); }
	inline float get_NodeBodyOpacity_14() const { return ___NodeBodyOpacity_14; }
	inline float* get_address_of_NodeBodyOpacity_14() { return &___NodeBodyOpacity_14; }
	inline void set_NodeBodyOpacity_14(float value)
	{
		___NodeBodyOpacity_14 = value;
	}

	inline static int32_t get_offset_of_NodeDeleteButtonSize_15() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___NodeDeleteButtonSize_15)); }
	inline float get_NodeDeleteButtonSize_15() const { return ___NodeDeleteButtonSize_15; }
	inline float* get_address_of_NodeDeleteButtonSize_15() { return &___NodeDeleteButtonSize_15; }
	inline void set_NodeDeleteButtonSize_15(float value)
	{
		___NodeDeleteButtonSize_15 = value;
	}

	inline static int32_t get_offset_of_NodeGlowOpacity_16() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___NodeGlowOpacity_16)); }
	inline float get_NodeGlowOpacity_16() const { return ___NodeGlowOpacity_16; }
	inline float* get_address_of_NodeGlowOpacity_16() { return &___NodeGlowOpacity_16; }
	inline void set_NodeGlowOpacity_16(float value)
	{
		___NodeGlowOpacity_16 = value;
	}

	inline static int32_t get_offset_of_NodeHeaderHeight_17() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___NodeHeaderHeight_17)); }
	inline float get_NodeHeaderHeight_17() const { return ___NodeHeaderHeight_17; }
	inline float* get_address_of_NodeHeaderHeight_17() { return &___NodeHeaderHeight_17; }
	inline void set_NodeHeaderHeight_17(float value)
	{
		___NodeHeaderHeight_17 = value;
	}

	inline static int32_t get_offset_of_NodeHeaderIconSize_18() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___NodeHeaderIconSize_18)); }
	inline float get_NodeHeaderIconSize_18() const { return ___NodeHeaderIconSize_18; }
	inline float* get_address_of_NodeHeaderIconSize_18() { return &___NodeHeaderIconSize_18; }
	inline void set_NodeHeaderIconSize_18(float value)
	{
		___NodeHeaderIconSize_18 = value;
	}

	inline static int32_t get_offset_of_PingColorChangeSpeed_19() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___PingColorChangeSpeed_19)); }
	inline float get_PingColorChangeSpeed_19() const { return ___PingColorChangeSpeed_19; }
	inline float* get_address_of_PingColorChangeSpeed_19() { return &___PingColorChangeSpeed_19; }
	inline void set_PingColorChangeSpeed_19(float value)
	{
		___PingColorChangeSpeed_19 = value;
	}

	inline static int32_t get_offset_of_SocketConnectedOpacity_20() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___SocketConnectedOpacity_20)); }
	inline float get_SocketConnectedOpacity_20() const { return ___SocketConnectedOpacity_20; }
	inline float* get_address_of_SocketConnectedOpacity_20() { return &___SocketConnectedOpacity_20; }
	inline void set_SocketConnectedOpacity_20(float value)
	{
		___SocketConnectedOpacity_20 = value;
	}

	inline static int32_t get_offset_of_SocketCurveModifierMaxValue_21() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___SocketCurveModifierMaxValue_21)); }
	inline float get_SocketCurveModifierMaxValue_21() const { return ___SocketCurveModifierMaxValue_21; }
	inline float* get_address_of_SocketCurveModifierMaxValue_21() { return &___SocketCurveModifierMaxValue_21; }
	inline void set_SocketCurveModifierMaxValue_21(float value)
	{
		___SocketCurveModifierMaxValue_21 = value;
	}

	inline static int32_t get_offset_of_SocketCurveModifierMinValue_22() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___SocketCurveModifierMinValue_22)); }
	inline float get_SocketCurveModifierMinValue_22() const { return ___SocketCurveModifierMinValue_22; }
	inline float* get_address_of_SocketCurveModifierMinValue_22() { return &___SocketCurveModifierMinValue_22; }
	inline void set_SocketCurveModifierMinValue_22(float value)
	{
		___SocketCurveModifierMinValue_22 = value;
	}

	inline static int32_t get_offset_of_SocketDividerHeight_23() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___SocketDividerHeight_23)); }
	inline float get_SocketDividerHeight_23() const { return ___SocketDividerHeight_23; }
	inline float* get_address_of_SocketDividerHeight_23() { return &___SocketDividerHeight_23; }
	inline void set_SocketDividerHeight_23(float value)
	{
		___SocketDividerHeight_23 = value;
	}

	inline static int32_t get_offset_of_SocketDividerOpacity_24() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___SocketDividerOpacity_24)); }
	inline float get_SocketDividerOpacity_24() const { return ___SocketDividerOpacity_24; }
	inline float* get_address_of_SocketDividerOpacity_24() { return &___SocketDividerOpacity_24; }
	inline void set_SocketDividerOpacity_24(float value)
	{
		___SocketDividerOpacity_24 = value;
	}

	inline static int32_t get_offset_of_SocketHeight_25() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___SocketHeight_25)); }
	inline float get_SocketHeight_25() const { return ___SocketHeight_25; }
	inline float* get_address_of_SocketHeight_25() { return &___SocketHeight_25; }
	inline void set_SocketHeight_25(float value)
	{
		___SocketHeight_25 = value;
	}

	inline static int32_t get_offset_of_SocketNotConnectedOpacity_26() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___SocketNotConnectedOpacity_26)); }
	inline float get_SocketNotConnectedOpacity_26() const { return ___SocketNotConnectedOpacity_26; }
	inline float* get_address_of_SocketNotConnectedOpacity_26() { return &___SocketNotConnectedOpacity_26; }
	inline void set_SocketNotConnectedOpacity_26(float value)
	{
		___SocketNotConnectedOpacity_26 = value;
	}

	inline static int32_t get_offset_of_SocketVerticalSpacing_27() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___SocketVerticalSpacing_27)); }
	inline float get_SocketVerticalSpacing_27() const { return ___SocketVerticalSpacing_27; }
	inline float* get_address_of_SocketVerticalSpacing_27() { return &___SocketVerticalSpacing_27; }
	inline void set_SocketVerticalSpacing_27(float value)
	{
		___SocketVerticalSpacing_27 = value;
	}

	inline static int32_t get_offset_of_ConnectionPointHeight_28() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___ConnectionPointHeight_28)); }
	inline float get_ConnectionPointHeight_28() const { return ___ConnectionPointHeight_28; }
	inline float* get_address_of_ConnectionPointHeight_28() { return &___ConnectionPointHeight_28; }
	inline void set_ConnectionPointHeight_28(float value)
	{
		___ConnectionPointHeight_28 = value;
	}

	inline static int32_t get_offset_of_ConnectionPointOffsetFromLeftMargin_29() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___ConnectionPointOffsetFromLeftMargin_29)); }
	inline float get_ConnectionPointOffsetFromLeftMargin_29() const { return ___ConnectionPointOffsetFromLeftMargin_29; }
	inline float* get_address_of_ConnectionPointOffsetFromLeftMargin_29() { return &___ConnectionPointOffsetFromLeftMargin_29; }
	inline void set_ConnectionPointOffsetFromLeftMargin_29(float value)
	{
		___ConnectionPointOffsetFromLeftMargin_29 = value;
	}

	inline static int32_t get_offset_of_ConnectionPointOffsetFromRightMargin_30() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___ConnectionPointOffsetFromRightMargin_30)); }
	inline float get_ConnectionPointOffsetFromRightMargin_30() const { return ___ConnectionPointOffsetFromRightMargin_30; }
	inline float* get_address_of_ConnectionPointOffsetFromRightMargin_30() { return &___ConnectionPointOffsetFromRightMargin_30; }
	inline void set_ConnectionPointOffsetFromRightMargin_30(float value)
	{
		___ConnectionPointOffsetFromRightMargin_30 = value;
	}

	inline static int32_t get_offset_of_ConnectionPointWidth_31() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___ConnectionPointWidth_31)); }
	inline float get_ConnectionPointWidth_31() const { return ___ConnectionPointWidth_31; }
	inline float* get_address_of_ConnectionPointWidth_31() { return &___ConnectionPointWidth_31; }
	inline void set_ConnectionPointWidth_31(float value)
	{
		___ConnectionPointWidth_31 = value;
	}

	inline static int32_t get_offset_of_CurvePointsMultiplier_32() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___CurvePointsMultiplier_32)); }
	inline float get_CurvePointsMultiplier_32() const { return ___CurvePointsMultiplier_32; }
	inline float* get_address_of_CurvePointsMultiplier_32() { return &___CurvePointsMultiplier_32; }
	inline void set_CurvePointsMultiplier_32(float value)
	{
		___CurvePointsMultiplier_32 = value;
	}

	inline static int32_t get_offset_of_CurveStrengthModifier_33() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___CurveStrengthModifier_33)); }
	inline float get_CurveStrengthModifier_33() const { return ___CurveStrengthModifier_33; }
	inline float* get_address_of_CurveStrengthModifier_33() { return &___CurveStrengthModifier_33; }
	inline void set_CurveStrengthModifier_33(float value)
	{
		___CurveStrengthModifier_33 = value;
	}

	inline static int32_t get_offset_of_CurveWidth_34() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___CurveWidth_34)); }
	inline float get_CurveWidth_34() const { return ___CurveWidth_34; }
	inline float* get_address_of_CurveWidth_34() { return &___CurveWidth_34; }
	inline void set_CurveWidth_34(float value)
	{
		___CurveWidth_34 = value;
	}

	inline static int32_t get_offset_of_DefaultCurveModifier_35() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___DefaultCurveModifier_35)); }
	inline float get_DefaultCurveModifier_35() const { return ___DefaultCurveModifier_35; }
	inline float* get_address_of_DefaultCurveModifier_35() { return &___DefaultCurveModifier_35; }
	inline void set_DefaultCurveModifier_35(float value)
	{
		___DefaultCurveModifier_35 = value;
	}

	inline static int32_t get_offset_of_MaxCurveModifier_36() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___MaxCurveModifier_36)); }
	inline float get_MaxCurveModifier_36() const { return ___MaxCurveModifier_36; }
	inline float* get_address_of_MaxCurveModifier_36() { return &___MaxCurveModifier_36; }
	inline void set_MaxCurveModifier_36(float value)
	{
		___MaxCurveModifier_36 = value;
	}

	inline static int32_t get_offset_of_MinCurveModifier_37() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___MinCurveModifier_37)); }
	inline float get_MinCurveModifier_37() const { return ___MinCurveModifier_37; }
	inline float* get_address_of_MinCurveModifier_37() { return &___MinCurveModifier_37; }
	inline void set_MinCurveModifier_37(float value)
	{
		___MinCurveModifier_37 = value;
	}

	inline static int32_t get_offset_of_GraphTabDividerWidth_38() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___GraphTabDividerWidth_38)); }
	inline float get_GraphTabDividerWidth_38() const { return ___GraphTabDividerWidth_38; }
	inline float* get_address_of_GraphTabDividerWidth_38() { return &___GraphTabDividerWidth_38; }
	inline void set_GraphTabDividerWidth_38(float value)
	{
		___GraphTabDividerWidth_38 = value;
	}

	inline static int32_t get_offset_of_GraphTabElementSpacing_39() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___GraphTabElementSpacing_39)); }
	inline float get_GraphTabElementSpacing_39() const { return ___GraphTabElementSpacing_39; }
	inline float* get_address_of_GraphTabElementSpacing_39() { return &___GraphTabElementSpacing_39; }
	inline void set_GraphTabElementSpacing_39(float value)
	{
		___GraphTabElementSpacing_39 = value;
	}

	inline static int32_t get_offset_of_GraphTabMaximumWidth_40() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___GraphTabMaximumWidth_40)); }
	inline float get_GraphTabMaximumWidth_40() const { return ___GraphTabMaximumWidth_40; }
	inline float* get_address_of_GraphTabMaximumWidth_40() { return &___GraphTabMaximumWidth_40; }
	inline void set_GraphTabMaximumWidth_40(float value)
	{
		___GraphTabMaximumWidth_40 = value;
	}

	inline static int32_t get_offset_of_GraphTabMinimumWidth_41() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___GraphTabMinimumWidth_41)); }
	inline float get_GraphTabMinimumWidth_41() const { return ___GraphTabMinimumWidth_41; }
	inline float* get_address_of_GraphTabMinimumWidth_41() { return &___GraphTabMinimumWidth_41; }
	inline void set_GraphTabMinimumWidth_41(float value)
	{
		___GraphTabMinimumWidth_41 = value;
	}

	inline static int32_t get_offset_of_GraphTabsAreaHeight_42() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___GraphTabsAreaHeight_42)); }
	inline float get_GraphTabsAreaHeight_42() const { return ___GraphTabsAreaHeight_42; }
	inline float* get_address_of_GraphTabsAreaHeight_42() { return &___GraphTabsAreaHeight_42; }
	inline void set_GraphTabsAreaHeight_42(float value)
	{
		___GraphTabsAreaHeight_42 = value;
	}

	inline static int32_t get_offset_of_GraphTabsBackgroundOpacity_43() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___GraphTabsBackgroundOpacity_43)); }
	inline float get_GraphTabsBackgroundOpacity_43() const { return ___GraphTabsBackgroundOpacity_43; }
	inline float* get_address_of_GraphTabsBackgroundOpacity_43() { return &___GraphTabsBackgroundOpacity_43; }
	inline void set_GraphTabsBackgroundOpacity_43(float value)
	{
		___GraphTabsBackgroundOpacity_43 = value;
	}

	inline static int32_t get_offset_of_RepaintIntervalDuringPlayMode_44() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___RepaintIntervalDuringPlayMode_44)); }
	inline double get_RepaintIntervalDuringPlayMode_44() const { return ___RepaintIntervalDuringPlayMode_44; }
	inline double* get_address_of_RepaintIntervalDuringPlayMode_44() { return &___RepaintIntervalDuringPlayMode_44; }
	inline void set_RepaintIntervalDuringPlayMode_44(double value)
	{
		___RepaintIntervalDuringPlayMode_44 = value;
	}

	inline static int32_t get_offset_of_RepaintIntervalWhileIdle_45() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___RepaintIntervalWhileIdle_45)); }
	inline double get_RepaintIntervalWhileIdle_45() const { return ___RepaintIntervalWhileIdle_45; }
	inline double* get_address_of_RepaintIntervalWhileIdle_45() { return &___RepaintIntervalWhileIdle_45; }
	inline void set_RepaintIntervalWhileIdle_45(double value)
	{
		___RepaintIntervalWhileIdle_45 = value;
	}

	inline static int32_t get_offset_of_EditorPrefsKeyWindowToolbar_46() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___EditorPrefsKeyWindowToolbar_46)); }
	inline String_t* get_EditorPrefsKeyWindowToolbar_46() const { return ___EditorPrefsKeyWindowToolbar_46; }
	inline String_t** get_address_of_EditorPrefsKeyWindowToolbar_46() { return &___EditorPrefsKeyWindowToolbar_46; }
	inline void set_EditorPrefsKeyWindowToolbar_46(String_t* value)
	{
		___EditorPrefsKeyWindowToolbar_46 = value;
		Il2CppCodeGenWriteBarrier((&___EditorPrefsKeyWindowToolbar_46), value);
	}

	inline static int32_t get_offset_of_EditorPrefsKeyDotAnimationSpeed_47() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___EditorPrefsKeyDotAnimationSpeed_47)); }
	inline String_t* get_EditorPrefsKeyDotAnimationSpeed_47() const { return ___EditorPrefsKeyDotAnimationSpeed_47; }
	inline String_t** get_address_of_EditorPrefsKeyDotAnimationSpeed_47() { return &___EditorPrefsKeyDotAnimationSpeed_47; }
	inline void set_EditorPrefsKeyDotAnimationSpeed_47(String_t* value)
	{
		___EditorPrefsKeyDotAnimationSpeed_47 = value;
		Il2CppCodeGenWriteBarrier((&___EditorPrefsKeyDotAnimationSpeed_47), value);
	}

	inline static int32_t get_offset_of_DefaultNodeHeight_48() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___DefaultNodeHeight_48)); }
	inline float get_DefaultNodeHeight_48() const { return ___DefaultNodeHeight_48; }
	inline float* get_address_of_DefaultNodeHeight_48() { return &___DefaultNodeHeight_48; }
	inline void set_DefaultNodeHeight_48(float value)
	{
		___DefaultNodeHeight_48 = value;
	}

	inline static int32_t get_offset_of_DefaultNodeWidth_49() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___DefaultNodeWidth_49)); }
	inline float get_DefaultNodeWidth_49() const { return ___DefaultNodeWidth_49; }
	inline float* get_address_of_DefaultNodeWidth_49() { return &___DefaultNodeWidth_49; }
	inline void set_DefaultNodeWidth_49(float value)
	{
		___DefaultNodeWidth_49 = value;
	}

	inline static int32_t get_offset_of_EnterNodeWidth_50() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___EnterNodeWidth_50)); }
	inline float get_EnterNodeWidth_50() const { return ___EnterNodeWidth_50; }
	inline float* get_address_of_EnterNodeWidth_50() { return &___EnterNodeWidth_50; }
	inline void set_EnterNodeWidth_50(float value)
	{
		___EnterNodeWidth_50 = value;
	}

	inline static int32_t get_offset_of_ExitNodeWidth_51() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___ExitNodeWidth_51)); }
	inline float get_ExitNodeWidth_51() const { return ___ExitNodeWidth_51; }
	inline float* get_address_of_ExitNodeWidth_51() { return &___ExitNodeWidth_51; }
	inline void set_ExitNodeWidth_51(float value)
	{
		___ExitNodeWidth_51 = value;
	}

	inline static int32_t get_offset_of_StartNodeWidth_52() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___StartNodeWidth_52)); }
	inline float get_StartNodeWidth_52() const { return ___StartNodeWidth_52; }
	inline float* get_address_of_StartNodeWidth_52() { return &___StartNodeWidth_52; }
	inline void set_StartNodeWidth_52(float value)
	{
		___StartNodeWidth_52 = value;
	}

	inline static int32_t get_offset_of_SubGraphNodeWidth_53() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___SubGraphNodeWidth_53)); }
	inline float get_SubGraphNodeWidth_53() const { return ___SubGraphNodeWidth_53; }
	inline float* get_address_of_SubGraphNodeWidth_53() { return &___SubGraphNodeWidth_53; }
	inline void set_SubGraphNodeWidth_53(float value)
	{
		___SubGraphNodeWidth_53 = value;
	}

	inline static int32_t get_offset_of_SwitchBackNodeWidth_54() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___SwitchBackNodeWidth_54)); }
	inline float get_SwitchBackNodeWidth_54() const { return ___SwitchBackNodeWidth_54; }
	inline float* get_address_of_SwitchBackNodeWidth_54() { return &___SwitchBackNodeWidth_54; }
	inline void set_SwitchBackNodeWidth_54(float value)
	{
		___SwitchBackNodeWidth_54 = value;
	}

	inline static int32_t get_offset_of_DefaultHideFlagsForNodes_55() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795, ___DefaultHideFlagsForNodes_55)); }
	inline int32_t get_DefaultHideFlagsForNodes_55() const { return ___DefaultHideFlagsForNodes_55; }
	inline int32_t* get_address_of_DefaultHideFlagsForNodes_55() { return &___DefaultHideFlagsForNodes_55; }
	inline void set_DefaultHideFlagsForNodes_55(int32_t value)
	{
		___DefaultHideFlagsForNodes_55 = value;
	}
};

struct NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795_StaticFields
{
public:
	// Doozy.Engine.Nody.NodySettings Doozy.Engine.Nody.NodySettings::s_instance
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795 * ___s_instance_5;

public:
	inline static int32_t get_offset_of_s_instance_5() { return static_cast<int32_t>(offsetof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795_StaticFields, ___s_instance_5)); }
	inline NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795 * get_s_instance_5() const { return ___s_instance_5; }
	inline NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795 ** get_address_of_s_instance_5() { return &___s_instance_5; }
	inline void set_s_instance_5(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795 * value)
	{
		___s_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODYSETTINGS_T1603F6FD41B39178DA2C000D8ACA773F4AAFA795_H
#ifndef DOOZYSETTINGS_T69243029B2FB2791093E753122C0199C98F836AF_H
#define DOOZYSETTINGS_T69243029B2FB2791093E753122C0199C98F836AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Settings.DoozySettings
struct  DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Doozy.Engine.Language Doozy.Engine.Settings.DoozySettings::SelectedLanguage
	int32_t ___SelectedLanguage_6;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::AutoDisableUIInteractions
	bool ___AutoDisableUIInteractions_7;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugBackButton
	bool ___DebugBackButton_8;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugGameEventListener
	bool ___DebugGameEventListener_9;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugGameEventManager
	bool ___DebugGameEventManager_10;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugGestureListener
	bool ___DebugGestureListener_11;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugGraphController
	bool ___DebugGraphController_12;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugKeyToGameEvent
	bool ___DebugKeyToGameEvent_13;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugOrientationDetector
	bool ___DebugOrientationDetector_14;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugProgressor
	bool ___DebugProgressor_15;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugProgressorGroup
	bool ___DebugProgressorGroup_16;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugSceneDirector
	bool ___DebugSceneDirector_17;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugSceneLoader
	bool ___DebugSceneLoader_18;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugSoundyController
	bool ___DebugSoundyController_19;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugSoundyManager
	bool ___DebugSoundyManager_20;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugSoundyPooler
	bool ___DebugSoundyPooler_21;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugTouchDetector
	bool ___DebugTouchDetector_22;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugUIButton
	bool ___DebugUIButton_23;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugUIButtonListener
	bool ___DebugUIButtonListener_24;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugUICanvas
	bool ___DebugUICanvas_25;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugUIDrawer
	bool ___DebugUIDrawer_26;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugUIDrawerListener
	bool ___DebugUIDrawerListener_27;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugUIPopup
	bool ___DebugUIPopup_28;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugUIPopupManager
	bool ___DebugUIPopupManager_29;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugUIToggle
	bool ___DebugUIToggle_30;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugUIView
	bool ___DebugUIView_31;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DebugUIViewListener
	bool ___DebugUIViewListener_32;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DOTweenDetected
	bool ___DOTweenDetected_33;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DoozyUIVersion3Detected
	bool ___DoozyUIVersion3Detected_34;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::DoozyUIVersion2Detected
	bool ___DoozyUIVersion2Detected_35;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::MasterAudioDetected
	bool ___MasterAudioDetected_36;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::PlaymakerDetected
	bool ___PlaymakerDetected_37;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::TextMeshProDetected
	bool ___TextMeshProDetected_38;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::IgnoreUnityTimescale
	bool ___IgnoreUnityTimescale_39;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::SpeedBasedAnimations
	bool ___SpeedBasedAnimations_40;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::UseBackButton
	bool ___UseBackButton_41;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::UseMasterAudio
	bool ___UseMasterAudio_42;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::UseOrientationDetector
	bool ___UseOrientationDetector_43;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::UsePlaymaker
	bool ___UsePlaymaker_44;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::UseTextMeshPro
	bool ___UseTextMeshPro_45;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::AssetDatabaseSaveAssetsNeeded
	bool ___AssetDatabaseSaveAssetsNeeded_46;
	// System.Boolean Doozy.Engine.Settings.DoozySettings::AssetDatabaseRefreshNeeded
	bool ___AssetDatabaseRefreshNeeded_47;

public:
	inline static int32_t get_offset_of_SelectedLanguage_6() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___SelectedLanguage_6)); }
	inline int32_t get_SelectedLanguage_6() const { return ___SelectedLanguage_6; }
	inline int32_t* get_address_of_SelectedLanguage_6() { return &___SelectedLanguage_6; }
	inline void set_SelectedLanguage_6(int32_t value)
	{
		___SelectedLanguage_6 = value;
	}

	inline static int32_t get_offset_of_AutoDisableUIInteractions_7() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___AutoDisableUIInteractions_7)); }
	inline bool get_AutoDisableUIInteractions_7() const { return ___AutoDisableUIInteractions_7; }
	inline bool* get_address_of_AutoDisableUIInteractions_7() { return &___AutoDisableUIInteractions_7; }
	inline void set_AutoDisableUIInteractions_7(bool value)
	{
		___AutoDisableUIInteractions_7 = value;
	}

	inline static int32_t get_offset_of_DebugBackButton_8() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugBackButton_8)); }
	inline bool get_DebugBackButton_8() const { return ___DebugBackButton_8; }
	inline bool* get_address_of_DebugBackButton_8() { return &___DebugBackButton_8; }
	inline void set_DebugBackButton_8(bool value)
	{
		___DebugBackButton_8 = value;
	}

	inline static int32_t get_offset_of_DebugGameEventListener_9() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugGameEventListener_9)); }
	inline bool get_DebugGameEventListener_9() const { return ___DebugGameEventListener_9; }
	inline bool* get_address_of_DebugGameEventListener_9() { return &___DebugGameEventListener_9; }
	inline void set_DebugGameEventListener_9(bool value)
	{
		___DebugGameEventListener_9 = value;
	}

	inline static int32_t get_offset_of_DebugGameEventManager_10() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugGameEventManager_10)); }
	inline bool get_DebugGameEventManager_10() const { return ___DebugGameEventManager_10; }
	inline bool* get_address_of_DebugGameEventManager_10() { return &___DebugGameEventManager_10; }
	inline void set_DebugGameEventManager_10(bool value)
	{
		___DebugGameEventManager_10 = value;
	}

	inline static int32_t get_offset_of_DebugGestureListener_11() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugGestureListener_11)); }
	inline bool get_DebugGestureListener_11() const { return ___DebugGestureListener_11; }
	inline bool* get_address_of_DebugGestureListener_11() { return &___DebugGestureListener_11; }
	inline void set_DebugGestureListener_11(bool value)
	{
		___DebugGestureListener_11 = value;
	}

	inline static int32_t get_offset_of_DebugGraphController_12() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugGraphController_12)); }
	inline bool get_DebugGraphController_12() const { return ___DebugGraphController_12; }
	inline bool* get_address_of_DebugGraphController_12() { return &___DebugGraphController_12; }
	inline void set_DebugGraphController_12(bool value)
	{
		___DebugGraphController_12 = value;
	}

	inline static int32_t get_offset_of_DebugKeyToGameEvent_13() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugKeyToGameEvent_13)); }
	inline bool get_DebugKeyToGameEvent_13() const { return ___DebugKeyToGameEvent_13; }
	inline bool* get_address_of_DebugKeyToGameEvent_13() { return &___DebugKeyToGameEvent_13; }
	inline void set_DebugKeyToGameEvent_13(bool value)
	{
		___DebugKeyToGameEvent_13 = value;
	}

	inline static int32_t get_offset_of_DebugOrientationDetector_14() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugOrientationDetector_14)); }
	inline bool get_DebugOrientationDetector_14() const { return ___DebugOrientationDetector_14; }
	inline bool* get_address_of_DebugOrientationDetector_14() { return &___DebugOrientationDetector_14; }
	inline void set_DebugOrientationDetector_14(bool value)
	{
		___DebugOrientationDetector_14 = value;
	}

	inline static int32_t get_offset_of_DebugProgressor_15() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugProgressor_15)); }
	inline bool get_DebugProgressor_15() const { return ___DebugProgressor_15; }
	inline bool* get_address_of_DebugProgressor_15() { return &___DebugProgressor_15; }
	inline void set_DebugProgressor_15(bool value)
	{
		___DebugProgressor_15 = value;
	}

	inline static int32_t get_offset_of_DebugProgressorGroup_16() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugProgressorGroup_16)); }
	inline bool get_DebugProgressorGroup_16() const { return ___DebugProgressorGroup_16; }
	inline bool* get_address_of_DebugProgressorGroup_16() { return &___DebugProgressorGroup_16; }
	inline void set_DebugProgressorGroup_16(bool value)
	{
		___DebugProgressorGroup_16 = value;
	}

	inline static int32_t get_offset_of_DebugSceneDirector_17() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugSceneDirector_17)); }
	inline bool get_DebugSceneDirector_17() const { return ___DebugSceneDirector_17; }
	inline bool* get_address_of_DebugSceneDirector_17() { return &___DebugSceneDirector_17; }
	inline void set_DebugSceneDirector_17(bool value)
	{
		___DebugSceneDirector_17 = value;
	}

	inline static int32_t get_offset_of_DebugSceneLoader_18() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugSceneLoader_18)); }
	inline bool get_DebugSceneLoader_18() const { return ___DebugSceneLoader_18; }
	inline bool* get_address_of_DebugSceneLoader_18() { return &___DebugSceneLoader_18; }
	inline void set_DebugSceneLoader_18(bool value)
	{
		___DebugSceneLoader_18 = value;
	}

	inline static int32_t get_offset_of_DebugSoundyController_19() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugSoundyController_19)); }
	inline bool get_DebugSoundyController_19() const { return ___DebugSoundyController_19; }
	inline bool* get_address_of_DebugSoundyController_19() { return &___DebugSoundyController_19; }
	inline void set_DebugSoundyController_19(bool value)
	{
		___DebugSoundyController_19 = value;
	}

	inline static int32_t get_offset_of_DebugSoundyManager_20() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugSoundyManager_20)); }
	inline bool get_DebugSoundyManager_20() const { return ___DebugSoundyManager_20; }
	inline bool* get_address_of_DebugSoundyManager_20() { return &___DebugSoundyManager_20; }
	inline void set_DebugSoundyManager_20(bool value)
	{
		___DebugSoundyManager_20 = value;
	}

	inline static int32_t get_offset_of_DebugSoundyPooler_21() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugSoundyPooler_21)); }
	inline bool get_DebugSoundyPooler_21() const { return ___DebugSoundyPooler_21; }
	inline bool* get_address_of_DebugSoundyPooler_21() { return &___DebugSoundyPooler_21; }
	inline void set_DebugSoundyPooler_21(bool value)
	{
		___DebugSoundyPooler_21 = value;
	}

	inline static int32_t get_offset_of_DebugTouchDetector_22() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugTouchDetector_22)); }
	inline bool get_DebugTouchDetector_22() const { return ___DebugTouchDetector_22; }
	inline bool* get_address_of_DebugTouchDetector_22() { return &___DebugTouchDetector_22; }
	inline void set_DebugTouchDetector_22(bool value)
	{
		___DebugTouchDetector_22 = value;
	}

	inline static int32_t get_offset_of_DebugUIButton_23() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugUIButton_23)); }
	inline bool get_DebugUIButton_23() const { return ___DebugUIButton_23; }
	inline bool* get_address_of_DebugUIButton_23() { return &___DebugUIButton_23; }
	inline void set_DebugUIButton_23(bool value)
	{
		___DebugUIButton_23 = value;
	}

	inline static int32_t get_offset_of_DebugUIButtonListener_24() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugUIButtonListener_24)); }
	inline bool get_DebugUIButtonListener_24() const { return ___DebugUIButtonListener_24; }
	inline bool* get_address_of_DebugUIButtonListener_24() { return &___DebugUIButtonListener_24; }
	inline void set_DebugUIButtonListener_24(bool value)
	{
		___DebugUIButtonListener_24 = value;
	}

	inline static int32_t get_offset_of_DebugUICanvas_25() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugUICanvas_25)); }
	inline bool get_DebugUICanvas_25() const { return ___DebugUICanvas_25; }
	inline bool* get_address_of_DebugUICanvas_25() { return &___DebugUICanvas_25; }
	inline void set_DebugUICanvas_25(bool value)
	{
		___DebugUICanvas_25 = value;
	}

	inline static int32_t get_offset_of_DebugUIDrawer_26() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugUIDrawer_26)); }
	inline bool get_DebugUIDrawer_26() const { return ___DebugUIDrawer_26; }
	inline bool* get_address_of_DebugUIDrawer_26() { return &___DebugUIDrawer_26; }
	inline void set_DebugUIDrawer_26(bool value)
	{
		___DebugUIDrawer_26 = value;
	}

	inline static int32_t get_offset_of_DebugUIDrawerListener_27() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugUIDrawerListener_27)); }
	inline bool get_DebugUIDrawerListener_27() const { return ___DebugUIDrawerListener_27; }
	inline bool* get_address_of_DebugUIDrawerListener_27() { return &___DebugUIDrawerListener_27; }
	inline void set_DebugUIDrawerListener_27(bool value)
	{
		___DebugUIDrawerListener_27 = value;
	}

	inline static int32_t get_offset_of_DebugUIPopup_28() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugUIPopup_28)); }
	inline bool get_DebugUIPopup_28() const { return ___DebugUIPopup_28; }
	inline bool* get_address_of_DebugUIPopup_28() { return &___DebugUIPopup_28; }
	inline void set_DebugUIPopup_28(bool value)
	{
		___DebugUIPopup_28 = value;
	}

	inline static int32_t get_offset_of_DebugUIPopupManager_29() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugUIPopupManager_29)); }
	inline bool get_DebugUIPopupManager_29() const { return ___DebugUIPopupManager_29; }
	inline bool* get_address_of_DebugUIPopupManager_29() { return &___DebugUIPopupManager_29; }
	inline void set_DebugUIPopupManager_29(bool value)
	{
		___DebugUIPopupManager_29 = value;
	}

	inline static int32_t get_offset_of_DebugUIToggle_30() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugUIToggle_30)); }
	inline bool get_DebugUIToggle_30() const { return ___DebugUIToggle_30; }
	inline bool* get_address_of_DebugUIToggle_30() { return &___DebugUIToggle_30; }
	inline void set_DebugUIToggle_30(bool value)
	{
		___DebugUIToggle_30 = value;
	}

	inline static int32_t get_offset_of_DebugUIView_31() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugUIView_31)); }
	inline bool get_DebugUIView_31() const { return ___DebugUIView_31; }
	inline bool* get_address_of_DebugUIView_31() { return &___DebugUIView_31; }
	inline void set_DebugUIView_31(bool value)
	{
		___DebugUIView_31 = value;
	}

	inline static int32_t get_offset_of_DebugUIViewListener_32() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DebugUIViewListener_32)); }
	inline bool get_DebugUIViewListener_32() const { return ___DebugUIViewListener_32; }
	inline bool* get_address_of_DebugUIViewListener_32() { return &___DebugUIViewListener_32; }
	inline void set_DebugUIViewListener_32(bool value)
	{
		___DebugUIViewListener_32 = value;
	}

	inline static int32_t get_offset_of_DOTweenDetected_33() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DOTweenDetected_33)); }
	inline bool get_DOTweenDetected_33() const { return ___DOTweenDetected_33; }
	inline bool* get_address_of_DOTweenDetected_33() { return &___DOTweenDetected_33; }
	inline void set_DOTweenDetected_33(bool value)
	{
		___DOTweenDetected_33 = value;
	}

	inline static int32_t get_offset_of_DoozyUIVersion3Detected_34() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DoozyUIVersion3Detected_34)); }
	inline bool get_DoozyUIVersion3Detected_34() const { return ___DoozyUIVersion3Detected_34; }
	inline bool* get_address_of_DoozyUIVersion3Detected_34() { return &___DoozyUIVersion3Detected_34; }
	inline void set_DoozyUIVersion3Detected_34(bool value)
	{
		___DoozyUIVersion3Detected_34 = value;
	}

	inline static int32_t get_offset_of_DoozyUIVersion2Detected_35() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___DoozyUIVersion2Detected_35)); }
	inline bool get_DoozyUIVersion2Detected_35() const { return ___DoozyUIVersion2Detected_35; }
	inline bool* get_address_of_DoozyUIVersion2Detected_35() { return &___DoozyUIVersion2Detected_35; }
	inline void set_DoozyUIVersion2Detected_35(bool value)
	{
		___DoozyUIVersion2Detected_35 = value;
	}

	inline static int32_t get_offset_of_MasterAudioDetected_36() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___MasterAudioDetected_36)); }
	inline bool get_MasterAudioDetected_36() const { return ___MasterAudioDetected_36; }
	inline bool* get_address_of_MasterAudioDetected_36() { return &___MasterAudioDetected_36; }
	inline void set_MasterAudioDetected_36(bool value)
	{
		___MasterAudioDetected_36 = value;
	}

	inline static int32_t get_offset_of_PlaymakerDetected_37() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___PlaymakerDetected_37)); }
	inline bool get_PlaymakerDetected_37() const { return ___PlaymakerDetected_37; }
	inline bool* get_address_of_PlaymakerDetected_37() { return &___PlaymakerDetected_37; }
	inline void set_PlaymakerDetected_37(bool value)
	{
		___PlaymakerDetected_37 = value;
	}

	inline static int32_t get_offset_of_TextMeshProDetected_38() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___TextMeshProDetected_38)); }
	inline bool get_TextMeshProDetected_38() const { return ___TextMeshProDetected_38; }
	inline bool* get_address_of_TextMeshProDetected_38() { return &___TextMeshProDetected_38; }
	inline void set_TextMeshProDetected_38(bool value)
	{
		___TextMeshProDetected_38 = value;
	}

	inline static int32_t get_offset_of_IgnoreUnityTimescale_39() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___IgnoreUnityTimescale_39)); }
	inline bool get_IgnoreUnityTimescale_39() const { return ___IgnoreUnityTimescale_39; }
	inline bool* get_address_of_IgnoreUnityTimescale_39() { return &___IgnoreUnityTimescale_39; }
	inline void set_IgnoreUnityTimescale_39(bool value)
	{
		___IgnoreUnityTimescale_39 = value;
	}

	inline static int32_t get_offset_of_SpeedBasedAnimations_40() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___SpeedBasedAnimations_40)); }
	inline bool get_SpeedBasedAnimations_40() const { return ___SpeedBasedAnimations_40; }
	inline bool* get_address_of_SpeedBasedAnimations_40() { return &___SpeedBasedAnimations_40; }
	inline void set_SpeedBasedAnimations_40(bool value)
	{
		___SpeedBasedAnimations_40 = value;
	}

	inline static int32_t get_offset_of_UseBackButton_41() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___UseBackButton_41)); }
	inline bool get_UseBackButton_41() const { return ___UseBackButton_41; }
	inline bool* get_address_of_UseBackButton_41() { return &___UseBackButton_41; }
	inline void set_UseBackButton_41(bool value)
	{
		___UseBackButton_41 = value;
	}

	inline static int32_t get_offset_of_UseMasterAudio_42() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___UseMasterAudio_42)); }
	inline bool get_UseMasterAudio_42() const { return ___UseMasterAudio_42; }
	inline bool* get_address_of_UseMasterAudio_42() { return &___UseMasterAudio_42; }
	inline void set_UseMasterAudio_42(bool value)
	{
		___UseMasterAudio_42 = value;
	}

	inline static int32_t get_offset_of_UseOrientationDetector_43() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___UseOrientationDetector_43)); }
	inline bool get_UseOrientationDetector_43() const { return ___UseOrientationDetector_43; }
	inline bool* get_address_of_UseOrientationDetector_43() { return &___UseOrientationDetector_43; }
	inline void set_UseOrientationDetector_43(bool value)
	{
		___UseOrientationDetector_43 = value;
	}

	inline static int32_t get_offset_of_UsePlaymaker_44() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___UsePlaymaker_44)); }
	inline bool get_UsePlaymaker_44() const { return ___UsePlaymaker_44; }
	inline bool* get_address_of_UsePlaymaker_44() { return &___UsePlaymaker_44; }
	inline void set_UsePlaymaker_44(bool value)
	{
		___UsePlaymaker_44 = value;
	}

	inline static int32_t get_offset_of_UseTextMeshPro_45() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___UseTextMeshPro_45)); }
	inline bool get_UseTextMeshPro_45() const { return ___UseTextMeshPro_45; }
	inline bool* get_address_of_UseTextMeshPro_45() { return &___UseTextMeshPro_45; }
	inline void set_UseTextMeshPro_45(bool value)
	{
		___UseTextMeshPro_45 = value;
	}

	inline static int32_t get_offset_of_AssetDatabaseSaveAssetsNeeded_46() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___AssetDatabaseSaveAssetsNeeded_46)); }
	inline bool get_AssetDatabaseSaveAssetsNeeded_46() const { return ___AssetDatabaseSaveAssetsNeeded_46; }
	inline bool* get_address_of_AssetDatabaseSaveAssetsNeeded_46() { return &___AssetDatabaseSaveAssetsNeeded_46; }
	inline void set_AssetDatabaseSaveAssetsNeeded_46(bool value)
	{
		___AssetDatabaseSaveAssetsNeeded_46 = value;
	}

	inline static int32_t get_offset_of_AssetDatabaseRefreshNeeded_47() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF, ___AssetDatabaseRefreshNeeded_47)); }
	inline bool get_AssetDatabaseRefreshNeeded_47() const { return ___AssetDatabaseRefreshNeeded_47; }
	inline bool* get_address_of_AssetDatabaseRefreshNeeded_47() { return &___AssetDatabaseRefreshNeeded_47; }
	inline void set_AssetDatabaseRefreshNeeded_47(bool value)
	{
		___AssetDatabaseRefreshNeeded_47 = value;
	}
};

struct DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF_StaticFields
{
public:
	// Doozy.Engine.Settings.DoozySettings Doozy.Engine.Settings.DoozySettings::s_instance
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF * ___s_instance_5;

public:
	inline static int32_t get_offset_of_s_instance_5() { return static_cast<int32_t>(offsetof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF_StaticFields, ___s_instance_5)); }
	inline DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF * get_s_instance_5() const { return ___s_instance_5; }
	inline DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF ** get_address_of_s_instance_5() { return &___s_instance_5; }
	inline void set_s_instance_5(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF * value)
	{
		___s_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOOZYSETTINGS_T69243029B2FB2791093E753122C0199C98F836AF_H
#ifndef SOUNDYSETTINGS_TE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E_H
#define SOUNDYSETTINGS_TE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundySettings
struct  SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Doozy.Engine.Soundy.SoundyDatabase Doozy.Engine.Soundy.SoundySettings::database
	SoundyDatabase_t053E1E9D0784E5A630FB918EFC5A7ACAA1D45656 * ___database_6;
	// System.Boolean Doozy.Engine.Soundy.SoundySettings::AutoKillIdleControllers
	bool ___AutoKillIdleControllers_17;
	// System.Single Doozy.Engine.Soundy.SoundySettings::ControllerIdleKillDuration
	float ___ControllerIdleKillDuration_18;
	// System.Single Doozy.Engine.Soundy.SoundySettings::IdleCheckInterval
	float ___IdleCheckInterval_19;
	// System.Int32 Doozy.Engine.Soundy.SoundySettings::MinimumNumberOfControllers
	int32_t ___MinimumNumberOfControllers_20;

public:
	inline static int32_t get_offset_of_database_6() { return static_cast<int32_t>(offsetof(SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E, ___database_6)); }
	inline SoundyDatabase_t053E1E9D0784E5A630FB918EFC5A7ACAA1D45656 * get_database_6() const { return ___database_6; }
	inline SoundyDatabase_t053E1E9D0784E5A630FB918EFC5A7ACAA1D45656 ** get_address_of_database_6() { return &___database_6; }
	inline void set_database_6(SoundyDatabase_t053E1E9D0784E5A630FB918EFC5A7ACAA1D45656 * value)
	{
		___database_6 = value;
		Il2CppCodeGenWriteBarrier((&___database_6), value);
	}

	inline static int32_t get_offset_of_AutoKillIdleControllers_17() { return static_cast<int32_t>(offsetof(SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E, ___AutoKillIdleControllers_17)); }
	inline bool get_AutoKillIdleControllers_17() const { return ___AutoKillIdleControllers_17; }
	inline bool* get_address_of_AutoKillIdleControllers_17() { return &___AutoKillIdleControllers_17; }
	inline void set_AutoKillIdleControllers_17(bool value)
	{
		___AutoKillIdleControllers_17 = value;
	}

	inline static int32_t get_offset_of_ControllerIdleKillDuration_18() { return static_cast<int32_t>(offsetof(SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E, ___ControllerIdleKillDuration_18)); }
	inline float get_ControllerIdleKillDuration_18() const { return ___ControllerIdleKillDuration_18; }
	inline float* get_address_of_ControllerIdleKillDuration_18() { return &___ControllerIdleKillDuration_18; }
	inline void set_ControllerIdleKillDuration_18(float value)
	{
		___ControllerIdleKillDuration_18 = value;
	}

	inline static int32_t get_offset_of_IdleCheckInterval_19() { return static_cast<int32_t>(offsetof(SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E, ___IdleCheckInterval_19)); }
	inline float get_IdleCheckInterval_19() const { return ___IdleCheckInterval_19; }
	inline float* get_address_of_IdleCheckInterval_19() { return &___IdleCheckInterval_19; }
	inline void set_IdleCheckInterval_19(float value)
	{
		___IdleCheckInterval_19 = value;
	}

	inline static int32_t get_offset_of_MinimumNumberOfControllers_20() { return static_cast<int32_t>(offsetof(SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E, ___MinimumNumberOfControllers_20)); }
	inline int32_t get_MinimumNumberOfControllers_20() const { return ___MinimumNumberOfControllers_20; }
	inline int32_t* get_address_of_MinimumNumberOfControllers_20() { return &___MinimumNumberOfControllers_20; }
	inline void set_MinimumNumberOfControllers_20(int32_t value)
	{
		___MinimumNumberOfControllers_20 = value;
	}
};

struct SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E_StaticFields
{
public:
	// Doozy.Engine.Soundy.SoundySettings Doozy.Engine.Soundy.SoundySettings::s_instance
	SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E * ___s_instance_5;

public:
	inline static int32_t get_offset_of_s_instance_5() { return static_cast<int32_t>(offsetof(SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E_StaticFields, ___s_instance_5)); }
	inline SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E * get_s_instance_5() const { return ___s_instance_5; }
	inline SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E ** get_address_of_s_instance_5() { return &___s_instance_5; }
	inline void set_s_instance_5(SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E * value)
	{
		___s_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDYSETTINGS_TE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef ENTERNODE_T9939AA17A69CB75E2DFECC851387FADD605B73EC_H
#define ENTERNODE_T9939AA17A69CB75E2DFECC851387FADD605B73EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Nodes.EnterNode
struct  EnterNode_t9939AA17A69CB75E2DFECC851387FADD605B73EC  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTERNODE_T9939AA17A69CB75E2DFECC851387FADD605B73EC_H
#ifndef EXITNODE_TECFEE135BC0FB5750DD87802345C614D28A4F69F_H
#define EXITNODE_TECFEE135BC0FB5750DD87802345C614D28A4F69F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Nodes.ExitNode
struct  ExitNode_tECFEE135BC0FB5750DD87802345C614D28A4F69F  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXITNODE_TECFEE135BC0FB5750DD87802345C614D28A4F69F_H
#ifndef STARTNODE_TBE558A1A7130F82DC87D5575BA19B8E42122D756_H
#define STARTNODE_TBE558A1A7130F82DC87D5575BA19B8E42122D756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Nodes.StartNode
struct  StartNode_tBE558A1A7130F82DC87D5575BA19B8E42122D756  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTNODE_TBE558A1A7130F82DC87D5575BA19B8E42122D756_H
#ifndef SUBGRAPHNODE_T29FD18A981D70336B38CE5270357955B79C8409B_H
#define SUBGRAPHNODE_T29FD18A981D70336B38CE5270357955B79C8409B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Nodes.SubGraphNode
struct  SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// Doozy.Engine.Nody.Models.Graph Doozy.Engine.Nody.Nodes.SubGraphNode::m_subGraph
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * ___m_subGraph_25;
	// System.Boolean Doozy.Engine.Nody.Nodes.SubGraphNode::ErrorNoGraphReferenced
	bool ___ErrorNoGraphReferenced_26;
	// System.Boolean Doozy.Engine.Nody.Nodes.SubGraphNode::ErrorReferencedGraphIsNotSubGraph
	bool ___ErrorReferencedGraphIsNotSubGraph_27;

public:
	inline static int32_t get_offset_of_m_subGraph_25() { return static_cast<int32_t>(offsetof(SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B, ___m_subGraph_25)); }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * get_m_subGraph_25() const { return ___m_subGraph_25; }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 ** get_address_of_m_subGraph_25() { return &___m_subGraph_25; }
	inline void set_m_subGraph_25(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * value)
	{
		___m_subGraph_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_subGraph_25), value);
	}

	inline static int32_t get_offset_of_ErrorNoGraphReferenced_26() { return static_cast<int32_t>(offsetof(SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B, ___ErrorNoGraphReferenced_26)); }
	inline bool get_ErrorNoGraphReferenced_26() const { return ___ErrorNoGraphReferenced_26; }
	inline bool* get_address_of_ErrorNoGraphReferenced_26() { return &___ErrorNoGraphReferenced_26; }
	inline void set_ErrorNoGraphReferenced_26(bool value)
	{
		___ErrorNoGraphReferenced_26 = value;
	}

	inline static int32_t get_offset_of_ErrorReferencedGraphIsNotSubGraph_27() { return static_cast<int32_t>(offsetof(SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B, ___ErrorReferencedGraphIsNotSubGraph_27)); }
	inline bool get_ErrorReferencedGraphIsNotSubGraph_27() const { return ___ErrorReferencedGraphIsNotSubGraph_27; }
	inline bool* get_address_of_ErrorReferencedGraphIsNotSubGraph_27() { return &___ErrorReferencedGraphIsNotSubGraph_27; }
	inline void set_ErrorReferencedGraphIsNotSubGraph_27(bool value)
	{
		___ErrorReferencedGraphIsNotSubGraph_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBGRAPHNODE_T29FD18A981D70336B38CE5270357955B79C8409B_H
#ifndef SWITCHBACKNODE_TB478F26E72806C34F90C0B802B316881A6C7F0B0_H
#define SWITCHBACKNODE_TB478F26E72806C34F90C0B802B316881A6C7F0B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Nodes.SwitchBackNode
struct  SwitchBackNode_tB478F26E72806C34F90C0B802B316881A6C7F0B0  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// Doozy.Engine.Nody.Models.Graph Doozy.Engine.Nody.Nodes.SwitchBackNode::m_targetGraph
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * ___m_targetGraph_25;
	// System.String Doozy.Engine.Nody.Nodes.SwitchBackNode::m_returnSourceOutputSocketId
	String_t* ___m_returnSourceOutputSocketId_26;
	// System.Collections.Generic.List`1<Doozy.Engine.Nody.Nodes.SwitchBackNode_SourceInfo> Doozy.Engine.Nody.Nodes.SwitchBackNode::Sources
	List_1_t33387D4A0B481B002D4EAF57889F1CB4358AF7F1 * ___Sources_27;

public:
	inline static int32_t get_offset_of_m_targetGraph_25() { return static_cast<int32_t>(offsetof(SwitchBackNode_tB478F26E72806C34F90C0B802B316881A6C7F0B0, ___m_targetGraph_25)); }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * get_m_targetGraph_25() const { return ___m_targetGraph_25; }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 ** get_address_of_m_targetGraph_25() { return &___m_targetGraph_25; }
	inline void set_m_targetGraph_25(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * value)
	{
		___m_targetGraph_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_targetGraph_25), value);
	}

	inline static int32_t get_offset_of_m_returnSourceOutputSocketId_26() { return static_cast<int32_t>(offsetof(SwitchBackNode_tB478F26E72806C34F90C0B802B316881A6C7F0B0, ___m_returnSourceOutputSocketId_26)); }
	inline String_t* get_m_returnSourceOutputSocketId_26() const { return ___m_returnSourceOutputSocketId_26; }
	inline String_t** get_address_of_m_returnSourceOutputSocketId_26() { return &___m_returnSourceOutputSocketId_26; }
	inline void set_m_returnSourceOutputSocketId_26(String_t* value)
	{
		___m_returnSourceOutputSocketId_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_returnSourceOutputSocketId_26), value);
	}

	inline static int32_t get_offset_of_Sources_27() { return static_cast<int32_t>(offsetof(SwitchBackNode_tB478F26E72806C34F90C0B802B316881A6C7F0B0, ___Sources_27)); }
	inline List_1_t33387D4A0B481B002D4EAF57889F1CB4358AF7F1 * get_Sources_27() const { return ___Sources_27; }
	inline List_1_t33387D4A0B481B002D4EAF57889F1CB4358AF7F1 ** get_address_of_Sources_27() { return &___Sources_27; }
	inline void set_Sources_27(List_1_t33387D4A0B481B002D4EAF57889F1CB4358AF7F1 * value)
	{
		___Sources_27 = value;
		Il2CppCodeGenWriteBarrier((&___Sources_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHBACKNODE_TB478F26E72806C34F90C0B802B316881A6C7F0B0_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef GRAPHCONTROLLER_T13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8_H
#define GRAPHCONTROLLER_T13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.GraphController
struct  GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.Nody.GraphController::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_7;
	// System.Boolean Doozy.Engine.Nody.GraphController::DebugMode
	bool ___DebugMode_8;
	// System.String Doozy.Engine.Nody.GraphController::ControllerName
	String_t* ___ControllerName_9;
	// System.Boolean Doozy.Engine.Nody.GraphController::DontDestroyControllerOnLoad
	bool ___DontDestroyControllerOnLoad_10;
	// Doozy.Engine.Nody.Models.Graph Doozy.Engine.Nody.GraphController::m_graphModel
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * ___m_graphModel_11;
	// Doozy.Engine.Nody.Models.Graph Doozy.Engine.Nody.GraphController::m_graph
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * ___m_graph_12;

public:
	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8, ___U3CInitializedU3Ek__BackingField_7)); }
	inline bool get_U3CInitializedU3Ek__BackingField_7() const { return ___U3CInitializedU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_7() { return &___U3CInitializedU3Ek__BackingField_7; }
	inline void set_U3CInitializedU3Ek__BackingField_7(bool value)
	{
		___U3CInitializedU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_DebugMode_8() { return static_cast<int32_t>(offsetof(GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8, ___DebugMode_8)); }
	inline bool get_DebugMode_8() const { return ___DebugMode_8; }
	inline bool* get_address_of_DebugMode_8() { return &___DebugMode_8; }
	inline void set_DebugMode_8(bool value)
	{
		___DebugMode_8 = value;
	}

	inline static int32_t get_offset_of_ControllerName_9() { return static_cast<int32_t>(offsetof(GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8, ___ControllerName_9)); }
	inline String_t* get_ControllerName_9() const { return ___ControllerName_9; }
	inline String_t** get_address_of_ControllerName_9() { return &___ControllerName_9; }
	inline void set_ControllerName_9(String_t* value)
	{
		___ControllerName_9 = value;
		Il2CppCodeGenWriteBarrier((&___ControllerName_9), value);
	}

	inline static int32_t get_offset_of_DontDestroyControllerOnLoad_10() { return static_cast<int32_t>(offsetof(GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8, ___DontDestroyControllerOnLoad_10)); }
	inline bool get_DontDestroyControllerOnLoad_10() const { return ___DontDestroyControllerOnLoad_10; }
	inline bool* get_address_of_DontDestroyControllerOnLoad_10() { return &___DontDestroyControllerOnLoad_10; }
	inline void set_DontDestroyControllerOnLoad_10(bool value)
	{
		___DontDestroyControllerOnLoad_10 = value;
	}

	inline static int32_t get_offset_of_m_graphModel_11() { return static_cast<int32_t>(offsetof(GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8, ___m_graphModel_11)); }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * get_m_graphModel_11() const { return ___m_graphModel_11; }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 ** get_address_of_m_graphModel_11() { return &___m_graphModel_11; }
	inline void set_m_graphModel_11(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * value)
	{
		___m_graphModel_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphModel_11), value);
	}

	inline static int32_t get_offset_of_m_graph_12() { return static_cast<int32_t>(offsetof(GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8, ___m_graph_12)); }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * get_m_graph_12() const { return ___m_graph_12; }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 ** get_address_of_m_graph_12() { return &___m_graph_12; }
	inline void set_m_graph_12(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * value)
	{
		___m_graph_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_graph_12), value);
	}
};

struct GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8_StaticFields
{
public:
	// System.Collections.Generic.List`1<Doozy.Engine.Nody.GraphController> Doozy.Engine.Nody.GraphController::Database
	List_1_t31A111DAFD6F46A820A121BA6E1C065DC3352DCB * ___Database_6;

public:
	inline static int32_t get_offset_of_Database_6() { return static_cast<int32_t>(offsetof(GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8_StaticFields, ___Database_6)); }
	inline List_1_t31A111DAFD6F46A820A121BA6E1C065DC3352DCB * get_Database_6() const { return ___Database_6; }
	inline List_1_t31A111DAFD6F46A820A121BA6E1C065DC3352DCB ** get_address_of_Database_6() { return &___Database_6; }
	inline void set_Database_6(List_1_t31A111DAFD6F46A820A121BA6E1C065DC3352DCB * value)
	{
		___Database_6 = value;
		Il2CppCodeGenWriteBarrier((&___Database_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHCONTROLLER_T13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8_H
#ifndef ORIENTATIONDETECTOR_TD98C708BDAF5E3BA382397F9480244931C56625F_H
#define ORIENTATIONDETECTOR_TD98C708BDAF5E3BA382397F9480244931C56625F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Orientation.OrientationDetector
struct  OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.Orientation.OrientationDetector::DebugMode
	bool ___DebugMode_6;
	// Doozy.Engine.Orientation.OrientationEvent Doozy.Engine.Orientation.OrientationDetector::OnOrientationEvent
	OrientationEvent_tCC6D3CFE9251CC74E8F13085EE96BF9A1B2B267B * ___OnOrientationEvent_7;
	// Doozy.Engine.Orientation.DetectedOrientation Doozy.Engine.Orientation.OrientationDetector::m_currentOrientation
	int32_t ___m_currentOrientation_8;
	// UnityEngine.RectTransform Doozy.Engine.Orientation.OrientationDetector::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_9;
	// UnityEngine.Canvas Doozy.Engine.Orientation.OrientationDetector::m_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_canvas_10;
	// System.Int32 Doozy.Engine.Orientation.OrientationDetector::m_deviceOrientationCheckCount
	int32_t ___m_deviceOrientationCheckCount_11;

public:
	inline static int32_t get_offset_of_DebugMode_6() { return static_cast<int32_t>(offsetof(OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F, ___DebugMode_6)); }
	inline bool get_DebugMode_6() const { return ___DebugMode_6; }
	inline bool* get_address_of_DebugMode_6() { return &___DebugMode_6; }
	inline void set_DebugMode_6(bool value)
	{
		___DebugMode_6 = value;
	}

	inline static int32_t get_offset_of_OnOrientationEvent_7() { return static_cast<int32_t>(offsetof(OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F, ___OnOrientationEvent_7)); }
	inline OrientationEvent_tCC6D3CFE9251CC74E8F13085EE96BF9A1B2B267B * get_OnOrientationEvent_7() const { return ___OnOrientationEvent_7; }
	inline OrientationEvent_tCC6D3CFE9251CC74E8F13085EE96BF9A1B2B267B ** get_address_of_OnOrientationEvent_7() { return &___OnOrientationEvent_7; }
	inline void set_OnOrientationEvent_7(OrientationEvent_tCC6D3CFE9251CC74E8F13085EE96BF9A1B2B267B * value)
	{
		___OnOrientationEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnOrientationEvent_7), value);
	}

	inline static int32_t get_offset_of_m_currentOrientation_8() { return static_cast<int32_t>(offsetof(OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F, ___m_currentOrientation_8)); }
	inline int32_t get_m_currentOrientation_8() const { return ___m_currentOrientation_8; }
	inline int32_t* get_address_of_m_currentOrientation_8() { return &___m_currentOrientation_8; }
	inline void set_m_currentOrientation_8(int32_t value)
	{
		___m_currentOrientation_8 = value;
	}

	inline static int32_t get_offset_of_m_rectTransform_9() { return static_cast<int32_t>(offsetof(OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F, ___m_rectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_9() const { return ___m_rectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_9() { return &___m_rectTransform_9; }
	inline void set_m_rectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_canvas_10() { return static_cast<int32_t>(offsetof(OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F, ___m_canvas_10)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_canvas_10() const { return ___m_canvas_10; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_canvas_10() { return &___m_canvas_10; }
	inline void set_m_canvas_10(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_canvas_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_10), value);
	}

	inline static int32_t get_offset_of_m_deviceOrientationCheckCount_11() { return static_cast<int32_t>(offsetof(OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F, ___m_deviceOrientationCheckCount_11)); }
	inline int32_t get_m_deviceOrientationCheckCount_11() const { return ___m_deviceOrientationCheckCount_11; }
	inline int32_t* get_address_of_m_deviceOrientationCheckCount_11() { return &___m_deviceOrientationCheckCount_11; }
	inline void set_m_deviceOrientationCheckCount_11(int32_t value)
	{
		___m_deviceOrientationCheckCount_11 = value;
	}
};

struct OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F_StaticFields
{
public:
	// Doozy.Engine.Orientation.OrientationDetector Doozy.Engine.Orientation.OrientationDetector::s_instance
	OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F * ___s_instance_4;
	// System.Boolean Doozy.Engine.Orientation.OrientationDetector::<ApplicationIsQuitting>k__BackingField
	bool ___U3CApplicationIsQuittingU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_s_instance_4() { return static_cast<int32_t>(offsetof(OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F_StaticFields, ___s_instance_4)); }
	inline OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F * get_s_instance_4() const { return ___s_instance_4; }
	inline OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F ** get_address_of_s_instance_4() { return &___s_instance_4; }
	inline void set_s_instance_4(OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F * value)
	{
		___s_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_4), value);
	}

	inline static int32_t get_offset_of_U3CApplicationIsQuittingU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F_StaticFields, ___U3CApplicationIsQuittingU3Ek__BackingField_5)); }
	inline bool get_U3CApplicationIsQuittingU3Ek__BackingField_5() const { return ___U3CApplicationIsQuittingU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CApplicationIsQuittingU3Ek__BackingField_5() { return &___U3CApplicationIsQuittingU3Ek__BackingField_5; }
	inline void set_U3CApplicationIsQuittingU3Ek__BackingField_5(bool value)
	{
		___U3CApplicationIsQuittingU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATIONDETECTOR_TD98C708BDAF5E3BA382397F9480244931C56625F_H
#ifndef PROGRESSTARGET_T36E87AFF7AE1F4463726F5A65528252FC9A0AB93_H
#define PROGRESSTARGET_T36E87AFF7AE1F4463726F5A65528252FC9A0AB93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.ProgressTarget
struct  ProgressTarget_t36E87AFF7AE1F4463726F5A65528252FC9A0AB93  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSTARGET_T36E87AFF7AE1F4463726F5A65528252FC9A0AB93_H
#ifndef PROGRESSOR_T235D7E32E01D56976A39145AFDDBF6483EA8498A_H
#define PROGRESSOR_T235D7E32E01D56976A39145AFDDBF6483EA8498A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.Progressor
struct  Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.Progress.Progressor::DebugMode
	bool ___DebugMode_9;
	// System.Collections.Generic.List`1<Doozy.Engine.Progress.ProgressTarget> Doozy.Engine.Progress.Progressor::ProgressTargets
	List_1_t2A31FDCF6ACB40936AC743A2EFAF773FF65FE3F9 * ___ProgressTargets_10;
	// System.Boolean Doozy.Engine.Progress.Progressor::AnimateValue
	bool ___AnimateValue_11;
	// System.Single Doozy.Engine.Progress.Progressor::AnimationDuration
	float ___AnimationDuration_12;
	// DG.Tweening.Ease Doozy.Engine.Progress.Progressor::AnimationEase
	int32_t ___AnimationEase_13;
	// System.Boolean Doozy.Engine.Progress.Progressor::AnimationIgnoresUnityTimescale
	bool ___AnimationIgnoresUnityTimescale_14;
	// Doozy.Engine.Progress.ResetValue Doozy.Engine.Progress.Progressor::OnEnableResetValue
	int32_t ___OnEnableResetValue_15;
	// Doozy.Engine.Progress.ResetValue Doozy.Engine.Progress.Progressor::OnDisableResetValue
	int32_t ___OnDisableResetValue_16;
	// System.Single Doozy.Engine.Progress.Progressor::CustomResetValue
	float ___CustomResetValue_17;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.Progress.Progressor::OnValueChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnValueChanged_18;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.Progress.Progressor::OnProgressChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnProgressChanged_19;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.Progress.Progressor::OnInverseProgressChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnInverseProgressChanged_20;
	// System.Single Doozy.Engine.Progress.Progressor::m_minValue
	float ___m_minValue_21;
	// System.Single Doozy.Engine.Progress.Progressor::m_maxValue
	float ___m_maxValue_22;
	// System.Boolean Doozy.Engine.Progress.Progressor::m_wholeNumbers
	bool ___m_wholeNumbers_23;
	// System.Single Doozy.Engine.Progress.Progressor::m_currentValue
	float ___m_currentValue_24;
	// System.Single Doozy.Engine.Progress.Progressor::m_previousValue
	float ___m_previousValue_25;
	// DG.Tweening.Sequence Doozy.Engine.Progress.Progressor::m_animationSequence
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___m_animationSequence_26;
	// System.Single Doozy.Engine.Progress.Progressor::m_value
	float ___m_value_27;
	// System.Single Doozy.Engine.Progress.Progressor::m_progress
	float ___m_progress_28;
	// System.Single Doozy.Engine.Progress.Progressor::m_inverseProgress
	float ___m_inverseProgress_29;
	// System.Boolean Doozy.Engine.Progress.Progressor::m_updatePreviousValue
	bool ___m_updatePreviousValue_30;

public:
	inline static int32_t get_offset_of_DebugMode_9() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___DebugMode_9)); }
	inline bool get_DebugMode_9() const { return ___DebugMode_9; }
	inline bool* get_address_of_DebugMode_9() { return &___DebugMode_9; }
	inline void set_DebugMode_9(bool value)
	{
		___DebugMode_9 = value;
	}

	inline static int32_t get_offset_of_ProgressTargets_10() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___ProgressTargets_10)); }
	inline List_1_t2A31FDCF6ACB40936AC743A2EFAF773FF65FE3F9 * get_ProgressTargets_10() const { return ___ProgressTargets_10; }
	inline List_1_t2A31FDCF6ACB40936AC743A2EFAF773FF65FE3F9 ** get_address_of_ProgressTargets_10() { return &___ProgressTargets_10; }
	inline void set_ProgressTargets_10(List_1_t2A31FDCF6ACB40936AC743A2EFAF773FF65FE3F9 * value)
	{
		___ProgressTargets_10 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressTargets_10), value);
	}

	inline static int32_t get_offset_of_AnimateValue_11() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___AnimateValue_11)); }
	inline bool get_AnimateValue_11() const { return ___AnimateValue_11; }
	inline bool* get_address_of_AnimateValue_11() { return &___AnimateValue_11; }
	inline void set_AnimateValue_11(bool value)
	{
		___AnimateValue_11 = value;
	}

	inline static int32_t get_offset_of_AnimationDuration_12() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___AnimationDuration_12)); }
	inline float get_AnimationDuration_12() const { return ___AnimationDuration_12; }
	inline float* get_address_of_AnimationDuration_12() { return &___AnimationDuration_12; }
	inline void set_AnimationDuration_12(float value)
	{
		___AnimationDuration_12 = value;
	}

	inline static int32_t get_offset_of_AnimationEase_13() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___AnimationEase_13)); }
	inline int32_t get_AnimationEase_13() const { return ___AnimationEase_13; }
	inline int32_t* get_address_of_AnimationEase_13() { return &___AnimationEase_13; }
	inline void set_AnimationEase_13(int32_t value)
	{
		___AnimationEase_13 = value;
	}

	inline static int32_t get_offset_of_AnimationIgnoresUnityTimescale_14() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___AnimationIgnoresUnityTimescale_14)); }
	inline bool get_AnimationIgnoresUnityTimescale_14() const { return ___AnimationIgnoresUnityTimescale_14; }
	inline bool* get_address_of_AnimationIgnoresUnityTimescale_14() { return &___AnimationIgnoresUnityTimescale_14; }
	inline void set_AnimationIgnoresUnityTimescale_14(bool value)
	{
		___AnimationIgnoresUnityTimescale_14 = value;
	}

	inline static int32_t get_offset_of_OnEnableResetValue_15() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___OnEnableResetValue_15)); }
	inline int32_t get_OnEnableResetValue_15() const { return ___OnEnableResetValue_15; }
	inline int32_t* get_address_of_OnEnableResetValue_15() { return &___OnEnableResetValue_15; }
	inline void set_OnEnableResetValue_15(int32_t value)
	{
		___OnEnableResetValue_15 = value;
	}

	inline static int32_t get_offset_of_OnDisableResetValue_16() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___OnDisableResetValue_16)); }
	inline int32_t get_OnDisableResetValue_16() const { return ___OnDisableResetValue_16; }
	inline int32_t* get_address_of_OnDisableResetValue_16() { return &___OnDisableResetValue_16; }
	inline void set_OnDisableResetValue_16(int32_t value)
	{
		___OnDisableResetValue_16 = value;
	}

	inline static int32_t get_offset_of_CustomResetValue_17() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___CustomResetValue_17)); }
	inline float get_CustomResetValue_17() const { return ___CustomResetValue_17; }
	inline float* get_address_of_CustomResetValue_17() { return &___CustomResetValue_17; }
	inline void set_CustomResetValue_17(float value)
	{
		___CustomResetValue_17 = value;
	}

	inline static int32_t get_offset_of_OnValueChanged_18() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___OnValueChanged_18)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnValueChanged_18() const { return ___OnValueChanged_18; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnValueChanged_18() { return &___OnValueChanged_18; }
	inline void set_OnValueChanged_18(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnValueChanged_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_18), value);
	}

	inline static int32_t get_offset_of_OnProgressChanged_19() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___OnProgressChanged_19)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnProgressChanged_19() const { return ___OnProgressChanged_19; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnProgressChanged_19() { return &___OnProgressChanged_19; }
	inline void set_OnProgressChanged_19(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnProgressChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnProgressChanged_19), value);
	}

	inline static int32_t get_offset_of_OnInverseProgressChanged_20() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___OnInverseProgressChanged_20)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnInverseProgressChanged_20() const { return ___OnInverseProgressChanged_20; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnInverseProgressChanged_20() { return &___OnInverseProgressChanged_20; }
	inline void set_OnInverseProgressChanged_20(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnInverseProgressChanged_20 = value;
		Il2CppCodeGenWriteBarrier((&___OnInverseProgressChanged_20), value);
	}

	inline static int32_t get_offset_of_m_minValue_21() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___m_minValue_21)); }
	inline float get_m_minValue_21() const { return ___m_minValue_21; }
	inline float* get_address_of_m_minValue_21() { return &___m_minValue_21; }
	inline void set_m_minValue_21(float value)
	{
		___m_minValue_21 = value;
	}

	inline static int32_t get_offset_of_m_maxValue_22() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___m_maxValue_22)); }
	inline float get_m_maxValue_22() const { return ___m_maxValue_22; }
	inline float* get_address_of_m_maxValue_22() { return &___m_maxValue_22; }
	inline void set_m_maxValue_22(float value)
	{
		___m_maxValue_22 = value;
	}

	inline static int32_t get_offset_of_m_wholeNumbers_23() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___m_wholeNumbers_23)); }
	inline bool get_m_wholeNumbers_23() const { return ___m_wholeNumbers_23; }
	inline bool* get_address_of_m_wholeNumbers_23() { return &___m_wholeNumbers_23; }
	inline void set_m_wholeNumbers_23(bool value)
	{
		___m_wholeNumbers_23 = value;
	}

	inline static int32_t get_offset_of_m_currentValue_24() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___m_currentValue_24)); }
	inline float get_m_currentValue_24() const { return ___m_currentValue_24; }
	inline float* get_address_of_m_currentValue_24() { return &___m_currentValue_24; }
	inline void set_m_currentValue_24(float value)
	{
		___m_currentValue_24 = value;
	}

	inline static int32_t get_offset_of_m_previousValue_25() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___m_previousValue_25)); }
	inline float get_m_previousValue_25() const { return ___m_previousValue_25; }
	inline float* get_address_of_m_previousValue_25() { return &___m_previousValue_25; }
	inline void set_m_previousValue_25(float value)
	{
		___m_previousValue_25 = value;
	}

	inline static int32_t get_offset_of_m_animationSequence_26() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___m_animationSequence_26)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_m_animationSequence_26() const { return ___m_animationSequence_26; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_m_animationSequence_26() { return &___m_animationSequence_26; }
	inline void set_m_animationSequence_26(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___m_animationSequence_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_animationSequence_26), value);
	}

	inline static int32_t get_offset_of_m_value_27() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___m_value_27)); }
	inline float get_m_value_27() const { return ___m_value_27; }
	inline float* get_address_of_m_value_27() { return &___m_value_27; }
	inline void set_m_value_27(float value)
	{
		___m_value_27 = value;
	}

	inline static int32_t get_offset_of_m_progress_28() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___m_progress_28)); }
	inline float get_m_progress_28() const { return ___m_progress_28; }
	inline float* get_address_of_m_progress_28() { return &___m_progress_28; }
	inline void set_m_progress_28(float value)
	{
		___m_progress_28 = value;
	}

	inline static int32_t get_offset_of_m_inverseProgress_29() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___m_inverseProgress_29)); }
	inline float get_m_inverseProgress_29() const { return ___m_inverseProgress_29; }
	inline float* get_address_of_m_inverseProgress_29() { return &___m_inverseProgress_29; }
	inline void set_m_inverseProgress_29(float value)
	{
		___m_inverseProgress_29 = value;
	}

	inline static int32_t get_offset_of_m_updatePreviousValue_30() { return static_cast<int32_t>(offsetof(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A, ___m_updatePreviousValue_30)); }
	inline bool get_m_updatePreviousValue_30() const { return ___m_updatePreviousValue_30; }
	inline bool* get_address_of_m_updatePreviousValue_30() { return &___m_updatePreviousValue_30; }
	inline void set_m_updatePreviousValue_30(bool value)
	{
		___m_updatePreviousValue_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSOR_T235D7E32E01D56976A39145AFDDBF6483EA8498A_H
#ifndef PROGRESSORGROUP_TECF735DF73AE7B1A65C59038545569EE8633265D_H
#define PROGRESSORGROUP_TECF735DF73AE7B1A65C59038545569EE8633265D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.ProgressorGroup
struct  ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.Progress.ProgressorGroup::DebugMode
	bool ___DebugMode_5;
	// System.Collections.Generic.List`1<Doozy.Engine.Progress.Progressor> Doozy.Engine.Progress.ProgressorGroup::Progressors
	List_1_tBB346642EB44B9D182B9B1708BF9EE34A696E946 * ___Progressors_6;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.Progress.ProgressorGroup::OnProgressChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnProgressChanged_7;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.Progress.ProgressorGroup::OnInverseProgressChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnInverseProgressChanged_8;
	// DG.Tweening.Sequence Doozy.Engine.Progress.ProgressorGroup::m_animationSequence
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___m_animationSequence_9;
	// System.Single Doozy.Engine.Progress.ProgressorGroup::m_previousProgress
	float ___m_previousProgress_10;
	// System.Single Doozy.Engine.Progress.ProgressorGroup::m_progress
	float ___m_progress_11;

public:
	inline static int32_t get_offset_of_DebugMode_5() { return static_cast<int32_t>(offsetof(ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D, ___DebugMode_5)); }
	inline bool get_DebugMode_5() const { return ___DebugMode_5; }
	inline bool* get_address_of_DebugMode_5() { return &___DebugMode_5; }
	inline void set_DebugMode_5(bool value)
	{
		___DebugMode_5 = value;
	}

	inline static int32_t get_offset_of_Progressors_6() { return static_cast<int32_t>(offsetof(ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D, ___Progressors_6)); }
	inline List_1_tBB346642EB44B9D182B9B1708BF9EE34A696E946 * get_Progressors_6() const { return ___Progressors_6; }
	inline List_1_tBB346642EB44B9D182B9B1708BF9EE34A696E946 ** get_address_of_Progressors_6() { return &___Progressors_6; }
	inline void set_Progressors_6(List_1_tBB346642EB44B9D182B9B1708BF9EE34A696E946 * value)
	{
		___Progressors_6 = value;
		Il2CppCodeGenWriteBarrier((&___Progressors_6), value);
	}

	inline static int32_t get_offset_of_OnProgressChanged_7() { return static_cast<int32_t>(offsetof(ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D, ___OnProgressChanged_7)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnProgressChanged_7() const { return ___OnProgressChanged_7; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnProgressChanged_7() { return &___OnProgressChanged_7; }
	inline void set_OnProgressChanged_7(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnProgressChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnProgressChanged_7), value);
	}

	inline static int32_t get_offset_of_OnInverseProgressChanged_8() { return static_cast<int32_t>(offsetof(ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D, ___OnInverseProgressChanged_8)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnInverseProgressChanged_8() const { return ___OnInverseProgressChanged_8; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnInverseProgressChanged_8() { return &___OnInverseProgressChanged_8; }
	inline void set_OnInverseProgressChanged_8(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnInverseProgressChanged_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnInverseProgressChanged_8), value);
	}

	inline static int32_t get_offset_of_m_animationSequence_9() { return static_cast<int32_t>(offsetof(ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D, ___m_animationSequence_9)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_m_animationSequence_9() const { return ___m_animationSequence_9; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_m_animationSequence_9() { return &___m_animationSequence_9; }
	inline void set_m_animationSequence_9(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___m_animationSequence_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_animationSequence_9), value);
	}

	inline static int32_t get_offset_of_m_previousProgress_10() { return static_cast<int32_t>(offsetof(ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D, ___m_previousProgress_10)); }
	inline float get_m_previousProgress_10() const { return ___m_previousProgress_10; }
	inline float* get_address_of_m_previousProgress_10() { return &___m_previousProgress_10; }
	inline void set_m_previousProgress_10(float value)
	{
		___m_previousProgress_10 = value;
	}

	inline static int32_t get_offset_of_m_progress_11() { return static_cast<int32_t>(offsetof(ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D, ___m_progress_11)); }
	inline float get_m_progress_11() const { return ___m_progress_11; }
	inline float* get_address_of_m_progress_11() { return &___m_progress_11; }
	inline void set_m_progress_11(float value)
	{
		___m_progress_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSORGROUP_TECF735DF73AE7B1A65C59038545569EE8633265D_H
#ifndef SCENEDIRECTOR_T29FCDE77B5163926AAC8A3F2173F0B8C57EECA76_H
#define SCENEDIRECTOR_T29FCDE77B5163926AAC8A3F2173F0B8C57EECA76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SceneManagement.SceneDirector
struct  SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.SceneManagement.SceneDirector::DebugMode
	bool ___DebugMode_6;
	// Doozy.Engine.SceneManagement.ActiveSceneChangedEvent Doozy.Engine.SceneManagement.SceneDirector::OnActiveSceneChanged
	ActiveSceneChangedEvent_t0AB76BCA01E7396F308C3216662C1A70015DD7D4 * ___OnActiveSceneChanged_7;
	// Doozy.Engine.SceneManagement.SceneLoadedEvent Doozy.Engine.SceneManagement.SceneDirector::OnSceneLoaded
	SceneLoadedEvent_t9EED90839B7F10C9CF694B1DBFC750B7E9632E82 * ___OnSceneLoaded_8;
	// Doozy.Engine.SceneManagement.SceneUnloadedEvent Doozy.Engine.SceneManagement.SceneDirector::OnSceneUnloaded
	SceneUnloadedEvent_tF6E196C99EC905E8F0AA9DE9F5246F3448B41471 * ___OnSceneUnloaded_9;

public:
	inline static int32_t get_offset_of_DebugMode_6() { return static_cast<int32_t>(offsetof(SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76, ___DebugMode_6)); }
	inline bool get_DebugMode_6() const { return ___DebugMode_6; }
	inline bool* get_address_of_DebugMode_6() { return &___DebugMode_6; }
	inline void set_DebugMode_6(bool value)
	{
		___DebugMode_6 = value;
	}

	inline static int32_t get_offset_of_OnActiveSceneChanged_7() { return static_cast<int32_t>(offsetof(SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76, ___OnActiveSceneChanged_7)); }
	inline ActiveSceneChangedEvent_t0AB76BCA01E7396F308C3216662C1A70015DD7D4 * get_OnActiveSceneChanged_7() const { return ___OnActiveSceneChanged_7; }
	inline ActiveSceneChangedEvent_t0AB76BCA01E7396F308C3216662C1A70015DD7D4 ** get_address_of_OnActiveSceneChanged_7() { return &___OnActiveSceneChanged_7; }
	inline void set_OnActiveSceneChanged_7(ActiveSceneChangedEvent_t0AB76BCA01E7396F308C3216662C1A70015DD7D4 * value)
	{
		___OnActiveSceneChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnActiveSceneChanged_7), value);
	}

	inline static int32_t get_offset_of_OnSceneLoaded_8() { return static_cast<int32_t>(offsetof(SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76, ___OnSceneLoaded_8)); }
	inline SceneLoadedEvent_t9EED90839B7F10C9CF694B1DBFC750B7E9632E82 * get_OnSceneLoaded_8() const { return ___OnSceneLoaded_8; }
	inline SceneLoadedEvent_t9EED90839B7F10C9CF694B1DBFC750B7E9632E82 ** get_address_of_OnSceneLoaded_8() { return &___OnSceneLoaded_8; }
	inline void set_OnSceneLoaded_8(SceneLoadedEvent_t9EED90839B7F10C9CF694B1DBFC750B7E9632E82 * value)
	{
		___OnSceneLoaded_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnSceneLoaded_8), value);
	}

	inline static int32_t get_offset_of_OnSceneUnloaded_9() { return static_cast<int32_t>(offsetof(SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76, ___OnSceneUnloaded_9)); }
	inline SceneUnloadedEvent_tF6E196C99EC905E8F0AA9DE9F5246F3448B41471 * get_OnSceneUnloaded_9() const { return ___OnSceneUnloaded_9; }
	inline SceneUnloadedEvent_tF6E196C99EC905E8F0AA9DE9F5246F3448B41471 ** get_address_of_OnSceneUnloaded_9() { return &___OnSceneUnloaded_9; }
	inline void set_OnSceneUnloaded_9(SceneUnloadedEvent_tF6E196C99EC905E8F0AA9DE9F5246F3448B41471 * value)
	{
		___OnSceneUnloaded_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnSceneUnloaded_9), value);
	}
};

struct SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76_StaticFields
{
public:
	// Doozy.Engine.SceneManagement.SceneDirector Doozy.Engine.SceneManagement.SceneDirector::s_instance
	SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76 * ___s_instance_4;
	// System.Boolean Doozy.Engine.SceneManagement.SceneDirector::<ApplicationIsQuitting>k__BackingField
	bool ___U3CApplicationIsQuittingU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_s_instance_4() { return static_cast<int32_t>(offsetof(SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76_StaticFields, ___s_instance_4)); }
	inline SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76 * get_s_instance_4() const { return ___s_instance_4; }
	inline SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76 ** get_address_of_s_instance_4() { return &___s_instance_4; }
	inline void set_s_instance_4(SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76 * value)
	{
		___s_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_4), value);
	}

	inline static int32_t get_offset_of_U3CApplicationIsQuittingU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76_StaticFields, ___U3CApplicationIsQuittingU3Ek__BackingField_5)); }
	inline bool get_U3CApplicationIsQuittingU3Ek__BackingField_5() const { return ___U3CApplicationIsQuittingU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CApplicationIsQuittingU3Ek__BackingField_5() { return &___U3CApplicationIsQuittingU3Ek__BackingField_5; }
	inline void set_U3CApplicationIsQuittingU3Ek__BackingField_5(bool value)
	{
		___U3CApplicationIsQuittingU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEDIRECTOR_T29FCDE77B5163926AAC8A3F2173F0B8C57EECA76_H
#ifndef SCENELOADER_T63D89F22F7E4B632E2F79D117B762F5D38504438_H
#define SCENELOADER_T63D89F22F7E4B632E2F79D117B762F5D38504438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SceneManagement.SceneLoader
struct  SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AsyncOperation Doozy.Engine.SceneManagement.SceneLoader::<CurrentAsyncOperation>k__BackingField
	AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * ___U3CCurrentAsyncOperationU3Ek__BackingField_12;
	// System.Boolean Doozy.Engine.SceneManagement.SceneLoader::AllowSceneActivation
	bool ___AllowSceneActivation_13;
	// System.Boolean Doozy.Engine.SceneManagement.SceneLoader::DebugMode
	bool ___DebugMode_14;
	// Doozy.Engine.SceneManagement.SceneLoadBehavior Doozy.Engine.SceneManagement.SceneLoader::LoadBehavior
	SceneLoadBehavior_t09385F7DAC85769326248B04A71A5434C14D45AF * ___LoadBehavior_15;
	// Doozy.Engine.SceneManagement.GetSceneBy Doozy.Engine.SceneManagement.SceneLoader::GetSceneBy
	int32_t ___GetSceneBy_16;
	// UnityEngine.SceneManagement.LoadSceneMode Doozy.Engine.SceneManagement.SceneLoader::LoadSceneMode
	int32_t ___LoadSceneMode_17;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.SceneManagement.SceneLoader::OnProgressChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnProgressChanged_18;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.SceneManagement.SceneLoader::OnInverseProgressChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnInverseProgressChanged_19;
	// Doozy.Engine.Progress.Progressor Doozy.Engine.SceneManagement.SceneLoader::Progressor
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * ___Progressor_20;
	// System.Single Doozy.Engine.SceneManagement.SceneLoader::SceneActivationDelay
	float ___SceneActivationDelay_21;
	// System.Int32 Doozy.Engine.SceneManagement.SceneLoader::SceneBuildIndex
	int32_t ___SceneBuildIndex_22;
	// System.String Doozy.Engine.SceneManagement.SceneLoader::SceneName
	String_t* ___SceneName_23;
	// System.Boolean Doozy.Engine.SceneManagement.SceneLoader::SelfDestructAfterSceneLoaded
	bool ___SelfDestructAfterSceneLoaded_24;
	// System.Boolean Doozy.Engine.SceneManagement.SceneLoader::m_loadInProgress
	bool ___m_loadInProgress_25;
	// System.Boolean Doozy.Engine.SceneManagement.SceneLoader::m_sceneLoadedAndReady
	bool ___m_sceneLoadedAndReady_26;
	// System.Boolean Doozy.Engine.SceneManagement.SceneLoader::m_activatingScene
	bool ___m_activatingScene_27;
	// System.Single Doozy.Engine.SceneManagement.SceneLoader::m_sceneLoadedAndReadyTime
	float ___m_sceneLoadedAndReadyTime_28;
	// System.Single Doozy.Engine.SceneManagement.SceneLoader::m_progress
	float ___m_progress_29;

public:
	inline static int32_t get_offset_of_U3CCurrentAsyncOperationU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___U3CCurrentAsyncOperationU3Ek__BackingField_12)); }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * get_U3CCurrentAsyncOperationU3Ek__BackingField_12() const { return ___U3CCurrentAsyncOperationU3Ek__BackingField_12; }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D ** get_address_of_U3CCurrentAsyncOperationU3Ek__BackingField_12() { return &___U3CCurrentAsyncOperationU3Ek__BackingField_12; }
	inline void set_U3CCurrentAsyncOperationU3Ek__BackingField_12(AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * value)
	{
		___U3CCurrentAsyncOperationU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentAsyncOperationU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_AllowSceneActivation_13() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___AllowSceneActivation_13)); }
	inline bool get_AllowSceneActivation_13() const { return ___AllowSceneActivation_13; }
	inline bool* get_address_of_AllowSceneActivation_13() { return &___AllowSceneActivation_13; }
	inline void set_AllowSceneActivation_13(bool value)
	{
		___AllowSceneActivation_13 = value;
	}

	inline static int32_t get_offset_of_DebugMode_14() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___DebugMode_14)); }
	inline bool get_DebugMode_14() const { return ___DebugMode_14; }
	inline bool* get_address_of_DebugMode_14() { return &___DebugMode_14; }
	inline void set_DebugMode_14(bool value)
	{
		___DebugMode_14 = value;
	}

	inline static int32_t get_offset_of_LoadBehavior_15() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___LoadBehavior_15)); }
	inline SceneLoadBehavior_t09385F7DAC85769326248B04A71A5434C14D45AF * get_LoadBehavior_15() const { return ___LoadBehavior_15; }
	inline SceneLoadBehavior_t09385F7DAC85769326248B04A71A5434C14D45AF ** get_address_of_LoadBehavior_15() { return &___LoadBehavior_15; }
	inline void set_LoadBehavior_15(SceneLoadBehavior_t09385F7DAC85769326248B04A71A5434C14D45AF * value)
	{
		___LoadBehavior_15 = value;
		Il2CppCodeGenWriteBarrier((&___LoadBehavior_15), value);
	}

	inline static int32_t get_offset_of_GetSceneBy_16() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___GetSceneBy_16)); }
	inline int32_t get_GetSceneBy_16() const { return ___GetSceneBy_16; }
	inline int32_t* get_address_of_GetSceneBy_16() { return &___GetSceneBy_16; }
	inline void set_GetSceneBy_16(int32_t value)
	{
		___GetSceneBy_16 = value;
	}

	inline static int32_t get_offset_of_LoadSceneMode_17() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___LoadSceneMode_17)); }
	inline int32_t get_LoadSceneMode_17() const { return ___LoadSceneMode_17; }
	inline int32_t* get_address_of_LoadSceneMode_17() { return &___LoadSceneMode_17; }
	inline void set_LoadSceneMode_17(int32_t value)
	{
		___LoadSceneMode_17 = value;
	}

	inline static int32_t get_offset_of_OnProgressChanged_18() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___OnProgressChanged_18)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnProgressChanged_18() const { return ___OnProgressChanged_18; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnProgressChanged_18() { return &___OnProgressChanged_18; }
	inline void set_OnProgressChanged_18(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnProgressChanged_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnProgressChanged_18), value);
	}

	inline static int32_t get_offset_of_OnInverseProgressChanged_19() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___OnInverseProgressChanged_19)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnInverseProgressChanged_19() const { return ___OnInverseProgressChanged_19; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnInverseProgressChanged_19() { return &___OnInverseProgressChanged_19; }
	inline void set_OnInverseProgressChanged_19(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnInverseProgressChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnInverseProgressChanged_19), value);
	}

	inline static int32_t get_offset_of_Progressor_20() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___Progressor_20)); }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * get_Progressor_20() const { return ___Progressor_20; }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A ** get_address_of_Progressor_20() { return &___Progressor_20; }
	inline void set_Progressor_20(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * value)
	{
		___Progressor_20 = value;
		Il2CppCodeGenWriteBarrier((&___Progressor_20), value);
	}

	inline static int32_t get_offset_of_SceneActivationDelay_21() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___SceneActivationDelay_21)); }
	inline float get_SceneActivationDelay_21() const { return ___SceneActivationDelay_21; }
	inline float* get_address_of_SceneActivationDelay_21() { return &___SceneActivationDelay_21; }
	inline void set_SceneActivationDelay_21(float value)
	{
		___SceneActivationDelay_21 = value;
	}

	inline static int32_t get_offset_of_SceneBuildIndex_22() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___SceneBuildIndex_22)); }
	inline int32_t get_SceneBuildIndex_22() const { return ___SceneBuildIndex_22; }
	inline int32_t* get_address_of_SceneBuildIndex_22() { return &___SceneBuildIndex_22; }
	inline void set_SceneBuildIndex_22(int32_t value)
	{
		___SceneBuildIndex_22 = value;
	}

	inline static int32_t get_offset_of_SceneName_23() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___SceneName_23)); }
	inline String_t* get_SceneName_23() const { return ___SceneName_23; }
	inline String_t** get_address_of_SceneName_23() { return &___SceneName_23; }
	inline void set_SceneName_23(String_t* value)
	{
		___SceneName_23 = value;
		Il2CppCodeGenWriteBarrier((&___SceneName_23), value);
	}

	inline static int32_t get_offset_of_SelfDestructAfterSceneLoaded_24() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___SelfDestructAfterSceneLoaded_24)); }
	inline bool get_SelfDestructAfterSceneLoaded_24() const { return ___SelfDestructAfterSceneLoaded_24; }
	inline bool* get_address_of_SelfDestructAfterSceneLoaded_24() { return &___SelfDestructAfterSceneLoaded_24; }
	inline void set_SelfDestructAfterSceneLoaded_24(bool value)
	{
		___SelfDestructAfterSceneLoaded_24 = value;
	}

	inline static int32_t get_offset_of_m_loadInProgress_25() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___m_loadInProgress_25)); }
	inline bool get_m_loadInProgress_25() const { return ___m_loadInProgress_25; }
	inline bool* get_address_of_m_loadInProgress_25() { return &___m_loadInProgress_25; }
	inline void set_m_loadInProgress_25(bool value)
	{
		___m_loadInProgress_25 = value;
	}

	inline static int32_t get_offset_of_m_sceneLoadedAndReady_26() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___m_sceneLoadedAndReady_26)); }
	inline bool get_m_sceneLoadedAndReady_26() const { return ___m_sceneLoadedAndReady_26; }
	inline bool* get_address_of_m_sceneLoadedAndReady_26() { return &___m_sceneLoadedAndReady_26; }
	inline void set_m_sceneLoadedAndReady_26(bool value)
	{
		___m_sceneLoadedAndReady_26 = value;
	}

	inline static int32_t get_offset_of_m_activatingScene_27() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___m_activatingScene_27)); }
	inline bool get_m_activatingScene_27() const { return ___m_activatingScene_27; }
	inline bool* get_address_of_m_activatingScene_27() { return &___m_activatingScene_27; }
	inline void set_m_activatingScene_27(bool value)
	{
		___m_activatingScene_27 = value;
	}

	inline static int32_t get_offset_of_m_sceneLoadedAndReadyTime_28() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___m_sceneLoadedAndReadyTime_28)); }
	inline float get_m_sceneLoadedAndReadyTime_28() const { return ___m_sceneLoadedAndReadyTime_28; }
	inline float* get_address_of_m_sceneLoadedAndReadyTime_28() { return &___m_sceneLoadedAndReadyTime_28; }
	inline void set_m_sceneLoadedAndReadyTime_28(float value)
	{
		___m_sceneLoadedAndReadyTime_28 = value;
	}

	inline static int32_t get_offset_of_m_progress_29() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438, ___m_progress_29)); }
	inline float get_m_progress_29() const { return ___m_progress_29; }
	inline float* get_address_of_m_progress_29() { return &___m_progress_29; }
	inline void set_m_progress_29(float value)
	{
		___m_progress_29 = value;
	}
};

struct SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438_StaticFields
{
public:
	// System.Collections.Generic.List`1<Doozy.Engine.SceneManagement.SceneLoader> Doozy.Engine.SceneManagement.SceneLoader::Database
	List_1_tA9542A65BB25F900DB32842C29255A9A48523618 * ___Database_11;

public:
	inline static int32_t get_offset_of_Database_11() { return static_cast<int32_t>(offsetof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438_StaticFields, ___Database_11)); }
	inline List_1_tA9542A65BB25F900DB32842C29255A9A48523618 * get_Database_11() const { return ___Database_11; }
	inline List_1_tA9542A65BB25F900DB32842C29255A9A48523618 ** get_address_of_Database_11() { return &___Database_11; }
	inline void set_Database_11(List_1_tA9542A65BB25F900DB32842C29255A9A48523618 * value)
	{
		___Database_11 = value;
		Il2CppCodeGenWriteBarrier((&___Database_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENELOADER_T63D89F22F7E4B632E2F79D117B762F5D38504438_H
#ifndef SOUNDYMANAGER_T5549AA03D32E68D075C99BD35574C9297F1B5EA2_H
#define SOUNDYMANAGER_T5549AA03D32E68D075C99BD35574C9297F1B5EA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundyManager
struct  SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2_StaticFields
{
public:
	// Doozy.Engine.Soundy.SoundyManager Doozy.Engine.Soundy.SoundyManager::s_instance
	SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2 * ___s_instance_4;
	// System.Boolean Doozy.Engine.Soundy.SoundyManager::ApplicationIsQuitting
	bool ___ApplicationIsQuitting_11;
	// System.Boolean Doozy.Engine.Soundy.SoundyManager::s_initialized
	bool ___s_initialized_12;
	// Doozy.Engine.Soundy.SoundyPooler Doozy.Engine.Soundy.SoundyManager::s_pooler
	SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282 * ___s_pooler_13;

public:
	inline static int32_t get_offset_of_s_instance_4() { return static_cast<int32_t>(offsetof(SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2_StaticFields, ___s_instance_4)); }
	inline SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2 * get_s_instance_4() const { return ___s_instance_4; }
	inline SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2 ** get_address_of_s_instance_4() { return &___s_instance_4; }
	inline void set_s_instance_4(SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2 * value)
	{
		___s_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_4), value);
	}

	inline static int32_t get_offset_of_ApplicationIsQuitting_11() { return static_cast<int32_t>(offsetof(SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2_StaticFields, ___ApplicationIsQuitting_11)); }
	inline bool get_ApplicationIsQuitting_11() const { return ___ApplicationIsQuitting_11; }
	inline bool* get_address_of_ApplicationIsQuitting_11() { return &___ApplicationIsQuitting_11; }
	inline void set_ApplicationIsQuitting_11(bool value)
	{
		___ApplicationIsQuitting_11 = value;
	}

	inline static int32_t get_offset_of_s_initialized_12() { return static_cast<int32_t>(offsetof(SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2_StaticFields, ___s_initialized_12)); }
	inline bool get_s_initialized_12() const { return ___s_initialized_12; }
	inline bool* get_address_of_s_initialized_12() { return &___s_initialized_12; }
	inline void set_s_initialized_12(bool value)
	{
		___s_initialized_12 = value;
	}

	inline static int32_t get_offset_of_s_pooler_13() { return static_cast<int32_t>(offsetof(SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2_StaticFields, ___s_pooler_13)); }
	inline SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282 * get_s_pooler_13() const { return ___s_pooler_13; }
	inline SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282 ** get_address_of_s_pooler_13() { return &___s_pooler_13; }
	inline void set_s_pooler_13(SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282 * value)
	{
		___s_pooler_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_pooler_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDYMANAGER_T5549AA03D32E68D075C99BD35574C9297F1B5EA2_H
#ifndef SOUNDYPOOLER_T8A21CB4F2E65975C256838DCC588368F49FE6282_H
#define SOUNDYPOOLER_T8A21CB4F2E65975C256838DCC588368F49FE6282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundyPooler
struct  SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Coroutine Doozy.Engine.Soundy.SoundyPooler::m_idleCheckCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_idleCheckCoroutine_5;
	// UnityEngine.WaitForSecondsRealtime Doozy.Engine.Soundy.SoundyPooler::m_idleCheckIntervalWaitForSecondsRealtime
	WaitForSecondsRealtime_t0CF361107C4A9E25E0D4CF2F37732CE785235739 * ___m_idleCheckIntervalWaitForSecondsRealtime_6;
	// Doozy.Engine.Soundy.SoundyController Doozy.Engine.Soundy.SoundyPooler::m_tempController
	SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55 * ___m_tempController_7;

public:
	inline static int32_t get_offset_of_m_idleCheckCoroutine_5() { return static_cast<int32_t>(offsetof(SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282, ___m_idleCheckCoroutine_5)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_idleCheckCoroutine_5() const { return ___m_idleCheckCoroutine_5; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_idleCheckCoroutine_5() { return &___m_idleCheckCoroutine_5; }
	inline void set_m_idleCheckCoroutine_5(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_idleCheckCoroutine_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_idleCheckCoroutine_5), value);
	}

	inline static int32_t get_offset_of_m_idleCheckIntervalWaitForSecondsRealtime_6() { return static_cast<int32_t>(offsetof(SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282, ___m_idleCheckIntervalWaitForSecondsRealtime_6)); }
	inline WaitForSecondsRealtime_t0CF361107C4A9E25E0D4CF2F37732CE785235739 * get_m_idleCheckIntervalWaitForSecondsRealtime_6() const { return ___m_idleCheckIntervalWaitForSecondsRealtime_6; }
	inline WaitForSecondsRealtime_t0CF361107C4A9E25E0D4CF2F37732CE785235739 ** get_address_of_m_idleCheckIntervalWaitForSecondsRealtime_6() { return &___m_idleCheckIntervalWaitForSecondsRealtime_6; }
	inline void set_m_idleCheckIntervalWaitForSecondsRealtime_6(WaitForSecondsRealtime_t0CF361107C4A9E25E0D4CF2F37732CE785235739 * value)
	{
		___m_idleCheckIntervalWaitForSecondsRealtime_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_idleCheckIntervalWaitForSecondsRealtime_6), value);
	}

	inline static int32_t get_offset_of_m_tempController_7() { return static_cast<int32_t>(offsetof(SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282, ___m_tempController_7)); }
	inline SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55 * get_m_tempController_7() const { return ___m_tempController_7; }
	inline SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55 ** get_address_of_m_tempController_7() { return &___m_tempController_7; }
	inline void set_m_tempController_7(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55 * value)
	{
		___m_tempController_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_tempController_7), value);
	}
};

struct SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282_StaticFields
{
public:
	// System.Collections.Generic.List`1<Doozy.Engine.Soundy.SoundyController> Doozy.Engine.Soundy.SoundyPooler::s_pool
	List_1_t9BE00C8B071DC24C9723A6663A60205AB423B921 * ___s_pool_4;

public:
	inline static int32_t get_offset_of_s_pool_4() { return static_cast<int32_t>(offsetof(SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282_StaticFields, ___s_pool_4)); }
	inline List_1_t9BE00C8B071DC24C9723A6663A60205AB423B921 * get_s_pool_4() const { return ___s_pool_4; }
	inline List_1_t9BE00C8B071DC24C9723A6663A60205AB423B921 ** get_address_of_s_pool_4() { return &___s_pool_4; }
	inline void set_s_pool_4(List_1_t9BE00C8B071DC24C9723A6663A60205AB423B921 * value)
	{
		___s_pool_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_pool_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDYPOOLER_T8A21CB4F2E65975C256838DCC588368F49FE6282_H
#ifndef PROGRESSTARGETANIMATOR_T192042EEC2CFB1239EDA6E52A9F776FF0FBA95E8_H
#define PROGRESSTARGETANIMATOR_T192042EEC2CFB1239EDA6E52A9F776FF0FBA95E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.ProgressTargetAnimator
struct  ProgressTargetAnimator_t192042EEC2CFB1239EDA6E52A9F776FF0FBA95E8  : public ProgressTarget_t36E87AFF7AE1F4463726F5A65528252FC9A0AB93
{
public:
	// UnityEngine.Animator Doozy.Engine.Progress.ProgressTargetAnimator::Animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___Animator_4;
	// System.String Doozy.Engine.Progress.ProgressTargetAnimator::ParameterName
	String_t* ___ParameterName_5;
	// Doozy.Engine.Progress.TargetProgress Doozy.Engine.Progress.ProgressTargetAnimator::TargetProgress
	int32_t ___TargetProgress_6;

public:
	inline static int32_t get_offset_of_Animator_4() { return static_cast<int32_t>(offsetof(ProgressTargetAnimator_t192042EEC2CFB1239EDA6E52A9F776FF0FBA95E8, ___Animator_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_Animator_4() const { return ___Animator_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_Animator_4() { return &___Animator_4; }
	inline void set_Animator_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___Animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___Animator_4), value);
	}

	inline static int32_t get_offset_of_ParameterName_5() { return static_cast<int32_t>(offsetof(ProgressTargetAnimator_t192042EEC2CFB1239EDA6E52A9F776FF0FBA95E8, ___ParameterName_5)); }
	inline String_t* get_ParameterName_5() const { return ___ParameterName_5; }
	inline String_t** get_address_of_ParameterName_5() { return &___ParameterName_5; }
	inline void set_ParameterName_5(String_t* value)
	{
		___ParameterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParameterName_5), value);
	}

	inline static int32_t get_offset_of_TargetProgress_6() { return static_cast<int32_t>(offsetof(ProgressTargetAnimator_t192042EEC2CFB1239EDA6E52A9F776FF0FBA95E8, ___TargetProgress_6)); }
	inline int32_t get_TargetProgress_6() const { return ___TargetProgress_6; }
	inline int32_t* get_address_of_TargetProgress_6() { return &___TargetProgress_6; }
	inline void set_TargetProgress_6(int32_t value)
	{
		___TargetProgress_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSTARGETANIMATOR_T192042EEC2CFB1239EDA6E52A9F776FF0FBA95E8_H
#ifndef PROGRESSTARGETIMAGE_TF7B42B85F978B44F956C48E024DE81EF62D35D6A_H
#define PROGRESSTARGETIMAGE_TF7B42B85F978B44F956C48E024DE81EF62D35D6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.ProgressTargetImage
struct  ProgressTargetImage_tF7B42B85F978B44F956C48E024DE81EF62D35D6A  : public ProgressTarget_t36E87AFF7AE1F4463726F5A65528252FC9A0AB93
{
public:
	// UnityEngine.UI.Image Doozy.Engine.Progress.ProgressTargetImage::Image
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Image_4;
	// Doozy.Engine.Progress.TargetProgress Doozy.Engine.Progress.ProgressTargetImage::TargetProgress
	int32_t ___TargetProgress_5;

public:
	inline static int32_t get_offset_of_Image_4() { return static_cast<int32_t>(offsetof(ProgressTargetImage_tF7B42B85F978B44F956C48E024DE81EF62D35D6A, ___Image_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Image_4() const { return ___Image_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Image_4() { return &___Image_4; }
	inline void set_Image_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Image_4 = value;
		Il2CppCodeGenWriteBarrier((&___Image_4), value);
	}

	inline static int32_t get_offset_of_TargetProgress_5() { return static_cast<int32_t>(offsetof(ProgressTargetImage_tF7B42B85F978B44F956C48E024DE81EF62D35D6A, ___TargetProgress_5)); }
	inline int32_t get_TargetProgress_5() const { return ___TargetProgress_5; }
	inline int32_t* get_address_of_TargetProgress_5() { return &___TargetProgress_5; }
	inline void set_TargetProgress_5(int32_t value)
	{
		___TargetProgress_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSTARGETIMAGE_TF7B42B85F978B44F956C48E024DE81EF62D35D6A_H
#ifndef PROGRESSTARGETTEXT_T57464BB36CC943114F00DDBAB6AC1F336B04196D_H
#define PROGRESSTARGETTEXT_T57464BB36CC943114F00DDBAB6AC1F336B04196D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.ProgressTargetText
struct  ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D  : public ProgressTarget_t36E87AFF7AE1F4463726F5A65528252FC9A0AB93
{
public:
	// UnityEngine.UI.Text Doozy.Engine.Progress.ProgressTargetText::Text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___Text_4;
	// Doozy.Engine.Progress.TargetVariable Doozy.Engine.Progress.ProgressTargetText::TargetVariable
	int32_t ___TargetVariable_5;
	// System.Boolean Doozy.Engine.Progress.ProgressTargetText::WholeNumbers
	bool ___WholeNumbers_6;
	// System.Boolean Doozy.Engine.Progress.ProgressTargetText::UseMultiplier
	bool ___UseMultiplier_7;
	// System.Single Doozy.Engine.Progress.ProgressTargetText::Multiplier
	float ___Multiplier_8;
	// System.String Doozy.Engine.Progress.ProgressTargetText::Prefix
	String_t* ___Prefix_9;
	// System.String Doozy.Engine.Progress.ProgressTargetText::Suffix
	String_t* ___Suffix_10;
	// System.Single Doozy.Engine.Progress.ProgressTargetText::m_targetValue
	float ___m_targetValue_11;

public:
	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D, ___Text_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_Text_4() const { return ___Text_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier((&___Text_4), value);
	}

	inline static int32_t get_offset_of_TargetVariable_5() { return static_cast<int32_t>(offsetof(ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D, ___TargetVariable_5)); }
	inline int32_t get_TargetVariable_5() const { return ___TargetVariable_5; }
	inline int32_t* get_address_of_TargetVariable_5() { return &___TargetVariable_5; }
	inline void set_TargetVariable_5(int32_t value)
	{
		___TargetVariable_5 = value;
	}

	inline static int32_t get_offset_of_WholeNumbers_6() { return static_cast<int32_t>(offsetof(ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D, ___WholeNumbers_6)); }
	inline bool get_WholeNumbers_6() const { return ___WholeNumbers_6; }
	inline bool* get_address_of_WholeNumbers_6() { return &___WholeNumbers_6; }
	inline void set_WholeNumbers_6(bool value)
	{
		___WholeNumbers_6 = value;
	}

	inline static int32_t get_offset_of_UseMultiplier_7() { return static_cast<int32_t>(offsetof(ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D, ___UseMultiplier_7)); }
	inline bool get_UseMultiplier_7() const { return ___UseMultiplier_7; }
	inline bool* get_address_of_UseMultiplier_7() { return &___UseMultiplier_7; }
	inline void set_UseMultiplier_7(bool value)
	{
		___UseMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_Multiplier_8() { return static_cast<int32_t>(offsetof(ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D, ___Multiplier_8)); }
	inline float get_Multiplier_8() const { return ___Multiplier_8; }
	inline float* get_address_of_Multiplier_8() { return &___Multiplier_8; }
	inline void set_Multiplier_8(float value)
	{
		___Multiplier_8 = value;
	}

	inline static int32_t get_offset_of_Prefix_9() { return static_cast<int32_t>(offsetof(ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D, ___Prefix_9)); }
	inline String_t* get_Prefix_9() const { return ___Prefix_9; }
	inline String_t** get_address_of_Prefix_9() { return &___Prefix_9; }
	inline void set_Prefix_9(String_t* value)
	{
		___Prefix_9 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_9), value);
	}

	inline static int32_t get_offset_of_Suffix_10() { return static_cast<int32_t>(offsetof(ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D, ___Suffix_10)); }
	inline String_t* get_Suffix_10() const { return ___Suffix_10; }
	inline String_t** get_address_of_Suffix_10() { return &___Suffix_10; }
	inline void set_Suffix_10(String_t* value)
	{
		___Suffix_10 = value;
		Il2CppCodeGenWriteBarrier((&___Suffix_10), value);
	}

	inline static int32_t get_offset_of_m_targetValue_11() { return static_cast<int32_t>(offsetof(ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D, ___m_targetValue_11)); }
	inline float get_m_targetValue_11() const { return ___m_targetValue_11; }
	inline float* get_address_of_m_targetValue_11() { return &___m_targetValue_11; }
	inline void set_m_targetValue_11(float value)
	{
		___m_targetValue_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSTARGETTEXT_T57464BB36CC943114F00DDBAB6AC1F336B04196D_H
#ifndef PROGRESSTARGETTEXTMESHPRO_T072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E_H
#define PROGRESSTARGETTEXTMESHPRO_T072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Progress.ProgressTargetTextMeshPro
struct  ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E  : public ProgressTarget_t36E87AFF7AE1F4463726F5A65528252FC9A0AB93
{
public:
	// TMPro.TMP_Text Doozy.Engine.Progress.ProgressTargetTextMeshPro::TextMeshPro
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___TextMeshPro_4;
	// Doozy.Engine.Progress.TargetVariable Doozy.Engine.Progress.ProgressTargetTextMeshPro::TargetVariable
	int32_t ___TargetVariable_5;
	// System.Boolean Doozy.Engine.Progress.ProgressTargetTextMeshPro::WholeNumbers
	bool ___WholeNumbers_6;
	// System.Boolean Doozy.Engine.Progress.ProgressTargetTextMeshPro::UseMultiplier
	bool ___UseMultiplier_7;
	// System.Single Doozy.Engine.Progress.ProgressTargetTextMeshPro::Multiplier
	float ___Multiplier_8;
	// System.String Doozy.Engine.Progress.ProgressTargetTextMeshPro::Prefix
	String_t* ___Prefix_9;
	// System.String Doozy.Engine.Progress.ProgressTargetTextMeshPro::Suffix
	String_t* ___Suffix_10;
	// System.Single Doozy.Engine.Progress.ProgressTargetTextMeshPro::m_targetValue
	float ___m_targetValue_11;

public:
	inline static int32_t get_offset_of_TextMeshPro_4() { return static_cast<int32_t>(offsetof(ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E, ___TextMeshPro_4)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_TextMeshPro_4() const { return ___TextMeshPro_4; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_TextMeshPro_4() { return &___TextMeshPro_4; }
	inline void set_TextMeshPro_4(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___TextMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshPro_4), value);
	}

	inline static int32_t get_offset_of_TargetVariable_5() { return static_cast<int32_t>(offsetof(ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E, ___TargetVariable_5)); }
	inline int32_t get_TargetVariable_5() const { return ___TargetVariable_5; }
	inline int32_t* get_address_of_TargetVariable_5() { return &___TargetVariable_5; }
	inline void set_TargetVariable_5(int32_t value)
	{
		___TargetVariable_5 = value;
	}

	inline static int32_t get_offset_of_WholeNumbers_6() { return static_cast<int32_t>(offsetof(ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E, ___WholeNumbers_6)); }
	inline bool get_WholeNumbers_6() const { return ___WholeNumbers_6; }
	inline bool* get_address_of_WholeNumbers_6() { return &___WholeNumbers_6; }
	inline void set_WholeNumbers_6(bool value)
	{
		___WholeNumbers_6 = value;
	}

	inline static int32_t get_offset_of_UseMultiplier_7() { return static_cast<int32_t>(offsetof(ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E, ___UseMultiplier_7)); }
	inline bool get_UseMultiplier_7() const { return ___UseMultiplier_7; }
	inline bool* get_address_of_UseMultiplier_7() { return &___UseMultiplier_7; }
	inline void set_UseMultiplier_7(bool value)
	{
		___UseMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_Multiplier_8() { return static_cast<int32_t>(offsetof(ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E, ___Multiplier_8)); }
	inline float get_Multiplier_8() const { return ___Multiplier_8; }
	inline float* get_address_of_Multiplier_8() { return &___Multiplier_8; }
	inline void set_Multiplier_8(float value)
	{
		___Multiplier_8 = value;
	}

	inline static int32_t get_offset_of_Prefix_9() { return static_cast<int32_t>(offsetof(ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E, ___Prefix_9)); }
	inline String_t* get_Prefix_9() const { return ___Prefix_9; }
	inline String_t** get_address_of_Prefix_9() { return &___Prefix_9; }
	inline void set_Prefix_9(String_t* value)
	{
		___Prefix_9 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_9), value);
	}

	inline static int32_t get_offset_of_Suffix_10() { return static_cast<int32_t>(offsetof(ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E, ___Suffix_10)); }
	inline String_t* get_Suffix_10() const { return ___Suffix_10; }
	inline String_t** get_address_of_Suffix_10() { return &___Suffix_10; }
	inline void set_Suffix_10(String_t* value)
	{
		___Suffix_10 = value;
		Il2CppCodeGenWriteBarrier((&___Suffix_10), value);
	}

	inline static int32_t get_offset_of_m_targetValue_11() { return static_cast<int32_t>(offsetof(ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E, ___m_targetValue_11)); }
	inline float get_m_targetValue_11() const { return ___m_targetValue_11; }
	inline float* get_address_of_m_targetValue_11() { return &___m_targetValue_11; }
	inline void set_m_targetValue_11(float value)
	{
		___m_targetValue_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSTARGETTEXTMESHPRO_T072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5800 = { sizeof (U3CU3Ec_tB47CCCB4810E026898C7B7306A51ED6D85E61E1D), -1, sizeof(U3CU3Ec_tB47CCCB4810E026898C7B7306A51ED6D85E61E1D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5800[2] = 
{
	U3CU3Ec_tB47CCCB4810E026898C7B7306A51ED6D85E61E1D_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB47CCCB4810E026898C7B7306A51ED6D85E61E1D_StaticFields::get_offset_of_U3CU3E9__56_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5801 = { sizeof (SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5801[5] = 
{
	SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720::get_offset_of_SoundSource_0(),
	SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720::get_offset_of_DatabaseName_1(),
	SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720::get_offset_of_SoundName_2(),
	SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720::get_offset_of_AudioClip_3(),
	SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720::get_offset_of_OutputAudioMixerGroup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5802 = { sizeof (SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2), -1, sizeof(SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5802[10] = 
{
	SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2_StaticFields::get_offset_of_s_instance_4(),
	0,
	0,
	0,
	0,
	0,
	0,
	SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2_StaticFields::get_offset_of_ApplicationIsQuitting_11(),
	SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2_StaticFields::get_offset_of_s_initialized_12(),
	SoundyManager_t5549AA03D32E68D075C99BD35574C9297F1B5EA2_StaticFields::get_offset_of_s_pooler_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5803 = { sizeof (SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282), -1, sizeof(SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5803[4] = 
{
	SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282_StaticFields::get_offset_of_s_pool_4(),
	SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282::get_offset_of_m_idleCheckCoroutine_5(),
	SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282::get_offset_of_m_idleCheckIntervalWaitForSecondsRealtime_6(),
	SoundyPooler_t8A21CB4F2E65975C256838DCC588368F49FE6282::get_offset_of_m_tempController_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5804 = { sizeof (U3CU3Ec_tEAFC2663C9C29EC1AE07289F84C469987E3C9535), -1, sizeof(U3CU3Ec_tEAFC2663C9C29EC1AE07289F84C469987E3C9535_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5804[2] = 
{
	U3CU3Ec_tEAFC2663C9C29EC1AE07289F84C469987E3C9535_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tEAFC2663C9C29EC1AE07289F84C469987E3C9535_StaticFields::get_offset_of_U3CU3E9__28_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5805 = { sizeof (U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5805[6] = 
{
	U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558::get_offset_of_U3CU3E1__state_0(),
	U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558::get_offset_of_U3CU3E2__current_1(),
	U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558::get_offset_of_U3CU3E4__this_2(),
	U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558::get_offset_of_U3CminimumNumberOfControllersU3E5__1_3(),
	U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558::get_offset_of_U3CcontrollerIdleKillDurationU3E5__2_4(),
	U3CKillIdleControllersEnumeratorU3Ed__29_t2905789CD4F4443550082A565343E00767D3D558::get_offset_of_U3CiU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5806 = { sizeof (SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E), -1, sizeof(SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5806[17] = 
{
	0,
	SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E_StaticFields::get_offset_of_s_instance_5(),
	SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E::get_offset_of_database_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E::get_offset_of_AutoKillIdleControllers_17(),
	SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E::get_offset_of_ControllerIdleKillDuration_18(),
	SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E::get_offset_of_IdleCheckInterval_19(),
	SoundySettings_tE1AD13772BB6358F1652240DC3C22CA8CF8A8F2E::get_offset_of_MinimumNumberOfControllers_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5807 = { sizeof (SoundyUtils_t9EE5EFFD61B9D1B30391E6E0093714AE15664040), -1, sizeof(SoundyUtils_t9EE5EFFD61B9D1B30391E6E0093714AE15664040_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5807[1] = 
{
	SoundyUtils_t9EE5EFFD61B9D1B30391E6E0093714AE15664040_StaticFields::get_offset_of_TwelfthRootOfTwo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5808 = { sizeof (DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF), -1, sizeof(DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5808[69] = 
{
	0,
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF_StaticFields::get_offset_of_s_instance_5(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_SelectedLanguage_6(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_AutoDisableUIInteractions_7(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugBackButton_8(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugGameEventListener_9(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugGameEventManager_10(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugGestureListener_11(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugGraphController_12(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugKeyToGameEvent_13(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugOrientationDetector_14(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugProgressor_15(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugProgressorGroup_16(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugSceneDirector_17(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugSceneLoader_18(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugSoundyController_19(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugSoundyManager_20(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugSoundyPooler_21(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugTouchDetector_22(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugUIButton_23(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugUIButtonListener_24(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugUICanvas_25(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugUIDrawer_26(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugUIDrawerListener_27(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugUIPopup_28(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugUIPopupManager_29(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugUIToggle_30(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugUIView_31(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DebugUIViewListener_32(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DOTweenDetected_33(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DoozyUIVersion3Detected_34(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_DoozyUIVersion2Detected_35(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_MasterAudioDetected_36(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_PlaymakerDetected_37(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_TextMeshProDetected_38(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_IgnoreUnityTimescale_39(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_SpeedBasedAnimations_40(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_UseBackButton_41(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_UseMasterAudio_42(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_UseOrientationDetector_43(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_UsePlaymaker_44(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_UseTextMeshPro_45(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_AssetDatabaseSaveAssetsNeeded_46(),
	DoozySettings_t69243029B2FB2791093E753122C0199C98F836AF::get_offset_of_AssetDatabaseRefreshNeeded_47(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5809 = { sizeof (ActiveSceneChangedEvent_t0AB76BCA01E7396F308C3216662C1A70015DD7D4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5810 = { sizeof (GetSceneBy_t7E8029F50A80BF74544C7791080F0A2205968CE9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5810[3] = 
{
	GetSceneBy_t7E8029F50A80BF74544C7791080F0A2205968CE9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5811 = { sizeof (SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76), -1, sizeof(SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5811[6] = 
{
	SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76_StaticFields::get_offset_of_s_instance_4(),
	SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76_StaticFields::get_offset_of_U3CApplicationIsQuittingU3Ek__BackingField_5(),
	SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76::get_offset_of_DebugMode_6(),
	SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76::get_offset_of_OnActiveSceneChanged_7(),
	SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76::get_offset_of_OnSceneLoaded_8(),
	SceneDirector_t29FCDE77B5163926AAC8A3F2173F0B8C57EECA76::get_offset_of_OnSceneUnloaded_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5812 = { sizeof (SceneLoadBehavior_t09385F7DAC85769326248B04A71A5434C14D45AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5812[2] = 
{
	SceneLoadBehavior_t09385F7DAC85769326248B04A71A5434C14D45AF::get_offset_of_OnLoadScene_0(),
	SceneLoadBehavior_t09385F7DAC85769326248B04A71A5434C14D45AF::get_offset_of_OnSceneLoaded_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5813 = { sizeof (SceneLoadedEvent_t9EED90839B7F10C9CF694B1DBFC750B7E9632E82), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5814 = { sizeof (SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438), -1, sizeof(SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5814[26] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438_StaticFields::get_offset_of_Database_11(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_U3CCurrentAsyncOperationU3Ek__BackingField_12(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_AllowSceneActivation_13(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_DebugMode_14(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_LoadBehavior_15(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_GetSceneBy_16(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_LoadSceneMode_17(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_OnProgressChanged_18(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_OnInverseProgressChanged_19(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_Progressor_20(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_SceneActivationDelay_21(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_SceneBuildIndex_22(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_SceneName_23(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_SelfDestructAfterSceneLoaded_24(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_m_loadInProgress_25(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_m_sceneLoadedAndReady_26(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_m_activatingScene_27(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_m_sceneLoadedAndReadyTime_28(),
	SceneLoader_t63D89F22F7E4B632E2F79D117B762F5D38504438::get_offset_of_m_progress_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5815 = { sizeof (U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5815[7] = 
{
	U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B::get_offset_of_U3CU3E1__state_0(),
	U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B::get_offset_of_U3CU3E2__current_1(),
	U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B::get_offset_of_sceneName_2(),
	U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B::get_offset_of_mode_3(),
	U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B::get_offset_of_U3CU3E4__this_4(),
	U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B::get_offset_of_U3CsceneLoadedAndReadyU3E5__1_5(),
	U3CAsynchronousLoadU3Ed__59_t48E51BB1B1B1C9604E9A7F967A7C690B33D2301B::get_offset_of_U3CactivatingSceneU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5816 = { sizeof (U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5816[7] = 
{
	U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993::get_offset_of_U3CU3E1__state_0(),
	U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993::get_offset_of_U3CU3E2__current_1(),
	U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993::get_offset_of_sceneBuildIndex_2(),
	U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993::get_offset_of_mode_3(),
	U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993::get_offset_of_U3CU3E4__this_4(),
	U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993::get_offset_of_U3CsceneLoadedAndReadyU3E5__1_5(),
	U3CAsynchronousLoadU3Ed__60_t5B726064BF29675FC3B03046A8B4D70FFF140993::get_offset_of_U3CactivatingSceneU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5817 = { sizeof (U3CSelfDestructU3Ed__61_t16E49D474D5F94504CCFB97853EA0E6841DEA028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5817[3] = 
{
	U3CSelfDestructU3Ed__61_t16E49D474D5F94504CCFB97853EA0E6841DEA028::get_offset_of_U3CU3E1__state_0(),
	U3CSelfDestructU3Ed__61_t16E49D474D5F94504CCFB97853EA0E6841DEA028::get_offset_of_U3CU3E2__current_1(),
	U3CSelfDestructU3Ed__61_t16E49D474D5F94504CCFB97853EA0E6841DEA028::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5818 = { sizeof (SceneUnloadedEvent_tF6E196C99EC905E8F0AA9DE9F5246F3448B41471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5819 = { sizeof (ResetValue_t30211A108252757290364E5762AB7B860211DA12)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5819[5] = 
{
	ResetValue_t30211A108252757290364E5762AB7B860211DA12::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5820 = { sizeof (TargetProgress_tDEED5DA2E9A8C4C19A1ACECE025915AA7D35C5C9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5820[3] = 
{
	TargetProgress_tDEED5DA2E9A8C4C19A1ACECE025915AA7D35C5C9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5821 = { sizeof (TargetVariable_tA72CCEF209B5E96B94AC7906D4089D70A57030FF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5821[6] = 
{
	TargetVariable_tA72CCEF209B5E96B94AC7906D4089D70A57030FF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5822 = { sizeof (ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5823 = { sizeof (Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5823[27] = 
{
	0,
	0,
	0,
	0,
	0,
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_DebugMode_9(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_ProgressTargets_10(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_AnimateValue_11(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_AnimationDuration_12(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_AnimationEase_13(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_AnimationIgnoresUnityTimescale_14(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_OnEnableResetValue_15(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_OnDisableResetValue_16(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_CustomResetValue_17(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_OnValueChanged_18(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_OnProgressChanged_19(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_OnInverseProgressChanged_20(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_m_minValue_21(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_m_maxValue_22(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_m_wholeNumbers_23(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_m_currentValue_24(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_m_previousValue_25(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_m_animationSequence_26(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_m_value_27(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_m_progress_28(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_m_inverseProgress_29(),
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A::get_offset_of_m_updatePreviousValue_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5824 = { sizeof (ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5824[8] = 
{
	0,
	ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D::get_offset_of_DebugMode_5(),
	ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D::get_offset_of_Progressors_6(),
	ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D::get_offset_of_OnProgressChanged_7(),
	ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D::get_offset_of_OnInverseProgressChanged_8(),
	ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D::get_offset_of_m_animationSequence_9(),
	ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D::get_offset_of_m_previousProgress_10(),
	ProgressorGroup_tECF735DF73AE7B1A65C59038545569EE8633265D::get_offset_of_m_progress_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5825 = { sizeof (U3CU3Ec_t25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E), -1, sizeof(U3CU3Ec_t25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5825[2] = 
{
	U3CU3Ec_t25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t25837C5648FB8DA1B6000D6E2C1668F8C0DFAE2E_StaticFields::get_offset_of_U3CU3E9__20_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5826 = { sizeof (ProgressTarget_t36E87AFF7AE1F4463726F5A65528252FC9A0AB93), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5827 = { sizeof (ProgressTargetAnimator_t192042EEC2CFB1239EDA6E52A9F776FF0FBA95E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5827[3] = 
{
	ProgressTargetAnimator_t192042EEC2CFB1239EDA6E52A9F776FF0FBA95E8::get_offset_of_Animator_4(),
	ProgressTargetAnimator_t192042EEC2CFB1239EDA6E52A9F776FF0FBA95E8::get_offset_of_ParameterName_5(),
	ProgressTargetAnimator_t192042EEC2CFB1239EDA6E52A9F776FF0FBA95E8::get_offset_of_TargetProgress_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5828 = { sizeof (ProgressTargetImage_tF7B42B85F978B44F956C48E024DE81EF62D35D6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5828[2] = 
{
	ProgressTargetImage_tF7B42B85F978B44F956C48E024DE81EF62D35D6A::get_offset_of_Image_4(),
	ProgressTargetImage_tF7B42B85F978B44F956C48E024DE81EF62D35D6A::get_offset_of_TargetProgress_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5829 = { sizeof (ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5829[8] = 
{
	ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D::get_offset_of_Text_4(),
	ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D::get_offset_of_TargetVariable_5(),
	ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D::get_offset_of_WholeNumbers_6(),
	ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D::get_offset_of_UseMultiplier_7(),
	ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D::get_offset_of_Multiplier_8(),
	ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D::get_offset_of_Prefix_9(),
	ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D::get_offset_of_Suffix_10(),
	ProgressTargetText_t57464BB36CC943114F00DDBAB6AC1F336B04196D::get_offset_of_m_targetValue_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5830 = { sizeof (ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5830[8] = 
{
	ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E::get_offset_of_TextMeshPro_4(),
	ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E::get_offset_of_TargetVariable_5(),
	ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E::get_offset_of_WholeNumbers_6(),
	ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E::get_offset_of_UseMultiplier_7(),
	ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E::get_offset_of_Multiplier_8(),
	ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E::get_offset_of_Prefix_9(),
	ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E::get_offset_of_Suffix_10(),
	ProgressTargetTextMeshPro_t072BA171035E1B4C88C6DDE0FEEF68B3371EEA2E::get_offset_of_m_targetValue_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5831 = { sizeof (DetectedOrientation_tDB199EB6BDD094C3916416E502ABF997746D6ECA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5831[4] = 
{
	DetectedOrientation_tDB199EB6BDD094C3916416E502ABF997746D6ECA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5832 = { sizeof (OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F), -1, sizeof(OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5832[8] = 
{
	OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F_StaticFields::get_offset_of_s_instance_4(),
	OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F_StaticFields::get_offset_of_U3CApplicationIsQuittingU3Ek__BackingField_5(),
	OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F::get_offset_of_DebugMode_6(),
	OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F::get_offset_of_OnOrientationEvent_7(),
	OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F::get_offset_of_m_currentOrientation_8(),
	OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F::get_offset_of_m_rectTransform_9(),
	OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F::get_offset_of_m_canvas_10(),
	OrientationDetector_tD98C708BDAF5E3BA382397F9480244931C56625F::get_offset_of_m_deviceOrientationCheckCount_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5833 = { sizeof (OrientationEvent_tCC6D3CFE9251CC74E8F13085EE96BF9A1B2B267B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5834 = { sizeof (GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8), -1, sizeof(GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5834[9] = 
{
	0,
	0,
	GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8_StaticFields::get_offset_of_Database_6(),
	GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8::get_offset_of_U3CInitializedU3Ek__BackingField_7(),
	GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8::get_offset_of_DebugMode_8(),
	GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8::get_offset_of_ControllerName_9(),
	GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8::get_offset_of_DontDestroyControllerOnLoad_10(),
	GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8::get_offset_of_m_graphModel_11(),
	GraphController_t13B77A3E20F93D6AC924E57A0914A4A1F49EF0E8::get_offset_of_m_graph_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5835 = { sizeof (U3CInitializeGraphEnumeratorU3Ed__32_t0097E6EE24AD048D8871F3B8CB08A92D033FCA46), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5835[3] = 
{
	U3CInitializeGraphEnumeratorU3Ed__32_t0097E6EE24AD048D8871F3B8CB08A92D033FCA46::get_offset_of_U3CU3E1__state_0(),
	U3CInitializeGraphEnumeratorU3Ed__32_t0097E6EE24AD048D8871F3B8CB08A92D033FCA46::get_offset_of_U3CU3E2__current_1(),
	U3CInitializeGraphEnumeratorU3Ed__32_t0097E6EE24AD048D8871F3B8CB08A92D033FCA46::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5836 = { sizeof (NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795), -1, sizeof(NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5836[52] = 
{
	0,
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795_StaticFields::get_offset_of_s_instance_5(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_NormalOpacity_6(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_ActiveOpacity_7(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_HoverOpacity_8(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_FooterHeight_9(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_MaxNodeWidth_10(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_MinNodeWidth_11(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_NodeAccentLineHeight_12(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_NodeAddSocketButtonSize_13(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_NodeBodyOpacity_14(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_NodeDeleteButtonSize_15(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_NodeGlowOpacity_16(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_NodeHeaderHeight_17(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_NodeHeaderIconSize_18(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_PingColorChangeSpeed_19(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_SocketConnectedOpacity_20(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_SocketCurveModifierMaxValue_21(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_SocketCurveModifierMinValue_22(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_SocketDividerHeight_23(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_SocketDividerOpacity_24(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_SocketHeight_25(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_SocketNotConnectedOpacity_26(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_SocketVerticalSpacing_27(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_ConnectionPointHeight_28(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_ConnectionPointOffsetFromLeftMargin_29(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_ConnectionPointOffsetFromRightMargin_30(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_ConnectionPointWidth_31(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_CurvePointsMultiplier_32(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_CurveStrengthModifier_33(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_CurveWidth_34(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_DefaultCurveModifier_35(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_MaxCurveModifier_36(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_MinCurveModifier_37(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_GraphTabDividerWidth_38(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_GraphTabElementSpacing_39(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_GraphTabMaximumWidth_40(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_GraphTabMinimumWidth_41(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_GraphTabsAreaHeight_42(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_GraphTabsBackgroundOpacity_43(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_RepaintIntervalDuringPlayMode_44(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_RepaintIntervalWhileIdle_45(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_EditorPrefsKeyWindowToolbar_46(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_EditorPrefsKeyDotAnimationSpeed_47(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_DefaultNodeHeight_48(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_DefaultNodeWidth_49(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_EnterNodeWidth_50(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_ExitNodeWidth_51(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_StartNodeWidth_52(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_SubGraphNodeWidth_53(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_SwitchBackNodeWidth_54(),
	NodySettings_t1603F6FD41B39178DA2C000D8ACA773F4AAFA795::get_offset_of_DefaultHideFlagsForNodes_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5837 = { sizeof (EnterNode_t9939AA17A69CB75E2DFECC851387FADD605B73EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5838 = { sizeof (ExitNode_tECFEE135BC0FB5750DD87802345C614D28A4F69F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5839 = { sizeof (StartNode_tBE558A1A7130F82DC87D5575BA19B8E42122D756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5840 = { sizeof (SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5840[3] = 
{
	SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B::get_offset_of_m_subGraph_25(),
	SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B::get_offset_of_ErrorNoGraphReferenced_26(),
	SubGraphNode_t29FD18A981D70336B38CE5270357955B79C8409B::get_offset_of_ErrorReferencedGraphIsNotSubGraph_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5841 = { sizeof (SwitchBackNode_tB478F26E72806C34F90C0B802B316881A6C7F0B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5841[3] = 
{
	SwitchBackNode_tB478F26E72806C34F90C0B802B316881A6C7F0B0::get_offset_of_m_targetGraph_25(),
	SwitchBackNode_tB478F26E72806C34F90C0B802B316881A6C7F0B0::get_offset_of_m_returnSourceOutputSocketId_26(),
	SwitchBackNode_tB478F26E72806C34F90C0B802B316881A6C7F0B0::get_offset_of_Sources_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5842 = { sizeof (SourceInfo_tE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5842[5] = 
{
	SourceInfo_tE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2::get_offset_of_SourceName_0(),
	SourceInfo_tE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2::get_offset_of_InputSocketId_1(),
	SourceInfo_tE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2::get_offset_of_OutputSocketId_2(),
	SourceInfo_tE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2::get_offset_of_InputSocketIsConnected_3(),
	SourceInfo_tE42D34CB0B83D7DDAB7969C133F9D1C328E7CCC2::get_offset_of_OutputSocketIsConnected_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5843 = { sizeof (Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5843[8] = 
{
	Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6::get_offset_of_U3CPingU3Ek__BackingField_0(),
	Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6::get_offset_of_m_inputConnectionPoint_1(),
	Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6::get_offset_of_m_outputConnectionPoint_2(),
	Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6::get_offset_of_m_id_3(),
	Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6::get_offset_of_m_inputNodeId_4(),
	Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6::get_offset_of_m_inputSocketId_5(),
	Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6::get_offset_of_m_outputNodeId_6(),
	Connection_t245E995D76298FBABC194CFFAFF0FDFEF8030EA6::get_offset_of_m_outputSocketId_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5844 = { sizeof (ConnectionMode_t02A78013BE4577E4FE874D973349D38F1CD46CB9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5844[3] = 
{
	ConnectionMode_t02A78013BE4577E4FE874D973349D38F1CD46CB9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5845 = { sizeof (NodeType_tDA55D450054F4809E7380A0CFE79C0AE83045BAF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5845[7] = 
{
	NodeType_tDA55D450054F4809E7380A0CFE79C0AE83045BAF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5846 = { sizeof (SocketDirection_tA05CAE38A57F695A9EACFF2522207DEF430E5C42)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5846[3] = 
{
	SocketDirection_tA05CAE38A57F695A9EACFF2522207DEF430E5C42::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5847 = { sizeof (Graph_t26D9C31435F4455B6EF025E506031462302E7AD5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5847[22] = 
{
	0,
	0,
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_U3CActiveSubGraphU3Ek__BackingField_6(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_U3CActiveNodeU3Ek__BackingField_7(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_U3CPreviousActiveNodeU3Ek__BackingField_8(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_DebugMode_9(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_ParentGraph_10(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_ParentSubGraphNode_11(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_activatedNodesHistory_12(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_globalNodes_13(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_enterNode_14(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_exitNode_15(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_startNode_16(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_isDirty_17(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_infiniteLoopTimerStart_18(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_infiniteLoopTimeBreak_19(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_nodes_20(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_isSubGraph_21(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_version_22(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_description_23(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_id_24(),
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5::get_offset_of_m_lastModified_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5848 = { sizeof (Node_t06F79187A31E6958DED19533B1A8783AD1633439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5848[21] = 
{
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_inputSockets_4(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_outputSockets_5(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_nodeType_6(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_allowDuplicateNodeName_7(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_allowEmptyNodeName_8(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_canBeDeleted_9(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_debugMode_10(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_useFixedUpdate_11(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_useLateUpdate_12(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_useUpdate_13(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_height_14(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_width_15(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_x_16(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_y_17(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_minimumInputSocketsCount_18(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_minimumOutputSocketsCount_19(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_graphId_20(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_id_21(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_name_22(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_m_activeGraph_23(),
	Node_t06F79187A31E6958DED19533B1A8783AD1633439::get_offset_of_U3CPingU3Ek__BackingField_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5849 = { sizeof (Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5849[20] = 
{
	0,
	0,
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_connectionMode_2(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_connections_3(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_connectionPoints_4(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_direction_5(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_valueType_6(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_canBeDeleted_7(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_canBeReordered_8(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_curveModifier_9(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_height_10(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_width_11(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_x_12(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_y_13(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_id_14(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_nodeId_15(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_socketName_16(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_value_17(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_valueTypeQualifiedName_18(),
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8::get_offset_of_m_hoverRect_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5850 = { sizeof (PassthroughConnection_tF2D74D39C4C4A2639441B6C52F7C6EF7EB3AB03A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5851 = { sizeof (NodeMenu_t78619524EE5A6B30E1DC06320AB591EC49A3AA52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5851[4] = 
{
	NodeMenu_t78619524EE5A6B30E1DC06320AB591EC49A3AA52::get_offset_of_MenuName_0(),
	NodeMenu_t78619524EE5A6B30E1DC06320AB591EC49A3AA52::get_offset_of_Order_1(),
	NodeMenu_t78619524EE5A6B30E1DC06320AB591EC49A3AA52::get_offset_of_AddSeparatorAfter_2(),
	NodeMenu_t78619524EE5A6B30E1DC06320AB591EC49A3AA52::get_offset_of_AddSeparatorBefore_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5852 = { sizeof (ClassUtils_t379F9EC7BB5DAFF4671B33798096B694B0105537), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5853 = { sizeof (ColorExtensions_t9ECD992F74C8A039DBC62CD2E384DAD3164DF4FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5853[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5854 = { sizeof (RectExtensions_t7FB4B606FB0775D16CA27A6BE2B71AB0CAAAF3AA), -1, sizeof(RectExtensions_t7FB4B606FB0775D16CA27A6BE2B71AB0CAAAF3AA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5854[1] = 
{
	RectExtensions_t7FB4B606FB0775D16CA27A6BE2B71AB0CAAAF3AA_StaticFields::get_offset_of_s_tmpTopLeft_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5855 = { sizeof (U3CU3Ec__DisplayClass37_0_t5A2E68FA6B3433A283A0D75E6763FE3075C72C4D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5855[2] = 
{
	U3CU3Ec__DisplayClass37_0_t5A2E68FA6B3433A283A0D75E6763FE3075C72C4D::get_offset_of_x_0(),
	U3CU3Ec__DisplayClass37_0_t5A2E68FA6B3433A283A0D75E6763FE3075C72C4D::get_offset_of_y_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5856 = { sizeof (U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E), -1, sizeof(U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5856[3] = 
{
	U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E_StaticFields::get_offset_of_U3CU3E9__37_0_1(),
	U3CU3Ec_t8D9C2D68963FDD49864A9E337708F024EF48F38E_StaticFields::get_offset_of_U3CU3E9__37_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5857 = { sizeof (RectTransformExtensions_tD52A31E4FDFCF5BD5727BE8110099C6FD967F811), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5858 = { sizeof (AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5858[7] = 
{
	AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621::get_offset_of_Animator_0(),
	AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621::get_offset_of_BoolValue_1(),
	AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621::get_offset_of_FloatValue_2(),
	AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621::get_offset_of_IntValue_3(),
	AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621::get_offset_of_ParameterName_4(),
	AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621::get_offset_of_ResetTrigger_5(),
	AnimatorEvent_t0342FEEFA357FBFBA4099D8D6B09DB667F448621::get_offset_of_TargetParameterType_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5859 = { sizeof (ParameterType_t8D9374EA942C50D46303896C40AB7D37917E74CF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5859[5] = 
{
	ParameterType_t8D9374EA942C50D46303896C40AB7D37917E74CF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5860 = { sizeof (BoolEvent_tC2164AC1844ACBA9870E7D339F6FD57462A40D3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5861 = { sizeof (GameObjectEvent_tAD0B88F693C994943924525B6521DE200B874CCE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5862 = { sizeof (StringEvent_t03BF969408548F9F12B69569AAED599BE5BC9FB6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5863 = { sizeof (MinMaxRangeAttribute_t0403C3BF156C9F9122007D03EE93B8F74FD1D025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5863[2] = 
{
	MinMaxRangeAttribute_t0403C3BF156C9F9122007D03EE93B8F74FD1D025::get_offset_of_U3CMinU3Ek__BackingField_0(),
	MinMaxRangeAttribute_t0403C3BF156C9F9122007D03EE93B8F74FD1D025::get_offset_of_U3CMaxU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5864 = { sizeof (DOTweenModuleAudio_t769A1BF9E3697BFACC9815179E7A39085E56CE4D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5865 = { sizeof (U3CU3Ec__DisplayClass0_0_t72C207EC4FC022EB09F6F8D33004BEC6FBC66E6F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5865[1] = 
{
	U3CU3Ec__DisplayClass0_0_t72C207EC4FC022EB09F6F8D33004BEC6FBC66E6F::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5866 = { sizeof (U3CU3Ec__DisplayClass1_0_t4A743D54E80E5072E25F38A09BE27414B88BC2DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5866[1] = 
{
	U3CU3Ec__DisplayClass1_0_t4A743D54E80E5072E25F38A09BE27414B88BC2DD::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5867 = { sizeof (U3CU3Ec__DisplayClass2_0_t9C60BFCCE659FB7698FF542E41008F990B147ACC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5867[2] = 
{
	U3CU3Ec__DisplayClass2_0_t9C60BFCCE659FB7698FF542E41008F990B147ACC::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass2_0_t9C60BFCCE659FB7698FF542E41008F990B147ACC::get_offset_of_floatName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5868 = { sizeof (DOTweenModulePhysics_t54AF484E9A4CEC236EE03303DDE8634FC383938E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5869 = { sizeof (U3CU3Ec__DisplayClass0_0_t5C64C7513BD29248DFA2579BC02E0EB936F7BFE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5869[1] = 
{
	U3CU3Ec__DisplayClass0_0_t5C64C7513BD29248DFA2579BC02E0EB936F7BFE3::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5870 = { sizeof (U3CU3Ec__DisplayClass1_0_tA56D94394389743477D8477AC85120BFC5536CC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5870[1] = 
{
	U3CU3Ec__DisplayClass1_0_tA56D94394389743477D8477AC85120BFC5536CC3::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5871 = { sizeof (U3CU3Ec__DisplayClass2_0_t0C665D49874F06B1EA3D9C3145443B48B75733FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5871[1] = 
{
	U3CU3Ec__DisplayClass2_0_t0C665D49874F06B1EA3D9C3145443B48B75733FF::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5872 = { sizeof (U3CU3Ec__DisplayClass3_0_tFD9A3293995CC5CFCD9996BB2B9DBECE1F2853EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5872[1] = 
{
	U3CU3Ec__DisplayClass3_0_tFD9A3293995CC5CFCD9996BB2B9DBECE1F2853EB::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5873 = { sizeof (U3CU3Ec__DisplayClass4_0_t95C132B298C8FED300DCB1A2CF98CCC5CFBA9AE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5873[1] = 
{
	U3CU3Ec__DisplayClass4_0_t95C132B298C8FED300DCB1A2CF98CCC5CFBA9AE3::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5874 = { sizeof (U3CU3Ec__DisplayClass5_0_t41A0920837839D6F1EBED888FCA9A88B12AC0F18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5874[1] = 
{
	U3CU3Ec__DisplayClass5_0_t41A0920837839D6F1EBED888FCA9A88B12AC0F18::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5875 = { sizeof (U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5875[7] = 
{
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_startPosY_1(),
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_offsetYSet_2(),
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_offsetY_3(),
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_s_4(),
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_endValue_5(),
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_yTween_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5876 = { sizeof (U3CU3Ec__DisplayClass7_0_t08A1ECA62C440E40725FC57E64C7EB336CECCC84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5876[1] = 
{
	U3CU3Ec__DisplayClass7_0_t08A1ECA62C440E40725FC57E64C7EB336CECCC84::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5877 = { sizeof (U3CU3Ec__DisplayClass8_0_tB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5877[2] = 
{
	U3CU3Ec__DisplayClass8_0_tB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1::get_offset_of_trans_0(),
	U3CU3Ec__DisplayClass8_0_tB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5878 = { sizeof (U3CU3Ec__DisplayClass9_0_t96637B7BE8971D6AE57F08CD85C9BFF8E11C2B6F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5878[1] = 
{
	U3CU3Ec__DisplayClass9_0_t96637B7BE8971D6AE57F08CD85C9BFF8E11C2B6F::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5879 = { sizeof (U3CU3Ec__DisplayClass10_0_t41907D10A4F8492809D26E4C4B12EEF791CD791F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5879[2] = 
{
	U3CU3Ec__DisplayClass10_0_t41907D10A4F8492809D26E4C4B12EEF791CD791F::get_offset_of_trans_0(),
	U3CU3Ec__DisplayClass10_0_t41907D10A4F8492809D26E4C4B12EEF791CD791F::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5880 = { sizeof (DOTweenModulePhysics2D_t31D337CBA76C3DFD0CF747008022FBBAB46343BF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5881 = { sizeof (U3CU3Ec__DisplayClass0_0_tAE598412087F01228B45541B5C42672EA61EC671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5881[1] = 
{
	U3CU3Ec__DisplayClass0_0_tAE598412087F01228B45541B5C42672EA61EC671::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5882 = { sizeof (U3CU3Ec__DisplayClass1_0_t4A0577007A5B036CAFBBD362531FDA652965EF09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5882[1] = 
{
	U3CU3Ec__DisplayClass1_0_t4A0577007A5B036CAFBBD362531FDA652965EF09::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5883 = { sizeof (U3CU3Ec__DisplayClass2_0_t2674AB8293A4F3119319017A1FFF86A555F234A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5883[1] = 
{
	U3CU3Ec__DisplayClass2_0_t2674AB8293A4F3119319017A1FFF86A555F234A7::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5884 = { sizeof (U3CU3Ec__DisplayClass3_0_t6012E00DCCD7C59473E11E70423E98FFAEAB126F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5884[1] = 
{
	U3CU3Ec__DisplayClass3_0_t6012E00DCCD7C59473E11E70423E98FFAEAB126F::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5885 = { sizeof (U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5885[7] = 
{
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_startPosY_1(),
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_offsetYSet_2(),
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_offsetY_3(),
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_s_4(),
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_endValue_5(),
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_yTween_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5886 = { sizeof (DOTweenModuleSprite_tA07D646AA9234481CF1BA55D046BC7F824A818F2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5887 = { sizeof (U3CU3Ec__DisplayClass0_0_tF979E38F1CE3544D9E454AE0AF7ECB621C359998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5887[1] = 
{
	U3CU3Ec__DisplayClass0_0_tF979E38F1CE3544D9E454AE0AF7ECB621C359998::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5888 = { sizeof (U3CU3Ec__DisplayClass1_0_t3C1F8A4FCB252FA03523157E817DEDAD87EBA1C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5888[1] = 
{
	U3CU3Ec__DisplayClass1_0_t3C1F8A4FCB252FA03523157E817DEDAD87EBA1C7::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5889 = { sizeof (U3CU3Ec__DisplayClass3_0_t23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5889[2] = 
{
	U3CU3Ec__DisplayClass3_0_t23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass3_0_t23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5890 = { sizeof (DOTweenModuleUI_t39E58130742D7CFD84DE6B0EA1DC7ED3C0A839D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5891 = { sizeof (Utils_tF7D730835163762D9317B6FB65E30C704BD3BF7F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5892 = { sizeof (U3CU3Ec__DisplayClass0_0_t1CD861E6D228C2C19A057642CB25BC00977708E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5892[1] = 
{
	U3CU3Ec__DisplayClass0_0_t1CD861E6D228C2C19A057642CB25BC00977708E7::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5893 = { sizeof (U3CU3Ec__DisplayClass1_0_t551B53D6928422D334300FB5FE36087E17A7199A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5893[1] = 
{
	U3CU3Ec__DisplayClass1_0_t551B53D6928422D334300FB5FE36087E17A7199A::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5894 = { sizeof (U3CU3Ec__DisplayClass2_0_tE5F3CF3169FC68264B35D20D81204FFF0F52D2CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5894[1] = 
{
	U3CU3Ec__DisplayClass2_0_tE5F3CF3169FC68264B35D20D81204FFF0F52D2CF::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5895 = { sizeof (U3CU3Ec__DisplayClass3_0_t8723E84900FA09817B34237CBECC97C7A463F288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5895[1] = 
{
	U3CU3Ec__DisplayClass3_0_t8723E84900FA09817B34237CBECC97C7A463F288::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5896 = { sizeof (U3CU3Ec__DisplayClass4_0_t3D7A8AD757769FD8E3E12FE825921D885FBAC98E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5896[1] = 
{
	U3CU3Ec__DisplayClass4_0_t3D7A8AD757769FD8E3E12FE825921D885FBAC98E::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5897 = { sizeof (U3CU3Ec__DisplayClass5_0_t89CE27455F60C162B98656F385CDF2F8C83B3813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5897[1] = 
{
	U3CU3Ec__DisplayClass5_0_t89CE27455F60C162B98656F385CDF2F8C83B3813::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5898 = { sizeof (U3CU3Ec__DisplayClass7_0_t1997C2BD49954A2A3D92FD767A822B8F1B4B95DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5898[1] = 
{
	U3CU3Ec__DisplayClass7_0_t1997C2BD49954A2A3D92FD767A822B8F1B4B95DB::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5899 = { sizeof (U3CU3Ec__DisplayClass8_0_t4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5899[1] = 
{
	U3CU3Ec__DisplayClass8_0_t4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05::get_offset_of_target_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
