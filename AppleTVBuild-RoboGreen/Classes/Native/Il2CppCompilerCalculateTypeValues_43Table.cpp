﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Rewired.ActionElementMap
struct ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7;
// Rewired.ControllerElementIdentifier
struct ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66;
// Rewired.ControllerTemplateElementIdentifier
struct ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40;
// Rewired.Data.ConfigVars
struct ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41;
// Rewired.Data.ControllerTemplateElementIdentifier_Editor[]
struct ControllerTemplateElementIdentifier_EditorU5BU5D_t87B20AA01A6CCFBAE5D3E8F8955144061B683CDA;
// Rewired.Data.EditorPlatformData/Platform
struct Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA;
// Rewired.Data.Mapping.ActionCategoryMap
struct ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F;
// Rewired.Data.Mapping.ControllerMap_Editor
struct ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7;
// Rewired.Data.Mapping.CustomCalculation
struct CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038;
// Rewired.Data.Mapping.HardwareAxisInfo
struct HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4;
// Rewired.Data.Mapping.HardwareButtonInfo
struct HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273;
// Rewired.Data.Mapping.HardwareJoystickMap
struct HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB;
// Rewired.Data.Mapping.HardwareJoystickMap/AxisCalibrationInfoEntry[]
struct AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7;
// Rewired.Data.Mapping.HardwareJoystickMap/MatchingCriteria_Base/ElementCount_Base[]
struct ElementCount_BaseU5BU5D_tCC1A9A7741F132924D329C62382FF47C506B516C;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Custom/Axis
struct Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Custom/Axis[]
struct AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Custom/Button
struct Button_tBB6149DD7A45B873206F15797DC2B002019BF09C;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Custom/Button[]
struct ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Custom/CustomCalculationSourceData[]
struct CustomCalculationSourceDataU5BU5D_t9CB27E1BF97E6EEF9BF27CECA4DC1B02B002F8D0;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_InternalDriver_Base
struct Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_InternalDriver_Base/Axis[]
struct AxisU5BU5D_tD13716E57490B18A6B388AAF5CDA0B27255CDD92;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_InternalDriver_Base/Button[]
struct ButtonU5BU5D_t74601BECE712AA3EDBBCBFF2B29C73C01CA30C6A;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_InternalDriver_Base/Elements
struct Elements_t656A4A2A4154F0048E692B5D67D8596354228F5B;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_InternalDriver_Base/MatchingCriteria
struct MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_InternalDriver_Base[]
struct Platform_InternalDriver_BaseU5BU5D_tC41BF5CE8D7EAC464A23255CC1CF6CDBB9E16A89;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_NintendoSwitch_Base
struct Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_NintendoSwitch_Base/Elements
struct Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_NintendoSwitch_Base/MatchingCriteria
struct MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_NintendoSwitch_Base[]
struct Platform_NintendoSwitch_BaseU5BU5D_t5A831640E428C590033C4E197F51DFE58722CF87;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_SDL2_Base
struct Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_SDL2_Base/Axis
struct Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_SDL2_Base/Axis[]
struct AxisU5BU5D_t2BB57D29B9968F1A2F0BD26315755F640062E809;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_SDL2_Base/Button
struct Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_SDL2_Base/Button[]
struct ButtonU5BU5D_tAC2CF36EC9CD62CE6C8B6706ECFE77173EF2AA04;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_SDL2_Base/Elements
struct Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_SDL2_Base/MatchingCriteria
struct MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_SDL2_Base[]
struct Platform_SDL2_BaseU5BU5D_t0D50401C9DEFF2022C857D02FC2A2A01E67CE601;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Steam_Base/Elements
struct Elements_tF245BB8E30C6E928077EEBF35DD28D1B5EC292A7;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Steam_Base/MatchingCriteria
struct MatchingCriteria_t722FD8AEF7B412C35CE3507CFCBDF57E5BEE4062;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Steam_Base[]
struct Platform_Steam_BaseU5BU5D_t71D5D330D85FA219CFBA26A2C1638E139BBBA543;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WebGL_Base
struct Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WebGL_Base/Axis[]
struct AxisU5BU5D_tE5CCB8C13EC5AF3F10051527E4A58A369A12BC2E;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WebGL_Base/Button[]
struct ButtonU5BU5D_t8418C265860277AB2757F7094850543B630021C7;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WebGL_Base/Elements
struct Elements_tB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WebGL_Base/MatchingCriteria
struct MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WebGL_Base/MatchingCriteria/ClientInfo[]
struct ClientInfoU5BU5D_tCBF411919F035B6414E4785B2500E345B9C82CB0;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WebGL_Base[]
struct Platform_WebGL_BaseU5BU5D_t9AE6837A8724775CAEDAC80A8DFB1BD543CAD1F3;
// Rewired.Data.Mapping.HardwareJoystickMap/VidPid[]
struct VidPidU5BU5D_t6020E7B4C895694E39E0A81C72C285189098A971;
// Rewired.Data.Mapping.HardwareJoystickTemplateMap
struct HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74;
// Rewired.Data.Mapping.HardwareJoystickTemplateMap/SpecialElementEntry[]
struct SpecialElementEntryU5BU5D_tB783CEBBEC38B526E421CCB50CF07ACD27235CEB;
// Rewired.Data.Player_Editor/ControllerMapEnablerSettings
struct ControllerMapEnablerSettings_t34E49435CD1543BBA8B8420276849C0AFE4E02F1;
// Rewired.Data.Player_Editor/ControllerMapLayoutManagerSettings
struct ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98;
// Rewired.Data.Player_Editor/CreateControllerInfo
struct CreateControllerInfo_t1174758D9532AC61484D41E38E94D3C7CF852CC4;
// Rewired.Data.Player_Editor/Mapping
struct Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26;
// Rewired.Data.UserData
struct UserData_t23CF13BC0883AADA4A3457583033292E49A77D48;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.Data.ControllerMapEnabler_RuleSet_Editor>
struct DdxYPCrLAHFMStEcIPuCjbnFlwc_tEFED26C63F0010C91EBDCEC819B13B77A015CA22;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor>
struct DdxYPCrLAHFMStEcIPuCjbnFlwc_t37E43D3555409AECD07AC85061A407F9A6C1EA3A;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.Data.Mapping.ControllerMap_Editor>
struct DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.Data.Player_Editor>
struct DdxYPCrLAHFMStEcIPuCjbnFlwc_tFA9792A0CF5145B0C7873E7868C79A0586B9A796;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.InputAction>
struct DdxYPCrLAHFMStEcIPuCjbnFlwc_t9FCA482A374CAF40C516A9E02F48B4072BD4D00B;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/bkEsZMWLqQWRgIhnNcnweEhXFrJI
struct bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/bkEsZMWLqQWRgIhnNcnweEhXFrJI/CduakqgeNqOahwGMecbydkSyXGVi
struct CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/fBIlKDTPhFeObzPVPLNsWIWwMRV
struct fBIlKDTPhFeObzPVPLNsWIWwMRV_t84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/jDowvQWVkIomHXVEUMGnVlhpeXz
struct jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/jDowvQWVkIomHXVEUMGnVlhpeXz/VYWeAqBBKwjbwzwIKICDmtpATxj
struct VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/qEcNdMVgmNokdCmhdodTTEqmHlz
struct qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/qEcNdMVgmNokdCmhdodTTEqmHlz/FJLrnfWkvQiShLGCYOOXtXsMLrS
struct FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/qEcNdMVgmNokdCmhdodTTEqmHlz/NtFyIiXIvuJrPzjtoNMcQcjFQhu
struct NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F;
// Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/qEcNdMVgmNokdCmhdodTTEqmHlz/sifvSqThLQGuVclMuQKUhwYRlPoN
struct sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3;
// Rewired.Interfaces.IControllerElementIdentifierCommon_Internal
struct IControllerElementIdentifierCommon_Internal_t93DB413F3C179009FCDD22BB0C3EF143D0517557;
// System.Action`2<System.Collections.Generic.List`1<Rewired.Data.Player_Editor/Mapping>,System.Int32>
struct Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IList`1<Rewired.Data.ControllerMapEnabler_RuleSet_Editor>
struct IList_1_t46C24F8D784DCA2A18025F34522FCC283FB3740D;
// System.Collections.Generic.IList`1<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor>
struct IList_1_tEE9813B3CAD449A22BD3403F651329395F4AF6B2;
// System.Collections.Generic.IList`1<Rewired.Data.Mapping.ControllerMap_Editor>
struct IList_1_t0F62C70B1E355013B780D862CE6D7E829538DD2D;
// System.Collections.Generic.IList`1<Rewired.Data.Player_Editor>
struct IList_1_t5218A5C0654351AD5290FF47AAE203126799833E;
// System.Collections.Generic.IList`1<Rewired.InputAction>
struct IList_1_tB9BABEEA7BBC8942C95B7BE18322CE83CA619DC2;
// System.Collections.Generic.IList`1<Rewired.InputBehavior>
struct IList_1_tE69D8FDDF240FFBEE8DD59F7381C2DFA26A34F5C;
// System.Collections.Generic.IList`1<Rewired.InputCategory>
struct IList_1_t5DDB1FF508861E6C5B788FA02A0E5A4E8027A5A4;
// System.Collections.Generic.IList`1<Rewired.InputLayout>
struct IList_1_t1CC67189F3705C938901A0CAB2BFEA25E3D8F8FF;
// System.Collections.Generic.IList`1<Rewired.InputMapCategory>
struct IList_1_t5B60DF2CB96AC22521115041FCC0199546CBF120;
// System.Collections.Generic.List`1<Rewired.Data.ControllerMapEnabler_RuleSet_Editor>
struct List_1_tB43994C449405FA051845A34A47EB5A1FD88FC1D;
// System.Collections.Generic.List`1<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor>
struct List_1_t458243618BB2E4CE738A13EB434D2D7F4D7B0C8B;
// System.Collections.Generic.List`1<Rewired.Data.CustomController_Editor>
struct List_1_t54DD5AA01FFBBAAF392103CFEC81ED0E4765E320;
// System.Collections.Generic.List`1<Rewired.Data.Mapping.ControllerMap_Editor>
struct List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C;
// System.Collections.Generic.List`1<Rewired.Data.Mapping.HardwareJoystickTemplateMap/ElementIdentifierMap>
struct List_1_t5A6709ED126B183372B44781780EBCFAF4B40B12;
// System.Collections.Generic.List`1<Rewired.Data.Mapping.HardwareJoystickTemplateMap/Entry>
struct List_1_t0A4167A45AFA6D61B1522ADC5208FFC30594F6DF;
// System.Collections.Generic.List`1<Rewired.Data.Player_Editor/CreateControllerInfo>
struct List_1_tB69BD32C8767F97DE719C7E69179AFBEE7636A2E;
// System.Collections.Generic.List`1<Rewired.Data.Player_Editor/Mapping>
struct List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D;
// System.Collections.Generic.List`1<Rewired.Data.Player_Editor/RuleSetMapping>
struct List_1_tCF7B09047A3AD51F8785C6EB6C912FCD6497810C;
// System.Collections.Generic.List`1<Rewired.Data.Player_Editor>
struct List_1_t5A03789D060E8ADDB4C9E0E05DC5BFA7E2530390;
// System.Collections.Generic.List`1<Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/qYkefkBebypsciWTBlnXilfwNlYB>
struct List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997;
// System.Collections.Generic.List`1<Rewired.InputAction>
struct List_1_tED09A8A751202103D07AF4B1286EA07FBF9CF702;
// System.Collections.Generic.List`1<Rewired.InputBehavior>
struct List_1_t00493746A9C09D2A4E213DD073FD95F9A4FAA144;
// System.Collections.Generic.List`1<Rewired.InputCategory>
struct List_1_tF4A678B2CE5927472957C1C908BCCAE515A36D8E;
// System.Collections.Generic.List`1<Rewired.InputLayout>
struct List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2;
// System.Collections.Generic.List`1<Rewired.InputMapCategory>
struct List_1_tF6EC72522F33236C9C1694F690FF7AD3EBEB8FB9;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<UnityEngine.TextAsset>
struct List_1_t46F5218A6E83BACB1E173DD59FD4C3895FD044C5;
// System.Func`2<Rewired.ControllerType,System.Collections.Generic.List`1<Rewired.Data.UserData/fdlAxojmklCKPkEqkHYmFTOHUCz/qYkefkBebypsciWTBlnXilfwNlYB>>
struct Func_2_tEE11FFC8EA9AA3ABD47DD780F6118C2F7C71F883;
// System.Func`2<Rewired.Data.ControllerMapEnabler_RuleSet_Editor,System.Int32>
struct Func_2_t0BD8682532A5DDF677C14A98D91388945D78C1E3;
// System.Func`2<Rewired.Data.ControllerMapEnabler_RuleSet_Editor,System.String>
struct Func_2_t66E0B7465F8F43E9C65DE7EE121532C322F9BA49;
// System.Func`2<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor,System.Int32>
struct Func_2_t06A6BFD16BD610B44F74E00E9E5E29831CDC8501;
// System.Func`2<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor,System.String>
struct Func_2_t36944FB9B92A66876D44BDB1FB6B96161E8C3AC4;
// System.Func`2<Rewired.Data.CustomController_Editor,System.Int32>
struct Func_2_tE26EB9A7D57FA6EFFF354471D51E059241C47BC2;
// System.Func`2<Rewired.Data.CustomController_Editor,System.String>
struct Func_2_tF71711714BA304BBA684F98ED721844B75360E14;
// System.Func`2<Rewired.Data.Mapping.ControllerMap_Editor,System.Int32>
struct Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31;
// System.Func`2<Rewired.Data.Mapping.ControllerMap_Editor,System.String>
struct Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405;
// System.Func`2<Rewired.Data.Player_Editor,System.Int32>
struct Func_2_t865F0E70E6F8AAE22876AE6F68EBA08629B7DFA5;
// System.Func`2<Rewired.Data.Player_Editor,System.String>
struct Func_2_tC14D01FAD79942C4E6779CDA169131C264BA046D;
// System.Func`2<Rewired.InputAction,System.Int32>
struct Func_2_t55F6CD5F3D43065DAF930674859E54C38CD29BF1;
// System.Func`2<Rewired.InputAction,System.String>
struct Func_2_t399B7E27C786D4E284EB485BD673C9660B0232BC;
// System.Func`2<Rewired.InputBehavior,System.Int32>
struct Func_2_tA22B529B781FA354C275B9212340CBEA87BA9704;
// System.Func`2<Rewired.InputBehavior,System.String>
struct Func_2_t7E8255AF8E4F6CBB697B0E1ABC106F943B2F57F6;
// System.Func`2<Rewired.InputCategory,System.Int32>
struct Func_2_tDFADAE8DD881DBEA38A028A4E5542ADD97170619;
// System.Func`2<Rewired.InputCategory,System.String>
struct Func_2_tEA5DF77336278B340269FE419B5C91926472DE8B;
// System.Func`2<Rewired.InputLayout,System.Int32>
struct Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2;
// System.Func`2<Rewired.InputLayout,System.String>
struct Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69;
// System.Func`2<Rewired.InputMapCategory,System.Int32>
struct Func_2_tECFA09A75B179FF9DFC326F62FA06569D68300CD;
// System.Func`2<Rewired.InputMapCategory,System.String>
struct Func_2_t53CF4F43E1D63F334C7CE91B4C7A0D780A24FC93;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618;
// System.Func`3<Rewired.ActionElementMap,System.Collections.Generic.IList`1<Rewired.ActionElementMap>,System.Int32>
struct Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613;
// System.Func`3<Rewired.Data.ControllerMapEnabler_RuleSet_Editor,System.Collections.Generic.IList`1<Rewired.Data.ControllerMapEnabler_RuleSet_Editor>,System.Int32>
struct Func_3_t38123FDEE2EF591B7CB6BC01B9943DF19D56B825;
// System.Func`3<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor,System.Collections.Generic.IList`1<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor>,System.Int32>
struct Func_3_tCE00B2B3F8B70F9AAA4B29BC0F08E4FCD0C37B34;
// System.Func`3<Rewired.Data.CustomController_Editor,System.Collections.Generic.IList`1<Rewired.Data.CustomController_Editor>,System.Int32>
struct Func_3_t6E3FD79D70ABB176EF74FACE38BDD967AD80523B;
// System.Func`3<Rewired.Data.Player_Editor,System.Collections.Generic.IList`1<Rewired.Data.Player_Editor>,System.Int32>
struct Func_3_t02F9564D90C91BD1B71AA6C7BCE8F19B9D6114E8;
// System.Func`3<Rewired.Data.Player_Editor/CreateControllerInfo,System.Collections.Generic.IList`1<Rewired.Data.Player_Editor/CreateControllerInfo>,System.Int32>
struct Func_3_tFDE88F7817E43EC72692E815C47511E6199FF9B9;
// System.Func`3<Rewired.Data.Player_Editor/Mapping,System.Collections.Generic.IList`1<Rewired.Data.Player_Editor/Mapping>,System.Int32>
struct Func_3_t90F2989D2602006A5B0F44BB59F5358F8BE452CF;
// System.Func`3<Rewired.InputAction,System.Collections.Generic.IList`1<Rewired.InputAction>,System.Int32>
struct Func_3_t8D5A1432AA54EBBB1CDCE4B14E784E045C8B925C;
// System.Func`3<Rewired.InputBehavior,System.Collections.Generic.IList`1<Rewired.InputBehavior>,System.Int32>
struct Func_3_tAD715980CBF49C148A305ED67A19769B424B7A8D;
// System.Func`3<Rewired.InputCategory,System.Collections.Generic.IList`1<Rewired.InputCategory>,System.Int32>
struct Func_3_t6617EB9E51F7A0A6716CDA949E95165FAC2BA4E2;
// System.Func`3<Rewired.InputLayout,System.Collections.Generic.IList`1<Rewired.InputLayout>,System.Int32>
struct Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0;
// System.Func`3<Rewired.InputMapCategory,System.Collections.Generic.IList`1<Rewired.InputMapCategory>,System.Int32>
struct Func_3_t44BF8A5EBD1E9FB6D8359B14CF212C650D574F7F;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.TextAsset[]
struct TextAssetU5BU5D_t9FC0EEC53F9A98843DD1CA63B9CEF514CD664AD6;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef PLATFORM_TAF8E68882BCD8C635053FA486B819B0235E9CFCA_H
#define PLATFORM_TAF8E68882BCD8C635053FA486B819B0235E9CFCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.EditorPlatformData_Platform
struct  Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA  : public RuntimeObject
{
public:
	// UnityEngine.TextAsset[] Rewired.Data.EditorPlatformData_Platform::libraries
	TextAssetU5BU5D_t9FC0EEC53F9A98843DD1CA63B9CEF514CD664AD6* ___libraries_0;

public:
	inline static int32_t get_offset_of_libraries_0() { return static_cast<int32_t>(offsetof(Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA, ___libraries_0)); }
	inline TextAssetU5BU5D_t9FC0EEC53F9A98843DD1CA63B9CEF514CD664AD6* get_libraries_0() const { return ___libraries_0; }
	inline TextAssetU5BU5D_t9FC0EEC53F9A98843DD1CA63B9CEF514CD664AD6** get_address_of_libraries_0() { return &___libraries_0; }
	inline void set_libraries_0(TextAssetU5BU5D_t9FC0EEC53F9A98843DD1CA63B9CEF514CD664AD6* value)
	{
		___libraries_0 = value;
		Il2CppCodeGenWriteBarrier((&___libraries_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_TAF8E68882BCD8C635053FA486B819B0235E9CFCA_H
#ifndef HARDWAREBUTTONINFO_T9C07E8DF615040EE25315B98F1D28E5D08224273_H
#define HARDWAREBUTTONINFO_T9C07E8DF615040EE25315B98F1D28E5D08224273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareButtonInfo
struct  HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareButtonInfo::_excludeFromPolling
	bool ____excludeFromPolling_0;
	// System.Boolean Rewired.Data.Mapping.HardwareButtonInfo::_isPressureSensitive
	bool ____isPressureSensitive_1;

public:
	inline static int32_t get_offset_of__excludeFromPolling_0() { return static_cast<int32_t>(offsetof(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273, ____excludeFromPolling_0)); }
	inline bool get__excludeFromPolling_0() const { return ____excludeFromPolling_0; }
	inline bool* get_address_of__excludeFromPolling_0() { return &____excludeFromPolling_0; }
	inline void set__excludeFromPolling_0(bool value)
	{
		____excludeFromPolling_0 = value;
	}

	inline static int32_t get_offset_of__isPressureSensitive_1() { return static_cast<int32_t>(offsetof(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273, ____isPressureSensitive_1)); }
	inline bool get__isPressureSensitive_1() const { return ____isPressureSensitive_1; }
	inline bool* get_address_of__isPressureSensitive_1() { return &____isPressureSensitive_1; }
	inline void set__isPressureSensitive_1(bool value)
	{
		____isPressureSensitive_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWAREBUTTONINFO_T9C07E8DF615040EE25315B98F1D28E5D08224273_H
#ifndef ELEMENTS_BASE_T646A0ACACC7BEB9CE31C70CB74E59EF0312756F0_H
#define ELEMENTS_BASE_T646A0ACACC7BEB9CE31C70CB74E59EF0312756F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Elements_Base
struct  Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_BASE_T646A0ACACC7BEB9CE31C70CB74E59EF0312756F0_H
#ifndef MATCHINGCRITERIA_BASE_T9475B14364CDF4EB6C2585C9F29E9394954F3065_H
#define MATCHINGCRITERIA_BASE_T9475B14364CDF4EB6C2585C9F29E9394954F3065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base
struct  MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base::axisCount
	int32_t ___axisCount_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base::buttonCount
	int32_t ___buttonCount_1;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base::disabled
	bool ___disabled_2;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base::tag
	String_t* ___tag_3;

public:
	inline static int32_t get_offset_of_axisCount_0() { return static_cast<int32_t>(offsetof(MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065, ___axisCount_0)); }
	inline int32_t get_axisCount_0() const { return ___axisCount_0; }
	inline int32_t* get_address_of_axisCount_0() { return &___axisCount_0; }
	inline void set_axisCount_0(int32_t value)
	{
		___axisCount_0 = value;
	}

	inline static int32_t get_offset_of_buttonCount_1() { return static_cast<int32_t>(offsetof(MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065, ___buttonCount_1)); }
	inline int32_t get_buttonCount_1() const { return ___buttonCount_1; }
	inline int32_t* get_address_of_buttonCount_1() { return &___buttonCount_1; }
	inline void set_buttonCount_1(int32_t value)
	{
		___buttonCount_1 = value;
	}

	inline static int32_t get_offset_of_disabled_2() { return static_cast<int32_t>(offsetof(MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065, ___disabled_2)); }
	inline bool get_disabled_2() const { return ___disabled_2; }
	inline bool* get_address_of_disabled_2() { return &___disabled_2; }
	inline void set_disabled_2(bool value)
	{
		___disabled_2 = value;
	}

	inline static int32_t get_offset_of_tag_3() { return static_cast<int32_t>(offsetof(MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065, ___tag_3)); }
	inline String_t* get_tag_3() const { return ___tag_3; }
	inline String_t** get_address_of_tag_3() { return &___tag_3; }
	inline void set_tag_3(String_t* value)
	{
		___tag_3 = value;
		Il2CppCodeGenWriteBarrier((&___tag_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_BASE_T9475B14364CDF4EB6C2585C9F29E9394954F3065_H
#ifndef ELEMENTCOUNT_BASE_T73ADB57E490B282EC131BA5EAF3AFABDD618ECBC_H
#define ELEMENTCOUNT_BASE_T73ADB57E490B282EC131BA5EAF3AFABDD618ECBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base_ElementCount_Base
struct  ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base_ElementCount_Base::axisCount
	int32_t ___axisCount_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base_ElementCount_Base::buttonCount
	int32_t ___buttonCount_1;

public:
	inline static int32_t get_offset_of_axisCount_0() { return static_cast<int32_t>(offsetof(ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC, ___axisCount_0)); }
	inline int32_t get_axisCount_0() const { return ___axisCount_0; }
	inline int32_t* get_address_of_axisCount_0() { return &___axisCount_0; }
	inline void set_axisCount_0(int32_t value)
	{
		___axisCount_0 = value;
	}

	inline static int32_t get_offset_of_buttonCount_1() { return static_cast<int32_t>(offsetof(ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC, ___buttonCount_1)); }
	inline int32_t get_buttonCount_1() const { return ___buttonCount_1; }
	inline int32_t* get_address_of_buttonCount_1() { return &___buttonCount_1; }
	inline void set_buttonCount_1(int32_t value)
	{
		___buttonCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTCOUNT_BASE_T73ADB57E490B282EC131BA5EAF3AFABDD618ECBC_H
#ifndef PLATFORM_T0E28F5AD2665BAE68E81D3FF477CA1E6E1574789_H
#define PLATFORM_T0E28F5AD2665BAE68E81D3FF477CA1E6E1574789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform
struct  Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789  : public RuntimeObject
{
public:
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_Platform::description
	String_t* ___description_0;

public:
	inline static int32_t get_offset_of_description_0() { return static_cast<int32_t>(offsetof(Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789, ___description_0)); }
	inline String_t* get_description_0() const { return ___description_0; }
	inline String_t** get_address_of_description_0() { return &___description_0; }
	inline void set_description_0(String_t* value)
	{
		___description_0 = value;
		Il2CppCodeGenWriteBarrier((&___description_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T0E28F5AD2665BAE68E81D3FF477CA1E6E1574789_H
#ifndef ELEMENT_T917DF3B3892BA48FD5AE1595F8C288DC67D972BF_H
#define ELEMENT_T917DF3B3892BA48FD5AE1595F8C288DC67D972BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element
struct  Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::elementIdentifier
	int32_t ___elementIdentifier_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::sourceType
	int32_t ___sourceType_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::sourceAxis
	int32_t ___sourceAxis_2;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::axisDeadZone
	float ___axisDeadZone_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::sourceButton
	int32_t ___sourceButton_4;
	// Rewired.Data.Mapping.CustomCalculation Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::customCalculation
	CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * ___customCalculation_5;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::customCalculationSourceData
	CustomCalculationSourceDataU5BU5D_t9CB27E1BF97E6EEF9BF27CECA4DC1B02B002F8D0* ___customCalculationSourceData_6;

public:
	inline static int32_t get_offset_of_elementIdentifier_0() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___elementIdentifier_0)); }
	inline int32_t get_elementIdentifier_0() const { return ___elementIdentifier_0; }
	inline int32_t* get_address_of_elementIdentifier_0() { return &___elementIdentifier_0; }
	inline void set_elementIdentifier_0(int32_t value)
	{
		___elementIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_sourceType_1() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___sourceType_1)); }
	inline int32_t get_sourceType_1() const { return ___sourceType_1; }
	inline int32_t* get_address_of_sourceType_1() { return &___sourceType_1; }
	inline void set_sourceType_1(int32_t value)
	{
		___sourceType_1 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_2() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___sourceAxis_2)); }
	inline int32_t get_sourceAxis_2() const { return ___sourceAxis_2; }
	inline int32_t* get_address_of_sourceAxis_2() { return &___sourceAxis_2; }
	inline void set_sourceAxis_2(int32_t value)
	{
		___sourceAxis_2 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_3() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___axisDeadZone_3)); }
	inline float get_axisDeadZone_3() const { return ___axisDeadZone_3; }
	inline float* get_address_of_axisDeadZone_3() { return &___axisDeadZone_3; }
	inline void set_axisDeadZone_3(float value)
	{
		___axisDeadZone_3 = value;
	}

	inline static int32_t get_offset_of_sourceButton_4() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___sourceButton_4)); }
	inline int32_t get_sourceButton_4() const { return ___sourceButton_4; }
	inline int32_t* get_address_of_sourceButton_4() { return &___sourceButton_4; }
	inline void set_sourceButton_4(int32_t value)
	{
		___sourceButton_4 = value;
	}

	inline static int32_t get_offset_of_customCalculation_5() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___customCalculation_5)); }
	inline CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * get_customCalculation_5() const { return ___customCalculation_5; }
	inline CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 ** get_address_of_customCalculation_5() { return &___customCalculation_5; }
	inline void set_customCalculation_5(CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * value)
	{
		___customCalculation_5 = value;
		Il2CppCodeGenWriteBarrier((&___customCalculation_5), value);
	}

	inline static int32_t get_offset_of_customCalculationSourceData_6() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___customCalculationSourceData_6)); }
	inline CustomCalculationSourceDataU5BU5D_t9CB27E1BF97E6EEF9BF27CECA4DC1B02B002F8D0* get_customCalculationSourceData_6() const { return ___customCalculationSourceData_6; }
	inline CustomCalculationSourceDataU5BU5D_t9CB27E1BF97E6EEF9BF27CECA4DC1B02B002F8D0** get_address_of_customCalculationSourceData_6() { return &___customCalculationSourceData_6; }
	inline void set_customCalculationSourceData_6(CustomCalculationSourceDataU5BU5D_t9CB27E1BF97E6EEF9BF27CECA4DC1B02B002F8D0* value)
	{
		___customCalculationSourceData_6 = value;
		Il2CppCodeGenWriteBarrier((&___customCalculationSourceData_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T917DF3B3892BA48FD5AE1595F8C288DC67D972BF_H
#ifndef RTUSLRSCMKUXFIQNJOVKDLNQGZE_TA770BD36FE7EED984B466118875ED99A2E7B79F2_H
#define RTUSLRSCMKUXFIQNJOVKDLNQGZE_TA770BD36FE7EED984B466118875ED99A2E7B79F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_RtuSLRsCmKuxFIQNJOVKdlNqgzE
struct  RtuSLRsCmKuxFIQNJOVKdlNqgzE_tA770BD36FE7EED984B466118875ED99A2E7B79F2  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_RtuSLRsCmKuxFIQNJOVKdlNqgzE::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_RtuSLRsCmKuxFIQNJOVKdlNqgzE::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_RtuSLRsCmKuxFIQNJOVKdlNqgzE::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_RtuSLRsCmKuxFIQNJOVKdlNqgzE::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_RtuSLRsCmKuxFIQNJOVKdlNqgzE::QfooGUvlRPCffgHolLoolnUhxmeD
	int32_t ___QfooGUvlRPCffgHolLoolnUhxmeD_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(RtuSLRsCmKuxFIQNJOVKdlNqgzE_tA770BD36FE7EED984B466118875ED99A2E7B79F2, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(RtuSLRsCmKuxFIQNJOVKdlNqgzE_tA770BD36FE7EED984B466118875ED99A2E7B79F2, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(RtuSLRsCmKuxFIQNJOVKdlNqgzE_tA770BD36FE7EED984B466118875ED99A2E7B79F2, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(RtuSLRsCmKuxFIQNJOVKdlNqgzE_tA770BD36FE7EED984B466118875ED99A2E7B79F2, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_QfooGUvlRPCffgHolLoolnUhxmeD_4() { return static_cast<int32_t>(offsetof(RtuSLRsCmKuxFIQNJOVKdlNqgzE_tA770BD36FE7EED984B466118875ED99A2E7B79F2, ___QfooGUvlRPCffgHolLoolnUhxmeD_4)); }
	inline int32_t get_QfooGUvlRPCffgHolLoolnUhxmeD_4() const { return ___QfooGUvlRPCffgHolLoolnUhxmeD_4; }
	inline int32_t* get_address_of_QfooGUvlRPCffgHolLoolnUhxmeD_4() { return &___QfooGUvlRPCffgHolLoolnUhxmeD_4; }
	inline void set_QfooGUvlRPCffgHolLoolnUhxmeD_4(int32_t value)
	{
		___QfooGUvlRPCffgHolLoolnUhxmeD_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RTUSLRSCMKUXFIQNJOVKDLNQGZE_TA770BD36FE7EED984B466118875ED99A2E7B79F2_H
#ifndef UFZZQDLIIUKOYVXNGYLYLJZDSQN_T2B54B04BE98FF1404C7AB978B376C5862541D535_H
#define UFZZQDLIIUKOYVXNGYLYLJZDSQN_T2B54B04BE98FF1404C7AB978B376C5862541D535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_UfZZQdlIiUkoyVXNGylyljzDsQN
struct  UfZZQdlIiUkoyVXNGylyljzDsQN_t2B54B04BE98FF1404C7AB978B376C5862541D535  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_UfZZQdlIiUkoyVXNGylyljzDsQN::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_UfZZQdlIiUkoyVXNGylyljzDsQN::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_UfZZQdlIiUkoyVXNGylyljzDsQN::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_UfZZQdlIiUkoyVXNGylyljzDsQN::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_UfZZQdlIiUkoyVXNGylyljzDsQN::xKNjJpCLurgrZZiHKtexKxikNFS
	int32_t ___xKNjJpCLurgrZZiHKtexKxikNFS_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(UfZZQdlIiUkoyVXNGylyljzDsQN_t2B54B04BE98FF1404C7AB978B376C5862541D535, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_tBB6149DD7A45B873206F15797DC2B002019BF09C ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(UfZZQdlIiUkoyVXNGylyljzDsQN_t2B54B04BE98FF1404C7AB978B376C5862541D535, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(UfZZQdlIiUkoyVXNGylyljzDsQN_t2B54B04BE98FF1404C7AB978B376C5862541D535, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(UfZZQdlIiUkoyVXNGylyljzDsQN_t2B54B04BE98FF1404C7AB978B376C5862541D535, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_xKNjJpCLurgrZZiHKtexKxikNFS_4() { return static_cast<int32_t>(offsetof(UfZZQdlIiUkoyVXNGylyljzDsQN_t2B54B04BE98FF1404C7AB978B376C5862541D535, ___xKNjJpCLurgrZZiHKtexKxikNFS_4)); }
	inline int32_t get_xKNjJpCLurgrZZiHKtexKxikNFS_4() const { return ___xKNjJpCLurgrZZiHKtexKxikNFS_4; }
	inline int32_t* get_address_of_xKNjJpCLurgrZZiHKtexKxikNFS_4() { return &___xKNjJpCLurgrZZiHKtexKxikNFS_4; }
	inline void set_xKNjJpCLurgrZZiHKtexKxikNFS_4(int32_t value)
	{
		___xKNjJpCLurgrZZiHKtexKxikNFS_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UFZZQDLIIUKOYVXNGYLYLJZDSQN_T2B54B04BE98FF1404C7AB978B376C5862541D535_H
#ifndef OHEGRRIHKFCWXGXTTZAYOITPCTFY_TCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9_H
#define OHEGRRIHKFCWXGXTTZAYOITPCTFY_TCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_OHegRriHkFCWxgxTTZaYoitpctfY
struct  OHegRriHkFCWxgxTTZaYoitpctfY_tCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_OHegRriHkFCWxgxTTZaYoitpctfY::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_OHegRriHkFCWxgxTTZaYoitpctfY::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_OHegRriHkFCWxgxTTZaYoitpctfY::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_OHegRriHkFCWxgxTTZaYoitpctfY::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_OHegRriHkFCWxgxTTZaYoitpctfY::BgDlucDEoLoZRSdDIkrIXfxGyiu
	int32_t ___BgDlucDEoLoZRSdDIkrIXfxGyiu_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(OHegRriHkFCWxgxTTZaYoitpctfY_tCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_tBB6149DD7A45B873206F15797DC2B002019BF09C ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(OHegRriHkFCWxgxTTZaYoitpctfY_tCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(OHegRriHkFCWxgxTTZaYoitpctfY_tCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(OHegRriHkFCWxgxTTZaYoitpctfY_tCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_BgDlucDEoLoZRSdDIkrIXfxGyiu_4() { return static_cast<int32_t>(offsetof(OHegRriHkFCWxgxTTZaYoitpctfY_tCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9, ___BgDlucDEoLoZRSdDIkrIXfxGyiu_4)); }
	inline int32_t get_BgDlucDEoLoZRSdDIkrIXfxGyiu_4() const { return ___BgDlucDEoLoZRSdDIkrIXfxGyiu_4; }
	inline int32_t* get_address_of_BgDlucDEoLoZRSdDIkrIXfxGyiu_4() { return &___BgDlucDEoLoZRSdDIkrIXfxGyiu_4; }
	inline void set_BgDlucDEoLoZRSdDIkrIXfxGyiu_4(int32_t value)
	{
		___BgDlucDEoLoZRSdDIkrIXfxGyiu_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OHEGRRIHKFCWXGXTTZAYOITPCTFY_TCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9_H
#ifndef DWDOUWHZCAFDNGHRTDSEPRIVPDI_T91E67F0372694CC0677E30D10669664E019D5A74_H
#define DWDOUWHZCAFDNGHRTDSEPRIVPDI_T91E67F0372694CC0677E30D10669664E019D5A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_DWDOuWhzCAFDNGhrTdSePriVPdI
struct  DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_DWDOuWhzCAFDNGhrTdSePriVPdI::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_DWDOuWhzCAFDNGhrTdSePriVPdI::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_DWDOuWhzCAFDNGhrTdSePriVPdI::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_DWDOuWhzCAFDNGhrTdSePriVPdI::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_DWDOuWhzCAFDNGhrTdSePriVPdI::LGqEJTUIaUohKyYOPHcKkgWwfFY
	int32_t ___LGqEJTUIaUohKyYOPHcKkgWwfFY_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_DWDOuWhzCAFDNGhrTdSePriVPdI::kYgrjxfdNwsUSkHBcOTxeZezFoB
	int32_t ___kYgrjxfdNwsUSkHBcOTxeZezFoB_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_LGqEJTUIaUohKyYOPHcKkgWwfFY_4() { return static_cast<int32_t>(offsetof(DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74, ___LGqEJTUIaUohKyYOPHcKkgWwfFY_4)); }
	inline int32_t get_LGqEJTUIaUohKyYOPHcKkgWwfFY_4() const { return ___LGqEJTUIaUohKyYOPHcKkgWwfFY_4; }
	inline int32_t* get_address_of_LGqEJTUIaUohKyYOPHcKkgWwfFY_4() { return &___LGqEJTUIaUohKyYOPHcKkgWwfFY_4; }
	inline void set_LGqEJTUIaUohKyYOPHcKkgWwfFY_4(int32_t value)
	{
		___LGqEJTUIaUohKyYOPHcKkgWwfFY_4 = value;
	}

	inline static int32_t get_offset_of_kYgrjxfdNwsUSkHBcOTxeZezFoB_5() { return static_cast<int32_t>(offsetof(DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74, ___kYgrjxfdNwsUSkHBcOTxeZezFoB_5)); }
	inline int32_t get_kYgrjxfdNwsUSkHBcOTxeZezFoB_5() const { return ___kYgrjxfdNwsUSkHBcOTxeZezFoB_5; }
	inline int32_t* get_address_of_kYgrjxfdNwsUSkHBcOTxeZezFoB_5() { return &___kYgrjxfdNwsUSkHBcOTxeZezFoB_5; }
	inline void set_kYgrjxfdNwsUSkHBcOTxeZezFoB_5(int32_t value)
	{
		___kYgrjxfdNwsUSkHBcOTxeZezFoB_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DWDOUWHZCAFDNGHRTDSEPRIVPDI_T91E67F0372694CC0677E30D10669664E019D5A74_H
#ifndef ELEMENT_TF82C07643751F54246D1E673496F2473BADBAD89_H
#define ELEMENT_TF82C07643751F54246D1E673496F2473BADBAD89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Element
struct  Element_tF82C07643751F54246D1E673496F2473BADBAD89  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_TF82C07643751F54246D1E673496F2473BADBAD89_H
#ifndef VPBAYYBIBKPMZGODCPSLHXZUFZE_TE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30_H
#define VPBAYYBIBKPMZGODCPSLHXZUFZE_TE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements_VpBAYyBibKpmZGODcPslhxzuFZe
struct  VpBAYyBibKpmZGODcPslhxzuFZe_tE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements_VpBAYyBibKpmZGODcPslhxzuFZe::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements_VpBAYyBibKpmZGODcPslhxzuFZe::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements_VpBAYyBibKpmZGODcPslhxzuFZe::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements_VpBAYyBibKpmZGODcPslhxzuFZe::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements_VpBAYyBibKpmZGODcPslhxzuFZe::szdBYzMOzBEiNOyTEALnijcajARk
	int32_t ___szdBYzMOzBEiNOyTEALnijcajARk_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(VpBAYyBibKpmZGODcPslhxzuFZe_tE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(VpBAYyBibKpmZGODcPslhxzuFZe_tE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(VpBAYyBibKpmZGODcPslhxzuFZe_tE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(VpBAYyBibKpmZGODcPslhxzuFZe_tE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_szdBYzMOzBEiNOyTEALnijcajARk_4() { return static_cast<int32_t>(offsetof(VpBAYyBibKpmZGODcPslhxzuFZe_tE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30, ___szdBYzMOzBEiNOyTEALnijcajARk_4)); }
	inline int32_t get_szdBYzMOzBEiNOyTEALnijcajARk_4() const { return ___szdBYzMOzBEiNOyTEALnijcajARk_4; }
	inline int32_t* get_address_of_szdBYzMOzBEiNOyTEALnijcajARk_4() { return &___szdBYzMOzBEiNOyTEALnijcajARk_4; }
	inline void set_szdBYzMOzBEiNOyTEALnijcajARk_4(int32_t value)
	{
		___szdBYzMOzBEiNOyTEALnijcajARk_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VPBAYYBIBKPMZGODCPSLHXZUFZE_TE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30_H
#ifndef NPSZMSMHLJRWSDCBYDTJBBOXWPZ_TA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF_H
#define NPSZMSMHLJRWSDCBYDTJBBOXWPZ_TA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements_npSZmSmhLjrWsDCbyDTJbboxwPZ
struct  npSZmSmhLjrWsDCbyDTJbboxwPZ_tA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements_npSZmSmhLjrWsDCbyDTJbboxwPZ::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements_npSZmSmhLjrWsDCbyDTJbboxwPZ::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements_npSZmSmhLjrWsDCbyDTJbboxwPZ::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements_npSZmSmhLjrWsDCbyDTJbboxwPZ::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements_npSZmSmhLjrWsDCbyDTJbboxwPZ::oGFjPAXAAzOpDCMxHYfXjDOrGqNj
	int32_t ___oGFjPAXAAzOpDCMxHYfXjDOrGqNj_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(npSZmSmhLjrWsDCbyDTJbboxwPZ_tA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(npSZmSmhLjrWsDCbyDTJbboxwPZ_tA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(npSZmSmhLjrWsDCbyDTJbboxwPZ_tA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(npSZmSmhLjrWsDCbyDTJbboxwPZ_tA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_oGFjPAXAAzOpDCMxHYfXjDOrGqNj_4() { return static_cast<int32_t>(offsetof(npSZmSmhLjrWsDCbyDTJbboxwPZ_tA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF, ___oGFjPAXAAzOpDCMxHYfXjDOrGqNj_4)); }
	inline int32_t get_oGFjPAXAAzOpDCMxHYfXjDOrGqNj_4() const { return ___oGFjPAXAAzOpDCMxHYfXjDOrGqNj_4; }
	inline int32_t* get_address_of_oGFjPAXAAzOpDCMxHYfXjDOrGqNj_4() { return &___oGFjPAXAAzOpDCMxHYfXjDOrGqNj_4; }
	inline void set_oGFjPAXAAzOpDCMxHYfXjDOrGqNj_4(int32_t value)
	{
		___oGFjPAXAAzOpDCMxHYfXjDOrGqNj_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NPSZMSMHLJRWSDCBYDTJBBOXWPZ_TA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF_H
#ifndef VICAMHESIFEZHRNLOCVIGZJDXCYA_TB5CD5A134029C163B14277BB6281FD1B6726B7A6_H
#define VICAMHESIFEZHRNLOCVIGZJDXCYA_TB5CD5A134029C163B14277BB6281FD1B6726B7A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_vIcamHEsifEzhrnLOcviGZJdxCyA
struct  vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_vIcamHEsifEzhrnLOcviGZJdxCyA::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_vIcamHEsifEzhrnLOcviGZJdxCyA::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_vIcamHEsifEzhrnLOcviGZJdxCyA::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_vIcamHEsifEzhrnLOcviGZJdxCyA::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_vIcamHEsifEzhrnLOcviGZJdxCyA::YxkCWVIBkqTatlcFupPKznTVXyU
	int32_t ___YxkCWVIBkqTatlcFupPKznTVXyU_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_vIcamHEsifEzhrnLOcviGZJdxCyA::KKuxmoDgeRdLyJsMGksADDUIEEWv
	int32_t ___KKuxmoDgeRdLyJsMGksADDUIEEWv_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_YxkCWVIBkqTatlcFupPKznTVXyU_4() { return static_cast<int32_t>(offsetof(vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6, ___YxkCWVIBkqTatlcFupPKznTVXyU_4)); }
	inline int32_t get_YxkCWVIBkqTatlcFupPKznTVXyU_4() const { return ___YxkCWVIBkqTatlcFupPKznTVXyU_4; }
	inline int32_t* get_address_of_YxkCWVIBkqTatlcFupPKznTVXyU_4() { return &___YxkCWVIBkqTatlcFupPKznTVXyU_4; }
	inline void set_YxkCWVIBkqTatlcFupPKznTVXyU_4(int32_t value)
	{
		___YxkCWVIBkqTatlcFupPKznTVXyU_4 = value;
	}

	inline static int32_t get_offset_of_KKuxmoDgeRdLyJsMGksADDUIEEWv_5() { return static_cast<int32_t>(offsetof(vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6, ___KKuxmoDgeRdLyJsMGksADDUIEEWv_5)); }
	inline int32_t get_KKuxmoDgeRdLyJsMGksADDUIEEWv_5() const { return ___KKuxmoDgeRdLyJsMGksADDUIEEWv_5; }
	inline int32_t* get_address_of_KKuxmoDgeRdLyJsMGksADDUIEEWv_5() { return &___KKuxmoDgeRdLyJsMGksADDUIEEWv_5; }
	inline void set_KKuxmoDgeRdLyJsMGksADDUIEEWv_5(int32_t value)
	{
		___KKuxmoDgeRdLyJsMGksADDUIEEWv_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VICAMHESIFEZHRNLOCVIGZJDXCYA_TB5CD5A134029C163B14277BB6281FD1B6726B7A6_H
#ifndef CLIENTINFO_T861C31EFF3BB9900E4362653BE3451F9171E059A_H
#define CLIENTINFO_T861C31EFF3BB9900E4362653BE3451F9171E059A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria_ClientInfo
struct  ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria_ClientInfo::browser
	int32_t ___browser_0;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria_ClientInfo::browserVersionMin
	String_t* ___browserVersionMin_1;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria_ClientInfo::browserVersionMax
	String_t* ___browserVersionMax_2;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria_ClientInfo::os
	int32_t ___os_3;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria_ClientInfo::osVersionMin
	String_t* ___osVersionMin_4;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria_ClientInfo::osVersionMax
	String_t* ___osVersionMax_5;

public:
	inline static int32_t get_offset_of_browser_0() { return static_cast<int32_t>(offsetof(ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A, ___browser_0)); }
	inline int32_t get_browser_0() const { return ___browser_0; }
	inline int32_t* get_address_of_browser_0() { return &___browser_0; }
	inline void set_browser_0(int32_t value)
	{
		___browser_0 = value;
	}

	inline static int32_t get_offset_of_browserVersionMin_1() { return static_cast<int32_t>(offsetof(ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A, ___browserVersionMin_1)); }
	inline String_t* get_browserVersionMin_1() const { return ___browserVersionMin_1; }
	inline String_t** get_address_of_browserVersionMin_1() { return &___browserVersionMin_1; }
	inline void set_browserVersionMin_1(String_t* value)
	{
		___browserVersionMin_1 = value;
		Il2CppCodeGenWriteBarrier((&___browserVersionMin_1), value);
	}

	inline static int32_t get_offset_of_browserVersionMax_2() { return static_cast<int32_t>(offsetof(ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A, ___browserVersionMax_2)); }
	inline String_t* get_browserVersionMax_2() const { return ___browserVersionMax_2; }
	inline String_t** get_address_of_browserVersionMax_2() { return &___browserVersionMax_2; }
	inline void set_browserVersionMax_2(String_t* value)
	{
		___browserVersionMax_2 = value;
		Il2CppCodeGenWriteBarrier((&___browserVersionMax_2), value);
	}

	inline static int32_t get_offset_of_os_3() { return static_cast<int32_t>(offsetof(ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A, ___os_3)); }
	inline int32_t get_os_3() const { return ___os_3; }
	inline int32_t* get_address_of_os_3() { return &___os_3; }
	inline void set_os_3(int32_t value)
	{
		___os_3 = value;
	}

	inline static int32_t get_offset_of_osVersionMin_4() { return static_cast<int32_t>(offsetof(ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A, ___osVersionMin_4)); }
	inline String_t* get_osVersionMin_4() const { return ___osVersionMin_4; }
	inline String_t** get_address_of_osVersionMin_4() { return &___osVersionMin_4; }
	inline void set_osVersionMin_4(String_t* value)
	{
		___osVersionMin_4 = value;
		Il2CppCodeGenWriteBarrier((&___osVersionMin_4), value);
	}

	inline static int32_t get_offset_of_osVersionMax_5() { return static_cast<int32_t>(offsetof(ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A, ___osVersionMax_5)); }
	inline String_t* get_osVersionMax_5() const { return ___osVersionMax_5; }
	inline String_t** get_address_of_osVersionMax_5() { return &___osVersionMax_5; }
	inline void set_osVersionMax_5(String_t* value)
	{
		___osVersionMax_5 = value;
		Il2CppCodeGenWriteBarrier((&___osVersionMax_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTINFO_T861C31EFF3BB9900E4362653BE3451F9171E059A_H
#ifndef AEVRRNDUXIDXHBKXBCZKTGXWCWW_T57F24F185240CA9A922A55B97B19CF698A68A60C_H
#define AEVRRNDUXIDXHBKXBCZKTGXWCWW_T57F24F185240CA9A922A55B97B19CF698A68A60C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_aeVrRnduXidXhBkxBczKtGXwcww
struct  aeVrRnduXidXhBkxBczKtGXwcww_t57F24F185240CA9A922A55B97B19CF698A68A60C  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_aeVrRnduXidXhBkxBczKtGXwcww::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_aeVrRnduXidXhBkxBczKtGXwcww::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_aeVrRnduXidXhBkxBczKtGXwcww::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_aeVrRnduXidXhBkxBczKtGXwcww::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_aeVrRnduXidXhBkxBczKtGXwcww::VhcVrGiSXfYUOQpmCimLWGMiopq
	int32_t ___VhcVrGiSXfYUOQpmCimLWGMiopq_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(aeVrRnduXidXhBkxBczKtGXwcww_t57F24F185240CA9A922A55B97B19CF698A68A60C, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_tBB6149DD7A45B873206F15797DC2B002019BF09C ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(aeVrRnduXidXhBkxBczKtGXwcww_t57F24F185240CA9A922A55B97B19CF698A68A60C, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(aeVrRnduXidXhBkxBczKtGXwcww_t57F24F185240CA9A922A55B97B19CF698A68A60C, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(aeVrRnduXidXhBkxBczKtGXwcww_t57F24F185240CA9A922A55B97B19CF698A68A60C, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_VhcVrGiSXfYUOQpmCimLWGMiopq_4() { return static_cast<int32_t>(offsetof(aeVrRnduXidXhBkxBczKtGXwcww_t57F24F185240CA9A922A55B97B19CF698A68A60C, ___VhcVrGiSXfYUOQpmCimLWGMiopq_4)); }
	inline int32_t get_VhcVrGiSXfYUOQpmCimLWGMiopq_4() const { return ___VhcVrGiSXfYUOQpmCimLWGMiopq_4; }
	inline int32_t* get_address_of_VhcVrGiSXfYUOQpmCimLWGMiopq_4() { return &___VhcVrGiSXfYUOQpmCimLWGMiopq_4; }
	inline void set_VhcVrGiSXfYUOQpmCimLWGMiopq_4(int32_t value)
	{
		___VhcVrGiSXfYUOQpmCimLWGMiopq_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AEVRRNDUXIDXHBKXBCZKTGXWCWW_T57F24F185240CA9A922A55B97B19CF698A68A60C_H
#ifndef HEXSRBBVNDFDNVYIZQNEQMIKFDYK_T01E1558A075EF9DA08743233E7C2B00F769DF914_H
#define HEXSRBBVNDFDNVYIZQNEQMIKFDYK_T01E1558A075EF9DA08743233E7C2B00F769DF914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_heXSrBbVNdfDNVYIZQneqmiKFDyk
struct  heXSrBbVNdfDNVYIZQneqmiKFDyk_t01E1558A075EF9DA08743233E7C2B00F769DF914  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_heXSrBbVNdfDNVYIZQneqmiKFDyk::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_heXSrBbVNdfDNVYIZQneqmiKFDyk::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_heXSrBbVNdfDNVYIZQneqmiKFDyk::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_heXSrBbVNdfDNVYIZQneqmiKFDyk::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_heXSrBbVNdfDNVYIZQneqmiKFDyk::uZfwYVMWOeVjGdOLmpFJhiBPqqi
	int32_t ___uZfwYVMWOeVjGdOLmpFJhiBPqqi_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(heXSrBbVNdfDNVYIZQneqmiKFDyk_t01E1558A075EF9DA08743233E7C2B00F769DF914, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(heXSrBbVNdfDNVYIZQneqmiKFDyk_t01E1558A075EF9DA08743233E7C2B00F769DF914, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(heXSrBbVNdfDNVYIZQneqmiKFDyk_t01E1558A075EF9DA08743233E7C2B00F769DF914, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(heXSrBbVNdfDNVYIZQneqmiKFDyk_t01E1558A075EF9DA08743233E7C2B00F769DF914, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_uZfwYVMWOeVjGdOLmpFJhiBPqqi_4() { return static_cast<int32_t>(offsetof(heXSrBbVNdfDNVYIZQneqmiKFDyk_t01E1558A075EF9DA08743233E7C2B00F769DF914, ___uZfwYVMWOeVjGdOLmpFJhiBPqqi_4)); }
	inline int32_t get_uZfwYVMWOeVjGdOLmpFJhiBPqqi_4() const { return ___uZfwYVMWOeVjGdOLmpFJhiBPqqi_4; }
	inline int32_t* get_address_of_uZfwYVMWOeVjGdOLmpFJhiBPqqi_4() { return &___uZfwYVMWOeVjGdOLmpFJhiBPqqi_4; }
	inline void set_uZfwYVMWOeVjGdOLmpFJhiBPqqi_4(int32_t value)
	{
		___uZfwYVMWOeVjGdOLmpFJhiBPqqi_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXSRBBVNDFDNVYIZQNEQMIKFDYK_T01E1558A075EF9DA08743233E7C2B00F769DF914_H
#ifndef TZUQNKYTLNKVJFPRUGSLYVFBXEG_TF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9_H
#define TZUQNKYTLNKVJFPRUGSLYVFBXEG_TF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_TzUqnKytlNKvJFprUGslyvFbxeg
struct  TzUqnKytlNKvJFprUGslyvFbxeg_tF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9  : public RuntimeObject
{
public:
	// Rewired.Interfaces.IControllerElementIdentifierCommon_Internal Rewired.Data.Mapping.HardwareJoystickMap_TzUqnKytlNKvJFprUGslyvFbxeg::NOArrsNdfKDShNFftfbPqYDzhpq
	RuntimeObject* ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_TzUqnKytlNKvJFprUGslyvFbxeg::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_TzUqnKytlNKvJFprUGslyvFbxeg::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap Rewired.Data.Mapping.HardwareJoystickMap_TzUqnKytlNKvJFprUGslyvFbxeg::DKaFTWTMhFeLQHDExnDMRxZfmROL
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_TzUqnKytlNKvJFprUGslyvFbxeg::pRNxTDMuDtMhsXMShfGGUiXeGQk
	int32_t ___pRNxTDMuDtMhsXMShfGGUiXeGQk_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(TzUqnKytlNKvJFprUGslyvFbxeg_tF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline RuntimeObject* get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline RuntimeObject** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(RuntimeObject* value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(TzUqnKytlNKvJFprUGslyvFbxeg_tF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(TzUqnKytlNKvJFprUGslyvFbxeg_tF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(TzUqnKytlNKvJFprUGslyvFbxeg_tF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_pRNxTDMuDtMhsXMShfGGUiXeGQk_4() { return static_cast<int32_t>(offsetof(TzUqnKytlNKvJFprUGslyvFbxeg_tF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9, ___pRNxTDMuDtMhsXMShfGGUiXeGQk_4)); }
	inline int32_t get_pRNxTDMuDtMhsXMShfGGUiXeGQk_4() const { return ___pRNxTDMuDtMhsXMShfGGUiXeGQk_4; }
	inline int32_t* get_address_of_pRNxTDMuDtMhsXMShfGGUiXeGQk_4() { return &___pRNxTDMuDtMhsXMShfGGUiXeGQk_4; }
	inline void set_pRNxTDMuDtMhsXMShfGGUiXeGQk_4(int32_t value)
	{
		___pRNxTDMuDtMhsXMShfGGUiXeGQk_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TZUQNKYTLNKVJFPRUGSLYVFBXEG_TF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9_H
#ifndef VWAFIZERBRBMOYRYCKHZQVAXSSIK_TA49E49245CC40AB29644D73EEC167BD87D5FFEAF_H
#define VWAFIZERBRBMOYRYCKHZQVAXSSIK_TA49E49245CC40AB29644D73EEC167BD87D5FFEAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_VWAfIzERbRbMOyRyCKHzqvAxSsik
struct  VWAfIzERbRbMOyRyCKHzqvAxSsik_tA49E49245CC40AB29644D73EEC167BD87D5FFEAF  : public RuntimeObject
{
public:
	// Rewired.ControllerElementIdentifier Rewired.Data.Mapping.HardwareJoystickMap_VWAfIzERbRbMOyRyCKHzqvAxSsik::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_VWAfIzERbRbMOyRyCKHzqvAxSsik::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_VWAfIzERbRbMOyRyCKHzqvAxSsik::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap Rewired.Data.Mapping.HardwareJoystickMap_VWAfIzERbRbMOyRyCKHzqvAxSsik::DKaFTWTMhFeLQHDExnDMRxZfmROL
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_VWAfIzERbRbMOyRyCKHzqvAxSsik::yLrOOaqYzmIyaXSCvApOKYnGotw
	int32_t ___yLrOOaqYzmIyaXSCvApOKYnGotw_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(VWAfIzERbRbMOyRyCKHzqvAxSsik_tA49E49245CC40AB29644D73EEC167BD87D5FFEAF, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(VWAfIzERbRbMOyRyCKHzqvAxSsik_tA49E49245CC40AB29644D73EEC167BD87D5FFEAF, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(VWAfIzERbRbMOyRyCKHzqvAxSsik_tA49E49245CC40AB29644D73EEC167BD87D5FFEAF, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(VWAfIzERbRbMOyRyCKHzqvAxSsik_tA49E49245CC40AB29644D73EEC167BD87D5FFEAF, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_yLrOOaqYzmIyaXSCvApOKYnGotw_4() { return static_cast<int32_t>(offsetof(VWAfIzERbRbMOyRyCKHzqvAxSsik_tA49E49245CC40AB29644D73EEC167BD87D5FFEAF, ___yLrOOaqYzmIyaXSCvApOKYnGotw_4)); }
	inline int32_t get_yLrOOaqYzmIyaXSCvApOKYnGotw_4() const { return ___yLrOOaqYzmIyaXSCvApOKYnGotw_4; }
	inline int32_t* get_address_of_yLrOOaqYzmIyaXSCvApOKYnGotw_4() { return &___yLrOOaqYzmIyaXSCvApOKYnGotw_4; }
	inline void set_yLrOOaqYzmIyaXSCvApOKYnGotw_4(int32_t value)
	{
		___yLrOOaqYzmIyaXSCvApOKYnGotw_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VWAFIZERBRBMOYRYCKHZQVAXSSIK_TA49E49245CC40AB29644D73EEC167BD87D5FFEAF_H
#ifndef ELEMENTIDENTIFIERMAP_TD6D9491C76F5D9987CDC83D5E6D2E12403D4C864_H
#define ELEMENTIDENTIFIERMAP_TD6D9491C76F5D9987CDC83D5E6D2E12403D4C864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickTemplateMap_ElementIdentifierMap
struct  ElementIdentifierMap_tD6D9491C76F5D9987CDC83D5E6D2E12403D4C864  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap_ElementIdentifierMap::templateId
	int32_t ___templateId_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap_ElementIdentifierMap::joystickId
	int32_t ___joystickId_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap_ElementIdentifierMap::joystickId2
	int32_t ___joystickId2_2;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickTemplateMap_ElementIdentifierMap::splitAxis
	bool ___splitAxis_3;

public:
	inline static int32_t get_offset_of_templateId_0() { return static_cast<int32_t>(offsetof(ElementIdentifierMap_tD6D9491C76F5D9987CDC83D5E6D2E12403D4C864, ___templateId_0)); }
	inline int32_t get_templateId_0() const { return ___templateId_0; }
	inline int32_t* get_address_of_templateId_0() { return &___templateId_0; }
	inline void set_templateId_0(int32_t value)
	{
		___templateId_0 = value;
	}

	inline static int32_t get_offset_of_joystickId_1() { return static_cast<int32_t>(offsetof(ElementIdentifierMap_tD6D9491C76F5D9987CDC83D5E6D2E12403D4C864, ___joystickId_1)); }
	inline int32_t get_joystickId_1() const { return ___joystickId_1; }
	inline int32_t* get_address_of_joystickId_1() { return &___joystickId_1; }
	inline void set_joystickId_1(int32_t value)
	{
		___joystickId_1 = value;
	}

	inline static int32_t get_offset_of_joystickId2_2() { return static_cast<int32_t>(offsetof(ElementIdentifierMap_tD6D9491C76F5D9987CDC83D5E6D2E12403D4C864, ___joystickId2_2)); }
	inline int32_t get_joystickId2_2() const { return ___joystickId2_2; }
	inline int32_t* get_address_of_joystickId2_2() { return &___joystickId2_2; }
	inline void set_joystickId2_2(int32_t value)
	{
		___joystickId2_2 = value;
	}

	inline static int32_t get_offset_of_splitAxis_3() { return static_cast<int32_t>(offsetof(ElementIdentifierMap_tD6D9491C76F5D9987CDC83D5E6D2E12403D4C864, ___splitAxis_3)); }
	inline bool get_splitAxis_3() const { return ___splitAxis_3; }
	inline bool* get_address_of_splitAxis_3() { return &___splitAxis_3; }
	inline void set_splitAxis_3(bool value)
	{
		___splitAxis_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTIDENTIFIERMAP_TD6D9491C76F5D9987CDC83D5E6D2E12403D4C864_H
#ifndef ENTRY_TE166222F1AF07AD57E6D234739AB39929E5A84FF_H
#define ENTRY_TE166222F1AF07AD57E6D234739AB39929E5A84FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickTemplateMap_Entry
struct  Entry_tE166222F1AF07AD57E6D234739AB39929E5A84FF  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap_Entry::id
	int32_t ___id_0;
	// System.String Rewired.Data.Mapping.HardwareJoystickTemplateMap_Entry::name
	String_t* ___name_1;
	// System.String Rewired.Data.Mapping.HardwareJoystickTemplateMap_Entry::joystickGuid
	String_t* ___joystickGuid_2;
	// System.String Rewired.Data.Mapping.HardwareJoystickTemplateMap_Entry::fileGuid
	String_t* ___fileGuid_3;
	// System.Collections.Generic.List`1<Rewired.Data.Mapping.HardwareJoystickTemplateMap_ElementIdentifierMap> Rewired.Data.Mapping.HardwareJoystickTemplateMap_Entry::elementIdentifierMappings
	List_1_t5A6709ED126B183372B44781780EBCFAF4B40B12 * ___elementIdentifierMappings_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Entry_tE166222F1AF07AD57E6D234739AB39929E5A84FF, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Entry_tE166222F1AF07AD57E6D234739AB39929E5A84FF, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_joystickGuid_2() { return static_cast<int32_t>(offsetof(Entry_tE166222F1AF07AD57E6D234739AB39929E5A84FF, ___joystickGuid_2)); }
	inline String_t* get_joystickGuid_2() const { return ___joystickGuid_2; }
	inline String_t** get_address_of_joystickGuid_2() { return &___joystickGuid_2; }
	inline void set_joystickGuid_2(String_t* value)
	{
		___joystickGuid_2 = value;
		Il2CppCodeGenWriteBarrier((&___joystickGuid_2), value);
	}

	inline static int32_t get_offset_of_fileGuid_3() { return static_cast<int32_t>(offsetof(Entry_tE166222F1AF07AD57E6D234739AB39929E5A84FF, ___fileGuid_3)); }
	inline String_t* get_fileGuid_3() const { return ___fileGuid_3; }
	inline String_t** get_address_of_fileGuid_3() { return &___fileGuid_3; }
	inline void set_fileGuid_3(String_t* value)
	{
		___fileGuid_3 = value;
		Il2CppCodeGenWriteBarrier((&___fileGuid_3), value);
	}

	inline static int32_t get_offset_of_elementIdentifierMappings_4() { return static_cast<int32_t>(offsetof(Entry_tE166222F1AF07AD57E6D234739AB39929E5A84FF, ___elementIdentifierMappings_4)); }
	inline List_1_t5A6709ED126B183372B44781780EBCFAF4B40B12 * get_elementIdentifierMappings_4() const { return ___elementIdentifierMappings_4; }
	inline List_1_t5A6709ED126B183372B44781780EBCFAF4B40B12 ** get_address_of_elementIdentifierMappings_4() { return &___elementIdentifierMappings_4; }
	inline void set_elementIdentifierMappings_4(List_1_t5A6709ED126B183372B44781780EBCFAF4B40B12 * value)
	{
		___elementIdentifierMappings_4 = value;
		Il2CppCodeGenWriteBarrier((&___elementIdentifierMappings_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_TE166222F1AF07AD57E6D234739AB39929E5A84FF_H
#ifndef SPECIALELEMENTENTRY_TAFD3A24233FA5CA2DD106FF59575CDDDF0D47149_H
#define SPECIALELEMENTENTRY_TAFD3A24233FA5CA2DD106FF59575CDDDF0D47149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickTemplateMap_SpecialElementEntry
struct  SpecialElementEntry_tAFD3A24233FA5CA2DD106FF59575CDDDF0D47149  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap_SpecialElementEntry::elementIdentifierId
	int32_t ___elementIdentifierId_0;
	// System.String Rewired.Data.Mapping.HardwareJoystickTemplateMap_SpecialElementEntry::data
	String_t* ___data_1;

public:
	inline static int32_t get_offset_of_elementIdentifierId_0() { return static_cast<int32_t>(offsetof(SpecialElementEntry_tAFD3A24233FA5CA2DD106FF59575CDDDF0D47149, ___elementIdentifierId_0)); }
	inline int32_t get_elementIdentifierId_0() const { return ___elementIdentifierId_0; }
	inline int32_t* get_address_of_elementIdentifierId_0() { return &___elementIdentifierId_0; }
	inline void set_elementIdentifierId_0(int32_t value)
	{
		___elementIdentifierId_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(SpecialElementEntry_tAFD3A24233FA5CA2DD106FF59575CDDDF0D47149, ___data_1)); }
	inline String_t* get_data_1() const { return ___data_1; }
	inline String_t** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(String_t* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALELEMENTENTRY_TAFD3A24233FA5CA2DD106FF59575CDDDF0D47149_H
#ifndef OJMSNZEGRAAVSFPFAWVMPWWOYKTL_T7DA8D3D9AA0D25A904860868B7B990A346C4C76C_H
#define OJMSNZEGRAAVSFPFAWVMPWWOYKTL_T7DA8D3D9AA0D25A904860868B7B990A346C4C76C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickTemplateMap_ojmsnzeGRaAVSFPFaWvMPwwoyKTl
struct  ojmsnzeGRaAVSFPFaWvMPwwoyKTl_t7DA8D3D9AA0D25A904860868B7B990A346C4C76C  : public RuntimeObject
{
public:
	// Rewired.ControllerTemplateElementIdentifier Rewired.Data.Mapping.HardwareJoystickTemplateMap_ojmsnzeGRaAVSFPFaWvMPwwoyKTl::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap_ojmsnzeGRaAVSFPFaWvMPwwoyKTl::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap_ojmsnzeGRaAVSFPFaWvMPwwoyKTl::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickTemplateMap Rewired.Data.Mapping.HardwareJoystickTemplateMap_ojmsnzeGRaAVSFPFaWvMPwwoyKTl::DKaFTWTMhFeLQHDExnDMRxZfmROL
	HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap_ojmsnzeGRaAVSFPFaWvMPwwoyKTl::okMFupfyoBNZDyKMyvAtwGIUiAd
	int32_t ___okMFupfyoBNZDyKMyvAtwGIUiAd_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(ojmsnzeGRaAVSFPFaWvMPwwoyKTl_t7DA8D3D9AA0D25A904860868B7B990A346C4C76C, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(ojmsnzeGRaAVSFPFaWvMPwwoyKTl_t7DA8D3D9AA0D25A904860868B7B990A346C4C76C, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(ojmsnzeGRaAVSFPFaWvMPwwoyKTl_t7DA8D3D9AA0D25A904860868B7B990A346C4C76C, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(ojmsnzeGRaAVSFPFaWvMPwwoyKTl_t7DA8D3D9AA0D25A904860868B7B990A346C4C76C, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4() { return static_cast<int32_t>(offsetof(ojmsnzeGRaAVSFPFaWvMPwwoyKTl_t7DA8D3D9AA0D25A904860868B7B990A346C4C76C, ___okMFupfyoBNZDyKMyvAtwGIUiAd_4)); }
	inline int32_t get_okMFupfyoBNZDyKMyvAtwGIUiAd_4() const { return ___okMFupfyoBNZDyKMyvAtwGIUiAd_4; }
	inline int32_t* get_address_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4() { return &___okMFupfyoBNZDyKMyvAtwGIUiAd_4; }
	inline void set_okMFupfyoBNZDyKMyvAtwGIUiAd_4(int32_t value)
	{
		___okMFupfyoBNZDyKMyvAtwGIUiAd_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OJMSNZEGRAAVSFPFAWVMPWWOYKTL_T7DA8D3D9AA0D25A904860868B7B990A346C4C76C_H
#ifndef YNTTKZIOVNNBGLSXIHRFAGJODMM_T443007B2F5EF88F5ED01E4BA3295DE86AA0E9578_H
#define YNTTKZIOVNNBGLSXIHRFAGJODMM_T443007B2F5EF88F5ED01E4BA3295DE86AA0E9578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickTemplateMap_yntTKZiOvnnBgLSXIhRFagJODMM
struct  yntTKZiOvnnBgLSXIhRFagJODMM_t443007B2F5EF88F5ED01E4BA3295DE86AA0E9578  : public RuntimeObject
{
public:
	// Rewired.Interfaces.IControllerElementIdentifierCommon_Internal Rewired.Data.Mapping.HardwareJoystickTemplateMap_yntTKZiOvnnBgLSXIhRFagJODMM::NOArrsNdfKDShNFftfbPqYDzhpq
	RuntimeObject* ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap_yntTKZiOvnnBgLSXIhRFagJODMM::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap_yntTKZiOvnnBgLSXIhRFagJODMM::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickTemplateMap Rewired.Data.Mapping.HardwareJoystickTemplateMap_yntTKZiOvnnBgLSXIhRFagJODMM::DKaFTWTMhFeLQHDExnDMRxZfmROL
	HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap_yntTKZiOvnnBgLSXIhRFagJODMM::yLrOOaqYzmIyaXSCvApOKYnGotw
	int32_t ___yLrOOaqYzmIyaXSCvApOKYnGotw_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(yntTKZiOvnnBgLSXIhRFagJODMM_t443007B2F5EF88F5ED01E4BA3295DE86AA0E9578, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline RuntimeObject* get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline RuntimeObject** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(RuntimeObject* value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(yntTKZiOvnnBgLSXIhRFagJODMM_t443007B2F5EF88F5ED01E4BA3295DE86AA0E9578, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(yntTKZiOvnnBgLSXIhRFagJODMM_t443007B2F5EF88F5ED01E4BA3295DE86AA0E9578, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(yntTKZiOvnnBgLSXIhRFagJODMM_t443007B2F5EF88F5ED01E4BA3295DE86AA0E9578, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_yLrOOaqYzmIyaXSCvApOKYnGotw_4() { return static_cast<int32_t>(offsetof(yntTKZiOvnnBgLSXIhRFagJODMM_t443007B2F5EF88F5ED01E4BA3295DE86AA0E9578, ___yLrOOaqYzmIyaXSCvApOKYnGotw_4)); }
	inline int32_t get_yLrOOaqYzmIyaXSCvApOKYnGotw_4() const { return ___yLrOOaqYzmIyaXSCvApOKYnGotw_4; }
	inline int32_t* get_address_of_yLrOOaqYzmIyaXSCvApOKYnGotw_4() { return &___yLrOOaqYzmIyaXSCvApOKYnGotw_4; }
	inline void set_yLrOOaqYzmIyaXSCvApOKYnGotw_4(int32_t value)
	{
		___yLrOOaqYzmIyaXSCvApOKYnGotw_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YNTTKZIOVNNBGLSXIHRFAGJODMM_T443007B2F5EF88F5ED01E4BA3295DE86AA0E9578_H
#ifndef PLAYER_EDITOR_T17BA86B71CF8428935542F1EC81DD218A7EF36B7_H
#define PLAYER_EDITOR_T17BA86B71CF8428935542F1EC81DD218A7EF36B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Player_Editor
struct  Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Player_Editor::_id
	int32_t ____id_0;
	// System.String Rewired.Data.Player_Editor::_name
	String_t* ____name_1;
	// System.String Rewired.Data.Player_Editor::_descriptiveName
	String_t* ____descriptiveName_2;
	// System.Boolean Rewired.Data.Player_Editor::_startPlaying
	bool ____startPlaying_3;
	// System.Collections.Generic.List`1<Rewired.Data.Player_Editor_Mapping> Rewired.Data.Player_Editor::_defaultJoystickMaps
	List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D * ____defaultJoystickMaps_4;
	// System.Collections.Generic.List`1<Rewired.Data.Player_Editor_Mapping> Rewired.Data.Player_Editor::_defaultMouseMaps
	List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D * ____defaultMouseMaps_5;
	// System.Collections.Generic.List`1<Rewired.Data.Player_Editor_Mapping> Rewired.Data.Player_Editor::_defaultKeyboardMaps
	List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D * ____defaultKeyboardMaps_6;
	// System.Collections.Generic.List`1<Rewired.Data.Player_Editor_Mapping> Rewired.Data.Player_Editor::_defaultCustomControllerMaps
	List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D * ____defaultCustomControllerMaps_7;
	// System.Collections.Generic.List`1<Rewired.Data.Player_Editor_CreateControllerInfo> Rewired.Data.Player_Editor::_startingCustomControllers
	List_1_tB69BD32C8767F97DE719C7E69179AFBEE7636A2E * ____startingCustomControllers_8;
	// System.Boolean Rewired.Data.Player_Editor::_assignMouseOnStart
	bool ____assignMouseOnStart_9;
	// System.Boolean Rewired.Data.Player_Editor::_assignKeyboardOnStart
	bool ____assignKeyboardOnStart_10;
	// System.Boolean Rewired.Data.Player_Editor::_excludeFromControllerAutoAssignment
	bool ____excludeFromControllerAutoAssignment_11;
	// Rewired.Data.Player_Editor_ControllerMapLayoutManagerSettings Rewired.Data.Player_Editor::_controllerMapLayoutManagerSettings
	ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98 * ____controllerMapLayoutManagerSettings_12;
	// Rewired.Data.Player_Editor_ControllerMapEnablerSettings Rewired.Data.Player_Editor::_controllerMapEnablerSettings
	ControllerMapEnablerSettings_t34E49435CD1543BBA8B8420276849C0AFE4E02F1 * ____controllerMapEnablerSettings_13;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____name_1)); }
	inline String_t* get__name_1() const { return ____name_1; }
	inline String_t** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(String_t* value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier((&____name_1), value);
	}

	inline static int32_t get_offset_of__descriptiveName_2() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____descriptiveName_2)); }
	inline String_t* get__descriptiveName_2() const { return ____descriptiveName_2; }
	inline String_t** get_address_of__descriptiveName_2() { return &____descriptiveName_2; }
	inline void set__descriptiveName_2(String_t* value)
	{
		____descriptiveName_2 = value;
		Il2CppCodeGenWriteBarrier((&____descriptiveName_2), value);
	}

	inline static int32_t get_offset_of__startPlaying_3() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____startPlaying_3)); }
	inline bool get__startPlaying_3() const { return ____startPlaying_3; }
	inline bool* get_address_of__startPlaying_3() { return &____startPlaying_3; }
	inline void set__startPlaying_3(bool value)
	{
		____startPlaying_3 = value;
	}

	inline static int32_t get_offset_of__defaultJoystickMaps_4() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____defaultJoystickMaps_4)); }
	inline List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D * get__defaultJoystickMaps_4() const { return ____defaultJoystickMaps_4; }
	inline List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D ** get_address_of__defaultJoystickMaps_4() { return &____defaultJoystickMaps_4; }
	inline void set__defaultJoystickMaps_4(List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D * value)
	{
		____defaultJoystickMaps_4 = value;
		Il2CppCodeGenWriteBarrier((&____defaultJoystickMaps_4), value);
	}

	inline static int32_t get_offset_of__defaultMouseMaps_5() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____defaultMouseMaps_5)); }
	inline List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D * get__defaultMouseMaps_5() const { return ____defaultMouseMaps_5; }
	inline List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D ** get_address_of__defaultMouseMaps_5() { return &____defaultMouseMaps_5; }
	inline void set__defaultMouseMaps_5(List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D * value)
	{
		____defaultMouseMaps_5 = value;
		Il2CppCodeGenWriteBarrier((&____defaultMouseMaps_5), value);
	}

	inline static int32_t get_offset_of__defaultKeyboardMaps_6() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____defaultKeyboardMaps_6)); }
	inline List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D * get__defaultKeyboardMaps_6() const { return ____defaultKeyboardMaps_6; }
	inline List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D ** get_address_of__defaultKeyboardMaps_6() { return &____defaultKeyboardMaps_6; }
	inline void set__defaultKeyboardMaps_6(List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D * value)
	{
		____defaultKeyboardMaps_6 = value;
		Il2CppCodeGenWriteBarrier((&____defaultKeyboardMaps_6), value);
	}

	inline static int32_t get_offset_of__defaultCustomControllerMaps_7() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____defaultCustomControllerMaps_7)); }
	inline List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D * get__defaultCustomControllerMaps_7() const { return ____defaultCustomControllerMaps_7; }
	inline List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D ** get_address_of__defaultCustomControllerMaps_7() { return &____defaultCustomControllerMaps_7; }
	inline void set__defaultCustomControllerMaps_7(List_1_t679B0E7A0C72BCFE4026A8A915205B49644B988D * value)
	{
		____defaultCustomControllerMaps_7 = value;
		Il2CppCodeGenWriteBarrier((&____defaultCustomControllerMaps_7), value);
	}

	inline static int32_t get_offset_of__startingCustomControllers_8() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____startingCustomControllers_8)); }
	inline List_1_tB69BD32C8767F97DE719C7E69179AFBEE7636A2E * get__startingCustomControllers_8() const { return ____startingCustomControllers_8; }
	inline List_1_tB69BD32C8767F97DE719C7E69179AFBEE7636A2E ** get_address_of__startingCustomControllers_8() { return &____startingCustomControllers_8; }
	inline void set__startingCustomControllers_8(List_1_tB69BD32C8767F97DE719C7E69179AFBEE7636A2E * value)
	{
		____startingCustomControllers_8 = value;
		Il2CppCodeGenWriteBarrier((&____startingCustomControllers_8), value);
	}

	inline static int32_t get_offset_of__assignMouseOnStart_9() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____assignMouseOnStart_9)); }
	inline bool get__assignMouseOnStart_9() const { return ____assignMouseOnStart_9; }
	inline bool* get_address_of__assignMouseOnStart_9() { return &____assignMouseOnStart_9; }
	inline void set__assignMouseOnStart_9(bool value)
	{
		____assignMouseOnStart_9 = value;
	}

	inline static int32_t get_offset_of__assignKeyboardOnStart_10() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____assignKeyboardOnStart_10)); }
	inline bool get__assignKeyboardOnStart_10() const { return ____assignKeyboardOnStart_10; }
	inline bool* get_address_of__assignKeyboardOnStart_10() { return &____assignKeyboardOnStart_10; }
	inline void set__assignKeyboardOnStart_10(bool value)
	{
		____assignKeyboardOnStart_10 = value;
	}

	inline static int32_t get_offset_of__excludeFromControllerAutoAssignment_11() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____excludeFromControllerAutoAssignment_11)); }
	inline bool get__excludeFromControllerAutoAssignment_11() const { return ____excludeFromControllerAutoAssignment_11; }
	inline bool* get_address_of__excludeFromControllerAutoAssignment_11() { return &____excludeFromControllerAutoAssignment_11; }
	inline void set__excludeFromControllerAutoAssignment_11(bool value)
	{
		____excludeFromControllerAutoAssignment_11 = value;
	}

	inline static int32_t get_offset_of__controllerMapLayoutManagerSettings_12() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____controllerMapLayoutManagerSettings_12)); }
	inline ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98 * get__controllerMapLayoutManagerSettings_12() const { return ____controllerMapLayoutManagerSettings_12; }
	inline ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98 ** get_address_of__controllerMapLayoutManagerSettings_12() { return &____controllerMapLayoutManagerSettings_12; }
	inline void set__controllerMapLayoutManagerSettings_12(ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98 * value)
	{
		____controllerMapLayoutManagerSettings_12 = value;
		Il2CppCodeGenWriteBarrier((&____controllerMapLayoutManagerSettings_12), value);
	}

	inline static int32_t get_offset_of__controllerMapEnablerSettings_13() { return static_cast<int32_t>(offsetof(Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7, ____controllerMapEnablerSettings_13)); }
	inline ControllerMapEnablerSettings_t34E49435CD1543BBA8B8420276849C0AFE4E02F1 * get__controllerMapEnablerSettings_13() const { return ____controllerMapEnablerSettings_13; }
	inline ControllerMapEnablerSettings_t34E49435CD1543BBA8B8420276849C0AFE4E02F1 ** get_address_of__controllerMapEnablerSettings_13() { return &____controllerMapEnablerSettings_13; }
	inline void set__controllerMapEnablerSettings_13(ControllerMapEnablerSettings_t34E49435CD1543BBA8B8420276849C0AFE4E02F1 * value)
	{
		____controllerMapEnablerSettings_13 = value;
		Il2CppCodeGenWriteBarrier((&____controllerMapEnablerSettings_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_EDITOR_T17BA86B71CF8428935542F1EC81DD218A7EF36B7_H
#ifndef CONTROLLERMAPENABLERSETTINGS_T34E49435CD1543BBA8B8420276849C0AFE4E02F1_H
#define CONTROLLERMAPENABLERSETTINGS_T34E49435CD1543BBA8B8420276849C0AFE4E02F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Player_Editor_ControllerMapEnablerSettings
struct  ControllerMapEnablerSettings_t34E49435CD1543BBA8B8420276849C0AFE4E02F1  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Data.Player_Editor_ControllerMapEnablerSettings::_enabled
	bool ____enabled_0;
	// System.Collections.Generic.List`1<Rewired.Data.Player_Editor_RuleSetMapping> Rewired.Data.Player_Editor_ControllerMapEnablerSettings::_ruleSets
	List_1_tCF7B09047A3AD51F8785C6EB6C912FCD6497810C * ____ruleSets_1;

public:
	inline static int32_t get_offset_of__enabled_0() { return static_cast<int32_t>(offsetof(ControllerMapEnablerSettings_t34E49435CD1543BBA8B8420276849C0AFE4E02F1, ____enabled_0)); }
	inline bool get__enabled_0() const { return ____enabled_0; }
	inline bool* get_address_of__enabled_0() { return &____enabled_0; }
	inline void set__enabled_0(bool value)
	{
		____enabled_0 = value;
	}

	inline static int32_t get_offset_of__ruleSets_1() { return static_cast<int32_t>(offsetof(ControllerMapEnablerSettings_t34E49435CD1543BBA8B8420276849C0AFE4E02F1, ____ruleSets_1)); }
	inline List_1_tCF7B09047A3AD51F8785C6EB6C912FCD6497810C * get__ruleSets_1() const { return ____ruleSets_1; }
	inline List_1_tCF7B09047A3AD51F8785C6EB6C912FCD6497810C ** get_address_of__ruleSets_1() { return &____ruleSets_1; }
	inline void set__ruleSets_1(List_1_tCF7B09047A3AD51F8785C6EB6C912FCD6497810C * value)
	{
		____ruleSets_1 = value;
		Il2CppCodeGenWriteBarrier((&____ruleSets_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAPENABLERSETTINGS_T34E49435CD1543BBA8B8420276849C0AFE4E02F1_H
#ifndef CONTROLLERMAPLAYOUTMANAGERSETTINGS_T19039A51E7FB5D60CE1C64FD41E90ED027EE4C98_H
#define CONTROLLERMAPLAYOUTMANAGERSETTINGS_T19039A51E7FB5D60CE1C64FD41E90ED027EE4C98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Player_Editor_ControllerMapLayoutManagerSettings
struct  ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Data.Player_Editor_ControllerMapLayoutManagerSettings::_enabled
	bool ____enabled_0;
	// System.Boolean Rewired.Data.Player_Editor_ControllerMapLayoutManagerSettings::_loadFromUserDataStore
	bool ____loadFromUserDataStore_1;
	// System.Collections.Generic.List`1<Rewired.Data.Player_Editor_RuleSetMapping> Rewired.Data.Player_Editor_ControllerMapLayoutManagerSettings::_ruleSets
	List_1_tCF7B09047A3AD51F8785C6EB6C912FCD6497810C * ____ruleSets_2;

public:
	inline static int32_t get_offset_of__enabled_0() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98, ____enabled_0)); }
	inline bool get__enabled_0() const { return ____enabled_0; }
	inline bool* get_address_of__enabled_0() { return &____enabled_0; }
	inline void set__enabled_0(bool value)
	{
		____enabled_0 = value;
	}

	inline static int32_t get_offset_of__loadFromUserDataStore_1() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98, ____loadFromUserDataStore_1)); }
	inline bool get__loadFromUserDataStore_1() const { return ____loadFromUserDataStore_1; }
	inline bool* get_address_of__loadFromUserDataStore_1() { return &____loadFromUserDataStore_1; }
	inline void set__loadFromUserDataStore_1(bool value)
	{
		____loadFromUserDataStore_1 = value;
	}

	inline static int32_t get_offset_of__ruleSets_2() { return static_cast<int32_t>(offsetof(ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98, ____ruleSets_2)); }
	inline List_1_tCF7B09047A3AD51F8785C6EB6C912FCD6497810C * get__ruleSets_2() const { return ____ruleSets_2; }
	inline List_1_tCF7B09047A3AD51F8785C6EB6C912FCD6497810C ** get_address_of__ruleSets_2() { return &____ruleSets_2; }
	inline void set__ruleSets_2(List_1_tCF7B09047A3AD51F8785C6EB6C912FCD6497810C * value)
	{
		____ruleSets_2 = value;
		Il2CppCodeGenWriteBarrier((&____ruleSets_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAPLAYOUTMANAGERSETTINGS_T19039A51E7FB5D60CE1C64FD41E90ED027EE4C98_H
#ifndef CREATECONTROLLERINFO_T1174758D9532AC61484D41E38E94D3C7CF852CC4_H
#define CREATECONTROLLERINFO_T1174758D9532AC61484D41E38E94D3C7CF852CC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Player_Editor_CreateControllerInfo
struct  CreateControllerInfo_t1174758D9532AC61484D41E38E94D3C7CF852CC4  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Player_Editor_CreateControllerInfo::_sourceId
	int32_t ____sourceId_0;
	// System.String Rewired.Data.Player_Editor_CreateControllerInfo::_tag
	String_t* ____tag_1;

public:
	inline static int32_t get_offset_of__sourceId_0() { return static_cast<int32_t>(offsetof(CreateControllerInfo_t1174758D9532AC61484D41E38E94D3C7CF852CC4, ____sourceId_0)); }
	inline int32_t get__sourceId_0() const { return ____sourceId_0; }
	inline int32_t* get_address_of__sourceId_0() { return &____sourceId_0; }
	inline void set__sourceId_0(int32_t value)
	{
		____sourceId_0 = value;
	}

	inline static int32_t get_offset_of__tag_1() { return static_cast<int32_t>(offsetof(CreateControllerInfo_t1174758D9532AC61484D41E38E94D3C7CF852CC4, ____tag_1)); }
	inline String_t* get__tag_1() const { return ____tag_1; }
	inline String_t** get_address_of__tag_1() { return &____tag_1; }
	inline void set__tag_1(String_t* value)
	{
		____tag_1 = value;
		Il2CppCodeGenWriteBarrier((&____tag_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECONTROLLERINFO_T1174758D9532AC61484D41E38E94D3C7CF852CC4_H
#ifndef MAPPING_T2A45E365FF4F7D770614C838C4F98C4E197A0D26_H
#define MAPPING_T2A45E365FF4F7D770614C838C4F98C4E197A0D26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Player_Editor_Mapping
struct  Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Data.Player_Editor_Mapping::_enabled
	bool ____enabled_0;
	// System.Int32 Rewired.Data.Player_Editor_Mapping::_categoryId
	int32_t ____categoryId_1;
	// System.Int32 Rewired.Data.Player_Editor_Mapping::_layoutId
	int32_t ____layoutId_2;

public:
	inline static int32_t get_offset_of__enabled_0() { return static_cast<int32_t>(offsetof(Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26, ____enabled_0)); }
	inline bool get__enabled_0() const { return ____enabled_0; }
	inline bool* get_address_of__enabled_0() { return &____enabled_0; }
	inline void set__enabled_0(bool value)
	{
		____enabled_0 = value;
	}

	inline static int32_t get_offset_of__categoryId_1() { return static_cast<int32_t>(offsetof(Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26, ____categoryId_1)); }
	inline int32_t get__categoryId_1() const { return ____categoryId_1; }
	inline int32_t* get_address_of__categoryId_1() { return &____categoryId_1; }
	inline void set__categoryId_1(int32_t value)
	{
		____categoryId_1 = value;
	}

	inline static int32_t get_offset_of__layoutId_2() { return static_cast<int32_t>(offsetof(Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26, ____layoutId_2)); }
	inline int32_t get__layoutId_2() const { return ____layoutId_2; }
	inline int32_t* get_address_of__layoutId_2() { return &____layoutId_2; }
	inline void set__layoutId_2(int32_t value)
	{
		____layoutId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPING_T2A45E365FF4F7D770614C838C4F98C4E197A0D26_H
#ifndef RULESETMAPPING_T0CEF1919C7E8D5DE5660F309FA564049DA4E69F2_H
#define RULESETMAPPING_T0CEF1919C7E8D5DE5660F309FA564049DA4E69F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Player_Editor_RuleSetMapping
struct  RuleSetMapping_t0CEF1919C7E8D5DE5660F309FA564049DA4E69F2  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Data.Player_Editor_RuleSetMapping::_enabled
	bool ____enabled_0;
	// System.Int32 Rewired.Data.Player_Editor_RuleSetMapping::_id
	int32_t ____id_1;

public:
	inline static int32_t get_offset_of__enabled_0() { return static_cast<int32_t>(offsetof(RuleSetMapping_t0CEF1919C7E8D5DE5660F309FA564049DA4E69F2, ____enabled_0)); }
	inline bool get__enabled_0() const { return ____enabled_0; }
	inline bool* get_address_of__enabled_0() { return &____enabled_0; }
	inline void set__enabled_0(bool value)
	{
		____enabled_0 = value;
	}

	inline static int32_t get_offset_of__id_1() { return static_cast<int32_t>(offsetof(RuleSetMapping_t0CEF1919C7E8D5DE5660F309FA564049DA4E69F2, ____id_1)); }
	inline int32_t get__id_1() const { return ____id_1; }
	inline int32_t* get_address_of__id_1() { return &____id_1; }
	inline void set__id_1(int32_t value)
	{
		____id_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RULESETMAPPING_T0CEF1919C7E8D5DE5660F309FA564049DA4E69F2_H
#ifndef USERDATA_T23CF13BC0883AADA4A3457583033292E49A77D48_H
#define USERDATA_T23CF13BC0883AADA4A3457583033292E49A77D48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData
struct  UserData_t23CF13BC0883AADA4A3457583033292E49A77D48  : public RuntimeObject
{
public:
	// Rewired.Data.ConfigVars Rewired.Data.UserData::configVars
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * ___configVars_0;
	// System.Collections.Generic.List`1<Rewired.Data.Player_Editor> Rewired.Data.UserData::players
	List_1_t5A03789D060E8ADDB4C9E0E05DC5BFA7E2530390 * ___players_1;
	// System.Collections.Generic.List`1<Rewired.InputAction> Rewired.Data.UserData::actions
	List_1_tED09A8A751202103D07AF4B1286EA07FBF9CF702 * ___actions_2;
	// System.Collections.Generic.List`1<Rewired.InputCategory> Rewired.Data.UserData::actionCategories
	List_1_tF4A678B2CE5927472957C1C908BCCAE515A36D8E * ___actionCategories_3;
	// Rewired.Data.Mapping.ActionCategoryMap Rewired.Data.UserData::actionCategoryMap
	ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F * ___actionCategoryMap_4;
	// System.Collections.Generic.List`1<Rewired.InputBehavior> Rewired.Data.UserData::inputBehaviors
	List_1_t00493746A9C09D2A4E213DD073FD95F9A4FAA144 * ___inputBehaviors_5;
	// System.Collections.Generic.List`1<Rewired.InputMapCategory> Rewired.Data.UserData::mapCategories
	List_1_tF6EC72522F33236C9C1694F690FF7AD3EBEB8FB9 * ___mapCategories_6;
	// System.Collections.Generic.List`1<Rewired.InputLayout> Rewired.Data.UserData::joystickLayouts
	List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * ___joystickLayouts_7;
	// System.Collections.Generic.List`1<Rewired.InputLayout> Rewired.Data.UserData::keyboardLayouts
	List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * ___keyboardLayouts_8;
	// System.Collections.Generic.List`1<Rewired.InputLayout> Rewired.Data.UserData::mouseLayouts
	List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * ___mouseLayouts_9;
	// System.Collections.Generic.List`1<Rewired.InputLayout> Rewired.Data.UserData::customControllerLayouts
	List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * ___customControllerLayouts_10;
	// System.Collections.Generic.List`1<Rewired.Data.Mapping.ControllerMap_Editor> Rewired.Data.UserData::joystickMaps
	List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C * ___joystickMaps_11;
	// System.Collections.Generic.List`1<Rewired.Data.Mapping.ControllerMap_Editor> Rewired.Data.UserData::keyboardMaps
	List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C * ___keyboardMaps_12;
	// System.Collections.Generic.List`1<Rewired.Data.Mapping.ControllerMap_Editor> Rewired.Data.UserData::mouseMaps
	List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C * ___mouseMaps_13;
	// System.Collections.Generic.List`1<Rewired.Data.Mapping.ControllerMap_Editor> Rewired.Data.UserData::customControllerMaps
	List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C * ___customControllerMaps_14;
	// System.Collections.Generic.List`1<Rewired.Data.CustomController_Editor> Rewired.Data.UserData::customControllers
	List_1_t54DD5AA01FFBBAAF392103CFEC81ED0E4765E320 * ___customControllers_15;
	// System.Collections.Generic.List`1<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor> Rewired.Data.UserData::controllerMapLayoutManagerRuleSets
	List_1_t458243618BB2E4CE738A13EB434D2D7F4D7B0C8B * ___controllerMapLayoutManagerRuleSets_16;
	// System.Collections.Generic.List`1<Rewired.Data.ControllerMapEnabler_RuleSet_Editor> Rewired.Data.UserData::controllerMapEnablerRuleSets
	List_1_tB43994C449405FA051845A34A47EB5A1FD88FC1D * ___controllerMapEnablerRuleSets_17;
	// System.Int32 Rewired.Data.UserData::playerIdCounter
	int32_t ___playerIdCounter_18;
	// System.Int32 Rewired.Data.UserData::actionIdCounter
	int32_t ___actionIdCounter_19;
	// System.Int32 Rewired.Data.UserData::actionCategoryIdCounter
	int32_t ___actionCategoryIdCounter_20;
	// System.Int32 Rewired.Data.UserData::inputBehaviorIdCounter
	int32_t ___inputBehaviorIdCounter_21;
	// System.Int32 Rewired.Data.UserData::mapCategoryIdCounter
	int32_t ___mapCategoryIdCounter_22;
	// System.Int32 Rewired.Data.UserData::joystickLayoutIdCounter
	int32_t ___joystickLayoutIdCounter_23;
	// System.Int32 Rewired.Data.UserData::keyboardLayoutIdCounter
	int32_t ___keyboardLayoutIdCounter_24;
	// System.Int32 Rewired.Data.UserData::mouseLayoutIdCounter
	int32_t ___mouseLayoutIdCounter_25;
	// System.Int32 Rewired.Data.UserData::customControllerLayoutIdCounter
	int32_t ___customControllerLayoutIdCounter_26;
	// System.Int32 Rewired.Data.UserData::joystickMapIdCounter
	int32_t ___joystickMapIdCounter_27;
	// System.Int32 Rewired.Data.UserData::keyboardMapIdCounter
	int32_t ___keyboardMapIdCounter_28;
	// System.Int32 Rewired.Data.UserData::mouseMapIdCounter
	int32_t ___mouseMapIdCounter_29;
	// System.Int32 Rewired.Data.UserData::customControllerMapIdCounter
	int32_t ___customControllerMapIdCounter_30;
	// System.Int32 Rewired.Data.UserData::customControllerIdCounter
	int32_t ___customControllerIdCounter_31;
	// System.Int32 Rewired.Data.UserData::controllerMapLayoutManagerSetIdCounter
	int32_t ___controllerMapLayoutManagerSetIdCounter_32;
	// System.Int32 Rewired.Data.UserData::controllerMapEnablerSetIdCounter
	int32_t ___controllerMapEnablerSetIdCounter_33;
	// System.Func`2<System.Int32,System.Boolean> Rewired.Data.UserData::containsActionDelegate
	Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * ___containsActionDelegate_34;
	// System.Collections.Generic.IList`1<Rewired.Data.Player_Editor> Rewired.Data.UserData::<Players_readOnly>k__BackingField
	RuntimeObject* ___U3CPlayers_readOnlyU3Ek__BackingField_35;
	// System.Collections.Generic.IList`1<Rewired.InputAction> Rewired.Data.UserData::<Actions_readOnly>k__BackingField
	RuntimeObject* ___U3CActions_readOnlyU3Ek__BackingField_36;
	// System.Collections.Generic.IList`1<Rewired.InputCategory> Rewired.Data.UserData::<ActionCategories_readOnly>k__BackingField
	RuntimeObject* ___U3CActionCategories_readOnlyU3Ek__BackingField_37;
	// System.Collections.Generic.IList`1<Rewired.InputBehavior> Rewired.Data.UserData::<InputBehaviors_readOnly>k__BackingField
	RuntimeObject* ___U3CInputBehaviors_readOnlyU3Ek__BackingField_38;
	// System.Collections.Generic.IList`1<Rewired.InputMapCategory> Rewired.Data.UserData::<MapCategories_readOnly>k__BackingField
	RuntimeObject* ___U3CMapCategories_readOnlyU3Ek__BackingField_39;
	// System.Collections.Generic.IList`1<Rewired.InputLayout> Rewired.Data.UserData::<JoystickLayouts_readOnly>k__BackingField
	RuntimeObject* ___U3CJoystickLayouts_readOnlyU3Ek__BackingField_40;
	// System.Collections.Generic.IList`1<Rewired.InputLayout> Rewired.Data.UserData::<KeyboardLayouts_readOnly>k__BackingField
	RuntimeObject* ___U3CKeyboardLayouts_readOnlyU3Ek__BackingField_41;
	// System.Collections.Generic.IList`1<Rewired.InputLayout> Rewired.Data.UserData::<MouseLayouts_readOnly>k__BackingField
	RuntimeObject* ___U3CMouseLayouts_readOnlyU3Ek__BackingField_42;
	// System.Collections.Generic.IList`1<Rewired.InputLayout> Rewired.Data.UserData::<CustomControllerLayouts_readOnly>k__BackingField
	RuntimeObject* ___U3CCustomControllerLayouts_readOnlyU3Ek__BackingField_43;
	// System.Collections.Generic.IList`1<Rewired.Data.Mapping.ControllerMap_Editor> Rewired.Data.UserData::<JoystickMaps_readOnly>k__BackingField
	RuntimeObject* ___U3CJoystickMaps_readOnlyU3Ek__BackingField_44;
	// System.Collections.Generic.IList`1<Rewired.Data.Mapping.ControllerMap_Editor> Rewired.Data.UserData::<KeyboardMaps_readOnly>k__BackingField
	RuntimeObject* ___U3CKeyboardMaps_readOnlyU3Ek__BackingField_45;
	// System.Collections.Generic.IList`1<Rewired.Data.Mapping.ControllerMap_Editor> Rewired.Data.UserData::<MouseMaps_readOnly>k__BackingField
	RuntimeObject* ___U3CMouseMaps_readOnlyU3Ek__BackingField_46;
	// System.Collections.Generic.IList`1<Rewired.Data.Mapping.ControllerMap_Editor> Rewired.Data.UserData::<CustomControllerMaps_readOnly>k__BackingField
	RuntimeObject* ___U3CCustomControllerMaps_readOnlyU3Ek__BackingField_47;
	// System.Collections.Generic.IList`1<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor> Rewired.Data.UserData::<ControllerMapLayoutManagerRuleSets_readOnly>k__BackingField
	RuntimeObject* ___U3CControllerMapLayoutManagerRuleSets_readOnlyU3Ek__BackingField_48;
	// System.Collections.Generic.IList`1<Rewired.Data.ControllerMapEnabler_RuleSet_Editor> Rewired.Data.UserData::<ControllerMapEnablerRuleSets_readOnly>k__BackingField
	RuntimeObject* ___U3CControllerMapEnablerRuleSets_readOnlyU3Ek__BackingField_49;

public:
	inline static int32_t get_offset_of_configVars_0() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___configVars_0)); }
	inline ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * get_configVars_0() const { return ___configVars_0; }
	inline ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 ** get_address_of_configVars_0() { return &___configVars_0; }
	inline void set_configVars_0(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * value)
	{
		___configVars_0 = value;
		Il2CppCodeGenWriteBarrier((&___configVars_0), value);
	}

	inline static int32_t get_offset_of_players_1() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___players_1)); }
	inline List_1_t5A03789D060E8ADDB4C9E0E05DC5BFA7E2530390 * get_players_1() const { return ___players_1; }
	inline List_1_t5A03789D060E8ADDB4C9E0E05DC5BFA7E2530390 ** get_address_of_players_1() { return &___players_1; }
	inline void set_players_1(List_1_t5A03789D060E8ADDB4C9E0E05DC5BFA7E2530390 * value)
	{
		___players_1 = value;
		Il2CppCodeGenWriteBarrier((&___players_1), value);
	}

	inline static int32_t get_offset_of_actions_2() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___actions_2)); }
	inline List_1_tED09A8A751202103D07AF4B1286EA07FBF9CF702 * get_actions_2() const { return ___actions_2; }
	inline List_1_tED09A8A751202103D07AF4B1286EA07FBF9CF702 ** get_address_of_actions_2() { return &___actions_2; }
	inline void set_actions_2(List_1_tED09A8A751202103D07AF4B1286EA07FBF9CF702 * value)
	{
		___actions_2 = value;
		Il2CppCodeGenWriteBarrier((&___actions_2), value);
	}

	inline static int32_t get_offset_of_actionCategories_3() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___actionCategories_3)); }
	inline List_1_tF4A678B2CE5927472957C1C908BCCAE515A36D8E * get_actionCategories_3() const { return ___actionCategories_3; }
	inline List_1_tF4A678B2CE5927472957C1C908BCCAE515A36D8E ** get_address_of_actionCategories_3() { return &___actionCategories_3; }
	inline void set_actionCategories_3(List_1_tF4A678B2CE5927472957C1C908BCCAE515A36D8E * value)
	{
		___actionCategories_3 = value;
		Il2CppCodeGenWriteBarrier((&___actionCategories_3), value);
	}

	inline static int32_t get_offset_of_actionCategoryMap_4() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___actionCategoryMap_4)); }
	inline ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F * get_actionCategoryMap_4() const { return ___actionCategoryMap_4; }
	inline ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F ** get_address_of_actionCategoryMap_4() { return &___actionCategoryMap_4; }
	inline void set_actionCategoryMap_4(ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F * value)
	{
		___actionCategoryMap_4 = value;
		Il2CppCodeGenWriteBarrier((&___actionCategoryMap_4), value);
	}

	inline static int32_t get_offset_of_inputBehaviors_5() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___inputBehaviors_5)); }
	inline List_1_t00493746A9C09D2A4E213DD073FD95F9A4FAA144 * get_inputBehaviors_5() const { return ___inputBehaviors_5; }
	inline List_1_t00493746A9C09D2A4E213DD073FD95F9A4FAA144 ** get_address_of_inputBehaviors_5() { return &___inputBehaviors_5; }
	inline void set_inputBehaviors_5(List_1_t00493746A9C09D2A4E213DD073FD95F9A4FAA144 * value)
	{
		___inputBehaviors_5 = value;
		Il2CppCodeGenWriteBarrier((&___inputBehaviors_5), value);
	}

	inline static int32_t get_offset_of_mapCategories_6() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___mapCategories_6)); }
	inline List_1_tF6EC72522F33236C9C1694F690FF7AD3EBEB8FB9 * get_mapCategories_6() const { return ___mapCategories_6; }
	inline List_1_tF6EC72522F33236C9C1694F690FF7AD3EBEB8FB9 ** get_address_of_mapCategories_6() { return &___mapCategories_6; }
	inline void set_mapCategories_6(List_1_tF6EC72522F33236C9C1694F690FF7AD3EBEB8FB9 * value)
	{
		___mapCategories_6 = value;
		Il2CppCodeGenWriteBarrier((&___mapCategories_6), value);
	}

	inline static int32_t get_offset_of_joystickLayouts_7() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___joystickLayouts_7)); }
	inline List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * get_joystickLayouts_7() const { return ___joystickLayouts_7; }
	inline List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 ** get_address_of_joystickLayouts_7() { return &___joystickLayouts_7; }
	inline void set_joystickLayouts_7(List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * value)
	{
		___joystickLayouts_7 = value;
		Il2CppCodeGenWriteBarrier((&___joystickLayouts_7), value);
	}

	inline static int32_t get_offset_of_keyboardLayouts_8() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___keyboardLayouts_8)); }
	inline List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * get_keyboardLayouts_8() const { return ___keyboardLayouts_8; }
	inline List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 ** get_address_of_keyboardLayouts_8() { return &___keyboardLayouts_8; }
	inline void set_keyboardLayouts_8(List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * value)
	{
		___keyboardLayouts_8 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardLayouts_8), value);
	}

	inline static int32_t get_offset_of_mouseLayouts_9() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___mouseLayouts_9)); }
	inline List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * get_mouseLayouts_9() const { return ___mouseLayouts_9; }
	inline List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 ** get_address_of_mouseLayouts_9() { return &___mouseLayouts_9; }
	inline void set_mouseLayouts_9(List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * value)
	{
		___mouseLayouts_9 = value;
		Il2CppCodeGenWriteBarrier((&___mouseLayouts_9), value);
	}

	inline static int32_t get_offset_of_customControllerLayouts_10() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___customControllerLayouts_10)); }
	inline List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * get_customControllerLayouts_10() const { return ___customControllerLayouts_10; }
	inline List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 ** get_address_of_customControllerLayouts_10() { return &___customControllerLayouts_10; }
	inline void set_customControllerLayouts_10(List_1_t090E32AFF8969861BBD777C62EE88E3921E4F8B2 * value)
	{
		___customControllerLayouts_10 = value;
		Il2CppCodeGenWriteBarrier((&___customControllerLayouts_10), value);
	}

	inline static int32_t get_offset_of_joystickMaps_11() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___joystickMaps_11)); }
	inline List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C * get_joystickMaps_11() const { return ___joystickMaps_11; }
	inline List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C ** get_address_of_joystickMaps_11() { return &___joystickMaps_11; }
	inline void set_joystickMaps_11(List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C * value)
	{
		___joystickMaps_11 = value;
		Il2CppCodeGenWriteBarrier((&___joystickMaps_11), value);
	}

	inline static int32_t get_offset_of_keyboardMaps_12() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___keyboardMaps_12)); }
	inline List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C * get_keyboardMaps_12() const { return ___keyboardMaps_12; }
	inline List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C ** get_address_of_keyboardMaps_12() { return &___keyboardMaps_12; }
	inline void set_keyboardMaps_12(List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C * value)
	{
		___keyboardMaps_12 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardMaps_12), value);
	}

	inline static int32_t get_offset_of_mouseMaps_13() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___mouseMaps_13)); }
	inline List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C * get_mouseMaps_13() const { return ___mouseMaps_13; }
	inline List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C ** get_address_of_mouseMaps_13() { return &___mouseMaps_13; }
	inline void set_mouseMaps_13(List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C * value)
	{
		___mouseMaps_13 = value;
		Il2CppCodeGenWriteBarrier((&___mouseMaps_13), value);
	}

	inline static int32_t get_offset_of_customControllerMaps_14() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___customControllerMaps_14)); }
	inline List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C * get_customControllerMaps_14() const { return ___customControllerMaps_14; }
	inline List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C ** get_address_of_customControllerMaps_14() { return &___customControllerMaps_14; }
	inline void set_customControllerMaps_14(List_1_t9F85C67249EA6270D47FEDC0642E6E82CC08001C * value)
	{
		___customControllerMaps_14 = value;
		Il2CppCodeGenWriteBarrier((&___customControllerMaps_14), value);
	}

	inline static int32_t get_offset_of_customControllers_15() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___customControllers_15)); }
	inline List_1_t54DD5AA01FFBBAAF392103CFEC81ED0E4765E320 * get_customControllers_15() const { return ___customControllers_15; }
	inline List_1_t54DD5AA01FFBBAAF392103CFEC81ED0E4765E320 ** get_address_of_customControllers_15() { return &___customControllers_15; }
	inline void set_customControllers_15(List_1_t54DD5AA01FFBBAAF392103CFEC81ED0E4765E320 * value)
	{
		___customControllers_15 = value;
		Il2CppCodeGenWriteBarrier((&___customControllers_15), value);
	}

	inline static int32_t get_offset_of_controllerMapLayoutManagerRuleSets_16() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___controllerMapLayoutManagerRuleSets_16)); }
	inline List_1_t458243618BB2E4CE738A13EB434D2D7F4D7B0C8B * get_controllerMapLayoutManagerRuleSets_16() const { return ___controllerMapLayoutManagerRuleSets_16; }
	inline List_1_t458243618BB2E4CE738A13EB434D2D7F4D7B0C8B ** get_address_of_controllerMapLayoutManagerRuleSets_16() { return &___controllerMapLayoutManagerRuleSets_16; }
	inline void set_controllerMapLayoutManagerRuleSets_16(List_1_t458243618BB2E4CE738A13EB434D2D7F4D7B0C8B * value)
	{
		___controllerMapLayoutManagerRuleSets_16 = value;
		Il2CppCodeGenWriteBarrier((&___controllerMapLayoutManagerRuleSets_16), value);
	}

	inline static int32_t get_offset_of_controllerMapEnablerRuleSets_17() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___controllerMapEnablerRuleSets_17)); }
	inline List_1_tB43994C449405FA051845A34A47EB5A1FD88FC1D * get_controllerMapEnablerRuleSets_17() const { return ___controllerMapEnablerRuleSets_17; }
	inline List_1_tB43994C449405FA051845A34A47EB5A1FD88FC1D ** get_address_of_controllerMapEnablerRuleSets_17() { return &___controllerMapEnablerRuleSets_17; }
	inline void set_controllerMapEnablerRuleSets_17(List_1_tB43994C449405FA051845A34A47EB5A1FD88FC1D * value)
	{
		___controllerMapEnablerRuleSets_17 = value;
		Il2CppCodeGenWriteBarrier((&___controllerMapEnablerRuleSets_17), value);
	}

	inline static int32_t get_offset_of_playerIdCounter_18() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___playerIdCounter_18)); }
	inline int32_t get_playerIdCounter_18() const { return ___playerIdCounter_18; }
	inline int32_t* get_address_of_playerIdCounter_18() { return &___playerIdCounter_18; }
	inline void set_playerIdCounter_18(int32_t value)
	{
		___playerIdCounter_18 = value;
	}

	inline static int32_t get_offset_of_actionIdCounter_19() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___actionIdCounter_19)); }
	inline int32_t get_actionIdCounter_19() const { return ___actionIdCounter_19; }
	inline int32_t* get_address_of_actionIdCounter_19() { return &___actionIdCounter_19; }
	inline void set_actionIdCounter_19(int32_t value)
	{
		___actionIdCounter_19 = value;
	}

	inline static int32_t get_offset_of_actionCategoryIdCounter_20() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___actionCategoryIdCounter_20)); }
	inline int32_t get_actionCategoryIdCounter_20() const { return ___actionCategoryIdCounter_20; }
	inline int32_t* get_address_of_actionCategoryIdCounter_20() { return &___actionCategoryIdCounter_20; }
	inline void set_actionCategoryIdCounter_20(int32_t value)
	{
		___actionCategoryIdCounter_20 = value;
	}

	inline static int32_t get_offset_of_inputBehaviorIdCounter_21() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___inputBehaviorIdCounter_21)); }
	inline int32_t get_inputBehaviorIdCounter_21() const { return ___inputBehaviorIdCounter_21; }
	inline int32_t* get_address_of_inputBehaviorIdCounter_21() { return &___inputBehaviorIdCounter_21; }
	inline void set_inputBehaviorIdCounter_21(int32_t value)
	{
		___inputBehaviorIdCounter_21 = value;
	}

	inline static int32_t get_offset_of_mapCategoryIdCounter_22() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___mapCategoryIdCounter_22)); }
	inline int32_t get_mapCategoryIdCounter_22() const { return ___mapCategoryIdCounter_22; }
	inline int32_t* get_address_of_mapCategoryIdCounter_22() { return &___mapCategoryIdCounter_22; }
	inline void set_mapCategoryIdCounter_22(int32_t value)
	{
		___mapCategoryIdCounter_22 = value;
	}

	inline static int32_t get_offset_of_joystickLayoutIdCounter_23() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___joystickLayoutIdCounter_23)); }
	inline int32_t get_joystickLayoutIdCounter_23() const { return ___joystickLayoutIdCounter_23; }
	inline int32_t* get_address_of_joystickLayoutIdCounter_23() { return &___joystickLayoutIdCounter_23; }
	inline void set_joystickLayoutIdCounter_23(int32_t value)
	{
		___joystickLayoutIdCounter_23 = value;
	}

	inline static int32_t get_offset_of_keyboardLayoutIdCounter_24() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___keyboardLayoutIdCounter_24)); }
	inline int32_t get_keyboardLayoutIdCounter_24() const { return ___keyboardLayoutIdCounter_24; }
	inline int32_t* get_address_of_keyboardLayoutIdCounter_24() { return &___keyboardLayoutIdCounter_24; }
	inline void set_keyboardLayoutIdCounter_24(int32_t value)
	{
		___keyboardLayoutIdCounter_24 = value;
	}

	inline static int32_t get_offset_of_mouseLayoutIdCounter_25() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___mouseLayoutIdCounter_25)); }
	inline int32_t get_mouseLayoutIdCounter_25() const { return ___mouseLayoutIdCounter_25; }
	inline int32_t* get_address_of_mouseLayoutIdCounter_25() { return &___mouseLayoutIdCounter_25; }
	inline void set_mouseLayoutIdCounter_25(int32_t value)
	{
		___mouseLayoutIdCounter_25 = value;
	}

	inline static int32_t get_offset_of_customControllerLayoutIdCounter_26() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___customControllerLayoutIdCounter_26)); }
	inline int32_t get_customControllerLayoutIdCounter_26() const { return ___customControllerLayoutIdCounter_26; }
	inline int32_t* get_address_of_customControllerLayoutIdCounter_26() { return &___customControllerLayoutIdCounter_26; }
	inline void set_customControllerLayoutIdCounter_26(int32_t value)
	{
		___customControllerLayoutIdCounter_26 = value;
	}

	inline static int32_t get_offset_of_joystickMapIdCounter_27() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___joystickMapIdCounter_27)); }
	inline int32_t get_joystickMapIdCounter_27() const { return ___joystickMapIdCounter_27; }
	inline int32_t* get_address_of_joystickMapIdCounter_27() { return &___joystickMapIdCounter_27; }
	inline void set_joystickMapIdCounter_27(int32_t value)
	{
		___joystickMapIdCounter_27 = value;
	}

	inline static int32_t get_offset_of_keyboardMapIdCounter_28() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___keyboardMapIdCounter_28)); }
	inline int32_t get_keyboardMapIdCounter_28() const { return ___keyboardMapIdCounter_28; }
	inline int32_t* get_address_of_keyboardMapIdCounter_28() { return &___keyboardMapIdCounter_28; }
	inline void set_keyboardMapIdCounter_28(int32_t value)
	{
		___keyboardMapIdCounter_28 = value;
	}

	inline static int32_t get_offset_of_mouseMapIdCounter_29() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___mouseMapIdCounter_29)); }
	inline int32_t get_mouseMapIdCounter_29() const { return ___mouseMapIdCounter_29; }
	inline int32_t* get_address_of_mouseMapIdCounter_29() { return &___mouseMapIdCounter_29; }
	inline void set_mouseMapIdCounter_29(int32_t value)
	{
		___mouseMapIdCounter_29 = value;
	}

	inline static int32_t get_offset_of_customControllerMapIdCounter_30() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___customControllerMapIdCounter_30)); }
	inline int32_t get_customControllerMapIdCounter_30() const { return ___customControllerMapIdCounter_30; }
	inline int32_t* get_address_of_customControllerMapIdCounter_30() { return &___customControllerMapIdCounter_30; }
	inline void set_customControllerMapIdCounter_30(int32_t value)
	{
		___customControllerMapIdCounter_30 = value;
	}

	inline static int32_t get_offset_of_customControllerIdCounter_31() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___customControllerIdCounter_31)); }
	inline int32_t get_customControllerIdCounter_31() const { return ___customControllerIdCounter_31; }
	inline int32_t* get_address_of_customControllerIdCounter_31() { return &___customControllerIdCounter_31; }
	inline void set_customControllerIdCounter_31(int32_t value)
	{
		___customControllerIdCounter_31 = value;
	}

	inline static int32_t get_offset_of_controllerMapLayoutManagerSetIdCounter_32() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___controllerMapLayoutManagerSetIdCounter_32)); }
	inline int32_t get_controllerMapLayoutManagerSetIdCounter_32() const { return ___controllerMapLayoutManagerSetIdCounter_32; }
	inline int32_t* get_address_of_controllerMapLayoutManagerSetIdCounter_32() { return &___controllerMapLayoutManagerSetIdCounter_32; }
	inline void set_controllerMapLayoutManagerSetIdCounter_32(int32_t value)
	{
		___controllerMapLayoutManagerSetIdCounter_32 = value;
	}

	inline static int32_t get_offset_of_controllerMapEnablerSetIdCounter_33() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___controllerMapEnablerSetIdCounter_33)); }
	inline int32_t get_controllerMapEnablerSetIdCounter_33() const { return ___controllerMapEnablerSetIdCounter_33; }
	inline int32_t* get_address_of_controllerMapEnablerSetIdCounter_33() { return &___controllerMapEnablerSetIdCounter_33; }
	inline void set_controllerMapEnablerSetIdCounter_33(int32_t value)
	{
		___controllerMapEnablerSetIdCounter_33 = value;
	}

	inline static int32_t get_offset_of_containsActionDelegate_34() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___containsActionDelegate_34)); }
	inline Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * get_containsActionDelegate_34() const { return ___containsActionDelegate_34; }
	inline Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 ** get_address_of_containsActionDelegate_34() { return &___containsActionDelegate_34; }
	inline void set_containsActionDelegate_34(Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * value)
	{
		___containsActionDelegate_34 = value;
		Il2CppCodeGenWriteBarrier((&___containsActionDelegate_34), value);
	}

	inline static int32_t get_offset_of_U3CPlayers_readOnlyU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CPlayers_readOnlyU3Ek__BackingField_35)); }
	inline RuntimeObject* get_U3CPlayers_readOnlyU3Ek__BackingField_35() const { return ___U3CPlayers_readOnlyU3Ek__BackingField_35; }
	inline RuntimeObject** get_address_of_U3CPlayers_readOnlyU3Ek__BackingField_35() { return &___U3CPlayers_readOnlyU3Ek__BackingField_35; }
	inline void set_U3CPlayers_readOnlyU3Ek__BackingField_35(RuntimeObject* value)
	{
		___U3CPlayers_readOnlyU3Ek__BackingField_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlayers_readOnlyU3Ek__BackingField_35), value);
	}

	inline static int32_t get_offset_of_U3CActions_readOnlyU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CActions_readOnlyU3Ek__BackingField_36)); }
	inline RuntimeObject* get_U3CActions_readOnlyU3Ek__BackingField_36() const { return ___U3CActions_readOnlyU3Ek__BackingField_36; }
	inline RuntimeObject** get_address_of_U3CActions_readOnlyU3Ek__BackingField_36() { return &___U3CActions_readOnlyU3Ek__BackingField_36; }
	inline void set_U3CActions_readOnlyU3Ek__BackingField_36(RuntimeObject* value)
	{
		___U3CActions_readOnlyU3Ek__BackingField_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActions_readOnlyU3Ek__BackingField_36), value);
	}

	inline static int32_t get_offset_of_U3CActionCategories_readOnlyU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CActionCategories_readOnlyU3Ek__BackingField_37)); }
	inline RuntimeObject* get_U3CActionCategories_readOnlyU3Ek__BackingField_37() const { return ___U3CActionCategories_readOnlyU3Ek__BackingField_37; }
	inline RuntimeObject** get_address_of_U3CActionCategories_readOnlyU3Ek__BackingField_37() { return &___U3CActionCategories_readOnlyU3Ek__BackingField_37; }
	inline void set_U3CActionCategories_readOnlyU3Ek__BackingField_37(RuntimeObject* value)
	{
		___U3CActionCategories_readOnlyU3Ek__BackingField_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActionCategories_readOnlyU3Ek__BackingField_37), value);
	}

	inline static int32_t get_offset_of_U3CInputBehaviors_readOnlyU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CInputBehaviors_readOnlyU3Ek__BackingField_38)); }
	inline RuntimeObject* get_U3CInputBehaviors_readOnlyU3Ek__BackingField_38() const { return ___U3CInputBehaviors_readOnlyU3Ek__BackingField_38; }
	inline RuntimeObject** get_address_of_U3CInputBehaviors_readOnlyU3Ek__BackingField_38() { return &___U3CInputBehaviors_readOnlyU3Ek__BackingField_38; }
	inline void set_U3CInputBehaviors_readOnlyU3Ek__BackingField_38(RuntimeObject* value)
	{
		___U3CInputBehaviors_readOnlyU3Ek__BackingField_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInputBehaviors_readOnlyU3Ek__BackingField_38), value);
	}

	inline static int32_t get_offset_of_U3CMapCategories_readOnlyU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CMapCategories_readOnlyU3Ek__BackingField_39)); }
	inline RuntimeObject* get_U3CMapCategories_readOnlyU3Ek__BackingField_39() const { return ___U3CMapCategories_readOnlyU3Ek__BackingField_39; }
	inline RuntimeObject** get_address_of_U3CMapCategories_readOnlyU3Ek__BackingField_39() { return &___U3CMapCategories_readOnlyU3Ek__BackingField_39; }
	inline void set_U3CMapCategories_readOnlyU3Ek__BackingField_39(RuntimeObject* value)
	{
		___U3CMapCategories_readOnlyU3Ek__BackingField_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMapCategories_readOnlyU3Ek__BackingField_39), value);
	}

	inline static int32_t get_offset_of_U3CJoystickLayouts_readOnlyU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CJoystickLayouts_readOnlyU3Ek__BackingField_40)); }
	inline RuntimeObject* get_U3CJoystickLayouts_readOnlyU3Ek__BackingField_40() const { return ___U3CJoystickLayouts_readOnlyU3Ek__BackingField_40; }
	inline RuntimeObject** get_address_of_U3CJoystickLayouts_readOnlyU3Ek__BackingField_40() { return &___U3CJoystickLayouts_readOnlyU3Ek__BackingField_40; }
	inline void set_U3CJoystickLayouts_readOnlyU3Ek__BackingField_40(RuntimeObject* value)
	{
		___U3CJoystickLayouts_readOnlyU3Ek__BackingField_40 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJoystickLayouts_readOnlyU3Ek__BackingField_40), value);
	}

	inline static int32_t get_offset_of_U3CKeyboardLayouts_readOnlyU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CKeyboardLayouts_readOnlyU3Ek__BackingField_41)); }
	inline RuntimeObject* get_U3CKeyboardLayouts_readOnlyU3Ek__BackingField_41() const { return ___U3CKeyboardLayouts_readOnlyU3Ek__BackingField_41; }
	inline RuntimeObject** get_address_of_U3CKeyboardLayouts_readOnlyU3Ek__BackingField_41() { return &___U3CKeyboardLayouts_readOnlyU3Ek__BackingField_41; }
	inline void set_U3CKeyboardLayouts_readOnlyU3Ek__BackingField_41(RuntimeObject* value)
	{
		___U3CKeyboardLayouts_readOnlyU3Ek__BackingField_41 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyboardLayouts_readOnlyU3Ek__BackingField_41), value);
	}

	inline static int32_t get_offset_of_U3CMouseLayouts_readOnlyU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CMouseLayouts_readOnlyU3Ek__BackingField_42)); }
	inline RuntimeObject* get_U3CMouseLayouts_readOnlyU3Ek__BackingField_42() const { return ___U3CMouseLayouts_readOnlyU3Ek__BackingField_42; }
	inline RuntimeObject** get_address_of_U3CMouseLayouts_readOnlyU3Ek__BackingField_42() { return &___U3CMouseLayouts_readOnlyU3Ek__BackingField_42; }
	inline void set_U3CMouseLayouts_readOnlyU3Ek__BackingField_42(RuntimeObject* value)
	{
		___U3CMouseLayouts_readOnlyU3Ek__BackingField_42 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseLayouts_readOnlyU3Ek__BackingField_42), value);
	}

	inline static int32_t get_offset_of_U3CCustomControllerLayouts_readOnlyU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CCustomControllerLayouts_readOnlyU3Ek__BackingField_43)); }
	inline RuntimeObject* get_U3CCustomControllerLayouts_readOnlyU3Ek__BackingField_43() const { return ___U3CCustomControllerLayouts_readOnlyU3Ek__BackingField_43; }
	inline RuntimeObject** get_address_of_U3CCustomControllerLayouts_readOnlyU3Ek__BackingField_43() { return &___U3CCustomControllerLayouts_readOnlyU3Ek__BackingField_43; }
	inline void set_U3CCustomControllerLayouts_readOnlyU3Ek__BackingField_43(RuntimeObject* value)
	{
		___U3CCustomControllerLayouts_readOnlyU3Ek__BackingField_43 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomControllerLayouts_readOnlyU3Ek__BackingField_43), value);
	}

	inline static int32_t get_offset_of_U3CJoystickMaps_readOnlyU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CJoystickMaps_readOnlyU3Ek__BackingField_44)); }
	inline RuntimeObject* get_U3CJoystickMaps_readOnlyU3Ek__BackingField_44() const { return ___U3CJoystickMaps_readOnlyU3Ek__BackingField_44; }
	inline RuntimeObject** get_address_of_U3CJoystickMaps_readOnlyU3Ek__BackingField_44() { return &___U3CJoystickMaps_readOnlyU3Ek__BackingField_44; }
	inline void set_U3CJoystickMaps_readOnlyU3Ek__BackingField_44(RuntimeObject* value)
	{
		___U3CJoystickMaps_readOnlyU3Ek__BackingField_44 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJoystickMaps_readOnlyU3Ek__BackingField_44), value);
	}

	inline static int32_t get_offset_of_U3CKeyboardMaps_readOnlyU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CKeyboardMaps_readOnlyU3Ek__BackingField_45)); }
	inline RuntimeObject* get_U3CKeyboardMaps_readOnlyU3Ek__BackingField_45() const { return ___U3CKeyboardMaps_readOnlyU3Ek__BackingField_45; }
	inline RuntimeObject** get_address_of_U3CKeyboardMaps_readOnlyU3Ek__BackingField_45() { return &___U3CKeyboardMaps_readOnlyU3Ek__BackingField_45; }
	inline void set_U3CKeyboardMaps_readOnlyU3Ek__BackingField_45(RuntimeObject* value)
	{
		___U3CKeyboardMaps_readOnlyU3Ek__BackingField_45 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyboardMaps_readOnlyU3Ek__BackingField_45), value);
	}

	inline static int32_t get_offset_of_U3CMouseMaps_readOnlyU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CMouseMaps_readOnlyU3Ek__BackingField_46)); }
	inline RuntimeObject* get_U3CMouseMaps_readOnlyU3Ek__BackingField_46() const { return ___U3CMouseMaps_readOnlyU3Ek__BackingField_46; }
	inline RuntimeObject** get_address_of_U3CMouseMaps_readOnlyU3Ek__BackingField_46() { return &___U3CMouseMaps_readOnlyU3Ek__BackingField_46; }
	inline void set_U3CMouseMaps_readOnlyU3Ek__BackingField_46(RuntimeObject* value)
	{
		___U3CMouseMaps_readOnlyU3Ek__BackingField_46 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseMaps_readOnlyU3Ek__BackingField_46), value);
	}

	inline static int32_t get_offset_of_U3CCustomControllerMaps_readOnlyU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CCustomControllerMaps_readOnlyU3Ek__BackingField_47)); }
	inline RuntimeObject* get_U3CCustomControllerMaps_readOnlyU3Ek__BackingField_47() const { return ___U3CCustomControllerMaps_readOnlyU3Ek__BackingField_47; }
	inline RuntimeObject** get_address_of_U3CCustomControllerMaps_readOnlyU3Ek__BackingField_47() { return &___U3CCustomControllerMaps_readOnlyU3Ek__BackingField_47; }
	inline void set_U3CCustomControllerMaps_readOnlyU3Ek__BackingField_47(RuntimeObject* value)
	{
		___U3CCustomControllerMaps_readOnlyU3Ek__BackingField_47 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomControllerMaps_readOnlyU3Ek__BackingField_47), value);
	}

	inline static int32_t get_offset_of_U3CControllerMapLayoutManagerRuleSets_readOnlyU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CControllerMapLayoutManagerRuleSets_readOnlyU3Ek__BackingField_48)); }
	inline RuntimeObject* get_U3CControllerMapLayoutManagerRuleSets_readOnlyU3Ek__BackingField_48() const { return ___U3CControllerMapLayoutManagerRuleSets_readOnlyU3Ek__BackingField_48; }
	inline RuntimeObject** get_address_of_U3CControllerMapLayoutManagerRuleSets_readOnlyU3Ek__BackingField_48() { return &___U3CControllerMapLayoutManagerRuleSets_readOnlyU3Ek__BackingField_48; }
	inline void set_U3CControllerMapLayoutManagerRuleSets_readOnlyU3Ek__BackingField_48(RuntimeObject* value)
	{
		___U3CControllerMapLayoutManagerRuleSets_readOnlyU3Ek__BackingField_48 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerMapLayoutManagerRuleSets_readOnlyU3Ek__BackingField_48), value);
	}

	inline static int32_t get_offset_of_U3CControllerMapEnablerRuleSets_readOnlyU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48, ___U3CControllerMapEnablerRuleSets_readOnlyU3Ek__BackingField_49)); }
	inline RuntimeObject* get_U3CControllerMapEnablerRuleSets_readOnlyU3Ek__BackingField_49() const { return ___U3CControllerMapEnablerRuleSets_readOnlyU3Ek__BackingField_49; }
	inline RuntimeObject** get_address_of_U3CControllerMapEnablerRuleSets_readOnlyU3Ek__BackingField_49() { return &___U3CControllerMapEnablerRuleSets_readOnlyU3Ek__BackingField_49; }
	inline void set_U3CControllerMapEnablerRuleSets_readOnlyU3Ek__BackingField_49(RuntimeObject* value)
	{
		___U3CControllerMapEnablerRuleSets_readOnlyU3Ek__BackingField_49 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerMapEnablerRuleSets_readOnlyU3Ek__BackingField_49), value);
	}
};

struct UserData_t23CF13BC0883AADA4A3457583033292E49A77D48_StaticFields
{
public:
	// System.Action`2<System.Collections.Generic.List`1<Rewired.Data.Player_Editor_Mapping>,System.Int32> Rewired.Data.UserData::CSU24<>9__CachedAnonymousMethodDelegate60
	Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate60_50;
	// System.Action`2<System.Collections.Generic.List`1<Rewired.Data.Player_Editor_Mapping>,System.Int32> Rewired.Data.UserData::CSU24<>9__CachedAnonymousMethodDelegate62
	Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate62_51;
	// System.Action`2<System.Collections.Generic.List`1<Rewired.Data.Player_Editor_Mapping>,System.Int32> Rewired.Data.UserData::CSU24<>9__CachedAnonymousMethodDelegate64
	Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate64_52;
	// System.Action`2<System.Collections.Generic.List`1<Rewired.Data.Player_Editor_Mapping>,System.Int32> Rewired.Data.UserData::CSU24<>9__CachedAnonymousMethodDelegate66
	Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate66_53;
	// System.Action`2<System.Collections.Generic.List`1<Rewired.Data.Player_Editor_Mapping>,System.Int32> Rewired.Data.UserData::CSU24<>9__CachedAnonymousMethodDelegate68
	Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate68_54;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate60_50() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate60_50)); }
	inline Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate60_50() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate60_50; }
	inline Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate60_50() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate60_50; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate60_50(Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate60_50 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate60_50), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate62_51() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate62_51)); }
	inline Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate62_51() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate62_51; }
	inline Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate62_51() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate62_51; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate62_51(Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate62_51 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate62_51), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate64_52() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate64_52)); }
	inline Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate64_52() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate64_52; }
	inline Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate64_52() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate64_52; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate64_52(Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate64_52 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate64_52), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate66_53() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate66_53)); }
	inline Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate66_53() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate66_53; }
	inline Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate66_53() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate66_53; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate66_53(Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate66_53 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate66_53), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate68_54() { return static_cast<int32_t>(offsetof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate68_54)); }
	inline Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate68_54() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate68_54; }
	inline Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate68_54() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate68_54; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate68_54(Action_2_t0FCF523A2C6C1237B7BD9C68EEA7E60A907FD4CD * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate68_54 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate68_54), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDATA_T23CF13BC0883AADA4A3457583033292E49A77D48_H
#ifndef FDLAXOJMKLCKPKEQKHYMFTOHUCZ_TF7E0CA3B84559682290E4F4B18A16E84D5E07D66_H
#define FDLAXOJMKLCKPKEQKHYMFTOHUCZ_TF7E0CA3B84559682290E4F4B18A16E84D5E07D66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz
struct  fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66  : public RuntimeObject
{
public:

public:
};

struct fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields
{
public:
	// System.Func`2<Rewired.InputCategory,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::UvtPZKSXeJnDxVJSEasBclzynvF
	Func_2_tDFADAE8DD881DBEA38A028A4E5542ADD97170619 * ___UvtPZKSXeJnDxVJSEasBclzynvF_0;
	// System.Func`2<Rewired.InputCategory,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::WIidHThRtMWAWPYztBTsUQgGahC
	Func_2_tEA5DF77336278B340269FE419B5C91926472DE8B * ___WIidHThRtMWAWPYztBTsUQgGahC_1;
	// System.Func`3<Rewired.InputCategory,System.Collections.Generic.IList`1<Rewired.InputCategory>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::HVfVQjDvaSqnCeUWBJkGGVtUyEr
	Func_3_t6617EB9E51F7A0A6716CDA949E95165FAC2BA4E2 * ___HVfVQjDvaSqnCeUWBJkGGVtUyEr_2;
	// System.Func`2<Rewired.InputBehavior,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::AFMhLWAUCrzwxwbRrxIOznFdYOn
	Func_2_tA22B529B781FA354C275B9212340CBEA87BA9704 * ___AFMhLWAUCrzwxwbRrxIOznFdYOn_3;
	// System.Func`2<Rewired.InputBehavior,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::gotEvJAFvtFqZDvpNxgnZpryZZUa
	Func_2_t7E8255AF8E4F6CBB697B0E1ABC106F943B2F57F6 * ___gotEvJAFvtFqZDvpNxgnZpryZZUa_4;
	// System.Func`3<Rewired.InputBehavior,System.Collections.Generic.IList`1<Rewired.InputBehavior>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::vSNAAzZNWjxpJKWvEeqQUwMQucG
	Func_3_tAD715980CBF49C148A305ED67A19769B424B7A8D * ___vSNAAzZNWjxpJKWvEeqQUwMQucG_5;
	// System.Func`2<Rewired.InputAction,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::mhqDbxUnUExiYStPEGCsJxNXbAc
	Func_2_t55F6CD5F3D43065DAF930674859E54C38CD29BF1 * ___mhqDbxUnUExiYStPEGCsJxNXbAc_6;
	// System.Func`2<Rewired.InputAction,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::nTVoTodzUyJPsANsPjLJsigWXvw
	Func_2_t399B7E27C786D4E284EB485BD673C9660B0232BC * ___nTVoTodzUyJPsANsPjLJsigWXvw_7;
	// System.Func`3<Rewired.InputAction,System.Collections.Generic.IList`1<Rewired.InputAction>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::YxTAfIvDCajfOaPrADetuPvJMNj
	Func_3_t8D5A1432AA54EBBB1CDCE4B14E784E045C8B925C * ___YxTAfIvDCajfOaPrADetuPvJMNj_8;
	// System.Func`2<Rewired.InputMapCategory,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::IJOPFSEVQJmcwTcVodgHCPoxrk
	Func_2_tECFA09A75B179FF9DFC326F62FA06569D68300CD * ___IJOPFSEVQJmcwTcVodgHCPoxrk_9;
	// System.Func`2<Rewired.InputMapCategory,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::wNIYbxBbEKGwHmkNTVruiNwyrLC
	Func_2_t53CF4F43E1D63F334C7CE91B4C7A0D780A24FC93 * ___wNIYbxBbEKGwHmkNTVruiNwyrLC_10;
	// System.Func`3<Rewired.InputMapCategory,System.Collections.Generic.IList`1<Rewired.InputMapCategory>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::sQIJBdStgelbqDDvPRPIPAJnAUEE
	Func_3_t44BF8A5EBD1E9FB6D8359B14CF212C650D574F7F * ___sQIJBdStgelbqDDvPRPIPAJnAUEE_11;
	// System.Func`2<Rewired.InputLayout,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::UTAFKbChSGhgSyrvJeBrjrcTVqiP
	Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 * ___UTAFKbChSGhgSyrvJeBrjrcTVqiP_12;
	// System.Func`2<Rewired.InputLayout,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::QpQFpmiwqTLTiLxGczgngBXVDrlq
	Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 * ___QpQFpmiwqTLTiLxGczgngBXVDrlq_13;
	// System.Func`3<Rewired.InputLayout,System.Collections.Generic.IList`1<Rewired.InputLayout>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::EAkfTFIVcsJxmXRWhVaSxIykuMJ
	Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 * ___EAkfTFIVcsJxmXRWhVaSxIykuMJ_14;
	// System.Func`2<Rewired.InputLayout,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::mptfiioUeYsSSBauHlhQdUpfZMw
	Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 * ___mptfiioUeYsSSBauHlhQdUpfZMw_15;
	// System.Func`2<Rewired.InputLayout,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::ZQYAxnlLgqDLnrxWKekUYqNoARSk
	Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 * ___ZQYAxnlLgqDLnrxWKekUYqNoARSk_16;
	// System.Func`3<Rewired.InputLayout,System.Collections.Generic.IList`1<Rewired.InputLayout>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::UQuijZsMqgNPpdrMEFiBrUgtguM
	Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 * ___UQuijZsMqgNPpdrMEFiBrUgtguM_17;
	// System.Func`2<Rewired.InputLayout,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::FehaJfEEjpeYLfpmUMzkFAbCRwsA
	Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 * ___FehaJfEEjpeYLfpmUMzkFAbCRwsA_18;
	// System.Func`2<Rewired.InputLayout,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::GMPHKVIYVSfxfzouOasvunIymPL
	Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 * ___GMPHKVIYVSfxfzouOasvunIymPL_19;
	// System.Func`3<Rewired.InputLayout,System.Collections.Generic.IList`1<Rewired.InputLayout>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::mMURPWMpQTiDTaELQZQNlGDUORm
	Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 * ___mMURPWMpQTiDTaELQZQNlGDUORm_20;
	// System.Func`2<Rewired.InputLayout,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::EkGbeTUTBdDIoDPcAfixArdMdHyG
	Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 * ___EkGbeTUTBdDIoDPcAfixArdMdHyG_21;
	// System.Func`2<Rewired.InputLayout,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::OJQStWFoTlmLdwbYTlSqDmvhecEF
	Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 * ___OJQStWFoTlmLdwbYTlSqDmvhecEF_22;
	// System.Func`3<Rewired.InputLayout,System.Collections.Generic.IList`1<Rewired.InputLayout>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::CPbJNfDOyMEaxLGISJMyUUybFdb
	Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 * ___CPbJNfDOyMEaxLGISJMyUUybFdb_23;
	// System.Func`2<Rewired.Data.CustomController_Editor,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::cxXZmRlaCQAyKoQbkTpCKLxMeafJ
	Func_2_tE26EB9A7D57FA6EFFF354471D51E059241C47BC2 * ___cxXZmRlaCQAyKoQbkTpCKLxMeafJ_24;
	// System.Func`2<Rewired.Data.CustomController_Editor,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::VcsAAbjgYjzjuZBULbyTZUnxEDT
	Func_2_tF71711714BA304BBA684F98ED721844B75360E14 * ___VcsAAbjgYjzjuZBULbyTZUnxEDT_25;
	// System.Func`3<Rewired.Data.CustomController_Editor,System.Collections.Generic.IList`1<Rewired.Data.CustomController_Editor>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::gkHZsBCnDVFtfkyxjPSvYoYeBAH
	Func_3_t6E3FD79D70ABB176EF74FACE38BDD967AD80523B * ___gkHZsBCnDVFtfkyxjPSvYoYeBAH_26;
	// System.Func`2<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::VuMrHWmvddoFySkCJvPQTJiRgjte
	Func_2_t06A6BFD16BD610B44F74E00E9E5E29831CDC8501 * ___VuMrHWmvddoFySkCJvPQTJiRgjte_27;
	// System.Func`2<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::jQkRkqxPYteIQfTNhJWgjBqkqBW
	Func_2_t36944FB9B92A66876D44BDB1FB6B96161E8C3AC4 * ___jQkRkqxPYteIQfTNhJWgjBqkqBW_28;
	// System.Func`3<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor,System.Collections.Generic.IList`1<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::hXMyukkkNaccKiRnnwzkwGEelOf
	Func_3_tCE00B2B3F8B70F9AAA4B29BC0F08E4FCD0C37B34 * ___hXMyukkkNaccKiRnnwzkwGEelOf_29;
	// System.Func`2<Rewired.Data.ControllerMapEnabler_RuleSet_Editor,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::KJrESYbqfbZhDRLPhKWUyBvPeCs
	Func_2_t0BD8682532A5DDF677C14A98D91388945D78C1E3 * ___KJrESYbqfbZhDRLPhKWUyBvPeCs_30;
	// System.Func`2<Rewired.Data.ControllerMapEnabler_RuleSet_Editor,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::fsbZNQRjTBLHNOepkccIwgwucJc
	Func_2_t66E0B7465F8F43E9C65DE7EE121532C322F9BA49 * ___fsbZNQRjTBLHNOepkccIwgwucJc_31;
	// System.Func`3<Rewired.Data.ControllerMapEnabler_RuleSet_Editor,System.Collections.Generic.IList`1<Rewired.Data.ControllerMapEnabler_RuleSet_Editor>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::nzFeekGdwmUMlLwcEwKJntNAkEGM
	Func_3_t38123FDEE2EF591B7CB6BC01B9943DF19D56B825 * ___nzFeekGdwmUMlLwcEwKJntNAkEGM_32;
	// System.Func`2<Rewired.Data.Player_Editor,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::NfVGdYdohwmJHTsplappHYUOsPsR
	Func_2_t865F0E70E6F8AAE22876AE6F68EBA08629B7DFA5 * ___NfVGdYdohwmJHTsplappHYUOsPsR_33;
	// System.Func`2<Rewired.Data.Player_Editor,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::frKVGvyneiZCwFdtOeqcMHrhDOv
	Func_2_tC14D01FAD79942C4E6779CDA169131C264BA046D * ___frKVGvyneiZCwFdtOeqcMHrhDOv_34;
	// System.Func`3<Rewired.Data.Player_Editor,System.Collections.Generic.IList`1<Rewired.Data.Player_Editor>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::xXuvRzDYDBHeentzKeRYhawKXNU
	Func_3_t02F9564D90C91BD1B71AA6C7BCE8F19B9D6114E8 * ___xXuvRzDYDBHeentzKeRYhawKXNU_35;
	// System.Func`2<Rewired.Data.Mapping.ControllerMap_Editor,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::WoWqKbSwDuVKPbFnTFWDbGbdmBlC
	Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 * ___WoWqKbSwDuVKPbFnTFWDbGbdmBlC_36;
	// System.Func`2<Rewired.Data.Mapping.ControllerMap_Editor,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::qvCbtckBsJVyCpGbnyHgLrvkvvrF
	Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 * ___qvCbtckBsJVyCpGbnyHgLrvkvvrF_37;
	// System.Func`2<Rewired.Data.Mapping.ControllerMap_Editor,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::BsQedYMhzLeOgRBAqlcSwEKRJe
	Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 * ___BsQedYMhzLeOgRBAqlcSwEKRJe_38;
	// System.Func`2<Rewired.Data.Mapping.ControllerMap_Editor,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::zPIkxlHoMHhQsRiXhblCcaLZntNr
	Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 * ___zPIkxlHoMHhQsRiXhblCcaLZntNr_39;
	// System.Func`2<Rewired.Data.Mapping.ControllerMap_Editor,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::NOlflOkpsDJptCcPBDLrHLwIPxiE
	Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 * ___NOlflOkpsDJptCcPBDLrHLwIPxiE_40;
	// System.Func`2<Rewired.Data.Mapping.ControllerMap_Editor,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::KiMVpqIgjIxAwIUdwQYMdYMAvOS
	Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 * ___KiMVpqIgjIxAwIUdwQYMdYMAvOS_41;
	// System.Func`2<Rewired.Data.Mapping.ControllerMap_Editor,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::fIsQOyHbYrzpWeIqPEBIaAAoAGl
	Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 * ___fIsQOyHbYrzpWeIqPEBIaAAoAGl_42;
	// System.Func`2<Rewired.Data.Mapping.ControllerMap_Editor,System.String> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz::fSMotZYUgsyrkDyYRKjUnGvbdUB
	Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 * ___fSMotZYUgsyrkDyYRKjUnGvbdUB_43;

public:
	inline static int32_t get_offset_of_UvtPZKSXeJnDxVJSEasBclzynvF_0() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___UvtPZKSXeJnDxVJSEasBclzynvF_0)); }
	inline Func_2_tDFADAE8DD881DBEA38A028A4E5542ADD97170619 * get_UvtPZKSXeJnDxVJSEasBclzynvF_0() const { return ___UvtPZKSXeJnDxVJSEasBclzynvF_0; }
	inline Func_2_tDFADAE8DD881DBEA38A028A4E5542ADD97170619 ** get_address_of_UvtPZKSXeJnDxVJSEasBclzynvF_0() { return &___UvtPZKSXeJnDxVJSEasBclzynvF_0; }
	inline void set_UvtPZKSXeJnDxVJSEasBclzynvF_0(Func_2_tDFADAE8DD881DBEA38A028A4E5542ADD97170619 * value)
	{
		___UvtPZKSXeJnDxVJSEasBclzynvF_0 = value;
		Il2CppCodeGenWriteBarrier((&___UvtPZKSXeJnDxVJSEasBclzynvF_0), value);
	}

	inline static int32_t get_offset_of_WIidHThRtMWAWPYztBTsUQgGahC_1() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___WIidHThRtMWAWPYztBTsUQgGahC_1)); }
	inline Func_2_tEA5DF77336278B340269FE419B5C91926472DE8B * get_WIidHThRtMWAWPYztBTsUQgGahC_1() const { return ___WIidHThRtMWAWPYztBTsUQgGahC_1; }
	inline Func_2_tEA5DF77336278B340269FE419B5C91926472DE8B ** get_address_of_WIidHThRtMWAWPYztBTsUQgGahC_1() { return &___WIidHThRtMWAWPYztBTsUQgGahC_1; }
	inline void set_WIidHThRtMWAWPYztBTsUQgGahC_1(Func_2_tEA5DF77336278B340269FE419B5C91926472DE8B * value)
	{
		___WIidHThRtMWAWPYztBTsUQgGahC_1 = value;
		Il2CppCodeGenWriteBarrier((&___WIidHThRtMWAWPYztBTsUQgGahC_1), value);
	}

	inline static int32_t get_offset_of_HVfVQjDvaSqnCeUWBJkGGVtUyEr_2() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___HVfVQjDvaSqnCeUWBJkGGVtUyEr_2)); }
	inline Func_3_t6617EB9E51F7A0A6716CDA949E95165FAC2BA4E2 * get_HVfVQjDvaSqnCeUWBJkGGVtUyEr_2() const { return ___HVfVQjDvaSqnCeUWBJkGGVtUyEr_2; }
	inline Func_3_t6617EB9E51F7A0A6716CDA949E95165FAC2BA4E2 ** get_address_of_HVfVQjDvaSqnCeUWBJkGGVtUyEr_2() { return &___HVfVQjDvaSqnCeUWBJkGGVtUyEr_2; }
	inline void set_HVfVQjDvaSqnCeUWBJkGGVtUyEr_2(Func_3_t6617EB9E51F7A0A6716CDA949E95165FAC2BA4E2 * value)
	{
		___HVfVQjDvaSqnCeUWBJkGGVtUyEr_2 = value;
		Il2CppCodeGenWriteBarrier((&___HVfVQjDvaSqnCeUWBJkGGVtUyEr_2), value);
	}

	inline static int32_t get_offset_of_AFMhLWAUCrzwxwbRrxIOznFdYOn_3() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___AFMhLWAUCrzwxwbRrxIOznFdYOn_3)); }
	inline Func_2_tA22B529B781FA354C275B9212340CBEA87BA9704 * get_AFMhLWAUCrzwxwbRrxIOznFdYOn_3() const { return ___AFMhLWAUCrzwxwbRrxIOznFdYOn_3; }
	inline Func_2_tA22B529B781FA354C275B9212340CBEA87BA9704 ** get_address_of_AFMhLWAUCrzwxwbRrxIOznFdYOn_3() { return &___AFMhLWAUCrzwxwbRrxIOznFdYOn_3; }
	inline void set_AFMhLWAUCrzwxwbRrxIOznFdYOn_3(Func_2_tA22B529B781FA354C275B9212340CBEA87BA9704 * value)
	{
		___AFMhLWAUCrzwxwbRrxIOznFdYOn_3 = value;
		Il2CppCodeGenWriteBarrier((&___AFMhLWAUCrzwxwbRrxIOznFdYOn_3), value);
	}

	inline static int32_t get_offset_of_gotEvJAFvtFqZDvpNxgnZpryZZUa_4() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___gotEvJAFvtFqZDvpNxgnZpryZZUa_4)); }
	inline Func_2_t7E8255AF8E4F6CBB697B0E1ABC106F943B2F57F6 * get_gotEvJAFvtFqZDvpNxgnZpryZZUa_4() const { return ___gotEvJAFvtFqZDvpNxgnZpryZZUa_4; }
	inline Func_2_t7E8255AF8E4F6CBB697B0E1ABC106F943B2F57F6 ** get_address_of_gotEvJAFvtFqZDvpNxgnZpryZZUa_4() { return &___gotEvJAFvtFqZDvpNxgnZpryZZUa_4; }
	inline void set_gotEvJAFvtFqZDvpNxgnZpryZZUa_4(Func_2_t7E8255AF8E4F6CBB697B0E1ABC106F943B2F57F6 * value)
	{
		___gotEvJAFvtFqZDvpNxgnZpryZZUa_4 = value;
		Il2CppCodeGenWriteBarrier((&___gotEvJAFvtFqZDvpNxgnZpryZZUa_4), value);
	}

	inline static int32_t get_offset_of_vSNAAzZNWjxpJKWvEeqQUwMQucG_5() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___vSNAAzZNWjxpJKWvEeqQUwMQucG_5)); }
	inline Func_3_tAD715980CBF49C148A305ED67A19769B424B7A8D * get_vSNAAzZNWjxpJKWvEeqQUwMQucG_5() const { return ___vSNAAzZNWjxpJKWvEeqQUwMQucG_5; }
	inline Func_3_tAD715980CBF49C148A305ED67A19769B424B7A8D ** get_address_of_vSNAAzZNWjxpJKWvEeqQUwMQucG_5() { return &___vSNAAzZNWjxpJKWvEeqQUwMQucG_5; }
	inline void set_vSNAAzZNWjxpJKWvEeqQUwMQucG_5(Func_3_tAD715980CBF49C148A305ED67A19769B424B7A8D * value)
	{
		___vSNAAzZNWjxpJKWvEeqQUwMQucG_5 = value;
		Il2CppCodeGenWriteBarrier((&___vSNAAzZNWjxpJKWvEeqQUwMQucG_5), value);
	}

	inline static int32_t get_offset_of_mhqDbxUnUExiYStPEGCsJxNXbAc_6() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___mhqDbxUnUExiYStPEGCsJxNXbAc_6)); }
	inline Func_2_t55F6CD5F3D43065DAF930674859E54C38CD29BF1 * get_mhqDbxUnUExiYStPEGCsJxNXbAc_6() const { return ___mhqDbxUnUExiYStPEGCsJxNXbAc_6; }
	inline Func_2_t55F6CD5F3D43065DAF930674859E54C38CD29BF1 ** get_address_of_mhqDbxUnUExiYStPEGCsJxNXbAc_6() { return &___mhqDbxUnUExiYStPEGCsJxNXbAc_6; }
	inline void set_mhqDbxUnUExiYStPEGCsJxNXbAc_6(Func_2_t55F6CD5F3D43065DAF930674859E54C38CD29BF1 * value)
	{
		___mhqDbxUnUExiYStPEGCsJxNXbAc_6 = value;
		Il2CppCodeGenWriteBarrier((&___mhqDbxUnUExiYStPEGCsJxNXbAc_6), value);
	}

	inline static int32_t get_offset_of_nTVoTodzUyJPsANsPjLJsigWXvw_7() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___nTVoTodzUyJPsANsPjLJsigWXvw_7)); }
	inline Func_2_t399B7E27C786D4E284EB485BD673C9660B0232BC * get_nTVoTodzUyJPsANsPjLJsigWXvw_7() const { return ___nTVoTodzUyJPsANsPjLJsigWXvw_7; }
	inline Func_2_t399B7E27C786D4E284EB485BD673C9660B0232BC ** get_address_of_nTVoTodzUyJPsANsPjLJsigWXvw_7() { return &___nTVoTodzUyJPsANsPjLJsigWXvw_7; }
	inline void set_nTVoTodzUyJPsANsPjLJsigWXvw_7(Func_2_t399B7E27C786D4E284EB485BD673C9660B0232BC * value)
	{
		___nTVoTodzUyJPsANsPjLJsigWXvw_7 = value;
		Il2CppCodeGenWriteBarrier((&___nTVoTodzUyJPsANsPjLJsigWXvw_7), value);
	}

	inline static int32_t get_offset_of_YxTAfIvDCajfOaPrADetuPvJMNj_8() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___YxTAfIvDCajfOaPrADetuPvJMNj_8)); }
	inline Func_3_t8D5A1432AA54EBBB1CDCE4B14E784E045C8B925C * get_YxTAfIvDCajfOaPrADetuPvJMNj_8() const { return ___YxTAfIvDCajfOaPrADetuPvJMNj_8; }
	inline Func_3_t8D5A1432AA54EBBB1CDCE4B14E784E045C8B925C ** get_address_of_YxTAfIvDCajfOaPrADetuPvJMNj_8() { return &___YxTAfIvDCajfOaPrADetuPvJMNj_8; }
	inline void set_YxTAfIvDCajfOaPrADetuPvJMNj_8(Func_3_t8D5A1432AA54EBBB1CDCE4B14E784E045C8B925C * value)
	{
		___YxTAfIvDCajfOaPrADetuPvJMNj_8 = value;
		Il2CppCodeGenWriteBarrier((&___YxTAfIvDCajfOaPrADetuPvJMNj_8), value);
	}

	inline static int32_t get_offset_of_IJOPFSEVQJmcwTcVodgHCPoxrk_9() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___IJOPFSEVQJmcwTcVodgHCPoxrk_9)); }
	inline Func_2_tECFA09A75B179FF9DFC326F62FA06569D68300CD * get_IJOPFSEVQJmcwTcVodgHCPoxrk_9() const { return ___IJOPFSEVQJmcwTcVodgHCPoxrk_9; }
	inline Func_2_tECFA09A75B179FF9DFC326F62FA06569D68300CD ** get_address_of_IJOPFSEVQJmcwTcVodgHCPoxrk_9() { return &___IJOPFSEVQJmcwTcVodgHCPoxrk_9; }
	inline void set_IJOPFSEVQJmcwTcVodgHCPoxrk_9(Func_2_tECFA09A75B179FF9DFC326F62FA06569D68300CD * value)
	{
		___IJOPFSEVQJmcwTcVodgHCPoxrk_9 = value;
		Il2CppCodeGenWriteBarrier((&___IJOPFSEVQJmcwTcVodgHCPoxrk_9), value);
	}

	inline static int32_t get_offset_of_wNIYbxBbEKGwHmkNTVruiNwyrLC_10() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___wNIYbxBbEKGwHmkNTVruiNwyrLC_10)); }
	inline Func_2_t53CF4F43E1D63F334C7CE91B4C7A0D780A24FC93 * get_wNIYbxBbEKGwHmkNTVruiNwyrLC_10() const { return ___wNIYbxBbEKGwHmkNTVruiNwyrLC_10; }
	inline Func_2_t53CF4F43E1D63F334C7CE91B4C7A0D780A24FC93 ** get_address_of_wNIYbxBbEKGwHmkNTVruiNwyrLC_10() { return &___wNIYbxBbEKGwHmkNTVruiNwyrLC_10; }
	inline void set_wNIYbxBbEKGwHmkNTVruiNwyrLC_10(Func_2_t53CF4F43E1D63F334C7CE91B4C7A0D780A24FC93 * value)
	{
		___wNIYbxBbEKGwHmkNTVruiNwyrLC_10 = value;
		Il2CppCodeGenWriteBarrier((&___wNIYbxBbEKGwHmkNTVruiNwyrLC_10), value);
	}

	inline static int32_t get_offset_of_sQIJBdStgelbqDDvPRPIPAJnAUEE_11() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___sQIJBdStgelbqDDvPRPIPAJnAUEE_11)); }
	inline Func_3_t44BF8A5EBD1E9FB6D8359B14CF212C650D574F7F * get_sQIJBdStgelbqDDvPRPIPAJnAUEE_11() const { return ___sQIJBdStgelbqDDvPRPIPAJnAUEE_11; }
	inline Func_3_t44BF8A5EBD1E9FB6D8359B14CF212C650D574F7F ** get_address_of_sQIJBdStgelbqDDvPRPIPAJnAUEE_11() { return &___sQIJBdStgelbqDDvPRPIPAJnAUEE_11; }
	inline void set_sQIJBdStgelbqDDvPRPIPAJnAUEE_11(Func_3_t44BF8A5EBD1E9FB6D8359B14CF212C650D574F7F * value)
	{
		___sQIJBdStgelbqDDvPRPIPAJnAUEE_11 = value;
		Il2CppCodeGenWriteBarrier((&___sQIJBdStgelbqDDvPRPIPAJnAUEE_11), value);
	}

	inline static int32_t get_offset_of_UTAFKbChSGhgSyrvJeBrjrcTVqiP_12() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___UTAFKbChSGhgSyrvJeBrjrcTVqiP_12)); }
	inline Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 * get_UTAFKbChSGhgSyrvJeBrjrcTVqiP_12() const { return ___UTAFKbChSGhgSyrvJeBrjrcTVqiP_12; }
	inline Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 ** get_address_of_UTAFKbChSGhgSyrvJeBrjrcTVqiP_12() { return &___UTAFKbChSGhgSyrvJeBrjrcTVqiP_12; }
	inline void set_UTAFKbChSGhgSyrvJeBrjrcTVqiP_12(Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 * value)
	{
		___UTAFKbChSGhgSyrvJeBrjrcTVqiP_12 = value;
		Il2CppCodeGenWriteBarrier((&___UTAFKbChSGhgSyrvJeBrjrcTVqiP_12), value);
	}

	inline static int32_t get_offset_of_QpQFpmiwqTLTiLxGczgngBXVDrlq_13() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___QpQFpmiwqTLTiLxGczgngBXVDrlq_13)); }
	inline Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 * get_QpQFpmiwqTLTiLxGczgngBXVDrlq_13() const { return ___QpQFpmiwqTLTiLxGczgngBXVDrlq_13; }
	inline Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 ** get_address_of_QpQFpmiwqTLTiLxGczgngBXVDrlq_13() { return &___QpQFpmiwqTLTiLxGczgngBXVDrlq_13; }
	inline void set_QpQFpmiwqTLTiLxGczgngBXVDrlq_13(Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 * value)
	{
		___QpQFpmiwqTLTiLxGczgngBXVDrlq_13 = value;
		Il2CppCodeGenWriteBarrier((&___QpQFpmiwqTLTiLxGczgngBXVDrlq_13), value);
	}

	inline static int32_t get_offset_of_EAkfTFIVcsJxmXRWhVaSxIykuMJ_14() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___EAkfTFIVcsJxmXRWhVaSxIykuMJ_14)); }
	inline Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 * get_EAkfTFIVcsJxmXRWhVaSxIykuMJ_14() const { return ___EAkfTFIVcsJxmXRWhVaSxIykuMJ_14; }
	inline Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 ** get_address_of_EAkfTFIVcsJxmXRWhVaSxIykuMJ_14() { return &___EAkfTFIVcsJxmXRWhVaSxIykuMJ_14; }
	inline void set_EAkfTFIVcsJxmXRWhVaSxIykuMJ_14(Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 * value)
	{
		___EAkfTFIVcsJxmXRWhVaSxIykuMJ_14 = value;
		Il2CppCodeGenWriteBarrier((&___EAkfTFIVcsJxmXRWhVaSxIykuMJ_14), value);
	}

	inline static int32_t get_offset_of_mptfiioUeYsSSBauHlhQdUpfZMw_15() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___mptfiioUeYsSSBauHlhQdUpfZMw_15)); }
	inline Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 * get_mptfiioUeYsSSBauHlhQdUpfZMw_15() const { return ___mptfiioUeYsSSBauHlhQdUpfZMw_15; }
	inline Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 ** get_address_of_mptfiioUeYsSSBauHlhQdUpfZMw_15() { return &___mptfiioUeYsSSBauHlhQdUpfZMw_15; }
	inline void set_mptfiioUeYsSSBauHlhQdUpfZMw_15(Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 * value)
	{
		___mptfiioUeYsSSBauHlhQdUpfZMw_15 = value;
		Il2CppCodeGenWriteBarrier((&___mptfiioUeYsSSBauHlhQdUpfZMw_15), value);
	}

	inline static int32_t get_offset_of_ZQYAxnlLgqDLnrxWKekUYqNoARSk_16() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___ZQYAxnlLgqDLnrxWKekUYqNoARSk_16)); }
	inline Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 * get_ZQYAxnlLgqDLnrxWKekUYqNoARSk_16() const { return ___ZQYAxnlLgqDLnrxWKekUYqNoARSk_16; }
	inline Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 ** get_address_of_ZQYAxnlLgqDLnrxWKekUYqNoARSk_16() { return &___ZQYAxnlLgqDLnrxWKekUYqNoARSk_16; }
	inline void set_ZQYAxnlLgqDLnrxWKekUYqNoARSk_16(Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 * value)
	{
		___ZQYAxnlLgqDLnrxWKekUYqNoARSk_16 = value;
		Il2CppCodeGenWriteBarrier((&___ZQYAxnlLgqDLnrxWKekUYqNoARSk_16), value);
	}

	inline static int32_t get_offset_of_UQuijZsMqgNPpdrMEFiBrUgtguM_17() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___UQuijZsMqgNPpdrMEFiBrUgtguM_17)); }
	inline Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 * get_UQuijZsMqgNPpdrMEFiBrUgtguM_17() const { return ___UQuijZsMqgNPpdrMEFiBrUgtguM_17; }
	inline Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 ** get_address_of_UQuijZsMqgNPpdrMEFiBrUgtguM_17() { return &___UQuijZsMqgNPpdrMEFiBrUgtguM_17; }
	inline void set_UQuijZsMqgNPpdrMEFiBrUgtguM_17(Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 * value)
	{
		___UQuijZsMqgNPpdrMEFiBrUgtguM_17 = value;
		Il2CppCodeGenWriteBarrier((&___UQuijZsMqgNPpdrMEFiBrUgtguM_17), value);
	}

	inline static int32_t get_offset_of_FehaJfEEjpeYLfpmUMzkFAbCRwsA_18() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___FehaJfEEjpeYLfpmUMzkFAbCRwsA_18)); }
	inline Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 * get_FehaJfEEjpeYLfpmUMzkFAbCRwsA_18() const { return ___FehaJfEEjpeYLfpmUMzkFAbCRwsA_18; }
	inline Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 ** get_address_of_FehaJfEEjpeYLfpmUMzkFAbCRwsA_18() { return &___FehaJfEEjpeYLfpmUMzkFAbCRwsA_18; }
	inline void set_FehaJfEEjpeYLfpmUMzkFAbCRwsA_18(Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 * value)
	{
		___FehaJfEEjpeYLfpmUMzkFAbCRwsA_18 = value;
		Il2CppCodeGenWriteBarrier((&___FehaJfEEjpeYLfpmUMzkFAbCRwsA_18), value);
	}

	inline static int32_t get_offset_of_GMPHKVIYVSfxfzouOasvunIymPL_19() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___GMPHKVIYVSfxfzouOasvunIymPL_19)); }
	inline Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 * get_GMPHKVIYVSfxfzouOasvunIymPL_19() const { return ___GMPHKVIYVSfxfzouOasvunIymPL_19; }
	inline Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 ** get_address_of_GMPHKVIYVSfxfzouOasvunIymPL_19() { return &___GMPHKVIYVSfxfzouOasvunIymPL_19; }
	inline void set_GMPHKVIYVSfxfzouOasvunIymPL_19(Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 * value)
	{
		___GMPHKVIYVSfxfzouOasvunIymPL_19 = value;
		Il2CppCodeGenWriteBarrier((&___GMPHKVIYVSfxfzouOasvunIymPL_19), value);
	}

	inline static int32_t get_offset_of_mMURPWMpQTiDTaELQZQNlGDUORm_20() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___mMURPWMpQTiDTaELQZQNlGDUORm_20)); }
	inline Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 * get_mMURPWMpQTiDTaELQZQNlGDUORm_20() const { return ___mMURPWMpQTiDTaELQZQNlGDUORm_20; }
	inline Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 ** get_address_of_mMURPWMpQTiDTaELQZQNlGDUORm_20() { return &___mMURPWMpQTiDTaELQZQNlGDUORm_20; }
	inline void set_mMURPWMpQTiDTaELQZQNlGDUORm_20(Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 * value)
	{
		___mMURPWMpQTiDTaELQZQNlGDUORm_20 = value;
		Il2CppCodeGenWriteBarrier((&___mMURPWMpQTiDTaELQZQNlGDUORm_20), value);
	}

	inline static int32_t get_offset_of_EkGbeTUTBdDIoDPcAfixArdMdHyG_21() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___EkGbeTUTBdDIoDPcAfixArdMdHyG_21)); }
	inline Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 * get_EkGbeTUTBdDIoDPcAfixArdMdHyG_21() const { return ___EkGbeTUTBdDIoDPcAfixArdMdHyG_21; }
	inline Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 ** get_address_of_EkGbeTUTBdDIoDPcAfixArdMdHyG_21() { return &___EkGbeTUTBdDIoDPcAfixArdMdHyG_21; }
	inline void set_EkGbeTUTBdDIoDPcAfixArdMdHyG_21(Func_2_tCCAACD4CE891C87DF5FEF337262FDF90531B87A2 * value)
	{
		___EkGbeTUTBdDIoDPcAfixArdMdHyG_21 = value;
		Il2CppCodeGenWriteBarrier((&___EkGbeTUTBdDIoDPcAfixArdMdHyG_21), value);
	}

	inline static int32_t get_offset_of_OJQStWFoTlmLdwbYTlSqDmvhecEF_22() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___OJQStWFoTlmLdwbYTlSqDmvhecEF_22)); }
	inline Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 * get_OJQStWFoTlmLdwbYTlSqDmvhecEF_22() const { return ___OJQStWFoTlmLdwbYTlSqDmvhecEF_22; }
	inline Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 ** get_address_of_OJQStWFoTlmLdwbYTlSqDmvhecEF_22() { return &___OJQStWFoTlmLdwbYTlSqDmvhecEF_22; }
	inline void set_OJQStWFoTlmLdwbYTlSqDmvhecEF_22(Func_2_t1A3652426423EFEAF009C83C30B7F92D229FED69 * value)
	{
		___OJQStWFoTlmLdwbYTlSqDmvhecEF_22 = value;
		Il2CppCodeGenWriteBarrier((&___OJQStWFoTlmLdwbYTlSqDmvhecEF_22), value);
	}

	inline static int32_t get_offset_of_CPbJNfDOyMEaxLGISJMyUUybFdb_23() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___CPbJNfDOyMEaxLGISJMyUUybFdb_23)); }
	inline Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 * get_CPbJNfDOyMEaxLGISJMyUUybFdb_23() const { return ___CPbJNfDOyMEaxLGISJMyUUybFdb_23; }
	inline Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 ** get_address_of_CPbJNfDOyMEaxLGISJMyUUybFdb_23() { return &___CPbJNfDOyMEaxLGISJMyUUybFdb_23; }
	inline void set_CPbJNfDOyMEaxLGISJMyUUybFdb_23(Func_3_t2C043EA62935ED2191F6275688F1AF77C52861F0 * value)
	{
		___CPbJNfDOyMEaxLGISJMyUUybFdb_23 = value;
		Il2CppCodeGenWriteBarrier((&___CPbJNfDOyMEaxLGISJMyUUybFdb_23), value);
	}

	inline static int32_t get_offset_of_cxXZmRlaCQAyKoQbkTpCKLxMeafJ_24() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___cxXZmRlaCQAyKoQbkTpCKLxMeafJ_24)); }
	inline Func_2_tE26EB9A7D57FA6EFFF354471D51E059241C47BC2 * get_cxXZmRlaCQAyKoQbkTpCKLxMeafJ_24() const { return ___cxXZmRlaCQAyKoQbkTpCKLxMeafJ_24; }
	inline Func_2_tE26EB9A7D57FA6EFFF354471D51E059241C47BC2 ** get_address_of_cxXZmRlaCQAyKoQbkTpCKLxMeafJ_24() { return &___cxXZmRlaCQAyKoQbkTpCKLxMeafJ_24; }
	inline void set_cxXZmRlaCQAyKoQbkTpCKLxMeafJ_24(Func_2_tE26EB9A7D57FA6EFFF354471D51E059241C47BC2 * value)
	{
		___cxXZmRlaCQAyKoQbkTpCKLxMeafJ_24 = value;
		Il2CppCodeGenWriteBarrier((&___cxXZmRlaCQAyKoQbkTpCKLxMeafJ_24), value);
	}

	inline static int32_t get_offset_of_VcsAAbjgYjzjuZBULbyTZUnxEDT_25() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___VcsAAbjgYjzjuZBULbyTZUnxEDT_25)); }
	inline Func_2_tF71711714BA304BBA684F98ED721844B75360E14 * get_VcsAAbjgYjzjuZBULbyTZUnxEDT_25() const { return ___VcsAAbjgYjzjuZBULbyTZUnxEDT_25; }
	inline Func_2_tF71711714BA304BBA684F98ED721844B75360E14 ** get_address_of_VcsAAbjgYjzjuZBULbyTZUnxEDT_25() { return &___VcsAAbjgYjzjuZBULbyTZUnxEDT_25; }
	inline void set_VcsAAbjgYjzjuZBULbyTZUnxEDT_25(Func_2_tF71711714BA304BBA684F98ED721844B75360E14 * value)
	{
		___VcsAAbjgYjzjuZBULbyTZUnxEDT_25 = value;
		Il2CppCodeGenWriteBarrier((&___VcsAAbjgYjzjuZBULbyTZUnxEDT_25), value);
	}

	inline static int32_t get_offset_of_gkHZsBCnDVFtfkyxjPSvYoYeBAH_26() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___gkHZsBCnDVFtfkyxjPSvYoYeBAH_26)); }
	inline Func_3_t6E3FD79D70ABB176EF74FACE38BDD967AD80523B * get_gkHZsBCnDVFtfkyxjPSvYoYeBAH_26() const { return ___gkHZsBCnDVFtfkyxjPSvYoYeBAH_26; }
	inline Func_3_t6E3FD79D70ABB176EF74FACE38BDD967AD80523B ** get_address_of_gkHZsBCnDVFtfkyxjPSvYoYeBAH_26() { return &___gkHZsBCnDVFtfkyxjPSvYoYeBAH_26; }
	inline void set_gkHZsBCnDVFtfkyxjPSvYoYeBAH_26(Func_3_t6E3FD79D70ABB176EF74FACE38BDD967AD80523B * value)
	{
		___gkHZsBCnDVFtfkyxjPSvYoYeBAH_26 = value;
		Il2CppCodeGenWriteBarrier((&___gkHZsBCnDVFtfkyxjPSvYoYeBAH_26), value);
	}

	inline static int32_t get_offset_of_VuMrHWmvddoFySkCJvPQTJiRgjte_27() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___VuMrHWmvddoFySkCJvPQTJiRgjte_27)); }
	inline Func_2_t06A6BFD16BD610B44F74E00E9E5E29831CDC8501 * get_VuMrHWmvddoFySkCJvPQTJiRgjte_27() const { return ___VuMrHWmvddoFySkCJvPQTJiRgjte_27; }
	inline Func_2_t06A6BFD16BD610B44F74E00E9E5E29831CDC8501 ** get_address_of_VuMrHWmvddoFySkCJvPQTJiRgjte_27() { return &___VuMrHWmvddoFySkCJvPQTJiRgjte_27; }
	inline void set_VuMrHWmvddoFySkCJvPQTJiRgjte_27(Func_2_t06A6BFD16BD610B44F74E00E9E5E29831CDC8501 * value)
	{
		___VuMrHWmvddoFySkCJvPQTJiRgjte_27 = value;
		Il2CppCodeGenWriteBarrier((&___VuMrHWmvddoFySkCJvPQTJiRgjte_27), value);
	}

	inline static int32_t get_offset_of_jQkRkqxPYteIQfTNhJWgjBqkqBW_28() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___jQkRkqxPYteIQfTNhJWgjBqkqBW_28)); }
	inline Func_2_t36944FB9B92A66876D44BDB1FB6B96161E8C3AC4 * get_jQkRkqxPYteIQfTNhJWgjBqkqBW_28() const { return ___jQkRkqxPYteIQfTNhJWgjBqkqBW_28; }
	inline Func_2_t36944FB9B92A66876D44BDB1FB6B96161E8C3AC4 ** get_address_of_jQkRkqxPYteIQfTNhJWgjBqkqBW_28() { return &___jQkRkqxPYteIQfTNhJWgjBqkqBW_28; }
	inline void set_jQkRkqxPYteIQfTNhJWgjBqkqBW_28(Func_2_t36944FB9B92A66876D44BDB1FB6B96161E8C3AC4 * value)
	{
		___jQkRkqxPYteIQfTNhJWgjBqkqBW_28 = value;
		Il2CppCodeGenWriteBarrier((&___jQkRkqxPYteIQfTNhJWgjBqkqBW_28), value);
	}

	inline static int32_t get_offset_of_hXMyukkkNaccKiRnnwzkwGEelOf_29() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___hXMyukkkNaccKiRnnwzkwGEelOf_29)); }
	inline Func_3_tCE00B2B3F8B70F9AAA4B29BC0F08E4FCD0C37B34 * get_hXMyukkkNaccKiRnnwzkwGEelOf_29() const { return ___hXMyukkkNaccKiRnnwzkwGEelOf_29; }
	inline Func_3_tCE00B2B3F8B70F9AAA4B29BC0F08E4FCD0C37B34 ** get_address_of_hXMyukkkNaccKiRnnwzkwGEelOf_29() { return &___hXMyukkkNaccKiRnnwzkwGEelOf_29; }
	inline void set_hXMyukkkNaccKiRnnwzkwGEelOf_29(Func_3_tCE00B2B3F8B70F9AAA4B29BC0F08E4FCD0C37B34 * value)
	{
		___hXMyukkkNaccKiRnnwzkwGEelOf_29 = value;
		Il2CppCodeGenWriteBarrier((&___hXMyukkkNaccKiRnnwzkwGEelOf_29), value);
	}

	inline static int32_t get_offset_of_KJrESYbqfbZhDRLPhKWUyBvPeCs_30() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___KJrESYbqfbZhDRLPhKWUyBvPeCs_30)); }
	inline Func_2_t0BD8682532A5DDF677C14A98D91388945D78C1E3 * get_KJrESYbqfbZhDRLPhKWUyBvPeCs_30() const { return ___KJrESYbqfbZhDRLPhKWUyBvPeCs_30; }
	inline Func_2_t0BD8682532A5DDF677C14A98D91388945D78C1E3 ** get_address_of_KJrESYbqfbZhDRLPhKWUyBvPeCs_30() { return &___KJrESYbqfbZhDRLPhKWUyBvPeCs_30; }
	inline void set_KJrESYbqfbZhDRLPhKWUyBvPeCs_30(Func_2_t0BD8682532A5DDF677C14A98D91388945D78C1E3 * value)
	{
		___KJrESYbqfbZhDRLPhKWUyBvPeCs_30 = value;
		Il2CppCodeGenWriteBarrier((&___KJrESYbqfbZhDRLPhKWUyBvPeCs_30), value);
	}

	inline static int32_t get_offset_of_fsbZNQRjTBLHNOepkccIwgwucJc_31() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___fsbZNQRjTBLHNOepkccIwgwucJc_31)); }
	inline Func_2_t66E0B7465F8F43E9C65DE7EE121532C322F9BA49 * get_fsbZNQRjTBLHNOepkccIwgwucJc_31() const { return ___fsbZNQRjTBLHNOepkccIwgwucJc_31; }
	inline Func_2_t66E0B7465F8F43E9C65DE7EE121532C322F9BA49 ** get_address_of_fsbZNQRjTBLHNOepkccIwgwucJc_31() { return &___fsbZNQRjTBLHNOepkccIwgwucJc_31; }
	inline void set_fsbZNQRjTBLHNOepkccIwgwucJc_31(Func_2_t66E0B7465F8F43E9C65DE7EE121532C322F9BA49 * value)
	{
		___fsbZNQRjTBLHNOepkccIwgwucJc_31 = value;
		Il2CppCodeGenWriteBarrier((&___fsbZNQRjTBLHNOepkccIwgwucJc_31), value);
	}

	inline static int32_t get_offset_of_nzFeekGdwmUMlLwcEwKJntNAkEGM_32() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___nzFeekGdwmUMlLwcEwKJntNAkEGM_32)); }
	inline Func_3_t38123FDEE2EF591B7CB6BC01B9943DF19D56B825 * get_nzFeekGdwmUMlLwcEwKJntNAkEGM_32() const { return ___nzFeekGdwmUMlLwcEwKJntNAkEGM_32; }
	inline Func_3_t38123FDEE2EF591B7CB6BC01B9943DF19D56B825 ** get_address_of_nzFeekGdwmUMlLwcEwKJntNAkEGM_32() { return &___nzFeekGdwmUMlLwcEwKJntNAkEGM_32; }
	inline void set_nzFeekGdwmUMlLwcEwKJntNAkEGM_32(Func_3_t38123FDEE2EF591B7CB6BC01B9943DF19D56B825 * value)
	{
		___nzFeekGdwmUMlLwcEwKJntNAkEGM_32 = value;
		Il2CppCodeGenWriteBarrier((&___nzFeekGdwmUMlLwcEwKJntNAkEGM_32), value);
	}

	inline static int32_t get_offset_of_NfVGdYdohwmJHTsplappHYUOsPsR_33() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___NfVGdYdohwmJHTsplappHYUOsPsR_33)); }
	inline Func_2_t865F0E70E6F8AAE22876AE6F68EBA08629B7DFA5 * get_NfVGdYdohwmJHTsplappHYUOsPsR_33() const { return ___NfVGdYdohwmJHTsplappHYUOsPsR_33; }
	inline Func_2_t865F0E70E6F8AAE22876AE6F68EBA08629B7DFA5 ** get_address_of_NfVGdYdohwmJHTsplappHYUOsPsR_33() { return &___NfVGdYdohwmJHTsplappHYUOsPsR_33; }
	inline void set_NfVGdYdohwmJHTsplappHYUOsPsR_33(Func_2_t865F0E70E6F8AAE22876AE6F68EBA08629B7DFA5 * value)
	{
		___NfVGdYdohwmJHTsplappHYUOsPsR_33 = value;
		Il2CppCodeGenWriteBarrier((&___NfVGdYdohwmJHTsplappHYUOsPsR_33), value);
	}

	inline static int32_t get_offset_of_frKVGvyneiZCwFdtOeqcMHrhDOv_34() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___frKVGvyneiZCwFdtOeqcMHrhDOv_34)); }
	inline Func_2_tC14D01FAD79942C4E6779CDA169131C264BA046D * get_frKVGvyneiZCwFdtOeqcMHrhDOv_34() const { return ___frKVGvyneiZCwFdtOeqcMHrhDOv_34; }
	inline Func_2_tC14D01FAD79942C4E6779CDA169131C264BA046D ** get_address_of_frKVGvyneiZCwFdtOeqcMHrhDOv_34() { return &___frKVGvyneiZCwFdtOeqcMHrhDOv_34; }
	inline void set_frKVGvyneiZCwFdtOeqcMHrhDOv_34(Func_2_tC14D01FAD79942C4E6779CDA169131C264BA046D * value)
	{
		___frKVGvyneiZCwFdtOeqcMHrhDOv_34 = value;
		Il2CppCodeGenWriteBarrier((&___frKVGvyneiZCwFdtOeqcMHrhDOv_34), value);
	}

	inline static int32_t get_offset_of_xXuvRzDYDBHeentzKeRYhawKXNU_35() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___xXuvRzDYDBHeentzKeRYhawKXNU_35)); }
	inline Func_3_t02F9564D90C91BD1B71AA6C7BCE8F19B9D6114E8 * get_xXuvRzDYDBHeentzKeRYhawKXNU_35() const { return ___xXuvRzDYDBHeentzKeRYhawKXNU_35; }
	inline Func_3_t02F9564D90C91BD1B71AA6C7BCE8F19B9D6114E8 ** get_address_of_xXuvRzDYDBHeentzKeRYhawKXNU_35() { return &___xXuvRzDYDBHeentzKeRYhawKXNU_35; }
	inline void set_xXuvRzDYDBHeentzKeRYhawKXNU_35(Func_3_t02F9564D90C91BD1B71AA6C7BCE8F19B9D6114E8 * value)
	{
		___xXuvRzDYDBHeentzKeRYhawKXNU_35 = value;
		Il2CppCodeGenWriteBarrier((&___xXuvRzDYDBHeentzKeRYhawKXNU_35), value);
	}

	inline static int32_t get_offset_of_WoWqKbSwDuVKPbFnTFWDbGbdmBlC_36() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___WoWqKbSwDuVKPbFnTFWDbGbdmBlC_36)); }
	inline Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 * get_WoWqKbSwDuVKPbFnTFWDbGbdmBlC_36() const { return ___WoWqKbSwDuVKPbFnTFWDbGbdmBlC_36; }
	inline Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 ** get_address_of_WoWqKbSwDuVKPbFnTFWDbGbdmBlC_36() { return &___WoWqKbSwDuVKPbFnTFWDbGbdmBlC_36; }
	inline void set_WoWqKbSwDuVKPbFnTFWDbGbdmBlC_36(Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 * value)
	{
		___WoWqKbSwDuVKPbFnTFWDbGbdmBlC_36 = value;
		Il2CppCodeGenWriteBarrier((&___WoWqKbSwDuVKPbFnTFWDbGbdmBlC_36), value);
	}

	inline static int32_t get_offset_of_qvCbtckBsJVyCpGbnyHgLrvkvvrF_37() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___qvCbtckBsJVyCpGbnyHgLrvkvvrF_37)); }
	inline Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 * get_qvCbtckBsJVyCpGbnyHgLrvkvvrF_37() const { return ___qvCbtckBsJVyCpGbnyHgLrvkvvrF_37; }
	inline Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 ** get_address_of_qvCbtckBsJVyCpGbnyHgLrvkvvrF_37() { return &___qvCbtckBsJVyCpGbnyHgLrvkvvrF_37; }
	inline void set_qvCbtckBsJVyCpGbnyHgLrvkvvrF_37(Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 * value)
	{
		___qvCbtckBsJVyCpGbnyHgLrvkvvrF_37 = value;
		Il2CppCodeGenWriteBarrier((&___qvCbtckBsJVyCpGbnyHgLrvkvvrF_37), value);
	}

	inline static int32_t get_offset_of_BsQedYMhzLeOgRBAqlcSwEKRJe_38() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___BsQedYMhzLeOgRBAqlcSwEKRJe_38)); }
	inline Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 * get_BsQedYMhzLeOgRBAqlcSwEKRJe_38() const { return ___BsQedYMhzLeOgRBAqlcSwEKRJe_38; }
	inline Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 ** get_address_of_BsQedYMhzLeOgRBAqlcSwEKRJe_38() { return &___BsQedYMhzLeOgRBAqlcSwEKRJe_38; }
	inline void set_BsQedYMhzLeOgRBAqlcSwEKRJe_38(Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 * value)
	{
		___BsQedYMhzLeOgRBAqlcSwEKRJe_38 = value;
		Il2CppCodeGenWriteBarrier((&___BsQedYMhzLeOgRBAqlcSwEKRJe_38), value);
	}

	inline static int32_t get_offset_of_zPIkxlHoMHhQsRiXhblCcaLZntNr_39() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___zPIkxlHoMHhQsRiXhblCcaLZntNr_39)); }
	inline Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 * get_zPIkxlHoMHhQsRiXhblCcaLZntNr_39() const { return ___zPIkxlHoMHhQsRiXhblCcaLZntNr_39; }
	inline Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 ** get_address_of_zPIkxlHoMHhQsRiXhblCcaLZntNr_39() { return &___zPIkxlHoMHhQsRiXhblCcaLZntNr_39; }
	inline void set_zPIkxlHoMHhQsRiXhblCcaLZntNr_39(Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 * value)
	{
		___zPIkxlHoMHhQsRiXhblCcaLZntNr_39 = value;
		Il2CppCodeGenWriteBarrier((&___zPIkxlHoMHhQsRiXhblCcaLZntNr_39), value);
	}

	inline static int32_t get_offset_of_NOlflOkpsDJptCcPBDLrHLwIPxiE_40() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___NOlflOkpsDJptCcPBDLrHLwIPxiE_40)); }
	inline Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 * get_NOlflOkpsDJptCcPBDLrHLwIPxiE_40() const { return ___NOlflOkpsDJptCcPBDLrHLwIPxiE_40; }
	inline Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 ** get_address_of_NOlflOkpsDJptCcPBDLrHLwIPxiE_40() { return &___NOlflOkpsDJptCcPBDLrHLwIPxiE_40; }
	inline void set_NOlflOkpsDJptCcPBDLrHLwIPxiE_40(Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 * value)
	{
		___NOlflOkpsDJptCcPBDLrHLwIPxiE_40 = value;
		Il2CppCodeGenWriteBarrier((&___NOlflOkpsDJptCcPBDLrHLwIPxiE_40), value);
	}

	inline static int32_t get_offset_of_KiMVpqIgjIxAwIUdwQYMdYMAvOS_41() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___KiMVpqIgjIxAwIUdwQYMdYMAvOS_41)); }
	inline Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 * get_KiMVpqIgjIxAwIUdwQYMdYMAvOS_41() const { return ___KiMVpqIgjIxAwIUdwQYMdYMAvOS_41; }
	inline Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 ** get_address_of_KiMVpqIgjIxAwIUdwQYMdYMAvOS_41() { return &___KiMVpqIgjIxAwIUdwQYMdYMAvOS_41; }
	inline void set_KiMVpqIgjIxAwIUdwQYMdYMAvOS_41(Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 * value)
	{
		___KiMVpqIgjIxAwIUdwQYMdYMAvOS_41 = value;
		Il2CppCodeGenWriteBarrier((&___KiMVpqIgjIxAwIUdwQYMdYMAvOS_41), value);
	}

	inline static int32_t get_offset_of_fIsQOyHbYrzpWeIqPEBIaAAoAGl_42() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___fIsQOyHbYrzpWeIqPEBIaAAoAGl_42)); }
	inline Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 * get_fIsQOyHbYrzpWeIqPEBIaAAoAGl_42() const { return ___fIsQOyHbYrzpWeIqPEBIaAAoAGl_42; }
	inline Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 ** get_address_of_fIsQOyHbYrzpWeIqPEBIaAAoAGl_42() { return &___fIsQOyHbYrzpWeIqPEBIaAAoAGl_42; }
	inline void set_fIsQOyHbYrzpWeIqPEBIaAAoAGl_42(Func_2_t8A410DCD6E3542D3D2965DD83F3BFE1DA538AE31 * value)
	{
		___fIsQOyHbYrzpWeIqPEBIaAAoAGl_42 = value;
		Il2CppCodeGenWriteBarrier((&___fIsQOyHbYrzpWeIqPEBIaAAoAGl_42), value);
	}

	inline static int32_t get_offset_of_fSMotZYUgsyrkDyYRKjUnGvbdUB_43() { return static_cast<int32_t>(offsetof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields, ___fSMotZYUgsyrkDyYRKjUnGvbdUB_43)); }
	inline Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 * get_fSMotZYUgsyrkDyYRKjUnGvbdUB_43() const { return ___fSMotZYUgsyrkDyYRKjUnGvbdUB_43; }
	inline Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 ** get_address_of_fSMotZYUgsyrkDyYRKjUnGvbdUB_43() { return &___fSMotZYUgsyrkDyYRKjUnGvbdUB_43; }
	inline void set_fSMotZYUgsyrkDyYRKjUnGvbdUB_43(Func_2_t00E452676AD5F981952EBBA6E3C8B33972C47405 * value)
	{
		___fSMotZYUgsyrkDyYRKjUnGvbdUB_43 = value;
		Il2CppCodeGenWriteBarrier((&___fSMotZYUgsyrkDyYRKjUnGvbdUB_43), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FDLAXOJMKLCKPKEQKHYMFTOHUCZ_TF7E0CA3B84559682290E4F4B18A16E84D5E07D66_H
#ifndef BKESZMWLQQWRGIHNNCNWEEHXFRJI_T86263D9C78C2A10FCF92015E80A0AAF1C64009F5_H
#define BKESZMWLQQWRGIHNNCNWEEHXFRJI_T86263D9C78C2A10FCF92015E80A0AAF1C64009F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI
struct  bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_0;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI::fTbGTbjBkADLQBKzHERKZMhLtdA
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___fTbGTbjBkADLQBKzHERKZMhLtdA_1;

public:
	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return static_cast<int32_t>(offsetof(bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5, ___NxecGThpOUHPMAqQltxuibhjsBi_0)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_0() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return &___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_0(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_0 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_0), value);
	}

	inline static int32_t get_offset_of_fTbGTbjBkADLQBKzHERKZMhLtdA_1() { return static_cast<int32_t>(offsetof(bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5, ___fTbGTbjBkADLQBKzHERKZMhLtdA_1)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_fTbGTbjBkADLQBKzHERKZMhLtdA_1() const { return ___fTbGTbjBkADLQBKzHERKZMhLtdA_1; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_fTbGTbjBkADLQBKzHERKZMhLtdA_1() { return &___fTbGTbjBkADLQBKzHERKZMhLtdA_1; }
	inline void set_fTbGTbjBkADLQBKzHERKZMhLtdA_1(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___fTbGTbjBkADLQBKzHERKZMhLtdA_1 = value;
		Il2CppCodeGenWriteBarrier((&___fTbGTbjBkADLQBKzHERKZMhLtdA_1), value);
	}
};

struct bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5_StaticFields
{
public:
	// System.Func`3<Rewired.ActionElementMap,System.Collections.Generic.IList`1<Rewired.ActionElementMap>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI::TPQSqycCaeBumhepwDvKRxltnozF
	Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 * ___TPQSqycCaeBumhepwDvKRxltnozF_2;

public:
	inline static int32_t get_offset_of_TPQSqycCaeBumhepwDvKRxltnozF_2() { return static_cast<int32_t>(offsetof(bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5_StaticFields, ___TPQSqycCaeBumhepwDvKRxltnozF_2)); }
	inline Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 * get_TPQSqycCaeBumhepwDvKRxltnozF_2() const { return ___TPQSqycCaeBumhepwDvKRxltnozF_2; }
	inline Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 ** get_address_of_TPQSqycCaeBumhepwDvKRxltnozF_2() { return &___TPQSqycCaeBumhepwDvKRxltnozF_2; }
	inline void set_TPQSqycCaeBumhepwDvKRxltnozF_2(Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 * value)
	{
		___TPQSqycCaeBumhepwDvKRxltnozF_2 = value;
		Il2CppCodeGenWriteBarrier((&___TPQSqycCaeBumhepwDvKRxltnozF_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BKESZMWLQQWRGIHNNCNWEEHXFRJI_T86263D9C78C2A10FCF92015E80A0AAF1C64009F5_H
#ifndef CDUAKQGENQOAHWGMECBYDKSYXGVI_T6D48E9D33100579CD0E582CBD0BE0870A58D3971_H
#define CDUAKQGENQOAHWGMECBYDKSYXGVI_T6D48E9D33100579CD0E582CBD0BE0870A58D3971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_CduakqgeNqOahwGMecbydkSyXGVi
struct  CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_CduakqgeNqOahwGMecbydkSyXGVi::ROoUOKoOmHPdALerupegCWsflOz
	bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5 * ___ROoUOKoOmHPdALerupegCWsflOz_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_CduakqgeNqOahwGMecbydkSyXGVi::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// Rewired.Data.Mapping.ControllerMap_Editor Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_CduakqgeNqOahwGMecbydkSyXGVi::ONFkOOHVwwNwinsmwCbKAlnoQkX
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * ___ONFkOOHVwwNwinsmwCbKAlnoQkX_2;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.Data.Mapping.ControllerMap_Editor> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_CduakqgeNqOahwGMecbydkSyXGVi::RnYngjobuYlteiRkTweuGcJpYtu
	DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E * ___RnYngjobuYlteiRkTweuGcJpYtu_3;

public:
	inline static int32_t get_offset_of_ROoUOKoOmHPdALerupegCWsflOz_0() { return static_cast<int32_t>(offsetof(CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971, ___ROoUOKoOmHPdALerupegCWsflOz_0)); }
	inline bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5 * get_ROoUOKoOmHPdALerupegCWsflOz_0() const { return ___ROoUOKoOmHPdALerupegCWsflOz_0; }
	inline bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5 ** get_address_of_ROoUOKoOmHPdALerupegCWsflOz_0() { return &___ROoUOKoOmHPdALerupegCWsflOz_0; }
	inline void set_ROoUOKoOmHPdALerupegCWsflOz_0(bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5 * value)
	{
		___ROoUOKoOmHPdALerupegCWsflOz_0 = value;
		Il2CppCodeGenWriteBarrier((&___ROoUOKoOmHPdALerupegCWsflOz_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_ONFkOOHVwwNwinsmwCbKAlnoQkX_2() { return static_cast<int32_t>(offsetof(CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971, ___ONFkOOHVwwNwinsmwCbKAlnoQkX_2)); }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * get_ONFkOOHVwwNwinsmwCbKAlnoQkX_2() const { return ___ONFkOOHVwwNwinsmwCbKAlnoQkX_2; }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 ** get_address_of_ONFkOOHVwwNwinsmwCbKAlnoQkX_2() { return &___ONFkOOHVwwNwinsmwCbKAlnoQkX_2; }
	inline void set_ONFkOOHVwwNwinsmwCbKAlnoQkX_2(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * value)
	{
		___ONFkOOHVwwNwinsmwCbKAlnoQkX_2 = value;
		Il2CppCodeGenWriteBarrier((&___ONFkOOHVwwNwinsmwCbKAlnoQkX_2), value);
	}

	inline static int32_t get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_3() { return static_cast<int32_t>(offsetof(CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971, ___RnYngjobuYlteiRkTweuGcJpYtu_3)); }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E * get_RnYngjobuYlteiRkTweuGcJpYtu_3() const { return ___RnYngjobuYlteiRkTweuGcJpYtu_3; }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E ** get_address_of_RnYngjobuYlteiRkTweuGcJpYtu_3() { return &___RnYngjobuYlteiRkTweuGcJpYtu_3; }
	inline void set_RnYngjobuYlteiRkTweuGcJpYtu_3(DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E * value)
	{
		___RnYngjobuYlteiRkTweuGcJpYtu_3 = value;
		Il2CppCodeGenWriteBarrier((&___RnYngjobuYlteiRkTweuGcJpYtu_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CDUAKQGENQOAHWGMECBYDKSYXGVI_T6D48E9D33100579CD0E582CBD0BE0870A58D3971_H
#ifndef IUJUWBPGTWVWKEDCPXRDPYYMOSJ_T791891BF6A23CC4871681551B044307F3DC53F61_H
#define IUJUWBPGTWVWKEDCPXRDPYYMOSJ_T791891BF6A23CC4871681551B044307F3DC53F61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_IUJUwBPGtwVwKedCpXrDpyYMoSj
struct  IUJUwBPGtwVwKedCpXrDpyYMoSj_t791891BF6A23CC4871681551B044307F3DC53F61  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_CduakqgeNqOahwGMecbydkSyXGVi Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_IUJUwBPGtwVwKedCpXrDpyYMoSj::ZJXznATWkJySNrKPVKIJNCwndjN
	CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971 * ___ZJXznATWkJySNrKPVKIJNCwndjN_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_IUJUwBPGtwVwKedCpXrDpyYMoSj::ROoUOKoOmHPdALerupegCWsflOz
	bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5 * ___ROoUOKoOmHPdALerupegCWsflOz_1;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_IUJUwBPGtwVwKedCpXrDpyYMoSj::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_2;
	// Rewired.ActionElementMap Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_IUJUwBPGtwVwKedCpXrDpyYMoSj::XaepNDmdlDDRBgSPSIhKadPGxnPE
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___XaepNDmdlDDRBgSPSIhKadPGxnPE_3;

public:
	inline static int32_t get_offset_of_ZJXznATWkJySNrKPVKIJNCwndjN_0() { return static_cast<int32_t>(offsetof(IUJUwBPGtwVwKedCpXrDpyYMoSj_t791891BF6A23CC4871681551B044307F3DC53F61, ___ZJXznATWkJySNrKPVKIJNCwndjN_0)); }
	inline CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971 * get_ZJXznATWkJySNrKPVKIJNCwndjN_0() const { return ___ZJXznATWkJySNrKPVKIJNCwndjN_0; }
	inline CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971 ** get_address_of_ZJXznATWkJySNrKPVKIJNCwndjN_0() { return &___ZJXznATWkJySNrKPVKIJNCwndjN_0; }
	inline void set_ZJXznATWkJySNrKPVKIJNCwndjN_0(CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971 * value)
	{
		___ZJXznATWkJySNrKPVKIJNCwndjN_0 = value;
		Il2CppCodeGenWriteBarrier((&___ZJXznATWkJySNrKPVKIJNCwndjN_0), value);
	}

	inline static int32_t get_offset_of_ROoUOKoOmHPdALerupegCWsflOz_1() { return static_cast<int32_t>(offsetof(IUJUwBPGtwVwKedCpXrDpyYMoSj_t791891BF6A23CC4871681551B044307F3DC53F61, ___ROoUOKoOmHPdALerupegCWsflOz_1)); }
	inline bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5 * get_ROoUOKoOmHPdALerupegCWsflOz_1() const { return ___ROoUOKoOmHPdALerupegCWsflOz_1; }
	inline bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5 ** get_address_of_ROoUOKoOmHPdALerupegCWsflOz_1() { return &___ROoUOKoOmHPdALerupegCWsflOz_1; }
	inline void set_ROoUOKoOmHPdALerupegCWsflOz_1(bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5 * value)
	{
		___ROoUOKoOmHPdALerupegCWsflOz_1 = value;
		Il2CppCodeGenWriteBarrier((&___ROoUOKoOmHPdALerupegCWsflOz_1), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_2() { return static_cast<int32_t>(offsetof(IUJUwBPGtwVwKedCpXrDpyYMoSj_t791891BF6A23CC4871681551B044307F3DC53F61, ___NxecGThpOUHPMAqQltxuibhjsBi_2)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_2() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_2; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_2() { return &___NxecGThpOUHPMAqQltxuibhjsBi_2; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_2(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_2 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_2), value);
	}

	inline static int32_t get_offset_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_3() { return static_cast<int32_t>(offsetof(IUJUwBPGtwVwKedCpXrDpyYMoSj_t791891BF6A23CC4871681551B044307F3DC53F61, ___XaepNDmdlDDRBgSPSIhKadPGxnPE_3)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_XaepNDmdlDDRBgSPSIhKadPGxnPE_3() const { return ___XaepNDmdlDDRBgSPSIhKadPGxnPE_3; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_3() { return &___XaepNDmdlDDRBgSPSIhKadPGxnPE_3; }
	inline void set_XaepNDmdlDDRBgSPSIhKadPGxnPE_3(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___XaepNDmdlDDRBgSPSIhKadPGxnPE_3 = value;
		Il2CppCodeGenWriteBarrier((&___XaepNDmdlDDRBgSPSIhKadPGxnPE_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IUJUWBPGTWVWKEDCPXRDPYYMOSJ_T791891BF6A23CC4871681551B044307F3DC53F61_H
#ifndef MVIWAGPECYFQYVYVNVJIVSAHUVH_TC83CB64516C2A9EB0B141CC5D56F86292F68FA52_H
#define MVIWAGPECYFQYVYVNVJIVSAHUVH_TC83CB64516C2A9EB0B141CC5D56F86292F68FA52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_mViWaGpecyFqyVYVNVJivSaHUvH
struct  mViWaGpecyFqyVYVNVJivSaHUvH_tC83CB64516C2A9EB0B141CC5D56F86292F68FA52  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_mViWaGpecyFqyVYVNVJivSaHUvH::ROoUOKoOmHPdALerupegCWsflOz
	bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5 * ___ROoUOKoOmHPdALerupegCWsflOz_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_mViWaGpecyFqyVYVNVJivSaHUvH::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// Rewired.Data.Mapping.ControllerMap_Editor Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_bkEsZMWLqQWRgIhnNcnweEhXFrJI_mViWaGpecyFqyVYVNVJivSaHUvH::doEmDrDRMAhaULBVLOODFaMXMSO
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * ___doEmDrDRMAhaULBVLOODFaMXMSO_2;

public:
	inline static int32_t get_offset_of_ROoUOKoOmHPdALerupegCWsflOz_0() { return static_cast<int32_t>(offsetof(mViWaGpecyFqyVYVNVJivSaHUvH_tC83CB64516C2A9EB0B141CC5D56F86292F68FA52, ___ROoUOKoOmHPdALerupegCWsflOz_0)); }
	inline bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5 * get_ROoUOKoOmHPdALerupegCWsflOz_0() const { return ___ROoUOKoOmHPdALerupegCWsflOz_0; }
	inline bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5 ** get_address_of_ROoUOKoOmHPdALerupegCWsflOz_0() { return &___ROoUOKoOmHPdALerupegCWsflOz_0; }
	inline void set_ROoUOKoOmHPdALerupegCWsflOz_0(bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5 * value)
	{
		___ROoUOKoOmHPdALerupegCWsflOz_0 = value;
		Il2CppCodeGenWriteBarrier((&___ROoUOKoOmHPdALerupegCWsflOz_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(mViWaGpecyFqyVYVNVJivSaHUvH_tC83CB64516C2A9EB0B141CC5D56F86292F68FA52, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_doEmDrDRMAhaULBVLOODFaMXMSO_2() { return static_cast<int32_t>(offsetof(mViWaGpecyFqyVYVNVJivSaHUvH_tC83CB64516C2A9EB0B141CC5D56F86292F68FA52, ___doEmDrDRMAhaULBVLOODFaMXMSO_2)); }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * get_doEmDrDRMAhaULBVLOODFaMXMSO_2() const { return ___doEmDrDRMAhaULBVLOODFaMXMSO_2; }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 ** get_address_of_doEmDrDRMAhaULBVLOODFaMXMSO_2() { return &___doEmDrDRMAhaULBVLOODFaMXMSO_2; }
	inline void set_doEmDrDRMAhaULBVLOODFaMXMSO_2(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * value)
	{
		___doEmDrDRMAhaULBVLOODFaMXMSO_2 = value;
		Il2CppCodeGenWriteBarrier((&___doEmDrDRMAhaULBVLOODFaMXMSO_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MVIWAGPECYFQYVYVNVJIVSAHUVH_TC83CB64516C2A9EB0B141CC5D56F86292F68FA52_H
#ifndef FBILKDTPHFEOBZPVPLNSWIWWMRV_T84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A_H
#define FBILKDTPHFEOBZPVPLNSWIWWMRV_T84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_fBIlKDTPhFeObzPVPLNsWIWwMRV
struct  fBIlKDTPhFeObzPVPLNsWIWwMRV_t84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_fBIlKDTPhFeObzPVPLNsWIWwMRV::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_0;
	// System.Collections.Generic.List`1<System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_fBIlKDTPhFeObzPVPLNsWIWwMRV::upTZSBfMuYEorlKpXpjtyzocIsB
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___upTZSBfMuYEorlKpXpjtyzocIsB_1;

public:
	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return static_cast<int32_t>(offsetof(fBIlKDTPhFeObzPVPLNsWIWwMRV_t84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A, ___NxecGThpOUHPMAqQltxuibhjsBi_0)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_0() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return &___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_0(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_0 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_0), value);
	}

	inline static int32_t get_offset_of_upTZSBfMuYEorlKpXpjtyzocIsB_1() { return static_cast<int32_t>(offsetof(fBIlKDTPhFeObzPVPLNsWIWwMRV_t84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A, ___upTZSBfMuYEorlKpXpjtyzocIsB_1)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_upTZSBfMuYEorlKpXpjtyzocIsB_1() const { return ___upTZSBfMuYEorlKpXpjtyzocIsB_1; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_upTZSBfMuYEorlKpXpjtyzocIsB_1() { return &___upTZSBfMuYEorlKpXpjtyzocIsB_1; }
	inline void set_upTZSBfMuYEorlKpXpjtyzocIsB_1(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___upTZSBfMuYEorlKpXpjtyzocIsB_1 = value;
		Il2CppCodeGenWriteBarrier((&___upTZSBfMuYEorlKpXpjtyzocIsB_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FBILKDTPHFEOBZPVPLNSWIWWMRV_T84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A_H
#ifndef JDOWVQWVKIOMHXVEUMGNVLHPEXZ_TD4C4341C1144FA3F0B9D439B7C7537501188CE95_H
#define JDOWVQWVKIOMHXVEUMGNVLHPEXZ_TD4C4341C1144FA3F0B9D439B7C7537501188CE95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz
struct  jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_0;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz::fTbGTbjBkADLQBKzHERKZMhLtdA
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___fTbGTbjBkADLQBKzHERKZMhLtdA_1;

public:
	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return static_cast<int32_t>(offsetof(jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95, ___NxecGThpOUHPMAqQltxuibhjsBi_0)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_0() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return &___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_0(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_0 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_0), value);
	}

	inline static int32_t get_offset_of_fTbGTbjBkADLQBKzHERKZMhLtdA_1() { return static_cast<int32_t>(offsetof(jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95, ___fTbGTbjBkADLQBKzHERKZMhLtdA_1)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_fTbGTbjBkADLQBKzHERKZMhLtdA_1() const { return ___fTbGTbjBkADLQBKzHERKZMhLtdA_1; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_fTbGTbjBkADLQBKzHERKZMhLtdA_1() { return &___fTbGTbjBkADLQBKzHERKZMhLtdA_1; }
	inline void set_fTbGTbjBkADLQBKzHERKZMhLtdA_1(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___fTbGTbjBkADLQBKzHERKZMhLtdA_1 = value;
		Il2CppCodeGenWriteBarrier((&___fTbGTbjBkADLQBKzHERKZMhLtdA_1), value);
	}
};

struct jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95_StaticFields
{
public:
	// System.Func`3<Rewired.ActionElementMap,System.Collections.Generic.IList`1<Rewired.ActionElementMap>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz::jbTUVVWhKMONmSiBrZPsoRVOEug
	Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 * ___jbTUVVWhKMONmSiBrZPsoRVOEug_2;

public:
	inline static int32_t get_offset_of_jbTUVVWhKMONmSiBrZPsoRVOEug_2() { return static_cast<int32_t>(offsetof(jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95_StaticFields, ___jbTUVVWhKMONmSiBrZPsoRVOEug_2)); }
	inline Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 * get_jbTUVVWhKMONmSiBrZPsoRVOEug_2() const { return ___jbTUVVWhKMONmSiBrZPsoRVOEug_2; }
	inline Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 ** get_address_of_jbTUVVWhKMONmSiBrZPsoRVOEug_2() { return &___jbTUVVWhKMONmSiBrZPsoRVOEug_2; }
	inline void set_jbTUVVWhKMONmSiBrZPsoRVOEug_2(Func_3_t6DC6D56E1BC7AB39FCAF0000A4D0686B1DF4D613 * value)
	{
		___jbTUVVWhKMONmSiBrZPsoRVOEug_2 = value;
		Il2CppCodeGenWriteBarrier((&___jbTUVVWhKMONmSiBrZPsoRVOEug_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JDOWVQWVKIOMHXVEUMGNVLHPEXZ_TD4C4341C1144FA3F0B9D439B7C7537501188CE95_H
#ifndef VYWEAQBBKWJBWZWIKICDMTPATXJ_TC3241E3F84DFEFB6D5ABCABCEB699C780875807F_H
#define VYWEAQBBKWJBWZWIKICDMTPATXJ_TC3241E3F84DFEFB6D5ABCABCEB699C780875807F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_VYWeAqBBKwjbwzwIKICDmtpATxj
struct  VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_VYWeAqBBKwjbwzwIKICDmtpATxj::pHFLPIzwyoQlzDoggykLsjJmQtx
	jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95 * ___pHFLPIzwyoQlzDoggykLsjJmQtx_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_VYWeAqBBKwjbwzwIKICDmtpATxj::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// Rewired.Data.Mapping.ControllerMap_Editor Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_VYWeAqBBKwjbwzwIKICDmtpATxj::ONFkOOHVwwNwinsmwCbKAlnoQkX
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * ___ONFkOOHVwwNwinsmwCbKAlnoQkX_2;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.Data.Mapping.ControllerMap_Editor> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_VYWeAqBBKwjbwzwIKICDmtpATxj::RnYngjobuYlteiRkTweuGcJpYtu
	DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E * ___RnYngjobuYlteiRkTweuGcJpYtu_3;

public:
	inline static int32_t get_offset_of_pHFLPIzwyoQlzDoggykLsjJmQtx_0() { return static_cast<int32_t>(offsetof(VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F, ___pHFLPIzwyoQlzDoggykLsjJmQtx_0)); }
	inline jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95 * get_pHFLPIzwyoQlzDoggykLsjJmQtx_0() const { return ___pHFLPIzwyoQlzDoggykLsjJmQtx_0; }
	inline jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95 ** get_address_of_pHFLPIzwyoQlzDoggykLsjJmQtx_0() { return &___pHFLPIzwyoQlzDoggykLsjJmQtx_0; }
	inline void set_pHFLPIzwyoQlzDoggykLsjJmQtx_0(jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95 * value)
	{
		___pHFLPIzwyoQlzDoggykLsjJmQtx_0 = value;
		Il2CppCodeGenWriteBarrier((&___pHFLPIzwyoQlzDoggykLsjJmQtx_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_ONFkOOHVwwNwinsmwCbKAlnoQkX_2() { return static_cast<int32_t>(offsetof(VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F, ___ONFkOOHVwwNwinsmwCbKAlnoQkX_2)); }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * get_ONFkOOHVwwNwinsmwCbKAlnoQkX_2() const { return ___ONFkOOHVwwNwinsmwCbKAlnoQkX_2; }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 ** get_address_of_ONFkOOHVwwNwinsmwCbKAlnoQkX_2() { return &___ONFkOOHVwwNwinsmwCbKAlnoQkX_2; }
	inline void set_ONFkOOHVwwNwinsmwCbKAlnoQkX_2(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * value)
	{
		___ONFkOOHVwwNwinsmwCbKAlnoQkX_2 = value;
		Il2CppCodeGenWriteBarrier((&___ONFkOOHVwwNwinsmwCbKAlnoQkX_2), value);
	}

	inline static int32_t get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_3() { return static_cast<int32_t>(offsetof(VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F, ___RnYngjobuYlteiRkTweuGcJpYtu_3)); }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E * get_RnYngjobuYlteiRkTweuGcJpYtu_3() const { return ___RnYngjobuYlteiRkTweuGcJpYtu_3; }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E ** get_address_of_RnYngjobuYlteiRkTweuGcJpYtu_3() { return &___RnYngjobuYlteiRkTweuGcJpYtu_3; }
	inline void set_RnYngjobuYlteiRkTweuGcJpYtu_3(DdxYPCrLAHFMStEcIPuCjbnFlwc_t9138F65283AE466006F5CE932F433CFA579DB43E * value)
	{
		___RnYngjobuYlteiRkTweuGcJpYtu_3 = value;
		Il2CppCodeGenWriteBarrier((&___RnYngjobuYlteiRkTweuGcJpYtu_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VYWEAQBBKWJBWZWIKICDMTPATXJ_TC3241E3F84DFEFB6D5ABCABCEB699C780875807F_H
#ifndef WBSUAOAHYIWSTFHSEGUZYBZDJNQ_T3BF5F59F8AA7F25388D8EA40DB384A1B5678668B_H
#define WBSUAOAHYIWSTFHSEGUZYBZDJNQ_T3BF5F59F8AA7F25388D8EA40DB384A1B5678668B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_WbsuaOAHYiWSTfHSeGUzYBzdJnQ
struct  WbsuaOAHYiWSTfHSeGUzYBzdJnQ_t3BF5F59F8AA7F25388D8EA40DB384A1B5678668B  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_WbsuaOAHYiWSTfHSeGUzYBzdJnQ::pHFLPIzwyoQlzDoggykLsjJmQtx
	jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95 * ___pHFLPIzwyoQlzDoggykLsjJmQtx_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_WbsuaOAHYiWSTfHSeGUzYBzdJnQ::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// Rewired.Data.Mapping.ControllerMap_Editor Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_WbsuaOAHYiWSTfHSeGUzYBzdJnQ::doEmDrDRMAhaULBVLOODFaMXMSO
	ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * ___doEmDrDRMAhaULBVLOODFaMXMSO_2;

public:
	inline static int32_t get_offset_of_pHFLPIzwyoQlzDoggykLsjJmQtx_0() { return static_cast<int32_t>(offsetof(WbsuaOAHYiWSTfHSeGUzYBzdJnQ_t3BF5F59F8AA7F25388D8EA40DB384A1B5678668B, ___pHFLPIzwyoQlzDoggykLsjJmQtx_0)); }
	inline jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95 * get_pHFLPIzwyoQlzDoggykLsjJmQtx_0() const { return ___pHFLPIzwyoQlzDoggykLsjJmQtx_0; }
	inline jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95 ** get_address_of_pHFLPIzwyoQlzDoggykLsjJmQtx_0() { return &___pHFLPIzwyoQlzDoggykLsjJmQtx_0; }
	inline void set_pHFLPIzwyoQlzDoggykLsjJmQtx_0(jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95 * value)
	{
		___pHFLPIzwyoQlzDoggykLsjJmQtx_0 = value;
		Il2CppCodeGenWriteBarrier((&___pHFLPIzwyoQlzDoggykLsjJmQtx_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(WbsuaOAHYiWSTfHSeGUzYBzdJnQ_t3BF5F59F8AA7F25388D8EA40DB384A1B5678668B, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_doEmDrDRMAhaULBVLOODFaMXMSO_2() { return static_cast<int32_t>(offsetof(WbsuaOAHYiWSTfHSeGUzYBzdJnQ_t3BF5F59F8AA7F25388D8EA40DB384A1B5678668B, ___doEmDrDRMAhaULBVLOODFaMXMSO_2)); }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * get_doEmDrDRMAhaULBVLOODFaMXMSO_2() const { return ___doEmDrDRMAhaULBVLOODFaMXMSO_2; }
	inline ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 ** get_address_of_doEmDrDRMAhaULBVLOODFaMXMSO_2() { return &___doEmDrDRMAhaULBVLOODFaMXMSO_2; }
	inline void set_doEmDrDRMAhaULBVLOODFaMXMSO_2(ControllerMap_Editor_tFF9297D0655C06B97D1BCA1D434828B2614A7CA7 * value)
	{
		___doEmDrDRMAhaULBVLOODFaMXMSO_2 = value;
		Il2CppCodeGenWriteBarrier((&___doEmDrDRMAhaULBVLOODFaMXMSO_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WBSUAOAHYIWSTFHSEGUZYBZDJNQ_T3BF5F59F8AA7F25388D8EA40DB384A1B5678668B_H
#ifndef SRBPYUNDPHGBOPRVKTKTMAUILHX_T527D1FE4889972D42620A98A61448BA95F3D52CB_H
#define SRBPYUNDPHGBOPRVKTKTMAUILHX_T527D1FE4889972D42620A98A61448BA95F3D52CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_srBPyUndPHgBOpRvKtKTmaUIlHx
struct  srBPyUndPHgBOpRvKtKTmaUIlHx_t527D1FE4889972D42620A98A61448BA95F3D52CB  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_VYWeAqBBKwjbwzwIKICDmtpATxj Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_srBPyUndPHgBOpRvKtKTmaUIlHx::pzLXCtoFGGhhvjRYfqtjXVZRtPf
	VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F * ___pzLXCtoFGGhhvjRYfqtjXVZRtPf_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_srBPyUndPHgBOpRvKtKTmaUIlHx::pHFLPIzwyoQlzDoggykLsjJmQtx
	jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95 * ___pHFLPIzwyoQlzDoggykLsjJmQtx_1;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_srBPyUndPHgBOpRvKtKTmaUIlHx::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_2;
	// Rewired.ActionElementMap Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_jDowvQWVkIomHXVEUMGnVlhpeXz_srBPyUndPHgBOpRvKtKTmaUIlHx::XaepNDmdlDDRBgSPSIhKadPGxnPE
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___XaepNDmdlDDRBgSPSIhKadPGxnPE_3;

public:
	inline static int32_t get_offset_of_pzLXCtoFGGhhvjRYfqtjXVZRtPf_0() { return static_cast<int32_t>(offsetof(srBPyUndPHgBOpRvKtKTmaUIlHx_t527D1FE4889972D42620A98A61448BA95F3D52CB, ___pzLXCtoFGGhhvjRYfqtjXVZRtPf_0)); }
	inline VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F * get_pzLXCtoFGGhhvjRYfqtjXVZRtPf_0() const { return ___pzLXCtoFGGhhvjRYfqtjXVZRtPf_0; }
	inline VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F ** get_address_of_pzLXCtoFGGhhvjRYfqtjXVZRtPf_0() { return &___pzLXCtoFGGhhvjRYfqtjXVZRtPf_0; }
	inline void set_pzLXCtoFGGhhvjRYfqtjXVZRtPf_0(VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F * value)
	{
		___pzLXCtoFGGhhvjRYfqtjXVZRtPf_0 = value;
		Il2CppCodeGenWriteBarrier((&___pzLXCtoFGGhhvjRYfqtjXVZRtPf_0), value);
	}

	inline static int32_t get_offset_of_pHFLPIzwyoQlzDoggykLsjJmQtx_1() { return static_cast<int32_t>(offsetof(srBPyUndPHgBOpRvKtKTmaUIlHx_t527D1FE4889972D42620A98A61448BA95F3D52CB, ___pHFLPIzwyoQlzDoggykLsjJmQtx_1)); }
	inline jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95 * get_pHFLPIzwyoQlzDoggykLsjJmQtx_1() const { return ___pHFLPIzwyoQlzDoggykLsjJmQtx_1; }
	inline jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95 ** get_address_of_pHFLPIzwyoQlzDoggykLsjJmQtx_1() { return &___pHFLPIzwyoQlzDoggykLsjJmQtx_1; }
	inline void set_pHFLPIzwyoQlzDoggykLsjJmQtx_1(jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95 * value)
	{
		___pHFLPIzwyoQlzDoggykLsjJmQtx_1 = value;
		Il2CppCodeGenWriteBarrier((&___pHFLPIzwyoQlzDoggykLsjJmQtx_1), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_2() { return static_cast<int32_t>(offsetof(srBPyUndPHgBOpRvKtKTmaUIlHx_t527D1FE4889972D42620A98A61448BA95F3D52CB, ___NxecGThpOUHPMAqQltxuibhjsBi_2)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_2() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_2; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_2() { return &___NxecGThpOUHPMAqQltxuibhjsBi_2; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_2(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_2 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_2), value);
	}

	inline static int32_t get_offset_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_3() { return static_cast<int32_t>(offsetof(srBPyUndPHgBOpRvKtKTmaUIlHx_t527D1FE4889972D42620A98A61448BA95F3D52CB, ___XaepNDmdlDDRBgSPSIhKadPGxnPE_3)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_XaepNDmdlDDRBgSPSIhKadPGxnPE_3() const { return ___XaepNDmdlDDRBgSPSIhKadPGxnPE_3; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_3() { return &___XaepNDmdlDDRBgSPSIhKadPGxnPE_3; }
	inline void set_XaepNDmdlDDRBgSPSIhKadPGxnPE_3(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___XaepNDmdlDDRBgSPSIhKadPGxnPE_3 = value;
		Il2CppCodeGenWriteBarrier((&___XaepNDmdlDDRBgSPSIhKadPGxnPE_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRBPYUNDPHGBOPRVKTKTMAUILHX_T527D1FE4889972D42620A98A61448BA95F3D52CB_H
#ifndef PMUHWAVZPYKUWHGXJJCHBUAAOUM_T130B30CBE6F351181E7BC28C2186765AF060CFAB_H
#define PMUHWAVZPYKUWHGXJJCHBUAAOUM_T130B30CBE6F351181E7BC28C2186765AF060CFAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_pMUhWAvzpyKUWHgXjJchbuAaouM
struct  pMUhWAvzpyKUWHgXjJchbuAaouM_t130B30CBE6F351181E7BC28C2186765AF060CFAB  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_fBIlKDTPhFeObzPVPLNsWIWwMRV Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_pMUhWAvzpyKUWHgXjJchbuAaouM::vRNayqdHvXglMdVKXeqzKwdwmKy
	fBIlKDTPhFeObzPVPLNsWIWwMRV_t84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A * ___vRNayqdHvXglMdVKXeqzKwdwmKy_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_pMUhWAvzpyKUWHgXjJchbuAaouM::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_pMUhWAvzpyKUWHgXjJchbuAaouM::wTqpkSxtmPJzjdXGhVkxSDXgQnn
	int32_t ___wTqpkSxtmPJzjdXGhVkxSDXgQnn_2;

public:
	inline static int32_t get_offset_of_vRNayqdHvXglMdVKXeqzKwdwmKy_0() { return static_cast<int32_t>(offsetof(pMUhWAvzpyKUWHgXjJchbuAaouM_t130B30CBE6F351181E7BC28C2186765AF060CFAB, ___vRNayqdHvXglMdVKXeqzKwdwmKy_0)); }
	inline fBIlKDTPhFeObzPVPLNsWIWwMRV_t84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A * get_vRNayqdHvXglMdVKXeqzKwdwmKy_0() const { return ___vRNayqdHvXglMdVKXeqzKwdwmKy_0; }
	inline fBIlKDTPhFeObzPVPLNsWIWwMRV_t84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A ** get_address_of_vRNayqdHvXglMdVKXeqzKwdwmKy_0() { return &___vRNayqdHvXglMdVKXeqzKwdwmKy_0; }
	inline void set_vRNayqdHvXglMdVKXeqzKwdwmKy_0(fBIlKDTPhFeObzPVPLNsWIWwMRV_t84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A * value)
	{
		___vRNayqdHvXglMdVKXeqzKwdwmKy_0 = value;
		Il2CppCodeGenWriteBarrier((&___vRNayqdHvXglMdVKXeqzKwdwmKy_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(pMUhWAvzpyKUWHgXjJchbuAaouM_t130B30CBE6F351181E7BC28C2186765AF060CFAB, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_wTqpkSxtmPJzjdXGhVkxSDXgQnn_2() { return static_cast<int32_t>(offsetof(pMUhWAvzpyKUWHgXjJchbuAaouM_t130B30CBE6F351181E7BC28C2186765AF060CFAB, ___wTqpkSxtmPJzjdXGhVkxSDXgQnn_2)); }
	inline int32_t get_wTqpkSxtmPJzjdXGhVkxSDXgQnn_2() const { return ___wTqpkSxtmPJzjdXGhVkxSDXgQnn_2; }
	inline int32_t* get_address_of_wTqpkSxtmPJzjdXGhVkxSDXgQnn_2() { return &___wTqpkSxtmPJzjdXGhVkxSDXgQnn_2; }
	inline void set_wTqpkSxtmPJzjdXGhVkxSDXgQnn_2(int32_t value)
	{
		___wTqpkSxtmPJzjdXGhVkxSDXgQnn_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PMUHWAVZPYKUWHGXJJCHBUAAOUM_T130B30CBE6F351181E7BC28C2186765AF060CFAB_H
#ifndef QECNDMVGMNOKDCMHDODTTEQMHLZ_T10B096873136F9E3D09A5074339839425CE5BAA7_H
#define QECNDMVGMNOKDCMHDODTTEQMHLZ_T10B096873136F9E3D09A5074339839425CE5BAA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz
struct  qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7  : public RuntimeObject
{
public:
	// Rewired.Data.UserData Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::FEikJUTNOoLkorMnhCrDzsJJboh
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___FEikJUTNOoLkorMnhCrDzsJJboh_0;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::SgTbKBJGpDdiuIuuGINVETpHRFVa
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___SgTbKBJGpDdiuIuuGINVETpHRFVa_1;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::GNkuqHIdiyAmglJvHSiuApKqTTr
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___GNkuqHIdiyAmglJvHSiuApKqTTr_2;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::ApJgBPKxhYzRTTCtQIlUvChiDHh
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___ApJgBPKxhYzRTTCtQIlUvChiDHh_3;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::ztRGUsKePehkfkkfXQpcYZPiSlUp
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___ztRGUsKePehkfkkfXQpcYZPiSlUp_4;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::FkpzToLDwFusqNEnwaZnCinExMY
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___FkpzToLDwFusqNEnwaZnCinExMY_5;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::DXKpFEwfUTAbcrnFSNdZiDEEYca
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___DXKpFEwfUTAbcrnFSNdZiDEEYca_6;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::fanCAtBiATkpaRifdlVehGCddkSD
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___fanCAtBiATkpaRifdlVehGCddkSD_7;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::sIsogoYFBHcssSFmtGLudsUedfW
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___sIsogoYFBHcssSFmtGLudsUedfW_8;
	// System.Func`2<Rewired.ControllerType,System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB>> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::waGTpGPLWBJjQDZqHaDpWKDGDvA
	Func_2_tEE11FFC8EA9AA3ABD47DD780F6118C2F7C71F883 * ___waGTpGPLWBJjQDZqHaDpWKDGDvA_9;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::IIhzuPhpMWHpoDhTFTtrmnVgGEEy
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___IIhzuPhpMWHpoDhTFTtrmnVgGEEy_10;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::PYJejRfBCKYfFkxfuPgkLIGZSQEO
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___PYJejRfBCKYfFkxfuPgkLIGZSQEO_11;
	// System.Collections.Generic.List`1<Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::SJlgjYZrCeAMyfhIkZhzFBwFBASl
	List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * ___SJlgjYZrCeAMyfhIkZhzFBwFBASl_12;

public:
	inline static int32_t get_offset_of_FEikJUTNOoLkorMnhCrDzsJJboh_0() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___FEikJUTNOoLkorMnhCrDzsJJboh_0)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_FEikJUTNOoLkorMnhCrDzsJJboh_0() const { return ___FEikJUTNOoLkorMnhCrDzsJJboh_0; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_FEikJUTNOoLkorMnhCrDzsJJboh_0() { return &___FEikJUTNOoLkorMnhCrDzsJJboh_0; }
	inline void set_FEikJUTNOoLkorMnhCrDzsJJboh_0(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___FEikJUTNOoLkorMnhCrDzsJJboh_0 = value;
		Il2CppCodeGenWriteBarrier((&___FEikJUTNOoLkorMnhCrDzsJJboh_0), value);
	}

	inline static int32_t get_offset_of_SgTbKBJGpDdiuIuuGINVETpHRFVa_1() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___SgTbKBJGpDdiuIuuGINVETpHRFVa_1)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_SgTbKBJGpDdiuIuuGINVETpHRFVa_1() const { return ___SgTbKBJGpDdiuIuuGINVETpHRFVa_1; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_SgTbKBJGpDdiuIuuGINVETpHRFVa_1() { return &___SgTbKBJGpDdiuIuuGINVETpHRFVa_1; }
	inline void set_SgTbKBJGpDdiuIuuGINVETpHRFVa_1(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___SgTbKBJGpDdiuIuuGINVETpHRFVa_1 = value;
		Il2CppCodeGenWriteBarrier((&___SgTbKBJGpDdiuIuuGINVETpHRFVa_1), value);
	}

	inline static int32_t get_offset_of_GNkuqHIdiyAmglJvHSiuApKqTTr_2() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___GNkuqHIdiyAmglJvHSiuApKqTTr_2)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_GNkuqHIdiyAmglJvHSiuApKqTTr_2() const { return ___GNkuqHIdiyAmglJvHSiuApKqTTr_2; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_GNkuqHIdiyAmglJvHSiuApKqTTr_2() { return &___GNkuqHIdiyAmglJvHSiuApKqTTr_2; }
	inline void set_GNkuqHIdiyAmglJvHSiuApKqTTr_2(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___GNkuqHIdiyAmglJvHSiuApKqTTr_2 = value;
		Il2CppCodeGenWriteBarrier((&___GNkuqHIdiyAmglJvHSiuApKqTTr_2), value);
	}

	inline static int32_t get_offset_of_ApJgBPKxhYzRTTCtQIlUvChiDHh_3() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___ApJgBPKxhYzRTTCtQIlUvChiDHh_3)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_ApJgBPKxhYzRTTCtQIlUvChiDHh_3() const { return ___ApJgBPKxhYzRTTCtQIlUvChiDHh_3; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_ApJgBPKxhYzRTTCtQIlUvChiDHh_3() { return &___ApJgBPKxhYzRTTCtQIlUvChiDHh_3; }
	inline void set_ApJgBPKxhYzRTTCtQIlUvChiDHh_3(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___ApJgBPKxhYzRTTCtQIlUvChiDHh_3 = value;
		Il2CppCodeGenWriteBarrier((&___ApJgBPKxhYzRTTCtQIlUvChiDHh_3), value);
	}

	inline static int32_t get_offset_of_ztRGUsKePehkfkkfXQpcYZPiSlUp_4() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___ztRGUsKePehkfkkfXQpcYZPiSlUp_4)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_ztRGUsKePehkfkkfXQpcYZPiSlUp_4() const { return ___ztRGUsKePehkfkkfXQpcYZPiSlUp_4; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_ztRGUsKePehkfkkfXQpcYZPiSlUp_4() { return &___ztRGUsKePehkfkkfXQpcYZPiSlUp_4; }
	inline void set_ztRGUsKePehkfkkfXQpcYZPiSlUp_4(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___ztRGUsKePehkfkkfXQpcYZPiSlUp_4 = value;
		Il2CppCodeGenWriteBarrier((&___ztRGUsKePehkfkkfXQpcYZPiSlUp_4), value);
	}

	inline static int32_t get_offset_of_FkpzToLDwFusqNEnwaZnCinExMY_5() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___FkpzToLDwFusqNEnwaZnCinExMY_5)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_FkpzToLDwFusqNEnwaZnCinExMY_5() const { return ___FkpzToLDwFusqNEnwaZnCinExMY_5; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_FkpzToLDwFusqNEnwaZnCinExMY_5() { return &___FkpzToLDwFusqNEnwaZnCinExMY_5; }
	inline void set_FkpzToLDwFusqNEnwaZnCinExMY_5(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___FkpzToLDwFusqNEnwaZnCinExMY_5 = value;
		Il2CppCodeGenWriteBarrier((&___FkpzToLDwFusqNEnwaZnCinExMY_5), value);
	}

	inline static int32_t get_offset_of_DXKpFEwfUTAbcrnFSNdZiDEEYca_6() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___DXKpFEwfUTAbcrnFSNdZiDEEYca_6)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_DXKpFEwfUTAbcrnFSNdZiDEEYca_6() const { return ___DXKpFEwfUTAbcrnFSNdZiDEEYca_6; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_DXKpFEwfUTAbcrnFSNdZiDEEYca_6() { return &___DXKpFEwfUTAbcrnFSNdZiDEEYca_6; }
	inline void set_DXKpFEwfUTAbcrnFSNdZiDEEYca_6(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___DXKpFEwfUTAbcrnFSNdZiDEEYca_6 = value;
		Il2CppCodeGenWriteBarrier((&___DXKpFEwfUTAbcrnFSNdZiDEEYca_6), value);
	}

	inline static int32_t get_offset_of_fanCAtBiATkpaRifdlVehGCddkSD_7() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___fanCAtBiATkpaRifdlVehGCddkSD_7)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_fanCAtBiATkpaRifdlVehGCddkSD_7() const { return ___fanCAtBiATkpaRifdlVehGCddkSD_7; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_fanCAtBiATkpaRifdlVehGCddkSD_7() { return &___fanCAtBiATkpaRifdlVehGCddkSD_7; }
	inline void set_fanCAtBiATkpaRifdlVehGCddkSD_7(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___fanCAtBiATkpaRifdlVehGCddkSD_7 = value;
		Il2CppCodeGenWriteBarrier((&___fanCAtBiATkpaRifdlVehGCddkSD_7), value);
	}

	inline static int32_t get_offset_of_sIsogoYFBHcssSFmtGLudsUedfW_8() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___sIsogoYFBHcssSFmtGLudsUedfW_8)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_sIsogoYFBHcssSFmtGLudsUedfW_8() const { return ___sIsogoYFBHcssSFmtGLudsUedfW_8; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_sIsogoYFBHcssSFmtGLudsUedfW_8() { return &___sIsogoYFBHcssSFmtGLudsUedfW_8; }
	inline void set_sIsogoYFBHcssSFmtGLudsUedfW_8(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___sIsogoYFBHcssSFmtGLudsUedfW_8 = value;
		Il2CppCodeGenWriteBarrier((&___sIsogoYFBHcssSFmtGLudsUedfW_8), value);
	}

	inline static int32_t get_offset_of_waGTpGPLWBJjQDZqHaDpWKDGDvA_9() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___waGTpGPLWBJjQDZqHaDpWKDGDvA_9)); }
	inline Func_2_tEE11FFC8EA9AA3ABD47DD780F6118C2F7C71F883 * get_waGTpGPLWBJjQDZqHaDpWKDGDvA_9() const { return ___waGTpGPLWBJjQDZqHaDpWKDGDvA_9; }
	inline Func_2_tEE11FFC8EA9AA3ABD47DD780F6118C2F7C71F883 ** get_address_of_waGTpGPLWBJjQDZqHaDpWKDGDvA_9() { return &___waGTpGPLWBJjQDZqHaDpWKDGDvA_9; }
	inline void set_waGTpGPLWBJjQDZqHaDpWKDGDvA_9(Func_2_tEE11FFC8EA9AA3ABD47DD780F6118C2F7C71F883 * value)
	{
		___waGTpGPLWBJjQDZqHaDpWKDGDvA_9 = value;
		Il2CppCodeGenWriteBarrier((&___waGTpGPLWBJjQDZqHaDpWKDGDvA_9), value);
	}

	inline static int32_t get_offset_of_IIhzuPhpMWHpoDhTFTtrmnVgGEEy_10() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___IIhzuPhpMWHpoDhTFTtrmnVgGEEy_10)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_IIhzuPhpMWHpoDhTFTtrmnVgGEEy_10() const { return ___IIhzuPhpMWHpoDhTFTtrmnVgGEEy_10; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_IIhzuPhpMWHpoDhTFTtrmnVgGEEy_10() { return &___IIhzuPhpMWHpoDhTFTtrmnVgGEEy_10; }
	inline void set_IIhzuPhpMWHpoDhTFTtrmnVgGEEy_10(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___IIhzuPhpMWHpoDhTFTtrmnVgGEEy_10 = value;
		Il2CppCodeGenWriteBarrier((&___IIhzuPhpMWHpoDhTFTtrmnVgGEEy_10), value);
	}

	inline static int32_t get_offset_of_PYJejRfBCKYfFkxfuPgkLIGZSQEO_11() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___PYJejRfBCKYfFkxfuPgkLIGZSQEO_11)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_PYJejRfBCKYfFkxfuPgkLIGZSQEO_11() const { return ___PYJejRfBCKYfFkxfuPgkLIGZSQEO_11; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_PYJejRfBCKYfFkxfuPgkLIGZSQEO_11() { return &___PYJejRfBCKYfFkxfuPgkLIGZSQEO_11; }
	inline void set_PYJejRfBCKYfFkxfuPgkLIGZSQEO_11(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___PYJejRfBCKYfFkxfuPgkLIGZSQEO_11 = value;
		Il2CppCodeGenWriteBarrier((&___PYJejRfBCKYfFkxfuPgkLIGZSQEO_11), value);
	}

	inline static int32_t get_offset_of_SJlgjYZrCeAMyfhIkZhzFBwFBASl_12() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7, ___SJlgjYZrCeAMyfhIkZhzFBwFBASl_12)); }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * get_SJlgjYZrCeAMyfhIkZhzFBwFBASl_12() const { return ___SJlgjYZrCeAMyfhIkZhzFBwFBASl_12; }
	inline List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 ** get_address_of_SJlgjYZrCeAMyfhIkZhzFBwFBASl_12() { return &___SJlgjYZrCeAMyfhIkZhzFBwFBASl_12; }
	inline void set_SJlgjYZrCeAMyfhIkZhzFBwFBASl_12(List_1_t7589BD93B4C339F1D39305986FF54D3512F2B997 * value)
	{
		___SJlgjYZrCeAMyfhIkZhzFBwFBASl_12 = value;
		Il2CppCodeGenWriteBarrier((&___SJlgjYZrCeAMyfhIkZhzFBwFBASl_12), value);
	}
};

struct qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7_StaticFields
{
public:
	// System.Func`3<Rewired.Data.Player_Editor_Mapping,System.Collections.Generic.IList`1<Rewired.Data.Player_Editor_Mapping>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::zHgvSkrXnaeLQooQlFrBzVUbOvG
	Func_3_t90F2989D2602006A5B0F44BB59F5358F8BE452CF * ___zHgvSkrXnaeLQooQlFrBzVUbOvG_13;
	// System.Func`3<Rewired.Data.Player_Editor_CreateControllerInfo,System.Collections.Generic.IList`1<Rewired.Data.Player_Editor_CreateControllerInfo>,System.Int32> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz::seZfuqFtvNbnRBJoZFKyZHfHiLTi
	Func_3_tFDE88F7817E43EC72692E815C47511E6199FF9B9 * ___seZfuqFtvNbnRBJoZFKyZHfHiLTi_14;

public:
	inline static int32_t get_offset_of_zHgvSkrXnaeLQooQlFrBzVUbOvG_13() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7_StaticFields, ___zHgvSkrXnaeLQooQlFrBzVUbOvG_13)); }
	inline Func_3_t90F2989D2602006A5B0F44BB59F5358F8BE452CF * get_zHgvSkrXnaeLQooQlFrBzVUbOvG_13() const { return ___zHgvSkrXnaeLQooQlFrBzVUbOvG_13; }
	inline Func_3_t90F2989D2602006A5B0F44BB59F5358F8BE452CF ** get_address_of_zHgvSkrXnaeLQooQlFrBzVUbOvG_13() { return &___zHgvSkrXnaeLQooQlFrBzVUbOvG_13; }
	inline void set_zHgvSkrXnaeLQooQlFrBzVUbOvG_13(Func_3_t90F2989D2602006A5B0F44BB59F5358F8BE452CF * value)
	{
		___zHgvSkrXnaeLQooQlFrBzVUbOvG_13 = value;
		Il2CppCodeGenWriteBarrier((&___zHgvSkrXnaeLQooQlFrBzVUbOvG_13), value);
	}

	inline static int32_t get_offset_of_seZfuqFtvNbnRBJoZFKyZHfHiLTi_14() { return static_cast<int32_t>(offsetof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7_StaticFields, ___seZfuqFtvNbnRBJoZFKyZHfHiLTi_14)); }
	inline Func_3_tFDE88F7817E43EC72692E815C47511E6199FF9B9 * get_seZfuqFtvNbnRBJoZFKyZHfHiLTi_14() const { return ___seZfuqFtvNbnRBJoZFKyZHfHiLTi_14; }
	inline Func_3_tFDE88F7817E43EC72692E815C47511E6199FF9B9 ** get_address_of_seZfuqFtvNbnRBJoZFKyZHfHiLTi_14() { return &___seZfuqFtvNbnRBJoZFKyZHfHiLTi_14; }
	inline void set_seZfuqFtvNbnRBJoZFKyZHfHiLTi_14(Func_3_tFDE88F7817E43EC72692E815C47511E6199FF9B9 * value)
	{
		___seZfuqFtvNbnRBJoZFKyZHfHiLTi_14 = value;
		Il2CppCodeGenWriteBarrier((&___seZfuqFtvNbnRBJoZFKyZHfHiLTi_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QECNDMVGMNOKDCMHDODTTEQMHLZ_T10B096873136F9E3D09A5074339839425CE5BAA7_H
#ifndef EVPCCGNNOHSVPBMZFULUQTOAWXL_TC77C62C648E18669A1736909896107EF73741A96_H
#define EVPCCGNNOHSVPBMZFULUQTOAWXL_TC77C62C648E18669A1736909896107EF73741A96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_EVpcCGNnohSVpBMzFUluQToAwxL
struct  EVpcCGNnohSVpBMzFUluQToAwxL_tC77C62C648E18669A1736909896107EF73741A96  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_NtFyIiXIvuJrPzjtoNMcQcjFQhu Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_EVpcCGNnohSVpBMzFUluQToAwxL::ICgDBhwsJOqNdttFCWCDIQrsJUJ
	NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F * ___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_EVpcCGNnohSVpBMzFUluQToAwxL::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_EVpcCGNnohSVpBMzFUluQToAwxL::MlHuhkxTPKDycqbMwTBkBIGihuU
	int32_t ___MlHuhkxTPKDycqbMwTBkBIGihuU_2;

public:
	inline static int32_t get_offset_of_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0() { return static_cast<int32_t>(offsetof(EVpcCGNnohSVpBMzFUluQToAwxL_tC77C62C648E18669A1736909896107EF73741A96, ___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0)); }
	inline NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F * get_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0() const { return ___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0; }
	inline NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F ** get_address_of_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0() { return &___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0; }
	inline void set_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0(NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F * value)
	{
		___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0 = value;
		Il2CppCodeGenWriteBarrier((&___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(EVpcCGNnohSVpBMzFUluQToAwxL_tC77C62C648E18669A1736909896107EF73741A96, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_MlHuhkxTPKDycqbMwTBkBIGihuU_2() { return static_cast<int32_t>(offsetof(EVpcCGNnohSVpBMzFUluQToAwxL_tC77C62C648E18669A1736909896107EF73741A96, ___MlHuhkxTPKDycqbMwTBkBIGihuU_2)); }
	inline int32_t get_MlHuhkxTPKDycqbMwTBkBIGihuU_2() const { return ___MlHuhkxTPKDycqbMwTBkBIGihuU_2; }
	inline int32_t* get_address_of_MlHuhkxTPKDycqbMwTBkBIGihuU_2() { return &___MlHuhkxTPKDycqbMwTBkBIGihuU_2; }
	inline void set_MlHuhkxTPKDycqbMwTBkBIGihuU_2(int32_t value)
	{
		___MlHuhkxTPKDycqbMwTBkBIGihuU_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVPCCGNNOHSVPBMZFULUQTOAWXL_TC77C62C648E18669A1736909896107EF73741A96_H
#ifndef FJLRNFWKVQISHLGCYOOXTXSMLRS_TA65FA0A7F76AD34694505AB31036F0E37BBB9913_H
#define FJLRNFWKVQISHLGCYOOXTXSMLRS_TA65FA0A7F76AD34694505AB31036F0E37BBB9913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_FJLrnfWkvQiShLGCYOOXtXsMLrS
struct  FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.Data.ControllerMapEnabler_RuleSet_Editor> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_FJLrnfWkvQiShLGCYOOXtXsMLrS::RnYngjobuYlteiRkTweuGcJpYtu
	DdxYPCrLAHFMStEcIPuCjbnFlwc_tEFED26C63F0010C91EBDCEC819B13B77A015CA22 * ___RnYngjobuYlteiRkTweuGcJpYtu_0;

public:
	inline static int32_t get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_0() { return static_cast<int32_t>(offsetof(FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913, ___RnYngjobuYlteiRkTweuGcJpYtu_0)); }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_tEFED26C63F0010C91EBDCEC819B13B77A015CA22 * get_RnYngjobuYlteiRkTweuGcJpYtu_0() const { return ___RnYngjobuYlteiRkTweuGcJpYtu_0; }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_tEFED26C63F0010C91EBDCEC819B13B77A015CA22 ** get_address_of_RnYngjobuYlteiRkTweuGcJpYtu_0() { return &___RnYngjobuYlteiRkTweuGcJpYtu_0; }
	inline void set_RnYngjobuYlteiRkTweuGcJpYtu_0(DdxYPCrLAHFMStEcIPuCjbnFlwc_tEFED26C63F0010C91EBDCEC819B13B77A015CA22 * value)
	{
		___RnYngjobuYlteiRkTweuGcJpYtu_0 = value;
		Il2CppCodeGenWriteBarrier((&___RnYngjobuYlteiRkTweuGcJpYtu_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FJLRNFWKVQISHLGCYOOXTXSMLRS_TA65FA0A7F76AD34694505AB31036F0E37BBB9913_H
#ifndef IJZCRBELHIQHIQBKGKHQJYLIVVX_TD2B567F857B4C0549850B687B47C11EB8A35A2C5_H
#define IJZCRBELHIQHIQBKGKHQJYLIVVX_TD2B567F857B4C0549850B687B47C11EB8A35A2C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_IJZcRBeLhIqHiqbkGKHQJyLiVVX
struct  IJZcRBeLhIqHiqbkGKHQJyLiVVX_tD2B567F857B4C0549850B687B47C11EB8A35A2C5  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_FJLrnfWkvQiShLGCYOOXtXsMLrS Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_IJZcRBeLhIqHiqbkGKHQJyLiVVX::IgEDSugxpNXIhdXRDUlKvxxKtPMB
	FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913 * ___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_IJZcRBeLhIqHiqbkGKHQJyLiVVX::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_IJZcRBeLhIqHiqbkGKHQJyLiVVX::xntvcYLUoptzzWTrXAfZKQVzOVK
	int32_t ___xntvcYLUoptzzWTrXAfZKQVzOVK_2;

public:
	inline static int32_t get_offset_of_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0() { return static_cast<int32_t>(offsetof(IJZcRBeLhIqHiqbkGKHQJyLiVVX_tD2B567F857B4C0549850B687B47C11EB8A35A2C5, ___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0)); }
	inline FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913 * get_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0() const { return ___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0; }
	inline FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913 ** get_address_of_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0() { return &___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0; }
	inline void set_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0(FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913 * value)
	{
		___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0 = value;
		Il2CppCodeGenWriteBarrier((&___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(IJZcRBeLhIqHiqbkGKHQJyLiVVX_tD2B567F857B4C0549850B687B47C11EB8A35A2C5, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2() { return static_cast<int32_t>(offsetof(IJZcRBeLhIqHiqbkGKHQJyLiVVX_tD2B567F857B4C0549850B687B47C11EB8A35A2C5, ___xntvcYLUoptzzWTrXAfZKQVzOVK_2)); }
	inline int32_t get_xntvcYLUoptzzWTrXAfZKQVzOVK_2() const { return ___xntvcYLUoptzzWTrXAfZKQVzOVK_2; }
	inline int32_t* get_address_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2() { return &___xntvcYLUoptzzWTrXAfZKQVzOVK_2; }
	inline void set_xntvcYLUoptzzWTrXAfZKQVzOVK_2(int32_t value)
	{
		___xntvcYLUoptzzWTrXAfZKQVzOVK_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IJZCRBELHIQHIQBKGKHQJYLIVVX_TD2B567F857B4C0549850B687B47C11EB8A35A2C5_H
#ifndef NTFYIIXIVUJRPZJTONMCQCJFQHU_TFB08F3D16A43B9CB4CBB14E22D181ED812C2976F_H
#define NTFYIIXIVUJRPZJTONMCQCJFQHU_TFB08F3D16A43B9CB4CBB14E22D181ED812C2976F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_NtFyIiXIvuJrPzjtoNMcQcjFQhu
struct  NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_NtFyIiXIvuJrPzjtoNMcQcjFQhu::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.Data.Player_Editor> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_NtFyIiXIvuJrPzjtoNMcQcjFQhu::RnYngjobuYlteiRkTweuGcJpYtu
	DdxYPCrLAHFMStEcIPuCjbnFlwc_tFA9792A0CF5145B0C7873E7868C79A0586B9A796 * ___RnYngjobuYlteiRkTweuGcJpYtu_1;

public:
	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return static_cast<int32_t>(offsetof(NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F, ___NxecGThpOUHPMAqQltxuibhjsBi_0)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_0() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return &___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_0(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_0 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_0), value);
	}

	inline static int32_t get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_1() { return static_cast<int32_t>(offsetof(NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F, ___RnYngjobuYlteiRkTweuGcJpYtu_1)); }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_tFA9792A0CF5145B0C7873E7868C79A0586B9A796 * get_RnYngjobuYlteiRkTweuGcJpYtu_1() const { return ___RnYngjobuYlteiRkTweuGcJpYtu_1; }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_tFA9792A0CF5145B0C7873E7868C79A0586B9A796 ** get_address_of_RnYngjobuYlteiRkTweuGcJpYtu_1() { return &___RnYngjobuYlteiRkTweuGcJpYtu_1; }
	inline void set_RnYngjobuYlteiRkTweuGcJpYtu_1(DdxYPCrLAHFMStEcIPuCjbnFlwc_tFA9792A0CF5145B0C7873E7868C79A0586B9A796 * value)
	{
		___RnYngjobuYlteiRkTweuGcJpYtu_1 = value;
		Il2CppCodeGenWriteBarrier((&___RnYngjobuYlteiRkTweuGcJpYtu_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTFYIIXIVUJRPZJTONMCQCJFQHU_TFB08F3D16A43B9CB4CBB14E22D181ED812C2976F_H
#ifndef NAPQOJMPLJOJHVLKAOZSDWGXDDD_T9F140326E9435B9C8BA3B9097F963BDEFEE5FBA6_H
#define NAPQOJMPLJOJHVLKAOZSDWGXDDD_T9F140326E9435B9C8BA3B9097F963BDEFEE5FBA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_NtFyIiXIvuJrPzjtoNMcQcjFQhu_NAPqojmPLJojhvLKAoZSDwgXdDD
struct  NAPqojmPLJojhvLKAoZSDwgXdDD_t9F140326E9435B9C8BA3B9097F963BDEFEE5FBA6  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_NtFyIiXIvuJrPzjtoNMcQcjFQhu Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_NtFyIiXIvuJrPzjtoNMcQcjFQhu_NAPqojmPLJojhvLKAoZSDwgXdDD::ICgDBhwsJOqNdttFCWCDIQrsJUJ
	NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F * ___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_NtFyIiXIvuJrPzjtoNMcQcjFQhu_NAPqojmPLJojhvLKAoZSDwgXdDD::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// Rewired.Data.Player_Editor_Mapping Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_NtFyIiXIvuJrPzjtoNMcQcjFQhu_NAPqojmPLJojhvLKAoZSDwgXdDD::XaepNDmdlDDRBgSPSIhKadPGxnPE
	Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26 * ___XaepNDmdlDDRBgSPSIhKadPGxnPE_2;

public:
	inline static int32_t get_offset_of_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0() { return static_cast<int32_t>(offsetof(NAPqojmPLJojhvLKAoZSDwgXdDD_t9F140326E9435B9C8BA3B9097F963BDEFEE5FBA6, ___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0)); }
	inline NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F * get_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0() const { return ___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0; }
	inline NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F ** get_address_of_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0() { return &___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0; }
	inline void set_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0(NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F * value)
	{
		___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0 = value;
		Il2CppCodeGenWriteBarrier((&___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(NAPqojmPLJojhvLKAoZSDwgXdDD_t9F140326E9435B9C8BA3B9097F963BDEFEE5FBA6, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_2() { return static_cast<int32_t>(offsetof(NAPqojmPLJojhvLKAoZSDwgXdDD_t9F140326E9435B9C8BA3B9097F963BDEFEE5FBA6, ___XaepNDmdlDDRBgSPSIhKadPGxnPE_2)); }
	inline Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26 * get_XaepNDmdlDDRBgSPSIhKadPGxnPE_2() const { return ___XaepNDmdlDDRBgSPSIhKadPGxnPE_2; }
	inline Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26 ** get_address_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_2() { return &___XaepNDmdlDDRBgSPSIhKadPGxnPE_2; }
	inline void set_XaepNDmdlDDRBgSPSIhKadPGxnPE_2(Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26 * value)
	{
		___XaepNDmdlDDRBgSPSIhKadPGxnPE_2 = value;
		Il2CppCodeGenWriteBarrier((&___XaepNDmdlDDRBgSPSIhKadPGxnPE_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAPQOJMPLJOJHVLKAOZSDWGXDDD_T9F140326E9435B9C8BA3B9097F963BDEFEE5FBA6_H
#ifndef QAKRASQKWREIYNAAJIKDIYPUMBH_T02E27901404660825F0FBC2F3D61CF0D6647E506_H
#define QAKRASQKWREIYNAAJIKDIYPUMBH_T02E27901404660825F0FBC2F3D61CF0D6647E506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_QakRAsQkwrEIYNAAJIkDIyPUMBh
struct  QakRAsQkwrEIYNAAJIkDIyPUMBh_t02E27901404660825F0FBC2F3D61CF0D6647E506  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_FJLrnfWkvQiShLGCYOOXtXsMLrS Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_QakRAsQkwrEIYNAAJIkDIyPUMBh::IgEDSugxpNXIhdXRDUlKvxxKtPMB
	FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913 * ___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_QakRAsQkwrEIYNAAJIkDIyPUMBh::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_QakRAsQkwrEIYNAAJIkDIyPUMBh::xntvcYLUoptzzWTrXAfZKQVzOVK
	int32_t ___xntvcYLUoptzzWTrXAfZKQVzOVK_2;

public:
	inline static int32_t get_offset_of_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0() { return static_cast<int32_t>(offsetof(QakRAsQkwrEIYNAAJIkDIyPUMBh_t02E27901404660825F0FBC2F3D61CF0D6647E506, ___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0)); }
	inline FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913 * get_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0() const { return ___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0; }
	inline FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913 ** get_address_of_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0() { return &___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0; }
	inline void set_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0(FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913 * value)
	{
		___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0 = value;
		Il2CppCodeGenWriteBarrier((&___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(QakRAsQkwrEIYNAAJIkDIyPUMBh_t02E27901404660825F0FBC2F3D61CF0D6647E506, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2() { return static_cast<int32_t>(offsetof(QakRAsQkwrEIYNAAJIkDIyPUMBh_t02E27901404660825F0FBC2F3D61CF0D6647E506, ___xntvcYLUoptzzWTrXAfZKQVzOVK_2)); }
	inline int32_t get_xntvcYLUoptzzWTrXAfZKQVzOVK_2() const { return ___xntvcYLUoptzzWTrXAfZKQVzOVK_2; }
	inline int32_t* get_address_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2() { return &___xntvcYLUoptzzWTrXAfZKQVzOVK_2; }
	inline void set_xntvcYLUoptzzWTrXAfZKQVzOVK_2(int32_t value)
	{
		___xntvcYLUoptzzWTrXAfZKQVzOVK_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QAKRASQKWREIYNAAJIKDIYPUMBH_T02E27901404660825F0FBC2F3D61CF0D6647E506_H
#ifndef UOLCDNQNMXRLLPVZSJBQSPBQGHT_T277D82B3205FB7B547EA923D355DEF3A47D11B2A_H
#define UOLCDNQNMXRLLPVZSJBQSPBQGHT_T277D82B3205FB7B547EA923D355DEF3A47D11B2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_UOLCDnQnmXrLlpvzsjBqSPbQGht
struct  UOLCDnQnmXrLlpvzsjBqSPbQGht_t277D82B3205FB7B547EA923D355DEF3A47D11B2A  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_UOLCDnQnmXrLlpvzsjBqSPbQGht::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.InputAction> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_UOLCDnQnmXrLlpvzsjBqSPbQGht::RnYngjobuYlteiRkTweuGcJpYtu
	DdxYPCrLAHFMStEcIPuCjbnFlwc_t9FCA482A374CAF40C516A9E02F48B4072BD4D00B * ___RnYngjobuYlteiRkTweuGcJpYtu_1;

public:
	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return static_cast<int32_t>(offsetof(UOLCDnQnmXrLlpvzsjBqSPbQGht_t277D82B3205FB7B547EA923D355DEF3A47D11B2A, ___NxecGThpOUHPMAqQltxuibhjsBi_0)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_0() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_0() { return &___NxecGThpOUHPMAqQltxuibhjsBi_0; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_0(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_0 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_0), value);
	}

	inline static int32_t get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_1() { return static_cast<int32_t>(offsetof(UOLCDnQnmXrLlpvzsjBqSPbQGht_t277D82B3205FB7B547EA923D355DEF3A47D11B2A, ___RnYngjobuYlteiRkTweuGcJpYtu_1)); }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_t9FCA482A374CAF40C516A9E02F48B4072BD4D00B * get_RnYngjobuYlteiRkTweuGcJpYtu_1() const { return ___RnYngjobuYlteiRkTweuGcJpYtu_1; }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_t9FCA482A374CAF40C516A9E02F48B4072BD4D00B ** get_address_of_RnYngjobuYlteiRkTweuGcJpYtu_1() { return &___RnYngjobuYlteiRkTweuGcJpYtu_1; }
	inline void set_RnYngjobuYlteiRkTweuGcJpYtu_1(DdxYPCrLAHFMStEcIPuCjbnFlwc_t9FCA482A374CAF40C516A9E02F48B4072BD4D00B * value)
	{
		___RnYngjobuYlteiRkTweuGcJpYtu_1 = value;
		Il2CppCodeGenWriteBarrier((&___RnYngjobuYlteiRkTweuGcJpYtu_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UOLCDNQNMXRLLPVZSJBQSPBQGHT_T277D82B3205FB7B547EA923D355DEF3A47D11B2A_H
#ifndef UTRIICSKYRYPFAUOIFVUFDLLHYD_T38005E0C747F974B17989189926B46E9401794EE_H
#define UTRIICSKYRYPFAUOIFVUFDLLHYD_T38005E0C747F974B17989189926B46E9401794EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_UTRiicSKYrYpfaUOifvuFDlLHyd
struct  UTRiicSKYrYpfaUOifvuFDlLHyd_t38005E0C747F974B17989189926B46E9401794EE  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_sifvSqThLQGuVclMuQKUhwYRlPoN Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_UTRiicSKYrYpfaUOifvuFDlLHyd::yRBXJeDJiaSvuoZWvBMXqrfBkDz
	sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3 * ___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_UTRiicSKYrYpfaUOifvuFDlLHyd::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_UTRiicSKYrYpfaUOifvuFDlLHyd::xntvcYLUoptzzWTrXAfZKQVzOVK
	int32_t ___xntvcYLUoptzzWTrXAfZKQVzOVK_2;

public:
	inline static int32_t get_offset_of_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0() { return static_cast<int32_t>(offsetof(UTRiicSKYrYpfaUOifvuFDlLHyd_t38005E0C747F974B17989189926B46E9401794EE, ___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0)); }
	inline sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3 * get_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0() const { return ___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0; }
	inline sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3 ** get_address_of_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0() { return &___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0; }
	inline void set_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0(sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3 * value)
	{
		___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0 = value;
		Il2CppCodeGenWriteBarrier((&___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(UTRiicSKYrYpfaUOifvuFDlLHyd_t38005E0C747F974B17989189926B46E9401794EE, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2() { return static_cast<int32_t>(offsetof(UTRiicSKYrYpfaUOifvuFDlLHyd_t38005E0C747F974B17989189926B46E9401794EE, ___xntvcYLUoptzzWTrXAfZKQVzOVK_2)); }
	inline int32_t get_xntvcYLUoptzzWTrXAfZKQVzOVK_2() const { return ___xntvcYLUoptzzWTrXAfZKQVzOVK_2; }
	inline int32_t* get_address_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2() { return &___xntvcYLUoptzzWTrXAfZKQVzOVK_2; }
	inline void set_xntvcYLUoptzzWTrXAfZKQVzOVK_2(int32_t value)
	{
		___xntvcYLUoptzzWTrXAfZKQVzOVK_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTRIICSKYRYPFAUOIFVUFDLLHYD_T38005E0C747F974B17989189926B46E9401794EE_H
#ifndef FRZVLHJWONDTHSZLELOBGYXQDPB_TBF3BC5BE01851163C440E2657BBC5CD6199F8B33_H
#define FRZVLHJWONDTHSZLELOBGYXQDPB_TBF3BC5BE01851163C440E2657BBC5CD6199F8B33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_fRZVLhJwOndThSZLelobGYXQDpb
struct  fRZVLhJwOndThSZLelobGYXQDpb_tBF3BC5BE01851163C440E2657BBC5CD6199F8B33  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_FJLrnfWkvQiShLGCYOOXtXsMLrS Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_fRZVLhJwOndThSZLelobGYXQDpb::IgEDSugxpNXIhdXRDUlKvxxKtPMB
	FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913 * ___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_fRZVLhJwOndThSZLelobGYXQDpb::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_fRZVLhJwOndThSZLelobGYXQDpb::xntvcYLUoptzzWTrXAfZKQVzOVK
	int32_t ___xntvcYLUoptzzWTrXAfZKQVzOVK_2;

public:
	inline static int32_t get_offset_of_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0() { return static_cast<int32_t>(offsetof(fRZVLhJwOndThSZLelobGYXQDpb_tBF3BC5BE01851163C440E2657BBC5CD6199F8B33, ___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0)); }
	inline FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913 * get_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0() const { return ___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0; }
	inline FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913 ** get_address_of_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0() { return &___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0; }
	inline void set_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0(FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913 * value)
	{
		___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0 = value;
		Il2CppCodeGenWriteBarrier((&___IgEDSugxpNXIhdXRDUlKvxxKtPMB_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(fRZVLhJwOndThSZLelobGYXQDpb_tBF3BC5BE01851163C440E2657BBC5CD6199F8B33, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2() { return static_cast<int32_t>(offsetof(fRZVLhJwOndThSZLelobGYXQDpb_tBF3BC5BE01851163C440E2657BBC5CD6199F8B33, ___xntvcYLUoptzzWTrXAfZKQVzOVK_2)); }
	inline int32_t get_xntvcYLUoptzzWTrXAfZKQVzOVK_2() const { return ___xntvcYLUoptzzWTrXAfZKQVzOVK_2; }
	inline int32_t* get_address_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2() { return &___xntvcYLUoptzzWTrXAfZKQVzOVK_2; }
	inline void set_xntvcYLUoptzzWTrXAfZKQVzOVK_2(int32_t value)
	{
		___xntvcYLUoptzzWTrXAfZKQVzOVK_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRZVLHJWONDTHSZLELOBGYXQDPB_TBF3BC5BE01851163C440E2657BBC5CD6199F8B33_H
#ifndef GLSWZMILELUEHTMBVTBSHJZVDFQD_T14C0F6080488C7AEACE9CD4F37A3D8D26F2D852E_H
#define GLSWZMILELUEHTMBVTBSHJZVDFQD_T14C0F6080488C7AEACE9CD4F37A3D8D26F2D852E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_glswZMileLueHtMBVTBsHJZVDFQd
struct  glswZMileLueHtMBVTBsHJZVDFQd_t14C0F6080488C7AEACE9CD4F37A3D8D26F2D852E  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_NtFyIiXIvuJrPzjtoNMcQcjFQhu Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_glswZMileLueHtMBVTBsHJZVDFQd::ICgDBhwsJOqNdttFCWCDIQrsJUJ
	NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F * ___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_glswZMileLueHtMBVTBsHJZVDFQd::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// Rewired.Data.Player_Editor_CreateControllerInfo Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_glswZMileLueHtMBVTBsHJZVDFQd::doEmDrDRMAhaULBVLOODFaMXMSO
	CreateControllerInfo_t1174758D9532AC61484D41E38E94D3C7CF852CC4 * ___doEmDrDRMAhaULBVLOODFaMXMSO_2;

public:
	inline static int32_t get_offset_of_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0() { return static_cast<int32_t>(offsetof(glswZMileLueHtMBVTBsHJZVDFQd_t14C0F6080488C7AEACE9CD4F37A3D8D26F2D852E, ___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0)); }
	inline NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F * get_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0() const { return ___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0; }
	inline NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F ** get_address_of_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0() { return &___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0; }
	inline void set_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0(NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F * value)
	{
		___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0 = value;
		Il2CppCodeGenWriteBarrier((&___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(glswZMileLueHtMBVTBsHJZVDFQd_t14C0F6080488C7AEACE9CD4F37A3D8D26F2D852E, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_doEmDrDRMAhaULBVLOODFaMXMSO_2() { return static_cast<int32_t>(offsetof(glswZMileLueHtMBVTBsHJZVDFQd_t14C0F6080488C7AEACE9CD4F37A3D8D26F2D852E, ___doEmDrDRMAhaULBVLOODFaMXMSO_2)); }
	inline CreateControllerInfo_t1174758D9532AC61484D41E38E94D3C7CF852CC4 * get_doEmDrDRMAhaULBVLOODFaMXMSO_2() const { return ___doEmDrDRMAhaULBVLOODFaMXMSO_2; }
	inline CreateControllerInfo_t1174758D9532AC61484D41E38E94D3C7CF852CC4 ** get_address_of_doEmDrDRMAhaULBVLOODFaMXMSO_2() { return &___doEmDrDRMAhaULBVLOODFaMXMSO_2; }
	inline void set_doEmDrDRMAhaULBVLOODFaMXMSO_2(CreateControllerInfo_t1174758D9532AC61484D41E38E94D3C7CF852CC4 * value)
	{
		___doEmDrDRMAhaULBVLOODFaMXMSO_2 = value;
		Il2CppCodeGenWriteBarrier((&___doEmDrDRMAhaULBVLOODFaMXMSO_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLSWZMILELUEHTMBVTBSHJZVDFQD_T14C0F6080488C7AEACE9CD4F37A3D8D26F2D852E_H
#ifndef ORVSYVICQUXENEQPDJENHPGXHOSD_T9E3A101FD193F16EFA0AB47C028A96B8B21A23E7_H
#define ORVSYVICQUXENEQPDJENHPGXHOSD_T9E3A101FD193F16EFA0AB47C028A96B8B21A23E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_orvsyVicQUXENeQpdjenHPgXhoSd
struct  orvsyVicQUXENeQpdjenHPgXhoSd_t9E3A101FD193F16EFA0AB47C028A96B8B21A23E7  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_sifvSqThLQGuVclMuQKUhwYRlPoN Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_orvsyVicQUXENeQpdjenHPgXhoSd::yRBXJeDJiaSvuoZWvBMXqrfBkDz
	sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3 * ___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_orvsyVicQUXENeQpdjenHPgXhoSd::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_orvsyVicQUXENeQpdjenHPgXhoSd::xntvcYLUoptzzWTrXAfZKQVzOVK
	int32_t ___xntvcYLUoptzzWTrXAfZKQVzOVK_2;

public:
	inline static int32_t get_offset_of_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0() { return static_cast<int32_t>(offsetof(orvsyVicQUXENeQpdjenHPgXhoSd_t9E3A101FD193F16EFA0AB47C028A96B8B21A23E7, ___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0)); }
	inline sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3 * get_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0() const { return ___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0; }
	inline sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3 ** get_address_of_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0() { return &___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0; }
	inline void set_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0(sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3 * value)
	{
		___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0 = value;
		Il2CppCodeGenWriteBarrier((&___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(orvsyVicQUXENeQpdjenHPgXhoSd_t9E3A101FD193F16EFA0AB47C028A96B8B21A23E7, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2() { return static_cast<int32_t>(offsetof(orvsyVicQUXENeQpdjenHPgXhoSd_t9E3A101FD193F16EFA0AB47C028A96B8B21A23E7, ___xntvcYLUoptzzWTrXAfZKQVzOVK_2)); }
	inline int32_t get_xntvcYLUoptzzWTrXAfZKQVzOVK_2() const { return ___xntvcYLUoptzzWTrXAfZKQVzOVK_2; }
	inline int32_t* get_address_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2() { return &___xntvcYLUoptzzWTrXAfZKQVzOVK_2; }
	inline void set_xntvcYLUoptzzWTrXAfZKQVzOVK_2(int32_t value)
	{
		___xntvcYLUoptzzWTrXAfZKQVzOVK_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORVSYVICQUXENEQPDJENHPGXHOSD_T9E3A101FD193F16EFA0AB47C028A96B8B21A23E7_H
#ifndef SIFVSQTHLQGUVCLMUQKUHWYRLPON_T212C7580AAB5431C3C054AD4FD560157AA5531E3_H
#define SIFVSQTHLQGUVCLMUQKUHWYRLPON_T212C7580AAB5431C3C054AD4FD560157AA5531E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_sifvSqThLQGuVclMuQKUhwYRlPoN
struct  sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_DdxYPCrLAHFMStEcIPuCjbnFlwc<Rewired.Data.ControllerMapLayoutManager_RuleSet_Editor> Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_sifvSqThLQGuVclMuQKUhwYRlPoN::RnYngjobuYlteiRkTweuGcJpYtu
	DdxYPCrLAHFMStEcIPuCjbnFlwc_t37E43D3555409AECD07AC85061A407F9A6C1EA3A * ___RnYngjobuYlteiRkTweuGcJpYtu_0;

public:
	inline static int32_t get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_0() { return static_cast<int32_t>(offsetof(sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3, ___RnYngjobuYlteiRkTweuGcJpYtu_0)); }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_t37E43D3555409AECD07AC85061A407F9A6C1EA3A * get_RnYngjobuYlteiRkTweuGcJpYtu_0() const { return ___RnYngjobuYlteiRkTweuGcJpYtu_0; }
	inline DdxYPCrLAHFMStEcIPuCjbnFlwc_t37E43D3555409AECD07AC85061A407F9A6C1EA3A ** get_address_of_RnYngjobuYlteiRkTweuGcJpYtu_0() { return &___RnYngjobuYlteiRkTweuGcJpYtu_0; }
	inline void set_RnYngjobuYlteiRkTweuGcJpYtu_0(DdxYPCrLAHFMStEcIPuCjbnFlwc_t37E43D3555409AECD07AC85061A407F9A6C1EA3A * value)
	{
		___RnYngjobuYlteiRkTweuGcJpYtu_0 = value;
		Il2CppCodeGenWriteBarrier((&___RnYngjobuYlteiRkTweuGcJpYtu_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIFVSQTHLQGUVCLMUQKUHWYRLPON_T212C7580AAB5431C3C054AD4FD560157AA5531E3_H
#ifndef VKIQITAMANNUWKINHFAJYASZEWR_T92F5D611EB59564DA780613AD88070B0010B1B78_H
#define VKIQITAMANNUWKINHFAJYASZEWR_T92F5D611EB59564DA780613AD88070B0010B1B78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_vkIQItAMANnUwKiNhfajyaszEWR
struct  vkIQItAMANnUwKiNhfajyaszEWR_t92F5D611EB59564DA780613AD88070B0010B1B78  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_NtFyIiXIvuJrPzjtoNMcQcjFQhu Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_vkIQItAMANnUwKiNhfajyaszEWR::ICgDBhwsJOqNdttFCWCDIQrsJUJ
	NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F * ___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_vkIQItAMANnUwKiNhfajyaszEWR::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_vkIQItAMANnUwKiNhfajyaszEWR::MlHuhkxTPKDycqbMwTBkBIGihuU
	int32_t ___MlHuhkxTPKDycqbMwTBkBIGihuU_2;

public:
	inline static int32_t get_offset_of_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0() { return static_cast<int32_t>(offsetof(vkIQItAMANnUwKiNhfajyaszEWR_t92F5D611EB59564DA780613AD88070B0010B1B78, ___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0)); }
	inline NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F * get_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0() const { return ___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0; }
	inline NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F ** get_address_of_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0() { return &___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0; }
	inline void set_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0(NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F * value)
	{
		___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0 = value;
		Il2CppCodeGenWriteBarrier((&___ICgDBhwsJOqNdttFCWCDIQrsJUJ_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(vkIQItAMANnUwKiNhfajyaszEWR_t92F5D611EB59564DA780613AD88070B0010B1B78, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_MlHuhkxTPKDycqbMwTBkBIGihuU_2() { return static_cast<int32_t>(offsetof(vkIQItAMANnUwKiNhfajyaszEWR_t92F5D611EB59564DA780613AD88070B0010B1B78, ___MlHuhkxTPKDycqbMwTBkBIGihuU_2)); }
	inline int32_t get_MlHuhkxTPKDycqbMwTBkBIGihuU_2() const { return ___MlHuhkxTPKDycqbMwTBkBIGihuU_2; }
	inline int32_t* get_address_of_MlHuhkxTPKDycqbMwTBkBIGihuU_2() { return &___MlHuhkxTPKDycqbMwTBkBIGihuU_2; }
	inline void set_MlHuhkxTPKDycqbMwTBkBIGihuU_2(int32_t value)
	{
		___MlHuhkxTPKDycqbMwTBkBIGihuU_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VKIQITAMANNUWKINHFAJYASZEWR_T92F5D611EB59564DA780613AD88070B0010B1B78_H
#ifndef ZUFUCERQKTSWIADVEFAJCKXXTQ_TFC1D79BFC752F82CB540273B095A07CED5970C72_H
#define ZUFUCERQKTSWIADVEFAJCKXXTQ_TFC1D79BFC752F82CB540273B095A07CED5970C72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_zUFUcerQktswIadVEFajckxxTQ
struct  zUFUcerQktswIadVEFajckxxTQ_tFC1D79BFC752F82CB540273B095A07CED5970C72  : public RuntimeObject
{
public:
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_sifvSqThLQGuVclMuQKUhwYRlPoN Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_zUFUcerQktswIadVEFajckxxTQ::yRBXJeDJiaSvuoZWvBMXqrfBkDz
	sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3 * ___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0;
	// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_zUFUcerQktswIadVEFajckxxTQ::NxecGThpOUHPMAqQltxuibhjsBi
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * ___NxecGThpOUHPMAqQltxuibhjsBi_1;
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qEcNdMVgmNokdCmhdodTTEqmHlz_zUFUcerQktswIadVEFajckxxTQ::xntvcYLUoptzzWTrXAfZKQVzOVK
	int32_t ___xntvcYLUoptzzWTrXAfZKQVzOVK_2;

public:
	inline static int32_t get_offset_of_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0() { return static_cast<int32_t>(offsetof(zUFUcerQktswIadVEFajckxxTQ_tFC1D79BFC752F82CB540273B095A07CED5970C72, ___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0)); }
	inline sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3 * get_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0() const { return ___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0; }
	inline sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3 ** get_address_of_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0() { return &___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0; }
	inline void set_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0(sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3 * value)
	{
		___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0 = value;
		Il2CppCodeGenWriteBarrier((&___yRBXJeDJiaSvuoZWvBMXqrfBkDz_0), value);
	}

	inline static int32_t get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return static_cast<int32_t>(offsetof(zUFUcerQktswIadVEFajckxxTQ_tFC1D79BFC752F82CB540273B095A07CED5970C72, ___NxecGThpOUHPMAqQltxuibhjsBi_1)); }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * get_NxecGThpOUHPMAqQltxuibhjsBi_1() const { return ___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 ** get_address_of_NxecGThpOUHPMAqQltxuibhjsBi_1() { return &___NxecGThpOUHPMAqQltxuibhjsBi_1; }
	inline void set_NxecGThpOUHPMAqQltxuibhjsBi_1(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7 * value)
	{
		___NxecGThpOUHPMAqQltxuibhjsBi_1 = value;
		Il2CppCodeGenWriteBarrier((&___NxecGThpOUHPMAqQltxuibhjsBi_1), value);
	}

	inline static int32_t get_offset_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2() { return static_cast<int32_t>(offsetof(zUFUcerQktswIadVEFajckxxTQ_tFC1D79BFC752F82CB540273B095A07CED5970C72, ___xntvcYLUoptzzWTrXAfZKQVzOVK_2)); }
	inline int32_t get_xntvcYLUoptzzWTrXAfZKQVzOVK_2() const { return ___xntvcYLUoptzzWTrXAfZKQVzOVK_2; }
	inline int32_t* get_address_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2() { return &___xntvcYLUoptzzWTrXAfZKQVzOVK_2; }
	inline void set_xntvcYLUoptzzWTrXAfZKQVzOVK_2(int32_t value)
	{
		___xntvcYLUoptzzWTrXAfZKQVzOVK_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZUFUCERQKTSWIADVEFAJCKXXTQ_TFC1D79BFC752F82CB540273B095A07CED5970C72_H
#ifndef QYKEFKBEBYPSCIWTBLNXILFWNLYB_T92BAD69DDB6B5E2E6881BEEE81A4FB36D4E9C6DE_H
#define QYKEFKBEBYPSCIWTBLNXILFWNLYB_T92BAD69DDB6B5E2E6881BEEE81A4FB36D4E9C6DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB
struct  qYkefkBebypsciWTBlnXilfwNlYB_t92BAD69DDB6B5E2E6881BEEE81A4FB36D4E9C6DE  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB::kNVCFCmWKtvcerfyfgOooaREcZT
	int32_t ___kNVCFCmWKtvcerfyfgOooaREcZT_0;
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB::wTqpkSxtmPJzjdXGhVkxSDXgQnn
	int32_t ___wTqpkSxtmPJzjdXGhVkxSDXgQnn_1;
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB::ZVntyEgnPndjqVEvkGltHHRFDxI
	int32_t ___ZVntyEgnPndjqVEvkGltHHRFDxI_2;

public:
	inline static int32_t get_offset_of_kNVCFCmWKtvcerfyfgOooaREcZT_0() { return static_cast<int32_t>(offsetof(qYkefkBebypsciWTBlnXilfwNlYB_t92BAD69DDB6B5E2E6881BEEE81A4FB36D4E9C6DE, ___kNVCFCmWKtvcerfyfgOooaREcZT_0)); }
	inline int32_t get_kNVCFCmWKtvcerfyfgOooaREcZT_0() const { return ___kNVCFCmWKtvcerfyfgOooaREcZT_0; }
	inline int32_t* get_address_of_kNVCFCmWKtvcerfyfgOooaREcZT_0() { return &___kNVCFCmWKtvcerfyfgOooaREcZT_0; }
	inline void set_kNVCFCmWKtvcerfyfgOooaREcZT_0(int32_t value)
	{
		___kNVCFCmWKtvcerfyfgOooaREcZT_0 = value;
	}

	inline static int32_t get_offset_of_wTqpkSxtmPJzjdXGhVkxSDXgQnn_1() { return static_cast<int32_t>(offsetof(qYkefkBebypsciWTBlnXilfwNlYB_t92BAD69DDB6B5E2E6881BEEE81A4FB36D4E9C6DE, ___wTqpkSxtmPJzjdXGhVkxSDXgQnn_1)); }
	inline int32_t get_wTqpkSxtmPJzjdXGhVkxSDXgQnn_1() const { return ___wTqpkSxtmPJzjdXGhVkxSDXgQnn_1; }
	inline int32_t* get_address_of_wTqpkSxtmPJzjdXGhVkxSDXgQnn_1() { return &___wTqpkSxtmPJzjdXGhVkxSDXgQnn_1; }
	inline void set_wTqpkSxtmPJzjdXGhVkxSDXgQnn_1(int32_t value)
	{
		___wTqpkSxtmPJzjdXGhVkxSDXgQnn_1 = value;
	}

	inline static int32_t get_offset_of_ZVntyEgnPndjqVEvkGltHHRFDxI_2() { return static_cast<int32_t>(offsetof(qYkefkBebypsciWTBlnXilfwNlYB_t92BAD69DDB6B5E2E6881BEEE81A4FB36D4E9C6DE, ___ZVntyEgnPndjqVEvkGltHHRFDxI_2)); }
	inline int32_t get_ZVntyEgnPndjqVEvkGltHHRFDxI_2() const { return ___ZVntyEgnPndjqVEvkGltHHRFDxI_2; }
	inline int32_t* get_address_of_ZVntyEgnPndjqVEvkGltHHRFDxI_2() { return &___ZVntyEgnPndjqVEvkGltHHRFDxI_2; }
	inline void set_ZVntyEgnPndjqVEvkGltHHRFDxI_2(int32_t value)
	{
		___ZVntyEgnPndjqVEvkGltHHRFDxI_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QYKEFKBEBYPSCIWTBLNXILFWNLYB_T92BAD69DDB6B5E2E6881BEEE81A4FB36D4E9C6DE_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef PLATFORM_CUSTOM_T38C5DD986F65AF512626A73FA4EA96ECF76A0C77_H
#define PLATFORM_CUSTOM_T38C5DD986F65AF512626A73FA4EA96ECF76A0C77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom
struct  Platform_Custom_t38C5DD986F65AF512626A73FA4EA96ECF76A0C77  : public Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_CUSTOM_T38C5DD986F65AF512626A73FA4EA96ECF76A0C77_H
#ifndef ELEMENTS_TC57D226564DCD7C2CFC9AFA7A212201FCC98129B_H
#define ELEMENTS_TC57D226564DCD7C2CFC9AFA7A212201FCC98129B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Elements
struct  Elements_tC57D226564DCD7C2CFC9AFA7A212201FCC98129B  : public Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_TC57D226564DCD7C2CFC9AFA7A212201FCC98129B_H
#ifndef MATCHINGCRITERIA_T1A1DB371BD17248F7D375FC7FA3B8704BEB1C795_H
#define MATCHINGCRITERIA_T1A1DB371BD17248F7D375FC7FA3B8704BEB1C795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_MatchingCriteria
struct  MatchingCriteria_t1A1DB371BD17248F7D375FC7FA3B8704BEB1C795  : public MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_MatchingCriteria::alwaysMatch
	bool ___alwaysMatch_4;

public:
	inline static int32_t get_offset_of_alwaysMatch_4() { return static_cast<int32_t>(offsetof(MatchingCriteria_t1A1DB371BD17248F7D375FC7FA3B8704BEB1C795, ___alwaysMatch_4)); }
	inline bool get_alwaysMatch_4() const { return ___alwaysMatch_4; }
	inline bool* get_address_of_alwaysMatch_4() { return &___alwaysMatch_4; }
	inline void set_alwaysMatch_4(bool value)
	{
		___alwaysMatch_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_T1A1DB371BD17248F7D375FC7FA3B8704BEB1C795_H
#ifndef PLATFORM_SDL2_BASE_T00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE_H
#define PLATFORM_SDL2_BASE_T00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base
struct  Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE  : public Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base::matchingCriteria
	MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base::elements
	Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24 * ___elements_2;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE, ___matchingCriteria_1)); }
	inline MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE, ___elements_2)); }
	inline Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24 * get_elements_2() const { return ___elements_2; }
	inline Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24 ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24 * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_SDL2_BASE_T00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE_H
#ifndef ELEMENTS_T088498B33BCB6FB61A33B3CEC46957FA6623FC24_H
#define ELEMENTS_T088498B33BCB6FB61A33B3CEC46957FA6623FC24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements
struct  Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24  : public Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements::axes
	AxisU5BU5D_t2BB57D29B9968F1A2F0BD26315755F640062E809* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Elements::buttons
	ButtonU5BU5D_tAC2CF36EC9CD62CE6C8B6706ECFE77173EF2AA04* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24, ___axes_0)); }
	inline AxisU5BU5D_t2BB57D29B9968F1A2F0BD26315755F640062E809* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_t2BB57D29B9968F1A2F0BD26315755F640062E809** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_t2BB57D29B9968F1A2F0BD26315755F640062E809* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24, ___buttons_1)); }
	inline ButtonU5BU5D_tAC2CF36EC9CD62CE6C8B6706ECFE77173EF2AA04* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_tAC2CF36EC9CD62CE6C8B6706ECFE77173EF2AA04** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_tAC2CF36EC9CD62CE6C8B6706ECFE77173EF2AA04* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_T088498B33BCB6FB61A33B3CEC46957FA6623FC24_H
#ifndef MATCHINGCRITERIA_TCE6CD5A42D352239F501CC5F1A5F33125857D8CA_H
#define MATCHINGCRITERIA_TCE6CD5A42D352239F501CC5F1A5F33125857D8CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_MatchingCriteria
struct  MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA  : public MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_MatchingCriteria::hatCount
	int32_t ___hatCount_4;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_MatchingCriteria::manufacturer_useRegex
	bool ___manufacturer_useRegex_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_MatchingCriteria::productName_useRegex
	bool ___productName_useRegex_6;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_MatchingCriteria::systemName_useRegex
	bool ___systemName_useRegex_7;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_MatchingCriteria::manufacturer
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___manufacturer_8;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_MatchingCriteria::productName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productName_9;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_MatchingCriteria::systemName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___systemName_10;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_MatchingCriteria::productGUID
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productGUID_11;

public:
	inline static int32_t get_offset_of_hatCount_4() { return static_cast<int32_t>(offsetof(MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA, ___hatCount_4)); }
	inline int32_t get_hatCount_4() const { return ___hatCount_4; }
	inline int32_t* get_address_of_hatCount_4() { return &___hatCount_4; }
	inline void set_hatCount_4(int32_t value)
	{
		___hatCount_4 = value;
	}

	inline static int32_t get_offset_of_manufacturer_useRegex_5() { return static_cast<int32_t>(offsetof(MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA, ___manufacturer_useRegex_5)); }
	inline bool get_manufacturer_useRegex_5() const { return ___manufacturer_useRegex_5; }
	inline bool* get_address_of_manufacturer_useRegex_5() { return &___manufacturer_useRegex_5; }
	inline void set_manufacturer_useRegex_5(bool value)
	{
		___manufacturer_useRegex_5 = value;
	}

	inline static int32_t get_offset_of_productName_useRegex_6() { return static_cast<int32_t>(offsetof(MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA, ___productName_useRegex_6)); }
	inline bool get_productName_useRegex_6() const { return ___productName_useRegex_6; }
	inline bool* get_address_of_productName_useRegex_6() { return &___productName_useRegex_6; }
	inline void set_productName_useRegex_6(bool value)
	{
		___productName_useRegex_6 = value;
	}

	inline static int32_t get_offset_of_systemName_useRegex_7() { return static_cast<int32_t>(offsetof(MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA, ___systemName_useRegex_7)); }
	inline bool get_systemName_useRegex_7() const { return ___systemName_useRegex_7; }
	inline bool* get_address_of_systemName_useRegex_7() { return &___systemName_useRegex_7; }
	inline void set_systemName_useRegex_7(bool value)
	{
		___systemName_useRegex_7 = value;
	}

	inline static int32_t get_offset_of_manufacturer_8() { return static_cast<int32_t>(offsetof(MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA, ___manufacturer_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_manufacturer_8() const { return ___manufacturer_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_manufacturer_8() { return &___manufacturer_8; }
	inline void set_manufacturer_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___manufacturer_8 = value;
		Il2CppCodeGenWriteBarrier((&___manufacturer_8), value);
	}

	inline static int32_t get_offset_of_productName_9() { return static_cast<int32_t>(offsetof(MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA, ___productName_9)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productName_9() const { return ___productName_9; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productName_9() { return &___productName_9; }
	inline void set_productName_9(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productName_9 = value;
		Il2CppCodeGenWriteBarrier((&___productName_9), value);
	}

	inline static int32_t get_offset_of_systemName_10() { return static_cast<int32_t>(offsetof(MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA, ___systemName_10)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_systemName_10() const { return ___systemName_10; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_systemName_10() { return &___systemName_10; }
	inline void set_systemName_10(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___systemName_10 = value;
		Il2CppCodeGenWriteBarrier((&___systemName_10), value);
	}

	inline static int32_t get_offset_of_productGUID_11() { return static_cast<int32_t>(offsetof(MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA, ___productGUID_11)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productGUID_11() const { return ___productGUID_11; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productGUID_11() { return &___productGUID_11; }
	inline void set_productGUID_11(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productGUID_11 = value;
		Il2CppCodeGenWriteBarrier((&___productGUID_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_TCE6CD5A42D352239F501CC5F1A5F33125857D8CA_H
#ifndef ELEMENTCOUNT_T087149C9BF78BF1188AAEACAE1911000DD398F89_H
#define ELEMENTCOUNT_T087149C9BF78BF1188AAEACAE1911000DD398F89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_MatchingCriteria_ElementCount
struct  ElementCount_t087149C9BF78BF1188AAEACAE1911000DD398F89  : public ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_MatchingCriteria_ElementCount::hatCount
	int32_t ___hatCount_2;

public:
	inline static int32_t get_offset_of_hatCount_2() { return static_cast<int32_t>(offsetof(ElementCount_t087149C9BF78BF1188AAEACAE1911000DD398F89, ___hatCount_2)); }
	inline int32_t get_hatCount_2() const { return ___hatCount_2; }
	inline int32_t* get_address_of_hatCount_2() { return &___hatCount_2; }
	inline void set_hatCount_2(int32_t value)
	{
		___hatCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTCOUNT_T087149C9BF78BF1188AAEACAE1911000DD398F89_H
#ifndef PLATFORM_STEAM_BASE_TF32E32817D4F61E024BC61EA2D01BFB9C8E5690F_H
#define PLATFORM_STEAM_BASE_TF32E32817D4F61E024BC61EA2D01BFB9C8E5690F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Steam_Base
struct  Platform_Steam_Base_tF32E32817D4F61E024BC61EA2D01BFB9C8E5690F  : public Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Steam_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_Steam_Base::matchingCriteria
	MatchingCriteria_t722FD8AEF7B412C35CE3507CFCBDF57E5BEE4062 * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Steam_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_Steam_Base::elements
	Elements_tF245BB8E30C6E928077EEBF35DD28D1B5EC292A7 * ___elements_2;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_Steam_Base_tF32E32817D4F61E024BC61EA2D01BFB9C8E5690F, ___matchingCriteria_1)); }
	inline MatchingCriteria_t722FD8AEF7B412C35CE3507CFCBDF57E5BEE4062 * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_t722FD8AEF7B412C35CE3507CFCBDF57E5BEE4062 ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_t722FD8AEF7B412C35CE3507CFCBDF57E5BEE4062 * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_Steam_Base_tF32E32817D4F61E024BC61EA2D01BFB9C8E5690F, ___elements_2)); }
	inline Elements_tF245BB8E30C6E928077EEBF35DD28D1B5EC292A7 * get_elements_2() const { return ___elements_2; }
	inline Elements_tF245BB8E30C6E928077EEBF35DD28D1B5EC292A7 ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_tF245BB8E30C6E928077EEBF35DD28D1B5EC292A7 * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_STEAM_BASE_TF32E32817D4F61E024BC61EA2D01BFB9C8E5690F_H
#ifndef ELEMENTS_TF245BB8E30C6E928077EEBF35DD28D1B5EC292A7_H
#define ELEMENTS_TF245BB8E30C6E928077EEBF35DD28D1B5EC292A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Steam_Base_Elements
struct  Elements_tF245BB8E30C6E928077EEBF35DD28D1B5EC292A7  : public Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_TF245BB8E30C6E928077EEBF35DD28D1B5EC292A7_H
#ifndef MATCHINGCRITERIA_T722FD8AEF7B412C35CE3507CFCBDF57E5BEE4062_H
#define MATCHINGCRITERIA_T722FD8AEF7B412C35CE3507CFCBDF57E5BEE4062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Steam_Base_MatchingCriteria
struct  MatchingCriteria_t722FD8AEF7B412C35CE3507CFCBDF57E5BEE4062  : public MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_T722FD8AEF7B412C35CE3507CFCBDF57E5BEE4062_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#define AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisCoordinateMode
struct  AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868 
{
public:
	// System.Int32 Rewired.AxisCoordinateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifndef AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#define AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisRange
struct  AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652 
{
public:
	// System.Int32 Rewired.AxisRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifndef AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#define AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisSensitivityType
struct  AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A 
{
public:
	// System.Int32 Rewired.AxisSensitivityType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#ifndef ALTERNATEAXISCALIBRATIONTYPE_T13E810E9252891A41BDB4417F660AE3C9EEA526F_H
#define ALTERNATEAXISCALIBRATIONTYPE_T13E810E9252891A41BDB4417F660AE3C9EEA526F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.AlternateAxisCalibrationType
struct  AlternateAxisCalibrationType_t13E810E9252891A41BDB4417F660AE3C9EEA526F 
{
public:
	// System.Int32 Rewired.Data.Mapping.AlternateAxisCalibrationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlternateAxisCalibrationType_t13E810E9252891A41BDB4417F660AE3C9EEA526F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALTERNATEAXISCALIBRATIONTYPE_T13E810E9252891A41BDB4417F660AE3C9EEA526F_H
#ifndef AXISCALIBRATIONTYPE_TE13AFD356AB96E532C90470EC4A227DEF86B598C_H
#define AXISCALIBRATIONTYPE_TE13AFD356AB96E532C90470EC4A227DEF86B598C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.AxisCalibrationType
struct  AxisCalibrationType_tE13AFD356AB96E532C90470EC4A227DEF86B598C 
{
public:
	// System.Int32 Rewired.Data.Mapping.AxisCalibrationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisCalibrationType_tE13AFD356AB96E532C90470EC4A227DEF86B598C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCALIBRATIONTYPE_TE13AFD356AB96E532C90470EC4A227DEF86B598C_H
#ifndef AXISDIRECTION_TE4953CDA51E7D3BD480078C8768EBCD213C2EF49_H
#define AXISDIRECTION_TE4953CDA51E7D3BD480078C8768EBCD213C2EF49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.AxisDirection
struct  AxisDirection_tE4953CDA51E7D3BD480078C8768EBCD213C2EF49 
{
public:
	// System.Int32 Rewired.Data.Mapping.AxisDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisDirection_tE4953CDA51E7D3BD480078C8768EBCD213C2EF49, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISDIRECTION_TE4953CDA51E7D3BD480078C8768EBCD213C2EF49_H
#ifndef HARDWAREAXISTYPE_T59D6929FB2AFD99B0BD06CCFA53470F5A17E89AA_H
#define HARDWAREAXISTYPE_T59D6929FB2AFD99B0BD06CCFA53470F5A17E89AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareAxisType
struct  HardwareAxisType_t59D6929FB2AFD99B0BD06CCFA53470F5A17E89AA 
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareAxisType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HardwareAxisType_t59D6929FB2AFD99B0BD06CCFA53470F5A17E89AA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWAREAXISTYPE_T59D6929FB2AFD99B0BD06CCFA53470F5A17E89AA_H
#ifndef HARDWAREELEMENTSOURCETYPE_T440266C7790C9BE17B04056BE8C554EE04C34675_H
#define HARDWAREELEMENTSOURCETYPE_T440266C7790C9BE17B04056BE8C554EE04C34675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareElementSourceType
struct  HardwareElementSourceType_t440266C7790C9BE17B04056BE8C554EE04C34675 
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareElementSourceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HardwareElementSourceType_t440266C7790C9BE17B04056BE8C554EE04C34675, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWAREELEMENTSOURCETYPE_T440266C7790C9BE17B04056BE8C554EE04C34675_H
#ifndef HARDWAREELEMENTSOURCETYPEWITHHAT_TDC84EB6512EDD5A1BB8A15FFF31201CC0E318520_H
#define HARDWAREELEMENTSOURCETYPEWITHHAT_TDC84EB6512EDD5A1BB8A15FFF31201CC0E318520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat
struct  HardwareElementSourceTypeWithHat_tDC84EB6512EDD5A1BB8A15FFF31201CC0E318520 
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareElementSourceTypeWithHat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HardwareElementSourceTypeWithHat_tDC84EB6512EDD5A1BB8A15FFF31201CC0E318520, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWAREELEMENTSOURCETYPEWITHHAT_TDC84EB6512EDD5A1BB8A15FFF31201CC0E318520_H
#ifndef EJMKVXLXOHRRCZSDGIQDOWHTDKHK_TBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06_H
#define EJMKVXLXOHRRCZSDGIQDOWHTDKHK_TBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_EjmkvxlXoHRRczsdGIQdOwhTdKHK
struct  EjmkvxlXoHRRczsdGIQdOwhTdKHK_tBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06  : public RuntimeObject
{
public:
	// System.Guid Rewired.Data.Mapping.HardwareJoystickMap_EjmkvxlXoHRRczsdGIQdOwhTdKHK::NOArrsNdfKDShNFftfbPqYDzhpq
	Guid_t  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_EjmkvxlXoHRRczsdGIQdOwhTdKHK::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_EjmkvxlXoHRRczsdGIQdOwhTdKHK::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap Rewired.Data.Mapping.HardwareJoystickMap_EjmkvxlXoHRRczsdGIQdOwhTdKHK::DKaFTWTMhFeLQHDExnDMRxZfmROL
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_EjmkvxlXoHRRczsdGIQdOwhTdKHK::okMFupfyoBNZDyKMyvAtwGIUiAd
	int32_t ___okMFupfyoBNZDyKMyvAtwGIUiAd_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(EjmkvxlXoHRRczsdGIQdOwhTdKHK_tBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Guid_t  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Guid_t * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Guid_t  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(EjmkvxlXoHRRczsdGIQdOwhTdKHK_tBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(EjmkvxlXoHRRczsdGIQdOwhTdKHK_tBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(EjmkvxlXoHRRczsdGIQdOwhTdKHK_tBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4() { return static_cast<int32_t>(offsetof(EjmkvxlXoHRRczsdGIQdOwhTdKHK_tBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06, ___okMFupfyoBNZDyKMyvAtwGIUiAd_4)); }
	inline int32_t get_okMFupfyoBNZDyKMyvAtwGIUiAd_4() const { return ___okMFupfyoBNZDyKMyvAtwGIUiAd_4; }
	inline int32_t* get_address_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4() { return &___okMFupfyoBNZDyKMyvAtwGIUiAd_4; }
	inline void set_okMFupfyoBNZDyKMyvAtwGIUiAd_4(int32_t value)
	{
		___okMFupfyoBNZDyKMyvAtwGIUiAd_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EJMKVXLXOHRRCZSDGIQDOWHTDKHK_TBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06_H
#ifndef PLATFORM_INTERNALDRIVER_BASE_T130672B998BA3D805D6AEE615F7BD2C7058D6E21_H
#define PLATFORM_INTERNALDRIVER_BASE_T130672B998BA3D805D6AEE615F7BD2C7058D6E21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base
struct  Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21  : public Platform_Custom_t38C5DD986F65AF512626A73FA4EA96ECF76A0C77
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base::matchingCriteria
	MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5 * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base::elements
	Elements_t656A4A2A4154F0048E692B5D67D8596354228F5B * ___elements_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base::_axesOrigGame
	AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* ____axesOrigGame_3;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base::_buttonsOrigGame
	ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* ____buttonsOrigGame_4;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21, ___matchingCriteria_1)); }
	inline MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5 * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5 ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5 * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21, ___elements_2)); }
	inline Elements_t656A4A2A4154F0048E692B5D67D8596354228F5B * get_elements_2() const { return ___elements_2; }
	inline Elements_t656A4A2A4154F0048E692B5D67D8596354228F5B ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_t656A4A2A4154F0048E692B5D67D8596354228F5B * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}

	inline static int32_t get_offset_of__axesOrigGame_3() { return static_cast<int32_t>(offsetof(Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21, ____axesOrigGame_3)); }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* get__axesOrigGame_3() const { return ____axesOrigGame_3; }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8** get_address_of__axesOrigGame_3() { return &____axesOrigGame_3; }
	inline void set__axesOrigGame_3(AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* value)
	{
		____axesOrigGame_3 = value;
		Il2CppCodeGenWriteBarrier((&____axesOrigGame_3), value);
	}

	inline static int32_t get_offset_of__buttonsOrigGame_4() { return static_cast<int32_t>(offsetof(Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21, ____buttonsOrigGame_4)); }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* get__buttonsOrigGame_4() const { return ____buttonsOrigGame_4; }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E** get_address_of__buttonsOrigGame_4() { return &____buttonsOrigGame_4; }
	inline void set__buttonsOrigGame_4(ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* value)
	{
		____buttonsOrigGame_4 = value;
		Il2CppCodeGenWriteBarrier((&____buttonsOrigGame_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_INTERNALDRIVER_BASE_T130672B998BA3D805D6AEE615F7BD2C7058D6E21_H
#ifndef ELEMENTS_T656A4A2A4154F0048E692B5D67D8596354228F5B_H
#define ELEMENTS_T656A4A2A4154F0048E692B5D67D8596354228F5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Elements
struct  Elements_t656A4A2A4154F0048E692B5D67D8596354228F5B  : public Elements_tC57D226564DCD7C2CFC9AFA7A212201FCC98129B
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Elements::axes
	AxisU5BU5D_tD13716E57490B18A6B388AAF5CDA0B27255CDD92* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Elements::buttons
	ButtonU5BU5D_t74601BECE712AA3EDBBCBFF2B29C73C01CA30C6A* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_t656A4A2A4154F0048E692B5D67D8596354228F5B, ___axes_0)); }
	inline AxisU5BU5D_tD13716E57490B18A6B388AAF5CDA0B27255CDD92* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_tD13716E57490B18A6B388AAF5CDA0B27255CDD92** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_tD13716E57490B18A6B388AAF5CDA0B27255CDD92* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_t656A4A2A4154F0048E692B5D67D8596354228F5B, ___buttons_1)); }
	inline ButtonU5BU5D_t74601BECE712AA3EDBBCBFF2B29C73C01CA30C6A* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_t74601BECE712AA3EDBBCBFF2B29C73C01CA30C6A** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_t74601BECE712AA3EDBBCBFF2B29C73C01CA30C6A* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_T656A4A2A4154F0048E692B5D67D8596354228F5B_H
#ifndef MATCHINGCRITERIA_TD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5_H
#define MATCHINGCRITERIA_TD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_MatchingCriteria
struct  MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5  : public MatchingCriteria_t1A1DB371BD17248F7D375FC7FA3B8704BEB1C795
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_MatchingCriteria::productName_useRegex
	bool ___productName_useRegex_5;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_MatchingCriteria::productName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productName_6;
	// Rewired.Data.Mapping.HardwareJoystickMap_VidPid[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_MatchingCriteria::vidPid
	VidPidU5BU5D_t6020E7B4C895694E39E0A81C72C285189098A971* ___vidPid_7;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_MatchingCriteria::hatCount
	int32_t ___hatCount_8;

public:
	inline static int32_t get_offset_of_productName_useRegex_5() { return static_cast<int32_t>(offsetof(MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5, ___productName_useRegex_5)); }
	inline bool get_productName_useRegex_5() const { return ___productName_useRegex_5; }
	inline bool* get_address_of_productName_useRegex_5() { return &___productName_useRegex_5; }
	inline void set_productName_useRegex_5(bool value)
	{
		___productName_useRegex_5 = value;
	}

	inline static int32_t get_offset_of_productName_6() { return static_cast<int32_t>(offsetof(MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5, ___productName_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productName_6() const { return ___productName_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productName_6() { return &___productName_6; }
	inline void set_productName_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productName_6 = value;
		Il2CppCodeGenWriteBarrier((&___productName_6), value);
	}

	inline static int32_t get_offset_of_vidPid_7() { return static_cast<int32_t>(offsetof(MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5, ___vidPid_7)); }
	inline VidPidU5BU5D_t6020E7B4C895694E39E0A81C72C285189098A971* get_vidPid_7() const { return ___vidPid_7; }
	inline VidPidU5BU5D_t6020E7B4C895694E39E0A81C72C285189098A971** get_address_of_vidPid_7() { return &___vidPid_7; }
	inline void set_vidPid_7(VidPidU5BU5D_t6020E7B4C895694E39E0A81C72C285189098A971* value)
	{
		___vidPid_7 = value;
		Il2CppCodeGenWriteBarrier((&___vidPid_7), value);
	}

	inline static int32_t get_offset_of_hatCount_8() { return static_cast<int32_t>(offsetof(MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5, ___hatCount_8)); }
	inline int32_t get_hatCount_8() const { return ___hatCount_8; }
	inline int32_t* get_address_of_hatCount_8() { return &___hatCount_8; }
	inline void set_hatCount_8(int32_t value)
	{
		___hatCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_TD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5_H
#ifndef PLATFORM_NINTENDOSWITCH_BASE_T762348F60D107794795951289C5E38A71195D6EF_H
#define PLATFORM_NINTENDOSWITCH_BASE_T762348F60D107794795951289C5E38A71195D6EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base
struct  Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF  : public Platform_Custom_t38C5DD986F65AF512626A73FA4EA96ECF76A0C77
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base::matchingCriteria
	MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base::elements
	Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D * ___elements_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base::_axesOrigGame
	AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* ____axesOrigGame_3;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base::_buttonsOrigGame
	ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* ____buttonsOrigGame_4;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF, ___matchingCriteria_1)); }
	inline MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF, ___elements_2)); }
	inline Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D * get_elements_2() const { return ___elements_2; }
	inline Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}

	inline static int32_t get_offset_of__axesOrigGame_3() { return static_cast<int32_t>(offsetof(Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF, ____axesOrigGame_3)); }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* get__axesOrigGame_3() const { return ____axesOrigGame_3; }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8** get_address_of__axesOrigGame_3() { return &____axesOrigGame_3; }
	inline void set__axesOrigGame_3(AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* value)
	{
		____axesOrigGame_3 = value;
		Il2CppCodeGenWriteBarrier((&____axesOrigGame_3), value);
	}

	inline static int32_t get_offset_of__buttonsOrigGame_4() { return static_cast<int32_t>(offsetof(Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF, ____buttonsOrigGame_4)); }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* get__buttonsOrigGame_4() const { return ____buttonsOrigGame_4; }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E** get_address_of__buttonsOrigGame_4() { return &____buttonsOrigGame_4; }
	inline void set__buttonsOrigGame_4(ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* value)
	{
		____buttonsOrigGame_4 = value;
		Il2CppCodeGenWriteBarrier((&____buttonsOrigGame_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_NINTENDOSWITCH_BASE_T762348F60D107794795951289C5E38A71195D6EF_H
#ifndef PLATFORM_SDL2_T5339C1F7963CAC4185CB1400C3EA0174D7A7B80A_H
#define PLATFORM_SDL2_T5339C1F7963CAC4185CB1400C3EA0174D7A7B80A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2
struct  Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A  : public Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2::variants
	Platform_SDL2_BaseU5BU5D_t0D50401C9DEFF2022C857D02FC2A2A01E67CE601* ___variants_3;

public:
	inline static int32_t get_offset_of_variants_3() { return static_cast<int32_t>(offsetof(Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A, ___variants_3)); }
	inline Platform_SDL2_BaseU5BU5D_t0D50401C9DEFF2022C857D02FC2A2A01E67CE601* get_variants_3() const { return ___variants_3; }
	inline Platform_SDL2_BaseU5BU5D_t0D50401C9DEFF2022C857D02FC2A2A01E67CE601** get_address_of_variants_3() { return &___variants_3; }
	inline void set_variants_3(Platform_SDL2_BaseU5BU5D_t0D50401C9DEFF2022C857D02FC2A2A01E67CE601* value)
	{
		___variants_3 = value;
		Il2CppCodeGenWriteBarrier((&___variants_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_SDL2_T5339C1F7963CAC4185CB1400C3EA0174D7A7B80A_H
#ifndef PLATFORM_STEAM_TDC1F4D175B21289CADE6C876D6B697B3675C1676_H
#define PLATFORM_STEAM_TDC1F4D175B21289CADE6C876D6B697B3675C1676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Steam
struct  Platform_Steam_tDC1F4D175B21289CADE6C876D6B697B3675C1676  : public Platform_Steam_Base_tF32E32817D4F61E024BC61EA2D01BFB9C8E5690F
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Steam_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Steam::variants
	Platform_Steam_BaseU5BU5D_t71D5D330D85FA219CFBA26A2C1638E139BBBA543* ___variants_3;

public:
	inline static int32_t get_offset_of_variants_3() { return static_cast<int32_t>(offsetof(Platform_Steam_tDC1F4D175B21289CADE6C876D6B697B3675C1676, ___variants_3)); }
	inline Platform_Steam_BaseU5BU5D_t71D5D330D85FA219CFBA26A2C1638E139BBBA543* get_variants_3() const { return ___variants_3; }
	inline Platform_Steam_BaseU5BU5D_t71D5D330D85FA219CFBA26A2C1638E139BBBA543** get_address_of_variants_3() { return &___variants_3; }
	inline void set_variants_3(Platform_Steam_BaseU5BU5D_t71D5D330D85FA219CFBA26A2C1638E139BBBA543* value)
	{
		___variants_3 = value;
		Il2CppCodeGenWriteBarrier((&___variants_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_STEAM_TDC1F4D175B21289CADE6C876D6B697B3675C1676_H
#ifndef PLATFORM_WEBGL_BASE_T0E47618689C13B4A5BC159DE9975829A2534A295_H
#define PLATFORM_WEBGL_BASE_T0E47618689C13B4A5BC159DE9975829A2534A295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base
struct  Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295  : public Platform_Custom_t38C5DD986F65AF512626A73FA4EA96ECF76A0C77
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base::matchingCriteria
	MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587 * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base::elements
	Elements_tB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022 * ___elements_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base::_axesOrigGame
	AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* ____axesOrigGame_3;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base::_buttonsOrigGame
	ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* ____buttonsOrigGame_4;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295, ___matchingCriteria_1)); }
	inline MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587 * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587 ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587 * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295, ___elements_2)); }
	inline Elements_tB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022 * get_elements_2() const { return ___elements_2; }
	inline Elements_tB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022 ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_tB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022 * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}

	inline static int32_t get_offset_of__axesOrigGame_3() { return static_cast<int32_t>(offsetof(Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295, ____axesOrigGame_3)); }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* get__axesOrigGame_3() const { return ____axesOrigGame_3; }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8** get_address_of__axesOrigGame_3() { return &____axesOrigGame_3; }
	inline void set__axesOrigGame_3(AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* value)
	{
		____axesOrigGame_3 = value;
		Il2CppCodeGenWriteBarrier((&____axesOrigGame_3), value);
	}

	inline static int32_t get_offset_of__buttonsOrigGame_4() { return static_cast<int32_t>(offsetof(Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295, ____buttonsOrigGame_4)); }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* get__buttonsOrigGame_4() const { return ____buttonsOrigGame_4; }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E** get_address_of__buttonsOrigGame_4() { return &____buttonsOrigGame_4; }
	inline void set__buttonsOrigGame_4(ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* value)
	{
		____buttonsOrigGame_4 = value;
		Il2CppCodeGenWriteBarrier((&____buttonsOrigGame_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_WEBGL_BASE_T0E47618689C13B4A5BC159DE9975829A2534A295_H
#ifndef ELEMENTS_TB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022_H
#define ELEMENTS_TB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_Elements
struct  Elements_tB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022  : public Elements_tC57D226564DCD7C2CFC9AFA7A212201FCC98129B
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_Elements::axes
	AxisU5BU5D_tE5CCB8C13EC5AF3F10051527E4A58A369A12BC2E* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_Elements::buttons
	ButtonU5BU5D_t8418C265860277AB2757F7094850543B630021C7* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_tB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022, ___axes_0)); }
	inline AxisU5BU5D_tE5CCB8C13EC5AF3F10051527E4A58A369A12BC2E* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_tE5CCB8C13EC5AF3F10051527E4A58A369A12BC2E** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_tE5CCB8C13EC5AF3F10051527E4A58A369A12BC2E* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_tB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022, ___buttons_1)); }
	inline ButtonU5BU5D_t8418C265860277AB2757F7094850543B630021C7* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_t8418C265860277AB2757F7094850543B630021C7** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_t8418C265860277AB2757F7094850543B630021C7* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_TB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022_H
#ifndef MATCHINGCRITERIA_T17AADED3B6E208F07E9D67778843352BC980A587_H
#define MATCHINGCRITERIA_T17AADED3B6E208F07E9D67778843352BC980A587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria
struct  MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587  : public MatchingCriteria_t1A1DB371BD17248F7D375FC7FA3B8704BEB1C795
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria::productName_useRegex
	bool ___productName_useRegex_5;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria::productName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productName_6;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria::productGUID
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productGUID_7;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria::mapping
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mapping_8;
	// Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base_ElementCount_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria::elementCount
	ElementCount_BaseU5BU5D_tCC1A9A7741F132924D329C62382FF47C506B516C* ___elementCount_9;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria_ClientInfo[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_MatchingCriteria::clientInfo
	ClientInfoU5BU5D_tCBF411919F035B6414E4785B2500E345B9C82CB0* ___clientInfo_10;

public:
	inline static int32_t get_offset_of_productName_useRegex_5() { return static_cast<int32_t>(offsetof(MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587, ___productName_useRegex_5)); }
	inline bool get_productName_useRegex_5() const { return ___productName_useRegex_5; }
	inline bool* get_address_of_productName_useRegex_5() { return &___productName_useRegex_5; }
	inline void set_productName_useRegex_5(bool value)
	{
		___productName_useRegex_5 = value;
	}

	inline static int32_t get_offset_of_productName_6() { return static_cast<int32_t>(offsetof(MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587, ___productName_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productName_6() const { return ___productName_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productName_6() { return &___productName_6; }
	inline void set_productName_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productName_6 = value;
		Il2CppCodeGenWriteBarrier((&___productName_6), value);
	}

	inline static int32_t get_offset_of_productGUID_7() { return static_cast<int32_t>(offsetof(MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587, ___productGUID_7)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productGUID_7() const { return ___productGUID_7; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productGUID_7() { return &___productGUID_7; }
	inline void set_productGUID_7(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productGUID_7 = value;
		Il2CppCodeGenWriteBarrier((&___productGUID_7), value);
	}

	inline static int32_t get_offset_of_mapping_8() { return static_cast<int32_t>(offsetof(MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587, ___mapping_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mapping_8() const { return ___mapping_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mapping_8() { return &___mapping_8; }
	inline void set_mapping_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mapping_8 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_8), value);
	}

	inline static int32_t get_offset_of_elementCount_9() { return static_cast<int32_t>(offsetof(MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587, ___elementCount_9)); }
	inline ElementCount_BaseU5BU5D_tCC1A9A7741F132924D329C62382FF47C506B516C* get_elementCount_9() const { return ___elementCount_9; }
	inline ElementCount_BaseU5BU5D_tCC1A9A7741F132924D329C62382FF47C506B516C** get_address_of_elementCount_9() { return &___elementCount_9; }
	inline void set_elementCount_9(ElementCount_BaseU5BU5D_tCC1A9A7741F132924D329C62382FF47C506B516C* value)
	{
		___elementCount_9 = value;
		Il2CppCodeGenWriteBarrier((&___elementCount_9), value);
	}

	inline static int32_t get_offset_of_clientInfo_10() { return static_cast<int32_t>(offsetof(MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587, ___clientInfo_10)); }
	inline ClientInfoU5BU5D_tCBF411919F035B6414E4785B2500E345B9C82CB0* get_clientInfo_10() const { return ___clientInfo_10; }
	inline ClientInfoU5BU5D_tCBF411919F035B6414E4785B2500E345B9C82CB0** get_address_of_clientInfo_10() { return &___clientInfo_10; }
	inline void set_clientInfo_10(ClientInfoU5BU5D_tCBF411919F035B6414E4785B2500E345B9C82CB0* value)
	{
		___clientInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___clientInfo_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_T17AADED3B6E208F07E9D67778843352BC980A587_H
#ifndef HATDIRECTION_T8B3787E56706986A258EEBCF16F989D2075785BA_H
#define HATDIRECTION_T8B3787E56706986A258EEBCF16F989D2075785BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HatDirection
struct  HatDirection_t8B3787E56706986A258EEBCF16F989D2075785BA 
{
public:
	// System.Int32 Rewired.Data.Mapping.HatDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HatDirection_t8B3787E56706986A258EEBCF16F989D2075785BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HATDIRECTION_T8B3787E56706986A258EEBCF16F989D2075785BA_H
#ifndef HATTYPE_T83B584B8E763A7B6924539BA31ECF4BD99E395F3_H
#define HATTYPE_T83B584B8E763A7B6924539BA31ECF4BD99E395F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HatType
struct  HatType_t83B584B8E763A7B6924539BA31ECF4BD99E395F3 
{
public:
	// System.Int32 Rewired.Data.Mapping.HatType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HatType_t83B584B8E763A7B6924539BA31ECF4BD99E395F3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HATTYPE_T83B584B8E763A7B6924539BA31ECF4BD99E395F3_H
#ifndef SPECIALAXISTYPE_TDEFE7FBCBA9C5D22178D908D1568E975D1BE513E_H
#define SPECIALAXISTYPE_TDEFE7FBCBA9C5D22178D908D1568E975D1BE513E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.SpecialAxisType
struct  SpecialAxisType_tDEFE7FBCBA9C5D22178D908D1568E975D1BE513E 
{
public:
	// System.Int32 Rewired.Data.Mapping.SpecialAxisType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialAxisType_tDEFE7FBCBA9C5D22178D908D1568E975D1BE513E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALAXISTYPE_TDEFE7FBCBA9C5D22178D908D1568E975D1BE513E_H
#ifndef VOGDFDANINJFFCUBARGNHLXVWSXD_T773885846FA1F1AB6C0A66CE64635110D52189B1_H
#define VOGDFDANINJFFCUBARGNHLXVWSXD_T773885846FA1F1AB6C0A66CE64635110D52189B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB_VogdFdaNiNjFfcuBargNhLXvWsXd
struct  VogdFdaNiNjFfcuBargNhLXvWsXd_t773885846FA1F1AB6C0A66CE64635110D52189B1 
{
public:
	// System.Int32 Rewired.Data.UserData_fdlAxojmklCKPkEqkHYmFTOHUCz_qYkefkBebypsciWTBlnXilfwNlYB_VogdFdaNiNjFfcuBargNhLXvWsXd::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VogdFdaNiNjFfcuBargNhLXvWsXd_t773885846FA1F1AB6C0A66CE64635110D52189B1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOGDFDANINJFFCUBARGNHLXVWSXD_T773885846FA1F1AB6C0A66CE64635110D52189B1_H
#ifndef JOYSTICKTYPE_TC4C48EF9EED8725C2514BD8934385DBC867DA1B0_H
#define JOYSTICKTYPE_TC4C48EF9EED8725C2514BD8934385DBC867DA1B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.JoystickType
struct  JoystickType_tC4C48EF9EED8725C2514BD8934385DBC867DA1B0 
{
public:
	// System.Int32 Rewired.JoystickType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JoystickType_tC4C48EF9EED8725C2514BD8934385DBC867DA1B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKTYPE_TC4C48EF9EED8725C2514BD8934385DBC867DA1B0_H
#ifndef EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#define EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.EditorPlatform
struct  EditorPlatform_tBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC 
{
public:
	// System.Int32 Rewired.Platforms.EditorPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EditorPlatform_tBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#ifndef PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#define PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Platform
struct  Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD 
{
public:
	// System.Int32 Rewired.Platforms.Platform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#ifndef WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#define WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebplayerPlatform
struct  WebplayerPlatform_tCE2DAFDB23E582591F2C39C73279153D7E0C3A82 
{
public:
	// System.Int32 Rewired.Platforms.WebplayerPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebplayerPlatform_tCE2DAFDB23E582591F2C39C73279153D7E0C3A82, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#ifndef POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#define POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Pole
struct  Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C 
{
public:
	// System.Int32 Rewired.Pole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef AXISCALIBRATIONINFO_T2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5_H
#define AXISCALIBRATIONINFO_T2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.AxisCalibrationInfo
struct  AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Data.Mapping.AxisCalibrationInfo::_applyRangeCalibration
	bool ____applyRangeCalibration_0;
	// System.Boolean Rewired.Data.Mapping.AxisCalibrationInfo::_invert
	bool ____invert_1;
	// System.Single Rewired.Data.Mapping.AxisCalibrationInfo::_deadZone
	float ____deadZone_2;
	// System.Single Rewired.Data.Mapping.AxisCalibrationInfo::_zero
	float ____zero_3;
	// System.Single Rewired.Data.Mapping.AxisCalibrationInfo::_min
	float ____min_4;
	// System.Single Rewired.Data.Mapping.AxisCalibrationInfo::_max
	float ____max_5;
	// Rewired.AxisSensitivityType Rewired.Data.Mapping.AxisCalibrationInfo::_sensitivityType
	int32_t ____sensitivityType_6;
	// System.Single Rewired.Data.Mapping.AxisCalibrationInfo::_sensitivity
	float ____sensitivity_7;
	// UnityEngine.AnimationCurve Rewired.Data.Mapping.AxisCalibrationInfo::_sensitivityCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ____sensitivityCurve_8;

public:
	inline static int32_t get_offset_of__applyRangeCalibration_0() { return static_cast<int32_t>(offsetof(AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5, ____applyRangeCalibration_0)); }
	inline bool get__applyRangeCalibration_0() const { return ____applyRangeCalibration_0; }
	inline bool* get_address_of__applyRangeCalibration_0() { return &____applyRangeCalibration_0; }
	inline void set__applyRangeCalibration_0(bool value)
	{
		____applyRangeCalibration_0 = value;
	}

	inline static int32_t get_offset_of__invert_1() { return static_cast<int32_t>(offsetof(AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5, ____invert_1)); }
	inline bool get__invert_1() const { return ____invert_1; }
	inline bool* get_address_of__invert_1() { return &____invert_1; }
	inline void set__invert_1(bool value)
	{
		____invert_1 = value;
	}

	inline static int32_t get_offset_of__deadZone_2() { return static_cast<int32_t>(offsetof(AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5, ____deadZone_2)); }
	inline float get__deadZone_2() const { return ____deadZone_2; }
	inline float* get_address_of__deadZone_2() { return &____deadZone_2; }
	inline void set__deadZone_2(float value)
	{
		____deadZone_2 = value;
	}

	inline static int32_t get_offset_of__zero_3() { return static_cast<int32_t>(offsetof(AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5, ____zero_3)); }
	inline float get__zero_3() const { return ____zero_3; }
	inline float* get_address_of__zero_3() { return &____zero_3; }
	inline void set__zero_3(float value)
	{
		____zero_3 = value;
	}

	inline static int32_t get_offset_of__min_4() { return static_cast<int32_t>(offsetof(AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5, ____min_4)); }
	inline float get__min_4() const { return ____min_4; }
	inline float* get_address_of__min_4() { return &____min_4; }
	inline void set__min_4(float value)
	{
		____min_4 = value;
	}

	inline static int32_t get_offset_of__max_5() { return static_cast<int32_t>(offsetof(AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5, ____max_5)); }
	inline float get__max_5() const { return ____max_5; }
	inline float* get_address_of__max_5() { return &____max_5; }
	inline void set__max_5(float value)
	{
		____max_5 = value;
	}

	inline static int32_t get_offset_of__sensitivityType_6() { return static_cast<int32_t>(offsetof(AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5, ____sensitivityType_6)); }
	inline int32_t get__sensitivityType_6() const { return ____sensitivityType_6; }
	inline int32_t* get_address_of__sensitivityType_6() { return &____sensitivityType_6; }
	inline void set__sensitivityType_6(int32_t value)
	{
		____sensitivityType_6 = value;
	}

	inline static int32_t get_offset_of__sensitivity_7() { return static_cast<int32_t>(offsetof(AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5, ____sensitivity_7)); }
	inline float get__sensitivity_7() const { return ____sensitivity_7; }
	inline float* get_address_of__sensitivity_7() { return &____sensitivity_7; }
	inline void set__sensitivity_7(float value)
	{
		____sensitivity_7 = value;
	}

	inline static int32_t get_offset_of__sensitivityCurve_8() { return static_cast<int32_t>(offsetof(AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5, ____sensitivityCurve_8)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get__sensitivityCurve_8() const { return ____sensitivityCurve_8; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of__sensitivityCurve_8() { return &____sensitivityCurve_8; }
	inline void set__sensitivityCurve_8(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		____sensitivityCurve_8 = value;
		Il2CppCodeGenWriteBarrier((&____sensitivityCurve_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCALIBRATIONINFO_T2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5_H
#ifndef HARDWAREAXISINFO_TABFC6F993A6738A4283168A404AB2C5F413341F4_H
#define HARDWAREAXISINFO_TABFC6F993A6738A4283168A404AB2C5F413341F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareAxisInfo
struct  HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4  : public RuntimeObject
{
public:
	// Rewired.AxisCoordinateMode Rewired.Data.Mapping.HardwareAxisInfo::_dataFormat
	int32_t ____dataFormat_0;
	// System.Boolean Rewired.Data.Mapping.HardwareAxisInfo::_excludeFromPolling
	bool ____excludeFromPolling_1;
	// Rewired.Data.Mapping.SpecialAxisType Rewired.Data.Mapping.HardwareAxisInfo::_specialAxisType
	int32_t ____specialAxisType_2;

public:
	inline static int32_t get_offset_of__dataFormat_0() { return static_cast<int32_t>(offsetof(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4, ____dataFormat_0)); }
	inline int32_t get__dataFormat_0() const { return ____dataFormat_0; }
	inline int32_t* get_address_of__dataFormat_0() { return &____dataFormat_0; }
	inline void set__dataFormat_0(int32_t value)
	{
		____dataFormat_0 = value;
	}

	inline static int32_t get_offset_of__excludeFromPolling_1() { return static_cast<int32_t>(offsetof(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4, ____excludeFromPolling_1)); }
	inline bool get__excludeFromPolling_1() const { return ____excludeFromPolling_1; }
	inline bool* get_address_of__excludeFromPolling_1() { return &____excludeFromPolling_1; }
	inline void set__excludeFromPolling_1(bool value)
	{
		____excludeFromPolling_1 = value;
	}

	inline static int32_t get_offset_of__specialAxisType_2() { return static_cast<int32_t>(offsetof(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4, ____specialAxisType_2)); }
	inline int32_t get__specialAxisType_2() const { return ____specialAxisType_2; }
	inline int32_t* get_address_of__specialAxisType_2() { return &____specialAxisType_2; }
	inline void set__specialAxisType_2(int32_t value)
	{
		____specialAxisType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWAREAXISINFO_TABFC6F993A6738A4283168A404AB2C5F413341F4_H
#ifndef AXIS_T9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E_H
#define AXIS_T9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis
struct  Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E  : public Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::invert
	bool ___invert_7;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::sourceAxisRange
	int32_t ___sourceAxisRange_8;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::buttonAxisContribution
	int32_t ___buttonAxisContribution_9;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::calibrateAxis
	bool ___calibrateAxis_10;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::axisZero
	float ___axisZero_11;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::axisMin
	float ___axisMin_12;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::axisMax
	float ___axisMax_13;
	// Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::alternateCalibrations
	AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* ___alternateCalibrations_14;
	// Rewired.Data.Mapping.HardwareAxisInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::axisInfo
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * ___axisInfo_15;

public:
	inline static int32_t get_offset_of_invert_7() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___invert_7)); }
	inline bool get_invert_7() const { return ___invert_7; }
	inline bool* get_address_of_invert_7() { return &___invert_7; }
	inline void set_invert_7(bool value)
	{
		___invert_7 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_8() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___sourceAxisRange_8)); }
	inline int32_t get_sourceAxisRange_8() const { return ___sourceAxisRange_8; }
	inline int32_t* get_address_of_sourceAxisRange_8() { return &___sourceAxisRange_8; }
	inline void set_sourceAxisRange_8(int32_t value)
	{
		___sourceAxisRange_8 = value;
	}

	inline static int32_t get_offset_of_buttonAxisContribution_9() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___buttonAxisContribution_9)); }
	inline int32_t get_buttonAxisContribution_9() const { return ___buttonAxisContribution_9; }
	inline int32_t* get_address_of_buttonAxisContribution_9() { return &___buttonAxisContribution_9; }
	inline void set_buttonAxisContribution_9(int32_t value)
	{
		___buttonAxisContribution_9 = value;
	}

	inline static int32_t get_offset_of_calibrateAxis_10() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___calibrateAxis_10)); }
	inline bool get_calibrateAxis_10() const { return ___calibrateAxis_10; }
	inline bool* get_address_of_calibrateAxis_10() { return &___calibrateAxis_10; }
	inline void set_calibrateAxis_10(bool value)
	{
		___calibrateAxis_10 = value;
	}

	inline static int32_t get_offset_of_axisZero_11() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___axisZero_11)); }
	inline float get_axisZero_11() const { return ___axisZero_11; }
	inline float* get_address_of_axisZero_11() { return &___axisZero_11; }
	inline void set_axisZero_11(float value)
	{
		___axisZero_11 = value;
	}

	inline static int32_t get_offset_of_axisMin_12() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___axisMin_12)); }
	inline float get_axisMin_12() const { return ___axisMin_12; }
	inline float* get_address_of_axisMin_12() { return &___axisMin_12; }
	inline void set_axisMin_12(float value)
	{
		___axisMin_12 = value;
	}

	inline static int32_t get_offset_of_axisMax_13() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___axisMax_13)); }
	inline float get_axisMax_13() const { return ___axisMax_13; }
	inline float* get_address_of_axisMax_13() { return &___axisMax_13; }
	inline void set_axisMax_13(float value)
	{
		___axisMax_13 = value;
	}

	inline static int32_t get_offset_of_alternateCalibrations_14() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___alternateCalibrations_14)); }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* get_alternateCalibrations_14() const { return ___alternateCalibrations_14; }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7** get_address_of_alternateCalibrations_14() { return &___alternateCalibrations_14; }
	inline void set_alternateCalibrations_14(AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* value)
	{
		___alternateCalibrations_14 = value;
		Il2CppCodeGenWriteBarrier((&___alternateCalibrations_14), value);
	}

	inline static int32_t get_offset_of_axisInfo_15() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___axisInfo_15)); }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * get_axisInfo_15() const { return ___axisInfo_15; }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 ** get_address_of_axisInfo_15() { return &___axisInfo_15; }
	inline void set_axisInfo_15(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * value)
	{
		___axisInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___axisInfo_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E_H
#ifndef BUTTON_TBB6149DD7A45B873206F15797DC2B002019BF09C_H
#define BUTTON_TBB6149DD7A45B873206F15797DC2B002019BF09C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button
struct  Button_tBB6149DD7A45B873206F15797DC2B002019BF09C  : public Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF
{
public:
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button::sourceAxisPole
	int32_t ___sourceAxisPole_7;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button::requireMultipleButtons
	bool ___requireMultipleButtons_8;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button::requiredButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___requiredButtons_9;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button::ignoreIfButtonsActive
	bool ___ignoreIfButtonsActive_10;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button::ignoreIfButtonsActiveButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ignoreIfButtonsActiveButtons_11;
	// Rewired.Data.Mapping.HardwareButtonInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button::buttonInfo
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * ___buttonInfo_12;

public:
	inline static int32_t get_offset_of_sourceAxisPole_7() { return static_cast<int32_t>(offsetof(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C, ___sourceAxisPole_7)); }
	inline int32_t get_sourceAxisPole_7() const { return ___sourceAxisPole_7; }
	inline int32_t* get_address_of_sourceAxisPole_7() { return &___sourceAxisPole_7; }
	inline void set_sourceAxisPole_7(int32_t value)
	{
		___sourceAxisPole_7 = value;
	}

	inline static int32_t get_offset_of_requireMultipleButtons_8() { return static_cast<int32_t>(offsetof(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C, ___requireMultipleButtons_8)); }
	inline bool get_requireMultipleButtons_8() const { return ___requireMultipleButtons_8; }
	inline bool* get_address_of_requireMultipleButtons_8() { return &___requireMultipleButtons_8; }
	inline void set_requireMultipleButtons_8(bool value)
	{
		___requireMultipleButtons_8 = value;
	}

	inline static int32_t get_offset_of_requiredButtons_9() { return static_cast<int32_t>(offsetof(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C, ___requiredButtons_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_requiredButtons_9() const { return ___requiredButtons_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_requiredButtons_9() { return &___requiredButtons_9; }
	inline void set_requiredButtons_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___requiredButtons_9 = value;
		Il2CppCodeGenWriteBarrier((&___requiredButtons_9), value);
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActive_10() { return static_cast<int32_t>(offsetof(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C, ___ignoreIfButtonsActive_10)); }
	inline bool get_ignoreIfButtonsActive_10() const { return ___ignoreIfButtonsActive_10; }
	inline bool* get_address_of_ignoreIfButtonsActive_10() { return &___ignoreIfButtonsActive_10; }
	inline void set_ignoreIfButtonsActive_10(bool value)
	{
		___ignoreIfButtonsActive_10 = value;
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActiveButtons_11() { return static_cast<int32_t>(offsetof(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C, ___ignoreIfButtonsActiveButtons_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ignoreIfButtonsActiveButtons_11() const { return ___ignoreIfButtonsActiveButtons_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ignoreIfButtonsActiveButtons_11() { return &___ignoreIfButtonsActiveButtons_11; }
	inline void set_ignoreIfButtonsActiveButtons_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ignoreIfButtonsActiveButtons_11 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreIfButtonsActiveButtons_11), value);
	}

	inline static int32_t get_offset_of_buttonInfo_12() { return static_cast<int32_t>(offsetof(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C, ___buttonInfo_12)); }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * get_buttonInfo_12() const { return ___buttonInfo_12; }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 ** get_address_of_buttonInfo_12() { return &___buttonInfo_12; }
	inline void set_buttonInfo_12(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * value)
	{
		___buttonInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___buttonInfo_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_TBB6149DD7A45B873206F15797DC2B002019BF09C_H
#ifndef PLATFORM_INTERNALDRIVER_T8A5A88BC6C79D793F1E687A676A4EC95C644007F_H
#define PLATFORM_INTERNALDRIVER_T8A5A88BC6C79D793F1E687A676A4EC95C644007F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver
struct  Platform_InternalDriver_t8A5A88BC6C79D793F1E687A676A4EC95C644007F  : public Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver::variants
	Platform_InternalDriver_BaseU5BU5D_tC41BF5CE8D7EAC464A23255CC1CF6CDBB9E16A89* ___variants_5;

public:
	inline static int32_t get_offset_of_variants_5() { return static_cast<int32_t>(offsetof(Platform_InternalDriver_t8A5A88BC6C79D793F1E687A676A4EC95C644007F, ___variants_5)); }
	inline Platform_InternalDriver_BaseU5BU5D_tC41BF5CE8D7EAC464A23255CC1CF6CDBB9E16A89* get_variants_5() const { return ___variants_5; }
	inline Platform_InternalDriver_BaseU5BU5D_tC41BF5CE8D7EAC464A23255CC1CF6CDBB9E16A89** get_address_of_variants_5() { return &___variants_5; }
	inline void set_variants_5(Platform_InternalDriver_BaseU5BU5D_tC41BF5CE8D7EAC464A23255CC1CF6CDBB9E16A89* value)
	{
		___variants_5 = value;
		Il2CppCodeGenWriteBarrier((&___variants_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_INTERNALDRIVER_T8A5A88BC6C79D793F1E687A676A4EC95C644007F_H
#ifndef PLATFORM_NINTENDOSWITCH_T2696C2B97DA28B7AFFF2A0AA8CE74563C99F99C8_H
#define PLATFORM_NINTENDOSWITCH_T2696C2B97DA28B7AFFF2A0AA8CE74563C99F99C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch
struct  Platform_NintendoSwitch_t2696C2B97DA28B7AFFF2A0AA8CE74563C99F99C8  : public Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch::variants
	Platform_NintendoSwitch_BaseU5BU5D_t5A831640E428C590033C4E197F51DFE58722CF87* ___variants_5;

public:
	inline static int32_t get_offset_of_variants_5() { return static_cast<int32_t>(offsetof(Platform_NintendoSwitch_t2696C2B97DA28B7AFFF2A0AA8CE74563C99F99C8, ___variants_5)); }
	inline Platform_NintendoSwitch_BaseU5BU5D_t5A831640E428C590033C4E197F51DFE58722CF87* get_variants_5() const { return ___variants_5; }
	inline Platform_NintendoSwitch_BaseU5BU5D_t5A831640E428C590033C4E197F51DFE58722CF87** get_address_of_variants_5() { return &___variants_5; }
	inline void set_variants_5(Platform_NintendoSwitch_BaseU5BU5D_t5A831640E428C590033C4E197F51DFE58722CF87* value)
	{
		___variants_5 = value;
		Il2CppCodeGenWriteBarrier((&___variants_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_NINTENDOSWITCH_T2696C2B97DA28B7AFFF2A0AA8CE74563C99F99C8_H
#ifndef AXIS_TA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39_H
#define AXIS_TA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis
struct  Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39  : public Element_tF82C07643751F54246D1E673496F2473BADBAD89
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::elementIdentifier
	int32_t ___elementIdentifier_0;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::sourceType
	int32_t ___sourceType_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::sourceAxis
	int32_t ___sourceAxis_2;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::sourceAxisRange
	int32_t ___sourceAxisRange_3;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::invert
	bool ___invert_4;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::axisDeadZone
	float ___axisDeadZone_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::calibrateAxis
	bool ___calibrateAxis_6;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::axisZero
	float ___axisZero_7;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::axisMin
	float ___axisMin_8;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::axisMax
	float ___axisMax_9;
	// Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::alternateCalibrations
	AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* ___alternateCalibrations_10;
	// Rewired.Data.Mapping.HardwareAxisInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::axisInfo
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * ___axisInfo_11;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::sourceButton
	int32_t ___sourceButton_12;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::buttonAxisContribution
	int32_t ___buttonAxisContribution_13;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::sourceHat
	int32_t ___sourceHat_14;
	// Rewired.Data.Mapping.AxisDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::sourceHatDirection
	int32_t ___sourceHatDirection_15;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Axis::sourceHatRange
	int32_t ___sourceHatRange_16;

public:
	inline static int32_t get_offset_of_elementIdentifier_0() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___elementIdentifier_0)); }
	inline int32_t get_elementIdentifier_0() const { return ___elementIdentifier_0; }
	inline int32_t* get_address_of_elementIdentifier_0() { return &___elementIdentifier_0; }
	inline void set_elementIdentifier_0(int32_t value)
	{
		___elementIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_sourceType_1() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___sourceType_1)); }
	inline int32_t get_sourceType_1() const { return ___sourceType_1; }
	inline int32_t* get_address_of_sourceType_1() { return &___sourceType_1; }
	inline void set_sourceType_1(int32_t value)
	{
		___sourceType_1 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_2() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___sourceAxis_2)); }
	inline int32_t get_sourceAxis_2() const { return ___sourceAxis_2; }
	inline int32_t* get_address_of_sourceAxis_2() { return &___sourceAxis_2; }
	inline void set_sourceAxis_2(int32_t value)
	{
		___sourceAxis_2 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_3() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___sourceAxisRange_3)); }
	inline int32_t get_sourceAxisRange_3() const { return ___sourceAxisRange_3; }
	inline int32_t* get_address_of_sourceAxisRange_3() { return &___sourceAxisRange_3; }
	inline void set_sourceAxisRange_3(int32_t value)
	{
		___sourceAxisRange_3 = value;
	}

	inline static int32_t get_offset_of_invert_4() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___invert_4)); }
	inline bool get_invert_4() const { return ___invert_4; }
	inline bool* get_address_of_invert_4() { return &___invert_4; }
	inline void set_invert_4(bool value)
	{
		___invert_4 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_5() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___axisDeadZone_5)); }
	inline float get_axisDeadZone_5() const { return ___axisDeadZone_5; }
	inline float* get_address_of_axisDeadZone_5() { return &___axisDeadZone_5; }
	inline void set_axisDeadZone_5(float value)
	{
		___axisDeadZone_5 = value;
	}

	inline static int32_t get_offset_of_calibrateAxis_6() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___calibrateAxis_6)); }
	inline bool get_calibrateAxis_6() const { return ___calibrateAxis_6; }
	inline bool* get_address_of_calibrateAxis_6() { return &___calibrateAxis_6; }
	inline void set_calibrateAxis_6(bool value)
	{
		___calibrateAxis_6 = value;
	}

	inline static int32_t get_offset_of_axisZero_7() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___axisZero_7)); }
	inline float get_axisZero_7() const { return ___axisZero_7; }
	inline float* get_address_of_axisZero_7() { return &___axisZero_7; }
	inline void set_axisZero_7(float value)
	{
		___axisZero_7 = value;
	}

	inline static int32_t get_offset_of_axisMin_8() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___axisMin_8)); }
	inline float get_axisMin_8() const { return ___axisMin_8; }
	inline float* get_address_of_axisMin_8() { return &___axisMin_8; }
	inline void set_axisMin_8(float value)
	{
		___axisMin_8 = value;
	}

	inline static int32_t get_offset_of_axisMax_9() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___axisMax_9)); }
	inline float get_axisMax_9() const { return ___axisMax_9; }
	inline float* get_address_of_axisMax_9() { return &___axisMax_9; }
	inline void set_axisMax_9(float value)
	{
		___axisMax_9 = value;
	}

	inline static int32_t get_offset_of_alternateCalibrations_10() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___alternateCalibrations_10)); }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* get_alternateCalibrations_10() const { return ___alternateCalibrations_10; }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7** get_address_of_alternateCalibrations_10() { return &___alternateCalibrations_10; }
	inline void set_alternateCalibrations_10(AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* value)
	{
		___alternateCalibrations_10 = value;
		Il2CppCodeGenWriteBarrier((&___alternateCalibrations_10), value);
	}

	inline static int32_t get_offset_of_axisInfo_11() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___axisInfo_11)); }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * get_axisInfo_11() const { return ___axisInfo_11; }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 ** get_address_of_axisInfo_11() { return &___axisInfo_11; }
	inline void set_axisInfo_11(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * value)
	{
		___axisInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___axisInfo_11), value);
	}

	inline static int32_t get_offset_of_sourceButton_12() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___sourceButton_12)); }
	inline int32_t get_sourceButton_12() const { return ___sourceButton_12; }
	inline int32_t* get_address_of_sourceButton_12() { return &___sourceButton_12; }
	inline void set_sourceButton_12(int32_t value)
	{
		___sourceButton_12 = value;
	}

	inline static int32_t get_offset_of_buttonAxisContribution_13() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___buttonAxisContribution_13)); }
	inline int32_t get_buttonAxisContribution_13() const { return ___buttonAxisContribution_13; }
	inline int32_t* get_address_of_buttonAxisContribution_13() { return &___buttonAxisContribution_13; }
	inline void set_buttonAxisContribution_13(int32_t value)
	{
		___buttonAxisContribution_13 = value;
	}

	inline static int32_t get_offset_of_sourceHat_14() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___sourceHat_14)); }
	inline int32_t get_sourceHat_14() const { return ___sourceHat_14; }
	inline int32_t* get_address_of_sourceHat_14() { return &___sourceHat_14; }
	inline void set_sourceHat_14(int32_t value)
	{
		___sourceHat_14 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_15() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___sourceHatDirection_15)); }
	inline int32_t get_sourceHatDirection_15() const { return ___sourceHatDirection_15; }
	inline int32_t* get_address_of_sourceHatDirection_15() { return &___sourceHatDirection_15; }
	inline void set_sourceHatDirection_15(int32_t value)
	{
		___sourceHatDirection_15 = value;
	}

	inline static int32_t get_offset_of_sourceHatRange_16() { return static_cast<int32_t>(offsetof(Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39, ___sourceHatRange_16)); }
	inline int32_t get_sourceHatRange_16() const { return ___sourceHatRange_16; }
	inline int32_t* get_address_of_sourceHatRange_16() { return &___sourceHatRange_16; }
	inline void set_sourceHatRange_16(int32_t value)
	{
		___sourceHatRange_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_TA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39_H
#ifndef BUTTON_TD33661ADF917D2C5EB39121A35878FCB0C95F734_H
#define BUTTON_TD33661ADF917D2C5EB39121A35878FCB0C95F734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button
struct  Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734  : public Element_tF82C07643751F54246D1E673496F2473BADBAD89
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::elementIdentifier
	int32_t ___elementIdentifier_0;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::sourceType
	int32_t ___sourceType_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::sourceButton
	int32_t ___sourceButton_2;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::sourceAxis
	int32_t ___sourceAxis_3;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::sourceAxisPole
	int32_t ___sourceAxisPole_4;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::axisDeadZone
	float ___axisDeadZone_5;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::sourceHat
	int32_t ___sourceHat_6;
	// Rewired.Data.Mapping.HatType Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::sourceHatType
	int32_t ___sourceHatType_7;
	// Rewired.Data.Mapping.HatDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::sourceHatDirection
	int32_t ___sourceHatDirection_8;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::requireMultipleButtons
	bool ___requireMultipleButtons_9;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::requiredButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___requiredButtons_10;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::ignoreIfButtonsActive
	bool ___ignoreIfButtonsActive_11;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::ignoreIfButtonsActiveButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ignoreIfButtonsActiveButtons_12;
	// Rewired.Data.Mapping.HardwareButtonInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2_Base_Button::buttonInfo
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * ___buttonInfo_13;

public:
	inline static int32_t get_offset_of_elementIdentifier_0() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___elementIdentifier_0)); }
	inline int32_t get_elementIdentifier_0() const { return ___elementIdentifier_0; }
	inline int32_t* get_address_of_elementIdentifier_0() { return &___elementIdentifier_0; }
	inline void set_elementIdentifier_0(int32_t value)
	{
		___elementIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_sourceType_1() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___sourceType_1)); }
	inline int32_t get_sourceType_1() const { return ___sourceType_1; }
	inline int32_t* get_address_of_sourceType_1() { return &___sourceType_1; }
	inline void set_sourceType_1(int32_t value)
	{
		___sourceType_1 = value;
	}

	inline static int32_t get_offset_of_sourceButton_2() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___sourceButton_2)); }
	inline int32_t get_sourceButton_2() const { return ___sourceButton_2; }
	inline int32_t* get_address_of_sourceButton_2() { return &___sourceButton_2; }
	inline void set_sourceButton_2(int32_t value)
	{
		___sourceButton_2 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_3() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___sourceAxis_3)); }
	inline int32_t get_sourceAxis_3() const { return ___sourceAxis_3; }
	inline int32_t* get_address_of_sourceAxis_3() { return &___sourceAxis_3; }
	inline void set_sourceAxis_3(int32_t value)
	{
		___sourceAxis_3 = value;
	}

	inline static int32_t get_offset_of_sourceAxisPole_4() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___sourceAxisPole_4)); }
	inline int32_t get_sourceAxisPole_4() const { return ___sourceAxisPole_4; }
	inline int32_t* get_address_of_sourceAxisPole_4() { return &___sourceAxisPole_4; }
	inline void set_sourceAxisPole_4(int32_t value)
	{
		___sourceAxisPole_4 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_5() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___axisDeadZone_5)); }
	inline float get_axisDeadZone_5() const { return ___axisDeadZone_5; }
	inline float* get_address_of_axisDeadZone_5() { return &___axisDeadZone_5; }
	inline void set_axisDeadZone_5(float value)
	{
		___axisDeadZone_5 = value;
	}

	inline static int32_t get_offset_of_sourceHat_6() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___sourceHat_6)); }
	inline int32_t get_sourceHat_6() const { return ___sourceHat_6; }
	inline int32_t* get_address_of_sourceHat_6() { return &___sourceHat_6; }
	inline void set_sourceHat_6(int32_t value)
	{
		___sourceHat_6 = value;
	}

	inline static int32_t get_offset_of_sourceHatType_7() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___sourceHatType_7)); }
	inline int32_t get_sourceHatType_7() const { return ___sourceHatType_7; }
	inline int32_t* get_address_of_sourceHatType_7() { return &___sourceHatType_7; }
	inline void set_sourceHatType_7(int32_t value)
	{
		___sourceHatType_7 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_8() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___sourceHatDirection_8)); }
	inline int32_t get_sourceHatDirection_8() const { return ___sourceHatDirection_8; }
	inline int32_t* get_address_of_sourceHatDirection_8() { return &___sourceHatDirection_8; }
	inline void set_sourceHatDirection_8(int32_t value)
	{
		___sourceHatDirection_8 = value;
	}

	inline static int32_t get_offset_of_requireMultipleButtons_9() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___requireMultipleButtons_9)); }
	inline bool get_requireMultipleButtons_9() const { return ___requireMultipleButtons_9; }
	inline bool* get_address_of_requireMultipleButtons_9() { return &___requireMultipleButtons_9; }
	inline void set_requireMultipleButtons_9(bool value)
	{
		___requireMultipleButtons_9 = value;
	}

	inline static int32_t get_offset_of_requiredButtons_10() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___requiredButtons_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_requiredButtons_10() const { return ___requiredButtons_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_requiredButtons_10() { return &___requiredButtons_10; }
	inline void set_requiredButtons_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___requiredButtons_10 = value;
		Il2CppCodeGenWriteBarrier((&___requiredButtons_10), value);
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActive_11() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___ignoreIfButtonsActive_11)); }
	inline bool get_ignoreIfButtonsActive_11() const { return ___ignoreIfButtonsActive_11; }
	inline bool* get_address_of_ignoreIfButtonsActive_11() { return &___ignoreIfButtonsActive_11; }
	inline void set_ignoreIfButtonsActive_11(bool value)
	{
		___ignoreIfButtonsActive_11 = value;
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActiveButtons_12() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___ignoreIfButtonsActiveButtons_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ignoreIfButtonsActiveButtons_12() const { return ___ignoreIfButtonsActiveButtons_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ignoreIfButtonsActiveButtons_12() { return &___ignoreIfButtonsActiveButtons_12; }
	inline void set_ignoreIfButtonsActiveButtons_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ignoreIfButtonsActiveButtons_12 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreIfButtonsActiveButtons_12), value);
	}

	inline static int32_t get_offset_of_buttonInfo_13() { return static_cast<int32_t>(offsetof(Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734, ___buttonInfo_13)); }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * get_buttonInfo_13() const { return ___buttonInfo_13; }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 ** get_address_of_buttonInfo_13() { return &___buttonInfo_13; }
	inline void set_buttonInfo_13(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * value)
	{
		___buttonInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___buttonInfo_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_TD33661ADF917D2C5EB39121A35878FCB0C95F734_H
#ifndef PLATFORM_WEBGL_T7C026B328916B42D37B442AE084EABCB320F318A_H
#define PLATFORM_WEBGL_T7C026B328916B42D37B442AE084EABCB320F318A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL
struct  Platform_WebGL_t7C026B328916B42D37B442AE084EABCB320F318A  : public Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL::variants
	Platform_WebGL_BaseU5BU5D_t9AE6837A8724775CAEDAC80A8DFB1BD543CAD1F3* ___variants_5;

public:
	inline static int32_t get_offset_of_variants_5() { return static_cast<int32_t>(offsetof(Platform_WebGL_t7C026B328916B42D37B442AE084EABCB320F318A, ___variants_5)); }
	inline Platform_WebGL_BaseU5BU5D_t9AE6837A8724775CAEDAC80A8DFB1BD543CAD1F3* get_variants_5() const { return ___variants_5; }
	inline Platform_WebGL_BaseU5BU5D_t9AE6837A8724775CAEDAC80A8DFB1BD543CAD1F3** get_address_of_variants_5() { return &___variants_5; }
	inline void set_variants_5(Platform_WebGL_BaseU5BU5D_t9AE6837A8724775CAEDAC80A8DFB1BD543CAD1F3* value)
	{
		___variants_5 = value;
		Il2CppCodeGenWriteBarrier((&___variants_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_WEBGL_T7C026B328916B42D37B442AE084EABCB320F318A_H
#ifndef TPTAEHMDHLOFHVFWEOLPNDPUDDB_T7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A_H
#define TPTAEHMDHLOFHVFWEOLPNDPUDDB_T7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_TptaEHmdHlOFhVFWEoLpnDpUddB
struct  TptaEHmdHlOFhVFWEoLpnDpUddB_t7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A  : public RuntimeObject
{
public:
	// Rewired.JoystickType Rewired.Data.Mapping.HardwareJoystickMap_TptaEHmdHlOFhVFWEoLpnDpUddB::NOArrsNdfKDShNFftfbPqYDzhpq
	int32_t ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_TptaEHmdHlOFhVFWEoLpnDpUddB::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_TptaEHmdHlOFhVFWEoLpnDpUddB::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap Rewired.Data.Mapping.HardwareJoystickMap_TptaEHmdHlOFhVFWEoLpnDpUddB::DKaFTWTMhFeLQHDExnDMRxZfmROL
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_TptaEHmdHlOFhVFWEoLpnDpUddB::lEJfSplllLlGeeWbNJFrzDmIVEp
	int32_t ___lEJfSplllLlGeeWbNJFrzDmIVEp_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(TptaEHmdHlOFhVFWEoLpnDpUddB_t7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline int32_t get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline int32_t* get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(int32_t value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(TptaEHmdHlOFhVFWEoLpnDpUddB_t7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(TptaEHmdHlOFhVFWEoLpnDpUddB_t7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(TptaEHmdHlOFhVFWEoLpnDpUddB_t7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_lEJfSplllLlGeeWbNJFrzDmIVEp_4() { return static_cast<int32_t>(offsetof(TptaEHmdHlOFhVFWEoLpnDpUddB_t7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A, ___lEJfSplllLlGeeWbNJFrzDmIVEp_4)); }
	inline int32_t get_lEJfSplllLlGeeWbNJFrzDmIVEp_4() const { return ___lEJfSplllLlGeeWbNJFrzDmIVEp_4; }
	inline int32_t* get_address_of_lEJfSplllLlGeeWbNJFrzDmIVEp_4() { return &___lEJfSplllLlGeeWbNJFrzDmIVEp_4; }
	inline void set_lEJfSplllLlGeeWbNJFrzDmIVEp_4(int32_t value)
	{
		___lEJfSplllLlGeeWbNJFrzDmIVEp_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TPTAEHMDHLOFHVFWEOLPNDPUDDB_T7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef EDITORPLATFORMDATA_TEE233DE574172C4348010F5BEF4CC55CF384DCB9_H
#define EDITORPLATFORMDATA_TEE233DE574172C4348010F5BEF4CC55CF384DCB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.EditorPlatformData
struct  EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Rewired.Data.EditorPlatformData_Platform Rewired.Data.EditorPlatformData::windowsStandalone
	Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * ___windowsStandalone_4;
	// Rewired.Data.EditorPlatformData_Platform Rewired.Data.EditorPlatformData::windowsStore
	Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * ___windowsStore_5;
	// Rewired.Data.EditorPlatformData_Platform Rewired.Data.EditorPlatformData::osxStandalone
	Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * ___osxStandalone_6;
	// Rewired.Data.EditorPlatformData_Platform Rewired.Data.EditorPlatformData::linuxStandalone
	Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * ___linuxStandalone_7;
	// Rewired.Data.EditorPlatformData_Platform Rewired.Data.EditorPlatformData::webplayer
	Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * ___webplayer_8;
	// Rewired.Data.EditorPlatformData_Platform Rewired.Data.EditorPlatformData::fallback
	Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * ___fallback_9;

public:
	inline static int32_t get_offset_of_windowsStandalone_4() { return static_cast<int32_t>(offsetof(EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9, ___windowsStandalone_4)); }
	inline Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * get_windowsStandalone_4() const { return ___windowsStandalone_4; }
	inline Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA ** get_address_of_windowsStandalone_4() { return &___windowsStandalone_4; }
	inline void set_windowsStandalone_4(Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * value)
	{
		___windowsStandalone_4 = value;
		Il2CppCodeGenWriteBarrier((&___windowsStandalone_4), value);
	}

	inline static int32_t get_offset_of_windowsStore_5() { return static_cast<int32_t>(offsetof(EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9, ___windowsStore_5)); }
	inline Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * get_windowsStore_5() const { return ___windowsStore_5; }
	inline Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA ** get_address_of_windowsStore_5() { return &___windowsStore_5; }
	inline void set_windowsStore_5(Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * value)
	{
		___windowsStore_5 = value;
		Il2CppCodeGenWriteBarrier((&___windowsStore_5), value);
	}

	inline static int32_t get_offset_of_osxStandalone_6() { return static_cast<int32_t>(offsetof(EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9, ___osxStandalone_6)); }
	inline Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * get_osxStandalone_6() const { return ___osxStandalone_6; }
	inline Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA ** get_address_of_osxStandalone_6() { return &___osxStandalone_6; }
	inline void set_osxStandalone_6(Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * value)
	{
		___osxStandalone_6 = value;
		Il2CppCodeGenWriteBarrier((&___osxStandalone_6), value);
	}

	inline static int32_t get_offset_of_linuxStandalone_7() { return static_cast<int32_t>(offsetof(EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9, ___linuxStandalone_7)); }
	inline Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * get_linuxStandalone_7() const { return ___linuxStandalone_7; }
	inline Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA ** get_address_of_linuxStandalone_7() { return &___linuxStandalone_7; }
	inline void set_linuxStandalone_7(Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * value)
	{
		___linuxStandalone_7 = value;
		Il2CppCodeGenWriteBarrier((&___linuxStandalone_7), value);
	}

	inline static int32_t get_offset_of_webplayer_8() { return static_cast<int32_t>(offsetof(EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9, ___webplayer_8)); }
	inline Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * get_webplayer_8() const { return ___webplayer_8; }
	inline Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA ** get_address_of_webplayer_8() { return &___webplayer_8; }
	inline void set_webplayer_8(Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * value)
	{
		___webplayer_8 = value;
		Il2CppCodeGenWriteBarrier((&___webplayer_8), value);
	}

	inline static int32_t get_offset_of_fallback_9() { return static_cast<int32_t>(offsetof(EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9, ___fallback_9)); }
	inline Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * get_fallback_9() const { return ___fallback_9; }
	inline Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA ** get_address_of_fallback_9() { return &___fallback_9; }
	inline void set_fallback_9(Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA * value)
	{
		___fallback_9 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORPLATFORMDATA_TEE233DE574172C4348010F5BEF4CC55CF384DCB9_H
#ifndef HARDWARECONTROLLERTEMPLATEMAP_TCB52D05ACF6814505874AB59323F65EC4D695633_H
#define HARDWARECONTROLLERTEMPLATEMAP_TCB52D05ACF6814505874AB59323F65EC4D695633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareControllerTemplateMap
struct  HardwareControllerTemplateMap_tCB52D05ACF6814505874AB59323F65EC4D695633  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWARECONTROLLERTEMPLATEMAP_TCB52D05ACF6814505874AB59323F65EC4D695633_H
#ifndef AXIS_TA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D_H
#define AXIS_TA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Axis
struct  Axis_tA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D  : public Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Axis::sourceHat
	int32_t ___sourceHat_16;
	// Rewired.Data.Mapping.AxisDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Axis::sourceHatDirection
	int32_t ___sourceHatDirection_17;
	// Rewired.Data.Mapping.HatType Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Axis::sourceHatType
	int32_t ___sourceHatType_18;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Axis::sourceHatRange
	int32_t ___sourceHatRange_19;

public:
	inline static int32_t get_offset_of_sourceHat_16() { return static_cast<int32_t>(offsetof(Axis_tA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D, ___sourceHat_16)); }
	inline int32_t get_sourceHat_16() const { return ___sourceHat_16; }
	inline int32_t* get_address_of_sourceHat_16() { return &___sourceHat_16; }
	inline void set_sourceHat_16(int32_t value)
	{
		___sourceHat_16 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_17() { return static_cast<int32_t>(offsetof(Axis_tA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D, ___sourceHatDirection_17)); }
	inline int32_t get_sourceHatDirection_17() const { return ___sourceHatDirection_17; }
	inline int32_t* get_address_of_sourceHatDirection_17() { return &___sourceHatDirection_17; }
	inline void set_sourceHatDirection_17(int32_t value)
	{
		___sourceHatDirection_17 = value;
	}

	inline static int32_t get_offset_of_sourceHatType_18() { return static_cast<int32_t>(offsetof(Axis_tA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D, ___sourceHatType_18)); }
	inline int32_t get_sourceHatType_18() const { return ___sourceHatType_18; }
	inline int32_t* get_address_of_sourceHatType_18() { return &___sourceHatType_18; }
	inline void set_sourceHatType_18(int32_t value)
	{
		___sourceHatType_18 = value;
	}

	inline static int32_t get_offset_of_sourceHatRange_19() { return static_cast<int32_t>(offsetof(Axis_tA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D, ___sourceHatRange_19)); }
	inline int32_t get_sourceHatRange_19() const { return ___sourceHatRange_19; }
	inline int32_t* get_address_of_sourceHatRange_19() { return &___sourceHatRange_19; }
	inline void set_sourceHatRange_19(int32_t value)
	{
		___sourceHatRange_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_TA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D_H
#ifndef BUTTON_T1F0CBE52174773C028D5F99AEBAF1394D5F8B407_H
#define BUTTON_T1F0CBE52174773C028D5F99AEBAF1394D5F8B407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Button
struct  Button_t1F0CBE52174773C028D5F99AEBAF1394D5F8B407  : public Button_tBB6149DD7A45B873206F15797DC2B002019BF09C
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Button::sourceHat
	int32_t ___sourceHat_13;
	// Rewired.Data.Mapping.HatDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Button::sourceHatDirection
	int32_t ___sourceHatDirection_14;
	// Rewired.Data.Mapping.HatType Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver_Base_Button::sourceHatType
	int32_t ___sourceHatType_15;

public:
	inline static int32_t get_offset_of_sourceHat_13() { return static_cast<int32_t>(offsetof(Button_t1F0CBE52174773C028D5F99AEBAF1394D5F8B407, ___sourceHat_13)); }
	inline int32_t get_sourceHat_13() const { return ___sourceHat_13; }
	inline int32_t* get_address_of_sourceHat_13() { return &___sourceHat_13; }
	inline void set_sourceHat_13(int32_t value)
	{
		___sourceHat_13 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_14() { return static_cast<int32_t>(offsetof(Button_t1F0CBE52174773C028D5F99AEBAF1394D5F8B407, ___sourceHatDirection_14)); }
	inline int32_t get_sourceHatDirection_14() const { return ___sourceHatDirection_14; }
	inline int32_t* get_address_of_sourceHatDirection_14() { return &___sourceHatDirection_14; }
	inline void set_sourceHatDirection_14(int32_t value)
	{
		___sourceHatDirection_14 = value;
	}

	inline static int32_t get_offset_of_sourceHatType_15() { return static_cast<int32_t>(offsetof(Button_t1F0CBE52174773C028D5F99AEBAF1394D5F8B407, ___sourceHatType_15)); }
	inline int32_t get_sourceHatType_15() const { return ___sourceHatType_15; }
	inline int32_t* get_address_of_sourceHatType_15() { return &___sourceHatType_15; }
	inline void set_sourceHatType_15(int32_t value)
	{
		___sourceHatType_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T1F0CBE52174773C028D5F99AEBAF1394D5F8B407_H
#ifndef AXIS_TDF1E851680691A45E36BC337FFC785D8CADAD56A_H
#define AXIS_TDF1E851680691A45E36BC337FFC785D8CADAD56A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_Axis
struct  Axis_tDF1E851680691A45E36BC337FFC785D8CADAD56A  : public Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_TDF1E851680691A45E36BC337FFC785D8CADAD56A_H
#ifndef BUTTON_T8FDD2499024E761C938C105D1958EB05F580B8E2_H
#define BUTTON_T8FDD2499024E761C938C105D1958EB05F580B8E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL_Base_Button
struct  Button_t8FDD2499024E761C938C105D1958EB05F580B8E2  : public Button_tBB6149DD7A45B873206F15797DC2B002019BF09C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T8FDD2499024E761C938C105D1958EB05F580B8E2_H
#ifndef RUNTIMEDATA_T5673D05BFF62DAB9EA4A418EEA9C4304C2A27187_H
#define RUNTIMEDATA_T5673D05BFF62DAB9EA4A418EEA9C4304C2A27187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.RuntimeData
struct  RuntimeData_t5673D05BFF62DAB9EA4A418EEA9C4304C2A27187  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Rewired.Platforms.Platform Rewired.Data.RuntimeData::platform
	int32_t ___platform_4;
	// Rewired.Platforms.WebplayerPlatform Rewired.Data.RuntimeData::webplayerPlatform
	int32_t ___webplayerPlatform_5;
	// Rewired.Platforms.EditorPlatform Rewired.Data.RuntimeData::editorPlatform
	int32_t ___editorPlatform_6;
	// System.Collections.Generic.List`1<UnityEngine.TextAsset> Rewired.Data.RuntimeData::libraries
	List_1_t46F5218A6E83BACB1E173DD59FD4C3895FD044C5 * ___libraries_7;

public:
	inline static int32_t get_offset_of_platform_4() { return static_cast<int32_t>(offsetof(RuntimeData_t5673D05BFF62DAB9EA4A418EEA9C4304C2A27187, ___platform_4)); }
	inline int32_t get_platform_4() const { return ___platform_4; }
	inline int32_t* get_address_of_platform_4() { return &___platform_4; }
	inline void set_platform_4(int32_t value)
	{
		___platform_4 = value;
	}

	inline static int32_t get_offset_of_webplayerPlatform_5() { return static_cast<int32_t>(offsetof(RuntimeData_t5673D05BFF62DAB9EA4A418EEA9C4304C2A27187, ___webplayerPlatform_5)); }
	inline int32_t get_webplayerPlatform_5() const { return ___webplayerPlatform_5; }
	inline int32_t* get_address_of_webplayerPlatform_5() { return &___webplayerPlatform_5; }
	inline void set_webplayerPlatform_5(int32_t value)
	{
		___webplayerPlatform_5 = value;
	}

	inline static int32_t get_offset_of_editorPlatform_6() { return static_cast<int32_t>(offsetof(RuntimeData_t5673D05BFF62DAB9EA4A418EEA9C4304C2A27187, ___editorPlatform_6)); }
	inline int32_t get_editorPlatform_6() const { return ___editorPlatform_6; }
	inline int32_t* get_address_of_editorPlatform_6() { return &___editorPlatform_6; }
	inline void set_editorPlatform_6(int32_t value)
	{
		___editorPlatform_6 = value;
	}

	inline static int32_t get_offset_of_libraries_7() { return static_cast<int32_t>(offsetof(RuntimeData_t5673D05BFF62DAB9EA4A418EEA9C4304C2A27187, ___libraries_7)); }
	inline List_1_t46F5218A6E83BACB1E173DD59FD4C3895FD044C5 * get_libraries_7() const { return ___libraries_7; }
	inline List_1_t46F5218A6E83BACB1E173DD59FD4C3895FD044C5 ** get_address_of_libraries_7() { return &___libraries_7; }
	inline void set_libraries_7(List_1_t46F5218A6E83BACB1E173DD59FD4C3895FD044C5 * value)
	{
		___libraries_7 = value;
		Il2CppCodeGenWriteBarrier((&___libraries_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEDATA_T5673D05BFF62DAB9EA4A418EEA9C4304C2A27187_H
#ifndef HARDWAREJOYSTICKTEMPLATEMAP_T17329E301C6E7A6EDDF624C8E7F714C96637FB74_H
#define HARDWAREJOYSTICKTEMPLATEMAP_T17329E301C6E7A6EDDF624C8E7F714C96637FB74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickTemplateMap
struct  HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74  : public HardwareControllerTemplateMap_tCB52D05ACF6814505874AB59323F65EC4D695633
{
public:
	// System.String Rewired.Data.Mapping.HardwareJoystickTemplateMap::controllerName
	String_t* ___controllerName_4;
	// System.String Rewired.Data.Mapping.HardwareJoystickTemplateMap::description
	String_t* ___description_5;
	// System.String Rewired.Data.Mapping.HardwareJoystickTemplateMap::templateGuid
	String_t* ___templateGuid_6;
	// System.String Rewired.Data.Mapping.HardwareJoystickTemplateMap::className
	String_t* ___className_7;
	// Rewired.Data.ControllerTemplateElementIdentifier_Editor[] Rewired.Data.Mapping.HardwareJoystickTemplateMap::elementIdentifiers
	ControllerTemplateElementIdentifier_EditorU5BU5D_t87B20AA01A6CCFBAE5D3E8F8955144061B683CDA* ___elementIdentifiers_8;
	// System.Collections.Generic.List`1<Rewired.Data.Mapping.HardwareJoystickTemplateMap_Entry> Rewired.Data.Mapping.HardwareJoystickTemplateMap::joysticks
	List_1_t0A4167A45AFA6D61B1522ADC5208FFC30594F6DF * ___joysticks_9;
	// Rewired.Data.Mapping.HardwareJoystickTemplateMap_SpecialElementEntry[] Rewired.Data.Mapping.HardwareJoystickTemplateMap::specialElements
	SpecialElementEntryU5BU5D_tB783CEBBEC38B526E421CCB50CF07ACD27235CEB* ___specialElements_10;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap::elementIdentifierIdCounter
	int32_t ___elementIdentifierIdCounter_11;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickTemplateMap::joystickIdCounter
	int32_t ___joystickIdCounter_12;

public:
	inline static int32_t get_offset_of_controllerName_4() { return static_cast<int32_t>(offsetof(HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74, ___controllerName_4)); }
	inline String_t* get_controllerName_4() const { return ___controllerName_4; }
	inline String_t** get_address_of_controllerName_4() { return &___controllerName_4; }
	inline void set_controllerName_4(String_t* value)
	{
		___controllerName_4 = value;
		Il2CppCodeGenWriteBarrier((&___controllerName_4), value);
	}

	inline static int32_t get_offset_of_description_5() { return static_cast<int32_t>(offsetof(HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74, ___description_5)); }
	inline String_t* get_description_5() const { return ___description_5; }
	inline String_t** get_address_of_description_5() { return &___description_5; }
	inline void set_description_5(String_t* value)
	{
		___description_5 = value;
		Il2CppCodeGenWriteBarrier((&___description_5), value);
	}

	inline static int32_t get_offset_of_templateGuid_6() { return static_cast<int32_t>(offsetof(HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74, ___templateGuid_6)); }
	inline String_t* get_templateGuid_6() const { return ___templateGuid_6; }
	inline String_t** get_address_of_templateGuid_6() { return &___templateGuid_6; }
	inline void set_templateGuid_6(String_t* value)
	{
		___templateGuid_6 = value;
		Il2CppCodeGenWriteBarrier((&___templateGuid_6), value);
	}

	inline static int32_t get_offset_of_className_7() { return static_cast<int32_t>(offsetof(HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74, ___className_7)); }
	inline String_t* get_className_7() const { return ___className_7; }
	inline String_t** get_address_of_className_7() { return &___className_7; }
	inline void set_className_7(String_t* value)
	{
		___className_7 = value;
		Il2CppCodeGenWriteBarrier((&___className_7), value);
	}

	inline static int32_t get_offset_of_elementIdentifiers_8() { return static_cast<int32_t>(offsetof(HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74, ___elementIdentifiers_8)); }
	inline ControllerTemplateElementIdentifier_EditorU5BU5D_t87B20AA01A6CCFBAE5D3E8F8955144061B683CDA* get_elementIdentifiers_8() const { return ___elementIdentifiers_8; }
	inline ControllerTemplateElementIdentifier_EditorU5BU5D_t87B20AA01A6CCFBAE5D3E8F8955144061B683CDA** get_address_of_elementIdentifiers_8() { return &___elementIdentifiers_8; }
	inline void set_elementIdentifiers_8(ControllerTemplateElementIdentifier_EditorU5BU5D_t87B20AA01A6CCFBAE5D3E8F8955144061B683CDA* value)
	{
		___elementIdentifiers_8 = value;
		Il2CppCodeGenWriteBarrier((&___elementIdentifiers_8), value);
	}

	inline static int32_t get_offset_of_joysticks_9() { return static_cast<int32_t>(offsetof(HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74, ___joysticks_9)); }
	inline List_1_t0A4167A45AFA6D61B1522ADC5208FFC30594F6DF * get_joysticks_9() const { return ___joysticks_9; }
	inline List_1_t0A4167A45AFA6D61B1522ADC5208FFC30594F6DF ** get_address_of_joysticks_9() { return &___joysticks_9; }
	inline void set_joysticks_9(List_1_t0A4167A45AFA6D61B1522ADC5208FFC30594F6DF * value)
	{
		___joysticks_9 = value;
		Il2CppCodeGenWriteBarrier((&___joysticks_9), value);
	}

	inline static int32_t get_offset_of_specialElements_10() { return static_cast<int32_t>(offsetof(HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74, ___specialElements_10)); }
	inline SpecialElementEntryU5BU5D_tB783CEBBEC38B526E421CCB50CF07ACD27235CEB* get_specialElements_10() const { return ___specialElements_10; }
	inline SpecialElementEntryU5BU5D_tB783CEBBEC38B526E421CCB50CF07ACD27235CEB** get_address_of_specialElements_10() { return &___specialElements_10; }
	inline void set_specialElements_10(SpecialElementEntryU5BU5D_tB783CEBBEC38B526E421CCB50CF07ACD27235CEB* value)
	{
		___specialElements_10 = value;
		Il2CppCodeGenWriteBarrier((&___specialElements_10), value);
	}

	inline static int32_t get_offset_of_elementIdentifierIdCounter_11() { return static_cast<int32_t>(offsetof(HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74, ___elementIdentifierIdCounter_11)); }
	inline int32_t get_elementIdentifierIdCounter_11() const { return ___elementIdentifierIdCounter_11; }
	inline int32_t* get_address_of_elementIdentifierIdCounter_11() { return &___elementIdentifierIdCounter_11; }
	inline void set_elementIdentifierIdCounter_11(int32_t value)
	{
		___elementIdentifierIdCounter_11 = value;
	}

	inline static int32_t get_offset_of_joystickIdCounter_12() { return static_cast<int32_t>(offsetof(HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74, ___joystickIdCounter_12)); }
	inline int32_t get_joystickIdCounter_12() const { return ___joystickIdCounter_12; }
	inline int32_t* get_address_of_joystickIdCounter_12() { return &___joystickIdCounter_12; }
	inline void set_joystickIdCounter_12(int32_t value)
	{
		___joystickIdCounter_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWAREJOYSTICKTEMPLATEMAP_T17329E301C6E7A6EDDF624C8E7F714C96637FB74_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4300 = { sizeof (OHegRriHkFCWxgxTTZaYoitpctfY_tCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4300[5] = 
{
	OHegRriHkFCWxgxTTZaYoitpctfY_tCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	OHegRriHkFCWxgxTTZaYoitpctfY_tCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	OHegRriHkFCWxgxTTZaYoitpctfY_tCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	OHegRriHkFCWxgxTTZaYoitpctfY_tCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	OHegRriHkFCWxgxTTZaYoitpctfY_tCE8E858E64E95FE2CCF5C19D7D15A47CAF1330C9::get_offset_of_BgDlucDEoLoZRSdDIkrIXfxGyiu_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4301 = { sizeof (Platform_NintendoSwitch_t2696C2B97DA28B7AFFF2A0AA8CE74563C99F99C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4301[1] = 
{
	Platform_NintendoSwitch_t2696C2B97DA28B7AFFF2A0AA8CE74563C99F99C8::get_offset_of_variants_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4302 = { sizeof (Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4302[4] = 
{
	Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21::get_offset_of_matchingCriteria_1(),
	Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21::get_offset_of_elements_2(),
	Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21::get_offset_of__axesOrigGame_3(),
	Platform_InternalDriver_Base_t130672B998BA3D805D6AEE615F7BD2C7058D6E21::get_offset_of__buttonsOrigGame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4303 = { sizeof (MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4303[4] = 
{
	MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5::get_offset_of_productName_useRegex_5(),
	MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5::get_offset_of_productName_6(),
	MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5::get_offset_of_vidPid_7(),
	MatchingCriteria_tD7427C7537EDD7AEB728AC2BAC7B767A4B445DD5::get_offset_of_hatCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4304 = { sizeof (Elements_t656A4A2A4154F0048E692B5D67D8596354228F5B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4304[2] = 
{
	Elements_t656A4A2A4154F0048E692B5D67D8596354228F5B::get_offset_of_axes_0(),
	Elements_t656A4A2A4154F0048E692B5D67D8596354228F5B::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4305 = { sizeof (Button_t1F0CBE52174773C028D5F99AEBAF1394D5F8B407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4305[3] = 
{
	Button_t1F0CBE52174773C028D5F99AEBAF1394D5F8B407::get_offset_of_sourceHat_13(),
	Button_t1F0CBE52174773C028D5F99AEBAF1394D5F8B407::get_offset_of_sourceHatDirection_14(),
	Button_t1F0CBE52174773C028D5F99AEBAF1394D5F8B407::get_offset_of_sourceHatType_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4306 = { sizeof (Axis_tA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4306[4] = 
{
	Axis_tA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D::get_offset_of_sourceHat_16(),
	Axis_tA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D::get_offset_of_sourceHatDirection_17(),
	Axis_tA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D::get_offset_of_sourceHatType_18(),
	Axis_tA2E6BADC8DEB5ADA265263556D7C9004D10BDB4D::get_offset_of_sourceHatRange_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4307 = { sizeof (RtuSLRsCmKuxFIQNJOVKdlNqgzE_tA770BD36FE7EED984B466118875ED99A2E7B79F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4307[5] = 
{
	RtuSLRsCmKuxFIQNJOVKdlNqgzE_tA770BD36FE7EED984B466118875ED99A2E7B79F2::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	RtuSLRsCmKuxFIQNJOVKdlNqgzE_tA770BD36FE7EED984B466118875ED99A2E7B79F2::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	RtuSLRsCmKuxFIQNJOVKdlNqgzE_tA770BD36FE7EED984B466118875ED99A2E7B79F2::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	RtuSLRsCmKuxFIQNJOVKdlNqgzE_tA770BD36FE7EED984B466118875ED99A2E7B79F2::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	RtuSLRsCmKuxFIQNJOVKdlNqgzE_tA770BD36FE7EED984B466118875ED99A2E7B79F2::get_offset_of_QfooGUvlRPCffgHolLoolnUhxmeD_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4308 = { sizeof (UfZZQdlIiUkoyVXNGylyljzDsQN_t2B54B04BE98FF1404C7AB978B376C5862541D535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4308[5] = 
{
	UfZZQdlIiUkoyVXNGylyljzDsQN_t2B54B04BE98FF1404C7AB978B376C5862541D535::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	UfZZQdlIiUkoyVXNGylyljzDsQN_t2B54B04BE98FF1404C7AB978B376C5862541D535::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	UfZZQdlIiUkoyVXNGylyljzDsQN_t2B54B04BE98FF1404C7AB978B376C5862541D535::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	UfZZQdlIiUkoyVXNGylyljzDsQN_t2B54B04BE98FF1404C7AB978B376C5862541D535::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	UfZZQdlIiUkoyVXNGylyljzDsQN_t2B54B04BE98FF1404C7AB978B376C5862541D535::get_offset_of_xKNjJpCLurgrZZiHKtexKxikNFS_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4309 = { sizeof (Platform_InternalDriver_t8A5A88BC6C79D793F1E687A676A4EC95C644007F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4309[1] = 
{
	Platform_InternalDriver_t8A5A88BC6C79D793F1E687A676A4EC95C644007F::get_offset_of_variants_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4310 = { sizeof (Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4310[2] = 
{
	Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE::get_offset_of_matchingCriteria_1(),
	Platform_SDL2_Base_t00BE1A6DA12A0C9133AFBCF942D80B9A0CB794EE::get_offset_of_elements_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4311 = { sizeof (MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4311[8] = 
{
	MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA::get_offset_of_hatCount_4(),
	MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA::get_offset_of_manufacturer_useRegex_5(),
	MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA::get_offset_of_productName_useRegex_6(),
	MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA::get_offset_of_systemName_useRegex_7(),
	MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA::get_offset_of_manufacturer_8(),
	MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA::get_offset_of_productName_9(),
	MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA::get_offset_of_systemName_10(),
	MatchingCriteria_tCE6CD5A42D352239F501CC5F1A5F33125857D8CA::get_offset_of_productGUID_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4312 = { sizeof (ElementCount_t087149C9BF78BF1188AAEACAE1911000DD398F89), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4312[1] = 
{
	ElementCount_t087149C9BF78BF1188AAEACAE1911000DD398F89::get_offset_of_hatCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4313 = { sizeof (Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4313[2] = 
{
	Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24::get_offset_of_axes_0(),
	Elements_t088498B33BCB6FB61A33B3CEC46957FA6623FC24::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4314 = { sizeof (npSZmSmhLjrWsDCbyDTJbboxwPZ_tA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4314[5] = 
{
	npSZmSmhLjrWsDCbyDTJbboxwPZ_tA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	npSZmSmhLjrWsDCbyDTJbboxwPZ_tA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	npSZmSmhLjrWsDCbyDTJbboxwPZ_tA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	npSZmSmhLjrWsDCbyDTJbboxwPZ_tA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	npSZmSmhLjrWsDCbyDTJbboxwPZ_tA94DCA52AF8BA0DE49781A49F9E685CF8097BAEF::get_offset_of_oGFjPAXAAzOpDCMxHYfXjDOrGqNj_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4315 = { sizeof (VpBAYyBibKpmZGODcPslhxzuFZe_tE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4315[5] = 
{
	VpBAYyBibKpmZGODcPslhxzuFZe_tE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	VpBAYyBibKpmZGODcPslhxzuFZe_tE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	VpBAYyBibKpmZGODcPslhxzuFZe_tE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	VpBAYyBibKpmZGODcPslhxzuFZe_tE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	VpBAYyBibKpmZGODcPslhxzuFZe_tE599C3EF2310DCAD9B5C70DA8FC5EDFBBAD1EE30::get_offset_of_szdBYzMOzBEiNOyTEALnijcajARk_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4316 = { sizeof (Element_tF82C07643751F54246D1E673496F2473BADBAD89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4317 = { sizeof (Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4317[14] = 
{
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_elementIdentifier_0(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_sourceType_1(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_sourceButton_2(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_sourceAxis_3(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_sourceAxisPole_4(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_axisDeadZone_5(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_sourceHat_6(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_sourceHatType_7(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_sourceHatDirection_8(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_requireMultipleButtons_9(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_requiredButtons_10(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_ignoreIfButtonsActive_11(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_ignoreIfButtonsActiveButtons_12(),
	Button_tD33661ADF917D2C5EB39121A35878FCB0C95F734::get_offset_of_buttonInfo_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4318 = { sizeof (Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4318[17] = 
{
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_elementIdentifier_0(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_sourceType_1(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_sourceAxis_2(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_sourceAxisRange_3(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_invert_4(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_axisDeadZone_5(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_calibrateAxis_6(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_axisZero_7(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_axisMin_8(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_axisMax_9(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_alternateCalibrations_10(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_axisInfo_11(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_sourceButton_12(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_buttonAxisContribution_13(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_sourceHat_14(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_sourceHatDirection_15(),
	Axis_tA2DF7A0AF80EF7950C44018B1EFF17460F5F4A39::get_offset_of_sourceHatRange_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4319 = { sizeof (DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4319[6] = 
{
	DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74::get_offset_of_LGqEJTUIaUohKyYOPHcKkgWwfFY_4(),
	DWDOuWhzCAFDNGhrTdSePriVPdI_t91E67F0372694CC0677E30D10669664E019D5A74::get_offset_of_kYgrjxfdNwsUSkHBcOTxeZezFoB_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4320 = { sizeof (vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4320[6] = 
{
	vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6::get_offset_of_YxkCWVIBkqTatlcFupPKznTVXyU_4(),
	vIcamHEsifEzhrnLOcviGZJdxCyA_tB5CD5A134029C163B14277BB6281FD1B6726B7A6::get_offset_of_KKuxmoDgeRdLyJsMGksADDUIEEWv_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4321 = { sizeof (Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4321[1] = 
{
	Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A::get_offset_of_variants_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4322 = { sizeof (Platform_Steam_Base_tF32E32817D4F61E024BC61EA2D01BFB9C8E5690F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4322[2] = 
{
	Platform_Steam_Base_tF32E32817D4F61E024BC61EA2D01BFB9C8E5690F::get_offset_of_matchingCriteria_1(),
	Platform_Steam_Base_tF32E32817D4F61E024BC61EA2D01BFB9C8E5690F::get_offset_of_elements_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4323 = { sizeof (MatchingCriteria_t722FD8AEF7B412C35CE3507CFCBDF57E5BEE4062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4324 = { sizeof (Elements_tF245BB8E30C6E928077EEBF35DD28D1B5EC292A7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4325 = { sizeof (Platform_Steam_tDC1F4D175B21289CADE6C876D6B697B3675C1676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4325[1] = 
{
	Platform_Steam_tDC1F4D175B21289CADE6C876D6B697B3675C1676::get_offset_of_variants_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4326 = { sizeof (Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4326[4] = 
{
	Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295::get_offset_of_matchingCriteria_1(),
	Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295::get_offset_of_elements_2(),
	Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295::get_offset_of__axesOrigGame_3(),
	Platform_WebGL_Base_t0E47618689C13B4A5BC159DE9975829A2534A295::get_offset_of__buttonsOrigGame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4327 = { sizeof (MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4327[6] = 
{
	MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587::get_offset_of_productName_useRegex_5(),
	MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587::get_offset_of_productName_6(),
	MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587::get_offset_of_productGUID_7(),
	MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587::get_offset_of_mapping_8(),
	MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587::get_offset_of_elementCount_9(),
	MatchingCriteria_t17AADED3B6E208F07E9D67778843352BC980A587::get_offset_of_clientInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4328 = { sizeof (ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4328[6] = 
{
	ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A::get_offset_of_browser_0(),
	ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A::get_offset_of_browserVersionMin_1(),
	ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A::get_offset_of_browserVersionMax_2(),
	ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A::get_offset_of_os_3(),
	ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A::get_offset_of_osVersionMin_4(),
	ClientInfo_t861C31EFF3BB9900E4362653BE3451F9171E059A::get_offset_of_osVersionMax_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4329 = { sizeof (Elements_tB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4329[2] = 
{
	Elements_tB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022::get_offset_of_axes_0(),
	Elements_tB2E0C04F68E02A4DFE5BE7FC6F757C4A8AFAC022::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4330 = { sizeof (Button_t8FDD2499024E761C938C105D1958EB05F580B8E2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4331 = { sizeof (Axis_tDF1E851680691A45E36BC337FFC785D8CADAD56A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4332 = { sizeof (heXSrBbVNdfDNVYIZQneqmiKFDyk_t01E1558A075EF9DA08743233E7C2B00F769DF914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4332[5] = 
{
	heXSrBbVNdfDNVYIZQneqmiKFDyk_t01E1558A075EF9DA08743233E7C2B00F769DF914::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	heXSrBbVNdfDNVYIZQneqmiKFDyk_t01E1558A075EF9DA08743233E7C2B00F769DF914::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	heXSrBbVNdfDNVYIZQneqmiKFDyk_t01E1558A075EF9DA08743233E7C2B00F769DF914::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	heXSrBbVNdfDNVYIZQneqmiKFDyk_t01E1558A075EF9DA08743233E7C2B00F769DF914::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	heXSrBbVNdfDNVYIZQneqmiKFDyk_t01E1558A075EF9DA08743233E7C2B00F769DF914::get_offset_of_uZfwYVMWOeVjGdOLmpFJhiBPqqi_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4333 = { sizeof (aeVrRnduXidXhBkxBczKtGXwcww_t57F24F185240CA9A922A55B97B19CF698A68A60C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4333[5] = 
{
	aeVrRnduXidXhBkxBczKtGXwcww_t57F24F185240CA9A922A55B97B19CF698A68A60C::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	aeVrRnduXidXhBkxBczKtGXwcww_t57F24F185240CA9A922A55B97B19CF698A68A60C::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	aeVrRnduXidXhBkxBczKtGXwcww_t57F24F185240CA9A922A55B97B19CF698A68A60C::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	aeVrRnduXidXhBkxBczKtGXwcww_t57F24F185240CA9A922A55B97B19CF698A68A60C::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	aeVrRnduXidXhBkxBczKtGXwcww_t57F24F185240CA9A922A55B97B19CF698A68A60C::get_offset_of_VhcVrGiSXfYUOQpmCimLWGMiopq_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4334 = { sizeof (Platform_WebGL_t7C026B328916B42D37B442AE084EABCB320F318A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4334[1] = 
{
	Platform_WebGL_t7C026B328916B42D37B442AE084EABCB320F318A::get_offset_of_variants_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4335 = { sizeof (EjmkvxlXoHRRczsdGIQdOwhTdKHK_tBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4335[5] = 
{
	EjmkvxlXoHRRczsdGIQdOwhTdKHK_tBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	EjmkvxlXoHRRczsdGIQdOwhTdKHK_tBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	EjmkvxlXoHRRczsdGIQdOwhTdKHK_tBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	EjmkvxlXoHRRczsdGIQdOwhTdKHK_tBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	EjmkvxlXoHRRczsdGIQdOwhTdKHK_tBFA96BD9AFF32B1D2A21E2AF23A5031233F5BF06::get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4336 = { sizeof (VWAfIzERbRbMOyRyCKHzqvAxSsik_tA49E49245CC40AB29644D73EEC167BD87D5FFEAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4336[5] = 
{
	VWAfIzERbRbMOyRyCKHzqvAxSsik_tA49E49245CC40AB29644D73EEC167BD87D5FFEAF::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	VWAfIzERbRbMOyRyCKHzqvAxSsik_tA49E49245CC40AB29644D73EEC167BD87D5FFEAF::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	VWAfIzERbRbMOyRyCKHzqvAxSsik_tA49E49245CC40AB29644D73EEC167BD87D5FFEAF::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	VWAfIzERbRbMOyRyCKHzqvAxSsik_tA49E49245CC40AB29644D73EEC167BD87D5FFEAF::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	VWAfIzERbRbMOyRyCKHzqvAxSsik_tA49E49245CC40AB29644D73EEC167BD87D5FFEAF::get_offset_of_yLrOOaqYzmIyaXSCvApOKYnGotw_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4337 = { sizeof (TptaEHmdHlOFhVFWEoLpnDpUddB_t7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4337[5] = 
{
	TptaEHmdHlOFhVFWEoLpnDpUddB_t7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	TptaEHmdHlOFhVFWEoLpnDpUddB_t7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	TptaEHmdHlOFhVFWEoLpnDpUddB_t7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	TptaEHmdHlOFhVFWEoLpnDpUddB_t7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	TptaEHmdHlOFhVFWEoLpnDpUddB_t7CAAD74A5C0C180D729ABA0AFE25E3F86CC23D8A::get_offset_of_lEJfSplllLlGeeWbNJFrzDmIVEp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4338 = { sizeof (TzUqnKytlNKvJFprUGslyvFbxeg_tF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4338[5] = 
{
	TzUqnKytlNKvJFprUGslyvFbxeg_tF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	TzUqnKytlNKvJFprUGslyvFbxeg_tF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	TzUqnKytlNKvJFprUGslyvFbxeg_tF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	TzUqnKytlNKvJFprUGslyvFbxeg_tF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	TzUqnKytlNKvJFprUGslyvFbxeg_tF3E75E9B8D9DFCBCA4F0F1100955E4D3579DA5E9::get_offset_of_pRNxTDMuDtMhsXMShfGGUiXeGQk_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4339 = { sizeof (HardwareControllerTemplateMap_tCB52D05ACF6814505874AB59323F65EC4D695633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4340 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4341 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4342 = { sizeof (HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4342[9] = 
{
	HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74::get_offset_of_controllerName_4(),
	HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74::get_offset_of_description_5(),
	HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74::get_offset_of_templateGuid_6(),
	HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74::get_offset_of_className_7(),
	HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74::get_offset_of_elementIdentifiers_8(),
	HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74::get_offset_of_joysticks_9(),
	HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74::get_offset_of_specialElements_10(),
	HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74::get_offset_of_elementIdentifierIdCounter_11(),
	HardwareJoystickTemplateMap_t17329E301C6E7A6EDDF624C8E7F714C96637FB74::get_offset_of_joystickIdCounter_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4343 = { sizeof (Entry_tE166222F1AF07AD57E6D234739AB39929E5A84FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4343[5] = 
{
	Entry_tE166222F1AF07AD57E6D234739AB39929E5A84FF::get_offset_of_id_0(),
	Entry_tE166222F1AF07AD57E6D234739AB39929E5A84FF::get_offset_of_name_1(),
	Entry_tE166222F1AF07AD57E6D234739AB39929E5A84FF::get_offset_of_joystickGuid_2(),
	Entry_tE166222F1AF07AD57E6D234739AB39929E5A84FF::get_offset_of_fileGuid_3(),
	Entry_tE166222F1AF07AD57E6D234739AB39929E5A84FF::get_offset_of_elementIdentifierMappings_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4344 = { sizeof (ElementIdentifierMap_tD6D9491C76F5D9987CDC83D5E6D2E12403D4C864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4344[4] = 
{
	ElementIdentifierMap_tD6D9491C76F5D9987CDC83D5E6D2E12403D4C864::get_offset_of_templateId_0(),
	ElementIdentifierMap_tD6D9491C76F5D9987CDC83D5E6D2E12403D4C864::get_offset_of_joystickId_1(),
	ElementIdentifierMap_tD6D9491C76F5D9987CDC83D5E6D2E12403D4C864::get_offset_of_joystickId2_2(),
	ElementIdentifierMap_tD6D9491C76F5D9987CDC83D5E6D2E12403D4C864::get_offset_of_splitAxis_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4345 = { sizeof (SpecialElementEntry_tAFD3A24233FA5CA2DD106FF59575CDDDF0D47149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4345[2] = 
{
	SpecialElementEntry_tAFD3A24233FA5CA2DD106FF59575CDDDF0D47149::get_offset_of_elementIdentifierId_0(),
	SpecialElementEntry_tAFD3A24233FA5CA2DD106FF59575CDDDF0D47149::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4346 = { sizeof (ojmsnzeGRaAVSFPFaWvMPwwoyKTl_t7DA8D3D9AA0D25A904860868B7B990A346C4C76C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4346[5] = 
{
	ojmsnzeGRaAVSFPFaWvMPwwoyKTl_t7DA8D3D9AA0D25A904860868B7B990A346C4C76C::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	ojmsnzeGRaAVSFPFaWvMPwwoyKTl_t7DA8D3D9AA0D25A904860868B7B990A346C4C76C::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	ojmsnzeGRaAVSFPFaWvMPwwoyKTl_t7DA8D3D9AA0D25A904860868B7B990A346C4C76C::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	ojmsnzeGRaAVSFPFaWvMPwwoyKTl_t7DA8D3D9AA0D25A904860868B7B990A346C4C76C::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	ojmsnzeGRaAVSFPFaWvMPwwoyKTl_t7DA8D3D9AA0D25A904860868B7B990A346C4C76C::get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4347 = { sizeof (yntTKZiOvnnBgLSXIhRFagJODMM_t443007B2F5EF88F5ED01E4BA3295DE86AA0E9578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4347[5] = 
{
	yntTKZiOvnnBgLSXIhRFagJODMM_t443007B2F5EF88F5ED01E4BA3295DE86AA0E9578::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	yntTKZiOvnnBgLSXIhRFagJODMM_t443007B2F5EF88F5ED01E4BA3295DE86AA0E9578::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	yntTKZiOvnnBgLSXIhRFagJODMM_t443007B2F5EF88F5ED01E4BA3295DE86AA0E9578::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	yntTKZiOvnnBgLSXIhRFagJODMM_t443007B2F5EF88F5ED01E4BA3295DE86AA0E9578::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	yntTKZiOvnnBgLSXIhRFagJODMM_t443007B2F5EF88F5ED01E4BA3295DE86AA0E9578::get_offset_of_yLrOOaqYzmIyaXSCvApOKYnGotw_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4348 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4349 = { sizeof (HardwareElementSourceType_t440266C7790C9BE17B04056BE8C554EE04C34675)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4349[5] = 
{
	HardwareElementSourceType_t440266C7790C9BE17B04056BE8C554EE04C34675::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4350 = { sizeof (HardwareElementSourceTypeWithHat_tDC84EB6512EDD5A1BB8A15FFF31201CC0E318520)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4350[6] = 
{
	HardwareElementSourceTypeWithHat_tDC84EB6512EDD5A1BB8A15FFF31201CC0E318520::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4351 = { sizeof (HatDirection_t8B3787E56706986A258EEBCF16F989D2075785BA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4351[9] = 
{
	HatDirection_t8B3787E56706986A258EEBCF16F989D2075785BA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4352 = { sizeof (HatType_t83B584B8E763A7B6924539BA31ECF4BD99E395F3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4352[3] = 
{
	HatType_t83B584B8E763A7B6924539BA31ECF4BD99E395F3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4353 = { sizeof (AxisDirection_tE4953CDA51E7D3BD480078C8768EBCD213C2EF49)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4353[3] = 
{
	AxisDirection_tE4953CDA51E7D3BD480078C8768EBCD213C2EF49::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4354 = { sizeof (HardwareAxisType_t59D6929FB2AFD99B0BD06CCFA53470F5A17E89AA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4354[3] = 
{
	HardwareAxisType_t59D6929FB2AFD99B0BD06CCFA53470F5A17E89AA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4355 = { sizeof (AxisCalibrationType_tE13AFD356AB96E532C90470EC4A227DEF86B598C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4355[4] = 
{
	AxisCalibrationType_tE13AFD356AB96E532C90470EC4A227DEF86B598C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4356 = { sizeof (SpecialAxisType_tDEFE7FBCBA9C5D22178D908D1568E975D1BE513E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4356[3] = 
{
	SpecialAxisType_tDEFE7FBCBA9C5D22178D908D1568E975D1BE513E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4357 = { sizeof (AlternateAxisCalibrationType_t13E810E9252891A41BDB4417F660AE3C9EEA526F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4357[3] = 
{
	AlternateAxisCalibrationType_t13E810E9252891A41BDB4417F660AE3C9EEA526F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4358 = { sizeof (HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4358[3] = 
{
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4::get_offset_of__dataFormat_0(),
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4::get_offset_of__excludeFromPolling_1(),
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4::get_offset_of__specialAxisType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4359 = { sizeof (HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4359[2] = 
{
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273::get_offset_of__excludeFromPolling_0(),
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273::get_offset_of__isPressureSensitive_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4360 = { sizeof (AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4360[9] = 
{
	AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5::get_offset_of__applyRangeCalibration_0(),
	AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5::get_offset_of__invert_1(),
	AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5::get_offset_of__deadZone_2(),
	AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5::get_offset_of__zero_3(),
	AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5::get_offset_of__min_4(),
	AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5::get_offset_of__max_5(),
	AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5::get_offset_of__sensitivityType_6(),
	AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5::get_offset_of__sensitivity_7(),
	AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5::get_offset_of__sensitivityCurve_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4361 = { sizeof (RuntimeData_t5673D05BFF62DAB9EA4A418EEA9C4304C2A27187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4361[4] = 
{
	RuntimeData_t5673D05BFF62DAB9EA4A418EEA9C4304C2A27187::get_offset_of_platform_4(),
	RuntimeData_t5673D05BFF62DAB9EA4A418EEA9C4304C2A27187::get_offset_of_webplayerPlatform_5(),
	RuntimeData_t5673D05BFF62DAB9EA4A418EEA9C4304C2A27187::get_offset_of_editorPlatform_6(),
	RuntimeData_t5673D05BFF62DAB9EA4A418EEA9C4304C2A27187::get_offset_of_libraries_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4362 = { sizeof (EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4362[6] = 
{
	EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9::get_offset_of_windowsStandalone_4(),
	EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9::get_offset_of_windowsStore_5(),
	EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9::get_offset_of_osxStandalone_6(),
	EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9::get_offset_of_linuxStandalone_7(),
	EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9::get_offset_of_webplayer_8(),
	EditorPlatformData_tEE233DE574172C4348010F5BEF4CC55CF384DCB9::get_offset_of_fallback_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4363 = { sizeof (Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4363[1] = 
{
	Platform_tAF8E68882BCD8C635053FA486B819B0235E9CFCA::get_offset_of_libraries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4364 = { sizeof (Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4364[14] = 
{
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__id_0(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__name_1(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__descriptiveName_2(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__startPlaying_3(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__defaultJoystickMaps_4(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__defaultMouseMaps_5(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__defaultKeyboardMaps_6(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__defaultCustomControllerMaps_7(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__startingCustomControllers_8(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__assignMouseOnStart_9(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__assignKeyboardOnStart_10(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__excludeFromControllerAutoAssignment_11(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__controllerMapLayoutManagerSettings_12(),
	Player_Editor_t17BA86B71CF8428935542F1EC81DD218A7EF36B7::get_offset_of__controllerMapEnablerSettings_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4365 = { sizeof (Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4365[3] = 
{
	Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26::get_offset_of__enabled_0(),
	Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26::get_offset_of__categoryId_1(),
	Mapping_t2A45E365FF4F7D770614C838C4F98C4E197A0D26::get_offset_of__layoutId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4366 = { sizeof (ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4366[3] = 
{
	ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98::get_offset_of__enabled_0(),
	ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98::get_offset_of__loadFromUserDataStore_1(),
	ControllerMapLayoutManagerSettings_t19039A51E7FB5D60CE1C64FD41E90ED027EE4C98::get_offset_of__ruleSets_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4367 = { sizeof (ControllerMapEnablerSettings_t34E49435CD1543BBA8B8420276849C0AFE4E02F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4367[2] = 
{
	ControllerMapEnablerSettings_t34E49435CD1543BBA8B8420276849C0AFE4E02F1::get_offset_of__enabled_0(),
	ControllerMapEnablerSettings_t34E49435CD1543BBA8B8420276849C0AFE4E02F1::get_offset_of__ruleSets_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4368 = { sizeof (RuleSetMapping_t0CEF1919C7E8D5DE5660F309FA564049DA4E69F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4368[2] = 
{
	RuleSetMapping_t0CEF1919C7E8D5DE5660F309FA564049DA4E69F2::get_offset_of__enabled_0(),
	RuleSetMapping_t0CEF1919C7E8D5DE5660F309FA564049DA4E69F2::get_offset_of__id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4369 = { sizeof (CreateControllerInfo_t1174758D9532AC61484D41E38E94D3C7CF852CC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4369[2] = 
{
	CreateControllerInfo_t1174758D9532AC61484D41E38E94D3C7CF852CC4::get_offset_of__sourceId_0(),
	CreateControllerInfo_t1174758D9532AC61484D41E38E94D3C7CF852CC4::get_offset_of__tag_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4370 = { sizeof (UserData_t23CF13BC0883AADA4A3457583033292E49A77D48), -1, sizeof(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4370[55] = 
{
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_configVars_0(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_players_1(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_actions_2(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_actionCategories_3(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_actionCategoryMap_4(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_inputBehaviors_5(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_mapCategories_6(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_joystickLayouts_7(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_keyboardLayouts_8(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_mouseLayouts_9(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_customControllerLayouts_10(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_joystickMaps_11(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_keyboardMaps_12(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_mouseMaps_13(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_customControllerMaps_14(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_customControllers_15(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_controllerMapLayoutManagerRuleSets_16(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_controllerMapEnablerRuleSets_17(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_playerIdCounter_18(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_actionIdCounter_19(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_actionCategoryIdCounter_20(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_inputBehaviorIdCounter_21(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_mapCategoryIdCounter_22(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_joystickLayoutIdCounter_23(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_keyboardLayoutIdCounter_24(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_mouseLayoutIdCounter_25(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_customControllerLayoutIdCounter_26(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_joystickMapIdCounter_27(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_keyboardMapIdCounter_28(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_mouseMapIdCounter_29(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_customControllerMapIdCounter_30(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_customControllerIdCounter_31(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_controllerMapLayoutManagerSetIdCounter_32(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_controllerMapEnablerSetIdCounter_33(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_containsActionDelegate_34(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CPlayers_readOnlyU3Ek__BackingField_35(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CActions_readOnlyU3Ek__BackingField_36(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CActionCategories_readOnlyU3Ek__BackingField_37(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CInputBehaviors_readOnlyU3Ek__BackingField_38(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CMapCategories_readOnlyU3Ek__BackingField_39(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CJoystickLayouts_readOnlyU3Ek__BackingField_40(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CKeyboardLayouts_readOnlyU3Ek__BackingField_41(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CMouseLayouts_readOnlyU3Ek__BackingField_42(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CCustomControllerLayouts_readOnlyU3Ek__BackingField_43(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CJoystickMaps_readOnlyU3Ek__BackingField_44(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CKeyboardMaps_readOnlyU3Ek__BackingField_45(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CMouseMaps_readOnlyU3Ek__BackingField_46(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CCustomControllerMaps_readOnlyU3Ek__BackingField_47(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CControllerMapLayoutManagerRuleSets_readOnlyU3Ek__BackingField_48(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48::get_offset_of_U3CControllerMapEnablerRuleSets_readOnlyU3Ek__BackingField_49(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate60_50(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate62_51(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate64_52(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate66_53(),
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate68_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4371 = { sizeof (fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66), -1, sizeof(fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4371[44] = 
{
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_UvtPZKSXeJnDxVJSEasBclzynvF_0(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_WIidHThRtMWAWPYztBTsUQgGahC_1(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_HVfVQjDvaSqnCeUWBJkGGVtUyEr_2(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_AFMhLWAUCrzwxwbRrxIOznFdYOn_3(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_gotEvJAFvtFqZDvpNxgnZpryZZUa_4(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_vSNAAzZNWjxpJKWvEeqQUwMQucG_5(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_mhqDbxUnUExiYStPEGCsJxNXbAc_6(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_nTVoTodzUyJPsANsPjLJsigWXvw_7(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_YxTAfIvDCajfOaPrADetuPvJMNj_8(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_IJOPFSEVQJmcwTcVodgHCPoxrk_9(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_wNIYbxBbEKGwHmkNTVruiNwyrLC_10(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_sQIJBdStgelbqDDvPRPIPAJnAUEE_11(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_UTAFKbChSGhgSyrvJeBrjrcTVqiP_12(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_QpQFpmiwqTLTiLxGczgngBXVDrlq_13(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_EAkfTFIVcsJxmXRWhVaSxIykuMJ_14(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_mptfiioUeYsSSBauHlhQdUpfZMw_15(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_ZQYAxnlLgqDLnrxWKekUYqNoARSk_16(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_UQuijZsMqgNPpdrMEFiBrUgtguM_17(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_FehaJfEEjpeYLfpmUMzkFAbCRwsA_18(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_GMPHKVIYVSfxfzouOasvunIymPL_19(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_mMURPWMpQTiDTaELQZQNlGDUORm_20(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_EkGbeTUTBdDIoDPcAfixArdMdHyG_21(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_OJQStWFoTlmLdwbYTlSqDmvhecEF_22(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_CPbJNfDOyMEaxLGISJMyUUybFdb_23(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_cxXZmRlaCQAyKoQbkTpCKLxMeafJ_24(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_VcsAAbjgYjzjuZBULbyTZUnxEDT_25(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_gkHZsBCnDVFtfkyxjPSvYoYeBAH_26(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_VuMrHWmvddoFySkCJvPQTJiRgjte_27(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_jQkRkqxPYteIQfTNhJWgjBqkqBW_28(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_hXMyukkkNaccKiRnnwzkwGEelOf_29(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_KJrESYbqfbZhDRLPhKWUyBvPeCs_30(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_fsbZNQRjTBLHNOepkccIwgwucJc_31(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_nzFeekGdwmUMlLwcEwKJntNAkEGM_32(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_NfVGdYdohwmJHTsplappHYUOsPsR_33(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_frKVGvyneiZCwFdtOeqcMHrhDOv_34(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_xXuvRzDYDBHeentzKeRYhawKXNU_35(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_WoWqKbSwDuVKPbFnTFWDbGbdmBlC_36(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_qvCbtckBsJVyCpGbnyHgLrvkvvrF_37(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_BsQedYMhzLeOgRBAqlcSwEKRJe_38(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_zPIkxlHoMHhQsRiXhblCcaLZntNr_39(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_NOlflOkpsDJptCcPBDLrHLwIPxiE_40(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_KiMVpqIgjIxAwIUdwQYMdYMAvOS_41(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_fIsQOyHbYrzpWeIqPEBIaAAoAGl_42(),
	fdlAxojmklCKPkEqkHYmFTOHUCz_tF7E0CA3B84559682290E4F4B18A16E84D5E07D66_StaticFields::get_offset_of_fSMotZYUgsyrkDyYRKjUnGvbdUB_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4372 = { sizeof (qYkefkBebypsciWTBlnXilfwNlYB_t92BAD69DDB6B5E2E6881BEEE81A4FB36D4E9C6DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4372[3] = 
{
	qYkefkBebypsciWTBlnXilfwNlYB_t92BAD69DDB6B5E2E6881BEEE81A4FB36D4E9C6DE::get_offset_of_kNVCFCmWKtvcerfyfgOooaREcZT_0(),
	qYkefkBebypsciWTBlnXilfwNlYB_t92BAD69DDB6B5E2E6881BEEE81A4FB36D4E9C6DE::get_offset_of_wTqpkSxtmPJzjdXGhVkxSDXgQnn_1(),
	qYkefkBebypsciWTBlnXilfwNlYB_t92BAD69DDB6B5E2E6881BEEE81A4FB36D4E9C6DE::get_offset_of_ZVntyEgnPndjqVEvkGltHHRFDxI_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4373 = { sizeof (VogdFdaNiNjFfcuBargNhLXvWsXd_t773885846FA1F1AB6C0A66CE64635110D52189B1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4373[4] = 
{
	VogdFdaNiNjFfcuBargNhLXvWsXd_t773885846FA1F1AB6C0A66CE64635110D52189B1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4374 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4374[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4375 = { sizeof (qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7), -1, sizeof(qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4375[15] = 
{
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_FEikJUTNOoLkorMnhCrDzsJJboh_0(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_SgTbKBJGpDdiuIuuGINVETpHRFVa_1(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_GNkuqHIdiyAmglJvHSiuApKqTTr_2(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_ApJgBPKxhYzRTTCtQIlUvChiDHh_3(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_ztRGUsKePehkfkkfXQpcYZPiSlUp_4(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_FkpzToLDwFusqNEnwaZnCinExMY_5(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_DXKpFEwfUTAbcrnFSNdZiDEEYca_6(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_fanCAtBiATkpaRifdlVehGCddkSD_7(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_sIsogoYFBHcssSFmtGLudsUedfW_8(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_waGTpGPLWBJjQDZqHaDpWKDGDvA_9(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_IIhzuPhpMWHpoDhTFTtrmnVgGEEy_10(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_PYJejRfBCKYfFkxfuPgkLIGZSQEO_11(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7::get_offset_of_SJlgjYZrCeAMyfhIkZhzFBwFBASl_12(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7_StaticFields::get_offset_of_zHgvSkrXnaeLQooQlFrBzVUbOvG_13(),
	qEcNdMVgmNokdCmhdodTTEqmHlz_t10B096873136F9E3D09A5074339839425CE5BAA7_StaticFields::get_offset_of_seZfuqFtvNbnRBJoZFKyZHfHiLTi_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4376 = { sizeof (UOLCDnQnmXrLlpvzsjBqSPbQGht_t277D82B3205FB7B547EA923D355DEF3A47D11B2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4376[2] = 
{
	UOLCDnQnmXrLlpvzsjBqSPbQGht_t277D82B3205FB7B547EA923D355DEF3A47D11B2A::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0(),
	UOLCDnQnmXrLlpvzsjBqSPbQGht_t277D82B3205FB7B547EA923D355DEF3A47D11B2A::get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4377 = { sizeof (sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4377[1] = 
{
	sifvSqThLQGuVclMuQKUhwYRlPoN_t212C7580AAB5431C3C054AD4FD560157AA5531E3::get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4378 = { sizeof (UTRiicSKYrYpfaUOifvuFDlLHyd_t38005E0C747F974B17989189926B46E9401794EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4378[3] = 
{
	UTRiicSKYrYpfaUOifvuFDlLHyd_t38005E0C747F974B17989189926B46E9401794EE::get_offset_of_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0(),
	UTRiicSKYrYpfaUOifvuFDlLHyd_t38005E0C747F974B17989189926B46E9401794EE::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	UTRiicSKYrYpfaUOifvuFDlLHyd_t38005E0C747F974B17989189926B46E9401794EE::get_offset_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4379 = { sizeof (orvsyVicQUXENeQpdjenHPgXhoSd_t9E3A101FD193F16EFA0AB47C028A96B8B21A23E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4379[3] = 
{
	orvsyVicQUXENeQpdjenHPgXhoSd_t9E3A101FD193F16EFA0AB47C028A96B8B21A23E7::get_offset_of_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0(),
	orvsyVicQUXENeQpdjenHPgXhoSd_t9E3A101FD193F16EFA0AB47C028A96B8B21A23E7::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	orvsyVicQUXENeQpdjenHPgXhoSd_t9E3A101FD193F16EFA0AB47C028A96B8B21A23E7::get_offset_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4380 = { sizeof (zUFUcerQktswIadVEFajckxxTQ_tFC1D79BFC752F82CB540273B095A07CED5970C72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4380[3] = 
{
	zUFUcerQktswIadVEFajckxxTQ_tFC1D79BFC752F82CB540273B095A07CED5970C72::get_offset_of_yRBXJeDJiaSvuoZWvBMXqrfBkDz_0(),
	zUFUcerQktswIadVEFajckxxTQ_tFC1D79BFC752F82CB540273B095A07CED5970C72::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	zUFUcerQktswIadVEFajckxxTQ_tFC1D79BFC752F82CB540273B095A07CED5970C72::get_offset_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4381 = { sizeof (FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4381[1] = 
{
	FJLrnfWkvQiShLGCYOOXtXsMLrS_tA65FA0A7F76AD34694505AB31036F0E37BBB9913::get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4382 = { sizeof (IJZcRBeLhIqHiqbkGKHQJyLiVVX_tD2B567F857B4C0549850B687B47C11EB8A35A2C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4382[3] = 
{
	IJZcRBeLhIqHiqbkGKHQJyLiVVX_tD2B567F857B4C0549850B687B47C11EB8A35A2C5::get_offset_of_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0(),
	IJZcRBeLhIqHiqbkGKHQJyLiVVX_tD2B567F857B4C0549850B687B47C11EB8A35A2C5::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	IJZcRBeLhIqHiqbkGKHQJyLiVVX_tD2B567F857B4C0549850B687B47C11EB8A35A2C5::get_offset_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4383 = { sizeof (fRZVLhJwOndThSZLelobGYXQDpb_tBF3BC5BE01851163C440E2657BBC5CD6199F8B33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4383[3] = 
{
	fRZVLhJwOndThSZLelobGYXQDpb_tBF3BC5BE01851163C440E2657BBC5CD6199F8B33::get_offset_of_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0(),
	fRZVLhJwOndThSZLelobGYXQDpb_tBF3BC5BE01851163C440E2657BBC5CD6199F8B33::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	fRZVLhJwOndThSZLelobGYXQDpb_tBF3BC5BE01851163C440E2657BBC5CD6199F8B33::get_offset_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4384 = { sizeof (QakRAsQkwrEIYNAAJIkDIyPUMBh_t02E27901404660825F0FBC2F3D61CF0D6647E506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4384[3] = 
{
	QakRAsQkwrEIYNAAJIkDIyPUMBh_t02E27901404660825F0FBC2F3D61CF0D6647E506::get_offset_of_IgEDSugxpNXIhdXRDUlKvxxKtPMB_0(),
	QakRAsQkwrEIYNAAJIkDIyPUMBh_t02E27901404660825F0FBC2F3D61CF0D6647E506::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	QakRAsQkwrEIYNAAJIkDIyPUMBh_t02E27901404660825F0FBC2F3D61CF0D6647E506::get_offset_of_xntvcYLUoptzzWTrXAfZKQVzOVK_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4385 = { sizeof (NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4385[2] = 
{
	NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0(),
	NtFyIiXIvuJrPzjtoNMcQcjFQhu_tFB08F3D16A43B9CB4CBB14E22D181ED812C2976F::get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4386 = { sizeof (NAPqojmPLJojhvLKAoZSDwgXdDD_t9F140326E9435B9C8BA3B9097F963BDEFEE5FBA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4386[3] = 
{
	NAPqojmPLJojhvLKAoZSDwgXdDD_t9F140326E9435B9C8BA3B9097F963BDEFEE5FBA6::get_offset_of_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0(),
	NAPqojmPLJojhvLKAoZSDwgXdDD_t9F140326E9435B9C8BA3B9097F963BDEFEE5FBA6::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	NAPqojmPLJojhvLKAoZSDwgXdDD_t9F140326E9435B9C8BA3B9097F963BDEFEE5FBA6::get_offset_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4387 = { sizeof (glswZMileLueHtMBVTBsHJZVDFQd_t14C0F6080488C7AEACE9CD4F37A3D8D26F2D852E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4387[3] = 
{
	glswZMileLueHtMBVTBsHJZVDFQd_t14C0F6080488C7AEACE9CD4F37A3D8D26F2D852E::get_offset_of_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0(),
	glswZMileLueHtMBVTBsHJZVDFQd_t14C0F6080488C7AEACE9CD4F37A3D8D26F2D852E::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	glswZMileLueHtMBVTBsHJZVDFQd_t14C0F6080488C7AEACE9CD4F37A3D8D26F2D852E::get_offset_of_doEmDrDRMAhaULBVLOODFaMXMSO_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4388 = { sizeof (EVpcCGNnohSVpBMzFUluQToAwxL_tC77C62C648E18669A1736909896107EF73741A96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4388[3] = 
{
	EVpcCGNnohSVpBMzFUluQToAwxL_tC77C62C648E18669A1736909896107EF73741A96::get_offset_of_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0(),
	EVpcCGNnohSVpBMzFUluQToAwxL_tC77C62C648E18669A1736909896107EF73741A96::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	EVpcCGNnohSVpBMzFUluQToAwxL_tC77C62C648E18669A1736909896107EF73741A96::get_offset_of_MlHuhkxTPKDycqbMwTBkBIGihuU_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4389 = { sizeof (vkIQItAMANnUwKiNhfajyaszEWR_t92F5D611EB59564DA780613AD88070B0010B1B78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4389[3] = 
{
	vkIQItAMANnUwKiNhfajyaszEWR_t92F5D611EB59564DA780613AD88070B0010B1B78::get_offset_of_ICgDBhwsJOqNdttFCWCDIQrsJUJ_0(),
	vkIQItAMANnUwKiNhfajyaszEWR_t92F5D611EB59564DA780613AD88070B0010B1B78::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	vkIQItAMANnUwKiNhfajyaszEWR_t92F5D611EB59564DA780613AD88070B0010B1B78::get_offset_of_MlHuhkxTPKDycqbMwTBkBIGihuU_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4390 = { sizeof (fBIlKDTPhFeObzPVPLNsWIWwMRV_t84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4390[2] = 
{
	fBIlKDTPhFeObzPVPLNsWIWwMRV_t84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0(),
	fBIlKDTPhFeObzPVPLNsWIWwMRV_t84C4E88F573B5CF6B9D5EC17192725BF7B9AB75A::get_offset_of_upTZSBfMuYEorlKpXpjtyzocIsB_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4391 = { sizeof (pMUhWAvzpyKUWHgXjJchbuAaouM_t130B30CBE6F351181E7BC28C2186765AF060CFAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4391[3] = 
{
	pMUhWAvzpyKUWHgXjJchbuAaouM_t130B30CBE6F351181E7BC28C2186765AF060CFAB::get_offset_of_vRNayqdHvXglMdVKXeqzKwdwmKy_0(),
	pMUhWAvzpyKUWHgXjJchbuAaouM_t130B30CBE6F351181E7BC28C2186765AF060CFAB::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	pMUhWAvzpyKUWHgXjJchbuAaouM_t130B30CBE6F351181E7BC28C2186765AF060CFAB::get_offset_of_wTqpkSxtmPJzjdXGhVkxSDXgQnn_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4392 = { sizeof (bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5), -1, sizeof(bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4392[3] = 
{
	bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0(),
	bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5::get_offset_of_fTbGTbjBkADLQBKzHERKZMhLtdA_1(),
	bkEsZMWLqQWRgIhnNcnweEhXFrJI_t86263D9C78C2A10FCF92015E80A0AAF1C64009F5_StaticFields::get_offset_of_TPQSqycCaeBumhepwDvKRxltnozF_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4393 = { sizeof (mViWaGpecyFqyVYVNVJivSaHUvH_tC83CB64516C2A9EB0B141CC5D56F86292F68FA52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4393[3] = 
{
	mViWaGpecyFqyVYVNVJivSaHUvH_tC83CB64516C2A9EB0B141CC5D56F86292F68FA52::get_offset_of_ROoUOKoOmHPdALerupegCWsflOz_0(),
	mViWaGpecyFqyVYVNVJivSaHUvH_tC83CB64516C2A9EB0B141CC5D56F86292F68FA52::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	mViWaGpecyFqyVYVNVJivSaHUvH_tC83CB64516C2A9EB0B141CC5D56F86292F68FA52::get_offset_of_doEmDrDRMAhaULBVLOODFaMXMSO_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4394 = { sizeof (CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4394[4] = 
{
	CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971::get_offset_of_ROoUOKoOmHPdALerupegCWsflOz_0(),
	CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971::get_offset_of_ONFkOOHVwwNwinsmwCbKAlnoQkX_2(),
	CduakqgeNqOahwGMecbydkSyXGVi_t6D48E9D33100579CD0E582CBD0BE0870A58D3971::get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4395 = { sizeof (IUJUwBPGtwVwKedCpXrDpyYMoSj_t791891BF6A23CC4871681551B044307F3DC53F61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4395[4] = 
{
	IUJUwBPGtwVwKedCpXrDpyYMoSj_t791891BF6A23CC4871681551B044307F3DC53F61::get_offset_of_ZJXznATWkJySNrKPVKIJNCwndjN_0(),
	IUJUwBPGtwVwKedCpXrDpyYMoSj_t791891BF6A23CC4871681551B044307F3DC53F61::get_offset_of_ROoUOKoOmHPdALerupegCWsflOz_1(),
	IUJUwBPGtwVwKedCpXrDpyYMoSj_t791891BF6A23CC4871681551B044307F3DC53F61::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_2(),
	IUJUwBPGtwVwKedCpXrDpyYMoSj_t791891BF6A23CC4871681551B044307F3DC53F61::get_offset_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4396 = { sizeof (jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95), -1, sizeof(jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4396[3] = 
{
	jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_0(),
	jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95::get_offset_of_fTbGTbjBkADLQBKzHERKZMhLtdA_1(),
	jDowvQWVkIomHXVEUMGnVlhpeXz_tD4C4341C1144FA3F0B9D439B7C7537501188CE95_StaticFields::get_offset_of_jbTUVVWhKMONmSiBrZPsoRVOEug_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4397 = { sizeof (WbsuaOAHYiWSTfHSeGUzYBzdJnQ_t3BF5F59F8AA7F25388D8EA40DB384A1B5678668B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4397[3] = 
{
	WbsuaOAHYiWSTfHSeGUzYBzdJnQ_t3BF5F59F8AA7F25388D8EA40DB384A1B5678668B::get_offset_of_pHFLPIzwyoQlzDoggykLsjJmQtx_0(),
	WbsuaOAHYiWSTfHSeGUzYBzdJnQ_t3BF5F59F8AA7F25388D8EA40DB384A1B5678668B::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	WbsuaOAHYiWSTfHSeGUzYBzdJnQ_t3BF5F59F8AA7F25388D8EA40DB384A1B5678668B::get_offset_of_doEmDrDRMAhaULBVLOODFaMXMSO_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4398 = { sizeof (VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4398[4] = 
{
	VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F::get_offset_of_pHFLPIzwyoQlzDoggykLsjJmQtx_0(),
	VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_1(),
	VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F::get_offset_of_ONFkOOHVwwNwinsmwCbKAlnoQkX_2(),
	VYWeAqBBKwjbwzwIKICDmtpATxj_tC3241E3F84DFEFB6D5ABCABCEB699C780875807F::get_offset_of_RnYngjobuYlteiRkTweuGcJpYtu_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4399 = { sizeof (srBPyUndPHgBOpRvKtKTmaUIlHx_t527D1FE4889972D42620A98A61448BA95F3D52CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4399[4] = 
{
	srBPyUndPHgBOpRvKtKTmaUIlHx_t527D1FE4889972D42620A98A61448BA95F3D52CB::get_offset_of_pzLXCtoFGGhhvjRYfqtjXVZRtPf_0(),
	srBPyUndPHgBOpRvKtKTmaUIlHx_t527D1FE4889972D42620A98A61448BA95F3D52CB::get_offset_of_pHFLPIzwyoQlzDoggykLsjJmQtx_1(),
	srBPyUndPHgBOpRvKtKTmaUIlHx_t527D1FE4889972D42620A98A61448BA95F3D52CB::get_offset_of_NxecGThpOUHPMAqQltxuibhjsBi_2(),
	srBPyUndPHgBOpRvKtKTmaUIlHx_t527D1FE4889972D42620A98A61448BA95F3D52CB::get_offset_of_XaepNDmdlDDRBgSPSIhKadPGxnPE_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
