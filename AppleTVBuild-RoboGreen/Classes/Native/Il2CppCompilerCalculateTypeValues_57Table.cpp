﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t2EFEFA739D64C7BAD901C3FE5F6CC722DC129AF2;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t0F370D46554FA08EB85F093101FF3731C6D93348;
// DG.Tweening.Sequence
struct Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2;
// DG.Tweening.TweenCallback
struct TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83;
// Doozy.Engine.Message/OnMessageHandleDelegate
struct OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D;
// Doozy.Engine.Nody.Models.Graph
struct Graph_t26D9C31435F4455B6EF025E506031462302E7AD5;
// Doozy.Engine.Nody.Models.Socket
struct Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8;
// Doozy.Engine.Soundy.AudioData
struct AudioData_t5195938BA169F80D32B29A92FE22CDBD99F346D6;
// Doozy.Engine.Soundy.SoundyData
struct SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720;
// Doozy.Engine.Touchy.GestureListener
struct GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E;
// Doozy.Engine.Touchy.SimulatedTouch
struct SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0;
// Doozy.Engine.Touchy.TouchInfoEvent
struct TouchInfoEvent_t7C133938279F0BC34E525D4C9F62B88A6B206429;
// Doozy.Engine.UI.Animation.Fade
struct Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85;
// Doozy.Engine.UI.Animation.Move
struct Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0;
// Doozy.Engine.UI.Animation.Rotate
struct Rotate_t6F8FF960182701FD1D2F58856066262753CF688A;
// Doozy.Engine.UI.Animation.Scale
struct Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD;
// Doozy.Engine.UI.Animation.UIAnimation
struct UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C;
// Doozy.Engine.UI.Animation.UIAnimationsDatabase
struct UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E;
// Doozy.Engine.UI.Base.NamesDatabase
struct NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D;
// Doozy.Engine.UI.Base.UIEffect
struct UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB;
// Doozy.Engine.UI.Input.InputData
struct InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC;
// Doozy.Engine.UI.UIPopupDatabase
struct UIPopupDatabase_t2DD34A70147D17B8FB3EE2E8F0C95131852AD382;
// Doozy.Engine.UI.UIView
struct UIView_t65D38836246049951821D4E45B1C81947591EBDF;
// Doozy.Engine.UI.UIViewEvent
struct UIViewEvent_t5DBF09498B7D574591E73E4A18578AE6CCE35F29;
// System.Action`1<Doozy.Engine.Touchy.TouchInfo>
struct Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Delegate>>
struct Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA;
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.FieldInfo>
struct Dictionary_2_tDB555DD6652A3DB55616E5F6EE5FB13C4113B901;
// System.Collections.Generic.List`1<Doozy.Engine.Events.AnimatorEvent>
struct List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5;
// System.Collections.Generic.List`1<Doozy.Engine.Nody.Models.Socket>
struct List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B;
// System.Collections.Generic.List`1<Doozy.Engine.Soundy.AudioData>
struct List_1_t5E5F2D8D8792466A43CA18341D76212F4AD6529E;
// System.Collections.Generic.List`1<Doozy.Engine.Soundy.SoundDatabase>
struct List_1_tC81AEA829B78D727C4AC1CE4F27E604E48B96677;
// System.Collections.Generic.List`1<Doozy.Engine.Soundy.SoundGroupData>
struct List_1_tACA120E9FFB2F8DB8A48B88331EE4C66A4BA6543;
// System.Collections.Generic.List`1<Doozy.Engine.Soundy.SoundyController>
struct List_1_t9BE00C8B071DC24C9723A6663A60205AB423B921;
// System.Collections.Generic.List`1<Doozy.Engine.UI.Animation.UIAnimationData>
struct List_1_t0E33A3C0603495E2EF4F7E425A76DDA4F6162703;
// System.Collections.Generic.List`1<Doozy.Engine.UI.Animation.UIAnimationDatabase>
struct List_1_t41FB19A7A05774A4182316CF9D1128B7B8ABB916;
// System.Collections.Generic.List`1<Doozy.Engine.UI.Base.ListOfNames>
struct List_1_t425AC22162310E5E66FFC809C05DD87C5E6288C7;
// System.Collections.Generic.List`1<Doozy.Engine.UI.Internal.UIViewCategoryName>
struct List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028;
// System.Collections.Generic.List`1<UnityEngine.Touch>
struct List_1_t7EE3B0C4AD74ED82F86902199B1BD59B4459037F;
// System.Func`2<Doozy.Engine.Events.AnimatorEvent,System.Boolean>
struct Func_2_tC06206105288935C0BAF47C89691557006948D2D;
// System.Func`2<Doozy.Engine.Soundy.SoundGroupData,System.Boolean>
struct Func_2_t0ACDC22D8BC11CF2104CBE24E0E0FE293BE4B1C8;
// System.Func`2<Doozy.Engine.Soundy.SoundGroupData,System.String>
struct Func_2_tB1C0C356F5BC072499AF39795FCEC352C207F398;
// System.Func`2<Doozy.Engine.UI.Animation.UIAnimationData,System.Boolean>
struct Func_2_t262FD7FA0685CFBCA5C39E4D2BBE1A7763D0C296;
// System.Func`2<Doozy.Engine.UI.Animation.UIAnimationData,System.String>
struct Func_2_tF6844311A5AA0D3E79F0368CE5AEEF9D9BF7BF32;
// System.Func`2<Doozy.Engine.UI.Animation.UIAnimationDatabase,System.String>
struct Func_2_t6BAC501EA51E783169BE18A236388CA9171B974D;
// System.Func`2<Doozy.Engine.UI.Base.ListOfNames,System.String>
struct Func_2_tB96379E2252E88B031C08DF949C3B57DADF66980;
// System.Func`2<Doozy.Engine.UI.Internal.UIViewCategoryName,System.String>
struct Func_2_tD1F8BEB4EB075BAE0739D7ABDC5C87FA52622C73;
// System.Func`2<System.Linq.IGrouping`2<System.String,Doozy.Engine.Soundy.SoundGroupData>,Doozy.Engine.Soundy.SoundGroupData>
struct Func_2_t0FEA44EB4A1E1E761470708E632F4EF7A661B07C;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Audio.AudioMixerGroup
struct AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MESSAGE_TE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_H
#define MESSAGE_TE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Message
struct  Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E  : public RuntimeObject
{
public:

public:
};

struct Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Delegate>> Doozy.Engine.Message::Handlers
	Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA * ___Handlers_1;
	// Doozy.Engine.Message_OnMessageHandleDelegate Doozy.Engine.Message::OnMessageHandle
	OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D * ___OnMessageHandle_2;

public:
	inline static int32_t get_offset_of_Handlers_1() { return static_cast<int32_t>(offsetof(Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_StaticFields, ___Handlers_1)); }
	inline Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA * get_Handlers_1() const { return ___Handlers_1; }
	inline Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA ** get_address_of_Handlers_1() { return &___Handlers_1; }
	inline void set_Handlers_1(Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA * value)
	{
		___Handlers_1 = value;
		Il2CppCodeGenWriteBarrier((&___Handlers_1), value);
	}

	inline static int32_t get_offset_of_OnMessageHandle_2() { return static_cast<int32_t>(offsetof(Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_StaticFields, ___OnMessageHandle_2)); }
	inline OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D * get_OnMessageHandle_2() const { return ___OnMessageHandle_2; }
	inline OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D ** get_address_of_OnMessageHandle_2() { return &___OnMessageHandle_2; }
	inline void set_OnMessageHandle_2(OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D * value)
	{
		___OnMessageHandle_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnMessageHandle_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGE_TE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_H
#ifndef PASSTHROUGHCONNECTION_TF2D74D39C4C4A2639441B6C52F7C6EF7EB3AB03A_H
#define PASSTHROUGHCONNECTION_TF2D74D39C4C4A2639441B6C52F7C6EF7EB3AB03A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Connections.PassthroughConnection
struct  PassthroughConnection_tF2D74D39C4C4A2639441B6C52F7C6EF7EB3AB03A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSTHROUGHCONNECTION_TF2D74D39C4C4A2639441B6C52F7C6EF7EB3AB03A_H
#ifndef AUDIODATA_T5195938BA169F80D32B29A92FE22CDBD99F346D6_H
#define AUDIODATA_T5195938BA169F80D32B29A92FE22CDBD99F346D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.AudioData
struct  AudioData_t5195938BA169F80D32B29A92FE22CDBD99F346D6  : public RuntimeObject
{
public:
	// UnityEngine.AudioClip Doozy.Engine.Soundy.AudioData::AudioClip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___AudioClip_3;
	// System.Single Doozy.Engine.Soundy.AudioData::Weight
	float ___Weight_4;

public:
	inline static int32_t get_offset_of_AudioClip_3() { return static_cast<int32_t>(offsetof(AudioData_t5195938BA169F80D32B29A92FE22CDBD99F346D6, ___AudioClip_3)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_AudioClip_3() const { return ___AudioClip_3; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_AudioClip_3() { return &___AudioClip_3; }
	inline void set_AudioClip_3(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___AudioClip_3 = value;
		Il2CppCodeGenWriteBarrier((&___AudioClip_3), value);
	}

	inline static int32_t get_offset_of_Weight_4() { return static_cast<int32_t>(offsetof(AudioData_t5195938BA169F80D32B29A92FE22CDBD99F346D6, ___Weight_4)); }
	inline float get_Weight_4() const { return ___Weight_4; }
	inline float* get_address_of_Weight_4() { return &___Weight_4; }
	inline void set_Weight_4(float value)
	{
		___Weight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIODATA_T5195938BA169F80D32B29A92FE22CDBD99F346D6_H
#ifndef U3CU3EC_TB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_H
#define U3CU3EC_TB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundDatabase_<>c
struct  U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_StaticFields
{
public:
	// Doozy.Engine.Soundy.SoundDatabase_<>c Doozy.Engine.Soundy.SoundDatabase_<>c::<>9
	U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0 * ___U3CU3E9_0;
	// System.Func`2<Doozy.Engine.Soundy.SoundGroupData,System.String> Doozy.Engine.Soundy.SoundDatabase_<>c::<>9__17_0
	Func_2_tB1C0C356F5BC072499AF39795FCEC352C207F398 * ___U3CU3E9__17_0_1;
	// System.Func`2<System.Linq.IGrouping`2<System.String,Doozy.Engine.Soundy.SoundGroupData>,Doozy.Engine.Soundy.SoundGroupData> Doozy.Engine.Soundy.SoundDatabase_<>c::<>9__17_1
	Func_2_t0FEA44EB4A1E1E761470708E632F4EF7A661B07C * ___U3CU3E9__17_1_2;
	// System.Func`2<Doozy.Engine.Soundy.SoundGroupData,System.Boolean> Doozy.Engine.Soundy.SoundDatabase_<>c::<>9__18_0
	Func_2_t0ACDC22D8BC11CF2104CBE24E0E0FE293BE4B1C8 * ___U3CU3E9__18_0_3;
	// System.Func`2<Doozy.Engine.Soundy.SoundGroupData,System.String> Doozy.Engine.Soundy.SoundDatabase_<>c::<>9__20_0
	Func_2_tB1C0C356F5BC072499AF39795FCEC352C207F398 * ___U3CU3E9__20_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_StaticFields, ___U3CU3E9__17_0_1)); }
	inline Func_2_tB1C0C356F5BC072499AF39795FCEC352C207F398 * get_U3CU3E9__17_0_1() const { return ___U3CU3E9__17_0_1; }
	inline Func_2_tB1C0C356F5BC072499AF39795FCEC352C207F398 ** get_address_of_U3CU3E9__17_0_1() { return &___U3CU3E9__17_0_1; }
	inline void set_U3CU3E9__17_0_1(Func_2_tB1C0C356F5BC072499AF39795FCEC352C207F398 * value)
	{
		___U3CU3E9__17_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_StaticFields, ___U3CU3E9__17_1_2)); }
	inline Func_2_t0FEA44EB4A1E1E761470708E632F4EF7A661B07C * get_U3CU3E9__17_1_2() const { return ___U3CU3E9__17_1_2; }
	inline Func_2_t0FEA44EB4A1E1E761470708E632F4EF7A661B07C ** get_address_of_U3CU3E9__17_1_2() { return &___U3CU3E9__17_1_2; }
	inline void set_U3CU3E9__17_1_2(Func_2_t0FEA44EB4A1E1E761470708E632F4EF7A661B07C * value)
	{
		___U3CU3E9__17_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_StaticFields, ___U3CU3E9__18_0_3)); }
	inline Func_2_t0ACDC22D8BC11CF2104CBE24E0E0FE293BE4B1C8 * get_U3CU3E9__18_0_3() const { return ___U3CU3E9__18_0_3; }
	inline Func_2_t0ACDC22D8BC11CF2104CBE24E0E0FE293BE4B1C8 ** get_address_of_U3CU3E9__18_0_3() { return &___U3CU3E9__18_0_3; }
	inline void set_U3CU3E9__18_0_3(Func_2_t0ACDC22D8BC11CF2104CBE24E0E0FE293BE4B1C8 * value)
	{
		___U3CU3E9__18_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__20_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_StaticFields, ___U3CU3E9__20_0_4)); }
	inline Func_2_tB1C0C356F5BC072499AF39795FCEC352C207F398 * get_U3CU3E9__20_0_4() const { return ___U3CU3E9__20_0_4; }
	inline Func_2_tB1C0C356F5BC072499AF39795FCEC352C207F398 ** get_address_of_U3CU3E9__20_0_4() { return &___U3CU3E9__20_0_4; }
	inline void set_U3CU3E9__20_0_4(Func_2_tB1C0C356F5BC072499AF39795FCEC352C207F398 * value)
	{
		___U3CU3E9__20_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__20_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_H
#ifndef U3CU3EC_TDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5_H
#define U3CU3EC_TDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.GestureListener_<>c
struct  U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5_StaticFields
{
public:
	// Doozy.Engine.Touchy.GestureListener_<>c Doozy.Engine.Touchy.GestureListener_<>c::<>9
	U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5 * ___U3CU3E9_0;
	// System.Action`1<Doozy.Engine.Touchy.TouchInfo> Doozy.Engine.Touchy.GestureListener_<>c::<>9__13_0
	Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * ___U3CU3E9__13_0_1;
	// System.Action`1<Doozy.Engine.Touchy.TouchInfo> Doozy.Engine.Touchy.GestureListener_<>c::<>9__14_0
	Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * ___U3CU3E9__14_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__13_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5_StaticFields, ___U3CU3E9__13_0_1)); }
	inline Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * get_U3CU3E9__13_0_1() const { return ___U3CU3E9__13_0_1; }
	inline Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A ** get_address_of_U3CU3E9__13_0_1() { return &___U3CU3E9__13_0_1; }
	inline void set_U3CU3E9__13_0_1(Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * value)
	{
		___U3CU3E9__13_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__13_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5_StaticFields, ___U3CU3E9__14_0_2)); }
	inline Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * get_U3CU3E9__14_0_2() const { return ___U3CU3E9__14_0_2; }
	inline Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A ** get_address_of_U3CU3E9__14_0_2() { return &___U3CU3E9__14_0_2; }
	inline void set_U3CU3E9__14_0_2(Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * value)
	{
		___U3CU3E9__14_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5_H
#ifndef U3CSENDGAMEEVENTSINTHENEXTFRAMEU3ED__25_TA18E59F7918146A0837C52E421A4DC83C67C3F23_H
#define U3CSENDGAMEEVENTSINTHENEXTFRAMEU3ED__25_TA18E59F7918146A0837C52E421A4DC83C67C3F23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.GestureListener_<SendGameEventsInTheNextFrame>d__25
struct  U3CSendGameEventsInTheNextFrameU3Ed__25_tA18E59F7918146A0837C52E421A4DC83C67C3F23  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.Touchy.GestureListener_<SendGameEventsInTheNextFrame>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.Touchy.GestureListener_<SendGameEventsInTheNextFrame>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.Touchy.GestureListener Doozy.Engine.Touchy.GestureListener_<SendGameEventsInTheNextFrame>d__25::<>4__this
	GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSendGameEventsInTheNextFrameU3Ed__25_tA18E59F7918146A0837C52E421A4DC83C67C3F23, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSendGameEventsInTheNextFrameU3Ed__25_tA18E59F7918146A0837C52E421A4DC83C67C3F23, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSendGameEventsInTheNextFrameU3Ed__25_tA18E59F7918146A0837C52E421A4DC83C67C3F23, ___U3CU3E4__this_2)); }
	inline GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDGAMEEVENTSINTHENEXTFRAMEU3ED__25_TA18E59F7918146A0837C52E421A4DC83C67C3F23_H
#ifndef TOUCHHELPER_T3D8F618D46469C95E7BF3D16CF42CF3115BD630E_H
#define TOUCHHELPER_T3D8F618D46469C95E7BF3D16CF42CF3115BD630E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.TouchHelper
struct  TouchHelper_t3D8F618D46469C95E7BF3D16CF42CF3115BD630E  : public RuntimeObject
{
public:

public:
};

struct TouchHelper_t3D8F618D46469C95E7BF3D16CF42CF3115BD630E_StaticFields
{
public:
	// Doozy.Engine.Touchy.SimulatedTouch Doozy.Engine.Touchy.TouchHelper::s_lastSimulatedTouch
	SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0 * ___s_lastSimulatedTouch_0;
	// System.Collections.Generic.List`1<UnityEngine.Touch> Doozy.Engine.Touchy.TouchHelper::s_touches
	List_1_t7EE3B0C4AD74ED82F86902199B1BD59B4459037F * ___s_touches_1;

public:
	inline static int32_t get_offset_of_s_lastSimulatedTouch_0() { return static_cast<int32_t>(offsetof(TouchHelper_t3D8F618D46469C95E7BF3D16CF42CF3115BD630E_StaticFields, ___s_lastSimulatedTouch_0)); }
	inline SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0 * get_s_lastSimulatedTouch_0() const { return ___s_lastSimulatedTouch_0; }
	inline SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0 ** get_address_of_s_lastSimulatedTouch_0() { return &___s_lastSimulatedTouch_0; }
	inline void set_s_lastSimulatedTouch_0(SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0 * value)
	{
		___s_lastSimulatedTouch_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_lastSimulatedTouch_0), value);
	}

	inline static int32_t get_offset_of_s_touches_1() { return static_cast<int32_t>(offsetof(TouchHelper_t3D8F618D46469C95E7BF3D16CF42CF3115BD630E_StaticFields, ___s_touches_1)); }
	inline List_1_t7EE3B0C4AD74ED82F86902199B1BD59B4459037F * get_s_touches_1() const { return ___s_touches_1; }
	inline List_1_t7EE3B0C4AD74ED82F86902199B1BD59B4459037F ** get_address_of_s_touches_1() { return &___s_touches_1; }
	inline void set_s_touches_1(List_1_t7EE3B0C4AD74ED82F86902199B1BD59B4459037F * value)
	{
		___s_touches_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_touches_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHHELPER_T3D8F618D46469C95E7BF3D16CF42CF3115BD630E_H
#ifndef U3CU3EC_T45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A_H
#define U3CU3EC_T45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimationDatabase_<>c
struct  U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A_StaticFields
{
public:
	// Doozy.Engine.UI.Animation.UIAnimationDatabase_<>c Doozy.Engine.UI.Animation.UIAnimationDatabase_<>c::<>9
	U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A * ___U3CU3E9_0;
	// System.Func`2<Doozy.Engine.UI.Animation.UIAnimationData,System.Boolean> Doozy.Engine.UI.Animation.UIAnimationDatabase_<>c::<>9__15_0
	Func_2_t262FD7FA0685CFBCA5C39E4D2BBE1A7763D0C296 * ___U3CU3E9__15_0_1;
	// System.Func`2<Doozy.Engine.UI.Animation.UIAnimationData,System.String> Doozy.Engine.UI.Animation.UIAnimationDatabase_<>c::<>9__17_0
	Func_2_tF6844311A5AA0D3E79F0368CE5AEEF9D9BF7BF32 * ___U3CU3E9__17_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A_StaticFields, ___U3CU3E9__15_0_1)); }
	inline Func_2_t262FD7FA0685CFBCA5C39E4D2BBE1A7763D0C296 * get_U3CU3E9__15_0_1() const { return ___U3CU3E9__15_0_1; }
	inline Func_2_t262FD7FA0685CFBCA5C39E4D2BBE1A7763D0C296 ** get_address_of_U3CU3E9__15_0_1() { return &___U3CU3E9__15_0_1; }
	inline void set_U3CU3E9__15_0_1(Func_2_t262FD7FA0685CFBCA5C39E4D2BBE1A7763D0C296 * value)
	{
		___U3CU3E9__15_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__15_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A_StaticFields, ___U3CU3E9__17_0_2)); }
	inline Func_2_tF6844311A5AA0D3E79F0368CE5AEEF9D9BF7BF32 * get_U3CU3E9__17_0_2() const { return ___U3CU3E9__17_0_2; }
	inline Func_2_tF6844311A5AA0D3E79F0368CE5AEEF9D9BF7BF32 ** get_address_of_U3CU3E9__17_0_2() { return &___U3CU3E9__17_0_2; }
	inline void set_U3CU3E9__17_0_2(Func_2_tF6844311A5AA0D3E79F0368CE5AEEF9D9BF7BF32 * value)
	{
		___U3CU3E9__17_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A_H
#ifndef U3CU3EC_T8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E_H
#define U3CU3EC_T8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimationsDatabase_<>c
struct  U3CU3Ec_t8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E_StaticFields
{
public:
	// Doozy.Engine.UI.Animation.UIAnimationsDatabase_<>c Doozy.Engine.UI.Animation.UIAnimationsDatabase_<>c::<>9
	U3CU3Ec_t8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E * ___U3CU3E9_0;
	// System.Func`2<Doozy.Engine.UI.Animation.UIAnimationDatabase,System.String> Doozy.Engine.UI.Animation.UIAnimationsDatabase_<>c::<>9__13_0
	Func_2_t6BAC501EA51E783169BE18A236388CA9171B974D * ___U3CU3E9__13_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__13_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E_StaticFields, ___U3CU3E9__13_0_1)); }
	inline Func_2_t6BAC501EA51E783169BE18A236388CA9171B974D * get_U3CU3E9__13_0_1() const { return ___U3CU3E9__13_0_1; }
	inline Func_2_t6BAC501EA51E783169BE18A236388CA9171B974D ** get_address_of_U3CU3E9__13_0_1() { return &___U3CU3E9__13_0_1; }
	inline void set_U3CU3E9__13_0_1(Func_2_t6BAC501EA51E783169BE18A236388CA9171B974D * value)
	{
		___U3CU3E9__13_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__13_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E_H
#ifndef U3CU3EC__DISPLAYCLASS19_0_T2F9D7F22B9250B7B006049317BE992248CC3DF80_H
#define U3CU3EC__DISPLAYCLASS19_0_T2F9D7F22B9250B7B006049317BE992248CC3DF80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_t2F9D7F22B9250B7B006049317BE992248CC3DF80  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass19_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_0;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass19_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_1;

public:
	inline static int32_t get_offset_of_onStartCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t2F9D7F22B9250B7B006049317BE992248CC3DF80, ___onStartCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_0() const { return ___onStartCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_0() { return &___onStartCallback_0; }
	inline void set_onStartCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_0), value);
	}

	inline static int32_t get_offset_of_onCompleteCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t2F9D7F22B9250B7B006049317BE992248CC3DF80, ___onCompleteCallback_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_1() const { return ___onCompleteCallback_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_1() { return &___onCompleteCallback_1; }
	inline void set_onCompleteCallback_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS19_0_T2F9D7F22B9250B7B006049317BE992248CC3DF80_H
#ifndef U3CU3EC__DISPLAYCLASS20_0_T66065B2A72E9B51E7F49E46EBEB76377B478C8E6_H
#define U3CU3EC__DISPLAYCLASS20_0_T66065B2A72E9B51E7F49E46EBEB76377B478C8E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass20_0
struct  U3CU3Ec__DisplayClass20_0_t66065B2A72E9B51E7F49E46EBEB76377B478C8E6  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass20_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_0;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass20_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_1;

public:
	inline static int32_t get_offset_of_onStartCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_t66065B2A72E9B51E7F49E46EBEB76377B478C8E6, ___onStartCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_0() const { return ___onStartCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_0() { return &___onStartCallback_0; }
	inline void set_onStartCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_0), value);
	}

	inline static int32_t get_offset_of_onCompleteCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_t66065B2A72E9B51E7F49E46EBEB76377B478C8E6, ___onCompleteCallback_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_1() const { return ___onCompleteCallback_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_1() { return &___onCompleteCallback_1; }
	inline void set_onCompleteCallback_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS20_0_T66065B2A72E9B51E7F49E46EBEB76377B478C8E6_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_TFF9AA632B7DAFDC7C547D8BA816362D828AFC3C4_H
#define U3CU3EC__DISPLAYCLASS21_0_TFF9AA632B7DAFDC7C547D8BA816362D828AFC3C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_tFF9AA632B7DAFDC7C547D8BA816362D828AFC3C4  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass21_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_0;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass21_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_1;

public:
	inline static int32_t get_offset_of_onStartCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tFF9AA632B7DAFDC7C547D8BA816362D828AFC3C4, ___onStartCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_0() const { return ___onStartCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_0() { return &___onStartCallback_0; }
	inline void set_onStartCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_0), value);
	}

	inline static int32_t get_offset_of_onCompleteCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tFF9AA632B7DAFDC7C547D8BA816362D828AFC3C4, ___onCompleteCallback_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_1() const { return ___onCompleteCallback_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_1() { return &___onCompleteCallback_1; }
	inline void set_onCompleteCallback_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_TFF9AA632B7DAFDC7C547D8BA816362D828AFC3C4_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T88D2AF56ED233D6FC9E857124C31704C3DFFAFB7_H
#define U3CU3EC__DISPLAYCLASS22_0_T88D2AF56ED233D6FC9E857124C31704C3DFFAFB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t88D2AF56ED233D6FC9E857124C31704C3DFFAFB7  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass22_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_0;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass22_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_1;

public:
	inline static int32_t get_offset_of_onStartCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t88D2AF56ED233D6FC9E857124C31704C3DFFAFB7, ___onStartCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_0() const { return ___onStartCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_0() { return &___onStartCallback_0; }
	inline void set_onStartCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_0), value);
	}

	inline static int32_t get_offset_of_onCompleteCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t88D2AF56ED233D6FC9E857124C31704C3DFFAFB7, ___onCompleteCallback_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_1() const { return ___onCompleteCallback_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_1() { return &___onCompleteCallback_1; }
	inline void set_onCompleteCallback_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T88D2AF56ED233D6FC9E857124C31704C3DFFAFB7_H
#ifndef U3CU3EC__DISPLAYCLASS23_0_T29DB0CCABAC978920A0873CA9297F62A1247E9BB_H
#define U3CU3EC__DISPLAYCLASS23_0_T29DB0CCABAC978920A0873CA9297F62A1247E9BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass23_0
struct  U3CU3Ec__DisplayClass23_0_t29DB0CCABAC978920A0873CA9297F62A1247E9BB  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass23_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_0;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass23_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_1;
	// DG.Tweening.Sequence Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass23_0::loopSequence
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___loopSequence_2;

public:
	inline static int32_t get_offset_of_onCompleteCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_t29DB0CCABAC978920A0873CA9297F62A1247E9BB, ___onCompleteCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_0() const { return ___onCompleteCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_0() { return &___onCompleteCallback_0; }
	inline void set_onCompleteCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_0), value);
	}

	inline static int32_t get_offset_of_onStartCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_t29DB0CCABAC978920A0873CA9297F62A1247E9BB, ___onStartCallback_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_1() const { return ___onStartCallback_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_1() { return &___onStartCallback_1; }
	inline void set_onStartCallback_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_1), value);
	}

	inline static int32_t get_offset_of_loopSequence_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_t29DB0CCABAC978920A0873CA9297F62A1247E9BB, ___loopSequence_2)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_loopSequence_2() const { return ___loopSequence_2; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_loopSequence_2() { return &___loopSequence_2; }
	inline void set_loopSequence_2(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___loopSequence_2 = value;
		Il2CppCodeGenWriteBarrier((&___loopSequence_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS23_0_T29DB0CCABAC978920A0873CA9297F62A1247E9BB_H
#ifndef U3CU3EC__DISPLAYCLASS24_0_TF1206CFBA406C0094C223635951927705FD4D38F_H
#define U3CU3EC__DISPLAYCLASS24_0_TF1206CFBA406C0094C223635951927705FD4D38F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass24_0
struct  U3CU3Ec__DisplayClass24_0_tF1206CFBA406C0094C223635951927705FD4D38F  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass24_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_0;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass24_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_1;
	// DG.Tweening.Sequence Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass24_0::loopSequence
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___loopSequence_2;

public:
	inline static int32_t get_offset_of_onCompleteCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tF1206CFBA406C0094C223635951927705FD4D38F, ___onCompleteCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_0() const { return ___onCompleteCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_0() { return &___onCompleteCallback_0; }
	inline void set_onCompleteCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_0), value);
	}

	inline static int32_t get_offset_of_onStartCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tF1206CFBA406C0094C223635951927705FD4D38F, ___onStartCallback_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_1() const { return ___onStartCallback_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_1() { return &___onStartCallback_1; }
	inline void set_onStartCallback_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_1), value);
	}

	inline static int32_t get_offset_of_loopSequence_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tF1206CFBA406C0094C223635951927705FD4D38F, ___loopSequence_2)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_loopSequence_2() const { return ___loopSequence_2; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_loopSequence_2() { return &___loopSequence_2; }
	inline void set_loopSequence_2(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___loopSequence_2 = value;
		Il2CppCodeGenWriteBarrier((&___loopSequence_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS24_0_TF1206CFBA406C0094C223635951927705FD4D38F_H
#ifndef U3CU3EC__DISPLAYCLASS25_0_TB0B9378FA586263B15202387D5547E8E52BCAC0D_H
#define U3CU3EC__DISPLAYCLASS25_0_TB0B9378FA586263B15202387D5547E8E52BCAC0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass25_0
struct  U3CU3Ec__DisplayClass25_0_tB0B9378FA586263B15202387D5547E8E52BCAC0D  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass25_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_0;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass25_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_1;
	// DG.Tweening.Sequence Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass25_0::loopSequence
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___loopSequence_2;

public:
	inline static int32_t get_offset_of_onCompleteCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_tB0B9378FA586263B15202387D5547E8E52BCAC0D, ___onCompleteCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_0() const { return ___onCompleteCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_0() { return &___onCompleteCallback_0; }
	inline void set_onCompleteCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_0), value);
	}

	inline static int32_t get_offset_of_onStartCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_tB0B9378FA586263B15202387D5547E8E52BCAC0D, ___onStartCallback_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_1() const { return ___onStartCallback_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_1() { return &___onStartCallback_1; }
	inline void set_onStartCallback_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_1), value);
	}

	inline static int32_t get_offset_of_loopSequence_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_tB0B9378FA586263B15202387D5547E8E52BCAC0D, ___loopSequence_2)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_loopSequence_2() const { return ___loopSequence_2; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_loopSequence_2() { return &___loopSequence_2; }
	inline void set_loopSequence_2(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___loopSequence_2 = value;
		Il2CppCodeGenWriteBarrier((&___loopSequence_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS25_0_TB0B9378FA586263B15202387D5547E8E52BCAC0D_H
#ifndef U3CU3EC__DISPLAYCLASS26_0_T0414A0654B7904B5A7AE86341AD7C08014C81770_H
#define U3CU3EC__DISPLAYCLASS26_0_T0414A0654B7904B5A7AE86341AD7C08014C81770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass26_0
struct  U3CU3Ec__DisplayClass26_0_t0414A0654B7904B5A7AE86341AD7C08014C81770  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass26_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_0;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass26_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_1;
	// DG.Tweening.Sequence Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass26_0::loopSequence
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___loopSequence_2;

public:
	inline static int32_t get_offset_of_onCompleteCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_t0414A0654B7904B5A7AE86341AD7C08014C81770, ___onCompleteCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_0() const { return ___onCompleteCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_0() { return &___onCompleteCallback_0; }
	inline void set_onCompleteCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_0), value);
	}

	inline static int32_t get_offset_of_onStartCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_t0414A0654B7904B5A7AE86341AD7C08014C81770, ___onStartCallback_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_1() const { return ___onStartCallback_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_1() { return &___onStartCallback_1; }
	inline void set_onStartCallback_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_1), value);
	}

	inline static int32_t get_offset_of_loopSequence_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_t0414A0654B7904B5A7AE86341AD7C08014C81770, ___loopSequence_2)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_loopSequence_2() const { return ___loopSequence_2; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_loopSequence_2() { return &___loopSequence_2; }
	inline void set_loopSequence_2(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___loopSequence_2 = value;
		Il2CppCodeGenWriteBarrier((&___loopSequence_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS26_0_T0414A0654B7904B5A7AE86341AD7C08014C81770_H
#ifndef U3CU3EC__DISPLAYCLASS30_0_T790946858609600312966E9E1AB3DA69BA830567_H
#define U3CU3EC__DISPLAYCLASS30_0_T790946858609600312966E9E1AB3DA69BA830567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t790946858609600312966E9E1AB3DA69BA830567  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass30_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_0;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass30_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_1;

public:
	inline static int32_t get_offset_of_onStartCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t790946858609600312966E9E1AB3DA69BA830567, ___onStartCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_0() const { return ___onStartCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_0() { return &___onStartCallback_0; }
	inline void set_onStartCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_0), value);
	}

	inline static int32_t get_offset_of_onCompleteCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t790946858609600312966E9E1AB3DA69BA830567, ___onCompleteCallback_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_1() const { return ___onCompleteCallback_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_1() { return &___onCompleteCallback_1; }
	inline void set_onCompleteCallback_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS30_0_T790946858609600312966E9E1AB3DA69BA830567_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_TF2CA7158AD49E2ACF9E72CC50115C62B4420154B_H
#define U3CU3EC__DISPLAYCLASS31_0_TF2CA7158AD49E2ACF9E72CC50115C62B4420154B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_tF2CA7158AD49E2ACF9E72CC50115C62B4420154B  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass31_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_0;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass31_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_1;

public:
	inline static int32_t get_offset_of_onStartCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_tF2CA7158AD49E2ACF9E72CC50115C62B4420154B, ___onStartCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_0() const { return ___onStartCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_0() { return &___onStartCallback_0; }
	inline void set_onStartCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_0), value);
	}

	inline static int32_t get_offset_of_onCompleteCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_tF2CA7158AD49E2ACF9E72CC50115C62B4420154B, ___onCompleteCallback_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_1() const { return ___onCompleteCallback_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_1() { return &___onCompleteCallback_1; }
	inline void set_onCompleteCallback_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_TF2CA7158AD49E2ACF9E72CC50115C62B4420154B_H
#ifndef U3CU3EC__DISPLAYCLASS32_0_T0CB4F51C7F781AA37A05C2C5BE4088FAE60498F1_H
#define U3CU3EC__DISPLAYCLASS32_0_T0CB4F51C7F781AA37A05C2C5BE4088FAE60498F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass32_0
struct  U3CU3Ec__DisplayClass32_0_t0CB4F51C7F781AA37A05C2C5BE4088FAE60498F1  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass32_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_0;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass32_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_1;

public:
	inline static int32_t get_offset_of_onStartCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t0CB4F51C7F781AA37A05C2C5BE4088FAE60498F1, ___onStartCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_0() const { return ___onStartCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_0() { return &___onStartCallback_0; }
	inline void set_onStartCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_0), value);
	}

	inline static int32_t get_offset_of_onCompleteCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t0CB4F51C7F781AA37A05C2C5BE4088FAE60498F1, ___onCompleteCallback_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_1() const { return ___onCompleteCallback_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_1() { return &___onCompleteCallback_1; }
	inline void set_onCompleteCallback_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS32_0_T0CB4F51C7F781AA37A05C2C5BE4088FAE60498F1_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T17EA18FC0EE94072B29A30295F7B0779E7A624B4_H
#define U3CU3EC__DISPLAYCLASS33_0_T17EA18FC0EE94072B29A30295F7B0779E7A624B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t17EA18FC0EE94072B29A30295F7B0779E7A624B4  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass33_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_0;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass33_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_1;

public:
	inline static int32_t get_offset_of_onStartCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t17EA18FC0EE94072B29A30295F7B0779E7A624B4, ___onStartCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_0() const { return ___onStartCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_0() { return &___onStartCallback_0; }
	inline void set_onStartCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_0), value);
	}

	inline static int32_t get_offset_of_onCompleteCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t17EA18FC0EE94072B29A30295F7B0779E7A624B4, ___onCompleteCallback_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_1() const { return ___onCompleteCallback_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_1() { return &___onCompleteCallback_1; }
	inline void set_onCompleteCallback_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T17EA18FC0EE94072B29A30295F7B0779E7A624B4_H
#ifndef U3CU3EC_T19877D93E3BD9B5FE3C1EC0AF34B2C252794D707_H
#define U3CU3EC_T19877D93E3BD9B5FE3C1EC0AF34B2C252794D707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.NamesDatabase_<>c
struct  U3CU3Ec_t19877D93E3BD9B5FE3C1EC0AF34B2C252794D707  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t19877D93E3BD9B5FE3C1EC0AF34B2C252794D707_StaticFields
{
public:
	// Doozy.Engine.UI.Base.NamesDatabase_<>c Doozy.Engine.UI.Base.NamesDatabase_<>c::<>9
	U3CU3Ec_t19877D93E3BD9B5FE3C1EC0AF34B2C252794D707 * ___U3CU3E9_0;
	// System.Func`2<Doozy.Engine.UI.Base.ListOfNames,System.String> Doozy.Engine.UI.Base.NamesDatabase_<>c::<>9__33_0
	Func_2_tB96379E2252E88B031C08DF949C3B57DADF66980 * ___U3CU3E9__33_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t19877D93E3BD9B5FE3C1EC0AF34B2C252794D707_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t19877D93E3BD9B5FE3C1EC0AF34B2C252794D707 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t19877D93E3BD9B5FE3C1EC0AF34B2C252794D707 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t19877D93E3BD9B5FE3C1EC0AF34B2C252794D707 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__33_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t19877D93E3BD9B5FE3C1EC0AF34B2C252794D707_StaticFields, ___U3CU3E9__33_0_1)); }
	inline Func_2_tB96379E2252E88B031C08DF949C3B57DADF66980 * get_U3CU3E9__33_0_1() const { return ___U3CU3E9__33_0_1; }
	inline Func_2_tB96379E2252E88B031C08DF949C3B57DADF66980 ** get_address_of_U3CU3E9__33_0_1() { return &___U3CU3E9__33_0_1; }
	inline void set_U3CU3E9__33_0_1(Func_2_tB96379E2252E88B031C08DF949C3B57DADF66980 * value)
	{
		___U3CU3E9__33_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__33_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T19877D93E3BD9B5FE3C1EC0AF34B2C252794D707_H
#ifndef UIACTION_T7E575EC5B7B0E812AF25B2EF5B745B10155537A7_H
#define UIACTION_T7E575EC5B7B0E812AF25B2EF5B745B10155537A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.UIAction
struct  UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.GameObject> Doozy.Engine.UI.Base.UIAction::Action
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___Action_0;
	// System.Collections.Generic.List`1<Doozy.Engine.Events.AnimatorEvent> Doozy.Engine.UI.Base.UIAction::AnimatorEvents
	List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5 * ___AnimatorEvents_1;
	// Doozy.Engine.UI.Base.UIEffect Doozy.Engine.UI.Base.UIAction::Effect
	UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB * ___Effect_2;
	// UnityEngine.Events.UnityEvent Doozy.Engine.UI.Base.UIAction::Event
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___Event_3;
	// System.Collections.Generic.List`1<System.String> Doozy.Engine.UI.Base.UIAction::GameEvents
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___GameEvents_4;
	// Doozy.Engine.Soundy.SoundyData Doozy.Engine.UI.Base.UIAction::SoundData
	SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720 * ___SoundData_5;
	// UnityEngine.Canvas Doozy.Engine.UI.Base.UIAction::m_canvasForEffect
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_canvasForEffect_6;

public:
	inline static int32_t get_offset_of_Action_0() { return static_cast<int32_t>(offsetof(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7, ___Action_0)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_Action_0() const { return ___Action_0; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_Action_0() { return &___Action_0; }
	inline void set_Action_0(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___Action_0 = value;
		Il2CppCodeGenWriteBarrier((&___Action_0), value);
	}

	inline static int32_t get_offset_of_AnimatorEvents_1() { return static_cast<int32_t>(offsetof(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7, ___AnimatorEvents_1)); }
	inline List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5 * get_AnimatorEvents_1() const { return ___AnimatorEvents_1; }
	inline List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5 ** get_address_of_AnimatorEvents_1() { return &___AnimatorEvents_1; }
	inline void set_AnimatorEvents_1(List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5 * value)
	{
		___AnimatorEvents_1 = value;
		Il2CppCodeGenWriteBarrier((&___AnimatorEvents_1), value);
	}

	inline static int32_t get_offset_of_Effect_2() { return static_cast<int32_t>(offsetof(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7, ___Effect_2)); }
	inline UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB * get_Effect_2() const { return ___Effect_2; }
	inline UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB ** get_address_of_Effect_2() { return &___Effect_2; }
	inline void set_Effect_2(UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB * value)
	{
		___Effect_2 = value;
		Il2CppCodeGenWriteBarrier((&___Effect_2), value);
	}

	inline static int32_t get_offset_of_Event_3() { return static_cast<int32_t>(offsetof(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7, ___Event_3)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_Event_3() const { return ___Event_3; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_Event_3() { return &___Event_3; }
	inline void set_Event_3(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___Event_3 = value;
		Il2CppCodeGenWriteBarrier((&___Event_3), value);
	}

	inline static int32_t get_offset_of_GameEvents_4() { return static_cast<int32_t>(offsetof(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7, ___GameEvents_4)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_GameEvents_4() const { return ___GameEvents_4; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_GameEvents_4() { return &___GameEvents_4; }
	inline void set_GameEvents_4(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___GameEvents_4 = value;
		Il2CppCodeGenWriteBarrier((&___GameEvents_4), value);
	}

	inline static int32_t get_offset_of_SoundData_5() { return static_cast<int32_t>(offsetof(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7, ___SoundData_5)); }
	inline SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720 * get_SoundData_5() const { return ___SoundData_5; }
	inline SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720 ** get_address_of_SoundData_5() { return &___SoundData_5; }
	inline void set_SoundData_5(SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720 * value)
	{
		___SoundData_5 = value;
		Il2CppCodeGenWriteBarrier((&___SoundData_5), value);
	}

	inline static int32_t get_offset_of_m_canvasForEffect_6() { return static_cast<int32_t>(offsetof(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7, ___m_canvasForEffect_6)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_canvasForEffect_6() const { return ___m_canvasForEffect_6; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_canvasForEffect_6() { return &___m_canvasForEffect_6; }
	inline void set_m_canvasForEffect_6(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_canvasForEffect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasForEffect_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIACTION_T7E575EC5B7B0E812AF25B2EF5B745B10155537A7_H
#ifndef U3CU3EC_T63BC486140C51DB2C9CB24E1559AFB01EE1F374E_H
#define U3CU3EC_T63BC486140C51DB2C9CB24E1559AFB01EE1F374E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.UIAction_<>c
struct  U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E_StaticFields
{
public:
	// Doozy.Engine.UI.Base.UIAction_<>c Doozy.Engine.UI.Base.UIAction_<>c::<>9
	U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E * ___U3CU3E9_0;
	// System.Action`1<UnityEngine.GameObject> Doozy.Engine.UI.Base.UIAction_<>c::<>9__23_0
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___U3CU3E9__23_0_1;
	// System.Func`2<Doozy.Engine.Events.AnimatorEvent,System.Boolean> Doozy.Engine.UI.Base.UIAction_<>c::<>9__25_0
	Func_2_tC06206105288935C0BAF47C89691557006948D2D * ___U3CU3E9__25_0_2;
	// System.Func`2<System.String,System.Boolean> Doozy.Engine.UI.Base.UIAction_<>c::<>9__27_0
	Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * ___U3CU3E9__27_0_3;
	// System.Action`1<UnityEngine.GameObject> Doozy.Engine.UI.Base.UIAction_<>c::<>9__35_0
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___U3CU3E9__35_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__23_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E_StaticFields, ___U3CU3E9__23_0_1)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_U3CU3E9__23_0_1() const { return ___U3CU3E9__23_0_1; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_U3CU3E9__23_0_1() { return &___U3CU3E9__23_0_1; }
	inline void set_U3CU3E9__23_0_1(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___U3CU3E9__23_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__23_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E_StaticFields, ___U3CU3E9__25_0_2)); }
	inline Func_2_tC06206105288935C0BAF47C89691557006948D2D * get_U3CU3E9__25_0_2() const { return ___U3CU3E9__25_0_2; }
	inline Func_2_tC06206105288935C0BAF47C89691557006948D2D ** get_address_of_U3CU3E9__25_0_2() { return &___U3CU3E9__25_0_2; }
	inline void set_U3CU3E9__25_0_2(Func_2_tC06206105288935C0BAF47C89691557006948D2D * value)
	{
		___U3CU3E9__25_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E_StaticFields, ___U3CU3E9__27_0_3)); }
	inline Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * get_U3CU3E9__27_0_3() const { return ___U3CU3E9__27_0_3; }
	inline Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC ** get_address_of_U3CU3E9__27_0_3() { return &___U3CU3E9__27_0_3; }
	inline void set_U3CU3E9__27_0_3(Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * value)
	{
		___U3CU3E9__27_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E_StaticFields, ___U3CU3E9__35_0_4)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_U3CU3E9__35_0_4() const { return ___U3CU3E9__35_0_4; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_U3CU3E9__35_0_4() { return &___U3CU3E9__35_0_4; }
	inline void set_U3CU3E9__35_0_4(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___U3CU3E9__35_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__35_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T63BC486140C51DB2C9CB24E1559AFB01EE1F374E_H
#ifndef UIVIEWCATEGORYNAME_T952A945B741872B3CCEA6A8410D543AA2C881FA2_H
#define UIVIEWCATEGORYNAME_T952A945B741872B3CCEA6A8410D543AA2C881FA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Internal.UIViewCategoryName
struct  UIViewCategoryName_t952A945B741872B3CCEA6A8410D543AA2C881FA2  : public RuntimeObject
{
public:
	// System.String Doozy.Engine.UI.Internal.UIViewCategoryName::Category
	String_t* ___Category_3;
	// System.Boolean Doozy.Engine.UI.Internal.UIViewCategoryName::InstantAction
	bool ___InstantAction_4;
	// System.String Doozy.Engine.UI.Internal.UIViewCategoryName::Name
	String_t* ___Name_5;

public:
	inline static int32_t get_offset_of_Category_3() { return static_cast<int32_t>(offsetof(UIViewCategoryName_t952A945B741872B3CCEA6A8410D543AA2C881FA2, ___Category_3)); }
	inline String_t* get_Category_3() const { return ___Category_3; }
	inline String_t** get_address_of_Category_3() { return &___Category_3; }
	inline void set_Category_3(String_t* value)
	{
		___Category_3 = value;
		Il2CppCodeGenWriteBarrier((&___Category_3), value);
	}

	inline static int32_t get_offset_of_InstantAction_4() { return static_cast<int32_t>(offsetof(UIViewCategoryName_t952A945B741872B3CCEA6A8410D543AA2C881FA2, ___InstantAction_4)); }
	inline bool get_InstantAction_4() const { return ___InstantAction_4; }
	inline bool* get_address_of_InstantAction_4() { return &___InstantAction_4; }
	inline void set_InstantAction_4(bool value)
	{
		___InstantAction_4 = value;
	}

	inline static int32_t get_offset_of_Name_5() { return static_cast<int32_t>(offsetof(UIViewCategoryName_t952A945B741872B3CCEA6A8410D543AA2C881FA2, ___Name_5)); }
	inline String_t* get_Name_5() const { return ___Name_5; }
	inline String_t** get_address_of_Name_5() { return &___Name_5; }
	inline void set_Name_5(String_t* value)
	{
		___Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___Name_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWCATEGORYNAME_T952A945B741872B3CCEA6A8410D543AA2C881FA2_H
#ifndef U3CU3EC_T62A0543766338BF66F80BC9E8045F3D9F5E9ACFE_H
#define U3CU3EC_T62A0543766338BF66F80BC9E8045F3D9F5E9ACFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.TimeScaleNode_<>c
struct  U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE_StaticFields
{
public:
	// Doozy.Engine.UI.Nodes.TimeScaleNode_<>c Doozy.Engine.UI.Nodes.TimeScaleNode_<>c::<>9
	U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE * ___U3CU3E9_0;
	// DG.Tweening.Core.DOGetter`1<System.Single> Doozy.Engine.UI.Nodes.TimeScaleNode_<>c::<>9__28_0
	DOGetter_1_t2EFEFA739D64C7BAD901C3FE5F6CC722DC129AF2 * ___U3CU3E9__28_0_1;
	// DG.Tweening.Core.DOSetter`1<System.Single> Doozy.Engine.UI.Nodes.TimeScaleNode_<>c::<>9__28_1
	DOSetter_1_t0F370D46554FA08EB85F093101FF3731C6D93348 * ___U3CU3E9__28_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__28_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE_StaticFields, ___U3CU3E9__28_0_1)); }
	inline DOGetter_1_t2EFEFA739D64C7BAD901C3FE5F6CC722DC129AF2 * get_U3CU3E9__28_0_1() const { return ___U3CU3E9__28_0_1; }
	inline DOGetter_1_t2EFEFA739D64C7BAD901C3FE5F6CC722DC129AF2 ** get_address_of_U3CU3E9__28_0_1() { return &___U3CU3E9__28_0_1; }
	inline void set_U3CU3E9__28_0_1(DOGetter_1_t2EFEFA739D64C7BAD901C3FE5F6CC722DC129AF2 * value)
	{
		___U3CU3E9__28_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__28_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__28_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE_StaticFields, ___U3CU3E9__28_1_2)); }
	inline DOSetter_1_t0F370D46554FA08EB85F093101FF3731C6D93348 * get_U3CU3E9__28_1_2() const { return ___U3CU3E9__28_1_2; }
	inline DOSetter_1_t0F370D46554FA08EB85F093101FF3731C6D93348 ** get_address_of_U3CU3E9__28_1_2() { return &___U3CU3E9__28_1_2; }
	inline void set_U3CU3E9__28_1_2(DOSetter_1_t0F370D46554FA08EB85F093101FF3731C6D93348 * value)
	{
		___U3CU3E9__28_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__28_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T62A0543766338BF66F80BC9E8045F3D9F5E9ACFE_H
#ifndef U3CU3EC_T1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A_H
#define U3CU3EC_T1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.UINode_<>c
struct  U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A_StaticFields
{
public:
	// Doozy.Engine.UI.Nodes.UINode_<>c Doozy.Engine.UI.Nodes.UINode_<>c::<>9
	U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A * ___U3CU3E9_0;
	// System.Func`2<Doozy.Engine.UI.Internal.UIViewCategoryName,System.String> Doozy.Engine.UI.Nodes.UINode_<>c::<>9__26_0
	Func_2_tD1F8BEB4EB075BAE0739D7ABDC5C87FA52622C73 * ___U3CU3E9__26_0_1;
	// System.Func`2<Doozy.Engine.UI.Internal.UIViewCategoryName,System.String> Doozy.Engine.UI.Nodes.UINode_<>c::<>9__26_1
	Func_2_tD1F8BEB4EB075BAE0739D7ABDC5C87FA52622C73 * ___U3CU3E9__26_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A_StaticFields, ___U3CU3E9__26_0_1)); }
	inline Func_2_tD1F8BEB4EB075BAE0739D7ABDC5C87FA52622C73 * get_U3CU3E9__26_0_1() const { return ___U3CU3E9__26_0_1; }
	inline Func_2_tD1F8BEB4EB075BAE0739D7ABDC5C87FA52622C73 ** get_address_of_U3CU3E9__26_0_1() { return &___U3CU3E9__26_0_1; }
	inline void set_U3CU3E9__26_0_1(Func_2_tD1F8BEB4EB075BAE0739D7ABDC5C87FA52622C73 * value)
	{
		___U3CU3E9__26_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A_StaticFields, ___U3CU3E9__26_1_2)); }
	inline Func_2_tD1F8BEB4EB075BAE0739D7ABDC5C87FA52622C73 * get_U3CU3E9__26_1_2() const { return ___U3CU3E9__26_1_2; }
	inline Func_2_tD1F8BEB4EB075BAE0739D7ABDC5C87FA52622C73 ** get_address_of_U3CU3E9__26_1_2() { return &___U3CU3E9__26_1_2; }
	inline void set_U3CU3E9__26_1_2(Func_2_tD1F8BEB4EB075BAE0739D7ABDC5C87FA52622C73 * value)
	{
		___U3CU3E9__26_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef RANGEDFLOAT_T2374617685E2BBFB2ADC9396135C5C1A27133109_H
#define RANGEDFLOAT_T2374617685E2BBFB2ADC9396135C5C1A27133109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.RangedFloat
struct  RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109 
{
public:
	// System.Single Doozy.Engine.RangedFloat::MinValue
	float ___MinValue_0;
	// System.Single Doozy.Engine.RangedFloat::MaxValue
	float ___MaxValue_1;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109, ___MinValue_0)); }
	inline float get_MinValue_0() const { return ___MinValue_0; }
	inline float* get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(float value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109, ___MaxValue_1)); }
	inline float get_MaxValue_1() const { return ___MaxValue_1; }
	inline float* get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(float value)
	{
		___MaxValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEDFLOAT_T2374617685E2BBFB2ADC9396135C5C1A27133109_H
#ifndef WEIGHTEDCONNECTION_T60BE9E9F1D7B09005AC5CC3B3E0329509AAAF484_H
#define WEIGHTEDCONNECTION_T60BE9E9F1D7B09005AC5CC3B3E0329509AAAF484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Connections.WeightedConnection
struct  WeightedConnection_t60BE9E9F1D7B09005AC5CC3B3E0329509AAAF484  : public PassthroughConnection_tF2D74D39C4C4A2639441B6C52F7C6EF7EB3AB03A
{
public:
	// System.Int32 Doozy.Engine.UI.Connections.WeightedConnection::Weight
	int32_t ___Weight_1;

public:
	inline static int32_t get_offset_of_Weight_1() { return static_cast<int32_t>(offsetof(WeightedConnection_t60BE9E9F1D7B09005AC5CC3B3E0329509AAAF484, ___Weight_1)); }
	inline int32_t get_Weight_1() const { return ___Weight_1; }
	inline int32_t* get_address_of_Weight_1() { return &___Weight_1; }
	inline void set_Weight_1(int32_t value)
	{
		___Weight_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEIGHTEDCONNECTION_T60BE9E9F1D7B09005AC5CC3B3E0329509AAAF484_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UNITYEVENT_1_TDB41739993304B231F35321B7159BBD75AF51257_H
#define UNITYEVENT_1_TDB41739993304B231F35321B7159BBD75AF51257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Doozy.Engine.Touchy.TouchInfo>
struct  UnityEvent_1_tDB41739993304B231F35321B7159BBD75AF51257  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_tDB41739993304B231F35321B7159BBD75AF51257, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TDB41739993304B231F35321B7159BBD75AF51257_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#define EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifndef LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#define LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#ifndef ROTATEMODE_T7DB9BF2546AEC18F78B34B2B7111AED4416368C7_H
#define ROTATEMODE_T7DB9BF2546AEC18F78B34B2B7111AED4416368C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.RotateMode
struct  RotateMode_t7DB9BF2546AEC18F78B34B2B7111AED4416368C7 
{
public:
	// System.Int32 DG.Tweening.RotateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotateMode_t7DB9BF2546AEC18F78B34B2B7111AED4416368C7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T7DB9BF2546AEC18F78B34B2B7111AED4416368C7_H
#ifndef NODETYPE_TDA55D450054F4809E7380A0CFE79C0AE83045BAF_H
#define NODETYPE_TDA55D450054F4809E7380A0CFE79C0AE83045BAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Models.NodeType
struct  NodeType_tDA55D450054F4809E7380A0CFE79C0AE83045BAF 
{
public:
	// System.Int32 Doozy.Engine.Nody.Models.NodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NodeType_tDA55D450054F4809E7380A0CFE79C0AE83045BAF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODETYPE_TDA55D450054F4809E7380A0CFE79C0AE83045BAF_H
#ifndef GETSCENEBY_T7E8029F50A80BF74544C7791080F0A2205968CE9_H
#define GETSCENEBY_T7E8029F50A80BF74544C7791080F0A2205968CE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.SceneManagement.GetSceneBy
struct  GetSceneBy_t7E8029F50A80BF74544C7791080F0A2205968CE9 
{
public:
	// System.Int32 Doozy.Engine.SceneManagement.GetSceneBy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GetSceneBy_t7E8029F50A80BF74544C7791080F0A2205968CE9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEBY_T7E8029F50A80BF74544C7791080F0A2205968CE9_H
#ifndef PLAYMODE_TC2F7EE563955D38E4EFAC0C1EBFABF1E0C622015_H
#define PLAYMODE_TC2F7EE563955D38E4EFAC0C1EBFABF1E0C622015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundGroupData_PlayMode
struct  PlayMode_tC2F7EE563955D38E4EFAC0C1EBFABF1E0C622015 
{
public:
	// System.Int32 Doozy.Engine.Soundy.SoundGroupData_PlayMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlayMode_tC2F7EE563955D38E4EFAC0C1EBFABF1E0C622015, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODE_TC2F7EE563955D38E4EFAC0C1EBFABF1E0C622015_H
#ifndef SOUNDSOURCE_T7FA55BAC1FEA972D82594371862BBA8698B3D3E9_H
#define SOUNDSOURCE_T7FA55BAC1FEA972D82594371862BBA8698B3D3E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundSource
struct  SoundSource_t7FA55BAC1FEA972D82594371862BBA8698B3D3E9 
{
public:
	// System.Int32 Doozy.Engine.Soundy.SoundSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SoundSource_t7FA55BAC1FEA972D82594371862BBA8698B3D3E9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDSOURCE_T7FA55BAC1FEA972D82594371862BBA8698B3D3E9_H
#ifndef CARDINALDIRECTION_T74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_H
#define CARDINALDIRECTION_T74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.CardinalDirection
struct  CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3  : public RuntimeObject
{
public:

public:
};

struct CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields
{
public:
	// UnityEngine.Vector2 Doozy.Engine.Touchy.CardinalDirection::None
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___None_0;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.CardinalDirection::Up
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Up_1;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.CardinalDirection::Down
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Down_2;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.CardinalDirection::Right
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Right_3;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.CardinalDirection::Left
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Left_4;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.CardinalDirection::UpRight
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___UpRight_5;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.CardinalDirection::UpLeft
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___UpLeft_6;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.CardinalDirection::DownRight
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___DownRight_7;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.CardinalDirection::DownLeft
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___DownLeft_8;

public:
	inline static int32_t get_offset_of_None_0() { return static_cast<int32_t>(offsetof(CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields, ___None_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_None_0() const { return ___None_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_None_0() { return &___None_0; }
	inline void set_None_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___None_0 = value;
	}

	inline static int32_t get_offset_of_Up_1() { return static_cast<int32_t>(offsetof(CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields, ___Up_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Up_1() const { return ___Up_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Up_1() { return &___Up_1; }
	inline void set_Up_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Up_1 = value;
	}

	inline static int32_t get_offset_of_Down_2() { return static_cast<int32_t>(offsetof(CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields, ___Down_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Down_2() const { return ___Down_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Down_2() { return &___Down_2; }
	inline void set_Down_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Down_2 = value;
	}

	inline static int32_t get_offset_of_Right_3() { return static_cast<int32_t>(offsetof(CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields, ___Right_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Right_3() const { return ___Right_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Right_3() { return &___Right_3; }
	inline void set_Right_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Right_3 = value;
	}

	inline static int32_t get_offset_of_Left_4() { return static_cast<int32_t>(offsetof(CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields, ___Left_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Left_4() const { return ___Left_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Left_4() { return &___Left_4; }
	inline void set_Left_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Left_4 = value;
	}

	inline static int32_t get_offset_of_UpRight_5() { return static_cast<int32_t>(offsetof(CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields, ___UpRight_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_UpRight_5() const { return ___UpRight_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_UpRight_5() { return &___UpRight_5; }
	inline void set_UpRight_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___UpRight_5 = value;
	}

	inline static int32_t get_offset_of_UpLeft_6() { return static_cast<int32_t>(offsetof(CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields, ___UpLeft_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_UpLeft_6() const { return ___UpLeft_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_UpLeft_6() { return &___UpLeft_6; }
	inline void set_UpLeft_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___UpLeft_6 = value;
	}

	inline static int32_t get_offset_of_DownRight_7() { return static_cast<int32_t>(offsetof(CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields, ___DownRight_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_DownRight_7() const { return ___DownRight_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_DownRight_7() { return &___DownRight_7; }
	inline void set_DownRight_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___DownRight_7 = value;
	}

	inline static int32_t get_offset_of_DownLeft_8() { return static_cast<int32_t>(offsetof(CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields, ___DownLeft_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_DownLeft_8() const { return ___DownLeft_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_DownLeft_8() { return &___DownLeft_8; }
	inline void set_DownLeft_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___DownLeft_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARDINALDIRECTION_T74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_H
#ifndef GESTURETYPE_T7B2018D460536412C99F26FB3C8D0433610104B1_H
#define GESTURETYPE_T7B2018D460536412C99F26FB3C8D0433610104B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.GestureType
struct  GestureType_t7B2018D460536412C99F26FB3C8D0433610104B1 
{
public:
	// System.Int32 Doozy.Engine.Touchy.GestureType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GestureType_t7B2018D460536412C99F26FB3C8D0433610104B1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURETYPE_T7B2018D460536412C99F26FB3C8D0433610104B1_H
#ifndef SIMPLESWIPE_T3BEB318CEBE340D43602826A0843D0D23C848473_H
#define SIMPLESWIPE_T3BEB318CEBE340D43602826A0843D0D23C848473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.SimpleSwipe
struct  SimpleSwipe_t3BEB318CEBE340D43602826A0843D0D23C848473 
{
public:
	// System.Int32 Doozy.Engine.Touchy.SimpleSwipe::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SimpleSwipe_t3BEB318CEBE340D43602826A0843D0D23C848473, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESWIPE_T3BEB318CEBE340D43602826A0843D0D23C848473_H
#ifndef SWIPE_TE682C3C53AFA8D69B4A374881EFCFAD97CCCD83D_H
#define SWIPE_TE682C3C53AFA8D69B4A374881EFCFAD97CCCD83D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.Swipe
struct  Swipe_tE682C3C53AFA8D69B4A374881EFCFAD97CCCD83D 
{
public:
	// System.Int32 Doozy.Engine.Touchy.Swipe::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Swipe_tE682C3C53AFA8D69B4A374881EFCFAD97CCCD83D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPE_TE682C3C53AFA8D69B4A374881EFCFAD97CCCD83D_H
#ifndef TOUCHINFOEVENT_T7C133938279F0BC34E525D4C9F62B88A6B206429_H
#define TOUCHINFOEVENT_T7C133938279F0BC34E525D4C9F62B88A6B206429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.TouchInfoEvent
struct  TouchInfoEvent_t7C133938279F0BC34E525D4C9F62B88A6B206429  : public UnityEvent_1_tDB41739993304B231F35321B7159BBD75AF51257
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHINFOEVENT_T7C133938279F0BC34E525D4C9F62B88A6B206429_H
#ifndef ANIMATIONACTION_T8AE7A333593BDCDFDDBA25BA7B6AC165078DDE8F_H
#define ANIMATIONACTION_T8AE7A333593BDCDFDDBA25BA7B6AC165078DDE8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.AnimationAction
struct  AnimationAction_t8AE7A333593BDCDFDDBA25BA7B6AC165078DDE8F 
{
public:
	// System.Int32 Doozy.Engine.UI.Animation.AnimationAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimationAction_t8AE7A333593BDCDFDDBA25BA7B6AC165078DDE8F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONACTION_T8AE7A333593BDCDFDDBA25BA7B6AC165078DDE8F_H
#ifndef ANIMATIONTYPE_TD5E6CBA919BA471B2AE3978400DFBFD83760E003_H
#define ANIMATIONTYPE_TD5E6CBA919BA471B2AE3978400DFBFD83760E003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.AnimationType
struct  AnimationType_tD5E6CBA919BA471B2AE3978400DFBFD83760E003 
{
public:
	// System.Int32 Doozy.Engine.UI.Animation.AnimationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimationType_tD5E6CBA919BA471B2AE3978400DFBFD83760E003, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTYPE_TD5E6CBA919BA471B2AE3978400DFBFD83760E003_H
#ifndef BUTTONANIMATIONTYPE_T574DD43CF3A425FF5B14A6F3AF4561B5D5EAB33F_H
#define BUTTONANIMATIONTYPE_T574DD43CF3A425FF5B14A6F3AF4561B5D5EAB33F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.ButtonAnimationType
struct  ButtonAnimationType_t574DD43CF3A425FF5B14A6F3AF4561B5D5EAB33F 
{
public:
	// System.Int32 Doozy.Engine.UI.Animation.ButtonAnimationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonAnimationType_t574DD43CF3A425FF5B14A6F3AF4561B5D5EAB33F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONANIMATIONTYPE_T574DD43CF3A425FF5B14A6F3AF4561B5D5EAB33F_H
#ifndef BUTTONLOOPANIMATIONTYPE_T50E9901AFE81E2202E3A009B903F0F2131705A12_H
#define BUTTONLOOPANIMATIONTYPE_T50E9901AFE81E2202E3A009B903F0F2131705A12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.ButtonLoopAnimationType
struct  ButtonLoopAnimationType_t50E9901AFE81E2202E3A009B903F0F2131705A12 
{
public:
	// System.Int32 Doozy.Engine.UI.Animation.ButtonLoopAnimationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonLoopAnimationType_t50E9901AFE81E2202E3A009B903F0F2131705A12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONLOOPANIMATIONTYPE_T50E9901AFE81E2202E3A009B903F0F2131705A12_H
#ifndef DIRECTION_T4CA8328698AB79AE745CCB89105C6D8DB8346C6F_H
#define DIRECTION_T4CA8328698AB79AE745CCB89105C6D8DB8346C6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.Direction
struct  Direction_t4CA8328698AB79AE745CCB89105C6D8DB8346C6F 
{
public:
	// System.Int32 Doozy.Engine.UI.Animation.Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_t4CA8328698AB79AE745CCB89105C6D8DB8346C6F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T4CA8328698AB79AE745CCB89105C6D8DB8346C6F_H
#ifndef EASETYPE_T3CACE9D6C8199CA4AB913608DD4C4D6F73A8E932_H
#define EASETYPE_T3CACE9D6C8199CA4AB913608DD4C4D6F73A8E932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.EaseType
struct  EaseType_t3CACE9D6C8199CA4AB913608DD4C4D6F73A8E932 
{
public:
	// System.Int32 Doozy.Engine.UI.Animation.EaseType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EaseType_t3CACE9D6C8199CA4AB913608DD4C4D6F73A8E932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASETYPE_T3CACE9D6C8199CA4AB913608DD4C4D6F73A8E932_H
#ifndef U3CU3EC__DISPLAYCLASS27_0_T5CDCAEBF430A54FC491777998136B83E42D607B4_H
#define U3CU3EC__DISPLAYCLASS27_0_T5CDCAEBF430A54FC491777998136B83E42D607B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass27_0
struct  U3CU3Ec__DisplayClass27_0_t5CDCAEBF430A54FC491777998136B83E42D607B4  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass27_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_0;
	// UnityEngine.RectTransform Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass27_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_1;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass27_0::startValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startValue_2;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass27_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_3;
	// DG.Tweening.TweenCallback Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass27_0::<>9__2
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___U3CU3E9__2_4;

public:
	inline static int32_t get_offset_of_onStartCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t5CDCAEBF430A54FC491777998136B83E42D607B4, ___onStartCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_0() const { return ___onStartCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_0() { return &___onStartCallback_0; }
	inline void set_onStartCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_0), value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t5CDCAEBF430A54FC491777998136B83E42D607B4, ___target_1)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_1() const { return ___target_1; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}

	inline static int32_t get_offset_of_startValue_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t5CDCAEBF430A54FC491777998136B83E42D607B4, ___startValue_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startValue_2() const { return ___startValue_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startValue_2() { return &___startValue_2; }
	inline void set_startValue_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startValue_2 = value;
	}

	inline static int32_t get_offset_of_onCompleteCallback_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t5CDCAEBF430A54FC491777998136B83E42D607B4, ___onCompleteCallback_3)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_3() const { return ___onCompleteCallback_3; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_3() { return &___onCompleteCallback_3; }
	inline void set_onCompleteCallback_3(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t5CDCAEBF430A54FC491777998136B83E42D607B4, ___U3CU3E9__2_4)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_U3CU3E9__2_4() const { return ___U3CU3E9__2_4; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_U3CU3E9__2_4() { return &___U3CU3E9__2_4; }
	inline void set_U3CU3E9__2_4(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___U3CU3E9__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS27_0_T5CDCAEBF430A54FC491777998136B83E42D607B4_H
#ifndef U3CU3EC__DISPLAYCLASS28_0_T6E537E419A8AC0CBFE5800DA06310C218426BF9C_H
#define U3CU3EC__DISPLAYCLASS28_0_T6E537E419A8AC0CBFE5800DA06310C218426BF9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass28_0
struct  U3CU3Ec__DisplayClass28_0_t6E537E419A8AC0CBFE5800DA06310C218426BF9C  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass28_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_0;
	// UnityEngine.RectTransform Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass28_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_1;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass28_0::startValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startValue_2;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass28_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_3;
	// DG.Tweening.TweenCallback Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass28_0::<>9__2
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___U3CU3E9__2_4;

public:
	inline static int32_t get_offset_of_onStartCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t6E537E419A8AC0CBFE5800DA06310C218426BF9C, ___onStartCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_0() const { return ___onStartCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_0() { return &___onStartCallback_0; }
	inline void set_onStartCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_0), value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t6E537E419A8AC0CBFE5800DA06310C218426BF9C, ___target_1)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_1() const { return ___target_1; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}

	inline static int32_t get_offset_of_startValue_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t6E537E419A8AC0CBFE5800DA06310C218426BF9C, ___startValue_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startValue_2() const { return ___startValue_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startValue_2() { return &___startValue_2; }
	inline void set_startValue_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startValue_2 = value;
	}

	inline static int32_t get_offset_of_onCompleteCallback_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t6E537E419A8AC0CBFE5800DA06310C218426BF9C, ___onCompleteCallback_3)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_3() const { return ___onCompleteCallback_3; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_3() { return &___onCompleteCallback_3; }
	inline void set_onCompleteCallback_3(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t6E537E419A8AC0CBFE5800DA06310C218426BF9C, ___U3CU3E9__2_4)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_U3CU3E9__2_4() const { return ___U3CU3E9__2_4; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_U3CU3E9__2_4() { return &___U3CU3E9__2_4; }
	inline void set_U3CU3E9__2_4(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___U3CU3E9__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS28_0_T6E537E419A8AC0CBFE5800DA06310C218426BF9C_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_TDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43_H
#define U3CU3EC__DISPLAYCLASS29_0_TDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_tDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass29_0::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_0;
	// UnityEngine.RectTransform Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass29_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_1;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass29_0::startValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startValue_2;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass29_0::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_3;
	// DG.Tweening.TweenCallback Doozy.Engine.UI.Animation.UIAnimator_<>c__DisplayClass29_0::<>9__2
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___U3CU3E9__2_4;

public:
	inline static int32_t get_offset_of_onStartCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43, ___onStartCallback_0)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_0() const { return ___onStartCallback_0; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_0() { return &___onStartCallback_0; }
	inline void set_onStartCallback_0(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_0), value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43, ___target_1)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_1() const { return ___target_1; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}

	inline static int32_t get_offset_of_startValue_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43, ___startValue_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startValue_2() const { return ___startValue_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startValue_2() { return &___startValue_2; }
	inline void set_startValue_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startValue_2 = value;
	}

	inline static int32_t get_offset_of_onCompleteCallback_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43, ___onCompleteCallback_3)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_3() const { return ___onCompleteCallback_3; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_3() { return &___onCompleteCallback_3; }
	inline void set_onCompleteCallback_3(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43, ___U3CU3E9__2_4)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_U3CU3E9__2_4() const { return ___U3CU3E9__2_4; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_U3CU3E9__2_4() { return &___U3CU3E9__2_4; }
	inline void set_U3CU3E9__2_4(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___U3CU3E9__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_TDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43_H
#ifndef NAMESDATABASETYPE_T47DE159D2D9D15B56BEA1A627CA3AEE7962A4A86_H
#define NAMESDATABASETYPE_T47DE159D2D9D15B56BEA1A627CA3AEE7962A4A86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.NamesDatabaseType
struct  NamesDatabaseType_t47DE159D2D9D15B56BEA1A627CA3AEE7962A4A86 
{
public:
	// System.Int32 Doozy.Engine.UI.Base.NamesDatabaseType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamesDatabaseType_t47DE159D2D9D15B56BEA1A627CA3AEE7962A4A86, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESDATABASETYPE_T47DE159D2D9D15B56BEA1A627CA3AEE7962A4A86_H
#ifndef UICONTAINER_T19EF532BD0C41D5271FE710E5665E0A1845A600B_H
#define UICONTAINER_T19EF532BD0C41D5271FE710E5665E0A1845A600B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.UIContainer
struct  UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B  : public RuntimeObject
{
public:
	// UnityEngine.Canvas Doozy.Engine.UI.Base.UIContainer::Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___Canvas_4;
	// UnityEngine.CanvasGroup Doozy.Engine.UI.Base.UIContainer::CanvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___CanvasGroup_5;
	// System.Boolean Doozy.Engine.UI.Base.UIContainer::DisableCanvas
	bool ___DisableCanvas_6;
	// System.Boolean Doozy.Engine.UI.Base.UIContainer::DisableGameObject
	bool ___DisableGameObject_7;
	// System.Boolean Doozy.Engine.UI.Base.UIContainer::DisableGraphicRaycaster
	bool ___DisableGraphicRaycaster_8;
	// System.Boolean Doozy.Engine.UI.Base.UIContainer::Enabled
	bool ___Enabled_9;
	// UnityEngine.UI.GraphicRaycaster Doozy.Engine.UI.Base.UIContainer::GraphicRaycaster
	GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * ___GraphicRaycaster_10;
	// UnityEngine.RectTransform Doozy.Engine.UI.Base.UIContainer::RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___RectTransform_11;
	// System.Single Doozy.Engine.UI.Base.UIContainer::StartAlpha
	float ___StartAlpha_12;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIContainer::StartPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartPosition_13;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIContainer::StartRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartRotation_14;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIContainer::StartScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartScale_15;

public:
	inline static int32_t get_offset_of_Canvas_4() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___Canvas_4)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_Canvas_4() const { return ___Canvas_4; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_Canvas_4() { return &___Canvas_4; }
	inline void set_Canvas_4(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___Canvas_4 = value;
		Il2CppCodeGenWriteBarrier((&___Canvas_4), value);
	}

	inline static int32_t get_offset_of_CanvasGroup_5() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___CanvasGroup_5)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_CanvasGroup_5() const { return ___CanvasGroup_5; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_CanvasGroup_5() { return &___CanvasGroup_5; }
	inline void set_CanvasGroup_5(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___CanvasGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&___CanvasGroup_5), value);
	}

	inline static int32_t get_offset_of_DisableCanvas_6() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___DisableCanvas_6)); }
	inline bool get_DisableCanvas_6() const { return ___DisableCanvas_6; }
	inline bool* get_address_of_DisableCanvas_6() { return &___DisableCanvas_6; }
	inline void set_DisableCanvas_6(bool value)
	{
		___DisableCanvas_6 = value;
	}

	inline static int32_t get_offset_of_DisableGameObject_7() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___DisableGameObject_7)); }
	inline bool get_DisableGameObject_7() const { return ___DisableGameObject_7; }
	inline bool* get_address_of_DisableGameObject_7() { return &___DisableGameObject_7; }
	inline void set_DisableGameObject_7(bool value)
	{
		___DisableGameObject_7 = value;
	}

	inline static int32_t get_offset_of_DisableGraphicRaycaster_8() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___DisableGraphicRaycaster_8)); }
	inline bool get_DisableGraphicRaycaster_8() const { return ___DisableGraphicRaycaster_8; }
	inline bool* get_address_of_DisableGraphicRaycaster_8() { return &___DisableGraphicRaycaster_8; }
	inline void set_DisableGraphicRaycaster_8(bool value)
	{
		___DisableGraphicRaycaster_8 = value;
	}

	inline static int32_t get_offset_of_Enabled_9() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___Enabled_9)); }
	inline bool get_Enabled_9() const { return ___Enabled_9; }
	inline bool* get_address_of_Enabled_9() { return &___Enabled_9; }
	inline void set_Enabled_9(bool value)
	{
		___Enabled_9 = value;
	}

	inline static int32_t get_offset_of_GraphicRaycaster_10() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___GraphicRaycaster_10)); }
	inline GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * get_GraphicRaycaster_10() const { return ___GraphicRaycaster_10; }
	inline GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 ** get_address_of_GraphicRaycaster_10() { return &___GraphicRaycaster_10; }
	inline void set_GraphicRaycaster_10(GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * value)
	{
		___GraphicRaycaster_10 = value;
		Il2CppCodeGenWriteBarrier((&___GraphicRaycaster_10), value);
	}

	inline static int32_t get_offset_of_RectTransform_11() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___RectTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_RectTransform_11() const { return ___RectTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_RectTransform_11() { return &___RectTransform_11; }
	inline void set_RectTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___RectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___RectTransform_11), value);
	}

	inline static int32_t get_offset_of_StartAlpha_12() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___StartAlpha_12)); }
	inline float get_StartAlpha_12() const { return ___StartAlpha_12; }
	inline float* get_address_of_StartAlpha_12() { return &___StartAlpha_12; }
	inline void set_StartAlpha_12(float value)
	{
		___StartAlpha_12 = value;
	}

	inline static int32_t get_offset_of_StartPosition_13() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___StartPosition_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartPosition_13() const { return ___StartPosition_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartPosition_13() { return &___StartPosition_13; }
	inline void set_StartPosition_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartPosition_13 = value;
	}

	inline static int32_t get_offset_of_StartRotation_14() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___StartRotation_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartRotation_14() const { return ___StartRotation_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartRotation_14() { return &___StartRotation_14; }
	inline void set_StartRotation_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartRotation_14 = value;
	}

	inline static int32_t get_offset_of_StartScale_15() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___StartScale_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartScale_15() const { return ___StartScale_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartScale_15() { return &___StartScale_15; }
	inline void set_StartScale_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartScale_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICONTAINER_T19EF532BD0C41D5271FE710E5665E0A1845A600B_H
#ifndef UICONNECTIONTRIGGER_T258BDC535A9D6BE85D5643658C051A161786E709_H
#define UICONNECTIONTRIGGER_T258BDC535A9D6BE85D5643658C051A161786E709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Connections.UIConnectionTrigger
struct  UIConnectionTrigger_t258BDC535A9D6BE85D5643658C051A161786E709 
{
public:
	// System.Int32 Doozy.Engine.UI.Connections.UIConnectionTrigger::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIConnectionTrigger_t258BDC535A9D6BE85D5643658C051A161786E709, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICONNECTIONTRIGGER_T258BDC535A9D6BE85D5643658C051A161786E709_H
#ifndef DYNAMICSORTING_TE88845A7C1E8C4071C615A938ABB17C18E7D8230_H
#define DYNAMICSORTING_TE88845A7C1E8C4071C615A938ABB17C18E7D8230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.DynamicSorting
struct  DynamicSorting_tE88845A7C1E8C4071C615A938ABB17C18E7D8230 
{
public:
	// System.Int32 Doozy.Engine.UI.DynamicSorting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DynamicSorting_tE88845A7C1E8C4071C615A938ABB17C18E7D8230, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICSORTING_TE88845A7C1E8C4071C615A938ABB17C18E7D8230_H
#ifndef INPUTMODE_T366797558992CE7DCC33C117AEAD8C8AEEE9FB70_H
#define INPUTMODE_T366797558992CE7DCC33C117AEAD8C8AEEE9FB70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Input.InputMode
struct  InputMode_t366797558992CE7DCC33C117AEAD8C8AEEE9FB70 
{
public:
	// System.Int32 Doozy.Engine.UI.Input.InputMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputMode_t366797558992CE7DCC33C117AEAD8C8AEEE9FB70, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMODE_T366797558992CE7DCC33C117AEAD8C8AEEE9FB70_H
#ifndef BACKBUTTONSTATE_TAE2539790826973E14AB750C9D577738BC052ACE_H
#define BACKBUTTONSTATE_TAE2539790826973E14AB750C9D577738BC052ACE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.BackButtonNode_BackButtonState
struct  BackButtonState_tAE2539790826973E14AB750C9D577738BC052ACE 
{
public:
	// System.Int32 Doozy.Engine.UI.Nodes.BackButtonNode_BackButtonState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BackButtonState_tAE2539790826973E14AB750C9D577738BC052ACE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKBUTTONSTATE_TAE2539790826973E14AB750C9D577738BC052ACE_H
#ifndef SOUNDACTIONS_TC86159034630D7DC2DF75A09AEF63950690144B3_H
#define SOUNDACTIONS_TC86159034630D7DC2DF75A09AEF63950690144B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.SoundNode_SoundActions
struct  SoundActions_tC86159034630D7DC2DF75A09AEF63950690144B3 
{
public:
	// System.Int32 Doozy.Engine.UI.Nodes.SoundNode_SoundActions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SoundActions_tC86159034630D7DC2DF75A09AEF63950690144B3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDACTIONS_TC86159034630D7DC2DF75A09AEF63950690144B3_H
#ifndef DRAWERACTION_T2F28EDC561CF29F6E96B1B838E65505EFA29983F_H
#define DRAWERACTION_T2F28EDC561CF29F6E96B1B838E65505EFA29983F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.UIDrawerNode_DrawerAction
struct  DrawerAction_t2F28EDC561CF29F6E96B1B838E65505EFA29983F 
{
public:
	// System.Int32 Doozy.Engine.UI.Nodes.UIDrawerNode_DrawerAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DrawerAction_t2F28EDC561CF29F6E96B1B838E65505EFA29983F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWERACTION_T2F28EDC561CF29F6E96B1B838E65505EFA29983F_H
#ifndef NODESTATE_T2D144849186A3DD6BE3A6DD8D16796EC877247EE_H
#define NODESTATE_T2D144849186A3DD6BE3A6DD8D16796EC877247EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.UINode_NodeState
struct  NodeState_t2D144849186A3DD6BE3A6DD8D16796EC877247EE 
{
public:
	// System.Int32 Doozy.Engine.UI.Nodes.UINode_NodeState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NodeState_t2D144849186A3DD6BE3A6DD8D16796EC877247EE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODESTATE_T2D144849186A3DD6BE3A6DD8D16796EC877247EE_H
#ifndef VIEWACTION_TC1F53B173C52BCF4277C1C2BC392EBF6531D1297_H
#define VIEWACTION_TC1F53B173C52BCF4277C1C2BC392EBF6531D1297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.UINode_ViewAction
struct  ViewAction_tC1F53B173C52BCF4277C1C2BC392EBF6531D1297 
{
public:
	// System.Int32 Doozy.Engine.UI.Nodes.UINode_ViewAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ViewAction_tC1F53B173C52BCF4277C1C2BC392EBF6531D1297, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWACTION_TC1F53B173C52BCF4277C1C2BC392EBF6531D1297_H
#ifndef WAITTYPE_T46CC2C441C408F4DCA4C233B5500176E8460FA22_H
#define WAITTYPE_T46CC2C441C408F4DCA4C233B5500176E8460FA22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.WaitNode_WaitType
struct  WaitType_t46CC2C441C408F4DCA4C233B5500176E8460FA22 
{
public:
	// System.Int32 Doozy.Engine.UI.Nodes.WaitNode_WaitType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WaitType_t46CC2C441C408F4DCA4C233B5500176E8460FA22, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITTYPE_T46CC2C441C408F4DCA4C233B5500176E8460FA22_H
#ifndef POPUPDISPLAYON_T889AC013674992F05BFC1482F6E0F119436B3A62_H
#define POPUPDISPLAYON_T889AC013674992F05BFC1482F6E0F119436B3A62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.PopupDisplayOn
struct  PopupDisplayOn_t889AC013674992F05BFC1482F6E0F119436B3A62 
{
public:
	// System.Int32 Doozy.Engine.UI.PopupDisplayOn::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PopupDisplayOn_t889AC013674992F05BFC1482F6E0F119436B3A62, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPDISPLAYON_T889AC013674992F05BFC1482F6E0F119436B3A62_H
#ifndef SINGLECLICKMODE_TDB58245544CF8536DD5907066283135060CAEEB5_H
#define SINGLECLICKMODE_TDB58245544CF8536DD5907066283135060CAEEB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.SingleClickMode
struct  SingleClickMode_tDB58245544CF8536DD5907066283135060CAEEB5 
{
public:
	// System.Int32 Doozy.Engine.UI.SingleClickMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SingleClickMode_tDB58245544CF8536DD5907066283135060CAEEB5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLECLICKMODE_TDB58245544CF8536DD5907066283135060CAEEB5_H
#ifndef TARGETORIENTATION_TC5F5E22D9BFB8892A068DB6956AE047A4D07AFCC_H
#define TARGETORIENTATION_TC5F5E22D9BFB8892A068DB6956AE047A4D07AFCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.TargetOrientation
struct  TargetOrientation_tC5F5E22D9BFB8892A068DB6956AE047A4D07AFCC 
{
public:
	// System.Int32 Doozy.Engine.UI.TargetOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TargetOrientation_tC5F5E22D9BFB8892A068DB6956AE047A4D07AFCC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETORIENTATION_TC5F5E22D9BFB8892A068DB6956AE047A4D07AFCC_H
#ifndef UIEFFECTBEHAVIOR_T65B6EA484234ECF2B725C4ED8FAFDCF3F8E81F25_H
#define UIEFFECTBEHAVIOR_T65B6EA484234ECF2B725C4ED8FAFDCF3F8E81F25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIEffectBehavior
struct  UIEffectBehavior_t65B6EA484234ECF2B725C4ED8FAFDCF3F8E81F25 
{
public:
	// System.Int32 Doozy.Engine.UI.UIEffectBehavior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIEffectBehavior_t65B6EA484234ECF2B725C4ED8FAFDCF3F8E81F25, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEFFECTBEHAVIOR_T65B6EA484234ECF2B725C4ED8FAFDCF3F8E81F25_H
#ifndef UIVIEWBEHAVIORTYPE_T652DB3CABD15D72ADC5D734651A8CAD49C8B0ECB_H
#define UIVIEWBEHAVIORTYPE_T652DB3CABD15D72ADC5D734651A8CAD49C8B0ECB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIViewBehaviorType
struct  UIViewBehaviorType_t652DB3CABD15D72ADC5D734651A8CAD49C8B0ECB 
{
public:
	// System.Int32 Doozy.Engine.UI.UIViewBehaviorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIViewBehaviorType_t652DB3CABD15D72ADC5D734651A8CAD49C8B0ECB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWBEHAVIORTYPE_T652DB3CABD15D72ADC5D734651A8CAD49C8B0ECB_H
#ifndef UIVIEWSTARTBEHAVIOR_T1C1D97FA8BECE7BDEAC82D7BA95BE81C411619B0_H
#define UIVIEWSTARTBEHAVIOR_T1C1D97FA8BECE7BDEAC82D7BA95BE81C411619B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIViewStartBehavior
struct  UIViewStartBehavior_t1C1D97FA8BECE7BDEAC82D7BA95BE81C411619B0 
{
public:
	// System.Int32 Doozy.Engine.UI.UIViewStartBehavior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIViewStartBehavior_t1C1D97FA8BECE7BDEAC82D7BA95BE81C411619B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWSTARTBEHAVIOR_T1C1D97FA8BECE7BDEAC82D7BA95BE81C411619B0_H
#ifndef VISIBILITYSTATE_T57C44223D66727F9AEBFE2E0EDDD2895E58C296D_H
#define VISIBILITYSTATE_T57C44223D66727F9AEBFE2E0EDDD2895E58C296D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.VisibilityState
struct  VisibilityState_t57C44223D66727F9AEBFE2E0EDDD2895E58C296D 
{
public:
	// System.Int32 Doozy.Engine.UI.VisibilityState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VisibilityState_t57C44223D66727F9AEBFE2E0EDDD2895E58C296D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITYSTATE_T57C44223D66727F9AEBFE2E0EDDD2895E58C296D_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef PARTICLESYSTEMSTOPBEHAVIOR_T391374C3CA738F223224E1F294A11CD06D3E1EF0_H
#define PARTICLESYSTEMSTOPBEHAVIOR_T391374C3CA738F223224E1F294A11CD06D3E1EF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemStopBehavior
struct  ParticleSystemStopBehavior_t391374C3CA738F223224E1F294A11CD06D3E1EF0 
{
public:
	// System.Int32 UnityEngine.ParticleSystemStopBehavior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemStopBehavior_t391374C3CA738F223224E1F294A11CD06D3E1EF0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMSTOPBEHAVIOR_T391374C3CA738F223224E1F294A11CD06D3E1EF0_H
#ifndef LOADSCENEMODE_T75F0B96794398942671B8315D2A9AC25C40A22D5_H
#define LOADSCENEMODE_T75F0B96794398942671B8315D2A9AC25C40A22D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t75F0B96794398942671B8315D2A9AC25C40A22D5 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadSceneMode_t75F0B96794398942671B8315D2A9AC25C40A22D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEMODE_T75F0B96794398942671B8315D2A9AC25C40A22D5_H
#ifndef TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#define TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_tD902305F0B673116C42548A58E8BEED50177A33D 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_tD902305F0B673116C42548A58E8BEED50177A33D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#ifndef TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#define TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifndef SIMULATEDTOUCH_T16FAD0A464067569D42CDB2B121305F3D6D23BD0_H
#define SIMULATEDTOUCH_T16FAD0A464067569D42CDB2B121305F3D6D23BD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.SimulatedTouch
struct  SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0  : public RuntimeObject
{
public:
	// System.Boolean Doozy.Engine.Touchy.SimulatedTouch::<WasModified>k__BackingField
	bool ___U3CWasModifiedU3Ek__BackingField_0;
	// System.Object Doozy.Engine.Touchy.SimulatedTouch::m_touch
	RuntimeObject * ___m_touch_3;

public:
	inline static int32_t get_offset_of_U3CWasModifiedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0, ___U3CWasModifiedU3Ek__BackingField_0)); }
	inline bool get_U3CWasModifiedU3Ek__BackingField_0() const { return ___U3CWasModifiedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CWasModifiedU3Ek__BackingField_0() { return &___U3CWasModifiedU3Ek__BackingField_0; }
	inline void set_U3CWasModifiedU3Ek__BackingField_0(bool value)
	{
		___U3CWasModifiedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_touch_3() { return static_cast<int32_t>(offsetof(SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0, ___m_touch_3)); }
	inline RuntimeObject * get_m_touch_3() const { return ___m_touch_3; }
	inline RuntimeObject ** get_address_of_m_touch_3() { return &___m_touch_3; }
	inline void set_m_touch_3(RuntimeObject * value)
	{
		___m_touch_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_touch_3), value);
	}
};

struct SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.FieldInfo> Doozy.Engine.Touchy.SimulatedTouch::Fields
	Dictionary_2_tDB555DD6652A3DB55616E5F6EE5FB13C4113B901 * ___Fields_2;

public:
	inline static int32_t get_offset_of_Fields_2() { return static_cast<int32_t>(offsetof(SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0_StaticFields, ___Fields_2)); }
	inline Dictionary_2_tDB555DD6652A3DB55616E5F6EE5FB13C4113B901 * get_Fields_2() const { return ___Fields_2; }
	inline Dictionary_2_tDB555DD6652A3DB55616E5F6EE5FB13C4113B901 ** get_address_of_Fields_2() { return &___Fields_2; }
	inline void set_Fields_2(Dictionary_2_tDB555DD6652A3DB55616E5F6EE5FB13C4113B901 * value)
	{
		___Fields_2 = value;
		Il2CppCodeGenWriteBarrier((&___Fields_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMULATEDTOUCH_T16FAD0A464067569D42CDB2B121305F3D6D23BD0_H
#ifndef FADE_TC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85_H
#define FADE_TC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.Fade
struct  Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.Animation.AnimationType Doozy.Engine.UI.Animation.Fade::AnimationType
	int32_t ___AnimationType_0;
	// System.Boolean Doozy.Engine.UI.Animation.Fade::Enabled
	bool ___Enabled_1;
	// System.Single Doozy.Engine.UI.Animation.Fade::From
	float ___From_2;
	// System.Single Doozy.Engine.UI.Animation.Fade::To
	float ___To_3;
	// System.Single Doozy.Engine.UI.Animation.Fade::By
	float ___By_4;
	// System.Boolean Doozy.Engine.UI.Animation.Fade::UseCustomFromAndTo
	bool ___UseCustomFromAndTo_5;
	// System.Int32 Doozy.Engine.UI.Animation.Fade::NumberOfLoops
	int32_t ___NumberOfLoops_6;
	// DG.Tweening.LoopType Doozy.Engine.UI.Animation.Fade::LoopType
	int32_t ___LoopType_7;
	// Doozy.Engine.UI.Animation.EaseType Doozy.Engine.UI.Animation.Fade::EaseType
	int32_t ___EaseType_8;
	// DG.Tweening.Ease Doozy.Engine.UI.Animation.Fade::Ease
	int32_t ___Ease_9;
	// UnityEngine.AnimationCurve Doozy.Engine.UI.Animation.Fade::AnimationCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___AnimationCurve_10;
	// System.Single Doozy.Engine.UI.Animation.Fade::StartDelay
	float ___StartDelay_11;
	// System.Single Doozy.Engine.UI.Animation.Fade::Duration
	float ___Duration_12;

public:
	inline static int32_t get_offset_of_AnimationType_0() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___AnimationType_0)); }
	inline int32_t get_AnimationType_0() const { return ___AnimationType_0; }
	inline int32_t* get_address_of_AnimationType_0() { return &___AnimationType_0; }
	inline void set_AnimationType_0(int32_t value)
	{
		___AnimationType_0 = value;
	}

	inline static int32_t get_offset_of_Enabled_1() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___Enabled_1)); }
	inline bool get_Enabled_1() const { return ___Enabled_1; }
	inline bool* get_address_of_Enabled_1() { return &___Enabled_1; }
	inline void set_Enabled_1(bool value)
	{
		___Enabled_1 = value;
	}

	inline static int32_t get_offset_of_From_2() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___From_2)); }
	inline float get_From_2() const { return ___From_2; }
	inline float* get_address_of_From_2() { return &___From_2; }
	inline void set_From_2(float value)
	{
		___From_2 = value;
	}

	inline static int32_t get_offset_of_To_3() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___To_3)); }
	inline float get_To_3() const { return ___To_3; }
	inline float* get_address_of_To_3() { return &___To_3; }
	inline void set_To_3(float value)
	{
		___To_3 = value;
	}

	inline static int32_t get_offset_of_By_4() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___By_4)); }
	inline float get_By_4() const { return ___By_4; }
	inline float* get_address_of_By_4() { return &___By_4; }
	inline void set_By_4(float value)
	{
		___By_4 = value;
	}

	inline static int32_t get_offset_of_UseCustomFromAndTo_5() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___UseCustomFromAndTo_5)); }
	inline bool get_UseCustomFromAndTo_5() const { return ___UseCustomFromAndTo_5; }
	inline bool* get_address_of_UseCustomFromAndTo_5() { return &___UseCustomFromAndTo_5; }
	inline void set_UseCustomFromAndTo_5(bool value)
	{
		___UseCustomFromAndTo_5 = value;
	}

	inline static int32_t get_offset_of_NumberOfLoops_6() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___NumberOfLoops_6)); }
	inline int32_t get_NumberOfLoops_6() const { return ___NumberOfLoops_6; }
	inline int32_t* get_address_of_NumberOfLoops_6() { return &___NumberOfLoops_6; }
	inline void set_NumberOfLoops_6(int32_t value)
	{
		___NumberOfLoops_6 = value;
	}

	inline static int32_t get_offset_of_LoopType_7() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___LoopType_7)); }
	inline int32_t get_LoopType_7() const { return ___LoopType_7; }
	inline int32_t* get_address_of_LoopType_7() { return &___LoopType_7; }
	inline void set_LoopType_7(int32_t value)
	{
		___LoopType_7 = value;
	}

	inline static int32_t get_offset_of_EaseType_8() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___EaseType_8)); }
	inline int32_t get_EaseType_8() const { return ___EaseType_8; }
	inline int32_t* get_address_of_EaseType_8() { return &___EaseType_8; }
	inline void set_EaseType_8(int32_t value)
	{
		___EaseType_8 = value;
	}

	inline static int32_t get_offset_of_Ease_9() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___Ease_9)); }
	inline int32_t get_Ease_9() const { return ___Ease_9; }
	inline int32_t* get_address_of_Ease_9() { return &___Ease_9; }
	inline void set_Ease_9(int32_t value)
	{
		___Ease_9 = value;
	}

	inline static int32_t get_offset_of_AnimationCurve_10() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___AnimationCurve_10)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_AnimationCurve_10() const { return ___AnimationCurve_10; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_AnimationCurve_10() { return &___AnimationCurve_10; }
	inline void set_AnimationCurve_10(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___AnimationCurve_10 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationCurve_10), value);
	}

	inline static int32_t get_offset_of_StartDelay_11() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___StartDelay_11)); }
	inline float get_StartDelay_11() const { return ___StartDelay_11; }
	inline float* get_address_of_StartDelay_11() { return &___StartDelay_11; }
	inline void set_StartDelay_11(float value)
	{
		___StartDelay_11 = value;
	}

	inline static int32_t get_offset_of_Duration_12() { return static_cast<int32_t>(offsetof(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85, ___Duration_12)); }
	inline float get_Duration_12() const { return ___Duration_12; }
	inline float* get_address_of_Duration_12() { return &___Duration_12; }
	inline void set_Duration_12(float value)
	{
		___Duration_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADE_TC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85_H
#ifndef MOVE_TBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0_H
#define MOVE_TBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.Move
struct  Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.Animation.AnimationType Doozy.Engine.UI.Animation.Move::AnimationType
	int32_t ___AnimationType_0;
	// System.Boolean Doozy.Engine.UI.Animation.Move::Enabled
	bool ___Enabled_1;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.Move::From
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___From_2;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.Move::To
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___To_3;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.Move::By
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___By_4;
	// System.Boolean Doozy.Engine.UI.Animation.Move::UseCustomFromAndTo
	bool ___UseCustomFromAndTo_5;
	// System.Int32 Doozy.Engine.UI.Animation.Move::Vibrato
	int32_t ___Vibrato_6;
	// System.Single Doozy.Engine.UI.Animation.Move::Elasticity
	float ___Elasticity_7;
	// System.Int32 Doozy.Engine.UI.Animation.Move::NumberOfLoops
	int32_t ___NumberOfLoops_8;
	// DG.Tweening.LoopType Doozy.Engine.UI.Animation.Move::LoopType
	int32_t ___LoopType_9;
	// Doozy.Engine.UI.Animation.Direction Doozy.Engine.UI.Animation.Move::Direction
	int32_t ___Direction_10;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.Move::CustomPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___CustomPosition_11;
	// Doozy.Engine.UI.Animation.EaseType Doozy.Engine.UI.Animation.Move::EaseType
	int32_t ___EaseType_12;
	// DG.Tweening.Ease Doozy.Engine.UI.Animation.Move::Ease
	int32_t ___Ease_13;
	// UnityEngine.AnimationCurve Doozy.Engine.UI.Animation.Move::AnimationCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___AnimationCurve_14;
	// System.Single Doozy.Engine.UI.Animation.Move::StartDelay
	float ___StartDelay_15;
	// System.Single Doozy.Engine.UI.Animation.Move::Duration
	float ___Duration_16;

public:
	inline static int32_t get_offset_of_AnimationType_0() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___AnimationType_0)); }
	inline int32_t get_AnimationType_0() const { return ___AnimationType_0; }
	inline int32_t* get_address_of_AnimationType_0() { return &___AnimationType_0; }
	inline void set_AnimationType_0(int32_t value)
	{
		___AnimationType_0 = value;
	}

	inline static int32_t get_offset_of_Enabled_1() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___Enabled_1)); }
	inline bool get_Enabled_1() const { return ___Enabled_1; }
	inline bool* get_address_of_Enabled_1() { return &___Enabled_1; }
	inline void set_Enabled_1(bool value)
	{
		___Enabled_1 = value;
	}

	inline static int32_t get_offset_of_From_2() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___From_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_From_2() const { return ___From_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_From_2() { return &___From_2; }
	inline void set_From_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___From_2 = value;
	}

	inline static int32_t get_offset_of_To_3() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___To_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_To_3() const { return ___To_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_To_3() { return &___To_3; }
	inline void set_To_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___To_3 = value;
	}

	inline static int32_t get_offset_of_By_4() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___By_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_By_4() const { return ___By_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_By_4() { return &___By_4; }
	inline void set_By_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___By_4 = value;
	}

	inline static int32_t get_offset_of_UseCustomFromAndTo_5() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___UseCustomFromAndTo_5)); }
	inline bool get_UseCustomFromAndTo_5() const { return ___UseCustomFromAndTo_5; }
	inline bool* get_address_of_UseCustomFromAndTo_5() { return &___UseCustomFromAndTo_5; }
	inline void set_UseCustomFromAndTo_5(bool value)
	{
		___UseCustomFromAndTo_5 = value;
	}

	inline static int32_t get_offset_of_Vibrato_6() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___Vibrato_6)); }
	inline int32_t get_Vibrato_6() const { return ___Vibrato_6; }
	inline int32_t* get_address_of_Vibrato_6() { return &___Vibrato_6; }
	inline void set_Vibrato_6(int32_t value)
	{
		___Vibrato_6 = value;
	}

	inline static int32_t get_offset_of_Elasticity_7() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___Elasticity_7)); }
	inline float get_Elasticity_7() const { return ___Elasticity_7; }
	inline float* get_address_of_Elasticity_7() { return &___Elasticity_7; }
	inline void set_Elasticity_7(float value)
	{
		___Elasticity_7 = value;
	}

	inline static int32_t get_offset_of_NumberOfLoops_8() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___NumberOfLoops_8)); }
	inline int32_t get_NumberOfLoops_8() const { return ___NumberOfLoops_8; }
	inline int32_t* get_address_of_NumberOfLoops_8() { return &___NumberOfLoops_8; }
	inline void set_NumberOfLoops_8(int32_t value)
	{
		___NumberOfLoops_8 = value;
	}

	inline static int32_t get_offset_of_LoopType_9() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___LoopType_9)); }
	inline int32_t get_LoopType_9() const { return ___LoopType_9; }
	inline int32_t* get_address_of_LoopType_9() { return &___LoopType_9; }
	inline void set_LoopType_9(int32_t value)
	{
		___LoopType_9 = value;
	}

	inline static int32_t get_offset_of_Direction_10() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___Direction_10)); }
	inline int32_t get_Direction_10() const { return ___Direction_10; }
	inline int32_t* get_address_of_Direction_10() { return &___Direction_10; }
	inline void set_Direction_10(int32_t value)
	{
		___Direction_10 = value;
	}

	inline static int32_t get_offset_of_CustomPosition_11() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___CustomPosition_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_CustomPosition_11() const { return ___CustomPosition_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_CustomPosition_11() { return &___CustomPosition_11; }
	inline void set_CustomPosition_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___CustomPosition_11 = value;
	}

	inline static int32_t get_offset_of_EaseType_12() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___EaseType_12)); }
	inline int32_t get_EaseType_12() const { return ___EaseType_12; }
	inline int32_t* get_address_of_EaseType_12() { return &___EaseType_12; }
	inline void set_EaseType_12(int32_t value)
	{
		___EaseType_12 = value;
	}

	inline static int32_t get_offset_of_Ease_13() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___Ease_13)); }
	inline int32_t get_Ease_13() const { return ___Ease_13; }
	inline int32_t* get_address_of_Ease_13() { return &___Ease_13; }
	inline void set_Ease_13(int32_t value)
	{
		___Ease_13 = value;
	}

	inline static int32_t get_offset_of_AnimationCurve_14() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___AnimationCurve_14)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_AnimationCurve_14() const { return ___AnimationCurve_14; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_AnimationCurve_14() { return &___AnimationCurve_14; }
	inline void set_AnimationCurve_14(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___AnimationCurve_14 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationCurve_14), value);
	}

	inline static int32_t get_offset_of_StartDelay_15() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___StartDelay_15)); }
	inline float get_StartDelay_15() const { return ___StartDelay_15; }
	inline float* get_address_of_StartDelay_15() { return &___StartDelay_15; }
	inline void set_StartDelay_15(float value)
	{
		___StartDelay_15 = value;
	}

	inline static int32_t get_offset_of_Duration_16() { return static_cast<int32_t>(offsetof(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0, ___Duration_16)); }
	inline float get_Duration_16() const { return ___Duration_16; }
	inline float* get_address_of_Duration_16() { return &___Duration_16; }
	inline void set_Duration_16(float value)
	{
		___Duration_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVE_TBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0_H
#ifndef ROTATE_T6F8FF960182701FD1D2F58856066262753CF688A_H
#define ROTATE_T6F8FF960182701FD1D2F58856066262753CF688A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.Rotate
struct  Rotate_t6F8FF960182701FD1D2F58856066262753CF688A  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.Animation.AnimationType Doozy.Engine.UI.Animation.Rotate::AnimationType
	int32_t ___AnimationType_0;
	// System.Boolean Doozy.Engine.UI.Animation.Rotate::Enabled
	bool ___Enabled_1;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.Rotate::From
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___From_2;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.Rotate::To
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___To_3;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.Rotate::By
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___By_4;
	// System.Boolean Doozy.Engine.UI.Animation.Rotate::UseCustomFromAndTo
	bool ___UseCustomFromAndTo_5;
	// System.Int32 Doozy.Engine.UI.Animation.Rotate::Vibrato
	int32_t ___Vibrato_6;
	// System.Single Doozy.Engine.UI.Animation.Rotate::Elasticity
	float ___Elasticity_7;
	// System.Int32 Doozy.Engine.UI.Animation.Rotate::NumberOfLoops
	int32_t ___NumberOfLoops_8;
	// DG.Tweening.LoopType Doozy.Engine.UI.Animation.Rotate::LoopType
	int32_t ___LoopType_9;
	// DG.Tweening.RotateMode Doozy.Engine.UI.Animation.Rotate::RotateMode
	int32_t ___RotateMode_10;
	// Doozy.Engine.UI.Animation.EaseType Doozy.Engine.UI.Animation.Rotate::EaseType
	int32_t ___EaseType_11;
	// DG.Tweening.Ease Doozy.Engine.UI.Animation.Rotate::Ease
	int32_t ___Ease_12;
	// UnityEngine.AnimationCurve Doozy.Engine.UI.Animation.Rotate::AnimationCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___AnimationCurve_13;
	// System.Single Doozy.Engine.UI.Animation.Rotate::StartDelay
	float ___StartDelay_14;
	// System.Single Doozy.Engine.UI.Animation.Rotate::Duration
	float ___Duration_15;

public:
	inline static int32_t get_offset_of_AnimationType_0() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___AnimationType_0)); }
	inline int32_t get_AnimationType_0() const { return ___AnimationType_0; }
	inline int32_t* get_address_of_AnimationType_0() { return &___AnimationType_0; }
	inline void set_AnimationType_0(int32_t value)
	{
		___AnimationType_0 = value;
	}

	inline static int32_t get_offset_of_Enabled_1() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___Enabled_1)); }
	inline bool get_Enabled_1() const { return ___Enabled_1; }
	inline bool* get_address_of_Enabled_1() { return &___Enabled_1; }
	inline void set_Enabled_1(bool value)
	{
		___Enabled_1 = value;
	}

	inline static int32_t get_offset_of_From_2() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___From_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_From_2() const { return ___From_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_From_2() { return &___From_2; }
	inline void set_From_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___From_2 = value;
	}

	inline static int32_t get_offset_of_To_3() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___To_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_To_3() const { return ___To_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_To_3() { return &___To_3; }
	inline void set_To_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___To_3 = value;
	}

	inline static int32_t get_offset_of_By_4() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___By_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_By_4() const { return ___By_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_By_4() { return &___By_4; }
	inline void set_By_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___By_4 = value;
	}

	inline static int32_t get_offset_of_UseCustomFromAndTo_5() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___UseCustomFromAndTo_5)); }
	inline bool get_UseCustomFromAndTo_5() const { return ___UseCustomFromAndTo_5; }
	inline bool* get_address_of_UseCustomFromAndTo_5() { return &___UseCustomFromAndTo_5; }
	inline void set_UseCustomFromAndTo_5(bool value)
	{
		___UseCustomFromAndTo_5 = value;
	}

	inline static int32_t get_offset_of_Vibrato_6() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___Vibrato_6)); }
	inline int32_t get_Vibrato_6() const { return ___Vibrato_6; }
	inline int32_t* get_address_of_Vibrato_6() { return &___Vibrato_6; }
	inline void set_Vibrato_6(int32_t value)
	{
		___Vibrato_6 = value;
	}

	inline static int32_t get_offset_of_Elasticity_7() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___Elasticity_7)); }
	inline float get_Elasticity_7() const { return ___Elasticity_7; }
	inline float* get_address_of_Elasticity_7() { return &___Elasticity_7; }
	inline void set_Elasticity_7(float value)
	{
		___Elasticity_7 = value;
	}

	inline static int32_t get_offset_of_NumberOfLoops_8() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___NumberOfLoops_8)); }
	inline int32_t get_NumberOfLoops_8() const { return ___NumberOfLoops_8; }
	inline int32_t* get_address_of_NumberOfLoops_8() { return &___NumberOfLoops_8; }
	inline void set_NumberOfLoops_8(int32_t value)
	{
		___NumberOfLoops_8 = value;
	}

	inline static int32_t get_offset_of_LoopType_9() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___LoopType_9)); }
	inline int32_t get_LoopType_9() const { return ___LoopType_9; }
	inline int32_t* get_address_of_LoopType_9() { return &___LoopType_9; }
	inline void set_LoopType_9(int32_t value)
	{
		___LoopType_9 = value;
	}

	inline static int32_t get_offset_of_RotateMode_10() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___RotateMode_10)); }
	inline int32_t get_RotateMode_10() const { return ___RotateMode_10; }
	inline int32_t* get_address_of_RotateMode_10() { return &___RotateMode_10; }
	inline void set_RotateMode_10(int32_t value)
	{
		___RotateMode_10 = value;
	}

	inline static int32_t get_offset_of_EaseType_11() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___EaseType_11)); }
	inline int32_t get_EaseType_11() const { return ___EaseType_11; }
	inline int32_t* get_address_of_EaseType_11() { return &___EaseType_11; }
	inline void set_EaseType_11(int32_t value)
	{
		___EaseType_11 = value;
	}

	inline static int32_t get_offset_of_Ease_12() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___Ease_12)); }
	inline int32_t get_Ease_12() const { return ___Ease_12; }
	inline int32_t* get_address_of_Ease_12() { return &___Ease_12; }
	inline void set_Ease_12(int32_t value)
	{
		___Ease_12 = value;
	}

	inline static int32_t get_offset_of_AnimationCurve_13() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___AnimationCurve_13)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_AnimationCurve_13() const { return ___AnimationCurve_13; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_AnimationCurve_13() { return &___AnimationCurve_13; }
	inline void set_AnimationCurve_13(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___AnimationCurve_13 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationCurve_13), value);
	}

	inline static int32_t get_offset_of_StartDelay_14() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___StartDelay_14)); }
	inline float get_StartDelay_14() const { return ___StartDelay_14; }
	inline float* get_address_of_StartDelay_14() { return &___StartDelay_14; }
	inline void set_StartDelay_14(float value)
	{
		___StartDelay_14 = value;
	}

	inline static int32_t get_offset_of_Duration_15() { return static_cast<int32_t>(offsetof(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A, ___Duration_15)); }
	inline float get_Duration_15() const { return ___Duration_15; }
	inline float* get_address_of_Duration_15() { return &___Duration_15; }
	inline void set_Duration_15(float value)
	{
		___Duration_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATE_T6F8FF960182701FD1D2F58856066262753CF688A_H
#ifndef SCALE_T697FC642414D4F91B28906F5A44839C0697EE9AD_H
#define SCALE_T697FC642414D4F91B28906F5A44839C0697EE9AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.Scale
struct  Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.Animation.AnimationType Doozy.Engine.UI.Animation.Scale::AnimationType
	int32_t ___AnimationType_0;
	// System.Boolean Doozy.Engine.UI.Animation.Scale::Enabled
	bool ___Enabled_1;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.Scale::From
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___From_2;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.Scale::To
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___To_3;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.Scale::By
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___By_4;
	// System.Boolean Doozy.Engine.UI.Animation.Scale::UseCustomFromAndTo
	bool ___UseCustomFromAndTo_5;
	// System.Int32 Doozy.Engine.UI.Animation.Scale::Vibrato
	int32_t ___Vibrato_6;
	// System.Single Doozy.Engine.UI.Animation.Scale::Elasticity
	float ___Elasticity_7;
	// System.Int32 Doozy.Engine.UI.Animation.Scale::NumberOfLoops
	int32_t ___NumberOfLoops_8;
	// DG.Tweening.LoopType Doozy.Engine.UI.Animation.Scale::LoopType
	int32_t ___LoopType_9;
	// Doozy.Engine.UI.Animation.EaseType Doozy.Engine.UI.Animation.Scale::EaseType
	int32_t ___EaseType_10;
	// DG.Tweening.Ease Doozy.Engine.UI.Animation.Scale::Ease
	int32_t ___Ease_11;
	// UnityEngine.AnimationCurve Doozy.Engine.UI.Animation.Scale::AnimationCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___AnimationCurve_12;
	// System.Single Doozy.Engine.UI.Animation.Scale::StartDelay
	float ___StartDelay_13;
	// System.Single Doozy.Engine.UI.Animation.Scale::Duration
	float ___Duration_14;

public:
	inline static int32_t get_offset_of_AnimationType_0() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___AnimationType_0)); }
	inline int32_t get_AnimationType_0() const { return ___AnimationType_0; }
	inline int32_t* get_address_of_AnimationType_0() { return &___AnimationType_0; }
	inline void set_AnimationType_0(int32_t value)
	{
		___AnimationType_0 = value;
	}

	inline static int32_t get_offset_of_Enabled_1() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___Enabled_1)); }
	inline bool get_Enabled_1() const { return ___Enabled_1; }
	inline bool* get_address_of_Enabled_1() { return &___Enabled_1; }
	inline void set_Enabled_1(bool value)
	{
		___Enabled_1 = value;
	}

	inline static int32_t get_offset_of_From_2() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___From_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_From_2() const { return ___From_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_From_2() { return &___From_2; }
	inline void set_From_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___From_2 = value;
	}

	inline static int32_t get_offset_of_To_3() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___To_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_To_3() const { return ___To_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_To_3() { return &___To_3; }
	inline void set_To_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___To_3 = value;
	}

	inline static int32_t get_offset_of_By_4() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___By_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_By_4() const { return ___By_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_By_4() { return &___By_4; }
	inline void set_By_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___By_4 = value;
	}

	inline static int32_t get_offset_of_UseCustomFromAndTo_5() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___UseCustomFromAndTo_5)); }
	inline bool get_UseCustomFromAndTo_5() const { return ___UseCustomFromAndTo_5; }
	inline bool* get_address_of_UseCustomFromAndTo_5() { return &___UseCustomFromAndTo_5; }
	inline void set_UseCustomFromAndTo_5(bool value)
	{
		___UseCustomFromAndTo_5 = value;
	}

	inline static int32_t get_offset_of_Vibrato_6() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___Vibrato_6)); }
	inline int32_t get_Vibrato_6() const { return ___Vibrato_6; }
	inline int32_t* get_address_of_Vibrato_6() { return &___Vibrato_6; }
	inline void set_Vibrato_6(int32_t value)
	{
		___Vibrato_6 = value;
	}

	inline static int32_t get_offset_of_Elasticity_7() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___Elasticity_7)); }
	inline float get_Elasticity_7() const { return ___Elasticity_7; }
	inline float* get_address_of_Elasticity_7() { return &___Elasticity_7; }
	inline void set_Elasticity_7(float value)
	{
		___Elasticity_7 = value;
	}

	inline static int32_t get_offset_of_NumberOfLoops_8() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___NumberOfLoops_8)); }
	inline int32_t get_NumberOfLoops_8() const { return ___NumberOfLoops_8; }
	inline int32_t* get_address_of_NumberOfLoops_8() { return &___NumberOfLoops_8; }
	inline void set_NumberOfLoops_8(int32_t value)
	{
		___NumberOfLoops_8 = value;
	}

	inline static int32_t get_offset_of_LoopType_9() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___LoopType_9)); }
	inline int32_t get_LoopType_9() const { return ___LoopType_9; }
	inline int32_t* get_address_of_LoopType_9() { return &___LoopType_9; }
	inline void set_LoopType_9(int32_t value)
	{
		___LoopType_9 = value;
	}

	inline static int32_t get_offset_of_EaseType_10() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___EaseType_10)); }
	inline int32_t get_EaseType_10() const { return ___EaseType_10; }
	inline int32_t* get_address_of_EaseType_10() { return &___EaseType_10; }
	inline void set_EaseType_10(int32_t value)
	{
		___EaseType_10 = value;
	}

	inline static int32_t get_offset_of_Ease_11() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___Ease_11)); }
	inline int32_t get_Ease_11() const { return ___Ease_11; }
	inline int32_t* get_address_of_Ease_11() { return &___Ease_11; }
	inline void set_Ease_11(int32_t value)
	{
		___Ease_11 = value;
	}

	inline static int32_t get_offset_of_AnimationCurve_12() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___AnimationCurve_12)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_AnimationCurve_12() const { return ___AnimationCurve_12; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_AnimationCurve_12() { return &___AnimationCurve_12; }
	inline void set_AnimationCurve_12(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___AnimationCurve_12 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationCurve_12), value);
	}

	inline static int32_t get_offset_of_StartDelay_13() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___StartDelay_13)); }
	inline float get_StartDelay_13() const { return ___StartDelay_13; }
	inline float* get_address_of_StartDelay_13() { return &___StartDelay_13; }
	inline void set_StartDelay_13(float value)
	{
		___StartDelay_13 = value;
	}

	inline static int32_t get_offset_of_Duration_14() { return static_cast<int32_t>(offsetof(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD, ___Duration_14)); }
	inline float get_Duration_14() const { return ___Duration_14; }
	inline float* get_address_of_Duration_14() { return &___Duration_14; }
	inline void set_Duration_14(float value)
	{
		___Duration_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALE_T697FC642414D4F91B28906F5A44839C0697EE9AD_H
#ifndef UIANIMATION_T5594956F11A5F167AC1C8C083516B3BE07CF374C_H
#define UIANIMATION_T5594956F11A5F167AC1C8C083516B3BE07CF374C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimation
struct  UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.Animation.AnimationType Doozy.Engine.UI.Animation.UIAnimation::AnimationType
	int32_t ___AnimationType_0;
	// Doozy.Engine.UI.Animation.Move Doozy.Engine.UI.Animation.UIAnimation::Move
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0 * ___Move_1;
	// Doozy.Engine.UI.Animation.Rotate Doozy.Engine.UI.Animation.UIAnimation::Rotate
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A * ___Rotate_2;
	// Doozy.Engine.UI.Animation.Scale Doozy.Engine.UI.Animation.UIAnimation::Scale
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD * ___Scale_3;
	// Doozy.Engine.UI.Animation.Fade Doozy.Engine.UI.Animation.UIAnimation::Fade
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85 * ___Fade_4;

public:
	inline static int32_t get_offset_of_AnimationType_0() { return static_cast<int32_t>(offsetof(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C, ___AnimationType_0)); }
	inline int32_t get_AnimationType_0() const { return ___AnimationType_0; }
	inline int32_t* get_address_of_AnimationType_0() { return &___AnimationType_0; }
	inline void set_AnimationType_0(int32_t value)
	{
		___AnimationType_0 = value;
	}

	inline static int32_t get_offset_of_Move_1() { return static_cast<int32_t>(offsetof(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C, ___Move_1)); }
	inline Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0 * get_Move_1() const { return ___Move_1; }
	inline Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0 ** get_address_of_Move_1() { return &___Move_1; }
	inline void set_Move_1(Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0 * value)
	{
		___Move_1 = value;
		Il2CppCodeGenWriteBarrier((&___Move_1), value);
	}

	inline static int32_t get_offset_of_Rotate_2() { return static_cast<int32_t>(offsetof(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C, ___Rotate_2)); }
	inline Rotate_t6F8FF960182701FD1D2F58856066262753CF688A * get_Rotate_2() const { return ___Rotate_2; }
	inline Rotate_t6F8FF960182701FD1D2F58856066262753CF688A ** get_address_of_Rotate_2() { return &___Rotate_2; }
	inline void set_Rotate_2(Rotate_t6F8FF960182701FD1D2F58856066262753CF688A * value)
	{
		___Rotate_2 = value;
		Il2CppCodeGenWriteBarrier((&___Rotate_2), value);
	}

	inline static int32_t get_offset_of_Scale_3() { return static_cast<int32_t>(offsetof(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C, ___Scale_3)); }
	inline Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD * get_Scale_3() const { return ___Scale_3; }
	inline Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD ** get_address_of_Scale_3() { return &___Scale_3; }
	inline void set_Scale_3(Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD * value)
	{
		___Scale_3 = value;
		Il2CppCodeGenWriteBarrier((&___Scale_3), value);
	}

	inline static int32_t get_offset_of_Fade_4() { return static_cast<int32_t>(offsetof(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C, ___Fade_4)); }
	inline Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85 * get_Fade_4() const { return ___Fade_4; }
	inline Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85 ** get_address_of_Fade_4() { return &___Fade_4; }
	inline void set_Fade_4(Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85 * value)
	{
		___Fade_4 = value;
		Il2CppCodeGenWriteBarrier((&___Fade_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIANIMATION_T5594956F11A5F167AC1C8C083516B3BE07CF374C_H
#ifndef UIANIMATIONSDATABASE_T68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E_H
#define UIANIMATIONSDATABASE_T68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimationsDatabase
struct  UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Doozy.Engine.UI.Animation.UIAnimationsDatabase::DatabaseNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___DatabaseNames_0;
	// Doozy.Engine.UI.Animation.AnimationType Doozy.Engine.UI.Animation.UIAnimationsDatabase::DatabaseType
	int32_t ___DatabaseType_1;
	// System.Collections.Generic.List`1<Doozy.Engine.UI.Animation.UIAnimationDatabase> Doozy.Engine.UI.Animation.UIAnimationsDatabase::Databases
	List_1_t41FB19A7A05774A4182316CF9D1128B7B8ABB916 * ___Databases_2;

public:
	inline static int32_t get_offset_of_DatabaseNames_0() { return static_cast<int32_t>(offsetof(UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E, ___DatabaseNames_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_DatabaseNames_0() const { return ___DatabaseNames_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_DatabaseNames_0() { return &___DatabaseNames_0; }
	inline void set_DatabaseNames_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___DatabaseNames_0 = value;
		Il2CppCodeGenWriteBarrier((&___DatabaseNames_0), value);
	}

	inline static int32_t get_offset_of_DatabaseType_1() { return static_cast<int32_t>(offsetof(UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E, ___DatabaseType_1)); }
	inline int32_t get_DatabaseType_1() const { return ___DatabaseType_1; }
	inline int32_t* get_address_of_DatabaseType_1() { return &___DatabaseType_1; }
	inline void set_DatabaseType_1(int32_t value)
	{
		___DatabaseType_1 = value;
	}

	inline static int32_t get_offset_of_Databases_2() { return static_cast<int32_t>(offsetof(UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E, ___Databases_2)); }
	inline List_1_t41FB19A7A05774A4182316CF9D1128B7B8ABB916 * get_Databases_2() const { return ___Databases_2; }
	inline List_1_t41FB19A7A05774A4182316CF9D1128B7B8ABB916 ** get_address_of_Databases_2() { return &___Databases_2; }
	inline void set_Databases_2(List_1_t41FB19A7A05774A4182316CF9D1128B7B8ABB916 * value)
	{
		___Databases_2 = value;
		Il2CppCodeGenWriteBarrier((&___Databases_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIANIMATIONSDATABASE_T68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E_H
#ifndef UIANIMATOR_TAEE3CDC93D85154346DB11446DE396889AFB4777_H
#define UIANIMATOR_TAEE3CDC93D85154346DB11446DE396889AFB4777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimator
struct  UIAnimator_tAEE3CDC93D85154346DB11446DE396889AFB4777  : public RuntimeObject
{
public:

public:
};

struct UIAnimator_tAEE3CDC93D85154346DB11446DE396889AFB4777_StaticFields
{
public:
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.UIAnimator::DEFAULT_START_POSITION
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___DEFAULT_START_POSITION_0;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.UIAnimator::DEFAULT_START_ROTATION
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___DEFAULT_START_ROTATION_1;
	// UnityEngine.Vector3 Doozy.Engine.UI.Animation.UIAnimator::DEFAULT_START_SCALE
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___DEFAULT_START_SCALE_2;

public:
	inline static int32_t get_offset_of_DEFAULT_START_POSITION_0() { return static_cast<int32_t>(offsetof(UIAnimator_tAEE3CDC93D85154346DB11446DE396889AFB4777_StaticFields, ___DEFAULT_START_POSITION_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_DEFAULT_START_POSITION_0() const { return ___DEFAULT_START_POSITION_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_DEFAULT_START_POSITION_0() { return &___DEFAULT_START_POSITION_0; }
	inline void set_DEFAULT_START_POSITION_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___DEFAULT_START_POSITION_0 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_START_ROTATION_1() { return static_cast<int32_t>(offsetof(UIAnimator_tAEE3CDC93D85154346DB11446DE396889AFB4777_StaticFields, ___DEFAULT_START_ROTATION_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_DEFAULT_START_ROTATION_1() const { return ___DEFAULT_START_ROTATION_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_DEFAULT_START_ROTATION_1() { return &___DEFAULT_START_ROTATION_1; }
	inline void set_DEFAULT_START_ROTATION_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___DEFAULT_START_ROTATION_1 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_START_SCALE_2() { return static_cast<int32_t>(offsetof(UIAnimator_tAEE3CDC93D85154346DB11446DE396889AFB4777_StaticFields, ___DEFAULT_START_SCALE_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_DEFAULT_START_SCALE_2() const { return ___DEFAULT_START_SCALE_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_DEFAULT_START_SCALE_2() { return &___DEFAULT_START_SCALE_2; }
	inline void set_DEFAULT_START_SCALE_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___DEFAULT_START_SCALE_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIANIMATOR_TAEE3CDC93D85154346DB11446DE396889AFB4777_H
#ifndef UIEFFECT_TBF391E23875775E4FD00E9CFE482A7006B0450BB_H
#define UIEFFECT_TBF391E23875775E4FD00E9CFE482A7006B0450BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.UIEffect
struct  UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.DynamicSorting Doozy.Engine.UI.Base.UIEffect::AutoSort
	int32_t ___AutoSort_6;
	// Doozy.Engine.UI.UIEffectBehavior Doozy.Engine.UI.Base.UIEffect::Behavior
	int32_t ___Behavior_7;
	// System.Int32 Doozy.Engine.UI.Base.UIEffect::CustomSortingOrder
	int32_t ___CustomSortingOrder_8;
	// System.Int32 Doozy.Engine.UI.Base.UIEffect::SortingSteps
	int32_t ___SortingSteps_9;
	// UnityEngine.ParticleSystem Doozy.Engine.UI.Base.UIEffect::ParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___ParticleSystem_10;
	// UnityEngine.ParticleSystemStopBehavior Doozy.Engine.UI.Base.UIEffect::StopBehavior
	int32_t ___StopBehavior_11;
	// System.String Doozy.Engine.UI.Base.UIEffect::CustomSortingLayer
	String_t* ___CustomSortingLayer_12;
	// UnityEngine.Renderer[] Doozy.Engine.UI.Base.UIEffect::m_renderers
	RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* ___m_renderers_13;

public:
	inline static int32_t get_offset_of_AutoSort_6() { return static_cast<int32_t>(offsetof(UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB, ___AutoSort_6)); }
	inline int32_t get_AutoSort_6() const { return ___AutoSort_6; }
	inline int32_t* get_address_of_AutoSort_6() { return &___AutoSort_6; }
	inline void set_AutoSort_6(int32_t value)
	{
		___AutoSort_6 = value;
	}

	inline static int32_t get_offset_of_Behavior_7() { return static_cast<int32_t>(offsetof(UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB, ___Behavior_7)); }
	inline int32_t get_Behavior_7() const { return ___Behavior_7; }
	inline int32_t* get_address_of_Behavior_7() { return &___Behavior_7; }
	inline void set_Behavior_7(int32_t value)
	{
		___Behavior_7 = value;
	}

	inline static int32_t get_offset_of_CustomSortingOrder_8() { return static_cast<int32_t>(offsetof(UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB, ___CustomSortingOrder_8)); }
	inline int32_t get_CustomSortingOrder_8() const { return ___CustomSortingOrder_8; }
	inline int32_t* get_address_of_CustomSortingOrder_8() { return &___CustomSortingOrder_8; }
	inline void set_CustomSortingOrder_8(int32_t value)
	{
		___CustomSortingOrder_8 = value;
	}

	inline static int32_t get_offset_of_SortingSteps_9() { return static_cast<int32_t>(offsetof(UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB, ___SortingSteps_9)); }
	inline int32_t get_SortingSteps_9() const { return ___SortingSteps_9; }
	inline int32_t* get_address_of_SortingSteps_9() { return &___SortingSteps_9; }
	inline void set_SortingSteps_9(int32_t value)
	{
		___SortingSteps_9 = value;
	}

	inline static int32_t get_offset_of_ParticleSystem_10() { return static_cast<int32_t>(offsetof(UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB, ___ParticleSystem_10)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_ParticleSystem_10() const { return ___ParticleSystem_10; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_ParticleSystem_10() { return &___ParticleSystem_10; }
	inline void set_ParticleSystem_10(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___ParticleSystem_10 = value;
		Il2CppCodeGenWriteBarrier((&___ParticleSystem_10), value);
	}

	inline static int32_t get_offset_of_StopBehavior_11() { return static_cast<int32_t>(offsetof(UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB, ___StopBehavior_11)); }
	inline int32_t get_StopBehavior_11() const { return ___StopBehavior_11; }
	inline int32_t* get_address_of_StopBehavior_11() { return &___StopBehavior_11; }
	inline void set_StopBehavior_11(int32_t value)
	{
		___StopBehavior_11 = value;
	}

	inline static int32_t get_offset_of_CustomSortingLayer_12() { return static_cast<int32_t>(offsetof(UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB, ___CustomSortingLayer_12)); }
	inline String_t* get_CustomSortingLayer_12() const { return ___CustomSortingLayer_12; }
	inline String_t** get_address_of_CustomSortingLayer_12() { return &___CustomSortingLayer_12; }
	inline void set_CustomSortingLayer_12(String_t* value)
	{
		___CustomSortingLayer_12 = value;
		Il2CppCodeGenWriteBarrier((&___CustomSortingLayer_12), value);
	}

	inline static int32_t get_offset_of_m_renderers_13() { return static_cast<int32_t>(offsetof(UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB, ___m_renderers_13)); }
	inline RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* get_m_renderers_13() const { return ___m_renderers_13; }
	inline RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93** get_address_of_m_renderers_13() { return &___m_renderers_13; }
	inline void set_m_renderers_13(RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* value)
	{
		___m_renderers_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_renderers_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEFFECT_TBF391E23875775E4FD00E9CFE482A7006B0450BB_H
#ifndef UICONNECTION_T53E7ACB7EFF3340C004AEB665984865335084B07_H
#define UICONNECTION_T53E7ACB7EFF3340C004AEB665984865335084B07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Connections.UIConnection
struct  UIConnection_t53E7ACB7EFF3340C004AEB665984865335084B07  : public RuntimeObject
{
public:
	// System.String Doozy.Engine.UI.Connections.UIConnection::ButtonCategory
	String_t* ___ButtonCategory_1;
	// System.String Doozy.Engine.UI.Connections.UIConnection::ButtonName
	String_t* ___ButtonName_2;
	// System.String Doozy.Engine.UI.Connections.UIConnection::GameEvent
	String_t* ___GameEvent_3;
	// System.Single Doozy.Engine.UI.Connections.UIConnection::TimeDelay
	float ___TimeDelay_4;
	// Doozy.Engine.UI.Connections.UIConnectionTrigger Doozy.Engine.UI.Connections.UIConnection::Trigger
	int32_t ___Trigger_5;

public:
	inline static int32_t get_offset_of_ButtonCategory_1() { return static_cast<int32_t>(offsetof(UIConnection_t53E7ACB7EFF3340C004AEB665984865335084B07, ___ButtonCategory_1)); }
	inline String_t* get_ButtonCategory_1() const { return ___ButtonCategory_1; }
	inline String_t** get_address_of_ButtonCategory_1() { return &___ButtonCategory_1; }
	inline void set_ButtonCategory_1(String_t* value)
	{
		___ButtonCategory_1 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonCategory_1), value);
	}

	inline static int32_t get_offset_of_ButtonName_2() { return static_cast<int32_t>(offsetof(UIConnection_t53E7ACB7EFF3340C004AEB665984865335084B07, ___ButtonName_2)); }
	inline String_t* get_ButtonName_2() const { return ___ButtonName_2; }
	inline String_t** get_address_of_ButtonName_2() { return &___ButtonName_2; }
	inline void set_ButtonName_2(String_t* value)
	{
		___ButtonName_2 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonName_2), value);
	}

	inline static int32_t get_offset_of_GameEvent_3() { return static_cast<int32_t>(offsetof(UIConnection_t53E7ACB7EFF3340C004AEB665984865335084B07, ___GameEvent_3)); }
	inline String_t* get_GameEvent_3() const { return ___GameEvent_3; }
	inline String_t** get_address_of_GameEvent_3() { return &___GameEvent_3; }
	inline void set_GameEvent_3(String_t* value)
	{
		___GameEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___GameEvent_3), value);
	}

	inline static int32_t get_offset_of_TimeDelay_4() { return static_cast<int32_t>(offsetof(UIConnection_t53E7ACB7EFF3340C004AEB665984865335084B07, ___TimeDelay_4)); }
	inline float get_TimeDelay_4() const { return ___TimeDelay_4; }
	inline float* get_address_of_TimeDelay_4() { return &___TimeDelay_4; }
	inline void set_TimeDelay_4(float value)
	{
		___TimeDelay_4 = value;
	}

	inline static int32_t get_offset_of_Trigger_5() { return static_cast<int32_t>(offsetof(UIConnection_t53E7ACB7EFF3340C004AEB665984865335084B07, ___Trigger_5)); }
	inline int32_t get_Trigger_5() const { return ___Trigger_5; }
	inline int32_t* get_address_of_Trigger_5() { return &___Trigger_5; }
	inline void set_Trigger_5(int32_t value)
	{
		___Trigger_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICONNECTION_T53E7ACB7EFF3340C004AEB665984865335084B07_H
#ifndef INPUTDATA_TCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC_H
#define INPUTDATA_TCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Input.InputData
struct  InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC  : public RuntimeObject
{
public:
	// System.Boolean Doozy.Engine.UI.Input.InputData::EnableAlternateInputs
	bool ___EnableAlternateInputs_6;
	// Doozy.Engine.UI.Input.InputMode Doozy.Engine.UI.Input.InputData::InputMode
	int32_t ___InputMode_7;
	// UnityEngine.KeyCode Doozy.Engine.UI.Input.InputData::KeyCode
	int32_t ___KeyCode_8;
	// UnityEngine.KeyCode Doozy.Engine.UI.Input.InputData::KeyCodeAlt
	int32_t ___KeyCodeAlt_9;
	// System.String Doozy.Engine.UI.Input.InputData::VirtualButtonName
	String_t* ___VirtualButtonName_10;
	// System.String Doozy.Engine.UI.Input.InputData::VirtualButtonNameAlt
	String_t* ___VirtualButtonNameAlt_11;

public:
	inline static int32_t get_offset_of_EnableAlternateInputs_6() { return static_cast<int32_t>(offsetof(InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC, ___EnableAlternateInputs_6)); }
	inline bool get_EnableAlternateInputs_6() const { return ___EnableAlternateInputs_6; }
	inline bool* get_address_of_EnableAlternateInputs_6() { return &___EnableAlternateInputs_6; }
	inline void set_EnableAlternateInputs_6(bool value)
	{
		___EnableAlternateInputs_6 = value;
	}

	inline static int32_t get_offset_of_InputMode_7() { return static_cast<int32_t>(offsetof(InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC, ___InputMode_7)); }
	inline int32_t get_InputMode_7() const { return ___InputMode_7; }
	inline int32_t* get_address_of_InputMode_7() { return &___InputMode_7; }
	inline void set_InputMode_7(int32_t value)
	{
		___InputMode_7 = value;
	}

	inline static int32_t get_offset_of_KeyCode_8() { return static_cast<int32_t>(offsetof(InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC, ___KeyCode_8)); }
	inline int32_t get_KeyCode_8() const { return ___KeyCode_8; }
	inline int32_t* get_address_of_KeyCode_8() { return &___KeyCode_8; }
	inline void set_KeyCode_8(int32_t value)
	{
		___KeyCode_8 = value;
	}

	inline static int32_t get_offset_of_KeyCodeAlt_9() { return static_cast<int32_t>(offsetof(InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC, ___KeyCodeAlt_9)); }
	inline int32_t get_KeyCodeAlt_9() const { return ___KeyCodeAlt_9; }
	inline int32_t* get_address_of_KeyCodeAlt_9() { return &___KeyCodeAlt_9; }
	inline void set_KeyCodeAlt_9(int32_t value)
	{
		___KeyCodeAlt_9 = value;
	}

	inline static int32_t get_offset_of_VirtualButtonName_10() { return static_cast<int32_t>(offsetof(InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC, ___VirtualButtonName_10)); }
	inline String_t* get_VirtualButtonName_10() const { return ___VirtualButtonName_10; }
	inline String_t** get_address_of_VirtualButtonName_10() { return &___VirtualButtonName_10; }
	inline void set_VirtualButtonName_10(String_t* value)
	{
		___VirtualButtonName_10 = value;
		Il2CppCodeGenWriteBarrier((&___VirtualButtonName_10), value);
	}

	inline static int32_t get_offset_of_VirtualButtonNameAlt_11() { return static_cast<int32_t>(offsetof(InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC, ___VirtualButtonNameAlt_11)); }
	inline String_t* get_VirtualButtonNameAlt_11() const { return ___VirtualButtonNameAlt_11; }
	inline String_t** get_address_of_VirtualButtonNameAlt_11() { return &___VirtualButtonNameAlt_11; }
	inline void set_VirtualButtonNameAlt_11(String_t* value)
	{
		___VirtualButtonNameAlt_11 = value;
		Il2CppCodeGenWriteBarrier((&___VirtualButtonNameAlt_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTDATA_TCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC_H
#ifndef UIVIEWMESSAGE_T1E69AF139CF5288975D421C8AD6C71A72EB318D5_H
#define UIVIEWMESSAGE_T1E69AF139CF5288975D421C8AD6C71A72EB318D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIViewMessage
struct  UIViewMessage_t1E69AF139CF5288975D421C8AD6C71A72EB318D5  : public Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E
{
public:
	// Doozy.Engine.UI.UIView Doozy.Engine.UI.UIViewMessage::View
	UIView_t65D38836246049951821D4E45B1C81947591EBDF * ___View_3;
	// Doozy.Engine.UI.UIViewBehaviorType Doozy.Engine.UI.UIViewMessage::Type
	int32_t ___Type_4;

public:
	inline static int32_t get_offset_of_View_3() { return static_cast<int32_t>(offsetof(UIViewMessage_t1E69AF139CF5288975D421C8AD6C71A72EB318D5, ___View_3)); }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF * get_View_3() const { return ___View_3; }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF ** get_address_of_View_3() { return &___View_3; }
	inline void set_View_3(UIView_t65D38836246049951821D4E45B1C81947591EBDF * value)
	{
		___View_3 = value;
		Il2CppCodeGenWriteBarrier((&___View_3), value);
	}

	inline static int32_t get_offset_of_Type_4() { return static_cast<int32_t>(offsetof(UIViewMessage_t1E69AF139CF5288975D421C8AD6C71A72EB318D5, ___Type_4)); }
	inline int32_t get_Type_4() const { return ___Type_4; }
	inline int32_t* get_address_of_Type_4() { return &___Type_4; }
	inline void set_Type_4(int32_t value)
	{
		___Type_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWMESSAGE_T1E69AF139CF5288975D421C8AD6C71A72EB318D5_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#define TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t806752C775BA713A91B6588A07CA98417CABC003 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Position_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_RawPosition_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_PositionDelta_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#ifndef NODE_T06F79187A31E6958DED19533B1A8783AD1633439_H
#define NODE_T06F79187A31E6958DED19533B1A8783AD1633439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Nody.Models.Node
struct  Node_t06F79187A31E6958DED19533B1A8783AD1633439  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<Doozy.Engine.Nody.Models.Socket> Doozy.Engine.Nody.Models.Node::m_inputSockets
	List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B * ___m_inputSockets_4;
	// System.Collections.Generic.List`1<Doozy.Engine.Nody.Models.Socket> Doozy.Engine.Nody.Models.Node::m_outputSockets
	List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B * ___m_outputSockets_5;
	// Doozy.Engine.Nody.Models.NodeType Doozy.Engine.Nody.Models.Node::m_nodeType
	int32_t ___m_nodeType_6;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_allowDuplicateNodeName
	bool ___m_allowDuplicateNodeName_7;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_allowEmptyNodeName
	bool ___m_allowEmptyNodeName_8;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_canBeDeleted
	bool ___m_canBeDeleted_9;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_debugMode
	bool ___m_debugMode_10;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_useFixedUpdate
	bool ___m_useFixedUpdate_11;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_useLateUpdate
	bool ___m_useLateUpdate_12;
	// System.Boolean Doozy.Engine.Nody.Models.Node::m_useUpdate
	bool ___m_useUpdate_13;
	// System.Single Doozy.Engine.Nody.Models.Node::m_height
	float ___m_height_14;
	// System.Single Doozy.Engine.Nody.Models.Node::m_width
	float ___m_width_15;
	// System.Single Doozy.Engine.Nody.Models.Node::m_x
	float ___m_x_16;
	// System.Single Doozy.Engine.Nody.Models.Node::m_y
	float ___m_y_17;
	// System.Int32 Doozy.Engine.Nody.Models.Node::m_minimumInputSocketsCount
	int32_t ___m_minimumInputSocketsCount_18;
	// System.Int32 Doozy.Engine.Nody.Models.Node::m_minimumOutputSocketsCount
	int32_t ___m_minimumOutputSocketsCount_19;
	// System.String Doozy.Engine.Nody.Models.Node::m_graphId
	String_t* ___m_graphId_20;
	// System.String Doozy.Engine.Nody.Models.Node::m_id
	String_t* ___m_id_21;
	// System.String Doozy.Engine.Nody.Models.Node::m_name
	String_t* ___m_name_22;
	// Doozy.Engine.Nody.Models.Graph Doozy.Engine.Nody.Models.Node::m_activeGraph
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * ___m_activeGraph_23;
	// System.Boolean Doozy.Engine.Nody.Models.Node::<Ping>k__BackingField
	bool ___U3CPingU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_m_inputSockets_4() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_inputSockets_4)); }
	inline List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B * get_m_inputSockets_4() const { return ___m_inputSockets_4; }
	inline List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B ** get_address_of_m_inputSockets_4() { return &___m_inputSockets_4; }
	inline void set_m_inputSockets_4(List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B * value)
	{
		___m_inputSockets_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputSockets_4), value);
	}

	inline static int32_t get_offset_of_m_outputSockets_5() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_outputSockets_5)); }
	inline List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B * get_m_outputSockets_5() const { return ___m_outputSockets_5; }
	inline List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B ** get_address_of_m_outputSockets_5() { return &___m_outputSockets_5; }
	inline void set_m_outputSockets_5(List_1_t106207FAB8D0B68CB968AD90CBFDF98F6A0FCE9B * value)
	{
		___m_outputSockets_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_outputSockets_5), value);
	}

	inline static int32_t get_offset_of_m_nodeType_6() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_nodeType_6)); }
	inline int32_t get_m_nodeType_6() const { return ___m_nodeType_6; }
	inline int32_t* get_address_of_m_nodeType_6() { return &___m_nodeType_6; }
	inline void set_m_nodeType_6(int32_t value)
	{
		___m_nodeType_6 = value;
	}

	inline static int32_t get_offset_of_m_allowDuplicateNodeName_7() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_allowDuplicateNodeName_7)); }
	inline bool get_m_allowDuplicateNodeName_7() const { return ___m_allowDuplicateNodeName_7; }
	inline bool* get_address_of_m_allowDuplicateNodeName_7() { return &___m_allowDuplicateNodeName_7; }
	inline void set_m_allowDuplicateNodeName_7(bool value)
	{
		___m_allowDuplicateNodeName_7 = value;
	}

	inline static int32_t get_offset_of_m_allowEmptyNodeName_8() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_allowEmptyNodeName_8)); }
	inline bool get_m_allowEmptyNodeName_8() const { return ___m_allowEmptyNodeName_8; }
	inline bool* get_address_of_m_allowEmptyNodeName_8() { return &___m_allowEmptyNodeName_8; }
	inline void set_m_allowEmptyNodeName_8(bool value)
	{
		___m_allowEmptyNodeName_8 = value;
	}

	inline static int32_t get_offset_of_m_canBeDeleted_9() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_canBeDeleted_9)); }
	inline bool get_m_canBeDeleted_9() const { return ___m_canBeDeleted_9; }
	inline bool* get_address_of_m_canBeDeleted_9() { return &___m_canBeDeleted_9; }
	inline void set_m_canBeDeleted_9(bool value)
	{
		___m_canBeDeleted_9 = value;
	}

	inline static int32_t get_offset_of_m_debugMode_10() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_debugMode_10)); }
	inline bool get_m_debugMode_10() const { return ___m_debugMode_10; }
	inline bool* get_address_of_m_debugMode_10() { return &___m_debugMode_10; }
	inline void set_m_debugMode_10(bool value)
	{
		___m_debugMode_10 = value;
	}

	inline static int32_t get_offset_of_m_useFixedUpdate_11() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_useFixedUpdate_11)); }
	inline bool get_m_useFixedUpdate_11() const { return ___m_useFixedUpdate_11; }
	inline bool* get_address_of_m_useFixedUpdate_11() { return &___m_useFixedUpdate_11; }
	inline void set_m_useFixedUpdate_11(bool value)
	{
		___m_useFixedUpdate_11 = value;
	}

	inline static int32_t get_offset_of_m_useLateUpdate_12() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_useLateUpdate_12)); }
	inline bool get_m_useLateUpdate_12() const { return ___m_useLateUpdate_12; }
	inline bool* get_address_of_m_useLateUpdate_12() { return &___m_useLateUpdate_12; }
	inline void set_m_useLateUpdate_12(bool value)
	{
		___m_useLateUpdate_12 = value;
	}

	inline static int32_t get_offset_of_m_useUpdate_13() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_useUpdate_13)); }
	inline bool get_m_useUpdate_13() const { return ___m_useUpdate_13; }
	inline bool* get_address_of_m_useUpdate_13() { return &___m_useUpdate_13; }
	inline void set_m_useUpdate_13(bool value)
	{
		___m_useUpdate_13 = value;
	}

	inline static int32_t get_offset_of_m_height_14() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_height_14)); }
	inline float get_m_height_14() const { return ___m_height_14; }
	inline float* get_address_of_m_height_14() { return &___m_height_14; }
	inline void set_m_height_14(float value)
	{
		___m_height_14 = value;
	}

	inline static int32_t get_offset_of_m_width_15() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_width_15)); }
	inline float get_m_width_15() const { return ___m_width_15; }
	inline float* get_address_of_m_width_15() { return &___m_width_15; }
	inline void set_m_width_15(float value)
	{
		___m_width_15 = value;
	}

	inline static int32_t get_offset_of_m_x_16() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_x_16)); }
	inline float get_m_x_16() const { return ___m_x_16; }
	inline float* get_address_of_m_x_16() { return &___m_x_16; }
	inline void set_m_x_16(float value)
	{
		___m_x_16 = value;
	}

	inline static int32_t get_offset_of_m_y_17() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_y_17)); }
	inline float get_m_y_17() const { return ___m_y_17; }
	inline float* get_address_of_m_y_17() { return &___m_y_17; }
	inline void set_m_y_17(float value)
	{
		___m_y_17 = value;
	}

	inline static int32_t get_offset_of_m_minimumInputSocketsCount_18() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_minimumInputSocketsCount_18)); }
	inline int32_t get_m_minimumInputSocketsCount_18() const { return ___m_minimumInputSocketsCount_18; }
	inline int32_t* get_address_of_m_minimumInputSocketsCount_18() { return &___m_minimumInputSocketsCount_18; }
	inline void set_m_minimumInputSocketsCount_18(int32_t value)
	{
		___m_minimumInputSocketsCount_18 = value;
	}

	inline static int32_t get_offset_of_m_minimumOutputSocketsCount_19() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_minimumOutputSocketsCount_19)); }
	inline int32_t get_m_minimumOutputSocketsCount_19() const { return ___m_minimumOutputSocketsCount_19; }
	inline int32_t* get_address_of_m_minimumOutputSocketsCount_19() { return &___m_minimumOutputSocketsCount_19; }
	inline void set_m_minimumOutputSocketsCount_19(int32_t value)
	{
		___m_minimumOutputSocketsCount_19 = value;
	}

	inline static int32_t get_offset_of_m_graphId_20() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_graphId_20)); }
	inline String_t* get_m_graphId_20() const { return ___m_graphId_20; }
	inline String_t** get_address_of_m_graphId_20() { return &___m_graphId_20; }
	inline void set_m_graphId_20(String_t* value)
	{
		___m_graphId_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphId_20), value);
	}

	inline static int32_t get_offset_of_m_id_21() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_id_21)); }
	inline String_t* get_m_id_21() const { return ___m_id_21; }
	inline String_t** get_address_of_m_id_21() { return &___m_id_21; }
	inline void set_m_id_21(String_t* value)
	{
		___m_id_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_id_21), value);
	}

	inline static int32_t get_offset_of_m_name_22() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_name_22)); }
	inline String_t* get_m_name_22() const { return ___m_name_22; }
	inline String_t** get_address_of_m_name_22() { return &___m_name_22; }
	inline void set_m_name_22(String_t* value)
	{
		___m_name_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_22), value);
	}

	inline static int32_t get_offset_of_m_activeGraph_23() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___m_activeGraph_23)); }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * get_m_activeGraph_23() const { return ___m_activeGraph_23; }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 ** get_address_of_m_activeGraph_23() { return &___m_activeGraph_23; }
	inline void set_m_activeGraph_23(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * value)
	{
		___m_activeGraph_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_activeGraph_23), value);
	}

	inline static int32_t get_offset_of_U3CPingU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Node_t06F79187A31E6958DED19533B1A8783AD1633439, ___U3CPingU3Ek__BackingField_24)); }
	inline bool get_U3CPingU3Ek__BackingField_24() const { return ___U3CPingU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CPingU3Ek__BackingField_24() { return &___U3CPingU3Ek__BackingField_24; }
	inline void set_U3CPingU3Ek__BackingField_24(bool value)
	{
		___U3CPingU3Ek__BackingField_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_T06F79187A31E6958DED19533B1A8783AD1633439_H
#ifndef SOUNDDATABASE_TD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF_H
#define SOUNDDATABASE_TD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundDatabase
struct  SoundDatabase_tD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String Doozy.Engine.Soundy.SoundDatabase::DatabaseName
	String_t* ___DatabaseName_4;
	// UnityEngine.Audio.AudioMixerGroup Doozy.Engine.Soundy.SoundDatabase::OutputAudioMixerGroup
	AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 * ___OutputAudioMixerGroup_5;
	// System.Collections.Generic.List`1<System.String> Doozy.Engine.Soundy.SoundDatabase::SoundNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___SoundNames_6;
	// System.Collections.Generic.List`1<Doozy.Engine.Soundy.SoundGroupData> Doozy.Engine.Soundy.SoundDatabase::Database
	List_1_tACA120E9FFB2F8DB8A48B88331EE4C66A4BA6543 * ___Database_7;

public:
	inline static int32_t get_offset_of_DatabaseName_4() { return static_cast<int32_t>(offsetof(SoundDatabase_tD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF, ___DatabaseName_4)); }
	inline String_t* get_DatabaseName_4() const { return ___DatabaseName_4; }
	inline String_t** get_address_of_DatabaseName_4() { return &___DatabaseName_4; }
	inline void set_DatabaseName_4(String_t* value)
	{
		___DatabaseName_4 = value;
		Il2CppCodeGenWriteBarrier((&___DatabaseName_4), value);
	}

	inline static int32_t get_offset_of_OutputAudioMixerGroup_5() { return static_cast<int32_t>(offsetof(SoundDatabase_tD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF, ___OutputAudioMixerGroup_5)); }
	inline AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 * get_OutputAudioMixerGroup_5() const { return ___OutputAudioMixerGroup_5; }
	inline AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 ** get_address_of_OutputAudioMixerGroup_5() { return &___OutputAudioMixerGroup_5; }
	inline void set_OutputAudioMixerGroup_5(AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 * value)
	{
		___OutputAudioMixerGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&___OutputAudioMixerGroup_5), value);
	}

	inline static int32_t get_offset_of_SoundNames_6() { return static_cast<int32_t>(offsetof(SoundDatabase_tD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF, ___SoundNames_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_SoundNames_6() const { return ___SoundNames_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_SoundNames_6() { return &___SoundNames_6; }
	inline void set_SoundNames_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___SoundNames_6 = value;
		Il2CppCodeGenWriteBarrier((&___SoundNames_6), value);
	}

	inline static int32_t get_offset_of_Database_7() { return static_cast<int32_t>(offsetof(SoundDatabase_tD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF, ___Database_7)); }
	inline List_1_tACA120E9FFB2F8DB8A48B88331EE4C66A4BA6543 * get_Database_7() const { return ___Database_7; }
	inline List_1_tACA120E9FFB2F8DB8A48B88331EE4C66A4BA6543 ** get_address_of_Database_7() { return &___Database_7; }
	inline void set_Database_7(List_1_tACA120E9FFB2F8DB8A48B88331EE4C66A4BA6543 * value)
	{
		___Database_7 = value;
		Il2CppCodeGenWriteBarrier((&___Database_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDDATABASE_TD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF_H
#ifndef SOUNDGROUPDATA_T9CA54942DA1AC5375A6C212C1210DAA5241BE37B_H
#define SOUNDGROUPDATA_T9CA54942DA1AC5375A6C212C1210DAA5241BE37B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundGroupData
struct  SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String Doozy.Engine.Soundy.SoundGroupData::DatabaseName
	String_t* ___DatabaseName_19;
	// System.String Doozy.Engine.Soundy.SoundGroupData::SoundName
	String_t* ___SoundName_20;
	// System.Boolean Doozy.Engine.Soundy.SoundGroupData::IgnoreListenerPause
	bool ___IgnoreListenerPause_21;
	// Doozy.Engine.RangedFloat Doozy.Engine.Soundy.SoundGroupData::Volume
	RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109  ___Volume_22;
	// Doozy.Engine.RangedFloat Doozy.Engine.Soundy.SoundGroupData::Pitch
	RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109  ___Pitch_23;
	// System.Single Doozy.Engine.Soundy.SoundGroupData::SpatialBlend
	float ___SpatialBlend_24;
	// System.Boolean Doozy.Engine.Soundy.SoundGroupData::Loop
	bool ___Loop_25;
	// Doozy.Engine.Soundy.SoundGroupData_PlayMode Doozy.Engine.Soundy.SoundGroupData::Mode
	int32_t ___Mode_26;
	// System.Boolean Doozy.Engine.Soundy.SoundGroupData::ResetSequenceAfterInactiveTime
	bool ___ResetSequenceAfterInactiveTime_27;
	// System.Single Doozy.Engine.Soundy.SoundGroupData::SequenceResetTime
	float ___SequenceResetTime_28;
	// System.Collections.Generic.List`1<Doozy.Engine.Soundy.AudioData> Doozy.Engine.Soundy.SoundGroupData::Sounds
	List_1_t5E5F2D8D8792466A43CA18341D76212F4AD6529E * ___Sounds_29;
	// System.Int32 Doozy.Engine.Soundy.SoundGroupData::m_lastPlayedSoundsIndex
	int32_t ___m_lastPlayedSoundsIndex_30;
	// System.Single Doozy.Engine.Soundy.SoundGroupData::m_lastPlayedSoundTime
	float ___m_lastPlayedSoundTime_31;
	// System.Collections.Generic.List`1<Doozy.Engine.Soundy.AudioData> Doozy.Engine.Soundy.SoundGroupData::m_playedSounds
	List_1_t5E5F2D8D8792466A43CA18341D76212F4AD6529E * ___m_playedSounds_32;
	// Doozy.Engine.Soundy.AudioData Doozy.Engine.Soundy.SoundGroupData::m_lastPlayedAudioData
	AudioData_t5195938BA169F80D32B29A92FE22CDBD99F346D6 * ___m_lastPlayedAudioData_33;

public:
	inline static int32_t get_offset_of_DatabaseName_19() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___DatabaseName_19)); }
	inline String_t* get_DatabaseName_19() const { return ___DatabaseName_19; }
	inline String_t** get_address_of_DatabaseName_19() { return &___DatabaseName_19; }
	inline void set_DatabaseName_19(String_t* value)
	{
		___DatabaseName_19 = value;
		Il2CppCodeGenWriteBarrier((&___DatabaseName_19), value);
	}

	inline static int32_t get_offset_of_SoundName_20() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___SoundName_20)); }
	inline String_t* get_SoundName_20() const { return ___SoundName_20; }
	inline String_t** get_address_of_SoundName_20() { return &___SoundName_20; }
	inline void set_SoundName_20(String_t* value)
	{
		___SoundName_20 = value;
		Il2CppCodeGenWriteBarrier((&___SoundName_20), value);
	}

	inline static int32_t get_offset_of_IgnoreListenerPause_21() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___IgnoreListenerPause_21)); }
	inline bool get_IgnoreListenerPause_21() const { return ___IgnoreListenerPause_21; }
	inline bool* get_address_of_IgnoreListenerPause_21() { return &___IgnoreListenerPause_21; }
	inline void set_IgnoreListenerPause_21(bool value)
	{
		___IgnoreListenerPause_21 = value;
	}

	inline static int32_t get_offset_of_Volume_22() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___Volume_22)); }
	inline RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109  get_Volume_22() const { return ___Volume_22; }
	inline RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109 * get_address_of_Volume_22() { return &___Volume_22; }
	inline void set_Volume_22(RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109  value)
	{
		___Volume_22 = value;
	}

	inline static int32_t get_offset_of_Pitch_23() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___Pitch_23)); }
	inline RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109  get_Pitch_23() const { return ___Pitch_23; }
	inline RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109 * get_address_of_Pitch_23() { return &___Pitch_23; }
	inline void set_Pitch_23(RangedFloat_t2374617685E2BBFB2ADC9396135C5C1A27133109  value)
	{
		___Pitch_23 = value;
	}

	inline static int32_t get_offset_of_SpatialBlend_24() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___SpatialBlend_24)); }
	inline float get_SpatialBlend_24() const { return ___SpatialBlend_24; }
	inline float* get_address_of_SpatialBlend_24() { return &___SpatialBlend_24; }
	inline void set_SpatialBlend_24(float value)
	{
		___SpatialBlend_24 = value;
	}

	inline static int32_t get_offset_of_Loop_25() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___Loop_25)); }
	inline bool get_Loop_25() const { return ___Loop_25; }
	inline bool* get_address_of_Loop_25() { return &___Loop_25; }
	inline void set_Loop_25(bool value)
	{
		___Loop_25 = value;
	}

	inline static int32_t get_offset_of_Mode_26() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___Mode_26)); }
	inline int32_t get_Mode_26() const { return ___Mode_26; }
	inline int32_t* get_address_of_Mode_26() { return &___Mode_26; }
	inline void set_Mode_26(int32_t value)
	{
		___Mode_26 = value;
	}

	inline static int32_t get_offset_of_ResetSequenceAfterInactiveTime_27() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___ResetSequenceAfterInactiveTime_27)); }
	inline bool get_ResetSequenceAfterInactiveTime_27() const { return ___ResetSequenceAfterInactiveTime_27; }
	inline bool* get_address_of_ResetSequenceAfterInactiveTime_27() { return &___ResetSequenceAfterInactiveTime_27; }
	inline void set_ResetSequenceAfterInactiveTime_27(bool value)
	{
		___ResetSequenceAfterInactiveTime_27 = value;
	}

	inline static int32_t get_offset_of_SequenceResetTime_28() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___SequenceResetTime_28)); }
	inline float get_SequenceResetTime_28() const { return ___SequenceResetTime_28; }
	inline float* get_address_of_SequenceResetTime_28() { return &___SequenceResetTime_28; }
	inline void set_SequenceResetTime_28(float value)
	{
		___SequenceResetTime_28 = value;
	}

	inline static int32_t get_offset_of_Sounds_29() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___Sounds_29)); }
	inline List_1_t5E5F2D8D8792466A43CA18341D76212F4AD6529E * get_Sounds_29() const { return ___Sounds_29; }
	inline List_1_t5E5F2D8D8792466A43CA18341D76212F4AD6529E ** get_address_of_Sounds_29() { return &___Sounds_29; }
	inline void set_Sounds_29(List_1_t5E5F2D8D8792466A43CA18341D76212F4AD6529E * value)
	{
		___Sounds_29 = value;
		Il2CppCodeGenWriteBarrier((&___Sounds_29), value);
	}

	inline static int32_t get_offset_of_m_lastPlayedSoundsIndex_30() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___m_lastPlayedSoundsIndex_30)); }
	inline int32_t get_m_lastPlayedSoundsIndex_30() const { return ___m_lastPlayedSoundsIndex_30; }
	inline int32_t* get_address_of_m_lastPlayedSoundsIndex_30() { return &___m_lastPlayedSoundsIndex_30; }
	inline void set_m_lastPlayedSoundsIndex_30(int32_t value)
	{
		___m_lastPlayedSoundsIndex_30 = value;
	}

	inline static int32_t get_offset_of_m_lastPlayedSoundTime_31() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___m_lastPlayedSoundTime_31)); }
	inline float get_m_lastPlayedSoundTime_31() const { return ___m_lastPlayedSoundTime_31; }
	inline float* get_address_of_m_lastPlayedSoundTime_31() { return &___m_lastPlayedSoundTime_31; }
	inline void set_m_lastPlayedSoundTime_31(float value)
	{
		___m_lastPlayedSoundTime_31 = value;
	}

	inline static int32_t get_offset_of_m_playedSounds_32() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___m_playedSounds_32)); }
	inline List_1_t5E5F2D8D8792466A43CA18341D76212F4AD6529E * get_m_playedSounds_32() const { return ___m_playedSounds_32; }
	inline List_1_t5E5F2D8D8792466A43CA18341D76212F4AD6529E ** get_address_of_m_playedSounds_32() { return &___m_playedSounds_32; }
	inline void set_m_playedSounds_32(List_1_t5E5F2D8D8792466A43CA18341D76212F4AD6529E * value)
	{
		___m_playedSounds_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_playedSounds_32), value);
	}

	inline static int32_t get_offset_of_m_lastPlayedAudioData_33() { return static_cast<int32_t>(offsetof(SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B, ___m_lastPlayedAudioData_33)); }
	inline AudioData_t5195938BA169F80D32B29A92FE22CDBD99F346D6 * get_m_lastPlayedAudioData_33() const { return ___m_lastPlayedAudioData_33; }
	inline AudioData_t5195938BA169F80D32B29A92FE22CDBD99F346D6 ** get_address_of_m_lastPlayedAudioData_33() { return &___m_lastPlayedAudioData_33; }
	inline void set_m_lastPlayedAudioData_33(AudioData_t5195938BA169F80D32B29A92FE22CDBD99F346D6 * value)
	{
		___m_lastPlayedAudioData_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_lastPlayedAudioData_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDGROUPDATA_T9CA54942DA1AC5375A6C212C1210DAA5241BE37B_H
#ifndef SOUNDYDATABASE_T053E1E9D0784E5A630FB918EFC5A7ACAA1D45656_H
#define SOUNDYDATABASE_T053E1E9D0784E5A630FB918EFC5A7ACAA1D45656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundyDatabase
struct  SoundyDatabase_t053E1E9D0784E5A630FB918EFC5A7ACAA1D45656  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<System.String> Doozy.Engine.Soundy.SoundyDatabase::DatabaseNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___DatabaseNames_4;
	// System.Collections.Generic.List`1<Doozy.Engine.Soundy.SoundDatabase> Doozy.Engine.Soundy.SoundyDatabase::SoundDatabases
	List_1_tC81AEA829B78D727C4AC1CE4F27E604E48B96677 * ___SoundDatabases_5;

public:
	inline static int32_t get_offset_of_DatabaseNames_4() { return static_cast<int32_t>(offsetof(SoundyDatabase_t053E1E9D0784E5A630FB918EFC5A7ACAA1D45656, ___DatabaseNames_4)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_DatabaseNames_4() const { return ___DatabaseNames_4; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_DatabaseNames_4() { return &___DatabaseNames_4; }
	inline void set_DatabaseNames_4(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___DatabaseNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___DatabaseNames_4), value);
	}

	inline static int32_t get_offset_of_SoundDatabases_5() { return static_cast<int32_t>(offsetof(SoundyDatabase_t053E1E9D0784E5A630FB918EFC5A7ACAA1D45656, ___SoundDatabases_5)); }
	inline List_1_tC81AEA829B78D727C4AC1CE4F27E604E48B96677 * get_SoundDatabases_5() const { return ___SoundDatabases_5; }
	inline List_1_tC81AEA829B78D727C4AC1CE4F27E604E48B96677 ** get_address_of_SoundDatabases_5() { return &___SoundDatabases_5; }
	inline void set_SoundDatabases_5(List_1_tC81AEA829B78D727C4AC1CE4F27E604E48B96677 * value)
	{
		___SoundDatabases_5 = value;
		Il2CppCodeGenWriteBarrier((&___SoundDatabases_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDYDATABASE_T053E1E9D0784E5A630FB918EFC5A7ACAA1D45656_H
#ifndef TOUCHINFO_T498F97DAD203F630B0C4E27944F9DEDAE95BE9A8_H
#define TOUCHINFO_T498F97DAD203F630B0C4E27944F9DEDAE95BE9A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.TouchInfo
struct  TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8 
{
public:
	// UnityEngine.Touch Doozy.Engine.Touchy.TouchInfo::Touch
	Touch_t806752C775BA713A91B6588A07CA98417CABC003  ___Touch_0;
	// Doozy.Engine.Touchy.Swipe Doozy.Engine.Touchy.TouchInfo::Direction
	int32_t ___Direction_1;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.TouchInfo::RawDirection
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___RawDirection_2;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.TouchInfo::StartPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___StartPosition_3;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.TouchInfo::EndPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___EndPosition_4;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.TouchInfo::Velocity
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Velocity_5;
	// System.Single Doozy.Engine.Touchy.TouchInfo::StartTime
	float ___StartTime_6;
	// System.Single Doozy.Engine.Touchy.TouchInfo::EndTime
	float ___EndTime_7;
	// System.Single Doozy.Engine.Touchy.TouchInfo::Duration
	float ___Duration_8;
	// System.Boolean Doozy.Engine.Touchy.TouchInfo::Tap
	bool ___Tap_9;
	// System.Boolean Doozy.Engine.Touchy.TouchInfo::LongTap
	bool ___LongTap_10;
	// System.Single Doozy.Engine.Touchy.TouchInfo::Distance
	float ___Distance_11;
	// System.Single Doozy.Engine.Touchy.TouchInfo::LongestDistance
	float ___LongestDistance_12;
	// UnityEngine.GameObject Doozy.Engine.Touchy.TouchInfo::GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___GameObject_13;
	// UnityEngine.GameObject Doozy.Engine.Touchy.TouchInfo::DraggedObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DraggedObject_14;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.TouchInfo::CurrentTouchPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___CurrentTouchPosition_15;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.TouchInfo::PreviousTouchPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___PreviousTouchPosition_16;
	// System.Single Doozy.Engine.Touchy.TouchInfo::TouchDeltaTime
	float ___TouchDeltaTime_17;

public:
	inline static int32_t get_offset_of_Touch_0() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___Touch_0)); }
	inline Touch_t806752C775BA713A91B6588A07CA98417CABC003  get_Touch_0() const { return ___Touch_0; }
	inline Touch_t806752C775BA713A91B6588A07CA98417CABC003 * get_address_of_Touch_0() { return &___Touch_0; }
	inline void set_Touch_0(Touch_t806752C775BA713A91B6588A07CA98417CABC003  value)
	{
		___Touch_0 = value;
	}

	inline static int32_t get_offset_of_Direction_1() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___Direction_1)); }
	inline int32_t get_Direction_1() const { return ___Direction_1; }
	inline int32_t* get_address_of_Direction_1() { return &___Direction_1; }
	inline void set_Direction_1(int32_t value)
	{
		___Direction_1 = value;
	}

	inline static int32_t get_offset_of_RawDirection_2() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___RawDirection_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_RawDirection_2() const { return ___RawDirection_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_RawDirection_2() { return &___RawDirection_2; }
	inline void set_RawDirection_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___RawDirection_2 = value;
	}

	inline static int32_t get_offset_of_StartPosition_3() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___StartPosition_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_StartPosition_3() const { return ___StartPosition_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_StartPosition_3() { return &___StartPosition_3; }
	inline void set_StartPosition_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___StartPosition_3 = value;
	}

	inline static int32_t get_offset_of_EndPosition_4() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___EndPosition_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_EndPosition_4() const { return ___EndPosition_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_EndPosition_4() { return &___EndPosition_4; }
	inline void set_EndPosition_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___EndPosition_4 = value;
	}

	inline static int32_t get_offset_of_Velocity_5() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___Velocity_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Velocity_5() const { return ___Velocity_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Velocity_5() { return &___Velocity_5; }
	inline void set_Velocity_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Velocity_5 = value;
	}

	inline static int32_t get_offset_of_StartTime_6() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___StartTime_6)); }
	inline float get_StartTime_6() const { return ___StartTime_6; }
	inline float* get_address_of_StartTime_6() { return &___StartTime_6; }
	inline void set_StartTime_6(float value)
	{
		___StartTime_6 = value;
	}

	inline static int32_t get_offset_of_EndTime_7() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___EndTime_7)); }
	inline float get_EndTime_7() const { return ___EndTime_7; }
	inline float* get_address_of_EndTime_7() { return &___EndTime_7; }
	inline void set_EndTime_7(float value)
	{
		___EndTime_7 = value;
	}

	inline static int32_t get_offset_of_Duration_8() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___Duration_8)); }
	inline float get_Duration_8() const { return ___Duration_8; }
	inline float* get_address_of_Duration_8() { return &___Duration_8; }
	inline void set_Duration_8(float value)
	{
		___Duration_8 = value;
	}

	inline static int32_t get_offset_of_Tap_9() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___Tap_9)); }
	inline bool get_Tap_9() const { return ___Tap_9; }
	inline bool* get_address_of_Tap_9() { return &___Tap_9; }
	inline void set_Tap_9(bool value)
	{
		___Tap_9 = value;
	}

	inline static int32_t get_offset_of_LongTap_10() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___LongTap_10)); }
	inline bool get_LongTap_10() const { return ___LongTap_10; }
	inline bool* get_address_of_LongTap_10() { return &___LongTap_10; }
	inline void set_LongTap_10(bool value)
	{
		___LongTap_10 = value;
	}

	inline static int32_t get_offset_of_Distance_11() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___Distance_11)); }
	inline float get_Distance_11() const { return ___Distance_11; }
	inline float* get_address_of_Distance_11() { return &___Distance_11; }
	inline void set_Distance_11(float value)
	{
		___Distance_11 = value;
	}

	inline static int32_t get_offset_of_LongestDistance_12() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___LongestDistance_12)); }
	inline float get_LongestDistance_12() const { return ___LongestDistance_12; }
	inline float* get_address_of_LongestDistance_12() { return &___LongestDistance_12; }
	inline void set_LongestDistance_12(float value)
	{
		___LongestDistance_12 = value;
	}

	inline static int32_t get_offset_of_GameObject_13() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___GameObject_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_GameObject_13() const { return ___GameObject_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_GameObject_13() { return &___GameObject_13; }
	inline void set_GameObject_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___GameObject_13 = value;
		Il2CppCodeGenWriteBarrier((&___GameObject_13), value);
	}

	inline static int32_t get_offset_of_DraggedObject_14() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___DraggedObject_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DraggedObject_14() const { return ___DraggedObject_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DraggedObject_14() { return &___DraggedObject_14; }
	inline void set_DraggedObject_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DraggedObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___DraggedObject_14), value);
	}

	inline static int32_t get_offset_of_CurrentTouchPosition_15() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___CurrentTouchPosition_15)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_CurrentTouchPosition_15() const { return ___CurrentTouchPosition_15; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_CurrentTouchPosition_15() { return &___CurrentTouchPosition_15; }
	inline void set_CurrentTouchPosition_15(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___CurrentTouchPosition_15 = value;
	}

	inline static int32_t get_offset_of_PreviousTouchPosition_16() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___PreviousTouchPosition_16)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_PreviousTouchPosition_16() const { return ___PreviousTouchPosition_16; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_PreviousTouchPosition_16() { return &___PreviousTouchPosition_16; }
	inline void set_PreviousTouchPosition_16(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___PreviousTouchPosition_16 = value;
	}

	inline static int32_t get_offset_of_TouchDeltaTime_17() { return static_cast<int32_t>(offsetof(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8, ___TouchDeltaTime_17)); }
	inline float get_TouchDeltaTime_17() const { return ___TouchDeltaTime_17; }
	inline float* get_address_of_TouchDeltaTime_17() { return &___TouchDeltaTime_17; }
	inline void set_TouchDeltaTime_17(float value)
	{
		___TouchDeltaTime_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Doozy.Engine.Touchy.TouchInfo
struct TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8_marshaled_pinvoke
{
	Touch_t806752C775BA713A91B6588A07CA98417CABC003  ___Touch_0;
	int32_t ___Direction_1;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___RawDirection_2;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___StartPosition_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___EndPosition_4;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Velocity_5;
	float ___StartTime_6;
	float ___EndTime_7;
	float ___Duration_8;
	int32_t ___Tap_9;
	int32_t ___LongTap_10;
	float ___Distance_11;
	float ___LongestDistance_12;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___GameObject_13;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DraggedObject_14;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___CurrentTouchPosition_15;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___PreviousTouchPosition_16;
	float ___TouchDeltaTime_17;
};
// Native definition for COM marshalling of Doozy.Engine.Touchy.TouchInfo
struct TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8_marshaled_com
{
	Touch_t806752C775BA713A91B6588A07CA98417CABC003  ___Touch_0;
	int32_t ___Direction_1;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___RawDirection_2;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___StartPosition_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___EndPosition_4;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Velocity_5;
	float ___StartTime_6;
	float ___EndTime_7;
	float ___Duration_8;
	int32_t ___Tap_9;
	int32_t ___LongTap_10;
	float ___Distance_11;
	float ___LongestDistance_12;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___GameObject_13;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DraggedObject_14;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___CurrentTouchPosition_15;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___PreviousTouchPosition_16;
	float ___TouchDeltaTime_17;
};
#endif // TOUCHINFO_T498F97DAD203F630B0C4E27944F9DEDAE95BE9A8_H
#ifndef TOUCHYSETTINGS_TC0D66C57A1895ECBB5447AE365175AE4FAA3F459_H
#define TOUCHYSETTINGS_TC0D66C57A1895ECBB5447AE365175AE4FAA3F459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.TouchySettings
struct  TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Single Doozy.Engine.Touchy.TouchySettings::LongTapDuration
	float ___LongTapDuration_12;
	// System.Single Doozy.Engine.Touchy.TouchySettings::SwipeLength
	float ___SwipeLength_13;

public:
	inline static int32_t get_offset_of_LongTapDuration_12() { return static_cast<int32_t>(offsetof(TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459, ___LongTapDuration_12)); }
	inline float get_LongTapDuration_12() const { return ___LongTapDuration_12; }
	inline float* get_address_of_LongTapDuration_12() { return &___LongTapDuration_12; }
	inline void set_LongTapDuration_12(float value)
	{
		___LongTapDuration_12 = value;
	}

	inline static int32_t get_offset_of_SwipeLength_13() { return static_cast<int32_t>(offsetof(TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459, ___SwipeLength_13)); }
	inline float get_SwipeLength_13() const { return ___SwipeLength_13; }
	inline float* get_address_of_SwipeLength_13() { return &___SwipeLength_13; }
	inline void set_SwipeLength_13(float value)
	{
		___SwipeLength_13 = value;
	}
};

struct TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459_StaticFields
{
public:
	// Doozy.Engine.Touchy.TouchySettings Doozy.Engine.Touchy.TouchySettings::s_instance
	TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459 * ___s_instance_5;

public:
	inline static int32_t get_offset_of_s_instance_5() { return static_cast<int32_t>(offsetof(TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459_StaticFields, ___s_instance_5)); }
	inline TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459 * get_s_instance_5() const { return ___s_instance_5; }
	inline TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459 ** get_address_of_s_instance_5() { return &___s_instance_5; }
	inline void set_s_instance_5(TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459 * value)
	{
		___s_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHYSETTINGS_TC0D66C57A1895ECBB5447AE365175AE4FAA3F459_H
#ifndef UIANIMATIONDATA_TC39262163A9335ACF2F2E20FAB5E1AC8898F75AC_H
#define UIANIMATIONDATA_TC39262163A9335ACF2F2E20FAB5E1AC8898F75AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimationData
struct  UIAnimationData_tC39262163A9335ACF2F2E20FAB5E1AC8898F75AC  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Doozy.Engine.UI.Animation.UIAnimation Doozy.Engine.UI.Animation.UIAnimationData::Animation
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * ___Animation_4;
	// System.String Doozy.Engine.UI.Animation.UIAnimationData::Category
	String_t* ___Category_5;
	// System.String Doozy.Engine.UI.Animation.UIAnimationData::Name
	String_t* ___Name_6;

public:
	inline static int32_t get_offset_of_Animation_4() { return static_cast<int32_t>(offsetof(UIAnimationData_tC39262163A9335ACF2F2E20FAB5E1AC8898F75AC, ___Animation_4)); }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * get_Animation_4() const { return ___Animation_4; }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C ** get_address_of_Animation_4() { return &___Animation_4; }
	inline void set_Animation_4(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * value)
	{
		___Animation_4 = value;
		Il2CppCodeGenWriteBarrier((&___Animation_4), value);
	}

	inline static int32_t get_offset_of_Category_5() { return static_cast<int32_t>(offsetof(UIAnimationData_tC39262163A9335ACF2F2E20FAB5E1AC8898F75AC, ___Category_5)); }
	inline String_t* get_Category_5() const { return ___Category_5; }
	inline String_t** get_address_of_Category_5() { return &___Category_5; }
	inline void set_Category_5(String_t* value)
	{
		___Category_5 = value;
		Il2CppCodeGenWriteBarrier((&___Category_5), value);
	}

	inline static int32_t get_offset_of_Name_6() { return static_cast<int32_t>(offsetof(UIAnimationData_tC39262163A9335ACF2F2E20FAB5E1AC8898F75AC, ___Name_6)); }
	inline String_t* get_Name_6() const { return ___Name_6; }
	inline String_t** get_address_of_Name_6() { return &___Name_6; }
	inline void set_Name_6(String_t* value)
	{
		___Name_6 = value;
		Il2CppCodeGenWriteBarrier((&___Name_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIANIMATIONDATA_TC39262163A9335ACF2F2E20FAB5E1AC8898F75AC_H
#ifndef UIANIMATIONDATABASE_TB406B517A2E003ABEFCE8F9C27E3CACBABAC7987_H
#define UIANIMATIONDATABASE_TB406B517A2E003ABEFCE8F9C27E3CACBABAC7987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimationDatabase
struct  UIAnimationDatabase_tB406B517A2E003ABEFCE8F9C27E3CACBABAC7987  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<System.String> Doozy.Engine.UI.Animation.UIAnimationDatabase::AnimationNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___AnimationNames_4;
	// System.Collections.Generic.List`1<Doozy.Engine.UI.Animation.UIAnimationData> Doozy.Engine.UI.Animation.UIAnimationDatabase::Database
	List_1_t0E33A3C0603495E2EF4F7E425A76DDA4F6162703 * ___Database_5;
	// System.String Doozy.Engine.UI.Animation.UIAnimationDatabase::DatabaseName
	String_t* ___DatabaseName_6;
	// Doozy.Engine.UI.Animation.AnimationType Doozy.Engine.UI.Animation.UIAnimationDatabase::DataType
	int32_t ___DataType_7;

public:
	inline static int32_t get_offset_of_AnimationNames_4() { return static_cast<int32_t>(offsetof(UIAnimationDatabase_tB406B517A2E003ABEFCE8F9C27E3CACBABAC7987, ___AnimationNames_4)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_AnimationNames_4() const { return ___AnimationNames_4; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_AnimationNames_4() { return &___AnimationNames_4; }
	inline void set_AnimationNames_4(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___AnimationNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationNames_4), value);
	}

	inline static int32_t get_offset_of_Database_5() { return static_cast<int32_t>(offsetof(UIAnimationDatabase_tB406B517A2E003ABEFCE8F9C27E3CACBABAC7987, ___Database_5)); }
	inline List_1_t0E33A3C0603495E2EF4F7E425A76DDA4F6162703 * get_Database_5() const { return ___Database_5; }
	inline List_1_t0E33A3C0603495E2EF4F7E425A76DDA4F6162703 ** get_address_of_Database_5() { return &___Database_5; }
	inline void set_Database_5(List_1_t0E33A3C0603495E2EF4F7E425A76DDA4F6162703 * value)
	{
		___Database_5 = value;
		Il2CppCodeGenWriteBarrier((&___Database_5), value);
	}

	inline static int32_t get_offset_of_DatabaseName_6() { return static_cast<int32_t>(offsetof(UIAnimationDatabase_tB406B517A2E003ABEFCE8F9C27E3CACBABAC7987, ___DatabaseName_6)); }
	inline String_t* get_DatabaseName_6() const { return ___DatabaseName_6; }
	inline String_t** get_address_of_DatabaseName_6() { return &___DatabaseName_6; }
	inline void set_DatabaseName_6(String_t* value)
	{
		___DatabaseName_6 = value;
		Il2CppCodeGenWriteBarrier((&___DatabaseName_6), value);
	}

	inline static int32_t get_offset_of_DataType_7() { return static_cast<int32_t>(offsetof(UIAnimationDatabase_tB406B517A2E003ABEFCE8F9C27E3CACBABAC7987, ___DataType_7)); }
	inline int32_t get_DataType_7() const { return ___DataType_7; }
	inline int32_t* get_address_of_DataType_7() { return &___DataType_7; }
	inline void set_DataType_7(int32_t value)
	{
		___DataType_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIANIMATIONDATABASE_TB406B517A2E003ABEFCE8F9C27E3CACBABAC7987_H
#ifndef UIANIMATIONS_TBAAC15BCACC939558EFA45A2D4840A592D34E4B5_H
#define UIANIMATIONS_TBAAC15BCACC939558EFA45A2D4840A592D34E4B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.UIAnimations
struct  UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Doozy.Engine.UI.Animation.UIAnimationsDatabase Doozy.Engine.UI.Animation.UIAnimations::Show
	UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * ___Show_8;
	// Doozy.Engine.UI.Animation.UIAnimationsDatabase Doozy.Engine.UI.Animation.UIAnimations::Hide
	UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * ___Hide_9;
	// Doozy.Engine.UI.Animation.UIAnimationsDatabase Doozy.Engine.UI.Animation.UIAnimations::Loop
	UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * ___Loop_10;
	// Doozy.Engine.UI.Animation.UIAnimationsDatabase Doozy.Engine.UI.Animation.UIAnimations::Punch
	UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * ___Punch_11;
	// Doozy.Engine.UI.Animation.UIAnimationsDatabase Doozy.Engine.UI.Animation.UIAnimations::State
	UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * ___State_12;

public:
	inline static int32_t get_offset_of_Show_8() { return static_cast<int32_t>(offsetof(UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5, ___Show_8)); }
	inline UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * get_Show_8() const { return ___Show_8; }
	inline UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E ** get_address_of_Show_8() { return &___Show_8; }
	inline void set_Show_8(UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * value)
	{
		___Show_8 = value;
		Il2CppCodeGenWriteBarrier((&___Show_8), value);
	}

	inline static int32_t get_offset_of_Hide_9() { return static_cast<int32_t>(offsetof(UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5, ___Hide_9)); }
	inline UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * get_Hide_9() const { return ___Hide_9; }
	inline UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E ** get_address_of_Hide_9() { return &___Hide_9; }
	inline void set_Hide_9(UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * value)
	{
		___Hide_9 = value;
		Il2CppCodeGenWriteBarrier((&___Hide_9), value);
	}

	inline static int32_t get_offset_of_Loop_10() { return static_cast<int32_t>(offsetof(UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5, ___Loop_10)); }
	inline UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * get_Loop_10() const { return ___Loop_10; }
	inline UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E ** get_address_of_Loop_10() { return &___Loop_10; }
	inline void set_Loop_10(UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * value)
	{
		___Loop_10 = value;
		Il2CppCodeGenWriteBarrier((&___Loop_10), value);
	}

	inline static int32_t get_offset_of_Punch_11() { return static_cast<int32_t>(offsetof(UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5, ___Punch_11)); }
	inline UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * get_Punch_11() const { return ___Punch_11; }
	inline UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E ** get_address_of_Punch_11() { return &___Punch_11; }
	inline void set_Punch_11(UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * value)
	{
		___Punch_11 = value;
		Il2CppCodeGenWriteBarrier((&___Punch_11), value);
	}

	inline static int32_t get_offset_of_State_12() { return static_cast<int32_t>(offsetof(UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5, ___State_12)); }
	inline UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * get_State_12() const { return ___State_12; }
	inline UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E ** get_address_of_State_12() { return &___State_12; }
	inline void set_State_12(UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E * value)
	{
		___State_12 = value;
		Il2CppCodeGenWriteBarrier((&___State_12), value);
	}
};

struct UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5_StaticFields
{
public:
	// Doozy.Engine.UI.Animation.UIAnimations Doozy.Engine.UI.Animation.UIAnimations::s_instance
	UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5 * ___s_instance_7;

public:
	inline static int32_t get_offset_of_s_instance_7() { return static_cast<int32_t>(offsetof(UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5_StaticFields, ___s_instance_7)); }
	inline UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5 * get_s_instance_7() const { return ___s_instance_7; }
	inline UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5 ** get_address_of_s_instance_7() { return &___s_instance_7; }
	inline void set_s_instance_7(UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5 * value)
	{
		___s_instance_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIANIMATIONS_TBAAC15BCACC939558EFA45A2D4840A592D34E4B5_H
#ifndef LISTOFNAMES_TD6460E5DEC1D83139F7FCA386EF755D7EB6ABED1_H
#define LISTOFNAMES_TD6460E5DEC1D83139F7FCA386EF755D7EB6ABED1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.ListOfNames
struct  ListOfNames_tD6460E5DEC1D83139F7FCA386EF755D7EB6ABED1  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String Doozy.Engine.UI.Base.ListOfNames::CategoryName
	String_t* ___CategoryName_4;
	// Doozy.Engine.UI.Base.NamesDatabaseType Doozy.Engine.UI.Base.ListOfNames::DatabaseType
	int32_t ___DatabaseType_5;
	// System.Collections.Generic.List`1<System.String> Doozy.Engine.UI.Base.ListOfNames::Names
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___Names_6;

public:
	inline static int32_t get_offset_of_CategoryName_4() { return static_cast<int32_t>(offsetof(ListOfNames_tD6460E5DEC1D83139F7FCA386EF755D7EB6ABED1, ___CategoryName_4)); }
	inline String_t* get_CategoryName_4() const { return ___CategoryName_4; }
	inline String_t** get_address_of_CategoryName_4() { return &___CategoryName_4; }
	inline void set_CategoryName_4(String_t* value)
	{
		___CategoryName_4 = value;
		Il2CppCodeGenWriteBarrier((&___CategoryName_4), value);
	}

	inline static int32_t get_offset_of_DatabaseType_5() { return static_cast<int32_t>(offsetof(ListOfNames_tD6460E5DEC1D83139F7FCA386EF755D7EB6ABED1, ___DatabaseType_5)); }
	inline int32_t get_DatabaseType_5() const { return ___DatabaseType_5; }
	inline int32_t* get_address_of_DatabaseType_5() { return &___DatabaseType_5; }
	inline void set_DatabaseType_5(int32_t value)
	{
		___DatabaseType_5 = value;
	}

	inline static int32_t get_offset_of_Names_6() { return static_cast<int32_t>(offsetof(ListOfNames_tD6460E5DEC1D83139F7FCA386EF755D7EB6ABED1, ___Names_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_Names_6() const { return ___Names_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_Names_6() { return &___Names_6; }
	inline void set_Names_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___Names_6 = value;
		Il2CppCodeGenWriteBarrier((&___Names_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTOFNAMES_TD6460E5DEC1D83139F7FCA386EF755D7EB6ABED1_H
#ifndef NAMESDATABASE_T30D5844C9F956C46140FCFA6F3F88F97B35FF36D_H
#define NAMESDATABASE_T30D5844C9F956C46140FCFA6F3F88F97B35FF36D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.NamesDatabase
struct  NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Doozy.Engine.UI.Base.NamesDatabaseType Doozy.Engine.UI.Base.NamesDatabase::DatabaseType
	int32_t ___DatabaseType_13;
	// System.Collections.Generic.List`1<System.String> Doozy.Engine.UI.Base.NamesDatabase::CategoryNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___CategoryNames_14;
	// System.Collections.Generic.List`1<Doozy.Engine.UI.Base.ListOfNames> Doozy.Engine.UI.Base.NamesDatabase::Categories
	List_1_t425AC22162310E5E66FFC809C05DD87C5E6288C7 * ___Categories_15;

public:
	inline static int32_t get_offset_of_DatabaseType_13() { return static_cast<int32_t>(offsetof(NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D, ___DatabaseType_13)); }
	inline int32_t get_DatabaseType_13() const { return ___DatabaseType_13; }
	inline int32_t* get_address_of_DatabaseType_13() { return &___DatabaseType_13; }
	inline void set_DatabaseType_13(int32_t value)
	{
		___DatabaseType_13 = value;
	}

	inline static int32_t get_offset_of_CategoryNames_14() { return static_cast<int32_t>(offsetof(NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D, ___CategoryNames_14)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_CategoryNames_14() const { return ___CategoryNames_14; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_CategoryNames_14() { return &___CategoryNames_14; }
	inline void set_CategoryNames_14(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___CategoryNames_14 = value;
		Il2CppCodeGenWriteBarrier((&___CategoryNames_14), value);
	}

	inline static int32_t get_offset_of_Categories_15() { return static_cast<int32_t>(offsetof(NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D, ___Categories_15)); }
	inline List_1_t425AC22162310E5E66FFC809C05DD87C5E6288C7 * get_Categories_15() const { return ___Categories_15; }
	inline List_1_t425AC22162310E5E66FFC809C05DD87C5E6288C7 ** get_address_of_Categories_15() { return &___Categories_15; }
	inline void set_Categories_15(List_1_t425AC22162310E5E66FFC809C05DD87C5E6288C7 * value)
	{
		___Categories_15 = value;
		Il2CppCodeGenWriteBarrier((&___Categories_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESDATABASE_T30D5844C9F956C46140FCFA6F3F88F97B35FF36D_H
#ifndef UIBUTTONSETTINGS_TF1C628E7B5F15F250AC021A5B7E92CE91096659A_H
#define UIBUTTONSETTINGS_TF1C628E7B5F15F250AC021A5B7E92CE91096659A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Settings.UIButtonSettings
struct  UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Doozy.Engine.UI.Base.NamesDatabase Doozy.Engine.UI.Settings.UIButtonSettings::database
	NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D * ___database_6;
	// Doozy.Engine.UI.Input.InputMode Doozy.Engine.UI.Settings.UIButtonSettings::InputMode
	int32_t ___InputMode_17;
	// UnityEngine.KeyCode Doozy.Engine.UI.Settings.UIButtonSettings::KeyCode
	int32_t ___KeyCode_18;
	// UnityEngine.KeyCode Doozy.Engine.UI.Settings.UIButtonSettings::KeyCodeAlt
	int32_t ___KeyCodeAlt_19;
	// Doozy.Engine.UI.SingleClickMode Doozy.Engine.UI.Settings.UIButtonSettings::ClickMode
	int32_t ___ClickMode_20;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::AllowMultipleClicks
	bool ___AllowMultipleClicks_21;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::DeselectButtonAfterClick
	bool ___DeselectButtonAfterClick_22;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::EnableAlternateInputs
	bool ___EnableAlternateInputs_23;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::ShowNormalLoopAnimation
	bool ___ShowNormalLoopAnimation_24;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::ShowOnButtonDeselected
	bool ___ShowOnButtonDeselected_25;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::ShowOnButtonSelected
	bool ___ShowOnButtonSelected_26;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::ShowOnClick
	bool ___ShowOnClick_27;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::ShowOnDoubleClick
	bool ___ShowOnDoubleClick_28;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::ShowOnLongClick
	bool ___ShowOnLongClick_29;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::ShowOnPointerDown
	bool ___ShowOnPointerDown_30;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::ShowOnPointerEnter
	bool ___ShowOnPointerEnter_31;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::ShowOnPointerExit
	bool ___ShowOnPointerExit_32;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::ShowOnPointerUp
	bool ___ShowOnPointerUp_33;
	// System.Boolean Doozy.Engine.UI.Settings.UIButtonSettings::ShowSelectedLoopAnimation
	bool ___ShowSelectedLoopAnimation_34;
	// System.Single Doozy.Engine.UI.Settings.UIButtonSettings::DisableButtonBetweenClicksInterval
	float ___DisableButtonBetweenClicksInterval_35;
	// System.String Doozy.Engine.UI.Settings.UIButtonSettings::RenamePrefix
	String_t* ___RenamePrefix_36;
	// System.String Doozy.Engine.UI.Settings.UIButtonSettings::RenameSuffix
	String_t* ___RenameSuffix_37;
	// System.String Doozy.Engine.UI.Settings.UIButtonSettings::VirtualButtonName
	String_t* ___VirtualButtonName_38;
	// System.String Doozy.Engine.UI.Settings.UIButtonSettings::VirtualButtonNameAlt
	String_t* ___VirtualButtonNameAlt_39;

public:
	inline static int32_t get_offset_of_database_6() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___database_6)); }
	inline NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D * get_database_6() const { return ___database_6; }
	inline NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D ** get_address_of_database_6() { return &___database_6; }
	inline void set_database_6(NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D * value)
	{
		___database_6 = value;
		Il2CppCodeGenWriteBarrier((&___database_6), value);
	}

	inline static int32_t get_offset_of_InputMode_17() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___InputMode_17)); }
	inline int32_t get_InputMode_17() const { return ___InputMode_17; }
	inline int32_t* get_address_of_InputMode_17() { return &___InputMode_17; }
	inline void set_InputMode_17(int32_t value)
	{
		___InputMode_17 = value;
	}

	inline static int32_t get_offset_of_KeyCode_18() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___KeyCode_18)); }
	inline int32_t get_KeyCode_18() const { return ___KeyCode_18; }
	inline int32_t* get_address_of_KeyCode_18() { return &___KeyCode_18; }
	inline void set_KeyCode_18(int32_t value)
	{
		___KeyCode_18 = value;
	}

	inline static int32_t get_offset_of_KeyCodeAlt_19() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___KeyCodeAlt_19)); }
	inline int32_t get_KeyCodeAlt_19() const { return ___KeyCodeAlt_19; }
	inline int32_t* get_address_of_KeyCodeAlt_19() { return &___KeyCodeAlt_19; }
	inline void set_KeyCodeAlt_19(int32_t value)
	{
		___KeyCodeAlt_19 = value;
	}

	inline static int32_t get_offset_of_ClickMode_20() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___ClickMode_20)); }
	inline int32_t get_ClickMode_20() const { return ___ClickMode_20; }
	inline int32_t* get_address_of_ClickMode_20() { return &___ClickMode_20; }
	inline void set_ClickMode_20(int32_t value)
	{
		___ClickMode_20 = value;
	}

	inline static int32_t get_offset_of_AllowMultipleClicks_21() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___AllowMultipleClicks_21)); }
	inline bool get_AllowMultipleClicks_21() const { return ___AllowMultipleClicks_21; }
	inline bool* get_address_of_AllowMultipleClicks_21() { return &___AllowMultipleClicks_21; }
	inline void set_AllowMultipleClicks_21(bool value)
	{
		___AllowMultipleClicks_21 = value;
	}

	inline static int32_t get_offset_of_DeselectButtonAfterClick_22() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___DeselectButtonAfterClick_22)); }
	inline bool get_DeselectButtonAfterClick_22() const { return ___DeselectButtonAfterClick_22; }
	inline bool* get_address_of_DeselectButtonAfterClick_22() { return &___DeselectButtonAfterClick_22; }
	inline void set_DeselectButtonAfterClick_22(bool value)
	{
		___DeselectButtonAfterClick_22 = value;
	}

	inline static int32_t get_offset_of_EnableAlternateInputs_23() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___EnableAlternateInputs_23)); }
	inline bool get_EnableAlternateInputs_23() const { return ___EnableAlternateInputs_23; }
	inline bool* get_address_of_EnableAlternateInputs_23() { return &___EnableAlternateInputs_23; }
	inline void set_EnableAlternateInputs_23(bool value)
	{
		___EnableAlternateInputs_23 = value;
	}

	inline static int32_t get_offset_of_ShowNormalLoopAnimation_24() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___ShowNormalLoopAnimation_24)); }
	inline bool get_ShowNormalLoopAnimation_24() const { return ___ShowNormalLoopAnimation_24; }
	inline bool* get_address_of_ShowNormalLoopAnimation_24() { return &___ShowNormalLoopAnimation_24; }
	inline void set_ShowNormalLoopAnimation_24(bool value)
	{
		___ShowNormalLoopAnimation_24 = value;
	}

	inline static int32_t get_offset_of_ShowOnButtonDeselected_25() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___ShowOnButtonDeselected_25)); }
	inline bool get_ShowOnButtonDeselected_25() const { return ___ShowOnButtonDeselected_25; }
	inline bool* get_address_of_ShowOnButtonDeselected_25() { return &___ShowOnButtonDeselected_25; }
	inline void set_ShowOnButtonDeselected_25(bool value)
	{
		___ShowOnButtonDeselected_25 = value;
	}

	inline static int32_t get_offset_of_ShowOnButtonSelected_26() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___ShowOnButtonSelected_26)); }
	inline bool get_ShowOnButtonSelected_26() const { return ___ShowOnButtonSelected_26; }
	inline bool* get_address_of_ShowOnButtonSelected_26() { return &___ShowOnButtonSelected_26; }
	inline void set_ShowOnButtonSelected_26(bool value)
	{
		___ShowOnButtonSelected_26 = value;
	}

	inline static int32_t get_offset_of_ShowOnClick_27() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___ShowOnClick_27)); }
	inline bool get_ShowOnClick_27() const { return ___ShowOnClick_27; }
	inline bool* get_address_of_ShowOnClick_27() { return &___ShowOnClick_27; }
	inline void set_ShowOnClick_27(bool value)
	{
		___ShowOnClick_27 = value;
	}

	inline static int32_t get_offset_of_ShowOnDoubleClick_28() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___ShowOnDoubleClick_28)); }
	inline bool get_ShowOnDoubleClick_28() const { return ___ShowOnDoubleClick_28; }
	inline bool* get_address_of_ShowOnDoubleClick_28() { return &___ShowOnDoubleClick_28; }
	inline void set_ShowOnDoubleClick_28(bool value)
	{
		___ShowOnDoubleClick_28 = value;
	}

	inline static int32_t get_offset_of_ShowOnLongClick_29() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___ShowOnLongClick_29)); }
	inline bool get_ShowOnLongClick_29() const { return ___ShowOnLongClick_29; }
	inline bool* get_address_of_ShowOnLongClick_29() { return &___ShowOnLongClick_29; }
	inline void set_ShowOnLongClick_29(bool value)
	{
		___ShowOnLongClick_29 = value;
	}

	inline static int32_t get_offset_of_ShowOnPointerDown_30() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___ShowOnPointerDown_30)); }
	inline bool get_ShowOnPointerDown_30() const { return ___ShowOnPointerDown_30; }
	inline bool* get_address_of_ShowOnPointerDown_30() { return &___ShowOnPointerDown_30; }
	inline void set_ShowOnPointerDown_30(bool value)
	{
		___ShowOnPointerDown_30 = value;
	}

	inline static int32_t get_offset_of_ShowOnPointerEnter_31() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___ShowOnPointerEnter_31)); }
	inline bool get_ShowOnPointerEnter_31() const { return ___ShowOnPointerEnter_31; }
	inline bool* get_address_of_ShowOnPointerEnter_31() { return &___ShowOnPointerEnter_31; }
	inline void set_ShowOnPointerEnter_31(bool value)
	{
		___ShowOnPointerEnter_31 = value;
	}

	inline static int32_t get_offset_of_ShowOnPointerExit_32() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___ShowOnPointerExit_32)); }
	inline bool get_ShowOnPointerExit_32() const { return ___ShowOnPointerExit_32; }
	inline bool* get_address_of_ShowOnPointerExit_32() { return &___ShowOnPointerExit_32; }
	inline void set_ShowOnPointerExit_32(bool value)
	{
		___ShowOnPointerExit_32 = value;
	}

	inline static int32_t get_offset_of_ShowOnPointerUp_33() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___ShowOnPointerUp_33)); }
	inline bool get_ShowOnPointerUp_33() const { return ___ShowOnPointerUp_33; }
	inline bool* get_address_of_ShowOnPointerUp_33() { return &___ShowOnPointerUp_33; }
	inline void set_ShowOnPointerUp_33(bool value)
	{
		___ShowOnPointerUp_33 = value;
	}

	inline static int32_t get_offset_of_ShowSelectedLoopAnimation_34() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___ShowSelectedLoopAnimation_34)); }
	inline bool get_ShowSelectedLoopAnimation_34() const { return ___ShowSelectedLoopAnimation_34; }
	inline bool* get_address_of_ShowSelectedLoopAnimation_34() { return &___ShowSelectedLoopAnimation_34; }
	inline void set_ShowSelectedLoopAnimation_34(bool value)
	{
		___ShowSelectedLoopAnimation_34 = value;
	}

	inline static int32_t get_offset_of_DisableButtonBetweenClicksInterval_35() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___DisableButtonBetweenClicksInterval_35)); }
	inline float get_DisableButtonBetweenClicksInterval_35() const { return ___DisableButtonBetweenClicksInterval_35; }
	inline float* get_address_of_DisableButtonBetweenClicksInterval_35() { return &___DisableButtonBetweenClicksInterval_35; }
	inline void set_DisableButtonBetweenClicksInterval_35(float value)
	{
		___DisableButtonBetweenClicksInterval_35 = value;
	}

	inline static int32_t get_offset_of_RenamePrefix_36() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___RenamePrefix_36)); }
	inline String_t* get_RenamePrefix_36() const { return ___RenamePrefix_36; }
	inline String_t** get_address_of_RenamePrefix_36() { return &___RenamePrefix_36; }
	inline void set_RenamePrefix_36(String_t* value)
	{
		___RenamePrefix_36 = value;
		Il2CppCodeGenWriteBarrier((&___RenamePrefix_36), value);
	}

	inline static int32_t get_offset_of_RenameSuffix_37() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___RenameSuffix_37)); }
	inline String_t* get_RenameSuffix_37() const { return ___RenameSuffix_37; }
	inline String_t** get_address_of_RenameSuffix_37() { return &___RenameSuffix_37; }
	inline void set_RenameSuffix_37(String_t* value)
	{
		___RenameSuffix_37 = value;
		Il2CppCodeGenWriteBarrier((&___RenameSuffix_37), value);
	}

	inline static int32_t get_offset_of_VirtualButtonName_38() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___VirtualButtonName_38)); }
	inline String_t* get_VirtualButtonName_38() const { return ___VirtualButtonName_38; }
	inline String_t** get_address_of_VirtualButtonName_38() { return &___VirtualButtonName_38; }
	inline void set_VirtualButtonName_38(String_t* value)
	{
		___VirtualButtonName_38 = value;
		Il2CppCodeGenWriteBarrier((&___VirtualButtonName_38), value);
	}

	inline static int32_t get_offset_of_VirtualButtonNameAlt_39() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A, ___VirtualButtonNameAlt_39)); }
	inline String_t* get_VirtualButtonNameAlt_39() const { return ___VirtualButtonNameAlt_39; }
	inline String_t** get_address_of_VirtualButtonNameAlt_39() { return &___VirtualButtonNameAlt_39; }
	inline void set_VirtualButtonNameAlt_39(String_t* value)
	{
		___VirtualButtonNameAlt_39 = value;
		Il2CppCodeGenWriteBarrier((&___VirtualButtonNameAlt_39), value);
	}
};

struct UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A_StaticFields
{
public:
	// Doozy.Engine.UI.Settings.UIButtonSettings Doozy.Engine.UI.Settings.UIButtonSettings::s_instance
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A * ___s_instance_5;

public:
	inline static int32_t get_offset_of_s_instance_5() { return static_cast<int32_t>(offsetof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A_StaticFields, ___s_instance_5)); }
	inline UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A * get_s_instance_5() const { return ___s_instance_5; }
	inline UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A ** get_address_of_s_instance_5() { return &___s_instance_5; }
	inline void set_s_instance_5(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A * value)
	{
		___s_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONSETTINGS_TF1C628E7B5F15F250AC021A5B7E92CE91096659A_H
#ifndef UICANVASSETTINGS_T64ABCAFE0349BD9BD11818C0E2A95401234771E4_H
#define UICANVASSETTINGS_T64ABCAFE0349BD9BD11818C0E2A95401234771E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Settings.UICanvasSettings
struct  UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Doozy.Engine.UI.Base.NamesDatabase Doozy.Engine.UI.Settings.UICanvasSettings::database
	NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D * ___database_6;
	// System.Boolean Doozy.Engine.UI.Settings.UICanvasSettings::DontDestroyCanvasOnLoad
	bool ___DontDestroyCanvasOnLoad_10;
	// System.String Doozy.Engine.UI.Settings.UICanvasSettings::RenamePrefix
	String_t* ___RenamePrefix_11;
	// System.String Doozy.Engine.UI.Settings.UICanvasSettings::RenameSuffix
	String_t* ___RenameSuffix_12;

public:
	inline static int32_t get_offset_of_database_6() { return static_cast<int32_t>(offsetof(UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4, ___database_6)); }
	inline NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D * get_database_6() const { return ___database_6; }
	inline NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D ** get_address_of_database_6() { return &___database_6; }
	inline void set_database_6(NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D * value)
	{
		___database_6 = value;
		Il2CppCodeGenWriteBarrier((&___database_6), value);
	}

	inline static int32_t get_offset_of_DontDestroyCanvasOnLoad_10() { return static_cast<int32_t>(offsetof(UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4, ___DontDestroyCanvasOnLoad_10)); }
	inline bool get_DontDestroyCanvasOnLoad_10() const { return ___DontDestroyCanvasOnLoad_10; }
	inline bool* get_address_of_DontDestroyCanvasOnLoad_10() { return &___DontDestroyCanvasOnLoad_10; }
	inline void set_DontDestroyCanvasOnLoad_10(bool value)
	{
		___DontDestroyCanvasOnLoad_10 = value;
	}

	inline static int32_t get_offset_of_RenamePrefix_11() { return static_cast<int32_t>(offsetof(UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4, ___RenamePrefix_11)); }
	inline String_t* get_RenamePrefix_11() const { return ___RenamePrefix_11; }
	inline String_t** get_address_of_RenamePrefix_11() { return &___RenamePrefix_11; }
	inline void set_RenamePrefix_11(String_t* value)
	{
		___RenamePrefix_11 = value;
		Il2CppCodeGenWriteBarrier((&___RenamePrefix_11), value);
	}

	inline static int32_t get_offset_of_RenameSuffix_12() { return static_cast<int32_t>(offsetof(UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4, ___RenameSuffix_12)); }
	inline String_t* get_RenameSuffix_12() const { return ___RenameSuffix_12; }
	inline String_t** get_address_of_RenameSuffix_12() { return &___RenameSuffix_12; }
	inline void set_RenameSuffix_12(String_t* value)
	{
		___RenameSuffix_12 = value;
		Il2CppCodeGenWriteBarrier((&___RenameSuffix_12), value);
	}
};

struct UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4_StaticFields
{
public:
	// Doozy.Engine.UI.Settings.UICanvasSettings Doozy.Engine.UI.Settings.UICanvasSettings::s_instance
	UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4 * ___s_instance_5;

public:
	inline static int32_t get_offset_of_s_instance_5() { return static_cast<int32_t>(offsetof(UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4_StaticFields, ___s_instance_5)); }
	inline UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4 * get_s_instance_5() const { return ___s_instance_5; }
	inline UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4 ** get_address_of_s_instance_5() { return &___s_instance_5; }
	inline void set_s_instance_5(UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4 * value)
	{
		___s_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICANVASSETTINGS_T64ABCAFE0349BD9BD11818C0E2A95401234771E4_H
#ifndef UIDRAWERSETTINGS_TBE608F9D9B3C0450D2F9B6F76D201C581628B50F_H
#define UIDRAWERSETTINGS_TBE608F9D9B3C0450D2F9B6F76D201C581628B50F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Settings.UIDrawerSettings
struct  UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Doozy.Engine.UI.Base.NamesDatabase Doozy.Engine.UI.Settings.UIDrawerSettings::database
	NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D * ___database_6;
	// Doozy.Engine.Touchy.SimpleSwipe Doozy.Engine.UI.Settings.UIDrawerSettings::CloseDirection
	int32_t ___CloseDirection_15;
	// UnityEngine.Vector3 Doozy.Engine.UI.Settings.UIDrawerSettings::CustomStartAnchoredPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___CustomStartAnchoredPosition_16;
	// System.Boolean Doozy.Engine.UI.Settings.UIDrawerSettings::DetectGestures
	bool ___DetectGestures_17;
	// System.Boolean Doozy.Engine.UI.Settings.UIDrawerSettings::UseCustomStartAnchoredPosition
	bool ___UseCustomStartAnchoredPosition_18;
	// System.Single Doozy.Engine.UI.Settings.UIDrawerSettings::CloseSpeed
	float ___CloseSpeed_19;
	// System.Single Doozy.Engine.UI.Settings.UIDrawerSettings::OpenSpeed
	float ___OpenSpeed_20;
	// System.String Doozy.Engine.UI.Settings.UIDrawerSettings::RenamePrefix
	String_t* ___RenamePrefix_21;
	// System.String Doozy.Engine.UI.Settings.UIDrawerSettings::RenameSuffix
	String_t* ___RenameSuffix_22;

public:
	inline static int32_t get_offset_of_database_6() { return static_cast<int32_t>(offsetof(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F, ___database_6)); }
	inline NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D * get_database_6() const { return ___database_6; }
	inline NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D ** get_address_of_database_6() { return &___database_6; }
	inline void set_database_6(NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D * value)
	{
		___database_6 = value;
		Il2CppCodeGenWriteBarrier((&___database_6), value);
	}

	inline static int32_t get_offset_of_CloseDirection_15() { return static_cast<int32_t>(offsetof(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F, ___CloseDirection_15)); }
	inline int32_t get_CloseDirection_15() const { return ___CloseDirection_15; }
	inline int32_t* get_address_of_CloseDirection_15() { return &___CloseDirection_15; }
	inline void set_CloseDirection_15(int32_t value)
	{
		___CloseDirection_15 = value;
	}

	inline static int32_t get_offset_of_CustomStartAnchoredPosition_16() { return static_cast<int32_t>(offsetof(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F, ___CustomStartAnchoredPosition_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_CustomStartAnchoredPosition_16() const { return ___CustomStartAnchoredPosition_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_CustomStartAnchoredPosition_16() { return &___CustomStartAnchoredPosition_16; }
	inline void set_CustomStartAnchoredPosition_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___CustomStartAnchoredPosition_16 = value;
	}

	inline static int32_t get_offset_of_DetectGestures_17() { return static_cast<int32_t>(offsetof(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F, ___DetectGestures_17)); }
	inline bool get_DetectGestures_17() const { return ___DetectGestures_17; }
	inline bool* get_address_of_DetectGestures_17() { return &___DetectGestures_17; }
	inline void set_DetectGestures_17(bool value)
	{
		___DetectGestures_17 = value;
	}

	inline static int32_t get_offset_of_UseCustomStartAnchoredPosition_18() { return static_cast<int32_t>(offsetof(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F, ___UseCustomStartAnchoredPosition_18)); }
	inline bool get_UseCustomStartAnchoredPosition_18() const { return ___UseCustomStartAnchoredPosition_18; }
	inline bool* get_address_of_UseCustomStartAnchoredPosition_18() { return &___UseCustomStartAnchoredPosition_18; }
	inline void set_UseCustomStartAnchoredPosition_18(bool value)
	{
		___UseCustomStartAnchoredPosition_18 = value;
	}

	inline static int32_t get_offset_of_CloseSpeed_19() { return static_cast<int32_t>(offsetof(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F, ___CloseSpeed_19)); }
	inline float get_CloseSpeed_19() const { return ___CloseSpeed_19; }
	inline float* get_address_of_CloseSpeed_19() { return &___CloseSpeed_19; }
	inline void set_CloseSpeed_19(float value)
	{
		___CloseSpeed_19 = value;
	}

	inline static int32_t get_offset_of_OpenSpeed_20() { return static_cast<int32_t>(offsetof(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F, ___OpenSpeed_20)); }
	inline float get_OpenSpeed_20() const { return ___OpenSpeed_20; }
	inline float* get_address_of_OpenSpeed_20() { return &___OpenSpeed_20; }
	inline void set_OpenSpeed_20(float value)
	{
		___OpenSpeed_20 = value;
	}

	inline static int32_t get_offset_of_RenamePrefix_21() { return static_cast<int32_t>(offsetof(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F, ___RenamePrefix_21)); }
	inline String_t* get_RenamePrefix_21() const { return ___RenamePrefix_21; }
	inline String_t** get_address_of_RenamePrefix_21() { return &___RenamePrefix_21; }
	inline void set_RenamePrefix_21(String_t* value)
	{
		___RenamePrefix_21 = value;
		Il2CppCodeGenWriteBarrier((&___RenamePrefix_21), value);
	}

	inline static int32_t get_offset_of_RenameSuffix_22() { return static_cast<int32_t>(offsetof(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F, ___RenameSuffix_22)); }
	inline String_t* get_RenameSuffix_22() const { return ___RenameSuffix_22; }
	inline String_t** get_address_of_RenameSuffix_22() { return &___RenameSuffix_22; }
	inline void set_RenameSuffix_22(String_t* value)
	{
		___RenameSuffix_22 = value;
		Il2CppCodeGenWriteBarrier((&___RenameSuffix_22), value);
	}
};

struct UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F_StaticFields
{
public:
	// Doozy.Engine.UI.Settings.UIDrawerSettings Doozy.Engine.UI.Settings.UIDrawerSettings::s_instance
	UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F * ___s_instance_5;
	// UnityEngine.Vector3 Doozy.Engine.UI.Settings.UIDrawerSettings::CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_14;

public:
	inline static int32_t get_offset_of_s_instance_5() { return static_cast<int32_t>(offsetof(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F_StaticFields, ___s_instance_5)); }
	inline UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F * get_s_instance_5() const { return ___s_instance_5; }
	inline UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F ** get_address_of_s_instance_5() { return &___s_instance_5; }
	inline void set_s_instance_5(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F * value)
	{
		___s_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_5), value);
	}

	inline static int32_t get_offset_of_CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_14() { return static_cast<int32_t>(offsetof(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F_StaticFields, ___CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_14() const { return ___CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_14() { return &___CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_14; }
	inline void set_CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWERSETTINGS_TBE608F9D9B3C0450D2F9B6F76D201C581628B50F_H
#ifndef UIPOPUPSETTINGS_T4565021723312AC24CAD09E7ECB40A0E6893EA3A_H
#define UIPOPUPSETTINGS_T4565021723312AC24CAD09E7ECB40A0E6893EA3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Settings.UIPopupSettings
struct  UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Doozy.Engine.UI.UIPopupDatabase Doozy.Engine.UI.Settings.UIPopupSettings::database
	UIPopupDatabase_t2DD34A70147D17B8FB3EE2E8F0C95131852AD382 * ___database_6;
	// Doozy.Engine.UI.PopupDisplayOn Doozy.Engine.UI.Settings.UIPopupSettings::DisplayTarget
	int32_t ___DisplayTarget_23;
	// System.Boolean Doozy.Engine.UI.Settings.UIPopupSettings::AddToPopupQueue
	bool ___AddToPopupQueue_24;
	// System.Boolean Doozy.Engine.UI.Settings.UIPopupSettings::AutoHideAfterShow
	bool ___AutoHideAfterShow_25;
	// System.Boolean Doozy.Engine.UI.Settings.UIPopupSettings::AutoSelectButtonAfterShow
	bool ___AutoSelectButtonAfterShow_26;
	// System.Boolean Doozy.Engine.UI.Settings.UIPopupSettings::CustomCanvasName
	bool ___CustomCanvasName_27;
	// System.Boolean Doozy.Engine.UI.Settings.UIPopupSettings::DestroyAfterHide
	bool ___DestroyAfterHide_28;
	// System.Boolean Doozy.Engine.UI.Settings.UIPopupSettings::HideOnBackButton
	bool ___HideOnBackButton_29;
	// System.Boolean Doozy.Engine.UI.Settings.UIPopupSettings::HideOnClickAnywhere
	bool ___HideOnClickAnywhere_30;
	// System.Boolean Doozy.Engine.UI.Settings.UIPopupSettings::HideOnClickContainer
	bool ___HideOnClickContainer_31;
	// System.Boolean Doozy.Engine.UI.Settings.UIPopupSettings::HideOnClickOverlay
	bool ___HideOnClickOverlay_32;
	// System.Boolean Doozy.Engine.UI.Settings.UIPopupSettings::UpdateHideProgressorOnShow
	bool ___UpdateHideProgressorOnShow_33;
	// System.Boolean Doozy.Engine.UI.Settings.UIPopupSettings::UpdateShowProgressorOnHide
	bool ___UpdateShowProgressorOnHide_34;
	// System.Boolean Doozy.Engine.UI.Settings.UIPopupSettings::UseOverlay
	bool ___UseOverlay_35;
	// System.Single Doozy.Engine.UI.Settings.UIPopupSettings::AutoHideAfterShowDelay
	float ___AutoHideAfterShowDelay_36;
	// System.String Doozy.Engine.UI.Settings.UIPopupSettings::CanvasName
	String_t* ___CanvasName_37;

public:
	inline static int32_t get_offset_of_database_6() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___database_6)); }
	inline UIPopupDatabase_t2DD34A70147D17B8FB3EE2E8F0C95131852AD382 * get_database_6() const { return ___database_6; }
	inline UIPopupDatabase_t2DD34A70147D17B8FB3EE2E8F0C95131852AD382 ** get_address_of_database_6() { return &___database_6; }
	inline void set_database_6(UIPopupDatabase_t2DD34A70147D17B8FB3EE2E8F0C95131852AD382 * value)
	{
		___database_6 = value;
		Il2CppCodeGenWriteBarrier((&___database_6), value);
	}

	inline static int32_t get_offset_of_DisplayTarget_23() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___DisplayTarget_23)); }
	inline int32_t get_DisplayTarget_23() const { return ___DisplayTarget_23; }
	inline int32_t* get_address_of_DisplayTarget_23() { return &___DisplayTarget_23; }
	inline void set_DisplayTarget_23(int32_t value)
	{
		___DisplayTarget_23 = value;
	}

	inline static int32_t get_offset_of_AddToPopupQueue_24() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___AddToPopupQueue_24)); }
	inline bool get_AddToPopupQueue_24() const { return ___AddToPopupQueue_24; }
	inline bool* get_address_of_AddToPopupQueue_24() { return &___AddToPopupQueue_24; }
	inline void set_AddToPopupQueue_24(bool value)
	{
		___AddToPopupQueue_24 = value;
	}

	inline static int32_t get_offset_of_AutoHideAfterShow_25() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___AutoHideAfterShow_25)); }
	inline bool get_AutoHideAfterShow_25() const { return ___AutoHideAfterShow_25; }
	inline bool* get_address_of_AutoHideAfterShow_25() { return &___AutoHideAfterShow_25; }
	inline void set_AutoHideAfterShow_25(bool value)
	{
		___AutoHideAfterShow_25 = value;
	}

	inline static int32_t get_offset_of_AutoSelectButtonAfterShow_26() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___AutoSelectButtonAfterShow_26)); }
	inline bool get_AutoSelectButtonAfterShow_26() const { return ___AutoSelectButtonAfterShow_26; }
	inline bool* get_address_of_AutoSelectButtonAfterShow_26() { return &___AutoSelectButtonAfterShow_26; }
	inline void set_AutoSelectButtonAfterShow_26(bool value)
	{
		___AutoSelectButtonAfterShow_26 = value;
	}

	inline static int32_t get_offset_of_CustomCanvasName_27() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___CustomCanvasName_27)); }
	inline bool get_CustomCanvasName_27() const { return ___CustomCanvasName_27; }
	inline bool* get_address_of_CustomCanvasName_27() { return &___CustomCanvasName_27; }
	inline void set_CustomCanvasName_27(bool value)
	{
		___CustomCanvasName_27 = value;
	}

	inline static int32_t get_offset_of_DestroyAfterHide_28() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___DestroyAfterHide_28)); }
	inline bool get_DestroyAfterHide_28() const { return ___DestroyAfterHide_28; }
	inline bool* get_address_of_DestroyAfterHide_28() { return &___DestroyAfterHide_28; }
	inline void set_DestroyAfterHide_28(bool value)
	{
		___DestroyAfterHide_28 = value;
	}

	inline static int32_t get_offset_of_HideOnBackButton_29() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___HideOnBackButton_29)); }
	inline bool get_HideOnBackButton_29() const { return ___HideOnBackButton_29; }
	inline bool* get_address_of_HideOnBackButton_29() { return &___HideOnBackButton_29; }
	inline void set_HideOnBackButton_29(bool value)
	{
		___HideOnBackButton_29 = value;
	}

	inline static int32_t get_offset_of_HideOnClickAnywhere_30() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___HideOnClickAnywhere_30)); }
	inline bool get_HideOnClickAnywhere_30() const { return ___HideOnClickAnywhere_30; }
	inline bool* get_address_of_HideOnClickAnywhere_30() { return &___HideOnClickAnywhere_30; }
	inline void set_HideOnClickAnywhere_30(bool value)
	{
		___HideOnClickAnywhere_30 = value;
	}

	inline static int32_t get_offset_of_HideOnClickContainer_31() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___HideOnClickContainer_31)); }
	inline bool get_HideOnClickContainer_31() const { return ___HideOnClickContainer_31; }
	inline bool* get_address_of_HideOnClickContainer_31() { return &___HideOnClickContainer_31; }
	inline void set_HideOnClickContainer_31(bool value)
	{
		___HideOnClickContainer_31 = value;
	}

	inline static int32_t get_offset_of_HideOnClickOverlay_32() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___HideOnClickOverlay_32)); }
	inline bool get_HideOnClickOverlay_32() const { return ___HideOnClickOverlay_32; }
	inline bool* get_address_of_HideOnClickOverlay_32() { return &___HideOnClickOverlay_32; }
	inline void set_HideOnClickOverlay_32(bool value)
	{
		___HideOnClickOverlay_32 = value;
	}

	inline static int32_t get_offset_of_UpdateHideProgressorOnShow_33() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___UpdateHideProgressorOnShow_33)); }
	inline bool get_UpdateHideProgressorOnShow_33() const { return ___UpdateHideProgressorOnShow_33; }
	inline bool* get_address_of_UpdateHideProgressorOnShow_33() { return &___UpdateHideProgressorOnShow_33; }
	inline void set_UpdateHideProgressorOnShow_33(bool value)
	{
		___UpdateHideProgressorOnShow_33 = value;
	}

	inline static int32_t get_offset_of_UpdateShowProgressorOnHide_34() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___UpdateShowProgressorOnHide_34)); }
	inline bool get_UpdateShowProgressorOnHide_34() const { return ___UpdateShowProgressorOnHide_34; }
	inline bool* get_address_of_UpdateShowProgressorOnHide_34() { return &___UpdateShowProgressorOnHide_34; }
	inline void set_UpdateShowProgressorOnHide_34(bool value)
	{
		___UpdateShowProgressorOnHide_34 = value;
	}

	inline static int32_t get_offset_of_UseOverlay_35() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___UseOverlay_35)); }
	inline bool get_UseOverlay_35() const { return ___UseOverlay_35; }
	inline bool* get_address_of_UseOverlay_35() { return &___UseOverlay_35; }
	inline void set_UseOverlay_35(bool value)
	{
		___UseOverlay_35 = value;
	}

	inline static int32_t get_offset_of_AutoHideAfterShowDelay_36() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___AutoHideAfterShowDelay_36)); }
	inline float get_AutoHideAfterShowDelay_36() const { return ___AutoHideAfterShowDelay_36; }
	inline float* get_address_of_AutoHideAfterShowDelay_36() { return &___AutoHideAfterShowDelay_36; }
	inline void set_AutoHideAfterShowDelay_36(float value)
	{
		___AutoHideAfterShowDelay_36 = value;
	}

	inline static int32_t get_offset_of_CanvasName_37() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A, ___CanvasName_37)); }
	inline String_t* get_CanvasName_37() const { return ___CanvasName_37; }
	inline String_t** get_address_of_CanvasName_37() { return &___CanvasName_37; }
	inline void set_CanvasName_37(String_t* value)
	{
		___CanvasName_37 = value;
		Il2CppCodeGenWriteBarrier((&___CanvasName_37), value);
	}
};

struct UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A_StaticFields
{
public:
	// Doozy.Engine.UI.Settings.UIPopupSettings Doozy.Engine.UI.Settings.UIPopupSettings::s_instance
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A * ___s_instance_5;

public:
	inline static int32_t get_offset_of_s_instance_5() { return static_cast<int32_t>(offsetof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A_StaticFields, ___s_instance_5)); }
	inline UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A * get_s_instance_5() const { return ___s_instance_5; }
	inline UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A ** get_address_of_s_instance_5() { return &___s_instance_5; }
	inline void set_s_instance_5(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A * value)
	{
		___s_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOPUPSETTINGS_T4565021723312AC24CAD09E7ECB40A0E6893EA3A_H
#ifndef UITOGGLESETTINGS_T299D4343576C3294462B1CF2739E727DFD39E984_H
#define UITOGGLESETTINGS_T299D4343576C3294462B1CF2739E727DFD39E984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Settings.UIToggleSettings
struct  UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Doozy.Engine.UI.Input.InputMode Doozy.Engine.UI.Settings.UIToggleSettings::InputMode
	int32_t ___InputMode_11;
	// UnityEngine.KeyCode Doozy.Engine.UI.Settings.UIToggleSettings::KeyCode
	int32_t ___KeyCode_12;
	// UnityEngine.KeyCode Doozy.Engine.UI.Settings.UIToggleSettings::KeyCodeAlt
	int32_t ___KeyCodeAlt_13;
	// System.Boolean Doozy.Engine.UI.Settings.UIToggleSettings::AllowMultipleClicks
	bool ___AllowMultipleClicks_14;
	// System.Boolean Doozy.Engine.UI.Settings.UIToggleSettings::DeselectButtonAfterClick
	bool ___DeselectButtonAfterClick_15;
	// System.Boolean Doozy.Engine.UI.Settings.UIToggleSettings::EnableAlternateInputs
	bool ___EnableAlternateInputs_16;
	// System.Boolean Doozy.Engine.UI.Settings.UIToggleSettings::ShowOnButtonDeselected
	bool ___ShowOnButtonDeselected_17;
	// System.Boolean Doozy.Engine.UI.Settings.UIToggleSettings::ShowOnButtonSelected
	bool ___ShowOnButtonSelected_18;
	// System.Boolean Doozy.Engine.UI.Settings.UIToggleSettings::ShowOnClick
	bool ___ShowOnClick_19;
	// System.Boolean Doozy.Engine.UI.Settings.UIToggleSettings::ShowOnPointerEnter
	bool ___ShowOnPointerEnter_20;
	// System.Boolean Doozy.Engine.UI.Settings.UIToggleSettings::ShowOnPointerExit
	bool ___ShowOnPointerExit_21;
	// System.Single Doozy.Engine.UI.Settings.UIToggleSettings::DisableButtonBetweenClicksInterval
	float ___DisableButtonBetweenClicksInterval_22;
	// System.String Doozy.Engine.UI.Settings.UIToggleSettings::VirtualButtonName
	String_t* ___VirtualButtonName_23;
	// System.String Doozy.Engine.UI.Settings.UIToggleSettings::VirtualButtonNameAlt
	String_t* ___VirtualButtonNameAlt_24;

public:
	inline static int32_t get_offset_of_InputMode_11() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___InputMode_11)); }
	inline int32_t get_InputMode_11() const { return ___InputMode_11; }
	inline int32_t* get_address_of_InputMode_11() { return &___InputMode_11; }
	inline void set_InputMode_11(int32_t value)
	{
		___InputMode_11 = value;
	}

	inline static int32_t get_offset_of_KeyCode_12() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___KeyCode_12)); }
	inline int32_t get_KeyCode_12() const { return ___KeyCode_12; }
	inline int32_t* get_address_of_KeyCode_12() { return &___KeyCode_12; }
	inline void set_KeyCode_12(int32_t value)
	{
		___KeyCode_12 = value;
	}

	inline static int32_t get_offset_of_KeyCodeAlt_13() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___KeyCodeAlt_13)); }
	inline int32_t get_KeyCodeAlt_13() const { return ___KeyCodeAlt_13; }
	inline int32_t* get_address_of_KeyCodeAlt_13() { return &___KeyCodeAlt_13; }
	inline void set_KeyCodeAlt_13(int32_t value)
	{
		___KeyCodeAlt_13 = value;
	}

	inline static int32_t get_offset_of_AllowMultipleClicks_14() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___AllowMultipleClicks_14)); }
	inline bool get_AllowMultipleClicks_14() const { return ___AllowMultipleClicks_14; }
	inline bool* get_address_of_AllowMultipleClicks_14() { return &___AllowMultipleClicks_14; }
	inline void set_AllowMultipleClicks_14(bool value)
	{
		___AllowMultipleClicks_14 = value;
	}

	inline static int32_t get_offset_of_DeselectButtonAfterClick_15() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___DeselectButtonAfterClick_15)); }
	inline bool get_DeselectButtonAfterClick_15() const { return ___DeselectButtonAfterClick_15; }
	inline bool* get_address_of_DeselectButtonAfterClick_15() { return &___DeselectButtonAfterClick_15; }
	inline void set_DeselectButtonAfterClick_15(bool value)
	{
		___DeselectButtonAfterClick_15 = value;
	}

	inline static int32_t get_offset_of_EnableAlternateInputs_16() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___EnableAlternateInputs_16)); }
	inline bool get_EnableAlternateInputs_16() const { return ___EnableAlternateInputs_16; }
	inline bool* get_address_of_EnableAlternateInputs_16() { return &___EnableAlternateInputs_16; }
	inline void set_EnableAlternateInputs_16(bool value)
	{
		___EnableAlternateInputs_16 = value;
	}

	inline static int32_t get_offset_of_ShowOnButtonDeselected_17() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___ShowOnButtonDeselected_17)); }
	inline bool get_ShowOnButtonDeselected_17() const { return ___ShowOnButtonDeselected_17; }
	inline bool* get_address_of_ShowOnButtonDeselected_17() { return &___ShowOnButtonDeselected_17; }
	inline void set_ShowOnButtonDeselected_17(bool value)
	{
		___ShowOnButtonDeselected_17 = value;
	}

	inline static int32_t get_offset_of_ShowOnButtonSelected_18() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___ShowOnButtonSelected_18)); }
	inline bool get_ShowOnButtonSelected_18() const { return ___ShowOnButtonSelected_18; }
	inline bool* get_address_of_ShowOnButtonSelected_18() { return &___ShowOnButtonSelected_18; }
	inline void set_ShowOnButtonSelected_18(bool value)
	{
		___ShowOnButtonSelected_18 = value;
	}

	inline static int32_t get_offset_of_ShowOnClick_19() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___ShowOnClick_19)); }
	inline bool get_ShowOnClick_19() const { return ___ShowOnClick_19; }
	inline bool* get_address_of_ShowOnClick_19() { return &___ShowOnClick_19; }
	inline void set_ShowOnClick_19(bool value)
	{
		___ShowOnClick_19 = value;
	}

	inline static int32_t get_offset_of_ShowOnPointerEnter_20() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___ShowOnPointerEnter_20)); }
	inline bool get_ShowOnPointerEnter_20() const { return ___ShowOnPointerEnter_20; }
	inline bool* get_address_of_ShowOnPointerEnter_20() { return &___ShowOnPointerEnter_20; }
	inline void set_ShowOnPointerEnter_20(bool value)
	{
		___ShowOnPointerEnter_20 = value;
	}

	inline static int32_t get_offset_of_ShowOnPointerExit_21() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___ShowOnPointerExit_21)); }
	inline bool get_ShowOnPointerExit_21() const { return ___ShowOnPointerExit_21; }
	inline bool* get_address_of_ShowOnPointerExit_21() { return &___ShowOnPointerExit_21; }
	inline void set_ShowOnPointerExit_21(bool value)
	{
		___ShowOnPointerExit_21 = value;
	}

	inline static int32_t get_offset_of_DisableButtonBetweenClicksInterval_22() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___DisableButtonBetweenClicksInterval_22)); }
	inline float get_DisableButtonBetweenClicksInterval_22() const { return ___DisableButtonBetweenClicksInterval_22; }
	inline float* get_address_of_DisableButtonBetweenClicksInterval_22() { return &___DisableButtonBetweenClicksInterval_22; }
	inline void set_DisableButtonBetweenClicksInterval_22(float value)
	{
		___DisableButtonBetweenClicksInterval_22 = value;
	}

	inline static int32_t get_offset_of_VirtualButtonName_23() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___VirtualButtonName_23)); }
	inline String_t* get_VirtualButtonName_23() const { return ___VirtualButtonName_23; }
	inline String_t** get_address_of_VirtualButtonName_23() { return &___VirtualButtonName_23; }
	inline void set_VirtualButtonName_23(String_t* value)
	{
		___VirtualButtonName_23 = value;
		Il2CppCodeGenWriteBarrier((&___VirtualButtonName_23), value);
	}

	inline static int32_t get_offset_of_VirtualButtonNameAlt_24() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984, ___VirtualButtonNameAlt_24)); }
	inline String_t* get_VirtualButtonNameAlt_24() const { return ___VirtualButtonNameAlt_24; }
	inline String_t** get_address_of_VirtualButtonNameAlt_24() { return &___VirtualButtonNameAlt_24; }
	inline void set_VirtualButtonNameAlt_24(String_t* value)
	{
		___VirtualButtonNameAlt_24 = value;
		Il2CppCodeGenWriteBarrier((&___VirtualButtonNameAlt_24), value);
	}
};

struct UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984_StaticFields
{
public:
	// Doozy.Engine.UI.Settings.UIToggleSettings Doozy.Engine.UI.Settings.UIToggleSettings::s_instance
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984 * ___s_instance_5;

public:
	inline static int32_t get_offset_of_s_instance_5() { return static_cast<int32_t>(offsetof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984_StaticFields, ___s_instance_5)); }
	inline UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984 * get_s_instance_5() const { return ___s_instance_5; }
	inline UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984 ** get_address_of_s_instance_5() { return &___s_instance_5; }
	inline void set_s_instance_5(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984 * value)
	{
		___s_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLESETTINGS_T299D4343576C3294462B1CF2739E727DFD39E984_H
#ifndef UIVIEWSETTINGS_TF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B_H
#define UIVIEWSETTINGS_TF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Settings.UIViewSettings
struct  UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Doozy.Engine.UI.Base.NamesDatabase Doozy.Engine.UI.Settings.UIViewSettings::database
	NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D * ___database_6;
	// Doozy.Engine.UI.TargetOrientation Doozy.Engine.UI.Settings.UIViewSettings::TargetOrientation
	int32_t ___TargetOrientation_22;
	// Doozy.Engine.UI.UIViewStartBehavior Doozy.Engine.UI.Settings.UIViewSettings::BehaviorAtStart
	int32_t ___BehaviorAtStart_23;
	// UnityEngine.Vector3 Doozy.Engine.UI.Settings.UIViewSettings::CustomStartAnchoredPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___CustomStartAnchoredPosition_24;
	// System.Boolean Doozy.Engine.UI.Settings.UIViewSettings::DeselectAnyButtonSelectedOnHide
	bool ___DeselectAnyButtonSelectedOnHide_25;
	// System.Boolean Doozy.Engine.UI.Settings.UIViewSettings::DeselectAnyButtonSelectedOnShow
	bool ___DeselectAnyButtonSelectedOnShow_26;
	// System.Boolean Doozy.Engine.UI.Settings.UIViewSettings::DisableCanvasWhenHidden
	bool ___DisableCanvasWhenHidden_27;
	// System.Boolean Doozy.Engine.UI.Settings.UIViewSettings::DisableGameObjectWhenHidden
	bool ___DisableGameObjectWhenHidden_28;
	// System.Boolean Doozy.Engine.UI.Settings.UIViewSettings::DisableGraphicRaycasterWhenHidden
	bool ___DisableGraphicRaycasterWhenHidden_29;
	// System.Boolean Doozy.Engine.UI.Settings.UIViewSettings::UseCustomStartAnchoredPosition
	bool ___UseCustomStartAnchoredPosition_30;
	// System.String Doozy.Engine.UI.Settings.UIViewSettings::RenamePrefix
	String_t* ___RenamePrefix_31;
	// System.String Doozy.Engine.UI.Settings.UIViewSettings::RenameSuffix
	String_t* ___RenameSuffix_32;

public:
	inline static int32_t get_offset_of_database_6() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B, ___database_6)); }
	inline NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D * get_database_6() const { return ___database_6; }
	inline NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D ** get_address_of_database_6() { return &___database_6; }
	inline void set_database_6(NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D * value)
	{
		___database_6 = value;
		Il2CppCodeGenWriteBarrier((&___database_6), value);
	}

	inline static int32_t get_offset_of_TargetOrientation_22() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B, ___TargetOrientation_22)); }
	inline int32_t get_TargetOrientation_22() const { return ___TargetOrientation_22; }
	inline int32_t* get_address_of_TargetOrientation_22() { return &___TargetOrientation_22; }
	inline void set_TargetOrientation_22(int32_t value)
	{
		___TargetOrientation_22 = value;
	}

	inline static int32_t get_offset_of_BehaviorAtStart_23() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B, ___BehaviorAtStart_23)); }
	inline int32_t get_BehaviorAtStart_23() const { return ___BehaviorAtStart_23; }
	inline int32_t* get_address_of_BehaviorAtStart_23() { return &___BehaviorAtStart_23; }
	inline void set_BehaviorAtStart_23(int32_t value)
	{
		___BehaviorAtStart_23 = value;
	}

	inline static int32_t get_offset_of_CustomStartAnchoredPosition_24() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B, ___CustomStartAnchoredPosition_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_CustomStartAnchoredPosition_24() const { return ___CustomStartAnchoredPosition_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_CustomStartAnchoredPosition_24() { return &___CustomStartAnchoredPosition_24; }
	inline void set_CustomStartAnchoredPosition_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___CustomStartAnchoredPosition_24 = value;
	}

	inline static int32_t get_offset_of_DeselectAnyButtonSelectedOnHide_25() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B, ___DeselectAnyButtonSelectedOnHide_25)); }
	inline bool get_DeselectAnyButtonSelectedOnHide_25() const { return ___DeselectAnyButtonSelectedOnHide_25; }
	inline bool* get_address_of_DeselectAnyButtonSelectedOnHide_25() { return &___DeselectAnyButtonSelectedOnHide_25; }
	inline void set_DeselectAnyButtonSelectedOnHide_25(bool value)
	{
		___DeselectAnyButtonSelectedOnHide_25 = value;
	}

	inline static int32_t get_offset_of_DeselectAnyButtonSelectedOnShow_26() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B, ___DeselectAnyButtonSelectedOnShow_26)); }
	inline bool get_DeselectAnyButtonSelectedOnShow_26() const { return ___DeselectAnyButtonSelectedOnShow_26; }
	inline bool* get_address_of_DeselectAnyButtonSelectedOnShow_26() { return &___DeselectAnyButtonSelectedOnShow_26; }
	inline void set_DeselectAnyButtonSelectedOnShow_26(bool value)
	{
		___DeselectAnyButtonSelectedOnShow_26 = value;
	}

	inline static int32_t get_offset_of_DisableCanvasWhenHidden_27() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B, ___DisableCanvasWhenHidden_27)); }
	inline bool get_DisableCanvasWhenHidden_27() const { return ___DisableCanvasWhenHidden_27; }
	inline bool* get_address_of_DisableCanvasWhenHidden_27() { return &___DisableCanvasWhenHidden_27; }
	inline void set_DisableCanvasWhenHidden_27(bool value)
	{
		___DisableCanvasWhenHidden_27 = value;
	}

	inline static int32_t get_offset_of_DisableGameObjectWhenHidden_28() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B, ___DisableGameObjectWhenHidden_28)); }
	inline bool get_DisableGameObjectWhenHidden_28() const { return ___DisableGameObjectWhenHidden_28; }
	inline bool* get_address_of_DisableGameObjectWhenHidden_28() { return &___DisableGameObjectWhenHidden_28; }
	inline void set_DisableGameObjectWhenHidden_28(bool value)
	{
		___DisableGameObjectWhenHidden_28 = value;
	}

	inline static int32_t get_offset_of_DisableGraphicRaycasterWhenHidden_29() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B, ___DisableGraphicRaycasterWhenHidden_29)); }
	inline bool get_DisableGraphicRaycasterWhenHidden_29() const { return ___DisableGraphicRaycasterWhenHidden_29; }
	inline bool* get_address_of_DisableGraphicRaycasterWhenHidden_29() { return &___DisableGraphicRaycasterWhenHidden_29; }
	inline void set_DisableGraphicRaycasterWhenHidden_29(bool value)
	{
		___DisableGraphicRaycasterWhenHidden_29 = value;
	}

	inline static int32_t get_offset_of_UseCustomStartAnchoredPosition_30() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B, ___UseCustomStartAnchoredPosition_30)); }
	inline bool get_UseCustomStartAnchoredPosition_30() const { return ___UseCustomStartAnchoredPosition_30; }
	inline bool* get_address_of_UseCustomStartAnchoredPosition_30() { return &___UseCustomStartAnchoredPosition_30; }
	inline void set_UseCustomStartAnchoredPosition_30(bool value)
	{
		___UseCustomStartAnchoredPosition_30 = value;
	}

	inline static int32_t get_offset_of_RenamePrefix_31() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B, ___RenamePrefix_31)); }
	inline String_t* get_RenamePrefix_31() const { return ___RenamePrefix_31; }
	inline String_t** get_address_of_RenamePrefix_31() { return &___RenamePrefix_31; }
	inline void set_RenamePrefix_31(String_t* value)
	{
		___RenamePrefix_31 = value;
		Il2CppCodeGenWriteBarrier((&___RenamePrefix_31), value);
	}

	inline static int32_t get_offset_of_RenameSuffix_32() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B, ___RenameSuffix_32)); }
	inline String_t* get_RenameSuffix_32() const { return ___RenameSuffix_32; }
	inline String_t** get_address_of_RenameSuffix_32() { return &___RenameSuffix_32; }
	inline void set_RenameSuffix_32(String_t* value)
	{
		___RenameSuffix_32 = value;
		Il2CppCodeGenWriteBarrier((&___RenameSuffix_32), value);
	}
};

struct UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B_StaticFields
{
public:
	// Doozy.Engine.UI.Settings.UIViewSettings Doozy.Engine.UI.Settings.UIViewSettings::s_instance
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B * ___s_instance_5;
	// UnityEngine.Vector3 Doozy.Engine.UI.Settings.UIViewSettings::CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_21;

public:
	inline static int32_t get_offset_of_s_instance_5() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B_StaticFields, ___s_instance_5)); }
	inline UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B * get_s_instance_5() const { return ___s_instance_5; }
	inline UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B ** get_address_of_s_instance_5() { return &___s_instance_5; }
	inline void set_s_instance_5(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B * value)
	{
		___s_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_5), value);
	}

	inline static int32_t get_offset_of_CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_21() { return static_cast<int32_t>(offsetof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B_StaticFields, ___CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_21() const { return ___CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_21() { return &___CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_21; }
	inline void set_CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWSETTINGS_TF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef ACTIVATELOADEDSCENESNODE_T2B570621E8BD27CCACEB81E5A845592C4D04493F_H
#define ACTIVATELOADEDSCENESNODE_T2B570621E8BD27CCACEB81E5A845592C4D04493F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.ActivateLoadedScenesNode
struct  ActivateLoadedScenesNode_t2B570621E8BD27CCACEB81E5A845592C4D04493F  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATELOADEDSCENESNODE_T2B570621E8BD27CCACEB81E5A845592C4D04493F_H
#ifndef APPLICATIONQUITNODE_T9E079DF64C247A00F66E230F4ADD52FD61BF977A_H
#define APPLICATIONQUITNODE_T9E079DF64C247A00F66E230F4ADD52FD61BF977A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.ApplicationQuitNode
struct  ApplicationQuitNode_t9E079DF64C247A00F66E230F4ADD52FD61BF977A  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONQUITNODE_T9E079DF64C247A00F66E230F4ADD52FD61BF977A_H
#ifndef BACKBUTTONNODE_TED5FB71A4B61EE7ED773B29C5449CD1FF0EFBA28_H
#define BACKBUTTONNODE_TED5FB71A4B61EE7ED773B29C5449CD1FF0EFBA28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.BackButtonNode
struct  BackButtonNode_tED5FB71A4B61EE7ED773B29C5449CD1FF0EFBA28  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// Doozy.Engine.UI.Nodes.BackButtonNode_BackButtonState Doozy.Engine.UI.Nodes.BackButtonNode::BackButtonAction
	int32_t ___BackButtonAction_25;

public:
	inline static int32_t get_offset_of_BackButtonAction_25() { return static_cast<int32_t>(offsetof(BackButtonNode_tED5FB71A4B61EE7ED773B29C5449CD1FF0EFBA28, ___BackButtonAction_25)); }
	inline int32_t get_BackButtonAction_25() const { return ___BackButtonAction_25; }
	inline int32_t* get_address_of_BackButtonAction_25() { return &___BackButtonAction_25; }
	inline void set_BackButtonAction_25(int32_t value)
	{
		___BackButtonAction_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKBUTTONNODE_TED5FB71A4B61EE7ED773B29C5449CD1FF0EFBA28_H
#ifndef GAMEEVENTNODE_TA119DE2EADDCBFA3E7D36D02C37A0E2DB5FDCE9F_H
#define GAMEEVENTNODE_TA119DE2EADDCBFA3E7D36D02C37A0E2DB5FDCE9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.GameEventNode
struct  GameEventNode_tA119DE2EADDCBFA3E7D36D02C37A0E2DB5FDCE9F  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// System.String Doozy.Engine.UI.Nodes.GameEventNode::GameEvent
	String_t* ___GameEvent_25;

public:
	inline static int32_t get_offset_of_GameEvent_25() { return static_cast<int32_t>(offsetof(GameEventNode_tA119DE2EADDCBFA3E7D36D02C37A0E2DB5FDCE9F, ___GameEvent_25)); }
	inline String_t* get_GameEvent_25() const { return ___GameEvent_25; }
	inline String_t** get_address_of_GameEvent_25() { return &___GameEvent_25; }
	inline void set_GameEvent_25(String_t* value)
	{
		___GameEvent_25 = value;
		Il2CppCodeGenWriteBarrier((&___GameEvent_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEEVENTNODE_TA119DE2EADDCBFA3E7D36D02C37A0E2DB5FDCE9F_H
#ifndef LOADSCENENODE_T08E4097054040B108BCBA8284F735F7D403A39A9_H
#define LOADSCENENODE_T08E4097054040B108BCBA8284F735F7D403A39A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.LoadSceneNode
struct  LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// Doozy.Engine.SceneManagement.GetSceneBy Doozy.Engine.UI.Nodes.LoadSceneNode::GetSceneBy
	int32_t ___GetSceneBy_25;
	// UnityEngine.SceneManagement.LoadSceneMode Doozy.Engine.UI.Nodes.LoadSceneNode::LoadSceneMode
	int32_t ___LoadSceneMode_26;
	// System.Boolean Doozy.Engine.UI.Nodes.LoadSceneNode::AllowSceneActivation
	bool ___AllowSceneActivation_27;
	// System.Single Doozy.Engine.UI.Nodes.LoadSceneNode::SceneActivationDelay
	float ___SceneActivationDelay_28;
	// System.Int32 Doozy.Engine.UI.Nodes.LoadSceneNode::SceneBuildIndex
	int32_t ___SceneBuildIndex_29;
	// System.String Doozy.Engine.UI.Nodes.LoadSceneNode::SceneName
	String_t* ___SceneName_30;

public:
	inline static int32_t get_offset_of_GetSceneBy_25() { return static_cast<int32_t>(offsetof(LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9, ___GetSceneBy_25)); }
	inline int32_t get_GetSceneBy_25() const { return ___GetSceneBy_25; }
	inline int32_t* get_address_of_GetSceneBy_25() { return &___GetSceneBy_25; }
	inline void set_GetSceneBy_25(int32_t value)
	{
		___GetSceneBy_25 = value;
	}

	inline static int32_t get_offset_of_LoadSceneMode_26() { return static_cast<int32_t>(offsetof(LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9, ___LoadSceneMode_26)); }
	inline int32_t get_LoadSceneMode_26() const { return ___LoadSceneMode_26; }
	inline int32_t* get_address_of_LoadSceneMode_26() { return &___LoadSceneMode_26; }
	inline void set_LoadSceneMode_26(int32_t value)
	{
		___LoadSceneMode_26 = value;
	}

	inline static int32_t get_offset_of_AllowSceneActivation_27() { return static_cast<int32_t>(offsetof(LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9, ___AllowSceneActivation_27)); }
	inline bool get_AllowSceneActivation_27() const { return ___AllowSceneActivation_27; }
	inline bool* get_address_of_AllowSceneActivation_27() { return &___AllowSceneActivation_27; }
	inline void set_AllowSceneActivation_27(bool value)
	{
		___AllowSceneActivation_27 = value;
	}

	inline static int32_t get_offset_of_SceneActivationDelay_28() { return static_cast<int32_t>(offsetof(LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9, ___SceneActivationDelay_28)); }
	inline float get_SceneActivationDelay_28() const { return ___SceneActivationDelay_28; }
	inline float* get_address_of_SceneActivationDelay_28() { return &___SceneActivationDelay_28; }
	inline void set_SceneActivationDelay_28(float value)
	{
		___SceneActivationDelay_28 = value;
	}

	inline static int32_t get_offset_of_SceneBuildIndex_29() { return static_cast<int32_t>(offsetof(LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9, ___SceneBuildIndex_29)); }
	inline int32_t get_SceneBuildIndex_29() const { return ___SceneBuildIndex_29; }
	inline int32_t* get_address_of_SceneBuildIndex_29() { return &___SceneBuildIndex_29; }
	inline void set_SceneBuildIndex_29(int32_t value)
	{
		___SceneBuildIndex_29 = value;
	}

	inline static int32_t get_offset_of_SceneName_30() { return static_cast<int32_t>(offsetof(LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9, ___SceneName_30)); }
	inline String_t* get_SceneName_30() const { return ___SceneName_30; }
	inline String_t** get_address_of_SceneName_30() { return &___SceneName_30; }
	inline void set_SceneName_30(String_t* value)
	{
		___SceneName_30 = value;
		Il2CppCodeGenWriteBarrier((&___SceneName_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENENODE_T08E4097054040B108BCBA8284F735F7D403A39A9_H
#ifndef PORTALNODE_TC5531962EB486D1D57834E2101E5730889F9C645_H
#define PORTALNODE_TC5531962EB486D1D57834E2101E5730889F9C645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.PortalNode
struct  PortalNode_tC5531962EB486D1D57834E2101E5730889F9C645  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// System.String Doozy.Engine.UI.Nodes.PortalNode::m_gameEvent
	String_t* ___m_gameEvent_25;
	// Doozy.Engine.Nody.Models.Graph Doozy.Engine.UI.Nodes.PortalNode::m_portalGraph
	Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * ___m_portalGraph_26;

public:
	inline static int32_t get_offset_of_m_gameEvent_25() { return static_cast<int32_t>(offsetof(PortalNode_tC5531962EB486D1D57834E2101E5730889F9C645, ___m_gameEvent_25)); }
	inline String_t* get_m_gameEvent_25() const { return ___m_gameEvent_25; }
	inline String_t** get_address_of_m_gameEvent_25() { return &___m_gameEvent_25; }
	inline void set_m_gameEvent_25(String_t* value)
	{
		___m_gameEvent_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameEvent_25), value);
	}

	inline static int32_t get_offset_of_m_portalGraph_26() { return static_cast<int32_t>(offsetof(PortalNode_tC5531962EB486D1D57834E2101E5730889F9C645, ___m_portalGraph_26)); }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * get_m_portalGraph_26() const { return ___m_portalGraph_26; }
	inline Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 ** get_address_of_m_portalGraph_26() { return &___m_portalGraph_26; }
	inline void set_m_portalGraph_26(Graph_t26D9C31435F4455B6EF025E506031462302E7AD5 * value)
	{
		___m_portalGraph_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_portalGraph_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PORTALNODE_TC5531962EB486D1D57834E2101E5730889F9C645_H
#ifndef RANDOMNODE_T88E8FEAF540EF0CC3D0C96C1D6EE927BA51A7FAA_H
#define RANDOMNODE_T88E8FEAF540EF0CC3D0C96C1D6EE927BA51A7FAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.RandomNode
struct  RandomNode_t88E8FEAF540EF0CC3D0C96C1D6EE927BA51A7FAA  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// System.Collections.Generic.List`1<System.Int32> Doozy.Engine.UI.Nodes.RandomNode::m_selectChances
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_selectChances_25;
	// System.Int32 Doozy.Engine.UI.Nodes.RandomNode::<MaxChance>k__BackingField
	int32_t ___U3CMaxChanceU3Ek__BackingField_26;
	// System.Int32 Doozy.Engine.UI.Nodes.RandomNode::<ConnectedOutputSockets>k__BackingField
	int32_t ___U3CConnectedOutputSocketsU3Ek__BackingField_27;

public:
	inline static int32_t get_offset_of_m_selectChances_25() { return static_cast<int32_t>(offsetof(RandomNode_t88E8FEAF540EF0CC3D0C96C1D6EE927BA51A7FAA, ___m_selectChances_25)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_selectChances_25() const { return ___m_selectChances_25; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_selectChances_25() { return &___m_selectChances_25; }
	inline void set_m_selectChances_25(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_selectChances_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_selectChances_25), value);
	}

	inline static int32_t get_offset_of_U3CMaxChanceU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(RandomNode_t88E8FEAF540EF0CC3D0C96C1D6EE927BA51A7FAA, ___U3CMaxChanceU3Ek__BackingField_26)); }
	inline int32_t get_U3CMaxChanceU3Ek__BackingField_26() const { return ___U3CMaxChanceU3Ek__BackingField_26; }
	inline int32_t* get_address_of_U3CMaxChanceU3Ek__BackingField_26() { return &___U3CMaxChanceU3Ek__BackingField_26; }
	inline void set_U3CMaxChanceU3Ek__BackingField_26(int32_t value)
	{
		___U3CMaxChanceU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CConnectedOutputSocketsU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(RandomNode_t88E8FEAF540EF0CC3D0C96C1D6EE927BA51A7FAA, ___U3CConnectedOutputSocketsU3Ek__BackingField_27)); }
	inline int32_t get_U3CConnectedOutputSocketsU3Ek__BackingField_27() const { return ___U3CConnectedOutputSocketsU3Ek__BackingField_27; }
	inline int32_t* get_address_of_U3CConnectedOutputSocketsU3Ek__BackingField_27() { return &___U3CConnectedOutputSocketsU3Ek__BackingField_27; }
	inline void set_U3CConnectedOutputSocketsU3Ek__BackingField_27(int32_t value)
	{
		___U3CConnectedOutputSocketsU3Ek__BackingField_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMNODE_T88E8FEAF540EF0CC3D0C96C1D6EE927BA51A7FAA_H
#ifndef SOUNDNODE_T3F0578EDD4D6945DEC530F14C138AC8AC1691003_H
#define SOUNDNODE_T3F0578EDD4D6945DEC530F14C138AC8AC1691003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.SoundNode
struct  SoundNode_t3F0578EDD4D6945DEC530F14C138AC8AC1691003  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// Doozy.Engine.Soundy.SoundyData Doozy.Engine.UI.Nodes.SoundNode::SoundData
	SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720 * ___SoundData_25;
	// Doozy.Engine.UI.Nodes.SoundNode_SoundActions Doozy.Engine.UI.Nodes.SoundNode::SoundAction
	int32_t ___SoundAction_26;

public:
	inline static int32_t get_offset_of_SoundData_25() { return static_cast<int32_t>(offsetof(SoundNode_t3F0578EDD4D6945DEC530F14C138AC8AC1691003, ___SoundData_25)); }
	inline SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720 * get_SoundData_25() const { return ___SoundData_25; }
	inline SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720 ** get_address_of_SoundData_25() { return &___SoundData_25; }
	inline void set_SoundData_25(SoundyData_t1265C32F2FF565EF7D276CCAF3027B1BF7974720 * value)
	{
		___SoundData_25 = value;
		Il2CppCodeGenWriteBarrier((&___SoundData_25), value);
	}

	inline static int32_t get_offset_of_SoundAction_26() { return static_cast<int32_t>(offsetof(SoundNode_t3F0578EDD4D6945DEC530F14C138AC8AC1691003, ___SoundAction_26)); }
	inline int32_t get_SoundAction_26() const { return ___SoundAction_26; }
	inline int32_t* get_address_of_SoundAction_26() { return &___SoundAction_26; }
	inline void set_SoundAction_26(int32_t value)
	{
		___SoundAction_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDNODE_T3F0578EDD4D6945DEC530F14C138AC8AC1691003_H
#ifndef TIMESCALENODE_T9E967953D5E9624D707651E4AAF8C6B513F28364_H
#define TIMESCALENODE_T9E967953D5E9624D707651E4AAF8C6B513F28364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.TimeScaleNode
struct  TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// System.Single Doozy.Engine.UI.Nodes.TimeScaleNode::TargetValue
	float ___TargetValue_30;
	// System.Boolean Doozy.Engine.UI.Nodes.TimeScaleNode::AnimateValue
	bool ___AnimateValue_31;
	// System.Single Doozy.Engine.UI.Nodes.TimeScaleNode::AnimationDuration
	float ___AnimationDuration_32;
	// DG.Tweening.Ease Doozy.Engine.UI.Nodes.TimeScaleNode::AnimationEase
	int32_t ___AnimationEase_33;
	// System.Boolean Doozy.Engine.UI.Nodes.TimeScaleNode::WaitForAnimationToFinish
	bool ___WaitForAnimationToFinish_34;
	// DG.Tweening.Sequence Doozy.Engine.UI.Nodes.TimeScaleNode::m_animationSequence
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___m_animationSequence_35;
	// System.Boolean Doozy.Engine.UI.Nodes.TimeScaleNode::m_timerIsActive
	bool ___m_timerIsActive_36;
	// System.Double Doozy.Engine.UI.Nodes.TimeScaleNode::m_timerStart
	double ___m_timerStart_37;
	// System.Single Doozy.Engine.UI.Nodes.TimeScaleNode::m_timeDuration
	float ___m_timeDuration_38;

public:
	inline static int32_t get_offset_of_TargetValue_30() { return static_cast<int32_t>(offsetof(TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364, ___TargetValue_30)); }
	inline float get_TargetValue_30() const { return ___TargetValue_30; }
	inline float* get_address_of_TargetValue_30() { return &___TargetValue_30; }
	inline void set_TargetValue_30(float value)
	{
		___TargetValue_30 = value;
	}

	inline static int32_t get_offset_of_AnimateValue_31() { return static_cast<int32_t>(offsetof(TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364, ___AnimateValue_31)); }
	inline bool get_AnimateValue_31() const { return ___AnimateValue_31; }
	inline bool* get_address_of_AnimateValue_31() { return &___AnimateValue_31; }
	inline void set_AnimateValue_31(bool value)
	{
		___AnimateValue_31 = value;
	}

	inline static int32_t get_offset_of_AnimationDuration_32() { return static_cast<int32_t>(offsetof(TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364, ___AnimationDuration_32)); }
	inline float get_AnimationDuration_32() const { return ___AnimationDuration_32; }
	inline float* get_address_of_AnimationDuration_32() { return &___AnimationDuration_32; }
	inline void set_AnimationDuration_32(float value)
	{
		___AnimationDuration_32 = value;
	}

	inline static int32_t get_offset_of_AnimationEase_33() { return static_cast<int32_t>(offsetof(TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364, ___AnimationEase_33)); }
	inline int32_t get_AnimationEase_33() const { return ___AnimationEase_33; }
	inline int32_t* get_address_of_AnimationEase_33() { return &___AnimationEase_33; }
	inline void set_AnimationEase_33(int32_t value)
	{
		___AnimationEase_33 = value;
	}

	inline static int32_t get_offset_of_WaitForAnimationToFinish_34() { return static_cast<int32_t>(offsetof(TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364, ___WaitForAnimationToFinish_34)); }
	inline bool get_WaitForAnimationToFinish_34() const { return ___WaitForAnimationToFinish_34; }
	inline bool* get_address_of_WaitForAnimationToFinish_34() { return &___WaitForAnimationToFinish_34; }
	inline void set_WaitForAnimationToFinish_34(bool value)
	{
		___WaitForAnimationToFinish_34 = value;
	}

	inline static int32_t get_offset_of_m_animationSequence_35() { return static_cast<int32_t>(offsetof(TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364, ___m_animationSequence_35)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_m_animationSequence_35() const { return ___m_animationSequence_35; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_m_animationSequence_35() { return &___m_animationSequence_35; }
	inline void set_m_animationSequence_35(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___m_animationSequence_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_animationSequence_35), value);
	}

	inline static int32_t get_offset_of_m_timerIsActive_36() { return static_cast<int32_t>(offsetof(TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364, ___m_timerIsActive_36)); }
	inline bool get_m_timerIsActive_36() const { return ___m_timerIsActive_36; }
	inline bool* get_address_of_m_timerIsActive_36() { return &___m_timerIsActive_36; }
	inline void set_m_timerIsActive_36(bool value)
	{
		___m_timerIsActive_36 = value;
	}

	inline static int32_t get_offset_of_m_timerStart_37() { return static_cast<int32_t>(offsetof(TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364, ___m_timerStart_37)); }
	inline double get_m_timerStart_37() const { return ___m_timerStart_37; }
	inline double* get_address_of_m_timerStart_37() { return &___m_timerStart_37; }
	inline void set_m_timerStart_37(double value)
	{
		___m_timerStart_37 = value;
	}

	inline static int32_t get_offset_of_m_timeDuration_38() { return static_cast<int32_t>(offsetof(TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364, ___m_timeDuration_38)); }
	inline float get_m_timeDuration_38() const { return ___m_timeDuration_38; }
	inline float* get_address_of_m_timeDuration_38() { return &___m_timeDuration_38; }
	inline void set_m_timeDuration_38(float value)
	{
		___m_timeDuration_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESCALENODE_T9E967953D5E9624D707651E4AAF8C6B513F28364_H
#ifndef UIDRAWERNODE_TECEF2D0D957536DC85044CC06F086AB8335FC22F_H
#define UIDRAWERNODE_TECEF2D0D957536DC85044CC06F086AB8335FC22F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.UIDrawerNode
struct  UIDrawerNode_tECEF2D0D957536DC85044CC06F086AB8335FC22F  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// System.String Doozy.Engine.UI.Nodes.UIDrawerNode::DrawerName
	String_t* ___DrawerName_25;
	// System.Boolean Doozy.Engine.UI.Nodes.UIDrawerNode::CustomDrawerName
	bool ___CustomDrawerName_26;
	// Doozy.Engine.UI.Nodes.UIDrawerNode_DrawerAction Doozy.Engine.UI.Nodes.UIDrawerNode::Action
	int32_t ___Action_27;

public:
	inline static int32_t get_offset_of_DrawerName_25() { return static_cast<int32_t>(offsetof(UIDrawerNode_tECEF2D0D957536DC85044CC06F086AB8335FC22F, ___DrawerName_25)); }
	inline String_t* get_DrawerName_25() const { return ___DrawerName_25; }
	inline String_t** get_address_of_DrawerName_25() { return &___DrawerName_25; }
	inline void set_DrawerName_25(String_t* value)
	{
		___DrawerName_25 = value;
		Il2CppCodeGenWriteBarrier((&___DrawerName_25), value);
	}

	inline static int32_t get_offset_of_CustomDrawerName_26() { return static_cast<int32_t>(offsetof(UIDrawerNode_tECEF2D0D957536DC85044CC06F086AB8335FC22F, ___CustomDrawerName_26)); }
	inline bool get_CustomDrawerName_26() const { return ___CustomDrawerName_26; }
	inline bool* get_address_of_CustomDrawerName_26() { return &___CustomDrawerName_26; }
	inline void set_CustomDrawerName_26(bool value)
	{
		___CustomDrawerName_26 = value;
	}

	inline static int32_t get_offset_of_Action_27() { return static_cast<int32_t>(offsetof(UIDrawerNode_tECEF2D0D957536DC85044CC06F086AB8335FC22F, ___Action_27)); }
	inline int32_t get_Action_27() const { return ___Action_27; }
	inline int32_t* get_address_of_Action_27() { return &___Action_27; }
	inline void set_Action_27(int32_t value)
	{
		___Action_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWERNODE_TECEF2D0D957536DC85044CC06F086AB8335FC22F_H
#ifndef UINODE_TD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF_H
#define UINODE_TD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.UINode
struct  UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// System.Collections.Generic.List`1<Doozy.Engine.UI.Internal.UIViewCategoryName> Doozy.Engine.UI.Nodes.UINode::m_onEnterShowViews
	List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 * ___m_onEnterShowViews_25;
	// System.Collections.Generic.List`1<Doozy.Engine.UI.Internal.UIViewCategoryName> Doozy.Engine.UI.Nodes.UINode::m_onEnterHideViews
	List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 * ___m_onEnterHideViews_26;
	// System.Collections.Generic.List`1<Doozy.Engine.UI.Internal.UIViewCategoryName> Doozy.Engine.UI.Nodes.UINode::m_onExitShowViews
	List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 * ___m_onExitShowViews_27;
	// System.Collections.Generic.List`1<Doozy.Engine.UI.Internal.UIViewCategoryName> Doozy.Engine.UI.Nodes.UINode::m_onExitHideViews
	List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 * ___m_onExitHideViews_28;
	// System.Boolean Doozy.Engine.UI.Nodes.UINode::m_timerIsActive
	bool ___m_timerIsActive_29;
	// System.Double Doozy.Engine.UI.Nodes.UINode::m_timerStart
	double ___m_timerStart_30;
	// System.Single Doozy.Engine.UI.Nodes.UINode::m_timeDelay
	float ___m_timeDelay_31;
	// Doozy.Engine.Nody.Models.Socket Doozy.Engine.UI.Nodes.UINode::m_activeSocketAfterTimeDelay
	Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8 * ___m_activeSocketAfterTimeDelay_32;

public:
	inline static int32_t get_offset_of_m_onEnterShowViews_25() { return static_cast<int32_t>(offsetof(UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF, ___m_onEnterShowViews_25)); }
	inline List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 * get_m_onEnterShowViews_25() const { return ___m_onEnterShowViews_25; }
	inline List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 ** get_address_of_m_onEnterShowViews_25() { return &___m_onEnterShowViews_25; }
	inline void set_m_onEnterShowViews_25(List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 * value)
	{
		___m_onEnterShowViews_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_onEnterShowViews_25), value);
	}

	inline static int32_t get_offset_of_m_onEnterHideViews_26() { return static_cast<int32_t>(offsetof(UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF, ___m_onEnterHideViews_26)); }
	inline List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 * get_m_onEnterHideViews_26() const { return ___m_onEnterHideViews_26; }
	inline List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 ** get_address_of_m_onEnterHideViews_26() { return &___m_onEnterHideViews_26; }
	inline void set_m_onEnterHideViews_26(List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 * value)
	{
		___m_onEnterHideViews_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_onEnterHideViews_26), value);
	}

	inline static int32_t get_offset_of_m_onExitShowViews_27() { return static_cast<int32_t>(offsetof(UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF, ___m_onExitShowViews_27)); }
	inline List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 * get_m_onExitShowViews_27() const { return ___m_onExitShowViews_27; }
	inline List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 ** get_address_of_m_onExitShowViews_27() { return &___m_onExitShowViews_27; }
	inline void set_m_onExitShowViews_27(List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 * value)
	{
		___m_onExitShowViews_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_onExitShowViews_27), value);
	}

	inline static int32_t get_offset_of_m_onExitHideViews_28() { return static_cast<int32_t>(offsetof(UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF, ___m_onExitHideViews_28)); }
	inline List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 * get_m_onExitHideViews_28() const { return ___m_onExitHideViews_28; }
	inline List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 ** get_address_of_m_onExitHideViews_28() { return &___m_onExitHideViews_28; }
	inline void set_m_onExitHideViews_28(List_1_tDC375D8B4C01CC0DE1981A461F01FFBD53D827C6 * value)
	{
		___m_onExitHideViews_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_onExitHideViews_28), value);
	}

	inline static int32_t get_offset_of_m_timerIsActive_29() { return static_cast<int32_t>(offsetof(UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF, ___m_timerIsActive_29)); }
	inline bool get_m_timerIsActive_29() const { return ___m_timerIsActive_29; }
	inline bool* get_address_of_m_timerIsActive_29() { return &___m_timerIsActive_29; }
	inline void set_m_timerIsActive_29(bool value)
	{
		___m_timerIsActive_29 = value;
	}

	inline static int32_t get_offset_of_m_timerStart_30() { return static_cast<int32_t>(offsetof(UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF, ___m_timerStart_30)); }
	inline double get_m_timerStart_30() const { return ___m_timerStart_30; }
	inline double* get_address_of_m_timerStart_30() { return &___m_timerStart_30; }
	inline void set_m_timerStart_30(double value)
	{
		___m_timerStart_30 = value;
	}

	inline static int32_t get_offset_of_m_timeDelay_31() { return static_cast<int32_t>(offsetof(UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF, ___m_timeDelay_31)); }
	inline float get_m_timeDelay_31() const { return ___m_timeDelay_31; }
	inline float* get_address_of_m_timeDelay_31() { return &___m_timeDelay_31; }
	inline void set_m_timeDelay_31(float value)
	{
		___m_timeDelay_31 = value;
	}

	inline static int32_t get_offset_of_m_activeSocketAfterTimeDelay_32() { return static_cast<int32_t>(offsetof(UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF, ___m_activeSocketAfterTimeDelay_32)); }
	inline Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8 * get_m_activeSocketAfterTimeDelay_32() const { return ___m_activeSocketAfterTimeDelay_32; }
	inline Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8 ** get_address_of_m_activeSocketAfterTimeDelay_32() { return &___m_activeSocketAfterTimeDelay_32; }
	inline void set_m_activeSocketAfterTimeDelay_32(Socket_t0285F9E2EE130163067662CA798F6DEB4559E1E8 * value)
	{
		___m_activeSocketAfterTimeDelay_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_activeSocketAfterTimeDelay_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINODE_TD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF_H
#ifndef UNLOADSCENENODE_T890BCF3F9B7257583DD0A8F153DC0D78B406F944_H
#define UNLOADSCENENODE_T890BCF3F9B7257583DD0A8F153DC0D78B406F944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.UnloadSceneNode
struct  UnloadSceneNode_t890BCF3F9B7257583DD0A8F153DC0D78B406F944  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// Doozy.Engine.SceneManagement.GetSceneBy Doozy.Engine.UI.Nodes.UnloadSceneNode::GetSceneBy
	int32_t ___GetSceneBy_25;
	// System.Int32 Doozy.Engine.UI.Nodes.UnloadSceneNode::SceneBuildIndex
	int32_t ___SceneBuildIndex_26;
	// System.String Doozy.Engine.UI.Nodes.UnloadSceneNode::SceneName
	String_t* ___SceneName_27;
	// System.Boolean Doozy.Engine.UI.Nodes.UnloadSceneNode::WaitForSceneToUnload
	bool ___WaitForSceneToUnload_28;

public:
	inline static int32_t get_offset_of_GetSceneBy_25() { return static_cast<int32_t>(offsetof(UnloadSceneNode_t890BCF3F9B7257583DD0A8F153DC0D78B406F944, ___GetSceneBy_25)); }
	inline int32_t get_GetSceneBy_25() const { return ___GetSceneBy_25; }
	inline int32_t* get_address_of_GetSceneBy_25() { return &___GetSceneBy_25; }
	inline void set_GetSceneBy_25(int32_t value)
	{
		___GetSceneBy_25 = value;
	}

	inline static int32_t get_offset_of_SceneBuildIndex_26() { return static_cast<int32_t>(offsetof(UnloadSceneNode_t890BCF3F9B7257583DD0A8F153DC0D78B406F944, ___SceneBuildIndex_26)); }
	inline int32_t get_SceneBuildIndex_26() const { return ___SceneBuildIndex_26; }
	inline int32_t* get_address_of_SceneBuildIndex_26() { return &___SceneBuildIndex_26; }
	inline void set_SceneBuildIndex_26(int32_t value)
	{
		___SceneBuildIndex_26 = value;
	}

	inline static int32_t get_offset_of_SceneName_27() { return static_cast<int32_t>(offsetof(UnloadSceneNode_t890BCF3F9B7257583DD0A8F153DC0D78B406F944, ___SceneName_27)); }
	inline String_t* get_SceneName_27() const { return ___SceneName_27; }
	inline String_t** get_address_of_SceneName_27() { return &___SceneName_27; }
	inline void set_SceneName_27(String_t* value)
	{
		___SceneName_27 = value;
		Il2CppCodeGenWriteBarrier((&___SceneName_27), value);
	}

	inline static int32_t get_offset_of_WaitForSceneToUnload_28() { return static_cast<int32_t>(offsetof(UnloadSceneNode_t890BCF3F9B7257583DD0A8F153DC0D78B406F944, ___WaitForSceneToUnload_28)); }
	inline bool get_WaitForSceneToUnload_28() const { return ___WaitForSceneToUnload_28; }
	inline bool* get_address_of_WaitForSceneToUnload_28() { return &___WaitForSceneToUnload_28; }
	inline void set_WaitForSceneToUnload_28(bool value)
	{
		___WaitForSceneToUnload_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNLOADSCENENODE_T890BCF3F9B7257583DD0A8F153DC0D78B406F944_H
#ifndef WAITNODE_T475FA7E31B16E4B5C4D8F79995B75E38913FA203_H
#define WAITNODE_T475FA7E31B16E4B5C4D8F79995B75E38913FA203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Nodes.WaitNode
struct  WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203  : public Node_t06F79187A31E6958DED19533B1A8783AD1633439
{
public:
	// Doozy.Engine.SceneManagement.GetSceneBy Doozy.Engine.UI.Nodes.WaitNode::GetSceneBy
	int32_t ___GetSceneBy_33;
	// Doozy.Engine.UI.Nodes.WaitNode_WaitType Doozy.Engine.UI.Nodes.WaitNode::WaitFor
	int32_t ___WaitFor_34;
	// System.Boolean Doozy.Engine.UI.Nodes.WaitNode::AnyValue
	bool ___AnyValue_35;
	// System.Boolean Doozy.Engine.UI.Nodes.WaitNode::IgnoreUnityTimescale
	bool ___IgnoreUnityTimescale_36;
	// System.Boolean Doozy.Engine.UI.Nodes.WaitNode::RandomDuration
	bool ___RandomDuration_37;
	// System.Single Doozy.Engine.UI.Nodes.WaitNode::Duration
	float ___Duration_38;
	// System.Single Doozy.Engine.UI.Nodes.WaitNode::DurationMax
	float ___DurationMax_39;
	// System.Single Doozy.Engine.UI.Nodes.WaitNode::DurationMin
	float ___DurationMin_40;
	// System.Int32 Doozy.Engine.UI.Nodes.WaitNode::SceneBuildIndex
	int32_t ___SceneBuildIndex_41;
	// System.String Doozy.Engine.UI.Nodes.WaitNode::GameEvent
	String_t* ___GameEvent_42;
	// System.String Doozy.Engine.UI.Nodes.WaitNode::SceneName
	String_t* ___SceneName_43;
	// System.Single Doozy.Engine.UI.Nodes.WaitNode::CurrentDuration
	float ___CurrentDuration_44;
	// System.Boolean Doozy.Engine.UI.Nodes.WaitNode::m_timerIsActive
	bool ___m_timerIsActive_45;
	// System.Double Doozy.Engine.UI.Nodes.WaitNode::m_timerStart
	double ___m_timerStart_46;
	// System.Single Doozy.Engine.UI.Nodes.WaitNode::m_timeDelay
	float ___m_timeDelay_47;

public:
	inline static int32_t get_offset_of_GetSceneBy_33() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___GetSceneBy_33)); }
	inline int32_t get_GetSceneBy_33() const { return ___GetSceneBy_33; }
	inline int32_t* get_address_of_GetSceneBy_33() { return &___GetSceneBy_33; }
	inline void set_GetSceneBy_33(int32_t value)
	{
		___GetSceneBy_33 = value;
	}

	inline static int32_t get_offset_of_WaitFor_34() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___WaitFor_34)); }
	inline int32_t get_WaitFor_34() const { return ___WaitFor_34; }
	inline int32_t* get_address_of_WaitFor_34() { return &___WaitFor_34; }
	inline void set_WaitFor_34(int32_t value)
	{
		___WaitFor_34 = value;
	}

	inline static int32_t get_offset_of_AnyValue_35() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___AnyValue_35)); }
	inline bool get_AnyValue_35() const { return ___AnyValue_35; }
	inline bool* get_address_of_AnyValue_35() { return &___AnyValue_35; }
	inline void set_AnyValue_35(bool value)
	{
		___AnyValue_35 = value;
	}

	inline static int32_t get_offset_of_IgnoreUnityTimescale_36() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___IgnoreUnityTimescale_36)); }
	inline bool get_IgnoreUnityTimescale_36() const { return ___IgnoreUnityTimescale_36; }
	inline bool* get_address_of_IgnoreUnityTimescale_36() { return &___IgnoreUnityTimescale_36; }
	inline void set_IgnoreUnityTimescale_36(bool value)
	{
		___IgnoreUnityTimescale_36 = value;
	}

	inline static int32_t get_offset_of_RandomDuration_37() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___RandomDuration_37)); }
	inline bool get_RandomDuration_37() const { return ___RandomDuration_37; }
	inline bool* get_address_of_RandomDuration_37() { return &___RandomDuration_37; }
	inline void set_RandomDuration_37(bool value)
	{
		___RandomDuration_37 = value;
	}

	inline static int32_t get_offset_of_Duration_38() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___Duration_38)); }
	inline float get_Duration_38() const { return ___Duration_38; }
	inline float* get_address_of_Duration_38() { return &___Duration_38; }
	inline void set_Duration_38(float value)
	{
		___Duration_38 = value;
	}

	inline static int32_t get_offset_of_DurationMax_39() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___DurationMax_39)); }
	inline float get_DurationMax_39() const { return ___DurationMax_39; }
	inline float* get_address_of_DurationMax_39() { return &___DurationMax_39; }
	inline void set_DurationMax_39(float value)
	{
		___DurationMax_39 = value;
	}

	inline static int32_t get_offset_of_DurationMin_40() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___DurationMin_40)); }
	inline float get_DurationMin_40() const { return ___DurationMin_40; }
	inline float* get_address_of_DurationMin_40() { return &___DurationMin_40; }
	inline void set_DurationMin_40(float value)
	{
		___DurationMin_40 = value;
	}

	inline static int32_t get_offset_of_SceneBuildIndex_41() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___SceneBuildIndex_41)); }
	inline int32_t get_SceneBuildIndex_41() const { return ___SceneBuildIndex_41; }
	inline int32_t* get_address_of_SceneBuildIndex_41() { return &___SceneBuildIndex_41; }
	inline void set_SceneBuildIndex_41(int32_t value)
	{
		___SceneBuildIndex_41 = value;
	}

	inline static int32_t get_offset_of_GameEvent_42() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___GameEvent_42)); }
	inline String_t* get_GameEvent_42() const { return ___GameEvent_42; }
	inline String_t** get_address_of_GameEvent_42() { return &___GameEvent_42; }
	inline void set_GameEvent_42(String_t* value)
	{
		___GameEvent_42 = value;
		Il2CppCodeGenWriteBarrier((&___GameEvent_42), value);
	}

	inline static int32_t get_offset_of_SceneName_43() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___SceneName_43)); }
	inline String_t* get_SceneName_43() const { return ___SceneName_43; }
	inline String_t** get_address_of_SceneName_43() { return &___SceneName_43; }
	inline void set_SceneName_43(String_t* value)
	{
		___SceneName_43 = value;
		Il2CppCodeGenWriteBarrier((&___SceneName_43), value);
	}

	inline static int32_t get_offset_of_CurrentDuration_44() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___CurrentDuration_44)); }
	inline float get_CurrentDuration_44() const { return ___CurrentDuration_44; }
	inline float* get_address_of_CurrentDuration_44() { return &___CurrentDuration_44; }
	inline void set_CurrentDuration_44(float value)
	{
		___CurrentDuration_44 = value;
	}

	inline static int32_t get_offset_of_m_timerIsActive_45() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___m_timerIsActive_45)); }
	inline bool get_m_timerIsActive_45() const { return ___m_timerIsActive_45; }
	inline bool* get_address_of_m_timerIsActive_45() { return &___m_timerIsActive_45; }
	inline void set_m_timerIsActive_45(bool value)
	{
		___m_timerIsActive_45 = value;
	}

	inline static int32_t get_offset_of_m_timerStart_46() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___m_timerStart_46)); }
	inline double get_m_timerStart_46() const { return ___m_timerStart_46; }
	inline double* get_address_of_m_timerStart_46() { return &___m_timerStart_46; }
	inline void set_m_timerStart_46(double value)
	{
		___m_timerStart_46 = value;
	}

	inline static int32_t get_offset_of_m_timeDelay_47() { return static_cast<int32_t>(offsetof(WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203, ___m_timeDelay_47)); }
	inline float get_m_timeDelay_47() const { return ___m_timeDelay_47; }
	inline float* get_address_of_m_timeDelay_47() { return &___m_timeDelay_47; }
	inline void set_m_timeDelay_47(float value)
	{
		___m_timeDelay_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITNODE_T475FA7E31B16E4B5C4D8F79995B75E38913FA203_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef SOUNDYCONTROLLER_T88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55_H
#define SOUNDYCONTROLLER_T88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Soundy.SoundyController
struct  SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform Doozy.Engine.Soundy.SoundyController::m_transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_transform_5;
	// UnityEngine.Transform Doozy.Engine.Soundy.SoundyController::m_followTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_followTarget_6;
	// UnityEngine.AudioSource Doozy.Engine.Soundy.SoundyController::m_audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___m_audioSource_7;
	// System.Boolean Doozy.Engine.Soundy.SoundyController::m_inUse
	bool ___m_inUse_8;
	// System.Single Doozy.Engine.Soundy.SoundyController::m_playProgress
	float ___m_playProgress_9;
	// System.Boolean Doozy.Engine.Soundy.SoundyController::m_isPaused
	bool ___m_isPaused_10;
	// System.Boolean Doozy.Engine.Soundy.SoundyController::m_isMuted
	bool ___m_isMuted_11;
	// System.Single Doozy.Engine.Soundy.SoundyController::m_lastPlayedTime
	float ___m_lastPlayedTime_12;
	// System.Boolean Doozy.Engine.Soundy.SoundyController::m_isPlaying
	bool ___m_isPlaying_13;
	// System.Boolean Doozy.Engine.Soundy.SoundyController::m_autoPaused
	bool ___m_autoPaused_14;

public:
	inline static int32_t get_offset_of_m_transform_5() { return static_cast<int32_t>(offsetof(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55, ___m_transform_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_transform_5() const { return ___m_transform_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_transform_5() { return &___m_transform_5; }
	inline void set_m_transform_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_transform_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_5), value);
	}

	inline static int32_t get_offset_of_m_followTarget_6() { return static_cast<int32_t>(offsetof(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55, ___m_followTarget_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_followTarget_6() const { return ___m_followTarget_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_followTarget_6() { return &___m_followTarget_6; }
	inline void set_m_followTarget_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_followTarget_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_followTarget_6), value);
	}

	inline static int32_t get_offset_of_m_audioSource_7() { return static_cast<int32_t>(offsetof(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55, ___m_audioSource_7)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_m_audioSource_7() const { return ___m_audioSource_7; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_m_audioSource_7() { return &___m_audioSource_7; }
	inline void set_m_audioSource_7(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___m_audioSource_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioSource_7), value);
	}

	inline static int32_t get_offset_of_m_inUse_8() { return static_cast<int32_t>(offsetof(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55, ___m_inUse_8)); }
	inline bool get_m_inUse_8() const { return ___m_inUse_8; }
	inline bool* get_address_of_m_inUse_8() { return &___m_inUse_8; }
	inline void set_m_inUse_8(bool value)
	{
		___m_inUse_8 = value;
	}

	inline static int32_t get_offset_of_m_playProgress_9() { return static_cast<int32_t>(offsetof(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55, ___m_playProgress_9)); }
	inline float get_m_playProgress_9() const { return ___m_playProgress_9; }
	inline float* get_address_of_m_playProgress_9() { return &___m_playProgress_9; }
	inline void set_m_playProgress_9(float value)
	{
		___m_playProgress_9 = value;
	}

	inline static int32_t get_offset_of_m_isPaused_10() { return static_cast<int32_t>(offsetof(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55, ___m_isPaused_10)); }
	inline bool get_m_isPaused_10() const { return ___m_isPaused_10; }
	inline bool* get_address_of_m_isPaused_10() { return &___m_isPaused_10; }
	inline void set_m_isPaused_10(bool value)
	{
		___m_isPaused_10 = value;
	}

	inline static int32_t get_offset_of_m_isMuted_11() { return static_cast<int32_t>(offsetof(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55, ___m_isMuted_11)); }
	inline bool get_m_isMuted_11() const { return ___m_isMuted_11; }
	inline bool* get_address_of_m_isMuted_11() { return &___m_isMuted_11; }
	inline void set_m_isMuted_11(bool value)
	{
		___m_isMuted_11 = value;
	}

	inline static int32_t get_offset_of_m_lastPlayedTime_12() { return static_cast<int32_t>(offsetof(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55, ___m_lastPlayedTime_12)); }
	inline float get_m_lastPlayedTime_12() const { return ___m_lastPlayedTime_12; }
	inline float* get_address_of_m_lastPlayedTime_12() { return &___m_lastPlayedTime_12; }
	inline void set_m_lastPlayedTime_12(float value)
	{
		___m_lastPlayedTime_12 = value;
	}

	inline static int32_t get_offset_of_m_isPlaying_13() { return static_cast<int32_t>(offsetof(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55, ___m_isPlaying_13)); }
	inline bool get_m_isPlaying_13() const { return ___m_isPlaying_13; }
	inline bool* get_address_of_m_isPlaying_13() { return &___m_isPlaying_13; }
	inline void set_m_isPlaying_13(bool value)
	{
		___m_isPlaying_13 = value;
	}

	inline static int32_t get_offset_of_m_autoPaused_14() { return static_cast<int32_t>(offsetof(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55, ___m_autoPaused_14)); }
	inline bool get_m_autoPaused_14() const { return ___m_autoPaused_14; }
	inline bool* get_address_of_m_autoPaused_14() { return &___m_autoPaused_14; }
	inline void set_m_autoPaused_14(bool value)
	{
		___m_autoPaused_14 = value;
	}
};

struct SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55_StaticFields
{
public:
	// System.Collections.Generic.List`1<Doozy.Engine.Soundy.SoundyController> Doozy.Engine.Soundy.SoundyController::s_database
	List_1_t9BE00C8B071DC24C9723A6663A60205AB423B921 * ___s_database_4;

public:
	inline static int32_t get_offset_of_s_database_4() { return static_cast<int32_t>(offsetof(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55_StaticFields, ___s_database_4)); }
	inline List_1_t9BE00C8B071DC24C9723A6663A60205AB423B921 * get_s_database_4() const { return ___s_database_4; }
	inline List_1_t9BE00C8B071DC24C9723A6663A60205AB423B921 ** get_address_of_s_database_4() { return &___s_database_4; }
	inline void set_s_database_4(List_1_t9BE00C8B071DC24C9723A6663A60205AB423B921 * value)
	{
		___s_database_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_database_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDYCONTROLLER_T88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55_H
#ifndef GESTURELISTENER_T4A01A03F99D9617AE1E89514AF7591AA64CEDA4E_H
#define GESTURELISTENER_T4A01A03F99D9617AE1E89514AF7591AA64CEDA4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.GestureListener
struct  GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.Touchy.GestureListener::DebugMode
	bool ___DebugMode_4;
	// System.Boolean Doozy.Engine.Touchy.GestureListener::GlobalListener
	bool ___GlobalListener_5;
	// System.Boolean Doozy.Engine.Touchy.GestureListener::OverrideTarget
	bool ___OverrideTarget_6;
	// UnityEngine.GameObject Doozy.Engine.Touchy.GestureListener::TargetGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___TargetGameObject_7;
	// Doozy.Engine.Touchy.GestureType Doozy.Engine.Touchy.GestureListener::GestureType
	int32_t ___GestureType_8;
	// Doozy.Engine.Touchy.Swipe Doozy.Engine.Touchy.GestureListener::SwipeDirection
	int32_t ___SwipeDirection_9;
	// Doozy.Engine.Touchy.TouchInfoEvent Doozy.Engine.Touchy.GestureListener::OnGestureEvent
	TouchInfoEvent_t7C133938279F0BC34E525D4C9F62B88A6B206429 * ___OnGestureEvent_10;
	// System.Action`1<Doozy.Engine.Touchy.TouchInfo> Doozy.Engine.Touchy.GestureListener::OnGestureAction
	Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * ___OnGestureAction_11;
	// System.Collections.Generic.List`1<System.String> Doozy.Engine.Touchy.GestureListener::GameEvents
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___GameEvents_12;

public:
	inline static int32_t get_offset_of_DebugMode_4() { return static_cast<int32_t>(offsetof(GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E, ___DebugMode_4)); }
	inline bool get_DebugMode_4() const { return ___DebugMode_4; }
	inline bool* get_address_of_DebugMode_4() { return &___DebugMode_4; }
	inline void set_DebugMode_4(bool value)
	{
		___DebugMode_4 = value;
	}

	inline static int32_t get_offset_of_GlobalListener_5() { return static_cast<int32_t>(offsetof(GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E, ___GlobalListener_5)); }
	inline bool get_GlobalListener_5() const { return ___GlobalListener_5; }
	inline bool* get_address_of_GlobalListener_5() { return &___GlobalListener_5; }
	inline void set_GlobalListener_5(bool value)
	{
		___GlobalListener_5 = value;
	}

	inline static int32_t get_offset_of_OverrideTarget_6() { return static_cast<int32_t>(offsetof(GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E, ___OverrideTarget_6)); }
	inline bool get_OverrideTarget_6() const { return ___OverrideTarget_6; }
	inline bool* get_address_of_OverrideTarget_6() { return &___OverrideTarget_6; }
	inline void set_OverrideTarget_6(bool value)
	{
		___OverrideTarget_6 = value;
	}

	inline static int32_t get_offset_of_TargetGameObject_7() { return static_cast<int32_t>(offsetof(GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E, ___TargetGameObject_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_TargetGameObject_7() const { return ___TargetGameObject_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_TargetGameObject_7() { return &___TargetGameObject_7; }
	inline void set_TargetGameObject_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___TargetGameObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___TargetGameObject_7), value);
	}

	inline static int32_t get_offset_of_GestureType_8() { return static_cast<int32_t>(offsetof(GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E, ___GestureType_8)); }
	inline int32_t get_GestureType_8() const { return ___GestureType_8; }
	inline int32_t* get_address_of_GestureType_8() { return &___GestureType_8; }
	inline void set_GestureType_8(int32_t value)
	{
		___GestureType_8 = value;
	}

	inline static int32_t get_offset_of_SwipeDirection_9() { return static_cast<int32_t>(offsetof(GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E, ___SwipeDirection_9)); }
	inline int32_t get_SwipeDirection_9() const { return ___SwipeDirection_9; }
	inline int32_t* get_address_of_SwipeDirection_9() { return &___SwipeDirection_9; }
	inline void set_SwipeDirection_9(int32_t value)
	{
		___SwipeDirection_9 = value;
	}

	inline static int32_t get_offset_of_OnGestureEvent_10() { return static_cast<int32_t>(offsetof(GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E, ___OnGestureEvent_10)); }
	inline TouchInfoEvent_t7C133938279F0BC34E525D4C9F62B88A6B206429 * get_OnGestureEvent_10() const { return ___OnGestureEvent_10; }
	inline TouchInfoEvent_t7C133938279F0BC34E525D4C9F62B88A6B206429 ** get_address_of_OnGestureEvent_10() { return &___OnGestureEvent_10; }
	inline void set_OnGestureEvent_10(TouchInfoEvent_t7C133938279F0BC34E525D4C9F62B88A6B206429 * value)
	{
		___OnGestureEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnGestureEvent_10), value);
	}

	inline static int32_t get_offset_of_OnGestureAction_11() { return static_cast<int32_t>(offsetof(GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E, ___OnGestureAction_11)); }
	inline Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * get_OnGestureAction_11() const { return ___OnGestureAction_11; }
	inline Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A ** get_address_of_OnGestureAction_11() { return &___OnGestureAction_11; }
	inline void set_OnGestureAction_11(Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * value)
	{
		___OnGestureAction_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnGestureAction_11), value);
	}

	inline static int32_t get_offset_of_GameEvents_12() { return static_cast<int32_t>(offsetof(GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E, ___GameEvents_12)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_GameEvents_12() const { return ___GameEvents_12; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_GameEvents_12() { return &___GameEvents_12; }
	inline void set_GameEvents_12(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___GameEvents_12 = value;
		Il2CppCodeGenWriteBarrier((&___GameEvents_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURELISTENER_T4A01A03F99D9617AE1E89514AF7591AA64CEDA4E_H
#ifndef TOUCHDETECTOR_T6E812123D8251A11D1AF49732F0D2A9E4A6777C8_H
#define TOUCHDETECTOR_T6E812123D8251A11D1AF49732F0D2A9E4A6777C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.TouchDetector
struct  TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.Touchy.TouchDetector::<TouchInProgress>k__BackingField
	bool ___U3CTouchInProgressU3Ek__BackingField_6;
	// System.Action`1<Doozy.Engine.Touchy.TouchInfo> Doozy.Engine.Touchy.TouchDetector::OnTapAction
	Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * ___OnTapAction_7;
	// System.Action`1<Doozy.Engine.Touchy.TouchInfo> Doozy.Engine.Touchy.TouchDetector::OnLongTapAction
	Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * ___OnLongTapAction_8;
	// System.Action`1<Doozy.Engine.Touchy.TouchInfo> Doozy.Engine.Touchy.TouchDetector::OnSwipeAction
	Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * ___OnSwipeAction_9;
	// UnityEngine.Vector2 Doozy.Engine.Touchy.TouchDetector::m_currentSwipe
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_currentSwipe_10;
	// System.Boolean Doozy.Engine.Touchy.TouchDetector::m_swipeEnded
	bool ___m_swipeEnded_11;
	// Doozy.Engine.Touchy.TouchInfo Doozy.Engine.Touchy.TouchDetector::m_currentTouchInfo
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8  ___m_currentTouchInfo_12;
	// System.Collections.Generic.List`1<UnityEngine.Touch> Doozy.Engine.Touchy.TouchDetector::m_touches
	List_1_t7EE3B0C4AD74ED82F86902199B1BD59B4459037F * ___m_touches_13;
	// UnityEngine.Touch Doozy.Engine.Touchy.TouchDetector::m_touch
	Touch_t806752C775BA713A91B6588A07CA98417CABC003  ___m_touch_14;
	// UnityEngine.EventSystems.PointerEventData Doozy.Engine.Touchy.TouchDetector::m_pointerEventData
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___m_pointerEventData_15;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Doozy.Engine.Touchy.TouchDetector::m_raycastResults
	List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * ___m_raycastResults_16;

public:
	inline static int32_t get_offset_of_U3CTouchInProgressU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8, ___U3CTouchInProgressU3Ek__BackingField_6)); }
	inline bool get_U3CTouchInProgressU3Ek__BackingField_6() const { return ___U3CTouchInProgressU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CTouchInProgressU3Ek__BackingField_6() { return &___U3CTouchInProgressU3Ek__BackingField_6; }
	inline void set_U3CTouchInProgressU3Ek__BackingField_6(bool value)
	{
		___U3CTouchInProgressU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_OnTapAction_7() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8, ___OnTapAction_7)); }
	inline Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * get_OnTapAction_7() const { return ___OnTapAction_7; }
	inline Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A ** get_address_of_OnTapAction_7() { return &___OnTapAction_7; }
	inline void set_OnTapAction_7(Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * value)
	{
		___OnTapAction_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnTapAction_7), value);
	}

	inline static int32_t get_offset_of_OnLongTapAction_8() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8, ___OnLongTapAction_8)); }
	inline Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * get_OnLongTapAction_8() const { return ___OnLongTapAction_8; }
	inline Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A ** get_address_of_OnLongTapAction_8() { return &___OnLongTapAction_8; }
	inline void set_OnLongTapAction_8(Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * value)
	{
		___OnLongTapAction_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnLongTapAction_8), value);
	}

	inline static int32_t get_offset_of_OnSwipeAction_9() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8, ___OnSwipeAction_9)); }
	inline Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * get_OnSwipeAction_9() const { return ___OnSwipeAction_9; }
	inline Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A ** get_address_of_OnSwipeAction_9() { return &___OnSwipeAction_9; }
	inline void set_OnSwipeAction_9(Action_1_t83FE4F02D2A03DCF46A9D4CC1BFEE046A80F184A * value)
	{
		___OnSwipeAction_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnSwipeAction_9), value);
	}

	inline static int32_t get_offset_of_m_currentSwipe_10() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8, ___m_currentSwipe_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_currentSwipe_10() const { return ___m_currentSwipe_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_currentSwipe_10() { return &___m_currentSwipe_10; }
	inline void set_m_currentSwipe_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_currentSwipe_10 = value;
	}

	inline static int32_t get_offset_of_m_swipeEnded_11() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8, ___m_swipeEnded_11)); }
	inline bool get_m_swipeEnded_11() const { return ___m_swipeEnded_11; }
	inline bool* get_address_of_m_swipeEnded_11() { return &___m_swipeEnded_11; }
	inline void set_m_swipeEnded_11(bool value)
	{
		___m_swipeEnded_11 = value;
	}

	inline static int32_t get_offset_of_m_currentTouchInfo_12() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8, ___m_currentTouchInfo_12)); }
	inline TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8  get_m_currentTouchInfo_12() const { return ___m_currentTouchInfo_12; }
	inline TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8 * get_address_of_m_currentTouchInfo_12() { return &___m_currentTouchInfo_12; }
	inline void set_m_currentTouchInfo_12(TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8  value)
	{
		___m_currentTouchInfo_12 = value;
	}

	inline static int32_t get_offset_of_m_touches_13() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8, ___m_touches_13)); }
	inline List_1_t7EE3B0C4AD74ED82F86902199B1BD59B4459037F * get_m_touches_13() const { return ___m_touches_13; }
	inline List_1_t7EE3B0C4AD74ED82F86902199B1BD59B4459037F ** get_address_of_m_touches_13() { return &___m_touches_13; }
	inline void set_m_touches_13(List_1_t7EE3B0C4AD74ED82F86902199B1BD59B4459037F * value)
	{
		___m_touches_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_touches_13), value);
	}

	inline static int32_t get_offset_of_m_touch_14() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8, ___m_touch_14)); }
	inline Touch_t806752C775BA713A91B6588A07CA98417CABC003  get_m_touch_14() const { return ___m_touch_14; }
	inline Touch_t806752C775BA713A91B6588A07CA98417CABC003 * get_address_of_m_touch_14() { return &___m_touch_14; }
	inline void set_m_touch_14(Touch_t806752C775BA713A91B6588A07CA98417CABC003  value)
	{
		___m_touch_14 = value;
	}

	inline static int32_t get_offset_of_m_pointerEventData_15() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8, ___m_pointerEventData_15)); }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * get_m_pointerEventData_15() const { return ___m_pointerEventData_15; }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 ** get_address_of_m_pointerEventData_15() { return &___m_pointerEventData_15; }
	inline void set_m_pointerEventData_15(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * value)
	{
		___m_pointerEventData_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_pointerEventData_15), value);
	}

	inline static int32_t get_offset_of_m_raycastResults_16() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8, ___m_raycastResults_16)); }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * get_m_raycastResults_16() const { return ___m_raycastResults_16; }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 ** get_address_of_m_raycastResults_16() { return &___m_raycastResults_16; }
	inline void set_m_raycastResults_16(List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * value)
	{
		___m_raycastResults_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_raycastResults_16), value);
	}
};

struct TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8_StaticFields
{
public:
	// Doozy.Engine.Touchy.TouchDetector Doozy.Engine.Touchy.TouchDetector::s_instance
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8 * ___s_instance_4;
	// System.Boolean Doozy.Engine.Touchy.TouchDetector::<ApplicationIsQuitting>k__BackingField
	bool ___U3CApplicationIsQuittingU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_s_instance_4() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8_StaticFields, ___s_instance_4)); }
	inline TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8 * get_s_instance_4() const { return ___s_instance_4; }
	inline TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8 ** get_address_of_s_instance_4() { return &___s_instance_4; }
	inline void set_s_instance_4(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8 * value)
	{
		___s_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_4), value);
	}

	inline static int32_t get_offset_of_U3CApplicationIsQuittingU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8_StaticFields, ___U3CApplicationIsQuittingU3Ek__BackingField_5)); }
	inline bool get_U3CApplicationIsQuittingU3Ek__BackingField_5() const { return ___U3CApplicationIsQuittingU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CApplicationIsQuittingU3Ek__BackingField_5() { return &___U3CApplicationIsQuittingU3Ek__BackingField_5; }
	inline void set_U3CApplicationIsQuittingU3Ek__BackingField_5(bool value)
	{
		___U3CApplicationIsQuittingU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHDETECTOR_T6E812123D8251A11D1AF49732F0D2A9E4A6777C8_H
#ifndef BACKBUTTON_T0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA_H
#define BACKBUTTON_T0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Input.BackButton
struct  BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Doozy.Engine.UI.Input.InputData Doozy.Engine.UI.Input.BackButton::BackButtonInputData
	InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC * ___BackButtonInputData_15;
	// System.Boolean Doozy.Engine.UI.Input.BackButton::DebugMode
	bool ___DebugMode_16;
	// System.Int32 Doozy.Engine.UI.Input.BackButton::m_backButtonDisableLevel
	int32_t ___m_backButtonDisableLevel_17;
	// System.Double Doozy.Engine.UI.Input.BackButton::m_lastBackButtonPressTime
	double ___m_lastBackButtonPressTime_18;

public:
	inline static int32_t get_offset_of_BackButtonInputData_15() { return static_cast<int32_t>(offsetof(BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA, ___BackButtonInputData_15)); }
	inline InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC * get_BackButtonInputData_15() const { return ___BackButtonInputData_15; }
	inline InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC ** get_address_of_BackButtonInputData_15() { return &___BackButtonInputData_15; }
	inline void set_BackButtonInputData_15(InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC * value)
	{
		___BackButtonInputData_15 = value;
		Il2CppCodeGenWriteBarrier((&___BackButtonInputData_15), value);
	}

	inline static int32_t get_offset_of_DebugMode_16() { return static_cast<int32_t>(offsetof(BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA, ___DebugMode_16)); }
	inline bool get_DebugMode_16() const { return ___DebugMode_16; }
	inline bool* get_address_of_DebugMode_16() { return &___DebugMode_16; }
	inline void set_DebugMode_16(bool value)
	{
		___DebugMode_16 = value;
	}

	inline static int32_t get_offset_of_m_backButtonDisableLevel_17() { return static_cast<int32_t>(offsetof(BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA, ___m_backButtonDisableLevel_17)); }
	inline int32_t get_m_backButtonDisableLevel_17() const { return ___m_backButtonDisableLevel_17; }
	inline int32_t* get_address_of_m_backButtonDisableLevel_17() { return &___m_backButtonDisableLevel_17; }
	inline void set_m_backButtonDisableLevel_17(int32_t value)
	{
		___m_backButtonDisableLevel_17 = value;
	}

	inline static int32_t get_offset_of_m_lastBackButtonPressTime_18() { return static_cast<int32_t>(offsetof(BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA, ___m_lastBackButtonPressTime_18)); }
	inline double get_m_lastBackButtonPressTime_18() const { return ___m_lastBackButtonPressTime_18; }
	inline double* get_address_of_m_lastBackButtonPressTime_18() { return &___m_lastBackButtonPressTime_18; }
	inline void set_m_lastBackButtonPressTime_18(double value)
	{
		___m_lastBackButtonPressTime_18 = value;
	}
};

struct BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA_StaticFields
{
public:
	// Doozy.Engine.UI.Input.BackButton Doozy.Engine.UI.Input.BackButton::s_instance
	BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA * ___s_instance_4;
	// System.Boolean Doozy.Engine.UI.Input.BackButton::s_applicationIsQuitting
	bool ___s_applicationIsQuitting_13;
	// System.Boolean Doozy.Engine.UI.Input.BackButton::s_initialized
	bool ___s_initialized_14;

public:
	inline static int32_t get_offset_of_s_instance_4() { return static_cast<int32_t>(offsetof(BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA_StaticFields, ___s_instance_4)); }
	inline BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA * get_s_instance_4() const { return ___s_instance_4; }
	inline BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA ** get_address_of_s_instance_4() { return &___s_instance_4; }
	inline void set_s_instance_4(BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA * value)
	{
		___s_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_4), value);
	}

	inline static int32_t get_offset_of_s_applicationIsQuitting_13() { return static_cast<int32_t>(offsetof(BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA_StaticFields, ___s_applicationIsQuitting_13)); }
	inline bool get_s_applicationIsQuitting_13() const { return ___s_applicationIsQuitting_13; }
	inline bool* get_address_of_s_applicationIsQuitting_13() { return &___s_applicationIsQuitting_13; }
	inline void set_s_applicationIsQuitting_13(bool value)
	{
		___s_applicationIsQuitting_13 = value;
	}

	inline static int32_t get_offset_of_s_initialized_14() { return static_cast<int32_t>(offsetof(BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA_StaticFields, ___s_initialized_14)); }
	inline bool get_s_initialized_14() const { return ___s_initialized_14; }
	inline bool* get_address_of_s_initialized_14() { return &___s_initialized_14; }
	inline void set_s_initialized_14(bool value)
	{
		___s_initialized_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKBUTTON_T0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA_H
#ifndef KEYTOGAMEEVENT_T6A327EB12E2E03B507630190319BCDE26AB5439A_H
#define KEYTOGAMEEVENT_T6A327EB12E2E03B507630190319BCDE26AB5439A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Input.KeyToGameEvent
struct  KeyToGameEvent_t6A327EB12E2E03B507630190319BCDE26AB5439A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.UI.Input.KeyToGameEvent::DebugMode
	bool ___DebugMode_4;
	// Doozy.Engine.UI.Input.InputData Doozy.Engine.UI.Input.KeyToGameEvent::InputData
	InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC * ___InputData_5;
	// System.String Doozy.Engine.UI.Input.KeyToGameEvent::GameEvent
	String_t* ___GameEvent_6;

public:
	inline static int32_t get_offset_of_DebugMode_4() { return static_cast<int32_t>(offsetof(KeyToGameEvent_t6A327EB12E2E03B507630190319BCDE26AB5439A, ___DebugMode_4)); }
	inline bool get_DebugMode_4() const { return ___DebugMode_4; }
	inline bool* get_address_of_DebugMode_4() { return &___DebugMode_4; }
	inline void set_DebugMode_4(bool value)
	{
		___DebugMode_4 = value;
	}

	inline static int32_t get_offset_of_InputData_5() { return static_cast<int32_t>(offsetof(KeyToGameEvent_t6A327EB12E2E03B507630190319BCDE26AB5439A, ___InputData_5)); }
	inline InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC * get_InputData_5() const { return ___InputData_5; }
	inline InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC ** get_address_of_InputData_5() { return &___InputData_5; }
	inline void set_InputData_5(InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC * value)
	{
		___InputData_5 = value;
		Il2CppCodeGenWriteBarrier((&___InputData_5), value);
	}

	inline static int32_t get_offset_of_GameEvent_6() { return static_cast<int32_t>(offsetof(KeyToGameEvent_t6A327EB12E2E03B507630190319BCDE26AB5439A, ___GameEvent_6)); }
	inline String_t* get_GameEvent_6() const { return ___GameEvent_6; }
	inline String_t** get_address_of_GameEvent_6() { return &___GameEvent_6; }
	inline void set_GameEvent_6(String_t* value)
	{
		___GameEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___GameEvent_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYTOGAMEEVENT_T6A327EB12E2E03B507630190319BCDE26AB5439A_H
#ifndef UIVIEWLISTENER_T21A62F28E39C4E0D673DE579E54E1EE7A51CD79B_H
#define UIVIEWLISTENER_T21A62F28E39C4E0D673DE579E54E1EE7A51CD79B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIViewListener
struct  UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.UI.UIViewListener::DebugMode
	bool ___DebugMode_4;
	// Doozy.Engine.UI.UIViewEvent Doozy.Engine.UI.UIViewListener::Event
	UIViewEvent_t5DBF09498B7D574591E73E4A18578AE6CCE35F29 * ___Event_5;
	// System.Boolean Doozy.Engine.UI.UIViewListener::ListenForAllUIViews
	bool ___ListenForAllUIViews_6;
	// Doozy.Engine.UI.UIViewBehaviorType Doozy.Engine.UI.UIViewListener::TriggerAction
	int32_t ___TriggerAction_7;
	// System.String Doozy.Engine.UI.UIViewListener::ViewCategory
	String_t* ___ViewCategory_8;
	// System.String Doozy.Engine.UI.UIViewListener::ViewName
	String_t* ___ViewName_9;

public:
	inline static int32_t get_offset_of_DebugMode_4() { return static_cast<int32_t>(offsetof(UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B, ___DebugMode_4)); }
	inline bool get_DebugMode_4() const { return ___DebugMode_4; }
	inline bool* get_address_of_DebugMode_4() { return &___DebugMode_4; }
	inline void set_DebugMode_4(bool value)
	{
		___DebugMode_4 = value;
	}

	inline static int32_t get_offset_of_Event_5() { return static_cast<int32_t>(offsetof(UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B, ___Event_5)); }
	inline UIViewEvent_t5DBF09498B7D574591E73E4A18578AE6CCE35F29 * get_Event_5() const { return ___Event_5; }
	inline UIViewEvent_t5DBF09498B7D574591E73E4A18578AE6CCE35F29 ** get_address_of_Event_5() { return &___Event_5; }
	inline void set_Event_5(UIViewEvent_t5DBF09498B7D574591E73E4A18578AE6CCE35F29 * value)
	{
		___Event_5 = value;
		Il2CppCodeGenWriteBarrier((&___Event_5), value);
	}

	inline static int32_t get_offset_of_ListenForAllUIViews_6() { return static_cast<int32_t>(offsetof(UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B, ___ListenForAllUIViews_6)); }
	inline bool get_ListenForAllUIViews_6() const { return ___ListenForAllUIViews_6; }
	inline bool* get_address_of_ListenForAllUIViews_6() { return &___ListenForAllUIViews_6; }
	inline void set_ListenForAllUIViews_6(bool value)
	{
		___ListenForAllUIViews_6 = value;
	}

	inline static int32_t get_offset_of_TriggerAction_7() { return static_cast<int32_t>(offsetof(UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B, ___TriggerAction_7)); }
	inline int32_t get_TriggerAction_7() const { return ___TriggerAction_7; }
	inline int32_t* get_address_of_TriggerAction_7() { return &___TriggerAction_7; }
	inline void set_TriggerAction_7(int32_t value)
	{
		___TriggerAction_7 = value;
	}

	inline static int32_t get_offset_of_ViewCategory_8() { return static_cast<int32_t>(offsetof(UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B, ___ViewCategory_8)); }
	inline String_t* get_ViewCategory_8() const { return ___ViewCategory_8; }
	inline String_t** get_address_of_ViewCategory_8() { return &___ViewCategory_8; }
	inline void set_ViewCategory_8(String_t* value)
	{
		___ViewCategory_8 = value;
		Il2CppCodeGenWriteBarrier((&___ViewCategory_8), value);
	}

	inline static int32_t get_offset_of_ViewName_9() { return static_cast<int32_t>(offsetof(UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B, ___ViewName_9)); }
	inline String_t* get_ViewName_9() const { return ___ViewName_9; }
	inline String_t** get_address_of_ViewName_9() { return &___ViewName_9; }
	inline void set_ViewName_9(String_t* value)
	{
		___ViewName_9 = value;
		Il2CppCodeGenWriteBarrier((&___ViewName_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWLISTENER_T21A62F28E39C4E0D673DE579E54E1EE7A51CD79B_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5700 = { sizeof (UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5700[6] = 
{
	UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B::get_offset_of_DebugMode_4(),
	UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B::get_offset_of_Event_5(),
	UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B::get_offset_of_ListenForAllUIViews_6(),
	UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B::get_offset_of_TriggerAction_7(),
	UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B::get_offset_of_ViewCategory_8(),
	UIViewListener_t21A62F28E39C4E0D673DE579E54E1EE7A51CD79B::get_offset_of_ViewName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5701 = { sizeof (UIViewMessage_t1E69AF139CF5288975D421C8AD6C71A72EB318D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5701[2] = 
{
	UIViewMessage_t1E69AF139CF5288975D421C8AD6C71A72EB318D5::get_offset_of_View_3(),
	UIViewMessage_t1E69AF139CF5288975D421C8AD6C71A72EB318D5::get_offset_of_Type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5702 = { sizeof (UIViewCategoryName_t952A945B741872B3CCEA6A8410D543AA2C881FA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5702[6] = 
{
	0,
	0,
	0,
	UIViewCategoryName_t952A945B741872B3CCEA6A8410D543AA2C881FA2::get_offset_of_Category_3(),
	UIViewCategoryName_t952A945B741872B3CCEA6A8410D543AA2C881FA2::get_offset_of_InstantAction_4(),
	UIViewCategoryName_t952A945B741872B3CCEA6A8410D543AA2C881FA2::get_offset_of_Name_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5703 = { sizeof (UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A), -1, sizeof(UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5703[36] = 
{
	0,
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A_StaticFields::get_offset_of_s_instance_5(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_database_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_InputMode_17(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_KeyCode_18(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_KeyCodeAlt_19(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_ClickMode_20(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_AllowMultipleClicks_21(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_DeselectButtonAfterClick_22(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_EnableAlternateInputs_23(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_ShowNormalLoopAnimation_24(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_ShowOnButtonDeselected_25(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_ShowOnButtonSelected_26(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_ShowOnClick_27(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_ShowOnDoubleClick_28(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_ShowOnLongClick_29(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_ShowOnPointerDown_30(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_ShowOnPointerEnter_31(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_ShowOnPointerExit_32(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_ShowOnPointerUp_33(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_ShowSelectedLoopAnimation_34(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_DisableButtonBetweenClicksInterval_35(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_RenamePrefix_36(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_RenameSuffix_37(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_VirtualButtonName_38(),
	UIButtonSettings_tF1C628E7B5F15F250AC021A5B7E92CE91096659A::get_offset_of_VirtualButtonNameAlt_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5704 = { sizeof (UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4), -1, sizeof(UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5704[9] = 
{
	0,
	UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4_StaticFields::get_offset_of_s_instance_5(),
	UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4::get_offset_of_database_6(),
	0,
	0,
	0,
	UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4::get_offset_of_DontDestroyCanvasOnLoad_10(),
	UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4::get_offset_of_RenamePrefix_11(),
	UICanvasSettings_t64ABCAFE0349BD9BD11818C0E2A95401234771E4::get_offset_of_RenameSuffix_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5705 = { sizeof (UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F), -1, sizeof(UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5705[19] = 
{
	0,
	UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F_StaticFields::get_offset_of_s_instance_5(),
	UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F::get_offset_of_database_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F_StaticFields::get_offset_of_CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_14(),
	UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F::get_offset_of_CloseDirection_15(),
	UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F::get_offset_of_CustomStartAnchoredPosition_16(),
	UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F::get_offset_of_DetectGestures_17(),
	UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F::get_offset_of_UseCustomStartAnchoredPosition_18(),
	UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F::get_offset_of_CloseSpeed_19(),
	UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F::get_offset_of_OpenSpeed_20(),
	UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F::get_offset_of_RenamePrefix_21(),
	UIDrawerSettings_tBE608F9D9B3C0450D2F9B6F76D201C581628B50F::get_offset_of_RenameSuffix_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5706 = { sizeof (UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A), -1, sizeof(UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5706[34] = 
{
	0,
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A_StaticFields::get_offset_of_s_instance_5(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_database_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_DisplayTarget_23(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_AddToPopupQueue_24(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_AutoHideAfterShow_25(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_AutoSelectButtonAfterShow_26(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_CustomCanvasName_27(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_DestroyAfterHide_28(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_HideOnBackButton_29(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_HideOnClickAnywhere_30(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_HideOnClickContainer_31(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_HideOnClickOverlay_32(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_UpdateHideProgressorOnShow_33(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_UpdateShowProgressorOnHide_34(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_UseOverlay_35(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_AutoHideAfterShowDelay_36(),
	UIPopupSettings_t4565021723312AC24CAD09E7ECB40A0E6893EA3A::get_offset_of_CanvasName_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5707 = { sizeof (UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984), -1, sizeof(UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5707[21] = 
{
	0,
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984_StaticFields::get_offset_of_s_instance_5(),
	0,
	0,
	0,
	0,
	0,
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_InputMode_11(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_KeyCode_12(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_KeyCodeAlt_13(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_AllowMultipleClicks_14(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_DeselectButtonAfterClick_15(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_EnableAlternateInputs_16(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_ShowOnButtonDeselected_17(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_ShowOnButtonSelected_18(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_ShowOnClick_19(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_ShowOnPointerEnter_20(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_ShowOnPointerExit_21(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_DisableButtonBetweenClicksInterval_22(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_VirtualButtonName_23(),
	UIToggleSettings_t299D4343576C3294462B1CF2739E727DFD39E984::get_offset_of_VirtualButtonNameAlt_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5708 = { sizeof (UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B), -1, sizeof(UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5708[29] = 
{
	0,
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B_StaticFields::get_offset_of_s_instance_5(),
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B::get_offset_of_database_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B_StaticFields::get_offset_of_CUSTOM_START_ANCHORED_POSITION_DEFAULT_VALUE_21(),
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B::get_offset_of_TargetOrientation_22(),
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B::get_offset_of_BehaviorAtStart_23(),
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B::get_offset_of_CustomStartAnchoredPosition_24(),
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B::get_offset_of_DeselectAnyButtonSelectedOnHide_25(),
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B::get_offset_of_DeselectAnyButtonSelectedOnShow_26(),
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B::get_offset_of_DisableCanvasWhenHidden_27(),
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B::get_offset_of_DisableGameObjectWhenHidden_28(),
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B::get_offset_of_DisableGraphicRaycasterWhenHidden_29(),
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B::get_offset_of_UseCustomStartAnchoredPosition_30(),
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B::get_offset_of_RenamePrefix_31(),
	UIViewSettings_tF81C8EA0406922B3E128C4CCDAD4AB4FF4F4464B::get_offset_of_RenameSuffix_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5709 = { sizeof (ActivateLoadedScenesNode_t2B570621E8BD27CCACEB81E5A845592C4D04493F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5709[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5710 = { sizeof (ApplicationQuitNode_t9E079DF64C247A00F66E230F4ADD52FD61BF977A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5710[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5711 = { sizeof (BackButtonNode_tED5FB71A4B61EE7ED773B29C5449CD1FF0EFBA28), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5711[1] = 
{
	BackButtonNode_tED5FB71A4B61EE7ED773B29C5449CD1FF0EFBA28::get_offset_of_BackButtonAction_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5712 = { sizeof (BackButtonState_tAE2539790826973E14AB750C9D577738BC052ACE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5712[4] = 
{
	BackButtonState_tAE2539790826973E14AB750C9D577738BC052ACE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5713 = { sizeof (GameEventNode_tA119DE2EADDCBFA3E7D36D02C37A0E2DB5FDCE9F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5713[1] = 
{
	GameEventNode_tA119DE2EADDCBFA3E7D36D02C37A0E2DB5FDCE9F::get_offset_of_GameEvent_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5714 = { sizeof (LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5714[6] = 
{
	LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9::get_offset_of_GetSceneBy_25(),
	LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9::get_offset_of_LoadSceneMode_26(),
	LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9::get_offset_of_AllowSceneActivation_27(),
	LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9::get_offset_of_SceneActivationDelay_28(),
	LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9::get_offset_of_SceneBuildIndex_29(),
	LoadSceneNode_t08E4097054040B108BCBA8284F735F7D403A39A9::get_offset_of_SceneName_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5715 = { sizeof (PortalNode_tC5531962EB486D1D57834E2101E5730889F9C645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5715[2] = 
{
	PortalNode_tC5531962EB486D1D57834E2101E5730889F9C645::get_offset_of_m_gameEvent_25(),
	PortalNode_tC5531962EB486D1D57834E2101E5730889F9C645::get_offset_of_m_portalGraph_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5716 = { sizeof (RandomNode_t88E8FEAF540EF0CC3D0C96C1D6EE927BA51A7FAA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5716[3] = 
{
	RandomNode_t88E8FEAF540EF0CC3D0C96C1D6EE927BA51A7FAA::get_offset_of_m_selectChances_25(),
	RandomNode_t88E8FEAF540EF0CC3D0C96C1D6EE927BA51A7FAA::get_offset_of_U3CMaxChanceU3Ek__BackingField_26(),
	RandomNode_t88E8FEAF540EF0CC3D0C96C1D6EE927BA51A7FAA::get_offset_of_U3CConnectedOutputSocketsU3Ek__BackingField_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5717 = { sizeof (SoundNode_t3F0578EDD4D6945DEC530F14C138AC8AC1691003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5717[2] = 
{
	SoundNode_t3F0578EDD4D6945DEC530F14C138AC8AC1691003::get_offset_of_SoundData_25(),
	SoundNode_t3F0578EDD4D6945DEC530F14C138AC8AC1691003::get_offset_of_SoundAction_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5718 = { sizeof (SoundActions_tC86159034630D7DC2DF75A09AEF63950690144B3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5718[7] = 
{
	SoundActions_tC86159034630D7DC2DF75A09AEF63950690144B3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5719 = { sizeof (TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5719[14] = 
{
	0,
	0,
	0,
	0,
	0,
	TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364::get_offset_of_TargetValue_30(),
	TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364::get_offset_of_AnimateValue_31(),
	TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364::get_offset_of_AnimationDuration_32(),
	TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364::get_offset_of_AnimationEase_33(),
	TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364::get_offset_of_WaitForAnimationToFinish_34(),
	TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364::get_offset_of_m_animationSequence_35(),
	TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364::get_offset_of_m_timerIsActive_36(),
	TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364::get_offset_of_m_timerStart_37(),
	TimeScaleNode_t9E967953D5E9624D707651E4AAF8C6B513F28364::get_offset_of_m_timeDuration_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5720 = { sizeof (U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE), -1, sizeof(U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5720[3] = 
{
	U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE_StaticFields::get_offset_of_U3CU3E9__28_0_1(),
	U3CU3Ec_t62A0543766338BF66F80BC9E8045F3D9F5E9ACFE_StaticFields::get_offset_of_U3CU3E9__28_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5721 = { sizeof (UIDrawerNode_tECEF2D0D957536DC85044CC06F086AB8335FC22F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5721[3] = 
{
	UIDrawerNode_tECEF2D0D957536DC85044CC06F086AB8335FC22F::get_offset_of_DrawerName_25(),
	UIDrawerNode_tECEF2D0D957536DC85044CC06F086AB8335FC22F::get_offset_of_CustomDrawerName_26(),
	UIDrawerNode_tECEF2D0D957536DC85044CC06F086AB8335FC22F::get_offset_of_Action_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5722 = { sizeof (DrawerAction_t2F28EDC561CF29F6E96B1B838E65505EFA29983F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5722[4] = 
{
	DrawerAction_t2F28EDC561CF29F6E96B1B838E65505EFA29983F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5723 = { sizeof (UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5723[8] = 
{
	UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF::get_offset_of_m_onEnterShowViews_25(),
	UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF::get_offset_of_m_onEnterHideViews_26(),
	UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF::get_offset_of_m_onExitShowViews_27(),
	UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF::get_offset_of_m_onExitHideViews_28(),
	UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF::get_offset_of_m_timerIsActive_29(),
	UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF::get_offset_of_m_timerStart_30(),
	UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF::get_offset_of_m_timeDelay_31(),
	UINode_tD9B09AC921BB747962B4D5B1CCA33C3312CAE9CF::get_offset_of_m_activeSocketAfterTimeDelay_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5724 = { sizeof (NodeState_t2D144849186A3DD6BE3A6DD8D16796EC877247EE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5724[3] = 
{
	NodeState_t2D144849186A3DD6BE3A6DD8D16796EC877247EE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5725 = { sizeof (ViewAction_tC1F53B173C52BCF4277C1C2BC392EBF6531D1297)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5725[3] = 
{
	ViewAction_tC1F53B173C52BCF4277C1C2BC392EBF6531D1297::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5726 = { sizeof (U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A), -1, sizeof(U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5726[3] = 
{
	U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A_StaticFields::get_offset_of_U3CU3E9__26_0_1(),
	U3CU3Ec_t1CD2F20B94B7F591D28C749AA2AC14090C9A5D0A_StaticFields::get_offset_of_U3CU3E9__26_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5727 = { sizeof (UnloadSceneNode_t890BCF3F9B7257583DD0A8F153DC0D78B406F944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5727[4] = 
{
	UnloadSceneNode_t890BCF3F9B7257583DD0A8F153DC0D78B406F944::get_offset_of_GetSceneBy_25(),
	UnloadSceneNode_t890BCF3F9B7257583DD0A8F153DC0D78B406F944::get_offset_of_SceneBuildIndex_26(),
	UnloadSceneNode_t890BCF3F9B7257583DD0A8F153DC0D78B406F944::get_offset_of_SceneName_27(),
	UnloadSceneNode_t890BCF3F9B7257583DD0A8F153DC0D78B406F944::get_offset_of_WaitForSceneToUnload_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5728 = { sizeof (WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5728[23] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_GetSceneBy_33(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_WaitFor_34(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_AnyValue_35(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_IgnoreUnityTimescale_36(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_RandomDuration_37(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_Duration_38(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_DurationMax_39(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_DurationMin_40(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_SceneBuildIndex_41(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_GameEvent_42(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_SceneName_43(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_CurrentDuration_44(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_m_timerIsActive_45(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_m_timerStart_46(),
	WaitNode_t475FA7E31B16E4B5C4D8F79995B75E38913FA203::get_offset_of_m_timeDelay_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5729 = { sizeof (WaitType_t46CC2C441C408F4DCA4C233B5500176E8460FA22)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5729[6] = 
{
	WaitType_t46CC2C441C408F4DCA4C233B5500176E8460FA22::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5730 = { sizeof (BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA), -1, sizeof(BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5730[15] = 
{
	BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA_StaticFields::get_offset_of_s_instance_4(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA_StaticFields::get_offset_of_s_applicationIsQuitting_13(),
	BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA_StaticFields::get_offset_of_s_initialized_14(),
	BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA::get_offset_of_BackButtonInputData_15(),
	BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA::get_offset_of_DebugMode_16(),
	BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA::get_offset_of_m_backButtonDisableLevel_17(),
	BackButton_t0448B17947B5AFA7DB501EE0187FC12F6A2C7DDA::get_offset_of_m_lastBackButtonPressTime_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5731 = { sizeof (InputMode_t366797558992CE7DCC33C117AEAD8C8AEEE9FB70)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5731[4] = 
{
	InputMode_t366797558992CE7DCC33C117AEAD8C8AEEE9FB70::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5732 = { sizeof (InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5732[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC::get_offset_of_EnableAlternateInputs_6(),
	InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC::get_offset_of_InputMode_7(),
	InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC::get_offset_of_KeyCode_8(),
	InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC::get_offset_of_KeyCodeAlt_9(),
	InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC::get_offset_of_VirtualButtonName_10(),
	InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC::get_offset_of_VirtualButtonNameAlt_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5733 = { sizeof (KeyToGameEvent_t6A327EB12E2E03B507630190319BCDE26AB5439A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5733[3] = 
{
	KeyToGameEvent_t6A327EB12E2E03B507630190319BCDE26AB5439A::get_offset_of_DebugMode_4(),
	KeyToGameEvent_t6A327EB12E2E03B507630190319BCDE26AB5439A::get_offset_of_InputData_5(),
	KeyToGameEvent_t6A327EB12E2E03B507630190319BCDE26AB5439A::get_offset_of_GameEvent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5734 = { sizeof (UIConnectionTrigger_t258BDC535A9D6BE85D5643658C051A161786E709)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5734[6] = 
{
	UIConnectionTrigger_t258BDC535A9D6BE85D5643658C051A161786E709::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5735 = { sizeof (UIConnection_t53E7ACB7EFF3340C004AEB665984865335084B07), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5735[6] = 
{
	0,
	UIConnection_t53E7ACB7EFF3340C004AEB665984865335084B07::get_offset_of_ButtonCategory_1(),
	UIConnection_t53E7ACB7EFF3340C004AEB665984865335084B07::get_offset_of_ButtonName_2(),
	UIConnection_t53E7ACB7EFF3340C004AEB665984865335084B07::get_offset_of_GameEvent_3(),
	UIConnection_t53E7ACB7EFF3340C004AEB665984865335084B07::get_offset_of_TimeDelay_4(),
	UIConnection_t53E7ACB7EFF3340C004AEB665984865335084B07::get_offset_of_Trigger_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5736 = { sizeof (WeightedConnection_t60BE9E9F1D7B09005AC5CC3B3E0329509AAAF484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5736[2] = 
{
	0,
	WeightedConnection_t60BE9E9F1D7B09005AC5CC3B3E0329509AAAF484::get_offset_of_Weight_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5737 = { sizeof (NamesDatabaseType_t47DE159D2D9D15B56BEA1A627CA3AEE7962A4A86)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5737[5] = 
{
	NamesDatabaseType_t47DE159D2D9D15B56BEA1A627CA3AEE7962A4A86::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5738 = { sizeof (ListOfNames_tD6460E5DEC1D83139F7FCA386EF755D7EB6ABED1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5738[3] = 
{
	ListOfNames_tD6460E5DEC1D83139F7FCA386EF755D7EB6ABED1::get_offset_of_CategoryName_4(),
	ListOfNames_tD6460E5DEC1D83139F7FCA386EF755D7EB6ABED1::get_offset_of_DatabaseType_5(),
	ListOfNames_tD6460E5DEC1D83139F7FCA386EF755D7EB6ABED1::get_offset_of_Names_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5739 = { sizeof (NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5739[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D::get_offset_of_DatabaseType_13(),
	NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D::get_offset_of_CategoryNames_14(),
	NamesDatabase_t30D5844C9F956C46140FCFA6F3F88F97B35FF36D::get_offset_of_Categories_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5740 = { sizeof (U3CU3Ec_t19877D93E3BD9B5FE3C1EC0AF34B2C252794D707), -1, sizeof(U3CU3Ec_t19877D93E3BD9B5FE3C1EC0AF34B2C252794D707_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5740[2] = 
{
	U3CU3Ec_t19877D93E3BD9B5FE3C1EC0AF34B2C252794D707_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t19877D93E3BD9B5FE3C1EC0AF34B2C252794D707_StaticFields::get_offset_of_U3CU3E9__33_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5741 = { sizeof (UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5741[7] = 
{
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7::get_offset_of_Action_0(),
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7::get_offset_of_AnimatorEvents_1(),
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7::get_offset_of_Effect_2(),
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7::get_offset_of_Event_3(),
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7::get_offset_of_GameEvents_4(),
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7::get_offset_of_SoundData_5(),
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7::get_offset_of_m_canvasForEffect_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5742 = { sizeof (U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E), -1, sizeof(U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5742[5] = 
{
	U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E_StaticFields::get_offset_of_U3CU3E9__23_0_1(),
	U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E_StaticFields::get_offset_of_U3CU3E9__25_0_2(),
	U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E_StaticFields::get_offset_of_U3CU3E9__27_0_3(),
	U3CU3Ec_t63BC486140C51DB2C9CB24E1559AFB01EE1F374E_StaticFields::get_offset_of_U3CU3E9__35_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5743 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5743[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5744 = { sizeof (UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5744[16] = 
{
	0,
	0,
	0,
	0,
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B::get_offset_of_Canvas_4(),
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B::get_offset_of_CanvasGroup_5(),
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B::get_offset_of_DisableCanvas_6(),
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B::get_offset_of_DisableGameObject_7(),
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B::get_offset_of_DisableGraphicRaycaster_8(),
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B::get_offset_of_Enabled_9(),
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B::get_offset_of_GraphicRaycaster_10(),
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B::get_offset_of_RectTransform_11(),
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B::get_offset_of_StartAlpha_12(),
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B::get_offset_of_StartPosition_13(),
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B::get_offset_of_StartRotation_14(),
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B::get_offset_of_StartScale_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5745 = { sizeof (UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5745[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB::get_offset_of_AutoSort_6(),
	UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB::get_offset_of_Behavior_7(),
	UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB::get_offset_of_CustomSortingOrder_8(),
	UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB::get_offset_of_SortingSteps_9(),
	UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB::get_offset_of_ParticleSystem_10(),
	UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB::get_offset_of_StopBehavior_11(),
	UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB::get_offset_of_CustomSortingLayer_12(),
	UIEffect_tBF391E23875775E4FD00E9CFE482A7006B0450BB::get_offset_of_m_renderers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5746 = { sizeof (Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5746[17] = 
{
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_AnimationType_0(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_Enabled_1(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_From_2(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_To_3(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_By_4(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_UseCustomFromAndTo_5(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_Vibrato_6(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_Elasticity_7(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_NumberOfLoops_8(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_LoopType_9(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_Direction_10(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_CustomPosition_11(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_EaseType_12(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_Ease_13(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_AnimationCurve_14(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_StartDelay_15(),
	Move_tBD5F95C97A36A939BC8C3C367BB9253A8B0F8BF0::get_offset_of_Duration_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5747 = { sizeof (Rotate_t6F8FF960182701FD1D2F58856066262753CF688A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5747[16] = 
{
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_AnimationType_0(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_Enabled_1(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_From_2(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_To_3(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_By_4(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_UseCustomFromAndTo_5(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_Vibrato_6(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_Elasticity_7(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_NumberOfLoops_8(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_LoopType_9(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_RotateMode_10(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_EaseType_11(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_Ease_12(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_AnimationCurve_13(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_StartDelay_14(),
	Rotate_t6F8FF960182701FD1D2F58856066262753CF688A::get_offset_of_Duration_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5748 = { sizeof (Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5748[15] = 
{
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_AnimationType_0(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_Enabled_1(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_From_2(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_To_3(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_By_4(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_UseCustomFromAndTo_5(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_Vibrato_6(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_Elasticity_7(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_NumberOfLoops_8(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_LoopType_9(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_EaseType_10(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_Ease_11(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_AnimationCurve_12(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_StartDelay_13(),
	Scale_t697FC642414D4F91B28906F5A44839C0697EE9AD::get_offset_of_Duration_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5749 = { sizeof (Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5749[13] = 
{
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_AnimationType_0(),
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_Enabled_1(),
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_From_2(),
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_To_3(),
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_By_4(),
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_UseCustomFromAndTo_5(),
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_NumberOfLoops_6(),
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_LoopType_7(),
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_EaseType_8(),
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_Ease_9(),
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_AnimationCurve_10(),
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_StartDelay_11(),
	Fade_tC7463BA87B9CF43DBFA496F77FCA8FCDA1876D85::get_offset_of_Duration_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5750 = { sizeof (AnimationAction_t8AE7A333593BDCDFDDBA25BA7B6AC165078DDE8F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5750[5] = 
{
	AnimationAction_t8AE7A333593BDCDFDDBA25BA7B6AC165078DDE8F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5751 = { sizeof (AnimationType_tD5E6CBA919BA471B2AE3978400DFBFD83760E003)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5751[7] = 
{
	AnimationType_tD5E6CBA919BA471B2AE3978400DFBFD83760E003::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5752 = { sizeof (ButtonAnimationType_t574DD43CF3A425FF5B14A6F3AF4561B5D5EAB33F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5752[4] = 
{
	ButtonAnimationType_t574DD43CF3A425FF5B14A6F3AF4561B5D5EAB33F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5753 = { sizeof (ButtonLoopAnimationType_t50E9901AFE81E2202E3A009B903F0F2131705A12)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5753[3] = 
{
	ButtonLoopAnimationType_t50E9901AFE81E2202E3A009B903F0F2131705A12::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5754 = { sizeof (Direction_t4CA8328698AB79AE745CCB89105C6D8DB8346C6F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5754[15] = 
{
	Direction_t4CA8328698AB79AE745CCB89105C6D8DB8346C6F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5755 = { sizeof (EaseType_t3CACE9D6C8199CA4AB913608DD4C4D6F73A8E932)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5755[3] = 
{
	EaseType_t3CACE9D6C8199CA4AB913608DD4C4D6F73A8E932::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5756 = { sizeof (UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5756[5] = 
{
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C::get_offset_of_AnimationType_0(),
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C::get_offset_of_Move_1(),
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C::get_offset_of_Rotate_2(),
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C::get_offset_of_Scale_3(),
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C::get_offset_of_Fade_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5757 = { sizeof (UIAnimationData_tC39262163A9335ACF2F2E20FAB5E1AC8898F75AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5757[3] = 
{
	UIAnimationData_tC39262163A9335ACF2F2E20FAB5E1AC8898F75AC::get_offset_of_Animation_4(),
	UIAnimationData_tC39262163A9335ACF2F2E20FAB5E1AC8898F75AC::get_offset_of_Category_5(),
	UIAnimationData_tC39262163A9335ACF2F2E20FAB5E1AC8898F75AC::get_offset_of_Name_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5758 = { sizeof (UIAnimationDatabase_tB406B517A2E003ABEFCE8F9C27E3CACBABAC7987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5758[4] = 
{
	UIAnimationDatabase_tB406B517A2E003ABEFCE8F9C27E3CACBABAC7987::get_offset_of_AnimationNames_4(),
	UIAnimationDatabase_tB406B517A2E003ABEFCE8F9C27E3CACBABAC7987::get_offset_of_Database_5(),
	UIAnimationDatabase_tB406B517A2E003ABEFCE8F9C27E3CACBABAC7987::get_offset_of_DatabaseName_6(),
	UIAnimationDatabase_tB406B517A2E003ABEFCE8F9C27E3CACBABAC7987::get_offset_of_DataType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5759 = { sizeof (U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A), -1, sizeof(U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5759[3] = 
{
	U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A_StaticFields::get_offset_of_U3CU3E9__15_0_1(),
	U3CU3Ec_t45A802F9F7F4DF77B12B8C2D74EEC9B72DBF124A_StaticFields::get_offset_of_U3CU3E9__17_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5760 = { sizeof (UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5), -1, sizeof(UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5760[9] = 
{
	0,
	0,
	0,
	UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5_StaticFields::get_offset_of_s_instance_7(),
	UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5::get_offset_of_Show_8(),
	UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5::get_offset_of_Hide_9(),
	UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5::get_offset_of_Loop_10(),
	UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5::get_offset_of_Punch_11(),
	UIAnimations_tBAAC15BCACC939558EFA45A2D4840A592D34E4B5::get_offset_of_State_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5761 = { sizeof (UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5761[3] = 
{
	UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E::get_offset_of_DatabaseNames_0(),
	UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E::get_offset_of_DatabaseType_1(),
	UIAnimationsDatabase_t68CE627D3B9AB9C53D6DC91E3423EC089F5F7C1E::get_offset_of_Databases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5762 = { sizeof (U3CU3Ec_t8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E), -1, sizeof(U3CU3Ec_t8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5762[2] = 
{
	U3CU3Ec_t8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8F5FF4D8625F3931BFB1419A3005322CF0C0FD1E_StaticFields::get_offset_of_U3CU3E9__13_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5763 = { sizeof (UIAnimator_tAEE3CDC93D85154346DB11446DE396889AFB4777), -1, sizeof(UIAnimator_tAEE3CDC93D85154346DB11446DE396889AFB4777_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5763[18] = 
{
	UIAnimator_tAEE3CDC93D85154346DB11446DE396889AFB4777_StaticFields::get_offset_of_DEFAULT_START_POSITION_0(),
	UIAnimator_tAEE3CDC93D85154346DB11446DE396889AFB4777_StaticFields::get_offset_of_DEFAULT_START_ROTATION_1(),
	UIAnimator_tAEE3CDC93D85154346DB11446DE396889AFB4777_StaticFields::get_offset_of_DEFAULT_START_SCALE_2(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5764 = { sizeof (U3CU3Ec__DisplayClass19_0_t2F9D7F22B9250B7B006049317BE992248CC3DF80), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5764[2] = 
{
	U3CU3Ec__DisplayClass19_0_t2F9D7F22B9250B7B006049317BE992248CC3DF80::get_offset_of_onStartCallback_0(),
	U3CU3Ec__DisplayClass19_0_t2F9D7F22B9250B7B006049317BE992248CC3DF80::get_offset_of_onCompleteCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5765 = { sizeof (U3CU3Ec__DisplayClass20_0_t66065B2A72E9B51E7F49E46EBEB76377B478C8E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5765[2] = 
{
	U3CU3Ec__DisplayClass20_0_t66065B2A72E9B51E7F49E46EBEB76377B478C8E6::get_offset_of_onStartCallback_0(),
	U3CU3Ec__DisplayClass20_0_t66065B2A72E9B51E7F49E46EBEB76377B478C8E6::get_offset_of_onCompleteCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5766 = { sizeof (U3CU3Ec__DisplayClass21_0_tFF9AA632B7DAFDC7C547D8BA816362D828AFC3C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5766[2] = 
{
	U3CU3Ec__DisplayClass21_0_tFF9AA632B7DAFDC7C547D8BA816362D828AFC3C4::get_offset_of_onStartCallback_0(),
	U3CU3Ec__DisplayClass21_0_tFF9AA632B7DAFDC7C547D8BA816362D828AFC3C4::get_offset_of_onCompleteCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5767 = { sizeof (U3CU3Ec__DisplayClass22_0_t88D2AF56ED233D6FC9E857124C31704C3DFFAFB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5767[2] = 
{
	U3CU3Ec__DisplayClass22_0_t88D2AF56ED233D6FC9E857124C31704C3DFFAFB7::get_offset_of_onStartCallback_0(),
	U3CU3Ec__DisplayClass22_0_t88D2AF56ED233D6FC9E857124C31704C3DFFAFB7::get_offset_of_onCompleteCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5768 = { sizeof (U3CU3Ec__DisplayClass23_0_t29DB0CCABAC978920A0873CA9297F62A1247E9BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5768[3] = 
{
	U3CU3Ec__DisplayClass23_0_t29DB0CCABAC978920A0873CA9297F62A1247E9BB::get_offset_of_onCompleteCallback_0(),
	U3CU3Ec__DisplayClass23_0_t29DB0CCABAC978920A0873CA9297F62A1247E9BB::get_offset_of_onStartCallback_1(),
	U3CU3Ec__DisplayClass23_0_t29DB0CCABAC978920A0873CA9297F62A1247E9BB::get_offset_of_loopSequence_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5769 = { sizeof (U3CU3Ec__DisplayClass24_0_tF1206CFBA406C0094C223635951927705FD4D38F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5769[3] = 
{
	U3CU3Ec__DisplayClass24_0_tF1206CFBA406C0094C223635951927705FD4D38F::get_offset_of_onCompleteCallback_0(),
	U3CU3Ec__DisplayClass24_0_tF1206CFBA406C0094C223635951927705FD4D38F::get_offset_of_onStartCallback_1(),
	U3CU3Ec__DisplayClass24_0_tF1206CFBA406C0094C223635951927705FD4D38F::get_offset_of_loopSequence_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5770 = { sizeof (U3CU3Ec__DisplayClass25_0_tB0B9378FA586263B15202387D5547E8E52BCAC0D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5770[3] = 
{
	U3CU3Ec__DisplayClass25_0_tB0B9378FA586263B15202387D5547E8E52BCAC0D::get_offset_of_onCompleteCallback_0(),
	U3CU3Ec__DisplayClass25_0_tB0B9378FA586263B15202387D5547E8E52BCAC0D::get_offset_of_onStartCallback_1(),
	U3CU3Ec__DisplayClass25_0_tB0B9378FA586263B15202387D5547E8E52BCAC0D::get_offset_of_loopSequence_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5771 = { sizeof (U3CU3Ec__DisplayClass26_0_t0414A0654B7904B5A7AE86341AD7C08014C81770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5771[3] = 
{
	U3CU3Ec__DisplayClass26_0_t0414A0654B7904B5A7AE86341AD7C08014C81770::get_offset_of_onCompleteCallback_0(),
	U3CU3Ec__DisplayClass26_0_t0414A0654B7904B5A7AE86341AD7C08014C81770::get_offset_of_onStartCallback_1(),
	U3CU3Ec__DisplayClass26_0_t0414A0654B7904B5A7AE86341AD7C08014C81770::get_offset_of_loopSequence_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5772 = { sizeof (U3CU3Ec__DisplayClass27_0_t5CDCAEBF430A54FC491777998136B83E42D607B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5772[5] = 
{
	U3CU3Ec__DisplayClass27_0_t5CDCAEBF430A54FC491777998136B83E42D607B4::get_offset_of_onStartCallback_0(),
	U3CU3Ec__DisplayClass27_0_t5CDCAEBF430A54FC491777998136B83E42D607B4::get_offset_of_target_1(),
	U3CU3Ec__DisplayClass27_0_t5CDCAEBF430A54FC491777998136B83E42D607B4::get_offset_of_startValue_2(),
	U3CU3Ec__DisplayClass27_0_t5CDCAEBF430A54FC491777998136B83E42D607B4::get_offset_of_onCompleteCallback_3(),
	U3CU3Ec__DisplayClass27_0_t5CDCAEBF430A54FC491777998136B83E42D607B4::get_offset_of_U3CU3E9__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5773 = { sizeof (U3CU3Ec__DisplayClass28_0_t6E537E419A8AC0CBFE5800DA06310C218426BF9C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5773[5] = 
{
	U3CU3Ec__DisplayClass28_0_t6E537E419A8AC0CBFE5800DA06310C218426BF9C::get_offset_of_onStartCallback_0(),
	U3CU3Ec__DisplayClass28_0_t6E537E419A8AC0CBFE5800DA06310C218426BF9C::get_offset_of_target_1(),
	U3CU3Ec__DisplayClass28_0_t6E537E419A8AC0CBFE5800DA06310C218426BF9C::get_offset_of_startValue_2(),
	U3CU3Ec__DisplayClass28_0_t6E537E419A8AC0CBFE5800DA06310C218426BF9C::get_offset_of_onCompleteCallback_3(),
	U3CU3Ec__DisplayClass28_0_t6E537E419A8AC0CBFE5800DA06310C218426BF9C::get_offset_of_U3CU3E9__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5774 = { sizeof (U3CU3Ec__DisplayClass29_0_tDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5774[5] = 
{
	U3CU3Ec__DisplayClass29_0_tDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43::get_offset_of_onStartCallback_0(),
	U3CU3Ec__DisplayClass29_0_tDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43::get_offset_of_target_1(),
	U3CU3Ec__DisplayClass29_0_tDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43::get_offset_of_startValue_2(),
	U3CU3Ec__DisplayClass29_0_tDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43::get_offset_of_onCompleteCallback_3(),
	U3CU3Ec__DisplayClass29_0_tDAAA22E45C8273BAF518AEE7D7A850AE78CC2C43::get_offset_of_U3CU3E9__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5775 = { sizeof (U3CU3Ec__DisplayClass30_0_t790946858609600312966E9E1AB3DA69BA830567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5775[2] = 
{
	U3CU3Ec__DisplayClass30_0_t790946858609600312966E9E1AB3DA69BA830567::get_offset_of_onStartCallback_0(),
	U3CU3Ec__DisplayClass30_0_t790946858609600312966E9E1AB3DA69BA830567::get_offset_of_onCompleteCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5776 = { sizeof (U3CU3Ec__DisplayClass31_0_tF2CA7158AD49E2ACF9E72CC50115C62B4420154B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5776[2] = 
{
	U3CU3Ec__DisplayClass31_0_tF2CA7158AD49E2ACF9E72CC50115C62B4420154B::get_offset_of_onStartCallback_0(),
	U3CU3Ec__DisplayClass31_0_tF2CA7158AD49E2ACF9E72CC50115C62B4420154B::get_offset_of_onCompleteCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5777 = { sizeof (U3CU3Ec__DisplayClass32_0_t0CB4F51C7F781AA37A05C2C5BE4088FAE60498F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5777[2] = 
{
	U3CU3Ec__DisplayClass32_0_t0CB4F51C7F781AA37A05C2C5BE4088FAE60498F1::get_offset_of_onStartCallback_0(),
	U3CU3Ec__DisplayClass32_0_t0CB4F51C7F781AA37A05C2C5BE4088FAE60498F1::get_offset_of_onCompleteCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5778 = { sizeof (U3CU3Ec__DisplayClass33_0_t17EA18FC0EE94072B29A30295F7B0779E7A624B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5778[2] = 
{
	U3CU3Ec__DisplayClass33_0_t17EA18FC0EE94072B29A30295F7B0779E7A624B4::get_offset_of_onStartCallback_0(),
	U3CU3Ec__DisplayClass33_0_t17EA18FC0EE94072B29A30295F7B0779E7A624B4::get_offset_of_onCompleteCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5779 = { sizeof (CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3), -1, sizeof(CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5779[9] = 
{
	CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields::get_offset_of_None_0(),
	CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields::get_offset_of_Up_1(),
	CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields::get_offset_of_Down_2(),
	CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields::get_offset_of_Right_3(),
	CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields::get_offset_of_Left_4(),
	CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields::get_offset_of_UpRight_5(),
	CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields::get_offset_of_UpLeft_6(),
	CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields::get_offset_of_DownRight_7(),
	CardinalDirection_t74711FA9D8D99C45B61F0E1D66CE6C8ECA3CABD3_StaticFields::get_offset_of_DownLeft_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5780 = { sizeof (GestureType_t7B2018D460536412C99F26FB3C8D0433610104B1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5780[4] = 
{
	GestureType_t7B2018D460536412C99F26FB3C8D0433610104B1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5781 = { sizeof (SimpleSwipe_t3BEB318CEBE340D43602826A0843D0D23C848473)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5781[6] = 
{
	SimpleSwipe_t3BEB318CEBE340D43602826A0843D0D23C848473::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5782 = { sizeof (Swipe_tE682C3C53AFA8D69B4A374881EFCFAD97CCCD83D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5782[10] = 
{
	Swipe_tE682C3C53AFA8D69B4A374881EFCFAD97CCCD83D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5783 = { sizeof (GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5783[9] = 
{
	GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E::get_offset_of_DebugMode_4(),
	GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E::get_offset_of_GlobalListener_5(),
	GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E::get_offset_of_OverrideTarget_6(),
	GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E::get_offset_of_TargetGameObject_7(),
	GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E::get_offset_of_GestureType_8(),
	GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E::get_offset_of_SwipeDirection_9(),
	GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E::get_offset_of_OnGestureEvent_10(),
	GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E::get_offset_of_OnGestureAction_11(),
	GestureListener_t4A01A03F99D9617AE1E89514AF7591AA64CEDA4E::get_offset_of_GameEvents_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5784 = { sizeof (U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5), -1, sizeof(U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5784[3] = 
{
	U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5_StaticFields::get_offset_of_U3CU3E9__13_0_1(),
	U3CU3Ec_tDCC3A70F578F5ED98478E8D9772EE5137FDA7CA5_StaticFields::get_offset_of_U3CU3E9__14_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5785 = { sizeof (U3CSendGameEventsInTheNextFrameU3Ed__25_tA18E59F7918146A0837C52E421A4DC83C67C3F23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5785[3] = 
{
	U3CSendGameEventsInTheNextFrameU3Ed__25_tA18E59F7918146A0837C52E421A4DC83C67C3F23::get_offset_of_U3CU3E1__state_0(),
	U3CSendGameEventsInTheNextFrameU3Ed__25_tA18E59F7918146A0837C52E421A4DC83C67C3F23::get_offset_of_U3CU3E2__current_1(),
	U3CSendGameEventsInTheNextFrameU3Ed__25_tA18E59F7918146A0837C52E421A4DC83C67C3F23::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5786 = { sizeof (SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0), -1, sizeof(SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5786[4] = 
{
	SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0::get_offset_of_U3CWasModifiedU3Ek__BackingField_0(),
	0,
	SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0_StaticFields::get_offset_of_Fields_2(),
	SimulatedTouch_t16FAD0A464067569D42CDB2B121305F3D6D23BD0::get_offset_of_m_touch_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5787 = { sizeof (TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8), -1, sizeof(TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5787[13] = 
{
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8_StaticFields::get_offset_of_s_instance_4(),
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8_StaticFields::get_offset_of_U3CApplicationIsQuittingU3Ek__BackingField_5(),
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8::get_offset_of_U3CTouchInProgressU3Ek__BackingField_6(),
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8::get_offset_of_OnTapAction_7(),
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8::get_offset_of_OnLongTapAction_8(),
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8::get_offset_of_OnSwipeAction_9(),
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8::get_offset_of_m_currentSwipe_10(),
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8::get_offset_of_m_swipeEnded_11(),
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8::get_offset_of_m_currentTouchInfo_12(),
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8::get_offset_of_m_touches_13(),
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8::get_offset_of_m_touch_14(),
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8::get_offset_of_m_pointerEventData_15(),
	TouchDetector_t6E812123D8251A11D1AF49732F0D2A9E4A6777C8::get_offset_of_m_raycastResults_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5788 = { sizeof (TouchHelper_t3D8F618D46469C95E7BF3D16CF42CF3115BD630E), -1, sizeof(TouchHelper_t3D8F618D46469C95E7BF3D16CF42CF3115BD630E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5788[2] = 
{
	TouchHelper_t3D8F618D46469C95E7BF3D16CF42CF3115BD630E_StaticFields::get_offset_of_s_lastSimulatedTouch_0(),
	TouchHelper_t3D8F618D46469C95E7BF3D16CF42CF3115BD630E_StaticFields::get_offset_of_s_touches_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5789 = { sizeof (TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5789[18] = 
{
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_Touch_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_Direction_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_RawDirection_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_StartPosition_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_EndPosition_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_Velocity_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_StartTime_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_EndTime_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_Duration_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_Tap_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_LongTap_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_Distance_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_LongestDistance_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_GameObject_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_DraggedObject_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_CurrentTouchPosition_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_PreviousTouchPosition_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t498F97DAD203F630B0C4E27944F9DEDAE95BE9A8::get_offset_of_TouchDeltaTime_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5790 = { sizeof (TouchInfoEvent_t7C133938279F0BC34E525D4C9F62B88A6B206429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5791 = { sizeof (TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459), -1, sizeof(TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5791[10] = 
{
	0,
	TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459_StaticFields::get_offset_of_s_instance_5(),
	0,
	0,
	0,
	0,
	0,
	0,
	TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459::get_offset_of_LongTapDuration_12(),
	TouchySettings_tC0D66C57A1895ECBB5447AE365175AE4FAA3F459::get_offset_of_SwipeLength_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5792 = { sizeof (AudioData_t5195938BA169F80D32B29A92FE22CDBD99F346D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5792[5] = 
{
	0,
	0,
	0,
	AudioData_t5195938BA169F80D32B29A92FE22CDBD99F346D6::get_offset_of_AudioClip_3(),
	AudioData_t5195938BA169F80D32B29A92FE22CDBD99F346D6::get_offset_of_Weight_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5793 = { sizeof (SoundSource_t7FA55BAC1FEA972D82594371862BBA8698B3D3E9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5793[4] = 
{
	SoundSource_t7FA55BAC1FEA972D82594371862BBA8698B3D3E9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5794 = { sizeof (SoundDatabase_tD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5794[4] = 
{
	SoundDatabase_tD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF::get_offset_of_DatabaseName_4(),
	SoundDatabase_tD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF::get_offset_of_OutputAudioMixerGroup_5(),
	SoundDatabase_tD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF::get_offset_of_SoundNames_6(),
	SoundDatabase_tD2CD4D750A6C4BF0A6FB7A6592BD0C8836BF73EF::get_offset_of_Database_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5795 = { sizeof (U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0), -1, sizeof(U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5795[5] = 
{
	U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_StaticFields::get_offset_of_U3CU3E9__17_0_1(),
	U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_StaticFields::get_offset_of_U3CU3E9__17_1_2(),
	U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_StaticFields::get_offset_of_U3CU3E9__18_0_3(),
	U3CU3Ec_tB3C8DF2468C10DA4EDE6FF26A6567DA74B6A22E0_StaticFields::get_offset_of_U3CU3E9__20_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5796 = { sizeof (SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5796[30] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_DatabaseName_19(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_SoundName_20(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_IgnoreListenerPause_21(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_Volume_22(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_Pitch_23(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_SpatialBlend_24(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_Loop_25(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_Mode_26(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_ResetSequenceAfterInactiveTime_27(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_SequenceResetTime_28(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_Sounds_29(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_m_lastPlayedSoundsIndex_30(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_m_lastPlayedSoundTime_31(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_m_playedSounds_32(),
	SoundGroupData_t9CA54942DA1AC5375A6C212C1210DAA5241BE37B::get_offset_of_m_lastPlayedAudioData_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5797 = { sizeof (PlayMode_tC2F7EE563955D38E4EFAC0C1EBFABF1E0C622015)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5797[3] = 
{
	PlayMode_tC2F7EE563955D38E4EFAC0C1EBFABF1E0C622015::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5798 = { sizeof (SoundyDatabase_t053E1E9D0784E5A630FB918EFC5A7ACAA1D45656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5798[2] = 
{
	SoundyDatabase_t053E1E9D0784E5A630FB918EFC5A7ACAA1D45656::get_offset_of_DatabaseNames_4(),
	SoundyDatabase_t053E1E9D0784E5A630FB918EFC5A7ACAA1D45656::get_offset_of_SoundDatabases_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5799 = { sizeof (SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55), -1, sizeof(SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5799[11] = 
{
	SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55_StaticFields::get_offset_of_s_database_4(),
	SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55::get_offset_of_m_transform_5(),
	SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55::get_offset_of_m_followTarget_6(),
	SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55::get_offset_of_m_audioSource_7(),
	SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55::get_offset_of_m_inUse_8(),
	SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55::get_offset_of_m_playProgress_9(),
	SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55::get_offset_of_m_isPaused_10(),
	SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55::get_offset_of_m_isMuted_11(),
	SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55::get_offset_of_m_lastPlayedTime_12(),
	SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55::get_offset_of_m_isPlaying_13(),
	SoundyController_t88A0FA707F3D1E1F4D0EEA505BFF7FBFC8961F55::get_offset_of_m_autoPaused_14(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
