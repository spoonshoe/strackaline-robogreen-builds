﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// MUJAEshscmeXKPKBeiQXLdyHSgZ
struct MUJAEshscmeXKPKBeiQXLdyHSgZ_t94E1E4243BF2917620850C52F104B1ED4A91DEF2;
// Rewired.ActionElementMap
struct ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7;
// Rewired.Controller
struct Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671;
// Rewired.Controller/Button[]
struct ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C;
// Rewired.Controller/Extension
struct Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD;
// Rewired.ControllerDataUpdater
struct ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F;
// Rewired.ControllerMap
struct ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB;
// Rewired.HardwareControllerMap_Game
struct HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3;
// Rewired.HardwareJoystickMap_InputManager
struct HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0;
// Rewired.IControllerTemplate
struct IControllerTemplate_tB89722FA815A90C11CB2E71E1861B539F69285F3;
// Rewired.IControllerTemplate[]
struct IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6;
// Rewired.Utils.Classes.Data.AList`1<Rewired.IControllerTemplate>
struct AList_1_t974A66EA135B0300CE05935362C6C7C736059320;
// Rewired.Utils.Classes.Data.AList`1<System.Object>
struct AList_1_t6040A9B07E2104841A5B217A9311F51920A362AE;
// Rewired.Utils.Classes.Data.AList`1<rIcvLcWCuQghgaaiofvKjSvndJX/mGZguqdUJPvKvEhsdVIRYohlOty>
struct AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.ArgumentException
struct ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IEqualityComparer`1<Rewired.IControllerTemplate>
struct IEqualityComparer_1_tFBDFD26FDD792573AE3BD418C2A70F783F933B7F;
// System.Collections.Generic.IEqualityComparer`1<rIcvLcWCuQghgaaiofvKjSvndJX/mGZguqdUJPvKvEhsdVIRYohlOty>
struct IEqualityComparer_1_t8DB63C54A80BAB48EB99D738D6AFB17D2F1C04BC;
// System.Collections.Generic.IList`1<Rewired.Controller/Element>
struct IList_1_t743C6DA41FA4F41F7DDEC5C049DA8C51F315819C;
// System.Collections.Generic.IList`1<Rewired.IControllerTemplate>
struct IList_1_tE8EC6D08D356EC0C3DE0171A08C305CC97DB9752;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa/QnHiHbnYGCBSFowNhmjvNvYtpXM>
struct List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller/Button>
struct ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller/Element>
struct ReadOnlyCollection_1_tBF1621D3F7F9871C04D918E0DF726F42C36F91F4;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.IControllerTemplate>
struct ReadOnlyCollection_1_t29628C2D08376268FE32FB6C6D7854BE606C1802;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.Func`2<Rewired.BridgedControllerHWInfo,Rewired.HardwareJoystickMap_InputManager>
struct Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF;
// System.Func`3<Rewired.Controller,System.Guid,System.Boolean>
struct Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE;
// System.Func`3<Rewired.Controller,System.Type,System.Boolean>
struct Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.NotImplementedException
struct NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// qQSKQqvJRxFNPvSjTomQuZOntrH/lNgIgGeozTFHoKvpOBlVjbAtvLyd
struct lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489;
// qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa
struct tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773;
// qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa/QnHiHbnYGCBSFowNhmjvNvYtpXM
struct QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4;
// qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa/QnHiHbnYGCBSFowNhmjvNvYtpXM[]
struct QnHiHbnYGCBSFowNhmjvNvYtpXMU5BU5D_t6AA1991911A35BF88BDCFAFC53BA478A59903B87;
// rIcvLcWCuQghgaaiofvKjSvndJX
struct rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224;
// rIcvLcWCuQghgaaiofvKjSvndJX/mGZguqdUJPvKvEhsdVIRYohlOty
struct mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820;
// rIcvLcWCuQghgaaiofvKjSvndJX/mGZguqdUJPvKvEhsdVIRYohlOty[]
struct mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4;
// vLEBhncKpOPFOKOXDdCbdgTKhyd
struct vLEBhncKpOPFOKOXDdCbdgTKhyd_tA01C4EADEDC175C2FAC271C09D7E5245A52EFCBB;
// xvUPNQnWmcdqDjulQTyAIyRAxwG
struct xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032;

extern RuntimeClass* AList_1_t974A66EA135B0300CE05935362C6C7C736059320_il2cpp_TypeInfo_var;
extern RuntimeClass* AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
extern RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Guid_t_il2cpp_TypeInfo_var;
extern RuntimeClass* IList_1_tE8EC6D08D356EC0C3DE0171A08C305CC97DB9752_il2cpp_TypeInfo_var;
extern RuntimeClass* IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF_il2cpp_TypeInfo_var;
extern RuntimeClass* Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var;
extern RuntimeClass* ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0_il2cpp_TypeInfo_var;
extern RuntimeClass* NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4_il2cpp_TypeInfo_var;
extern RuntimeClass* QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820_il2cpp_TypeInfo_var;
extern RuntimeClass* szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B_il2cpp_TypeInfo_var;
extern RuntimeClass* tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_il2cpp_TypeInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_t2294A85ED0D53C43A0934AE9A86EBFD96609A170____U24U24method0x600327aU2D1_6_FieldInfo_var;
extern String_t* _stringLiteral04BAED45CE4E417FEF8C1A7402678C4F210373E8;
extern String_t* _stringLiteral46CF8DB16C69AEA191AC4FBC8E05DF3952FF60C4;
extern String_t* _stringLiteral834D53EC78A728EB6B4068BB273881DCCC16E645;
extern String_t* _stringLiteralBECD2AA75C348B6C1604A5F8AA72A306236BD7C2;
extern String_t* _stringLiteralD69C47B2876BE9D0D3ED8E67AAB6F47CAD39216F;
extern String_t* _stringLiteralF6DE2FBED7D1AA24173C74F69B1B226EFC989186;
extern const RuntimeMethod* AList_1_Add_m312C99F6192A18AB1E766E10CDFCC198694273D1_RuntimeMethod_var;
extern const RuntimeMethod* AList_1_Add_mFBC6CE8CADA220DFA6C8C2E04DE156D55F6FA850_RuntimeMethod_var;
extern const RuntimeMethod* AList_1_Remove_m4E877B89C4BEEB27D88C72D27D173CE7FDB3B8BE_RuntimeMethod_var;
extern const RuntimeMethod* AList_1__ctor_m185708896B55423867CBE384A91B2A3F155EE9BB_RuntimeMethod_var;
extern const RuntimeMethod* AList_1__ctor_mAC4A718F851BAFFFD2BFD4F2294903DF5A752CBA_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m379D7CE5CF8DEC6CDDD7E4C2261E6E81CA1D9E88_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_mFF720D6CA965549F775ABF1FB5A65662C057E67C_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mB7126049CA5EEEB8853C16CDD3ABD1D0447E0B65_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m4781C1E7D158FE73AE5792C98211036217CDD9B8_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var;
extern const RuntimeMethod* QnHiHbnYGCBSFowNhmjvNvYtpXM_rQVCTzxWRBTHwULULWYBtzSTRee_m022C9BF3BD1E87FE37078791C6135EC63CC7E491_RuntimeMethod_var;
extern const RuntimeMethod* rIcvLcWCuQghgaaiofvKjSvndJX__ctor_m1BB1C02B6BB5F46ACE01FA603A6CCAA4DDA418C0_RuntimeMethod_var;
extern const RuntimeMethod* tirTbIgerWCchDwImTBhEXMbbyDC_IIConZhTlDhyrUEUmJUbhmrJubd_m48AC53F1EA2DFC16CEF1B538F05F18F00EDB012E_RuntimeMethod_var;
extern const uint32_t QnHiHbnYGCBSFowNhmjvNvYtpXM_rQVCTzxWRBTHwULULWYBtzSTRee_m022C9BF3BD1E87FE37078791C6135EC63CC7E491_MetadataUsageId;
extern const uint32_t mGZguqdUJPvKvEhsdVIRYohlOty_HfTIrZGXLVOHtJVamKOSesGBJhAd_mBF6D59118267024E5CACA02B7DFF3E12FA51B8E8_MetadataUsageId;
extern const uint32_t mGZguqdUJPvKvEhsdVIRYohlOty__ctor_m544770ECC483EEE5DD5D78E02667618ACD3D11D6_MetadataUsageId;
extern const uint32_t mGZguqdUJPvKvEhsdVIRYohlOty_yhpcGPaRrkCsNfCHablLblmduzKL_m0C4D04CA4019577CD998EA769D6CD52F64459055_MetadataUsageId;
extern const uint32_t rIcvLcWCuQghgaaiofvKjSvndJX_TdkSBPFMSQkEGSjsYWogNISacuj_m263D429CB5D431B98BFCD789BC3792DF3A5A6952_MetadataUsageId;
extern const uint32_t rIcvLcWCuQghgaaiofvKjSvndJX__ctor_m1BB1C02B6BB5F46ACE01FA603A6CCAA4DDA418C0_MetadataUsageId;
extern const uint32_t rIcvLcWCuQghgaaiofvKjSvndJX_iEpVvatmquULZMNgmRjzSSryqGm_m9AF089DFAB7D07752D4C2A9D982C9F08A6C373DF_MetadataUsageId;
extern const uint32_t sSmyCZxlYXGixusuhxfHceqeCTr_VikOOOAEBIdZweuHRjgOuRqhkErr_mEBC31378984490FF5F558512123D623710766A29_MetadataUsageId;
extern const uint32_t sSmyCZxlYXGixusuhxfHceqeCTr_nbgZHGrFJfzAlaIQqyHguklefEZ_m8D80E0680D51BFA6E0C65F5FC7E34380DEFF7820_MetadataUsageId;
extern const uint32_t szJmXpILCMpJYoBHnspxyLIJAob_Equals_m696F1EA3F652019ADF7F2E0DCAE7BE71F63C469D_MetadataUsageId;
extern const uint32_t szJmXpILCMpJYoBHnspxyLIJAob_GetHashCode_m1CE2FB9D12F38DE1DC231A8C04D8AA6962CEEE75_MetadataUsageId;
extern const uint32_t tLqZqETDWvmFaUyFhoSscPGCHa_EOMjUgrIyGUAqNzWhSmDvzQVLte_m1AE5CC3FC40DADA74E1773BADCEF84F6CCCA334F_MetadataUsageId;
extern const uint32_t tLqZqETDWvmFaUyFhoSscPGCHa__ctor_m2D920DC9A17827969D4E71878C28273B958EFD60_MetadataUsageId;
extern const uint32_t tLqZqETDWvmFaUyFhoSscPGCHa_hDjCQPMEgCKqoDwfDwePyicJXwT_m8AF1A51683C8A6F250D62F88B0ED4B3967D79407_MetadataUsageId;
extern const uint32_t tLqZqETDWvmFaUyFhoSscPGCHa_hLFpabYsqKQzNRTSZlJUSavKbbnc_mFAE0A9186D75E723B30CE54A43C20048FBC9C8BF_MetadataUsageId;
extern const uint32_t tLqZqETDWvmFaUyFhoSscPGCHa_mRQRPVJljvuBdBeduwoolvGdjPA_m9E3A1DC07C170F846ACE888531DCEF41468C10DB_MetadataUsageId;
extern const uint32_t tirTbIgerWCchDwImTBhEXMbbyDC_IIConZhTlDhyrUEUmJUbhmrJubd_m48AC53F1EA2DFC16CEF1B538F05F18F00EDB012E_MetadataUsageId;
extern const uint32_t tirTbIgerWCchDwImTBhEXMbbyDC_MzJXkgGxfrtJiysYOBOEvYonDZs_m67F29672BD9863727FA97687C38BFD58BAD165F5_MetadataUsageId;
extern const uint32_t tirTbIgerWCchDwImTBhEXMbbyDC__cctor_mC2DC9B7831464ECC758D76428547E79134B1E1E7_MetadataUsageId;
extern const uint32_t tirTbIgerWCchDwImTBhEXMbbyDC_aPDcQrAxDVfpHqyPrEgiDuiWmsWb_m5DA589A3287320E721A735A3978A4046BFA0EB2F_MetadataUsageId;
extern const uint32_t tirTbIgerWCchDwImTBhEXMbbyDC_qiheowigyVKPVHALwnyqlItUxgJ_mD57CB3090D7F727F87CF4D3C1ECA15C5C3FA4D63_MetadataUsageId;
extern const uint32_t wACwxZjEFhAfUzXpGGPyAhJldeo_TsrpdEkqOGaRPyBjAPFdswjyuxG_m03DCFF4405EB60D229D590BEFE90248D47D463DA_MetadataUsageId;
extern const uint32_t wACwxZjEFhAfUzXpGGPyAhJldeo_kVeCKTaEcflZOVqHSNSieChnKJA_mDF438FB8F0475AD1740BA7D2BF35D611DD8F84A6_MetadataUsageId;
extern const uint32_t wACwxZjEFhAfUzXpGGPyAhJldeo_nbgZHGrFJfzAlaIQqyHguklefEZ_m65A3E6705BFA9A60D9D7FE15169969760CDE062D_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
struct mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ALIST_1_T974A66EA135B0300CE05935362C6C7C736059320_H
#define ALIST_1_T974A66EA135B0300CE05935362C6C7C736059320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.AList`1<Rewired.IControllerTemplate>
struct  AList_1_t974A66EA135B0300CE05935362C6C7C736059320  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> Rewired.Utils.Classes.Data.AList`1::amBrOsbBxECFodcMKICGbBtYcvQ
	RuntimeObject* ___amBrOsbBxECFodcMKICGbBtYcvQ_2;
	// T[] Rewired.Utils.Classes.Data.AList`1::_items
	IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6* ____items_3;
	// System.Int32 Rewired.Utils.Classes.Data.AList`1::sBiHWicpYhmNgEiaLGeoNmCayd
	int32_t ___sBiHWicpYhmNgEiaLGeoNmCayd_4;
	// System.Int32 Rewired.Utils.Classes.Data.AList`1::_count
	int32_t ____count_5;
	// System.Int32 Rewired.Utils.Classes.Data.AList`1::sEfkOujJtwAFcdBLZvZKNLKcsRow
	int32_t ___sEfkOujJtwAFcdBLZvZKNLKcsRow_6;
	// System.Boolean Rewired.Utils.Classes.Data.AList`1::tWSNHwJKDfFOvkjLdLwgxBPqlUEt
	bool ___tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7;
	// System.Int32 Rewired.Utils.Classes.Data.AList`1::rQpPIjHhnZIZRukMBcQxgFQNluPk
	int32_t ___rQpPIjHhnZIZRukMBcQxgFQNluPk_8;
	// System.Boolean Rewired.Utils.Classes.Data.AList`1::VOtGOZhnqshkcRdadDngatuXTwff
	bool ___VOtGOZhnqshkcRdadDngatuXTwff_9;
	// System.Int32 Rewired.Utils.Classes.Data.AList`1::FrgfpmHlyBElwQPLFJksZSvJtdD
	int32_t ___FrgfpmHlyBElwQPLFJksZSvJtdD_10;
	// System.Object Rewired.Utils.Classes.Data.AList`1::QSVrKCTKhANFtMKLOpoQxMlPWHC
	RuntimeObject * ___QSVrKCTKhANFtMKLOpoQxMlPWHC_11;

public:
	inline static int32_t get_offset_of_amBrOsbBxECFodcMKICGbBtYcvQ_2() { return static_cast<int32_t>(offsetof(AList_1_t974A66EA135B0300CE05935362C6C7C736059320, ___amBrOsbBxECFodcMKICGbBtYcvQ_2)); }
	inline RuntimeObject* get_amBrOsbBxECFodcMKICGbBtYcvQ_2() const { return ___amBrOsbBxECFodcMKICGbBtYcvQ_2; }
	inline RuntimeObject** get_address_of_amBrOsbBxECFodcMKICGbBtYcvQ_2() { return &___amBrOsbBxECFodcMKICGbBtYcvQ_2; }
	inline void set_amBrOsbBxECFodcMKICGbBtYcvQ_2(RuntimeObject* value)
	{
		___amBrOsbBxECFodcMKICGbBtYcvQ_2 = value;
		Il2CppCodeGenWriteBarrier((&___amBrOsbBxECFodcMKICGbBtYcvQ_2), value);
	}

	inline static int32_t get_offset_of__items_3() { return static_cast<int32_t>(offsetof(AList_1_t974A66EA135B0300CE05935362C6C7C736059320, ____items_3)); }
	inline IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6* get__items_3() const { return ____items_3; }
	inline IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6** get_address_of__items_3() { return &____items_3; }
	inline void set__items_3(IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6* value)
	{
		____items_3 = value;
		Il2CppCodeGenWriteBarrier((&____items_3), value);
	}

	inline static int32_t get_offset_of_sBiHWicpYhmNgEiaLGeoNmCayd_4() { return static_cast<int32_t>(offsetof(AList_1_t974A66EA135B0300CE05935362C6C7C736059320, ___sBiHWicpYhmNgEiaLGeoNmCayd_4)); }
	inline int32_t get_sBiHWicpYhmNgEiaLGeoNmCayd_4() const { return ___sBiHWicpYhmNgEiaLGeoNmCayd_4; }
	inline int32_t* get_address_of_sBiHWicpYhmNgEiaLGeoNmCayd_4() { return &___sBiHWicpYhmNgEiaLGeoNmCayd_4; }
	inline void set_sBiHWicpYhmNgEiaLGeoNmCayd_4(int32_t value)
	{
		___sBiHWicpYhmNgEiaLGeoNmCayd_4 = value;
	}

	inline static int32_t get_offset_of__count_5() { return static_cast<int32_t>(offsetof(AList_1_t974A66EA135B0300CE05935362C6C7C736059320, ____count_5)); }
	inline int32_t get__count_5() const { return ____count_5; }
	inline int32_t* get_address_of__count_5() { return &____count_5; }
	inline void set__count_5(int32_t value)
	{
		____count_5 = value;
	}

	inline static int32_t get_offset_of_sEfkOujJtwAFcdBLZvZKNLKcsRow_6() { return static_cast<int32_t>(offsetof(AList_1_t974A66EA135B0300CE05935362C6C7C736059320, ___sEfkOujJtwAFcdBLZvZKNLKcsRow_6)); }
	inline int32_t get_sEfkOujJtwAFcdBLZvZKNLKcsRow_6() const { return ___sEfkOujJtwAFcdBLZvZKNLKcsRow_6; }
	inline int32_t* get_address_of_sEfkOujJtwAFcdBLZvZKNLKcsRow_6() { return &___sEfkOujJtwAFcdBLZvZKNLKcsRow_6; }
	inline void set_sEfkOujJtwAFcdBLZvZKNLKcsRow_6(int32_t value)
	{
		___sEfkOujJtwAFcdBLZvZKNLKcsRow_6 = value;
	}

	inline static int32_t get_offset_of_tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7() { return static_cast<int32_t>(offsetof(AList_1_t974A66EA135B0300CE05935362C6C7C736059320, ___tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7)); }
	inline bool get_tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7() const { return ___tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7; }
	inline bool* get_address_of_tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7() { return &___tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7; }
	inline void set_tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7(bool value)
	{
		___tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7 = value;
	}

	inline static int32_t get_offset_of_rQpPIjHhnZIZRukMBcQxgFQNluPk_8() { return static_cast<int32_t>(offsetof(AList_1_t974A66EA135B0300CE05935362C6C7C736059320, ___rQpPIjHhnZIZRukMBcQxgFQNluPk_8)); }
	inline int32_t get_rQpPIjHhnZIZRukMBcQxgFQNluPk_8() const { return ___rQpPIjHhnZIZRukMBcQxgFQNluPk_8; }
	inline int32_t* get_address_of_rQpPIjHhnZIZRukMBcQxgFQNluPk_8() { return &___rQpPIjHhnZIZRukMBcQxgFQNluPk_8; }
	inline void set_rQpPIjHhnZIZRukMBcQxgFQNluPk_8(int32_t value)
	{
		___rQpPIjHhnZIZRukMBcQxgFQNluPk_8 = value;
	}

	inline static int32_t get_offset_of_VOtGOZhnqshkcRdadDngatuXTwff_9() { return static_cast<int32_t>(offsetof(AList_1_t974A66EA135B0300CE05935362C6C7C736059320, ___VOtGOZhnqshkcRdadDngatuXTwff_9)); }
	inline bool get_VOtGOZhnqshkcRdadDngatuXTwff_9() const { return ___VOtGOZhnqshkcRdadDngatuXTwff_9; }
	inline bool* get_address_of_VOtGOZhnqshkcRdadDngatuXTwff_9() { return &___VOtGOZhnqshkcRdadDngatuXTwff_9; }
	inline void set_VOtGOZhnqshkcRdadDngatuXTwff_9(bool value)
	{
		___VOtGOZhnqshkcRdadDngatuXTwff_9 = value;
	}

	inline static int32_t get_offset_of_FrgfpmHlyBElwQPLFJksZSvJtdD_10() { return static_cast<int32_t>(offsetof(AList_1_t974A66EA135B0300CE05935362C6C7C736059320, ___FrgfpmHlyBElwQPLFJksZSvJtdD_10)); }
	inline int32_t get_FrgfpmHlyBElwQPLFJksZSvJtdD_10() const { return ___FrgfpmHlyBElwQPLFJksZSvJtdD_10; }
	inline int32_t* get_address_of_FrgfpmHlyBElwQPLFJksZSvJtdD_10() { return &___FrgfpmHlyBElwQPLFJksZSvJtdD_10; }
	inline void set_FrgfpmHlyBElwQPLFJksZSvJtdD_10(int32_t value)
	{
		___FrgfpmHlyBElwQPLFJksZSvJtdD_10 = value;
	}

	inline static int32_t get_offset_of_QSVrKCTKhANFtMKLOpoQxMlPWHC_11() { return static_cast<int32_t>(offsetof(AList_1_t974A66EA135B0300CE05935362C6C7C736059320, ___QSVrKCTKhANFtMKLOpoQxMlPWHC_11)); }
	inline RuntimeObject * get_QSVrKCTKhANFtMKLOpoQxMlPWHC_11() const { return ___QSVrKCTKhANFtMKLOpoQxMlPWHC_11; }
	inline RuntimeObject ** get_address_of_QSVrKCTKhANFtMKLOpoQxMlPWHC_11() { return &___QSVrKCTKhANFtMKLOpoQxMlPWHC_11; }
	inline void set_QSVrKCTKhANFtMKLOpoQxMlPWHC_11(RuntimeObject * value)
	{
		___QSVrKCTKhANFtMKLOpoQxMlPWHC_11 = value;
		Il2CppCodeGenWriteBarrier((&___QSVrKCTKhANFtMKLOpoQxMlPWHC_11), value);
	}
};

struct AList_1_t974A66EA135B0300CE05935362C6C7C736059320_StaticFields
{
public:
	// T[] Rewired.Utils.Classes.Data.AList`1::twNSKGFaqUciuHlSKkXigWRNYFsa
	IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6* ___twNSKGFaqUciuHlSKkXigWRNYFsa_1;

public:
	inline static int32_t get_offset_of_twNSKGFaqUciuHlSKkXigWRNYFsa_1() { return static_cast<int32_t>(offsetof(AList_1_t974A66EA135B0300CE05935362C6C7C736059320_StaticFields, ___twNSKGFaqUciuHlSKkXigWRNYFsa_1)); }
	inline IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6* get_twNSKGFaqUciuHlSKkXigWRNYFsa_1() const { return ___twNSKGFaqUciuHlSKkXigWRNYFsa_1; }
	inline IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6** get_address_of_twNSKGFaqUciuHlSKkXigWRNYFsa_1() { return &___twNSKGFaqUciuHlSKkXigWRNYFsa_1; }
	inline void set_twNSKGFaqUciuHlSKkXigWRNYFsa_1(IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6* value)
	{
		___twNSKGFaqUciuHlSKkXigWRNYFsa_1 = value;
		Il2CppCodeGenWriteBarrier((&___twNSKGFaqUciuHlSKkXigWRNYFsa_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIST_1_T974A66EA135B0300CE05935362C6C7C736059320_H
#ifndef ALIST_1_TE4F325D076893D5254B2429A71BC0DDFBD65DC99_H
#define ALIST_1_TE4F325D076893D5254B2429A71BC0DDFBD65DC99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.AList`1<rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty>
struct  AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> Rewired.Utils.Classes.Data.AList`1::amBrOsbBxECFodcMKICGbBtYcvQ
	RuntimeObject* ___amBrOsbBxECFodcMKICGbBtYcvQ_2;
	// T[] Rewired.Utils.Classes.Data.AList`1::_items
	mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4* ____items_3;
	// System.Int32 Rewired.Utils.Classes.Data.AList`1::sBiHWicpYhmNgEiaLGeoNmCayd
	int32_t ___sBiHWicpYhmNgEiaLGeoNmCayd_4;
	// System.Int32 Rewired.Utils.Classes.Data.AList`1::_count
	int32_t ____count_5;
	// System.Int32 Rewired.Utils.Classes.Data.AList`1::sEfkOujJtwAFcdBLZvZKNLKcsRow
	int32_t ___sEfkOujJtwAFcdBLZvZKNLKcsRow_6;
	// System.Boolean Rewired.Utils.Classes.Data.AList`1::tWSNHwJKDfFOvkjLdLwgxBPqlUEt
	bool ___tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7;
	// System.Int32 Rewired.Utils.Classes.Data.AList`1::rQpPIjHhnZIZRukMBcQxgFQNluPk
	int32_t ___rQpPIjHhnZIZRukMBcQxgFQNluPk_8;
	// System.Boolean Rewired.Utils.Classes.Data.AList`1::VOtGOZhnqshkcRdadDngatuXTwff
	bool ___VOtGOZhnqshkcRdadDngatuXTwff_9;
	// System.Int32 Rewired.Utils.Classes.Data.AList`1::FrgfpmHlyBElwQPLFJksZSvJtdD
	int32_t ___FrgfpmHlyBElwQPLFJksZSvJtdD_10;
	// System.Object Rewired.Utils.Classes.Data.AList`1::QSVrKCTKhANFtMKLOpoQxMlPWHC
	RuntimeObject * ___QSVrKCTKhANFtMKLOpoQxMlPWHC_11;

public:
	inline static int32_t get_offset_of_amBrOsbBxECFodcMKICGbBtYcvQ_2() { return static_cast<int32_t>(offsetof(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99, ___amBrOsbBxECFodcMKICGbBtYcvQ_2)); }
	inline RuntimeObject* get_amBrOsbBxECFodcMKICGbBtYcvQ_2() const { return ___amBrOsbBxECFodcMKICGbBtYcvQ_2; }
	inline RuntimeObject** get_address_of_amBrOsbBxECFodcMKICGbBtYcvQ_2() { return &___amBrOsbBxECFodcMKICGbBtYcvQ_2; }
	inline void set_amBrOsbBxECFodcMKICGbBtYcvQ_2(RuntimeObject* value)
	{
		___amBrOsbBxECFodcMKICGbBtYcvQ_2 = value;
		Il2CppCodeGenWriteBarrier((&___amBrOsbBxECFodcMKICGbBtYcvQ_2), value);
	}

	inline static int32_t get_offset_of__items_3() { return static_cast<int32_t>(offsetof(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99, ____items_3)); }
	inline mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4* get__items_3() const { return ____items_3; }
	inline mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4** get_address_of__items_3() { return &____items_3; }
	inline void set__items_3(mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4* value)
	{
		____items_3 = value;
		Il2CppCodeGenWriteBarrier((&____items_3), value);
	}

	inline static int32_t get_offset_of_sBiHWicpYhmNgEiaLGeoNmCayd_4() { return static_cast<int32_t>(offsetof(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99, ___sBiHWicpYhmNgEiaLGeoNmCayd_4)); }
	inline int32_t get_sBiHWicpYhmNgEiaLGeoNmCayd_4() const { return ___sBiHWicpYhmNgEiaLGeoNmCayd_4; }
	inline int32_t* get_address_of_sBiHWicpYhmNgEiaLGeoNmCayd_4() { return &___sBiHWicpYhmNgEiaLGeoNmCayd_4; }
	inline void set_sBiHWicpYhmNgEiaLGeoNmCayd_4(int32_t value)
	{
		___sBiHWicpYhmNgEiaLGeoNmCayd_4 = value;
	}

	inline static int32_t get_offset_of__count_5() { return static_cast<int32_t>(offsetof(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99, ____count_5)); }
	inline int32_t get__count_5() const { return ____count_5; }
	inline int32_t* get_address_of__count_5() { return &____count_5; }
	inline void set__count_5(int32_t value)
	{
		____count_5 = value;
	}

	inline static int32_t get_offset_of_sEfkOujJtwAFcdBLZvZKNLKcsRow_6() { return static_cast<int32_t>(offsetof(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99, ___sEfkOujJtwAFcdBLZvZKNLKcsRow_6)); }
	inline int32_t get_sEfkOujJtwAFcdBLZvZKNLKcsRow_6() const { return ___sEfkOujJtwAFcdBLZvZKNLKcsRow_6; }
	inline int32_t* get_address_of_sEfkOujJtwAFcdBLZvZKNLKcsRow_6() { return &___sEfkOujJtwAFcdBLZvZKNLKcsRow_6; }
	inline void set_sEfkOujJtwAFcdBLZvZKNLKcsRow_6(int32_t value)
	{
		___sEfkOujJtwAFcdBLZvZKNLKcsRow_6 = value;
	}

	inline static int32_t get_offset_of_tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7() { return static_cast<int32_t>(offsetof(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99, ___tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7)); }
	inline bool get_tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7() const { return ___tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7; }
	inline bool* get_address_of_tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7() { return &___tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7; }
	inline void set_tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7(bool value)
	{
		___tWSNHwJKDfFOvkjLdLwgxBPqlUEt_7 = value;
	}

	inline static int32_t get_offset_of_rQpPIjHhnZIZRukMBcQxgFQNluPk_8() { return static_cast<int32_t>(offsetof(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99, ___rQpPIjHhnZIZRukMBcQxgFQNluPk_8)); }
	inline int32_t get_rQpPIjHhnZIZRukMBcQxgFQNluPk_8() const { return ___rQpPIjHhnZIZRukMBcQxgFQNluPk_8; }
	inline int32_t* get_address_of_rQpPIjHhnZIZRukMBcQxgFQNluPk_8() { return &___rQpPIjHhnZIZRukMBcQxgFQNluPk_8; }
	inline void set_rQpPIjHhnZIZRukMBcQxgFQNluPk_8(int32_t value)
	{
		___rQpPIjHhnZIZRukMBcQxgFQNluPk_8 = value;
	}

	inline static int32_t get_offset_of_VOtGOZhnqshkcRdadDngatuXTwff_9() { return static_cast<int32_t>(offsetof(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99, ___VOtGOZhnqshkcRdadDngatuXTwff_9)); }
	inline bool get_VOtGOZhnqshkcRdadDngatuXTwff_9() const { return ___VOtGOZhnqshkcRdadDngatuXTwff_9; }
	inline bool* get_address_of_VOtGOZhnqshkcRdadDngatuXTwff_9() { return &___VOtGOZhnqshkcRdadDngatuXTwff_9; }
	inline void set_VOtGOZhnqshkcRdadDngatuXTwff_9(bool value)
	{
		___VOtGOZhnqshkcRdadDngatuXTwff_9 = value;
	}

	inline static int32_t get_offset_of_FrgfpmHlyBElwQPLFJksZSvJtdD_10() { return static_cast<int32_t>(offsetof(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99, ___FrgfpmHlyBElwQPLFJksZSvJtdD_10)); }
	inline int32_t get_FrgfpmHlyBElwQPLFJksZSvJtdD_10() const { return ___FrgfpmHlyBElwQPLFJksZSvJtdD_10; }
	inline int32_t* get_address_of_FrgfpmHlyBElwQPLFJksZSvJtdD_10() { return &___FrgfpmHlyBElwQPLFJksZSvJtdD_10; }
	inline void set_FrgfpmHlyBElwQPLFJksZSvJtdD_10(int32_t value)
	{
		___FrgfpmHlyBElwQPLFJksZSvJtdD_10 = value;
	}

	inline static int32_t get_offset_of_QSVrKCTKhANFtMKLOpoQxMlPWHC_11() { return static_cast<int32_t>(offsetof(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99, ___QSVrKCTKhANFtMKLOpoQxMlPWHC_11)); }
	inline RuntimeObject * get_QSVrKCTKhANFtMKLOpoQxMlPWHC_11() const { return ___QSVrKCTKhANFtMKLOpoQxMlPWHC_11; }
	inline RuntimeObject ** get_address_of_QSVrKCTKhANFtMKLOpoQxMlPWHC_11() { return &___QSVrKCTKhANFtMKLOpoQxMlPWHC_11; }
	inline void set_QSVrKCTKhANFtMKLOpoQxMlPWHC_11(RuntimeObject * value)
	{
		___QSVrKCTKhANFtMKLOpoQxMlPWHC_11 = value;
		Il2CppCodeGenWriteBarrier((&___QSVrKCTKhANFtMKLOpoQxMlPWHC_11), value);
	}
};

struct AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99_StaticFields
{
public:
	// T[] Rewired.Utils.Classes.Data.AList`1::twNSKGFaqUciuHlSKkXigWRNYFsa
	mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4* ___twNSKGFaqUciuHlSKkXigWRNYFsa_1;

public:
	inline static int32_t get_offset_of_twNSKGFaqUciuHlSKkXigWRNYFsa_1() { return static_cast<int32_t>(offsetof(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99_StaticFields, ___twNSKGFaqUciuHlSKkXigWRNYFsa_1)); }
	inline mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4* get_twNSKGFaqUciuHlSKkXigWRNYFsa_1() const { return ___twNSKGFaqUciuHlSKkXigWRNYFsa_1; }
	inline mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4** get_address_of_twNSKGFaqUciuHlSKkXigWRNYFsa_1() { return &___twNSKGFaqUciuHlSKkXigWRNYFsa_1; }
	inline void set_twNSKGFaqUciuHlSKkXigWRNYFsa_1(mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4* value)
	{
		___twNSKGFaqUciuHlSKkXigWRNYFsa_1 = value;
		Il2CppCodeGenWriteBarrier((&___twNSKGFaqUciuHlSKkXigWRNYFsa_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIST_1_TE4F325D076893D5254B2429A71BC0DDFBD65DC99_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_TB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF_H
#define LIST_1_TB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM>
struct  List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	QnHiHbnYGCBSFowNhmjvNvYtpXMU5BU5D_t6AA1991911A35BF88BDCFAFC53BA478A59903B87* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF, ____items_1)); }
	inline QnHiHbnYGCBSFowNhmjvNvYtpXMU5BU5D_t6AA1991911A35BF88BDCFAFC53BA478A59903B87* get__items_1() const { return ____items_1; }
	inline QnHiHbnYGCBSFowNhmjvNvYtpXMU5BU5D_t6AA1991911A35BF88BDCFAFC53BA478A59903B87** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(QnHiHbnYGCBSFowNhmjvNvYtpXMU5BU5D_t6AA1991911A35BF88BDCFAFC53BA478A59903B87* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	QnHiHbnYGCBSFowNhmjvNvYtpXMU5BU5D_t6AA1991911A35BF88BDCFAFC53BA478A59903B87* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF_StaticFields, ____emptyArray_5)); }
	inline QnHiHbnYGCBSFowNhmjvNvYtpXMU5BU5D_t6AA1991911A35BF88BDCFAFC53BA478A59903B87* get__emptyArray_5() const { return ____emptyArray_5; }
	inline QnHiHbnYGCBSFowNhmjvNvYtpXMU5BU5D_t6AA1991911A35BF88BDCFAFC53BA478A59903B87** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(QnHiHbnYGCBSFowNhmjvNvYtpXMU5BU5D_t6AA1991911A35BF88BDCFAFC53BA478A59903B87* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef TLQZQETDWVMFAUYFHOSSCPGCHA_TA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773_H
#define TLQZQETDWVMFAUYFHOSSCPGCHA_TA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa
struct  tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM> qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa::AHOFIsQodiSzMcHMSUpdlsGdKfH
	List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0;

public:
	inline static int32_t get_offset_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() { return static_cast<int32_t>(offsetof(tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773, ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0)); }
	inline List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() const { return ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0; }
	inline List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF ** get_address_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() { return &___AHOFIsQodiSzMcHMSUpdlsGdKfH_0; }
	inline void set_AHOFIsQodiSzMcHMSUpdlsGdKfH_0(List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * value)
	{
		___AHOFIsQodiSzMcHMSUpdlsGdKfH_0 = value;
		Il2CppCodeGenWriteBarrier((&___AHOFIsQodiSzMcHMSUpdlsGdKfH_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLQZQETDWVMFAUYFHOSSCPGCHA_TA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773_H
#ifndef RICVLCWCUQGHGAAIOFVKJSVNDJX_T194A11E798E6C6497FA7FF7267CD63127C961224_H
#define RICVLCWCUQGHGAAIOFVKJSVNDJX_T194A11E798E6C6497FA7FF7267CD63127C961224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// rIcvLcWCuQghgaaiofvKjSvndJX
struct  rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224  : public RuntimeObject
{
public:
	// Rewired.Utils.Classes.Data.AList`1<rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty> rIcvLcWCuQghgaaiofvKjSvndJX::iwVNvyACihThlfmpkfURWPSdJYs
	AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * ___iwVNvyACihThlfmpkfURWPSdJYs_0;
	// System.Type[] rIcvLcWCuQghgaaiofvKjSvndJX::BnoetkxVNQEHfZTFFfghcpdJFzb
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___BnoetkxVNQEHfZTFFfghcpdJFzb_1;
	// System.Type[] rIcvLcWCuQghgaaiofvKjSvndJX::biNwZqyjNTmtSrqBtAcTREmMQcl
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___biNwZqyjNTmtSrqBtAcTREmMQcl_2;
	// System.Int32 rIcvLcWCuQghgaaiofvKjSvndJX::BtGnDdGQrTXaaWswowVwZiAMcDv
	int32_t ___BtGnDdGQrTXaaWswowVwZiAMcDv_3;

public:
	inline static int32_t get_offset_of_iwVNvyACihThlfmpkfURWPSdJYs_0() { return static_cast<int32_t>(offsetof(rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224, ___iwVNvyACihThlfmpkfURWPSdJYs_0)); }
	inline AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * get_iwVNvyACihThlfmpkfURWPSdJYs_0() const { return ___iwVNvyACihThlfmpkfURWPSdJYs_0; }
	inline AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 ** get_address_of_iwVNvyACihThlfmpkfURWPSdJYs_0() { return &___iwVNvyACihThlfmpkfURWPSdJYs_0; }
	inline void set_iwVNvyACihThlfmpkfURWPSdJYs_0(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * value)
	{
		___iwVNvyACihThlfmpkfURWPSdJYs_0 = value;
		Il2CppCodeGenWriteBarrier((&___iwVNvyACihThlfmpkfURWPSdJYs_0), value);
	}

	inline static int32_t get_offset_of_BnoetkxVNQEHfZTFFfghcpdJFzb_1() { return static_cast<int32_t>(offsetof(rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224, ___BnoetkxVNQEHfZTFFfghcpdJFzb_1)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_BnoetkxVNQEHfZTFFfghcpdJFzb_1() const { return ___BnoetkxVNQEHfZTFFfghcpdJFzb_1; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_BnoetkxVNQEHfZTFFfghcpdJFzb_1() { return &___BnoetkxVNQEHfZTFFfghcpdJFzb_1; }
	inline void set_BnoetkxVNQEHfZTFFfghcpdJFzb_1(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___BnoetkxVNQEHfZTFFfghcpdJFzb_1 = value;
		Il2CppCodeGenWriteBarrier((&___BnoetkxVNQEHfZTFFfghcpdJFzb_1), value);
	}

	inline static int32_t get_offset_of_biNwZqyjNTmtSrqBtAcTREmMQcl_2() { return static_cast<int32_t>(offsetof(rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224, ___biNwZqyjNTmtSrqBtAcTREmMQcl_2)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_biNwZqyjNTmtSrqBtAcTREmMQcl_2() const { return ___biNwZqyjNTmtSrqBtAcTREmMQcl_2; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_biNwZqyjNTmtSrqBtAcTREmMQcl_2() { return &___biNwZqyjNTmtSrqBtAcTREmMQcl_2; }
	inline void set_biNwZqyjNTmtSrqBtAcTREmMQcl_2(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___biNwZqyjNTmtSrqBtAcTREmMQcl_2 = value;
		Il2CppCodeGenWriteBarrier((&___biNwZqyjNTmtSrqBtAcTREmMQcl_2), value);
	}

	inline static int32_t get_offset_of_BtGnDdGQrTXaaWswowVwZiAMcDv_3() { return static_cast<int32_t>(offsetof(rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224, ___BtGnDdGQrTXaaWswowVwZiAMcDv_3)); }
	inline int32_t get_BtGnDdGQrTXaaWswowVwZiAMcDv_3() const { return ___BtGnDdGQrTXaaWswowVwZiAMcDv_3; }
	inline int32_t* get_address_of_BtGnDdGQrTXaaWswowVwZiAMcDv_3() { return &___BtGnDdGQrTXaaWswowVwZiAMcDv_3; }
	inline void set_BtGnDdGQrTXaaWswowVwZiAMcDv_3(int32_t value)
	{
		___BtGnDdGQrTXaaWswowVwZiAMcDv_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RICVLCWCUQGHGAAIOFVKJSVNDJX_T194A11E798E6C6497FA7FF7267CD63127C961224_H
#ifndef MGZGUQDUJPVKVEHSDVIRYOHLOTY_T6A4533BF4F06562072BCC6830BB48BBDBE2F8820_H
#define MGZGUQDUJPVKVEHSDVIRYOHLOTY_T6A4533BF4F06562072BCC6830BB48BBDBE2F8820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty
struct  mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820  : public RuntimeObject
{
public:
	// Rewired.Utils.Classes.Data.AList`1<Rewired.IControllerTemplate> rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty::zGAzEsGrTTTqWZqBgkORnHasWbZ
	AList_1_t974A66EA135B0300CE05935362C6C7C736059320 * ___zGAzEsGrTTTqWZqBgkORnHasWbZ_0;
	// System.Collections.IList rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty::HEgvcyfmgQRVtohyvBGRboKAUBC
	RuntimeObject* ___HEgvcyfmgQRVtohyvBGRboKAUBC_1;
	// System.Collections.IList rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty::gbMrYrhHSKKiezarhnNWxHrOFkF
	RuntimeObject* ___gbMrYrhHSKKiezarhnNWxHrOFkF_2;
	// System.Type rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty::umQghCmsfCFpiiISpgIfIkmOcxB
	Type_t * ___umQghCmsfCFpiiISpgIfIkmOcxB_3;

public:
	inline static int32_t get_offset_of_zGAzEsGrTTTqWZqBgkORnHasWbZ_0() { return static_cast<int32_t>(offsetof(mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820, ___zGAzEsGrTTTqWZqBgkORnHasWbZ_0)); }
	inline AList_1_t974A66EA135B0300CE05935362C6C7C736059320 * get_zGAzEsGrTTTqWZqBgkORnHasWbZ_0() const { return ___zGAzEsGrTTTqWZqBgkORnHasWbZ_0; }
	inline AList_1_t974A66EA135B0300CE05935362C6C7C736059320 ** get_address_of_zGAzEsGrTTTqWZqBgkORnHasWbZ_0() { return &___zGAzEsGrTTTqWZqBgkORnHasWbZ_0; }
	inline void set_zGAzEsGrTTTqWZqBgkORnHasWbZ_0(AList_1_t974A66EA135B0300CE05935362C6C7C736059320 * value)
	{
		___zGAzEsGrTTTqWZqBgkORnHasWbZ_0 = value;
		Il2CppCodeGenWriteBarrier((&___zGAzEsGrTTTqWZqBgkORnHasWbZ_0), value);
	}

	inline static int32_t get_offset_of_HEgvcyfmgQRVtohyvBGRboKAUBC_1() { return static_cast<int32_t>(offsetof(mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820, ___HEgvcyfmgQRVtohyvBGRboKAUBC_1)); }
	inline RuntimeObject* get_HEgvcyfmgQRVtohyvBGRboKAUBC_1() const { return ___HEgvcyfmgQRVtohyvBGRboKAUBC_1; }
	inline RuntimeObject** get_address_of_HEgvcyfmgQRVtohyvBGRboKAUBC_1() { return &___HEgvcyfmgQRVtohyvBGRboKAUBC_1; }
	inline void set_HEgvcyfmgQRVtohyvBGRboKAUBC_1(RuntimeObject* value)
	{
		___HEgvcyfmgQRVtohyvBGRboKAUBC_1 = value;
		Il2CppCodeGenWriteBarrier((&___HEgvcyfmgQRVtohyvBGRboKAUBC_1), value);
	}

	inline static int32_t get_offset_of_gbMrYrhHSKKiezarhnNWxHrOFkF_2() { return static_cast<int32_t>(offsetof(mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820, ___gbMrYrhHSKKiezarhnNWxHrOFkF_2)); }
	inline RuntimeObject* get_gbMrYrhHSKKiezarhnNWxHrOFkF_2() const { return ___gbMrYrhHSKKiezarhnNWxHrOFkF_2; }
	inline RuntimeObject** get_address_of_gbMrYrhHSKKiezarhnNWxHrOFkF_2() { return &___gbMrYrhHSKKiezarhnNWxHrOFkF_2; }
	inline void set_gbMrYrhHSKKiezarhnNWxHrOFkF_2(RuntimeObject* value)
	{
		___gbMrYrhHSKKiezarhnNWxHrOFkF_2 = value;
		Il2CppCodeGenWriteBarrier((&___gbMrYrhHSKKiezarhnNWxHrOFkF_2), value);
	}

	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_3() { return static_cast<int32_t>(offsetof(mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820, ___umQghCmsfCFpiiISpgIfIkmOcxB_3)); }
	inline Type_t * get_umQghCmsfCFpiiISpgIfIkmOcxB_3() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_3; }
	inline Type_t ** get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_3() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_3; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_3(Type_t * value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_3 = value;
		Il2CppCodeGenWriteBarrier((&___umQghCmsfCFpiiISpgIfIkmOcxB_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MGZGUQDUJPVKVEHSDVIRYOHLOTY_T6A4533BF4F06562072BCC6830BB48BBDBE2F8820_H
#ifndef SSMYCZXLYXGIXUSUHXFHCEQECTR_T23709CD7AD3923255200D20AD2F6D7232F5C3B26_H
#define SSMYCZXLYXGIXUSUHXFHCEQECTR_T23709CD7AD3923255200D20AD2F6D7232F5C3B26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// sSmyCZxlYXGixusuhxfHceqeCTr
struct  sSmyCZxlYXGixusuhxfHceqeCTr_t23709CD7AD3923255200D20AD2F6D7232F5C3B26  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSMYCZXLYXGIXUSUHXFHCEQECTR_T23709CD7AD3923255200D20AD2F6D7232F5C3B26_H
#ifndef TIRTBIGERWCCHDWIMTBHEXMBBYDC_T11C5ADD759CABF0E86B077DAE414A5E457DD753B_H
#define TIRTBIGERWCCHDWIMTBHEXMBBYDC_T11C5ADD759CABF0E86B077DAE414A5E457DD753B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// tirTbIgerWCchDwImTBhEXMbbyDC
struct  tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B  : public RuntimeObject
{
public:

public:
};

struct tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_StaticFields
{
public:
	// System.Int32[] tirTbIgerWCchDwImTBhEXMbbyDC::fxnhIWEsHEritNuKepbGhDThoNR
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___fxnhIWEsHEritNuKepbGhDThoNR_2;

public:
	inline static int32_t get_offset_of_fxnhIWEsHEritNuKepbGhDThoNR_2() { return static_cast<int32_t>(offsetof(tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_StaticFields, ___fxnhIWEsHEritNuKepbGhDThoNR_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_fxnhIWEsHEritNuKepbGhDThoNR_2() const { return ___fxnhIWEsHEritNuKepbGhDThoNR_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_fxnhIWEsHEritNuKepbGhDThoNR_2() { return &___fxnhIWEsHEritNuKepbGhDThoNR_2; }
	inline void set_fxnhIWEsHEritNuKepbGhDThoNR_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___fxnhIWEsHEritNuKepbGhDThoNR_2 = value;
		Il2CppCodeGenWriteBarrier((&___fxnhIWEsHEritNuKepbGhDThoNR_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIRTBIGERWCCHDWIMTBHEXMBBYDC_T11C5ADD759CABF0E86B077DAE414A5E457DD753B_H
#ifndef VLEBHNCKPOPFOKOXDDCBDGTKHYD_TA01C4EADEDC175C2FAC271C09D7E5245A52EFCBB_H
#define VLEBHNCKPOPFOKOXDDCBDGTKHYD_TA01C4EADEDC175C2FAC271C09D7E5245A52EFCBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// vLEBhncKpOPFOKOXDdCbdgTKhyd
struct  vLEBhncKpOPFOKOXDdCbdgTKhyd_tA01C4EADEDC175C2FAC271C09D7E5245A52EFCBB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VLEBHNCKPOPFOKOXDDCBDGTKHYD_TA01C4EADEDC175C2FAC271C09D7E5245A52EFCBB_H
#ifndef WACWXZJEFHAFUZXPGGPYAHJLDEO_T5F06EEC3446CA9E1242E6D3766779DEED89701E6_H
#define WACWXZJEFHAFUZXPGGPYAHJLDEO_T5F06EEC3446CA9E1242E6D3766779DEED89701E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// wACwxZjEFhAfUzXpGGPyAhJldeo
struct  wACwxZjEFhAfUzXpGGPyAhJldeo_t5F06EEC3446CA9E1242E6D3766779DEED89701E6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WACWXZJEFHAFUZXPGGPYAHJLDEO_T5F06EEC3446CA9E1242E6D3766779DEED89701E6_H
#ifndef __STATICARRAYINITTYPESIZEU3D13688_T4A2D8B9F062036EC2EA963FD2BFCB8934CB3AFB1_H
#define __STATICARRAYINITTYPESIZEU3D13688_T4A2D8B9F062036EC2EA963FD2BFCB8934CB3AFB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D13688
struct  __StaticArrayInitTypeSizeU3D13688_t4A2D8B9F062036EC2EA963FD2BFCB8934CB3AFB1 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D13688_t4A2D8B9F062036EC2EA963FD2BFCB8934CB3AFB1__padding[13688];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D13688_T4A2D8B9F062036EC2EA963FD2BFCB8934CB3AFB1_H
#ifndef __STATICARRAYINITTYPESIZEU3D16688_TD3F8C3F3B2B924F67544DD142369F3550C958C8E_H
#define __STATICARRAYINITTYPESIZEU3D16688_TD3F8C3F3B2B924F67544DD142369F3550C958C8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D16688
struct  __StaticArrayInitTypeSizeU3D16688_tD3F8C3F3B2B924F67544DD142369F3550C958C8E 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16688_tD3F8C3F3B2B924F67544DD142369F3550C958C8E__padding[16688];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D16688_TD3F8C3F3B2B924F67544DD142369F3550C958C8E_H
#ifndef __STATICARRAYINITTYPESIZEU3D288_T39E48415E8E26A109F9E815D317D482E7B70DDE6_H
#define __STATICARRAYINITTYPESIZEU3D288_T39E48415E8E26A109F9E815D317D482E7B70DDE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D288
struct  __StaticArrayInitTypeSizeU3D288_t39E48415E8E26A109F9E815D317D482E7B70DDE6 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D288_t39E48415E8E26A109F9E815D317D482E7B70DDE6__padding[288];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D288_T39E48415E8E26A109F9E815D317D482E7B70DDE6_H
#ifndef __STATICARRAYINITTYPESIZEU3D30352_TE6A7EEF980EF492A0E545B07E4574A4BCCC81750_H
#define __STATICARRAYINITTYPESIZEU3D30352_TE6A7EEF980EF492A0E545B07E4574A4BCCC81750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D30352
struct  __StaticArrayInitTypeSizeU3D30352_tE6A7EEF980EF492A0E545B07E4574A4BCCC81750 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D30352_tE6A7EEF980EF492A0E545B07E4574A4BCCC81750__padding[30352];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D30352_TE6A7EEF980EF492A0E545B07E4574A4BCCC81750_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_T65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD_H
#define __STATICARRAYINITTYPESIZEU3D40_T65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D40
struct  __StaticArrayInitTypeSizeU3D40_t65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_t65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_T65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD_H
#ifndef __STATICARRAYINITTYPESIZEU3D528_T47DA2E6BCA641B80C4DA2E0F1ACE6B51DF761610_H
#define __STATICARRAYINITTYPESIZEU3D528_T47DA2E6BCA641B80C4DA2E0F1ACE6B51DF761610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D528
struct  __StaticArrayInitTypeSizeU3D528_t47DA2E6BCA641B80C4DA2E0F1ACE6B51DF761610 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D528_t47DA2E6BCA641B80C4DA2E0F1ACE6B51DF761610__padding[528];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D528_T47DA2E6BCA641B80C4DA2E0F1ACE6B51DF761610_H
#ifndef PIDVID_TE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D_H
#define PIDVID_TE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PidVid
struct  PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D 
{
public:
	// System.UInt16 Rewired.PidVid::productId
	uint16_t ___productId_1;
	// System.UInt16 Rewired.PidVid::vendorId
	uint16_t ___vendorId_2;

public:
	inline static int32_t get_offset_of_productId_1() { return static_cast<int32_t>(offsetof(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D, ___productId_1)); }
	inline uint16_t get_productId_1() const { return ___productId_1; }
	inline uint16_t* get_address_of_productId_1() { return &___productId_1; }
	inline void set_productId_1(uint16_t value)
	{
		___productId_1 = value;
	}

	inline static int32_t get_offset_of_vendorId_2() { return static_cast<int32_t>(offsetof(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D, ___vendorId_2)); }
	inline uint16_t get_vendorId_2() const { return ___vendorId_2; }
	inline uint16_t* get_address_of_vendorId_2() { return &___vendorId_2; }
	inline void set_vendorId_2(uint16_t value)
	{
		___vendorId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIDVID_TE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#define BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#ifndef CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#define CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#ifndef DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#define DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INT16_T823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D_H
#define INT16_T823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int16
struct  Int16_t823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_t823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16_T823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SBYTE_T9070AEA2966184235653CB9B4D33B149CDA831DF_H
#define SBYTE_T9070AEA2966184235653CB9B4D33B149CDA831DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SByte
struct  SByte_t9070AEA2966184235653CB9B4D33B149CDA831DF 
{
public:
	// System.SByte System.SByte::m_value
	int8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(SByte_t9070AEA2966184235653CB9B4D33B149CDA831DF, ___m_value_0)); }
	inline int8_t get_m_value_0() const { return ___m_value_0; }
	inline int8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SBYTE_T9070AEA2966184235653CB9B4D33B149CDA831DF_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef UINT16_TAE45CEF73BF720100519F6867F32145D075F928E_H
#define UINT16_TAE45CEF73BF720100519F6867F32145D075F928E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt16
struct  UInt16_tAE45CEF73BF720100519F6867F32145D075F928E 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt16_tAE45CEF73BF720100519F6867F32145D075F928E, ___m_value_0)); }
	inline uint16_t get_m_value_0() const { return ___m_value_0; }
	inline uint16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16_TAE45CEF73BF720100519F6867F32145D075F928E_H
#ifndef UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#define UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef UZCGPPJQBITKWKQVAYRGZBIZQLX_TA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_H
#define UZCGPPJQBITKWKQVAYRGZBIZQLX_TA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// uzcGPPJqbITKwkQVaYRGzbIZqLX
struct  uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte uzcGPPJqbITKwkQVaYRGzbIZqLX::wTYLYalPavcctjXZrASJNkapMaJ
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.SByte uzcGPPJqbITKwkQVaYRGzbIZqLX::MJrfBrxoBTvDrzGeBrTAImwmNOZ
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Char uzcGPPJqbITKwkQVaYRGzbIZqLX::euLduoAiVMxRQGLWMCGBXUDrCKr
			Il2CppChar ___euLduoAiVMxRQGLWMCGBXUDrCKr_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppChar ___euLduoAiVMxRQGLWMCGBXUDrCKr_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int16 uzcGPPJqbITKwkQVaYRGzbIZqLX::BlWgkqEeUMGgBlbpbsWvFckFbDv
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt16 uzcGPPJqbITKwkQVaYRGzbIZqLX::WBbwvCOIPVbAJbjWvqCbJElmQLtd
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 uzcGPPJqbITKwkQVaYRGzbIZqLX::lxdbJzaQWeBlRYInsjMuuXhyCggl
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt32 uzcGPPJqbITKwkQVaYRGzbIZqLX::evDemCrkcBEYeHncrlFUdsIyAKPa
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single uzcGPPJqbITKwkQVaYRGzbIZqLX::mUqspDOssdvKUAQvOxwRVdbooCb
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Boolean uzcGPPJqbITKwkQVaYRGzbIZqLX::HqWkThUinZjstdlLHSIImaHNnAis
			bool ___HqWkThUinZjstdlLHSIImaHNnAis_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			bool ___HqWkThUinZjstdlLHSIImaHNnAis_8_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_wTYLYalPavcctjXZrASJNkapMaJ_0() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___wTYLYalPavcctjXZrASJNkapMaJ_0)); }
	inline uint8_t get_wTYLYalPavcctjXZrASJNkapMaJ_0() const { return ___wTYLYalPavcctjXZrASJNkapMaJ_0; }
	inline uint8_t* get_address_of_wTYLYalPavcctjXZrASJNkapMaJ_0() { return &___wTYLYalPavcctjXZrASJNkapMaJ_0; }
	inline void set_wTYLYalPavcctjXZrASJNkapMaJ_0(uint8_t value)
	{
		___wTYLYalPavcctjXZrASJNkapMaJ_0 = value;
	}

	inline static int32_t get_offset_of_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1)); }
	inline int8_t get_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() const { return ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1; }
	inline int8_t* get_address_of_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() { return &___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1; }
	inline void set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1(int8_t value)
	{
		___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1 = value;
	}

	inline static int32_t get_offset_of_euLduoAiVMxRQGLWMCGBXUDrCKr_2() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___euLduoAiVMxRQGLWMCGBXUDrCKr_2)); }
	inline Il2CppChar get_euLduoAiVMxRQGLWMCGBXUDrCKr_2() const { return ___euLduoAiVMxRQGLWMCGBXUDrCKr_2; }
	inline Il2CppChar* get_address_of_euLduoAiVMxRQGLWMCGBXUDrCKr_2() { return &___euLduoAiVMxRQGLWMCGBXUDrCKr_2; }
	inline void set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(Il2CppChar value)
	{
		___euLduoAiVMxRQGLWMCGBXUDrCKr_2 = value;
	}

	inline static int32_t get_offset_of_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3)); }
	inline int16_t get_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() const { return ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3; }
	inline int16_t* get_address_of_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() { return &___BlWgkqEeUMGgBlbpbsWvFckFbDv_3; }
	inline void set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3(int16_t value)
	{
		___BlWgkqEeUMGgBlbpbsWvFckFbDv_3 = value;
	}

	inline static int32_t get_offset_of_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4)); }
	inline uint16_t get_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() const { return ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4; }
	inline uint16_t* get_address_of_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() { return &___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4; }
	inline void set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4(uint16_t value)
	{
		___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4 = value;
	}

	inline static int32_t get_offset_of_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5)); }
	inline int32_t get_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() const { return ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5; }
	inline int32_t* get_address_of_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() { return &___lxdbJzaQWeBlRYInsjMuuXhyCggl_5; }
	inline void set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(int32_t value)
	{
		___lxdbJzaQWeBlRYInsjMuuXhyCggl_5 = value;
	}

	inline static int32_t get_offset_of_evDemCrkcBEYeHncrlFUdsIyAKPa_6() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___evDemCrkcBEYeHncrlFUdsIyAKPa_6)); }
	inline uint32_t get_evDemCrkcBEYeHncrlFUdsIyAKPa_6() const { return ___evDemCrkcBEYeHncrlFUdsIyAKPa_6; }
	inline uint32_t* get_address_of_evDemCrkcBEYeHncrlFUdsIyAKPa_6() { return &___evDemCrkcBEYeHncrlFUdsIyAKPa_6; }
	inline void set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(uint32_t value)
	{
		___evDemCrkcBEYeHncrlFUdsIyAKPa_6 = value;
	}

	inline static int32_t get_offset_of_mUqspDOssdvKUAQvOxwRVdbooCb_7() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___mUqspDOssdvKUAQvOxwRVdbooCb_7)); }
	inline float get_mUqspDOssdvKUAQvOxwRVdbooCb_7() const { return ___mUqspDOssdvKUAQvOxwRVdbooCb_7; }
	inline float* get_address_of_mUqspDOssdvKUAQvOxwRVdbooCb_7() { return &___mUqspDOssdvKUAQvOxwRVdbooCb_7; }
	inline void set_mUqspDOssdvKUAQvOxwRVdbooCb_7(float value)
	{
		___mUqspDOssdvKUAQvOxwRVdbooCb_7 = value;
	}

	inline static int32_t get_offset_of_HqWkThUinZjstdlLHSIImaHNnAis_8() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___HqWkThUinZjstdlLHSIImaHNnAis_8)); }
	inline bool get_HqWkThUinZjstdlLHSIImaHNnAis_8() const { return ___HqWkThUinZjstdlLHSIImaHNnAis_8; }
	inline bool* get_address_of_HqWkThUinZjstdlLHSIImaHNnAis_8() { return &___HqWkThUinZjstdlLHSIImaHNnAis_8; }
	inline void set_HqWkThUinZjstdlLHSIImaHNnAis_8(bool value)
	{
		___HqWkThUinZjstdlLHSIImaHNnAis_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of uzcGPPJqbITKwkQVaYRGzbIZqLX
struct uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_8_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of uzcGPPJqbITKwkQVaYRGzbIZqLX
struct uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_8_forAlignmentOnly;
		};
	};
};
#endif // UZCGPPJQBITKWKQVAYRGZBIZQLX_TA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_T2294A85ED0D53C43A0934AE9A86EBFD96609A170_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_T2294A85ED0D53C43A0934AE9A86EBFD96609A170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D
struct  U3CPrivateImplementationDetailsU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_t2294A85ED0D53C43A0934AE9A86EBFD96609A170  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_t2294A85ED0D53C43A0934AE9A86EBFD96609A170_StaticFields
{
public:
	// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D::U24U24method0x60006d6U2D1
	__StaticArrayInitTypeSizeU3D40_t65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD  ___U24U24method0x60006d6U2D1_0;
	// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D::U24U24method0x60006d6U2D2
	__StaticArrayInitTypeSizeU3D40_t65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD  ___U24U24method0x60006d6U2D2_1;
	// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D528 <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D::U24U24method0x60006d6U2D3
	__StaticArrayInitTypeSizeU3D528_t47DA2E6BCA641B80C4DA2E0F1ACE6B51DF761610  ___U24U24method0x60006d6U2D3_2;
	// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D16688 <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D::U24U24method0x600238eU2D1
	__StaticArrayInitTypeSizeU3D16688_tD3F8C3F3B2B924F67544DD142369F3550C958C8E  ___U24U24method0x600238eU2D1_3;
	// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D13688 <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D::U24U24method0x600238fU2D1
	__StaticArrayInitTypeSizeU3D13688_t4A2D8B9F062036EC2EA963FD2BFCB8934CB3AFB1  ___U24U24method0x600238fU2D1_4;
	// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D30352 <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D::U24U24method0x6002390U2D1
	__StaticArrayInitTypeSizeU3D30352_tE6A7EEF980EF492A0E545B07E4574A4BCCC81750  ___U24U24method0x6002390U2D1_5;
	// <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D___StaticArrayInitTypeSizeU3D288 <PrivateImplementationDetails>U7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D::U24U24method0x600327aU2D1
	__StaticArrayInitTypeSizeU3D288_t39E48415E8E26A109F9E815D317D482E7B70DDE6  ___U24U24method0x600327aU2D1_6;

public:
	inline static int32_t get_offset_of_U24U24method0x60006d6U2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_t2294A85ED0D53C43A0934AE9A86EBFD96609A170_StaticFields, ___U24U24method0x60006d6U2D1_0)); }
	inline __StaticArrayInitTypeSizeU3D40_t65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD  get_U24U24method0x60006d6U2D1_0() const { return ___U24U24method0x60006d6U2D1_0; }
	inline __StaticArrayInitTypeSizeU3D40_t65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD * get_address_of_U24U24method0x60006d6U2D1_0() { return &___U24U24method0x60006d6U2D1_0; }
	inline void set_U24U24method0x60006d6U2D1_0(__StaticArrayInitTypeSizeU3D40_t65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD  value)
	{
		___U24U24method0x60006d6U2D1_0 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60006d6U2D2_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_t2294A85ED0D53C43A0934AE9A86EBFD96609A170_StaticFields, ___U24U24method0x60006d6U2D2_1)); }
	inline __StaticArrayInitTypeSizeU3D40_t65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD  get_U24U24method0x60006d6U2D2_1() const { return ___U24U24method0x60006d6U2D2_1; }
	inline __StaticArrayInitTypeSizeU3D40_t65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD * get_address_of_U24U24method0x60006d6U2D2_1() { return &___U24U24method0x60006d6U2D2_1; }
	inline void set_U24U24method0x60006d6U2D2_1(__StaticArrayInitTypeSizeU3D40_t65C88F9BDAAA24CB95BAEE66249ED7B2F657F6AD  value)
	{
		___U24U24method0x60006d6U2D2_1 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60006d6U2D3_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_t2294A85ED0D53C43A0934AE9A86EBFD96609A170_StaticFields, ___U24U24method0x60006d6U2D3_2)); }
	inline __StaticArrayInitTypeSizeU3D528_t47DA2E6BCA641B80C4DA2E0F1ACE6B51DF761610  get_U24U24method0x60006d6U2D3_2() const { return ___U24U24method0x60006d6U2D3_2; }
	inline __StaticArrayInitTypeSizeU3D528_t47DA2E6BCA641B80C4DA2E0F1ACE6B51DF761610 * get_address_of_U24U24method0x60006d6U2D3_2() { return &___U24U24method0x60006d6U2D3_2; }
	inline void set_U24U24method0x60006d6U2D3_2(__StaticArrayInitTypeSizeU3D528_t47DA2E6BCA641B80C4DA2E0F1ACE6B51DF761610  value)
	{
		___U24U24method0x60006d6U2D3_2 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x600238eU2D1_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_t2294A85ED0D53C43A0934AE9A86EBFD96609A170_StaticFields, ___U24U24method0x600238eU2D1_3)); }
	inline __StaticArrayInitTypeSizeU3D16688_tD3F8C3F3B2B924F67544DD142369F3550C958C8E  get_U24U24method0x600238eU2D1_3() const { return ___U24U24method0x600238eU2D1_3; }
	inline __StaticArrayInitTypeSizeU3D16688_tD3F8C3F3B2B924F67544DD142369F3550C958C8E * get_address_of_U24U24method0x600238eU2D1_3() { return &___U24U24method0x600238eU2D1_3; }
	inline void set_U24U24method0x600238eU2D1_3(__StaticArrayInitTypeSizeU3D16688_tD3F8C3F3B2B924F67544DD142369F3550C958C8E  value)
	{
		___U24U24method0x600238eU2D1_3 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x600238fU2D1_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_t2294A85ED0D53C43A0934AE9A86EBFD96609A170_StaticFields, ___U24U24method0x600238fU2D1_4)); }
	inline __StaticArrayInitTypeSizeU3D13688_t4A2D8B9F062036EC2EA963FD2BFCB8934CB3AFB1  get_U24U24method0x600238fU2D1_4() const { return ___U24U24method0x600238fU2D1_4; }
	inline __StaticArrayInitTypeSizeU3D13688_t4A2D8B9F062036EC2EA963FD2BFCB8934CB3AFB1 * get_address_of_U24U24method0x600238fU2D1_4() { return &___U24U24method0x600238fU2D1_4; }
	inline void set_U24U24method0x600238fU2D1_4(__StaticArrayInitTypeSizeU3D13688_t4A2D8B9F062036EC2EA963FD2BFCB8934CB3AFB1  value)
	{
		___U24U24method0x600238fU2D1_4 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x6002390U2D1_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_t2294A85ED0D53C43A0934AE9A86EBFD96609A170_StaticFields, ___U24U24method0x6002390U2D1_5)); }
	inline __StaticArrayInitTypeSizeU3D30352_tE6A7EEF980EF492A0E545B07E4574A4BCCC81750  get_U24U24method0x6002390U2D1_5() const { return ___U24U24method0x6002390U2D1_5; }
	inline __StaticArrayInitTypeSizeU3D30352_tE6A7EEF980EF492A0E545B07E4574A4BCCC81750 * get_address_of_U24U24method0x6002390U2D1_5() { return &___U24U24method0x6002390U2D1_5; }
	inline void set_U24U24method0x6002390U2D1_5(__StaticArrayInitTypeSizeU3D30352_tE6A7EEF980EF492A0E545B07E4574A4BCCC81750  value)
	{
		___U24U24method0x6002390U2D1_5 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x600327aU2D1_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_t2294A85ED0D53C43A0934AE9A86EBFD96609A170_StaticFields, ___U24U24method0x600327aU2D1_6)); }
	inline __StaticArrayInitTypeSizeU3D288_t39E48415E8E26A109F9E815D317D482E7B70DDE6  get_U24U24method0x600327aU2D1_6() const { return ___U24U24method0x600327aU2D1_6; }
	inline __StaticArrayInitTypeSizeU3D288_t39E48415E8E26A109F9E815D317D482E7B70DDE6 * get_address_of_U24U24method0x600327aU2D1_6() { return &___U24U24method0x600327aU2D1_6; }
	inline void set_U24U24method0x600327aU2D1_6(__StaticArrayInitTypeSizeU3D288_t39E48415E8E26A109F9E815D317D482E7B70DDE6  value)
	{
		___U24U24method0x600327aU2D1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_T2294A85ED0D53C43A0934AE9A86EBFD96609A170_H
#ifndef AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#define AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisCoordinateMode
struct  AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868 
{
public:
	// System.Int32 Rewired.AxisCoordinateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifndef BUTTONSTATEFLAGS_TE48083CA4AA8079D0A5006CD787C46510592624A_H
#define BUTTONSTATEFLAGS_TE48083CA4AA8079D0A5006CD787C46510592624A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ButtonStateFlags
struct  ButtonStateFlags_tE48083CA4AA8079D0A5006CD787C46510592624A 
{
public:
	// System.Int32 Rewired.ButtonStateFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStateFlags_tE48083CA4AA8079D0A5006CD787C46510592624A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATEFLAGS_TE48083CA4AA8079D0A5006CD787C46510592624A_H
#ifndef CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#define CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerElementType
struct  ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA 
{
public:
	// System.Int32 Rewired.ControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifndef CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#define CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerType
struct  ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4 
{
public:
	// System.Int32 Rewired.ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifndef INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#define INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSource
struct  InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B 
{
public:
	// System.Int32 Rewired.InputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifndef MODIFIERKEY_T300DA428522CD195A6C4121312A8A2BD4EA7ACE0_H
#define MODIFIERKEY_T300DA428522CD195A6C4121312A8A2BD4EA7ACE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKey
struct  ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0 
{
public:
	// System.Int32 Rewired.ModifierKey::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEY_T300DA428522CD195A6C4121312A8A2BD4EA7ACE0_H
#ifndef MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#define MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKeyFlags
struct  ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651 
{
public:
	// System.Int32 Rewired.ModifierKeyFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifndef ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#define ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_paramName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#ifndef NOTIMPLEMENTEDEXCEPTION_T8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4_H
#define NOTIMPLEMENTEDEXCEPTION_T8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotImplementedException
struct  NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIMPLEMENTEDEXCEPTION_T8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#define RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#ifndef RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#define RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifndef QNHIHBNYGCBSFOWNHMJVNVYTPXM_T3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4_H
#define QNHIHBNYGCBSFOWNHMJVNVYTPXM_T3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM
struct  QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4  : public RuntimeObject
{
public:
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::nvorGNbxnIKRTEQakQRxoDaDbkF
	int32_t ___nvorGNbxnIKRTEQakQRxoDaDbkF_0;
	// System.Guid qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::nlEaopEverUmqALsSlwZYfPupTlP
	Guid_t  ___nlEaopEverUmqALsSlwZYfPupTlP_1;
	// System.Guid qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::bLJDeZdmqhplurpuKUnpKCpNysd
	Guid_t  ___bLJDeZdmqhplurpuKUnpKCpNysd_2;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::fYYSPmKTkyNnvcLPwwaCPYkcWqv
	int32_t ___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::UdWYvkBYkFbtQVrPZwZuKINGjC
	int32_t ___UdWYvkBYkFbtQVrPZwZuKINGjC_4;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::VYSASJjqEIsmJcmnIHADUYEaRjQC
	int32_t ___VYSASJjqEIsmJcmnIHADUYEaRjQC_5;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::aWhbUpAtiftmjIGzRSoPwmEnzb
	int32_t ___aWhbUpAtiftmjIGzRSoPwmEnzb_6;

public:
	inline static int32_t get_offset_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___nvorGNbxnIKRTEQakQRxoDaDbkF_0)); }
	inline int32_t get_nvorGNbxnIKRTEQakQRxoDaDbkF_0() const { return ___nvorGNbxnIKRTEQakQRxoDaDbkF_0; }
	inline int32_t* get_address_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0() { return &___nvorGNbxnIKRTEQakQRxoDaDbkF_0; }
	inline void set_nvorGNbxnIKRTEQakQRxoDaDbkF_0(int32_t value)
	{
		___nvorGNbxnIKRTEQakQRxoDaDbkF_0 = value;
	}

	inline static int32_t get_offset_of_nlEaopEverUmqALsSlwZYfPupTlP_1() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___nlEaopEverUmqALsSlwZYfPupTlP_1)); }
	inline Guid_t  get_nlEaopEverUmqALsSlwZYfPupTlP_1() const { return ___nlEaopEverUmqALsSlwZYfPupTlP_1; }
	inline Guid_t * get_address_of_nlEaopEverUmqALsSlwZYfPupTlP_1() { return &___nlEaopEverUmqALsSlwZYfPupTlP_1; }
	inline void set_nlEaopEverUmqALsSlwZYfPupTlP_1(Guid_t  value)
	{
		___nlEaopEverUmqALsSlwZYfPupTlP_1 = value;
	}

	inline static int32_t get_offset_of_bLJDeZdmqhplurpuKUnpKCpNysd_2() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___bLJDeZdmqhplurpuKUnpKCpNysd_2)); }
	inline Guid_t  get_bLJDeZdmqhplurpuKUnpKCpNysd_2() const { return ___bLJDeZdmqhplurpuKUnpKCpNysd_2; }
	inline Guid_t * get_address_of_bLJDeZdmqhplurpuKUnpKCpNysd_2() { return &___bLJDeZdmqhplurpuKUnpKCpNysd_2; }
	inline void set_bLJDeZdmqhplurpuKUnpKCpNysd_2(Guid_t  value)
	{
		___bLJDeZdmqhplurpuKUnpKCpNysd_2 = value;
	}

	inline static int32_t get_offset_of_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3)); }
	inline int32_t get_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3() const { return ___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3; }
	inline int32_t* get_address_of_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3() { return &___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3; }
	inline void set_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3(int32_t value)
	{
		___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3 = value;
	}

	inline static int32_t get_offset_of_UdWYvkBYkFbtQVrPZwZuKINGjC_4() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___UdWYvkBYkFbtQVrPZwZuKINGjC_4)); }
	inline int32_t get_UdWYvkBYkFbtQVrPZwZuKINGjC_4() const { return ___UdWYvkBYkFbtQVrPZwZuKINGjC_4; }
	inline int32_t* get_address_of_UdWYvkBYkFbtQVrPZwZuKINGjC_4() { return &___UdWYvkBYkFbtQVrPZwZuKINGjC_4; }
	inline void set_UdWYvkBYkFbtQVrPZwZuKINGjC_4(int32_t value)
	{
		___UdWYvkBYkFbtQVrPZwZuKINGjC_4 = value;
	}

	inline static int32_t get_offset_of_VYSASJjqEIsmJcmnIHADUYEaRjQC_5() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___VYSASJjqEIsmJcmnIHADUYEaRjQC_5)); }
	inline int32_t get_VYSASJjqEIsmJcmnIHADUYEaRjQC_5() const { return ___VYSASJjqEIsmJcmnIHADUYEaRjQC_5; }
	inline int32_t* get_address_of_VYSASJjqEIsmJcmnIHADUYEaRjQC_5() { return &___VYSASJjqEIsmJcmnIHADUYEaRjQC_5; }
	inline void set_VYSASJjqEIsmJcmnIHADUYEaRjQC_5(int32_t value)
	{
		___VYSASJjqEIsmJcmnIHADUYEaRjQC_5 = value;
	}

	inline static int32_t get_offset_of_aWhbUpAtiftmjIGzRSoPwmEnzb_6() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___aWhbUpAtiftmjIGzRSoPwmEnzb_6)); }
	inline int32_t get_aWhbUpAtiftmjIGzRSoPwmEnzb_6() const { return ___aWhbUpAtiftmjIGzRSoPwmEnzb_6; }
	inline int32_t* get_address_of_aWhbUpAtiftmjIGzRSoPwmEnzb_6() { return &___aWhbUpAtiftmjIGzRSoPwmEnzb_6; }
	inline void set_aWhbUpAtiftmjIGzRSoPwmEnzb_6(int32_t value)
	{
		___aWhbUpAtiftmjIGzRSoPwmEnzb_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QNHIHBNYGCBSFOWNHMJVNVYTPXM_T3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4_H
#ifndef ZSSAVPFNUYBPGVXQQMQBVEPVQJCO_T2FC14182EE0D01A0915FBE74BC6FE5FE071F7B68_H
#define ZSSAVPFNUYBPGVXQQMQBVEPVQJCO_T2FC14182EE0D01A0915FBE74BC6FE5FE071F7B68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_zSsAVpfNUYBPgVxqQMqbVEPVqJCO
struct  zSsAVpfNUYBPgVxqQMqbVEPVqJCO_t2FC14182EE0D01A0915FBE74BC6FE5FE071F7B68 
{
public:
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_zSsAVpfNUYBPgVxqQMqbVEPVqJCO::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(zSsAVpfNUYBPgVxqQMqbVEPVqJCO_t2FC14182EE0D01A0915FBE74BC6FE5FE071F7B68, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZSSAVPFNUYBPGVXQQMQBVEPVQJCO_T2FC14182EE0D01A0915FBE74BC6FE5FE071F7B68_H
#ifndef QGJNESSSNZSLMPHWBTXQPCMNOWQ_T763F7594985FF852608F0BE4356621CC5A42DE8D_H
#define QGJNESSSNZSLMPHWBTXQPCMNOWQ_T763F7594985FF852608F0BE4356621CC5A42DE8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// qgjNESsSnzSlMpHwBtxQPCMNOwq
struct  qgjNESsSnzSlMpHwBtxQPCMNOwq_t763F7594985FF852608F0BE4356621CC5A42DE8D 
{
public:
	// System.Int32 qgjNESsSnzSlMpHwBtxQPCMNOwq::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(qgjNESsSnzSlMpHwBtxQPCMNOwq_t763F7594985FF852608F0BE4356621CC5A42DE8D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QGJNESSSNZSLMPHWBTXQPCMNOWQ_T763F7594985FF852608F0BE4356621CC5A42DE8D_H
#ifndef XHNVLCKSUDXVFJOOBCKJTHSIFWDB_T1D62E3F1CF98F64BBE43F320A063B46FEB0CBCD5_H
#define XHNVLCKSUDXVFJOOBCKJTHSIFWDB_T1D62E3F1CF98F64BBE43F320A063B46FEB0CBCD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// xHnvlcKSudxVFJOObckJtHsIfwDB
struct  xHnvlcKSudxVFJOObckJtHsIfwDB_t1D62E3F1CF98F64BBE43F320A063B46FEB0CBCD5 
{
public:
	// System.Int32 xHnvlcKSudxVFJOObckJtHsIfwDB::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(xHnvlcKSudxVFJOObckJtHsIfwDB_t1D62E3F1CF98F64BBE43F320A063B46FEB0CBCD5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XHNVLCKSUDXVFJOOBCKJTHSIFWDB_T1D62E3F1CF98F64BBE43F320A063B46FEB0CBCD5_H
#ifndef YDIMZUFFVMUNTOIIJEYTBOMBALJ_TD984A3F451D6EC1352F9810F5C89709BB0F53387_H
#define YDIMZUFFVMUNTOIIJEYTBOMBALJ_TD984A3F451D6EC1352F9810F5C89709BB0F53387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ydiMzuFfVMUntoIIJEytBOmbAlJ
struct  ydiMzuFfVMUntoIIJEytBOmbAlJ_tD984A3F451D6EC1352F9810F5C89709BB0F53387 
{
public:
	// System.Int32 ydiMzuFfVMUntoIIJEytBOmbAlJ::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ydiMzuFfVMUntoIIJEytBOmbAlJ_tD984A3F451D6EC1352F9810F5C89709BB0F53387, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YDIMZUFFVMUNTOIIJEYTBOMBALJ_TD984A3F451D6EC1352F9810F5C89709BB0F53387_H
#ifndef CONTROLLERIDENTIFIER_T725D2A7A9387931F93131874C32B08A3BCFF9DD6_H
#define CONTROLLERIDENTIFIER_T725D2A7A9387931F93131874C32B08A3BCFF9DD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerIdentifier
struct  ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6 
{
public:
	// System.Int32 Rewired.ControllerIdentifier::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_0;
	// Rewired.ControllerType Rewired.ControllerIdentifier::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1;
	// System.Guid Rewired.ControllerIdentifier::hVfvAlYZzngYWNUKHfQeqsdsSsN
	Guid_t  ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2;
	// System.String Rewired.ControllerIdentifier::fGtUJMoDYGQBkIHcDCTnfmUopvdN
	String_t* ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3;
	// System.Guid Rewired.ControllerIdentifier::uoQfWOPIJljmoZOdOiUDjMDpAEI
	Guid_t  ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4;

public:
	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_0() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___CdiTZueJOweHxLVLesdWcZkZxNV_0)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_0() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_0; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_0() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_0; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_0(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_0 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1 = value;
	}

	inline static int32_t get_offset_of_hVfvAlYZzngYWNUKHfQeqsdsSsN_2() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2)); }
	inline Guid_t  get_hVfvAlYZzngYWNUKHfQeqsdsSsN_2() const { return ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2; }
	inline Guid_t * get_address_of_hVfvAlYZzngYWNUKHfQeqsdsSsN_2() { return &___hVfvAlYZzngYWNUKHfQeqsdsSsN_2; }
	inline void set_hVfvAlYZzngYWNUKHfQeqsdsSsN_2(Guid_t  value)
	{
		___hVfvAlYZzngYWNUKHfQeqsdsSsN_2 = value;
	}

	inline static int32_t get_offset_of_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3)); }
	inline String_t* get_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3() const { return ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3; }
	inline String_t** get_address_of_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3() { return &___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3; }
	inline void set_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3(String_t* value)
	{
		___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3 = value;
		Il2CppCodeGenWriteBarrier((&___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3), value);
	}

	inline static int32_t get_offset_of_uoQfWOPIJljmoZOdOiUDjMDpAEI_4() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4)); }
	inline Guid_t  get_uoQfWOPIJljmoZOdOiUDjMDpAEI_4() const { return ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4; }
	inline Guid_t * get_address_of_uoQfWOPIJljmoZOdOiUDjMDpAEI_4() { return &___uoQfWOPIJljmoZOdOiUDjMDpAEI_4; }
	inline void set_uoQfWOPIJljmoZOdOiUDjMDpAEI_4(Guid_t  value)
	{
		___uoQfWOPIJljmoZOdOiUDjMDpAEI_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ControllerIdentifier
struct ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6_marshaled_pinvoke
{
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_0;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1;
	Guid_t  ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2;
	char* ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3;
	Guid_t  ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4;
};
// Native definition for COM marshalling of Rewired.ControllerIdentifier
struct ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6_marshaled_com
{
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_0;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1;
	Guid_t  ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2;
	Il2CppChar* ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3;
	Guid_t  ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4;
};
#endif // CONTROLLERIDENTIFIER_T725D2A7A9387931F93131874C32B08A3BCFF9DD6_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef LNGIGGEOZTFHOKVPOBLVJBATVLYD_T36643A533B08EAB0B35FCE169CD422956CFC6489_H
#define LNGIGGEOZTFHOKVPOBLVJBATVLYD_T36643A533B08EAB0B35FCE169CD422956CFC6489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd
struct  lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489  : public RuntimeObject
{
public:
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::avoCosfGauXluftrTLZWMUMjMSvo
	int32_t ___avoCosfGauXluftrTLZWMUMjMSvo_0;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::vHhTUeEjpjXweHfcvokTPubqCBh
	int32_t ___vHhTUeEjpjXweHfcvokTPubqCBh_1;
	// System.Guid qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::mMvnhxixERRYbVLPXEwyRrtXYwt
	Guid_t  ___mMvnhxixERRYbVLPXEwyRrtXYwt_2;
	// System.String qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::espHBjclwGrxdYkIErbggrUHnMo
	String_t* ___espHBjclwGrxdYkIErbggrUHnMo_3;
	// MUJAEshscmeXKPKBeiQXLdyHSgZ qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::uyozhMHHhGNcfEwvclTjPxbbvxZ
	RuntimeObject* ___uyozhMHHhGNcfEwvclTjPxbbvxZ_4;
	// qgjNESsSnzSlMpHwBtxQPCMNOwq qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::hBqzkAFqJcxfwCrSYqTdWPhwRxi
	int32_t ___hBqzkAFqJcxfwCrSYqTdWPhwRxi_5;
	// System.String qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::amhGqEYcVGlMzaBhudMaalhDCbSB
	String_t* ___amhGqEYcVGlMzaBhudMaalhDCbSB_6;
	// System.String qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::zTBxQtRhsiyWjlXakgtukSbwLlp
	String_t* ___zTBxQtRhsiyWjlXakgtukSbwLlp_7;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::KclFaRHPVyrRHSfCxDIVgvgADJgN
	int32_t ___KclFaRHPVyrRHSfCxDIVgvgADJgN_8;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::qNNdzuExLuNfrdyOzaNcsNVrpIQc
	int32_t ___qNNdzuExLuNfrdyOzaNcsNVrpIQc_9;
	// System.Guid qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::RhEAYdViDLywbAAxYxVbxUVxiMM
	Guid_t  ___RhEAYdViDLywbAAxYxVbxUVxiMM_10;
	// Rewired.PidVid qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::yauaFjfmZQzNovSFRGthYyFJrrj
	PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D  ___yauaFjfmZQzNovSFRGthYyFJrrj_11;
	// System.Guid qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::bLJDeZdmqhplurpuKUnpKCpNysd
	Guid_t  ___bLJDeZdmqhplurpuKUnpKCpNysd_12;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::NQcfCUdrzCInuZTLuEEoKhWjImdc
	int32_t ___NQcfCUdrzCInuZTLuEEoKhWjImdc_13;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::BIMocAZqmOYbcFTwmpmMNqQvglvi
	int32_t ___BIMocAZqmOYbcFTwmpmMNqQvglvi_14;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::AfHFnomuXngvpySlBnOGFBvmcpy
	int32_t ___AfHFnomuXngvpySlBnOGFBvmcpy_15;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::UdWYvkBYkFbtQVrPZwZuKINGjC
	int32_t ___UdWYvkBYkFbtQVrPZwZuKINGjC_16;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::VYSASJjqEIsmJcmnIHADUYEaRjQC
	int32_t ___VYSASJjqEIsmJcmnIHADUYEaRjQC_17;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::aWhbUpAtiftmjIGzRSoPwmEnzb
	int32_t ___aWhbUpAtiftmjIGzRSoPwmEnzb_18;
	// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::ibgNLLocbIyPDBErUPhHUTjDZKC
	bool ___ibgNLLocbIyPDBErUPhHUTjDZKC_19;
	// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::ZsEfMISqALipCaWIrZdQwCvMLkx
	bool ___ZsEfMISqALipCaWIrZdQwCvMLkx_20;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::YJKKCodZAAdHLAznZhhKAnRrzmS
	int32_t ___YJKKCodZAAdHLAznZhhKAnRrzmS_21;
	// System.Single[] qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::iKsRvufntavyFjWZbUibgspoftw
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___iKsRvufntavyFjWZbUibgspoftw_22;
	// System.Boolean[] qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::YuSoQESZTtPuhZKVJFBavCSGQsh
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___YuSoQESZTtPuhZKVJFBavCSGQsh_23;
	// Rewired.HardwareJoystickMap_InputManager qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::motlnrXwRclwbbdquGWkOEYsYFy
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 * ___motlnrXwRclwbbdquGWkOEYsYFy_24;
	// System.Func`2<Rewired.BridgedControllerHWInfo,Rewired.HardwareJoystickMap_InputManager> qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::QEAEcDsIVKSxlRksWqQtzkbPdqrh
	Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_25;
	// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::JKdifYgUghWnBkRBRChckuzXKWst
	bool ___JKdifYgUghWnBkRBRChckuzXKWst_26;
	// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::uTrFqghzwmUUNLMxoQSRVgfDfsj
	bool ___uTrFqghzwmUUNLMxoQSRVgfDfsj_27;
	// Rewired.Controller_Extension qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::pwFxxEkPXgSZokcVFZSbwgxzpUm
	Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * ___pwFxxEkPXgSZokcVFZSbwgxzpUm_28;

public:
	inline static int32_t get_offset_of_avoCosfGauXluftrTLZWMUMjMSvo_0() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___avoCosfGauXluftrTLZWMUMjMSvo_0)); }
	inline int32_t get_avoCosfGauXluftrTLZWMUMjMSvo_0() const { return ___avoCosfGauXluftrTLZWMUMjMSvo_0; }
	inline int32_t* get_address_of_avoCosfGauXluftrTLZWMUMjMSvo_0() { return &___avoCosfGauXluftrTLZWMUMjMSvo_0; }
	inline void set_avoCosfGauXluftrTLZWMUMjMSvo_0(int32_t value)
	{
		___avoCosfGauXluftrTLZWMUMjMSvo_0 = value;
	}

	inline static int32_t get_offset_of_vHhTUeEjpjXweHfcvokTPubqCBh_1() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___vHhTUeEjpjXweHfcvokTPubqCBh_1)); }
	inline int32_t get_vHhTUeEjpjXweHfcvokTPubqCBh_1() const { return ___vHhTUeEjpjXweHfcvokTPubqCBh_1; }
	inline int32_t* get_address_of_vHhTUeEjpjXweHfcvokTPubqCBh_1() { return &___vHhTUeEjpjXweHfcvokTPubqCBh_1; }
	inline void set_vHhTUeEjpjXweHfcvokTPubqCBh_1(int32_t value)
	{
		___vHhTUeEjpjXweHfcvokTPubqCBh_1 = value;
	}

	inline static int32_t get_offset_of_mMvnhxixERRYbVLPXEwyRrtXYwt_2() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___mMvnhxixERRYbVLPXEwyRrtXYwt_2)); }
	inline Guid_t  get_mMvnhxixERRYbVLPXEwyRrtXYwt_2() const { return ___mMvnhxixERRYbVLPXEwyRrtXYwt_2; }
	inline Guid_t * get_address_of_mMvnhxixERRYbVLPXEwyRrtXYwt_2() { return &___mMvnhxixERRYbVLPXEwyRrtXYwt_2; }
	inline void set_mMvnhxixERRYbVLPXEwyRrtXYwt_2(Guid_t  value)
	{
		___mMvnhxixERRYbVLPXEwyRrtXYwt_2 = value;
	}

	inline static int32_t get_offset_of_espHBjclwGrxdYkIErbggrUHnMo_3() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___espHBjclwGrxdYkIErbggrUHnMo_3)); }
	inline String_t* get_espHBjclwGrxdYkIErbggrUHnMo_3() const { return ___espHBjclwGrxdYkIErbggrUHnMo_3; }
	inline String_t** get_address_of_espHBjclwGrxdYkIErbggrUHnMo_3() { return &___espHBjclwGrxdYkIErbggrUHnMo_3; }
	inline void set_espHBjclwGrxdYkIErbggrUHnMo_3(String_t* value)
	{
		___espHBjclwGrxdYkIErbggrUHnMo_3 = value;
		Il2CppCodeGenWriteBarrier((&___espHBjclwGrxdYkIErbggrUHnMo_3), value);
	}

	inline static int32_t get_offset_of_uyozhMHHhGNcfEwvclTjPxbbvxZ_4() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___uyozhMHHhGNcfEwvclTjPxbbvxZ_4)); }
	inline RuntimeObject* get_uyozhMHHhGNcfEwvclTjPxbbvxZ_4() const { return ___uyozhMHHhGNcfEwvclTjPxbbvxZ_4; }
	inline RuntimeObject** get_address_of_uyozhMHHhGNcfEwvclTjPxbbvxZ_4() { return &___uyozhMHHhGNcfEwvclTjPxbbvxZ_4; }
	inline void set_uyozhMHHhGNcfEwvclTjPxbbvxZ_4(RuntimeObject* value)
	{
		___uyozhMHHhGNcfEwvclTjPxbbvxZ_4 = value;
		Il2CppCodeGenWriteBarrier((&___uyozhMHHhGNcfEwvclTjPxbbvxZ_4), value);
	}

	inline static int32_t get_offset_of_hBqzkAFqJcxfwCrSYqTdWPhwRxi_5() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___hBqzkAFqJcxfwCrSYqTdWPhwRxi_5)); }
	inline int32_t get_hBqzkAFqJcxfwCrSYqTdWPhwRxi_5() const { return ___hBqzkAFqJcxfwCrSYqTdWPhwRxi_5; }
	inline int32_t* get_address_of_hBqzkAFqJcxfwCrSYqTdWPhwRxi_5() { return &___hBqzkAFqJcxfwCrSYqTdWPhwRxi_5; }
	inline void set_hBqzkAFqJcxfwCrSYqTdWPhwRxi_5(int32_t value)
	{
		___hBqzkAFqJcxfwCrSYqTdWPhwRxi_5 = value;
	}

	inline static int32_t get_offset_of_amhGqEYcVGlMzaBhudMaalhDCbSB_6() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___amhGqEYcVGlMzaBhudMaalhDCbSB_6)); }
	inline String_t* get_amhGqEYcVGlMzaBhudMaalhDCbSB_6() const { return ___amhGqEYcVGlMzaBhudMaalhDCbSB_6; }
	inline String_t** get_address_of_amhGqEYcVGlMzaBhudMaalhDCbSB_6() { return &___amhGqEYcVGlMzaBhudMaalhDCbSB_6; }
	inline void set_amhGqEYcVGlMzaBhudMaalhDCbSB_6(String_t* value)
	{
		___amhGqEYcVGlMzaBhudMaalhDCbSB_6 = value;
		Il2CppCodeGenWriteBarrier((&___amhGqEYcVGlMzaBhudMaalhDCbSB_6), value);
	}

	inline static int32_t get_offset_of_zTBxQtRhsiyWjlXakgtukSbwLlp_7() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___zTBxQtRhsiyWjlXakgtukSbwLlp_7)); }
	inline String_t* get_zTBxQtRhsiyWjlXakgtukSbwLlp_7() const { return ___zTBxQtRhsiyWjlXakgtukSbwLlp_7; }
	inline String_t** get_address_of_zTBxQtRhsiyWjlXakgtukSbwLlp_7() { return &___zTBxQtRhsiyWjlXakgtukSbwLlp_7; }
	inline void set_zTBxQtRhsiyWjlXakgtukSbwLlp_7(String_t* value)
	{
		___zTBxQtRhsiyWjlXakgtukSbwLlp_7 = value;
		Il2CppCodeGenWriteBarrier((&___zTBxQtRhsiyWjlXakgtukSbwLlp_7), value);
	}

	inline static int32_t get_offset_of_KclFaRHPVyrRHSfCxDIVgvgADJgN_8() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___KclFaRHPVyrRHSfCxDIVgvgADJgN_8)); }
	inline int32_t get_KclFaRHPVyrRHSfCxDIVgvgADJgN_8() const { return ___KclFaRHPVyrRHSfCxDIVgvgADJgN_8; }
	inline int32_t* get_address_of_KclFaRHPVyrRHSfCxDIVgvgADJgN_8() { return &___KclFaRHPVyrRHSfCxDIVgvgADJgN_8; }
	inline void set_KclFaRHPVyrRHSfCxDIVgvgADJgN_8(int32_t value)
	{
		___KclFaRHPVyrRHSfCxDIVgvgADJgN_8 = value;
	}

	inline static int32_t get_offset_of_qNNdzuExLuNfrdyOzaNcsNVrpIQc_9() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___qNNdzuExLuNfrdyOzaNcsNVrpIQc_9)); }
	inline int32_t get_qNNdzuExLuNfrdyOzaNcsNVrpIQc_9() const { return ___qNNdzuExLuNfrdyOzaNcsNVrpIQc_9; }
	inline int32_t* get_address_of_qNNdzuExLuNfrdyOzaNcsNVrpIQc_9() { return &___qNNdzuExLuNfrdyOzaNcsNVrpIQc_9; }
	inline void set_qNNdzuExLuNfrdyOzaNcsNVrpIQc_9(int32_t value)
	{
		___qNNdzuExLuNfrdyOzaNcsNVrpIQc_9 = value;
	}

	inline static int32_t get_offset_of_RhEAYdViDLywbAAxYxVbxUVxiMM_10() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___RhEAYdViDLywbAAxYxVbxUVxiMM_10)); }
	inline Guid_t  get_RhEAYdViDLywbAAxYxVbxUVxiMM_10() const { return ___RhEAYdViDLywbAAxYxVbxUVxiMM_10; }
	inline Guid_t * get_address_of_RhEAYdViDLywbAAxYxVbxUVxiMM_10() { return &___RhEAYdViDLywbAAxYxVbxUVxiMM_10; }
	inline void set_RhEAYdViDLywbAAxYxVbxUVxiMM_10(Guid_t  value)
	{
		___RhEAYdViDLywbAAxYxVbxUVxiMM_10 = value;
	}

	inline static int32_t get_offset_of_yauaFjfmZQzNovSFRGthYyFJrrj_11() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___yauaFjfmZQzNovSFRGthYyFJrrj_11)); }
	inline PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D  get_yauaFjfmZQzNovSFRGthYyFJrrj_11() const { return ___yauaFjfmZQzNovSFRGthYyFJrrj_11; }
	inline PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D * get_address_of_yauaFjfmZQzNovSFRGthYyFJrrj_11() { return &___yauaFjfmZQzNovSFRGthYyFJrrj_11; }
	inline void set_yauaFjfmZQzNovSFRGthYyFJrrj_11(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D  value)
	{
		___yauaFjfmZQzNovSFRGthYyFJrrj_11 = value;
	}

	inline static int32_t get_offset_of_bLJDeZdmqhplurpuKUnpKCpNysd_12() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___bLJDeZdmqhplurpuKUnpKCpNysd_12)); }
	inline Guid_t  get_bLJDeZdmqhplurpuKUnpKCpNysd_12() const { return ___bLJDeZdmqhplurpuKUnpKCpNysd_12; }
	inline Guid_t * get_address_of_bLJDeZdmqhplurpuKUnpKCpNysd_12() { return &___bLJDeZdmqhplurpuKUnpKCpNysd_12; }
	inline void set_bLJDeZdmqhplurpuKUnpKCpNysd_12(Guid_t  value)
	{
		___bLJDeZdmqhplurpuKUnpKCpNysd_12 = value;
	}

	inline static int32_t get_offset_of_NQcfCUdrzCInuZTLuEEoKhWjImdc_13() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___NQcfCUdrzCInuZTLuEEoKhWjImdc_13)); }
	inline int32_t get_NQcfCUdrzCInuZTLuEEoKhWjImdc_13() const { return ___NQcfCUdrzCInuZTLuEEoKhWjImdc_13; }
	inline int32_t* get_address_of_NQcfCUdrzCInuZTLuEEoKhWjImdc_13() { return &___NQcfCUdrzCInuZTLuEEoKhWjImdc_13; }
	inline void set_NQcfCUdrzCInuZTLuEEoKhWjImdc_13(int32_t value)
	{
		___NQcfCUdrzCInuZTLuEEoKhWjImdc_13 = value;
	}

	inline static int32_t get_offset_of_BIMocAZqmOYbcFTwmpmMNqQvglvi_14() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___BIMocAZqmOYbcFTwmpmMNqQvglvi_14)); }
	inline int32_t get_BIMocAZqmOYbcFTwmpmMNqQvglvi_14() const { return ___BIMocAZqmOYbcFTwmpmMNqQvglvi_14; }
	inline int32_t* get_address_of_BIMocAZqmOYbcFTwmpmMNqQvglvi_14() { return &___BIMocAZqmOYbcFTwmpmMNqQvglvi_14; }
	inline void set_BIMocAZqmOYbcFTwmpmMNqQvglvi_14(int32_t value)
	{
		___BIMocAZqmOYbcFTwmpmMNqQvglvi_14 = value;
	}

	inline static int32_t get_offset_of_AfHFnomuXngvpySlBnOGFBvmcpy_15() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___AfHFnomuXngvpySlBnOGFBvmcpy_15)); }
	inline int32_t get_AfHFnomuXngvpySlBnOGFBvmcpy_15() const { return ___AfHFnomuXngvpySlBnOGFBvmcpy_15; }
	inline int32_t* get_address_of_AfHFnomuXngvpySlBnOGFBvmcpy_15() { return &___AfHFnomuXngvpySlBnOGFBvmcpy_15; }
	inline void set_AfHFnomuXngvpySlBnOGFBvmcpy_15(int32_t value)
	{
		___AfHFnomuXngvpySlBnOGFBvmcpy_15 = value;
	}

	inline static int32_t get_offset_of_UdWYvkBYkFbtQVrPZwZuKINGjC_16() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___UdWYvkBYkFbtQVrPZwZuKINGjC_16)); }
	inline int32_t get_UdWYvkBYkFbtQVrPZwZuKINGjC_16() const { return ___UdWYvkBYkFbtQVrPZwZuKINGjC_16; }
	inline int32_t* get_address_of_UdWYvkBYkFbtQVrPZwZuKINGjC_16() { return &___UdWYvkBYkFbtQVrPZwZuKINGjC_16; }
	inline void set_UdWYvkBYkFbtQVrPZwZuKINGjC_16(int32_t value)
	{
		___UdWYvkBYkFbtQVrPZwZuKINGjC_16 = value;
	}

	inline static int32_t get_offset_of_VYSASJjqEIsmJcmnIHADUYEaRjQC_17() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___VYSASJjqEIsmJcmnIHADUYEaRjQC_17)); }
	inline int32_t get_VYSASJjqEIsmJcmnIHADUYEaRjQC_17() const { return ___VYSASJjqEIsmJcmnIHADUYEaRjQC_17; }
	inline int32_t* get_address_of_VYSASJjqEIsmJcmnIHADUYEaRjQC_17() { return &___VYSASJjqEIsmJcmnIHADUYEaRjQC_17; }
	inline void set_VYSASJjqEIsmJcmnIHADUYEaRjQC_17(int32_t value)
	{
		___VYSASJjqEIsmJcmnIHADUYEaRjQC_17 = value;
	}

	inline static int32_t get_offset_of_aWhbUpAtiftmjIGzRSoPwmEnzb_18() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___aWhbUpAtiftmjIGzRSoPwmEnzb_18)); }
	inline int32_t get_aWhbUpAtiftmjIGzRSoPwmEnzb_18() const { return ___aWhbUpAtiftmjIGzRSoPwmEnzb_18; }
	inline int32_t* get_address_of_aWhbUpAtiftmjIGzRSoPwmEnzb_18() { return &___aWhbUpAtiftmjIGzRSoPwmEnzb_18; }
	inline void set_aWhbUpAtiftmjIGzRSoPwmEnzb_18(int32_t value)
	{
		___aWhbUpAtiftmjIGzRSoPwmEnzb_18 = value;
	}

	inline static int32_t get_offset_of_ibgNLLocbIyPDBErUPhHUTjDZKC_19() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___ibgNLLocbIyPDBErUPhHUTjDZKC_19)); }
	inline bool get_ibgNLLocbIyPDBErUPhHUTjDZKC_19() const { return ___ibgNLLocbIyPDBErUPhHUTjDZKC_19; }
	inline bool* get_address_of_ibgNLLocbIyPDBErUPhHUTjDZKC_19() { return &___ibgNLLocbIyPDBErUPhHUTjDZKC_19; }
	inline void set_ibgNLLocbIyPDBErUPhHUTjDZKC_19(bool value)
	{
		___ibgNLLocbIyPDBErUPhHUTjDZKC_19 = value;
	}

	inline static int32_t get_offset_of_ZsEfMISqALipCaWIrZdQwCvMLkx_20() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___ZsEfMISqALipCaWIrZdQwCvMLkx_20)); }
	inline bool get_ZsEfMISqALipCaWIrZdQwCvMLkx_20() const { return ___ZsEfMISqALipCaWIrZdQwCvMLkx_20; }
	inline bool* get_address_of_ZsEfMISqALipCaWIrZdQwCvMLkx_20() { return &___ZsEfMISqALipCaWIrZdQwCvMLkx_20; }
	inline void set_ZsEfMISqALipCaWIrZdQwCvMLkx_20(bool value)
	{
		___ZsEfMISqALipCaWIrZdQwCvMLkx_20 = value;
	}

	inline static int32_t get_offset_of_YJKKCodZAAdHLAznZhhKAnRrzmS_21() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___YJKKCodZAAdHLAznZhhKAnRrzmS_21)); }
	inline int32_t get_YJKKCodZAAdHLAznZhhKAnRrzmS_21() const { return ___YJKKCodZAAdHLAznZhhKAnRrzmS_21; }
	inline int32_t* get_address_of_YJKKCodZAAdHLAznZhhKAnRrzmS_21() { return &___YJKKCodZAAdHLAznZhhKAnRrzmS_21; }
	inline void set_YJKKCodZAAdHLAznZhhKAnRrzmS_21(int32_t value)
	{
		___YJKKCodZAAdHLAznZhhKAnRrzmS_21 = value;
	}

	inline static int32_t get_offset_of_iKsRvufntavyFjWZbUibgspoftw_22() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___iKsRvufntavyFjWZbUibgspoftw_22)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_iKsRvufntavyFjWZbUibgspoftw_22() const { return ___iKsRvufntavyFjWZbUibgspoftw_22; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_iKsRvufntavyFjWZbUibgspoftw_22() { return &___iKsRvufntavyFjWZbUibgspoftw_22; }
	inline void set_iKsRvufntavyFjWZbUibgspoftw_22(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___iKsRvufntavyFjWZbUibgspoftw_22 = value;
		Il2CppCodeGenWriteBarrier((&___iKsRvufntavyFjWZbUibgspoftw_22), value);
	}

	inline static int32_t get_offset_of_YuSoQESZTtPuhZKVJFBavCSGQsh_23() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___YuSoQESZTtPuhZKVJFBavCSGQsh_23)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_YuSoQESZTtPuhZKVJFBavCSGQsh_23() const { return ___YuSoQESZTtPuhZKVJFBavCSGQsh_23; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_YuSoQESZTtPuhZKVJFBavCSGQsh_23() { return &___YuSoQESZTtPuhZKVJFBavCSGQsh_23; }
	inline void set_YuSoQESZTtPuhZKVJFBavCSGQsh_23(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___YuSoQESZTtPuhZKVJFBavCSGQsh_23 = value;
		Il2CppCodeGenWriteBarrier((&___YuSoQESZTtPuhZKVJFBavCSGQsh_23), value);
	}

	inline static int32_t get_offset_of_motlnrXwRclwbbdquGWkOEYsYFy_24() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___motlnrXwRclwbbdquGWkOEYsYFy_24)); }
	inline HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 * get_motlnrXwRclwbbdquGWkOEYsYFy_24() const { return ___motlnrXwRclwbbdquGWkOEYsYFy_24; }
	inline HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 ** get_address_of_motlnrXwRclwbbdquGWkOEYsYFy_24() { return &___motlnrXwRclwbbdquGWkOEYsYFy_24; }
	inline void set_motlnrXwRclwbbdquGWkOEYsYFy_24(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 * value)
	{
		___motlnrXwRclwbbdquGWkOEYsYFy_24 = value;
		Il2CppCodeGenWriteBarrier((&___motlnrXwRclwbbdquGWkOEYsYFy_24), value);
	}

	inline static int32_t get_offset_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_25() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_25)); }
	inline Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * get_QEAEcDsIVKSxlRksWqQtzkbPdqrh_25() const { return ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_25; }
	inline Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF ** get_address_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_25() { return &___QEAEcDsIVKSxlRksWqQtzkbPdqrh_25; }
	inline void set_QEAEcDsIVKSxlRksWqQtzkbPdqrh_25(Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * value)
	{
		___QEAEcDsIVKSxlRksWqQtzkbPdqrh_25 = value;
		Il2CppCodeGenWriteBarrier((&___QEAEcDsIVKSxlRksWqQtzkbPdqrh_25), value);
	}

	inline static int32_t get_offset_of_JKdifYgUghWnBkRBRChckuzXKWst_26() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___JKdifYgUghWnBkRBRChckuzXKWst_26)); }
	inline bool get_JKdifYgUghWnBkRBRChckuzXKWst_26() const { return ___JKdifYgUghWnBkRBRChckuzXKWst_26; }
	inline bool* get_address_of_JKdifYgUghWnBkRBRChckuzXKWst_26() { return &___JKdifYgUghWnBkRBRChckuzXKWst_26; }
	inline void set_JKdifYgUghWnBkRBRChckuzXKWst_26(bool value)
	{
		___JKdifYgUghWnBkRBRChckuzXKWst_26 = value;
	}

	inline static int32_t get_offset_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_27() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___uTrFqghzwmUUNLMxoQSRVgfDfsj_27)); }
	inline bool get_uTrFqghzwmUUNLMxoQSRVgfDfsj_27() const { return ___uTrFqghzwmUUNLMxoQSRVgfDfsj_27; }
	inline bool* get_address_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_27() { return &___uTrFqghzwmUUNLMxoQSRVgfDfsj_27; }
	inline void set_uTrFqghzwmUUNLMxoQSRVgfDfsj_27(bool value)
	{
		___uTrFqghzwmUUNLMxoQSRVgfDfsj_27 = value;
	}

	inline static int32_t get_offset_of_pwFxxEkPXgSZokcVFZSbwgxzpUm_28() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___pwFxxEkPXgSZokcVFZSbwgxzpUm_28)); }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * get_pwFxxEkPXgSZokcVFZSbwgxzpUm_28() const { return ___pwFxxEkPXgSZokcVFZSbwgxzpUm_28; }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD ** get_address_of_pwFxxEkPXgSZokcVFZSbwgxzpUm_28() { return &___pwFxxEkPXgSZokcVFZSbwgxzpUm_28; }
	inline void set_pwFxxEkPXgSZokcVFZSbwgxzpUm_28(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * value)
	{
		___pwFxxEkPXgSZokcVFZSbwgxzpUm_28 = value;
		Il2CppCodeGenWriteBarrier((&___pwFxxEkPXgSZokcVFZSbwgxzpUm_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LNGIGGEOZTFHOKVPOBLVJBATVLYD_T36643A533B08EAB0B35FCE169CD422956CFC6489_H
#ifndef SZJMXPILCMPJYOBHNSPXYLIJAOB_TE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B_H
#define SZJMXPILCMPJYOBHNSPXYLIJAOB_TE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// szJmXpILCMpJYoBHnspxyLIJAob
struct  szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B 
{
public:
	// Rewired.ModifierKey szJmXpILCMpJYoBHnspxyLIJAob::mnGBxrKlrMEtcbDkElEqgOWkWuSS
	int32_t ___mnGBxrKlrMEtcbDkElEqgOWkWuSS_0;
	// Rewired.ModifierKey szJmXpILCMpJYoBHnspxyLIJAob::LtcbhcBgmsmnragehPolhLChilja
	int32_t ___LtcbhcBgmsmnragehPolhLChilja_1;
	// Rewired.ModifierKey szJmXpILCMpJYoBHnspxyLIJAob::nwYAkwIgQIppweizjiwEOkDJuCOQ
	int32_t ___nwYAkwIgQIppweizjiwEOkDJuCOQ_2;

public:
	inline static int32_t get_offset_of_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0() { return static_cast<int32_t>(offsetof(szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B, ___mnGBxrKlrMEtcbDkElEqgOWkWuSS_0)); }
	inline int32_t get_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0() const { return ___mnGBxrKlrMEtcbDkElEqgOWkWuSS_0; }
	inline int32_t* get_address_of_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0() { return &___mnGBxrKlrMEtcbDkElEqgOWkWuSS_0; }
	inline void set_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0(int32_t value)
	{
		___mnGBxrKlrMEtcbDkElEqgOWkWuSS_0 = value;
	}

	inline static int32_t get_offset_of_LtcbhcBgmsmnragehPolhLChilja_1() { return static_cast<int32_t>(offsetof(szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B, ___LtcbhcBgmsmnragehPolhLChilja_1)); }
	inline int32_t get_LtcbhcBgmsmnragehPolhLChilja_1() const { return ___LtcbhcBgmsmnragehPolhLChilja_1; }
	inline int32_t* get_address_of_LtcbhcBgmsmnragehPolhLChilja_1() { return &___LtcbhcBgmsmnragehPolhLChilja_1; }
	inline void set_LtcbhcBgmsmnragehPolhLChilja_1(int32_t value)
	{
		___LtcbhcBgmsmnragehPolhLChilja_1 = value;
	}

	inline static int32_t get_offset_of_nwYAkwIgQIppweizjiwEOkDJuCOQ_2() { return static_cast<int32_t>(offsetof(szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B, ___nwYAkwIgQIppweizjiwEOkDJuCOQ_2)); }
	inline int32_t get_nwYAkwIgQIppweizjiwEOkDJuCOQ_2() const { return ___nwYAkwIgQIppweizjiwEOkDJuCOQ_2; }
	inline int32_t* get_address_of_nwYAkwIgQIppweizjiwEOkDJuCOQ_2() { return &___nwYAkwIgQIppweizjiwEOkDJuCOQ_2; }
	inline void set_nwYAkwIgQIppweizjiwEOkDJuCOQ_2(int32_t value)
	{
		___nwYAkwIgQIppweizjiwEOkDJuCOQ_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SZJMXPILCMPJYOBHNSPXYLIJAOB_TE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B_H
#ifndef XVUPNQNWMCDQDJULQTYAIYRAXWG_TD9F87B3D114024B9D7527B698A80BE4245E1D032_H
#define XVUPNQNWMCDQDJULQTYAIYRAXWG_TD9F87B3D114024B9D7527B698A80BE4245E1D032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// xvUPNQnWmcdqDjulQTyAIyRAxwG
struct  xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032  : public RuntimeObject
{
public:
	// System.Single xvUPNQnWmcdqDjulQTyAIyRAxwG::ujWJxEJcSVHSdnQYrtHnXDWQpnZ
	float ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0;
	// Rewired.ButtonStateFlags xvUPNQnWmcdqDjulQTyAIyRAxwG::UWLOvhrjuLGUzrhYiVuOYFsiVgW
	int32_t ___UWLOvhrjuLGUzrhYiVuOYFsiVgW_1;
	// Rewired.ControllerElementType xvUPNQnWmcdqDjulQTyAIyRAxwG::EDrPGdciFGATRHQnPSEzqJMwhNu
	int32_t ___EDrPGdciFGATRHQnPSEzqJMwhNu_2;
	// Rewired.ControllerType xvUPNQnWmcdqDjulQTyAIyRAxwG::jeLkzDDlxjdyTfidRDslEjdPyAn
	int32_t ___jeLkzDDlxjdyTfidRDslEjdPyAn_3;
	// Rewired.Controller xvUPNQnWmcdqDjulQTyAIyRAxwG::QaeAlqSBKVmDRaDTcYzPlckcanC
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___QaeAlqSBKVmDRaDTcYzPlckcanC_4;
	// Rewired.ControllerMap xvUPNQnWmcdqDjulQTyAIyRAxwG::eDZexKHojMIOIcGgWyZwyijReQJ
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___eDZexKHojMIOIcGgWyZwyijReQJ_5;
	// Rewired.ActionElementMap xvUPNQnWmcdqDjulQTyAIyRAxwG::CPuSQMXEaFTdKTziCifqLUFakpe
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___CPuSQMXEaFTdKTziCifqLUFakpe_6;
	// System.Int32 xvUPNQnWmcdqDjulQTyAIyRAxwG::maMGvvcIaoKXvKiwHYWRqpyTbPEe
	int32_t ___maMGvvcIaoKXvKiwHYWRqpyTbPEe_7;
	// System.Boolean xvUPNQnWmcdqDjulQTyAIyRAxwG::VLynsvsphSPgOwnGXUBktrUfpoK
	bool ___VLynsvsphSPgOwnGXUBktrUfpoK_8;
	// System.Boolean xvUPNQnWmcdqDjulQTyAIyRAxwG::iMsaNGIEEvaEEkHBQjTXifqapVaA
	bool ___iMsaNGIEEvaEEkHBQjTXifqapVaA_9;
	// Rewired.AxisCoordinateMode xvUPNQnWmcdqDjulQTyAIyRAxwG::AzcaYoGJNjQgjFntSQDdTBEQrJJI
	int32_t ___AzcaYoGJNjQgjFntSQDdTBEQrJJI_10;

public:
	inline static int32_t get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0)); }
	inline float get_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0() const { return ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0; }
	inline float* get_address_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0() { return &___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0; }
	inline void set_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0(float value)
	{
		___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0 = value;
	}

	inline static int32_t get_offset_of_UWLOvhrjuLGUzrhYiVuOYFsiVgW_1() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___UWLOvhrjuLGUzrhYiVuOYFsiVgW_1)); }
	inline int32_t get_UWLOvhrjuLGUzrhYiVuOYFsiVgW_1() const { return ___UWLOvhrjuLGUzrhYiVuOYFsiVgW_1; }
	inline int32_t* get_address_of_UWLOvhrjuLGUzrhYiVuOYFsiVgW_1() { return &___UWLOvhrjuLGUzrhYiVuOYFsiVgW_1; }
	inline void set_UWLOvhrjuLGUzrhYiVuOYFsiVgW_1(int32_t value)
	{
		___UWLOvhrjuLGUzrhYiVuOYFsiVgW_1 = value;
	}

	inline static int32_t get_offset_of_EDrPGdciFGATRHQnPSEzqJMwhNu_2() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___EDrPGdciFGATRHQnPSEzqJMwhNu_2)); }
	inline int32_t get_EDrPGdciFGATRHQnPSEzqJMwhNu_2() const { return ___EDrPGdciFGATRHQnPSEzqJMwhNu_2; }
	inline int32_t* get_address_of_EDrPGdciFGATRHQnPSEzqJMwhNu_2() { return &___EDrPGdciFGATRHQnPSEzqJMwhNu_2; }
	inline void set_EDrPGdciFGATRHQnPSEzqJMwhNu_2(int32_t value)
	{
		___EDrPGdciFGATRHQnPSEzqJMwhNu_2 = value;
	}

	inline static int32_t get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_3() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___jeLkzDDlxjdyTfidRDslEjdPyAn_3)); }
	inline int32_t get_jeLkzDDlxjdyTfidRDslEjdPyAn_3() const { return ___jeLkzDDlxjdyTfidRDslEjdPyAn_3; }
	inline int32_t* get_address_of_jeLkzDDlxjdyTfidRDslEjdPyAn_3() { return &___jeLkzDDlxjdyTfidRDslEjdPyAn_3; }
	inline void set_jeLkzDDlxjdyTfidRDslEjdPyAn_3(int32_t value)
	{
		___jeLkzDDlxjdyTfidRDslEjdPyAn_3 = value;
	}

	inline static int32_t get_offset_of_QaeAlqSBKVmDRaDTcYzPlckcanC_4() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___QaeAlqSBKVmDRaDTcYzPlckcanC_4)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_QaeAlqSBKVmDRaDTcYzPlckcanC_4() const { return ___QaeAlqSBKVmDRaDTcYzPlckcanC_4; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_QaeAlqSBKVmDRaDTcYzPlckcanC_4() { return &___QaeAlqSBKVmDRaDTcYzPlckcanC_4; }
	inline void set_QaeAlqSBKVmDRaDTcYzPlckcanC_4(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___QaeAlqSBKVmDRaDTcYzPlckcanC_4 = value;
		Il2CppCodeGenWriteBarrier((&___QaeAlqSBKVmDRaDTcYzPlckcanC_4), value);
	}

	inline static int32_t get_offset_of_eDZexKHojMIOIcGgWyZwyijReQJ_5() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___eDZexKHojMIOIcGgWyZwyijReQJ_5)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_eDZexKHojMIOIcGgWyZwyijReQJ_5() const { return ___eDZexKHojMIOIcGgWyZwyijReQJ_5; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_eDZexKHojMIOIcGgWyZwyijReQJ_5() { return &___eDZexKHojMIOIcGgWyZwyijReQJ_5; }
	inline void set_eDZexKHojMIOIcGgWyZwyijReQJ_5(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___eDZexKHojMIOIcGgWyZwyijReQJ_5 = value;
		Il2CppCodeGenWriteBarrier((&___eDZexKHojMIOIcGgWyZwyijReQJ_5), value);
	}

	inline static int32_t get_offset_of_CPuSQMXEaFTdKTziCifqLUFakpe_6() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___CPuSQMXEaFTdKTziCifqLUFakpe_6)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_CPuSQMXEaFTdKTziCifqLUFakpe_6() const { return ___CPuSQMXEaFTdKTziCifqLUFakpe_6; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_CPuSQMXEaFTdKTziCifqLUFakpe_6() { return &___CPuSQMXEaFTdKTziCifqLUFakpe_6; }
	inline void set_CPuSQMXEaFTdKTziCifqLUFakpe_6(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___CPuSQMXEaFTdKTziCifqLUFakpe_6 = value;
		Il2CppCodeGenWriteBarrier((&___CPuSQMXEaFTdKTziCifqLUFakpe_6), value);
	}

	inline static int32_t get_offset_of_maMGvvcIaoKXvKiwHYWRqpyTbPEe_7() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___maMGvvcIaoKXvKiwHYWRqpyTbPEe_7)); }
	inline int32_t get_maMGvvcIaoKXvKiwHYWRqpyTbPEe_7() const { return ___maMGvvcIaoKXvKiwHYWRqpyTbPEe_7; }
	inline int32_t* get_address_of_maMGvvcIaoKXvKiwHYWRqpyTbPEe_7() { return &___maMGvvcIaoKXvKiwHYWRqpyTbPEe_7; }
	inline void set_maMGvvcIaoKXvKiwHYWRqpyTbPEe_7(int32_t value)
	{
		___maMGvvcIaoKXvKiwHYWRqpyTbPEe_7 = value;
	}

	inline static int32_t get_offset_of_VLynsvsphSPgOwnGXUBktrUfpoK_8() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___VLynsvsphSPgOwnGXUBktrUfpoK_8)); }
	inline bool get_VLynsvsphSPgOwnGXUBktrUfpoK_8() const { return ___VLynsvsphSPgOwnGXUBktrUfpoK_8; }
	inline bool* get_address_of_VLynsvsphSPgOwnGXUBktrUfpoK_8() { return &___VLynsvsphSPgOwnGXUBktrUfpoK_8; }
	inline void set_VLynsvsphSPgOwnGXUBktrUfpoK_8(bool value)
	{
		___VLynsvsphSPgOwnGXUBktrUfpoK_8 = value;
	}

	inline static int32_t get_offset_of_iMsaNGIEEvaEEkHBQjTXifqapVaA_9() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___iMsaNGIEEvaEEkHBQjTXifqapVaA_9)); }
	inline bool get_iMsaNGIEEvaEEkHBQjTXifqapVaA_9() const { return ___iMsaNGIEEvaEEkHBQjTXifqapVaA_9; }
	inline bool* get_address_of_iMsaNGIEEvaEEkHBQjTXifqapVaA_9() { return &___iMsaNGIEEvaEEkHBQjTXifqapVaA_9; }
	inline void set_iMsaNGIEEvaEEkHBQjTXifqapVaA_9(bool value)
	{
		___iMsaNGIEEvaEEkHBQjTXifqapVaA_9 = value;
	}

	inline static int32_t get_offset_of_AzcaYoGJNjQgjFntSQDdTBEQrJJI_10() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___AzcaYoGJNjQgjFntSQDdTBEQrJJI_10)); }
	inline int32_t get_AzcaYoGJNjQgjFntSQDdTBEQrJJI_10() const { return ___AzcaYoGJNjQgjFntSQDdTBEQrJJI_10; }
	inline int32_t* get_address_of_AzcaYoGJNjQgjFntSQDdTBEQrJJI_10() { return &___AzcaYoGJNjQgjFntSQDdTBEQrJJI_10; }
	inline void set_AzcaYoGJNjQgjFntSQDdTBEQrJJI_10(int32_t value)
	{
		___AzcaYoGJNjQgjFntSQDdTBEQrJJI_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XVUPNQNWMCDQDJULQTYAIYRAXWG_TD9F87B3D114024B9D7527B698A80BE4245E1D032_H
#ifndef CONTROLLER_T5780C278CCFB7A81A0272F3B056B1EF463D9B671_H
#define CONTROLLER_T5780C278CCFB7A81A0272F3B056B1EF463D9B671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller
struct  Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Controller::id
	int32_t ___id_0;
	// System.String Rewired.Controller::_tag
	String_t* ____tag_1;
	// System.String Rewired.Controller::_name
	String_t* ____name_2;
	// System.String Rewired.Controller::_hardwareName
	String_t* ____hardwareName_3;
	// Rewired.ControllerType Rewired.Controller::_type
	int32_t ____type_4;
	// System.Guid Rewired.Controller::hVfvAlYZzngYWNUKHfQeqsdsSsN
	Guid_t  ___hVfvAlYZzngYWNUKHfQeqsdsSsN_5;
	// System.String Rewired.Controller::_hardwareIdentifier
	String_t* ____hardwareIdentifier_6;
	// System.Boolean Rewired.Controller::_isConnected
	bool ____isConnected_7;
	// Rewired.Controller_Extension Rewired.Controller::keBYOCobzoABWhYylPhjApVSLXnN
	Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * ___keBYOCobzoABWhYylPhjApVSLXnN_8;
	// System.Boolean Rewired.Controller::eWTjKkaJVjCZqrsQHQXMtJYuNhD
	bool ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_9;
	// Rewired.ControllerIdentifier Rewired.Controller::VlCVoudhBiuwhdYzYzMjmHCukZv
	ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6  ___VlCVoudhBiuwhdYzYzMjmHCukZv_10;
	// System.Int32 Rewired.Controller::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_11;
	// System.Int32 Rewired.Controller::_buttonCount
	int32_t ____buttonCount_12;
	// Rewired.Controller_Button[] Rewired.Controller::buttons
	ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C* ___buttons_13;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller_Button> Rewired.Controller::buttons_readOnly
	ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383 * ___buttons_readOnly_14;
	// System.Collections.Generic.IList`1<Rewired.Controller_Element> Rewired.Controller::tqrCjCDiVROjgZQMTxdrZcCDZcA
	RuntimeObject* ___tqrCjCDiVROjgZQMTxdrZcCDZcA_15;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller_Element> Rewired.Controller::HeiaDLCywuXsOrgGQhDKAJyFIubw
	ReadOnlyCollection_1_tBF1621D3F7F9871C04D918E0DF726F42C36F91F4 * ___HeiaDLCywuXsOrgGQhDKAJyFIubw_16;
	// Rewired.InputSource Rewired.Controller::lHdmDauerBwdDwLUcaCxBythtdWu
	int32_t ___lHdmDauerBwdDwLUcaCxBythtdWu_17;
	// Rewired.ControllerDataUpdater Rewired.Controller::NYtqDwOoerMRNEOsCUlBWwihMNG
	ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F * ___NYtqDwOoerMRNEOsCUlBWwihMNG_18;
	// Rewired.HardwareControllerMap_Game Rewired.Controller::motlnrXwRclwbbdquGWkOEYsYFy
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * ___motlnrXwRclwbbdquGWkOEYsYFy_19;
	// System.UInt32 Rewired.Controller::chxPGnvGYohtyammAxrWAboQuVg
	uint32_t ___chxPGnvGYohtyammAxrWAboQuVg_20;
	// System.UInt32 Rewired.Controller::iaxnzSzdijUTJnJzreKDdKvgbmk
	uint32_t ___iaxnzSzdijUTJnJzreKDdKvgbmk_21;
	// System.UInt32 Rewired.Controller::lMdEFUXdOFLrujmaUFFpIWQxdWyh
	uint32_t ___lMdEFUXdOFLrujmaUFFpIWQxdWyh_22;
	// System.Action`1<System.Boolean> Rewired.Controller::yQeGjgudbMdGxktFDlTRKRPYNpwO
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___yQeGjgudbMdGxktFDlTRKRPYNpwO_23;
	// Rewired.IControllerTemplate[] Rewired.Controller::RrVywEyDCOVEzMNbWtqScfAjGSfA
	IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6* ___RrVywEyDCOVEzMNbWtqScfAjGSfA_24;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.IControllerTemplate> Rewired.Controller::PWJCQVymGuHZlwtENLzIkOlXzFO
	ReadOnlyCollection_1_t29628C2D08376268FE32FB6C6D7854BE606C1802 * ___PWJCQVymGuHZlwtENLzIkOlXzFO_25;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of__tag_1() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____tag_1)); }
	inline String_t* get__tag_1() const { return ____tag_1; }
	inline String_t** get_address_of__tag_1() { return &____tag_1; }
	inline void set__tag_1(String_t* value)
	{
		____tag_1 = value;
		Il2CppCodeGenWriteBarrier((&____tag_1), value);
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}

	inline static int32_t get_offset_of__hardwareName_3() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____hardwareName_3)); }
	inline String_t* get__hardwareName_3() const { return ____hardwareName_3; }
	inline String_t** get_address_of__hardwareName_3() { return &____hardwareName_3; }
	inline void set__hardwareName_3(String_t* value)
	{
		____hardwareName_3 = value;
		Il2CppCodeGenWriteBarrier((&____hardwareName_3), value);
	}

	inline static int32_t get_offset_of__type_4() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____type_4)); }
	inline int32_t get__type_4() const { return ____type_4; }
	inline int32_t* get_address_of__type_4() { return &____type_4; }
	inline void set__type_4(int32_t value)
	{
		____type_4 = value;
	}

	inline static int32_t get_offset_of_hVfvAlYZzngYWNUKHfQeqsdsSsN_5() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___hVfvAlYZzngYWNUKHfQeqsdsSsN_5)); }
	inline Guid_t  get_hVfvAlYZzngYWNUKHfQeqsdsSsN_5() const { return ___hVfvAlYZzngYWNUKHfQeqsdsSsN_5; }
	inline Guid_t * get_address_of_hVfvAlYZzngYWNUKHfQeqsdsSsN_5() { return &___hVfvAlYZzngYWNUKHfQeqsdsSsN_5; }
	inline void set_hVfvAlYZzngYWNUKHfQeqsdsSsN_5(Guid_t  value)
	{
		___hVfvAlYZzngYWNUKHfQeqsdsSsN_5 = value;
	}

	inline static int32_t get_offset_of__hardwareIdentifier_6() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____hardwareIdentifier_6)); }
	inline String_t* get__hardwareIdentifier_6() const { return ____hardwareIdentifier_6; }
	inline String_t** get_address_of__hardwareIdentifier_6() { return &____hardwareIdentifier_6; }
	inline void set__hardwareIdentifier_6(String_t* value)
	{
		____hardwareIdentifier_6 = value;
		Il2CppCodeGenWriteBarrier((&____hardwareIdentifier_6), value);
	}

	inline static int32_t get_offset_of__isConnected_7() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____isConnected_7)); }
	inline bool get__isConnected_7() const { return ____isConnected_7; }
	inline bool* get_address_of__isConnected_7() { return &____isConnected_7; }
	inline void set__isConnected_7(bool value)
	{
		____isConnected_7 = value;
	}

	inline static int32_t get_offset_of_keBYOCobzoABWhYylPhjApVSLXnN_8() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___keBYOCobzoABWhYylPhjApVSLXnN_8)); }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * get_keBYOCobzoABWhYylPhjApVSLXnN_8() const { return ___keBYOCobzoABWhYylPhjApVSLXnN_8; }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD ** get_address_of_keBYOCobzoABWhYylPhjApVSLXnN_8() { return &___keBYOCobzoABWhYylPhjApVSLXnN_8; }
	inline void set_keBYOCobzoABWhYylPhjApVSLXnN_8(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * value)
	{
		___keBYOCobzoABWhYylPhjApVSLXnN_8 = value;
		Il2CppCodeGenWriteBarrier((&___keBYOCobzoABWhYylPhjApVSLXnN_8), value);
	}

	inline static int32_t get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_9() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_9)); }
	inline bool get_eWTjKkaJVjCZqrsQHQXMtJYuNhD_9() const { return ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_9; }
	inline bool* get_address_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_9() { return &___eWTjKkaJVjCZqrsQHQXMtJYuNhD_9; }
	inline void set_eWTjKkaJVjCZqrsQHQXMtJYuNhD_9(bool value)
	{
		___eWTjKkaJVjCZqrsQHQXMtJYuNhD_9 = value;
	}

	inline static int32_t get_offset_of_VlCVoudhBiuwhdYzYzMjmHCukZv_10() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___VlCVoudhBiuwhdYzYzMjmHCukZv_10)); }
	inline ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6  get_VlCVoudhBiuwhdYzYzMjmHCukZv_10() const { return ___VlCVoudhBiuwhdYzYzMjmHCukZv_10; }
	inline ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6 * get_address_of_VlCVoudhBiuwhdYzYzMjmHCukZv_10() { return &___VlCVoudhBiuwhdYzYzMjmHCukZv_10; }
	inline void set_VlCVoudhBiuwhdYzYzMjmHCukZv_10(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6  value)
	{
		___VlCVoudhBiuwhdYzYzMjmHCukZv_10 = value;
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_11() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_11)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_11() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_11; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_11() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_11; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_11(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_11 = value;
	}

	inline static int32_t get_offset_of__buttonCount_12() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____buttonCount_12)); }
	inline int32_t get__buttonCount_12() const { return ____buttonCount_12; }
	inline int32_t* get_address_of__buttonCount_12() { return &____buttonCount_12; }
	inline void set__buttonCount_12(int32_t value)
	{
		____buttonCount_12 = value;
	}

	inline static int32_t get_offset_of_buttons_13() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___buttons_13)); }
	inline ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C* get_buttons_13() const { return ___buttons_13; }
	inline ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C** get_address_of_buttons_13() { return &___buttons_13; }
	inline void set_buttons_13(ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C* value)
	{
		___buttons_13 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_13), value);
	}

	inline static int32_t get_offset_of_buttons_readOnly_14() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___buttons_readOnly_14)); }
	inline ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383 * get_buttons_readOnly_14() const { return ___buttons_readOnly_14; }
	inline ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383 ** get_address_of_buttons_readOnly_14() { return &___buttons_readOnly_14; }
	inline void set_buttons_readOnly_14(ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383 * value)
	{
		___buttons_readOnly_14 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_readOnly_14), value);
	}

	inline static int32_t get_offset_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_15() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___tqrCjCDiVROjgZQMTxdrZcCDZcA_15)); }
	inline RuntimeObject* get_tqrCjCDiVROjgZQMTxdrZcCDZcA_15() const { return ___tqrCjCDiVROjgZQMTxdrZcCDZcA_15; }
	inline RuntimeObject** get_address_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_15() { return &___tqrCjCDiVROjgZQMTxdrZcCDZcA_15; }
	inline void set_tqrCjCDiVROjgZQMTxdrZcCDZcA_15(RuntimeObject* value)
	{
		___tqrCjCDiVROjgZQMTxdrZcCDZcA_15 = value;
		Il2CppCodeGenWriteBarrier((&___tqrCjCDiVROjgZQMTxdrZcCDZcA_15), value);
	}

	inline static int32_t get_offset_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_16() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___HeiaDLCywuXsOrgGQhDKAJyFIubw_16)); }
	inline ReadOnlyCollection_1_tBF1621D3F7F9871C04D918E0DF726F42C36F91F4 * get_HeiaDLCywuXsOrgGQhDKAJyFIubw_16() const { return ___HeiaDLCywuXsOrgGQhDKAJyFIubw_16; }
	inline ReadOnlyCollection_1_tBF1621D3F7F9871C04D918E0DF726F42C36F91F4 ** get_address_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_16() { return &___HeiaDLCywuXsOrgGQhDKAJyFIubw_16; }
	inline void set_HeiaDLCywuXsOrgGQhDKAJyFIubw_16(ReadOnlyCollection_1_tBF1621D3F7F9871C04D918E0DF726F42C36F91F4 * value)
	{
		___HeiaDLCywuXsOrgGQhDKAJyFIubw_16 = value;
		Il2CppCodeGenWriteBarrier((&___HeiaDLCywuXsOrgGQhDKAJyFIubw_16), value);
	}

	inline static int32_t get_offset_of_lHdmDauerBwdDwLUcaCxBythtdWu_17() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___lHdmDauerBwdDwLUcaCxBythtdWu_17)); }
	inline int32_t get_lHdmDauerBwdDwLUcaCxBythtdWu_17() const { return ___lHdmDauerBwdDwLUcaCxBythtdWu_17; }
	inline int32_t* get_address_of_lHdmDauerBwdDwLUcaCxBythtdWu_17() { return &___lHdmDauerBwdDwLUcaCxBythtdWu_17; }
	inline void set_lHdmDauerBwdDwLUcaCxBythtdWu_17(int32_t value)
	{
		___lHdmDauerBwdDwLUcaCxBythtdWu_17 = value;
	}

	inline static int32_t get_offset_of_NYtqDwOoerMRNEOsCUlBWwihMNG_18() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___NYtqDwOoerMRNEOsCUlBWwihMNG_18)); }
	inline ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F * get_NYtqDwOoerMRNEOsCUlBWwihMNG_18() const { return ___NYtqDwOoerMRNEOsCUlBWwihMNG_18; }
	inline ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F ** get_address_of_NYtqDwOoerMRNEOsCUlBWwihMNG_18() { return &___NYtqDwOoerMRNEOsCUlBWwihMNG_18; }
	inline void set_NYtqDwOoerMRNEOsCUlBWwihMNG_18(ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F * value)
	{
		___NYtqDwOoerMRNEOsCUlBWwihMNG_18 = value;
		Il2CppCodeGenWriteBarrier((&___NYtqDwOoerMRNEOsCUlBWwihMNG_18), value);
	}

	inline static int32_t get_offset_of_motlnrXwRclwbbdquGWkOEYsYFy_19() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___motlnrXwRclwbbdquGWkOEYsYFy_19)); }
	inline HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * get_motlnrXwRclwbbdquGWkOEYsYFy_19() const { return ___motlnrXwRclwbbdquGWkOEYsYFy_19; }
	inline HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 ** get_address_of_motlnrXwRclwbbdquGWkOEYsYFy_19() { return &___motlnrXwRclwbbdquGWkOEYsYFy_19; }
	inline void set_motlnrXwRclwbbdquGWkOEYsYFy_19(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * value)
	{
		___motlnrXwRclwbbdquGWkOEYsYFy_19 = value;
		Il2CppCodeGenWriteBarrier((&___motlnrXwRclwbbdquGWkOEYsYFy_19), value);
	}

	inline static int32_t get_offset_of_chxPGnvGYohtyammAxrWAboQuVg_20() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___chxPGnvGYohtyammAxrWAboQuVg_20)); }
	inline uint32_t get_chxPGnvGYohtyammAxrWAboQuVg_20() const { return ___chxPGnvGYohtyammAxrWAboQuVg_20; }
	inline uint32_t* get_address_of_chxPGnvGYohtyammAxrWAboQuVg_20() { return &___chxPGnvGYohtyammAxrWAboQuVg_20; }
	inline void set_chxPGnvGYohtyammAxrWAboQuVg_20(uint32_t value)
	{
		___chxPGnvGYohtyammAxrWAboQuVg_20 = value;
	}

	inline static int32_t get_offset_of_iaxnzSzdijUTJnJzreKDdKvgbmk_21() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___iaxnzSzdijUTJnJzreKDdKvgbmk_21)); }
	inline uint32_t get_iaxnzSzdijUTJnJzreKDdKvgbmk_21() const { return ___iaxnzSzdijUTJnJzreKDdKvgbmk_21; }
	inline uint32_t* get_address_of_iaxnzSzdijUTJnJzreKDdKvgbmk_21() { return &___iaxnzSzdijUTJnJzreKDdKvgbmk_21; }
	inline void set_iaxnzSzdijUTJnJzreKDdKvgbmk_21(uint32_t value)
	{
		___iaxnzSzdijUTJnJzreKDdKvgbmk_21 = value;
	}

	inline static int32_t get_offset_of_lMdEFUXdOFLrujmaUFFpIWQxdWyh_22() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___lMdEFUXdOFLrujmaUFFpIWQxdWyh_22)); }
	inline uint32_t get_lMdEFUXdOFLrujmaUFFpIWQxdWyh_22() const { return ___lMdEFUXdOFLrujmaUFFpIWQxdWyh_22; }
	inline uint32_t* get_address_of_lMdEFUXdOFLrujmaUFFpIWQxdWyh_22() { return &___lMdEFUXdOFLrujmaUFFpIWQxdWyh_22; }
	inline void set_lMdEFUXdOFLrujmaUFFpIWQxdWyh_22(uint32_t value)
	{
		___lMdEFUXdOFLrujmaUFFpIWQxdWyh_22 = value;
	}

	inline static int32_t get_offset_of_yQeGjgudbMdGxktFDlTRKRPYNpwO_23() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___yQeGjgudbMdGxktFDlTRKRPYNpwO_23)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_yQeGjgudbMdGxktFDlTRKRPYNpwO_23() const { return ___yQeGjgudbMdGxktFDlTRKRPYNpwO_23; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_yQeGjgudbMdGxktFDlTRKRPYNpwO_23() { return &___yQeGjgudbMdGxktFDlTRKRPYNpwO_23; }
	inline void set_yQeGjgudbMdGxktFDlTRKRPYNpwO_23(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___yQeGjgudbMdGxktFDlTRKRPYNpwO_23 = value;
		Il2CppCodeGenWriteBarrier((&___yQeGjgudbMdGxktFDlTRKRPYNpwO_23), value);
	}

	inline static int32_t get_offset_of_RrVywEyDCOVEzMNbWtqScfAjGSfA_24() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___RrVywEyDCOVEzMNbWtqScfAjGSfA_24)); }
	inline IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6* get_RrVywEyDCOVEzMNbWtqScfAjGSfA_24() const { return ___RrVywEyDCOVEzMNbWtqScfAjGSfA_24; }
	inline IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6** get_address_of_RrVywEyDCOVEzMNbWtqScfAjGSfA_24() { return &___RrVywEyDCOVEzMNbWtqScfAjGSfA_24; }
	inline void set_RrVywEyDCOVEzMNbWtqScfAjGSfA_24(IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6* value)
	{
		___RrVywEyDCOVEzMNbWtqScfAjGSfA_24 = value;
		Il2CppCodeGenWriteBarrier((&___RrVywEyDCOVEzMNbWtqScfAjGSfA_24), value);
	}

	inline static int32_t get_offset_of_PWJCQVymGuHZlwtENLzIkOlXzFO_25() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___PWJCQVymGuHZlwtENLzIkOlXzFO_25)); }
	inline ReadOnlyCollection_1_t29628C2D08376268FE32FB6C6D7854BE606C1802 * get_PWJCQVymGuHZlwtENLzIkOlXzFO_25() const { return ___PWJCQVymGuHZlwtENLzIkOlXzFO_25; }
	inline ReadOnlyCollection_1_t29628C2D08376268FE32FB6C6D7854BE606C1802 ** get_address_of_PWJCQVymGuHZlwtENLzIkOlXzFO_25() { return &___PWJCQVymGuHZlwtENLzIkOlXzFO_25; }
	inline void set_PWJCQVymGuHZlwtENLzIkOlXzFO_25(ReadOnlyCollection_1_t29628C2D08376268FE32FB6C6D7854BE606C1802 * value)
	{
		___PWJCQVymGuHZlwtENLzIkOlXzFO_25 = value;
		Il2CppCodeGenWriteBarrier((&___PWJCQVymGuHZlwtENLzIkOlXzFO_25), value);
	}
};

struct Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields
{
public:
	// System.Func`3<Rewired.Controller,System.Guid,System.Boolean> Rewired.Controller::kpwpfTuXUblRlYTjfSeSLEksNTd
	Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE * ___kpwpfTuXUblRlYTjfSeSLEksNTd_26;
	// System.Func`3<Rewired.Controller,System.Type,System.Boolean> Rewired.Controller::fpQFUcijUIunFTiZNbdIrMQqJvMf
	Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 * ___fpQFUcijUIunFTiZNbdIrMQqJvMf_27;
	// System.Func`3<Rewired.Controller,System.Guid,System.Boolean> Rewired.Controller::jqUTJMOeCBaPwenidONjsgqqmhF
	Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE * ___jqUTJMOeCBaPwenidONjsgqqmhF_28;
	// System.Func`3<Rewired.Controller,System.Type,System.Boolean> Rewired.Controller::yGfaFeDTgjQCiyfEnFKwcIrTEKK
	Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 * ___yGfaFeDTgjQCiyfEnFKwcIrTEKK_29;

public:
	inline static int32_t get_offset_of_kpwpfTuXUblRlYTjfSeSLEksNTd_26() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields, ___kpwpfTuXUblRlYTjfSeSLEksNTd_26)); }
	inline Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE * get_kpwpfTuXUblRlYTjfSeSLEksNTd_26() const { return ___kpwpfTuXUblRlYTjfSeSLEksNTd_26; }
	inline Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE ** get_address_of_kpwpfTuXUblRlYTjfSeSLEksNTd_26() { return &___kpwpfTuXUblRlYTjfSeSLEksNTd_26; }
	inline void set_kpwpfTuXUblRlYTjfSeSLEksNTd_26(Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE * value)
	{
		___kpwpfTuXUblRlYTjfSeSLEksNTd_26 = value;
		Il2CppCodeGenWriteBarrier((&___kpwpfTuXUblRlYTjfSeSLEksNTd_26), value);
	}

	inline static int32_t get_offset_of_fpQFUcijUIunFTiZNbdIrMQqJvMf_27() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields, ___fpQFUcijUIunFTiZNbdIrMQqJvMf_27)); }
	inline Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 * get_fpQFUcijUIunFTiZNbdIrMQqJvMf_27() const { return ___fpQFUcijUIunFTiZNbdIrMQqJvMf_27; }
	inline Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 ** get_address_of_fpQFUcijUIunFTiZNbdIrMQqJvMf_27() { return &___fpQFUcijUIunFTiZNbdIrMQqJvMf_27; }
	inline void set_fpQFUcijUIunFTiZNbdIrMQqJvMf_27(Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 * value)
	{
		___fpQFUcijUIunFTiZNbdIrMQqJvMf_27 = value;
		Il2CppCodeGenWriteBarrier((&___fpQFUcijUIunFTiZNbdIrMQqJvMf_27), value);
	}

	inline static int32_t get_offset_of_jqUTJMOeCBaPwenidONjsgqqmhF_28() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields, ___jqUTJMOeCBaPwenidONjsgqqmhF_28)); }
	inline Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE * get_jqUTJMOeCBaPwenidONjsgqqmhF_28() const { return ___jqUTJMOeCBaPwenidONjsgqqmhF_28; }
	inline Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE ** get_address_of_jqUTJMOeCBaPwenidONjsgqqmhF_28() { return &___jqUTJMOeCBaPwenidONjsgqqmhF_28; }
	inline void set_jqUTJMOeCBaPwenidONjsgqqmhF_28(Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE * value)
	{
		___jqUTJMOeCBaPwenidONjsgqqmhF_28 = value;
		Il2CppCodeGenWriteBarrier((&___jqUTJMOeCBaPwenidONjsgqqmhF_28), value);
	}

	inline static int32_t get_offset_of_yGfaFeDTgjQCiyfEnFKwcIrTEKK_29() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields, ___yGfaFeDTgjQCiyfEnFKwcIrTEKK_29)); }
	inline Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 * get_yGfaFeDTgjQCiyfEnFKwcIrTEKK_29() const { return ___yGfaFeDTgjQCiyfEnFKwcIrTEKK_29; }
	inline Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 ** get_address_of_yGfaFeDTgjQCiyfEnFKwcIrTEKK_29() { return &___yGfaFeDTgjQCiyfEnFKwcIrTEKK_29; }
	inline void set_yGfaFeDTgjQCiyfEnFKwcIrTEKK_29(Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 * value)
	{
		___yGfaFeDTgjQCiyfEnFKwcIrTEKK_29 = value;
		Il2CppCodeGenWriteBarrier((&___yGfaFeDTgjQCiyfEnFKwcIrTEKK_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLER_T5780C278CCFB7A81A0272F3B056B1EF463D9B671_H
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty[]
struct mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * m_Items[1];

public:
	inline mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void List_1_RemoveAt_m1EC5117AD814B97460F8F95D49A428032FE37CBF_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 Rewired.Utils.Classes.Data.AList`1<System.Object>::Add(T)
extern "C" IL2CPP_METHOD_ATTR int32_t AList_1_Add_m64CA272B57A1FDE366247CAEA5744C27C1AEEECB_gshared (AList_1_t6040A9B07E2104841A5B217A9311F51920A362AE * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void Rewired.Utils.Classes.Data.AList`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AList_1__ctor_m942792AA4658B30765ED194FD07256274CDE1EAE_gshared (AList_1_t6040A9B07E2104841A5B217A9311F51920A362AE * __this, const RuntimeMethod* method);
// System.Boolean Rewired.Utils.Classes.Data.AList`1<System.Object>::Remove(T)
extern "C" IL2CPP_METHOD_ATTR bool AList_1_Remove_m9D38C6B751235F8F6FEF63A48F64D29A265487AC_gshared (AList_1_t6040A9B07E2104841A5B217A9311F51920A362AE * __this, RuntimeObject * p0, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa/QnHiHbnYGCBSFowNhmjvNvYtpXM>::.ctor()
inline void List_1__ctor_mB7126049CA5EEEB8853C16CDD3ABD1D0447E0B65 (List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa/QnHiHbnYGCBSFowNhmjvNvYtpXM>::get_Count()
inline int32_t List_1_get_Count_m4781C1E7D158FE73AE5792C98211036217CDD9B8 (List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared)(__this, method);
}
// System.Void qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa/QnHiHbnYGCBSFowNhmjvNvYtpXM::.ctor()
extern "C" IL2CPP_METHOD_ATTR void QnHiHbnYGCBSFowNhmjvNvYtpXM__ctor_mBA422540D78046CAC0BBF8E741DD729867995228 (QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * __this, const RuntimeMethod* method);
// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH/lNgIgGeozTFHoKvpOBlVjbAtvLyd::get_rewiredId()
extern "C" IL2CPP_METHOD_ATTR int32_t lNgIgGeozTFHoKvpOBlVjbAtvLyd_get_rewiredId_m31F382FBD51FDA347FDFAF8B6696A4ED3179799A (lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * __this, const RuntimeMethod* method);
// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH/lNgIgGeozTFHoKvpOBlVjbAtvLyd::get_inputManagerId()
extern "C" IL2CPP_METHOD_ATTR int32_t lNgIgGeozTFHoKvpOBlVjbAtvLyd_get_inputManagerId_m349E1F38D7EF28BA49B2E02D46F70A1B5600839C (lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa/QnHiHbnYGCBSFowNhmjvNvYtpXM>::Add(!0)
inline void List_1_Add_m379D7CE5CF8DEC6CDDD7E4C2261E6E81CA1D9E88 (List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * __this, QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF *, QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// !0 System.Collections.Generic.List`1<qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa/QnHiHbnYGCBSFowNhmjvNvYtpXM>::get_Item(System.Int32)
inline QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA (List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * (*) (List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Void qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa::mRQRPVJljvuBdBeduwoolvGdjPA(System.Int32,System.Guid,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void tLqZqETDWvmFaUyFhoSscPGCHa_mRQRPVJljvuBdBeduwoolvGdjPA_m9E3A1DC07C170F846ACE888531DCEF41468C10DB (tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773 * __this, int32_t p0, Guid_t  p1, int32_t p2, const RuntimeMethod* method);
// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa/QnHiHbnYGCBSFowNhmjvNvYtpXM::rQVCTzxWRBTHwULULWYBtzSTRee(qQSKQqvJRxFNPvSjTomQuZOntrH/lNgIgGeozTFHoKvpOBlVjbAtvLyd,qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa/zSsAVpfNUYBPgVxqQMqbVEPVqJCO)
extern "C" IL2CPP_METHOD_ATTR bool QnHiHbnYGCBSFowNhmjvNvYtpXM_rQVCTzxWRBTHwULULWYBtzSTRee_m022C9BF3BD1E87FE37078791C6135EC63CC7E491 (QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * __this, lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * p0, int32_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa/QnHiHbnYGCBSFowNhmjvNvYtpXM>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_mFF720D6CA965549F775ABF1FB5A65662C057E67C (List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m1EC5117AD814B97460F8F95D49A428032FE37CBF_gshared)(__this, p0, method);
}
// System.Boolean System.Guid::op_Equality(System.Guid,System.Guid)
extern "C" IL2CPP_METHOD_ATTR bool Guid_op_Equality_m3D98BA18CDAF0C6C329F86720B5CD61A170A3E52 (Guid_t  p0, Guid_t  p1, const RuntimeMethod* method);
// System.Void System.NotImplementedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotImplementedException__ctor_m8BEA657E260FC05F0C6D2C43A6E9BC08040F59C4 (NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4 * __this, const RuntimeMethod* method);
// System.Void rIcvLcWCuQghgaaiofvKjSvndJX/mGZguqdUJPvKvEhsdVIRYohlOty::.ctor(System.Type)
extern "C" IL2CPP_METHOD_ATTR void mGZguqdUJPvKvEhsdVIRYohlOty__ctor_m544770ECC483EEE5DD5D78E02667618ACD3D11D6 (mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Int32 Rewired.Utils.Classes.Data.AList`1<rIcvLcWCuQghgaaiofvKjSvndJX/mGZguqdUJPvKvEhsdVIRYohlOty>::Add(T)
inline int32_t AList_1_Add_m312C99F6192A18AB1E766E10CDFCC198694273D1 (AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * __this, mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * p0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 *, mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 *, const RuntimeMethod*))AList_1_Add_m64CA272B57A1FDE366247CAEA5744C27C1AEEECB_gshared)(__this, p0, method);
}
// System.Void System.Exception::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0 (Exception_t * __this, String_t* p0, const RuntimeMethod* method);
// System.Void Rewired.Utils.Classes.Data.AList`1<rIcvLcWCuQghgaaiofvKjSvndJX/mGZguqdUJPvKvEhsdVIRYohlOty>::.ctor()
inline void AList_1__ctor_m185708896B55423867CBE384A91B2A3F155EE9BB (AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * __this, const RuntimeMethod* method)
{
	((  void (*) (AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 *, const RuntimeMethod*))AList_1__ctor_m942792AA4658B30765ED194FD07256274CDE1EAE_gshared)(__this, method);
}
// rIcvLcWCuQghgaaiofvKjSvndJX/mGZguqdUJPvKvEhsdVIRYohlOty rIcvLcWCuQghgaaiofvKjSvndJX::tcvmbTUcTycOBqbsOekuKTXnTvlg(System.Type)
extern "C" IL2CPP_METHOD_ATTR mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * rIcvLcWCuQghgaaiofvKjSvndJX_tcvmbTUcTycOBqbsOekuKTXnTvlg_mA19A5F3F3A3BEEF63DF269DC5800679208C2757C (rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * __this, Type_t * p0, const RuntimeMethod* method);
// System.Type System.Object::GetType()
extern "C" IL2CPP_METHOD_ATTR Type_t * Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Type rIcvLcWCuQghgaaiofvKjSvndJX::EIDXeKXbSIrkasbXSElcJSYFHKB(System.Type)
extern "C" IL2CPP_METHOD_ATTR Type_t * rIcvLcWCuQghgaaiofvKjSvndJX_EIDXeKXbSIrkasbXSElcJSYFHKB_m638189E95A756923F2ABDC2D30A7B2A2B24E93AA (rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * __this, Type_t * p0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// System.Void Rewired.Logger::LogError(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Logger_LogError_mD867E7161DD5405B9A7B2B7E599B3283C07E6029 (RuntimeObject * ___msg0, const RuntimeMethod* method);
// System.Void rIcvLcWCuQghgaaiofvKjSvndJX/mGZguqdUJPvKvEhsdVIRYohlOty::HfTIrZGXLVOHtJVamKOSesGBJhAd(Rewired.IControllerTemplate)
extern "C" IL2CPP_METHOD_ATTR void mGZguqdUJPvKvEhsdVIRYohlOty_HfTIrZGXLVOHtJVamKOSesGBJhAd_mBF6D59118267024E5CACA02B7DFF3E12FA51B8E8 (mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Int32 Rewired.Controller::get_templateCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Controller_get_templateCount_mDA03FB01481C8DC35C340AD2E82DAB1AF2647C49 (Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IList`1<Rewired.IControllerTemplate> Rewired.Controller::get_Templates()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Controller_get_Templates_m5E4BFDFBC5E02E2D5C90E3C9B73761F23966848A (Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * __this, const RuntimeMethod* method);
// System.Void rIcvLcWCuQghgaaiofvKjSvndJX/mGZguqdUJPvKvEhsdVIRYohlOty::yhpcGPaRrkCsNfCHablLblmduzKL(Rewired.IControllerTemplate)
extern "C" IL2CPP_METHOD_ATTR void mGZguqdUJPvKvEhsdVIRYohlOty_yhpcGPaRrkCsNfCHablLblmduzKL_m0C4D04CA4019577CD998EA769D6CD52F64459055 (mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void Rewired.Utils.Classes.Data.AList`1<Rewired.IControllerTemplate>::.ctor()
inline void AList_1__ctor_mAC4A718F851BAFFFD2BFD4F2294903DF5A752CBA (AList_1_t974A66EA135B0300CE05935362C6C7C736059320 * __this, const RuntimeMethod* method)
{
	((  void (*) (AList_1_t974A66EA135B0300CE05935362C6C7C736059320 *, const RuntimeMethod*))AList_1__ctor_m942792AA4658B30765ED194FD07256274CDE1EAE_gshared)(__this, method);
}
// System.Int32 Rewired.Utils.Classes.Data.AList`1<Rewired.IControllerTemplate>::Add(T)
inline int32_t AList_1_Add_mFBC6CE8CADA220DFA6C8C2E04DE156D55F6FA850 (AList_1_t974A66EA135B0300CE05935362C6C7C736059320 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (AList_1_t974A66EA135B0300CE05935362C6C7C736059320 *, RuntimeObject*, const RuntimeMethod*))AList_1_Add_m64CA272B57A1FDE366247CAEA5744C27C1AEEECB_gshared)(__this, p0, method);
}
// System.Boolean Rewired.Utils.Classes.Data.AList`1<Rewired.IControllerTemplate>::Remove(T)
inline bool AList_1_Remove_m4E877B89C4BEEB27D88C72D27D173CE7FDB3B8BE (AList_1_t974A66EA135B0300CE05935362C6C7C736059320 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  bool (*) (AList_1_t974A66EA135B0300CE05935362C6C7C736059320 *, RuntimeObject*, const RuntimeMethod*))AList_1_Remove_m9D38C6B751235F8F6FEF63A48F64D29A265487AC_gshared)(__this, p0, method);
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * p0, const RuntimeMethod* method);
// Rewired.ModifierKey szJmXpILCMpJYoBHnspxyLIJAob::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t szJmXpILCMpJYoBHnspxyLIJAob_get_Item_mCF770935971354ACB80847CCDF1F5BD28343844E (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void szJmXpILCMpJYoBHnspxyLIJAob::set_Item(System.Int32,Rewired.ModifierKey)
extern "C" IL2CPP_METHOD_ATTR void szJmXpILCMpJYoBHnspxyLIJAob_set_Item_m9BC58E958D3AF599C3136D74AFC8CBEEF690C743 (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, int32_t ___index0, int32_t ___value1, const RuntimeMethod* method);
// System.Void szJmXpILCMpJYoBHnspxyLIJAob::.ctor(Rewired.ModifierKey,Rewired.ModifierKey,Rewired.ModifierKey)
extern "C" IL2CPP_METHOD_ATTR void szJmXpILCMpJYoBHnspxyLIJAob__ctor_m2AA846814F89820BCDE163061232DC8B0912BBBB (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, int32_t ___modifierKey10, int32_t ___modifierKey21, int32_t ___modifierKey32, const RuntimeMethod* method);
// System.Void szJmXpILCMpJYoBHnspxyLIJAob::KsUCrxkpJRkolcYgWumGYuxqCeFB()
extern "C" IL2CPP_METHOD_ATTR void szJmXpILCMpJYoBHnspxyLIJAob_KsUCrxkpJRkolcYgWumGYuxqCeFB_m753910193687ED68784706A5E93C17BA5ACAB1E0 (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, const RuntimeMethod* method);
// System.Boolean Rewired.Keyboard::ModifierKeyFlagsContain(Rewired.ModifierKeyFlags,Rewired.ModifierKey)
extern "C" IL2CPP_METHOD_ATTR bool Keyboard_ModifierKeyFlagsContain_m0C0AE5DE019717B1CA143A6B171E403B53E3FF60 (int32_t ___flags0, int32_t ___key1, const RuntimeMethod* method);
// System.Boolean szJmXpILCMpJYoBHnspxyLIJAob::Equals(szJmXpILCMpJYoBHnspxyLIJAob)
extern "C" IL2CPP_METHOD_ATTR bool szJmXpILCMpJYoBHnspxyLIJAob_Equals_m349FAED2699343DEC2F2EA44BF7BAA2DA0DF8CA8 (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  ___other0, const RuntimeMethod* method);
// System.Boolean szJmXpILCMpJYoBHnspxyLIJAob::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool szJmXpILCMpJYoBHnspxyLIJAob_Equals_m696F1EA3F652019ADF7F2E0DCAE7BE71F63C469D (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Int32 System.Int32::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t Int32_GetHashCode_m245C424ECE351E5FE3277A88EEB02132DAB8C25A (int32_t* __this, const RuntimeMethod* method);
// System.Int32 szJmXpILCMpJYoBHnspxyLIJAob::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t szJmXpILCMpJYoBHnspxyLIJAob_GetHashCode_m1CE2FB9D12F38DE1DC231A8C04D8AA6962CEEE75 (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, const RuntimeMethod* method);
// System.Boolean szJmXpILCMpJYoBHnspxyLIJAob::op_Equality(szJmXpILCMpJYoBHnspxyLIJAob,szJmXpILCMpJYoBHnspxyLIJAob)
extern "C" IL2CPP_METHOD_ATTR bool szJmXpILCMpJYoBHnspxyLIJAob_op_Equality_mC0329F9C051772871B889D895FA90432CC40D42D (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  ___a0, szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  ___b1, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7 (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * __this, String_t* p0, const RuntimeMethod* method);
// System.Boolean tirTbIgerWCchDwImTBhEXMbbyDC::aPDcQrAxDVfpHqyPrEgiDuiWmsWb(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool tirTbIgerWCchDwImTBhEXMbbyDC_aPDcQrAxDVfpHqyPrEgiDuiWmsWb_m5DA589A3287320E721A735A3978A4046BFA0EB2F (int32_t p0, const RuntimeMethod* method);
// System.Int32 tirTbIgerWCchDwImTBhEXMbbyDC::IIConZhTlDhyrUEUmJUbhmrJubd(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t tirTbIgerWCchDwImTBhEXMbbyDC_IIConZhTlDhyrUEUmJUbhmrJubd_m48AC53F1EA2DFC16CEF1B538F05F18F00EDB012E (int32_t p0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C" IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A (RuntimeArray * p0, RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  p1, const RuntimeMethod* method);
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.Byte)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m0C8AEAFA33EC5F222E5AF8F800E4B14FF1E898E0 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, uint8_t ___item0, const RuntimeMethod* method);
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.SByte)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m8F4523F3EC12BF60A64C9BDB32B0BEBF7A2AA1A3 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, int8_t ___item0, const RuntimeMethod* method);
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.Char)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mAF0678FB5A51C558C753BABBDA979A14090FE543 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, Il2CppChar ___item0, const RuntimeMethod* method);
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.Int16)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m871803D317A588AA09655DF15468C51C9E3199FB (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, int16_t ___item0, const RuntimeMethod* method);
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.UInt16)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m010ECA268A1AD0F48453089A49BDD474FAC57E14 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, uint16_t ___item0, const RuntimeMethod* method);
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m68321438AF088D4FD0EFE32AD5FEF7F1853FFD16 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, int32_t ___item0, const RuntimeMethod* method);
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.UInt32)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mF73F3B4D904B0C6B80C7A8CEC1FDCF84E4E2BD7A (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, uint32_t ___item0, const RuntimeMethod* method);
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mEFF87B0699854400E736A2FFE668B6CDF0DEA4A3 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, float ___item0, const RuntimeMethod* method);
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m02EE5FC729C82C032AC353BFEB62E106FD367804 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, bool ___item0, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void Rewired.Logger::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Logger_Log_m5DB6C749C057F4801A8DA9B700A85179B354F869 (RuntimeObject * ___msg0, const RuntimeMethod* method);
// System.Void Rewired.Logger::LogWarning(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Logger_LogWarning_m62073170C5D53E757FC5246E28635EAF44864197 (RuntimeObject * ___msg0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa::.ctor()
extern "C" IL2CPP_METHOD_ATTR void tLqZqETDWvmFaUyFhoSscPGCHa__ctor_m2D920DC9A17827969D4E71878C28273B958EFD60 (tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tLqZqETDWvmFaUyFhoSscPGCHa__ctor_m2D920DC9A17827969D4E71878C28273B958EFD60_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_0 = (List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF *)il2cpp_codegen_object_new(List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF_il2cpp_TypeInfo_var);
		List_1__ctor_mB7126049CA5EEEB8853C16CDD3ABD1D0447E0B65(L_0, /*hidden argument*/List_1__ctor_mB7126049CA5EEEB8853C16CDD3ABD1D0447E0B65_RuntimeMethod_var);
		__this->set_AHOFIsQodiSzMcHMSUpdlsGdKfH_0(L_0);
		return;
	}
}
// System.Void qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa::EOMjUgrIyGUAqNzWhSmDvzQVLte(qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd)
extern "C" IL2CPP_METHOD_ATTR void tLqZqETDWvmFaUyFhoSscPGCHa_EOMjUgrIyGUAqNzWhSmDvzQVLte_m1AE5CC3FC40DADA74E1773BADCEF84F6CCCA334F (tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773 * __this, lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tLqZqETDWvmFaUyFhoSscPGCHa_EOMjUgrIyGUAqNzWhSmDvzQVLte_m1AE5CC3FC40DADA74E1773BADCEF84F6CCCA334F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * V_2 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B13_1 = 0;
	int32_t G_B20_0 = 0;
	int32_t G_B20_1 = 0;
	{
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_0 = p0;
		if (L_0)
		{
			goto IL_0049;
		}
	}
	{
		return;
	}

IL_0004:
	{
		G_B3_0 = ((int32_t)877373942);
	}

IL_0009:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)877373951))))
		{
			case 0:
			{
				goto IL_0004;
			}
			case 1:
			{
				goto IL_01b9;
			}
			case 2:
			{
				goto IL_01f0;
			}
			case 3:
			{
				goto IL_0181;
			}
			case 4:
			{
				goto IL_014b;
			}
			case 5:
			{
				goto IL_00ce;
			}
			case 6:
			{
				goto IL_0223;
			}
			case 7:
			{
				goto IL_0219;
			}
			case 8:
			{
				goto IL_01c7;
			}
			case 9:
			{
				goto IL_0049;
			}
			case 10:
			{
				goto IL_005e;
			}
			case 11:
			{
				goto IL_0169;
			}
		}
	}
	{
		goto IL_0223;
	}

IL_0049:
	{
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_1 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m4781C1E7D158FE73AE5792C98211036217CDD9B8(L_1, /*hidden argument*/List_1_get_Count_m4781C1E7D158FE73AE5792C98211036217CDD9B8_RuntimeMethod_var);
		V_0 = L_2;
		V_1 = 0;
		G_B3_0 = ((int32_t)877373944);
		goto IL_0009;
	}

IL_005e:
	{
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_3 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_4 = (QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 *)il2cpp_codegen_object_new(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4_il2cpp_TypeInfo_var);
		QnHiHbnYGCBSFowNhmjvNvYtpXM__ctor_mBA422540D78046CAC0BBF8E741DD729867995228(L_4, /*hidden argument*/NULL);
		V_2 = L_4;
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_5 = V_2;
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_6 = p0;
		NullCheck(L_6);
		int32_t L_7 = lNgIgGeozTFHoKvpOBlVjbAtvLyd_get_rewiredId_m31F382FBD51FDA347FDFAF8B6696A4ED3179799A(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_nvorGNbxnIKRTEQakQRxoDaDbkF_0(L_7);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_8 = V_2;
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_9 = p0;
		NullCheck(L_9);
		Guid_t  L_10 = L_9->get_RhEAYdViDLywbAAxYxVbxUVxiMM_10();
		NullCheck(L_8);
		L_8->set_nlEaopEverUmqALsSlwZYfPupTlP_1(L_10);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_11 = V_2;
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_12 = p0;
		NullCheck(L_12);
		Guid_t  L_13 = L_12->get_bLJDeZdmqhplurpuKUnpKCpNysd_12();
		NullCheck(L_11);
		L_11->set_bLJDeZdmqhplurpuKUnpKCpNysd_2(L_13);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_14 = V_2;
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_15 = p0;
		NullCheck(L_15);
		int32_t L_16 = lNgIgGeozTFHoKvpOBlVjbAtvLyd_get_inputManagerId_m349E1F38D7EF28BA49B2E02D46F70A1B5600839C(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3(L_16);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_17 = V_2;
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_18 = p0;
		NullCheck(L_18);
		int32_t L_19 = L_18->get_UdWYvkBYkFbtQVrPZwZuKINGjC_16();
		NullCheck(L_17);
		L_17->set_UdWYvkBYkFbtQVrPZwZuKINGjC_4(L_19);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_20 = V_2;
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_21 = p0;
		NullCheck(L_21);
		int32_t L_22 = L_21->get_VYSASJjqEIsmJcmnIHADUYEaRjQC_17();
		NullCheck(L_20);
		L_20->set_VYSASJjqEIsmJcmnIHADUYEaRjQC_5(L_22);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_23 = V_2;
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_24 = p0;
		NullCheck(L_24);
		int32_t L_25 = L_24->get_aWhbUpAtiftmjIGzRSoPwmEnzb_18();
		NullCheck(L_23);
		L_23->set_aWhbUpAtiftmjIGzRSoPwmEnzb_6(L_25);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_26 = V_2;
		NullCheck(L_3);
		List_1_Add_m379D7CE5CF8DEC6CDDD7E4C2261E6E81CA1D9E88(L_3, L_26, /*hidden argument*/List_1_Add_m379D7CE5CF8DEC6CDDD7E4C2261E6E81CA1D9E88_RuntimeMethod_var);
		G_B3_0 = ((int32_t)877373943);
		goto IL_0009;
	}

IL_00ce:
	{
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_27 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_28 = V_1;
		NullCheck(L_27);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_29 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_27, L_28, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_30 = p0;
		NullCheck(L_30);
		int32_t L_31 = lNgIgGeozTFHoKvpOBlVjbAtvLyd_get_rewiredId_m31F382FBD51FDA347FDFAF8B6696A4ED3179799A(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		L_29->set_nvorGNbxnIKRTEQakQRxoDaDbkF_0(L_31);
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_32 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_33 = V_1;
		NullCheck(L_32);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_34 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_32, L_33, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_35 = p0;
		NullCheck(L_35);
		Guid_t  L_36 = L_35->get_RhEAYdViDLywbAAxYxVbxUVxiMM_10();
		NullCheck(L_34);
		L_34->set_nlEaopEverUmqALsSlwZYfPupTlP_1(L_36);
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_37 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_38 = V_1;
		NullCheck(L_37);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_39 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_37, L_38, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_40 = p0;
		NullCheck(L_40);
		Guid_t  L_41 = L_40->get_bLJDeZdmqhplurpuKUnpKCpNysd_12();
		NullCheck(L_39);
		L_39->set_bLJDeZdmqhplurpuKUnpKCpNysd_2(L_41);
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_42 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_43 = V_1;
		NullCheck(L_42);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_44 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_42, L_43, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_45 = p0;
		NullCheck(L_45);
		int32_t L_46 = lNgIgGeozTFHoKvpOBlVjbAtvLyd_get_inputManagerId_m349E1F38D7EF28BA49B2E02D46F70A1B5600839C(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		L_44->set_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3(L_46);
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_47 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_48 = V_1;
		NullCheck(L_47);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_49 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_47, L_48, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_50 = p0;
		NullCheck(L_50);
		int32_t L_51 = L_50->get_UdWYvkBYkFbtQVrPZwZuKINGjC_16();
		NullCheck(L_49);
		L_49->set_UdWYvkBYkFbtQVrPZwZuKINGjC_4(L_51);
		G_B3_0 = ((int32_t)877373948);
		goto IL_0009;
	}

IL_014b:
	{
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_52 = p0;
		NullCheck(L_52);
		int32_t L_53 = lNgIgGeozTFHoKvpOBlVjbAtvLyd_get_rewiredId_m31F382FBD51FDA347FDFAF8B6696A4ED3179799A(L_52, /*hidden argument*/NULL);
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_54 = p0;
		NullCheck(L_54);
		Guid_t  L_55 = L_54->get_RhEAYdViDLywbAAxYxVbxUVxiMM_10();
		int32_t L_56 = V_1;
		tLqZqETDWvmFaUyFhoSscPGCHa_mRQRPVJljvuBdBeduwoolvGdjPA_m9E3A1DC07C170F846ACE888531DCEF41468C10DB(__this, L_53, L_55, L_56, /*hidden argument*/NULL);
		return;
	}
	// Dead block : IL_015f: ldc.i4 877373950

IL_0169:
	{
		int32_t L_57 = V_1;
		int32_t L_58 = V_0;
		if ((((int32_t)L_57) < ((int32_t)L_58)))
		{
			goto IL_0175;
		}
	}
	{
		int32_t L_59 = ((int32_t)877373941);
		G_B13_0 = L_59;
		G_B13_1 = L_59;
		goto IL_017b;
	}

IL_0175:
	{
		int32_t L_60 = ((int32_t)877373949);
		G_B13_0 = L_60;
		G_B13_1 = L_60;
	}

IL_017b:
	{
		G_B3_0 = G_B13_1;
		goto IL_0009;
	}

IL_0181:
	{
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_61 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_62 = V_1;
		NullCheck(L_61);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_63 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_61, L_62, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_64 = p0;
		NullCheck(L_64);
		int32_t L_65 = L_64->get_VYSASJjqEIsmJcmnIHADUYEaRjQC_17();
		NullCheck(L_63);
		L_63->set_VYSASJjqEIsmJcmnIHADUYEaRjQC_5(L_65);
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_66 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_67 = V_1;
		NullCheck(L_66);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_68 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_66, L_67, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_69 = p0;
		NullCheck(L_69);
		int32_t L_70 = L_69->get_aWhbUpAtiftmjIGzRSoPwmEnzb_18();
		NullCheck(L_68);
		L_68->set_aWhbUpAtiftmjIGzRSoPwmEnzb_6(L_70);
		G_B3_0 = ((int32_t)877373947);
		goto IL_0009;
	}

IL_01b9:
	{
		int32_t L_71 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_71, (int32_t)1));
		G_B3_0 = ((int32_t)877373940);
		goto IL_0009;
	}

IL_01c7:
	{
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_72 = p0;
		NullCheck(L_72);
		int32_t L_73 = lNgIgGeozTFHoKvpOBlVjbAtvLyd_get_rewiredId_m31F382FBD51FDA347FDFAF8B6696A4ED3179799A(L_72, /*hidden argument*/NULL);
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_74 = p0;
		NullCheck(L_74);
		Guid_t  L_75 = L_74->get_RhEAYdViDLywbAAxYxVbxUVxiMM_10();
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_76 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		NullCheck(L_76);
		int32_t L_77 = List_1_get_Count_m4781C1E7D158FE73AE5792C98211036217CDD9B8(L_76, /*hidden argument*/List_1_get_Count_m4781C1E7D158FE73AE5792C98211036217CDD9B8_RuntimeMethod_var);
		tLqZqETDWvmFaUyFhoSscPGCHa_mRQRPVJljvuBdBeduwoolvGdjPA_m9E3A1DC07C170F846ACE888531DCEF41468C10DB(__this, L_73, L_75, ((int32_t)il2cpp_codegen_subtract((int32_t)L_77, (int32_t)1)), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)877373945);
		goto IL_0009;
	}

IL_01f0:
	{
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_78 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_79 = V_1;
		NullCheck(L_78);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_80 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_78, L_79, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_81 = p0;
		NullCheck(L_80);
		bool L_82 = QnHiHbnYGCBSFowNhmjvNvYtpXM_rQVCTzxWRBTHwULULWYBtzSTRee_m022C9BF3BD1E87FE37078791C6135EC63CC7E491(L_80, L_81, 0, /*hidden argument*/NULL);
		if (!L_82)
		{
			goto IL_020d;
		}
	}
	{
		int32_t L_83 = ((int32_t)877373946);
		G_B20_0 = L_83;
		G_B20_1 = L_83;
		goto IL_0213;
	}

IL_020d:
	{
		int32_t L_84 = ((int32_t)877373950);
		G_B20_0 = L_84;
		G_B20_1 = L_84;
	}

IL_0213:
	{
		G_B3_0 = G_B20_1;
		goto IL_0009;
	}

IL_0219:
	{
		G_B3_0 = ((int32_t)877373940);
		goto IL_0009;
	}

IL_0223:
	{
		return;
	}
}
// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa::hLFpabYsqKQzNRTSZlJUSavKbbnc(qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd,qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_zSsAVpfNUYBPgVxqQMqbVEPVqJCO)
extern "C" IL2CPP_METHOD_ATTR bool tLqZqETDWvmFaUyFhoSscPGCHa_hLFpabYsqKQzNRTSZlJUSavKbbnc_mFAE0A9186D75E723B30CE54A43C20048FBC9C8BF (tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773 * __this, lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * p0, int32_t p1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tLqZqETDWvmFaUyFhoSscPGCHa_hLFpabYsqKQzNRTSZlJUSavKbbnc_mFAE0A9186D75E723B30CE54A43C20048FBC9C8BF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_0 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m4781C1E7D158FE73AE5792C98211036217CDD9B8(L_0, /*hidden argument*/List_1_get_Count_m4781C1E7D158FE73AE5792C98211036217CDD9B8_RuntimeMethod_var);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0050;
	}

IL_0010:
	{
		G_B2_0 = ((int32_t)-55282404);
	}

IL_0015:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-55282402))))
		{
			case 0:
			{
				goto IL_0010;
			}
			case 1:
			{
				goto IL_0050;
			}
			case 2:
			{
				goto IL_002e;
			}
		}
	}
	{
		goto IL_0050;
	}

IL_002e:
	{
		CHECK_PAUSE_POINT;
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_2 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_4 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_2, L_3, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_5 = p0;
		int32_t L_6 = p1;
		NullCheck(L_4);
		bool L_7 = QnHiHbnYGCBSFowNhmjvNvYtpXM_rQVCTzxWRBTHwULULWYBtzSTRee_m022C9BF3BD1E87FE37078791C6135EC63CC7E491(L_4, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0045;
		}
	}
	{
		return (bool)1;
	}

IL_0045:
	{
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		G_B2_0 = ((int32_t)-55282401);
		goto IL_0015;
	}

IL_0050:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = V_0;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_002e;
		}
	}
	{
		return (bool)0;
	}
}
// qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa::hDjCQPMEgCKqoDwfDwePyicJXwT(qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd,qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_zSsAVpfNUYBPgVxqQMqbVEPVqJCO)
extern "C" IL2CPP_METHOD_ATTR QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * tLqZqETDWvmFaUyFhoSscPGCHa_hDjCQPMEgCKqoDwfDwePyicJXwT_m8AF1A51683C8A6F250D62F88B0ED4B3967D79407 (tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773 * __this, lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * p0, int32_t p1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tLqZqETDWvmFaUyFhoSscPGCHa_hDjCQPMEgCKqoDwfDwePyicJXwT_m8AF1A51683C8A6F250D62F88B0ED4B3967D79407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B9_1 = 0;
	{
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_0 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m4781C1E7D158FE73AE5792C98211036217CDD9B8(L_0, /*hidden argument*/List_1_get_Count_m4781C1E7D158FE73AE5792C98211036217CDD9B8_RuntimeMethod_var);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0052;
	}

IL_0010:
	{
		G_B2_0 = ((int32_t)499013927);
	}

IL_0015:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)499013925))))
		{
			case 0:
			{
				goto IL_007f;
			}
			case 1:
			{
				goto IL_0052;
			}
			case 2:
			{
				goto IL_0036;
			}
			case 3:
			{
				goto IL_0067;
			}
			case 4:
			{
				goto IL_0010;
			}
		}
	}
	{
		goto IL_007f;
	}

IL_0036:
	{
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_2 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_4 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_2, L_3, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_5 = p0;
		int32_t L_6 = p1;
		NullCheck(L_4);
		bool L_7 = QnHiHbnYGCBSFowNhmjvNvYtpXM_rQVCTzxWRBTHwULULWYBtzSTRee_m022C9BF3BD1E87FE37078791C6135EC63CC7E491(L_4, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0074;
		}
	}
	{
		G_B2_0 = ((int32_t)499013926);
		goto IL_0015;
	}

IL_0052:
	{
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_10 = ((int32_t)499013925);
		G_B9_0 = L_10;
		G_B9_1 = L_10;
		goto IL_0064;
	}

IL_005e:
	{
		int32_t L_11 = ((int32_t)499013927);
		G_B9_0 = L_11;
		G_B9_1 = L_11;
	}

IL_0064:
	{
		G_B2_0 = G_B9_1;
		goto IL_0015;
	}

IL_0067:
	{
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_12 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_13 = V_1;
		NullCheck(L_12);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_14 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_12, L_13, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		return L_14;
	}

IL_0074:
	{
		int32_t L_15 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
		G_B2_0 = ((int32_t)499013924);
		goto IL_0015;
	}

IL_007f:
	{
		return (QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 *)NULL;
	}
}
// System.Void qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa::mRQRPVJljvuBdBeduwoolvGdjPA(System.Int32,System.Guid,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void tLqZqETDWvmFaUyFhoSscPGCHa_mRQRPVJljvuBdBeduwoolvGdjPA_m9E3A1DC07C170F846ACE888531DCEF41468C10DB (tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773 * __this, int32_t p0, Guid_t  p1, int32_t p2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tLqZqETDWvmFaUyFhoSscPGCHa_mRQRPVJljvuBdBeduwoolvGdjPA_m9E3A1DC07C170F846ACE888531DCEF41468C10DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B9_1 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B13_1 = 0;
	int32_t G_B18_0 = 0;
	int32_t G_B18_1 = 0;
	{
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_0 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m4781C1E7D158FE73AE5792C98211036217CDD9B8(L_0, /*hidden argument*/List_1_get_Count_m4781C1E7D158FE73AE5792C98211036217CDD9B8_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
		goto IL_0095;
	}

IL_0013:
	{
		G_B2_0 = ((int32_t)-149136561);
	}

IL_0018:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-149136567))))
		{
			case 0:
			{
				goto IL_0044;
			}
			case 1:
			{
				goto IL_00d3;
			}
			case 2:
			{
				goto IL_0095;
			}
			case 3:
			{
				goto IL_0013;
			}
			case 4:
			{
				goto IL_0057;
			}
			case 5:
			{
				goto IL_00ad;
			}
			case 6:
			{
				goto IL_00bb;
			}
		}
	}
	{
		goto IL_00d3;
	}

IL_0044:
	{
		CHECK_PAUSE_POINT;
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_2 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		List_1_RemoveAt_mFF720D6CA965549F775ABF1FB5A65662C057E67C(L_2, L_3, /*hidden argument*/List_1_RemoveAt_mFF720D6CA965549F775ABF1FB5A65662C057E67C_RuntimeMethod_var);
		G_B2_0 = ((int32_t)-149136564);
		goto IL_0018;
	}

IL_0057:
	{
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_4 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_6 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_4, L_5, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		NullCheck(L_6);
		int32_t L_7 = L_6->get_nvorGNbxnIKRTEQakQRxoDaDbkF_0();
		int32_t L_8 = p0;
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_0044;
		}
	}
	{
		List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * L_9 = __this->get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * L_11 = List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA(L_9, L_10, /*hidden argument*/List_1_get_Item_m8563E9DDB7EA9BE2FE43E8CFF89C3B08C3C889BA_RuntimeMethod_var);
		NullCheck(L_11);
		Guid_t  L_12 = L_11->get_nlEaopEverUmqALsSlwZYfPupTlP_1();
		Guid_t  L_13 = p1;
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		bool L_14 = Guid_op_Equality_m3D98BA18CDAF0C6C329F86720B5CD61A170A3E52(L_12, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_15 = ((int32_t)-149136564);
		G_B9_0 = L_15;
		G_B9_1 = L_15;
		goto IL_0092;
	}

IL_008c:
	{
		int32_t L_16 = ((int32_t)-149136567);
		G_B9_0 = L_16;
		G_B9_1 = L_16;
	}

IL_0092:
	{
		G_B2_0 = G_B9_1;
		goto IL_0018;
	}

IL_0095:
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) >= ((int32_t)0)))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_18 = ((int32_t)-149136568);
		G_B13_0 = L_18;
		G_B13_1 = L_18;
		goto IL_00a7;
	}

IL_00a1:
	{
		int32_t L_19 = ((int32_t)-149136561);
		G_B13_0 = L_19;
		G_B13_1 = L_19;
	}

IL_00a7:
	{
		G_B2_0 = G_B13_1;
		goto IL_0018;
	}

IL_00ad:
	{
		int32_t L_20 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)1));
		G_B2_0 = ((int32_t)-149136565);
		goto IL_0018;
	}

IL_00bb:
	{
		int32_t L_21 = V_0;
		int32_t L_22 = p2;
		if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
		{
			goto IL_00c7;
		}
	}
	{
		int32_t L_23 = ((int32_t)-149136564);
		G_B18_0 = L_23;
		G_B18_1 = L_23;
		goto IL_00cd;
	}

IL_00c7:
	{
		int32_t L_24 = ((int32_t)-149136563);
		G_B18_0 = L_24;
		G_B18_1 = L_24;
	}

IL_00cd:
	{
		G_B2_0 = G_B18_1;
		goto IL_0018;
	}

IL_00d3:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::rQVCTzxWRBTHwULULWYBtzSTRee(qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd,qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_zSsAVpfNUYBPgVxqQMqbVEPVqJCO)
extern "C" IL2CPP_METHOD_ATTR bool QnHiHbnYGCBSFowNhmjvNvYtpXM_rQVCTzxWRBTHwULULWYBtzSTRee_m022C9BF3BD1E87FE37078791C6135EC63CC7E491 (QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * __this, lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * p0, int32_t p1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QnHiHbnYGCBSFowNhmjvNvYtpXM_rQVCTzxWRBTHwULULWYBtzSTRee_m022C9BF3BD1E87FE37078791C6135EC63CC7E491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_0 = p0;
		NullCheck(L_0);
		int32_t L_1 = lNgIgGeozTFHoKvpOBlVjbAtvLyd_get_rewiredId_m31F382FBD51FDA347FDFAF8B6696A4ED3179799A(L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_nvorGNbxnIKRTEQakQRxoDaDbkF_0();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0032;
		}
	}

IL_000e:
	{
		G_B2_0 = ((int32_t)-1110882317);
	}

IL_0013:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-1110882318))))
		{
			case 0:
			{
				goto IL_0067;
			}
			case 1:
			{
				goto IL_0030;
			}
			case 2:
			{
				goto IL_0073;
			}
			case 3:
			{
				goto IL_000e;
			}
		}
	}
	{
		goto IL_0073;
	}

IL_0030:
	{
		return (bool)1;
	}

IL_0032:
	{
		int32_t L_3 = __this->get_UdWYvkBYkFbtQVrPZwZuKINGjC_4();
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_4 = p0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_UdWYvkBYkFbtQVrPZwZuKINGjC_16();
		if ((((int32_t)L_3) == ((int32_t)L_5)))
		{
			goto IL_0042;
		}
	}
	{
		return (bool)0;
	}

IL_0042:
	{
		int32_t L_6 = __this->get_VYSASJjqEIsmJcmnIHADUYEaRjQC_5();
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_7 = p0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_VYSASJjqEIsmJcmnIHADUYEaRjQC_17();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0052;
		}
	}
	{
		return (bool)0;
	}

IL_0052:
	{
		int32_t L_9 = __this->get_aWhbUpAtiftmjIGzRSoPwmEnzb_6();
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_10 = p0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_aWhbUpAtiftmjIGzRSoPwmEnzb_18();
		if ((((int32_t)L_9) == ((int32_t)L_11)))
		{
			goto IL_0069;
		}
	}
	{
		G_B2_0 = ((int32_t)-1110882318);
		goto IL_0013;
	}

IL_0067:
	{
		return (bool)0;
	}

IL_0069:
	{
		int32_t L_12 = p1;
		if (L_12)
		{
			goto IL_0085;
		}
	}
	{
		G_B2_0 = ((int32_t)-1110882320);
		goto IL_0013;
	}

IL_0073:
	{
		Guid_t  L_13 = __this->get_nlEaopEverUmqALsSlwZYfPupTlP_1();
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_14 = p0;
		NullCheck(L_14);
		Guid_t  L_15 = L_14->get_RhEAYdViDLywbAAxYxVbxUVxiMM_10();
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		bool L_16 = Guid_op_Equality_m3D98BA18CDAF0C6C329F86720B5CD61A170A3E52(L_13, L_15, /*hidden argument*/NULL);
		return L_16;
	}

IL_0085:
	{
		int32_t L_17 = p1;
		if ((!(((uint32_t)L_17) == ((uint32_t)1))))
		{
			goto IL_009b;
		}
	}
	{
		Guid_t  L_18 = __this->get_bLJDeZdmqhplurpuKUnpKCpNysd_2();
		lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489 * L_19 = p0;
		NullCheck(L_19);
		Guid_t  L_20 = L_19->get_bLJDeZdmqhplurpuKUnpKCpNysd_12();
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		bool L_21 = Guid_op_Equality_m3D98BA18CDAF0C6C329F86720B5CD61A170A3E52(L_18, L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_009b:
	{
		NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4 * L_22 = (NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4 *)il2cpp_codegen_object_new(NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m8BEA657E260FC05F0C6D2C43A6E9BC08040F59C4(L_22, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22, NULL, QnHiHbnYGCBSFowNhmjvNvYtpXM_rQVCTzxWRBTHwULULWYBtzSTRee_m022C9BF3BD1E87FE37078791C6135EC63CC7E491_RuntimeMethod_var);
	}
}
// System.Void qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::.ctor()
extern "C" IL2CPP_METHOD_ATTR void QnHiHbnYGCBSFowNhmjvNvYtpXM__ctor_mBA422540D78046CAC0BBF8E741DD729867995228 (QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4 * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void rIcvLcWCuQghgaaiofvKjSvndJX::.ctor(System.Type[],System.Type[])
extern "C" IL2CPP_METHOD_ATTR void rIcvLcWCuQghgaaiofvKjSvndJX__ctor_m1BB1C02B6BB5F46ACE01FA603A6CCAA4DDA418C0 (rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * __this, TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___templateTypes0, TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___interfaceTypes1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (rIcvLcWCuQghgaaiofvKjSvndJX__ctor_m1BB1C02B6BB5F46ACE01FA603A6CCAA4DDA418C0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B7_1 = 0;
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
	}

IL_0006:
	{
		G_B2_0 = ((int32_t)375342475);
	}

IL_000b:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)375342478))))
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_00da;
			}
			case 2:
			{
				goto IL_0006;
			}
			case 3:
			{
				goto IL_0055;
			}
			case 4:
			{
				goto IL_007f;
			}
			case 5:
			{
				goto IL_00a6;
			}
			case 6:
			{
				goto IL_006a;
			}
			case 7:
			{
				goto IL_00c3;
			}
		}
	}
	{
		goto IL_00da;
	}

IL_003b:
	{
		int32_t L_0 = V_0;
		int32_t L_1 = __this->get_BtGnDdGQrTXaaWswowVwZiAMcDv_3();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_2 = ((int32_t)375342474);
		G_B7_0 = L_2;
		G_B7_1 = L_2;
		goto IL_0052;
	}

IL_004c:
	{
		int32_t L_3 = ((int32_t)375342479);
		G_B7_0 = L_3;
		G_B7_1 = L_3;
	}

IL_0052:
	{
		G_B2_0 = G_B7_1;
		goto IL_000b;
	}

IL_0055:
	{
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = __this->get_BnoetkxVNQEHfZTFFfghcpdJFzb_1();
		NullCheck(L_4);
		__this->set_BtGnDdGQrTXaaWswowVwZiAMcDv_3((((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length)))));
		G_B2_0 = ((int32_t)375342473);
		goto IL_000b;
	}

IL_006a:
	{
		CHECK_PAUSE_POINT;
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = ___templateTypes0;
		__this->set_BnoetkxVNQEHfZTFFfghcpdJFzb_1(L_5);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_6 = ___interfaceTypes1;
		__this->set_biNwZqyjNTmtSrqBtAcTREmMQcl_2(L_6);
		G_B2_0 = ((int32_t)375342477);
		goto IL_000b;
	}

IL_007f:
	{
		AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * L_7 = __this->get_iwVNvyACihThlfmpkfURWPSdJYs_0();
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = __this->get_biNwZqyjNTmtSrqBtAcTREmMQcl_2();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Type_t * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * L_12 = (mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 *)il2cpp_codegen_object_new(mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820_il2cpp_TypeInfo_var);
		mGZguqdUJPvKvEhsdVIRYohlOty__ctor_m544770ECC483EEE5DD5D78E02667618ACD3D11D6(L_12, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		AList_1_Add_m312C99F6192A18AB1E766E10CDFCC198694273D1(L_7, L_12, /*hidden argument*/AList_1_Add_m312C99F6192A18AB1E766E10CDFCC198694273D1_RuntimeMethod_var);
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
		G_B2_0 = ((int32_t)375342478);
		goto IL_000b;
	}

IL_00a6:
	{
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_14 = ___templateTypes0;
		NullCheck(L_14);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_15 = ___interfaceTypes1;
		NullCheck(L_15);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_14)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_15)->max_length)))))))
		{
			goto IL_006a;
		}
	}
	{
		Exception_t * L_16 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0(L_16, _stringLiteral46CF8DB16C69AEA191AC4FBC8E05DF3952FF60C4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16, NULL, rIcvLcWCuQghgaaiofvKjSvndJX__ctor_m1BB1C02B6BB5F46ACE01FA603A6CCAA4DDA418C0_RuntimeMethod_var);
	}
	// Dead block : IL_00b9: ldc.i4 375342472

IL_00c3:
	{
		AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * L_17 = (AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 *)il2cpp_codegen_object_new(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99_il2cpp_TypeInfo_var);
		AList_1__ctor_m185708896B55423867CBE384A91B2A3F155EE9BB(L_17, /*hidden argument*/AList_1__ctor_m185708896B55423867CBE384A91B2A3F155EE9BB_RuntimeMethod_var);
		__this->set_iwVNvyACihThlfmpkfURWPSdJYs_0(L_17);
		V_0 = 0;
		G_B2_0 = ((int32_t)375342478);
		goto IL_000b;
	}

IL_00da:
	{
		return;
	}
}
// System.Void rIcvLcWCuQghgaaiofvKjSvndJX::iEpVvatmquULZMNgmRjzSSryqGm(Rewired.Controller)
extern "C" IL2CPP_METHOD_ATTR void rIcvLcWCuQghgaaiofvKjSvndJX_iEpVvatmquULZMNgmRjzSSryqGm_m9AF089DFAB7D07752D4C2A9D982C9F08A6C373DF (rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * __this, Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (rIcvLcWCuQghgaaiofvKjSvndJX_iEpVvatmquULZMNgmRjzSSryqGm_m9AF089DFAB7D07752D4C2A9D982C9F08A6C373DF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RuntimeObject* V_2 = NULL;
	Type_t * V_3 = NULL;
	mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * V_4 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B8_1 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B12_1 = 0;
	{
		Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * L_0 = p0;
		if (L_0)
		{
			goto IL_00d3;
		}
	}

IL_0006:
	{
		G_B2_0 = ((int32_t)456001004);
	}

IL_000b:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)456001001))))
		{
			case 0:
			{
				goto IL_006f;
			}
			case 1:
			{
				goto IL_004b;
			}
			case 2:
			{
				goto IL_012a;
			}
			case 3:
			{
				goto IL_0006;
			}
			case 4:
			{
				goto IL_0084;
			}
			case 5:
			{
				goto IL_011f;
			}
			case 6:
			{
				goto IL_0108;
			}
			case 7:
			{
				goto IL_00bd;
			}
			case 8:
			{
				goto IL_00d3;
			}
			case 9:
			{
				goto IL_00f4;
			}
			case 10:
			{
				goto IL_005b;
			}
			case 11:
			{
				goto IL_00e6;
			}
		}
	}
	{
		goto IL_012a;
	}

IL_004b:
	{
		CHECK_PAUSE_POINT;
		Type_t * L_1 = V_3;
		mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * L_2 = rIcvLcWCuQghgaaiofvKjSvndJX_tcvmbTUcTycOBqbsOekuKTXnTvlg_mA19A5F3F3A3BEEF63DF269DC5800679208C2757C(__this, L_1, /*hidden argument*/NULL);
		V_4 = L_2;
		G_B2_0 = ((int32_t)456001006);
		goto IL_000b;
	}

IL_005b:
	{
		RuntimeObject* L_3 = V_2;
		if (!L_3)
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_4 = ((int32_t)456001005);
		G_B8_0 = L_4;
		G_B8_1 = L_4;
		goto IL_006c;
	}

IL_0066:
	{
		int32_t L_5 = ((int32_t)456000992);
		G_B8_0 = L_5;
		G_B8_1 = L_5;
	}

IL_006c:
	{
		G_B2_0 = G_B8_1;
		goto IL_000b;
	}

IL_006f:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_007b;
		}
	}
	{
		int32_t L_8 = ((int32_t)456001003);
		G_B12_0 = L_8;
		G_B12_1 = L_8;
		goto IL_0081;
	}

IL_007b:
	{
		int32_t L_9 = ((int32_t)456001007);
		G_B12_0 = L_9;
		G_B12_1 = L_9;
	}

IL_0081:
	{
		G_B2_0 = G_B12_1;
		goto IL_000b;
	}

IL_0084:
	{
		RuntimeObject* L_10 = V_2;
		NullCheck(L_10);
		Type_t * L_11 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60(L_10, /*hidden argument*/NULL);
		Type_t * L_12 = rIcvLcWCuQghgaaiofvKjSvndJX_EIDXeKXbSIrkasbXSElcJSYFHKB_m638189E95A756923F2ABDC2D30A7B2A2B24E93AA(__this, L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		Type_t * L_13 = V_3;
		if (L_13)
		{
			goto IL_004b;
		}
	}
	{
		RuntimeObject* L_14 = V_2;
		NullCheck(L_14);
		Type_t * L_15 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_15);
		String_t* L_17 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteral04BAED45CE4E417FEF8C1A7402678C4F210373E8, L_16, _stringLiteral834D53EC78A728EB6B4068BB273881DCCC16E645, /*hidden argument*/NULL);
		Logger_LogError_mD867E7161DD5405B9A7B2B7E599B3283C07E6029(L_17, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)456000994);
		goto IL_000b;
	}

IL_00bd:
	{
		mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * L_18 = V_4;
		if (!L_18)
		{
			goto IL_00e6;
		}
	}
	{
		mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * L_19 = V_4;
		RuntimeObject* L_20 = V_2;
		NullCheck(L_19);
		mGZguqdUJPvKvEhsdVIRYohlOty_HfTIrZGXLVOHtJVamKOSesGBJhAd_mBF6D59118267024E5CACA02B7DFF3E12FA51B8E8(L_19, L_20, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)456000994);
		goto IL_000b;
	}

IL_00d3:
	{
		Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * L_21 = p0;
		NullCheck(L_21);
		int32_t L_22 = Controller_get_templateCount_mDA03FB01481C8DC35C340AD2E82DAB1AF2647C49(L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		V_1 = 0;
		G_B2_0 = ((int32_t)456001001);
		goto IL_000b;
	}

IL_00e6:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
		G_B2_0 = ((int32_t)456001001);
		goto IL_000b;
	}

IL_00f4:
	{
		Logger_LogError_mD867E7161DD5405B9A7B2B7E599B3283C07E6029(_stringLiteralBECD2AA75C348B6C1604A5F8AA72A306236BD7C2, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)456000994);
		goto IL_000b;
	}

IL_0108:
	{
		Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * L_24 = p0;
		NullCheck(L_24);
		RuntimeObject* L_25 = Controller_get_Templates_m5E4BFDFBC5E02E2D5C90E3C9B73761F23966848A(L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_1;
		NullCheck(L_25);
		RuntimeObject* L_27 = InterfaceFuncInvoker1< RuntimeObject*, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<Rewired.IControllerTemplate>::get_Item(System.Int32) */, IList_1_tE8EC6D08D356EC0C3DE0171A08C305CC97DB9752_il2cpp_TypeInfo_var, L_25, L_26);
		V_2 = L_27;
		G_B2_0 = ((int32_t)456000995);
		goto IL_000b;
	}

IL_011f:
	{
		return;
	}
	// Dead block : IL_0120: ldc.i4 456000993

IL_012a:
	{
		return;
	}
}
// System.Void rIcvLcWCuQghgaaiofvKjSvndJX::TdkSBPFMSQkEGSjsYWogNISacuj(Rewired.Controller)
extern "C" IL2CPP_METHOD_ATTR void rIcvLcWCuQghgaaiofvKjSvndJX_TdkSBPFMSQkEGSjsYWogNISacuj_m263D429CB5D431B98BFCD789BC3792DF3A5A6952 (rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * __this, Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (rIcvLcWCuQghgaaiofvKjSvndJX_TdkSBPFMSQkEGSjsYWogNISacuj_m263D429CB5D431B98BFCD789BC3792DF3A5A6952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RuntimeObject* V_2 = NULL;
	Type_t * V_3 = NULL;
	mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * V_4 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	int32_t G_B15_0 = 0;
	int32_t G_B15_1 = 0;
	int32_t G_B19_0 = 0;
	int32_t G_B19_1 = 0;
	{
		Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * L_0 = p0;
		if (L_0)
		{
			goto IL_0099;
		}
	}
	{
		return;
	}

IL_0007:
	{
		G_B3_0 = ((int32_t)-1795441544);
	}

IL_000c:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)-1795441542))))
		{
			case 0:
			{
				goto IL_010a;
			}
			case 1:
			{
				goto IL_00e6;
			}
			case 2:
			{
				goto IL_0099;
			}
			case 3:
			{
				goto IL_0044;
			}
			case 4:
			{
				goto IL_005f;
			}
			case 5:
			{
				goto IL_004f;
			}
			case 6:
			{
				goto IL_00c2;
			}
			case 7:
			{
				goto IL_0007;
			}
			case 8:
			{
				goto IL_00ac;
			}
			case 9:
			{
				goto IL_0085;
			}
		}
	}
	{
		goto IL_010a;
	}

IL_0044:
	{
		CHECK_PAUSE_POINT;
		int32_t L_1 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)1));
		G_B3_0 = ((int32_t)-1795441542);
		goto IL_000c;
	}

IL_004f:
	{
		Type_t * L_2 = V_3;
		mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * L_3 = rIcvLcWCuQghgaaiofvKjSvndJX_tcvmbTUcTycOBqbsOekuKTXnTvlg_mA19A5F3F3A3BEEF63DF269DC5800679208C2757C(__this, L_2, /*hidden argument*/NULL);
		V_4 = L_3;
		G_B3_0 = ((int32_t)-1795441550);
		goto IL_000c;
	}

IL_005f:
	{
		RuntimeObject* L_4 = V_2;
		NullCheck(L_4);
		Type_t * L_5 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_5);
		String_t* L_7 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteral04BAED45CE4E417FEF8C1A7402678C4F210373E8, L_6, _stringLiteral834D53EC78A728EB6B4068BB273881DCCC16E645, /*hidden argument*/NULL);
		Logger_LogError_mD867E7161DD5405B9A7B2B7E599B3283C07E6029(L_7, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)-1795441543);
		goto IL_000c;
	}

IL_0085:
	{
		Logger_LogError_mD867E7161DD5405B9A7B2B7E599B3283C07E6029(_stringLiteralBECD2AA75C348B6C1604A5F8AA72A306236BD7C2, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)-1795441543);
		goto IL_000c;
	}

IL_0099:
	{
		Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * L_8 = p0;
		NullCheck(L_8);
		int32_t L_9 = Controller_get_templateCount_mDA03FB01481C8DC35C340AD2E82DAB1AF2647C49(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		V_1 = 0;
		G_B3_0 = ((int32_t)-1795441542);
		goto IL_000c;
	}

IL_00ac:
	{
		mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * L_10 = V_4;
		if (!L_10)
		{
			goto IL_0044;
		}
	}
	{
		mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * L_11 = V_4;
		RuntimeObject* L_12 = V_2;
		NullCheck(L_11);
		mGZguqdUJPvKvEhsdVIRYohlOty_yhpcGPaRrkCsNfCHablLblmduzKL_m0C4D04CA4019577CD998EA769D6CD52F64459055(L_11, L_12, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)-1795441543);
		goto IL_000c;
	}

IL_00c2:
	{
		CHECK_PAUSE_POINT;
		Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * L_13 = p0;
		NullCheck(L_13);
		RuntimeObject* L_14 = Controller_get_Templates_m5E4BFDFBC5E02E2D5C90E3C9B73761F23966848A(L_13, /*hidden argument*/NULL);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		RuntimeObject* L_16 = InterfaceFuncInvoker1< RuntimeObject*, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<Rewired.IControllerTemplate>::get_Item(System.Int32) */, IList_1_tE8EC6D08D356EC0C3DE0171A08C305CC97DB9752_il2cpp_TypeInfo_var, L_14, L_15);
		V_2 = L_16;
		RuntimeObject* L_17 = V_2;
		if (L_17)
		{
			goto IL_00da;
		}
	}
	{
		int32_t L_18 = ((int32_t)-1795441549);
		G_B15_0 = L_18;
		G_B15_1 = L_18;
		goto IL_00e0;
	}

IL_00da:
	{
		int32_t L_19 = ((int32_t)-1795441541);
		G_B15_0 = L_19;
		G_B15_1 = L_19;
	}

IL_00e0:
	{
		G_B3_0 = G_B15_1;
		goto IL_000c;
	}

IL_00e6:
	{
		RuntimeObject* L_20 = V_2;
		NullCheck(L_20);
		Type_t * L_21 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60(L_20, /*hidden argument*/NULL);
		Type_t * L_22 = rIcvLcWCuQghgaaiofvKjSvndJX_EIDXeKXbSIrkasbXSElcJSYFHKB_m638189E95A756923F2ABDC2D30A7B2A2B24E93AA(__this, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		Type_t * L_23 = V_3;
		if (L_23)
		{
			goto IL_00fe;
		}
	}
	{
		int32_t L_24 = ((int32_t)-1795441538);
		G_B19_0 = L_24;
		G_B19_1 = L_24;
		goto IL_0104;
	}

IL_00fe:
	{
		int32_t L_25 = ((int32_t)-1795441537);
		G_B19_0 = L_25;
		G_B19_1 = L_25;
	}

IL_0104:
	{
		G_B3_0 = G_B19_1;
		goto IL_000c;
	}

IL_010a:
	{
		int32_t L_26 = V_1;
		int32_t L_27 = V_0;
		if ((((int32_t)L_26) < ((int32_t)L_27)))
		{
			goto IL_00c2;
		}
	}
	{
		return;
	}
}
// rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty rIcvLcWCuQghgaaiofvKjSvndJX::tcvmbTUcTycOBqbsOekuKTXnTvlg(System.Type)
extern "C" IL2CPP_METHOD_ATTR mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * rIcvLcWCuQghgaaiofvKjSvndJX_tcvmbTUcTycOBqbsOekuKTXnTvlg_mA19A5F3F3A3BEEF63DF269DC5800679208C2757C (rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * __this, Type_t * p0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		V_0 = 0;
		goto IL_0055;
	}

IL_0004:
	{
		G_B2_0 = ((int32_t)-587794160);
	}

IL_0009:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-587794159))))
		{
			case 0:
			{
				goto IL_0055;
			}
			case 1:
			{
				goto IL_0022;
			}
			case 2:
			{
				goto IL_0004;
			}
		}
	}
	{
		goto IL_0055;
	}

IL_0022:
	{
		CHECK_PAUSE_POINT;
		Type_t * L_0 = p0;
		AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * L_1 = __this->get_iwVNvyACihThlfmpkfURWPSdJYs_0();
		NullCheck(L_1);
		mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4* L_2 = L_1->get__items_3();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		Type_t * L_6 = L_5->get_umQghCmsfCFpiiISpgIfIkmOcxB_3();
		bool L_7 = il2cpp_codegen_object_reference_equals(L_0, L_6);
		if (!L_7)
		{
			goto IL_004a;
		}
	}
	{
		AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * L_8 = __this->get_iwVNvyACihThlfmpkfURWPSdJYs_0();
		NullCheck(L_8);
		mGZguqdUJPvKvEhsdVIRYohlOtyU5BU5D_tC538330825FF10D8650853DC6271B92F9F6C35E4* L_9 = L_8->get__items_3();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		return L_12;
	}

IL_004a:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
		G_B2_0 = ((int32_t)-587794159);
		goto IL_0009;
	}

IL_0055:
	{
		int32_t L_14 = V_0;
		AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * L_15 = __this->get_iwVNvyACihThlfmpkfURWPSdJYs_0();
		NullCheck(L_15);
		int32_t L_16 = L_15->get__count_5();
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0022;
		}
	}
	{
		return (mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 *)NULL;
	}
}
// System.Type rIcvLcWCuQghgaaiofvKjSvndJX::EIDXeKXbSIrkasbXSElcJSYFHKB(System.Type)
extern "C" IL2CPP_METHOD_ATTR Type_t * rIcvLcWCuQghgaaiofvKjSvndJX_EIDXeKXbSIrkasbXSElcJSYFHKB_m638189E95A756923F2ABDC2D30A7B2A2B24E93AA (rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * __this, Type_t * p0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		V_0 = 0;
	}

IL_0002:
	{
		G_B2_0 = ((int32_t)-208086383);
	}

IL_0007:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-208086381))))
		{
			case 0:
			{
				goto IL_0002;
			}
			case 1:
			{
				goto IL_004f;
			}
			case 2:
			{
				goto IL_0024;
			}
			case 3:
			{
				goto IL_002b;
			}
		}
	}
	{
		goto IL_004f;
	}

IL_0024:
	{
		G_B2_0 = ((int32_t)-208086382);
		goto IL_0007;
	}

IL_002b:
	{
		CHECK_PAUSE_POINT;
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_0 = __this->get_BnoetkxVNQEHfZTFFfghcpdJFzb_1();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Type_t * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		Type_t * L_4 = p0;
		bool L_5 = il2cpp_codegen_object_reference_equals(L_3, L_4);
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_6 = __this->get_biNwZqyjNTmtSrqBtAcTREmMQcl_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Type_t * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		return L_9;
	}

IL_0044:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
		G_B2_0 = ((int32_t)-208086382);
		goto IL_0007;
	}

IL_004f:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = __this->get_BtGnDdGQrTXaaWswowVwZiAMcDv_3();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_002b;
		}
	}
	{
		return (Type_t *)NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty::.ctor(System.Type)
extern "C" IL2CPP_METHOD_ATTR void mGZguqdUJPvKvEhsdVIRYohlOty__ctor_m544770ECC483EEE5DD5D78E02667618ACD3D11D6 (mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (mGZguqdUJPvKvEhsdVIRYohlOty__ctor_m544770ECC483EEE5DD5D78E02667618ACD3D11D6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___type0;
		__this->set_umQghCmsfCFpiiISpgIfIkmOcxB_3(L_0);
		AList_1_t974A66EA135B0300CE05935362C6C7C736059320 * L_1 = (AList_1_t974A66EA135B0300CE05935362C6C7C736059320 *)il2cpp_codegen_object_new(AList_1_t974A66EA135B0300CE05935362C6C7C736059320_il2cpp_TypeInfo_var);
		AList_1__ctor_mAC4A718F851BAFFFD2BFD4F2294903DF5A752CBA(L_1, /*hidden argument*/AList_1__ctor_mAC4A718F851BAFFFD2BFD4F2294903DF5A752CBA_RuntimeMethod_var);
		__this->set_zGAzEsGrTTTqWZqBgkORnHasWbZ_0(L_1);
		return;
	}
}
// System.Void rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty::HfTIrZGXLVOHtJVamKOSesGBJhAd(Rewired.IControllerTemplate)
extern "C" IL2CPP_METHOD_ATTR void mGZguqdUJPvKvEhsdVIRYohlOty_HfTIrZGXLVOHtJVamKOSesGBJhAd_mBF6D59118267024E5CACA02B7DFF3E12FA51B8E8 (mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (mGZguqdUJPvKvEhsdVIRYohlOty_HfTIrZGXLVOHtJVamKOSesGBJhAd_mBF6D59118267024E5CACA02B7DFF3E12FA51B8E8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B8_1 = 0;
	{
		RuntimeObject* L_0 = p0;
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		return;
	}

IL_0004:
	{
		G_B3_0 = ((int32_t)1975976735);
	}

IL_0009:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)1975976734))))
		{
			case 0:
			{
				goto IL_004c;
			}
			case 1:
			{
				goto IL_0026;
			}
			case 2:
			{
				goto IL_0004;
			}
			case 3:
			{
				goto IL_0060;
			}
		}
	}
	{
		goto IL_0060;
	}

IL_0026:
	{
		AList_1_t974A66EA135B0300CE05935362C6C7C736059320 * L_1 = __this->get_zGAzEsGrTTTqWZqBgkORnHasWbZ_0();
		RuntimeObject* L_2 = p0;
		NullCheck(L_1);
		AList_1_Add_mFBC6CE8CADA220DFA6C8C2E04DE156D55F6FA850(L_1, L_2, /*hidden argument*/AList_1_Add_mFBC6CE8CADA220DFA6C8C2E04DE156D55F6FA850_RuntimeMethod_var);
		RuntimeObject* L_3 = __this->get_HEgvcyfmgQRVtohyvBGRboKAUBC_1();
		if (L_3)
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = ((int32_t)1975976733);
		G_B8_0 = L_4;
		G_B8_1 = L_4;
		goto IL_0049;
	}

IL_0043:
	{
		int32_t L_5 = ((int32_t)1975976734);
		G_B8_0 = L_5;
		G_B8_1 = L_5;
	}

IL_0049:
	{
		G_B3_0 = G_B8_1;
		goto IL_0009;
	}

IL_004c:
	{
		RuntimeObject* L_6 = __this->get_HEgvcyfmgQRVtohyvBGRboKAUBC_1();
		RuntimeObject* L_7 = p0;
		NullCheck(L_6);
		InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(2 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA_il2cpp_TypeInfo_var, L_6, L_7);
		G_B3_0 = ((int32_t)1975976733);
		goto IL_0009;
	}

IL_0060:
	{
		return;
	}
}
// System.Void rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty::yhpcGPaRrkCsNfCHablLblmduzKL(Rewired.IControllerTemplate)
extern "C" IL2CPP_METHOD_ATTR void mGZguqdUJPvKvEhsdVIRYohlOty_yhpcGPaRrkCsNfCHablLblmduzKL_m0C4D04CA4019577CD998EA769D6CD52F64459055 (mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (mGZguqdUJPvKvEhsdVIRYohlOty_yhpcGPaRrkCsNfCHablLblmduzKL_m0C4D04CA4019577CD998EA769D6CD52F64459055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		RuntimeObject* L_0 = p0;
		if (L_0)
		{
			goto IL_002d;
		}
	}

IL_0003:
	{
		G_B2_0 = ((int32_t)1725068853);
	}

IL_0008:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)1725068855))))
		{
			case 0:
			{
				goto IL_0003;
			}
			case 1:
			{
				goto IL_0055;
			}
			case 2:
			{
				goto IL_0025;
			}
			case 3:
			{
				goto IL_002d;
			}
		}
	}
	{
		goto IL_0055;
	}

IL_0025:
	{
		return;
	}
	// Dead block : IL_0026: ldc.i4 1725068852

IL_002d:
	{
		AList_1_t974A66EA135B0300CE05935362C6C7C736059320 * L_1 = __this->get_zGAzEsGrTTTqWZqBgkORnHasWbZ_0();
		RuntimeObject* L_2 = p0;
		NullCheck(L_1);
		AList_1_Remove_m4E877B89C4BEEB27D88C72D27D173CE7FDB3B8BE(L_1, L_2, /*hidden argument*/AList_1_Remove_m4E877B89C4BEEB27D88C72D27D173CE7FDB3B8BE_RuntimeMethod_var);
		RuntimeObject* L_3 = __this->get_HEgvcyfmgQRVtohyvBGRboKAUBC_1();
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		RuntimeObject* L_4 = __this->get_HEgvcyfmgQRVtohyvBGRboKAUBC_1();
		RuntimeObject* L_5 = p0;
		NullCheck(L_4);
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(9 /* System.Void System.Collections.IList::Remove(System.Object) */, IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA_il2cpp_TypeInfo_var, L_4, L_5);
		G_B2_0 = ((int32_t)1725068854);
		goto IL_0008;
	}

IL_0055:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void sSmyCZxlYXGixusuhxfHceqeCTr::nbgZHGrFJfzAlaIQqyHguklefEZ(System.Object)
extern "C" IL2CPP_METHOD_ATTR void sSmyCZxlYXGixusuhxfHceqeCTr_nbgZHGrFJfzAlaIQqyHguklefEZ_m8D80E0680D51BFA6E0C65F5FC7E34380DEFF7820 (RuntimeObject * p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (sSmyCZxlYXGixusuhxfHceqeCTr_nbgZHGrFJfzAlaIQqyHguklefEZ_m8D80E0680D51BFA6E0C65F5FC7E34380DEFF7820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		RuntimeObject * L_0 = p0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void sSmyCZxlYXGixusuhxfHceqeCTr::VikOOOAEBIdZweuHRjgOuRqhkErr(System.Object,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void sSmyCZxlYXGixusuhxfHceqeCTr_VikOOOAEBIdZweuHRjgOuRqhkErr_mEBC31378984490FF5F558512123D623710766A29 (RuntimeObject * p0, bool p1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (sSmyCZxlYXGixusuhxfHceqeCTr_VikOOOAEBIdZweuHRjgOuRqhkErr_mEBC31378984490FF5F558512123D623710766A29_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		bool L_0 = p1;
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		RuntimeObject * L_1 = p0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_1, /*hidden argument*/NULL);
	}

IL_0009:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Rewired.ModifierKey szJmXpILCMpJYoBHnspxyLIJAob::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t szJmXpILCMpJYoBHnspxyLIJAob_get_Item_mCF770935971354ACB80847CCDF1F5BD28343844E (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, int32_t ___index0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		int32_t L_1 = __this->get_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0();
		return L_1;
	}

IL_000b:
	{
		int32_t L_2 = ___index0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = __this->get_LtcbhcBgmsmnragehPolhLChilja_1();
		return L_3;
	}

IL_0016:
	{
		int32_t L_4 = ___index0;
		if ((((int32_t)L_4) < ((int32_t)2)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_5 = __this->get_nwYAkwIgQIppweizjiwEOkDJuCOQ_2();
		return L_5;
	}

IL_0021:
	{
		int32_t L_6 = __this->get_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0();
		return L_6;
	}
}
extern "C"  int32_t szJmXpILCMpJYoBHnspxyLIJAob_get_Item_mCF770935971354ACB80847CCDF1F5BD28343844E_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * _thisAdjusted = reinterpret_cast<szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *>(__this + 1);
	return szJmXpILCMpJYoBHnspxyLIJAob_get_Item_mCF770935971354ACB80847CCDF1F5BD28343844E(_thisAdjusted, ___index0, method);
}
// System.Void szJmXpILCMpJYoBHnspxyLIJAob::set_Item(System.Int32,Rewired.ModifierKey)
extern "C" IL2CPP_METHOD_ATTR void szJmXpILCMpJYoBHnspxyLIJAob_set_Item_m9BC58E958D3AF599C3136D74AFC8CBEEF690C743 (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, int32_t ___index0, int32_t ___value1, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_004a;
		}
	}

IL_0004:
	{
		G_B2_0 = ((int32_t)-1233261043);
	}

IL_0009:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-1233261047))))
		{
			case 0:
			{
				goto IL_0004;
			}
			case 1:
			{
				goto IL_004a;
			}
			case 2:
			{
				goto IL_0038;
			}
			case 3:
			{
				goto IL_005c;
			}
			case 4:
			{
				goto IL_002a;
			}
		}
	}
	{
		goto IL_005c;
	}

IL_002a:
	{
		int32_t L_1 = ___value1;
		__this->set_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0(L_1);
		G_B2_0 = ((int32_t)-1233261048);
		goto IL_0009;
	}

IL_0038:
	{
		CHECK_PAUSE_POINT;
		int32_t L_2 = ___index0;
		if ((((int32_t)L_2) < ((int32_t)2)))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = ___value1;
		__this->set_nwYAkwIgQIppweizjiwEOkDJuCOQ_2(L_3);
		G_B2_0 = ((int32_t)-1233261046);
		goto IL_0009;
	}

IL_004a:
	{
		int32_t L_4 = ___index0;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_5 = ___value1;
		__this->set_LtcbhcBgmsmnragehPolhLChilja_1(L_5);
		G_B2_0 = ((int32_t)-1233261045);
		goto IL_0009;
	}

IL_005c:
	{
		return;
	}
}
extern "C"  void szJmXpILCMpJYoBHnspxyLIJAob_set_Item_m9BC58E958D3AF599C3136D74AFC8CBEEF690C743_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, int32_t ___value1, const RuntimeMethod* method)
{
	szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * _thisAdjusted = reinterpret_cast<szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *>(__this + 1);
	szJmXpILCMpJYoBHnspxyLIJAob_set_Item_m9BC58E958D3AF599C3136D74AFC8CBEEF690C743(_thisAdjusted, ___index0, ___value1, method);
}
// System.Void szJmXpILCMpJYoBHnspxyLIJAob::.ctor(Rewired.ModifierKey,Rewired.ModifierKey,Rewired.ModifierKey)
extern "C" IL2CPP_METHOD_ATTR void szJmXpILCMpJYoBHnspxyLIJAob__ctor_m2AA846814F89820BCDE163061232DC8B0912BBBB (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, int32_t ___modifierKey10, int32_t ___modifierKey21, int32_t ___modifierKey32, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int32_t L_0 = ___modifierKey10;
		__this->set_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0(L_0);
		int32_t L_1 = ___modifierKey21;
		__this->set_LtcbhcBgmsmnragehPolhLChilja_1(L_1);
		int32_t L_2 = ___modifierKey32;
		__this->set_nwYAkwIgQIppweizjiwEOkDJuCOQ_2(L_2);
		return;
	}
}
extern "C"  void szJmXpILCMpJYoBHnspxyLIJAob__ctor_m2AA846814F89820BCDE163061232DC8B0912BBBB_AdjustorThunk (RuntimeObject * __this, int32_t ___modifierKey10, int32_t ___modifierKey21, int32_t ___modifierKey32, const RuntimeMethod* method)
{
	szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * _thisAdjusted = reinterpret_cast<szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *>(__this + 1);
	szJmXpILCMpJYoBHnspxyLIJAob__ctor_m2AA846814F89820BCDE163061232DC8B0912BBBB(_thisAdjusted, ___modifierKey10, ___modifierKey21, ___modifierKey32, method);
}
// System.Void szJmXpILCMpJYoBHnspxyLIJAob::KsUCrxkpJRkolcYgWumGYuxqCeFB()
extern "C" IL2CPP_METHOD_ATTR void szJmXpILCMpJYoBHnspxyLIJAob_KsUCrxkpJRkolcYgWumGYuxqCeFB_m753910193687ED68784706A5E93C17BA5ACAB1E0 (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = __this->get_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0();
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		__this->set_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0(0);
	}

IL_000f:
	{
		G_B3_0 = ((int32_t)-1465104705);
	}

IL_0014:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)-1465104706))))
		{
			case 0:
			{
				goto IL_005d;
			}
			case 1:
			{
				goto IL_0031;
			}
			case 2:
			{
				goto IL_0047;
			}
			case 3:
			{
				goto IL_000f;
			}
		}
	}
	{
		goto IL_005d;
	}

IL_0031:
	{
		int32_t L_1 = __this->get_LtcbhcBgmsmnragehPolhLChilja_1();
		if (!L_1)
		{
			goto IL_0047;
		}
	}
	{
		__this->set_LtcbhcBgmsmnragehPolhLChilja_1(0);
		G_B3_0 = ((int32_t)-1465104708);
		goto IL_0014;
	}

IL_0047:
	{
		int32_t L_2 = __this->get_nwYAkwIgQIppweizjiwEOkDJuCOQ_2();
		if (!L_2)
		{
			goto IL_005d;
		}
	}
	{
		__this->set_nwYAkwIgQIppweizjiwEOkDJuCOQ_2(0);
		G_B3_0 = ((int32_t)-1465104706);
		goto IL_0014;
	}

IL_005d:
	{
		return;
	}
}
extern "C"  void szJmXpILCMpJYoBHnspxyLIJAob_KsUCrxkpJRkolcYgWumGYuxqCeFB_m753910193687ED68784706A5E93C17BA5ACAB1E0_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * _thisAdjusted = reinterpret_cast<szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *>(__this + 1);
	szJmXpILCMpJYoBHnspxyLIJAob_KsUCrxkpJRkolcYgWumGYuxqCeFB_m753910193687ED68784706A5E93C17BA5ACAB1E0(_thisAdjusted, method);
}
// szJmXpILCMpJYoBHnspxyLIJAob szJmXpILCMpJYoBHnspxyLIJAob::ULAUEOVWRKLzwDBEFoayuMjWMcN(Rewired.ModifierKeyFlags)
extern "C" IL2CPP_METHOD_ATTR szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  szJmXpILCMpJYoBHnspxyLIJAob_ULAUEOVWRKLzwDBEFoayuMjWMcN_m927242B6D113201B3E38C18C9FB0C514122BBF20 (int32_t p0, const RuntimeMethod* method)
{
	szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	int32_t G_B15_0 = 0;
	int32_t G_B15_1 = 0;
	{
		il2cpp_codegen_initobj((&V_0), sizeof(szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B ));
		V_1 = 0;
		int32_t L_0 = p0;
		bool L_1 = Keyboard_ModifierKeyFlagsContain_m0C0AE5DE019717B1CA143A6B171E403B53E3FF60(L_0, 1, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00b2;
		}
	}

IL_0016:
	{
		G_B2_0 = ((int32_t)981142248);
	}

IL_001b:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)981142252))))
		{
			case 0:
			{
				goto IL_0016;
			}
			case 1:
			{
				goto IL_005b;
			}
			case 2:
			{
				goto IL_00cf;
			}
			case 3:
			{
				goto IL_00b2;
			}
			case 4:
			{
				goto IL_0078;
			}
			case 5:
			{
				goto IL_0047;
			}
			case 6:
			{
				goto IL_008c;
			}
		}
	}
	{
		goto IL_00cf;
	}

IL_0047:
	{
		int32_t L_2 = V_1;
		int32_t L_3 = L_2;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
		szJmXpILCMpJYoBHnspxyLIJAob_set_Item_m9BC58E958D3AF599C3136D74AFC8CBEEF690C743((szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *)(&V_0), L_3, 4, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)981142253);
		goto IL_001b;
	}

IL_005b:
	{
		int32_t L_4 = p0;
		bool L_5 = Keyboard_ModifierKeyFlagsContain_m0C0AE5DE019717B1CA143A6B171E403B53E3FF60(L_4, 2, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_6 = V_1;
		int32_t L_7 = L_6;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
		szJmXpILCMpJYoBHnspxyLIJAob_set_Item_m9BC58E958D3AF599C3136D74AFC8CBEEF690C743((szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *)(&V_0), L_7, 2, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)981142250);
		goto IL_001b;
	}

IL_0078:
	{
		int32_t L_8 = V_1;
		int32_t L_9 = L_8;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
		szJmXpILCMpJYoBHnspxyLIJAob_set_Item_m9BC58E958D3AF599C3136D74AFC8CBEEF690C743((szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *)(&V_0), L_9, 1, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)981142255);
		goto IL_001b;
	}

IL_008c:
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) < ((int32_t)3)))
		{
			goto IL_0092;
		}
	}
	{
		szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  L_11 = V_0;
		return L_11;
	}

IL_0092:
	{
		int32_t L_12 = p0;
		bool L_13 = Keyboard_ModifierKeyFlagsContain_m0C0AE5DE019717B1CA143A6B171E403B53E3FF60(L_12, 3, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00cf;
		}
	}
	{
		int32_t L_14 = V_1;
		int32_t L_15 = L_14;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
		szJmXpILCMpJYoBHnspxyLIJAob_set_Item_m9BC58E958D3AF599C3136D74AFC8CBEEF690C743((szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *)(&V_0), L_15, 3, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)981142254);
		goto IL_001b;
	}

IL_00b2:
	{
		int32_t L_16 = p0;
		bool L_17 = Keyboard_ModifierKeyFlagsContain_m0C0AE5DE019717B1CA143A6B171E403B53E3FF60(L_16, 4, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00c3;
		}
	}
	{
		int32_t L_18 = ((int32_t)981142249);
		G_B15_0 = L_18;
		G_B15_1 = L_18;
		goto IL_00c9;
	}

IL_00c3:
	{
		int32_t L_19 = ((int32_t)981142253);
		G_B15_0 = L_19;
		G_B15_1 = L_19;
	}

IL_00c9:
	{
		G_B2_0 = G_B15_1;
		goto IL_001b;
	}

IL_00cf:
	{
		szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  L_20 = V_0;
		return L_20;
	}
}
// System.Boolean szJmXpILCMpJYoBHnspxyLIJAob::Equals(szJmXpILCMpJYoBHnspxyLIJAob)
extern "C" IL2CPP_METHOD_ATTR bool szJmXpILCMpJYoBHnspxyLIJAob_Equals_m349FAED2699343DEC2F2EA44BF7BAA2DA0DF8CA8 (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  ___other0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int32_t L_0 = __this->get_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0();
		int32_t L_1 = (&___other0)->get_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = __this->get_LtcbhcBgmsmnragehPolhLChilja_1();
		int32_t L_3 = (&___other0)->get_LtcbhcBgmsmnragehPolhLChilja_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_4 = __this->get_nwYAkwIgQIppweizjiwEOkDJuCOQ_2();
		int32_t L_5 = (&___other0)->get_nwYAkwIgQIppweizjiwEOkDJuCOQ_2();
		return (bool)((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
	}

IL_002e:
	{
		return (bool)0;
	}
}
extern "C"  bool szJmXpILCMpJYoBHnspxyLIJAob_Equals_m349FAED2699343DEC2F2EA44BF7BAA2DA0DF8CA8_AdjustorThunk (RuntimeObject * __this, szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  ___other0, const RuntimeMethod* method)
{
	szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * _thisAdjusted = reinterpret_cast<szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *>(__this + 1);
	return szJmXpILCMpJYoBHnspxyLIJAob_Equals_m349FAED2699343DEC2F2EA44BF7BAA2DA0DF8CA8(_thisAdjusted, ___other0, method);
}
// System.Boolean szJmXpILCMpJYoBHnspxyLIJAob::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool szJmXpILCMpJYoBHnspxyLIJAob_Equals_m696F1EA3F652019ADF7F2E0DCAE7BE71F63C469D (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (szJmXpILCMpJYoBHnspxyLIJAob_Equals_m696F1EA3F652019ADF7F2E0DCAE7BE71F63C469D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		RuntimeObject * L_0 = ___obj0;
		if (!L_0)
		{
			goto IL_0030;
		}
	}

IL_0003:
	{
		G_B2_0 = ((int32_t)-151895056);
	}

IL_0008:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-151895055))))
		{
			case 0:
			{
				goto IL_0003;
			}
			case 1:
			{
				goto IL_0021;
			}
			case 2:
			{
				goto IL_0030;
			}
		}
	}
	{
		goto IL_0030;
	}

IL_0021:
	{
		RuntimeObject * L_1 = ___obj0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_1, szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B_il2cpp_TypeInfo_var)))
		{
			goto IL_0032;
		}
	}
	{
		G_B2_0 = ((int32_t)-151895053);
		goto IL_0008;
	}

IL_0030:
	{
		return (bool)0;
	}

IL_0032:
	{
		RuntimeObject * L_2 = ___obj0;
		bool L_3 = szJmXpILCMpJYoBHnspxyLIJAob_Equals_m349FAED2699343DEC2F2EA44BF7BAA2DA0DF8CA8((szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *)__this, ((*(szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *)((szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *)UnBox(L_2, szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_3;
	}
}
extern "C"  bool szJmXpILCMpJYoBHnspxyLIJAob_Equals_m696F1EA3F652019ADF7F2E0DCAE7BE71F63C469D_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * _thisAdjusted = reinterpret_cast<szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *>(__this + 1);
	return szJmXpILCMpJYoBHnspxyLIJAob_Equals_m696F1EA3F652019ADF7F2E0DCAE7BE71F63C469D(_thisAdjusted, ___obj0, method);
}
// System.Int32 szJmXpILCMpJYoBHnspxyLIJAob::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t szJmXpILCMpJYoBHnspxyLIJAob_GetHashCode_m1CE2FB9D12F38DE1DC231A8C04D8AA6962CEEE75 (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (szJmXpILCMpJYoBHnspxyLIJAob_GetHashCode_m1CE2FB9D12F38DE1DC231A8C04D8AA6962CEEE75_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CHECK_PAUSE_POINT;
	{
		V_0 = ((int32_t)17);
		int32_t L_0 = V_0;
		int32_t L_1 = __this->get_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0();
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = Int32_GetHashCode_m245C424ECE351E5FE3277A88EEB02132DAB8C25A((int32_t*)UnBox(L_3, ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0_il2cpp_TypeInfo_var), /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)((int32_t)29))), (int32_t)L_4));
		int32_t L_5 = V_0;
		int32_t L_6 = __this->get_LtcbhcBgmsmnragehPolhLChilja_1();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = Int32_GetHashCode_m245C424ECE351E5FE3277A88EEB02132DAB8C25A((int32_t*)UnBox(L_8, ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0_il2cpp_TypeInfo_var), /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)((int32_t)29))), (int32_t)L_9));
		int32_t L_10 = V_0;
		int32_t L_11 = __this->get_nwYAkwIgQIppweizjiwEOkDJuCOQ_2();
		int32_t L_12 = L_11;
		RuntimeObject * L_13 = Box(ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0_il2cpp_TypeInfo_var, &L_12);
		int32_t L_14 = Int32_GetHashCode_m245C424ECE351E5FE3277A88EEB02132DAB8C25A((int32_t*)UnBox(L_13, ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0_il2cpp_TypeInfo_var), /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_10, (int32_t)((int32_t)29))), (int32_t)L_14));
		int32_t L_15 = V_0;
		return L_15;
	}
}
extern "C"  int32_t szJmXpILCMpJYoBHnspxyLIJAob_GetHashCode_m1CE2FB9D12F38DE1DC231A8C04D8AA6962CEEE75_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B * _thisAdjusted = reinterpret_cast<szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B *>(__this + 1);
	return szJmXpILCMpJYoBHnspxyLIJAob_GetHashCode_m1CE2FB9D12F38DE1DC231A8C04D8AA6962CEEE75(_thisAdjusted, method);
}
// System.Boolean szJmXpILCMpJYoBHnspxyLIJAob::op_Equality(szJmXpILCMpJYoBHnspxyLIJAob,szJmXpILCMpJYoBHnspxyLIJAob)
extern "C" IL2CPP_METHOD_ATTR bool szJmXpILCMpJYoBHnspxyLIJAob_op_Equality_mC0329F9C051772871B889D895FA90432CC40D42D (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  ___a0, szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  ___b1, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int32_t L_0 = (&___a0)->get_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0();
		int32_t L_1 = (&___b1)->get_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_2 = (&___a0)->get_LtcbhcBgmsmnragehPolhLChilja_1();
		int32_t L_3 = (&___b1)->get_LtcbhcBgmsmnragehPolhLChilja_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_4 = (&___a0)->get_nwYAkwIgQIppweizjiwEOkDJuCOQ_2();
		int32_t L_5 = (&___b1)->get_nwYAkwIgQIppweizjiwEOkDJuCOQ_2();
		return (bool)((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
	}

IL_0031:
	{
		return (bool)0;
	}
}
// System.Boolean szJmXpILCMpJYoBHnspxyLIJAob::op_Inequality(szJmXpILCMpJYoBHnspxyLIJAob,szJmXpILCMpJYoBHnspxyLIJAob)
extern "C" IL2CPP_METHOD_ATTR bool szJmXpILCMpJYoBHnspxyLIJAob_op_Inequality_mF5FB9308E6B52B2930559801F1444E9500F38BD6 (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  ___a0, szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  ___b1, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  L_0 = ___a0;
		szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B  L_1 = ___b1;
		bool L_2 = szJmXpILCMpJYoBHnspxyLIJAob_op_Equality_mC0329F9C051772871B889D895FA90432CC40D42D(L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean tirTbIgerWCchDwImTBhEXMbbyDC::aPDcQrAxDVfpHqyPrEgiDuiWmsWb(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool tirTbIgerWCchDwImTBhEXMbbyDC_aPDcQrAxDVfpHqyPrEgiDuiWmsWb_m5DA589A3287320E721A735A3978A4046BFA0EB2F (int32_t p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tirTbIgerWCchDwImTBhEXMbbyDC_aPDcQrAxDVfpHqyPrEgiDuiWmsWb_m5DA589A3287320E721A735A3978A4046BFA0EB2F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = p0;
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_1 = p0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var);
		double L_2 = sqrt((((double)((double)L_1))));
		V_0 = (((int32_t)((int32_t)L_2)));
		V_1 = 3;
	}

IL_0010:
	{
		G_B3_0 = ((int32_t)766077879);
	}

IL_0015:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)766077878))))
		{
			case 0:
			{
				goto IL_0039;
			}
			case 1:
			{
				goto IL_0032;
			}
			case 2:
			{
				goto IL_004b;
			}
			case 3:
			{
				goto IL_0010;
			}
		}
	}
	{
		goto IL_004b;
	}

IL_0032:
	{
		G_B3_0 = ((int32_t)766077876);
		goto IL_0015;
	}

IL_0039:
	{
		CHECK_PAUSE_POINT;
		int32_t L_3 = p0;
		int32_t L_4 = V_1;
		if (((int32_t)((int32_t)L_3%(int32_t)L_4)))
		{
			goto IL_0040;
		}
	}
	{
		return (bool)0;
	}

IL_0040:
	{
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)2));
		G_B3_0 = ((int32_t)766077876);
		goto IL_0015;
	}

IL_004b:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) <= ((int32_t)L_7)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)1;
	}

IL_0051:
	{
		int32_t L_8 = p0;
		return (bool)((((int32_t)L_8) == ((int32_t)2))? 1 : 0);
	}
}
// System.Int32 tirTbIgerWCchDwImTBhEXMbbyDC::IIConZhTlDhyrUEUmJUbhmrJubd(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t tirTbIgerWCchDwImTBhEXMbbyDC_IIConZhTlDhyrUEUmJUbhmrJubd_m48AC53F1EA2DFC16CEF1B538F05F18F00EDB012E (int32_t p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tirTbIgerWCchDwImTBhEXMbbyDC_IIConZhTlDhyrUEUmJUbhmrJubd_m48AC53F1EA2DFC16CEF1B538F05F18F00EDB012E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B9_1 = 0;
	{
		int32_t L_0 = p0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_1 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_1, _stringLiteralD69C47B2876BE9D0D3ED8E67AAB6F47CAD39216F, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, tirTbIgerWCchDwImTBhEXMbbyDC_IIConZhTlDhyrUEUmJUbhmrJubd_m48AC53F1EA2DFC16CEF1B538F05F18F00EDB012E_RuntimeMethod_var);
	}

IL_000f:
	{
		G_B3_0 = ((int32_t)-2120015354);
	}

IL_0014:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)-2120015353))))
		{
			case 0:
			{
				goto IL_007c;
			}
			case 1:
			{
				goto IL_0045;
			}
			case 2:
			{
				goto IL_000f;
			}
			case 3:
			{
				goto IL_0089;
			}
			case 4:
			{
				goto IL_004e;
			}
			case 5:
			{
				goto IL_00c1;
			}
			case 6:
			{
				goto IL_00a7;
			}
			case 7:
			{
				goto IL_0099;
			}
			case 8:
			{
				goto IL_0069;
			}
		}
	}
	{
		goto IL_00c1;
	}

IL_0045:
	{
		V_0 = 0;
		G_B3_0 = ((int32_t)-2120015357);
		goto IL_0014;
	}

IL_004e:
	{
		int32_t L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_il2cpp_TypeInfo_var);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_3 = ((tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_StaticFields*)il2cpp_codegen_static_fields_for(tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_il2cpp_TypeInfo_var))->get_fxnhIWEsHEritNuKepbGhDThoNR_2();
		NullCheck(L_3);
		if ((((int32_t)L_2) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))))))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_4 = ((int32_t)-2120015360);
		G_B9_0 = L_4;
		G_B9_1 = L_4;
		goto IL_0066;
	}

IL_0060:
	{
		int32_t L_5 = ((int32_t)-2120015345);
		G_B9_0 = L_5;
		G_B9_1 = L_5;
	}

IL_0066:
	{
		G_B3_0 = G_B9_1;
		goto IL_0014;
	}

IL_0069:
	{
		IL2CPP_RUNTIME_CLASS_INIT(tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_il2cpp_TypeInfo_var);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_6 = ((tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_StaticFields*)il2cpp_codegen_static_fields_for(tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_il2cpp_TypeInfo_var))->get_fxnhIWEsHEritNuKepbGhDThoNR_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		int32_t L_10 = V_1;
		int32_t L_11 = p0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_008b;
		}
	}
	{
		G_B3_0 = ((int32_t)-2120015356);
		goto IL_0014;
	}

IL_007c:
	{
		int32_t L_12 = V_2;
		return L_12;
	}

IL_007e:
	{
		CHECK_PAUSE_POINT;
		int32_t L_13 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)2));
		G_B3_0 = ((int32_t)-2120015358);
		goto IL_0014;
	}

IL_0089:
	{
		int32_t L_14 = V_1;
		return L_14;
	}

IL_008b:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
		G_B3_0 = ((int32_t)-2120015357);
		goto IL_0014;
	}

IL_0099:
	{
		int32_t L_16 = p0;
		V_2 = ((int32_t)((int32_t)L_16|(int32_t)1));
		G_B3_0 = ((int32_t)-2120015358);
		goto IL_0014;
	}

IL_00a7:
	{
		CHECK_PAUSE_POINT;
		int32_t L_17 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_il2cpp_TypeInfo_var);
		bool L_18 = tirTbIgerWCchDwImTBhEXMbbyDC_aPDcQrAxDVfpHqyPrEgiDuiWmsWb_m5DA589A3287320E721A735A3978A4046BFA0EB2F(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_19 = V_2;
		if (!((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1))%(int32_t)((int32_t)101))))
		{
			goto IL_007e;
		}
	}
	{
		G_B3_0 = ((int32_t)-2120015353);
		goto IL_0014;
	}

IL_00c1:
	{
		int32_t L_20 = V_2;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)2147483647LL))))
		{
			goto IL_00a7;
		}
	}
	{
		int32_t L_21 = p0;
		return L_21;
	}
}
// System.Int32 tirTbIgerWCchDwImTBhEXMbbyDC::MzJXkgGxfrtJiysYOBOEvYonDZs()
extern "C" IL2CPP_METHOD_ATTR int32_t tirTbIgerWCchDwImTBhEXMbbyDC_MzJXkgGxfrtJiysYOBOEvYonDZs_m67F29672BD9863727FA97687C38BFD58BAD165F5 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tirTbIgerWCchDwImTBhEXMbbyDC_MzJXkgGxfrtJiysYOBOEvYonDZs_m67F29672BD9863727FA97687C38BFD58BAD165F5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		IL2CPP_RUNTIME_CLASS_INIT(tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_il2cpp_TypeInfo_var);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_0 = ((tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_StaticFields*)il2cpp_codegen_static_fields_for(tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_il2cpp_TypeInfo_var))->get_fxnhIWEsHEritNuKepbGhDThoNR_2();
		NullCheck(L_0);
		int32_t L_1 = 0;
		int32_t L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		return L_2;
	}
}
// System.Int32 tirTbIgerWCchDwImTBhEXMbbyDC::qiheowigyVKPVHALwnyqlItUxgJ(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t tirTbIgerWCchDwImTBhEXMbbyDC_qiheowigyVKPVHALwnyqlItUxgJ_mD57CB3090D7F727F87CF4D3C1ECA15C5C3FA4D63 (int32_t p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tirTbIgerWCchDwImTBhEXMbbyDC_qiheowigyVKPVHALwnyqlItUxgJ_mD57CB3090D7F727F87CF4D3C1ECA15C5C3FA4D63_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CHECK_PAUSE_POINT;
	{
		int32_t L_0 = p0;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_0));
		int32_t L_1 = V_0;
		if ((!(((uint32_t)L_1) > ((uint32_t)((int32_t)2146435069)))))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = p0;
		if ((((int32_t)((int32_t)2146435069)) <= ((int32_t)L_2)))
		{
			goto IL_001a;
		}
	}
	{
		return ((int32_t)2146435069);
	}

IL_001a:
	{
		int32_t L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_il2cpp_TypeInfo_var);
		int32_t L_4 = tirTbIgerWCchDwImTBhEXMbbyDC_IIConZhTlDhyrUEUmJUbhmrJubd_m48AC53F1EA2DFC16CEF1B538F05F18F00EDB012E(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void tirTbIgerWCchDwImTBhEXMbbyDC::.cctor()
extern "C" IL2CPP_METHOD_ATTR void tirTbIgerWCchDwImTBhEXMbbyDC__cctor_mC2DC9B7831464ECC758D76428547E79134B1E1E7 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tirTbIgerWCchDwImTBhEXMbbyDC__cctor_mC2DC9B7831464ECC758D76428547E79134B1E1E7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_0 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var, (uint32_t)((int32_t)72));
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_1 = L_0;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_2 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3EU7B09B43BCCU2D3BA7U2D4C9CU2D9EA3U2D9393C96CF203U7D_t2294A85ED0D53C43A0934AE9A86EBFD96609A170____U24U24method0x600327aU2D1_6_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_1, L_2, /*hidden argument*/NULL);
		((tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_StaticFields*)il2cpp_codegen_static_fields_for(tirTbIgerWCchDwImTBhEXMbbyDC_t11C5ADD759CABF0E86B077DAE414A5E457DD753B_il2cpp_TypeInfo_var))->set_fxnhIWEsHEritNuKepbGhDThoNR_2(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: uzcGPPJqbITKwkQVaYRGzbIZqLX
extern "C" void uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshal_pinvoke(const uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491& unmarshaled, uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshaled_pinvoke& marshaled)
{
	marshaled.___wTYLYalPavcctjXZrASJNkapMaJ_0 = unmarshaled.get_wTYLYalPavcctjXZrASJNkapMaJ_0();
	marshaled.___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1 = unmarshaled.get_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1();
	marshaled.___euLduoAiVMxRQGLWMCGBXUDrCKr_2 = static_cast<uint8_t>(unmarshaled.get_euLduoAiVMxRQGLWMCGBXUDrCKr_2());
	marshaled.___BlWgkqEeUMGgBlbpbsWvFckFbDv_3 = unmarshaled.get_BlWgkqEeUMGgBlbpbsWvFckFbDv_3();
	marshaled.___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4 = unmarshaled.get_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4();
	marshaled.___lxdbJzaQWeBlRYInsjMuuXhyCggl_5 = unmarshaled.get_lxdbJzaQWeBlRYInsjMuuXhyCggl_5();
	marshaled.___evDemCrkcBEYeHncrlFUdsIyAKPa_6 = unmarshaled.get_evDemCrkcBEYeHncrlFUdsIyAKPa_6();
	marshaled.___mUqspDOssdvKUAQvOxwRVdbooCb_7 = unmarshaled.get_mUqspDOssdvKUAQvOxwRVdbooCb_7();
	marshaled.___HqWkThUinZjstdlLHSIImaHNnAis_8 = static_cast<int32_t>(unmarshaled.get_HqWkThUinZjstdlLHSIImaHNnAis_8());
}
extern "C" void uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshal_pinvoke_back(const uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshaled_pinvoke& marshaled, uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491& unmarshaled)
{
	uint8_t unmarshaled_wTYLYalPavcctjXZrASJNkapMaJ_temp_0 = 0x0;
	unmarshaled_wTYLYalPavcctjXZrASJNkapMaJ_temp_0 = marshaled.___wTYLYalPavcctjXZrASJNkapMaJ_0;
	unmarshaled.set_wTYLYalPavcctjXZrASJNkapMaJ_0(unmarshaled_wTYLYalPavcctjXZrASJNkapMaJ_temp_0);
	int8_t unmarshaled_MJrfBrxoBTvDrzGeBrTAImwmNOZ_temp_1 = 0x0;
	unmarshaled_MJrfBrxoBTvDrzGeBrTAImwmNOZ_temp_1 = marshaled.___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
	unmarshaled.set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1(unmarshaled_MJrfBrxoBTvDrzGeBrTAImwmNOZ_temp_1);
	Il2CppChar unmarshaled_euLduoAiVMxRQGLWMCGBXUDrCKr_temp_2 = 0x0;
	unmarshaled_euLduoAiVMxRQGLWMCGBXUDrCKr_temp_2 = static_cast<Il2CppChar>(marshaled.___euLduoAiVMxRQGLWMCGBXUDrCKr_2);
	unmarshaled.set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(unmarshaled_euLduoAiVMxRQGLWMCGBXUDrCKr_temp_2);
	int16_t unmarshaled_BlWgkqEeUMGgBlbpbsWvFckFbDv_temp_3 = 0;
	unmarshaled_BlWgkqEeUMGgBlbpbsWvFckFbDv_temp_3 = marshaled.___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
	unmarshaled.set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3(unmarshaled_BlWgkqEeUMGgBlbpbsWvFckFbDv_temp_3);
	uint16_t unmarshaled_WBbwvCOIPVbAJbjWvqCbJElmQLtd_temp_4 = 0;
	unmarshaled_WBbwvCOIPVbAJbjWvqCbJElmQLtd_temp_4 = marshaled.___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
	unmarshaled.set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4(unmarshaled_WBbwvCOIPVbAJbjWvqCbJElmQLtd_temp_4);
	int32_t unmarshaled_lxdbJzaQWeBlRYInsjMuuXhyCggl_temp_5 = 0;
	unmarshaled_lxdbJzaQWeBlRYInsjMuuXhyCggl_temp_5 = marshaled.___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
	unmarshaled.set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(unmarshaled_lxdbJzaQWeBlRYInsjMuuXhyCggl_temp_5);
	uint32_t unmarshaled_evDemCrkcBEYeHncrlFUdsIyAKPa_temp_6 = 0;
	unmarshaled_evDemCrkcBEYeHncrlFUdsIyAKPa_temp_6 = marshaled.___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
	unmarshaled.set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(unmarshaled_evDemCrkcBEYeHncrlFUdsIyAKPa_temp_6);
	float unmarshaled_mUqspDOssdvKUAQvOxwRVdbooCb_temp_7 = 0.0f;
	unmarshaled_mUqspDOssdvKUAQvOxwRVdbooCb_temp_7 = marshaled.___mUqspDOssdvKUAQvOxwRVdbooCb_7;
	unmarshaled.set_mUqspDOssdvKUAQvOxwRVdbooCb_7(unmarshaled_mUqspDOssdvKUAQvOxwRVdbooCb_temp_7);
	bool unmarshaled_HqWkThUinZjstdlLHSIImaHNnAis_temp_8 = false;
	unmarshaled_HqWkThUinZjstdlLHSIImaHNnAis_temp_8 = static_cast<bool>(marshaled.___HqWkThUinZjstdlLHSIImaHNnAis_8);
	unmarshaled.set_HqWkThUinZjstdlLHSIImaHNnAis_8(unmarshaled_HqWkThUinZjstdlLHSIImaHNnAis_temp_8);
}
// Conversion method for clean up from marshalling of: uzcGPPJqbITKwkQVaYRGzbIZqLX
extern "C" void uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshal_pinvoke_cleanup(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: uzcGPPJqbITKwkQVaYRGzbIZqLX
extern "C" void uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshal_com(const uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491& unmarshaled, uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshaled_com& marshaled)
{
	marshaled.___wTYLYalPavcctjXZrASJNkapMaJ_0 = unmarshaled.get_wTYLYalPavcctjXZrASJNkapMaJ_0();
	marshaled.___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1 = unmarshaled.get_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1();
	marshaled.___euLduoAiVMxRQGLWMCGBXUDrCKr_2 = static_cast<uint8_t>(unmarshaled.get_euLduoAiVMxRQGLWMCGBXUDrCKr_2());
	marshaled.___BlWgkqEeUMGgBlbpbsWvFckFbDv_3 = unmarshaled.get_BlWgkqEeUMGgBlbpbsWvFckFbDv_3();
	marshaled.___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4 = unmarshaled.get_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4();
	marshaled.___lxdbJzaQWeBlRYInsjMuuXhyCggl_5 = unmarshaled.get_lxdbJzaQWeBlRYInsjMuuXhyCggl_5();
	marshaled.___evDemCrkcBEYeHncrlFUdsIyAKPa_6 = unmarshaled.get_evDemCrkcBEYeHncrlFUdsIyAKPa_6();
	marshaled.___mUqspDOssdvKUAQvOxwRVdbooCb_7 = unmarshaled.get_mUqspDOssdvKUAQvOxwRVdbooCb_7();
	marshaled.___HqWkThUinZjstdlLHSIImaHNnAis_8 = static_cast<int32_t>(unmarshaled.get_HqWkThUinZjstdlLHSIImaHNnAis_8());
}
extern "C" void uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshal_com_back(const uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshaled_com& marshaled, uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491& unmarshaled)
{
	uint8_t unmarshaled_wTYLYalPavcctjXZrASJNkapMaJ_temp_0 = 0x0;
	unmarshaled_wTYLYalPavcctjXZrASJNkapMaJ_temp_0 = marshaled.___wTYLYalPavcctjXZrASJNkapMaJ_0;
	unmarshaled.set_wTYLYalPavcctjXZrASJNkapMaJ_0(unmarshaled_wTYLYalPavcctjXZrASJNkapMaJ_temp_0);
	int8_t unmarshaled_MJrfBrxoBTvDrzGeBrTAImwmNOZ_temp_1 = 0x0;
	unmarshaled_MJrfBrxoBTvDrzGeBrTAImwmNOZ_temp_1 = marshaled.___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
	unmarshaled.set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1(unmarshaled_MJrfBrxoBTvDrzGeBrTAImwmNOZ_temp_1);
	Il2CppChar unmarshaled_euLduoAiVMxRQGLWMCGBXUDrCKr_temp_2 = 0x0;
	unmarshaled_euLduoAiVMxRQGLWMCGBXUDrCKr_temp_2 = static_cast<Il2CppChar>(marshaled.___euLduoAiVMxRQGLWMCGBXUDrCKr_2);
	unmarshaled.set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(unmarshaled_euLduoAiVMxRQGLWMCGBXUDrCKr_temp_2);
	int16_t unmarshaled_BlWgkqEeUMGgBlbpbsWvFckFbDv_temp_3 = 0;
	unmarshaled_BlWgkqEeUMGgBlbpbsWvFckFbDv_temp_3 = marshaled.___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
	unmarshaled.set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3(unmarshaled_BlWgkqEeUMGgBlbpbsWvFckFbDv_temp_3);
	uint16_t unmarshaled_WBbwvCOIPVbAJbjWvqCbJElmQLtd_temp_4 = 0;
	unmarshaled_WBbwvCOIPVbAJbjWvqCbJElmQLtd_temp_4 = marshaled.___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
	unmarshaled.set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4(unmarshaled_WBbwvCOIPVbAJbjWvqCbJElmQLtd_temp_4);
	int32_t unmarshaled_lxdbJzaQWeBlRYInsjMuuXhyCggl_temp_5 = 0;
	unmarshaled_lxdbJzaQWeBlRYInsjMuuXhyCggl_temp_5 = marshaled.___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
	unmarshaled.set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(unmarshaled_lxdbJzaQWeBlRYInsjMuuXhyCggl_temp_5);
	uint32_t unmarshaled_evDemCrkcBEYeHncrlFUdsIyAKPa_temp_6 = 0;
	unmarshaled_evDemCrkcBEYeHncrlFUdsIyAKPa_temp_6 = marshaled.___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
	unmarshaled.set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(unmarshaled_evDemCrkcBEYeHncrlFUdsIyAKPa_temp_6);
	float unmarshaled_mUqspDOssdvKUAQvOxwRVdbooCb_temp_7 = 0.0f;
	unmarshaled_mUqspDOssdvKUAQvOxwRVdbooCb_temp_7 = marshaled.___mUqspDOssdvKUAQvOxwRVdbooCb_7;
	unmarshaled.set_mUqspDOssdvKUAQvOxwRVdbooCb_7(unmarshaled_mUqspDOssdvKUAQvOxwRVdbooCb_temp_7);
	bool unmarshaled_HqWkThUinZjstdlLHSIImaHNnAis_temp_8 = false;
	unmarshaled_HqWkThUinZjstdlLHSIImaHNnAis_temp_8 = static_cast<bool>(marshaled.___HqWkThUinZjstdlLHSIImaHNnAis_8);
	unmarshaled.set_HqWkThUinZjstdlLHSIImaHNnAis_8(unmarshaled_HqWkThUinZjstdlLHSIImaHNnAis_temp_8);
}
// Conversion method for clean up from marshalling of: uzcGPPJqbITKwkQVaYRGzbIZqLX
extern "C" void uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshal_com_cleanup(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshaled_com& marshaled)
{
}
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.Byte)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m0C8AEAFA33EC5F222E5AF8F800E4B14FF1E898E0 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, uint8_t ___item0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		__this->set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1((int8_t)0);
		__this->set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(0);
		__this->set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3((int16_t)0);
		__this->set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4((uint16_t)0);
		__this->set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(0);
		__this->set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(0);
		__this->set_mUqspDOssdvKUAQvOxwRVdbooCb_7((0.0f));
		__this->set_HqWkThUinZjstdlLHSIImaHNnAis_8((bool)0);
		uint8_t L_0 = ___item0;
		__this->set_wTYLYalPavcctjXZrASJNkapMaJ_0(L_0);
		return;
	}
}
extern "C"  void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m0C8AEAFA33EC5F222E5AF8F800E4B14FF1E898E0_AdjustorThunk (RuntimeObject * __this, uint8_t ___item0, const RuntimeMethod* method)
{
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * _thisAdjusted = reinterpret_cast<uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 *>(__this + 1);
	uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m0C8AEAFA33EC5F222E5AF8F800E4B14FF1E898E0(_thisAdjusted, ___item0, method);
}
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.SByte)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m8F4523F3EC12BF60A64C9BDB32B0BEBF7A2AA1A3 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, int8_t ___item0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		__this->set_wTYLYalPavcctjXZrASJNkapMaJ_0((uint8_t)0);
		__this->set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(0);
		__this->set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3((int16_t)0);
		__this->set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4((uint16_t)0);
		__this->set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(0);
		__this->set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(0);
		__this->set_mUqspDOssdvKUAQvOxwRVdbooCb_7((0.0f));
		__this->set_HqWkThUinZjstdlLHSIImaHNnAis_8((bool)0);
		int8_t L_0 = ___item0;
		__this->set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1(L_0);
		return;
	}
}
extern "C"  void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m8F4523F3EC12BF60A64C9BDB32B0BEBF7A2AA1A3_AdjustorThunk (RuntimeObject * __this, int8_t ___item0, const RuntimeMethod* method)
{
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * _thisAdjusted = reinterpret_cast<uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 *>(__this + 1);
	uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m8F4523F3EC12BF60A64C9BDB32B0BEBF7A2AA1A3(_thisAdjusted, ___item0, method);
}
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.Char)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mAF0678FB5A51C558C753BABBDA979A14090FE543 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, Il2CppChar ___item0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		__this->set_wTYLYalPavcctjXZrASJNkapMaJ_0((uint8_t)0);
		__this->set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1((int8_t)0);
		__this->set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3((int16_t)0);
		__this->set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4((uint16_t)0);
		__this->set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(0);
		__this->set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(0);
		__this->set_mUqspDOssdvKUAQvOxwRVdbooCb_7((0.0f));
		__this->set_HqWkThUinZjstdlLHSIImaHNnAis_8((bool)0);
		Il2CppChar L_0 = ___item0;
		__this->set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(L_0);
		return;
	}
}
extern "C"  void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mAF0678FB5A51C558C753BABBDA979A14090FE543_AdjustorThunk (RuntimeObject * __this, Il2CppChar ___item0, const RuntimeMethod* method)
{
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * _thisAdjusted = reinterpret_cast<uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 *>(__this + 1);
	uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mAF0678FB5A51C558C753BABBDA979A14090FE543(_thisAdjusted, ___item0, method);
}
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.Int16)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m871803D317A588AA09655DF15468C51C9E3199FB (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, int16_t ___item0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		__this->set_wTYLYalPavcctjXZrASJNkapMaJ_0((uint8_t)0);
		__this->set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1((int8_t)0);
		__this->set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(0);
		__this->set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4((uint16_t)0);
		__this->set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(0);
		__this->set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(0);
		__this->set_mUqspDOssdvKUAQvOxwRVdbooCb_7((0.0f));
		__this->set_HqWkThUinZjstdlLHSIImaHNnAis_8((bool)0);
		int16_t L_0 = ___item0;
		__this->set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3(L_0);
		return;
	}
}
extern "C"  void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m871803D317A588AA09655DF15468C51C9E3199FB_AdjustorThunk (RuntimeObject * __this, int16_t ___item0, const RuntimeMethod* method)
{
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * _thisAdjusted = reinterpret_cast<uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 *>(__this + 1);
	uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m871803D317A588AA09655DF15468C51C9E3199FB(_thisAdjusted, ___item0, method);
}
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.UInt16)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m010ECA268A1AD0F48453089A49BDD474FAC57E14 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, uint16_t ___item0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		__this->set_wTYLYalPavcctjXZrASJNkapMaJ_0((uint8_t)0);
		__this->set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1((int8_t)0);
		__this->set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(0);
		__this->set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3((int16_t)0);
		__this->set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(0);
		__this->set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(0);
		__this->set_mUqspDOssdvKUAQvOxwRVdbooCb_7((0.0f));
		__this->set_HqWkThUinZjstdlLHSIImaHNnAis_8((bool)0);
		uint16_t L_0 = ___item0;
		__this->set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4(L_0);
		return;
	}
}
extern "C"  void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m010ECA268A1AD0F48453089A49BDD474FAC57E14_AdjustorThunk (RuntimeObject * __this, uint16_t ___item0, const RuntimeMethod* method)
{
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * _thisAdjusted = reinterpret_cast<uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 *>(__this + 1);
	uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m010ECA268A1AD0F48453089A49BDD474FAC57E14(_thisAdjusted, ___item0, method);
}
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m68321438AF088D4FD0EFE32AD5FEF7F1853FFD16 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		__this->set_wTYLYalPavcctjXZrASJNkapMaJ_0((uint8_t)0);
		__this->set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1((int8_t)0);
		__this->set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(0);
		__this->set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3((int16_t)0);
		__this->set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4((uint16_t)0);
		__this->set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(0);
		__this->set_mUqspDOssdvKUAQvOxwRVdbooCb_7((0.0f));
		__this->set_HqWkThUinZjstdlLHSIImaHNnAis_8((bool)0);
		int32_t L_0 = ___item0;
		__this->set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(L_0);
		return;
	}
}
extern "C"  void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m68321438AF088D4FD0EFE32AD5FEF7F1853FFD16_AdjustorThunk (RuntimeObject * __this, int32_t ___item0, const RuntimeMethod* method)
{
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * _thisAdjusted = reinterpret_cast<uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 *>(__this + 1);
	uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m68321438AF088D4FD0EFE32AD5FEF7F1853FFD16(_thisAdjusted, ___item0, method);
}
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.UInt32)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mF73F3B4D904B0C6B80C7A8CEC1FDCF84E4E2BD7A (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, uint32_t ___item0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		__this->set_wTYLYalPavcctjXZrASJNkapMaJ_0((uint8_t)0);
		__this->set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1((int8_t)0);
		__this->set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(0);
		__this->set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3((int16_t)0);
		__this->set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4((uint16_t)0);
		__this->set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(0);
		__this->set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(0);
		__this->set_mUqspDOssdvKUAQvOxwRVdbooCb_7((0.0f));
		__this->set_HqWkThUinZjstdlLHSIImaHNnAis_8((bool)0);
		uint32_t L_0 = ___item0;
		__this->set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(L_0);
		return;
	}
}
extern "C"  void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mF73F3B4D904B0C6B80C7A8CEC1FDCF84E4E2BD7A_AdjustorThunk (RuntimeObject * __this, uint32_t ___item0, const RuntimeMethod* method)
{
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * _thisAdjusted = reinterpret_cast<uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 *>(__this + 1);
	uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mF73F3B4D904B0C6B80C7A8CEC1FDCF84E4E2BD7A(_thisAdjusted, ___item0, method);
}
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mEFF87B0699854400E736A2FFE668B6CDF0DEA4A3 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, float ___item0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		__this->set_wTYLYalPavcctjXZrASJNkapMaJ_0((uint8_t)0);
		__this->set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1((int8_t)0);
		__this->set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(0);
		__this->set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3((int16_t)0);
		__this->set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4((uint16_t)0);
		__this->set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(0);
		__this->set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(0);
		__this->set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(0);
		__this->set_HqWkThUinZjstdlLHSIImaHNnAis_8((bool)0);
		float L_0 = ___item0;
		__this->set_mUqspDOssdvKUAQvOxwRVdbooCb_7(L_0);
		return;
	}
}
extern "C"  void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mEFF87B0699854400E736A2FFE668B6CDF0DEA4A3_AdjustorThunk (RuntimeObject * __this, float ___item0, const RuntimeMethod* method)
{
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * _thisAdjusted = reinterpret_cast<uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 *>(__this + 1);
	uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mEFF87B0699854400E736A2FFE668B6CDF0DEA4A3(_thisAdjusted, ___item0, method);
}
// System.Void uzcGPPJqbITKwkQVaYRGzbIZqLX::.ctor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m02EE5FC729C82C032AC353BFEB62E106FD367804 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * __this, bool ___item0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		__this->set_wTYLYalPavcctjXZrASJNkapMaJ_0((uint8_t)0);
		__this->set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1((int8_t)0);
		__this->set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(0);
		__this->set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3((int16_t)0);
		__this->set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4((uint16_t)0);
		__this->set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(0);
		__this->set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(0);
		__this->set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(0);
		__this->set_mUqspDOssdvKUAQvOxwRVdbooCb_7((0.0f));
		bool L_0 = ___item0;
		__this->set_HqWkThUinZjstdlLHSIImaHNnAis_8(L_0);
		return;
	}
}
extern "C"  void uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m02EE5FC729C82C032AC353BFEB62E106FD367804_AdjustorThunk (RuntimeObject * __this, bool ___item0, const RuntimeMethod* method)
{
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 * _thisAdjusted = reinterpret_cast<uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 *>(__this + 1);
	uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m02EE5FC729C82C032AC353BFEB62E106FD367804(_thisAdjusted, ___item0, method);
}
// System.Byte uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(uzcGPPJqbITKwkQVaYRGzbIZqLX)
extern "C" IL2CPP_METHOD_ATTR uint8_t uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_m4B429B56B26E0BEE736E6BBEA98B9A7D5E4FAF95 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		uint8_t L_0 = (&___obj0)->get_wTYLYalPavcctjXZrASJNkapMaJ_0();
		return L_0;
	}
}
// System.SByte uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(uzcGPPJqbITKwkQVaYRGzbIZqLX)
extern "C" IL2CPP_METHOD_ATTR int8_t uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_m4EF193804CC265C93E8EB519866BA9BB8EA43F14 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int8_t L_0 = (&___obj0)->get_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1();
		return L_0;
	}
}
// System.Char uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(uzcGPPJqbITKwkQVaYRGzbIZqLX)
extern "C" IL2CPP_METHOD_ATTR Il2CppChar uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_m1198A43DEBFE8077E95871A356F6E12E2A6FF298 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		Il2CppChar L_0 = (&___obj0)->get_euLduoAiVMxRQGLWMCGBXUDrCKr_2();
		return L_0;
	}
}
// System.Int32 uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(uzcGPPJqbITKwkQVaYRGzbIZqLX)
extern "C" IL2CPP_METHOD_ATTR int32_t uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_m1C3B64DF72B6A808E572E5645B8AC183BA699B3D (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int32_t L_0 = (&___obj0)->get_lxdbJzaQWeBlRYInsjMuuXhyCggl_5();
		return L_0;
	}
}
// System.UInt32 uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(uzcGPPJqbITKwkQVaYRGzbIZqLX)
extern "C" IL2CPP_METHOD_ATTR uint32_t uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_m87E5FD4FF364FBE84712275E1EEBEE5CD28136D0 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		uint32_t L_0 = (&___obj0)->get_evDemCrkcBEYeHncrlFUdsIyAKPa_6();
		return L_0;
	}
}
// System.Single uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(uzcGPPJqbITKwkQVaYRGzbIZqLX)
extern "C" IL2CPP_METHOD_ATTR float uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_mDB5046DD8BF221B95BF26DAF6E552F30F6919CAD (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		float L_0 = (&___obj0)->get_mUqspDOssdvKUAQvOxwRVdbooCb_7();
		return L_0;
	}
}
// System.Boolean uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(uzcGPPJqbITKwkQVaYRGzbIZqLX)
extern "C" IL2CPP_METHOD_ATTR bool uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_m22F33BBF457FA9B0616D009A16FEC0F29CFB2E19 (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		bool L_0 = (&___obj0)->get_HqWkThUinZjstdlLHSIImaHNnAis_8();
		return L_0;
	}
}
// uzcGPPJqbITKwkQVaYRGzbIZqLX uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(System.Byte)
extern "C" IL2CPP_METHOD_ATTR uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_mE59A7C97AB28C69064E73C96631B0E6AF8A5C266 (uint8_t ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		uint8_t L_0 = ___obj0;
		uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  L_1;
		memset(&L_1, 0, sizeof(L_1));
		uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m0C8AEAFA33EC5F222E5AF8F800E4B14FF1E898E0((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// uzcGPPJqbITKwkQVaYRGzbIZqLX uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(System.SByte)
extern "C" IL2CPP_METHOD_ATTR uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_m8CC35510BB0538EAB8F1953472354FCBECBAA2A1 (int8_t ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int8_t L_0 = ___obj0;
		uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  L_1;
		memset(&L_1, 0, sizeof(L_1));
		uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m8F4523F3EC12BF60A64C9BDB32B0BEBF7A2AA1A3((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// uzcGPPJqbITKwkQVaYRGzbIZqLX uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(System.Char)
extern "C" IL2CPP_METHOD_ATTR uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_m276A99E579A5FDBB320AF1427C8642E646CE9916 (Il2CppChar ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		Il2CppChar L_0 = ___obj0;
		uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  L_1;
		memset(&L_1, 0, sizeof(L_1));
		uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mAF0678FB5A51C558C753BABBDA979A14090FE543((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// uzcGPPJqbITKwkQVaYRGzbIZqLX uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(System.Int32)
extern "C" IL2CPP_METHOD_ATTR uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_mDB6E8E1AC084F7FCE1E5BF57DADCB99FA2F75F16 (int32_t ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int32_t L_0 = ___obj0;
		uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  L_1;
		memset(&L_1, 0, sizeof(L_1));
		uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m68321438AF088D4FD0EFE32AD5FEF7F1853FFD16((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// uzcGPPJqbITKwkQVaYRGzbIZqLX uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(System.UInt32)
extern "C" IL2CPP_METHOD_ATTR uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_m4AFA2E0DE1BCED56F7F5A77D89F353BC56B05619 (uint32_t ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		uint32_t L_0 = ___obj0;
		uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  L_1;
		memset(&L_1, 0, sizeof(L_1));
		uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mF73F3B4D904B0C6B80C7A8CEC1FDCF84E4E2BD7A((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// uzcGPPJqbITKwkQVaYRGzbIZqLX uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(System.Single)
extern "C" IL2CPP_METHOD_ATTR uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_m2760413E11DAEEEADA47D7E451141D7180BC3A5A (float ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		float L_0 = ___obj0;
		uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  L_1;
		memset(&L_1, 0, sizeof(L_1));
		uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_mEFF87B0699854400E736A2FFE668B6CDF0DEA4A3((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// uzcGPPJqbITKwkQVaYRGzbIZqLX uzcGPPJqbITKwkQVaYRGzbIZqLX::op_Implicit(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  uzcGPPJqbITKwkQVaYRGzbIZqLX_op_Implicit_m6FBB90B68A8CC6C168B1809F0954F26C0A724B66 (bool ___obj0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		bool L_0 = ___obj0;
		uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491  L_1;
		memset(&L_1, 0, sizeof(L_1));
		uzcGPPJqbITKwkQVaYRGzbIZqLX__ctor_m02EE5FC729C82C032AC353BFEB62E106FD367804((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vLEBhncKpOPFOKOXDdCbdgTKhyd::.ctor()
extern "C" IL2CPP_METHOD_ATTR void vLEBhncKpOPFOKOXDdCbdgTKhyd__ctor_m8F434D6AAEA68964101F4A78A83C240F8B85C960 (vLEBhncKpOPFOKOXDdCbdgTKhyd_tA01C4EADEDC175C2FAC271C09D7E5245A52EFCBB * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void wACwxZjEFhAfUzXpGGPyAhJldeo::nbgZHGrFJfzAlaIQqyHguklefEZ(System.Object)
extern "C" IL2CPP_METHOD_ATTR void wACwxZjEFhAfUzXpGGPyAhJldeo_nbgZHGrFJfzAlaIQqyHguklefEZ_m65A3E6705BFA9A60D9D7FE15169969760CDE062D (RuntimeObject * p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (wACwxZjEFhAfUzXpGGPyAhJldeo_nbgZHGrFJfzAlaIQqyHguklefEZ_m65A3E6705BFA9A60D9D7FE15169969760CDE062D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	{
		RuntimeObject * L_0 = p0;
		if (L_0)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		p0 = L_1;
	}

IL_000a:
	{
		G_B3_0 = ((int32_t)1575851469);
	}

IL_000f:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)1575851468))))
		{
			case 0:
			{
				goto IL_000a;
			}
			case 1:
			{
				goto IL_0028;
			}
			case 2:
			{
				goto IL_003f;
			}
		}
	}
	{
		goto IL_003f;
	}

IL_0028:
	{
		RuntimeObject * L_2 = p0;
		String_t* L_3 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralF6DE2FBED7D1AA24173C74F69B1B226EFC989186, L_2, /*hidden argument*/NULL);
		Logger_Log_m5DB6C749C057F4801A8DA9B700A85179B354F869(L_3, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)1575851470);
		goto IL_000f;
	}

IL_003f:
	{
		return;
	}
}
// System.Void wACwxZjEFhAfUzXpGGPyAhJldeo::TsrpdEkqOGaRPyBjAPFdswjyuxG(System.Object)
extern "C" IL2CPP_METHOD_ATTR void wACwxZjEFhAfUzXpGGPyAhJldeo_TsrpdEkqOGaRPyBjAPFdswjyuxG_m03DCFF4405EB60D229D590BEFE90248D47D463DA (RuntimeObject * p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (wACwxZjEFhAfUzXpGGPyAhJldeo_TsrpdEkqOGaRPyBjAPFdswjyuxG_m03DCFF4405EB60D229D590BEFE90248D47D463DA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		RuntimeObject * L_0 = p0;
		if (L_0)
		{
			goto IL_0033;
		}
	}

IL_0003:
	{
		G_B2_0 = ((int32_t)-293580807);
	}

IL_0008:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-293580808))))
		{
			case 0:
			{
				goto IL_004a;
			}
			case 1:
			{
				goto IL_0025;
			}
			case 2:
			{
				goto IL_0003;
			}
			case 3:
			{
				goto IL_0033;
			}
		}
	}
	{
		goto IL_004a;
	}

IL_0025:
	{
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		p0 = L_1;
		G_B2_0 = ((int32_t)-293580805);
		goto IL_0008;
	}

IL_0033:
	{
		RuntimeObject * L_2 = p0;
		String_t* L_3 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralF6DE2FBED7D1AA24173C74F69B1B226EFC989186, L_2, /*hidden argument*/NULL);
		Logger_LogWarning_m62073170C5D53E757FC5246E28635EAF44864197(L_3, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)-293580808);
		goto IL_0008;
	}

IL_004a:
	{
		return;
	}
}
// System.Void wACwxZjEFhAfUzXpGGPyAhJldeo::kVeCKTaEcflZOVqHSNSieChnKJA(System.Object)
extern "C" IL2CPP_METHOD_ATTR void wACwxZjEFhAfUzXpGGPyAhJldeo_kVeCKTaEcflZOVqHSNSieChnKJA_mDF438FB8F0475AD1740BA7D2BF35D611DD8F84A6 (RuntimeObject * p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (wACwxZjEFhAfUzXpGGPyAhJldeo_kVeCKTaEcflZOVqHSNSieChnKJA_mDF438FB8F0475AD1740BA7D2BF35D611DD8F84A6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		RuntimeObject * L_0 = p0;
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		p0 = L_1;
	}

IL_000a:
	{
		RuntimeObject * L_2 = p0;
		String_t* L_3 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralF6DE2FBED7D1AA24173C74F69B1B226EFC989186, L_2, /*hidden argument*/NULL);
		Logger_LogError_mD867E7161DD5405B9A7B2B7E599B3283C07E6029(L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void xvUPNQnWmcdqDjulQTyAIyRAxwG::.ctor()
extern "C" IL2CPP_METHOD_ATTR void xvUPNQnWmcdqDjulQTyAIyRAxwG__ctor_m510FF3786B83D27873B94FC3A3CC17B3D7F5A571 (xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032 * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
