﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DG.Tweening.Core.ABSAnimationComponent
struct ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072;
// DG.Tweening.Plugins.Core.PathCore.Path
struct Path_tCE251EA0F45EA8B91CAD11018E275B451FAC6FA2;
// DG.Tweening.Tween
struct Tween_t119487E0AB84EF563521F1043116BDBAE68AC437;
// EhbFMnIgrveIkvxMjDinIUjSQTaf/gbnJVSILPyqAUDavYEfkWfNLGue[]
struct gbnJVSILPyqAUDavYEfkWfNLGueU5BU5D_tDB430E13551D4D205A0E55D0359868F2D9B2FFDE;
// OmePuLXqingOShAfdEjGZVdOnDpG
struct OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708;
// Rewired.ActionElementMap
struct ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7;
// Rewired.ButtonStateRecorder/hxGGSVmQZTfybORfTgCfaxyzTkP[]
struct hxGGSVmQZTfybORfTgCfaxyzTkPU5BU5D_t908A9DCED320AADA7459319C977B2158437BE151;
// Rewired.Controller
struct Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671;
// Rewired.ControllerMap
struct ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB;
// Rewired.ThreadSafeUnityInput/Keyboard
struct Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70;
// Rewired.ThreadSafeUnityInput/Mouse
struct Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9;
// Rewired.UnknownControllerHat/HatButtons
struct HatButtons_t1754401799C3C4C561D1A750E79B02F35BBFB594;
// Rewired.UpdateLoopDataSet`1/fqOEnqCjzfqsSLfWbeeEWCDwDptc<Rewired.ButtonLoopSet/ButtonData>
struct fqOEnqCjzfqsSLfWbeeEWCDwDptc_t94EF923B215415665C576F97032CD5E66CA20199;
// Rewired.UpdateLoopDataSet`1/fqOEnqCjzfqsSLfWbeeEWCDwDptc<Rewired.ButtonLoopSet/ButtonData>[]
struct fqOEnqCjzfqsSLfWbeeEWCDwDptcU5BU5D_tA52DDE8E221AAE179D957A7BD3D0BF3F44580235;
// Rewired.Utils.Classes.Data.AList`1<Rewired.IControllerTemplate>
struct AList_1_t974A66EA135B0300CE05935362C6C7C736059320;
// Rewired.Utils.Classes.Data.AList`1<iNmAhOSEGUcqVFxmGFasdFHfdnr/lcCLwaBQyqptCsvRVWlzpdLzWrX>[]
struct AList_1U5BU5D_t87FEEAF55FE2B1BDD2D7A3F554ACBB8C174D2A92;
// Rewired.Utils.Classes.Data.AList`1<rIcvLcWCuQghgaaiofvKjSvndJX/mGZguqdUJPvKvEhsdVIRYohlOty>
struct AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99;
// Rewired.Utils.Classes.Utility.ObjectPool`1<Rewired.LowLevelButtonEvent>
struct ObjectPool_1_tF2B09DB44570E458D9E2A3700C91A70A74CA29B3;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<DG.Tweening.DOTweenPath>
struct Action_1_t9584A042310BC9A47625CC34F2259D4A9AB9884C;
// System.Action`1<Rewired.BridgedController>
struct Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5;
// System.Action`1<Rewired.ControllerDisconnectedEventArgs>
struct Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0;
// System.Action`1<Rewired.InputActionEventData>
struct Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8;
// System.Action`1<Rewired.UpdateControllerInfoEventArgs>
struct Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,Rewired.SteamAction>
struct Dictionary_2_t60634EC784EFFED4A2E1A65F8E4E2C04C45E2278;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.Func`1<Rewired.LowLevelButtonEvent>
struct Func_1_t2C8C7AF570C9781CB38423268805E9C7208378B8;
// System.Func`1<Rewired.Utils.Classes.Data.AList`1<iNmAhOSEGUcqVFxmGFasdFHfdnr/lcCLwaBQyqptCsvRVWlzpdLzWrX>>
struct Func_1_tE1ABE1F3BDE308A255EFC632258CA4A114FEC717;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;




#ifndef U3CMODULEU3E_T137236D74FF67F879C976AFCD30551A0AC83AF32_H
#define U3CMODULEU3E_T137236D74FF67F879C976AFCD30551A0AC83AF32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t137236D74FF67F879C976AFCD30551A0AC83AF32 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T137236D74FF67F879C976AFCD30551A0AC83AF32_H
#ifndef U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#define U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tCE4B768174CDE0294B05DD8ED59A7763FF34E99B 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#ifndef U3CMODULEU3E_T79596D1CF9533C008106C502C07E3DC3CC441D96_H
#define U3CMODULEU3E_T79596D1CF9533C008106C502C07E3DC3CC441D96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t79596D1CF9533C008106C502C07E3DC3CC441D96 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T79596D1CF9533C008106C502C07E3DC3CC441D96_H
#ifndef U3CMODULEU3E_T7203CD2929947A870302A54423522DDB869AC803_H
#define U3CMODULEU3E_T7203CD2929947A870302A54423522DDB869AC803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t7203CD2929947A870302A54423522DDB869AC803 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T7203CD2929947A870302A54423522DDB869AC803_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ABSTWEENPLUGIN_3_T2808B05B2F9480E2D94EC646B1B69E4BA80CAB99_H
#define ABSTWEENPLUGIN_3_T2808B05B2F9480E2D94EC646B1B69E4BA80CAB99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.SpiralOptions>
struct  ABSTweenPlugin_3_t2808B05B2F9480E2D94EC646B1B69E4BA80CAB99  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T2808B05B2F9480E2D94EC646B1B69E4BA80CAB99_H
#ifndef DDEBUG_T84DED928DEFD12E80D24749CF6180D4CD2DC5F0B_H
#define DDEBUG_T84DED928DEFD12E80D24749CF6180D4CD2DC5F0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.DDebug
struct  DDebug_t84DED928DEFD12E80D24749CF6180D4CD2DC5F0B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DDEBUG_T84DED928DEFD12E80D24749CF6180D4CD2DC5F0B_H
#ifndef EHBFMNIGRVEIKVXMJDINIUJSQTAF_TF09D190303034EC83B44DAFBC62D0DCFC736A8B5_H
#define EHBFMNIGRVEIKVXMJDINIUJSQTAF_TF09D190303034EC83B44DAFBC62D0DCFC736A8B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EhbFMnIgrveIkvxMjDinIUjSQTaf
struct  EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5  : public RuntimeObject
{
public:
	// System.Boolean EhbFMnIgrveIkvxMjDinIUjSQTaf::mvKHSWNtYxtrXAPVpClHLUktCzV
	bool ___mvKHSWNtYxtrXAPVpClHLUktCzV_1;
	// System.Boolean EhbFMnIgrveIkvxMjDinIUjSQTaf::KqsZQvXZGdcrJvGsJxuAvfvnhDe
	bool ___KqsZQvXZGdcrJvGsJxuAvfvnhDe_2;
	// System.Boolean EhbFMnIgrveIkvxMjDinIUjSQTaf::BLWYEEspSiesrpLjzxiQnjWOBUmf
	bool ___BLWYEEspSiesrpLjzxiQnjWOBUmf_3;
	// System.Single EhbFMnIgrveIkvxMjDinIUjSQTaf::ugxQlRwXrGpEMOQPtUvgtSVoUnh
	float ___ugxQlRwXrGpEMOQPtUvgtSVoUnh_4;
	// EhbFMnIgrveIkvxMjDinIUjSQTaf_gbnJVSILPyqAUDavYEfkWfNLGue[] EhbFMnIgrveIkvxMjDinIUjSQTaf::LJwjkYfhxNrwVJAlKTmoasSnlIE
	gbnJVSILPyqAUDavYEfkWfNLGueU5BU5D_tDB430E13551D4D205A0E55D0359868F2D9B2FFDE* ___LJwjkYfhxNrwVJAlKTmoasSnlIE_5;
	// System.Boolean EhbFMnIgrveIkvxMjDinIUjSQTaf::OfxqMMDGWdzhDRrNathCCkjqqeA
	bool ___OfxqMMDGWdzhDRrNathCCkjqqeA_6;
	// System.Boolean EhbFMnIgrveIkvxMjDinIUjSQTaf::xbzIuakPyoIxhUNJcNokVaqGbxB
	bool ___xbzIuakPyoIxhUNJcNokVaqGbxB_7;

public:
	inline static int32_t get_offset_of_mvKHSWNtYxtrXAPVpClHLUktCzV_1() { return static_cast<int32_t>(offsetof(EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5, ___mvKHSWNtYxtrXAPVpClHLUktCzV_1)); }
	inline bool get_mvKHSWNtYxtrXAPVpClHLUktCzV_1() const { return ___mvKHSWNtYxtrXAPVpClHLUktCzV_1; }
	inline bool* get_address_of_mvKHSWNtYxtrXAPVpClHLUktCzV_1() { return &___mvKHSWNtYxtrXAPVpClHLUktCzV_1; }
	inline void set_mvKHSWNtYxtrXAPVpClHLUktCzV_1(bool value)
	{
		___mvKHSWNtYxtrXAPVpClHLUktCzV_1 = value;
	}

	inline static int32_t get_offset_of_KqsZQvXZGdcrJvGsJxuAvfvnhDe_2() { return static_cast<int32_t>(offsetof(EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5, ___KqsZQvXZGdcrJvGsJxuAvfvnhDe_2)); }
	inline bool get_KqsZQvXZGdcrJvGsJxuAvfvnhDe_2() const { return ___KqsZQvXZGdcrJvGsJxuAvfvnhDe_2; }
	inline bool* get_address_of_KqsZQvXZGdcrJvGsJxuAvfvnhDe_2() { return &___KqsZQvXZGdcrJvGsJxuAvfvnhDe_2; }
	inline void set_KqsZQvXZGdcrJvGsJxuAvfvnhDe_2(bool value)
	{
		___KqsZQvXZGdcrJvGsJxuAvfvnhDe_2 = value;
	}

	inline static int32_t get_offset_of_BLWYEEspSiesrpLjzxiQnjWOBUmf_3() { return static_cast<int32_t>(offsetof(EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5, ___BLWYEEspSiesrpLjzxiQnjWOBUmf_3)); }
	inline bool get_BLWYEEspSiesrpLjzxiQnjWOBUmf_3() const { return ___BLWYEEspSiesrpLjzxiQnjWOBUmf_3; }
	inline bool* get_address_of_BLWYEEspSiesrpLjzxiQnjWOBUmf_3() { return &___BLWYEEspSiesrpLjzxiQnjWOBUmf_3; }
	inline void set_BLWYEEspSiesrpLjzxiQnjWOBUmf_3(bool value)
	{
		___BLWYEEspSiesrpLjzxiQnjWOBUmf_3 = value;
	}

	inline static int32_t get_offset_of_ugxQlRwXrGpEMOQPtUvgtSVoUnh_4() { return static_cast<int32_t>(offsetof(EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5, ___ugxQlRwXrGpEMOQPtUvgtSVoUnh_4)); }
	inline float get_ugxQlRwXrGpEMOQPtUvgtSVoUnh_4() const { return ___ugxQlRwXrGpEMOQPtUvgtSVoUnh_4; }
	inline float* get_address_of_ugxQlRwXrGpEMOQPtUvgtSVoUnh_4() { return &___ugxQlRwXrGpEMOQPtUvgtSVoUnh_4; }
	inline void set_ugxQlRwXrGpEMOQPtUvgtSVoUnh_4(float value)
	{
		___ugxQlRwXrGpEMOQPtUvgtSVoUnh_4 = value;
	}

	inline static int32_t get_offset_of_LJwjkYfhxNrwVJAlKTmoasSnlIE_5() { return static_cast<int32_t>(offsetof(EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5, ___LJwjkYfhxNrwVJAlKTmoasSnlIE_5)); }
	inline gbnJVSILPyqAUDavYEfkWfNLGueU5BU5D_tDB430E13551D4D205A0E55D0359868F2D9B2FFDE* get_LJwjkYfhxNrwVJAlKTmoasSnlIE_5() const { return ___LJwjkYfhxNrwVJAlKTmoasSnlIE_5; }
	inline gbnJVSILPyqAUDavYEfkWfNLGueU5BU5D_tDB430E13551D4D205A0E55D0359868F2D9B2FFDE** get_address_of_LJwjkYfhxNrwVJAlKTmoasSnlIE_5() { return &___LJwjkYfhxNrwVJAlKTmoasSnlIE_5; }
	inline void set_LJwjkYfhxNrwVJAlKTmoasSnlIE_5(gbnJVSILPyqAUDavYEfkWfNLGueU5BU5D_tDB430E13551D4D205A0E55D0359868F2D9B2FFDE* value)
	{
		___LJwjkYfhxNrwVJAlKTmoasSnlIE_5 = value;
		Il2CppCodeGenWriteBarrier((&___LJwjkYfhxNrwVJAlKTmoasSnlIE_5), value);
	}

	inline static int32_t get_offset_of_OfxqMMDGWdzhDRrNathCCkjqqeA_6() { return static_cast<int32_t>(offsetof(EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5, ___OfxqMMDGWdzhDRrNathCCkjqqeA_6)); }
	inline bool get_OfxqMMDGWdzhDRrNathCCkjqqeA_6() const { return ___OfxqMMDGWdzhDRrNathCCkjqqeA_6; }
	inline bool* get_address_of_OfxqMMDGWdzhDRrNathCCkjqqeA_6() { return &___OfxqMMDGWdzhDRrNathCCkjqqeA_6; }
	inline void set_OfxqMMDGWdzhDRrNathCCkjqqeA_6(bool value)
	{
		___OfxqMMDGWdzhDRrNathCCkjqqeA_6 = value;
	}

	inline static int32_t get_offset_of_xbzIuakPyoIxhUNJcNokVaqGbxB_7() { return static_cast<int32_t>(offsetof(EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5, ___xbzIuakPyoIxhUNJcNokVaqGbxB_7)); }
	inline bool get_xbzIuakPyoIxhUNJcNokVaqGbxB_7() const { return ___xbzIuakPyoIxhUNJcNokVaqGbxB_7; }
	inline bool* get_address_of_xbzIuakPyoIxhUNJcNokVaqGbxB_7() { return &___xbzIuakPyoIxhUNJcNokVaqGbxB_7; }
	inline void set_xbzIuakPyoIxhUNJcNokVaqGbxB_7(bool value)
	{
		___xbzIuakPyoIxhUNJcNokVaqGbxB_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EHBFMNIGRVEIKVXMJDINIUJSQTAF_TF09D190303034EC83B44DAFBC62D0DCFC736A8B5_H
#ifndef GBNJVSILPYQAUDAVYEFKWFNLGUE_T63790E87CDE9F924E3B5E0A6708EC2E4F2575E24_H
#define GBNJVSILPYQAUDAVYEFKWFNLGUE_T63790E87CDE9F924E3B5E0A6708EC2E4F2575E24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EhbFMnIgrveIkvxMjDinIUjSQTaf_gbnJVSILPyqAUDavYEfkWfNLGue
struct  gbnJVSILPyqAUDavYEfkWfNLGue_t63790E87CDE9F924E3B5E0A6708EC2E4F2575E24  : public RuntimeObject
{
public:
	// System.Boolean EhbFMnIgrveIkvxMjDinIUjSQTaf_gbnJVSILPyqAUDavYEfkWfNLGue::UZbYDMRHqoMYGDwwWIwoKVeBvKy
	bool ___UZbYDMRHqoMYGDwwWIwoKVeBvKy_0;
	// System.Boolean EhbFMnIgrveIkvxMjDinIUjSQTaf_gbnJVSILPyqAUDavYEfkWfNLGue::IyCUGfzcPoSzeXqQnApzwoMSQTs
	bool ___IyCUGfzcPoSzeXqQnApzwoMSQTs_1;
	// System.Single EhbFMnIgrveIkvxMjDinIUjSQTaf_gbnJVSILPyqAUDavYEfkWfNLGue::bAgBXUBUQoTpqsGAsnokCWHlafSk
	float ___bAgBXUBUQoTpqsGAsnokCWHlafSk_2;
	// System.Boolean EhbFMnIgrveIkvxMjDinIUjSQTaf_gbnJVSILPyqAUDavYEfkWfNLGue::nnUyLogROOGBhfASxGsUKPevawbu
	bool ___nnUyLogROOGBhfASxGsUKPevawbu_3;

public:
	inline static int32_t get_offset_of_UZbYDMRHqoMYGDwwWIwoKVeBvKy_0() { return static_cast<int32_t>(offsetof(gbnJVSILPyqAUDavYEfkWfNLGue_t63790E87CDE9F924E3B5E0A6708EC2E4F2575E24, ___UZbYDMRHqoMYGDwwWIwoKVeBvKy_0)); }
	inline bool get_UZbYDMRHqoMYGDwwWIwoKVeBvKy_0() const { return ___UZbYDMRHqoMYGDwwWIwoKVeBvKy_0; }
	inline bool* get_address_of_UZbYDMRHqoMYGDwwWIwoKVeBvKy_0() { return &___UZbYDMRHqoMYGDwwWIwoKVeBvKy_0; }
	inline void set_UZbYDMRHqoMYGDwwWIwoKVeBvKy_0(bool value)
	{
		___UZbYDMRHqoMYGDwwWIwoKVeBvKy_0 = value;
	}

	inline static int32_t get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_1() { return static_cast<int32_t>(offsetof(gbnJVSILPyqAUDavYEfkWfNLGue_t63790E87CDE9F924E3B5E0A6708EC2E4F2575E24, ___IyCUGfzcPoSzeXqQnApzwoMSQTs_1)); }
	inline bool get_IyCUGfzcPoSzeXqQnApzwoMSQTs_1() const { return ___IyCUGfzcPoSzeXqQnApzwoMSQTs_1; }
	inline bool* get_address_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_1() { return &___IyCUGfzcPoSzeXqQnApzwoMSQTs_1; }
	inline void set_IyCUGfzcPoSzeXqQnApzwoMSQTs_1(bool value)
	{
		___IyCUGfzcPoSzeXqQnApzwoMSQTs_1 = value;
	}

	inline static int32_t get_offset_of_bAgBXUBUQoTpqsGAsnokCWHlafSk_2() { return static_cast<int32_t>(offsetof(gbnJVSILPyqAUDavYEfkWfNLGue_t63790E87CDE9F924E3B5E0A6708EC2E4F2575E24, ___bAgBXUBUQoTpqsGAsnokCWHlafSk_2)); }
	inline float get_bAgBXUBUQoTpqsGAsnokCWHlafSk_2() const { return ___bAgBXUBUQoTpqsGAsnokCWHlafSk_2; }
	inline float* get_address_of_bAgBXUBUQoTpqsGAsnokCWHlafSk_2() { return &___bAgBXUBUQoTpqsGAsnokCWHlafSk_2; }
	inline void set_bAgBXUBUQoTpqsGAsnokCWHlafSk_2(float value)
	{
		___bAgBXUBUQoTpqsGAsnokCWHlafSk_2 = value;
	}

	inline static int32_t get_offset_of_nnUyLogROOGBhfASxGsUKPevawbu_3() { return static_cast<int32_t>(offsetof(gbnJVSILPyqAUDavYEfkWfNLGue_t63790E87CDE9F924E3B5E0A6708EC2E4F2575E24, ___nnUyLogROOGBhfASxGsUKPevawbu_3)); }
	inline bool get_nnUyLogROOGBhfASxGsUKPevawbu_3() const { return ___nnUyLogROOGBhfASxGsUKPevawbu_3; }
	inline bool* get_address_of_nnUyLogROOGBhfASxGsUKPevawbu_3() { return &___nnUyLogROOGBhfASxGsUKPevawbu_3; }
	inline void set_nnUyLogROOGBhfASxGsUKPevawbu_3(bool value)
	{
		___nnUyLogROOGBhfASxGsUKPevawbu_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GBNJVSILPYQAUDAVYEFKWFNLGUE_T63790E87CDE9F924E3B5E0A6708EC2E4F2575E24_H
#ifndef GMNQNYIPCHCRVIXASPIKMTNZGBC_T0972372CDB411C81FC52D05D4415722D62DAAF99_H
#define GMNQNYIPCHCRVIXASPIKMTNZGBC_T0972372CDB411C81FC52D05D4415722D62DAAF99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GmNQnYIPchcrVIxASPikMTNzgbC
struct  GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99  : public RuntimeObject
{
public:
	// System.Boolean GmNQnYIPchcrVIxASPikMTNzgbC::UZbYDMRHqoMYGDwwWIwoKVeBvKy
	bool ___UZbYDMRHqoMYGDwwWIwoKVeBvKy_0;
	// Rewired.Controller GmNQnYIPchcrVIxASPikMTNzgbC::QaeAlqSBKVmDRaDTcYzPlckcanC
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___QaeAlqSBKVmDRaDTcYzPlckcanC_1;
	// Rewired.ControllerMap GmNQnYIPchcrVIxASPikMTNzgbC::eDZexKHojMIOIcGgWyZwyijReQJ
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___eDZexKHojMIOIcGgWyZwyijReQJ_2;
	// Rewired.ActionElementMap GmNQnYIPchcrVIxASPikMTNzgbC::CPuSQMXEaFTdKTziCifqLUFakpe
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___CPuSQMXEaFTdKTziCifqLUFakpe_3;

public:
	inline static int32_t get_offset_of_UZbYDMRHqoMYGDwwWIwoKVeBvKy_0() { return static_cast<int32_t>(offsetof(GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99, ___UZbYDMRHqoMYGDwwWIwoKVeBvKy_0)); }
	inline bool get_UZbYDMRHqoMYGDwwWIwoKVeBvKy_0() const { return ___UZbYDMRHqoMYGDwwWIwoKVeBvKy_0; }
	inline bool* get_address_of_UZbYDMRHqoMYGDwwWIwoKVeBvKy_0() { return &___UZbYDMRHqoMYGDwwWIwoKVeBvKy_0; }
	inline void set_UZbYDMRHqoMYGDwwWIwoKVeBvKy_0(bool value)
	{
		___UZbYDMRHqoMYGDwwWIwoKVeBvKy_0 = value;
	}

	inline static int32_t get_offset_of_QaeAlqSBKVmDRaDTcYzPlckcanC_1() { return static_cast<int32_t>(offsetof(GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99, ___QaeAlqSBKVmDRaDTcYzPlckcanC_1)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_QaeAlqSBKVmDRaDTcYzPlckcanC_1() const { return ___QaeAlqSBKVmDRaDTcYzPlckcanC_1; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_QaeAlqSBKVmDRaDTcYzPlckcanC_1() { return &___QaeAlqSBKVmDRaDTcYzPlckcanC_1; }
	inline void set_QaeAlqSBKVmDRaDTcYzPlckcanC_1(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___QaeAlqSBKVmDRaDTcYzPlckcanC_1 = value;
		Il2CppCodeGenWriteBarrier((&___QaeAlqSBKVmDRaDTcYzPlckcanC_1), value);
	}

	inline static int32_t get_offset_of_eDZexKHojMIOIcGgWyZwyijReQJ_2() { return static_cast<int32_t>(offsetof(GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99, ___eDZexKHojMIOIcGgWyZwyijReQJ_2)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_eDZexKHojMIOIcGgWyZwyijReQJ_2() const { return ___eDZexKHojMIOIcGgWyZwyijReQJ_2; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_eDZexKHojMIOIcGgWyZwyijReQJ_2() { return &___eDZexKHojMIOIcGgWyZwyijReQJ_2; }
	inline void set_eDZexKHojMIOIcGgWyZwyijReQJ_2(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___eDZexKHojMIOIcGgWyZwyijReQJ_2 = value;
		Il2CppCodeGenWriteBarrier((&___eDZexKHojMIOIcGgWyZwyijReQJ_2), value);
	}

	inline static int32_t get_offset_of_CPuSQMXEaFTdKTziCifqLUFakpe_3() { return static_cast<int32_t>(offsetof(GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99, ___CPuSQMXEaFTdKTziCifqLUFakpe_3)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_CPuSQMXEaFTdKTziCifqLUFakpe_3() const { return ___CPuSQMXEaFTdKTziCifqLUFakpe_3; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_CPuSQMXEaFTdKTziCifqLUFakpe_3() { return &___CPuSQMXEaFTdKTziCifqLUFakpe_3; }
	inline void set_CPuSQMXEaFTdKTziCifqLUFakpe_3(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___CPuSQMXEaFTdKTziCifqLUFakpe_3 = value;
		Il2CppCodeGenWriteBarrier((&___CPuSQMXEaFTdKTziCifqLUFakpe_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GMNQNYIPCHCRVIXASPIKMTNZGBC_T0972372CDB411C81FC52D05D4415722D62DAAF99_H
#ifndef BUTTONSTATERECORDER_T0FCB8D46E64106B5362E2AE5074A70BEBEC3B148_H
#define BUTTONSTATERECORDER_T0FCB8D46E64106B5362E2AE5074A70BEBEC3B148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ButtonStateRecorder
struct  ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148  : public RuntimeObject
{
public:
	// Rewired.ButtonStateRecorder_hxGGSVmQZTfybORfTgCfaxyzTkP[] Rewired.ButtonStateRecorder::AHOFIsQodiSzMcHMSUpdlsGdKfH
	hxGGSVmQZTfybORfTgCfaxyzTkPU5BU5D_t908A9DCED320AADA7459319C977B2158437BE151* ___AHOFIsQodiSzMcHMSUpdlsGdKfH_1;
	// Rewired.ButtonStateRecorder_hxGGSVmQZTfybORfTgCfaxyzTkP[] Rewired.ButtonStateRecorder::ZzOKRylsDKYMRegaUFScNhcMSfN
	hxGGSVmQZTfybORfTgCfaxyzTkPU5BU5D_t908A9DCED320AADA7459319C977B2158437BE151* ___ZzOKRylsDKYMRegaUFScNhcMSfN_2;
	// System.Int32 Rewired.ButtonStateRecorder::dYjOkhnWbRzPVxoGlildEQfbYqZ
	int32_t ___dYjOkhnWbRzPVxoGlildEQfbYqZ_3;
	// System.Int32 Rewired.ButtonStateRecorder::AvDvBrCrpyFTuiktlFfteeQRFqy
	int32_t ___AvDvBrCrpyFTuiktlFfteeQRFqy_4;
	// System.UInt32 Rewired.ButtonStateRecorder::NWSqJJloZMsgLSarwMePrSnTDOO
	uint32_t ___NWSqJJloZMsgLSarwMePrSnTDOO_5;

public:
	inline static int32_t get_offset_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_1() { return static_cast<int32_t>(offsetof(ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148, ___AHOFIsQodiSzMcHMSUpdlsGdKfH_1)); }
	inline hxGGSVmQZTfybORfTgCfaxyzTkPU5BU5D_t908A9DCED320AADA7459319C977B2158437BE151* get_AHOFIsQodiSzMcHMSUpdlsGdKfH_1() const { return ___AHOFIsQodiSzMcHMSUpdlsGdKfH_1; }
	inline hxGGSVmQZTfybORfTgCfaxyzTkPU5BU5D_t908A9DCED320AADA7459319C977B2158437BE151** get_address_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_1() { return &___AHOFIsQodiSzMcHMSUpdlsGdKfH_1; }
	inline void set_AHOFIsQodiSzMcHMSUpdlsGdKfH_1(hxGGSVmQZTfybORfTgCfaxyzTkPU5BU5D_t908A9DCED320AADA7459319C977B2158437BE151* value)
	{
		___AHOFIsQodiSzMcHMSUpdlsGdKfH_1 = value;
		Il2CppCodeGenWriteBarrier((&___AHOFIsQodiSzMcHMSUpdlsGdKfH_1), value);
	}

	inline static int32_t get_offset_of_ZzOKRylsDKYMRegaUFScNhcMSfN_2() { return static_cast<int32_t>(offsetof(ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148, ___ZzOKRylsDKYMRegaUFScNhcMSfN_2)); }
	inline hxGGSVmQZTfybORfTgCfaxyzTkPU5BU5D_t908A9DCED320AADA7459319C977B2158437BE151* get_ZzOKRylsDKYMRegaUFScNhcMSfN_2() const { return ___ZzOKRylsDKYMRegaUFScNhcMSfN_2; }
	inline hxGGSVmQZTfybORfTgCfaxyzTkPU5BU5D_t908A9DCED320AADA7459319C977B2158437BE151** get_address_of_ZzOKRylsDKYMRegaUFScNhcMSfN_2() { return &___ZzOKRylsDKYMRegaUFScNhcMSfN_2; }
	inline void set_ZzOKRylsDKYMRegaUFScNhcMSfN_2(hxGGSVmQZTfybORfTgCfaxyzTkPU5BU5D_t908A9DCED320AADA7459319C977B2158437BE151* value)
	{
		___ZzOKRylsDKYMRegaUFScNhcMSfN_2 = value;
		Il2CppCodeGenWriteBarrier((&___ZzOKRylsDKYMRegaUFScNhcMSfN_2), value);
	}

	inline static int32_t get_offset_of_dYjOkhnWbRzPVxoGlildEQfbYqZ_3() { return static_cast<int32_t>(offsetof(ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148, ___dYjOkhnWbRzPVxoGlildEQfbYqZ_3)); }
	inline int32_t get_dYjOkhnWbRzPVxoGlildEQfbYqZ_3() const { return ___dYjOkhnWbRzPVxoGlildEQfbYqZ_3; }
	inline int32_t* get_address_of_dYjOkhnWbRzPVxoGlildEQfbYqZ_3() { return &___dYjOkhnWbRzPVxoGlildEQfbYqZ_3; }
	inline void set_dYjOkhnWbRzPVxoGlildEQfbYqZ_3(int32_t value)
	{
		___dYjOkhnWbRzPVxoGlildEQfbYqZ_3 = value;
	}

	inline static int32_t get_offset_of_AvDvBrCrpyFTuiktlFfteeQRFqy_4() { return static_cast<int32_t>(offsetof(ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148, ___AvDvBrCrpyFTuiktlFfteeQRFqy_4)); }
	inline int32_t get_AvDvBrCrpyFTuiktlFfteeQRFqy_4() const { return ___AvDvBrCrpyFTuiktlFfteeQRFqy_4; }
	inline int32_t* get_address_of_AvDvBrCrpyFTuiktlFfteeQRFqy_4() { return &___AvDvBrCrpyFTuiktlFfteeQRFqy_4; }
	inline void set_AvDvBrCrpyFTuiktlFfteeQRFqy_4(int32_t value)
	{
		___AvDvBrCrpyFTuiktlFfteeQRFqy_4 = value;
	}

	inline static int32_t get_offset_of_NWSqJJloZMsgLSarwMePrSnTDOO_5() { return static_cast<int32_t>(offsetof(ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148, ___NWSqJJloZMsgLSarwMePrSnTDOO_5)); }
	inline uint32_t get_NWSqJJloZMsgLSarwMePrSnTDOO_5() const { return ___NWSqJJloZMsgLSarwMePrSnTDOO_5; }
	inline uint32_t* get_address_of_NWSqJJloZMsgLSarwMePrSnTDOO_5() { return &___NWSqJJloZMsgLSarwMePrSnTDOO_5; }
	inline void set_NWSqJJloZMsgLSarwMePrSnTDOO_5(uint32_t value)
	{
		___NWSqJJloZMsgLSarwMePrSnTDOO_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATERECORDER_T0FCB8D46E64106B5362E2AE5074A70BEBEC3B148_H
#ifndef HXGGSVMQZTFYBORFTGCFAXYZTKP_TB58A4CAA84FD83D5A6C3094DBDAD23B9D4A0563F_H
#define HXGGSVMQZTFYBORFTGCFAXYZTKP_TB58A4CAA84FD83D5A6C3094DBDAD23B9D4A0563F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ButtonStateRecorder_hxGGSVmQZTfybORfTgCfaxyzTkP
struct  hxGGSVmQZTfybORfTgCfaxyzTkP_tB58A4CAA84FD83D5A6C3094DBDAD23B9D4A0563F  : public RuntimeObject
{
public:
	// System.Boolean Rewired.ButtonStateRecorder_hxGGSVmQZTfybORfTgCfaxyzTkP::IyCUGfzcPoSzeXqQnApzwoMSQTs
	bool ___IyCUGfzcPoSzeXqQnApzwoMSQTs_0;
	// System.Single Rewired.ButtonStateRecorder_hxGGSVmQZTfybORfTgCfaxyzTkP::bAgBXUBUQoTpqsGAsnokCWHlafSk
	float ___bAgBXUBUQoTpqsGAsnokCWHlafSk_1;

public:
	inline static int32_t get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_0() { return static_cast<int32_t>(offsetof(hxGGSVmQZTfybORfTgCfaxyzTkP_tB58A4CAA84FD83D5A6C3094DBDAD23B9D4A0563F, ___IyCUGfzcPoSzeXqQnApzwoMSQTs_0)); }
	inline bool get_IyCUGfzcPoSzeXqQnApzwoMSQTs_0() const { return ___IyCUGfzcPoSzeXqQnApzwoMSQTs_0; }
	inline bool* get_address_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_0() { return &___IyCUGfzcPoSzeXqQnApzwoMSQTs_0; }
	inline void set_IyCUGfzcPoSzeXqQnApzwoMSQTs_0(bool value)
	{
		___IyCUGfzcPoSzeXqQnApzwoMSQTs_0 = value;
	}

	inline static int32_t get_offset_of_bAgBXUBUQoTpqsGAsnokCWHlafSk_1() { return static_cast<int32_t>(offsetof(hxGGSVmQZTfybORfTgCfaxyzTkP_tB58A4CAA84FD83D5A6C3094DBDAD23B9D4A0563F, ___bAgBXUBUQoTpqsGAsnokCWHlafSk_1)); }
	inline float get_bAgBXUBUQoTpqsGAsnokCWHlafSk_1() const { return ___bAgBXUBUQoTpqsGAsnokCWHlafSk_1; }
	inline float* get_address_of_bAgBXUBUQoTpqsGAsnokCWHlafSk_1() { return &___bAgBXUBUQoTpqsGAsnokCWHlafSk_1; }
	inline void set_bAgBXUBUQoTpqsGAsnokCWHlafSk_1(float value)
	{
		___bAgBXUBUQoTpqsGAsnokCWHlafSk_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HXGGSVMQZTFYBORFTGCFAXYZTKP_TB58A4CAA84FD83D5A6C3094DBDAD23B9D4A0563F_H
#ifndef ENUMCONVERTER_T9250FAC43470C04C58D2E822F917AB20D2E7AAC5_H
#define ENUMCONVERTER_T9250FAC43470C04C58D2E822F917AB20D2E7AAC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.EnumConverter
struct  EnumConverter_t9250FAC43470C04C58D2E822F917AB20D2E7AAC5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMCONVERTER_T9250FAC43470C04C58D2E822F917AB20D2E7AAC5_H
#ifndef LOWLEVELEVENT_T0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC_H
#define LOWLEVELEVENT_T0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.LowLevelEvent
struct  LowLevelEvent_t0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC  : public RuntimeObject
{
public:
	// System.UInt32 Rewired.LowLevelEvent::id
	uint32_t ___id_0;
	// System.Single Rewired.LowLevelEvent::timestamp
	float ___timestamp_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(LowLevelEvent_t0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC, ___id_0)); }
	inline uint32_t get_id_0() const { return ___id_0; }
	inline uint32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(uint32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_timestamp_1() { return static_cast<int32_t>(offsetof(LowLevelEvent_t0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC, ___timestamp_1)); }
	inline float get_timestamp_1() const { return ___timestamp_1; }
	inline float* get_address_of_timestamp_1() { return &___timestamp_1; }
	inline void set_timestamp_1(float value)
	{
		___timestamp_1 = value;
	}
};

struct LowLevelEvent_t0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC_StaticFields
{
public:
	// System.UInt32 Rewired.LowLevelEvent::eKZYZsxlXCNUTCcqXlqIvvDriCI
	uint32_t ___eKZYZsxlXCNUTCcqXlqIvvDriCI_2;

public:
	inline static int32_t get_offset_of_eKZYZsxlXCNUTCcqXlqIvvDriCI_2() { return static_cast<int32_t>(offsetof(LowLevelEvent_t0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC_StaticFields, ___eKZYZsxlXCNUTCcqXlqIvvDriCI_2)); }
	inline uint32_t get_eKZYZsxlXCNUTCcqXlqIvvDriCI_2() const { return ___eKZYZsxlXCNUTCcqXlqIvvDriCI_2; }
	inline uint32_t* get_address_of_eKZYZsxlXCNUTCcqXlqIvvDriCI_2() { return &___eKZYZsxlXCNUTCcqXlqIvvDriCI_2; }
	inline void set_eKZYZsxlXCNUTCcqXlqIvvDriCI_2(uint32_t value)
	{
		___eKZYZsxlXCNUTCcqXlqIvvDriCI_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOWLEVELEVENT_T0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC_H
#ifndef PLATFORMINITIALIZER_T64ECE8EB49A26C8FEF0D737A195DF35E77D74E3A_H
#define PLATFORMINITIALIZER_T64ECE8EB49A26C8FEF0D737A195DF35E77D74E3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlatformInitializer
struct  PlatformInitializer_t64ECE8EB49A26C8FEF0D737A195DF35E77D74E3A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMINITIALIZER_T64ECE8EB49A26C8FEF0D737A195DF35E77D74E3A_H
#ifndef PLATFORMINPUTMANAGER_TF891982F1ED83426874318B2057900C730AAA62A_H
#define PLATFORMINPUTMANAGER_TF891982F1ED83426874318B2057900C730AAA62A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlatformInputManager
struct  PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.BridgedController> Rewired.PlatformInputManager::_DeviceConnectedEvent
	Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5 * ____DeviceConnectedEvent_0;
	// System.Action`1<Rewired.ControllerDisconnectedEventArgs> Rewired.PlatformInputManager::_DeviceDisconnectedEvent
	Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0 * ____DeviceDisconnectedEvent_1;
	// System.Action`1<Rewired.UpdateControllerInfoEventArgs> Rewired.PlatformInputManager::_UpdateControllerInfoEvent
	Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3 * ____UpdateControllerInfoEvent_2;
	// System.Action Rewired.PlatformInputManager::_SystemDeviceConnectedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____SystemDeviceConnectedEvent_3;
	// System.Action Rewired.PlatformInputManager::_SystemDeviceDisconnectedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____SystemDeviceDisconnectedEvent_4;

public:
	inline static int32_t get_offset_of__DeviceConnectedEvent_0() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____DeviceConnectedEvent_0)); }
	inline Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5 * get__DeviceConnectedEvent_0() const { return ____DeviceConnectedEvent_0; }
	inline Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5 ** get_address_of__DeviceConnectedEvent_0() { return &____DeviceConnectedEvent_0; }
	inline void set__DeviceConnectedEvent_0(Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5 * value)
	{
		____DeviceConnectedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____DeviceConnectedEvent_0), value);
	}

	inline static int32_t get_offset_of__DeviceDisconnectedEvent_1() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____DeviceDisconnectedEvent_1)); }
	inline Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0 * get__DeviceDisconnectedEvent_1() const { return ____DeviceDisconnectedEvent_1; }
	inline Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0 ** get_address_of__DeviceDisconnectedEvent_1() { return &____DeviceDisconnectedEvent_1; }
	inline void set__DeviceDisconnectedEvent_1(Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0 * value)
	{
		____DeviceDisconnectedEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&____DeviceDisconnectedEvent_1), value);
	}

	inline static int32_t get_offset_of__UpdateControllerInfoEvent_2() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____UpdateControllerInfoEvent_2)); }
	inline Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3 * get__UpdateControllerInfoEvent_2() const { return ____UpdateControllerInfoEvent_2; }
	inline Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3 ** get_address_of__UpdateControllerInfoEvent_2() { return &____UpdateControllerInfoEvent_2; }
	inline void set__UpdateControllerInfoEvent_2(Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3 * value)
	{
		____UpdateControllerInfoEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateControllerInfoEvent_2), value);
	}

	inline static int32_t get_offset_of__SystemDeviceConnectedEvent_3() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____SystemDeviceConnectedEvent_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__SystemDeviceConnectedEvent_3() const { return ____SystemDeviceConnectedEvent_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__SystemDeviceConnectedEvent_3() { return &____SystemDeviceConnectedEvent_3; }
	inline void set__SystemDeviceConnectedEvent_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____SystemDeviceConnectedEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&____SystemDeviceConnectedEvent_3), value);
	}

	inline static int32_t get_offset_of__SystemDeviceDisconnectedEvent_4() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____SystemDeviceDisconnectedEvent_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__SystemDeviceDisconnectedEvent_4() const { return ____SystemDeviceDisconnectedEvent_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__SystemDeviceDisconnectedEvent_4() { return &____SystemDeviceDisconnectedEvent_4; }
	inline void set__SystemDeviceDisconnectedEvent_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____SystemDeviceDisconnectedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&____SystemDeviceDisconnectedEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMINPUTMANAGER_TF891982F1ED83426874318B2057900C730AAA62A_H
#ifndef PROFILER_T489149C2DA8D0C908FD40198193B90E529F9DFE1_H
#define PROFILER_T489149C2DA8D0C908FD40198193B90E529F9DFE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Profiler
struct  Profiler_t489149C2DA8D0C908FD40198193B90E529F9DFE1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILER_T489149C2DA8D0C908FD40198193B90E529F9DFE1_H
#ifndef STEAMACTION_T767832F58FDB51E2B4366EFA8F0E75F303F334F9_H
#define STEAMACTION_T767832F58FDB51E2B4366EFA8F0E75F303F334F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.SteamAction
struct  SteamAction_t767832F58FDB51E2B4366EFA8F0E75F303F334F9  : public RuntimeObject
{
public:
	// System.String Rewired.SteamAction::name
	String_t* ___name_0;
	// System.UInt64 Rewired.SteamAction::handle
	uint64_t ___handle_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SteamAction_t767832F58FDB51E2B4366EFA8F0E75F303F334F9, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(SteamAction_t767832F58FDB51E2B4366EFA8F0E75F303F334F9, ___handle_1)); }
	inline uint64_t get_handle_1() const { return ___handle_1; }
	inline uint64_t* get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(uint64_t value)
	{
		___handle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEAMACTION_T767832F58FDB51E2B4366EFA8F0E75F303F334F9_H
#ifndef STEAMACTIONSET_T2915FD1D24DF9379EC6DFD4E260EBBB4D0C6751F_H
#define STEAMACTIONSET_T2915FD1D24DF9379EC6DFD4E260EBBB4D0C6751F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.SteamActionSet
struct  SteamActionSet_t2915FD1D24DF9379EC6DFD4E260EBBB4D0C6751F  : public RuntimeObject
{
public:
	// System.String Rewired.SteamActionSet::name
	String_t* ___name_0;
	// System.UInt64 Rewired.SteamActionSet::handle
	uint64_t ___handle_1;
	// System.Collections.Generic.Dictionary`2<System.String,Rewired.SteamAction> Rewired.SteamActionSet::actions
	Dictionary_2_t60634EC784EFFED4A2E1A65F8E4E2C04C45E2278 * ___actions_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SteamActionSet_t2915FD1D24DF9379EC6DFD4E260EBBB4D0C6751F, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(SteamActionSet_t2915FD1D24DF9379EC6DFD4E260EBBB4D0C6751F, ___handle_1)); }
	inline uint64_t get_handle_1() const { return ___handle_1; }
	inline uint64_t* get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(uint64_t value)
	{
		___handle_1 = value;
	}

	inline static int32_t get_offset_of_actions_2() { return static_cast<int32_t>(offsetof(SteamActionSet_t2915FD1D24DF9379EC6DFD4E260EBBB4D0C6751F, ___actions_2)); }
	inline Dictionary_2_t60634EC784EFFED4A2E1A65F8E4E2C04C45E2278 * get_actions_2() const { return ___actions_2; }
	inline Dictionary_2_t60634EC784EFFED4A2E1A65F8E4E2C04C45E2278 ** get_address_of_actions_2() { return &___actions_2; }
	inline void set_actions_2(Dictionary_2_t60634EC784EFFED4A2E1A65F8E4E2C04C45E2278 * value)
	{
		___actions_2 = value;
		Il2CppCodeGenWriteBarrier((&___actions_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEAMACTIONSET_T2915FD1D24DF9379EC6DFD4E260EBBB4D0C6751F_H
#ifndef THREADSAFEUNITYINPUT_T02E9717CF3743E0C27E835225F04C29E37CC93B1_H
#define THREADSAFEUNITYINPUT_T02E9717CF3743E0C27E835225F04C29E37CC93B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ThreadSafeUnityInput
struct  ThreadSafeUnityInput_t02E9717CF3743E0C27E835225F04C29E37CC93B1  : public RuntimeObject
{
public:

public:
};

struct ThreadSafeUnityInput_t02E9717CF3743E0C27E835225F04C29E37CC93B1_StaticFields
{
public:
	// Rewired.ThreadSafeUnityInput_Mouse Rewired.ThreadSafeUnityInput::pUaKhZxxkurJxwkFwfLAthUCtbX
	Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9 * ___pUaKhZxxkurJxwkFwfLAthUCtbX_0;
	// Rewired.ThreadSafeUnityInput_Keyboard Rewired.ThreadSafeUnityInput::SkajjHJRJlbzVUcqDEpAVsLcNRC
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70 * ___SkajjHJRJlbzVUcqDEpAVsLcNRC_1;

public:
	inline static int32_t get_offset_of_pUaKhZxxkurJxwkFwfLAthUCtbX_0() { return static_cast<int32_t>(offsetof(ThreadSafeUnityInput_t02E9717CF3743E0C27E835225F04C29E37CC93B1_StaticFields, ___pUaKhZxxkurJxwkFwfLAthUCtbX_0)); }
	inline Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9 * get_pUaKhZxxkurJxwkFwfLAthUCtbX_0() const { return ___pUaKhZxxkurJxwkFwfLAthUCtbX_0; }
	inline Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9 ** get_address_of_pUaKhZxxkurJxwkFwfLAthUCtbX_0() { return &___pUaKhZxxkurJxwkFwfLAthUCtbX_0; }
	inline void set_pUaKhZxxkurJxwkFwfLAthUCtbX_0(Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9 * value)
	{
		___pUaKhZxxkurJxwkFwfLAthUCtbX_0 = value;
		Il2CppCodeGenWriteBarrier((&___pUaKhZxxkurJxwkFwfLAthUCtbX_0), value);
	}

	inline static int32_t get_offset_of_SkajjHJRJlbzVUcqDEpAVsLcNRC_1() { return static_cast<int32_t>(offsetof(ThreadSafeUnityInput_t02E9717CF3743E0C27E835225F04C29E37CC93B1_StaticFields, ___SkajjHJRJlbzVUcqDEpAVsLcNRC_1)); }
	inline Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70 * get_SkajjHJRJlbzVUcqDEpAVsLcNRC_1() const { return ___SkajjHJRJlbzVUcqDEpAVsLcNRC_1; }
	inline Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70 ** get_address_of_SkajjHJRJlbzVUcqDEpAVsLcNRC_1() { return &___SkajjHJRJlbzVUcqDEpAVsLcNRC_1; }
	inline void set_SkajjHJRJlbzVUcqDEpAVsLcNRC_1(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70 * value)
	{
		___SkajjHJRJlbzVUcqDEpAVsLcNRC_1 = value;
		Il2CppCodeGenWriteBarrier((&___SkajjHJRJlbzVUcqDEpAVsLcNRC_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSAFEUNITYINPUT_T02E9717CF3743E0C27E835225F04C29E37CC93B1_H
#ifndef UNKNOWNCONTROLLERHAT_TFB49467CAB472A384C0BACB9AD9E46D7826B583E_H
#define UNKNOWNCONTROLLERHAT_TFB49467CAB472A384C0BACB9AD9E46D7826B583E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UnknownControllerHat
struct  UnknownControllerHat_tFB49467CAB472A384C0BACB9AD9E46D7826B583E  : public RuntimeObject
{
public:
	// Rewired.UnknownControllerHat_HatButtons Rewired.UnknownControllerHat::jygrDMKtEfPwNxIkfLKMOCmUyrS
	HatButtons_t1754401799C3C4C561D1A750E79B02F35BBFB594 * ___jygrDMKtEfPwNxIkfLKMOCmUyrS_0;

public:
	inline static int32_t get_offset_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_0() { return static_cast<int32_t>(offsetof(UnknownControllerHat_tFB49467CAB472A384C0BACB9AD9E46D7826B583E, ___jygrDMKtEfPwNxIkfLKMOCmUyrS_0)); }
	inline HatButtons_t1754401799C3C4C561D1A750E79B02F35BBFB594 * get_jygrDMKtEfPwNxIkfLKMOCmUyrS_0() const { return ___jygrDMKtEfPwNxIkfLKMOCmUyrS_0; }
	inline HatButtons_t1754401799C3C4C561D1A750E79B02F35BBFB594 ** get_address_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_0() { return &___jygrDMKtEfPwNxIkfLKMOCmUyrS_0; }
	inline void set_jygrDMKtEfPwNxIkfLKMOCmUyrS_0(HatButtons_t1754401799C3C4C561D1A750E79B02F35BBFB594 * value)
	{
		___jygrDMKtEfPwNxIkfLKMOCmUyrS_0 = value;
		Il2CppCodeGenWriteBarrier((&___jygrDMKtEfPwNxIkfLKMOCmUyrS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNKNOWNCONTROLLERHAT_TFB49467CAB472A384C0BACB9AD9E46D7826B583E_H
#ifndef HATBUTTONS_T1754401799C3C4C561D1A750E79B02F35BBFB594_H
#define HATBUTTONS_T1754401799C3C4C561D1A750E79B02F35BBFB594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UnknownControllerHat_HatButtons
struct  HatButtons_t1754401799C3C4C561D1A750E79B02F35BBFB594  : public RuntimeObject
{
public:
	// System.Int32[] Rewired.UnknownControllerHat_HatButtons::jygrDMKtEfPwNxIkfLKMOCmUyrS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___jygrDMKtEfPwNxIkfLKMOCmUyrS_0;

public:
	inline static int32_t get_offset_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_0() { return static_cast<int32_t>(offsetof(HatButtons_t1754401799C3C4C561D1A750E79B02F35BBFB594, ___jygrDMKtEfPwNxIkfLKMOCmUyrS_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_jygrDMKtEfPwNxIkfLKMOCmUyrS_0() const { return ___jygrDMKtEfPwNxIkfLKMOCmUyrS_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_0() { return &___jygrDMKtEfPwNxIkfLKMOCmUyrS_0; }
	inline void set_jygrDMKtEfPwNxIkfLKMOCmUyrS_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___jygrDMKtEfPwNxIkfLKMOCmUyrS_0 = value;
		Il2CppCodeGenWriteBarrier((&___jygrDMKtEfPwNxIkfLKMOCmUyrS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HATBUTTONS_T1754401799C3C4C561D1A750E79B02F35BBFB594_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#define VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.UI.VertexHelperExtension
struct  VertexHelperExtension_t593AF80A941B7208D2CC88AE671873CE6E2AA8E3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#ifndef BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#define BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifndef XRSVVJYCXRJJMMENAYACVMWIRT_T4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3_H
#define XRSVVJYCXRJJMMENAYACVMWIRT_T4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XRsvvJyCXrJjMmEnAyacvMwIrT
struct  XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3  : public RuntimeObject
{
public:
	// System.Single XRsvvJyCXrJjMmEnAyacvMwIrT::jUSDSUlgfBsSujTyNlnzLqBbFdw
	float ___jUSDSUlgfBsSujTyNlnzLqBbFdw_0;
	// System.Single XRsvvJyCXrJjMmEnAyacvMwIrT::dgLozgQVKHMtlxHeFMWimlOECpV
	float ___dgLozgQVKHMtlxHeFMWimlOECpV_1;
	// System.Single XRsvvJyCXrJjMmEnAyacvMwIrT::QhSKLZxOqGNPKRxKaNHfSppwcbn
	float ___QhSKLZxOqGNPKRxKaNHfSppwcbn_2;
	// System.Boolean XRsvvJyCXrJjMmEnAyacvMwIrT::RCEncWjZiMinThJdsjjbhSaYuNs
	bool ___RCEncWjZiMinThJdsjjbhSaYuNs_3;
	// System.Single XRsvvJyCXrJjMmEnAyacvMwIrT::YKgbhstxodGBLEVVbsiCVCSOOEoT
	float ___YKgbhstxodGBLEVVbsiCVCSOOEoT_4;
	// System.Single XRsvvJyCXrJjMmEnAyacvMwIrT::YHRBQXwgXlINPMUVNgZquixRWBi
	float ___YHRBQXwgXlINPMUVNgZquixRWBi_5;
	// System.Boolean XRsvvJyCXrJjMmEnAyacvMwIrT::axNahZYdpeLFpPhlwXuRRzqrbYr
	bool ___axNahZYdpeLFpPhlwXuRRzqrbYr_6;
	// System.Boolean XRsvvJyCXrJjMmEnAyacvMwIrT::xrZiXLzZxQBrLZscKvoqHlIUAaRa
	bool ___xrZiXLzZxQBrLZscKvoqHlIUAaRa_7;

public:
	inline static int32_t get_offset_of_jUSDSUlgfBsSujTyNlnzLqBbFdw_0() { return static_cast<int32_t>(offsetof(XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3, ___jUSDSUlgfBsSujTyNlnzLqBbFdw_0)); }
	inline float get_jUSDSUlgfBsSujTyNlnzLqBbFdw_0() const { return ___jUSDSUlgfBsSujTyNlnzLqBbFdw_0; }
	inline float* get_address_of_jUSDSUlgfBsSujTyNlnzLqBbFdw_0() { return &___jUSDSUlgfBsSujTyNlnzLqBbFdw_0; }
	inline void set_jUSDSUlgfBsSujTyNlnzLqBbFdw_0(float value)
	{
		___jUSDSUlgfBsSujTyNlnzLqBbFdw_0 = value;
	}

	inline static int32_t get_offset_of_dgLozgQVKHMtlxHeFMWimlOECpV_1() { return static_cast<int32_t>(offsetof(XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3, ___dgLozgQVKHMtlxHeFMWimlOECpV_1)); }
	inline float get_dgLozgQVKHMtlxHeFMWimlOECpV_1() const { return ___dgLozgQVKHMtlxHeFMWimlOECpV_1; }
	inline float* get_address_of_dgLozgQVKHMtlxHeFMWimlOECpV_1() { return &___dgLozgQVKHMtlxHeFMWimlOECpV_1; }
	inline void set_dgLozgQVKHMtlxHeFMWimlOECpV_1(float value)
	{
		___dgLozgQVKHMtlxHeFMWimlOECpV_1 = value;
	}

	inline static int32_t get_offset_of_QhSKLZxOqGNPKRxKaNHfSppwcbn_2() { return static_cast<int32_t>(offsetof(XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3, ___QhSKLZxOqGNPKRxKaNHfSppwcbn_2)); }
	inline float get_QhSKLZxOqGNPKRxKaNHfSppwcbn_2() const { return ___QhSKLZxOqGNPKRxKaNHfSppwcbn_2; }
	inline float* get_address_of_QhSKLZxOqGNPKRxKaNHfSppwcbn_2() { return &___QhSKLZxOqGNPKRxKaNHfSppwcbn_2; }
	inline void set_QhSKLZxOqGNPKRxKaNHfSppwcbn_2(float value)
	{
		___QhSKLZxOqGNPKRxKaNHfSppwcbn_2 = value;
	}

	inline static int32_t get_offset_of_RCEncWjZiMinThJdsjjbhSaYuNs_3() { return static_cast<int32_t>(offsetof(XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3, ___RCEncWjZiMinThJdsjjbhSaYuNs_3)); }
	inline bool get_RCEncWjZiMinThJdsjjbhSaYuNs_3() const { return ___RCEncWjZiMinThJdsjjbhSaYuNs_3; }
	inline bool* get_address_of_RCEncWjZiMinThJdsjjbhSaYuNs_3() { return &___RCEncWjZiMinThJdsjjbhSaYuNs_3; }
	inline void set_RCEncWjZiMinThJdsjjbhSaYuNs_3(bool value)
	{
		___RCEncWjZiMinThJdsjjbhSaYuNs_3 = value;
	}

	inline static int32_t get_offset_of_YKgbhstxodGBLEVVbsiCVCSOOEoT_4() { return static_cast<int32_t>(offsetof(XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3, ___YKgbhstxodGBLEVVbsiCVCSOOEoT_4)); }
	inline float get_YKgbhstxodGBLEVVbsiCVCSOOEoT_4() const { return ___YKgbhstxodGBLEVVbsiCVCSOOEoT_4; }
	inline float* get_address_of_YKgbhstxodGBLEVVbsiCVCSOOEoT_4() { return &___YKgbhstxodGBLEVVbsiCVCSOOEoT_4; }
	inline void set_YKgbhstxodGBLEVVbsiCVCSOOEoT_4(float value)
	{
		___YKgbhstxodGBLEVVbsiCVCSOOEoT_4 = value;
	}

	inline static int32_t get_offset_of_YHRBQXwgXlINPMUVNgZquixRWBi_5() { return static_cast<int32_t>(offsetof(XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3, ___YHRBQXwgXlINPMUVNgZquixRWBi_5)); }
	inline float get_YHRBQXwgXlINPMUVNgZquixRWBi_5() const { return ___YHRBQXwgXlINPMUVNgZquixRWBi_5; }
	inline float* get_address_of_YHRBQXwgXlINPMUVNgZquixRWBi_5() { return &___YHRBQXwgXlINPMUVNgZquixRWBi_5; }
	inline void set_YHRBQXwgXlINPMUVNgZquixRWBi_5(float value)
	{
		___YHRBQXwgXlINPMUVNgZquixRWBi_5 = value;
	}

	inline static int32_t get_offset_of_axNahZYdpeLFpPhlwXuRRzqrbYr_6() { return static_cast<int32_t>(offsetof(XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3, ___axNahZYdpeLFpPhlwXuRRzqrbYr_6)); }
	inline bool get_axNahZYdpeLFpPhlwXuRRzqrbYr_6() const { return ___axNahZYdpeLFpPhlwXuRRzqrbYr_6; }
	inline bool* get_address_of_axNahZYdpeLFpPhlwXuRRzqrbYr_6() { return &___axNahZYdpeLFpPhlwXuRRzqrbYr_6; }
	inline void set_axNahZYdpeLFpPhlwXuRRzqrbYr_6(bool value)
	{
		___axNahZYdpeLFpPhlwXuRRzqrbYr_6 = value;
	}

	inline static int32_t get_offset_of_xrZiXLzZxQBrLZscKvoqHlIUAaRa_7() { return static_cast<int32_t>(offsetof(XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3, ___xrZiXLzZxQBrLZscKvoqHlIUAaRa_7)); }
	inline bool get_xrZiXLzZxQBrLZscKvoqHlIUAaRa_7() const { return ___xrZiXLzZxQBrLZscKvoqHlIUAaRa_7; }
	inline bool* get_address_of_xrZiXLzZxQBrLZscKvoqHlIUAaRa_7() { return &___xrZiXLzZxQBrLZscKvoqHlIUAaRa_7; }
	inline void set_xrZiXLzZxQBrLZscKvoqHlIUAaRa_7(bool value)
	{
		___xrZiXLzZxQBrLZscKvoqHlIUAaRa_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRSVVJYCXRJJMMENAYACVMWIRT_T4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3_H
#ifndef INMAHOSEGUCQVFXMGFASDFHFDNR_TCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C_H
#define INMAHOSEGUCQVFXMGFASDFHFDNR_TCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iNmAhOSEGUcqVFxmGFasdFHfdnr
struct  iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C  : public RuntimeObject
{
public:
	// System.Boolean iNmAhOSEGUcqVFxmGFasdFHfdnr::ffNWJUuxPxuFMIZszOmCdhFAkUp
	bool ___ffNWJUuxPxuFMIZszOmCdhFAkUp_0;
	// Rewired.Utils.Classes.Data.AList`1<iNmAhOSEGUcqVFxmGFasdFHfdnr_lcCLwaBQyqptCsvRVWlzpdLzWrX>[] iNmAhOSEGUcqVFxmGFasdFHfdnr::gfxPfklYaqjBddECvDavUajksOb
	AList_1U5BU5D_t87FEEAF55FE2B1BDD2D7A3F554ACBB8C174D2A92* ___gfxPfklYaqjBddECvDavUajksOb_1;
	// System.Int32[] iNmAhOSEGUcqVFxmGFasdFHfdnr::rDzsPJfHrwraGtPiYaOBvJhwBKR
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___rDzsPJfHrwraGtPiYaOBvJhwBKR_2;
	// System.Int32 iNmAhOSEGUcqVFxmGFasdFHfdnr::VfgWBBRhJvXUrlfIjIDaVDbCzHO
	int32_t ___VfgWBBRhJvXUrlfIjIDaVDbCzHO_3;
	// System.Int32 iNmAhOSEGUcqVFxmGFasdFHfdnr::DlroOwqWNriauwRoYVGSrdUcMeH
	int32_t ___DlroOwqWNriauwRoYVGSrdUcMeH_4;

public:
	inline static int32_t get_offset_of_ffNWJUuxPxuFMIZszOmCdhFAkUp_0() { return static_cast<int32_t>(offsetof(iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C, ___ffNWJUuxPxuFMIZszOmCdhFAkUp_0)); }
	inline bool get_ffNWJUuxPxuFMIZszOmCdhFAkUp_0() const { return ___ffNWJUuxPxuFMIZszOmCdhFAkUp_0; }
	inline bool* get_address_of_ffNWJUuxPxuFMIZszOmCdhFAkUp_0() { return &___ffNWJUuxPxuFMIZszOmCdhFAkUp_0; }
	inline void set_ffNWJUuxPxuFMIZszOmCdhFAkUp_0(bool value)
	{
		___ffNWJUuxPxuFMIZszOmCdhFAkUp_0 = value;
	}

	inline static int32_t get_offset_of_gfxPfklYaqjBddECvDavUajksOb_1() { return static_cast<int32_t>(offsetof(iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C, ___gfxPfklYaqjBddECvDavUajksOb_1)); }
	inline AList_1U5BU5D_t87FEEAF55FE2B1BDD2D7A3F554ACBB8C174D2A92* get_gfxPfklYaqjBddECvDavUajksOb_1() const { return ___gfxPfklYaqjBddECvDavUajksOb_1; }
	inline AList_1U5BU5D_t87FEEAF55FE2B1BDD2D7A3F554ACBB8C174D2A92** get_address_of_gfxPfklYaqjBddECvDavUajksOb_1() { return &___gfxPfklYaqjBddECvDavUajksOb_1; }
	inline void set_gfxPfklYaqjBddECvDavUajksOb_1(AList_1U5BU5D_t87FEEAF55FE2B1BDD2D7A3F554ACBB8C174D2A92* value)
	{
		___gfxPfklYaqjBddECvDavUajksOb_1 = value;
		Il2CppCodeGenWriteBarrier((&___gfxPfklYaqjBddECvDavUajksOb_1), value);
	}

	inline static int32_t get_offset_of_rDzsPJfHrwraGtPiYaOBvJhwBKR_2() { return static_cast<int32_t>(offsetof(iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C, ___rDzsPJfHrwraGtPiYaOBvJhwBKR_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_rDzsPJfHrwraGtPiYaOBvJhwBKR_2() const { return ___rDzsPJfHrwraGtPiYaOBvJhwBKR_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_rDzsPJfHrwraGtPiYaOBvJhwBKR_2() { return &___rDzsPJfHrwraGtPiYaOBvJhwBKR_2; }
	inline void set_rDzsPJfHrwraGtPiYaOBvJhwBKR_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___rDzsPJfHrwraGtPiYaOBvJhwBKR_2 = value;
		Il2CppCodeGenWriteBarrier((&___rDzsPJfHrwraGtPiYaOBvJhwBKR_2), value);
	}

	inline static int32_t get_offset_of_VfgWBBRhJvXUrlfIjIDaVDbCzHO_3() { return static_cast<int32_t>(offsetof(iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C, ___VfgWBBRhJvXUrlfIjIDaVDbCzHO_3)); }
	inline int32_t get_VfgWBBRhJvXUrlfIjIDaVDbCzHO_3() const { return ___VfgWBBRhJvXUrlfIjIDaVDbCzHO_3; }
	inline int32_t* get_address_of_VfgWBBRhJvXUrlfIjIDaVDbCzHO_3() { return &___VfgWBBRhJvXUrlfIjIDaVDbCzHO_3; }
	inline void set_VfgWBBRhJvXUrlfIjIDaVDbCzHO_3(int32_t value)
	{
		___VfgWBBRhJvXUrlfIjIDaVDbCzHO_3 = value;
	}

	inline static int32_t get_offset_of_DlroOwqWNriauwRoYVGSrdUcMeH_4() { return static_cast<int32_t>(offsetof(iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C, ___DlroOwqWNriauwRoYVGSrdUcMeH_4)); }
	inline int32_t get_DlroOwqWNriauwRoYVGSrdUcMeH_4() const { return ___DlroOwqWNriauwRoYVGSrdUcMeH_4; }
	inline int32_t* get_address_of_DlroOwqWNriauwRoYVGSrdUcMeH_4() { return &___DlroOwqWNriauwRoYVGSrdUcMeH_4; }
	inline void set_DlroOwqWNriauwRoYVGSrdUcMeH_4(int32_t value)
	{
		___DlroOwqWNriauwRoYVGSrdUcMeH_4 = value;
	}
};

struct iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C_StaticFields
{
public:
	// System.Func`1<Rewired.Utils.Classes.Data.AList`1<iNmAhOSEGUcqVFxmGFasdFHfdnr_lcCLwaBQyqptCsvRVWlzpdLzWrX>> iNmAhOSEGUcqVFxmGFasdFHfdnr::wKZsRNpOhQctCfJnvfAdTetobSIx
	Func_1_tE1ABE1F3BDE308A255EFC632258CA4A114FEC717 * ___wKZsRNpOhQctCfJnvfAdTetobSIx_5;

public:
	inline static int32_t get_offset_of_wKZsRNpOhQctCfJnvfAdTetobSIx_5() { return static_cast<int32_t>(offsetof(iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C_StaticFields, ___wKZsRNpOhQctCfJnvfAdTetobSIx_5)); }
	inline Func_1_tE1ABE1F3BDE308A255EFC632258CA4A114FEC717 * get_wKZsRNpOhQctCfJnvfAdTetobSIx_5() const { return ___wKZsRNpOhQctCfJnvfAdTetobSIx_5; }
	inline Func_1_tE1ABE1F3BDE308A255EFC632258CA4A114FEC717 ** get_address_of_wKZsRNpOhQctCfJnvfAdTetobSIx_5() { return &___wKZsRNpOhQctCfJnvfAdTetobSIx_5; }
	inline void set_wKZsRNpOhQctCfJnvfAdTetobSIx_5(Func_1_tE1ABE1F3BDE308A255EFC632258CA4A114FEC717 * value)
	{
		___wKZsRNpOhQctCfJnvfAdTetobSIx_5 = value;
		Il2CppCodeGenWriteBarrier((&___wKZsRNpOhQctCfJnvfAdTetobSIx_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INMAHOSEGUCQVFXMGFASDFHFDNR_TCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C_H
#ifndef SIZIMZSFSDGOAXCQQKCEYTCOVSU_TF2A5904AE6368332273AEA1389945FFD00F0FCF9_H
#define SIZIMZSFSDGOAXCQQKCEYTCOVSU_TF2A5904AE6368332273AEA1389945FFD00F0FCF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iNmAhOSEGUcqVFxmGFasdFHfdnr_sIziMzSfsDgoaxCQQkceytcOvsu
struct  sIziMzSfsDgoaxCQQkceytcOvsu_tF2A5904AE6368332273AEA1389945FFD00F0FCF9  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.InputActionEventData> iNmAhOSEGUcqVFxmGFasdFHfdnr_sIziMzSfsDgoaxCQQkceytcOvsu::ekrwGUjbOGqAHCaagByDbWSadpQb
	Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * ___ekrwGUjbOGqAHCaagByDbWSadpQb_0;
	// System.Int32 iNmAhOSEGUcqVFxmGFasdFHfdnr_sIziMzSfsDgoaxCQQkceytcOvsu::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_1;

public:
	inline static int32_t get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return static_cast<int32_t>(offsetof(sIziMzSfsDgoaxCQQkceytcOvsu_tF2A5904AE6368332273AEA1389945FFD00F0FCF9, ___ekrwGUjbOGqAHCaagByDbWSadpQb_0)); }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * get_ekrwGUjbOGqAHCaagByDbWSadpQb_0() const { return ___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 ** get_address_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return &___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline void set_ekrwGUjbOGqAHCaagByDbWSadpQb_0(Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * value)
	{
		___ekrwGUjbOGqAHCaagByDbWSadpQb_0 = value;
		Il2CppCodeGenWriteBarrier((&___ekrwGUjbOGqAHCaagByDbWSadpQb_0), value);
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_1() { return static_cast<int32_t>(offsetof(sIziMzSfsDgoaxCQQkceytcOvsu_tF2A5904AE6368332273AEA1389945FFD00F0FCF9, ___pTVJDeVeJlQThAZkFztHqDAWDUa_1)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_1() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_1; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_1() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_1; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_1(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZIMZSFSDGOAXCQQKCEYTCOVSU_TF2A5904AE6368332273AEA1389945FFD00F0FCF9_H
#ifndef XNALZOOCRTHKPFEUVFRXBNCCDVS_T076F5D3CDBCCFE802032448DDB5F47D9DA960E34_H
#define XNALZOOCRTHKPFEUVFRXBNCCDVS_T076F5D3CDBCCFE802032448DDB5F47D9DA960E34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iNmAhOSEGUcqVFxmGFasdFHfdnr_xnALzOoCrTHKpfeuVfrXbNCCdvs
struct  xnALzOoCrTHKpfeuVfrXbNCCdvs_t076F5D3CDBCCFE802032448DDB5F47D9DA960E34  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.InputActionEventData> iNmAhOSEGUcqVFxmGFasdFHfdnr_xnALzOoCrTHKpfeuVfrXbNCCdvs::ekrwGUjbOGqAHCaagByDbWSadpQb
	Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * ___ekrwGUjbOGqAHCaagByDbWSadpQb_0;

public:
	inline static int32_t get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return static_cast<int32_t>(offsetof(xnALzOoCrTHKpfeuVfrXbNCCdvs_t076F5D3CDBCCFE802032448DDB5F47D9DA960E34, ___ekrwGUjbOGqAHCaagByDbWSadpQb_0)); }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * get_ekrwGUjbOGqAHCaagByDbWSadpQb_0() const { return ___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 ** get_address_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return &___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline void set_ekrwGUjbOGqAHCaagByDbWSadpQb_0(Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * value)
	{
		___ekrwGUjbOGqAHCaagByDbWSadpQb_0 = value;
		Il2CppCodeGenWriteBarrier((&___ekrwGUjbOGqAHCaagByDbWSadpQb_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XNALZOOCRTHKPFEUVFRXBNCCDVS_T076F5D3CDBCCFE802032448DDB5F47D9DA960E34_H
#ifndef RICVLCWCUQGHGAAIOFVKJSVNDJX_T194A11E798E6C6497FA7FF7267CD63127C961224_H
#define RICVLCWCUQGHGAAIOFVKJSVNDJX_T194A11E798E6C6497FA7FF7267CD63127C961224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// rIcvLcWCuQghgaaiofvKjSvndJX
struct  rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224  : public RuntimeObject
{
public:
	// Rewired.Utils.Classes.Data.AList`1<rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty> rIcvLcWCuQghgaaiofvKjSvndJX::iwVNvyACihThlfmpkfURWPSdJYs
	AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * ___iwVNvyACihThlfmpkfURWPSdJYs_0;
	// System.Type[] rIcvLcWCuQghgaaiofvKjSvndJX::BnoetkxVNQEHfZTFFfghcpdJFzb
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___BnoetkxVNQEHfZTFFfghcpdJFzb_1;
	// System.Type[] rIcvLcWCuQghgaaiofvKjSvndJX::biNwZqyjNTmtSrqBtAcTREmMQcl
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___biNwZqyjNTmtSrqBtAcTREmMQcl_2;
	// System.Int32 rIcvLcWCuQghgaaiofvKjSvndJX::BtGnDdGQrTXaaWswowVwZiAMcDv
	int32_t ___BtGnDdGQrTXaaWswowVwZiAMcDv_3;

public:
	inline static int32_t get_offset_of_iwVNvyACihThlfmpkfURWPSdJYs_0() { return static_cast<int32_t>(offsetof(rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224, ___iwVNvyACihThlfmpkfURWPSdJYs_0)); }
	inline AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * get_iwVNvyACihThlfmpkfURWPSdJYs_0() const { return ___iwVNvyACihThlfmpkfURWPSdJYs_0; }
	inline AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 ** get_address_of_iwVNvyACihThlfmpkfURWPSdJYs_0() { return &___iwVNvyACihThlfmpkfURWPSdJYs_0; }
	inline void set_iwVNvyACihThlfmpkfURWPSdJYs_0(AList_1_tE4F325D076893D5254B2429A71BC0DDFBD65DC99 * value)
	{
		___iwVNvyACihThlfmpkfURWPSdJYs_0 = value;
		Il2CppCodeGenWriteBarrier((&___iwVNvyACihThlfmpkfURWPSdJYs_0), value);
	}

	inline static int32_t get_offset_of_BnoetkxVNQEHfZTFFfghcpdJFzb_1() { return static_cast<int32_t>(offsetof(rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224, ___BnoetkxVNQEHfZTFFfghcpdJFzb_1)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_BnoetkxVNQEHfZTFFfghcpdJFzb_1() const { return ___BnoetkxVNQEHfZTFFfghcpdJFzb_1; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_BnoetkxVNQEHfZTFFfghcpdJFzb_1() { return &___BnoetkxVNQEHfZTFFfghcpdJFzb_1; }
	inline void set_BnoetkxVNQEHfZTFFfghcpdJFzb_1(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___BnoetkxVNQEHfZTFFfghcpdJFzb_1 = value;
		Il2CppCodeGenWriteBarrier((&___BnoetkxVNQEHfZTFFfghcpdJFzb_1), value);
	}

	inline static int32_t get_offset_of_biNwZqyjNTmtSrqBtAcTREmMQcl_2() { return static_cast<int32_t>(offsetof(rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224, ___biNwZqyjNTmtSrqBtAcTREmMQcl_2)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_biNwZqyjNTmtSrqBtAcTREmMQcl_2() const { return ___biNwZqyjNTmtSrqBtAcTREmMQcl_2; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_biNwZqyjNTmtSrqBtAcTREmMQcl_2() { return &___biNwZqyjNTmtSrqBtAcTREmMQcl_2; }
	inline void set_biNwZqyjNTmtSrqBtAcTREmMQcl_2(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___biNwZqyjNTmtSrqBtAcTREmMQcl_2 = value;
		Il2CppCodeGenWriteBarrier((&___biNwZqyjNTmtSrqBtAcTREmMQcl_2), value);
	}

	inline static int32_t get_offset_of_BtGnDdGQrTXaaWswowVwZiAMcDv_3() { return static_cast<int32_t>(offsetof(rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224, ___BtGnDdGQrTXaaWswowVwZiAMcDv_3)); }
	inline int32_t get_BtGnDdGQrTXaaWswowVwZiAMcDv_3() const { return ___BtGnDdGQrTXaaWswowVwZiAMcDv_3; }
	inline int32_t* get_address_of_BtGnDdGQrTXaaWswowVwZiAMcDv_3() { return &___BtGnDdGQrTXaaWswowVwZiAMcDv_3; }
	inline void set_BtGnDdGQrTXaaWswowVwZiAMcDv_3(int32_t value)
	{
		___BtGnDdGQrTXaaWswowVwZiAMcDv_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RICVLCWCUQGHGAAIOFVKJSVNDJX_T194A11E798E6C6497FA7FF7267CD63127C961224_H
#ifndef MGZGUQDUJPVKVEHSDVIRYOHLOTY_T6A4533BF4F06562072BCC6830BB48BBDBE2F8820_H
#define MGZGUQDUJPVKVEHSDVIRYOHLOTY_T6A4533BF4F06562072BCC6830BB48BBDBE2F8820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty
struct  mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820  : public RuntimeObject
{
public:
	// Rewired.Utils.Classes.Data.AList`1<Rewired.IControllerTemplate> rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty::zGAzEsGrTTTqWZqBgkORnHasWbZ
	AList_1_t974A66EA135B0300CE05935362C6C7C736059320 * ___zGAzEsGrTTTqWZqBgkORnHasWbZ_0;
	// System.Collections.IList rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty::HEgvcyfmgQRVtohyvBGRboKAUBC
	RuntimeObject* ___HEgvcyfmgQRVtohyvBGRboKAUBC_1;
	// System.Collections.IList rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty::gbMrYrhHSKKiezarhnNWxHrOFkF
	RuntimeObject* ___gbMrYrhHSKKiezarhnNWxHrOFkF_2;
	// System.Type rIcvLcWCuQghgaaiofvKjSvndJX_mGZguqdUJPvKvEhsdVIRYohlOty::umQghCmsfCFpiiISpgIfIkmOcxB
	Type_t * ___umQghCmsfCFpiiISpgIfIkmOcxB_3;

public:
	inline static int32_t get_offset_of_zGAzEsGrTTTqWZqBgkORnHasWbZ_0() { return static_cast<int32_t>(offsetof(mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820, ___zGAzEsGrTTTqWZqBgkORnHasWbZ_0)); }
	inline AList_1_t974A66EA135B0300CE05935362C6C7C736059320 * get_zGAzEsGrTTTqWZqBgkORnHasWbZ_0() const { return ___zGAzEsGrTTTqWZqBgkORnHasWbZ_0; }
	inline AList_1_t974A66EA135B0300CE05935362C6C7C736059320 ** get_address_of_zGAzEsGrTTTqWZqBgkORnHasWbZ_0() { return &___zGAzEsGrTTTqWZqBgkORnHasWbZ_0; }
	inline void set_zGAzEsGrTTTqWZqBgkORnHasWbZ_0(AList_1_t974A66EA135B0300CE05935362C6C7C736059320 * value)
	{
		___zGAzEsGrTTTqWZqBgkORnHasWbZ_0 = value;
		Il2CppCodeGenWriteBarrier((&___zGAzEsGrTTTqWZqBgkORnHasWbZ_0), value);
	}

	inline static int32_t get_offset_of_HEgvcyfmgQRVtohyvBGRboKAUBC_1() { return static_cast<int32_t>(offsetof(mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820, ___HEgvcyfmgQRVtohyvBGRboKAUBC_1)); }
	inline RuntimeObject* get_HEgvcyfmgQRVtohyvBGRboKAUBC_1() const { return ___HEgvcyfmgQRVtohyvBGRboKAUBC_1; }
	inline RuntimeObject** get_address_of_HEgvcyfmgQRVtohyvBGRboKAUBC_1() { return &___HEgvcyfmgQRVtohyvBGRboKAUBC_1; }
	inline void set_HEgvcyfmgQRVtohyvBGRboKAUBC_1(RuntimeObject* value)
	{
		___HEgvcyfmgQRVtohyvBGRboKAUBC_1 = value;
		Il2CppCodeGenWriteBarrier((&___HEgvcyfmgQRVtohyvBGRboKAUBC_1), value);
	}

	inline static int32_t get_offset_of_gbMrYrhHSKKiezarhnNWxHrOFkF_2() { return static_cast<int32_t>(offsetof(mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820, ___gbMrYrhHSKKiezarhnNWxHrOFkF_2)); }
	inline RuntimeObject* get_gbMrYrhHSKKiezarhnNWxHrOFkF_2() const { return ___gbMrYrhHSKKiezarhnNWxHrOFkF_2; }
	inline RuntimeObject** get_address_of_gbMrYrhHSKKiezarhnNWxHrOFkF_2() { return &___gbMrYrhHSKKiezarhnNWxHrOFkF_2; }
	inline void set_gbMrYrhHSKKiezarhnNWxHrOFkF_2(RuntimeObject* value)
	{
		___gbMrYrhHSKKiezarhnNWxHrOFkF_2 = value;
		Il2CppCodeGenWriteBarrier((&___gbMrYrhHSKKiezarhnNWxHrOFkF_2), value);
	}

	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_3() { return static_cast<int32_t>(offsetof(mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820, ___umQghCmsfCFpiiISpgIfIkmOcxB_3)); }
	inline Type_t * get_umQghCmsfCFpiiISpgIfIkmOcxB_3() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_3; }
	inline Type_t ** get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_3() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_3; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_3(Type_t * value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_3 = value;
		Il2CppCodeGenWriteBarrier((&___umQghCmsfCFpiiISpgIfIkmOcxB_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MGZGUQDUJPVKVEHSDVIRYOHLOTY_T6A4533BF4F06562072BCC6830BB48BBDBE2F8820_H
#ifndef U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#define U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifndef ACTIONIDFIELDINFOATTRIBUTE_T84C4D28B5D1C76068E9FD5F9FB2028098E26310B_H
#define ACTIONIDFIELDINFOATTRIBUTE_T84C4D28B5D1C76068E9FD5F9FB2028098E26310B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.ActionIdFieldInfoAttribute
struct  ActionIdFieldInfoAttribute_t84C4D28B5D1C76068E9FD5F9FB2028098E26310B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Rewired.Dev.ActionIdFieldInfoAttribute::categoryName
	String_t* ___categoryName_0;
	// System.String Rewired.Dev.ActionIdFieldInfoAttribute::friendlyName
	String_t* ___friendlyName_1;

public:
	inline static int32_t get_offset_of_categoryName_0() { return static_cast<int32_t>(offsetof(ActionIdFieldInfoAttribute_t84C4D28B5D1C76068E9FD5F9FB2028098E26310B, ___categoryName_0)); }
	inline String_t* get_categoryName_0() const { return ___categoryName_0; }
	inline String_t** get_address_of_categoryName_0() { return &___categoryName_0; }
	inline void set_categoryName_0(String_t* value)
	{
		___categoryName_0 = value;
		Il2CppCodeGenWriteBarrier((&___categoryName_0), value);
	}

	inline static int32_t get_offset_of_friendlyName_1() { return static_cast<int32_t>(offsetof(ActionIdFieldInfoAttribute_t84C4D28B5D1C76068E9FD5F9FB2028098E26310B, ___friendlyName_1)); }
	inline String_t* get_friendlyName_1() const { return ___friendlyName_1; }
	inline String_t** get_address_of_friendlyName_1() { return &___friendlyName_1; }
	inline void set_friendlyName_1(String_t* value)
	{
		___friendlyName_1 = value;
		Il2CppCodeGenWriteBarrier((&___friendlyName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONIDFIELDINFOATTRIBUTE_T84C4D28B5D1C76068E9FD5F9FB2028098E26310B_H
#ifndef PLAYERIDFIELDINFOATTRIBUTE_T96E234F121A683A77A9F56CFDF9BB1B819CF1B3D_H
#define PLAYERIDFIELDINFOATTRIBUTE_T96E234F121A683A77A9F56CFDF9BB1B819CF1B3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.PlayerIdFieldInfoAttribute
struct  PlayerIdFieldInfoAttribute_t96E234F121A683A77A9F56CFDF9BB1B819CF1B3D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Rewired.Dev.PlayerIdFieldInfoAttribute::friendlyName
	String_t* ___friendlyName_0;

public:
	inline static int32_t get_offset_of_friendlyName_0() { return static_cast<int32_t>(offsetof(PlayerIdFieldInfoAttribute_t96E234F121A683A77A9F56CFDF9BB1B819CF1B3D, ___friendlyName_0)); }
	inline String_t* get_friendlyName_0() const { return ___friendlyName_0; }
	inline String_t** get_address_of_friendlyName_0() { return &___friendlyName_0; }
	inline void set_friendlyName_0(String_t* value)
	{
		___friendlyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___friendlyName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERIDFIELDINFOATTRIBUTE_T96E234F121A683A77A9F56CFDF9BB1B819CF1B3D_H
#ifndef INPUTACTIONSOURCEDATA_TAB524E4A9B0BE8CB999545953717D3EC066E2FF7_H
#define INPUTACTIONSOURCEDATA_TAB524E4A9B0BE8CB999545953717D3EC066E2FF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputActionSourceData
struct  InputActionSourceData_tAB524E4A9B0BE8CB999545953717D3EC066E2FF7 
{
public:
	// Rewired.Controller Rewired.InputActionSourceData::gcFDOBoqpaBweHMwNlbneFOEEAm
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___gcFDOBoqpaBweHMwNlbneFOEEAm_0;
	// Rewired.ControllerMap Rewired.InputActionSourceData::uNIXhFwhKcbRMaCTeDSCheRwjLdb
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1;
	// Rewired.ActionElementMap Rewired.InputActionSourceData::vzRXOXEPrntdOVXFtuchDEexSwL
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___vzRXOXEPrntdOVXFtuchDEexSwL_2;

public:
	inline static int32_t get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_0() { return static_cast<int32_t>(offsetof(InputActionSourceData_tAB524E4A9B0BE8CB999545953717D3EC066E2FF7, ___gcFDOBoqpaBweHMwNlbneFOEEAm_0)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_gcFDOBoqpaBweHMwNlbneFOEEAm_0() const { return ___gcFDOBoqpaBweHMwNlbneFOEEAm_0; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_gcFDOBoqpaBweHMwNlbneFOEEAm_0() { return &___gcFDOBoqpaBweHMwNlbneFOEEAm_0; }
	inline void set_gcFDOBoqpaBweHMwNlbneFOEEAm_0(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___gcFDOBoqpaBweHMwNlbneFOEEAm_0 = value;
		Il2CppCodeGenWriteBarrier((&___gcFDOBoqpaBweHMwNlbneFOEEAm_0), value);
	}

	inline static int32_t get_offset_of_uNIXhFwhKcbRMaCTeDSCheRwjLdb_1() { return static_cast<int32_t>(offsetof(InputActionSourceData_tAB524E4A9B0BE8CB999545953717D3EC066E2FF7, ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_uNIXhFwhKcbRMaCTeDSCheRwjLdb_1() const { return ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_uNIXhFwhKcbRMaCTeDSCheRwjLdb_1() { return &___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1; }
	inline void set_uNIXhFwhKcbRMaCTeDSCheRwjLdb_1(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1 = value;
		Il2CppCodeGenWriteBarrier((&___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1), value);
	}

	inline static int32_t get_offset_of_vzRXOXEPrntdOVXFtuchDEexSwL_2() { return static_cast<int32_t>(offsetof(InputActionSourceData_tAB524E4A9B0BE8CB999545953717D3EC066E2FF7, ___vzRXOXEPrntdOVXFtuchDEexSwL_2)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_vzRXOXEPrntdOVXFtuchDEexSwL_2() const { return ___vzRXOXEPrntdOVXFtuchDEexSwL_2; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_vzRXOXEPrntdOVXFtuchDEexSwL_2() { return &___vzRXOXEPrntdOVXFtuchDEexSwL_2; }
	inline void set_vzRXOXEPrntdOVXFtuchDEexSwL_2(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___vzRXOXEPrntdOVXFtuchDEexSwL_2 = value;
		Il2CppCodeGenWriteBarrier((&___vzRXOXEPrntdOVXFtuchDEexSwL_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.InputActionSourceData
struct InputActionSourceData_tAB524E4A9B0BE8CB999545953717D3EC066E2FF7_marshaled_pinvoke
{
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___gcFDOBoqpaBweHMwNlbneFOEEAm_0;
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1;
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___vzRXOXEPrntdOVXFtuchDEexSwL_2;
};
// Native definition for COM marshalling of Rewired.InputActionSourceData
struct InputActionSourceData_tAB524E4A9B0BE8CB999545953717D3EC066E2FF7_marshaled_com
{
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___gcFDOBoqpaBweHMwNlbneFOEEAm_0;
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_1;
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___vzRXOXEPrntdOVXFtuchDEexSwL_2;
};
#endif // INPUTACTIONSOURCEDATA_TAB524E4A9B0BE8CB999545953717D3EC066E2FF7_H
#ifndef LOWLEVELBUTTONEVENT_TE21E5494A12F66FFF33D9B53EB51664F9C8C0F93_H
#define LOWLEVELBUTTONEVENT_TE21E5494A12F66FFF33D9B53EB51664F9C8C0F93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.LowLevelButtonEvent
struct  LowLevelButtonEvent_tE21E5494A12F66FFF33D9B53EB51664F9C8C0F93  : public LowLevelEvent_t0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC
{
public:
	// System.Single Rewired.LowLevelButtonEvent::value
	float ___value_4;

public:
	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(LowLevelButtonEvent_tE21E5494A12F66FFF33D9B53EB51664F9C8C0F93, ___value_4)); }
	inline float get_value_4() const { return ___value_4; }
	inline float* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(float value)
	{
		___value_4 = value;
	}
};

struct LowLevelButtonEvent_tE21E5494A12F66FFF33D9B53EB51664F9C8C0F93_StaticFields
{
public:
	// Rewired.Utils.Classes.Utility.ObjectPool`1<Rewired.LowLevelButtonEvent> Rewired.LowLevelButtonEvent::PavQAlUTQJCXgnaaIZJqMrVxxkr
	ObjectPool_1_tF2B09DB44570E458D9E2A3700C91A70A74CA29B3 * ___PavQAlUTQJCXgnaaIZJqMrVxxkr_5;
	// System.Func`1<Rewired.LowLevelButtonEvent> Rewired.LowLevelButtonEvent::wKZsRNpOhQctCfJnvfAdTetobSIx
	Func_1_t2C8C7AF570C9781CB38423268805E9C7208378B8 * ___wKZsRNpOhQctCfJnvfAdTetobSIx_6;

public:
	inline static int32_t get_offset_of_PavQAlUTQJCXgnaaIZJqMrVxxkr_5() { return static_cast<int32_t>(offsetof(LowLevelButtonEvent_tE21E5494A12F66FFF33D9B53EB51664F9C8C0F93_StaticFields, ___PavQAlUTQJCXgnaaIZJqMrVxxkr_5)); }
	inline ObjectPool_1_tF2B09DB44570E458D9E2A3700C91A70A74CA29B3 * get_PavQAlUTQJCXgnaaIZJqMrVxxkr_5() const { return ___PavQAlUTQJCXgnaaIZJqMrVxxkr_5; }
	inline ObjectPool_1_tF2B09DB44570E458D9E2A3700C91A70A74CA29B3 ** get_address_of_PavQAlUTQJCXgnaaIZJqMrVxxkr_5() { return &___PavQAlUTQJCXgnaaIZJqMrVxxkr_5; }
	inline void set_PavQAlUTQJCXgnaaIZJqMrVxxkr_5(ObjectPool_1_tF2B09DB44570E458D9E2A3700C91A70A74CA29B3 * value)
	{
		___PavQAlUTQJCXgnaaIZJqMrVxxkr_5 = value;
		Il2CppCodeGenWriteBarrier((&___PavQAlUTQJCXgnaaIZJqMrVxxkr_5), value);
	}

	inline static int32_t get_offset_of_wKZsRNpOhQctCfJnvfAdTetobSIx_6() { return static_cast<int32_t>(offsetof(LowLevelButtonEvent_tE21E5494A12F66FFF33D9B53EB51664F9C8C0F93_StaticFields, ___wKZsRNpOhQctCfJnvfAdTetobSIx_6)); }
	inline Func_1_t2C8C7AF570C9781CB38423268805E9C7208378B8 * get_wKZsRNpOhQctCfJnvfAdTetobSIx_6() const { return ___wKZsRNpOhQctCfJnvfAdTetobSIx_6; }
	inline Func_1_t2C8C7AF570C9781CB38423268805E9C7208378B8 ** get_address_of_wKZsRNpOhQctCfJnvfAdTetobSIx_6() { return &___wKZsRNpOhQctCfJnvfAdTetobSIx_6; }
	inline void set_wKZsRNpOhQctCfJnvfAdTetobSIx_6(Func_1_t2C8C7AF570C9781CB38423268805E9C7208378B8 * value)
	{
		___wKZsRNpOhQctCfJnvfAdTetobSIx_6 = value;
		Il2CppCodeGenWriteBarrier((&___wKZsRNpOhQctCfJnvfAdTetobSIx_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOWLEVELBUTTONEVENT_TE21E5494A12F66FFF33D9B53EB51664F9C8C0F93_H
#ifndef PIDVID_TE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D_H
#define PIDVID_TE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PidVid
struct  PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D 
{
public:
	// System.UInt16 Rewired.PidVid::productId
	uint16_t ___productId_1;
	// System.UInt16 Rewired.PidVid::vendorId
	uint16_t ___vendorId_2;

public:
	inline static int32_t get_offset_of_productId_1() { return static_cast<int32_t>(offsetof(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D, ___productId_1)); }
	inline uint16_t get_productId_1() const { return ___productId_1; }
	inline uint16_t* get_address_of_productId_1() { return &___productId_1; }
	inline void set_productId_1(uint16_t value)
	{
		___productId_1 = value;
	}

	inline static int32_t get_offset_of_vendorId_2() { return static_cast<int32_t>(offsetof(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D, ___vendorId_2)); }
	inline uint16_t get_vendorId_2() const { return ___vendorId_2; }
	inline uint16_t* get_address_of_vendorId_2() { return &___vendorId_2; }
	inline void set_vendorId_2(uint16_t value)
	{
		___vendorId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIDVID_TE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D_H
#ifndef REWIREDVERSION_T3436502C80D66857DD1EA07C63AE75F2D9040DEA_H
#define REWIREDVERSION_T3436502C80D66857DD1EA07C63AE75F2D9040DEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.RewiredVersion
struct  RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA 
{
public:
	// System.Int32 Rewired.RewiredVersion::version1
	int32_t ___version1_0;
	// System.Int32 Rewired.RewiredVersion::version2
	int32_t ___version2_1;
	// System.Int32 Rewired.RewiredVersion::version3
	int32_t ___version3_2;
	// System.Int32 Rewired.RewiredVersion::version4
	int32_t ___version4_3;
	// System.String Rewired.RewiredVersion::unityVersion
	String_t* ___unityVersion_4;

public:
	inline static int32_t get_offset_of_version1_0() { return static_cast<int32_t>(offsetof(RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA, ___version1_0)); }
	inline int32_t get_version1_0() const { return ___version1_0; }
	inline int32_t* get_address_of_version1_0() { return &___version1_0; }
	inline void set_version1_0(int32_t value)
	{
		___version1_0 = value;
	}

	inline static int32_t get_offset_of_version2_1() { return static_cast<int32_t>(offsetof(RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA, ___version2_1)); }
	inline int32_t get_version2_1() const { return ___version2_1; }
	inline int32_t* get_address_of_version2_1() { return &___version2_1; }
	inline void set_version2_1(int32_t value)
	{
		___version2_1 = value;
	}

	inline static int32_t get_offset_of_version3_2() { return static_cast<int32_t>(offsetof(RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA, ___version3_2)); }
	inline int32_t get_version3_2() const { return ___version3_2; }
	inline int32_t* get_address_of_version3_2() { return &___version3_2; }
	inline void set_version3_2(int32_t value)
	{
		___version3_2 = value;
	}

	inline static int32_t get_offset_of_version4_3() { return static_cast<int32_t>(offsetof(RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA, ___version4_3)); }
	inline int32_t get_version4_3() const { return ___version4_3; }
	inline int32_t* get_address_of_version4_3() { return &___version4_3; }
	inline void set_version4_3(int32_t value)
	{
		___version4_3 = value;
	}

	inline static int32_t get_offset_of_unityVersion_4() { return static_cast<int32_t>(offsetof(RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA, ___unityVersion_4)); }
	inline String_t* get_unityVersion_4() const { return ___unityVersion_4; }
	inline String_t** get_address_of_unityVersion_4() { return &___unityVersion_4; }
	inline void set_unityVersion_4(String_t* value)
	{
		___unityVersion_4 = value;
		Il2CppCodeGenWriteBarrier((&___unityVersion_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.RewiredVersion
struct RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA_marshaled_pinvoke
{
	int32_t ___version1_0;
	int32_t ___version2_1;
	int32_t ___version3_2;
	int32_t ___version4_3;
	char* ___unityVersion_4;
};
// Native definition for COM marshalling of Rewired.RewiredVersion
struct RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA_marshaled_com
{
	int32_t ___version1_0;
	int32_t ___version2_1;
	int32_t ___version3_2;
	int32_t ___version4_3;
	Il2CppChar* ___unityVersion_4;
};
#endif // REWIREDVERSION_T3436502C80D66857DD1EA07C63AE75F2D9040DEA_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T347DDFFCE59C6D5D51EBA70D452CB2079F01AC1B_H
#define MONOPINVOKECALLBACKATTRIBUTE_T347DDFFCE59C6D5D51EBA70D452CB2079F01AC1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Attributes.MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t347DDFFCE59C6D5D51EBA70D452CB2079F01AC1B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type Rewired.Utils.Attributes.MonoPInvokeCallbackAttribute::type
	Type_t * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(MonoPInvokeCallbackAttribute_t347DDFFCE59C6D5D51EBA70D452CB2079F01AC1B, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T347DDFFCE59C6D5D51EBA70D452CB2079F01AC1B_H
#ifndef PRESERVEATTRIBUTE_TF09697B331D2CFEDB87BE18244F6CA6C980A62B1_H
#define PRESERVEATTRIBUTE_TF09697B331D2CFEDB87BE18244F6CA6C980A62B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Attributes.PreserveAttribute
struct  PreserveAttribute_tF09697B331D2CFEDB87BE18244F6CA6C980A62B1  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEATTRIBUTE_TF09697B331D2CFEDB87BE18244F6CA6C980A62B1_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#define PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields
{
public:
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifndef AXISCONSTRAINT_T6929580E28515C207FCC805981F73EC49354E48A_H
#define AXISCONSTRAINT_T6929580E28515C207FCC805981F73EC49354E48A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AxisConstraint
struct  AxisConstraint_t6929580E28515C207FCC805981F73EC49354E48A 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisConstraint_t6929580E28515C207FCC805981F73EC49354E48A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCONSTRAINT_T6929580E28515C207FCC805981F73EC49354E48A_H
#ifndef ONDISABLEBEHAVIOUR_T4D4DDECAFD77D9245EED8E5940AB100CE15876B4_H
#define ONDISABLEBEHAVIOUR_T4D4DDECAFD77D9245EED8E5940AB100CE15876B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.OnDisableBehaviour
struct  OnDisableBehaviour_t4D4DDECAFD77D9245EED8E5940AB100CE15876B4 
{
public:
	// System.Int32 DG.Tweening.Core.OnDisableBehaviour::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OnDisableBehaviour_t4D4DDECAFD77D9245EED8E5940AB100CE15876B4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDISABLEBEHAVIOUR_T4D4DDECAFD77D9245EED8E5940AB100CE15876B4_H
#ifndef ONENABLEBEHAVIOUR_T3C98A3FDA65C44B5BA4E84752A15E9A6A68765B5_H
#define ONENABLEBEHAVIOUR_T3C98A3FDA65C44B5BA4E84752A15E9A6A68765B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.OnEnableBehaviour
struct  OnEnableBehaviour_t3C98A3FDA65C44B5BA4E84752A15E9A6A68765B5 
{
public:
	// System.Int32 DG.Tweening.Core.OnEnableBehaviour::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OnEnableBehaviour_t3C98A3FDA65C44B5BA4E84752A15E9A6A68765B5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONENABLEBEHAVIOUR_T3C98A3FDA65C44B5BA4E84752A15E9A6A68765B5_H
#ifndef VISUALMANAGERPRESET_TC4F634B0BE552725DBAD3DAEABBABA80B8596A7E_H
#define VISUALMANAGERPRESET_TC4F634B0BE552725DBAD3DAEABBABA80B8596A7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.VisualManagerPreset
struct  VisualManagerPreset_tC4F634B0BE552725DBAD3DAEABBABA80B8596A7E 
{
public:
	// System.Int32 DG.Tweening.Core.VisualManagerPreset::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VisualManagerPreset_tC4F634B0BE552725DBAD3DAEABBABA80B8596A7E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISUALMANAGERPRESET_TC4F634B0BE552725DBAD3DAEABBABA80B8596A7E_H
#ifndef DOTWEENINSPECTORMODE_T37B98D58E76A1A14E51F9295CEB00F574AADD9B1_H
#define DOTWEENINSPECTORMODE_T37B98D58E76A1A14E51F9295CEB00F574AADD9B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenInspectorMode
struct  DOTweenInspectorMode_t37B98D58E76A1A14E51F9295CEB00F574AADD9B1 
{
public:
	// System.Int32 DG.Tweening.DOTweenInspectorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DOTweenInspectorMode_t37B98D58E76A1A14E51F9295CEB00F574AADD9B1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENINSPECTORMODE_T37B98D58E76A1A14E51F9295CEB00F574AADD9B1_H
#ifndef EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#define EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifndef HANDLESDRAWMODE_TFED7FD59A78C0CB6399F023F742B93077C3F208A_H
#define HANDLESDRAWMODE_TFED7FD59A78C0CB6399F023F742B93077C3F208A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.HandlesDrawMode
struct  HandlesDrawMode_tFED7FD59A78C0CB6399F023F742B93077C3F208A 
{
public:
	// System.Int32 DG.Tweening.HandlesDrawMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HandlesDrawMode_tFED7FD59A78C0CB6399F023F742B93077C3F208A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLESDRAWMODE_TFED7FD59A78C0CB6399F023F742B93077C3F208A_H
#ifndef HANDLESTYPE_T65157C792644D0D925D426C656DB250524E0DADC_H
#define HANDLESTYPE_T65157C792644D0D925D426C656DB250524E0DADC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.HandlesType
struct  HandlesType_t65157C792644D0D925D426C656DB250524E0DADC 
{
public:
	// System.Int32 DG.Tweening.HandlesType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HandlesType_t65157C792644D0D925D426C656DB250524E0DADC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLESTYPE_T65157C792644D0D925D426C656DB250524E0DADC_H
#ifndef LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#define LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#ifndef PATHMODE_T2ACD9081A852C6AE7EBBC384C9BC14805080366B_H
#define PATHMODE_T2ACD9081A852C6AE7EBBC384C9BC14805080366B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathMode
struct  PathMode_t2ACD9081A852C6AE7EBBC384C9BC14805080366B 
{
public:
	// System.Int32 DG.Tweening.PathMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PathMode_t2ACD9081A852C6AE7EBBC384C9BC14805080366B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHMODE_T2ACD9081A852C6AE7EBBC384C9BC14805080366B_H
#ifndef PATHTYPE_T90AD7223A91AEA11EEF2E0646D064AFE49C92BCD_H
#define PATHTYPE_T90AD7223A91AEA11EEF2E0646D064AFE49C92BCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathType
struct  PathType_t90AD7223A91AEA11EEF2E0646D064AFE49C92BCD 
{
public:
	// System.Int32 DG.Tweening.PathType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PathType_t90AD7223A91AEA11EEF2E0646D064AFE49C92BCD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHTYPE_T90AD7223A91AEA11EEF2E0646D064AFE49C92BCD_H
#ifndef ORIENTTYPE_TA561E2F4F794660D71F18B057A4C6488691EA237_H
#define ORIENTTYPE_TA561E2F4F794660D71F18B057A4C6488691EA237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.OrientType
struct  OrientType_tA561E2F4F794660D71F18B057A4C6488691EA237 
{
public:
	// System.Int32 DG.Tweening.Plugins.Options.OrientType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OrientType_tA561E2F4F794660D71F18B057A4C6488691EA237, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTTYPE_TA561E2F4F794660D71F18B057A4C6488691EA237_H
#ifndef SPIRALPLUGIN_T5B827E6201D5FF36A7B4845D1281FCC65F5BC3CD_H
#define SPIRALPLUGIN_T5B827E6201D5FF36A7B4845D1281FCC65F5BC3CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.SpiralPlugin
struct  SpiralPlugin_t5B827E6201D5FF36A7B4845D1281FCC65F5BC3CD  : public ABSTweenPlugin_3_t2808B05B2F9480E2D94EC646B1B69E4BA80CAB99
{
public:

public:
};

struct SpiralPlugin_t5B827E6201D5FF36A7B4845D1281FCC65F5BC3CD_StaticFields
{
public:
	// UnityEngine.Vector3 DG.Tweening.Plugins.SpiralPlugin::DefaultDirection
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___DefaultDirection_0;

public:
	inline static int32_t get_offset_of_DefaultDirection_0() { return static_cast<int32_t>(offsetof(SpiralPlugin_t5B827E6201D5FF36A7B4845D1281FCC65F5BC3CD_StaticFields, ___DefaultDirection_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_DefaultDirection_0() const { return ___DefaultDirection_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_DefaultDirection_0() { return &___DefaultDirection_0; }
	inline void set_DefaultDirection_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___DefaultDirection_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPIRALPLUGIN_T5B827E6201D5FF36A7B4845D1281FCC65F5BC3CD_H
#ifndef SPIRALMODE_TFB14778D700215DC51AE35D0B8F3D54F397514DE_H
#define SPIRALMODE_TFB14778D700215DC51AE35D0B8F3D54F397514DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.SpiralMode
struct  SpiralMode_tFB14778D700215DC51AE35D0B8F3D54F397514DE 
{
public:
	// System.Int32 DG.Tweening.SpiralMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpiralMode_tFB14778D700215DC51AE35D0B8F3D54F397514DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPIRALMODE_TFB14778D700215DC51AE35D0B8F3D54F397514DE_H
#ifndef UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#define UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#ifndef ACTIONIDPROPERTYATTRIBUTE_T22FDCCC2B977E9E587A08FA1CD32E9750FE2DC20_H
#define ACTIONIDPROPERTYATTRIBUTE_T22FDCCC2B977E9E587A08FA1CD32E9750FE2DC20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ActionIdPropertyAttribute
struct  ActionIdPropertyAttribute_t22FDCCC2B977E9E587A08FA1CD32E9750FE2DC20  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:
	// System.Type Rewired.ActionIdPropertyAttribute::DzAgXbAsFcjwinjPLOBYLcFrhIr
	Type_t * ___DzAgXbAsFcjwinjPLOBYLcFrhIr_0;

public:
	inline static int32_t get_offset_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_0() { return static_cast<int32_t>(offsetof(ActionIdPropertyAttribute_t22FDCCC2B977E9E587A08FA1CD32E9750FE2DC20, ___DzAgXbAsFcjwinjPLOBYLcFrhIr_0)); }
	inline Type_t * get_DzAgXbAsFcjwinjPLOBYLcFrhIr_0() const { return ___DzAgXbAsFcjwinjPLOBYLcFrhIr_0; }
	inline Type_t ** get_address_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_0() { return &___DzAgXbAsFcjwinjPLOBYLcFrhIr_0; }
	inline void set_DzAgXbAsFcjwinjPLOBYLcFrhIr_0(Type_t * value)
	{
		___DzAgXbAsFcjwinjPLOBYLcFrhIr_0 = value;
		Il2CppCodeGenWriteBarrier((&___DzAgXbAsFcjwinjPLOBYLcFrhIr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONIDPROPERTYATTRIBUTE_T22FDCCC2B977E9E587A08FA1CD32E9750FE2DC20_H
#ifndef AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#define AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisCoordinateMode
struct  AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868 
{
public:
	// System.Int32 Rewired.AxisCoordinateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifndef AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#define AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisRange
struct  AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652 
{
public:
	// System.Int32 Rewired.AxisRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifndef BUTTONSTATEFLAGS_TE48083CA4AA8079D0A5006CD787C46510592624A_H
#define BUTTONSTATEFLAGS_TE48083CA4AA8079D0A5006CD787C46510592624A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ButtonStateFlags
struct  ButtonStateFlags_tE48083CA4AA8079D0A5006CD787C46510592624A 
{
public:
	// System.Int32 Rewired.ButtonStateFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStateFlags_tE48083CA4AA8079D0A5006CD787C46510592624A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATEFLAGS_TE48083CA4AA8079D0A5006CD787C46510592624A_H
#ifndef CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#define CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerElementType
struct  ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA 
{
public:
	// System.Int32 Rewired.ControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifndef TYPE_T182FB43AE32FF984996E805320E1D5244BCF5887_H
#define TYPE_T182FB43AE32FF984996E805320E1D5244BCF5887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerSetSelector_Type
struct  Type_t182FB43AE32FF984996E805320E1D5244BCF5887 
{
public:
	// System.Int32 Rewired.ControllerSetSelector_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t182FB43AE32FF984996E805320E1D5244BCF5887, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T182FB43AE32FF984996E805320E1D5244BCF5887_H
#ifndef CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#define CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerType
struct  ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4 
{
public:
	// System.Int32 Rewired.ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifndef ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#define ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentType
struct  ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394 
{
public:
	// System.Int32 Rewired.ElementAssignmentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#ifndef INPUTACTIONEVENTTYPE_T6774D649F0516D32F1C1A9FA807375A2584291DC_H
#define INPUTACTIONEVENTTYPE_T6774D649F0516D32F1C1A9FA807375A2584291DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputActionEventType
struct  InputActionEventType_t6774D649F0516D32F1C1A9FA807375A2584291DC 
{
public:
	// System.Int32 Rewired.InputActionEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputActionEventType_t6774D649F0516D32F1C1A9FA807375A2584291DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTACTIONEVENTTYPE_T6774D649F0516D32F1C1A9FA807375A2584291DC_H
#ifndef INPUTPLATFORM_T03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E_H
#define INPUTPLATFORM_T03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputPlatform
struct  InputPlatform_t03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E 
{
public:
	// System.Int32 Rewired.InputPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputPlatform_t03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTPLATFORM_T03DB45CD0E17381A8619EF9F5E89F208CCAF2F5E_H
#ifndef INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#define INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSource
struct  InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B 
{
public:
	// System.Int32 Rewired.InputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifndef KEYBOARDKEYCODE_TD0801C51ECEC814EC673F6E1849B0264EC307854_H
#define KEYBOARDKEYCODE_TD0801C51ECEC814EC673F6E1849B0264EC307854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.KeyboardKeyCode
struct  KeyboardKeyCode_tD0801C51ECEC814EC673F6E1849B0264EC307854 
{
public:
	// System.Int32 Rewired.KeyboardKeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyboardKeyCode_tD0801C51ECEC814EC673F6E1849B0264EC307854, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDKEYCODE_TD0801C51ECEC814EC673F6E1849B0264EC307854_H
#ifndef MODIFIERKEY_T300DA428522CD195A6C4121312A8A2BD4EA7ACE0_H
#define MODIFIERKEY_T300DA428522CD195A6C4121312A8A2BD4EA7ACE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKey
struct  ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0 
{
public:
	// System.Int32 Rewired.ModifierKey::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEY_T300DA428522CD195A6C4121312A8A2BD4EA7ACE0_H
#ifndef MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#define MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKeyFlags
struct  ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651 
{
public:
	// System.Int32 Rewired.ModifierKeyFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifndef PLAYERIDPROPERTYATTRIBUTE_TE61422D82980BEBA3A3306AA00086943DD361CE7_H
#define PLAYERIDPROPERTYATTRIBUTE_TE61422D82980BEBA3A3306AA00086943DD361CE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerIdPropertyAttribute
struct  PlayerIdPropertyAttribute_tE61422D82980BEBA3A3306AA00086943DD361CE7  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:
	// System.Type Rewired.PlayerIdPropertyAttribute::DzAgXbAsFcjwinjPLOBYLcFrhIr
	Type_t * ___DzAgXbAsFcjwinjPLOBYLcFrhIr_0;

public:
	inline static int32_t get_offset_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_0() { return static_cast<int32_t>(offsetof(PlayerIdPropertyAttribute_tE61422D82980BEBA3A3306AA00086943DD361CE7, ___DzAgXbAsFcjwinjPLOBYLcFrhIr_0)); }
	inline Type_t * get_DzAgXbAsFcjwinjPLOBYLcFrhIr_0() const { return ___DzAgXbAsFcjwinjPLOBYLcFrhIr_0; }
	inline Type_t ** get_address_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_0() { return &___DzAgXbAsFcjwinjPLOBYLcFrhIr_0; }
	inline void set_DzAgXbAsFcjwinjPLOBYLcFrhIr_0(Type_t * value)
	{
		___DzAgXbAsFcjwinjPLOBYLcFrhIr_0 = value;
		Il2CppCodeGenWriteBarrier((&___DzAgXbAsFcjwinjPLOBYLcFrhIr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERIDPROPERTYATTRIBUTE_TE61422D82980BEBA3A3306AA00086943DD361CE7_H
#ifndef POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#define POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Pole
struct  Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C 
{
public:
	// System.Int32 Rewired.Pole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifndef TOUCHINFO_T08476F6130C3D14C8795AFF139F5817E2938674D_H
#define TOUCHINFO_T08476F6130C3D14C8795AFF139F5817E2938674D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.TouchInfo
struct  TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D 
{
public:
	// System.Boolean Rewired.TouchInfo::EdmoIszvSQiLETihGiwjrkdiQJU
	bool ___EdmoIszvSQiLETihGiwjrkdiQJU_0;
	// System.Int32 Rewired.TouchInfo::shnrdezQGXIItRKVkXhhfZjvcwa
	int32_t ___shnrdezQGXIItRKVkXhhfZjvcwa_1;
	// UnityEngine.Vector2 Rewired.TouchInfo::YXkDhwfemImjfHFJhdSNRJexfGgA
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___YXkDhwfemImjfHFJhdSNRJexfGgA_2;
	// UnityEngine.Vector2 Rewired.TouchInfo::irIPhBYSHZlBFwUDdzEgTfZukJE
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___irIPhBYSHZlBFwUDdzEgTfZukJE_3;
	// UnityEngine.Vector2 Rewired.TouchInfo::eHxTHGHKMOrRUoTqLfqgKPRBDIt
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___eHxTHGHKMOrRUoTqLfqgKPRBDIt_4;
	// UnityEngine.Vector2 Rewired.TouchInfo::kSivYNpyDJjvIFHXOQPYhcizfxN
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___kSivYNpyDJjvIFHXOQPYhcizfxN_5;
	// System.Single Rewired.TouchInfo::iHeBEGNoWdoYQwsEbhDOjvTqonF
	float ___iHeBEGNoWdoYQwsEbhDOjvTqonF_6;
	// System.Int32 Rewired.TouchInfo::gNULUQfmkDmUrVjFTWlbDFfwiER
	int32_t ___gNULUQfmkDmUrVjFTWlbDFfwiER_7;

public:
	inline static int32_t get_offset_of_EdmoIszvSQiLETihGiwjrkdiQJU_0() { return static_cast<int32_t>(offsetof(TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D, ___EdmoIszvSQiLETihGiwjrkdiQJU_0)); }
	inline bool get_EdmoIszvSQiLETihGiwjrkdiQJU_0() const { return ___EdmoIszvSQiLETihGiwjrkdiQJU_0; }
	inline bool* get_address_of_EdmoIszvSQiLETihGiwjrkdiQJU_0() { return &___EdmoIszvSQiLETihGiwjrkdiQJU_0; }
	inline void set_EdmoIszvSQiLETihGiwjrkdiQJU_0(bool value)
	{
		___EdmoIszvSQiLETihGiwjrkdiQJU_0 = value;
	}

	inline static int32_t get_offset_of_shnrdezQGXIItRKVkXhhfZjvcwa_1() { return static_cast<int32_t>(offsetof(TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D, ___shnrdezQGXIItRKVkXhhfZjvcwa_1)); }
	inline int32_t get_shnrdezQGXIItRKVkXhhfZjvcwa_1() const { return ___shnrdezQGXIItRKVkXhhfZjvcwa_1; }
	inline int32_t* get_address_of_shnrdezQGXIItRKVkXhhfZjvcwa_1() { return &___shnrdezQGXIItRKVkXhhfZjvcwa_1; }
	inline void set_shnrdezQGXIItRKVkXhhfZjvcwa_1(int32_t value)
	{
		___shnrdezQGXIItRKVkXhhfZjvcwa_1 = value;
	}

	inline static int32_t get_offset_of_YXkDhwfemImjfHFJhdSNRJexfGgA_2() { return static_cast<int32_t>(offsetof(TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D, ___YXkDhwfemImjfHFJhdSNRJexfGgA_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_YXkDhwfemImjfHFJhdSNRJexfGgA_2() const { return ___YXkDhwfemImjfHFJhdSNRJexfGgA_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_YXkDhwfemImjfHFJhdSNRJexfGgA_2() { return &___YXkDhwfemImjfHFJhdSNRJexfGgA_2; }
	inline void set_YXkDhwfemImjfHFJhdSNRJexfGgA_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___YXkDhwfemImjfHFJhdSNRJexfGgA_2 = value;
	}

	inline static int32_t get_offset_of_irIPhBYSHZlBFwUDdzEgTfZukJE_3() { return static_cast<int32_t>(offsetof(TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D, ___irIPhBYSHZlBFwUDdzEgTfZukJE_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_irIPhBYSHZlBFwUDdzEgTfZukJE_3() const { return ___irIPhBYSHZlBFwUDdzEgTfZukJE_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_irIPhBYSHZlBFwUDdzEgTfZukJE_3() { return &___irIPhBYSHZlBFwUDdzEgTfZukJE_3; }
	inline void set_irIPhBYSHZlBFwUDdzEgTfZukJE_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___irIPhBYSHZlBFwUDdzEgTfZukJE_3 = value;
	}

	inline static int32_t get_offset_of_eHxTHGHKMOrRUoTqLfqgKPRBDIt_4() { return static_cast<int32_t>(offsetof(TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D, ___eHxTHGHKMOrRUoTqLfqgKPRBDIt_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_eHxTHGHKMOrRUoTqLfqgKPRBDIt_4() const { return ___eHxTHGHKMOrRUoTqLfqgKPRBDIt_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_eHxTHGHKMOrRUoTqLfqgKPRBDIt_4() { return &___eHxTHGHKMOrRUoTqLfqgKPRBDIt_4; }
	inline void set_eHxTHGHKMOrRUoTqLfqgKPRBDIt_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___eHxTHGHKMOrRUoTqLfqgKPRBDIt_4 = value;
	}

	inline static int32_t get_offset_of_kSivYNpyDJjvIFHXOQPYhcizfxN_5() { return static_cast<int32_t>(offsetof(TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D, ___kSivYNpyDJjvIFHXOQPYhcizfxN_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_kSivYNpyDJjvIFHXOQPYhcizfxN_5() const { return ___kSivYNpyDJjvIFHXOQPYhcizfxN_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_kSivYNpyDJjvIFHXOQPYhcizfxN_5() { return &___kSivYNpyDJjvIFHXOQPYhcizfxN_5; }
	inline void set_kSivYNpyDJjvIFHXOQPYhcizfxN_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___kSivYNpyDJjvIFHXOQPYhcizfxN_5 = value;
	}

	inline static int32_t get_offset_of_iHeBEGNoWdoYQwsEbhDOjvTqonF_6() { return static_cast<int32_t>(offsetof(TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D, ___iHeBEGNoWdoYQwsEbhDOjvTqonF_6)); }
	inline float get_iHeBEGNoWdoYQwsEbhDOjvTqonF_6() const { return ___iHeBEGNoWdoYQwsEbhDOjvTqonF_6; }
	inline float* get_address_of_iHeBEGNoWdoYQwsEbhDOjvTqonF_6() { return &___iHeBEGNoWdoYQwsEbhDOjvTqonF_6; }
	inline void set_iHeBEGNoWdoYQwsEbhDOjvTqonF_6(float value)
	{
		___iHeBEGNoWdoYQwsEbhDOjvTqonF_6 = value;
	}

	inline static int32_t get_offset_of_gNULUQfmkDmUrVjFTWlbDFfwiER_7() { return static_cast<int32_t>(offsetof(TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D, ___gNULUQfmkDmUrVjFTWlbDFfwiER_7)); }
	inline int32_t get_gNULUQfmkDmUrVjFTWlbDFfwiER_7() const { return ___gNULUQfmkDmUrVjFTWlbDFfwiER_7; }
	inline int32_t* get_address_of_gNULUQfmkDmUrVjFTWlbDFfwiER_7() { return &___gNULUQfmkDmUrVjFTWlbDFfwiER_7; }
	inline void set_gNULUQfmkDmUrVjFTWlbDFfwiER_7(int32_t value)
	{
		___gNULUQfmkDmUrVjFTWlbDFfwiER_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.TouchInfo
struct TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D_marshaled_pinvoke
{
	int32_t ___EdmoIszvSQiLETihGiwjrkdiQJU_0;
	int32_t ___shnrdezQGXIItRKVkXhhfZjvcwa_1;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___YXkDhwfemImjfHFJhdSNRJexfGgA_2;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___irIPhBYSHZlBFwUDdzEgTfZukJE_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___eHxTHGHKMOrRUoTqLfqgKPRBDIt_4;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___kSivYNpyDJjvIFHXOQPYhcizfxN_5;
	float ___iHeBEGNoWdoYQwsEbhDOjvTqonF_6;
	int32_t ___gNULUQfmkDmUrVjFTWlbDFfwiER_7;
};
// Native definition for COM marshalling of Rewired.TouchInfo
struct TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D_marshaled_com
{
	int32_t ___EdmoIszvSQiLETihGiwjrkdiQJU_0;
	int32_t ___shnrdezQGXIItRKVkXhhfZjvcwa_1;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___YXkDhwfemImjfHFJhdSNRJexfGgA_2;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___irIPhBYSHZlBFwUDdzEgTfZukJE_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___eHxTHGHKMOrRUoTqLfqgKPRBDIt_4;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___kSivYNpyDJjvIFHXOQPYhcizfxN_5;
	float ___iHeBEGNoWdoYQwsEbhDOjvTqonF_6;
	int32_t ___gNULUQfmkDmUrVjFTWlbDFfwiER_7;
};
#endif // TOUCHINFO_T08476F6130C3D14C8795AFF139F5817E2938674D_H
#ifndef UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#define UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UpdateLoopType
struct  UpdateLoopType_t407C8154C4B63048CA3DC1A59E6458956BC31F0A 
{
public:
	// System.Int32 Rewired.UpdateLoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateLoopType_t407C8154C4B63048CA3DC1A59E6458956BC31F0A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#ifndef BITMASKATTRIBUTE_T2DD0E46C57279F82891239B4EEE2CD936BDA2B74_H
#define BITMASKATTRIBUTE_T2DD0E46C57279F82891239B4EEE2CD936BDA2B74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Attributes.BitmaskAttribute
struct  BitmaskAttribute_t2DD0E46C57279F82891239B4EEE2CD936BDA2B74  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:
	// System.Type Rewired.Utils.Attributes.BitmaskAttribute::propType
	Type_t * ___propType_0;

public:
	inline static int32_t get_offset_of_propType_0() { return static_cast<int32_t>(offsetof(BitmaskAttribute_t2DD0E46C57279F82891239B4EEE2CD936BDA2B74, ___propType_0)); }
	inline Type_t * get_propType_0() const { return ___propType_0; }
	inline Type_t ** get_address_of_propType_0() { return &___propType_0; }
	inline void set_propType_0(Type_t * value)
	{
		___propType_0 = value;
		Il2CppCodeGenWriteBarrier((&___propType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITMASKATTRIBUTE_T2DD0E46C57279F82891239B4EEE2CD936BDA2B74_H
#ifndef BITMASKTOGGLEATTRIBUTE_TF36CA63EE97482524F92128A45D49659758A32EC_H
#define BITMASKTOGGLEATTRIBUTE_TF36CA63EE97482524F92128A45D49659758A32EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Attributes.BitmaskToggleAttribute
struct  BitmaskToggleAttribute_tF36CA63EE97482524F92128A45D49659758A32EC  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:
	// System.Type Rewired.Utils.Attributes.BitmaskToggleAttribute::propType
	Type_t * ___propType_0;
	// System.Boolean Rewired.Utils.Attributes.BitmaskToggleAttribute::showNone
	bool ___showNone_1;
	// System.Boolean Rewired.Utils.Attributes.BitmaskToggleAttribute::showAll
	bool ___showAll_2;

public:
	inline static int32_t get_offset_of_propType_0() { return static_cast<int32_t>(offsetof(BitmaskToggleAttribute_tF36CA63EE97482524F92128A45D49659758A32EC, ___propType_0)); }
	inline Type_t * get_propType_0() const { return ___propType_0; }
	inline Type_t ** get_address_of_propType_0() { return &___propType_0; }
	inline void set_propType_0(Type_t * value)
	{
		___propType_0 = value;
		Il2CppCodeGenWriteBarrier((&___propType_0), value);
	}

	inline static int32_t get_offset_of_showNone_1() { return static_cast<int32_t>(offsetof(BitmaskToggleAttribute_tF36CA63EE97482524F92128A45D49659758A32EC, ___showNone_1)); }
	inline bool get_showNone_1() const { return ___showNone_1; }
	inline bool* get_address_of_showNone_1() { return &___showNone_1; }
	inline void set_showNone_1(bool value)
	{
		___showNone_1 = value;
	}

	inline static int32_t get_offset_of_showAll_2() { return static_cast<int32_t>(offsetof(BitmaskToggleAttribute_tF36CA63EE97482524F92128A45D49659758A32EC, ___showAll_2)); }
	inline bool get_showAll_2() const { return ___showAll_2; }
	inline bool* get_address_of_showAll_2() { return &___showAll_2; }
	inline void set_showAll_2(bool value)
	{
		___showAll_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITMASKTOGGLEATTRIBUTE_TF36CA63EE97482524F92128A45D49659758A32EC_H
#ifndef FIELDRANGEATTRIBUTE_TDA409BA82B6D22DFF55734E94DEFD443F9C51149_H
#define FIELDRANGEATTRIBUTE_TDA409BA82B6D22DFF55734E94DEFD443F9C51149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Attributes.FieldRangeAttribute
struct  FieldRangeAttribute_tDA409BA82B6D22DFF55734E94DEFD443F9C51149  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:
	// System.Single Rewired.Utils.Attributes.FieldRangeAttribute::hIkxovEuYTMCRUaLMwxAoHjNNmY
	float ___hIkxovEuYTMCRUaLMwxAoHjNNmY_0;
	// System.Single Rewired.Utils.Attributes.FieldRangeAttribute::eSZsCzljrhuCIBopfnRisnptBxgE
	float ___eSZsCzljrhuCIBopfnRisnptBxgE_1;
	// System.Int32 Rewired.Utils.Attributes.FieldRangeAttribute::ywqbVCEqGBFXpKKfAQJxdJNcjca
	int32_t ___ywqbVCEqGBFXpKKfAQJxdJNcjca_2;
	// System.Int32 Rewired.Utils.Attributes.FieldRangeAttribute::dVmfgUxGQrgacYprIhqAqtAAWXp
	int32_t ___dVmfgUxGQrgacYprIhqAqtAAWXp_3;

public:
	inline static int32_t get_offset_of_hIkxovEuYTMCRUaLMwxAoHjNNmY_0() { return static_cast<int32_t>(offsetof(FieldRangeAttribute_tDA409BA82B6D22DFF55734E94DEFD443F9C51149, ___hIkxovEuYTMCRUaLMwxAoHjNNmY_0)); }
	inline float get_hIkxovEuYTMCRUaLMwxAoHjNNmY_0() const { return ___hIkxovEuYTMCRUaLMwxAoHjNNmY_0; }
	inline float* get_address_of_hIkxovEuYTMCRUaLMwxAoHjNNmY_0() { return &___hIkxovEuYTMCRUaLMwxAoHjNNmY_0; }
	inline void set_hIkxovEuYTMCRUaLMwxAoHjNNmY_0(float value)
	{
		___hIkxovEuYTMCRUaLMwxAoHjNNmY_0 = value;
	}

	inline static int32_t get_offset_of_eSZsCzljrhuCIBopfnRisnptBxgE_1() { return static_cast<int32_t>(offsetof(FieldRangeAttribute_tDA409BA82B6D22DFF55734E94DEFD443F9C51149, ___eSZsCzljrhuCIBopfnRisnptBxgE_1)); }
	inline float get_eSZsCzljrhuCIBopfnRisnptBxgE_1() const { return ___eSZsCzljrhuCIBopfnRisnptBxgE_1; }
	inline float* get_address_of_eSZsCzljrhuCIBopfnRisnptBxgE_1() { return &___eSZsCzljrhuCIBopfnRisnptBxgE_1; }
	inline void set_eSZsCzljrhuCIBopfnRisnptBxgE_1(float value)
	{
		___eSZsCzljrhuCIBopfnRisnptBxgE_1 = value;
	}

	inline static int32_t get_offset_of_ywqbVCEqGBFXpKKfAQJxdJNcjca_2() { return static_cast<int32_t>(offsetof(FieldRangeAttribute_tDA409BA82B6D22DFF55734E94DEFD443F9C51149, ___ywqbVCEqGBFXpKKfAQJxdJNcjca_2)); }
	inline int32_t get_ywqbVCEqGBFXpKKfAQJxdJNcjca_2() const { return ___ywqbVCEqGBFXpKKfAQJxdJNcjca_2; }
	inline int32_t* get_address_of_ywqbVCEqGBFXpKKfAQJxdJNcjca_2() { return &___ywqbVCEqGBFXpKKfAQJxdJNcjca_2; }
	inline void set_ywqbVCEqGBFXpKKfAQJxdJNcjca_2(int32_t value)
	{
		___ywqbVCEqGBFXpKKfAQJxdJNcjca_2 = value;
	}

	inline static int32_t get_offset_of_dVmfgUxGQrgacYprIhqAqtAAWXp_3() { return static_cast<int32_t>(offsetof(FieldRangeAttribute_tDA409BA82B6D22DFF55734E94DEFD443F9C51149, ___dVmfgUxGQrgacYprIhqAqtAAWXp_3)); }
	inline int32_t get_dVmfgUxGQrgacYprIhqAqtAAWXp_3() const { return ___dVmfgUxGQrgacYprIhqAqtAAWXp_3; }
	inline int32_t* get_address_of_dVmfgUxGQrgacYprIhqAqtAAWXp_3() { return &___dVmfgUxGQrgacYprIhqAqtAAWXp_3; }
	inline void set_dVmfgUxGQrgacYprIhqAqtAAWXp_3(int32_t value)
	{
		___dVmfgUxGQrgacYprIhqAqtAAWXp_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDRANGEATTRIBUTE_TDA409BA82B6D22DFF55734E94DEFD443F9C51149_H
#ifndef SERIALIZATIONTYPE_T67BE27A789E953C37E73B63BBC015AF220B8EFF7_H
#define SERIALIZATIONTYPE_T67BE27A789E953C37E73B63BBC015AF220B8EFF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Attributes.SerializationTypeAttribute_SerializationType
struct  SerializationType_t67BE27A789E953C37E73B63BBC015AF220B8EFF7 
{
public:
	// System.Int32 Rewired.Utils.Attributes.SerializationTypeAttribute_SerializationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SerializationType_t67BE27A789E953C37E73B63BBC015AF220B8EFF7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONTYPE_T67BE27A789E953C37E73B63BBC015AF220B8EFF7_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef SPIRALOPTIONS_T7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC_H
#define SPIRALOPTIONS_T7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.SpiralOptions
struct  SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC 
{
public:
	// System.Single DG.Tweening.Plugins.SpiralOptions::depth
	float ___depth_0;
	// System.Single DG.Tweening.Plugins.SpiralOptions::frequency
	float ___frequency_1;
	// System.Single DG.Tweening.Plugins.SpiralOptions::speed
	float ___speed_2;
	// DG.Tweening.SpiralMode DG.Tweening.Plugins.SpiralOptions::mode
	int32_t ___mode_3;
	// System.Boolean DG.Tweening.Plugins.SpiralOptions::snapping
	bool ___snapping_4;
	// System.Single DG.Tweening.Plugins.SpiralOptions::unit
	float ___unit_5;
	// UnityEngine.Quaternion DG.Tweening.Plugins.SpiralOptions::axisQ
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___axisQ_6;

public:
	inline static int32_t get_offset_of_depth_0() { return static_cast<int32_t>(offsetof(SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC, ___depth_0)); }
	inline float get_depth_0() const { return ___depth_0; }
	inline float* get_address_of_depth_0() { return &___depth_0; }
	inline void set_depth_0(float value)
	{
		___depth_0 = value;
	}

	inline static int32_t get_offset_of_frequency_1() { return static_cast<int32_t>(offsetof(SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC, ___frequency_1)); }
	inline float get_frequency_1() const { return ___frequency_1; }
	inline float* get_address_of_frequency_1() { return &___frequency_1; }
	inline void set_frequency_1(float value)
	{
		___frequency_1 = value;
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_mode_3() { return static_cast<int32_t>(offsetof(SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC, ___mode_3)); }
	inline int32_t get_mode_3() const { return ___mode_3; }
	inline int32_t* get_address_of_mode_3() { return &___mode_3; }
	inline void set_mode_3(int32_t value)
	{
		___mode_3 = value;
	}

	inline static int32_t get_offset_of_snapping_4() { return static_cast<int32_t>(offsetof(SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC, ___snapping_4)); }
	inline bool get_snapping_4() const { return ___snapping_4; }
	inline bool* get_address_of_snapping_4() { return &___snapping_4; }
	inline void set_snapping_4(bool value)
	{
		___snapping_4 = value;
	}

	inline static int32_t get_offset_of_unit_5() { return static_cast<int32_t>(offsetof(SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC, ___unit_5)); }
	inline float get_unit_5() const { return ___unit_5; }
	inline float* get_address_of_unit_5() { return &___unit_5; }
	inline void set_unit_5(float value)
	{
		___unit_5 = value;
	}

	inline static int32_t get_offset_of_axisQ_6() { return static_cast<int32_t>(offsetof(SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC, ___axisQ_6)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_axisQ_6() const { return ___axisQ_6; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_axisQ_6() { return &___axisQ_6; }
	inline void set_axisQ_6(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___axisQ_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.SpiralOptions
struct SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC_marshaled_pinvoke
{
	float ___depth_0;
	float ___frequency_1;
	float ___speed_2;
	int32_t ___mode_3;
	int32_t ___snapping_4;
	float ___unit_5;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___axisQ_6;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.SpiralOptions
struct SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC_marshaled_com
{
	float ___depth_0;
	float ___frequency_1;
	float ___speed_2;
	int32_t ___mode_3;
	int32_t ___snapping_4;
	float ___unit_5;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___axisQ_6;
};
#endif // SPIRALOPTIONS_T7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC_H
#ifndef QPXDMYPZSVDYLJDXEXKAYPCAQQW_TC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9_H
#define QPXDMYPZSVDYLJDXEXKAYPCAQQW_TC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QpxDmYPZSvdyLJdxEXKaYpCAqqW
struct  QpxDmYPZSvdyLJdxEXKaYpCAqqW_tC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9 
{
public:
	// Rewired.KeyboardKeyCode QpxDmYPZSvdyLJdxEXKaYpCAqqW::VnWWwfPKVKMcfzlEQdtUORAGuQr
	int32_t ___VnWWwfPKVKMcfzlEQdtUORAGuQr_0;
	// Rewired.ModifierKey QpxDmYPZSvdyLJdxEXKaYpCAqqW::mnGBxrKlrMEtcbDkElEqgOWkWuSS
	int32_t ___mnGBxrKlrMEtcbDkElEqgOWkWuSS_1;
	// Rewired.ModifierKey QpxDmYPZSvdyLJdxEXKaYpCAqqW::LtcbhcBgmsmnragehPolhLChilja
	int32_t ___LtcbhcBgmsmnragehPolhLChilja_2;
	// Rewired.ModifierKey QpxDmYPZSvdyLJdxEXKaYpCAqqW::nwYAkwIgQIppweizjiwEOkDJuCOQ
	int32_t ___nwYAkwIgQIppweizjiwEOkDJuCOQ_3;

public:
	inline static int32_t get_offset_of_VnWWwfPKVKMcfzlEQdtUORAGuQr_0() { return static_cast<int32_t>(offsetof(QpxDmYPZSvdyLJdxEXKaYpCAqqW_tC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9, ___VnWWwfPKVKMcfzlEQdtUORAGuQr_0)); }
	inline int32_t get_VnWWwfPKVKMcfzlEQdtUORAGuQr_0() const { return ___VnWWwfPKVKMcfzlEQdtUORAGuQr_0; }
	inline int32_t* get_address_of_VnWWwfPKVKMcfzlEQdtUORAGuQr_0() { return &___VnWWwfPKVKMcfzlEQdtUORAGuQr_0; }
	inline void set_VnWWwfPKVKMcfzlEQdtUORAGuQr_0(int32_t value)
	{
		___VnWWwfPKVKMcfzlEQdtUORAGuQr_0 = value;
	}

	inline static int32_t get_offset_of_mnGBxrKlrMEtcbDkElEqgOWkWuSS_1() { return static_cast<int32_t>(offsetof(QpxDmYPZSvdyLJdxEXKaYpCAqqW_tC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9, ___mnGBxrKlrMEtcbDkElEqgOWkWuSS_1)); }
	inline int32_t get_mnGBxrKlrMEtcbDkElEqgOWkWuSS_1() const { return ___mnGBxrKlrMEtcbDkElEqgOWkWuSS_1; }
	inline int32_t* get_address_of_mnGBxrKlrMEtcbDkElEqgOWkWuSS_1() { return &___mnGBxrKlrMEtcbDkElEqgOWkWuSS_1; }
	inline void set_mnGBxrKlrMEtcbDkElEqgOWkWuSS_1(int32_t value)
	{
		___mnGBxrKlrMEtcbDkElEqgOWkWuSS_1 = value;
	}

	inline static int32_t get_offset_of_LtcbhcBgmsmnragehPolhLChilja_2() { return static_cast<int32_t>(offsetof(QpxDmYPZSvdyLJdxEXKaYpCAqqW_tC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9, ___LtcbhcBgmsmnragehPolhLChilja_2)); }
	inline int32_t get_LtcbhcBgmsmnragehPolhLChilja_2() const { return ___LtcbhcBgmsmnragehPolhLChilja_2; }
	inline int32_t* get_address_of_LtcbhcBgmsmnragehPolhLChilja_2() { return &___LtcbhcBgmsmnragehPolhLChilja_2; }
	inline void set_LtcbhcBgmsmnragehPolhLChilja_2(int32_t value)
	{
		___LtcbhcBgmsmnragehPolhLChilja_2 = value;
	}

	inline static int32_t get_offset_of_nwYAkwIgQIppweizjiwEOkDJuCOQ_3() { return static_cast<int32_t>(offsetof(QpxDmYPZSvdyLJdxEXKaYpCAqqW_tC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9, ___nwYAkwIgQIppweizjiwEOkDJuCOQ_3)); }
	inline int32_t get_nwYAkwIgQIppweizjiwEOkDJuCOQ_3() const { return ___nwYAkwIgQIppweizjiwEOkDJuCOQ_3; }
	inline int32_t* get_address_of_nwYAkwIgQIppweizjiwEOkDJuCOQ_3() { return &___nwYAkwIgQIppweizjiwEOkDJuCOQ_3; }
	inline void set_nwYAkwIgQIppweizjiwEOkDJuCOQ_3(int32_t value)
	{
		___nwYAkwIgQIppweizjiwEOkDJuCOQ_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QPXDMYPZSVDYLJDXEXKAYPCAQQW_TC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9_H
#ifndef BUTTONDATA_TC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A_H
#define BUTTONDATA_TC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ButtonLoopSet_ButtonData
struct  ButtonData_tC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A  : public RuntimeObject
{
public:
	// Rewired.UpdateLoopType Rewired.ButtonLoopSet_ButtonData::updateLoop
	int32_t ___updateLoop_0;
	// System.Boolean[] Rewired.ButtonLoopSet_ButtonData::values
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___values_1;
	// System.Boolean[] Rewired.ButtonLoopSet_ButtonData::wasTrueThisFrame
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___wasTrueThisFrame_2;
	// System.Boolean[] Rewired.ButtonLoopSet_ButtonData::TJEuDyBUDSfPkJfaqCBiqZkqLul
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___TJEuDyBUDSfPkJfaqCBiqZkqLul_3;
	// System.Int32 Rewired.ButtonLoopSet_ButtonData::EhtoTVELaSIHwGdNrAhgaKELEwF
	int32_t ___EhtoTVELaSIHwGdNrAhgaKELEwF_4;

public:
	inline static int32_t get_offset_of_updateLoop_0() { return static_cast<int32_t>(offsetof(ButtonData_tC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A, ___updateLoop_0)); }
	inline int32_t get_updateLoop_0() const { return ___updateLoop_0; }
	inline int32_t* get_address_of_updateLoop_0() { return &___updateLoop_0; }
	inline void set_updateLoop_0(int32_t value)
	{
		___updateLoop_0 = value;
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(ButtonData_tC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A, ___values_1)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_values_1() const { return ___values_1; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_wasTrueThisFrame_2() { return static_cast<int32_t>(offsetof(ButtonData_tC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A, ___wasTrueThisFrame_2)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_wasTrueThisFrame_2() const { return ___wasTrueThisFrame_2; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_wasTrueThisFrame_2() { return &___wasTrueThisFrame_2; }
	inline void set_wasTrueThisFrame_2(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___wasTrueThisFrame_2 = value;
		Il2CppCodeGenWriteBarrier((&___wasTrueThisFrame_2), value);
	}

	inline static int32_t get_offset_of_TJEuDyBUDSfPkJfaqCBiqZkqLul_3() { return static_cast<int32_t>(offsetof(ButtonData_tC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A, ___TJEuDyBUDSfPkJfaqCBiqZkqLul_3)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_TJEuDyBUDSfPkJfaqCBiqZkqLul_3() const { return ___TJEuDyBUDSfPkJfaqCBiqZkqLul_3; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_TJEuDyBUDSfPkJfaqCBiqZkqLul_3() { return &___TJEuDyBUDSfPkJfaqCBiqZkqLul_3; }
	inline void set_TJEuDyBUDSfPkJfaqCBiqZkqLul_3(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___TJEuDyBUDSfPkJfaqCBiqZkqLul_3 = value;
		Il2CppCodeGenWriteBarrier((&___TJEuDyBUDSfPkJfaqCBiqZkqLul_3), value);
	}

	inline static int32_t get_offset_of_EhtoTVELaSIHwGdNrAhgaKELEwF_4() { return static_cast<int32_t>(offsetof(ButtonData_tC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A, ___EhtoTVELaSIHwGdNrAhgaKELEwF_4)); }
	inline int32_t get_EhtoTVELaSIHwGdNrAhgaKELEwF_4() const { return ___EhtoTVELaSIHwGdNrAhgaKELEwF_4; }
	inline int32_t* get_address_of_EhtoTVELaSIHwGdNrAhgaKELEwF_4() { return &___EhtoTVELaSIHwGdNrAhgaKELEwF_4; }
	inline void set_EhtoTVELaSIHwGdNrAhgaKELEwF_4(int32_t value)
	{
		___EhtoTVELaSIHwGdNrAhgaKELEwF_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONDATA_TC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A_H
#ifndef CONTROLLERASSIGNMENTCHANGEDEVENTARGS_TD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D_H
#define CONTROLLERASSIGNMENTCHANGEDEVENTARGS_TD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerAssignmentChangedEventArgs
struct  ControllerAssignmentChangedEventArgs_tD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Boolean Rewired.ControllerAssignmentChangedEventArgs::UBXHFojKQfGbvszJFuVFvDRPgNg
	bool ___UBXHFojKQfGbvszJFuVFvDRPgNg_1;
	// System.Int32 Rewired.ControllerAssignmentChangedEventArgs::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	// System.Int32 Rewired.ControllerAssignmentChangedEventArgs::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	// Rewired.ControllerType Rewired.ControllerAssignmentChangedEventArgs::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;

public:
	inline static int32_t get_offset_of_UBXHFojKQfGbvszJFuVFvDRPgNg_1() { return static_cast<int32_t>(offsetof(ControllerAssignmentChangedEventArgs_tD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D, ___UBXHFojKQfGbvszJFuVFvDRPgNg_1)); }
	inline bool get_UBXHFojKQfGbvszJFuVFvDRPgNg_1() const { return ___UBXHFojKQfGbvszJFuVFvDRPgNg_1; }
	inline bool* get_address_of_UBXHFojKQfGbvszJFuVFvDRPgNg_1() { return &___UBXHFojKQfGbvszJFuVFvDRPgNg_1; }
	inline void set_UBXHFojKQfGbvszJFuVFvDRPgNg_1(bool value)
	{
		___UBXHFojKQfGbvszJFuVFvDRPgNg_1 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return static_cast<int32_t>(offsetof(ControllerAssignmentChangedEventArgs_tD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D, ___FCIziTxDWAnyHATWwZeMEmiXvdc_2)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_2() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_2(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_2 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() { return static_cast<int32_t>(offsetof(ControllerAssignmentChangedEventArgs_tD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D, ___CdiTZueJOweHxLVLesdWcZkZxNV_3)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_3() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_3; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_3; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_3(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_3 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return static_cast<int32_t>(offsetof(ControllerAssignmentChangedEventArgs_tD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERASSIGNMENTCHANGEDEVENTARGS_TD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D_H
#ifndef CONTROLLERIDENTIFIER_T725D2A7A9387931F93131874C32B08A3BCFF9DD6_H
#define CONTROLLERIDENTIFIER_T725D2A7A9387931F93131874C32B08A3BCFF9DD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerIdentifier
struct  ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6 
{
public:
	// System.Int32 Rewired.ControllerIdentifier::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_0;
	// Rewired.ControllerType Rewired.ControllerIdentifier::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1;
	// System.Guid Rewired.ControllerIdentifier::hVfvAlYZzngYWNUKHfQeqsdsSsN
	Guid_t  ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2;
	// System.String Rewired.ControllerIdentifier::fGtUJMoDYGQBkIHcDCTnfmUopvdN
	String_t* ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3;
	// System.Guid Rewired.ControllerIdentifier::uoQfWOPIJljmoZOdOiUDjMDpAEI
	Guid_t  ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4;

public:
	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_0() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___CdiTZueJOweHxLVLesdWcZkZxNV_0)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_0() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_0; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_0() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_0; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_0(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_0 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1 = value;
	}

	inline static int32_t get_offset_of_hVfvAlYZzngYWNUKHfQeqsdsSsN_2() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2)); }
	inline Guid_t  get_hVfvAlYZzngYWNUKHfQeqsdsSsN_2() const { return ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2; }
	inline Guid_t * get_address_of_hVfvAlYZzngYWNUKHfQeqsdsSsN_2() { return &___hVfvAlYZzngYWNUKHfQeqsdsSsN_2; }
	inline void set_hVfvAlYZzngYWNUKHfQeqsdsSsN_2(Guid_t  value)
	{
		___hVfvAlYZzngYWNUKHfQeqsdsSsN_2 = value;
	}

	inline static int32_t get_offset_of_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3)); }
	inline String_t* get_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3() const { return ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3; }
	inline String_t** get_address_of_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3() { return &___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3; }
	inline void set_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3(String_t* value)
	{
		___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3 = value;
		Il2CppCodeGenWriteBarrier((&___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3), value);
	}

	inline static int32_t get_offset_of_uoQfWOPIJljmoZOdOiUDjMDpAEI_4() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4)); }
	inline Guid_t  get_uoQfWOPIJljmoZOdOiUDjMDpAEI_4() const { return ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4; }
	inline Guid_t * get_address_of_uoQfWOPIJljmoZOdOiUDjMDpAEI_4() { return &___uoQfWOPIJljmoZOdOiUDjMDpAEI_4; }
	inline void set_uoQfWOPIJljmoZOdOiUDjMDpAEI_4(Guid_t  value)
	{
		___uoQfWOPIJljmoZOdOiUDjMDpAEI_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ControllerIdentifier
struct ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6_marshaled_pinvoke
{
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_0;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1;
	Guid_t  ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2;
	char* ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3;
	Guid_t  ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4;
};
// Native definition for COM marshalling of Rewired.ControllerIdentifier
struct ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6_marshaled_com
{
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_0;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1;
	Guid_t  ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2;
	Il2CppChar* ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3;
	Guid_t  ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4;
};
#endif // CONTROLLERIDENTIFIER_T725D2A7A9387931F93131874C32B08A3BCFF9DD6_H
#ifndef CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#define CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerPollingInfo
struct  ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 
{
public:
	// System.Boolean Rewired.ControllerPollingInfo::XxREeDCHebPEeYaAATiXyFbnbIU
	bool ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	// System.Int32 Rewired.ControllerPollingInfo::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	// System.Int32 Rewired.ControllerPollingInfo::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	// System.String Rewired.ControllerPollingInfo::tUeBKTuOCBEsklhkCXrBJHMCCni
	String_t* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	// Rewired.ControllerType Rewired.ControllerPollingInfo::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	// Rewired.ControllerElementType Rewired.ControllerPollingInfo::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	// System.Int32 Rewired.ControllerPollingInfo::JcIOHtlyyDcisHJclHvGjyIzQHK
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	// Rewired.Pole Rewired.ControllerPollingInfo::gOvKiYbtKZOeaPYXdsADyDYKhyR
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	// System.String Rewired.ControllerPollingInfo::BpnqUtABZEySqFzPQqIktIgZaUw
	String_t* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	// System.Int32 Rewired.ControllerPollingInfo::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	// UnityEngine.KeyCode Rewired.ControllerPollingInfo::UBcIxoTNwLQLduWHPwWDYgtuvif
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;

public:
	inline static int32_t get_offset_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___XxREeDCHebPEeYaAATiXyFbnbIU_0)); }
	inline bool get_XxREeDCHebPEeYaAATiXyFbnbIU_0() const { return ___XxREeDCHebPEeYaAATiXyFbnbIU_0; }
	inline bool* get_address_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() { return &___XxREeDCHebPEeYaAATiXyFbnbIU_0; }
	inline void set_XxREeDCHebPEeYaAATiXyFbnbIU_0(bool value)
	{
		___XxREeDCHebPEeYaAATiXyFbnbIU_0 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___FCIziTxDWAnyHATWwZeMEmiXvdc_1)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_1() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_1(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_1 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___CdiTZueJOweHxLVLesdWcZkZxNV_2)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_2() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_2(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_2 = value;
	}

	inline static int32_t get_offset_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___tUeBKTuOCBEsklhkCXrBJHMCCni_3)); }
	inline String_t* get_tUeBKTuOCBEsklhkCXrBJHMCCni_3() const { return ___tUeBKTuOCBEsklhkCXrBJHMCCni_3; }
	inline String_t** get_address_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() { return &___tUeBKTuOCBEsklhkCXrBJHMCCni_3; }
	inline void set_tUeBKTuOCBEsklhkCXrBJHMCCni_3(String_t* value)
	{
		___tUeBKTuOCBEsklhkCXrBJHMCCni_3 = value;
		Il2CppCodeGenWriteBarrier((&___tUeBKTuOCBEsklhkCXrBJHMCCni_3), value);
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4 = value;
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___RmmmEkVblcqxoqGYhifgdSESkSn_5)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_5() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_5; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_5; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_5(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_5 = value;
	}

	inline static int32_t get_offset_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___JcIOHtlyyDcisHJclHvGjyIzQHK_6)); }
	inline int32_t get_JcIOHtlyyDcisHJclHvGjyIzQHK_6() const { return ___JcIOHtlyyDcisHJclHvGjyIzQHK_6; }
	inline int32_t* get_address_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() { return &___JcIOHtlyyDcisHJclHvGjyIzQHK_6; }
	inline void set_JcIOHtlyyDcisHJclHvGjyIzQHK_6(int32_t value)
	{
		___JcIOHtlyyDcisHJclHvGjyIzQHK_6 = value;
	}

	inline static int32_t get_offset_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7)); }
	inline int32_t get_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() const { return ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7; }
	inline int32_t* get_address_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() { return &___gOvKiYbtKZOeaPYXdsADyDYKhyR_7; }
	inline void set_gOvKiYbtKZOeaPYXdsADyDYKhyR_7(int32_t value)
	{
		___gOvKiYbtKZOeaPYXdsADyDYKhyR_7 = value;
	}

	inline static int32_t get_offset_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___BpnqUtABZEySqFzPQqIktIgZaUw_8)); }
	inline String_t* get_BpnqUtABZEySqFzPQqIktIgZaUw_8() const { return ___BpnqUtABZEySqFzPQqIktIgZaUw_8; }
	inline String_t** get_address_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() { return &___BpnqUtABZEySqFzPQqIktIgZaUw_8; }
	inline void set_BpnqUtABZEySqFzPQqIktIgZaUw_8(String_t* value)
	{
		___BpnqUtABZEySqFzPQqIktIgZaUw_8 = value;
		Il2CppCodeGenWriteBarrier((&___BpnqUtABZEySqFzPQqIktIgZaUw_8), value);
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___FrsWYgeIWMnjOcXDysagwZGcCMF_9)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_9() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_9; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_9; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_9(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_9 = value;
	}

	inline static int32_t get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___UBcIxoTNwLQLduWHPwWDYgtuvif_10)); }
	inline int32_t get_UBcIxoTNwLQLduWHPwWDYgtuvif_10() const { return ___UBcIxoTNwLQLduWHPwWDYgtuvif_10; }
	inline int32_t* get_address_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() { return &___UBcIxoTNwLQLduWHPwWDYgtuvif_10; }
	inline void set_UBcIxoTNwLQLduWHPwWDYgtuvif_10(int32_t value)
	{
		___UBcIxoTNwLQLduWHPwWDYgtuvif_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ControllerPollingInfo
struct ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_pinvoke
{
	int32_t ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	char* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	char* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;
};
// Native definition for COM marshalling of Rewired.ControllerPollingInfo
struct ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_com
{
	int32_t ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	Il2CppChar* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	Il2CppChar* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;
};
#endif // CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#ifndef CONTROLLERSETSELECTOR_TA23A5CBB83617E78B6AC0116E102C782BCB9ADCD_H
#define CONTROLLERSETSELECTOR_TA23A5CBB83617E78B6AC0116E102C782BCB9ADCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerSetSelector
struct  ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD  : public RuntimeObject
{
public:
	// Rewired.ControllerSetSelector_Type Rewired.ControllerSetSelector::_type
	int32_t ____type_0;
	// Rewired.ControllerType Rewired.ControllerSetSelector::_controllerType
	int32_t ____controllerType_1;
	// System.String Rewired.ControllerSetSelector::_guid
	String_t* ____guid_2;
	// System.String Rewired.ControllerSetSelector::_hardwareIdentifier
	String_t* ____hardwareIdentifier_3;
	// System.Int32 Rewired.ControllerSetSelector::_controllerId
	int32_t ____controllerId_4;
	// System.Guid Rewired.ControllerSetSelector::kAtYqKtxQwdzXJBwfHMpmjYXGum
	Guid_t  ___kAtYqKtxQwdzXJBwfHMpmjYXGum_5;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD, ____type_0)); }
	inline int32_t get__type_0() const { return ____type_0; }
	inline int32_t* get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(int32_t value)
	{
		____type_0 = value;
	}

	inline static int32_t get_offset_of__controllerType_1() { return static_cast<int32_t>(offsetof(ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD, ____controllerType_1)); }
	inline int32_t get__controllerType_1() const { return ____controllerType_1; }
	inline int32_t* get_address_of__controllerType_1() { return &____controllerType_1; }
	inline void set__controllerType_1(int32_t value)
	{
		____controllerType_1 = value;
	}

	inline static int32_t get_offset_of__guid_2() { return static_cast<int32_t>(offsetof(ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD, ____guid_2)); }
	inline String_t* get__guid_2() const { return ____guid_2; }
	inline String_t** get_address_of__guid_2() { return &____guid_2; }
	inline void set__guid_2(String_t* value)
	{
		____guid_2 = value;
		Il2CppCodeGenWriteBarrier((&____guid_2), value);
	}

	inline static int32_t get_offset_of__hardwareIdentifier_3() { return static_cast<int32_t>(offsetof(ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD, ____hardwareIdentifier_3)); }
	inline String_t* get__hardwareIdentifier_3() const { return ____hardwareIdentifier_3; }
	inline String_t** get_address_of__hardwareIdentifier_3() { return &____hardwareIdentifier_3; }
	inline void set__hardwareIdentifier_3(String_t* value)
	{
		____hardwareIdentifier_3 = value;
		Il2CppCodeGenWriteBarrier((&____hardwareIdentifier_3), value);
	}

	inline static int32_t get_offset_of__controllerId_4() { return static_cast<int32_t>(offsetof(ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD, ____controllerId_4)); }
	inline int32_t get__controllerId_4() const { return ____controllerId_4; }
	inline int32_t* get_address_of__controllerId_4() { return &____controllerId_4; }
	inline void set__controllerId_4(int32_t value)
	{
		____controllerId_4 = value;
	}

	inline static int32_t get_offset_of_kAtYqKtxQwdzXJBwfHMpmjYXGum_5() { return static_cast<int32_t>(offsetof(ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD, ___kAtYqKtxQwdzXJBwfHMpmjYXGum_5)); }
	inline Guid_t  get_kAtYqKtxQwdzXJBwfHMpmjYXGum_5() const { return ___kAtYqKtxQwdzXJBwfHMpmjYXGum_5; }
	inline Guid_t * get_address_of_kAtYqKtxQwdzXJBwfHMpmjYXGum_5() { return &___kAtYqKtxQwdzXJBwfHMpmjYXGum_5; }
	inline void set_kAtYqKtxQwdzXJBwfHMpmjYXGum_5(Guid_t  value)
	{
		___kAtYqKtxQwdzXJBwfHMpmjYXGum_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERSETSELECTOR_TA23A5CBB83617E78B6AC0116E102C782BCB9ADCD_H
#ifndef CONTROLLERSTATUSCHANGEDEVENTARGS_TCC162BDFB36A2B12C4E07F5B9B807BDC20167EFC_H
#define CONTROLLERSTATUSCHANGEDEVENTARGS_TCC162BDFB36A2B12C4E07F5B9B807BDC20167EFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerStatusChangedEventArgs
struct  ControllerStatusChangedEventArgs_tCC162BDFB36A2B12C4E07F5B9B807BDC20167EFC  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String Rewired.ControllerStatusChangedEventArgs::xzFIhZhMHNeQYGDUhfMfvvqdZQds
	String_t* ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1;
	// System.Int32 Rewired.ControllerStatusChangedEventArgs::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	// Rewired.ControllerType Rewired.ControllerStatusChangedEventArgs::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;

public:
	inline static int32_t get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1() { return static_cast<int32_t>(offsetof(ControllerStatusChangedEventArgs_tCC162BDFB36A2B12C4E07F5B9B807BDC20167EFC, ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1)); }
	inline String_t* get_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1() const { return ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1; }
	inline String_t** get_address_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1() { return &___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1; }
	inline void set_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1(String_t* value)
	{
		___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1 = value;
		Il2CppCodeGenWriteBarrier((&___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1), value);
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return static_cast<int32_t>(offsetof(ControllerStatusChangedEventArgs_tCC162BDFB36A2B12C4E07F5B9B807BDC20167EFC, ___CdiTZueJOweHxLVLesdWcZkZxNV_2)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_2() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_2(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_2 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return static_cast<int32_t>(offsetof(ControllerStatusChangedEventArgs_tCC162BDFB36A2B12C4E07F5B9B807BDC20167EFC, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERSTATUSCHANGEDEVENTARGS_TCC162BDFB36A2B12C4E07F5B9B807BDC20167EFC_H
#ifndef ELEMENTASSIGNMENT_TB4E14263A80B282326670C629692DBEE4AF1CC8C_H
#define ELEMENTASSIGNMENT_TB4E14263A80B282326670C629692DBEE4AF1CC8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignment
struct  ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C 
{
public:
	// Rewired.ElementAssignmentType Rewired.ElementAssignment::type
	int32_t ___type_0;
	// System.Int32 Rewired.ElementAssignment::elementMapId
	int32_t ___elementMapId_1;
	// System.Int32 Rewired.ElementAssignment::elementIdentifierId
	int32_t ___elementIdentifierId_2;
	// Rewired.AxisRange Rewired.ElementAssignment::axisRange
	int32_t ___axisRange_3;
	// UnityEngine.KeyCode Rewired.ElementAssignment::keyboardKey
	int32_t ___keyboardKey_4;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignment::modifierKeyFlags
	int32_t ___modifierKeyFlags_5;
	// System.Int32 Rewired.ElementAssignment::actionId
	int32_t ___actionId_6;
	// Rewired.Pole Rewired.ElementAssignment::axisContribution
	int32_t ___axisContribution_7;
	// System.Boolean Rewired.ElementAssignment::invert
	bool ___invert_8;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_elementMapId_1() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___elementMapId_1)); }
	inline int32_t get_elementMapId_1() const { return ___elementMapId_1; }
	inline int32_t* get_address_of_elementMapId_1() { return &___elementMapId_1; }
	inline void set_elementMapId_1(int32_t value)
	{
		___elementMapId_1 = value;
	}

	inline static int32_t get_offset_of_elementIdentifierId_2() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___elementIdentifierId_2)); }
	inline int32_t get_elementIdentifierId_2() const { return ___elementIdentifierId_2; }
	inline int32_t* get_address_of_elementIdentifierId_2() { return &___elementIdentifierId_2; }
	inline void set_elementIdentifierId_2(int32_t value)
	{
		___elementIdentifierId_2 = value;
	}

	inline static int32_t get_offset_of_axisRange_3() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___axisRange_3)); }
	inline int32_t get_axisRange_3() const { return ___axisRange_3; }
	inline int32_t* get_address_of_axisRange_3() { return &___axisRange_3; }
	inline void set_axisRange_3(int32_t value)
	{
		___axisRange_3 = value;
	}

	inline static int32_t get_offset_of_keyboardKey_4() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___keyboardKey_4)); }
	inline int32_t get_keyboardKey_4() const { return ___keyboardKey_4; }
	inline int32_t* get_address_of_keyboardKey_4() { return &___keyboardKey_4; }
	inline void set_keyboardKey_4(int32_t value)
	{
		___keyboardKey_4 = value;
	}

	inline static int32_t get_offset_of_modifierKeyFlags_5() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___modifierKeyFlags_5)); }
	inline int32_t get_modifierKeyFlags_5() const { return ___modifierKeyFlags_5; }
	inline int32_t* get_address_of_modifierKeyFlags_5() { return &___modifierKeyFlags_5; }
	inline void set_modifierKeyFlags_5(int32_t value)
	{
		___modifierKeyFlags_5 = value;
	}

	inline static int32_t get_offset_of_actionId_6() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___actionId_6)); }
	inline int32_t get_actionId_6() const { return ___actionId_6; }
	inline int32_t* get_address_of_actionId_6() { return &___actionId_6; }
	inline void set_actionId_6(int32_t value)
	{
		___actionId_6 = value;
	}

	inline static int32_t get_offset_of_axisContribution_7() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___axisContribution_7)); }
	inline int32_t get_axisContribution_7() const { return ___axisContribution_7; }
	inline int32_t* get_address_of_axisContribution_7() { return &___axisContribution_7; }
	inline void set_axisContribution_7(int32_t value)
	{
		___axisContribution_7 = value;
	}

	inline static int32_t get_offset_of_invert_8() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___invert_8)); }
	inline bool get_invert_8() const { return ___invert_8; }
	inline bool* get_address_of_invert_8() { return &___invert_8; }
	inline void set_invert_8(bool value)
	{
		___invert_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignment
struct ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C_marshaled_pinvoke
{
	int32_t ___type_0;
	int32_t ___elementMapId_1;
	int32_t ___elementIdentifierId_2;
	int32_t ___axisRange_3;
	int32_t ___keyboardKey_4;
	int32_t ___modifierKeyFlags_5;
	int32_t ___actionId_6;
	int32_t ___axisContribution_7;
	int32_t ___invert_8;
};
// Native definition for COM marshalling of Rewired.ElementAssignment
struct ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C_marshaled_com
{
	int32_t ___type_0;
	int32_t ___elementMapId_1;
	int32_t ___elementIdentifierId_2;
	int32_t ___axisRange_3;
	int32_t ___keyboardKey_4;
	int32_t ___modifierKeyFlags_5;
	int32_t ___actionId_6;
	int32_t ___axisContribution_7;
	int32_t ___invert_8;
};
#endif // ELEMENTASSIGNMENT_TB4E14263A80B282326670C629692DBEE4AF1CC8C_H
#ifndef ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#define ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentConflictCheck
struct  ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 
{
public:
	// Rewired.ElementAssignmentType Rewired.ElementAssignmentConflictCheck::PReGOawJrxHuYbkYlXhFoNBbppd
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	// Rewired.ControllerType Rewired.ElementAssignmentConflictCheck::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::kNupzNtNRIxDZONVUSwnnBWcpIT
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::fAMwdVQKMkdvbVWXbkkCQoSadHid
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::TXYPyQJDwYiLBxGVhCRAhruZxByr
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	// Rewired.AxisRange Rewired.ElementAssignmentConflictCheck::xTkhpClvSKvoiLfYNzcXIDURAPx
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	// UnityEngine.KeyCode Rewired.ElementAssignmentConflictCheck::UBcIxoTNwLQLduWHPwWDYgtuvif
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignmentConflictCheck::rUpHewPnhPrBuIDeIasWkyvRDkW
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	// Rewired.Pole Rewired.ElementAssignmentConflictCheck::magSqBEVFaJkoQfcAvXLVDzmcnZB
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	// System.Boolean Rewired.ElementAssignmentConflictCheck::GfvPtFqTnCIcrWebHAQzqOoKceI
	bool ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;

public:
	inline static int32_t get_offset_of_PReGOawJrxHuYbkYlXhFoNBbppd_0() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___PReGOawJrxHuYbkYlXhFoNBbppd_0)); }
	inline int32_t get_PReGOawJrxHuYbkYlXhFoNBbppd_0() const { return ___PReGOawJrxHuYbkYlXhFoNBbppd_0; }
	inline int32_t* get_address_of_PReGOawJrxHuYbkYlXhFoNBbppd_0() { return &___PReGOawJrxHuYbkYlXhFoNBbppd_0; }
	inline void set_PReGOawJrxHuYbkYlXhFoNBbppd_0(int32_t value)
	{
		___PReGOawJrxHuYbkYlXhFoNBbppd_0 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___FCIziTxDWAnyHATWwZeMEmiXvdc_1)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_1() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_1(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_1 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___CdiTZueJOweHxLVLesdWcZkZxNV_3)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_3() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_3; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_3; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_3(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_3 = value;
	}

	inline static int32_t get_offset_of_kNupzNtNRIxDZONVUSwnnBWcpIT_4() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___kNupzNtNRIxDZONVUSwnnBWcpIT_4)); }
	inline int32_t get_kNupzNtNRIxDZONVUSwnnBWcpIT_4() const { return ___kNupzNtNRIxDZONVUSwnnBWcpIT_4; }
	inline int32_t* get_address_of_kNupzNtNRIxDZONVUSwnnBWcpIT_4() { return &___kNupzNtNRIxDZONVUSwnnBWcpIT_4; }
	inline void set_kNupzNtNRIxDZONVUSwnnBWcpIT_4(int32_t value)
	{
		___kNupzNtNRIxDZONVUSwnnBWcpIT_4 = value;
	}

	inline static int32_t get_offset_of_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5)); }
	inline int32_t get_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() const { return ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5; }
	inline int32_t* get_address_of_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() { return &___fAMwdVQKMkdvbVWXbkkCQoSadHid_5; }
	inline void set_fAMwdVQKMkdvbVWXbkkCQoSadHid_5(int32_t value)
	{
		___fAMwdVQKMkdvbVWXbkkCQoSadHid_5 = value;
	}

	inline static int32_t get_offset_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6)); }
	inline int32_t get_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() const { return ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline int32_t* get_address_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return &___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline void set_TXYPyQJDwYiLBxGVhCRAhruZxByr_6(int32_t value)
	{
		___TXYPyQJDwYiLBxGVhCRAhruZxByr_6 = value;
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_7() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___FrsWYgeIWMnjOcXDysagwZGcCMF_7)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_7() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_7; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_7() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_7; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_7(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_7 = value;
	}

	inline static int32_t get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_8() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___xTkhpClvSKvoiLfYNzcXIDURAPx_8)); }
	inline int32_t get_xTkhpClvSKvoiLfYNzcXIDURAPx_8() const { return ___xTkhpClvSKvoiLfYNzcXIDURAPx_8; }
	inline int32_t* get_address_of_xTkhpClvSKvoiLfYNzcXIDURAPx_8() { return &___xTkhpClvSKvoiLfYNzcXIDURAPx_8; }
	inline void set_xTkhpClvSKvoiLfYNzcXIDURAPx_8(int32_t value)
	{
		___xTkhpClvSKvoiLfYNzcXIDURAPx_8 = value;
	}

	inline static int32_t get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_9() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___UBcIxoTNwLQLduWHPwWDYgtuvif_9)); }
	inline int32_t get_UBcIxoTNwLQLduWHPwWDYgtuvif_9() const { return ___UBcIxoTNwLQLduWHPwWDYgtuvif_9; }
	inline int32_t* get_address_of_UBcIxoTNwLQLduWHPwWDYgtuvif_9() { return &___UBcIxoTNwLQLduWHPwWDYgtuvif_9; }
	inline void set_UBcIxoTNwLQLduWHPwWDYgtuvif_9(int32_t value)
	{
		___UBcIxoTNwLQLduWHPwWDYgtuvif_9 = value;
	}

	inline static int32_t get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___rUpHewPnhPrBuIDeIasWkyvRDkW_10)); }
	inline int32_t get_rUpHewPnhPrBuIDeIasWkyvRDkW_10() const { return ___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline int32_t* get_address_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return &___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline void set_rUpHewPnhPrBuIDeIasWkyvRDkW_10(int32_t value)
	{
		___rUpHewPnhPrBuIDeIasWkyvRDkW_10 = value;
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___LmADKPFcmVAiabETiHqQTGSgvmcg_11)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_11() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_11(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_11 = value;
	}

	inline static int32_t get_offset_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12)); }
	inline int32_t get_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() const { return ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12; }
	inline int32_t* get_address_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() { return &___magSqBEVFaJkoQfcAvXLVDzmcnZB_12; }
	inline void set_magSqBEVFaJkoQfcAvXLVDzmcnZB_12(int32_t value)
	{
		___magSqBEVFaJkoQfcAvXLVDzmcnZB_12 = value;
	}

	inline static int32_t get_offset_of_GfvPtFqTnCIcrWebHAQzqOoKceI_13() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___GfvPtFqTnCIcrWebHAQzqOoKceI_13)); }
	inline bool get_GfvPtFqTnCIcrWebHAQzqOoKceI_13() const { return ___GfvPtFqTnCIcrWebHAQzqOoKceI_13; }
	inline bool* get_address_of_GfvPtFqTnCIcrWebHAQzqOoKceI_13() { return &___GfvPtFqTnCIcrWebHAQzqOoKceI_13; }
	inline void set_GfvPtFqTnCIcrWebHAQzqOoKceI_13(bool value)
	{
		___GfvPtFqTnCIcrWebHAQzqOoKceI_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignmentConflictCheck
struct ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_marshaled_pinvoke
{
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	int32_t ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;
};
// Native definition for COM marshalling of Rewired.ElementAssignmentConflictCheck
struct ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_marshaled_com
{
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	int32_t ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;
};
#endif // ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#ifndef ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#define ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentConflictInfo
struct  ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D 
{
public:
	// System.Boolean Rewired.ElementAssignmentConflictInfo::rLCeavZDeONdAmfQvjLvpIKqsIQ
	bool ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	// System.Boolean Rewired.ElementAssignmentConflictInfo::uUoPCsKlKtgHCnSDBCrTHvhFbFXy
	bool ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	// Rewired.ControllerType Rewired.ElementAssignmentConflictInfo::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::kNupzNtNRIxDZONVUSwnnBWcpIT
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::TXYPyQJDwYiLBxGVhCRAhruZxByr
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	// Rewired.ControllerElementType Rewired.ElementAssignmentConflictInfo::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	// UnityEngine.KeyCode Rewired.ElementAssignmentConflictInfo::SkzHHHthNTcRwnaarBvUnkLqLeI
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignmentConflictInfo::rUpHewPnhPrBuIDeIasWkyvRDkW
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;

public:
	inline static int32_t get_offset_of_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0)); }
	inline bool get_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() const { return ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0; }
	inline bool* get_address_of_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() { return &___rLCeavZDeONdAmfQvjLvpIKqsIQ_0; }
	inline void set_rLCeavZDeONdAmfQvjLvpIKqsIQ_0(bool value)
	{
		___rLCeavZDeONdAmfQvjLvpIKqsIQ_0 = value;
	}

	inline static int32_t get_offset_of_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1)); }
	inline bool get_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() const { return ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1; }
	inline bool* get_address_of_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() { return &___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1; }
	inline void set_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1(bool value)
	{
		___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___FCIziTxDWAnyHATWwZeMEmiXvdc_2)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_2() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_2(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_2 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___CdiTZueJOweHxLVLesdWcZkZxNV_4)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_4() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_4; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_4; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_4(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_4 = value;
	}

	inline static int32_t get_offset_of_kNupzNtNRIxDZONVUSwnnBWcpIT_5() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___kNupzNtNRIxDZONVUSwnnBWcpIT_5)); }
	inline int32_t get_kNupzNtNRIxDZONVUSwnnBWcpIT_5() const { return ___kNupzNtNRIxDZONVUSwnnBWcpIT_5; }
	inline int32_t* get_address_of_kNupzNtNRIxDZONVUSwnnBWcpIT_5() { return &___kNupzNtNRIxDZONVUSwnnBWcpIT_5; }
	inline void set_kNupzNtNRIxDZONVUSwnnBWcpIT_5(int32_t value)
	{
		___kNupzNtNRIxDZONVUSwnnBWcpIT_5 = value;
	}

	inline static int32_t get_offset_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6)); }
	inline int32_t get_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() const { return ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline int32_t* get_address_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return &___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline void set_TXYPyQJDwYiLBxGVhCRAhruZxByr_6(int32_t value)
	{
		___TXYPyQJDwYiLBxGVhCRAhruZxByr_6 = value;
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_7() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___RmmmEkVblcqxoqGYhifgdSESkSn_7)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_7() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_7; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_7() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_7; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_7(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_7 = value;
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_8() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___FrsWYgeIWMnjOcXDysagwZGcCMF_8)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_8() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_8; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_8() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_8; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_8(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_8 = value;
	}

	inline static int32_t get_offset_of_SkzHHHthNTcRwnaarBvUnkLqLeI_9() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___SkzHHHthNTcRwnaarBvUnkLqLeI_9)); }
	inline int32_t get_SkzHHHthNTcRwnaarBvUnkLqLeI_9() const { return ___SkzHHHthNTcRwnaarBvUnkLqLeI_9; }
	inline int32_t* get_address_of_SkzHHHthNTcRwnaarBvUnkLqLeI_9() { return &___SkzHHHthNTcRwnaarBvUnkLqLeI_9; }
	inline void set_SkzHHHthNTcRwnaarBvUnkLqLeI_9(int32_t value)
	{
		___SkzHHHthNTcRwnaarBvUnkLqLeI_9 = value;
	}

	inline static int32_t get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___rUpHewPnhPrBuIDeIasWkyvRDkW_10)); }
	inline int32_t get_rUpHewPnhPrBuIDeIasWkyvRDkW_10() const { return ___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline int32_t* get_address_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return &___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline void set_rUpHewPnhPrBuIDeIasWkyvRDkW_10(int32_t value)
	{
		___rUpHewPnhPrBuIDeIasWkyvRDkW_10 = value;
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___LmADKPFcmVAiabETiHqQTGSgvmcg_11)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_11() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_11(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignmentConflictInfo
struct ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_marshaled_pinvoke
{
	int32_t ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	int32_t ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
};
// Native definition for COM marshalling of Rewired.ElementAssignmentConflictInfo
struct ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_marshaled_com
{
	int32_t ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	int32_t ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
};
#endif // ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#ifndef ELEMENTASSIGNMENTINFO_T72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF_H
#define ELEMENTASSIGNMENTINFO_T72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentInfo
struct  ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF  : public RuntimeObject
{
public:
	// Rewired.ControllerMap Rewired.ElementAssignmentInfo::uNIXhFwhKcbRMaCTeDSCheRwjLdb
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_0;
	// Rewired.ControllerElementType Rewired.ElementAssignmentInfo::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_1;
	// System.Int32 Rewired.ElementAssignmentInfo::ljTHPqmrucaukQyhtWjEKicsbql
	int32_t ___ljTHPqmrucaukQyhtWjEKicsbql_2;
	// System.Int32 Rewired.ElementAssignmentInfo::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_3;
	// Rewired.AxisRange Rewired.ElementAssignmentInfo::xTkhpClvSKvoiLfYNzcXIDURAPx
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_4;
	// UnityEngine.KeyCode Rewired.ElementAssignmentInfo::SkzHHHthNTcRwnaarBvUnkLqLeI
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_5;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignmentInfo::rUpHewPnhPrBuIDeIasWkyvRDkW
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_6;
	// System.Int32 Rewired.ElementAssignmentInfo::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_7;
	// Rewired.Pole Rewired.ElementAssignmentInfo::magSqBEVFaJkoQfcAvXLVDzmcnZB
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_8;
	// System.Boolean Rewired.ElementAssignmentInfo::GfvPtFqTnCIcrWebHAQzqOoKceI
	bool ___GfvPtFqTnCIcrWebHAQzqOoKceI_9;

public:
	inline static int32_t get_offset_of_uNIXhFwhKcbRMaCTeDSCheRwjLdb_0() { return static_cast<int32_t>(offsetof(ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF, ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_0)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_uNIXhFwhKcbRMaCTeDSCheRwjLdb_0() const { return ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_0; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_uNIXhFwhKcbRMaCTeDSCheRwjLdb_0() { return &___uNIXhFwhKcbRMaCTeDSCheRwjLdb_0; }
	inline void set_uNIXhFwhKcbRMaCTeDSCheRwjLdb_0(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___uNIXhFwhKcbRMaCTeDSCheRwjLdb_0 = value;
		Il2CppCodeGenWriteBarrier((&___uNIXhFwhKcbRMaCTeDSCheRwjLdb_0), value);
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_1() { return static_cast<int32_t>(offsetof(ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF, ___RmmmEkVblcqxoqGYhifgdSESkSn_1)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_1() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_1; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_1() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_1; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_1(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_1 = value;
	}

	inline static int32_t get_offset_of_ljTHPqmrucaukQyhtWjEKicsbql_2() { return static_cast<int32_t>(offsetof(ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF, ___ljTHPqmrucaukQyhtWjEKicsbql_2)); }
	inline int32_t get_ljTHPqmrucaukQyhtWjEKicsbql_2() const { return ___ljTHPqmrucaukQyhtWjEKicsbql_2; }
	inline int32_t* get_address_of_ljTHPqmrucaukQyhtWjEKicsbql_2() { return &___ljTHPqmrucaukQyhtWjEKicsbql_2; }
	inline void set_ljTHPqmrucaukQyhtWjEKicsbql_2(int32_t value)
	{
		___ljTHPqmrucaukQyhtWjEKicsbql_2 = value;
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_3() { return static_cast<int32_t>(offsetof(ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF, ___FrsWYgeIWMnjOcXDysagwZGcCMF_3)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_3() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_3; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_3() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_3; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_3(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_3 = value;
	}

	inline static int32_t get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_4() { return static_cast<int32_t>(offsetof(ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF, ___xTkhpClvSKvoiLfYNzcXIDURAPx_4)); }
	inline int32_t get_xTkhpClvSKvoiLfYNzcXIDURAPx_4() const { return ___xTkhpClvSKvoiLfYNzcXIDURAPx_4; }
	inline int32_t* get_address_of_xTkhpClvSKvoiLfYNzcXIDURAPx_4() { return &___xTkhpClvSKvoiLfYNzcXIDURAPx_4; }
	inline void set_xTkhpClvSKvoiLfYNzcXIDURAPx_4(int32_t value)
	{
		___xTkhpClvSKvoiLfYNzcXIDURAPx_4 = value;
	}

	inline static int32_t get_offset_of_SkzHHHthNTcRwnaarBvUnkLqLeI_5() { return static_cast<int32_t>(offsetof(ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF, ___SkzHHHthNTcRwnaarBvUnkLqLeI_5)); }
	inline int32_t get_SkzHHHthNTcRwnaarBvUnkLqLeI_5() const { return ___SkzHHHthNTcRwnaarBvUnkLqLeI_5; }
	inline int32_t* get_address_of_SkzHHHthNTcRwnaarBvUnkLqLeI_5() { return &___SkzHHHthNTcRwnaarBvUnkLqLeI_5; }
	inline void set_SkzHHHthNTcRwnaarBvUnkLqLeI_5(int32_t value)
	{
		___SkzHHHthNTcRwnaarBvUnkLqLeI_5 = value;
	}

	inline static int32_t get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_6() { return static_cast<int32_t>(offsetof(ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF, ___rUpHewPnhPrBuIDeIasWkyvRDkW_6)); }
	inline int32_t get_rUpHewPnhPrBuIDeIasWkyvRDkW_6() const { return ___rUpHewPnhPrBuIDeIasWkyvRDkW_6; }
	inline int32_t* get_address_of_rUpHewPnhPrBuIDeIasWkyvRDkW_6() { return &___rUpHewPnhPrBuIDeIasWkyvRDkW_6; }
	inline void set_rUpHewPnhPrBuIDeIasWkyvRDkW_6(int32_t value)
	{
		___rUpHewPnhPrBuIDeIasWkyvRDkW_6 = value;
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_7() { return static_cast<int32_t>(offsetof(ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF, ___LmADKPFcmVAiabETiHqQTGSgvmcg_7)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_7() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_7; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_7() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_7; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_7(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_7 = value;
	}

	inline static int32_t get_offset_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_8() { return static_cast<int32_t>(offsetof(ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF, ___magSqBEVFaJkoQfcAvXLVDzmcnZB_8)); }
	inline int32_t get_magSqBEVFaJkoQfcAvXLVDzmcnZB_8() const { return ___magSqBEVFaJkoQfcAvXLVDzmcnZB_8; }
	inline int32_t* get_address_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_8() { return &___magSqBEVFaJkoQfcAvXLVDzmcnZB_8; }
	inline void set_magSqBEVFaJkoQfcAvXLVDzmcnZB_8(int32_t value)
	{
		___magSqBEVFaJkoQfcAvXLVDzmcnZB_8 = value;
	}

	inline static int32_t get_offset_of_GfvPtFqTnCIcrWebHAQzqOoKceI_9() { return static_cast<int32_t>(offsetof(ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF, ___GfvPtFqTnCIcrWebHAQzqOoKceI_9)); }
	inline bool get_GfvPtFqTnCIcrWebHAQzqOoKceI_9() const { return ___GfvPtFqTnCIcrWebHAQzqOoKceI_9; }
	inline bool* get_address_of_GfvPtFqTnCIcrWebHAQzqOoKceI_9() { return &___GfvPtFqTnCIcrWebHAQzqOoKceI_9; }
	inline void set_GfvPtFqTnCIcrWebHAQzqOoKceI_9(bool value)
	{
		___GfvPtFqTnCIcrWebHAQzqOoKceI_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTASSIGNMENTINFO_T72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF_H
#ifndef HARDWARECONTROLLERMAPIDENTIFIER_T7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4_H
#define HARDWARECONTROLLERMAPIDENTIFIER_T7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HardwareControllerMapIdentifier
struct  HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4 
{
public:
	// System.Guid Rewired.HardwareControllerMapIdentifier::guid
	Guid_t  ___guid_0;
	// Rewired.InputSource Rewired.HardwareControllerMapIdentifier::inputSource
	int32_t ___inputSource_1;
	// Rewired.InputPlatform Rewired.HardwareControllerMapIdentifier::actualInputPlatform
	int32_t ___actualInputPlatform_2;
	// System.Int32 Rewired.HardwareControllerMapIdentifier::variantIndex
	int32_t ___variantIndex_3;

public:
	inline static int32_t get_offset_of_guid_0() { return static_cast<int32_t>(offsetof(HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4, ___guid_0)); }
	inline Guid_t  get_guid_0() const { return ___guid_0; }
	inline Guid_t * get_address_of_guid_0() { return &___guid_0; }
	inline void set_guid_0(Guid_t  value)
	{
		___guid_0 = value;
	}

	inline static int32_t get_offset_of_inputSource_1() { return static_cast<int32_t>(offsetof(HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4, ___inputSource_1)); }
	inline int32_t get_inputSource_1() const { return ___inputSource_1; }
	inline int32_t* get_address_of_inputSource_1() { return &___inputSource_1; }
	inline void set_inputSource_1(int32_t value)
	{
		___inputSource_1 = value;
	}

	inline static int32_t get_offset_of_actualInputPlatform_2() { return static_cast<int32_t>(offsetof(HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4, ___actualInputPlatform_2)); }
	inline int32_t get_actualInputPlatform_2() const { return ___actualInputPlatform_2; }
	inline int32_t* get_address_of_actualInputPlatform_2() { return &___actualInputPlatform_2; }
	inline void set_actualInputPlatform_2(int32_t value)
	{
		___actualInputPlatform_2 = value;
	}

	inline static int32_t get_offset_of_variantIndex_3() { return static_cast<int32_t>(offsetof(HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4, ___variantIndex_3)); }
	inline int32_t get_variantIndex_3() const { return ___variantIndex_3; }
	inline int32_t* get_address_of_variantIndex_3() { return &___variantIndex_3; }
	inline void set_variantIndex_3(int32_t value)
	{
		___variantIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWARECONTROLLERMAPIDENTIFIER_T7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4_H
#ifndef INPUTACTIONEVENTDATA_TDC602553D1EFE9CF156BED6DF092B26377625E82_H
#define INPUTACTIONEVENTDATA_TDC602553D1EFE9CF156BED6DF092B26377625E82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputActionEventData
struct  InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82 
{
public:
	// OmePuLXqingOShAfdEjGZVdOnDpG Rewired.InputActionEventData::nOyACFHXpjgPWDTOdoqXegjcnvRy
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708 * ___nOyACFHXpjgPWDTOdoqXegjcnvRy_0;
	// Rewired.InputActionEventType Rewired.InputActionEventData::rvRtCzapOdDtBhWRBVphGbFutZU
	int32_t ___rvRtCzapOdDtBhWRBVphGbFutZU_1;
	// System.Int32 Rewired.InputActionEventData::playerId
	int32_t ___playerId_2;
	// System.Int32 Rewired.InputActionEventData::actionId
	int32_t ___actionId_3;
	// Rewired.UpdateLoopType Rewired.InputActionEventData::updateLoop
	int32_t ___updateLoop_4;

public:
	inline static int32_t get_offset_of_nOyACFHXpjgPWDTOdoqXegjcnvRy_0() { return static_cast<int32_t>(offsetof(InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82, ___nOyACFHXpjgPWDTOdoqXegjcnvRy_0)); }
	inline OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708 * get_nOyACFHXpjgPWDTOdoqXegjcnvRy_0() const { return ___nOyACFHXpjgPWDTOdoqXegjcnvRy_0; }
	inline OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708 ** get_address_of_nOyACFHXpjgPWDTOdoqXegjcnvRy_0() { return &___nOyACFHXpjgPWDTOdoqXegjcnvRy_0; }
	inline void set_nOyACFHXpjgPWDTOdoqXegjcnvRy_0(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708 * value)
	{
		___nOyACFHXpjgPWDTOdoqXegjcnvRy_0 = value;
		Il2CppCodeGenWriteBarrier((&___nOyACFHXpjgPWDTOdoqXegjcnvRy_0), value);
	}

	inline static int32_t get_offset_of_rvRtCzapOdDtBhWRBVphGbFutZU_1() { return static_cast<int32_t>(offsetof(InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82, ___rvRtCzapOdDtBhWRBVphGbFutZU_1)); }
	inline int32_t get_rvRtCzapOdDtBhWRBVphGbFutZU_1() const { return ___rvRtCzapOdDtBhWRBVphGbFutZU_1; }
	inline int32_t* get_address_of_rvRtCzapOdDtBhWRBVphGbFutZU_1() { return &___rvRtCzapOdDtBhWRBVphGbFutZU_1; }
	inline void set_rvRtCzapOdDtBhWRBVphGbFutZU_1(int32_t value)
	{
		___rvRtCzapOdDtBhWRBVphGbFutZU_1 = value;
	}

	inline static int32_t get_offset_of_playerId_2() { return static_cast<int32_t>(offsetof(InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82, ___playerId_2)); }
	inline int32_t get_playerId_2() const { return ___playerId_2; }
	inline int32_t* get_address_of_playerId_2() { return &___playerId_2; }
	inline void set_playerId_2(int32_t value)
	{
		___playerId_2 = value;
	}

	inline static int32_t get_offset_of_actionId_3() { return static_cast<int32_t>(offsetof(InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82, ___actionId_3)); }
	inline int32_t get_actionId_3() const { return ___actionId_3; }
	inline int32_t* get_address_of_actionId_3() { return &___actionId_3; }
	inline void set_actionId_3(int32_t value)
	{
		___actionId_3 = value;
	}

	inline static int32_t get_offset_of_updateLoop_4() { return static_cast<int32_t>(offsetof(InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82, ___updateLoop_4)); }
	inline int32_t get_updateLoop_4() const { return ___updateLoop_4; }
	inline int32_t* get_address_of_updateLoop_4() { return &___updateLoop_4; }
	inline void set_updateLoop_4(int32_t value)
	{
		___updateLoop_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.InputActionEventData
struct InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82_marshaled_pinvoke
{
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708 * ___nOyACFHXpjgPWDTOdoqXegjcnvRy_0;
	int32_t ___rvRtCzapOdDtBhWRBVphGbFutZU_1;
	int32_t ___playerId_2;
	int32_t ___actionId_3;
	int32_t ___updateLoop_4;
};
// Native definition for COM marshalling of Rewired.InputActionEventData
struct InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82_marshaled_com
{
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708 * ___nOyACFHXpjgPWDTOdoqXegjcnvRy_0;
	int32_t ___rvRtCzapOdDtBhWRBVphGbFutZU_1;
	int32_t ___playerId_2;
	int32_t ___actionId_3;
	int32_t ___updateLoop_4;
};
#endif // INPUTACTIONEVENTDATA_TDC602553D1EFE9CF156BED6DF092B26377625E82_H
#ifndef UPDATELOOPDATASET_1_TB820B43DB3CAAA2A3C73409971EEEB825B29EFEF_H
#define UPDATELOOPDATASET_1_TB820B43DB3CAAA2A3C73409971EEEB825B29EFEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UpdateLoopDataSet`1<Rewired.ButtonLoopSet_ButtonData>
struct  UpdateLoopDataSet_1_tB820B43DB3CAAA2A3C73409971EEEB825B29EFEF  : public RuntimeObject
{
public:
	// Rewired.UpdateLoopDataSet`1_fqOEnqCjzfqsSLfWbeeEWCDwDptc<T> Rewired.UpdateLoopDataSet`1::vOthzrjKkrHgxAcjSOqRNrOIOaj
	fqOEnqCjzfqsSLfWbeeEWCDwDptc_t94EF923B215415665C576F97032CD5E66CA20199 * ___vOthzrjKkrHgxAcjSOqRNrOIOaj_1;
	// System.Int32 Rewired.UpdateLoopDataSet`1::XsLjGWgCZGERykRkxPhPXStkauEY
	int32_t ___XsLjGWgCZGERykRkxPhPXStkauEY_2;
	// System.Int32 Rewired.UpdateLoopDataSet`1::fixedUpdateSetIndex
	int32_t ___fixedUpdateSetIndex_3;
	// System.Int32[] Rewired.UpdateLoopDataSet`1::UtyHrsDgBPEcHKmyNNRLLGUMtoub
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___UtyHrsDgBPEcHKmyNNRLLGUMtoub_4;
	// Rewired.UpdateLoopDataSet`1_fqOEnqCjzfqsSLfWbeeEWCDwDptc<T>[] Rewired.UpdateLoopDataSet`1::PwPjYSFGGMniIMfHkZVcHaYxjMi
	fqOEnqCjzfqsSLfWbeeEWCDwDptcU5BU5D_tA52DDE8E221AAE179D957A7BD3D0BF3F44580235* ___PwPjYSFGGMniIMfHkZVcHaYxjMi_5;
	// Rewired.UpdateLoopType Rewired.UpdateLoopDataSet`1::XpAdVXcoZZMaSjvODIcAqLfBoBgZ
	int32_t ___XpAdVXcoZZMaSjvODIcAqLfBoBgZ_6;

public:
	inline static int32_t get_offset_of_vOthzrjKkrHgxAcjSOqRNrOIOaj_1() { return static_cast<int32_t>(offsetof(UpdateLoopDataSet_1_tB820B43DB3CAAA2A3C73409971EEEB825B29EFEF, ___vOthzrjKkrHgxAcjSOqRNrOIOaj_1)); }
	inline fqOEnqCjzfqsSLfWbeeEWCDwDptc_t94EF923B215415665C576F97032CD5E66CA20199 * get_vOthzrjKkrHgxAcjSOqRNrOIOaj_1() const { return ___vOthzrjKkrHgxAcjSOqRNrOIOaj_1; }
	inline fqOEnqCjzfqsSLfWbeeEWCDwDptc_t94EF923B215415665C576F97032CD5E66CA20199 ** get_address_of_vOthzrjKkrHgxAcjSOqRNrOIOaj_1() { return &___vOthzrjKkrHgxAcjSOqRNrOIOaj_1; }
	inline void set_vOthzrjKkrHgxAcjSOqRNrOIOaj_1(fqOEnqCjzfqsSLfWbeeEWCDwDptc_t94EF923B215415665C576F97032CD5E66CA20199 * value)
	{
		___vOthzrjKkrHgxAcjSOqRNrOIOaj_1 = value;
		Il2CppCodeGenWriteBarrier((&___vOthzrjKkrHgxAcjSOqRNrOIOaj_1), value);
	}

	inline static int32_t get_offset_of_XsLjGWgCZGERykRkxPhPXStkauEY_2() { return static_cast<int32_t>(offsetof(UpdateLoopDataSet_1_tB820B43DB3CAAA2A3C73409971EEEB825B29EFEF, ___XsLjGWgCZGERykRkxPhPXStkauEY_2)); }
	inline int32_t get_XsLjGWgCZGERykRkxPhPXStkauEY_2() const { return ___XsLjGWgCZGERykRkxPhPXStkauEY_2; }
	inline int32_t* get_address_of_XsLjGWgCZGERykRkxPhPXStkauEY_2() { return &___XsLjGWgCZGERykRkxPhPXStkauEY_2; }
	inline void set_XsLjGWgCZGERykRkxPhPXStkauEY_2(int32_t value)
	{
		___XsLjGWgCZGERykRkxPhPXStkauEY_2 = value;
	}

	inline static int32_t get_offset_of_fixedUpdateSetIndex_3() { return static_cast<int32_t>(offsetof(UpdateLoopDataSet_1_tB820B43DB3CAAA2A3C73409971EEEB825B29EFEF, ___fixedUpdateSetIndex_3)); }
	inline int32_t get_fixedUpdateSetIndex_3() const { return ___fixedUpdateSetIndex_3; }
	inline int32_t* get_address_of_fixedUpdateSetIndex_3() { return &___fixedUpdateSetIndex_3; }
	inline void set_fixedUpdateSetIndex_3(int32_t value)
	{
		___fixedUpdateSetIndex_3 = value;
	}

	inline static int32_t get_offset_of_UtyHrsDgBPEcHKmyNNRLLGUMtoub_4() { return static_cast<int32_t>(offsetof(UpdateLoopDataSet_1_tB820B43DB3CAAA2A3C73409971EEEB825B29EFEF, ___UtyHrsDgBPEcHKmyNNRLLGUMtoub_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_UtyHrsDgBPEcHKmyNNRLLGUMtoub_4() const { return ___UtyHrsDgBPEcHKmyNNRLLGUMtoub_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_UtyHrsDgBPEcHKmyNNRLLGUMtoub_4() { return &___UtyHrsDgBPEcHKmyNNRLLGUMtoub_4; }
	inline void set_UtyHrsDgBPEcHKmyNNRLLGUMtoub_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___UtyHrsDgBPEcHKmyNNRLLGUMtoub_4 = value;
		Il2CppCodeGenWriteBarrier((&___UtyHrsDgBPEcHKmyNNRLLGUMtoub_4), value);
	}

	inline static int32_t get_offset_of_PwPjYSFGGMniIMfHkZVcHaYxjMi_5() { return static_cast<int32_t>(offsetof(UpdateLoopDataSet_1_tB820B43DB3CAAA2A3C73409971EEEB825B29EFEF, ___PwPjYSFGGMniIMfHkZVcHaYxjMi_5)); }
	inline fqOEnqCjzfqsSLfWbeeEWCDwDptcU5BU5D_tA52DDE8E221AAE179D957A7BD3D0BF3F44580235* get_PwPjYSFGGMniIMfHkZVcHaYxjMi_5() const { return ___PwPjYSFGGMniIMfHkZVcHaYxjMi_5; }
	inline fqOEnqCjzfqsSLfWbeeEWCDwDptcU5BU5D_tA52DDE8E221AAE179D957A7BD3D0BF3F44580235** get_address_of_PwPjYSFGGMniIMfHkZVcHaYxjMi_5() { return &___PwPjYSFGGMniIMfHkZVcHaYxjMi_5; }
	inline void set_PwPjYSFGGMniIMfHkZVcHaYxjMi_5(fqOEnqCjzfqsSLfWbeeEWCDwDptcU5BU5D_tA52DDE8E221AAE179D957A7BD3D0BF3F44580235* value)
	{
		___PwPjYSFGGMniIMfHkZVcHaYxjMi_5 = value;
		Il2CppCodeGenWriteBarrier((&___PwPjYSFGGMniIMfHkZVcHaYxjMi_5), value);
	}

	inline static int32_t get_offset_of_XpAdVXcoZZMaSjvODIcAqLfBoBgZ_6() { return static_cast<int32_t>(offsetof(UpdateLoopDataSet_1_tB820B43DB3CAAA2A3C73409971EEEB825B29EFEF, ___XpAdVXcoZZMaSjvODIcAqLfBoBgZ_6)); }
	inline int32_t get_XpAdVXcoZZMaSjvODIcAqLfBoBgZ_6() const { return ___XpAdVXcoZZMaSjvODIcAqLfBoBgZ_6; }
	inline int32_t* get_address_of_XpAdVXcoZZMaSjvODIcAqLfBoBgZ_6() { return &___XpAdVXcoZZMaSjvODIcAqLfBoBgZ_6; }
	inline void set_XpAdVXcoZZMaSjvODIcAqLfBoBgZ_6(int32_t value)
	{
		___XpAdVXcoZZMaSjvODIcAqLfBoBgZ_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATELOOPDATASET_1_TB820B43DB3CAAA2A3C73409971EEEB825B29EFEF_H
#ifndef SERIALIZATIONTYPEATTRIBUTE_T6A8F027F04EB9964418162F70013325B17AE0163_H
#define SERIALIZATIONTYPEATTRIBUTE_T6A8F027F04EB9964418162F70013325B17AE0163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Attributes.SerializationTypeAttribute
struct  SerializationTypeAttribute_t6A8F027F04EB9964418162F70013325B17AE0163  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// Rewired.Utils.Attributes.SerializationTypeAttribute_SerializationType Rewired.Utils.Attributes.SerializationTypeAttribute::_serializationType
	int32_t ____serializationType_0;

public:
	inline static int32_t get_offset_of__serializationType_0() { return static_cast<int32_t>(offsetof(SerializationTypeAttribute_t6A8F027F04EB9964418162F70013325B17AE0163, ____serializationType_0)); }
	inline int32_t get__serializationType_0() const { return ____serializationType_0; }
	inline int32_t* get_address_of__serializationType_0() { return &____serializationType_0; }
	inline void set__serializationType_0(int32_t value)
	{
		____serializationType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONTYPEATTRIBUTE_T6A8F027F04EB9964418162F70013325B17AE0163_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef HNDTWHYNYTHUVNZEISBTEIWDIAS_TC170FDA23FBA3F97C33BC5772C746C3A03F375B5_H
#define HNDTWHYNYTHUVNZEISBTEIWDIAS_TC170FDA23FBA3F97C33BC5772C746C3A03F375B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iNmAhOSEGUcqVFxmGFasdFHfdnr_HNdTWHYnytHuvNZEIsbtEiWDias
struct  HNdTWHYnytHuvNZEIsbtEiWDias_tC170FDA23FBA3F97C33BC5772C746C3A03F375B5  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.InputActionEventData> iNmAhOSEGUcqVFxmGFasdFHfdnr_HNdTWHYnytHuvNZEIsbtEiWDias::ekrwGUjbOGqAHCaagByDbWSadpQb
	Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * ___ekrwGUjbOGqAHCaagByDbWSadpQb_0;
	// Rewired.UpdateLoopType iNmAhOSEGUcqVFxmGFasdFHfdnr_HNdTWHYnytHuvNZEIsbtEiWDias::TWKNVXwJTlddHEpGDbHCUyznGXe
	int32_t ___TWKNVXwJTlddHEpGDbHCUyznGXe_1;
	// Rewired.InputActionEventType iNmAhOSEGUcqVFxmGFasdFHfdnr_HNdTWHYnytHuvNZEIsbtEiWDias::SKPOVoCvhMuMjlLtGtXsXLVxqih
	int32_t ___SKPOVoCvhMuMjlLtGtXsXLVxqih_2;

public:
	inline static int32_t get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return static_cast<int32_t>(offsetof(HNdTWHYnytHuvNZEIsbtEiWDias_tC170FDA23FBA3F97C33BC5772C746C3A03F375B5, ___ekrwGUjbOGqAHCaagByDbWSadpQb_0)); }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * get_ekrwGUjbOGqAHCaagByDbWSadpQb_0() const { return ___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 ** get_address_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return &___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline void set_ekrwGUjbOGqAHCaagByDbWSadpQb_0(Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * value)
	{
		___ekrwGUjbOGqAHCaagByDbWSadpQb_0 = value;
		Il2CppCodeGenWriteBarrier((&___ekrwGUjbOGqAHCaagByDbWSadpQb_0), value);
	}

	inline static int32_t get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1() { return static_cast<int32_t>(offsetof(HNdTWHYnytHuvNZEIsbtEiWDias_tC170FDA23FBA3F97C33BC5772C746C3A03F375B5, ___TWKNVXwJTlddHEpGDbHCUyznGXe_1)); }
	inline int32_t get_TWKNVXwJTlddHEpGDbHCUyznGXe_1() const { return ___TWKNVXwJTlddHEpGDbHCUyznGXe_1; }
	inline int32_t* get_address_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1() { return &___TWKNVXwJTlddHEpGDbHCUyznGXe_1; }
	inline void set_TWKNVXwJTlddHEpGDbHCUyznGXe_1(int32_t value)
	{
		___TWKNVXwJTlddHEpGDbHCUyznGXe_1 = value;
	}

	inline static int32_t get_offset_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_2() { return static_cast<int32_t>(offsetof(HNdTWHYnytHuvNZEIsbtEiWDias_tC170FDA23FBA3F97C33BC5772C746C3A03F375B5, ___SKPOVoCvhMuMjlLtGtXsXLVxqih_2)); }
	inline int32_t get_SKPOVoCvhMuMjlLtGtXsXLVxqih_2() const { return ___SKPOVoCvhMuMjlLtGtXsXLVxqih_2; }
	inline int32_t* get_address_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_2() { return &___SKPOVoCvhMuMjlLtGtXsXLVxqih_2; }
	inline void set_SKPOVoCvhMuMjlLtGtXsXLVxqih_2(int32_t value)
	{
		___SKPOVoCvhMuMjlLtGtXsXLVxqih_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HNDTWHYNYTHUVNZEISBTEIWDIAS_TC170FDA23FBA3F97C33BC5772C746C3A03F375B5_H
#ifndef NRCNGIFSTASSVDCVGKIZDSMVDSZ_T30324632229C71C3C3959B9AB8C431CFFED6A432_H
#define NRCNGIFSTASSVDCVGKIZDSMVDSZ_T30324632229C71C3C3959B9AB8C431CFFED6A432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iNmAhOSEGUcqVFxmGFasdFHfdnr_NrCngiFStASsVDcVGkiZDSMVDsZ
struct  NrCngiFStASsVDcVGkiZDSMVDsZ_t30324632229C71C3C3959B9AB8C431CFFED6A432  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.InputActionEventData> iNmAhOSEGUcqVFxmGFasdFHfdnr_NrCngiFStASsVDcVGkiZDSMVDsZ::ekrwGUjbOGqAHCaagByDbWSadpQb
	Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * ___ekrwGUjbOGqAHCaagByDbWSadpQb_0;
	// Rewired.InputActionEventType iNmAhOSEGUcqVFxmGFasdFHfdnr_NrCngiFStASsVDcVGkiZDSMVDsZ::SKPOVoCvhMuMjlLtGtXsXLVxqih
	int32_t ___SKPOVoCvhMuMjlLtGtXsXLVxqih_1;
	// System.Int32 iNmAhOSEGUcqVFxmGFasdFHfdnr_NrCngiFStASsVDcVGkiZDSMVDsZ::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_2;

public:
	inline static int32_t get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return static_cast<int32_t>(offsetof(NrCngiFStASsVDcVGkiZDSMVDsZ_t30324632229C71C3C3959B9AB8C431CFFED6A432, ___ekrwGUjbOGqAHCaagByDbWSadpQb_0)); }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * get_ekrwGUjbOGqAHCaagByDbWSadpQb_0() const { return ___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 ** get_address_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return &___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline void set_ekrwGUjbOGqAHCaagByDbWSadpQb_0(Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * value)
	{
		___ekrwGUjbOGqAHCaagByDbWSadpQb_0 = value;
		Il2CppCodeGenWriteBarrier((&___ekrwGUjbOGqAHCaagByDbWSadpQb_0), value);
	}

	inline static int32_t get_offset_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_1() { return static_cast<int32_t>(offsetof(NrCngiFStASsVDcVGkiZDSMVDsZ_t30324632229C71C3C3959B9AB8C431CFFED6A432, ___SKPOVoCvhMuMjlLtGtXsXLVxqih_1)); }
	inline int32_t get_SKPOVoCvhMuMjlLtGtXsXLVxqih_1() const { return ___SKPOVoCvhMuMjlLtGtXsXLVxqih_1; }
	inline int32_t* get_address_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_1() { return &___SKPOVoCvhMuMjlLtGtXsXLVxqih_1; }
	inline void set_SKPOVoCvhMuMjlLtGtXsXLVxqih_1(int32_t value)
	{
		___SKPOVoCvhMuMjlLtGtXsXLVxqih_1 = value;
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_2() { return static_cast<int32_t>(offsetof(NrCngiFStASsVDcVGkiZDSMVDsZ_t30324632229C71C3C3959B9AB8C431CFFED6A432, ___pTVJDeVeJlQThAZkFztHqDAWDUa_2)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_2() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_2; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_2() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_2; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_2(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NRCNGIFSTASSVDCVGKIZDSMVDSZ_T30324632229C71C3C3959B9AB8C431CFFED6A432_H
#ifndef ZJQLMQABXQOFIOBTMTEIZIFPZRJ_TB78186632CF73E7D0D4E9C0CA3BD76D40E70155C_H
#define ZJQLMQABXQOFIOBTMTEIZIFPZRJ_TB78186632CF73E7D0D4E9C0CA3BD76D40E70155C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iNmAhOSEGUcqVFxmGFasdFHfdnr_ZjQLmQAbXQOFiObTMteizIfPzRJ
struct  ZjQLmQAbXQOFiObTMteizIfPzRJ_tB78186632CF73E7D0D4E9C0CA3BD76D40E70155C  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.InputActionEventData> iNmAhOSEGUcqVFxmGFasdFHfdnr_ZjQLmQAbXQOFiObTMteizIfPzRJ::ekrwGUjbOGqAHCaagByDbWSadpQb
	Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * ___ekrwGUjbOGqAHCaagByDbWSadpQb_0;
	// Rewired.UpdateLoopType iNmAhOSEGUcqVFxmGFasdFHfdnr_ZjQLmQAbXQOFiObTMteizIfPzRJ::TWKNVXwJTlddHEpGDbHCUyznGXe
	int32_t ___TWKNVXwJTlddHEpGDbHCUyznGXe_1;
	// System.Int32 iNmAhOSEGUcqVFxmGFasdFHfdnr_ZjQLmQAbXQOFiObTMteizIfPzRJ::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_2;

public:
	inline static int32_t get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return static_cast<int32_t>(offsetof(ZjQLmQAbXQOFiObTMteizIfPzRJ_tB78186632CF73E7D0D4E9C0CA3BD76D40E70155C, ___ekrwGUjbOGqAHCaagByDbWSadpQb_0)); }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * get_ekrwGUjbOGqAHCaagByDbWSadpQb_0() const { return ___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 ** get_address_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return &___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline void set_ekrwGUjbOGqAHCaagByDbWSadpQb_0(Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * value)
	{
		___ekrwGUjbOGqAHCaagByDbWSadpQb_0 = value;
		Il2CppCodeGenWriteBarrier((&___ekrwGUjbOGqAHCaagByDbWSadpQb_0), value);
	}

	inline static int32_t get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1() { return static_cast<int32_t>(offsetof(ZjQLmQAbXQOFiObTMteizIfPzRJ_tB78186632CF73E7D0D4E9C0CA3BD76D40E70155C, ___TWKNVXwJTlddHEpGDbHCUyznGXe_1)); }
	inline int32_t get_TWKNVXwJTlddHEpGDbHCUyznGXe_1() const { return ___TWKNVXwJTlddHEpGDbHCUyznGXe_1; }
	inline int32_t* get_address_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1() { return &___TWKNVXwJTlddHEpGDbHCUyznGXe_1; }
	inline void set_TWKNVXwJTlddHEpGDbHCUyznGXe_1(int32_t value)
	{
		___TWKNVXwJTlddHEpGDbHCUyznGXe_1 = value;
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_2() { return static_cast<int32_t>(offsetof(ZjQLmQAbXQOFiObTMteizIfPzRJ_tB78186632CF73E7D0D4E9C0CA3BD76D40E70155C, ___pTVJDeVeJlQThAZkFztHqDAWDUa_2)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_2() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_2; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_2() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_2; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_2(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZJQLMQABXQOFIOBTMTEIZIFPZRJ_TB78186632CF73E7D0D4E9C0CA3BD76D40E70155C_H
#ifndef HPJUHYOOUKVQYOALQDLYCCDDHPSQ_T0B149D9BB277329A45ECFC663C989860A6F478F2_H
#define HPJUHYOOUKVQYOALQDLYCCDDHPSQ_T0B149D9BB277329A45ECFC663C989860A6F478F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iNmAhOSEGUcqVFxmGFasdFHfdnr_hPJUHyoOukVQYoALQdLyCcdDHpsQ
struct  hPJUHyoOukVQYoALQdLyCcdDHpsQ_t0B149D9BB277329A45ECFC663C989860A6F478F2  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.InputActionEventData> iNmAhOSEGUcqVFxmGFasdFHfdnr_hPJUHyoOukVQYoALQdLyCcdDHpsQ::ekrwGUjbOGqAHCaagByDbWSadpQb
	Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * ___ekrwGUjbOGqAHCaagByDbWSadpQb_0;
	// Rewired.UpdateLoopType iNmAhOSEGUcqVFxmGFasdFHfdnr_hPJUHyoOukVQYoALQdLyCcdDHpsQ::TWKNVXwJTlddHEpGDbHCUyznGXe
	int32_t ___TWKNVXwJTlddHEpGDbHCUyznGXe_1;

public:
	inline static int32_t get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return static_cast<int32_t>(offsetof(hPJUHyoOukVQYoALQdLyCcdDHpsQ_t0B149D9BB277329A45ECFC663C989860A6F478F2, ___ekrwGUjbOGqAHCaagByDbWSadpQb_0)); }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * get_ekrwGUjbOGqAHCaagByDbWSadpQb_0() const { return ___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 ** get_address_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return &___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline void set_ekrwGUjbOGqAHCaagByDbWSadpQb_0(Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * value)
	{
		___ekrwGUjbOGqAHCaagByDbWSadpQb_0 = value;
		Il2CppCodeGenWriteBarrier((&___ekrwGUjbOGqAHCaagByDbWSadpQb_0), value);
	}

	inline static int32_t get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1() { return static_cast<int32_t>(offsetof(hPJUHyoOukVQYoALQdLyCcdDHpsQ_t0B149D9BB277329A45ECFC663C989860A6F478F2, ___TWKNVXwJTlddHEpGDbHCUyznGXe_1)); }
	inline int32_t get_TWKNVXwJTlddHEpGDbHCUyznGXe_1() const { return ___TWKNVXwJTlddHEpGDbHCUyznGXe_1; }
	inline int32_t* get_address_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1() { return &___TWKNVXwJTlddHEpGDbHCUyznGXe_1; }
	inline void set_TWKNVXwJTlddHEpGDbHCUyznGXe_1(int32_t value)
	{
		___TWKNVXwJTlddHEpGDbHCUyznGXe_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HPJUHYOOUKVQYOALQDLYCCDDHPSQ_T0B149D9BB277329A45ECFC663C989860A6F478F2_H
#ifndef JQEBNECWPRQTOAVEGFPPDKVHYRFW_T1BC9163F7039E552E0CEAA6FF859C8C60D388CED_H
#define JQEBNECWPRQTOAVEGFPPDKVHYRFW_T1BC9163F7039E552E0CEAA6FF859C8C60D388CED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iNmAhOSEGUcqVFxmGFasdFHfdnr_jqEbnEcwpRqTOaveGfPPDkvHyRFW
struct  jqEbnEcwpRqTOaveGfPPDkvHyRFW_t1BC9163F7039E552E0CEAA6FF859C8C60D388CED  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.InputActionEventData> iNmAhOSEGUcqVFxmGFasdFHfdnr_jqEbnEcwpRqTOaveGfPPDkvHyRFW::ekrwGUjbOGqAHCaagByDbWSadpQb
	Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * ___ekrwGUjbOGqAHCaagByDbWSadpQb_0;
	// Rewired.InputActionEventType iNmAhOSEGUcqVFxmGFasdFHfdnr_jqEbnEcwpRqTOaveGfPPDkvHyRFW::SKPOVoCvhMuMjlLtGtXsXLVxqih
	int32_t ___SKPOVoCvhMuMjlLtGtXsXLVxqih_1;

public:
	inline static int32_t get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return static_cast<int32_t>(offsetof(jqEbnEcwpRqTOaveGfPPDkvHyRFW_t1BC9163F7039E552E0CEAA6FF859C8C60D388CED, ___ekrwGUjbOGqAHCaagByDbWSadpQb_0)); }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * get_ekrwGUjbOGqAHCaagByDbWSadpQb_0() const { return ___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 ** get_address_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return &___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline void set_ekrwGUjbOGqAHCaagByDbWSadpQb_0(Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * value)
	{
		___ekrwGUjbOGqAHCaagByDbWSadpQb_0 = value;
		Il2CppCodeGenWriteBarrier((&___ekrwGUjbOGqAHCaagByDbWSadpQb_0), value);
	}

	inline static int32_t get_offset_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_1() { return static_cast<int32_t>(offsetof(jqEbnEcwpRqTOaveGfPPDkvHyRFW_t1BC9163F7039E552E0CEAA6FF859C8C60D388CED, ___SKPOVoCvhMuMjlLtGtXsXLVxqih_1)); }
	inline int32_t get_SKPOVoCvhMuMjlLtGtXsXLVxqih_1() const { return ___SKPOVoCvhMuMjlLtGtXsXLVxqih_1; }
	inline int32_t* get_address_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_1() { return &___SKPOVoCvhMuMjlLtGtXsXLVxqih_1; }
	inline void set_SKPOVoCvhMuMjlLtGtXsXLVxqih_1(int32_t value)
	{
		___SKPOVoCvhMuMjlLtGtXsXLVxqih_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JQEBNECWPRQTOAVEGFPPDKVHYRFW_T1BC9163F7039E552E0CEAA6FF859C8C60D388CED_H
#ifndef LCCLWABQYQPTCSVRVWLZPDLZWRX_TD5BDC5B1D38F783311946D9C10153FAE51A1CFF3_H
#define LCCLWABQYQPTCSVRVWLZPDLZWRX_TD5BDC5B1D38F783311946D9C10153FAE51A1CFF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iNmAhOSEGUcqVFxmGFasdFHfdnr_lcCLwaBQyqptCsvRVWlzpdLzWrX
struct  lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.InputActionEventData> iNmAhOSEGUcqVFxmGFasdFHfdnr_lcCLwaBQyqptCsvRVWlzpdLzWrX::ekrwGUjbOGqAHCaagByDbWSadpQb
	Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * ___ekrwGUjbOGqAHCaagByDbWSadpQb_0;
	// Rewired.UpdateLoopType iNmAhOSEGUcqVFxmGFasdFHfdnr_lcCLwaBQyqptCsvRVWlzpdLzWrX::TWKNVXwJTlddHEpGDbHCUyznGXe
	int32_t ___TWKNVXwJTlddHEpGDbHCUyznGXe_1;
	// Rewired.InputActionEventType iNmAhOSEGUcqVFxmGFasdFHfdnr_lcCLwaBQyqptCsvRVWlzpdLzWrX::SKPOVoCvhMuMjlLtGtXsXLVxqih
	int32_t ___SKPOVoCvhMuMjlLtGtXsXLVxqih_2;
	// System.Int32 iNmAhOSEGUcqVFxmGFasdFHfdnr_lcCLwaBQyqptCsvRVWlzpdLzWrX::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_3;
	// System.Boolean iNmAhOSEGUcqVFxmGFasdFHfdnr_lcCLwaBQyqptCsvRVWlzpdLzWrX::TpZRbqnqIopRkkdXAgptIogmhWjQ
	bool ___TpZRbqnqIopRkkdXAgptIogmhWjQ_4;
	// System.Single[] iNmAhOSEGUcqVFxmGFasdFHfdnr_lcCLwaBQyqptCsvRVWlzpdLzWrX::qTlZkuASICRAgfMddrzensvBHjp
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___qTlZkuASICRAgfMddrzensvBHjp_5;

public:
	inline static int32_t get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return static_cast<int32_t>(offsetof(lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3, ___ekrwGUjbOGqAHCaagByDbWSadpQb_0)); }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * get_ekrwGUjbOGqAHCaagByDbWSadpQb_0() const { return ___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 ** get_address_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return &___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline void set_ekrwGUjbOGqAHCaagByDbWSadpQb_0(Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * value)
	{
		___ekrwGUjbOGqAHCaagByDbWSadpQb_0 = value;
		Il2CppCodeGenWriteBarrier((&___ekrwGUjbOGqAHCaagByDbWSadpQb_0), value);
	}

	inline static int32_t get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1() { return static_cast<int32_t>(offsetof(lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3, ___TWKNVXwJTlddHEpGDbHCUyznGXe_1)); }
	inline int32_t get_TWKNVXwJTlddHEpGDbHCUyznGXe_1() const { return ___TWKNVXwJTlddHEpGDbHCUyznGXe_1; }
	inline int32_t* get_address_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1() { return &___TWKNVXwJTlddHEpGDbHCUyznGXe_1; }
	inline void set_TWKNVXwJTlddHEpGDbHCUyznGXe_1(int32_t value)
	{
		___TWKNVXwJTlddHEpGDbHCUyznGXe_1 = value;
	}

	inline static int32_t get_offset_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_2() { return static_cast<int32_t>(offsetof(lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3, ___SKPOVoCvhMuMjlLtGtXsXLVxqih_2)); }
	inline int32_t get_SKPOVoCvhMuMjlLtGtXsXLVxqih_2() const { return ___SKPOVoCvhMuMjlLtGtXsXLVxqih_2; }
	inline int32_t* get_address_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_2() { return &___SKPOVoCvhMuMjlLtGtXsXLVxqih_2; }
	inline void set_SKPOVoCvhMuMjlLtGtXsXLVxqih_2(int32_t value)
	{
		___SKPOVoCvhMuMjlLtGtXsXLVxqih_2 = value;
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_3() { return static_cast<int32_t>(offsetof(lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3, ___pTVJDeVeJlQThAZkFztHqDAWDUa_3)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_3() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_3; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_3() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_3; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_3(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_3 = value;
	}

	inline static int32_t get_offset_of_TpZRbqnqIopRkkdXAgptIogmhWjQ_4() { return static_cast<int32_t>(offsetof(lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3, ___TpZRbqnqIopRkkdXAgptIogmhWjQ_4)); }
	inline bool get_TpZRbqnqIopRkkdXAgptIogmhWjQ_4() const { return ___TpZRbqnqIopRkkdXAgptIogmhWjQ_4; }
	inline bool* get_address_of_TpZRbqnqIopRkkdXAgptIogmhWjQ_4() { return &___TpZRbqnqIopRkkdXAgptIogmhWjQ_4; }
	inline void set_TpZRbqnqIopRkkdXAgptIogmhWjQ_4(bool value)
	{
		___TpZRbqnqIopRkkdXAgptIogmhWjQ_4 = value;
	}

	inline static int32_t get_offset_of_qTlZkuASICRAgfMddrzensvBHjp_5() { return static_cast<int32_t>(offsetof(lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3, ___qTlZkuASICRAgfMddrzensvBHjp_5)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_qTlZkuASICRAgfMddrzensvBHjp_5() const { return ___qTlZkuASICRAgfMddrzensvBHjp_5; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_qTlZkuASICRAgfMddrzensvBHjp_5() { return &___qTlZkuASICRAgfMddrzensvBHjp_5; }
	inline void set_qTlZkuASICRAgfMddrzensvBHjp_5(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___qTlZkuASICRAgfMddrzensvBHjp_5 = value;
		Il2CppCodeGenWriteBarrier((&___qTlZkuASICRAgfMddrzensvBHjp_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LCCLWABQYQPTCSVRVWLZPDLZWRX_TD5BDC5B1D38F783311946D9C10153FAE51A1CFF3_H
#ifndef YMBNMRIVEHTTAGLSHZTBZCRLIDT_TF3FC4094FBDA04968038F35233CAB7861952AB60_H
#define YMBNMRIVEHTTAGLSHZTBZCRLIDT_TF3FC4094FBDA04968038F35233CAB7861952AB60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iNmAhOSEGUcqVFxmGFasdFHfdnr_yMbNMrIvehttAgLshzTbZCRLIdT
struct  yMbNMrIvehttAgLshzTbZCRLIdT_tF3FC4094FBDA04968038F35233CAB7861952AB60  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.InputActionEventData> iNmAhOSEGUcqVFxmGFasdFHfdnr_yMbNMrIvehttAgLshzTbZCRLIdT::ekrwGUjbOGqAHCaagByDbWSadpQb
	Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * ___ekrwGUjbOGqAHCaagByDbWSadpQb_0;
	// Rewired.UpdateLoopType iNmAhOSEGUcqVFxmGFasdFHfdnr_yMbNMrIvehttAgLshzTbZCRLIdT::TWKNVXwJTlddHEpGDbHCUyznGXe
	int32_t ___TWKNVXwJTlddHEpGDbHCUyznGXe_1;
	// Rewired.InputActionEventType iNmAhOSEGUcqVFxmGFasdFHfdnr_yMbNMrIvehttAgLshzTbZCRLIdT::SKPOVoCvhMuMjlLtGtXsXLVxqih
	int32_t ___SKPOVoCvhMuMjlLtGtXsXLVxqih_2;
	// System.Int32 iNmAhOSEGUcqVFxmGFasdFHfdnr_yMbNMrIvehttAgLshzTbZCRLIdT::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_3;

public:
	inline static int32_t get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return static_cast<int32_t>(offsetof(yMbNMrIvehttAgLshzTbZCRLIdT_tF3FC4094FBDA04968038F35233CAB7861952AB60, ___ekrwGUjbOGqAHCaagByDbWSadpQb_0)); }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * get_ekrwGUjbOGqAHCaagByDbWSadpQb_0() const { return ___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 ** get_address_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0() { return &___ekrwGUjbOGqAHCaagByDbWSadpQb_0; }
	inline void set_ekrwGUjbOGqAHCaagByDbWSadpQb_0(Action_1_t28F49409729B45FC1FAC929033DDE76162DC4CC8 * value)
	{
		___ekrwGUjbOGqAHCaagByDbWSadpQb_0 = value;
		Il2CppCodeGenWriteBarrier((&___ekrwGUjbOGqAHCaagByDbWSadpQb_0), value);
	}

	inline static int32_t get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1() { return static_cast<int32_t>(offsetof(yMbNMrIvehttAgLshzTbZCRLIdT_tF3FC4094FBDA04968038F35233CAB7861952AB60, ___TWKNVXwJTlddHEpGDbHCUyznGXe_1)); }
	inline int32_t get_TWKNVXwJTlddHEpGDbHCUyznGXe_1() const { return ___TWKNVXwJTlddHEpGDbHCUyznGXe_1; }
	inline int32_t* get_address_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1() { return &___TWKNVXwJTlddHEpGDbHCUyznGXe_1; }
	inline void set_TWKNVXwJTlddHEpGDbHCUyznGXe_1(int32_t value)
	{
		___TWKNVXwJTlddHEpGDbHCUyznGXe_1 = value;
	}

	inline static int32_t get_offset_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_2() { return static_cast<int32_t>(offsetof(yMbNMrIvehttAgLshzTbZCRLIdT_tF3FC4094FBDA04968038F35233CAB7861952AB60, ___SKPOVoCvhMuMjlLtGtXsXLVxqih_2)); }
	inline int32_t get_SKPOVoCvhMuMjlLtGtXsXLVxqih_2() const { return ___SKPOVoCvhMuMjlLtGtXsXLVxqih_2; }
	inline int32_t* get_address_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_2() { return &___SKPOVoCvhMuMjlLtGtXsXLVxqih_2; }
	inline void set_SKPOVoCvhMuMjlLtGtXsXLVxqih_2(int32_t value)
	{
		___SKPOVoCvhMuMjlLtGtXsXLVxqih_2 = value;
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_3() { return static_cast<int32_t>(offsetof(yMbNMrIvehttAgLshzTbZCRLIdT_tF3FC4094FBDA04968038F35233CAB7861952AB60, ___pTVJDeVeJlQThAZkFztHqDAWDUa_3)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_3() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_3; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_3() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_3; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_3(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YMBNMRIVEHTTAGLSHZTBZCRLIDT_TF3FC4094FBDA04968038F35233CAB7861952AB60_H
#ifndef SZJMXPILCMPJYOBHNSPXYLIJAOB_TE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B_H
#define SZJMXPILCMPJYOBHNSPXYLIJAOB_TE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// szJmXpILCMpJYoBHnspxyLIJAob
struct  szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B 
{
public:
	// Rewired.ModifierKey szJmXpILCMpJYoBHnspxyLIJAob::mnGBxrKlrMEtcbDkElEqgOWkWuSS
	int32_t ___mnGBxrKlrMEtcbDkElEqgOWkWuSS_0;
	// Rewired.ModifierKey szJmXpILCMpJYoBHnspxyLIJAob::LtcbhcBgmsmnragehPolhLChilja
	int32_t ___LtcbhcBgmsmnragehPolhLChilja_1;
	// Rewired.ModifierKey szJmXpILCMpJYoBHnspxyLIJAob::nwYAkwIgQIppweizjiwEOkDJuCOQ
	int32_t ___nwYAkwIgQIppweizjiwEOkDJuCOQ_2;

public:
	inline static int32_t get_offset_of_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0() { return static_cast<int32_t>(offsetof(szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B, ___mnGBxrKlrMEtcbDkElEqgOWkWuSS_0)); }
	inline int32_t get_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0() const { return ___mnGBxrKlrMEtcbDkElEqgOWkWuSS_0; }
	inline int32_t* get_address_of_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0() { return &___mnGBxrKlrMEtcbDkElEqgOWkWuSS_0; }
	inline void set_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0(int32_t value)
	{
		___mnGBxrKlrMEtcbDkElEqgOWkWuSS_0 = value;
	}

	inline static int32_t get_offset_of_LtcbhcBgmsmnragehPolhLChilja_1() { return static_cast<int32_t>(offsetof(szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B, ___LtcbhcBgmsmnragehPolhLChilja_1)); }
	inline int32_t get_LtcbhcBgmsmnragehPolhLChilja_1() const { return ___LtcbhcBgmsmnragehPolhLChilja_1; }
	inline int32_t* get_address_of_LtcbhcBgmsmnragehPolhLChilja_1() { return &___LtcbhcBgmsmnragehPolhLChilja_1; }
	inline void set_LtcbhcBgmsmnragehPolhLChilja_1(int32_t value)
	{
		___LtcbhcBgmsmnragehPolhLChilja_1 = value;
	}

	inline static int32_t get_offset_of_nwYAkwIgQIppweizjiwEOkDJuCOQ_2() { return static_cast<int32_t>(offsetof(szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B, ___nwYAkwIgQIppweizjiwEOkDJuCOQ_2)); }
	inline int32_t get_nwYAkwIgQIppweizjiwEOkDJuCOQ_2() const { return ___nwYAkwIgQIppweizjiwEOkDJuCOQ_2; }
	inline int32_t* get_address_of_nwYAkwIgQIppweizjiwEOkDJuCOQ_2() { return &___nwYAkwIgQIppweizjiwEOkDJuCOQ_2; }
	inline void set_nwYAkwIgQIppweizjiwEOkDJuCOQ_2(int32_t value)
	{
		___nwYAkwIgQIppweizjiwEOkDJuCOQ_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SZJMXPILCMPJYOBHNSPXYLIJAOB_TE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B_H
#ifndef XVUPNQNWMCDQDJULQTYAIYRAXWG_TD9F87B3D114024B9D7527B698A80BE4245E1D032_H
#define XVUPNQNWMCDQDJULQTYAIYRAXWG_TD9F87B3D114024B9D7527B698A80BE4245E1D032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// xvUPNQnWmcdqDjulQTyAIyRAxwG
struct  xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032  : public RuntimeObject
{
public:
	// System.Single xvUPNQnWmcdqDjulQTyAIyRAxwG::ujWJxEJcSVHSdnQYrtHnXDWQpnZ
	float ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0;
	// Rewired.ButtonStateFlags xvUPNQnWmcdqDjulQTyAIyRAxwG::UWLOvhrjuLGUzrhYiVuOYFsiVgW
	int32_t ___UWLOvhrjuLGUzrhYiVuOYFsiVgW_1;
	// Rewired.ControllerElementType xvUPNQnWmcdqDjulQTyAIyRAxwG::EDrPGdciFGATRHQnPSEzqJMwhNu
	int32_t ___EDrPGdciFGATRHQnPSEzqJMwhNu_2;
	// Rewired.ControllerType xvUPNQnWmcdqDjulQTyAIyRAxwG::jeLkzDDlxjdyTfidRDslEjdPyAn
	int32_t ___jeLkzDDlxjdyTfidRDslEjdPyAn_3;
	// Rewired.Controller xvUPNQnWmcdqDjulQTyAIyRAxwG::QaeAlqSBKVmDRaDTcYzPlckcanC
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___QaeAlqSBKVmDRaDTcYzPlckcanC_4;
	// Rewired.ControllerMap xvUPNQnWmcdqDjulQTyAIyRAxwG::eDZexKHojMIOIcGgWyZwyijReQJ
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___eDZexKHojMIOIcGgWyZwyijReQJ_5;
	// Rewired.ActionElementMap xvUPNQnWmcdqDjulQTyAIyRAxwG::CPuSQMXEaFTdKTziCifqLUFakpe
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___CPuSQMXEaFTdKTziCifqLUFakpe_6;
	// System.Int32 xvUPNQnWmcdqDjulQTyAIyRAxwG::maMGvvcIaoKXvKiwHYWRqpyTbPEe
	int32_t ___maMGvvcIaoKXvKiwHYWRqpyTbPEe_7;
	// System.Boolean xvUPNQnWmcdqDjulQTyAIyRAxwG::VLynsvsphSPgOwnGXUBktrUfpoK
	bool ___VLynsvsphSPgOwnGXUBktrUfpoK_8;
	// System.Boolean xvUPNQnWmcdqDjulQTyAIyRAxwG::iMsaNGIEEvaEEkHBQjTXifqapVaA
	bool ___iMsaNGIEEvaEEkHBQjTXifqapVaA_9;
	// Rewired.AxisCoordinateMode xvUPNQnWmcdqDjulQTyAIyRAxwG::AzcaYoGJNjQgjFntSQDdTBEQrJJI
	int32_t ___AzcaYoGJNjQgjFntSQDdTBEQrJJI_10;

public:
	inline static int32_t get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0)); }
	inline float get_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0() const { return ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0; }
	inline float* get_address_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0() { return &___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0; }
	inline void set_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0(float value)
	{
		___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0 = value;
	}

	inline static int32_t get_offset_of_UWLOvhrjuLGUzrhYiVuOYFsiVgW_1() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___UWLOvhrjuLGUzrhYiVuOYFsiVgW_1)); }
	inline int32_t get_UWLOvhrjuLGUzrhYiVuOYFsiVgW_1() const { return ___UWLOvhrjuLGUzrhYiVuOYFsiVgW_1; }
	inline int32_t* get_address_of_UWLOvhrjuLGUzrhYiVuOYFsiVgW_1() { return &___UWLOvhrjuLGUzrhYiVuOYFsiVgW_1; }
	inline void set_UWLOvhrjuLGUzrhYiVuOYFsiVgW_1(int32_t value)
	{
		___UWLOvhrjuLGUzrhYiVuOYFsiVgW_1 = value;
	}

	inline static int32_t get_offset_of_EDrPGdciFGATRHQnPSEzqJMwhNu_2() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___EDrPGdciFGATRHQnPSEzqJMwhNu_2)); }
	inline int32_t get_EDrPGdciFGATRHQnPSEzqJMwhNu_2() const { return ___EDrPGdciFGATRHQnPSEzqJMwhNu_2; }
	inline int32_t* get_address_of_EDrPGdciFGATRHQnPSEzqJMwhNu_2() { return &___EDrPGdciFGATRHQnPSEzqJMwhNu_2; }
	inline void set_EDrPGdciFGATRHQnPSEzqJMwhNu_2(int32_t value)
	{
		___EDrPGdciFGATRHQnPSEzqJMwhNu_2 = value;
	}

	inline static int32_t get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_3() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___jeLkzDDlxjdyTfidRDslEjdPyAn_3)); }
	inline int32_t get_jeLkzDDlxjdyTfidRDslEjdPyAn_3() const { return ___jeLkzDDlxjdyTfidRDslEjdPyAn_3; }
	inline int32_t* get_address_of_jeLkzDDlxjdyTfidRDslEjdPyAn_3() { return &___jeLkzDDlxjdyTfidRDslEjdPyAn_3; }
	inline void set_jeLkzDDlxjdyTfidRDslEjdPyAn_3(int32_t value)
	{
		___jeLkzDDlxjdyTfidRDslEjdPyAn_3 = value;
	}

	inline static int32_t get_offset_of_QaeAlqSBKVmDRaDTcYzPlckcanC_4() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___QaeAlqSBKVmDRaDTcYzPlckcanC_4)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_QaeAlqSBKVmDRaDTcYzPlckcanC_4() const { return ___QaeAlqSBKVmDRaDTcYzPlckcanC_4; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_QaeAlqSBKVmDRaDTcYzPlckcanC_4() { return &___QaeAlqSBKVmDRaDTcYzPlckcanC_4; }
	inline void set_QaeAlqSBKVmDRaDTcYzPlckcanC_4(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___QaeAlqSBKVmDRaDTcYzPlckcanC_4 = value;
		Il2CppCodeGenWriteBarrier((&___QaeAlqSBKVmDRaDTcYzPlckcanC_4), value);
	}

	inline static int32_t get_offset_of_eDZexKHojMIOIcGgWyZwyijReQJ_5() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___eDZexKHojMIOIcGgWyZwyijReQJ_5)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_eDZexKHojMIOIcGgWyZwyijReQJ_5() const { return ___eDZexKHojMIOIcGgWyZwyijReQJ_5; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_eDZexKHojMIOIcGgWyZwyijReQJ_5() { return &___eDZexKHojMIOIcGgWyZwyijReQJ_5; }
	inline void set_eDZexKHojMIOIcGgWyZwyijReQJ_5(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___eDZexKHojMIOIcGgWyZwyijReQJ_5 = value;
		Il2CppCodeGenWriteBarrier((&___eDZexKHojMIOIcGgWyZwyijReQJ_5), value);
	}

	inline static int32_t get_offset_of_CPuSQMXEaFTdKTziCifqLUFakpe_6() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___CPuSQMXEaFTdKTziCifqLUFakpe_6)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_CPuSQMXEaFTdKTziCifqLUFakpe_6() const { return ___CPuSQMXEaFTdKTziCifqLUFakpe_6; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_CPuSQMXEaFTdKTziCifqLUFakpe_6() { return &___CPuSQMXEaFTdKTziCifqLUFakpe_6; }
	inline void set_CPuSQMXEaFTdKTziCifqLUFakpe_6(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___CPuSQMXEaFTdKTziCifqLUFakpe_6 = value;
		Il2CppCodeGenWriteBarrier((&___CPuSQMXEaFTdKTziCifqLUFakpe_6), value);
	}

	inline static int32_t get_offset_of_maMGvvcIaoKXvKiwHYWRqpyTbPEe_7() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___maMGvvcIaoKXvKiwHYWRqpyTbPEe_7)); }
	inline int32_t get_maMGvvcIaoKXvKiwHYWRqpyTbPEe_7() const { return ___maMGvvcIaoKXvKiwHYWRqpyTbPEe_7; }
	inline int32_t* get_address_of_maMGvvcIaoKXvKiwHYWRqpyTbPEe_7() { return &___maMGvvcIaoKXvKiwHYWRqpyTbPEe_7; }
	inline void set_maMGvvcIaoKXvKiwHYWRqpyTbPEe_7(int32_t value)
	{
		___maMGvvcIaoKXvKiwHYWRqpyTbPEe_7 = value;
	}

	inline static int32_t get_offset_of_VLynsvsphSPgOwnGXUBktrUfpoK_8() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___VLynsvsphSPgOwnGXUBktrUfpoK_8)); }
	inline bool get_VLynsvsphSPgOwnGXUBktrUfpoK_8() const { return ___VLynsvsphSPgOwnGXUBktrUfpoK_8; }
	inline bool* get_address_of_VLynsvsphSPgOwnGXUBktrUfpoK_8() { return &___VLynsvsphSPgOwnGXUBktrUfpoK_8; }
	inline void set_VLynsvsphSPgOwnGXUBktrUfpoK_8(bool value)
	{
		___VLynsvsphSPgOwnGXUBktrUfpoK_8 = value;
	}

	inline static int32_t get_offset_of_iMsaNGIEEvaEEkHBQjTXifqapVaA_9() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___iMsaNGIEEvaEEkHBQjTXifqapVaA_9)); }
	inline bool get_iMsaNGIEEvaEEkHBQjTXifqapVaA_9() const { return ___iMsaNGIEEvaEEkHBQjTXifqapVaA_9; }
	inline bool* get_address_of_iMsaNGIEEvaEEkHBQjTXifqapVaA_9() { return &___iMsaNGIEEvaEEkHBQjTXifqapVaA_9; }
	inline void set_iMsaNGIEEvaEEkHBQjTXifqapVaA_9(bool value)
	{
		___iMsaNGIEEvaEEkHBQjTXifqapVaA_9 = value;
	}

	inline static int32_t get_offset_of_AzcaYoGJNjQgjFntSQDdTBEQrJJI_10() { return static_cast<int32_t>(offsetof(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032, ___AzcaYoGJNjQgjFntSQDdTBEQrJJI_10)); }
	inline int32_t get_AzcaYoGJNjQgjFntSQDdTBEQrJJI_10() const { return ___AzcaYoGJNjQgjFntSQDdTBEQrJJI_10; }
	inline int32_t* get_address_of_AzcaYoGJNjQgjFntSQDdTBEQrJJI_10() { return &___AzcaYoGJNjQgjFntSQDdTBEQrJJI_10; }
	inline void set_AzcaYoGJNjQgjFntSQDdTBEQrJJI_10(int32_t value)
	{
		___AzcaYoGJNjQgjFntSQDdTBEQrJJI_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XVUPNQNWMCDQDJULQTYAIYRAXWG_TD9F87B3D114024B9D7527B698A80BE4245E1D032_H
#ifndef BUTTONLOOPSET_T043F70DD0163AEE263E80CB7B1AA6C550C46F0AC_H
#define BUTTONLOOPSET_T043F70DD0163AEE263E80CB7B1AA6C550C46F0AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ButtonLoopSet
struct  ButtonLoopSet_t043F70DD0163AEE263E80CB7B1AA6C550C46F0AC  : public UpdateLoopDataSet_1_tB820B43DB3CAAA2A3C73409971EEEB825B29EFEF
{
public:
	// System.Int32 Rewired.ButtonLoopSet::buttonCount
	int32_t ___buttonCount_7;

public:
	inline static int32_t get_offset_of_buttonCount_7() { return static_cast<int32_t>(offsetof(ButtonLoopSet_t043F70DD0163AEE263E80CB7B1AA6C550C46F0AC, ___buttonCount_7)); }
	inline int32_t get_buttonCount_7() const { return ___buttonCount_7; }
	inline int32_t* get_address_of_buttonCount_7() { return &___buttonCount_7; }
	inline void set_buttonCount_7(int32_t value)
	{
		___buttonCount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONLOOPSET_T043F70DD0163AEE263E80CB7B1AA6C550C46F0AC_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ABSANIMATIONCOMPONENT_T6052C9377A13F0ED0E92075769A9E26F58416072_H
#define ABSANIMATIONCOMPONENT_T6052C9377A13F0ED0E92075769A9E26F58416072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSAnimationComponent
struct  ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DG.Tweening.UpdateType DG.Tweening.Core.ABSAnimationComponent::updateType
	int32_t ___updateType_4;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::isSpeedBased
	bool ___isSpeedBased_5;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStart
	bool ___hasOnStart_6;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnPlay
	bool ___hasOnPlay_7;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnUpdate
	bool ___hasOnUpdate_8;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStepComplete
	bool ___hasOnStepComplete_9;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnComplete
	bool ___hasOnComplete_10;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnTweenCreated
	bool ___hasOnTweenCreated_11;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnRewind
	bool ___hasOnRewind_12;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStart
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onStart_13;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onPlay
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onPlay_14;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onUpdate
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onUpdate_15;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStepComplete
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onStepComplete_16;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onComplete
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onComplete_17;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onTweenCreated
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onTweenCreated_18;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onRewind
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onRewind_19;
	// DG.Tweening.Tween DG.Tweening.Core.ABSAnimationComponent::tween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___tween_20;

public:
	inline static int32_t get_offset_of_updateType_4() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___updateType_4)); }
	inline int32_t get_updateType_4() const { return ___updateType_4; }
	inline int32_t* get_address_of_updateType_4() { return &___updateType_4; }
	inline void set_updateType_4(int32_t value)
	{
		___updateType_4 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_5() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___isSpeedBased_5)); }
	inline bool get_isSpeedBased_5() const { return ___isSpeedBased_5; }
	inline bool* get_address_of_isSpeedBased_5() { return &___isSpeedBased_5; }
	inline void set_isSpeedBased_5(bool value)
	{
		___isSpeedBased_5 = value;
	}

	inline static int32_t get_offset_of_hasOnStart_6() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnStart_6)); }
	inline bool get_hasOnStart_6() const { return ___hasOnStart_6; }
	inline bool* get_address_of_hasOnStart_6() { return &___hasOnStart_6; }
	inline void set_hasOnStart_6(bool value)
	{
		___hasOnStart_6 = value;
	}

	inline static int32_t get_offset_of_hasOnPlay_7() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnPlay_7)); }
	inline bool get_hasOnPlay_7() const { return ___hasOnPlay_7; }
	inline bool* get_address_of_hasOnPlay_7() { return &___hasOnPlay_7; }
	inline void set_hasOnPlay_7(bool value)
	{
		___hasOnPlay_7 = value;
	}

	inline static int32_t get_offset_of_hasOnUpdate_8() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnUpdate_8)); }
	inline bool get_hasOnUpdate_8() const { return ___hasOnUpdate_8; }
	inline bool* get_address_of_hasOnUpdate_8() { return &___hasOnUpdate_8; }
	inline void set_hasOnUpdate_8(bool value)
	{
		___hasOnUpdate_8 = value;
	}

	inline static int32_t get_offset_of_hasOnStepComplete_9() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnStepComplete_9)); }
	inline bool get_hasOnStepComplete_9() const { return ___hasOnStepComplete_9; }
	inline bool* get_address_of_hasOnStepComplete_9() { return &___hasOnStepComplete_9; }
	inline void set_hasOnStepComplete_9(bool value)
	{
		___hasOnStepComplete_9 = value;
	}

	inline static int32_t get_offset_of_hasOnComplete_10() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnComplete_10)); }
	inline bool get_hasOnComplete_10() const { return ___hasOnComplete_10; }
	inline bool* get_address_of_hasOnComplete_10() { return &___hasOnComplete_10; }
	inline void set_hasOnComplete_10(bool value)
	{
		___hasOnComplete_10 = value;
	}

	inline static int32_t get_offset_of_hasOnTweenCreated_11() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnTweenCreated_11)); }
	inline bool get_hasOnTweenCreated_11() const { return ___hasOnTweenCreated_11; }
	inline bool* get_address_of_hasOnTweenCreated_11() { return &___hasOnTweenCreated_11; }
	inline void set_hasOnTweenCreated_11(bool value)
	{
		___hasOnTweenCreated_11 = value;
	}

	inline static int32_t get_offset_of_hasOnRewind_12() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___hasOnRewind_12)); }
	inline bool get_hasOnRewind_12() const { return ___hasOnRewind_12; }
	inline bool* get_address_of_hasOnRewind_12() { return &___hasOnRewind_12; }
	inline void set_hasOnRewind_12(bool value)
	{
		___hasOnRewind_12 = value;
	}

	inline static int32_t get_offset_of_onStart_13() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onStart_13)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onStart_13() const { return ___onStart_13; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onStart_13() { return &___onStart_13; }
	inline void set_onStart_13(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onStart_13 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_13), value);
	}

	inline static int32_t get_offset_of_onPlay_14() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onPlay_14)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onPlay_14() const { return ___onPlay_14; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onPlay_14() { return &___onPlay_14; }
	inline void set_onPlay_14(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onPlay_14 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_14), value);
	}

	inline static int32_t get_offset_of_onUpdate_15() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onUpdate_15)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onUpdate_15() const { return ___onUpdate_15; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onUpdate_15() { return &___onUpdate_15; }
	inline void set_onUpdate_15(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_15), value);
	}

	inline static int32_t get_offset_of_onStepComplete_16() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onStepComplete_16)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onStepComplete_16() const { return ___onStepComplete_16; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onStepComplete_16() { return &___onStepComplete_16; }
	inline void set_onStepComplete_16(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onStepComplete_16 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_16), value);
	}

	inline static int32_t get_offset_of_onComplete_17() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onComplete_17)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onComplete_17() const { return ___onComplete_17; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onComplete_17() { return &___onComplete_17; }
	inline void set_onComplete_17(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onComplete_17 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_17), value);
	}

	inline static int32_t get_offset_of_onTweenCreated_18() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onTweenCreated_18)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onTweenCreated_18() const { return ___onTweenCreated_18; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onTweenCreated_18() { return &___onTweenCreated_18; }
	inline void set_onTweenCreated_18(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onTweenCreated_18 = value;
		Il2CppCodeGenWriteBarrier((&___onTweenCreated_18), value);
	}

	inline static int32_t get_offset_of_onRewind_19() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___onRewind_19)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onRewind_19() const { return ___onRewind_19; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onRewind_19() { return &___onRewind_19; }
	inline void set_onRewind_19(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onRewind_19 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_19), value);
	}

	inline static int32_t get_offset_of_tween_20() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072, ___tween_20)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_tween_20() const { return ___tween_20; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_tween_20() { return &___tween_20; }
	inline void set_tween_20(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___tween_20 = value;
		Il2CppCodeGenWriteBarrier((&___tween_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSANIMATIONCOMPONENT_T6052C9377A13F0ED0E92075769A9E26F58416072_H
#ifndef DOTWEENVISUALMANAGER_T80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33_H
#define DOTWEENVISUALMANAGER_T80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenVisualManager
struct  DOTweenVisualManager_t80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DG.Tweening.Core.VisualManagerPreset DG.Tweening.DOTweenVisualManager::preset
	int32_t ___preset_4;
	// DG.Tweening.Core.OnEnableBehaviour DG.Tweening.DOTweenVisualManager::onEnableBehaviour
	int32_t ___onEnableBehaviour_5;
	// DG.Tweening.Core.OnDisableBehaviour DG.Tweening.DOTweenVisualManager::onDisableBehaviour
	int32_t ___onDisableBehaviour_6;
	// System.Boolean DG.Tweening.DOTweenVisualManager::_requiresRestartFromSpawnPoint
	bool ____requiresRestartFromSpawnPoint_7;
	// DG.Tweening.Core.ABSAnimationComponent DG.Tweening.DOTweenVisualManager::_animComponent
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072 * ____animComponent_8;

public:
	inline static int32_t get_offset_of_preset_4() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33, ___preset_4)); }
	inline int32_t get_preset_4() const { return ___preset_4; }
	inline int32_t* get_address_of_preset_4() { return &___preset_4; }
	inline void set_preset_4(int32_t value)
	{
		___preset_4 = value;
	}

	inline static int32_t get_offset_of_onEnableBehaviour_5() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33, ___onEnableBehaviour_5)); }
	inline int32_t get_onEnableBehaviour_5() const { return ___onEnableBehaviour_5; }
	inline int32_t* get_address_of_onEnableBehaviour_5() { return &___onEnableBehaviour_5; }
	inline void set_onEnableBehaviour_5(int32_t value)
	{
		___onEnableBehaviour_5 = value;
	}

	inline static int32_t get_offset_of_onDisableBehaviour_6() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33, ___onDisableBehaviour_6)); }
	inline int32_t get_onDisableBehaviour_6() const { return ___onDisableBehaviour_6; }
	inline int32_t* get_address_of_onDisableBehaviour_6() { return &___onDisableBehaviour_6; }
	inline void set_onDisableBehaviour_6(int32_t value)
	{
		___onDisableBehaviour_6 = value;
	}

	inline static int32_t get_offset_of__requiresRestartFromSpawnPoint_7() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33, ____requiresRestartFromSpawnPoint_7)); }
	inline bool get__requiresRestartFromSpawnPoint_7() const { return ____requiresRestartFromSpawnPoint_7; }
	inline bool* get_address_of__requiresRestartFromSpawnPoint_7() { return &____requiresRestartFromSpawnPoint_7; }
	inline void set__requiresRestartFromSpawnPoint_7(bool value)
	{
		____requiresRestartFromSpawnPoint_7 = value;
	}

	inline static int32_t get_offset_of__animComponent_8() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33, ____animComponent_8)); }
	inline ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072 * get__animComponent_8() const { return ____animComponent_8; }
	inline ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072 ** get_address_of__animComponent_8() { return &____animComponent_8; }
	inline void set__animComponent_8(ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072 * value)
	{
		____animComponent_8 = value;
		Il2CppCodeGenWriteBarrier((&____animComponent_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENVISUALMANAGER_T80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef DOTWEENPATH_T3F12DBCD6A0C3FF3376497F12339ECABE4EA6277_H
#define DOTWEENPATH_T3F12DBCD6A0C3FF3376497F12339ECABE4EA6277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenPath
struct  DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277  : public ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072
{
public:
	// System.Single DG.Tweening.DOTweenPath::delay
	float ___delay_22;
	// System.Single DG.Tweening.DOTweenPath::duration
	float ___duration_23;
	// DG.Tweening.Ease DG.Tweening.DOTweenPath::easeType
	int32_t ___easeType_24;
	// UnityEngine.AnimationCurve DG.Tweening.DOTweenPath::easeCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___easeCurve_25;
	// System.Int32 DG.Tweening.DOTweenPath::loops
	int32_t ___loops_26;
	// System.String DG.Tweening.DOTweenPath::id
	String_t* ___id_27;
	// DG.Tweening.LoopType DG.Tweening.DOTweenPath::loopType
	int32_t ___loopType_28;
	// DG.Tweening.Plugins.Options.OrientType DG.Tweening.DOTweenPath::orientType
	int32_t ___orientType_29;
	// UnityEngine.Transform DG.Tweening.DOTweenPath::lookAtTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___lookAtTransform_30;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::lookAtPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lookAtPosition_31;
	// System.Single DG.Tweening.DOTweenPath::lookAhead
	float ___lookAhead_32;
	// System.Boolean DG.Tweening.DOTweenPath::autoPlay
	bool ___autoPlay_33;
	// System.Boolean DG.Tweening.DOTweenPath::autoKill
	bool ___autoKill_34;
	// System.Boolean DG.Tweening.DOTweenPath::relative
	bool ___relative_35;
	// System.Boolean DG.Tweening.DOTweenPath::isLocal
	bool ___isLocal_36;
	// System.Boolean DG.Tweening.DOTweenPath::isClosedPath
	bool ___isClosedPath_37;
	// System.Int32 DG.Tweening.DOTweenPath::pathResolution
	int32_t ___pathResolution_38;
	// DG.Tweening.PathMode DG.Tweening.DOTweenPath::pathMode
	int32_t ___pathMode_39;
	// DG.Tweening.AxisConstraint DG.Tweening.DOTweenPath::lockRotation
	int32_t ___lockRotation_40;
	// System.Boolean DG.Tweening.DOTweenPath::assignForwardAndUp
	bool ___assignForwardAndUp_41;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::forwardDirection
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardDirection_42;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::upDirection
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upDirection_43;
	// System.Boolean DG.Tweening.DOTweenPath::tweenRigidbody
	bool ___tweenRigidbody_44;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DG.Tweening.DOTweenPath::wps
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___wps_45;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DG.Tweening.DOTweenPath::fullWps
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___fullWps_46;
	// DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.DOTweenPath::path
	Path_tCE251EA0F45EA8B91CAD11018E275B451FAC6FA2 * ___path_47;
	// DG.Tweening.DOTweenInspectorMode DG.Tweening.DOTweenPath::inspectorMode
	int32_t ___inspectorMode_48;
	// DG.Tweening.PathType DG.Tweening.DOTweenPath::pathType
	int32_t ___pathType_49;
	// DG.Tweening.HandlesType DG.Tweening.DOTweenPath::handlesType
	int32_t ___handlesType_50;
	// System.Boolean DG.Tweening.DOTweenPath::livePreview
	bool ___livePreview_51;
	// DG.Tweening.HandlesDrawMode DG.Tweening.DOTweenPath::handlesDrawMode
	int32_t ___handlesDrawMode_52;
	// System.Single DG.Tweening.DOTweenPath::perspectiveHandleSize
	float ___perspectiveHandleSize_53;
	// System.Boolean DG.Tweening.DOTweenPath::showIndexes
	bool ___showIndexes_54;
	// System.Boolean DG.Tweening.DOTweenPath::showWpLength
	bool ___showWpLength_55;
	// UnityEngine.Color DG.Tweening.DOTweenPath::pathColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___pathColor_56;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::lastSrcPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lastSrcPosition_57;
	// UnityEngine.Quaternion DG.Tweening.DOTweenPath::lastSrcRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___lastSrcRotation_58;
	// System.Boolean DG.Tweening.DOTweenPath::wpsDropdown
	bool ___wpsDropdown_59;
	// System.Single DG.Tweening.DOTweenPath::dropToFloorOffset
	float ___dropToFloorOffset_60;

public:
	inline static int32_t get_offset_of_delay_22() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___delay_22)); }
	inline float get_delay_22() const { return ___delay_22; }
	inline float* get_address_of_delay_22() { return &___delay_22; }
	inline void set_delay_22(float value)
	{
		___delay_22 = value;
	}

	inline static int32_t get_offset_of_duration_23() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___duration_23)); }
	inline float get_duration_23() const { return ___duration_23; }
	inline float* get_address_of_duration_23() { return &___duration_23; }
	inline void set_duration_23(float value)
	{
		___duration_23 = value;
	}

	inline static int32_t get_offset_of_easeType_24() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___easeType_24)); }
	inline int32_t get_easeType_24() const { return ___easeType_24; }
	inline int32_t* get_address_of_easeType_24() { return &___easeType_24; }
	inline void set_easeType_24(int32_t value)
	{
		___easeType_24 = value;
	}

	inline static int32_t get_offset_of_easeCurve_25() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___easeCurve_25)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_easeCurve_25() const { return ___easeCurve_25; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_easeCurve_25() { return &___easeCurve_25; }
	inline void set_easeCurve_25(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___easeCurve_25 = value;
		Il2CppCodeGenWriteBarrier((&___easeCurve_25), value);
	}

	inline static int32_t get_offset_of_loops_26() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___loops_26)); }
	inline int32_t get_loops_26() const { return ___loops_26; }
	inline int32_t* get_address_of_loops_26() { return &___loops_26; }
	inline void set_loops_26(int32_t value)
	{
		___loops_26 = value;
	}

	inline static int32_t get_offset_of_id_27() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___id_27)); }
	inline String_t* get_id_27() const { return ___id_27; }
	inline String_t** get_address_of_id_27() { return &___id_27; }
	inline void set_id_27(String_t* value)
	{
		___id_27 = value;
		Il2CppCodeGenWriteBarrier((&___id_27), value);
	}

	inline static int32_t get_offset_of_loopType_28() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___loopType_28)); }
	inline int32_t get_loopType_28() const { return ___loopType_28; }
	inline int32_t* get_address_of_loopType_28() { return &___loopType_28; }
	inline void set_loopType_28(int32_t value)
	{
		___loopType_28 = value;
	}

	inline static int32_t get_offset_of_orientType_29() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___orientType_29)); }
	inline int32_t get_orientType_29() const { return ___orientType_29; }
	inline int32_t* get_address_of_orientType_29() { return &___orientType_29; }
	inline void set_orientType_29(int32_t value)
	{
		___orientType_29 = value;
	}

	inline static int32_t get_offset_of_lookAtTransform_30() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___lookAtTransform_30)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_lookAtTransform_30() const { return ___lookAtTransform_30; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_lookAtTransform_30() { return &___lookAtTransform_30; }
	inline void set_lookAtTransform_30(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___lookAtTransform_30 = value;
		Il2CppCodeGenWriteBarrier((&___lookAtTransform_30), value);
	}

	inline static int32_t get_offset_of_lookAtPosition_31() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___lookAtPosition_31)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_lookAtPosition_31() const { return ___lookAtPosition_31; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_lookAtPosition_31() { return &___lookAtPosition_31; }
	inline void set_lookAtPosition_31(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___lookAtPosition_31 = value;
	}

	inline static int32_t get_offset_of_lookAhead_32() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___lookAhead_32)); }
	inline float get_lookAhead_32() const { return ___lookAhead_32; }
	inline float* get_address_of_lookAhead_32() { return &___lookAhead_32; }
	inline void set_lookAhead_32(float value)
	{
		___lookAhead_32 = value;
	}

	inline static int32_t get_offset_of_autoPlay_33() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___autoPlay_33)); }
	inline bool get_autoPlay_33() const { return ___autoPlay_33; }
	inline bool* get_address_of_autoPlay_33() { return &___autoPlay_33; }
	inline void set_autoPlay_33(bool value)
	{
		___autoPlay_33 = value;
	}

	inline static int32_t get_offset_of_autoKill_34() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___autoKill_34)); }
	inline bool get_autoKill_34() const { return ___autoKill_34; }
	inline bool* get_address_of_autoKill_34() { return &___autoKill_34; }
	inline void set_autoKill_34(bool value)
	{
		___autoKill_34 = value;
	}

	inline static int32_t get_offset_of_relative_35() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___relative_35)); }
	inline bool get_relative_35() const { return ___relative_35; }
	inline bool* get_address_of_relative_35() { return &___relative_35; }
	inline void set_relative_35(bool value)
	{
		___relative_35 = value;
	}

	inline static int32_t get_offset_of_isLocal_36() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___isLocal_36)); }
	inline bool get_isLocal_36() const { return ___isLocal_36; }
	inline bool* get_address_of_isLocal_36() { return &___isLocal_36; }
	inline void set_isLocal_36(bool value)
	{
		___isLocal_36 = value;
	}

	inline static int32_t get_offset_of_isClosedPath_37() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___isClosedPath_37)); }
	inline bool get_isClosedPath_37() const { return ___isClosedPath_37; }
	inline bool* get_address_of_isClosedPath_37() { return &___isClosedPath_37; }
	inline void set_isClosedPath_37(bool value)
	{
		___isClosedPath_37 = value;
	}

	inline static int32_t get_offset_of_pathResolution_38() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___pathResolution_38)); }
	inline int32_t get_pathResolution_38() const { return ___pathResolution_38; }
	inline int32_t* get_address_of_pathResolution_38() { return &___pathResolution_38; }
	inline void set_pathResolution_38(int32_t value)
	{
		___pathResolution_38 = value;
	}

	inline static int32_t get_offset_of_pathMode_39() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___pathMode_39)); }
	inline int32_t get_pathMode_39() const { return ___pathMode_39; }
	inline int32_t* get_address_of_pathMode_39() { return &___pathMode_39; }
	inline void set_pathMode_39(int32_t value)
	{
		___pathMode_39 = value;
	}

	inline static int32_t get_offset_of_lockRotation_40() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___lockRotation_40)); }
	inline int32_t get_lockRotation_40() const { return ___lockRotation_40; }
	inline int32_t* get_address_of_lockRotation_40() { return &___lockRotation_40; }
	inline void set_lockRotation_40(int32_t value)
	{
		___lockRotation_40 = value;
	}

	inline static int32_t get_offset_of_assignForwardAndUp_41() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___assignForwardAndUp_41)); }
	inline bool get_assignForwardAndUp_41() const { return ___assignForwardAndUp_41; }
	inline bool* get_address_of_assignForwardAndUp_41() { return &___assignForwardAndUp_41; }
	inline void set_assignForwardAndUp_41(bool value)
	{
		___assignForwardAndUp_41 = value;
	}

	inline static int32_t get_offset_of_forwardDirection_42() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___forwardDirection_42)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardDirection_42() const { return ___forwardDirection_42; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardDirection_42() { return &___forwardDirection_42; }
	inline void set_forwardDirection_42(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardDirection_42 = value;
	}

	inline static int32_t get_offset_of_upDirection_43() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___upDirection_43)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upDirection_43() const { return ___upDirection_43; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upDirection_43() { return &___upDirection_43; }
	inline void set_upDirection_43(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upDirection_43 = value;
	}

	inline static int32_t get_offset_of_tweenRigidbody_44() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___tweenRigidbody_44)); }
	inline bool get_tweenRigidbody_44() const { return ___tweenRigidbody_44; }
	inline bool* get_address_of_tweenRigidbody_44() { return &___tweenRigidbody_44; }
	inline void set_tweenRigidbody_44(bool value)
	{
		___tweenRigidbody_44 = value;
	}

	inline static int32_t get_offset_of_wps_45() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___wps_45)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_wps_45() const { return ___wps_45; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_wps_45() { return &___wps_45; }
	inline void set_wps_45(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___wps_45 = value;
		Il2CppCodeGenWriteBarrier((&___wps_45), value);
	}

	inline static int32_t get_offset_of_fullWps_46() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___fullWps_46)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_fullWps_46() const { return ___fullWps_46; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_fullWps_46() { return &___fullWps_46; }
	inline void set_fullWps_46(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___fullWps_46 = value;
		Il2CppCodeGenWriteBarrier((&___fullWps_46), value);
	}

	inline static int32_t get_offset_of_path_47() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___path_47)); }
	inline Path_tCE251EA0F45EA8B91CAD11018E275B451FAC6FA2 * get_path_47() const { return ___path_47; }
	inline Path_tCE251EA0F45EA8B91CAD11018E275B451FAC6FA2 ** get_address_of_path_47() { return &___path_47; }
	inline void set_path_47(Path_tCE251EA0F45EA8B91CAD11018E275B451FAC6FA2 * value)
	{
		___path_47 = value;
		Il2CppCodeGenWriteBarrier((&___path_47), value);
	}

	inline static int32_t get_offset_of_inspectorMode_48() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___inspectorMode_48)); }
	inline int32_t get_inspectorMode_48() const { return ___inspectorMode_48; }
	inline int32_t* get_address_of_inspectorMode_48() { return &___inspectorMode_48; }
	inline void set_inspectorMode_48(int32_t value)
	{
		___inspectorMode_48 = value;
	}

	inline static int32_t get_offset_of_pathType_49() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___pathType_49)); }
	inline int32_t get_pathType_49() const { return ___pathType_49; }
	inline int32_t* get_address_of_pathType_49() { return &___pathType_49; }
	inline void set_pathType_49(int32_t value)
	{
		___pathType_49 = value;
	}

	inline static int32_t get_offset_of_handlesType_50() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___handlesType_50)); }
	inline int32_t get_handlesType_50() const { return ___handlesType_50; }
	inline int32_t* get_address_of_handlesType_50() { return &___handlesType_50; }
	inline void set_handlesType_50(int32_t value)
	{
		___handlesType_50 = value;
	}

	inline static int32_t get_offset_of_livePreview_51() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___livePreview_51)); }
	inline bool get_livePreview_51() const { return ___livePreview_51; }
	inline bool* get_address_of_livePreview_51() { return &___livePreview_51; }
	inline void set_livePreview_51(bool value)
	{
		___livePreview_51 = value;
	}

	inline static int32_t get_offset_of_handlesDrawMode_52() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___handlesDrawMode_52)); }
	inline int32_t get_handlesDrawMode_52() const { return ___handlesDrawMode_52; }
	inline int32_t* get_address_of_handlesDrawMode_52() { return &___handlesDrawMode_52; }
	inline void set_handlesDrawMode_52(int32_t value)
	{
		___handlesDrawMode_52 = value;
	}

	inline static int32_t get_offset_of_perspectiveHandleSize_53() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___perspectiveHandleSize_53)); }
	inline float get_perspectiveHandleSize_53() const { return ___perspectiveHandleSize_53; }
	inline float* get_address_of_perspectiveHandleSize_53() { return &___perspectiveHandleSize_53; }
	inline void set_perspectiveHandleSize_53(float value)
	{
		___perspectiveHandleSize_53 = value;
	}

	inline static int32_t get_offset_of_showIndexes_54() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___showIndexes_54)); }
	inline bool get_showIndexes_54() const { return ___showIndexes_54; }
	inline bool* get_address_of_showIndexes_54() { return &___showIndexes_54; }
	inline void set_showIndexes_54(bool value)
	{
		___showIndexes_54 = value;
	}

	inline static int32_t get_offset_of_showWpLength_55() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___showWpLength_55)); }
	inline bool get_showWpLength_55() const { return ___showWpLength_55; }
	inline bool* get_address_of_showWpLength_55() { return &___showWpLength_55; }
	inline void set_showWpLength_55(bool value)
	{
		___showWpLength_55 = value;
	}

	inline static int32_t get_offset_of_pathColor_56() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___pathColor_56)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_pathColor_56() const { return ___pathColor_56; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_pathColor_56() { return &___pathColor_56; }
	inline void set_pathColor_56(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___pathColor_56 = value;
	}

	inline static int32_t get_offset_of_lastSrcPosition_57() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___lastSrcPosition_57)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_lastSrcPosition_57() const { return ___lastSrcPosition_57; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_lastSrcPosition_57() { return &___lastSrcPosition_57; }
	inline void set_lastSrcPosition_57(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___lastSrcPosition_57 = value;
	}

	inline static int32_t get_offset_of_lastSrcRotation_58() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___lastSrcRotation_58)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_lastSrcRotation_58() const { return ___lastSrcRotation_58; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_lastSrcRotation_58() { return &___lastSrcRotation_58; }
	inline void set_lastSrcRotation_58(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___lastSrcRotation_58 = value;
	}

	inline static int32_t get_offset_of_wpsDropdown_59() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___wpsDropdown_59)); }
	inline bool get_wpsDropdown_59() const { return ___wpsDropdown_59; }
	inline bool* get_address_of_wpsDropdown_59() { return &___wpsDropdown_59; }
	inline void set_wpsDropdown_59(bool value)
	{
		___wpsDropdown_59 = value;
	}

	inline static int32_t get_offset_of_dropToFloorOffset_60() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277, ___dropToFloorOffset_60)); }
	inline float get_dropToFloorOffset_60() const { return ___dropToFloorOffset_60; }
	inline float* get_address_of_dropToFloorOffset_60() { return &___dropToFloorOffset_60; }
	inline void set_dropToFloorOffset_60(float value)
	{
		___dropToFloorOffset_60 = value;
	}
};

struct DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277_StaticFields
{
public:
	// System.Action`1<DG.Tweening.DOTweenPath> DG.Tweening.DOTweenPath::OnReset
	Action_1_t9584A042310BC9A47625CC34F2259D4A9AB9884C * ___OnReset_21;
	// System.Reflection.MethodInfo DG.Tweening.DOTweenPath::_miCreateTween
	MethodInfo_t * ____miCreateTween_61;

public:
	inline static int32_t get_offset_of_OnReset_21() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277_StaticFields, ___OnReset_21)); }
	inline Action_1_t9584A042310BC9A47625CC34F2259D4A9AB9884C * get_OnReset_21() const { return ___OnReset_21; }
	inline Action_1_t9584A042310BC9A47625CC34F2259D4A9AB9884C ** get_address_of_OnReset_21() { return &___OnReset_21; }
	inline void set_OnReset_21(Action_1_t9584A042310BC9A47625CC34F2259D4A9AB9884C * value)
	{
		___OnReset_21 = value;
		Il2CppCodeGenWriteBarrier((&___OnReset_21), value);
	}

	inline static int32_t get_offset_of__miCreateTween_61() { return static_cast<int32_t>(offsetof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277_StaticFields, ____miCreateTween_61)); }
	inline MethodInfo_t * get__miCreateTween_61() const { return ____miCreateTween_61; }
	inline MethodInfo_t ** get_address_of__miCreateTween_61() { return &____miCreateTween_61; }
	inline void set__miCreateTween_61(MethodInfo_t * value)
	{
		____miCreateTween_61 = value;
		Il2CppCodeGenWriteBarrier((&____miCreateTween_61), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENPATH_T3F12DBCD6A0C3FF3376497F12339ECABE4EA6277_H
#ifndef BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#define BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5, ___m_Graphic_4)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifndef POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#define POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifndef SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#define SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectDistance_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifndef OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#define OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_tB750E496976B072E79142D51C0A991AC20183095  : public Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800 = { sizeof (VertexHelperExtension_t593AF80A941B7208D2CC88AE671873CE6E2AA8E3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801 = { sizeof (BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802 = { sizeof (BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3802[1] = 
{
	BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5::get_offset_of_m_Graphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805 = { sizeof (Outline_tB750E496976B072E79142D51C0A991AC20183095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806 = { sizeof (PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807 = { sizeof (Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3807[4] = 
{
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectColor_5(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectDistance_6(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_UseGraphicAlpha_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808 = { sizeof (U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537), -1, sizeof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3808[1] = 
{
	U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809 = { sizeof (U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810 = { sizeof (U3CModuleU3E_tCE4B768174CDE0294B05DD8ED59A7763FF34E99B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811 = { sizeof (U3CModuleU3E_t79596D1CF9533C008106C502C07E3DC3CC441D96), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812 = { sizeof (DDebug_t84DED928DEFD12E80D24749CF6180D4CD2DC5F0B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813 = { sizeof (U3CModuleU3E_t7203CD2929947A870302A54423522DDB869AC803), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814 = { sizeof (DOTweenVisualManager_t80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3814[5] = 
{
	DOTweenVisualManager_t80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33::get_offset_of_preset_4(),
	DOTweenVisualManager_t80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33::get_offset_of_onEnableBehaviour_5(),
	DOTweenVisualManager_t80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33::get_offset_of_onDisableBehaviour_6(),
	DOTweenVisualManager_t80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33::get_offset_of__requiresRestartFromSpawnPoint_7(),
	DOTweenVisualManager_t80E41B7FD73B5CD88A55D37DF54FA3FE47B2FA33::get_offset_of__animComponent_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815 = { sizeof (HandlesDrawMode_tFED7FD59A78C0CB6399F023F742B93077C3F208A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3815[3] = 
{
	HandlesDrawMode_tFED7FD59A78C0CB6399F023F742B93077C3F208A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816 = { sizeof (HandlesType_t65157C792644D0D925D426C656DB250524E0DADC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3816[3] = 
{
	HandlesType_t65157C792644D0D925D426C656DB250524E0DADC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817 = { sizeof (DOTweenInspectorMode_t37B98D58E76A1A14E51F9295CEB00F574AADD9B1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3817[5] = 
{
	DOTweenInspectorMode_t37B98D58E76A1A14E51F9295CEB00F574AADD9B1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818 = { sizeof (DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277), -1, sizeof(DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3818[41] = 
{
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277_StaticFields::get_offset_of_OnReset_21(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_delay_22(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_duration_23(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_easeType_24(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_easeCurve_25(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_loops_26(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_id_27(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_loopType_28(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_orientType_29(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_lookAtTransform_30(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_lookAtPosition_31(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_lookAhead_32(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_autoPlay_33(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_autoKill_34(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_relative_35(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_isLocal_36(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_isClosedPath_37(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_pathResolution_38(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_pathMode_39(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_lockRotation_40(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_assignForwardAndUp_41(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_forwardDirection_42(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_upDirection_43(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_tweenRigidbody_44(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_wps_45(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_fullWps_46(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_path_47(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_inspectorMode_48(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_pathType_49(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_handlesType_50(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_livePreview_51(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_handlesDrawMode_52(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_perspectiveHandleSize_53(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_showIndexes_54(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_showWpLength_55(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_pathColor_56(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_lastSrcPosition_57(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_lastSrcRotation_58(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_wpsDropdown_59(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277::get_offset_of_dropToFloorOffset_60(),
	DOTweenPath_t3F12DBCD6A0C3FF3376497F12339ECABE4EA6277_StaticFields::get_offset_of__miCreateTween_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819 = { sizeof (SpiralMode_tFB14778D700215DC51AE35D0B8F3D54F397514DE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3819[3] = 
{
	SpiralMode_tFB14778D700215DC51AE35D0B8F3D54F397514DE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820 = { sizeof (SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC)+ sizeof (RuntimeObject), sizeof(SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3820[7] = 
{
	SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC::get_offset_of_depth_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC::get_offset_of_frequency_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC::get_offset_of_speed_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC::get_offset_of_mode_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC::get_offset_of_snapping_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC::get_offset_of_unit_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpiralOptions_t7CA7DCE335F404FBB0E17BFFDD2E9191C2F417BC::get_offset_of_axisQ_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821 = { sizeof (SpiralPlugin_t5B827E6201D5FF36A7B4845D1281FCC65F5BC3CD), -1, sizeof(SpiralPlugin_t5B827E6201D5FF36A7B4845D1281FCC65F5BC3CD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3821[1] = 
{
	SpiralPlugin_t5B827E6201D5FF36A7B4845D1281FCC65F5BC3CD_StaticFields::get_offset_of_DefaultDirection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3822 = { sizeof (ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3822[17] = 
{
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_updateType_4(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_isSpeedBased_5(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_hasOnStart_6(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_hasOnPlay_7(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_hasOnUpdate_8(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_hasOnStepComplete_9(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_hasOnComplete_10(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_hasOnTweenCreated_11(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_hasOnRewind_12(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_onStart_13(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_onPlay_14(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_onUpdate_15(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_onStepComplete_16(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_onComplete_17(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_onTweenCreated_18(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_onRewind_19(),
	ABSAnimationComponent_t6052C9377A13F0ED0E92075769A9E26F58416072::get_offset_of_tween_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3823 = { sizeof (OnDisableBehaviour_t4D4DDECAFD77D9245EED8E5940AB100CE15876B4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3823[7] = 
{
	OnDisableBehaviour_t4D4DDECAFD77D9245EED8E5940AB100CE15876B4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3824 = { sizeof (OnEnableBehaviour_t3C98A3FDA65C44B5BA4E84752A15E9A6A68765B5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3824[5] = 
{
	OnEnableBehaviour_t3C98A3FDA65C44B5BA4E84752A15E9A6A68765B5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3825 = { sizeof (VisualManagerPreset_tC4F634B0BE552725DBAD3DAEABBABA80B8596A7E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3825[3] = 
{
	VisualManagerPreset_tC4F634B0BE552725DBAD3DAEABBABA80B8596A7E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3826 = { sizeof (U3CModuleU3E_t137236D74FF67F879C976AFCD30551A0AC83AF32), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3827 = { sizeof (ActionIdFieldInfoAttribute_t84C4D28B5D1C76068E9FD5F9FB2028098E26310B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3827[2] = 
{
	ActionIdFieldInfoAttribute_t84C4D28B5D1C76068E9FD5F9FB2028098E26310B::get_offset_of_categoryName_0(),
	ActionIdFieldInfoAttribute_t84C4D28B5D1C76068E9FD5F9FB2028098E26310B::get_offset_of_friendlyName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3828 = { sizeof (ActionIdPropertyAttribute_t22FDCCC2B977E9E587A08FA1CD32E9750FE2DC20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3828[1] = 
{
	ActionIdPropertyAttribute_t22FDCCC2B977E9E587A08FA1CD32E9750FE2DC20::get_offset_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3829 = { sizeof (BitmaskAttribute_t2DD0E46C57279F82891239B4EEE2CD936BDA2B74), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3829[1] = 
{
	BitmaskAttribute_t2DD0E46C57279F82891239B4EEE2CD936BDA2B74::get_offset_of_propType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3830 = { sizeof (BitmaskToggleAttribute_tF36CA63EE97482524F92128A45D49659758A32EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3830[3] = 
{
	BitmaskToggleAttribute_tF36CA63EE97482524F92128A45D49659758A32EC::get_offset_of_propType_0(),
	BitmaskToggleAttribute_tF36CA63EE97482524F92128A45D49659758A32EC::get_offset_of_showNone_1(),
	BitmaskToggleAttribute_tF36CA63EE97482524F92128A45D49659758A32EC::get_offset_of_showAll_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3831 = { sizeof (FieldRangeAttribute_tDA409BA82B6D22DFF55734E94DEFD443F9C51149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3831[4] = 
{
	FieldRangeAttribute_tDA409BA82B6D22DFF55734E94DEFD443F9C51149::get_offset_of_hIkxovEuYTMCRUaLMwxAoHjNNmY_0(),
	FieldRangeAttribute_tDA409BA82B6D22DFF55734E94DEFD443F9C51149::get_offset_of_eSZsCzljrhuCIBopfnRisnptBxgE_1(),
	FieldRangeAttribute_tDA409BA82B6D22DFF55734E94DEFD443F9C51149::get_offset_of_ywqbVCEqGBFXpKKfAQJxdJNcjca_2(),
	FieldRangeAttribute_tDA409BA82B6D22DFF55734E94DEFD443F9C51149::get_offset_of_dVmfgUxGQrgacYprIhqAqtAAWXp_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3832 = { sizeof (PreserveAttribute_tF09697B331D2CFEDB87BE18244F6CA6C980A62B1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3833 = { sizeof (MonoPInvokeCallbackAttribute_t347DDFFCE59C6D5D51EBA70D452CB2079F01AC1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3833[1] = 
{
	MonoPInvokeCallbackAttribute_t347DDFFCE59C6D5D51EBA70D452CB2079F01AC1B::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3834 = { sizeof (SerializationTypeAttribute_t6A8F027F04EB9964418162F70013325B17AE0163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3834[1] = 
{
	SerializationTypeAttribute_t6A8F027F04EB9964418162F70013325B17AE0163::get_offset_of__serializationType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3835 = { sizeof (SerializationType_t67BE27A789E953C37E73B63BBC015AF220B8EFF7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3835[3] = 
{
	SerializationType_t67BE27A789E953C37E73B63BBC015AF220B8EFF7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3836 = { sizeof (PlayerIdFieldInfoAttribute_t96E234F121A683A77A9F56CFDF9BB1B819CF1B3D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3836[1] = 
{
	PlayerIdFieldInfoAttribute_t96E234F121A683A77A9F56CFDF9BB1B819CF1B3D::get_offset_of_friendlyName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3837 = { sizeof (PlayerIdPropertyAttribute_tE61422D82980BEBA3A3306AA00086943DD361CE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3837[1] = 
{
	PlayerIdPropertyAttribute_tE61422D82980BEBA3A3306AA00086943DD361CE7::get_offset_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3838 = { sizeof (ControllerStatusChangedEventArgs_tCC162BDFB36A2B12C4E07F5B9B807BDC20167EFC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3838[3] = 
{
	ControllerStatusChangedEventArgs_tCC162BDFB36A2B12C4E07F5B9B807BDC20167EFC::get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1(),
	ControllerStatusChangedEventArgs_tCC162BDFB36A2B12C4E07F5B9B807BDC20167EFC::get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_2(),
	ControllerStatusChangedEventArgs_tCC162BDFB36A2B12C4E07F5B9B807BDC20167EFC::get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3839 = { sizeof (ControllerAssignmentChangedEventArgs_tD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3839[4] = 
{
	ControllerAssignmentChangedEventArgs_tD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D::get_offset_of_UBXHFojKQfGbvszJFuVFvDRPgNg_1(),
	ControllerAssignmentChangedEventArgs_tD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D::get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2(),
	ControllerAssignmentChangedEventArgs_tD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D::get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_3(),
	ControllerAssignmentChangedEventArgs_tD5DF0672EB6B01EA8809D346F99B25C3DFF31C5D::get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3840 = { sizeof (InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3840[5] = 
{
	InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82::get_offset_of_nOyACFHXpjgPWDTOdoqXegjcnvRy_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82::get_offset_of_rvRtCzapOdDtBhWRBVphGbFutZU_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82::get_offset_of_playerId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82::get_offset_of_actionId_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputActionEventData_tDC602553D1EFE9CF156BED6DF092B26377625E82::get_offset_of_updateLoop_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3841 = { sizeof (ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03)+ sizeof (RuntimeObject), sizeof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3841[11] = 
{
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03::get_offset_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03::get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03::get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03::get_offset_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03::get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03::get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03::get_offset_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03::get_offset_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03::get_offset_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03::get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03::get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3842 = { sizeof (ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D)+ sizeof (RuntimeObject), sizeof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3842[12] = 
{
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D::get_offset_of_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D::get_offset_of_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D::get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D::get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D::get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D::get_offset_of_kNupzNtNRIxDZONVUSwnnBWcpIT_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D::get_offset_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D::get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D::get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D::get_offset_of_SkzHHHthNTcRwnaarBvUnkLqLeI_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D::get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D::get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3843 = { sizeof (ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4)+ sizeof (RuntimeObject), sizeof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3843[14] = 
{
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_PReGOawJrxHuYbkYlXhFoNBbppd_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_kNupzNtNRIxDZONVUSwnnBWcpIT_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4::get_offset_of_GfvPtFqTnCIcrWebHAQzqOoKceI_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3844 = { sizeof (ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C)+ sizeof (RuntimeObject), sizeof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3844[9] = 
{
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C::get_offset_of_elementMapId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C::get_offset_of_elementIdentifierId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C::get_offset_of_axisRange_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C::get_offset_of_keyboardKey_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C::get_offset_of_modifierKeyFlags_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C::get_offset_of_actionId_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C::get_offset_of_axisContribution_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C::get_offset_of_invert_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3845 = { sizeof (ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3845[10] = 
{
	ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF::get_offset_of_uNIXhFwhKcbRMaCTeDSCheRwjLdb_0(),
	ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF::get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_1(),
	ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF::get_offset_of_ljTHPqmrucaukQyhtWjEKicsbql_2(),
	ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF::get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_3(),
	ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF::get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_4(),
	ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF::get_offset_of_SkzHHHthNTcRwnaarBvUnkLqLeI_5(),
	ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF::get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_6(),
	ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF::get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_7(),
	ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF::get_offset_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_8(),
	ElementAssignmentInfo_t72BB25E7B936C42E9D6C3ACC509D4D2B54F4F2FF::get_offset_of_GfvPtFqTnCIcrWebHAQzqOoKceI_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3846 = { sizeof (InputActionSourceData_tAB524E4A9B0BE8CB999545953717D3EC066E2FF7)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3846[3] = 
{
	InputActionSourceData_tAB524E4A9B0BE8CB999545953717D3EC066E2FF7::get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputActionSourceData_tAB524E4A9B0BE8CB999545953717D3EC066E2FF7::get_offset_of_uNIXhFwhKcbRMaCTeDSCheRwjLdb_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputActionSourceData_tAB524E4A9B0BE8CB999545953717D3EC066E2FF7::get_offset_of_vzRXOXEPrntdOVXFtuchDEexSwL_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3847 = { sizeof (ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6)+ sizeof (RuntimeObject), sizeof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3847[5] = 
{
	ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6::get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6::get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6::get_offset_of_hVfvAlYZzngYWNUKHfQeqsdsSsN_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6::get_offset_of_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6::get_offset_of_uoQfWOPIJljmoZOdOiUDjMDpAEI_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3848 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3849 = { sizeof (ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3849[6] = 
{
	ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD::get_offset_of__type_0(),
	ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD::get_offset_of__controllerType_1(),
	ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD::get_offset_of__guid_2(),
	ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD::get_offset_of__hardwareIdentifier_3(),
	ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD::get_offset_of__controllerId_4(),
	ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD::get_offset_of_kAtYqKtxQwdzXJBwfHMpmjYXGum_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3850 = { sizeof (Type_t182FB43AE32FF984996E805320E1D5244BCF5887)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3850[7] = 
{
	Type_t182FB43AE32FF984996E805320E1D5244BCF5887::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3851 = { sizeof (QpxDmYPZSvdyLJdxEXKaYpCAqqW_tC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9)+ sizeof (RuntimeObject), sizeof(QpxDmYPZSvdyLJdxEXKaYpCAqqW_tC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3851[4] = 
{
	QpxDmYPZSvdyLJdxEXKaYpCAqqW_tC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9::get_offset_of_VnWWwfPKVKMcfzlEQdtUORAGuQr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QpxDmYPZSvdyLJdxEXKaYpCAqqW_tC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9::get_offset_of_mnGBxrKlrMEtcbDkElEqgOWkWuSS_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QpxDmYPZSvdyLJdxEXKaYpCAqqW_tC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9::get_offset_of_LtcbhcBgmsmnragehPolhLChilja_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QpxDmYPZSvdyLJdxEXKaYpCAqqW_tC6206C9E858ACD970BA5CEB8A3AD7259C902CCD9::get_offset_of_nwYAkwIgQIppweizjiwEOkDJuCOQ_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3852 = { sizeof (szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B)+ sizeof (RuntimeObject), sizeof(szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B ), 0, 0 };
extern const int32_t g_FieldOffsetTable3852[3] = 
{
	szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B::get_offset_of_mnGBxrKlrMEtcbDkElEqgOWkWuSS_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B::get_offset_of_LtcbhcBgmsmnragehPolhLChilja_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	szJmXpILCMpJYoBHnspxyLIJAob_tE688779386D7C7AB70F4DB1C500F6A2C3A4F6E2B::get_offset_of_nwYAkwIgQIppweizjiwEOkDJuCOQ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3853 = { sizeof (ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3853[6] = 
{
	0,
	ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148::get_offset_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_1(),
	ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148::get_offset_of_ZzOKRylsDKYMRegaUFScNhcMSfN_2(),
	ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148::get_offset_of_dYjOkhnWbRzPVxoGlildEQfbYqZ_3(),
	ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148::get_offset_of_AvDvBrCrpyFTuiktlFfteeQRFqy_4(),
	ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148::get_offset_of_NWSqJJloZMsgLSarwMePrSnTDOO_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3854 = { sizeof (hxGGSVmQZTfybORfTgCfaxyzTkP_tB58A4CAA84FD83D5A6C3094DBDAD23B9D4A0563F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3854[2] = 
{
	hxGGSVmQZTfybORfTgCfaxyzTkP_tB58A4CAA84FD83D5A6C3094DBDAD23B9D4A0563F::get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_0(),
	hxGGSVmQZTfybORfTgCfaxyzTkP_tB58A4CAA84FD83D5A6C3094DBDAD23B9D4A0563F::get_offset_of_bAgBXUBUQoTpqsGAsnokCWHlafSk_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3855 = { sizeof (EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3855[8] = 
{
	0,
	EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5::get_offset_of_mvKHSWNtYxtrXAPVpClHLUktCzV_1(),
	EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5::get_offset_of_KqsZQvXZGdcrJvGsJxuAvfvnhDe_2(),
	EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5::get_offset_of_BLWYEEspSiesrpLjzxiQnjWOBUmf_3(),
	EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5::get_offset_of_ugxQlRwXrGpEMOQPtUvgtSVoUnh_4(),
	EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5::get_offset_of_LJwjkYfhxNrwVJAlKTmoasSnlIE_5(),
	EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5::get_offset_of_OfxqMMDGWdzhDRrNathCCkjqqeA_6(),
	EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5::get_offset_of_xbzIuakPyoIxhUNJcNokVaqGbxB_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3856 = { sizeof (gbnJVSILPyqAUDavYEfkWfNLGue_t63790E87CDE9F924E3B5E0A6708EC2E4F2575E24), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3856[4] = 
{
	gbnJVSILPyqAUDavYEfkWfNLGue_t63790E87CDE9F924E3B5E0A6708EC2E4F2575E24::get_offset_of_UZbYDMRHqoMYGDwwWIwoKVeBvKy_0(),
	gbnJVSILPyqAUDavYEfkWfNLGue_t63790E87CDE9F924E3B5E0A6708EC2E4F2575E24::get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_1(),
	gbnJVSILPyqAUDavYEfkWfNLGue_t63790E87CDE9F924E3B5E0A6708EC2E4F2575E24::get_offset_of_bAgBXUBUQoTpqsGAsnokCWHlafSk_2(),
	gbnJVSILPyqAUDavYEfkWfNLGue_t63790E87CDE9F924E3B5E0A6708EC2E4F2575E24::get_offset_of_nnUyLogROOGBhfASxGsUKPevawbu_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3857 = { sizeof (XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3857[8] = 
{
	XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3::get_offset_of_jUSDSUlgfBsSujTyNlnzLqBbFdw_0(),
	XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3::get_offset_of_dgLozgQVKHMtlxHeFMWimlOECpV_1(),
	XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3::get_offset_of_QhSKLZxOqGNPKRxKaNHfSppwcbn_2(),
	XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3::get_offset_of_RCEncWjZiMinThJdsjjbhSaYuNs_3(),
	XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3::get_offset_of_YKgbhstxodGBLEVVbsiCVCSOOEoT_4(),
	XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3::get_offset_of_YHRBQXwgXlINPMUVNgZquixRWBi_5(),
	XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3::get_offset_of_axNahZYdpeLFpPhlwXuRRzqrbYr_6(),
	XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3::get_offset_of_xrZiXLzZxQBrLZscKvoqHlIUAaRa_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3858 = { sizeof (xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3858[11] = 
{
	xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032::get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0(),
	xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032::get_offset_of_UWLOvhrjuLGUzrhYiVuOYFsiVgW_1(),
	xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032::get_offset_of_EDrPGdciFGATRHQnPSEzqJMwhNu_2(),
	xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032::get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_3(),
	xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032::get_offset_of_QaeAlqSBKVmDRaDTcYzPlckcanC_4(),
	xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032::get_offset_of_eDZexKHojMIOIcGgWyZwyijReQJ_5(),
	xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032::get_offset_of_CPuSQMXEaFTdKTziCifqLUFakpe_6(),
	xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032::get_offset_of_maMGvvcIaoKXvKiwHYWRqpyTbPEe_7(),
	xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032::get_offset_of_VLynsvsphSPgOwnGXUBktrUfpoK_8(),
	xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032::get_offset_of_iMsaNGIEEvaEEkHBQjTXifqapVaA_9(),
	xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032::get_offset_of_AzcaYoGJNjQgjFntSQDdTBEQrJJI_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3859 = { sizeof (rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3859[4] = 
{
	rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224::get_offset_of_iwVNvyACihThlfmpkfURWPSdJYs_0(),
	rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224::get_offset_of_BnoetkxVNQEHfZTFFfghcpdJFzb_1(),
	rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224::get_offset_of_biNwZqyjNTmtSrqBtAcTREmMQcl_2(),
	rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224::get_offset_of_BtGnDdGQrTXaaWswowVwZiAMcDv_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3860 = { sizeof (mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3860[4] = 
{
	mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820::get_offset_of_zGAzEsGrTTTqWZqBgkORnHasWbZ_0(),
	mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820::get_offset_of_HEgvcyfmgQRVtohyvBGRboKAUBC_1(),
	mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820::get_offset_of_gbMrYrhHSKKiezarhnNWxHrOFkF_2(),
	mGZguqdUJPvKvEhsdVIRYohlOty_t6A4533BF4F06562072BCC6830BB48BBDBE2F8820::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3861 = { sizeof (PlatformInitializer_t64ECE8EB49A26C8FEF0D737A195DF35E77D74E3A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3862 = { sizeof (PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3862[5] = 
{
	PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A::get_offset_of__DeviceConnectedEvent_0(),
	PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A::get_offset_of__DeviceDisconnectedEvent_1(),
	PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A::get_offset_of__UpdateControllerInfoEvent_2(),
	PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A::get_offset_of__SystemDeviceConnectedEvent_3(),
	PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A::get_offset_of__SystemDeviceDisconnectedEvent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3863 = { sizeof (UnknownControllerHat_tFB49467CAB472A384C0BACB9AD9E46D7826B583E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3863[1] = 
{
	UnknownControllerHat_tFB49467CAB472A384C0BACB9AD9E46D7826B583E::get_offset_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3864 = { sizeof (HatButtons_t1754401799C3C4C561D1A750E79B02F35BBFB594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3864[1] = 
{
	HatButtons_t1754401799C3C4C561D1A750E79B02F35BBFB594::get_offset_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3865 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3865[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3866 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3866[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3867 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3867[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3868 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3868[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3869 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3869[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3870 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3870[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3871 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3871[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3872 = { sizeof (iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C), -1, sizeof(iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3872[6] = 
{
	iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C::get_offset_of_ffNWJUuxPxuFMIZszOmCdhFAkUp_0(),
	iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C::get_offset_of_gfxPfklYaqjBddECvDavUajksOb_1(),
	iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C::get_offset_of_rDzsPJfHrwraGtPiYaOBvJhwBKR_2(),
	iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C::get_offset_of_VfgWBBRhJvXUrlfIjIDaVDbCzHO_3(),
	iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C::get_offset_of_DlroOwqWNriauwRoYVGSrdUcMeH_4(),
	iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C_StaticFields::get_offset_of_wKZsRNpOhQctCfJnvfAdTetobSIx_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3873 = { sizeof (lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3873[6] = 
{
	lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3::get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0(),
	lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3::get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1(),
	lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3::get_offset_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_2(),
	lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_3(),
	lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3::get_offset_of_TpZRbqnqIopRkkdXAgptIogmhWjQ_4(),
	lcCLwaBQyqptCsvRVWlzpdLzWrX_tD5BDC5B1D38F783311946D9C10153FAE51A1CFF3::get_offset_of_qTlZkuASICRAgfMddrzensvBHjp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3874 = { sizeof (xnALzOoCrTHKpfeuVfrXbNCCdvs_t076F5D3CDBCCFE802032448DDB5F47D9DA960E34), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3874[1] = 
{
	xnALzOoCrTHKpfeuVfrXbNCCdvs_t076F5D3CDBCCFE802032448DDB5F47D9DA960E34::get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3875 = { sizeof (sIziMzSfsDgoaxCQQkceytcOvsu_tF2A5904AE6368332273AEA1389945FFD00F0FCF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3875[2] = 
{
	sIziMzSfsDgoaxCQQkceytcOvsu_tF2A5904AE6368332273AEA1389945FFD00F0FCF9::get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0(),
	sIziMzSfsDgoaxCQQkceytcOvsu_tF2A5904AE6368332273AEA1389945FFD00F0FCF9::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3876 = { sizeof (hPJUHyoOukVQYoALQdLyCcdDHpsQ_t0B149D9BB277329A45ECFC663C989860A6F478F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3876[2] = 
{
	hPJUHyoOukVQYoALQdLyCcdDHpsQ_t0B149D9BB277329A45ECFC663C989860A6F478F2::get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0(),
	hPJUHyoOukVQYoALQdLyCcdDHpsQ_t0B149D9BB277329A45ECFC663C989860A6F478F2::get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3877 = { sizeof (jqEbnEcwpRqTOaveGfPPDkvHyRFW_t1BC9163F7039E552E0CEAA6FF859C8C60D388CED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3877[2] = 
{
	jqEbnEcwpRqTOaveGfPPDkvHyRFW_t1BC9163F7039E552E0CEAA6FF859C8C60D388CED::get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0(),
	jqEbnEcwpRqTOaveGfPPDkvHyRFW_t1BC9163F7039E552E0CEAA6FF859C8C60D388CED::get_offset_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3878 = { sizeof (ZjQLmQAbXQOFiObTMteizIfPzRJ_tB78186632CF73E7D0D4E9C0CA3BD76D40E70155C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3878[3] = 
{
	ZjQLmQAbXQOFiObTMteizIfPzRJ_tB78186632CF73E7D0D4E9C0CA3BD76D40E70155C::get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0(),
	ZjQLmQAbXQOFiObTMteizIfPzRJ_tB78186632CF73E7D0D4E9C0CA3BD76D40E70155C::get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1(),
	ZjQLmQAbXQOFiObTMteizIfPzRJ_tB78186632CF73E7D0D4E9C0CA3BD76D40E70155C::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3879 = { sizeof (yMbNMrIvehttAgLshzTbZCRLIdT_tF3FC4094FBDA04968038F35233CAB7861952AB60), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3879[4] = 
{
	yMbNMrIvehttAgLshzTbZCRLIdT_tF3FC4094FBDA04968038F35233CAB7861952AB60::get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0(),
	yMbNMrIvehttAgLshzTbZCRLIdT_tF3FC4094FBDA04968038F35233CAB7861952AB60::get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1(),
	yMbNMrIvehttAgLshzTbZCRLIdT_tF3FC4094FBDA04968038F35233CAB7861952AB60::get_offset_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_2(),
	yMbNMrIvehttAgLshzTbZCRLIdT_tF3FC4094FBDA04968038F35233CAB7861952AB60::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3880 = { sizeof (HNdTWHYnytHuvNZEIsbtEiWDias_tC170FDA23FBA3F97C33BC5772C746C3A03F375B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3880[3] = 
{
	HNdTWHYnytHuvNZEIsbtEiWDias_tC170FDA23FBA3F97C33BC5772C746C3A03F375B5::get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0(),
	HNdTWHYnytHuvNZEIsbtEiWDias_tC170FDA23FBA3F97C33BC5772C746C3A03F375B5::get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_1(),
	HNdTWHYnytHuvNZEIsbtEiWDias_tC170FDA23FBA3F97C33BC5772C746C3A03F375B5::get_offset_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3881 = { sizeof (NrCngiFStASsVDcVGkiZDSMVDsZ_t30324632229C71C3C3959B9AB8C431CFFED6A432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3881[3] = 
{
	NrCngiFStASsVDcVGkiZDSMVDsZ_t30324632229C71C3C3959B9AB8C431CFFED6A432::get_offset_of_ekrwGUjbOGqAHCaagByDbWSadpQb_0(),
	NrCngiFStASsVDcVGkiZDSMVDsZ_t30324632229C71C3C3959B9AB8C431CFFED6A432::get_offset_of_SKPOVoCvhMuMjlLtGtXsXLVxqih_1(),
	NrCngiFStASsVDcVGkiZDSMVDsZ_t30324632229C71C3C3959B9AB8C431CFFED6A432::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3882 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3883 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3883[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3884 = { sizeof (GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3884[4] = 
{
	GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99::get_offset_of_UZbYDMRHqoMYGDwwWIwoKVeBvKy_0(),
	GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99::get_offset_of_QaeAlqSBKVmDRaDTcYzPlckcanC_1(),
	GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99::get_offset_of_eDZexKHojMIOIcGgWyZwyijReQJ_2(),
	GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99::get_offset_of_CPuSQMXEaFTdKTziCifqLUFakpe_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3885 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3885[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3886 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3886[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3887 = { sizeof (ButtonLoopSet_t043F70DD0163AEE263E80CB7B1AA6C550C46F0AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3887[1] = 
{
	ButtonLoopSet_t043F70DD0163AEE263E80CB7B1AA6C550C46F0AC::get_offset_of_buttonCount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3888 = { sizeof (ButtonData_tC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3888[5] = 
{
	ButtonData_tC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A::get_offset_of_updateLoop_0(),
	ButtonData_tC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A::get_offset_of_values_1(),
	ButtonData_tC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A::get_offset_of_wasTrueThisFrame_2(),
	ButtonData_tC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A::get_offset_of_TJEuDyBUDSfPkJfaqCBiqZkqLul_3(),
	ButtonData_tC59217A5753F3A051AAC5E1F8A76B2746D7DEF2A::get_offset_of_EhtoTVELaSIHwGdNrAhgaKELEwF_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3889 = { sizeof (LowLevelEvent_t0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC), -1, sizeof(LowLevelEvent_t0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3889[3] = 
{
	LowLevelEvent_t0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC::get_offset_of_id_0(),
	LowLevelEvent_t0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC::get_offset_of_timestamp_1(),
	LowLevelEvent_t0900FF0F5AE9AF92AD2B3D40D6AB073149EBF9CC_StaticFields::get_offset_of_eKZYZsxlXCNUTCcqXlqIvvDriCI_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3890 = { sizeof (LowLevelButtonEvent_tE21E5494A12F66FFF33D9B53EB51664F9C8C0F93), -1, sizeof(LowLevelButtonEvent_tE21E5494A12F66FFF33D9B53EB51664F9C8C0F93_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3890[4] = 
{
	0,
	LowLevelButtonEvent_tE21E5494A12F66FFF33D9B53EB51664F9C8C0F93::get_offset_of_value_4(),
	LowLevelButtonEvent_tE21E5494A12F66FFF33D9B53EB51664F9C8C0F93_StaticFields::get_offset_of_PavQAlUTQJCXgnaaIZJqMrVxxkr_5(),
	LowLevelButtonEvent_tE21E5494A12F66FFF33D9B53EB51664F9C8C0F93_StaticFields::get_offset_of_wKZsRNpOhQctCfJnvfAdTetobSIx_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3891 = { sizeof (TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D)+ sizeof (RuntimeObject), sizeof(TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3891[8] = 
{
	TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D::get_offset_of_EdmoIszvSQiLETihGiwjrkdiQJU_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D::get_offset_of_shnrdezQGXIItRKVkXhhfZjvcwa_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D::get_offset_of_YXkDhwfemImjfHFJhdSNRJexfGgA_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D::get_offset_of_irIPhBYSHZlBFwUDdzEgTfZukJE_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D::get_offset_of_eHxTHGHKMOrRUoTqLfqgKPRBDIt_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D::get_offset_of_kSivYNpyDJjvIFHXOQPYhcizfxN_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D::get_offset_of_iHeBEGNoWdoYQwsEbhDOjvTqonF_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInfo_t08476F6130C3D14C8795AFF139F5817E2938674D::get_offset_of_gNULUQfmkDmUrVjFTWlbDFfwiER_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3892 = { sizeof (SteamAction_t767832F58FDB51E2B4366EFA8F0E75F303F334F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3892[2] = 
{
	SteamAction_t767832F58FDB51E2B4366EFA8F0E75F303F334F9::get_offset_of_name_0(),
	SteamAction_t767832F58FDB51E2B4366EFA8F0E75F303F334F9::get_offset_of_handle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3893 = { sizeof (SteamActionSet_t2915FD1D24DF9379EC6DFD4E260EBBB4D0C6751F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3893[3] = 
{
	SteamActionSet_t2915FD1D24DF9379EC6DFD4E260EBBB4D0C6751F::get_offset_of_name_0(),
	SteamActionSet_t2915FD1D24DF9379EC6DFD4E260EBBB4D0C6751F::get_offset_of_handle_1(),
	SteamActionSet_t2915FD1D24DF9379EC6DFD4E260EBBB4D0C6751F::get_offset_of_actions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3894 = { sizeof (HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4)+ sizeof (RuntimeObject), sizeof(HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3894[4] = 
{
	HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4::get_offset_of_guid_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4::get_offset_of_inputSource_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4::get_offset_of_actualInputPlatform_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HardwareControllerMapIdentifier_t7AF3FFFB9A50C7E7F96D6F1C88A5C2438C6203E4::get_offset_of_variantIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3895 = { sizeof (RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA)+ sizeof (RuntimeObject), sizeof(RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3895[5] = 
{
	RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA::get_offset_of_version1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA::get_offset_of_version2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA::get_offset_of_version3_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA::get_offset_of_version4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RewiredVersion_t3436502C80D66857DD1EA07C63AE75F2D9040DEA::get_offset_of_unityVersion_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3896 = { sizeof (PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D)+ sizeof (RuntimeObject), sizeof(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D ), 0, 0 };
extern const int32_t g_FieldOffsetTable3896[3] = 
{
	0,
	PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D::get_offset_of_productId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D::get_offset_of_vendorId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3897 = { sizeof (Profiler_t489149C2DA8D0C908FD40198193B90E529F9DFE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3897[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3898 = { sizeof (EnumConverter_t9250FAC43470C04C58D2E822F917AB20D2E7AAC5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3899 = { sizeof (ThreadSafeUnityInput_t02E9717CF3743E0C27E835225F04C29E37CC93B1), -1, sizeof(ThreadSafeUnityInput_t02E9717CF3743E0C27E835225F04C29E37CC93B1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3899[2] = 
{
	ThreadSafeUnityInput_t02E9717CF3743E0C27E835225F04C29E37CC93B1_StaticFields::get_offset_of_pUaKhZxxkurJxwkFwfLAthUCtbX_0(),
	ThreadSafeUnityInput_t02E9717CF3743E0C27E835225F04C29E37CC93B1_StaticFields::get_offset_of_SkajjHJRJlbzVUcqDEpAVsLcNRC_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
