﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// Rewired.ControllerMap
struct ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB;
// Rewired.ControllerMap[]
struct ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E;
// Rewired.InputCategory
struct InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918;
// Rewired.InputMapCategory
struct InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602;
// Rewired.ReInput/MappingHelper
struct MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6;
// Rewired.Utils.Classes.Data.AList`1<Rewired.ActionElementMap>
struct AList_1_t3CE454271698B03A6EA53DA068272454E1A22108;
// System.ArgumentNullException
struct ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IEnumerable`1<Rewired.ControllerMap>
struct IEnumerable_1_t06A83164121EF2D4BC775427918CA10743B9E150;
// System.Collections.Generic.IEnumerator`1<Rewired.ControllerMap>
struct IEnumerator_1_tE4594D4F4247CA1E6C800101476D4854FC0747E0;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_tDDB69E91697CCB64C7993B651487CEEC287DB7E8;
// System.Collections.Generic.IList`1<Rewired.ControllerMap>
struct IList_1_t0AA780B699DC47233A67CE0B7A7905781C4E78B4;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_tE09735A322C3B17000EF4E4BC8026FEDEB7B0D9B;
// System.Collections.Generic.List`1<Rewired.ControllerMap>
struct List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>
struct Stack_1_t8E8DE8B40F82D27FFD37A645E4DE60FF495EAE00;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>
struct Stack_1_t83EEA6C9C9B43999F0FB28B378D20F56711A9DB7;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct Stack_1_tA8954C7914CA3BFAB547D0F09691E26AFB6138A3;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct Stack_1_t24347DA243AA3515E9AE6C68F02B953993FF98D8;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct Stack_1_tE4D0BA5034D7B700F0C2C183AC42BD2C969E4A6C;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct Stack_1_tA30706EF9E94D43BFD43FE4AE0AB0499A9A1682B;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct Stack_1_tAB502A0341E85CB581B7AF26F8E45C60127741F9;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.ActionElementMap>
struct ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.ControllerMap>
struct ReadOnlyCollection_1_t42C6EC4956F81037B0696DDF4310EEAD899406F9;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t0B2A88F2C4A2D26A4E1526F7063F654FAA26CB2A;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t5D996E967221C71E4EC5CC11210C3076432D5A50;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.LocalDataStoreHolder
struct LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304;
// System.LocalDataStoreMgr
struct LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9;
// System.MulticastDelegate
struct MulticastDelegate_t;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.Security.Principal.IPrincipal
struct IPrincipal_t63FD7F58FBBE134C8FE4D31710AAEA00B000F0BF;
// System.String
struct String_t;
// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo>
struct AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A;
// System.Threading.ExecutionContext
struct ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70;
// System.Threading.InternalThread
struct InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192;
// System.Threading.Thread
struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Int32>>
struct UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>
struct UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
struct ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0;
// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback
struct FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>
struct U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>
struct ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>
struct ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39;
// UnityEngine.UI.ObjectPool`1<System.Object>
struct ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/EventFunction`2<System.Object,System.Boolean>
struct EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/EventFunction`2<System.Object,System.Object>
struct EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/EventFunction`2<System.Object,UnityEngine.Vector2>
struct EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/HierarchyEventHelper`2<System.Object,System.Boolean>
struct HierarchyEventHelper_2_t83507A10FBC53F7024CE976D2CCBF4918C8D50EB;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/HierarchyEventHelper`2<System.Object,System.Object>
struct HierarchyEventHelper_2_t21ADBD5576AA92276647AD1353AA78AC39209E80;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/HierarchyEventHelper`2<System.Object,UnityEngine.Vector2>
struct HierarchyEventHelper_2_t87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5;
// lVPGlCbkNvrVSnFFcfgwEyebHaly/iFUZfyBAetyFZkyXRrMrwIZFTkH<System.Object>
struct iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA;
// lVPGlCbkNvrVSnFFcfgwEyebHaly/vwXNfeiXbIWciLlYtCJvcEfEAOn<System.Object>
struct vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B;
// lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>
struct lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642;

extern RuntimeClass* ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var;
extern RuntimeClass* Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_il2cpp_TypeInfo_var;
extern RuntimeClass* ReadOnlyCollection_1_t42C6EC4956F81037B0696DDF4310EEAD899406F9_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral04231B44477132B3DBEFE7768A921AE5A13A00FC;
extern String_t* _stringLiteral52934C29CA4A1A7572C6905BD6AB705A2C135973;
extern String_t* _stringLiteralA413973124713A2B7B3570CE8D97C7151C8628A9;
extern const RuntimeMethod* HierarchyEventHelper_2__ctor_m62B5AA7CB95B62E5E997455E57BEC60104C7706E_RuntimeMethod_var;
extern const RuntimeMethod* HierarchyEventHelper_2__ctor_m8D0859A65CA8D3ADD3F30EA8AAD2E17BD58DC87C_RuntimeMethod_var;
extern const RuntimeMethod* HierarchyEventHelper_2__ctor_mE158238CBBCDE89D494FC2BC6A2257D2D07E1A70_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mBB024C6A0C23217B87FABE55861D2532378BEE1B_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m88D5862BAF30335E7FEA486544F28FA5CECFF6DE_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m89A219C1A47D8F7CEDA4FAD3FD848DBCC009D28C_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m57FD5A381EAD73CDC168FFEC93C5D22EEA7CCC7C_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m0839F8110366CBDBC753D441E962E51A20E93E14_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_mB87AFEFB187F15D3B2B41BA3C4919E74A8256840_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_mADF483DC22F60B179F834797734DFFBAE6123F5A_RuntimeMethod_var;
extern const RuntimeMethod* List_1_set_Item_m86D714200344F5D83977C34422287C90AC3DFD80_RuntimeMethod_var;
extern const RuntimeMethod* ReadOnlyCollection_1__ctor_m43F319804F61EAF5B1BA3BEE99D5EC8F37B473DA_RuntimeMethod_var;
extern const RuntimeMethod* U3CStartU3Ec__Iterator0_Reset_m439E251EE39AF2851415914D8F11867A65D9E63F_RuntimeMethod_var;
extern const RuntimeMethod* U3CStartU3Ec__Iterator0_Reset_m7703121C2007A9FF4EC664BC758BC00DF683B629_RuntimeMethod_var;
extern const RuntimeMethod* iFUZfyBAetyFZkyXRrMrwIZFTkH_System_Collections_IEnumerator_Reset_m7329C4E00CED4146273EC24B4A2B35EA71C4E098_RuntimeMethod_var;
extern const RuntimeMethod* vwXNfeiXbIWciLlYtCJvcEfEAOn_System_Collections_IEnumerator_Reset_m817D855ED6F146D85D71168D06FD7B2263491D77_RuntimeMethod_var;
extern const RuntimeType* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_0_0_0_var;
extern const RuntimeType* List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_0_0_0_var;
extern const RuntimeType* List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_0_0_0_var;
extern const RuntimeType* List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554_0_0_0_var;
extern const RuntimeType* List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5_0_0_0_var;
extern const RuntimeType* List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_0_0_0_var;
extern const RuntimeType* List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_0_0_0_var;
extern const RuntimeType* List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955_0_0_0_var;
extern const RuntimeType* ObjectPool_1_tF21D8F1FA7A3316A2EE53B04A5F84B392C1395E9_0_0_0_var;
extern const RuntimeType* RuntimeObject_0_0_0_var;
extern const RuntimeType* TweenRunner_1_t1F984CEC16263BC8733F0415773C3091CE134E36_0_0_0_var;
extern const RuntimeType* U3CStartU3Ec__Iterator0_t8ED475A52DA2C20C9BEB38F8EED315D0164E7594_0_0_0_var;
extern const RuntimeType* UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_0_0_0_var;
extern const uint32_t EventFunction_2_BeginInvoke_m1BA75FDD482EA91164FA95B9DC0A56B7B9E3EFB4_MetadataUsageId;
extern const uint32_t EventFunction_2_BeginInvoke_m678D466D954B18E90044F2E025C4819A4A463F72_MetadataUsageId;
extern const uint32_t HierarchyEventHelper_2_GetHandlers_m87C62AA34372043B8E1D58CF9D8538744876E955_MetadataUsageId;
extern const uint32_t HierarchyEventHelper_2_GetHandlers_mB462B74FCE3EEAEB3AA2C18AD13CD2A6D7BAE054_MetadataUsageId;
extern const uint32_t HierarchyEventHelper_2_GetHandlers_mCC5FAF87AB7009238B5D5BDD76458D229D975A00_MetadataUsageId;
extern const uint32_t HierarchyEventHelper_2__ctor_m62B5AA7CB95B62E5E997455E57BEC60104C7706E_MetadataUsageId;
extern const uint32_t HierarchyEventHelper_2__ctor_m8D0859A65CA8D3ADD3F30EA8AAD2E17BD58DC87C_MetadataUsageId;
extern const uint32_t HierarchyEventHelper_2__ctor_mE158238CBBCDE89D494FC2BC6A2257D2D07E1A70_MetadataUsageId;
extern const uint32_t ListPool_1_Get_m0DA6F4FA9B55233F3BCC582E1B5D1B82048B72DC_MetadataUsageId;
extern const uint32_t ListPool_1_Get_m1549C4AC8324D6BBB9CAA80B70EE13A6AC0617B7_MetadataUsageId;
extern const uint32_t ListPool_1_Get_m3D24B0F028698E7FA9EAB826563501BFF986BBFD_MetadataUsageId;
extern const uint32_t ListPool_1_Get_m4322F57FCB5B11C10CAEDE17A609B9E29401797F_MetadataUsageId;
extern const uint32_t ListPool_1_Get_m4C328048C1479EE1450837A0CF1BF5F18FF77C88_MetadataUsageId;
extern const uint32_t ListPool_1_Get_mAFB1F76A280309F30C0897B1FBABCC37A783344C_MetadataUsageId;
extern const uint32_t ListPool_1_Get_mECE6D9ED10FB4EEF28A6BE1F9B445CEE4312A937_MetadataUsageId;
extern const uint32_t ObjectPool_1_Get_m82BA8AF24CE96731493EC3C52B1218DB2C3C934F_MetadataUsageId;
extern const uint32_t ObjectPool_1_Release_mEF76D0678288FA4D4D8D81C57B3FEBF7AE87BD74_MetadataUsageId;
extern const uint32_t ObjectPool_1__ctor_mE14E9596DD21AFBA47727BDC5226C7F24FA234C7_MetadataUsageId;
extern const uint32_t ObjectPool_1_get_countActive_m12221F42D2CD25F418A1746CFEE20C65A630B6F1_MetadataUsageId;
extern const uint32_t ObjectPool_1_get_countAll_m2336F21CA9AFFCBF50463DD1836B14E663836D58_MetadataUsageId;
extern const uint32_t ObjectPool_1_get_countInactive_mF6EBCB20814CAE2E818F846689640BF4485E810F_MetadataUsageId;
extern const uint32_t ObjectPool_1_set_countAll_mF68D62D98B4F6AFACAC85C02E4DB858DA7196F64_MetadataUsageId;
extern const uint32_t TweenRunner_1_Init_m39096EAEB1B5BFC0E2FEDF4A946593353F8981CC_MetadataUsageId;
extern const uint32_t TweenRunner_1_Init_mA29C09ADC3EB6959A0F1572D48D84170443B670E_MetadataUsageId;
extern const uint32_t TweenRunner_1_StartTween_m8637A776CD96BAB0EDC8A8FA7479127E2BEC92C4_MetadataUsageId;
extern const uint32_t TweenRunner_1_StartTween_mE0CB96AF945209ABC26F2AA9899CB9794A64D92D_MetadataUsageId;
extern const uint32_t TweenRunner_1_StopTween_m861C40714D7A8C4B7EF8A7CC781B06C600877A5F_MetadataUsageId;
extern const uint32_t TweenRunner_1_StopTween_m9EDE8CC585AD166D4520BE8B374CA2169CDD0E8E_MetadataUsageId;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m17A6A3B6131EFE960C4DA7784DDE816250347E99_MetadataUsageId;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_mB6BE65FBF43A13162E304F0F012BAA539F3D0C9F_MetadataUsageId;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m439E251EE39AF2851415914D8F11867A65D9E63F_MetadataUsageId;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m7703121C2007A9FF4EC664BC758BC00DF683B629_MetadataUsageId;
extern const uint32_t iFUZfyBAetyFZkyXRrMrwIZFTkH_System_Collections_IEnumerator_Reset_m7329C4E00CED4146273EC24B4A2B35EA71C4E098_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_ClearByLayout_mEE74790663D60300420AEA46949EFD03F6EDE8B0_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_Clear_m03E91053683751E1C80B4D40F3901A426A586ADC_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_Clear_m17040078739B7A2EE787FDC6B7FD81FF4BAD9F70_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_HfTIrZGXLVOHtJVamKOSesGBJhAd_m587BDF7DB8BE120243C019DB388AC44085CAB584_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_RemoveById_m4AB157EE1AAACECF4CE3F2E7B04AEEF16172A787_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_Remove_m9A9E565055D1562B04563FC758F5E360530F24A7_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_Remove_mAFD5E3D961FAA25E86F20AA6DEF6402EEFDBFDB0_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMapsByCategory_m0716D053D759102191A5B66E93B52FD44750A4C8_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMapsByCategory_m8EB2F2A29929732745327E724492945704260FF0_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMaps_m009C48D29322842D64BD61AD8B68EE35D8A5A21C_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMaps_m44238BB3AF8633555345CEFB166746EC309E4A76_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_get_Item_m913ED211EB3D2D0DD0CC4A40E72054B8FC31E573_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly__ctor_mA441F7FE7C3019A0B8D567C19BF2AD8E37C94918_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_set_Item_mE7609C821890FAE4792AFB1E0301373AF644510F_MetadataUsageId;
extern const uint32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_yhpcGPaRrkCsNfCHablLblmduzKL_m18EEC4A91A776E5BF91177E6E25CB9860B71456D_MetadataUsageId;
extern const uint32_t vwXNfeiXbIWciLlYtCJvcEfEAOn_MoveNext_m8FB51981331E245C343BA404694A68C73F91CA62_MetadataUsageId;
extern const uint32_t vwXNfeiXbIWciLlYtCJvcEfEAOn_System_Collections_IEnumerator_Reset_m817D855ED6F146D85D71168D06FD7B2263491D77_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E;
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef INPUTCATEGORY_T775B2399E2070EF82BBA5D9676BD00C30159D918_H
#define INPUTCATEGORY_T775B2399E2070EF82BBA5D9676BD00C30159D918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputCategory
struct  InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918  : public RuntimeObject
{
public:
	// System.String Rewired.InputCategory::_name
	String_t* ____name_0;
	// System.String Rewired.InputCategory::_descriptiveName
	String_t* ____descriptiveName_1;
	// System.String Rewired.InputCategory::_tag
	String_t* ____tag_2;
	// System.Int32 Rewired.InputCategory::_id
	int32_t ____id_3;
	// System.Boolean Rewired.InputCategory::_userAssignable
	bool ____userAssignable_4;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__descriptiveName_1() { return static_cast<int32_t>(offsetof(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918, ____descriptiveName_1)); }
	inline String_t* get__descriptiveName_1() const { return ____descriptiveName_1; }
	inline String_t** get_address_of__descriptiveName_1() { return &____descriptiveName_1; }
	inline void set__descriptiveName_1(String_t* value)
	{
		____descriptiveName_1 = value;
		Il2CppCodeGenWriteBarrier((&____descriptiveName_1), value);
	}

	inline static int32_t get_offset_of__tag_2() { return static_cast<int32_t>(offsetof(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918, ____tag_2)); }
	inline String_t* get__tag_2() const { return ____tag_2; }
	inline String_t** get_address_of__tag_2() { return &____tag_2; }
	inline void set__tag_2(String_t* value)
	{
		____tag_2 = value;
		Il2CppCodeGenWriteBarrier((&____tag_2), value);
	}

	inline static int32_t get_offset_of__id_3() { return static_cast<int32_t>(offsetof(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918, ____id_3)); }
	inline int32_t get__id_3() const { return ____id_3; }
	inline int32_t* get_address_of__id_3() { return &____id_3; }
	inline void set__id_3(int32_t value)
	{
		____id_3 = value;
	}

	inline static int32_t get_offset_of__userAssignable_4() { return static_cast<int32_t>(offsetof(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918, ____userAssignable_4)); }
	inline bool get__userAssignable_4() const { return ____userAssignable_4; }
	inline bool* get_address_of__userAssignable_4() { return &____userAssignable_4; }
	inline void set__userAssignable_4(bool value)
	{
		____userAssignable_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTCATEGORY_T775B2399E2070EF82BBA5D9676BD00C30159D918_H
#ifndef CODEHELPER_T8618ECFBF040A78FEDA26D398742608E1AA036AB_H
#define CODEHELPER_T8618ECFBF040A78FEDA26D398742608E1AA036AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.CodeHelper
struct  CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEHELPER_T8618ECFBF040A78FEDA26D398742608E1AA036AB_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T4C65ABDD2ABD8E706F31D12EFF7B7308079F212E_H
#define LIST_1_T4C65ABDD2ABD8E706F31D12EFF7B7308079F212E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Rewired.ControllerMap>
struct  List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E, ____items_1)); }
	inline ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E* get__items_1() const { return ____items_1; }
	inline ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E_StaticFields, ____emptyArray_5)); }
	inline ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4C65ABDD2ABD8E706F31D12EFF7B7308079F212E_H
#ifndef LIST_1_TE1526161A558A17A39A8B69D8EEF3801393B6226_H
#define LIST_1_TE1526161A558A17A39A8B69D8EEF3801393B6226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____items_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TE1526161A558A17A39A8B69D8EEF3801393B6226_H
#ifndef LIST_1_T05CC3C859AB5E6024394EF9A42E3E696628CA02D_H
#define LIST_1_T05CC3C859AB5E6024394EF9A42E3E696628CA02D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T05CC3C859AB5E6024394EF9A42E3E696628CA02D_H
#ifndef LIST_1_T749ADA5233D9B421293A000DCB83608A24C3D5D5_H
#define LIST_1_T749ADA5233D9B421293A000DCB83608A24C3D5D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct  List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5, ____items_1)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get__items_1() const { return ____items_1; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5_StaticFields, ____emptyArray_5)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T749ADA5233D9B421293A000DCB83608A24C3D5D5_H
#ifndef LIST_1_T4CE16E1B496C9FE941554BB47727DFDD7C3D9554_H
#define LIST_1_T4CE16E1B496C9FE941554BB47727DFDD7C3D9554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct  List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554, ____items_1)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get__items_1() const { return ____items_1; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554_StaticFields, ____emptyArray_5)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4CE16E1B496C9FE941554BB47727DFDD7C3D9554_H
#ifndef LIST_1_T0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_H
#define LIST_1_T0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct  List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____items_1)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields, ____emptyArray_5)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_H
#ifndef LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#define LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____items_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#ifndef LIST_1_TFF4005B40E5BA433006DA11C56DB086B1E2FC955_H
#define LIST_1_TFF4005B40E5BA433006DA11C56DB086B1E2FC955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct  List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955, ____items_1)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get__items_1() const { return ____items_1; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955_StaticFields, ____emptyArray_5)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TFF4005B40E5BA433006DA11C56DB086B1E2FC955_H
#ifndef STACK_1_T5697A763CE21E705BB0297FFBE9AFCB5F95C9163_H
#define STACK_1_T5697A763CE21E705BB0297FFBE9AFCB5F95C9163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<System.Object>
struct  Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;
	// System.Object System.Collections.Generic.Stack`1::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163, ____array_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__array_0() const { return ____array_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T5697A763CE21E705BB0297FFBE9AFCB5F95C9163_H
#ifndef READONLYCOLLECTION_1_T42C6EC4956F81037B0696DDF4310EEAD899406F9_H
#define READONLYCOLLECTION_1_T42C6EC4956F81037B0696DDF4310EEAD899406F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.ControllerMap>
struct  ReadOnlyCollection_1_t42C6EC4956F81037B0696DDF4310EEAD899406F9  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1::list
	RuntimeObject* ___list_0;
	// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1::_syncRoot
	RuntimeObject * ____syncRoot_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollection_1_t42C6EC4956F81037B0696DDF4310EEAD899406F9, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of__syncRoot_1() { return static_cast<int32_t>(offsetof(ReadOnlyCollection_1_t42C6EC4956F81037B0696DDF4310EEAD899406F9, ____syncRoot_1)); }
	inline RuntimeObject * get__syncRoot_1() const { return ____syncRoot_1; }
	inline RuntimeObject ** get_address_of__syncRoot_1() { return &____syncRoot_1; }
	inline void set__syncRoot_1(RuntimeObject * value)
	{
		____syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYCOLLECTION_1_T42C6EC4956F81037B0696DDF4310EEAD899406F9_H
#ifndef READONLYCOLLECTION_1_T5D996E967221C71E4EC5CC11210C3076432D5A50_H
#define READONLYCOLLECTION_1_T5D996E967221C71E4EC5CC11210C3076432D5A50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct  ReadOnlyCollection_1_t5D996E967221C71E4EC5CC11210C3076432D5A50  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1::list
	RuntimeObject* ___list_0;
	// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1::_syncRoot
	RuntimeObject * ____syncRoot_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollection_1_t5D996E967221C71E4EC5CC11210C3076432D5A50, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of__syncRoot_1() { return static_cast<int32_t>(offsetof(ReadOnlyCollection_1_t5D996E967221C71E4EC5CC11210C3076432D5A50, ____syncRoot_1)); }
	inline RuntimeObject * get__syncRoot_1() const { return ____syncRoot_1; }
	inline RuntimeObject ** get_address_of__syncRoot_1() { return &____syncRoot_1; }
	inline void set__syncRoot_1(RuntimeObject * value)
	{
		____syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYCOLLECTION_1_T5D996E967221C71E4EC5CC11210C3076432D5A50_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#define CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct  CriticalFinalizerObject_t8B006E1DEE084E781F5C0F3283E9226E28894DD9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef TWEENRUNNER_1_T56CEB168ADE3739A1BDDBF258FDC759DF8927172_H
#define TWEENRUNNER_1_T56CEB168ADE3739A1BDDBF258FDC759DF8927172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct  TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour UnityEngine.UI.CoroutineTween.TweenRunner`1::m_CoroutineContainer
	MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___m_CoroutineContainer_0;
	// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1::m_Tween
	RuntimeObject* ___m_Tween_1;

public:
	inline static int32_t get_offset_of_m_CoroutineContainer_0() { return static_cast<int32_t>(offsetof(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172, ___m_CoroutineContainer_0)); }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * get_m_CoroutineContainer_0() const { return ___m_CoroutineContainer_0; }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** get_address_of_m_CoroutineContainer_0() { return &___m_CoroutineContainer_0; }
	inline void set_m_CoroutineContainer_0(MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		___m_CoroutineContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_CoroutineContainer_0), value);
	}

	inline static int32_t get_offset_of_m_Tween_1() { return static_cast<int32_t>(offsetof(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172, ___m_Tween_1)); }
	inline RuntimeObject* get_m_Tween_1() const { return ___m_Tween_1; }
	inline RuntimeObject** get_address_of_m_Tween_1() { return &___m_Tween_1; }
	inline void set_m_Tween_1(RuntimeObject* value)
	{
		___m_Tween_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tween_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENRUNNER_1_T56CEB168ADE3739A1BDDBF258FDC759DF8927172_H
#ifndef TWEENRUNNER_1_TA7C92F52BF30E9A20EDA2DD956E11A1493D098EF_H
#define TWEENRUNNER_1_TA7C92F52BF30E9A20EDA2DD956E11A1493D098EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct  TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour UnityEngine.UI.CoroutineTween.TweenRunner`1::m_CoroutineContainer
	MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___m_CoroutineContainer_0;
	// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1::m_Tween
	RuntimeObject* ___m_Tween_1;

public:
	inline static int32_t get_offset_of_m_CoroutineContainer_0() { return static_cast<int32_t>(offsetof(TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF, ___m_CoroutineContainer_0)); }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * get_m_CoroutineContainer_0() const { return ___m_CoroutineContainer_0; }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** get_address_of_m_CoroutineContainer_0() { return &___m_CoroutineContainer_0; }
	inline void set_m_CoroutineContainer_0(MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		___m_CoroutineContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_CoroutineContainer_0), value);
	}

	inline static int32_t get_offset_of_m_Tween_1() { return static_cast<int32_t>(offsetof(TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF, ___m_Tween_1)); }
	inline RuntimeObject* get_m_Tween_1() const { return ___m_Tween_1; }
	inline RuntimeObject** get_address_of_m_Tween_1() { return &___m_Tween_1; }
	inline void set_m_Tween_1(RuntimeObject* value)
	{
		___m_Tween_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tween_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENRUNNER_1_TA7C92F52BF30E9A20EDA2DD956E11A1493D098EF_H
#ifndef LISTPOOL_1_TFA23B363858EAC800B614A18D05C359F72028407_H
#define LISTPOOL_1_TFA23B363858EAC800B614A18D05C359F72028407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<System.Int32>
struct  ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_TFA23B363858EAC800B614A18D05C359F72028407_H
#ifndef LISTPOOL_1_T66B0CA6885E680896C9747F9C0E28458D59743BE_H
#define LISTPOOL_1_T66B0CA6885E680896C9747F9C0E28458D59743BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<System.Object>
struct  ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_T66B0CA6885E680896C9747F9C0E28458D59743BE_H
#ifndef LISTPOOL_1_TBA324F10FC7E73FB9F71457FFE143CD03160D463_H
#define LISTPOOL_1_TBA324F10FC7E73FB9F71457FFE143CD03160D463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<UnityEngine.Color32>
struct  ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_TBA324F10FC7E73FB9F71457FFE143CD03160D463_H
#ifndef LISTPOOL_1_TAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_H
#define LISTPOOL_1_TAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>
struct  ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_TAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_H
#ifndef LISTPOOL_1_TAB2044BEC36628D346141AEA4743A824A6FB688C_H
#define LISTPOOL_1_TAB2044BEC36628D346141AEA4743A824A6FB688C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<UnityEngine.Vector2>
struct  ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_TAB2044BEC36628D346141AEA4743A824A6FB688C_H
#ifndef LISTPOOL_1_TC0119DB1C2EC9C29F424EC953509E2CDC3995059_H
#define LISTPOOL_1_TC0119DB1C2EC9C29F424EC953509E2CDC3995059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<UnityEngine.Vector3>
struct  ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_TC0119DB1C2EC9C29F424EC953509E2CDC3995059_H
#ifndef LISTPOOL_1_TD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_H
#define LISTPOOL_1_TD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<UnityEngine.Vector4>
struct  ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_TD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_H
#ifndef OBJECTPOOL_1_TC08E18CA4686E07104774795FF479D68B6B2889A_H
#define OBJECTPOOL_1_TC08E18CA4686E07104774795FF479D68B6B2889A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>
struct  ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t8E8DE8B40F82D27FFD37A645E4DE60FF495EAE00 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A, ___m_Stack_0)); }
	inline Stack_1_t8E8DE8B40F82D27FFD37A645E4DE60FF495EAE00 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t8E8DE8B40F82D27FFD37A645E4DE60FF495EAE00 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t8E8DE8B40F82D27FFD37A645E4DE60FF495EAE00 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A, ___m_ActionOnGet_1)); }
	inline UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_TC08E18CA4686E07104774795FF479D68B6B2889A_H
#ifndef OBJECTPOOL_1_TDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B_H
#define OBJECTPOOL_1_TDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>
struct  ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t83EEA6C9C9B43999F0FB28B378D20F56711A9DB7 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B, ___m_Stack_0)); }
	inline Stack_1_t83EEA6C9C9B43999F0FB28B378D20F56711A9DB7 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t83EEA6C9C9B43999F0FB28B378D20F56711A9DB7 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t83EEA6C9C9B43999F0FB28B378D20F56711A9DB7 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_TDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B_H
#ifndef OBJECTPOOL_1_T6E42C9408E003E775EC7139A3F1EFC1346440D07_H
#define OBJECTPOOL_1_T6E42C9408E003E775EC7139A3F1EFC1346440D07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct  ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tA8954C7914CA3BFAB547D0F09691E26AFB6138A3 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07, ___m_Stack_0)); }
	inline Stack_1_tA8954C7914CA3BFAB547D0F09691E26AFB6138A3 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tA8954C7914CA3BFAB547D0F09691E26AFB6138A3 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tA8954C7914CA3BFAB547D0F09691E26AFB6138A3 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T6E42C9408E003E775EC7139A3F1EFC1346440D07_H
#ifndef OBJECTPOOL_1_TB641A4FECBF1E01BBA0C252F01EDE98D41033CF5_H
#define OBJECTPOOL_1_TB641A4FECBF1E01BBA0C252F01EDE98D41033CF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct  ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t24347DA243AA3515E9AE6C68F02B953993FF98D8 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5, ___m_Stack_0)); }
	inline Stack_1_t24347DA243AA3515E9AE6C68F02B953993FF98D8 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t24347DA243AA3515E9AE6C68F02B953993FF98D8 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t24347DA243AA3515E9AE6C68F02B953993FF98D8 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_TB641A4FECBF1E01BBA0C252F01EDE98D41033CF5_H
#ifndef OBJECTPOOL_1_T77D90EC466D5DC3CD8703898D0D3206B7D320D49_H
#define OBJECTPOOL_1_T77D90EC466D5DC3CD8703898D0D3206B7D320D49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct  ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tE4D0BA5034D7B700F0C2C183AC42BD2C969E4A6C * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49, ___m_Stack_0)); }
	inline Stack_1_tE4D0BA5034D7B700F0C2C183AC42BD2C969E4A6C * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tE4D0BA5034D7B700F0C2C183AC42BD2C969E4A6C ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tE4D0BA5034D7B700F0C2C183AC42BD2C969E4A6C * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T77D90EC466D5DC3CD8703898D0D3206B7D320D49_H
#ifndef OBJECTPOOL_1_T9CC17CF511664D2F103A4C4F73C9BD8820B88DF2_H
#define OBJECTPOOL_1_T9CC17CF511664D2F103A4C4F73C9BD8820B88DF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct  ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tA30706EF9E94D43BFD43FE4AE0AB0499A9A1682B * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2, ___m_Stack_0)); }
	inline Stack_1_tA30706EF9E94D43BFD43FE4AE0AB0499A9A1682B * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tA30706EF9E94D43BFD43FE4AE0AB0499A9A1682B ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tA30706EF9E94D43BFD43FE4AE0AB0499A9A1682B * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T9CC17CF511664D2F103A4C4F73C9BD8820B88DF2_H
#ifndef OBJECTPOOL_1_T89359398AF2898F35015A1938357AD5AC70B2C39_H
#define OBJECTPOOL_1_T89359398AF2898F35015A1938357AD5AC70B2C39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct  ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tAB502A0341E85CB581B7AF26F8E45C60127741F9 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39, ___m_Stack_0)); }
	inline Stack_1_tAB502A0341E85CB581B7AF26F8E45C60127741F9 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tAB502A0341E85CB581B7AF26F8E45C60127741F9 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tAB502A0341E85CB581B7AF26F8E45C60127741F9 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T89359398AF2898F35015A1938357AD5AC70B2C39_H
#ifndef OBJECTPOOL_1_T642A3D701C6162F913D9252AB3E5BEB96161F6BD_H
#define OBJECTPOOL_1_T642A3D701C6162F913D9252AB3E5BEB96161F6BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Object>
struct  ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD, ___m_Stack_0)); }
	inline Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T642A3D701C6162F913D9252AB3E5BEB96161F6BD_H
#ifndef YIELDINSTRUCTION_T836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_H
#define YIELDINSTRUCTION_T836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_H
#ifndef IFUZFYBAETYFZKYXRRMRWIZFTKH_TB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA_H
#define IFUZFYBAETYFZKYXRRMRWIZFTKH_TB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// lVPGlCbkNvrVSnFFcfgwEyebHaly_iFUZfyBAetyFZkyXRrMrwIZFTkH<System.Object>
struct  iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA  : public RuntimeObject
{
public:
	// T lVPGlCbkNvrVSnFFcfgwEyebHaly_iFUZfyBAetyFZkyXRrMrwIZFTkH::NOArrsNdfKDShNFftfbPqYDzhpq
	RuntimeObject * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly_iFUZfyBAetyFZkyXRrMrwIZFTkH::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// lVPGlCbkNvrVSnFFcfgwEyebHaly<T> lVPGlCbkNvrVSnFFcfgwEyebHaly_iFUZfyBAetyFZkyXRrMrwIZFTkH::DKaFTWTMhFeLQHDExnDMRxZfmROL
	lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2;
	// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly_iFUZfyBAetyFZkyXRrMrwIZFTkH::yLrOOaqYzmIyaXSCvApOKYnGotw
	int32_t ___yLrOOaqYzmIyaXSCvApOKYnGotw_3;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline RuntimeObject * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline RuntimeObject ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(RuntimeObject * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() { return static_cast<int32_t>(offsetof(iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2)); }
	inline lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2; }
	inline lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_2; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_2(lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_2 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_2), value);
	}

	inline static int32_t get_offset_of_yLrOOaqYzmIyaXSCvApOKYnGotw_3() { return static_cast<int32_t>(offsetof(iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA, ___yLrOOaqYzmIyaXSCvApOKYnGotw_3)); }
	inline int32_t get_yLrOOaqYzmIyaXSCvApOKYnGotw_3() const { return ___yLrOOaqYzmIyaXSCvApOKYnGotw_3; }
	inline int32_t* get_address_of_yLrOOaqYzmIyaXSCvApOKYnGotw_3() { return &___yLrOOaqYzmIyaXSCvApOKYnGotw_3; }
	inline void set_yLrOOaqYzmIyaXSCvApOKYnGotw_3(int32_t value)
	{
		___yLrOOaqYzmIyaXSCvApOKYnGotw_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IFUZFYBAETYFZKYXRRMRWIZFTKH_TB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA_H
#ifndef VWXNFEIXBIWCILLYTCJVCEFEAON_TBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B_H
#define VWXNFEIXBIWCILLYTCJVCEFEAON_TBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn<System.Object>
struct  vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B  : public RuntimeObject
{
public:
	// Rewired.ControllerMap lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// lVPGlCbkNvrVSnFFcfgwEyebHaly<T> lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn::DKaFTWTMhFeLQHDExnDMRxZfmROL
	lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn::OoFswlQbqBhHqGOCooflwYCYmYWF
	int32_t ___OoFswlQbqBhHqGOCooflwYCYmYWF_4;
	// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn::pyCASHftKaJjucJBlmeeWQrdKuRk
	int32_t ___pyCASHftKaJjucJBlmeeWQrdKuRk_5;
	// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn::okMFupfyoBNZDyKMyvAtwGIUiAd
	int32_t ___okMFupfyoBNZDyKMyvAtwGIUiAd_6;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_OoFswlQbqBhHqGOCooflwYCYmYWF_4() { return static_cast<int32_t>(offsetof(vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B, ___OoFswlQbqBhHqGOCooflwYCYmYWF_4)); }
	inline int32_t get_OoFswlQbqBhHqGOCooflwYCYmYWF_4() const { return ___OoFswlQbqBhHqGOCooflwYCYmYWF_4; }
	inline int32_t* get_address_of_OoFswlQbqBhHqGOCooflwYCYmYWF_4() { return &___OoFswlQbqBhHqGOCooflwYCYmYWF_4; }
	inline void set_OoFswlQbqBhHqGOCooflwYCYmYWF_4(int32_t value)
	{
		___OoFswlQbqBhHqGOCooflwYCYmYWF_4 = value;
	}

	inline static int32_t get_offset_of_pyCASHftKaJjucJBlmeeWQrdKuRk_5() { return static_cast<int32_t>(offsetof(vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B, ___pyCASHftKaJjucJBlmeeWQrdKuRk_5)); }
	inline int32_t get_pyCASHftKaJjucJBlmeeWQrdKuRk_5() const { return ___pyCASHftKaJjucJBlmeeWQrdKuRk_5; }
	inline int32_t* get_address_of_pyCASHftKaJjucJBlmeeWQrdKuRk_5() { return &___pyCASHftKaJjucJBlmeeWQrdKuRk_5; }
	inline void set_pyCASHftKaJjucJBlmeeWQrdKuRk_5(int32_t value)
	{
		___pyCASHftKaJjucJBlmeeWQrdKuRk_5 = value;
	}

	inline static int32_t get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_6() { return static_cast<int32_t>(offsetof(vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B, ___okMFupfyoBNZDyKMyvAtwGIUiAd_6)); }
	inline int32_t get_okMFupfyoBNZDyKMyvAtwGIUiAd_6() const { return ___okMFupfyoBNZDyKMyvAtwGIUiAd_6; }
	inline int32_t* get_address_of_okMFupfyoBNZDyKMyvAtwGIUiAd_6() { return &___okMFupfyoBNZDyKMyvAtwGIUiAd_6; }
	inline void set_okMFupfyoBNZDyKMyvAtwGIUiAd_6(int32_t value)
	{
		___okMFupfyoBNZDyKMyvAtwGIUiAd_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VWXNFEIXBIWCILLYTCJVCEFEAON_TBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B_H
#ifndef INPUTMAPCATEGORY_T3A6CAA2D6E69E45E734A026F26B816788D175602_H
#define INPUTMAPCATEGORY_T3A6CAA2D6E69E45E734A026F26B816788D175602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputMapCategory
struct  InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602  : public InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918
{
public:
	// System.Boolean Rewired.InputMapCategory::_checkConflictsWithAllCategories
	bool ____checkConflictsWithAllCategories_5;
	// System.Collections.Generic.List`1<System.Int32> Rewired.InputMapCategory::_checkConflictsCategoryIds
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____checkConflictsCategoryIds_6;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32> Rewired.InputMapCategory::_checkConflictsCategoryIds_readOnly
	ReadOnlyCollection_1_t0B2A88F2C4A2D26A4E1526F7063F654FAA26CB2A * ____checkConflictsCategoryIds_readOnly_7;

public:
	inline static int32_t get_offset_of__checkConflictsWithAllCategories_5() { return static_cast<int32_t>(offsetof(InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602, ____checkConflictsWithAllCategories_5)); }
	inline bool get__checkConflictsWithAllCategories_5() const { return ____checkConflictsWithAllCategories_5; }
	inline bool* get_address_of__checkConflictsWithAllCategories_5() { return &____checkConflictsWithAllCategories_5; }
	inline void set__checkConflictsWithAllCategories_5(bool value)
	{
		____checkConflictsWithAllCategories_5 = value;
	}

	inline static int32_t get_offset_of__checkConflictsCategoryIds_6() { return static_cast<int32_t>(offsetof(InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602, ____checkConflictsCategoryIds_6)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__checkConflictsCategoryIds_6() const { return ____checkConflictsCategoryIds_6; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__checkConflictsCategoryIds_6() { return &____checkConflictsCategoryIds_6; }
	inline void set__checkConflictsCategoryIds_6(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____checkConflictsCategoryIds_6 = value;
		Il2CppCodeGenWriteBarrier((&____checkConflictsCategoryIds_6), value);
	}

	inline static int32_t get_offset_of__checkConflictsCategoryIds_readOnly_7() { return static_cast<int32_t>(offsetof(InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602, ____checkConflictsCategoryIds_readOnly_7)); }
	inline ReadOnlyCollection_1_t0B2A88F2C4A2D26A4E1526F7063F654FAA26CB2A * get__checkConflictsCategoryIds_readOnly_7() const { return ____checkConflictsCategoryIds_readOnly_7; }
	inline ReadOnlyCollection_1_t0B2A88F2C4A2D26A4E1526F7063F654FAA26CB2A ** get_address_of__checkConflictsCategoryIds_readOnly_7() { return &____checkConflictsCategoryIds_readOnly_7; }
	inline void set__checkConflictsCategoryIds_readOnly_7(ReadOnlyCollection_1_t0B2A88F2C4A2D26A4E1526F7063F654FAA26CB2A * value)
	{
		____checkConflictsCategoryIds_readOnly_7 = value;
		Il2CppCodeGenWriteBarrier((&____checkConflictsCategoryIds_readOnly_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMAPCATEGORY_T3A6CAA2D6E69E45E734A026F26B816788D175602_H
#ifndef MAPPINGHELPER_T2F1FD9406CF911414A90FCB4852B174257FA2BF6_H
#define MAPPINGHELPER_T2F1FD9406CF911414A90FCB4852B174257FA2BF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_MappingHelper
struct  MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6  : public CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB
{
public:

public:
};

struct MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6_StaticFields
{
public:
	// Rewired.ReInput_MappingHelper Rewired.ReInput_MappingHelper::FBhGsngGbrZoBDJQYQqNPKqLfYtD
	MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0;

public:
	inline static int32_t get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return static_cast<int32_t>(offsetof(MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6_StaticFields, ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0)); }
	inline MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * get_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() const { return ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 ** get_address_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return &___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline void set_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * value)
	{
		___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0 = value;
		Il2CppCodeGenWriteBarrier((&___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGHELPER_T2F1FD9406CF911414A90FCB4852B174257FA2BF6_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef THREAD_TF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_H
#define THREAD_TF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Thread
struct  Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7  : public CriticalFinalizerObject_t8B006E1DEE084E781F5C0F3283E9226E28894DD9
{
public:
	// System.Threading.InternalThread System.Threading.Thread::internal_thread
	InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192 * ___internal_thread_6;
	// System.Object System.Threading.Thread::m_ThreadStartArg
	RuntimeObject * ___m_ThreadStartArg_7;
	// System.Object System.Threading.Thread::pending_exception
	RuntimeObject * ___pending_exception_8;
	// System.Security.Principal.IPrincipal System.Threading.Thread::principal
	RuntimeObject* ___principal_9;
	// System.Int32 System.Threading.Thread::principal_version
	int32_t ___principal_version_10;
	// System.MulticastDelegate System.Threading.Thread::m_Delegate
	MulticastDelegate_t * ___m_Delegate_12;
	// System.Threading.ExecutionContext System.Threading.Thread::m_ExecutionContext
	ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * ___m_ExecutionContext_13;
	// System.Boolean System.Threading.Thread::m_ExecutionContextBelongsToOuterScope
	bool ___m_ExecutionContextBelongsToOuterScope_14;

public:
	inline static int32_t get_offset_of_internal_thread_6() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___internal_thread_6)); }
	inline InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192 * get_internal_thread_6() const { return ___internal_thread_6; }
	inline InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192 ** get_address_of_internal_thread_6() { return &___internal_thread_6; }
	inline void set_internal_thread_6(InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192 * value)
	{
		___internal_thread_6 = value;
		Il2CppCodeGenWriteBarrier((&___internal_thread_6), value);
	}

	inline static int32_t get_offset_of_m_ThreadStartArg_7() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___m_ThreadStartArg_7)); }
	inline RuntimeObject * get_m_ThreadStartArg_7() const { return ___m_ThreadStartArg_7; }
	inline RuntimeObject ** get_address_of_m_ThreadStartArg_7() { return &___m_ThreadStartArg_7; }
	inline void set_m_ThreadStartArg_7(RuntimeObject * value)
	{
		___m_ThreadStartArg_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ThreadStartArg_7), value);
	}

	inline static int32_t get_offset_of_pending_exception_8() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___pending_exception_8)); }
	inline RuntimeObject * get_pending_exception_8() const { return ___pending_exception_8; }
	inline RuntimeObject ** get_address_of_pending_exception_8() { return &___pending_exception_8; }
	inline void set_pending_exception_8(RuntimeObject * value)
	{
		___pending_exception_8 = value;
		Il2CppCodeGenWriteBarrier((&___pending_exception_8), value);
	}

	inline static int32_t get_offset_of_principal_9() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___principal_9)); }
	inline RuntimeObject* get_principal_9() const { return ___principal_9; }
	inline RuntimeObject** get_address_of_principal_9() { return &___principal_9; }
	inline void set_principal_9(RuntimeObject* value)
	{
		___principal_9 = value;
		Il2CppCodeGenWriteBarrier((&___principal_9), value);
	}

	inline static int32_t get_offset_of_principal_version_10() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___principal_version_10)); }
	inline int32_t get_principal_version_10() const { return ___principal_version_10; }
	inline int32_t* get_address_of_principal_version_10() { return &___principal_version_10; }
	inline void set_principal_version_10(int32_t value)
	{
		___principal_version_10 = value;
	}

	inline static int32_t get_offset_of_m_Delegate_12() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___m_Delegate_12)); }
	inline MulticastDelegate_t * get_m_Delegate_12() const { return ___m_Delegate_12; }
	inline MulticastDelegate_t ** get_address_of_m_Delegate_12() { return &___m_Delegate_12; }
	inline void set_m_Delegate_12(MulticastDelegate_t * value)
	{
		___m_Delegate_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Delegate_12), value);
	}

	inline static int32_t get_offset_of_m_ExecutionContext_13() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___m_ExecutionContext_13)); }
	inline ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * get_m_ExecutionContext_13() const { return ___m_ExecutionContext_13; }
	inline ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 ** get_address_of_m_ExecutionContext_13() { return &___m_ExecutionContext_13; }
	inline void set_m_ExecutionContext_13(ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * value)
	{
		___m_ExecutionContext_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExecutionContext_13), value);
	}

	inline static int32_t get_offset_of_m_ExecutionContextBelongsToOuterScope_14() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___m_ExecutionContextBelongsToOuterScope_14)); }
	inline bool get_m_ExecutionContextBelongsToOuterScope_14() const { return ___m_ExecutionContextBelongsToOuterScope_14; }
	inline bool* get_address_of_m_ExecutionContextBelongsToOuterScope_14() { return &___m_ExecutionContextBelongsToOuterScope_14; }
	inline void set_m_ExecutionContextBelongsToOuterScope_14(bool value)
	{
		___m_ExecutionContextBelongsToOuterScope_14 = value;
	}
};

struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_StaticFields
{
public:
	// System.LocalDataStoreMgr System.Threading.Thread::s_LocalDataStoreMgr
	LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9 * ___s_LocalDataStoreMgr_0;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentCulture
	AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * ___s_asyncLocalCurrentCulture_4;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentUICulture
	AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * ___s_asyncLocalCurrentUICulture_5;

public:
	inline static int32_t get_offset_of_s_LocalDataStoreMgr_0() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_StaticFields, ___s_LocalDataStoreMgr_0)); }
	inline LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9 * get_s_LocalDataStoreMgr_0() const { return ___s_LocalDataStoreMgr_0; }
	inline LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9 ** get_address_of_s_LocalDataStoreMgr_0() { return &___s_LocalDataStoreMgr_0; }
	inline void set_s_LocalDataStoreMgr_0(LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9 * value)
	{
		___s_LocalDataStoreMgr_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalDataStoreMgr_0), value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentCulture_4() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_StaticFields, ___s_asyncLocalCurrentCulture_4)); }
	inline AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * get_s_asyncLocalCurrentCulture_4() const { return ___s_asyncLocalCurrentCulture_4; }
	inline AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A ** get_address_of_s_asyncLocalCurrentCulture_4() { return &___s_asyncLocalCurrentCulture_4; }
	inline void set_s_asyncLocalCurrentCulture_4(AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * value)
	{
		___s_asyncLocalCurrentCulture_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_asyncLocalCurrentCulture_4), value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentUICulture_5() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_StaticFields, ___s_asyncLocalCurrentUICulture_5)); }
	inline AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * get_s_asyncLocalCurrentUICulture_5() const { return ___s_asyncLocalCurrentUICulture_5; }
	inline AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A ** get_address_of_s_asyncLocalCurrentUICulture_5() { return &___s_asyncLocalCurrentUICulture_5; }
	inline void set_s_asyncLocalCurrentUICulture_5(AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * value)
	{
		___s_asyncLocalCurrentUICulture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_asyncLocalCurrentUICulture_5), value);
	}
};

struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields
{
public:
	// System.LocalDataStoreHolder System.Threading.Thread::s_LocalDataStore
	LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304 * ___s_LocalDataStore_1;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentCulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___m_CurrentCulture_2;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentUICulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___m_CurrentUICulture_3;
	// System.Threading.Thread System.Threading.Thread::current_thread
	Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ___current_thread_11;

public:
	inline static int32_t get_offset_of_s_LocalDataStore_1() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields, ___s_LocalDataStore_1)); }
	inline LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304 * get_s_LocalDataStore_1() const { return ___s_LocalDataStore_1; }
	inline LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304 ** get_address_of_s_LocalDataStore_1() { return &___s_LocalDataStore_1; }
	inline void set_s_LocalDataStore_1(LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304 * value)
	{
		___s_LocalDataStore_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalDataStore_1), value);
	}

	inline static int32_t get_offset_of_m_CurrentCulture_2() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields, ___m_CurrentCulture_2)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_m_CurrentCulture_2() const { return ___m_CurrentCulture_2; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_m_CurrentCulture_2() { return &___m_CurrentCulture_2; }
	inline void set_m_CurrentCulture_2(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___m_CurrentCulture_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentCulture_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentUICulture_3() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields, ___m_CurrentUICulture_3)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_m_CurrentUICulture_3() const { return ___m_CurrentUICulture_3; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_m_CurrentUICulture_3() { return &___m_CurrentUICulture_3; }
	inline void set_m_CurrentUICulture_3(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___m_CurrentUICulture_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentUICulture_3), value);
	}

	inline static int32_t get_offset_of_current_thread_11() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields, ___current_thread_11)); }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * get_current_thread_11() const { return ___current_thread_11; }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 ** get_address_of_current_thread_11() { return &___current_thread_11; }
	inline void set_current_thread_11(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * value)
	{
		___current_thread_11 = value;
		Il2CppCodeGenWriteBarrier((&___current_thread_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREAD_TF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_H
#ifndef UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#define UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef FLOATTWEEN_TF6BB24C266F36BD80E20C91AED453F7CE516919A_H
#define FLOATTWEEN_TF6BB24C266F36BD80E20C91AED453F7CE516919A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.FloatTween
struct  FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A 
{
public:
	// UnityEngine.UI.CoroutineTween.FloatTween_FloatTweenCallback UnityEngine.UI.CoroutineTween.FloatTween::m_Target
	FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * ___m_Target_0;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_StartValue
	float ___m_StartValue_1;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_TargetValue
	float ___m_TargetValue_2;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_Duration
	float ___m_Duration_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_Target_0)); }
	inline FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * get_m_Target_0() const { return ___m_Target_0; }
	inline FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_StartValue_1() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_StartValue_1)); }
	inline float get_m_StartValue_1() const { return ___m_StartValue_1; }
	inline float* get_address_of_m_StartValue_1() { return &___m_StartValue_1; }
	inline void set_m_StartValue_1(float value)
	{
		___m_StartValue_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetValue_2() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_TargetValue_2)); }
	inline float get_m_TargetValue_2() const { return ___m_TargetValue_2; }
	inline float* get_address_of_m_TargetValue_2() { return &___m_TargetValue_2; }
	inline void set_m_TargetValue_2(float value)
	{
		___m_TargetValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Duration_3() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_Duration_3)); }
	inline float get_m_Duration_3() const { return ___m_Duration_3; }
	inline float* get_address_of_m_Duration_3() { return &___m_Duration_3; }
	inline void set_m_Duration_3(float value)
	{
		___m_Duration_3 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_4() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_IgnoreTimeScale_4)); }
	inline bool get_m_IgnoreTimeScale_4() const { return ___m_IgnoreTimeScale_4; }
	inline bool* get_address_of_m_IgnoreTimeScale_4() { return &___m_IgnoreTimeScale_4; }
	inline void set_m_IgnoreTimeScale_4(bool value)
	{
		___m_IgnoreTimeScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A_marshaled_pinvoke
{
	FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A_marshaled_com
{
	FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
#endif // FLOATTWEEN_TF6BB24C266F36BD80E20C91AED453F7CE516919A_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef BOOLOPTION_T00F82CEAED2D32551FA96C1F943DC65E517DA4FB_H
#define BOOLOPTION_T00F82CEAED2D32551FA96C1F943DC65E517DA4FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.BoolOption
struct  BoolOption_t00F82CEAED2D32551FA96C1F943DC65E517DA4FB 
{
public:
	// System.Int32 Rewired.BoolOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BoolOption_t00F82CEAED2D32551FA96C1F943DC65E517DA4FB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLOPTION_T00F82CEAED2D32551FA96C1F943DC65E517DA4FB_H
#ifndef CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#define CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerType
struct  ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4 
{
public:
	// System.Int32 Rewired.ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifndef ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#define ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_paramName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef NOTSUPPORTEDEXCEPTION_TE75B318D6590A02A5D9B29FD97409B1750FA0010_H
#define NOTSUPPORTEDEXCEPTION_TE75B318D6590A02A5D9B29FD97409B1750FA0010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_TE75B318D6590A02A5D9B29FD97409B1750FA0010_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#define RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifndef COROUTINE_TAE7DB2FC70A0AE6477F896F852057CB0754F06EC_H
#define COROUTINE_TAE7DB2FC70A0AE6477F896F852057CB0754F06EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_TAE7DB2FC70A0AE6477F896F852057CB0754F06EC_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COLORTWEENMODE_TDCE018D37330F576ACCD00D16CAF91AE55315F2F_H
#define COLORTWEENMODE_TDCE018D37330F576ACCD00D16CAF91AE55315F2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenMode
struct  ColorTweenMode_tDCE018D37330F576ACCD00D16CAF91AE55315F2F 
{
public:
	// System.Int32 UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorTweenMode_tDCE018D37330F576ACCD00D16CAF91AE55315F2F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTWEENMODE_TDCE018D37330F576ACCD00D16CAF91AE55315F2F_H
#ifndef U3CSTARTU3EC__ITERATOR0_T468FE95258205EFB6F39EECD7F7D00F74B696286_H
#define U3CSTARTU3EC__ITERATOR0_T468FE95258205EFB6F39EECD7F7D00F74B696286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>
struct  U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286  : public RuntimeObject
{
public:
	// T UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::tweenInfo
	FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  ___tweenInfo_0;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_1;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::<percentage>__1
	float ___U3CpercentageU3E__1_2;
	// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::U24current
	RuntimeObject * ___U24current_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::U24disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::U24PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_tweenInfo_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286, ___tweenInfo_0)); }
	inline FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  get_tweenInfo_0() const { return ___tweenInfo_0; }
	inline FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * get_address_of_tweenInfo_0() { return &___tweenInfo_0; }
	inline void set_tweenInfo_0(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  value)
	{
		___tweenInfo_0 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286, ___U3CelapsedTimeU3E__0_1)); }
	inline float get_U3CelapsedTimeU3E__0_1() const { return ___U3CelapsedTimeU3E__0_1; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_1() { return &___U3CelapsedTimeU3E__0_1; }
	inline void set_U3CelapsedTimeU3E__0_1(float value)
	{
		___U3CelapsedTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CpercentageU3E__1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286, ___U3CpercentageU3E__1_2)); }
	inline float get_U3CpercentageU3E__1_2() const { return ___U3CpercentageU3E__1_2; }
	inline float* get_address_of_U3CpercentageU3E__1_2() { return &___U3CpercentageU3E__1_2; }
	inline void set_U3CpercentageU3E__1_2(float value)
	{
		___U3CpercentageU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T468FE95258205EFB6F39EECD7F7D00F74B696286_H
#ifndef ZEYNGEOBDVGNXIYWTZLSACBTWTY_T5A66EC4816C7441BE64C8F6403F6FA68DCD1F46F_H
#define ZEYNGEOBDVGNXIYWTZLSACBTWTY_T5A66EC4816C7441BE64C8F6403F6FA68DCD1F46F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2_zEynGeOBdVgNxiYwtZLsacBtwtY<System.Object,System.Boolean>
struct  zEynGeOBdVgNxiYwtZLsacBtwtY_t5A66EC4816C7441BE64C8F6403F6FA68DCD1F46F 
{
public:
	// System.Int32 hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2_zEynGeOBdVgNxiYwtZLsacBtwtY::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(zEynGeOBdVgNxiYwtZLsacBtwtY_t5A66EC4816C7441BE64C8F6403F6FA68DCD1F46F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZEYNGEOBDVGNXIYWTZLSACBTWTY_T5A66EC4816C7441BE64C8F6403F6FA68DCD1F46F_H
#ifndef ZEYNGEOBDVGNXIYWTZLSACBTWTY_T0CD84EAB886539AA0D196F1EAC09984E284E1EFE_H
#define ZEYNGEOBDVGNXIYWTZLSACBTWTY_T0CD84EAB886539AA0D196F1EAC09984E284E1EFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2_zEynGeOBdVgNxiYwtZLsacBtwtY<System.Object,System.Object>
struct  zEynGeOBdVgNxiYwtZLsacBtwtY_t0CD84EAB886539AA0D196F1EAC09984E284E1EFE 
{
public:
	// System.Int32 hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2_zEynGeOBdVgNxiYwtZLsacBtwtY::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(zEynGeOBdVgNxiYwtZLsacBtwtY_t0CD84EAB886539AA0D196F1EAC09984E284E1EFE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZEYNGEOBDVGNXIYWTZLSACBTWTY_T0CD84EAB886539AA0D196F1EAC09984E284E1EFE_H
#ifndef ZEYNGEOBDVGNXIYWTZLSACBTWTY_T0E3E9C17E36120A40F13BEA7BB929B13DA64E76C_H
#define ZEYNGEOBDVGNXIYWTZLSACBTWTY_T0E3E9C17E36120A40F13BEA7BB929B13DA64E76C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2_zEynGeOBdVgNxiYwtZLsacBtwtY<System.Object,UnityEngine.Vector2>
struct  zEynGeOBdVgNxiYwtZLsacBtwtY_t0E3E9C17E36120A40F13BEA7BB929B13DA64E76C 
{
public:
	// System.Int32 hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2_zEynGeOBdVgNxiYwtZLsacBtwtY::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(zEynGeOBdVgNxiYwtZLsacBtwtY_t0E3E9C17E36120A40F13BEA7BB929B13DA64E76C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZEYNGEOBDVGNXIYWTZLSACBTWTY_T0E3E9C17E36120A40F13BEA7BB929B13DA64E76C_H
#ifndef CONTROLLERMAP_T19BEF397BA067C157A2EA9878680F85EE15085CB_H
#define CONTROLLERMAP_T19BEF397BA067C157A2EA9878680F85EE15085CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMap
struct  ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB  : public RuntimeObject
{
public:
	// System.Int32 Rewired.ControllerMap::_id
	int32_t ____id_0;
	// System.Int32 Rewired.ControllerMap::_sourceMapId
	int32_t ____sourceMapId_1;
	// System.Int32 Rewired.ControllerMap::_categoryId
	int32_t ____categoryId_2;
	// System.Int32 Rewired.ControllerMap::_layoutId
	int32_t ____layoutId_3;
	// System.String Rewired.ControllerMap::_name
	String_t* ____name_4;
	// System.Guid Rewired.ControllerMap::_hardwareGuid
	Guid_t  ____hardwareGuid_5;
	// System.Boolean Rewired.ControllerMap::_enabled
	bool ____enabled_6;
	// System.Int32 Rewired.ControllerMap::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_7;
	// Rewired.Utils.Classes.Data.AList`1<Rewired.ActionElementMap> Rewired.ControllerMap::VWGXUewCkbjAzopbSknmvZALlIZ
	AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 * ___VWGXUewCkbjAzopbSknmvZALlIZ_8;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.ActionElementMap> Rewired.ControllerMap::TruAdbhnbvpEVsLpODTaGQmazwL
	ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * ___TruAdbhnbvpEVsLpODTaGQmazwL_9;
	// Rewired.Utils.Classes.Data.AList`1<Rewired.ActionElementMap> Rewired.ControllerMap::arINfjuBJNQHwNqdOQyXSRjsHrB
	AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 * ___arINfjuBJNQHwNqdOQyXSRjsHrB_10;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.ActionElementMap> Rewired.ControllerMap::eTKgFTcHaKMRVZDKtoHIpsbImmtw
	ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * ___eTKgFTcHaKMRVZDKtoHIpsbImmtw_11;
	// System.Int32 Rewired.ControllerMap::_playerId
	int32_t ____playerId_12;
	// System.Int32 Rewired.ControllerMap::_controllerId
	int32_t ____controllerId_13;
	// Rewired.ControllerType Rewired.ControllerMap::_controllerType
	int32_t ____controllerType_14;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__sourceMapId_1() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____sourceMapId_1)); }
	inline int32_t get__sourceMapId_1() const { return ____sourceMapId_1; }
	inline int32_t* get_address_of__sourceMapId_1() { return &____sourceMapId_1; }
	inline void set__sourceMapId_1(int32_t value)
	{
		____sourceMapId_1 = value;
	}

	inline static int32_t get_offset_of__categoryId_2() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____categoryId_2)); }
	inline int32_t get__categoryId_2() const { return ____categoryId_2; }
	inline int32_t* get_address_of__categoryId_2() { return &____categoryId_2; }
	inline void set__categoryId_2(int32_t value)
	{
		____categoryId_2 = value;
	}

	inline static int32_t get_offset_of__layoutId_3() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____layoutId_3)); }
	inline int32_t get__layoutId_3() const { return ____layoutId_3; }
	inline int32_t* get_address_of__layoutId_3() { return &____layoutId_3; }
	inline void set__layoutId_3(int32_t value)
	{
		____layoutId_3 = value;
	}

	inline static int32_t get_offset_of__name_4() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____name_4)); }
	inline String_t* get__name_4() const { return ____name_4; }
	inline String_t** get_address_of__name_4() { return &____name_4; }
	inline void set__name_4(String_t* value)
	{
		____name_4 = value;
		Il2CppCodeGenWriteBarrier((&____name_4), value);
	}

	inline static int32_t get_offset_of__hardwareGuid_5() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____hardwareGuid_5)); }
	inline Guid_t  get__hardwareGuid_5() const { return ____hardwareGuid_5; }
	inline Guid_t * get_address_of__hardwareGuid_5() { return &____hardwareGuid_5; }
	inline void set__hardwareGuid_5(Guid_t  value)
	{
		____hardwareGuid_5 = value;
	}

	inline static int32_t get_offset_of__enabled_6() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____enabled_6)); }
	inline bool get__enabled_6() const { return ____enabled_6; }
	inline bool* get_address_of__enabled_6() { return &____enabled_6; }
	inline void set__enabled_6(bool value)
	{
		____enabled_6 = value;
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_7() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_7)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_7() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_7; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_7() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_7; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_7(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_7 = value;
	}

	inline static int32_t get_offset_of_VWGXUewCkbjAzopbSknmvZALlIZ_8() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ___VWGXUewCkbjAzopbSknmvZALlIZ_8)); }
	inline AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 * get_VWGXUewCkbjAzopbSknmvZALlIZ_8() const { return ___VWGXUewCkbjAzopbSknmvZALlIZ_8; }
	inline AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 ** get_address_of_VWGXUewCkbjAzopbSknmvZALlIZ_8() { return &___VWGXUewCkbjAzopbSknmvZALlIZ_8; }
	inline void set_VWGXUewCkbjAzopbSknmvZALlIZ_8(AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 * value)
	{
		___VWGXUewCkbjAzopbSknmvZALlIZ_8 = value;
		Il2CppCodeGenWriteBarrier((&___VWGXUewCkbjAzopbSknmvZALlIZ_8), value);
	}

	inline static int32_t get_offset_of_TruAdbhnbvpEVsLpODTaGQmazwL_9() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ___TruAdbhnbvpEVsLpODTaGQmazwL_9)); }
	inline ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * get_TruAdbhnbvpEVsLpODTaGQmazwL_9() const { return ___TruAdbhnbvpEVsLpODTaGQmazwL_9; }
	inline ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 ** get_address_of_TruAdbhnbvpEVsLpODTaGQmazwL_9() { return &___TruAdbhnbvpEVsLpODTaGQmazwL_9; }
	inline void set_TruAdbhnbvpEVsLpODTaGQmazwL_9(ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * value)
	{
		___TruAdbhnbvpEVsLpODTaGQmazwL_9 = value;
		Il2CppCodeGenWriteBarrier((&___TruAdbhnbvpEVsLpODTaGQmazwL_9), value);
	}

	inline static int32_t get_offset_of_arINfjuBJNQHwNqdOQyXSRjsHrB_10() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ___arINfjuBJNQHwNqdOQyXSRjsHrB_10)); }
	inline AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 * get_arINfjuBJNQHwNqdOQyXSRjsHrB_10() const { return ___arINfjuBJNQHwNqdOQyXSRjsHrB_10; }
	inline AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 ** get_address_of_arINfjuBJNQHwNqdOQyXSRjsHrB_10() { return &___arINfjuBJNQHwNqdOQyXSRjsHrB_10; }
	inline void set_arINfjuBJNQHwNqdOQyXSRjsHrB_10(AList_1_t3CE454271698B03A6EA53DA068272454E1A22108 * value)
	{
		___arINfjuBJNQHwNqdOQyXSRjsHrB_10 = value;
		Il2CppCodeGenWriteBarrier((&___arINfjuBJNQHwNqdOQyXSRjsHrB_10), value);
	}

	inline static int32_t get_offset_of_eTKgFTcHaKMRVZDKtoHIpsbImmtw_11() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ___eTKgFTcHaKMRVZDKtoHIpsbImmtw_11)); }
	inline ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * get_eTKgFTcHaKMRVZDKtoHIpsbImmtw_11() const { return ___eTKgFTcHaKMRVZDKtoHIpsbImmtw_11; }
	inline ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 ** get_address_of_eTKgFTcHaKMRVZDKtoHIpsbImmtw_11() { return &___eTKgFTcHaKMRVZDKtoHIpsbImmtw_11; }
	inline void set_eTKgFTcHaKMRVZDKtoHIpsbImmtw_11(ReadOnlyCollection_1_t9214700BD46997461FA87658077EA4003B3D9A24 * value)
	{
		___eTKgFTcHaKMRVZDKtoHIpsbImmtw_11 = value;
		Il2CppCodeGenWriteBarrier((&___eTKgFTcHaKMRVZDKtoHIpsbImmtw_11), value);
	}

	inline static int32_t get_offset_of__playerId_12() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____playerId_12)); }
	inline int32_t get__playerId_12() const { return ____playerId_12; }
	inline int32_t* get_address_of__playerId_12() { return &____playerId_12; }
	inline void set__playerId_12(int32_t value)
	{
		____playerId_12 = value;
	}

	inline static int32_t get_offset_of__controllerId_13() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____controllerId_13)); }
	inline int32_t get__controllerId_13() const { return ____controllerId_13; }
	inline int32_t* get_address_of__controllerId_13() { return &____controllerId_13; }
	inline void set__controllerId_13(int32_t value)
	{
		____controllerId_13 = value;
	}

	inline static int32_t get_offset_of__controllerType_14() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB, ____controllerType_14)); }
	inline int32_t get__controllerType_14() const { return ____controllerType_14; }
	inline int32_t* get_address_of__controllerType_14() { return &____controllerType_14; }
	inline void set__controllerType_14(int32_t value)
	{
		____controllerType_14 = value;
	}
};

struct ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB_StaticFields
{
public:
	// System.Int32 Rewired.ControllerMap::FSpmeDwklQQlpZeqNSHMGkVYZQJ
	int32_t ___FSpmeDwklQQlpZeqNSHMGkVYZQJ_15;

public:
	inline static int32_t get_offset_of_FSpmeDwklQQlpZeqNSHMGkVYZQJ_15() { return static_cast<int32_t>(offsetof(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB_StaticFields, ___FSpmeDwklQQlpZeqNSHMGkVYZQJ_15)); }
	inline int32_t get_FSpmeDwklQQlpZeqNSHMGkVYZQJ_15() const { return ___FSpmeDwklQQlpZeqNSHMGkVYZQJ_15; }
	inline int32_t* get_address_of_FSpmeDwklQQlpZeqNSHMGkVYZQJ_15() { return &___FSpmeDwklQQlpZeqNSHMGkVYZQJ_15; }
	inline void set_FSpmeDwklQQlpZeqNSHMGkVYZQJ_15(int32_t value)
	{
		___FSpmeDwklQQlpZeqNSHMGkVYZQJ_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAP_T19BEF397BA067C157A2EA9878680F85EE15085CB_H
#ifndef ARGUMENTNULLEXCEPTION_T581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_H
#define ARGUMENTNULLEXCEPTION_T581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef COLORTWEEN_T4CBBF5875FA391053DB62E98D8D9603040413228_H
#define COLORTWEEN_T4CBBF5875FA391053DB62E98D8D9603040413228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.ColorTween
struct  ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 
{
public:
	// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenCallback UnityEngine.UI.CoroutineTween.ColorTween::m_Target
	ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * ___m_Target_0;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_StartColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_TargetColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::m_TweenMode
	int32_t ___m_TweenMode_3;
	// System.Single UnityEngine.UI.CoroutineTween.ColorTween::m_Duration
	float ___m_Duration_4;
	// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_5;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_Target_0)); }
	inline ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * get_m_Target_0() const { return ___m_Target_0; }
	inline ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_StartColor_1() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_StartColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_StartColor_1() const { return ___m_StartColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_StartColor_1() { return &___m_StartColor_1; }
	inline void set_m_StartColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_StartColor_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetColor_2() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_TargetColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_TargetColor_2() const { return ___m_TargetColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_TargetColor_2() { return &___m_TargetColor_2; }
	inline void set_m_TargetColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_TargetColor_2 = value;
	}

	inline static int32_t get_offset_of_m_TweenMode_3() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_TweenMode_3)); }
	inline int32_t get_m_TweenMode_3() const { return ___m_TweenMode_3; }
	inline int32_t* get_address_of_m_TweenMode_3() { return &___m_TweenMode_3; }
	inline void set_m_TweenMode_3(int32_t value)
	{
		___m_TweenMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Duration_4() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_Duration_4)); }
	inline float get_m_Duration_4() const { return ___m_Duration_4; }
	inline float* get_address_of_m_Duration_4() { return &___m_Duration_4; }
	inline void set_m_Duration_4(float value)
	{
		___m_Duration_4 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_5() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_IgnoreTimeScale_5)); }
	inline bool get_m_IgnoreTimeScale_5() const { return ___m_IgnoreTimeScale_5; }
	inline bool* get_address_of_m_IgnoreTimeScale_5() { return &___m_IgnoreTimeScale_5; }
	inline void set_m_IgnoreTimeScale_5(bool value)
	{
		___m_IgnoreTimeScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228_marshaled_pinvoke
{
	ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * ___m_Target_0;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228_marshaled_com
{
	ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * ___m_Target_0;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
#endif // COLORTWEEN_T4CBBF5875FA391053DB62E98D8D9603040413228_H
#ifndef HIERARCHYEVENTHELPER_2_T83507A10FBC53F7024CE976D2CCBF4918C8D50EB_H
#define HIERARCHYEVENTHELPER_2_T83507A10FBC53F7024CE976D2CCBF4918C8D50EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,System.Boolean>
struct  HierarchyEventHelper_2_t83507A10FBC53F7024CE976D2CCBF4918C8D50EB  : public RuntimeObject
{
public:
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<THandler,TValue> hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2::kRJcljaQFQASapamFTwqLIpinzNN
	EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * ___kRJcljaQFQASapamFTwqLIpinzNN_0;
	// System.Collections.Generic.List`1<THandler> hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2::KQSHiaoVzKDlVjvdaHbJcDxdjAy
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2_zEynGeOBdVgNxiYwtZLsacBtwtY<THandler,TValue> hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2::WwsFTLbfEnWIyENevELMTAgByIi
	int32_t ___WwsFTLbfEnWIyENevELMTAgByIi_2;

public:
	inline static int32_t get_offset_of_kRJcljaQFQASapamFTwqLIpinzNN_0() { return static_cast<int32_t>(offsetof(HierarchyEventHelper_2_t83507A10FBC53F7024CE976D2CCBF4918C8D50EB, ___kRJcljaQFQASapamFTwqLIpinzNN_0)); }
	inline EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * get_kRJcljaQFQASapamFTwqLIpinzNN_0() const { return ___kRJcljaQFQASapamFTwqLIpinzNN_0; }
	inline EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 ** get_address_of_kRJcljaQFQASapamFTwqLIpinzNN_0() { return &___kRJcljaQFQASapamFTwqLIpinzNN_0; }
	inline void set_kRJcljaQFQASapamFTwqLIpinzNN_0(EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * value)
	{
		___kRJcljaQFQASapamFTwqLIpinzNN_0 = value;
		Il2CppCodeGenWriteBarrier((&___kRJcljaQFQASapamFTwqLIpinzNN_0), value);
	}

	inline static int32_t get_offset_of_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1() { return static_cast<int32_t>(offsetof(HierarchyEventHelper_2_t83507A10FBC53F7024CE976D2CCBF4918C8D50EB, ___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1() const { return ___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1() { return &___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1; }
	inline void set_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1 = value;
		Il2CppCodeGenWriteBarrier((&___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1), value);
	}

	inline static int32_t get_offset_of_WwsFTLbfEnWIyENevELMTAgByIi_2() { return static_cast<int32_t>(offsetof(HierarchyEventHelper_2_t83507A10FBC53F7024CE976D2CCBF4918C8D50EB, ___WwsFTLbfEnWIyENevELMTAgByIi_2)); }
	inline int32_t get_WwsFTLbfEnWIyENevELMTAgByIi_2() const { return ___WwsFTLbfEnWIyENevELMTAgByIi_2; }
	inline int32_t* get_address_of_WwsFTLbfEnWIyENevELMTAgByIi_2() { return &___WwsFTLbfEnWIyENevELMTAgByIi_2; }
	inline void set_WwsFTLbfEnWIyENevELMTAgByIi_2(int32_t value)
	{
		___WwsFTLbfEnWIyENevELMTAgByIi_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIERARCHYEVENTHELPER_2_T83507A10FBC53F7024CE976D2CCBF4918C8D50EB_H
#ifndef HIERARCHYEVENTHELPER_2_T21ADBD5576AA92276647AD1353AA78AC39209E80_H
#define HIERARCHYEVENTHELPER_2_T21ADBD5576AA92276647AD1353AA78AC39209E80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,System.Object>
struct  HierarchyEventHelper_2_t21ADBD5576AA92276647AD1353AA78AC39209E80  : public RuntimeObject
{
public:
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<THandler,TValue> hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2::kRJcljaQFQASapamFTwqLIpinzNN
	EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * ___kRJcljaQFQASapamFTwqLIpinzNN_0;
	// System.Collections.Generic.List`1<THandler> hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2::KQSHiaoVzKDlVjvdaHbJcDxdjAy
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2_zEynGeOBdVgNxiYwtZLsacBtwtY<THandler,TValue> hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2::WwsFTLbfEnWIyENevELMTAgByIi
	int32_t ___WwsFTLbfEnWIyENevELMTAgByIi_2;

public:
	inline static int32_t get_offset_of_kRJcljaQFQASapamFTwqLIpinzNN_0() { return static_cast<int32_t>(offsetof(HierarchyEventHelper_2_t21ADBD5576AA92276647AD1353AA78AC39209E80, ___kRJcljaQFQASapamFTwqLIpinzNN_0)); }
	inline EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * get_kRJcljaQFQASapamFTwqLIpinzNN_0() const { return ___kRJcljaQFQASapamFTwqLIpinzNN_0; }
	inline EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 ** get_address_of_kRJcljaQFQASapamFTwqLIpinzNN_0() { return &___kRJcljaQFQASapamFTwqLIpinzNN_0; }
	inline void set_kRJcljaQFQASapamFTwqLIpinzNN_0(EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * value)
	{
		___kRJcljaQFQASapamFTwqLIpinzNN_0 = value;
		Il2CppCodeGenWriteBarrier((&___kRJcljaQFQASapamFTwqLIpinzNN_0), value);
	}

	inline static int32_t get_offset_of_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1() { return static_cast<int32_t>(offsetof(HierarchyEventHelper_2_t21ADBD5576AA92276647AD1353AA78AC39209E80, ___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1() const { return ___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1() { return &___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1; }
	inline void set_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1 = value;
		Il2CppCodeGenWriteBarrier((&___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1), value);
	}

	inline static int32_t get_offset_of_WwsFTLbfEnWIyENevELMTAgByIi_2() { return static_cast<int32_t>(offsetof(HierarchyEventHelper_2_t21ADBD5576AA92276647AD1353AA78AC39209E80, ___WwsFTLbfEnWIyENevELMTAgByIi_2)); }
	inline int32_t get_WwsFTLbfEnWIyENevELMTAgByIi_2() const { return ___WwsFTLbfEnWIyENevELMTAgByIi_2; }
	inline int32_t* get_address_of_WwsFTLbfEnWIyENevELMTAgByIi_2() { return &___WwsFTLbfEnWIyENevELMTAgByIi_2; }
	inline void set_WwsFTLbfEnWIyENevELMTAgByIi_2(int32_t value)
	{
		___WwsFTLbfEnWIyENevELMTAgByIi_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIERARCHYEVENTHELPER_2_T21ADBD5576AA92276647AD1353AA78AC39209E80_H
#ifndef HIERARCHYEVENTHELPER_2_T87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5_H
#define HIERARCHYEVENTHELPER_2_T87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,UnityEngine.Vector2>
struct  HierarchyEventHelper_2_t87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5  : public RuntimeObject
{
public:
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<THandler,TValue> hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2::kRJcljaQFQASapamFTwqLIpinzNN
	EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * ___kRJcljaQFQASapamFTwqLIpinzNN_0;
	// System.Collections.Generic.List`1<THandler> hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2::KQSHiaoVzKDlVjvdaHbJcDxdjAy
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2_zEynGeOBdVgNxiYwtZLsacBtwtY<THandler,TValue> hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2::WwsFTLbfEnWIyENevELMTAgByIi
	int32_t ___WwsFTLbfEnWIyENevELMTAgByIi_2;

public:
	inline static int32_t get_offset_of_kRJcljaQFQASapamFTwqLIpinzNN_0() { return static_cast<int32_t>(offsetof(HierarchyEventHelper_2_t87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5, ___kRJcljaQFQASapamFTwqLIpinzNN_0)); }
	inline EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * get_kRJcljaQFQASapamFTwqLIpinzNN_0() const { return ___kRJcljaQFQASapamFTwqLIpinzNN_0; }
	inline EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 ** get_address_of_kRJcljaQFQASapamFTwqLIpinzNN_0() { return &___kRJcljaQFQASapamFTwqLIpinzNN_0; }
	inline void set_kRJcljaQFQASapamFTwqLIpinzNN_0(EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * value)
	{
		___kRJcljaQFQASapamFTwqLIpinzNN_0 = value;
		Il2CppCodeGenWriteBarrier((&___kRJcljaQFQASapamFTwqLIpinzNN_0), value);
	}

	inline static int32_t get_offset_of_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1() { return static_cast<int32_t>(offsetof(HierarchyEventHelper_2_t87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5, ___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1() const { return ___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1() { return &___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1; }
	inline void set_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1 = value;
		Il2CppCodeGenWriteBarrier((&___KQSHiaoVzKDlVjvdaHbJcDxdjAy_1), value);
	}

	inline static int32_t get_offset_of_WwsFTLbfEnWIyENevELMTAgByIi_2() { return static_cast<int32_t>(offsetof(HierarchyEventHelper_2_t87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5, ___WwsFTLbfEnWIyENevELMTAgByIi_2)); }
	inline int32_t get_WwsFTLbfEnWIyENevELMTAgByIi_2() const { return ___WwsFTLbfEnWIyENevELMTAgByIi_2; }
	inline int32_t* get_address_of_WwsFTLbfEnWIyENevELMTAgByIi_2() { return &___WwsFTLbfEnWIyENevELMTAgByIi_2; }
	inline void set_WwsFTLbfEnWIyENevELMTAgByIi_2(int32_t value)
	{
		___WwsFTLbfEnWIyENevELMTAgByIi_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIERARCHYEVENTHELPER_2_T87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5_H
#ifndef LVPGLCBKNVRVSNFFCFGWEYEBHALY_T3E25F5263D0F87BC11AF4918E81A3244E39E5642_H
#define LVPGLCBKNVRVSNFFCFGWEYEBHALY_T3E25F5263D0F87BC11AF4918E81A3244E39E5642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>
struct  lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Rewired.ControllerMap> lVPGlCbkNvrVSnFFcfgwEyebHaly::BdaanHoIMYiyKBPrxEPAAvUoiXiO
	List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * ___BdaanHoIMYiyKBPrxEPAAvUoiXiO_0;
	// System.Collections.Generic.IList`1<Rewired.ControllerMap> lVPGlCbkNvrVSnFFcfgwEyebHaly::dSzXwUUoLuinZYUADiNfnihdPrl
	RuntimeObject* ___dSzXwUUoLuinZYUADiNfnihdPrl_1;
	// Rewired.ControllerType lVPGlCbkNvrVSnFFcfgwEyebHaly::jeLkzDDlxjdyTfidRDslEjdPyAn
	int32_t ___jeLkzDDlxjdyTfidRDslEjdPyAn_2;
	// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly::xJPYTeJZICkFpIWTwbMsXQVEALx
	int32_t ___xJPYTeJZICkFpIWTwbMsXQVEALx_3;
	// System.Collections.Generic.List`1<T> lVPGlCbkNvrVSnFFcfgwEyebHaly::HXrZyhBzDfszTlMEUEDAshPIFVI
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___HXrZyhBzDfszTlMEUEDAshPIFVI_4;
	// System.Collections.Generic.IList`1<T> lVPGlCbkNvrVSnFFcfgwEyebHaly::RQnbcoHDSILswMsVwUCuGnbyXvLt
	RuntimeObject* ___RQnbcoHDSILswMsVwUCuGnbyXvLt_5;

public:
	inline static int32_t get_offset_of_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0() { return static_cast<int32_t>(offsetof(lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642, ___BdaanHoIMYiyKBPrxEPAAvUoiXiO_0)); }
	inline List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0() const { return ___BdaanHoIMYiyKBPrxEPAAvUoiXiO_0; }
	inline List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E ** get_address_of_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0() { return &___BdaanHoIMYiyKBPrxEPAAvUoiXiO_0; }
	inline void set_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0(List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * value)
	{
		___BdaanHoIMYiyKBPrxEPAAvUoiXiO_0 = value;
		Il2CppCodeGenWriteBarrier((&___BdaanHoIMYiyKBPrxEPAAvUoiXiO_0), value);
	}

	inline static int32_t get_offset_of_dSzXwUUoLuinZYUADiNfnihdPrl_1() { return static_cast<int32_t>(offsetof(lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642, ___dSzXwUUoLuinZYUADiNfnihdPrl_1)); }
	inline RuntimeObject* get_dSzXwUUoLuinZYUADiNfnihdPrl_1() const { return ___dSzXwUUoLuinZYUADiNfnihdPrl_1; }
	inline RuntimeObject** get_address_of_dSzXwUUoLuinZYUADiNfnihdPrl_1() { return &___dSzXwUUoLuinZYUADiNfnihdPrl_1; }
	inline void set_dSzXwUUoLuinZYUADiNfnihdPrl_1(RuntimeObject* value)
	{
		___dSzXwUUoLuinZYUADiNfnihdPrl_1 = value;
		Il2CppCodeGenWriteBarrier((&___dSzXwUUoLuinZYUADiNfnihdPrl_1), value);
	}

	inline static int32_t get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_2() { return static_cast<int32_t>(offsetof(lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642, ___jeLkzDDlxjdyTfidRDslEjdPyAn_2)); }
	inline int32_t get_jeLkzDDlxjdyTfidRDslEjdPyAn_2() const { return ___jeLkzDDlxjdyTfidRDslEjdPyAn_2; }
	inline int32_t* get_address_of_jeLkzDDlxjdyTfidRDslEjdPyAn_2() { return &___jeLkzDDlxjdyTfidRDslEjdPyAn_2; }
	inline void set_jeLkzDDlxjdyTfidRDslEjdPyAn_2(int32_t value)
	{
		___jeLkzDDlxjdyTfidRDslEjdPyAn_2 = value;
	}

	inline static int32_t get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_3() { return static_cast<int32_t>(offsetof(lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642, ___xJPYTeJZICkFpIWTwbMsXQVEALx_3)); }
	inline int32_t get_xJPYTeJZICkFpIWTwbMsXQVEALx_3() const { return ___xJPYTeJZICkFpIWTwbMsXQVEALx_3; }
	inline int32_t* get_address_of_xJPYTeJZICkFpIWTwbMsXQVEALx_3() { return &___xJPYTeJZICkFpIWTwbMsXQVEALx_3; }
	inline void set_xJPYTeJZICkFpIWTwbMsXQVEALx_3(int32_t value)
	{
		___xJPYTeJZICkFpIWTwbMsXQVEALx_3 = value;
	}

	inline static int32_t get_offset_of_HXrZyhBzDfszTlMEUEDAshPIFVI_4() { return static_cast<int32_t>(offsetof(lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642, ___HXrZyhBzDfszTlMEUEDAshPIFVI_4)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_HXrZyhBzDfszTlMEUEDAshPIFVI_4() const { return ___HXrZyhBzDfszTlMEUEDAshPIFVI_4; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_HXrZyhBzDfszTlMEUEDAshPIFVI_4() { return &___HXrZyhBzDfszTlMEUEDAshPIFVI_4; }
	inline void set_HXrZyhBzDfszTlMEUEDAshPIFVI_4(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___HXrZyhBzDfszTlMEUEDAshPIFVI_4 = value;
		Il2CppCodeGenWriteBarrier((&___HXrZyhBzDfszTlMEUEDAshPIFVI_4), value);
	}

	inline static int32_t get_offset_of_RQnbcoHDSILswMsVwUCuGnbyXvLt_5() { return static_cast<int32_t>(offsetof(lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642, ___RQnbcoHDSILswMsVwUCuGnbyXvLt_5)); }
	inline RuntimeObject* get_RQnbcoHDSILswMsVwUCuGnbyXvLt_5() const { return ___RQnbcoHDSILswMsVwUCuGnbyXvLt_5; }
	inline RuntimeObject** get_address_of_RQnbcoHDSILswMsVwUCuGnbyXvLt_5() { return &___RQnbcoHDSILswMsVwUCuGnbyXvLt_5; }
	inline void set_RQnbcoHDSILswMsVwUCuGnbyXvLt_5(RuntimeObject* value)
	{
		___RQnbcoHDSILswMsVwUCuGnbyXvLt_5 = value;
		Il2CppCodeGenWriteBarrier((&___RQnbcoHDSILswMsVwUCuGnbyXvLt_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LVPGLCBKNVRVSNFFCFGWEYEBHALY_T3E25F5263D0F87BC11AF4918E81A3244E39E5642_H
#ifndef ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#define ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef UNITYACTION_1_TA99D005A9C291926F1FC4F9D3A8FABD18D895689_H
#define UNITYACTION_1_TA99D005A9C291926F1FC4F9D3A8FABD18D895689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Int32>>
struct  UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_TA99D005A9C291926F1FC4F9D3A8FABD18D895689_H
#ifndef UNITYACTION_1_T8AD8F8E44992547CC00157160617BE8482809363_H
#define UNITYACTION_1_T8AD8F8E44992547CC00157160617BE8482809363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>
struct  UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T8AD8F8E44992547CC00157160617BE8482809363_H
#ifndef UNITYACTION_1_T7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4_H
#define UNITYACTION_1_T7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct  UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4_H
#ifndef UNITYACTION_1_T7B2376CCD306AEB0D24B3479F62CE812058041D0_H
#define UNITYACTION_1_T7B2376CCD306AEB0D24B3479F62CE812058041D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct  UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T7B2376CCD306AEB0D24B3479F62CE812058041D0_H
#ifndef UNITYACTION_1_T5070210D9B8F86C2EDBB6772A8295FAD8FC32821_H
#define UNITYACTION_1_T5070210D9B8F86C2EDBB6772A8295FAD8FC32821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct  UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T5070210D9B8F86C2EDBB6772A8295FAD8FC32821_H
#ifndef UNITYACTION_1_T68BCED570CE215DF78AAA225E29C0959286C1A0E_H
#define UNITYACTION_1_T68BCED570CE215DF78AAA225E29C0959286C1A0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct  UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T68BCED570CE215DF78AAA225E29C0959286C1A0E_H
#ifndef UNITYACTION_1_T3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB_H
#define UNITYACTION_1_T3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct  UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB_H
#ifndef UNITYACTION_1_T330F97880F37E23D6D0C6618DD77F28AC9EF8FA9_H
#define UNITYACTION_1_T330F97880F37E23D6D0C6618DD77F28AC9EF8FA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Object>
struct  UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T330F97880F37E23D6D0C6618DD77F28AC9EF8FA9_H
#ifndef TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#define TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifndef U3CSTARTU3EC__ITERATOR0_TE6C906B4CE3463E1E9016DA76194239D06531E07_H
#define U3CSTARTU3EC__ITERATOR0_TE6C906B4CE3463E1E9016DA76194239D06531E07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct  U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07  : public RuntimeObject
{
public:
	// T UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::tweenInfo
	ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  ___tweenInfo_0;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_1;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::<percentage>__1
	float ___U3CpercentageU3E__1_2;
	// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::U24current
	RuntimeObject * ___U24current_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::U24disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::U24PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_tweenInfo_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07, ___tweenInfo_0)); }
	inline ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  get_tweenInfo_0() const { return ___tweenInfo_0; }
	inline ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * get_address_of_tweenInfo_0() { return &___tweenInfo_0; }
	inline void set_tweenInfo_0(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  value)
	{
		___tweenInfo_0 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07, ___U3CelapsedTimeU3E__0_1)); }
	inline float get_U3CelapsedTimeU3E__0_1() const { return ___U3CelapsedTimeU3E__0_1; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_1() { return &___U3CelapsedTimeU3E__0_1; }
	inline void set_U3CelapsedTimeU3E__0_1(float value)
	{
		___U3CelapsedTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CpercentageU3E__1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07, ___U3CpercentageU3E__1_2)); }
	inline float get_U3CpercentageU3E__1_2() const { return ___U3CpercentageU3E__1_2; }
	inline float* get_address_of_U3CpercentageU3E__1_2() { return &___U3CpercentageU3E__1_2; }
	inline void set_U3CpercentageU3E__1_2(float value)
	{
		___U3CpercentageU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_TE6C906B4CE3463E1E9016DA76194239D06531E07_H
#ifndef EVENTFUNCTION_2_T32F758F489DB646CAC012EE77FC7CCB77F2B6673_H
#define EVENTFUNCTION_2_T32F758F489DB646CAC012EE77FC7CCB77F2B6673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,System.Boolean>
struct  EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_2_T32F758F489DB646CAC012EE77FC7CCB77F2B6673_H
#ifndef EVENTFUNCTION_2_T9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80_H
#define EVENTFUNCTION_2_T9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,System.Object>
struct  EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_2_T9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80_H
#ifndef EVENTFUNCTION_2_T3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1_H
#define EVENTFUNCTION_2_T3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,UnityEngine.Vector2>
struct  EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_2_T3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Rewired.ControllerMap[]
struct ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * m_Items[1];

public:
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* List_1_ToArray_m801D4DEF3587F60F463F04EEABE5CBE711FE5612_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_set_Item_m451452782977192583A6374A799099FCCD9BD83E_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::.ctor(System.Collections.Generic.IList`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void ReadOnlyCollection_1__ctor_m8F7880F43C4E281DBF7CA5A9431D5E6DD3797B7E_gshared (ReadOnlyCollection_1_t5D996E967221C71E4EC5CC11210C3076432D5A50 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void List_1_RemoveAt_m1EC5117AD814B97460F8F95D49A428032FE37CBF_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C" IL2CPP_METHOD_ATTR bool List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::ValidTarget()
extern "C" IL2CPP_METHOD_ATTR bool ColorTween_ValidTarget_m847E9D6C8B97F1C9039BF80AD69EEFC74C989079 (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::get_ignoreTimeScale()
extern "C" IL2CPP_METHOD_ATTR bool ColorTween_get_ignoreTimeScale_mD27F5C7D70D340DBDFAE972BBE3857A26E29747A (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2 (const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::get_duration()
extern "C" IL2CPP_METHOD_ATTR float ColorTween_get_duration_mE4A9B4FFAB11CCF25EAACF5777991AB6749020B0 (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B (float p0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::TweenValue(System.Single)
extern "C" IL2CPP_METHOD_ATTR void ColorTween_TweenValue_mF6B10FEA49EB758AD37D95A7DD577D6AA9C32110 (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, float ___floatPercentage0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33 (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::ValidTarget()
extern "C" IL2CPP_METHOD_ATTR bool FloatTween_ValidTarget_m921F88A58CCB09A4D55DBB714F3538677363FAE6 (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::get_ignoreTimeScale()
extern "C" IL2CPP_METHOD_ATTR bool FloatTween_get_ignoreTimeScale_mCA3DA664CF6F78735BF3ED6301900FB849B49C34 (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.UI.CoroutineTween.FloatTween::get_duration()
extern "C" IL2CPP_METHOD_ATTR float FloatTween_get_duration_mBC42C5053BCB1A1315430E3E21ECE1597BB0B314 (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::TweenValue(System.Single)
extern "C" IL2CPP_METHOD_ATTR void FloatTween_TweenValue_m4ADF9CF3356268D7AD1CFF358BA252F1E52226F3 (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, float ___floatPercentage0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568 (RuntimeObject * p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C" IL2CPP_METHOD_ATTR bool GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour_StopCoroutine_m3CDD6C046CC660D4CD6583FCE97F88A9735FD5FA (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * __this, String_t* p0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_root()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Transform_get_root_m101A8B5C2CC6D868B6B66EEDBD5336FC1EB5DDD6 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Threading.Thread System.Threading.Thread::get_CurrentThread()
extern "C" IL2CPP_METHOD_ATTR Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * Thread_get_CurrentThread_mB7A83CAE2B9A74CEA053196DFD1AF1E7AB30A70E (const RuntimeMethod* method);
// System.Int32 System.Threading.Thread::get_ManagedThreadId()
extern "C" IL2CPP_METHOD_ATTR int32_t Thread_get_ManagedThreadId_m7FA85162CB00713B94EF5708B19120F791D3AAD1 (Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<Rewired.ControllerMap>::get_Item(System.Int32)
inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * List_1_get_Item_mADF483DC22F60B179F834797734DFFBAE6123F5A (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * (*) (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<Rewired.ControllerMap>::get_Count()
inline int32_t List_1_get_Count_mB87AFEFB187F15D3B2B41BA3C4919E74A8256840 (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared)(__this, method);
}
// System.Int32 Rewired.ControllerMap::get_categoryId()
extern "C" IL2CPP_METHOD_ATTR int32_t ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * __this, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<Rewired.ControllerMap>::ToArray()
inline ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E* List_1_ToArray_m57FD5A381EAD73CDC168FFEC93C5D22EEA7CCC7C (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * __this, const RuntimeMethod* method)
{
	return ((  ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E* (*) (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *, const RuntimeMethod*))List_1_ToArray_m801D4DEF3587F60F463F04EEABE5CBE711FE5612_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Rewired.ControllerMap>::Clear()
inline void List_1_Clear_m88D5862BAF30335E7FEA486544F28FA5CECFF6DE (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *, const RuntimeMethod*))List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Rewired.ControllerMap>::Add(!0)
inline void List_1_Add_mBB024C6A0C23217B87FABE55861D2532378BEE1B (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * __this, ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *, ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<Rewired.ControllerMap>::.ctor()
inline void List_1__ctor_m0839F8110366CBDBC753D441E962E51A20E93E14 (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Rewired.ControllerMap>::set_Item(System.Int32,!0)
inline void List_1_set_Item_m86D714200344F5D83977C34422287C90AC3DFD80 (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * __this, int32_t p0, ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * p1, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *, int32_t, ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *, const RuntimeMethod*))List_1_set_Item_m451452782977192583A6374A799099FCCD9BD83E_gshared)(__this, p0, p1, method);
}
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C" IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6 (RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  p0, const RuntimeMethod* method);
// Rewired.ControllerType SHhhwmrrEBdklqDsPHHCPGltOQH::hUOXtxZFfwOmheZLijxYDcjuVpA(System.Type)
extern "C" IL2CPP_METHOD_ATTR int32_t SHhhwmrrEBdklqDsPHHCPGltOQH_hUOXtxZFfwOmheZLijxYDcjuVpA_mB9A1DAD8C42C443BDCB687799A0AC29CFA9AF85D (Type_t * p0, const RuntimeMethod* method);
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.ControllerMap>::.ctor(System.Collections.Generic.IList`1<!0>)
inline void ReadOnlyCollection_1__ctor_m43F319804F61EAF5B1BA3BEE99D5EC8F37B473DA (ReadOnlyCollection_1_t42C6EC4956F81037B0696DDF4310EEAD899406F9 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyCollection_1_t42C6EC4956F81037B0696DDF4310EEAD899406F9 *, RuntimeObject*, const RuntimeMethod*))ReadOnlyCollection_1__ctor_m8F7880F43C4E281DBF7CA5A9431D5E6DD3797B7E_gshared)(__this, p0, method);
}
// System.Int32 Rewired.ControllerMap::get_layoutId()
extern "C" IL2CPP_METHOD_ATTR int32_t ControllerMap_get_layoutId_m1B90CB02DDE635CA339BA98082FD4F9480DF0EF7 (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * __this, const RuntimeMethod* method);
// System.Void Rewired.ControllerMap::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ControllerMap_set_enabled_m4A347FEEC7D6FF51515C88121C4F2A0437B472A8 (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean Rewired.ControllerMap::get_enabled()
extern "C" IL2CPP_METHOD_ATTR bool ControllerMap_get_enabled_m454281A8B80828B120C4F7EDF2BC52028AD35C6F (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Rewired.ControllerMap>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5 (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m1EC5117AD814B97460F8F95D49A428032FE37CBF_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1<Rewired.ControllerMap>::Remove(!0)
inline bool List_1_Remove_m89A219C1A47D8F7CEDA4FAD3FD848DBCC009D28C (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * __this, ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *, ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *, const RuntimeMethod*))List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared)(__this, p0, method);
}
// System.Int32 Rewired.ControllerMap::get_id()
extern "C" IL2CPP_METHOD_ATTR int32_t ControllerMap_get_id_m6FA8319C62C718FE081C3D490348B1235840E336 (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * __this, const RuntimeMethod* method);
// Rewired.ReInput/MappingHelper Rewired.ReInput::get_mapping()
extern "C" IL2CPP_METHOD_ATTR MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * ReInput_get_mapping_m57CBC64F23235AA736D4C057ED17A85421F00D5C (const RuntimeMethod* method);
// Rewired.InputMapCategory Rewired.ReInput/MappingHelper::GetMapCategory(System.Int32)
extern "C" IL2CPP_METHOD_ATTR InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * MappingHelper_GetMapCategory_mBC62128856B61C97B7AD00A9739EABA209EEFDD0 (MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * __this, int32_t ___mapCategoryId0, const RuntimeMethod* method);
// System.Boolean Rewired.InputCategory::get_userAssignable()
extern "C" IL2CPP_METHOD_ATTR bool InputCategory_get_userAssignable_m6F4B367A66B6F94277FB20F851F2280FB6059B64 (InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CStartU3Ec__Iterator0__ctor_m7C7663DF2836474A845DD182E23E6A3DD3D6BCEC_gshared (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CStartU3Ec__Iterator0_MoveNext_m17A6A3B6131EFE960C4DA7784DDE816250347E99_gshared (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m17A6A3B6131EFE960C4DA7784DDE816250347E99_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 14956);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 14957);
	float G_B7_0 = 0.0f;
	U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * G_B8_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00d5;
			}
		}
	}
	{
		goto IL_010f;
	}

IL_0021:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14958);
		CHECK_SEQ_POINT(methodExecutionContext, 14959);
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_2 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14960);
		STORE_SEQ_POINT(methodExecutionContext, 14981);
		bool L_3 = ColorTween_ValidTarget_m847E9D6C8B97F1C9039BF80AD69EEFC74C989079((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_2, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14981);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14961);
		goto IL_010f;
	}

IL_003d:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14962);
		__this->set_U3CelapsedTimeU3E__0_1((0.0f));
		CHECK_SEQ_POINT(methodExecutionContext, 14963);
		goto IL_00d6;
	}

IL_004d:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14964);
		CHECK_SEQ_POINT(methodExecutionContext, 14965);
		float L_4 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_5 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14966);
		STORE_SEQ_POINT(methodExecutionContext, 14982);
		bool L_6 = ColorTween_get_ignoreTimeScale_mD27F5C7D70D340DBDFAE972BBE3857A26E29747A((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_5, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14982);
		G_B6_0 = L_4;
		G_B6_1 = ((U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *)(__this));
		if (!L_6)
		{
			G_B7_0 = L_4;
			G_B7_1 = ((U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *)(__this));
			goto IL_0075;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14967);
		STORE_SEQ_POINT(methodExecutionContext, 14983);
		float L_7 = Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2(/*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14983);
		G_B8_0 = L_7;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *)(G_B6_1));
		goto IL_007a;
	}

IL_0075:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14968);
		STORE_SEQ_POINT(methodExecutionContext, 14984);
		float L_8 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14984);
		G_B8_0 = L_8;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *)(G_B7_1));
	}

IL_007a:
	{
		NullCheck(G_B8_2, il2cpp_codegen_get_sequence_point(14968));
		G_B8_2->set_U3CelapsedTimeU3E__0_1(((float)il2cpp_codegen_add((float)G_B8_1, (float)G_B8_0)));
		CHECK_SEQ_POINT(methodExecutionContext, 14969);
		float L_9 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_10 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14970);
		STORE_SEQ_POINT(methodExecutionContext, 14985);
		float L_11 = ColorTween_get_duration_mE4A9B4FFAB11CCF25EAACF5777991AB6749020B0((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_10, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14985);
		CHECK_SEQ_POINT(methodExecutionContext, 14971);
		STORE_SEQ_POINT(methodExecutionContext, 14986);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B((float)((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14986);
		__this->set_U3CpercentageU3E__1_2(L_12);
		CHECK_SEQ_POINT(methodExecutionContext, 14972);
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_13 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_0();
		float L_14 = (float)__this->get_U3CpercentageU3E__1_2();
		CHECK_SEQ_POINT(methodExecutionContext, 14973);
		STORE_SEQ_POINT(methodExecutionContext, 14987);
		ColorTween_TweenValue_mF6B10FEA49EB758AD37D95A7DD577D6AA9C32110((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_13, (float)L_14, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14987);
		CHECK_SEQ_POINT(methodExecutionContext, 14974);
		__this->set_U24current_3(NULL);
		bool L_15 = (bool)__this->get_U24disposing_4();
		if (L_15)
		{
			goto IL_00d0;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00d0:
	{
		goto IL_0111;
	}

IL_00d5:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14975);
	}

IL_00d6:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14976);
		float L_16 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_17 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14977);
		STORE_SEQ_POINT(methodExecutionContext, 14988);
		float L_18 = ColorTween_get_duration_mE4A9B4FFAB11CCF25EAACF5777991AB6749020B0((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_17, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14988);
		if ((((float)L_16) < ((float)L_18)))
		{
			goto IL_004d;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14978);
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_19 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14979);
		STORE_SEQ_POINT(methodExecutionContext, 14989);
		ColorTween_TweenValue_mF6B10FEA49EB758AD37D95A7DD577D6AA9C32110((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_19, (float)(1.0f), /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14989);
		CHECK_SEQ_POINT(methodExecutionContext, 14980);
		__this->set_U24PC_5((-1));
	}

IL_010f:
	{
		return (bool)0;
	}

IL_0111:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m10DD8AC5B8A3A0C73F0A8D9699C5D5220FECC227_gshared (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	CHECK_PAUSE_POINT;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U24current_3();
		V_0 = (RuntimeObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m680D4804588601644224E48606E6D66465689171_gshared (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	CHECK_PAUSE_POINT;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U24current_3();
		V_0 = (RuntimeObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CStartU3Ec__Iterator0_Dispose_mF26FE6FE2B6669E490946F8EF355DC0CCAC860F7_gshared (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CStartU3Ec__Iterator0_Reset_m439E251EE39AF2851415914D8F11867A65D9E63F_gshared (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m439E251EE39AF2851415914D8F11867A65D9E63F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CStartU3Ec__Iterator0_Reset_m439E251EE39AF2851415914D8F11867A65D9E63F_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CStartU3Ec__Iterator0__ctor_m8B54A1BBDA9AFF06C9286D672F19BDB1001C320D_gshared (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CStartU3Ec__Iterator0_MoveNext_mB6BE65FBF43A13162E304F0F012BAA539F3D0C9F_gshared (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_mB6BE65FBF43A13162E304F0F012BAA539F3D0C9F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 14956);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 14957);
	float G_B7_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * G_B8_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00d5;
			}
		}
	}
	{
		goto IL_010f;
	}

IL_0021:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14958);
		CHECK_SEQ_POINT(methodExecutionContext, 14959);
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_2 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14960);
		STORE_SEQ_POINT(methodExecutionContext, 14981);
		bool L_3 = FloatTween_ValidTarget_m921F88A58CCB09A4D55DBB714F3538677363FAE6((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_2, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14981);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14961);
		goto IL_010f;
	}

IL_003d:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14962);
		__this->set_U3CelapsedTimeU3E__0_1((0.0f));
		CHECK_SEQ_POINT(methodExecutionContext, 14963);
		goto IL_00d6;
	}

IL_004d:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14964);
		CHECK_SEQ_POINT(methodExecutionContext, 14965);
		float L_4 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_5 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14966);
		STORE_SEQ_POINT(methodExecutionContext, 14982);
		bool L_6 = FloatTween_get_ignoreTimeScale_mCA3DA664CF6F78735BF3ED6301900FB849B49C34((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_5, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14982);
		G_B6_0 = L_4;
		G_B6_1 = ((U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *)(__this));
		if (!L_6)
		{
			G_B7_0 = L_4;
			G_B7_1 = ((U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *)(__this));
			goto IL_0075;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14967);
		STORE_SEQ_POINT(methodExecutionContext, 14983);
		float L_7 = Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2(/*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14983);
		G_B8_0 = L_7;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *)(G_B6_1));
		goto IL_007a;
	}

IL_0075:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14968);
		STORE_SEQ_POINT(methodExecutionContext, 14984);
		float L_8 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14984);
		G_B8_0 = L_8;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *)(G_B7_1));
	}

IL_007a:
	{
		NullCheck(G_B8_2, il2cpp_codegen_get_sequence_point(14968));
		G_B8_2->set_U3CelapsedTimeU3E__0_1(((float)il2cpp_codegen_add((float)G_B8_1, (float)G_B8_0)));
		CHECK_SEQ_POINT(methodExecutionContext, 14969);
		float L_9 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_10 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14970);
		STORE_SEQ_POINT(methodExecutionContext, 14985);
		float L_11 = FloatTween_get_duration_mBC42C5053BCB1A1315430E3E21ECE1597BB0B314((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_10, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14985);
		CHECK_SEQ_POINT(methodExecutionContext, 14971);
		STORE_SEQ_POINT(methodExecutionContext, 14986);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B((float)((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14986);
		__this->set_U3CpercentageU3E__1_2(L_12);
		CHECK_SEQ_POINT(methodExecutionContext, 14972);
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_13 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_0();
		float L_14 = (float)__this->get_U3CpercentageU3E__1_2();
		CHECK_SEQ_POINT(methodExecutionContext, 14973);
		STORE_SEQ_POINT(methodExecutionContext, 14987);
		FloatTween_TweenValue_m4ADF9CF3356268D7AD1CFF358BA252F1E52226F3((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_13, (float)L_14, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14987);
		CHECK_SEQ_POINT(methodExecutionContext, 14974);
		__this->set_U24current_3(NULL);
		bool L_15 = (bool)__this->get_U24disposing_4();
		if (L_15)
		{
			goto IL_00d0;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00d0:
	{
		goto IL_0111;
	}

IL_00d5:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14975);
	}

IL_00d6:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14976);
		float L_16 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_17 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14977);
		STORE_SEQ_POINT(methodExecutionContext, 14988);
		float L_18 = FloatTween_get_duration_mBC42C5053BCB1A1315430E3E21ECE1597BB0B314((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_17, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14988);
		if ((((float)L_16) < ((float)L_18)))
		{
			goto IL_004d;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14978);
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_19 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14979);
		STORE_SEQ_POINT(methodExecutionContext, 14989);
		FloatTween_TweenValue_m4ADF9CF3356268D7AD1CFF358BA252F1E52226F3((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_19, (float)(1.0f), /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14989);
		CHECK_SEQ_POINT(methodExecutionContext, 14980);
		__this->set_U24PC_5((-1));
	}

IL_010f:
	{
		return (bool)0;
	}

IL_0111:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m528DCE76B49785C679B66AAB4D42407B127A9B38_gshared (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	CHECK_PAUSE_POINT;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U24current_3();
		V_0 = (RuntimeObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m38831D13E816549A5D51699B9594609F833670F9_gshared (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	CHECK_PAUSE_POINT;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U24current_3();
		V_0 = (RuntimeObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CStartU3Ec__Iterator0_Dispose_m5D0C71B726544207A28FAE9F57B9D7401512C3C8_gshared (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CStartU3Ec__Iterator0_Reset_m7703121C2007A9FF4EC664BC758BC00DF683B629_gshared (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m7703121C2007A9FF4EC664BC758BC00DF683B629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CStartU3Ec__Iterator0_Reset_m7703121C2007A9FF4EC664BC758BC00DF683B629_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TweenRunner_1__ctor_m2D6244FBF370723CF73C376DC4D2E44D3EEEE290_gshared (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TweenRunner_1_Start_m756F514128966B0D57875B80DB12696B2DB60B97_gshared (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  ___tweenInfo0, const RuntimeMethod* method)
{
	U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	CHECK_PAUSE_POINT;
	{
		U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * L_0 = (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *)L_0;
		U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * L_1 = V_0;
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * L_3 = V_0;
		V_1 = (RuntimeObject*)L_3;
		goto IL_0014;
	}

IL_0014:
	{
		RuntimeObject* L_4 = V_1;
		return L_4;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
extern "C" IL2CPP_METHOD_ATTR void TweenRunner_1_Init_mA29C09ADC3EB6959A0F1572D48D84170443B670E_gshared (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * __this, MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___coroutineContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_Init_mA29C09ADC3EB6959A0F1572D48D84170443B670E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___coroutineContainer0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 14909);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 14910);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14911);
		CHECK_SEQ_POINT(methodExecutionContext, 14912);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		CHECK_SEQ_POINT(methodExecutionContext, 14913);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
extern "C" IL2CPP_METHOD_ATTR void TweenRunner_1_StartTween_mE0CB96AF945209ABC26F2AA9899CB9794A64D92D_gshared (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * __this, ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_mE0CB96AF945209ABC26F2AA9899CB9794A64D92D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___info0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 14914);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 14915);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14916);
		CHECK_SEQ_POINT(methodExecutionContext, 14917);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_0 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14918);
		STORE_SEQ_POINT(methodExecutionContext, 14937);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C((Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14937);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14919);
		CHECK_SEQ_POINT(methodExecutionContext, 14920);
		CHECK_SEQ_POINT(methodExecutionContext, 14921);
		STORE_SEQ_POINT(methodExecutionContext, 14938);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568((RuntimeObject *)_stringLiteralA413973124713A2B7B3570CE8D97C7151C8628A9, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14938);
		CHECK_SEQ_POINT(methodExecutionContext, 14922);
		goto IL_0073;
	}

IL_0022:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14923);
		CHECK_SEQ_POINT(methodExecutionContext, 14924);
		STORE_SEQ_POINT(methodExecutionContext, 14939);
		NullCheck((TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 *)__this, il2cpp_codegen_get_sequence_point(14924));
		((  void (*) (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		CHECK_SEQ_POINT(methodExecutionContext, 14939);
		CHECK_SEQ_POINT(methodExecutionContext, 14925);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_2 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14926);
		STORE_SEQ_POINT(methodExecutionContext, 14940);
		NullCheck((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_2, il2cpp_codegen_get_sequence_point(14926));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_2, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14940);
		CHECK_SEQ_POINT(methodExecutionContext, 14927);
		STORE_SEQ_POINT(methodExecutionContext, 14941);
		NullCheck((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)L_3, il2cpp_codegen_get_sequence_point(14927));
		bool L_4 = GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)L_3, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14941);
		if (L_4)
		{
			goto IL_0055;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14928);
		CHECK_SEQ_POINT(methodExecutionContext, 14929);
		CHECK_SEQ_POINT(methodExecutionContext, 14930);
		STORE_SEQ_POINT(methodExecutionContext, 14942);
		ColorTween_TweenValue_mF6B10FEA49EB758AD37D95A7DD577D6AA9C32110((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14942);
		CHECK_SEQ_POINT(methodExecutionContext, 14931);
		goto IL_0073;
	}

IL_0055:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14932);
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  L_5 = ___info0;
		CHECK_SEQ_POINT(methodExecutionContext, 14933);
		STORE_SEQ_POINT(methodExecutionContext, 14943);
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		CHECK_SEQ_POINT(methodExecutionContext, 14943);
		__this->set_m_Tween_1(L_6);
		CHECK_SEQ_POINT(methodExecutionContext, 14934);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_7 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get_m_Tween_1();
		CHECK_SEQ_POINT(methodExecutionContext, 14935);
		STORE_SEQ_POINT(methodExecutionContext, 14944);
		NullCheck((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_7, il2cpp_codegen_get_sequence_point(14935));
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_7, (RuntimeObject*)L_8, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14944);
	}

IL_0073:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14936);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StopTween()
extern "C" IL2CPP_METHOD_ATTR void TweenRunner_1_StopTween_m861C40714D7A8C4B7EF8A7CC781B06C600877A5F_gshared (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StopTween_m861C40714D7A8C4B7EF8A7CC781B06C600877A5F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 14945);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 14946);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14947);
		CHECK_SEQ_POINT(methodExecutionContext, 14948);
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_m_Tween_1();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14949);
		CHECK_SEQ_POINT(methodExecutionContext, 14950);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_1 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_m_Tween_1();
		CHECK_SEQ_POINT(methodExecutionContext, 14951);
		STORE_SEQ_POINT(methodExecutionContext, 14955);
		NullCheck((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_1, il2cpp_codegen_get_sequence_point(14951));
		MonoBehaviour_StopCoroutine_m3CDD6C046CC660D4CD6583FCE97F88A9735FD5FA((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_1, (RuntimeObject*)L_2, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14955);
		CHECK_SEQ_POINT(methodExecutionContext, 14952);
		__this->set_m_Tween_1((RuntimeObject*)NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14953);
	}

IL_0026:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14954);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TweenRunner_1__ctor_m63EA04E5FC2204138452DCD5D7C9A7918B7AD5E6_gshared (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Start(T)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TweenRunner_1_Start_m2CFC7722714F904EC8123AA4B7DCB974E466FD83_gshared (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  ___tweenInfo0, const RuntimeMethod* method)
{
	U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	CHECK_PAUSE_POINT;
	{
		U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * L_0 = (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *)L_0;
		U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * L_1 = V_0;
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * L_3 = V_0;
		V_1 = (RuntimeObject*)L_3;
		goto IL_0014;
	}

IL_0014:
	{
		RuntimeObject* L_4 = V_1;
		return L_4;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Init(UnityEngine.MonoBehaviour)
extern "C" IL2CPP_METHOD_ATTR void TweenRunner_1_Init_m39096EAEB1B5BFC0E2FEDF4A946593353F8981CC_gshared (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * __this, MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___coroutineContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_Init_m39096EAEB1B5BFC0E2FEDF4A946593353F8981CC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___coroutineContainer0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 14909);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 14910);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14911);
		CHECK_SEQ_POINT(methodExecutionContext, 14912);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		CHECK_SEQ_POINT(methodExecutionContext, 14913);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StartTween(T)
extern "C" IL2CPP_METHOD_ATTR void TweenRunner_1_StartTween_m8637A776CD96BAB0EDC8A8FA7479127E2BEC92C4_gshared (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * __this, FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m8637A776CD96BAB0EDC8A8FA7479127E2BEC92C4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___info0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 14914);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 14915);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14916);
		CHECK_SEQ_POINT(methodExecutionContext, 14917);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_0 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14918);
		STORE_SEQ_POINT(methodExecutionContext, 14937);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C((Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14937);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14919);
		CHECK_SEQ_POINT(methodExecutionContext, 14920);
		CHECK_SEQ_POINT(methodExecutionContext, 14921);
		STORE_SEQ_POINT(methodExecutionContext, 14938);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568((RuntimeObject *)_stringLiteralA413973124713A2B7B3570CE8D97C7151C8628A9, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14938);
		CHECK_SEQ_POINT(methodExecutionContext, 14922);
		goto IL_0073;
	}

IL_0022:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14923);
		CHECK_SEQ_POINT(methodExecutionContext, 14924);
		STORE_SEQ_POINT(methodExecutionContext, 14939);
		NullCheck((TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF *)__this, il2cpp_codegen_get_sequence_point(14924));
		((  void (*) (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		CHECK_SEQ_POINT(methodExecutionContext, 14939);
		CHECK_SEQ_POINT(methodExecutionContext, 14925);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_2 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		CHECK_SEQ_POINT(methodExecutionContext, 14926);
		STORE_SEQ_POINT(methodExecutionContext, 14940);
		NullCheck((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_2, il2cpp_codegen_get_sequence_point(14926));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_2, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14940);
		CHECK_SEQ_POINT(methodExecutionContext, 14927);
		STORE_SEQ_POINT(methodExecutionContext, 14941);
		NullCheck((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)L_3, il2cpp_codegen_get_sequence_point(14927));
		bool L_4 = GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)L_3, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14941);
		if (L_4)
		{
			goto IL_0055;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14928);
		CHECK_SEQ_POINT(methodExecutionContext, 14929);
		CHECK_SEQ_POINT(methodExecutionContext, 14930);
		STORE_SEQ_POINT(methodExecutionContext, 14942);
		FloatTween_TweenValue_m4ADF9CF3356268D7AD1CFF358BA252F1E52226F3((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14942);
		CHECK_SEQ_POINT(methodExecutionContext, 14931);
		goto IL_0073;
	}

IL_0055:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14932);
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  L_5 = ___info0;
		CHECK_SEQ_POINT(methodExecutionContext, 14933);
		STORE_SEQ_POINT(methodExecutionContext, 14943);
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		CHECK_SEQ_POINT(methodExecutionContext, 14943);
		__this->set_m_Tween_1(L_6);
		CHECK_SEQ_POINT(methodExecutionContext, 14934);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_7 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get_m_Tween_1();
		CHECK_SEQ_POINT(methodExecutionContext, 14935);
		STORE_SEQ_POINT(methodExecutionContext, 14944);
		NullCheck((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_7, il2cpp_codegen_get_sequence_point(14935));
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_7, (RuntimeObject*)L_8, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14944);
	}

IL_0073:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14936);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StopTween()
extern "C" IL2CPP_METHOD_ATTR void TweenRunner_1_StopTween_m9EDE8CC585AD166D4520BE8B374CA2169CDD0E8E_gshared (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StopTween_m9EDE8CC585AD166D4520BE8B374CA2169CDD0E8E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 14945);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 14946);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14947);
		CHECK_SEQ_POINT(methodExecutionContext, 14948);
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_m_Tween_1();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14949);
		CHECK_SEQ_POINT(methodExecutionContext, 14950);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_1 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_m_Tween_1();
		CHECK_SEQ_POINT(methodExecutionContext, 14951);
		STORE_SEQ_POINT(methodExecutionContext, 14955);
		NullCheck((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_1, il2cpp_codegen_get_sequence_point(14951));
		MonoBehaviour_StopCoroutine_m3CDD6C046CC660D4CD6583FCE97F88A9735FD5FA((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_1, (RuntimeObject*)L_2, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14955);
		CHECK_SEQ_POINT(methodExecutionContext, 14952);
		__this->set_m_Tween_1((RuntimeObject*)NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 14953);
	}

IL_0026:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 14954);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Clear(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Clear_mD973571E32F104549B24EC4C48FE9C290C6223BB_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___l0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___l0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37221);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37222);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37223);
		CHECK_SEQ_POINT(methodExecutionContext, 37224);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_0 = ___l0;
		CHECK_SEQ_POINT(methodExecutionContext, 37225);
		STORE_SEQ_POINT(methodExecutionContext, 37227);
		NullCheck((List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)L_0, il2cpp_codegen_get_sequence_point(37225));
		((  void (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		CHECK_SEQ_POINT(methodExecutionContext, 37227);
		CHECK_SEQ_POINT(methodExecutionContext, 37226);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Int32>::Get()
extern "C" IL2CPP_METHOD_ATTR List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ListPool_1_Get_mAFB1F76A280309F30C0897B1FBABCC37A783344C_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Get_mAFB1F76A280309F30C0897B1FBABCC37A783344C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37228);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37229);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37230);
		CHECK_SEQ_POINT(methodExecutionContext, 37231);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A * L_0 = ((ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		CHECK_SEQ_POINT(methodExecutionContext, 37232);
		STORE_SEQ_POINT(methodExecutionContext, 37234);
		NullCheck((ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *)L_0, il2cpp_codegen_get_sequence_point(37232));
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_1 = ((  List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * (*) (ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		CHECK_SEQ_POINT(methodExecutionContext, 37234);
		V_0 = (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37233);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Release_m34FF8B9650A82F8B5ECF477604AEF059FB85A153_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___toRelease0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___toRelease0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37235);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37236);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37237);
		CHECK_SEQ_POINT(methodExecutionContext, 37238);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A * L_0 = ((ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_1 = ___toRelease0;
		CHECK_SEQ_POINT(methodExecutionContext, 37239);
		STORE_SEQ_POINT(methodExecutionContext, 37241);
		NullCheck((ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *)L_0, il2cpp_codegen_get_sequence_point(37239));
		((  void (*) (ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *, List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *)L_0, (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		CHECK_SEQ_POINT(methodExecutionContext, 37241);
		CHECK_SEQ_POINT(methodExecutionContext, 37240);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::.cctor()
extern "C" IL2CPP_METHOD_ATTR void ListPool_1__cctor_mF27548A30BD87F6F4CE445FC8E518043FB0B295E_gshared (const RuntimeMethod* method)
{
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37242);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37243);
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37244);
		UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * L_0 = ((ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		STORE_SEQ_POINT(methodExecutionContext, 37245);
		UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * L_1 = (UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		CHECK_SEQ_POINT(methodExecutionContext, 37245);
		((ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * L_2 = ((ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		STORE_SEQ_POINT(methodExecutionContext, 37246);
		ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A * L_3 = (ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *, UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 *, UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 *)G_B2_0, (UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		CHECK_SEQ_POINT(methodExecutionContext, 37246);
		((ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Clear(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Clear_m740111DCF3FFCAA14F5FB747FAAF0ED39C11D126_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___l0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___l0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37221);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37222);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37223);
		CHECK_SEQ_POINT(methodExecutionContext, 37224);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = ___l0;
		CHECK_SEQ_POINT(methodExecutionContext, 37225);
		STORE_SEQ_POINT(methodExecutionContext, 37227);
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, il2cpp_codegen_get_sequence_point(37225));
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		CHECK_SEQ_POINT(methodExecutionContext, 37227);
		CHECK_SEQ_POINT(methodExecutionContext, 37226);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Object>::Get()
extern "C" IL2CPP_METHOD_ATTR List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ListPool_1_Get_m4C328048C1479EE1450837A0CF1BF5F18FF77C88_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Get_m4C328048C1479EE1450837A0CF1BF5F18FF77C88_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37228);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37229);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37230);
		CHECK_SEQ_POINT(methodExecutionContext, 37231);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B * L_0 = ((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		CHECK_SEQ_POINT(methodExecutionContext, 37232);
		STORE_SEQ_POINT(methodExecutionContext, 37234);
		NullCheck((ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *)L_0, il2cpp_codegen_get_sequence_point(37232));
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_1 = ((  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * (*) (ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		CHECK_SEQ_POINT(methodExecutionContext, 37234);
		V_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37233);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Release_m2F58A45DA9F2BBE95A654A426FF72F0CA75D5B7E_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___toRelease0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___toRelease0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37235);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37236);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37237);
		CHECK_SEQ_POINT(methodExecutionContext, 37238);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B * L_0 = ((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_1 = ___toRelease0;
		CHECK_SEQ_POINT(methodExecutionContext, 37239);
		STORE_SEQ_POINT(methodExecutionContext, 37241);
		NullCheck((ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *)L_0, il2cpp_codegen_get_sequence_point(37239));
		((  void (*) (ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *)L_0, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		CHECK_SEQ_POINT(methodExecutionContext, 37241);
		CHECK_SEQ_POINT(methodExecutionContext, 37240);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::.cctor()
extern "C" IL2CPP_METHOD_ATTR void ListPool_1__cctor_mF042EAEBF32922CBFC61DBE6E1DA0BF06A9DBC26_gshared (const RuntimeMethod* method)
{
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37242);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37243);
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37244);
		UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * L_0 = ((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		STORE_SEQ_POINT(methodExecutionContext, 37245);
		UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * L_1 = (UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		CHECK_SEQ_POINT(methodExecutionContext, 37245);
		((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * L_2 = ((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		STORE_SEQ_POINT(methodExecutionContext, 37246);
		ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B * L_3 = (ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *, UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 *, UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 *)G_B2_0, (UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		CHECK_SEQ_POINT(methodExecutionContext, 37246);
		((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Clear(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Clear_m9F04DC60F78A36A41D42EEDD9688FDD685667DB6_gshared (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___l0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___l0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37221);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37222);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37223);
		CHECK_SEQ_POINT(methodExecutionContext, 37224);
		List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * L_0 = ___l0;
		CHECK_SEQ_POINT(methodExecutionContext, 37225);
		STORE_SEQ_POINT(methodExecutionContext, 37227);
		NullCheck((List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *)L_0, il2cpp_codegen_get_sequence_point(37225));
		((  void (*) (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		CHECK_SEQ_POINT(methodExecutionContext, 37227);
		CHECK_SEQ_POINT(methodExecutionContext, 37226);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Get()
extern "C" IL2CPP_METHOD_ATTR List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ListPool_1_Get_m1549C4AC8324D6BBB9CAA80B70EE13A6AC0617B7_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Get_m1549C4AC8324D6BBB9CAA80B70EE13A6AC0617B7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37228);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37229);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37230);
		CHECK_SEQ_POINT(methodExecutionContext, 37231);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 * L_0 = ((ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		CHECK_SEQ_POINT(methodExecutionContext, 37232);
		STORE_SEQ_POINT(methodExecutionContext, 37234);
		NullCheck((ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *)L_0, il2cpp_codegen_get_sequence_point(37232));
		List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * L_1 = ((  List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * (*) (ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		CHECK_SEQ_POINT(methodExecutionContext, 37234);
		V_0 = (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37233);
		List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Release(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Release_m52E37BEE7C08F871746F8E3B28556641EC754324_gshared (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___toRelease0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___toRelease0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37235);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37236);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37237);
		CHECK_SEQ_POINT(methodExecutionContext, 37238);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 * L_0 = ((ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * L_1 = ___toRelease0;
		CHECK_SEQ_POINT(methodExecutionContext, 37239);
		STORE_SEQ_POINT(methodExecutionContext, 37241);
		NullCheck((ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *)L_0, il2cpp_codegen_get_sequence_point(37239));
		((  void (*) (ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *, List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *)L_0, (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		CHECK_SEQ_POINT(methodExecutionContext, 37241);
		CHECK_SEQ_POINT(methodExecutionContext, 37240);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::.cctor()
extern "C" IL2CPP_METHOD_ATTR void ListPool_1__cctor_m7AEC73CEB51505546BA51E71698753674FE8781B_gshared (const RuntimeMethod* method)
{
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37242);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37243);
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37244);
		UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * L_0 = ((ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		STORE_SEQ_POINT(methodExecutionContext, 37245);
		UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * L_1 = (UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		CHECK_SEQ_POINT(methodExecutionContext, 37245);
		((ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * L_2 = ((ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		STORE_SEQ_POINT(methodExecutionContext, 37246);
		ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 * L_3 = (ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *, UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 *, UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 *)G_B2_0, (UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		CHECK_SEQ_POINT(methodExecutionContext, 37246);
		((ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Clear(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Clear_m2208C1BA3B3E3790C5EE59E69407637C41F10844_gshared (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * ___l0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___l0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37221);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37222);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37223);
		CHECK_SEQ_POINT(methodExecutionContext, 37224);
		List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * L_0 = ___l0;
		CHECK_SEQ_POINT(methodExecutionContext, 37225);
		STORE_SEQ_POINT(methodExecutionContext, 37227);
		NullCheck((List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *)L_0, il2cpp_codegen_get_sequence_point(37225));
		((  void (*) (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		CHECK_SEQ_POINT(methodExecutionContext, 37227);
		CHECK_SEQ_POINT(methodExecutionContext, 37226);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Get()
extern "C" IL2CPP_METHOD_ATTR List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * ListPool_1_Get_m3D24B0F028698E7FA9EAB826563501BFF986BBFD_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Get_m3D24B0F028698E7FA9EAB826563501BFF986BBFD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37228);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37229);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37230);
		CHECK_SEQ_POINT(methodExecutionContext, 37231);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 * L_0 = ((ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		CHECK_SEQ_POINT(methodExecutionContext, 37232);
		STORE_SEQ_POINT(methodExecutionContext, 37234);
		NullCheck((ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *)L_0, il2cpp_codegen_get_sequence_point(37232));
		List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * L_1 = ((  List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * (*) (ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		CHECK_SEQ_POINT(methodExecutionContext, 37234);
		V_0 = (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37233);
		List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Release(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Release_m4FBBEE3FE54B6B374CD3F3CA7586029E579FAD6C_gshared (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * ___toRelease0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___toRelease0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37235);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37236);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37237);
		CHECK_SEQ_POINT(methodExecutionContext, 37238);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 * L_0 = ((ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * L_1 = ___toRelease0;
		CHECK_SEQ_POINT(methodExecutionContext, 37239);
		STORE_SEQ_POINT(methodExecutionContext, 37241);
		NullCheck((ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *)L_0, il2cpp_codegen_get_sequence_point(37239));
		((  void (*) (ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *, List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *)L_0, (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		CHECK_SEQ_POINT(methodExecutionContext, 37241);
		CHECK_SEQ_POINT(methodExecutionContext, 37240);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::.cctor()
extern "C" IL2CPP_METHOD_ATTR void ListPool_1__cctor_m96D7F2BB0CC611C7E4C5828B2E10AE50192A4FCE_gshared (const RuntimeMethod* method)
{
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37242);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37243);
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37244);
		UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * L_0 = ((ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		STORE_SEQ_POINT(methodExecutionContext, 37245);
		UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * L_1 = (UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		CHECK_SEQ_POINT(methodExecutionContext, 37245);
		((ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * L_2 = ((ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		STORE_SEQ_POINT(methodExecutionContext, 37246);
		ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 * L_3 = (ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *, UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 *, UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 *)G_B2_0, (UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		CHECK_SEQ_POINT(methodExecutionContext, 37246);
		((ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Clear(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Clear_m6432D570E705BF537C4FB85407A31C9AAD6A99E6_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___l0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___l0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37221);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37222);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37223);
		CHECK_SEQ_POINT(methodExecutionContext, 37224);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_0 = ___l0;
		CHECK_SEQ_POINT(methodExecutionContext, 37225);
		STORE_SEQ_POINT(methodExecutionContext, 37227);
		NullCheck((List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)L_0, il2cpp_codegen_get_sequence_point(37225));
		((  void (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		CHECK_SEQ_POINT(methodExecutionContext, 37227);
		CHECK_SEQ_POINT(methodExecutionContext, 37226);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Get()
extern "C" IL2CPP_METHOD_ATTR List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ListPool_1_Get_m4322F57FCB5B11C10CAEDE17A609B9E29401797F_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Get_m4322F57FCB5B11C10CAEDE17A609B9E29401797F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37228);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37229);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37230);
		CHECK_SEQ_POINT(methodExecutionContext, 37231);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 * L_0 = ((ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		CHECK_SEQ_POINT(methodExecutionContext, 37232);
		STORE_SEQ_POINT(methodExecutionContext, 37234);
		NullCheck((ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *)L_0, il2cpp_codegen_get_sequence_point(37232));
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_1 = ((  List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * (*) (ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		CHECK_SEQ_POINT(methodExecutionContext, 37234);
		V_0 = (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37233);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Release(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Release_m3339B7710487E883FD2DB198643084787D08BBDD_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___toRelease0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___toRelease0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37235);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37236);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37237);
		CHECK_SEQ_POINT(methodExecutionContext, 37238);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 * L_0 = ((ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_1 = ___toRelease0;
		CHECK_SEQ_POINT(methodExecutionContext, 37239);
		STORE_SEQ_POINT(methodExecutionContext, 37241);
		NullCheck((ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *)L_0, il2cpp_codegen_get_sequence_point(37239));
		((  void (*) (ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *, List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *)L_0, (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		CHECK_SEQ_POINT(methodExecutionContext, 37241);
		CHECK_SEQ_POINT(methodExecutionContext, 37240);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::.cctor()
extern "C" IL2CPP_METHOD_ATTR void ListPool_1__cctor_m361000F7CAFBC65FFD4FC5D55BA85108853E0376_gshared (const RuntimeMethod* method)
{
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37242);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37243);
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37244);
		UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * L_0 = ((ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		STORE_SEQ_POINT(methodExecutionContext, 37245);
		UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * L_1 = (UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		CHECK_SEQ_POINT(methodExecutionContext, 37245);
		((ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * L_2 = ((ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		STORE_SEQ_POINT(methodExecutionContext, 37246);
		ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 * L_3 = (ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *, UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 *, UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 *)G_B2_0, (UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		CHECK_SEQ_POINT(methodExecutionContext, 37246);
		((ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Clear(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Clear_m8B0E6BEA4C6F4445E8F5BADD3C577054A3445893_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___l0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___l0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37221);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37222);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37223);
		CHECK_SEQ_POINT(methodExecutionContext, 37224);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_0 = ___l0;
		CHECK_SEQ_POINT(methodExecutionContext, 37225);
		STORE_SEQ_POINT(methodExecutionContext, 37227);
		NullCheck((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)L_0, il2cpp_codegen_get_sequence_point(37225));
		((  void (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		CHECK_SEQ_POINT(methodExecutionContext, 37227);
		CHECK_SEQ_POINT(methodExecutionContext, 37226);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Get()
extern "C" IL2CPP_METHOD_ATTR List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ListPool_1_Get_m0DA6F4FA9B55233F3BCC582E1B5D1B82048B72DC_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Get_m0DA6F4FA9B55233F3BCC582E1B5D1B82048B72DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37228);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37229);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37230);
		CHECK_SEQ_POINT(methodExecutionContext, 37231);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 * L_0 = ((ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		CHECK_SEQ_POINT(methodExecutionContext, 37232);
		STORE_SEQ_POINT(methodExecutionContext, 37234);
		NullCheck((ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *)L_0, il2cpp_codegen_get_sequence_point(37232));
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_1 = ((  List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * (*) (ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		CHECK_SEQ_POINT(methodExecutionContext, 37234);
		V_0 = (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37233);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Release(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Release_m427896919DD66BB0E8CF7274C8AE76036D36D3BD_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___toRelease0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___toRelease0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37235);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37236);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37237);
		CHECK_SEQ_POINT(methodExecutionContext, 37238);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 * L_0 = ((ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_1 = ___toRelease0;
		CHECK_SEQ_POINT(methodExecutionContext, 37239);
		STORE_SEQ_POINT(methodExecutionContext, 37241);
		NullCheck((ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *)L_0, il2cpp_codegen_get_sequence_point(37239));
		((  void (*) (ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *, List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *)L_0, (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		CHECK_SEQ_POINT(methodExecutionContext, 37241);
		CHECK_SEQ_POINT(methodExecutionContext, 37240);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::.cctor()
extern "C" IL2CPP_METHOD_ATTR void ListPool_1__cctor_m4BEC87EBA6EA67DFC457FDB45282F75A64C6C347_gshared (const RuntimeMethod* method)
{
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37242);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37243);
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37244);
		UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * L_0 = ((ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		STORE_SEQ_POINT(methodExecutionContext, 37245);
		UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * L_1 = (UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		CHECK_SEQ_POINT(methodExecutionContext, 37245);
		((ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * L_2 = ((ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		STORE_SEQ_POINT(methodExecutionContext, 37246);
		ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 * L_3 = (ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *, UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E *, UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E *)G_B2_0, (UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		CHECK_SEQ_POINT(methodExecutionContext, 37246);
		((ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Clear(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Clear_m0CCFC711C25B7AAC542D2ABBA0E1B4BFE8D037EA_gshared (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___l0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___l0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37221);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37222);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37223);
		CHECK_SEQ_POINT(methodExecutionContext, 37224);
		List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * L_0 = ___l0;
		CHECK_SEQ_POINT(methodExecutionContext, 37225);
		STORE_SEQ_POINT(methodExecutionContext, 37227);
		NullCheck((List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *)L_0, il2cpp_codegen_get_sequence_point(37225));
		((  void (*) (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		CHECK_SEQ_POINT(methodExecutionContext, 37227);
		CHECK_SEQ_POINT(methodExecutionContext, 37226);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Get()
extern "C" IL2CPP_METHOD_ATTR List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ListPool_1_Get_mECE6D9ED10FB4EEF28A6BE1F9B445CEE4312A937_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Get_mECE6D9ED10FB4EEF28A6BE1F9B445CEE4312A937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37228);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37229);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37230);
		CHECK_SEQ_POINT(methodExecutionContext, 37231);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 * L_0 = ((ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		CHECK_SEQ_POINT(methodExecutionContext, 37232);
		STORE_SEQ_POINT(methodExecutionContext, 37234);
		NullCheck((ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *)L_0, il2cpp_codegen_get_sequence_point(37232));
		List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * L_1 = ((  List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * (*) (ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		CHECK_SEQ_POINT(methodExecutionContext, 37234);
		V_0 = (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37233);
		List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Release(System.Collections.Generic.List`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ListPool_1_Release_m1252D062655820C50CAC05E08676836EBC1A7BA0_gshared (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___toRelease0, const RuntimeMethod* method)
{
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___toRelease0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37235);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37236);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37237);
		CHECK_SEQ_POINT(methodExecutionContext, 37238);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 * L_0 = ((ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * L_1 = ___toRelease0;
		CHECK_SEQ_POINT(methodExecutionContext, 37239);
		STORE_SEQ_POINT(methodExecutionContext, 37241);
		NullCheck((ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *)L_0, il2cpp_codegen_get_sequence_point(37239));
		((  void (*) (ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *, List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *)L_0, (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		CHECK_SEQ_POINT(methodExecutionContext, 37241);
		CHECK_SEQ_POINT(methodExecutionContext, 37240);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::.cctor()
extern "C" IL2CPP_METHOD_ATTR void ListPool_1__cctor_mFC21D520E6A974F00E7D85540B0973269E58626A_gshared (const RuntimeMethod* method)
{
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, NULL, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37242);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37243);
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37244);
		UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * L_0 = ((ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		STORE_SEQ_POINT(methodExecutionContext, 37245);
		UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * L_1 = (UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		CHECK_SEQ_POINT(methodExecutionContext, 37245);
		((ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * L_2 = ((ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		STORE_SEQ_POINT(methodExecutionContext, 37246);
		ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 * L_3 = (ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *, UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB *, UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB *)G_B2_0, (UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		CHECK_SEQ_POINT(methodExecutionContext, 37246);
		((ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C" IL2CPP_METHOD_ATTR void ObjectPool_1__ctor_mE14E9596DD21AFBA47727BDC5226C7F24FA234C7_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * ___actionOnGet0, UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * ___actionOnRelease1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1__ctor_mE14E9596DD21AFBA47727BDC5226C7F24FA234C7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___actionOnGet0), (&___actionOnRelease1));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37247);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37248);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37249);
		CHECK_SEQ_POINT(methodExecutionContext, 37250);
		STORE_SEQ_POINT(methodExecutionContext, 37256);
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_0 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		CHECK_SEQ_POINT(methodExecutionContext, 37256);
		__this->set_m_Stack_0(L_0);
		CHECK_SEQ_POINT(methodExecutionContext, 37251);
		STORE_SEQ_POINT(methodExecutionContext, 37257);
		NullCheck((RuntimeObject *)__this, il2cpp_codegen_get_sequence_point(37251));
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 37257);
		CHECK_SEQ_POINT(methodExecutionContext, 37252);
		CHECK_SEQ_POINT(methodExecutionContext, 37253);
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_1 = ___actionOnGet0;
		__this->set_m_ActionOnGet_1(L_1);
		CHECK_SEQ_POINT(methodExecutionContext, 37254);
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_2 = ___actionOnRelease1;
		__this->set_m_ActionOnRelease_2(L_2);
		CHECK_SEQ_POINT(methodExecutionContext, 37255);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countAll()
extern "C" IL2CPP_METHOD_ATTR int32_t ObjectPool_1_get_countAll_m2336F21CA9AFFCBF50463DD1836B14E663836D58_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_get_countAll_m2336F21CA9AFFCBF50463DD1836B14E663836D58_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37258);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37259);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37260);
		int32_t L_0 = (int32_t)__this->get_U3CcountAllU3Ek__BackingField_3();
		V_0 = (int32_t)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::set_countAll(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ObjectPool_1_set_countAll_mF68D62D98B4F6AFACAC85C02E4DB858DA7196F64_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_set_countAll_mF68D62D98B4F6AFACAC85C02E4DB858DA7196F64_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___value0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37261);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37262);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37263);
		int32_t L_0 = ___value0;
		__this->set_U3CcountAllU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countActive()
extern "C" IL2CPP_METHOD_ATTR int32_t ObjectPool_1_get_countActive_m12221F42D2CD25F418A1746CFEE20C65A630B6F1_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_get_countActive_m12221F42D2CD25F418A1746CFEE20C65A630B6F1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37264);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37265);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37266);
		CHECK_SEQ_POINT(methodExecutionContext, 37267);
		CHECK_SEQ_POINT(methodExecutionContext, 37268);
		STORE_SEQ_POINT(methodExecutionContext, 37271);
		NullCheck((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this, il2cpp_codegen_get_sequence_point(37268));
		int32_t L_0 = ((  int32_t (*) (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		CHECK_SEQ_POINT(methodExecutionContext, 37271);
		CHECK_SEQ_POINT(methodExecutionContext, 37269);
		STORE_SEQ_POINT(methodExecutionContext, 37272);
		NullCheck((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this, il2cpp_codegen_get_sequence_point(37269));
		int32_t L_1 = ((  int32_t (*) (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		CHECK_SEQ_POINT(methodExecutionContext, 37272);
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37270);
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countInactive()
extern "C" IL2CPP_METHOD_ATTR int32_t ObjectPool_1_get_countInactive_mF6EBCB20814CAE2E818F846689640BF4485E810F_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_get_countInactive_mF6EBCB20814CAE2E818F846689640BF4485E810F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, NULL, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37273);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37274);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37275);
		CHECK_SEQ_POINT(methodExecutionContext, 37276);
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_0 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)__this->get_m_Stack_0();
		CHECK_SEQ_POINT(methodExecutionContext, 37277);
		STORE_SEQ_POINT(methodExecutionContext, 37279);
		NullCheck((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_0, il2cpp_codegen_get_sequence_point(37277));
		int32_t L_1 = ((  int32_t (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		CHECK_SEQ_POINT(methodExecutionContext, 37279);
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37278);
		int32_t L_2 = V_0;
		return L_2;
	}
}
// T UnityEngine.UI.ObjectPool`1<System.Object>::Get()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * ObjectPool_1_Get_m82BA8AF24CE96731493EC3C52B1218DB2C3C934F_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Get_m82BA8AF24CE96731493EC3C52B1218DB2C3C934F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_LOCALS(methodExecutionContextLocals, (&V_0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, NULL, methodExecutionContextLocals);
	CHECK_SEQ_POINT(methodExecutionContext, 37280);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37281);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37282);
		CHECK_SEQ_POINT(methodExecutionContext, 37283);
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_0 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)__this->get_m_Stack_0();
		CHECK_SEQ_POINT(methodExecutionContext, 37284);
		STORE_SEQ_POINT(methodExecutionContext, 37300);
		NullCheck((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_0, il2cpp_codegen_get_sequence_point(37284));
		int32_t L_1 = ((  int32_t (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		CHECK_SEQ_POINT(methodExecutionContext, 37300);
		if (L_1)
		{
			goto IL_002c;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37285);
		CHECK_SEQ_POINT(methodExecutionContext, 37286);
		STORE_SEQ_POINT(methodExecutionContext, 37301);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		CHECK_SEQ_POINT(methodExecutionContext, 37301);
		V_0 = (RuntimeObject *)L_2;
		CHECK_SEQ_POINT(methodExecutionContext, 37287);
		CHECK_SEQ_POINT(methodExecutionContext, 37288);
		STORE_SEQ_POINT(methodExecutionContext, 37302);
		NullCheck((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this, il2cpp_codegen_get_sequence_point(37288));
		int32_t L_3 = ((  int32_t (*) (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		CHECK_SEQ_POINT(methodExecutionContext, 37302);
		CHECK_SEQ_POINT(methodExecutionContext, 37289);
		STORE_SEQ_POINT(methodExecutionContext, 37303);
		NullCheck((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this, il2cpp_codegen_get_sequence_point(37289));
		((  void (*) (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		CHECK_SEQ_POINT(methodExecutionContext, 37303);
		CHECK_SEQ_POINT(methodExecutionContext, 37290);
		goto IL_003a;
	}

IL_002c:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37291);
		CHECK_SEQ_POINT(methodExecutionContext, 37292);
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_4 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)__this->get_m_Stack_0();
		CHECK_SEQ_POINT(methodExecutionContext, 37293);
		STORE_SEQ_POINT(methodExecutionContext, 37304);
		NullCheck((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_4, il2cpp_codegen_get_sequence_point(37293));
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		CHECK_SEQ_POINT(methodExecutionContext, 37304);
		V_0 = (RuntimeObject *)L_5;
		CHECK_SEQ_POINT(methodExecutionContext, 37294);
	}

IL_003a:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37295);
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_6 = (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)__this->get_m_ActionOnGet_1();
		if (!L_6)
		{
			goto IL_0051;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37296);
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_7 = (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)__this->get_m_ActionOnGet_1();
		RuntimeObject * L_8 = V_0;
		CHECK_SEQ_POINT(methodExecutionContext, 37297);
		STORE_SEQ_POINT(methodExecutionContext, 37305);
		NullCheck((UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)L_7, il2cpp_codegen_get_sequence_point(37297));
		((  void (*) (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		CHECK_SEQ_POINT(methodExecutionContext, 37305);
	}

IL_0051:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37298);
		RuntimeObject * L_9 = V_0;
		V_1 = (RuntimeObject *)L_9;
		goto IL_0058;
	}

IL_0058:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37299);
		RuntimeObject * L_10 = V_1;
		return L_10;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::Release(T)
extern "C" IL2CPP_METHOD_ATTR void ObjectPool_1_Release_mEF76D0678288FA4D4D8D81C57B3FEBF7AE87BD74_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, RuntimeObject * ___element0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Release_mEF76D0678288FA4D4D8D81C57B3FEBF7AE87BD74_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_THIS(methodExecutionContextThis, (&__this));
	DECLARE_METHOD_PARAMS(methodExecutionContextParameters, (&___element0));
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, method, methodExecutionContextThis, methodExecutionContextParameters, NULL);
	CHECK_SEQ_POINT(methodExecutionContext, 37306);
	CHECK_METHOD_EXIT_SEQ_POINT(methodExitChecker, methodExecutionContext, 37307);
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37308);
		CHECK_SEQ_POINT(methodExecutionContext, 37309);
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_0 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)__this->get_m_Stack_0();
		CHECK_SEQ_POINT(methodExecutionContext, 37310);
		STORE_SEQ_POINT(methodExecutionContext, 37321);
		NullCheck((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_0, il2cpp_codegen_get_sequence_point(37310));
		int32_t L_1 = ((  int32_t (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		CHECK_SEQ_POINT(methodExecutionContext, 37321);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_2 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)__this->get_m_Stack_0();
		CHECK_SEQ_POINT(methodExecutionContext, 37311);
		STORE_SEQ_POINT(methodExecutionContext, 37322);
		NullCheck((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_2, il2cpp_codegen_get_sequence_point(37311));
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		CHECK_SEQ_POINT(methodExecutionContext, 37322);
		RuntimeObject * L_4 = ___element0;
		CHECK_SEQ_POINT(methodExecutionContext, 37312);
		STORE_SEQ_POINT(methodExecutionContext, 37323);
		bool L_5 = il2cpp_codegen_object_reference_equals((RuntimeObject *)L_3, (RuntimeObject *)L_4);
		CHECK_SEQ_POINT(methodExecutionContext, 37323);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37313);
		CHECK_SEQ_POINT(methodExecutionContext, 37314);
		STORE_SEQ_POINT(methodExecutionContext, 37324);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29((RuntimeObject *)_stringLiteral04231B44477132B3DBEFE7768A921AE5A13A00FC, /*hidden argument*/NULL);
		CHECK_SEQ_POINT(methodExecutionContext, 37324);
	}

IL_003c:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37315);
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_6 = (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)__this->get_m_ActionOnRelease_2();
		if (!L_6)
		{
			goto IL_0053;
		}
	}
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37316);
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_7 = (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)__this->get_m_ActionOnRelease_2();
		RuntimeObject * L_8 = ___element0;
		CHECK_SEQ_POINT(methodExecutionContext, 37317);
		STORE_SEQ_POINT(methodExecutionContext, 37325);
		NullCheck((UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)L_7, il2cpp_codegen_get_sequence_point(37317));
		((  void (*) (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		CHECK_SEQ_POINT(methodExecutionContext, 37325);
	}

IL_0053:
	{
		CHECK_SEQ_POINT(methodExecutionContext, 37318);
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_9 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)__this->get_m_Stack_0();
		RuntimeObject * L_10 = ___element0;
		CHECK_SEQ_POINT(methodExecutionContext, 37319);
		STORE_SEQ_POINT(methodExecutionContext, 37326);
		NullCheck((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_9, il2cpp_codegen_get_sequence_point(37319));
		((  void (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_9, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		CHECK_SEQ_POINT(methodExecutionContext, 37326);
		CHECK_SEQ_POINT(methodExecutionContext, 37320);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void EventFunction_2__ctor_m97D734DEDA398038AD83688A1B5D6FE74AC22727_gshared (EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,System.Boolean>::Invoke(T,TArgs)
extern "C" IL2CPP_METHOD_ATTR void EventFunction_2_Invoke_m14648E2CABB9C19C6789AFB2BC3D560703E22469_gshared (EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * __this, RuntimeObject * ___handler0, bool ___value1, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 2)
				{
					// open
					typedef void (*FunctionPointerType) (RuntimeObject *, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, RuntimeObject *, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, ___value1, targetMethod);
				}
			}
			else if (___parameterCount != 2)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< bool >::Invoke(targetMethod, ___handler0, ___value1);
							else
								GenericVirtActionInvoker1< bool >::Invoke(targetMethod, ___handler0, ___value1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___handler0, ___value1);
							else
								VirtActionInvoker1< bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___handler0, ___value1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (RuntimeObject *, bool, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker2< RuntimeObject *, bool >::Invoke(targetMethod, targetThis, ___handler0, ___value1);
							else
								GenericVirtActionInvoker2< RuntimeObject *, bool >::Invoke(targetMethod, targetThis, ___handler0, ___value1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker2< RuntimeObject *, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___handler0, ___value1);
							else
								VirtActionInvoker2< RuntimeObject *, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___handler0, ___value1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, RuntimeObject *, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, ___value1, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, ___value1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< bool >::Invoke(targetMethod, ___handler0, ___value1);
						else
							GenericVirtActionInvoker1< bool >::Invoke(targetMethod, ___handler0, ___value1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___handler0, ___value1);
						else
							VirtActionInvoker1< bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___handler0, ___value1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, bool, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker2< RuntimeObject *, bool >::Invoke(targetMethod, targetThis, ___handler0, ___value1);
						else
							GenericVirtActionInvoker2< RuntimeObject *, bool >::Invoke(targetMethod, targetThis, ___handler0, ___value1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker2< RuntimeObject *, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___handler0, ___value1);
						else
							VirtActionInvoker2< RuntimeObject *, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___handler0, ___value1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, ___value1, targetMethod);
			}
		}
	}
}
// System.IAsyncResult hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,System.Boolean>::BeginInvoke(T,TArgs,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* EventFunction_2_BeginInvoke_m678D466D954B18E90044F2E025C4819A4A463F72_gshared (EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * __this, RuntimeObject * ___handler0, bool ___value1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EventFunction_2_BeginInvoke_m678D466D954B18E90044F2E025C4819A4A463F72_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___handler0;
	__d_args[1] = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &___value1);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void EventFunction_2_EndInvoke_m4CE591A7EEDB6F1E546F12609501B9171C0F2459_gshared (EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void EventFunction_2__ctor_m127D177A7DC9888E77C0DA66A7B957AE9D2E270C_gshared (EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,System.Object>::Invoke(T,TArgs)
extern "C" IL2CPP_METHOD_ATTR void EventFunction_2_Invoke_m2F876CC93C664C166EFC995293BACB4CB5BE24B7_gshared (EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * __this, RuntimeObject * ___handler0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 2)
				{
					// open
					typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, ___value1, targetMethod);
				}
			}
			else if (___parameterCount != 2)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< RuntimeObject * >::Invoke(targetMethod, ___handler0, ___value1);
							else
								GenericVirtActionInvoker1< RuntimeObject * >::Invoke(targetMethod, ___handler0, ___value1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___handler0, ___value1);
							else
								VirtActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___handler0, ___value1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___handler0, ___value1);
							else
								GenericVirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___handler0, ___value1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___handler0, ___value1);
							else
								VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___handler0, ___value1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, ___value1, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, ___value1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< RuntimeObject * >::Invoke(targetMethod, ___handler0, ___value1);
						else
							GenericVirtActionInvoker1< RuntimeObject * >::Invoke(targetMethod, ___handler0, ___value1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___handler0, ___value1);
						else
							VirtActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___handler0, ___value1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___handler0, ___value1);
						else
							GenericVirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___handler0, ___value1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___handler0, ___value1);
						else
							VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___handler0, ___value1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, ___value1, targetMethod);
			}
		}
	}
}
// System.IAsyncResult hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,System.Object>::BeginInvoke(T,TArgs,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* EventFunction_2_BeginInvoke_m9C8C9E8133B6286D8ABDC2379FCE6291EBD89002_gshared (EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * __this, RuntimeObject * ___handler0, RuntimeObject * ___value1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___handler0;
	__d_args[1] = ___value1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void EventFunction_2_EndInvoke_mFA37A49D031F4250AE84360038EB0620DC211A68_gshared (EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void EventFunction_2__ctor_m2B082E3620193000B87D7572E2C2C904A83582B7_gshared (EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,UnityEngine.Vector2>::Invoke(T,TArgs)
extern "C" IL2CPP_METHOD_ATTR void EventFunction_2_Invoke_mB1474AAACF9A9C537026C23061874D1DA7DBFB99_gshared (EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * __this, RuntimeObject * ___handler0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value1, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 2)
				{
					// open
					typedef void (*FunctionPointerType) (RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, ___value1, targetMethod);
				}
			}
			else if (___parameterCount != 2)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(targetMethod, ___handler0, ___value1);
							else
								GenericVirtActionInvoker1< Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(targetMethod, ___handler0, ___value1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___handler0, ___value1);
							else
								VirtActionInvoker1< Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___handler0, ___value1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker2< RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(targetMethod, targetThis, ___handler0, ___value1);
							else
								GenericVirtActionInvoker2< RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(targetMethod, targetThis, ___handler0, ___value1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker2< RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___handler0, ___value1);
							else
								VirtActionInvoker2< RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___handler0, ___value1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, ___value1, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, ___value1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(targetMethod, ___handler0, ___value1);
						else
							GenericVirtActionInvoker1< Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(targetMethod, ___handler0, ___value1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___handler0, ___value1);
						else
							VirtActionInvoker1< Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___handler0, ___value1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___handler0, ___value1, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker2< RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(targetMethod, targetThis, ___handler0, ___value1);
						else
							GenericVirtActionInvoker2< RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(targetMethod, targetThis, ___handler0, ___value1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker2< RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___handler0, ___value1);
						else
							VirtActionInvoker2< RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___handler0, ___value1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, ___value1, targetMethod);
			}
		}
	}
}
// System.IAsyncResult hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,UnityEngine.Vector2>::BeginInvoke(T,TArgs,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* EventFunction_2_BeginInvoke_m1BA75FDD482EA91164FA95B9DC0A56B7B9E3EFB4_gshared (EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * __this, RuntimeObject * ___handler0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EventFunction_2_BeginInvoke_m1BA75FDD482EA91164FA95B9DC0A56B7B9E3EFB4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___handler0;
	__d_args[1] = Box(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var, &___value1);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<System.Object,UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void EventFunction_2_EndInvoke_mB5F8EA0CDB288304A74E01348749D9CBC56C6D49_gshared (EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,System.Boolean>::.ctor(hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<THandler,TValue>)
extern "C" IL2CPP_METHOD_ATTR void HierarchyEventHelper_2__ctor_m8AD189CE9CBD269D96F157FF0E9ACB54FD6BFC25_gshared (HierarchyEventHelper_2_t83507A10FBC53F7024CE976D2CCBF4918C8D50EB * __this, EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * ___executeDelegate0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * L_0 = ___executeDelegate0;
		NullCheck((HierarchyEventHelper_2_t83507A10FBC53F7024CE976D2CCBF4918C8D50EB *)__this);
		((  void (*) (HierarchyEventHelper_2_t83507A10FBC53F7024CE976D2CCBF4918C8D50EB *, EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((HierarchyEventHelper_2_t83507A10FBC53F7024CE976D2CCBF4918C8D50EB *)__this, (EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 *)L_0, (int32_t)5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return;
	}
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,System.Boolean>::.ctor(hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<THandler,TValue>,hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2_zEynGeOBdVgNxiYwtZLsacBtwtY<THandler,TValue>)
extern "C" IL2CPP_METHOD_ATTR void HierarchyEventHelper_2__ctor_mE158238CBBCDE89D494FC2BC6A2257D2D07E1A70_gshared (HierarchyEventHelper_2_t83507A10FBC53F7024CE976D2CCBF4918C8D50EB * __this, EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * ___executeDelegate0, int32_t ___executeOn1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HierarchyEventHelper_2__ctor_mE158238CBBCDE89D494FC2BC6A2257D2D07E1A70_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * L_0 = ___executeDelegate0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral52934C29CA4A1A7572C6905BD6AB705A2C135973, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, HierarchyEventHelper_2__ctor_mE158238CBBCDE89D494FC2BC6A2257D2D07E1A70_RuntimeMethod_var);
	}

IL_0014:
	{
		EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * L_2 = ___executeDelegate0;
		__this->set_kRJcljaQFQASapamFTwqLIpinzNN_0(L_2);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_3 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1));
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		__this->set_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1(L_3);
		int32_t L_4 = ___executeOn1;
		__this->set_WwsFTLbfEnWIyENevELMTAgByIi_2(L_4);
		return;
	}
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,System.Boolean>::ExecuteOnAll(TValue)
extern "C" IL2CPP_METHOD_ATTR void HierarchyEventHelper_2_ExecuteOnAll_mF6AB8D4A1FE54E0C76CCE7165A0F8A2F4D8FCDA4_gshared (HierarchyEventHelper_2_t83507A10FBC53F7024CE976D2CCBF4918C8D50EB * __this, bool ___value0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		bool L_1 = ___value0;
		EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 * L_2 = (EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 *)__this->get_kRJcljaQFQASapamFTwqLIpinzNN_0();
		((  void (*) (RuntimeObject*, bool, EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((RuntimeObject*)L_0, (bool)L_1, (EventFunction_2_t32F758F489DB646CAC012EE77FC7CCB77F2B6673 *)L_2, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return;
	}
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,System.Boolean>::GetHandlers(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void HierarchyEventHelper_2_GetHandlers_m87C62AA34372043B8E1D58CF9D8538744876E955_gshared (HierarchyEventHelper_2_t83507A10FBC53F7024CE976D2CCBF4918C8D50EB * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HierarchyEventHelper_2_GetHandlers_m87C62AA34372043B8E1D58CF9D8538744876E955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_00b1;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_1&(int32_t)4)))
		{
			goto IL_00b1;
		}
	}
	{
		int32_t L_2 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_2&(int32_t)8)))
		{
			goto IL_00b1;
		}
	}

IL_0027:
	{
		G_B4_0 = ((int32_t)1258466671);
	}

IL_002c:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B4_0^(int32_t)((int32_t)1258466669))))
		{
			case 0:
			{
				goto IL_00b1;
			}
			case 1:
			{
				goto IL_00dd;
			}
			case 2:
			{
				goto IL_008c;
			}
			case 3:
			{
				goto IL_0027;
			}
			case 4:
			{
				goto IL_0058;
			}
			case 5:
			{
				goto IL_0077;
			}
			case 6:
			{
				goto IL_00a6;
			}
		}
	}
	{
		goto IL_00dd;
	}

IL_0058:
	{
		CHECK_PAUSE_POINT;
		int32_t L_3 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_3&(int32_t)8)))
		{
			goto IL_00dd;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = ___transform0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_5 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		IL2CPP_RUNTIME_CLASS_INIT(UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var);
		((  int32_t (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_4, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_5, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		G_B4_0 = ((int32_t)1258466668);
		goto IL_002c;
	}

IL_0077:
	{
		CHECK_PAUSE_POINT;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = ___transform0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_7 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		IL2CPP_RUNTIME_CLASS_INIT(UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var);
		((  int32_t (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_6, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		G_B4_0 = ((int32_t)1258466665);
		goto IL_002c;
	}

IL_008c:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = ___transform0;
		NullCheck((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_8);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Transform_get_root_m101A8B5C2CC6D868B6B66EEDBD5336FC1EB5DDD6((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_8, /*hidden argument*/NULL);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_10 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		IL2CPP_RUNTIME_CLASS_INIT(UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var);
		((  int32_t (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_9, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_10, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		G_B4_0 = ((int32_t)1258466667);
		goto IL_002c;
	}

IL_00a6:
	{
		return;
	}
	// Dead block : IL_00a7: ldc.i4 1258466669

IL_00b1:
	{
		int32_t L_11 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_11&(int32_t)4)))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_12 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_12&(int32_t)1)))
		{
			goto IL_0077;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = ___transform0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_14 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		IL2CPP_RUNTIME_CLASS_INIT(UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var);
		((  int32_t (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_13, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_14, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		G_B4_0 = ((int32_t)1258466665);
		goto IL_002c;
	}

IL_00dd:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,System.Object>::.ctor(hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<THandler,TValue>)
extern "C" IL2CPP_METHOD_ATTR void HierarchyEventHelper_2__ctor_mB1381B3E5B33C497454B4AAF0A7FEA691C83248E_gshared (HierarchyEventHelper_2_t21ADBD5576AA92276647AD1353AA78AC39209E80 * __this, EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * ___executeDelegate0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * L_0 = ___executeDelegate0;
		NullCheck((HierarchyEventHelper_2_t21ADBD5576AA92276647AD1353AA78AC39209E80 *)__this);
		((  void (*) (HierarchyEventHelper_2_t21ADBD5576AA92276647AD1353AA78AC39209E80 *, EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((HierarchyEventHelper_2_t21ADBD5576AA92276647AD1353AA78AC39209E80 *)__this, (EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 *)L_0, (int32_t)5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return;
	}
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,System.Object>::.ctor(hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<THandler,TValue>,hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2_zEynGeOBdVgNxiYwtZLsacBtwtY<THandler,TValue>)
extern "C" IL2CPP_METHOD_ATTR void HierarchyEventHelper_2__ctor_m62B5AA7CB95B62E5E997455E57BEC60104C7706E_gshared (HierarchyEventHelper_2_t21ADBD5576AA92276647AD1353AA78AC39209E80 * __this, EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * ___executeDelegate0, int32_t ___executeOn1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HierarchyEventHelper_2__ctor_m62B5AA7CB95B62E5E997455E57BEC60104C7706E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * L_0 = ___executeDelegate0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral52934C29CA4A1A7572C6905BD6AB705A2C135973, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, HierarchyEventHelper_2__ctor_m62B5AA7CB95B62E5E997455E57BEC60104C7706E_RuntimeMethod_var);
	}

IL_0014:
	{
		EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * L_2 = ___executeDelegate0;
		__this->set_kRJcljaQFQASapamFTwqLIpinzNN_0(L_2);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_3 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1));
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		__this->set_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1(L_3);
		int32_t L_4 = ___executeOn1;
		__this->set_WwsFTLbfEnWIyENevELMTAgByIi_2(L_4);
		return;
	}
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,System.Object>::ExecuteOnAll(TValue)
extern "C" IL2CPP_METHOD_ATTR void HierarchyEventHelper_2_ExecuteOnAll_mE5CB02198C41D9B82C5647DAF560D273A5E94052_gshared (HierarchyEventHelper_2_t21ADBD5576AA92276647AD1353AA78AC39209E80 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		RuntimeObject * L_1 = ___value0;
		EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 * L_2 = (EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 *)__this->get_kRJcljaQFQASapamFTwqLIpinzNN_0();
		((  void (*) (RuntimeObject*, RuntimeObject *, EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((RuntimeObject*)L_0, (RuntimeObject *)L_1, (EventFunction_2_t9CFE6D2495C70F887DCE4964DEF64D50AEA5FF80 *)L_2, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return;
	}
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,System.Object>::GetHandlers(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void HierarchyEventHelper_2_GetHandlers_mB462B74FCE3EEAEB3AA2C18AD13CD2A6D7BAE054_gshared (HierarchyEventHelper_2_t21ADBD5576AA92276647AD1353AA78AC39209E80 * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HierarchyEventHelper_2_GetHandlers_mB462B74FCE3EEAEB3AA2C18AD13CD2A6D7BAE054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_00b1;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_1&(int32_t)4)))
		{
			goto IL_00b1;
		}
	}
	{
		int32_t L_2 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_2&(int32_t)8)))
		{
			goto IL_00b1;
		}
	}

IL_0027:
	{
		G_B4_0 = ((int32_t)1258466671);
	}

IL_002c:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B4_0^(int32_t)((int32_t)1258466669))))
		{
			case 0:
			{
				goto IL_00b1;
			}
			case 1:
			{
				goto IL_00dd;
			}
			case 2:
			{
				goto IL_008c;
			}
			case 3:
			{
				goto IL_0027;
			}
			case 4:
			{
				goto IL_0058;
			}
			case 5:
			{
				goto IL_0077;
			}
			case 6:
			{
				goto IL_00a6;
			}
		}
	}
	{
		goto IL_00dd;
	}

IL_0058:
	{
		CHECK_PAUSE_POINT;
		int32_t L_3 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_3&(int32_t)8)))
		{
			goto IL_00dd;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = ___transform0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_5 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		IL2CPP_RUNTIME_CLASS_INIT(UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var);
		((  int32_t (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_4, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_5, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		G_B4_0 = ((int32_t)1258466668);
		goto IL_002c;
	}

IL_0077:
	{
		CHECK_PAUSE_POINT;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = ___transform0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_7 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		IL2CPP_RUNTIME_CLASS_INIT(UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var);
		((  int32_t (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_6, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		G_B4_0 = ((int32_t)1258466665);
		goto IL_002c;
	}

IL_008c:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = ___transform0;
		NullCheck((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_8);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Transform_get_root_m101A8B5C2CC6D868B6B66EEDBD5336FC1EB5DDD6((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_8, /*hidden argument*/NULL);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_10 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		IL2CPP_RUNTIME_CLASS_INIT(UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var);
		((  int32_t (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_9, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_10, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		G_B4_0 = ((int32_t)1258466667);
		goto IL_002c;
	}

IL_00a6:
	{
		return;
	}
	// Dead block : IL_00a7: ldc.i4 1258466669

IL_00b1:
	{
		int32_t L_11 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_11&(int32_t)4)))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_12 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_12&(int32_t)1)))
		{
			goto IL_0077;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = ___transform0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_14 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		IL2CPP_RUNTIME_CLASS_INIT(UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var);
		((  int32_t (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_13, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_14, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		G_B4_0 = ((int32_t)1258466665);
		goto IL_002c;
	}

IL_00dd:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,UnityEngine.Vector2>::.ctor(hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<THandler,TValue>)
extern "C" IL2CPP_METHOD_ATTR void HierarchyEventHelper_2__ctor_m2F0D351136CFC0F610E29815741E8513A0660140_gshared (HierarchyEventHelper_2_t87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5 * __this, EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * ___executeDelegate0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * L_0 = ___executeDelegate0;
		NullCheck((HierarchyEventHelper_2_t87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5 *)__this);
		((  void (*) (HierarchyEventHelper_2_t87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5 *, EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((HierarchyEventHelper_2_t87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5 *)__this, (EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 *)L_0, (int32_t)5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return;
	}
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,UnityEngine.Vector2>::.ctor(hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<THandler,TValue>,hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2_zEynGeOBdVgNxiYwtZLsacBtwtY<THandler,TValue>)
extern "C" IL2CPP_METHOD_ATTR void HierarchyEventHelper_2__ctor_m8D0859A65CA8D3ADD3F30EA8AAD2E17BD58DC87C_gshared (HierarchyEventHelper_2_t87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5 * __this, EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * ___executeDelegate0, int32_t ___executeOn1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HierarchyEventHelper_2__ctor_m8D0859A65CA8D3ADD3F30EA8AAD2E17BD58DC87C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * L_0 = ___executeDelegate0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral52934C29CA4A1A7572C6905BD6AB705A2C135973, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, HierarchyEventHelper_2__ctor_m8D0859A65CA8D3ADD3F30EA8AAD2E17BD58DC87C_RuntimeMethod_var);
	}

IL_0014:
	{
		EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * L_2 = ___executeDelegate0;
		__this->set_kRJcljaQFQASapamFTwqLIpinzNN_0(L_2);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_3 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1));
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		__this->set_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1(L_3);
		int32_t L_4 = ___executeOn1;
		__this->set_WwsFTLbfEnWIyENevELMTAgByIi_2(L_4);
		return;
	}
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,UnityEngine.Vector2>::ExecuteOnAll(TValue)
extern "C" IL2CPP_METHOD_ATTR void HierarchyEventHelper_2_ExecuteOnAll_mD7296D9C074F5A2557958D39B7F9260E844DD3A5_gshared (HierarchyEventHelper_2_t87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = ___value0;
		EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 * L_2 = (EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 *)__this->get_kRJcljaQFQASapamFTwqLIpinzNN_0();
		((  void (*) (RuntimeObject*, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((RuntimeObject*)L_0, (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_1, (EventFunction_2_t3D7E9339A0E7C6C297FE0DDB8A6A1858AA1584A1 *)L_2, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return;
	}
}
// System.Void hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<System.Object,UnityEngine.Vector2>::GetHandlers(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void HierarchyEventHelper_2_GetHandlers_mCC5FAF87AB7009238B5D5BDD76458D229D975A00_gshared (HierarchyEventHelper_2_t87F6596C19C08D5BB25ADF9E3B3CBCCF5CF87DC5 * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HierarchyEventHelper_2_GetHandlers_mCC5FAF87AB7009238B5D5BDD76458D229D975A00_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_00b1;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_1&(int32_t)4)))
		{
			goto IL_00b1;
		}
	}
	{
		int32_t L_2 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_2&(int32_t)8)))
		{
			goto IL_00b1;
		}
	}

IL_0027:
	{
		G_B4_0 = ((int32_t)1258466671);
	}

IL_002c:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B4_0^(int32_t)((int32_t)1258466669))))
		{
			case 0:
			{
				goto IL_00b1;
			}
			case 1:
			{
				goto IL_00dd;
			}
			case 2:
			{
				goto IL_008c;
			}
			case 3:
			{
				goto IL_0027;
			}
			case 4:
			{
				goto IL_0058;
			}
			case 5:
			{
				goto IL_0077;
			}
			case 6:
			{
				goto IL_00a6;
			}
		}
	}
	{
		goto IL_00dd;
	}

IL_0058:
	{
		CHECK_PAUSE_POINT;
		int32_t L_3 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_3&(int32_t)8)))
		{
			goto IL_00dd;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = ___transform0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_5 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		IL2CPP_RUNTIME_CLASS_INIT(UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var);
		((  int32_t (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_4, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_5, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		G_B4_0 = ((int32_t)1258466668);
		goto IL_002c;
	}

IL_0077:
	{
		CHECK_PAUSE_POINT;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = ___transform0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_7 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		IL2CPP_RUNTIME_CLASS_INIT(UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var);
		((  int32_t (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_6, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		G_B4_0 = ((int32_t)1258466665);
		goto IL_002c;
	}

IL_008c:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = ___transform0;
		NullCheck((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_8);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Transform_get_root_m101A8B5C2CC6D868B6B66EEDBD5336FC1EB5DDD6((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_8, /*hidden argument*/NULL);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_10 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		IL2CPP_RUNTIME_CLASS_INIT(UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var);
		((  int32_t (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_9, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_10, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		G_B4_0 = ((int32_t)1258466667);
		goto IL_002c;
	}

IL_00a6:
	{
		return;
	}
	// Dead block : IL_00a7: ldc.i4 1258466669

IL_00b1:
	{
		int32_t L_11 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_11&(int32_t)4)))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_12 = (int32_t)__this->get_WwsFTLbfEnWIyENevELMTAgByIi_2();
		if (!((int32_t)((int32_t)L_12&(int32_t)1)))
		{
			goto IL_0077;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = ___transform0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_14 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_KQSHiaoVzKDlVjvdaHbJcDxdjAy_1();
		IL2CPP_RUNTIME_CLASS_INIT(UnityTools_t6854B0D1F5B4AD5CC66C40436BEAA1EF5FB9314E_il2cpp_TypeInfo_var);
		((  int32_t (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)L_13, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_14, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		G_B4_0 = ((int32_t)1258466665);
		goto IL_002c;
	}

IL_00dd:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean lVPGlCbkNvrVSnFFcfgwEyebHaly_iFUZfyBAetyFZkyXRrMrwIZFTkH<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool iFUZfyBAetyFZkyXRrMrwIZFTkH_MoveNext_m37F76A67A2E0AEAE73508FEDFF81C68B550408EF_gshared (iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B8_1 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B13_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0082;
			}
			case 1:
			{
				goto IL_00cf;
			}
		}
	}
	{
		goto IL_00f8;
	}

IL_001a:
	{
		G_B3_0 = ((int32_t)-712194135);
	}

IL_001f:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)-712194131))))
		{
			case 0:
			{
				goto IL_004b;
			}
			case 1:
			{
				goto IL_00f8;
			}
			case 2:
			{
				goto IL_0074;
			}
			case 3:
			{
				goto IL_001a;
			}
			case 4:
			{
				goto IL_0082;
			}
			case 5:
			{
				goto IL_00ee;
			}
			case 6:
			{
				goto IL_00aa;
			}
		}
	}
	{
		goto IL_00f8;
	}

IL_004b:
	{
		int32_t L_2 = (int32_t)__this->get_yLrOOaqYzmIyaXSCvApOKYnGotw_3();
		lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * L_3 = (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this->get_DKaFTWTMhFeLQHDExnDMRxZfmROL_2();
		NullCheck(L_3);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_4 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_3->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4);
		int32_t L_5 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		if ((((int32_t)L_2) < ((int32_t)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = (int32_t)((int32_t)-712194132);
		G_B8_0 = L_6;
		G_B8_1 = L_6;
		goto IL_0071;
	}

IL_006b:
	{
		int32_t L_7 = (int32_t)((int32_t)-712194133);
		G_B8_0 = L_7;
		G_B8_1 = L_7;
	}

IL_0071:
	{
		G_B3_0 = G_B8_1;
		goto IL_001f;
	}

IL_0074:
	{
		__this->set_yLrOOaqYzmIyaXSCvApOKYnGotw_3(0);
		G_B3_0 = ((int32_t)-712194136);
		goto IL_001f;
	}

IL_0082:
	{
		__this->set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1((-1));
		lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * L_8 = (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this->get_DKaFTWTMhFeLQHDExnDMRxZfmROL_2();
		NullCheck(L_8);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_9 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_8->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		if (L_9)
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_10 = (int32_t)((int32_t)-712194132);
		G_B13_0 = L_10;
		G_B13_1 = L_10;
		goto IL_00a4;
	}

IL_009e:
	{
		int32_t L_11 = (int32_t)((int32_t)-712194129);
		G_B13_0 = L_11;
		G_B13_1 = L_11;
	}

IL_00a4:
	{
		G_B3_0 = G_B13_1;
		goto IL_001f;
	}

IL_00aa:
	{
		lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * L_12 = (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this->get_DKaFTWTMhFeLQHDExnDMRxZfmROL_2();
		NullCheck(L_12);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_13 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_12->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_14 = (int32_t)__this->get_yLrOOaqYzmIyaXSCvApOKYnGotw_3();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_13);
		RuntimeObject * L_15 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_13, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		__this->set_NOArrsNdfKDShNFftfbPqYDzhpq_0(L_15);
		__this->set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(1);
		return (bool)1;
	}

IL_00cf:
	{
		__this->set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1((-1));
		int32_t L_16 = (int32_t)__this->get_yLrOOaqYzmIyaXSCvApOKYnGotw_3();
		__this->set_yLrOOaqYzmIyaXSCvApOKYnGotw_3(((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1)));
		G_B3_0 = ((int32_t)-712194131);
		goto IL_001f;
	}

IL_00ee:
	{
		G_B3_0 = ((int32_t)-712194131);
		goto IL_001f;
	}

IL_00f8:
	{
		return (bool)0;
	}
}
// T lVPGlCbkNvrVSnFFcfgwEyebHaly_iFUZfyBAetyFZkyXRrMrwIZFTkH<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * iFUZfyBAetyFZkyXRrMrwIZFTkH_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mF3E53E5193C4C2A75B1DCF88D0926540EB0B8743_gshared (iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_NOArrsNdfKDShNFftfbPqYDzhpq_0();
		return L_0;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly_iFUZfyBAetyFZkyXRrMrwIZFTkH<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" IL2CPP_METHOD_ATTR void iFUZfyBAetyFZkyXRrMrwIZFTkH_System_Collections_IEnumerator_Reset_m7329C4E00CED4146273EC24B4A2B35EA71C4E098_gshared (iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (iFUZfyBAetyFZkyXRrMrwIZFTkH_System_Collections_IEnumerator_Reset_m7329C4E00CED4146273EC24B4A2B35EA71C4E098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, iFUZfyBAetyFZkyXRrMrwIZFTkH_System_Collections_IEnumerator_Reset_m7329C4E00CED4146273EC24B4A2B35EA71C4E098_RuntimeMethod_var);
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly_iFUZfyBAetyFZkyXRrMrwIZFTkH<System.Object>::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void iFUZfyBAetyFZkyXRrMrwIZFTkH_System_IDisposable_Dispose_mCD8AD8E0E4E6F812BCB5891F290C7C0DC1F96C03_gshared (iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		return;
	}
}
// System.Object lVPGlCbkNvrVSnFFcfgwEyebHaly_iFUZfyBAetyFZkyXRrMrwIZFTkH<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * iFUZfyBAetyFZkyXRrMrwIZFTkH_System_Collections_IEnumerator_get_Current_mFF1210456415E9B616CA8598C0F638D0706E3845_gshared (iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_NOArrsNdfKDShNFftfbPqYDzhpq_0();
		return L_0;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly_iFUZfyBAetyFZkyXRrMrwIZFTkH<System.Object>::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void iFUZfyBAetyFZkyXRrMrwIZFTkH__ctor_m76B96E8955365212941F930492EB655F60BE6792_gshared (iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.IEnumerator`1<Rewired.ControllerMap> lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn<System.Object>::System.Collections.Generic.IEnumerable<Rewired.ControllerMap>.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* vwXNfeiXbIWciLlYtCJvcEfEAOn_System_Collections_Generic_IEnumerableU3CRewired_ControllerMapU3E_GetEnumerator_m422F868862411B5C14D685C52B09AF5A2F2B6AD2_gshared (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * __this, const RuntimeMethod* method)
{
	vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * V_0 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	{
		Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * L_0 = Thread_get_CurrentThread_mB7A83CAE2B9A74CEA053196DFD1AF1E7AB30A70E(/*hidden argument*/NULL);
		NullCheck((Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 *)L_0);
		int32_t L_1 = Thread_get_ManagedThreadId_m7FA85162CB00713B94EF5708B19120F791D3AAD1((Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 *)L_0, /*hidden argument*/NULL);
		int32_t L_2 = (int32_t)__this->get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0052;
		}
	}

IL_001c:
	{
		G_B3_0 = ((int32_t)1772642156);
	}

IL_0021:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)1772642157))))
		{
			case 0:
			{
				goto IL_0052;
			}
			case 1:
			{
				goto IL_0042;
			}
			case 2:
			{
				goto IL_0060;
			}
			case 3:
			{
				goto IL_0073;
			}
			case 4:
			{
				goto IL_001c;
			}
		}
	}
	{
		goto IL_0073;
	}

IL_0042:
	{
		__this->set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(0);
		V_0 = (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B *)__this;
		G_B3_0 = ((int32_t)1772642158);
		goto IL_0021;
	}

IL_0052:
	{
		vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * L_4 = (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_4, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B *)L_4;
		G_B3_0 = ((int32_t)1772642159);
		goto IL_0021;
	}

IL_0060:
	{
		vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * L_5 = V_0;
		lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * L_6 = (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this->get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3();
		NullCheck(L_5);
		L_5->set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(L_6);
		G_B3_0 = ((int32_t)1772642158);
		goto IL_0021;
	}

IL_0073:
	{
		vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * L_7 = V_0;
		int32_t L_8 = (int32_t)__this->get_pyCASHftKaJjucJBlmeeWQrdKuRk_5();
		NullCheck(L_7);
		L_7->set_OoFswlQbqBhHqGOCooflwYCYmYWF_4(L_8);
		vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.IEnumerator lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* vwXNfeiXbIWciLlYtCJvcEfEAOn_System_Collections_IEnumerable_GetEnumerator_m7AC62FF2F4D2290C43508F19500691A28683120C_gshared (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		NullCheck((vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		return L_0;
	}
}
// System.Boolean lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool vwXNfeiXbIWciLlYtCJvcEfEAOn_MoveNext_m8FB51981331E245C343BA404694A68C73F91CA62_gshared (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vwXNfeiXbIWciLlYtCJvcEfEAOn_MoveNext_m8FB51981331E245C343BA404694A68C73F91CA62_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B11_1 = 0;
	int32_t G_B15_0 = 0;
	int32_t G_B15_1 = 0;
	int32_t G_B20_0 = 0;
	int32_t G_B20_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_00c6;
			}
			case 1:
			{
				goto IL_0145;
			}
		}
	}
	{
		goto IL_0156;
	}

IL_001a:
	{
		G_B3_0 = ((int32_t)1406940904);
	}

IL_001f:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)1406940907))))
		{
			case 0:
			{
				goto IL_001a;
			}
			case 1:
			{
				goto IL_007e;
			}
			case 2:
			{
				goto IL_0143;
			}
			case 3:
			{
				goto IL_00c6;
			}
			case 4:
			{
				goto IL_0085;
			}
			case 5:
			{
				goto IL_009a;
			}
			case 6:
			{
				goto IL_0156;
			}
			case 7:
			{
				goto IL_0132;
			}
			case 8:
			{
				goto IL_005b;
			}
			case 9:
			{
				goto IL_00ea;
			}
			case 10:
			{
				goto IL_00fb;
			}
		}
	}
	{
		goto IL_0156;
	}

IL_005b:
	{
		lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * L_2 = (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this->get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3();
		NullCheck(L_2);
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_3 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_2->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		int32_t L_4 = (int32_t)__this->get_okMFupfyoBNZDyKMyvAtwGIUiAd_6();
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_3);
		ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * L_5 = List_1_get_Item_mADF483DC22F60B179F834797734DFFBAE6123F5A((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_3, (int32_t)L_4, /*hidden argument*/List_1_get_Item_mADF483DC22F60B179F834797734DFFBAE6123F5A_RuntimeMethod_var);
		__this->set_NOArrsNdfKDShNFftfbPqYDzhpq_0(L_5);
		G_B3_0 = ((int32_t)1406940898);
		goto IL_001f;
	}

IL_007e:
	{
		G_B3_0 = ((int32_t)1406940910);
		goto IL_001f;
	}

IL_0085:
	{
		int32_t L_6 = (int32_t)__this->get_okMFupfyoBNZDyKMyvAtwGIUiAd_6();
		__this->set_okMFupfyoBNZDyKMyvAtwGIUiAd_6(((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1)));
		G_B3_0 = ((int32_t)1406940910);
		goto IL_001f;
	}

IL_009a:
	{
		int32_t L_7 = (int32_t)__this->get_okMFupfyoBNZDyKMyvAtwGIUiAd_6();
		lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * L_8 = (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this->get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3();
		NullCheck(L_8);
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_9 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_8->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_9);
		int32_t L_10 = List_1_get_Count_mB87AFEFB187F15D3B2B41BA3C4919E74A8256840((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_9, /*hidden argument*/List_1_get_Count_mB87AFEFB187F15D3B2B41BA3C4919E74A8256840_RuntimeMethod_var);
		if ((((int32_t)L_7) < ((int32_t)L_10)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_11 = (int32_t)((int32_t)1406940909);
		G_B11_0 = L_11;
		G_B11_1 = L_11;
		goto IL_00c0;
	}

IL_00ba:
	{
		int32_t L_12 = (int32_t)((int32_t)1406940897);
		G_B11_0 = L_12;
		G_B11_1 = L_12;
	}

IL_00c0:
	{
		G_B3_0 = G_B11_1;
		goto IL_001f;
	}

IL_00c6:
	{
		__this->set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1((-1));
		int32_t L_13 = (int32_t)__this->get_OoFswlQbqBhHqGOCooflwYCYmYWF_4();
		if ((((int32_t)L_13) < ((int32_t)0)))
		{
			goto IL_00de;
		}
	}
	{
		int32_t L_14 = (int32_t)((int32_t)1406940908);
		G_B15_0 = L_14;
		G_B15_1 = L_14;
		goto IL_00e4;
	}

IL_00de:
	{
		int32_t L_15 = (int32_t)((int32_t)1406940909);
		G_B15_0 = L_15;
		G_B15_1 = L_15;
	}

IL_00e4:
	{
		G_B3_0 = G_B15_1;
		goto IL_001f;
	}

IL_00ea:
	{
		__this->set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(1);
		G_B3_0 = ((int32_t)1406940905);
		goto IL_001f;
	}

IL_00fb:
	{
		lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * L_16 = (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this->get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3();
		NullCheck(L_16);
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_17 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_16->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		int32_t L_18 = (int32_t)__this->get_okMFupfyoBNZDyKMyvAtwGIUiAd_6();
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_17);
		ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * L_19 = List_1_get_Item_mADF483DC22F60B179F834797734DFFBAE6123F5A((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_17, (int32_t)L_18, /*hidden argument*/List_1_get_Item_mADF483DC22F60B179F834797734DFFBAE6123F5A_RuntimeMethod_var);
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)L_19);
		int32_t L_20 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)L_19, /*hidden argument*/NULL);
		int32_t L_21 = (int32_t)__this->get_OoFswlQbqBhHqGOCooflwYCYmYWF_4();
		if ((!(((uint32_t)L_20) == ((uint32_t)L_21))))
		{
			goto IL_0126;
		}
	}
	{
		int32_t L_22 = (int32_t)((int32_t)1406940899);
		G_B20_0 = L_22;
		G_B20_1 = L_22;
		goto IL_012c;
	}

IL_0126:
	{
		int32_t L_23 = (int32_t)((int32_t)1406940911);
		G_B20_0 = L_23;
		G_B20_1 = L_23;
	}

IL_012c:
	{
		G_B3_0 = G_B20_1;
		goto IL_001f;
	}

IL_0132:
	{
		__this->set_okMFupfyoBNZDyKMyvAtwGIUiAd_6(0);
		G_B3_0 = ((int32_t)1406940906);
		goto IL_001f;
	}

IL_0143:
	{
		return (bool)1;
	}

IL_0145:
	{
		__this->set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1((-1));
		G_B3_0 = ((int32_t)1406940911);
		goto IL_001f;
	}

IL_0156:
	{
		return (bool)0;
	}
}
// Rewired.ControllerMap lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn<System.Object>::System.Collections.Generic.IEnumerator<Rewired.ControllerMap>.get_Current()
extern "C" IL2CPP_METHOD_ATTR ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * vwXNfeiXbIWciLlYtCJvcEfEAOn_System_Collections_Generic_IEnumeratorU3CRewired_ControllerMapU3E_get_Current_m3F515F00B7E86919E2992CB2B59FBCE70F546110_gshared (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * L_0 = (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)__this->get_NOArrsNdfKDShNFftfbPqYDzhpq_0();
		return L_0;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" IL2CPP_METHOD_ATTR void vwXNfeiXbIWciLlYtCJvcEfEAOn_System_Collections_IEnumerator_Reset_m817D855ED6F146D85D71168D06FD7B2263491D77_gshared (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vwXNfeiXbIWciLlYtCJvcEfEAOn_System_Collections_IEnumerator_Reset_m817D855ED6F146D85D71168D06FD7B2263491D77_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, vwXNfeiXbIWciLlYtCJvcEfEAOn_System_Collections_IEnumerator_Reset_m817D855ED6F146D85D71168D06FD7B2263491D77_RuntimeMethod_var);
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn<System.Object>::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void vwXNfeiXbIWciLlYtCJvcEfEAOn_System_IDisposable_Dispose_m19B77A9866E391E77A59548F23943459570F1A8D_gshared (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		return;
	}
}
// System.Object lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * vwXNfeiXbIWciLlYtCJvcEfEAOn_System_Collections_IEnumerator_get_Current_m5B0B4DE49EC93D326A4DA31623DFD47809D954FD_gshared (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * L_0 = (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)__this->get_NOArrsNdfKDShNFftfbPqYDzhpq_0();
		return L_0;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly_vwXNfeiXbIWciLlYtCJvcEfEAOn<System.Object>::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void vwXNfeiXbIWciLlYtCJvcEfEAOn__ctor_mC3FBEEAA0E5970C899C4B00D6F9C6AB15A5915AB_gshared (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(L_0);
		Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * L_1 = Thread_get_CurrentThread_mB7A83CAE2B9A74CEA053196DFD1AF1E7AB30A70E(/*hidden argument*/NULL);
		NullCheck((Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 *)L_1);
		int32_t L_2 = Thread_get_ManagedThreadId_m7FA85162CB00713B94EF5708B19120F791D3AAD1((Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 *)L_1, /*hidden argument*/NULL);
		__this->set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Rewired.ControllerMap lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Rewired.IControllerMapSet.get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_get_Item_m913ED211EB3D2D0DD0CC4A40E72054B8FC31E573_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_get_Item_m913ED211EB3D2D0DD0CC4A40E72054B8FC31E573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_0 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_0);
		ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * L_2 = List_1_get_Item_mADF483DC22F60B179F834797734DFFBAE6123F5A((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_0, (int32_t)L_1, /*hidden argument*/List_1_get_Item_mADF483DC22F60B179F834797734DFFBAE6123F5A_RuntimeMethod_var);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<Rewired.ControllerMap> lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Rewired.IControllerMapSet.get_Maps()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_get_Maps_m2B095415769A48C2AF0EDC6487C51629CC4B273B_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_dSzXwUUoLuinZYUADiNfnihdPrl_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerable`1<Rewired.ControllerMap> lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Rewired.IControllerMapSet.IterateMapsInCategory_ControllerMap(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_IterateMapsInCategory_ControllerMap_mD94027A98CB062C659095F18EBBA0F6E6D63E468_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, const RuntimeMethod* method)
{
	vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * V_0 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * L_0 = (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, (int32_t)((int32_t)-2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B *)L_0;
	}

IL_0008:
	{
		G_B2_0 = ((int32_t)66815339);
	}

IL_000d:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)66815337))))
		{
			case 0:
			{
				goto IL_0008;
			}
			case 1:
			{
				goto IL_0034;
			}
			case 2:
			{
				goto IL_0026;
			}
		}
	}
	{
		goto IL_0034;
	}

IL_0026:
	{
		vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(__this);
		G_B2_0 = ((int32_t)66815336);
		goto IL_000d;
	}

IL_0034:
	{
		vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * L_2 = V_0;
		int32_t L_3 = p0;
		NullCheck(L_2);
		L_2->set_pyCASHftKaJjucJBlmeeWQrdKuRk_5(L_3);
		vwXNfeiXbIWciLlYtCJvcEfEAOn_tBEECF4B9ABA0D88E321283B1EACD5B82F70B9A9B * L_4 = V_0;
		return L_4;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Rewired.IControllerMapSet.Add(Rewired.ControllerMap,Rewired.BoolOption)
extern "C" IL2CPP_METHOD_ATTR void lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_Add_m534D416B6B076B10E61DB1BD8261628AA0B0B67C_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * p0, int32_t p1, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * L_0 = p0;
		int32_t L_1 = p1;
		NullCheck((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this);
		((  void (*) (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Rewired.IControllerMapSet.Remove(Rewired.ControllerMap)
extern "C" IL2CPP_METHOD_ATTR void lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_Remove_m3671C66FC88EE1121F2114244B115D8D18309A0E_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * p0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * L_0 = p0;
		NullCheck((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this);
		((  void (*) (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// Rewired.ControllerMap lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Rewired.IControllerMapSet.GetMap(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMap_m62AC62F4118F0A886CA60175D4F9968221AEC7E6_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int32_t L_0 = p0;
		NullCheck((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this);
		RuntimeObject * L_1 = ((  RuntimeObject * (*) (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		return (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)L_1;
	}
}
// Rewired.ControllerMap lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Rewired.IControllerMapSet.GetMap(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMap_m05577E04A93FC7C3089DB1D2F46F91A678BCD6C5_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int32_t L_0 = p0;
		int32_t L_1 = p1;
		NullCheck((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		return (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)L_2;
	}
}
// Rewired.ControllerMap lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Rewired.IControllerMapSet.GetMapByCategory(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMapByCategory_mEADACDCF64BA80510478C61EA5D88963D4F42BD1_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int32_t L_0 = p0;
		NullCheck((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this);
		RuntimeObject * L_1 = ((  RuntimeObject * (*) (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		return (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)L_1;
	}
}
// Rewired.ControllerMap[] lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Rewired.IControllerMapSet.GetMaps()
extern "C" IL2CPP_METHOD_ATTR ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E* lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMaps_m009C48D29322842D64BD61AD8B68EE35D8A5A21C_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMaps_m009C48D29322842D64BD61AD8B68EE35D8A5A21C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_0 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_0);
		ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E* L_1 = List_1_ToArray_m57FD5A381EAD73CDC168FFEC93C5D22EEA7CCC7C((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_0, /*hidden argument*/List_1_ToArray_m57FD5A381EAD73CDC168FFEC93C5D22EEA7CCC7C_RuntimeMethod_var);
		return L_1;
	}
}
// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Rewired.IControllerMapSet.GetMaps(System.Collections.Generic.List`1<Rewired.ControllerMap>,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR int32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMaps_m44238BB3AF8633555345CEFB166746EC309E4A76_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * p0, bool p1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMaps_m44238BB3AF8633555345CEFB166746EC309E4A76_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CHECK_PAUSE_POINT;
	int32_t G_B4_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B13_1 = 0;
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_0 = p0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return 0;
	}

IL_0005:
	{
		V_0 = (int32_t)0;
		bool L_1 = p1;
		if (L_1)
		{
			goto IL_0067;
		}
	}

IL_000a:
	{
		G_B4_0 = ((int32_t)448132368);
	}

IL_000f:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B4_0^(int32_t)((int32_t)448132373))))
		{
			case 0:
			{
				goto IL_000a;
			}
			case 1:
			{
				goto IL_0067;
			}
			case 2:
			{
				goto IL_0075;
			}
			case 3:
			{
				goto IL_0045;
			}
			case 4:
			{
				goto IL_007e;
			}
			case 5:
			{
				goto IL_0038;
			}
			case 6:
			{
				goto IL_00a0;
			}
		}
	}
	{
		goto IL_00a0;
	}

IL_0038:
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_2 = p0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_2);
		List_1_Clear_m88D5862BAF30335E7FEA486544F28FA5CECFF6DE((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_2, /*hidden argument*/List_1_Clear_m88D5862BAF30335E7FEA486544F28FA5CECFF6DE_RuntimeMethod_var);
		G_B4_0 = ((int32_t)448132375);
		goto IL_000f;
	}

IL_0045:
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_3 = p0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_4 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_5 = V_1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4);
		RuntimeObject * L_6 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_3);
		List_1_Add_mBB024C6A0C23217B87FABE55861D2532378BEE1B((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_3, (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)L_6, /*hidden argument*/List_1_Add_mBB024C6A0C23217B87FABE55861D2532378BEE1B_RuntimeMethod_var);
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
		G_B4_0 = ((int32_t)448132369);
		goto IL_000f;
	}

IL_0067:
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_8 = p0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_8);
		int32_t L_9 = List_1_get_Count_mB87AFEFB187F15D3B2B41BA3C4919E74A8256840((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_8, /*hidden argument*/List_1_get_Count_mB87AFEFB187F15D3B2B41BA3C4919E74A8256840_RuntimeMethod_var);
		V_0 = (int32_t)L_9;
		G_B4_0 = ((int32_t)448132375);
		goto IL_000f;
	}

IL_0075:
	{
		V_1 = (int32_t)0;
		G_B4_0 = ((int32_t)448132369);
		goto IL_000f;
	}

IL_007e:
	{
		int32_t L_10 = V_1;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_11 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_11);
		int32_t L_12 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0094;
		}
	}
	{
		int32_t L_13 = (int32_t)((int32_t)448132371);
		G_B13_0 = L_13;
		G_B13_1 = L_13;
		goto IL_009a;
	}

IL_0094:
	{
		int32_t L_14 = (int32_t)((int32_t)448132374);
		G_B13_0 = L_14;
		G_B13_1 = L_14;
	}

IL_009a:
	{
		G_B4_0 = G_B13_1;
		goto IL_000f;
	}

IL_00a0:
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_15 = p0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_15);
		int32_t L_16 = List_1_get_Count_mB87AFEFB187F15D3B2B41BA3C4919E74A8256840((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_15, /*hidden argument*/List_1_get_Count_mB87AFEFB187F15D3B2B41BA3C4919E74A8256840_RuntimeMethod_var);
		int32_t L_17 = V_0;
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)L_17));
	}
}
// Rewired.ControllerMap[] lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Rewired.IControllerMapSet.GetMapsByCategory(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E* lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMapsByCategory_m8EB2F2A29929732745327E724492945704260FF0_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMapsByCategory_m8EB2F2A29929732745327E724492945704260FF0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B8_1 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B12_1 = 0;
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_0 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)il2cpp_codegen_object_new(List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E_il2cpp_TypeInfo_var);
		List_1__ctor_m0839F8110366CBDBC753D441E962E51A20E93E14(L_0, /*hidden argument*/List_1__ctor_m0839F8110366CBDBC753D441E962E51A20E93E14_RuntimeMethod_var);
		V_0 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_0;
		V_1 = (int32_t)0;
		goto IL_0067;
	}

IL_000a:
	{
		G_B2_0 = ((int32_t)686803366);
	}

IL_000f:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)686803362))))
		{
			case 0:
			{
				goto IL_0046;
			}
			case 1:
			{
				goto IL_00be;
			}
			case 2:
			{
				goto IL_000a;
			}
			case 3:
			{
				goto IL_0067;
			}
			case 4:
			{
				goto IL_00a7;
			}
			case 5:
			{
				goto IL_0086;
			}
			case 6:
			{
				goto IL_003b;
			}
		}
	}
	{
		goto IL_00be;
	}

IL_003b:
	{
		int32_t L_1 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)1));
		G_B2_0 = ((int32_t)686803361);
		goto IL_000f;
	}

IL_0046:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2));
		int32_t L_2 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2), /*hidden argument*/NULL);
		int32_t L_3 = p0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_4 = (int32_t)((int32_t)686803367);
		G_B8_0 = L_4;
		G_B8_1 = L_4;
		goto IL_0064;
	}

IL_005e:
	{
		int32_t L_5 = (int32_t)((int32_t)686803364);
		G_B8_0 = L_5;
		G_B8_1 = L_5;
	}

IL_0064:
	{
		G_B2_0 = G_B8_1;
		goto IL_000f;
	}

IL_0067:
	{
		int32_t L_6 = V_1;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_7 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7);
		int32_t L_8 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_9 = (int32_t)((int32_t)686803363);
		G_B12_0 = L_9;
		G_B12_1 = L_9;
		goto IL_0083;
	}

IL_007d:
	{
		int32_t L_10 = (int32_t)((int32_t)686803366);
		G_B12_0 = L_10;
		G_B12_1 = L_10;
	}

IL_0083:
	{
		G_B2_0 = G_B12_1;
		goto IL_000f;
	}

IL_0086:
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_11 = V_0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_12 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_13 = V_1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_12);
		RuntimeObject * L_14 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_12, (int32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_11);
		List_1_Add_mBB024C6A0C23217B87FABE55861D2532378BEE1B((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_11, (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)L_14, /*hidden argument*/List_1_Add_mBB024C6A0C23217B87FABE55861D2532378BEE1B_RuntimeMethod_var);
		G_B2_0 = ((int32_t)686803364);
		goto IL_000f;
	}

IL_00a7:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_15 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_16 = V_1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_15);
		RuntimeObject * L_17 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_2 = (RuntimeObject *)L_17;
		G_B2_0 = ((int32_t)686803362);
		goto IL_000f;
	}

IL_00be:
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_18 = V_0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_18);
		ControllerMapU5BU5D_t92A7A5230A71270E349387005F3086641260671E* L_19 = List_1_ToArray_m57FD5A381EAD73CDC168FFEC93C5D22EEA7CCC7C((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_18, /*hidden argument*/List_1_ToArray_m57FD5A381EAD73CDC168FFEC93C5D22EEA7CCC7C_RuntimeMethod_var);
		return L_19;
	}
}
// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Rewired.IControllerMapSet.GetMapsByCategory(System.Int32,System.Collections.Generic.List`1<Rewired.ControllerMap>,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR int32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMapsByCategory_m0716D053D759102191A5B66E93B52FD44750A4C8_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * p1, bool p2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_Rewired_IControllerMapSet_GetMapsByCategory_m0716D053D759102191A5B66E93B52FD44750A4C8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B4_0 = 0;
	int32_t G_B14_0 = 0;
	int32_t G_B14_1 = 0;
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_0 = p1;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return 0;
	}

IL_0005:
	{
		V_0 = (int32_t)0;
		bool L_1 = p2;
		if (L_1)
		{
			goto IL_004c;
		}
	}

IL_000a:
	{
		G_B4_0 = ((int32_t)141273058);
	}

IL_000f:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B4_0^(int32_t)((int32_t)141273059))))
		{
			case 0:
			{
				goto IL_00d4;
			}
			case 1:
			{
				goto IL_003f;
			}
			case 2:
			{
				goto IL_004c;
			}
			case 3:
			{
				goto IL_005a;
			}
			case 4:
			{
				goto IL_00a4;
			}
			case 5:
			{
				goto IL_00c6;
			}
			case 6:
			{
				goto IL_0098;
			}
			case 7:
			{
				goto IL_000a;
			}
		}
	}
	{
		goto IL_00d4;
	}

IL_003f:
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_2 = p1;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_2);
		List_1_Clear_m88D5862BAF30335E7FEA486544F28FA5CECFF6DE((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_2, /*hidden argument*/List_1_Clear_m88D5862BAF30335E7FEA486544F28FA5CECFF6DE_RuntimeMethod_var);
		G_B4_0 = ((int32_t)141273061);
		goto IL_000f;
	}

IL_004c:
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_3 = p1;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_3);
		int32_t L_4 = List_1_get_Count_mB87AFEFB187F15D3B2B41BA3C4919E74A8256840((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_3, /*hidden argument*/List_1_get_Count_mB87AFEFB187F15D3B2B41BA3C4919E74A8256840_RuntimeMethod_var);
		V_0 = (int32_t)L_4;
		G_B4_0 = ((int32_t)141273061);
		goto IL_000f;
	}

IL_005a:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_5 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_6 = V_1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_5);
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_2 = (RuntimeObject *)L_7;
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2));
		int32_t L_8 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2), /*hidden argument*/NULL);
		int32_t L_9 = p0;
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_10 = p1;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_11 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_12 = V_1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_10);
		List_1_Add_mBB024C6A0C23217B87FABE55861D2532378BEE1B((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_10, (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)L_13, /*hidden argument*/List_1_Add_mBB024C6A0C23217B87FABE55861D2532378BEE1B_RuntimeMethod_var);
		G_B4_0 = ((int32_t)141273062);
		goto IL_000f;
	}

IL_0098:
	{
		V_1 = (int32_t)0;
		G_B4_0 = ((int32_t)141273063);
		goto IL_000f;
	}

IL_00a4:
	{
		int32_t L_14 = V_1;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_15 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_15);
		int32_t L_16 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_17 = (int32_t)((int32_t)141273059);
		G_B14_0 = L_17;
		G_B14_1 = L_17;
		goto IL_00c0;
	}

IL_00ba:
	{
		int32_t L_18 = (int32_t)((int32_t)141273056);
		G_B14_0 = L_18;
		G_B14_1 = L_18;
	}

IL_00c0:
	{
		G_B4_0 = G_B14_1;
		goto IL_000f;
	}

IL_00c6:
	{
		int32_t L_19 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
		G_B4_0 = ((int32_t)141273063);
		goto IL_000f;
	}

IL_00d4:
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_20 = p1;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_20);
		int32_t L_21 = List_1_get_Count_mB87AFEFB187F15D3B2B41BA3C4919E74A8256840((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_20, /*hidden argument*/List_1_get_Count_mB87AFEFB187F15D3B2B41BA3C4919E74A8256840_RuntimeMethod_var);
		int32_t L_22 = V_0;
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_21, (int32_t)L_22));
	}
}
// System.Collections.Generic.IList`1<T> lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::get_Maps()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* lVPGlCbkNvrVSnFFcfgwEyebHaly_get_Maps_m2261CFEE3C4D13BE4E954D5B02BBCB88B88E41E1_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_RQnbcoHDSILswMsVwUCuGnbyXvLt_5();
		return L_0;
	}
}
// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_get_Count_m6FCA57F56FB01220C11284200077AC2F2B2130AD_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_1 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_1);
		int32_t L_2 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		return L_2;
	}
}
// T lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * lVPGlCbkNvrVSnFFcfgwEyebHaly_get_Item_m55563F83629472ACC65164C07E85545F0A858680_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return L_2;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::set_Item(System.Int32,T)
extern "C" IL2CPP_METHOD_ATTR void lVPGlCbkNvrVSnFFcfgwEyebHaly_set_Item_mE7609C821890FAE4792AFB1E0301373AF644510F_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_set_Item_mE7609C821890FAE4792AFB1E0301373AF644510F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_1 = ___index0;
		RuntimeObject * L_2 = ___value1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, (int32_t)L_1, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_3 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		int32_t L_4 = ___index0;
		RuntimeObject * L_5 = ___value1;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_3);
		List_1_set_Item_m86D714200344F5D83977C34422287C90AC3DFD80((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_3, (int32_t)L_4, (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)L_5, /*hidden argument*/List_1_set_Item_m86D714200344F5D83977C34422287C90AC3DFD80_RuntimeMethod_var);
		return;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void lVPGlCbkNvrVSnFFcfgwEyebHaly__ctor_mA441F7FE7C3019A0B8D567C19BF2AD8E37C94918_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t ___controllerId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly__ctor_mA441F7FE7C3019A0B8D567C19BF2AD8E37C94918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 11));
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12));
		__this->set_HXrZyhBzDfszTlMEUEDAshPIFVI_4(L_0);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_1 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		ReadOnlyCollection_1_t5D996E967221C71E4EC5CC11210C3076432D5A50 * L_2 = (ReadOnlyCollection_1_t5D996E967221C71E4EC5CC11210C3076432D5A50 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 13));
		((  void (*) (ReadOnlyCollection_1_t5D996E967221C71E4EC5CC11210C3076432D5A50 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)(L_2, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		__this->set_RQnbcoHDSILswMsVwUCuGnbyXvLt_5(L_2);
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_3 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->klass->rgctx_data, 15)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_3, /*hidden argument*/NULL);
		int32_t L_5 = SHhhwmrrEBdklqDsPHHCPGltOQH_hUOXtxZFfwOmheZLijxYDcjuVpA_mB9A1DAD8C42C443BDCB687799A0AC29CFA9AF85D((Type_t *)L_4, /*hidden argument*/NULL);
		__this->set_jeLkzDDlxjdyTfidRDslEjdPyAn_2(L_5);
		int32_t L_6 = ___controllerId0;
		__this->set_xJPYTeJZICkFpIWTwbMsXQVEALx_3(L_6);
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_7 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)il2cpp_codegen_object_new(List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E_il2cpp_TypeInfo_var);
		List_1__ctor_m0839F8110366CBDBC753D441E962E51A20E93E14(L_7, /*hidden argument*/List_1__ctor_m0839F8110366CBDBC753D441E962E51A20E93E14_RuntimeMethod_var);
		__this->set_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0(L_7);
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_8 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		ReadOnlyCollection_1_t42C6EC4956F81037B0696DDF4310EEAD899406F9 * L_9 = (ReadOnlyCollection_1_t42C6EC4956F81037B0696DDF4310EEAD899406F9 *)il2cpp_codegen_object_new(ReadOnlyCollection_1_t42C6EC4956F81037B0696DDF4310EEAD899406F9_il2cpp_TypeInfo_var);
		ReadOnlyCollection_1__ctor_m43F319804F61EAF5B1BA3BEE99D5EC8F37B473DA(L_9, (RuntimeObject*)L_8, /*hidden argument*/ReadOnlyCollection_1__ctor_m43F319804F61EAF5B1BA3BEE99D5EC8F37B473DA_RuntimeMethod_var);
		__this->set_dSzXwUUoLuinZYUADiNfnihdPrl_1(L_9);
		return;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::HfTIrZGXLVOHtJVamKOSesGBJhAd(T,Rewired.BoolOption)
extern "C" IL2CPP_METHOD_ATTR void lVPGlCbkNvrVSnFFcfgwEyebHaly_HfTIrZGXLVOHtJVamKOSesGBJhAd_m587BDF7DB8BE120243C019DB388AC44085CAB584_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_HfTIrZGXLVOHtJVamKOSesGBJhAd_m587BDF7DB8BE120243C019DB388AC44085CAB584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(p0));
		int32_t L_0 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(p0), /*hidden argument*/NULL);
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(p0));
		int32_t L_1 = ControllerMap_get_layoutId_m1B90CB02DDE635CA339BA98082FD4F9480DF0EF7((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(p0), /*hidden argument*/NULL);
		NullCheck((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this);
		int32_t L_2 = ((  int32_t (*) (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16)->methodPointer)((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16));
		V_0 = (int32_t)L_2;
		int32_t L_3 = p1;
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}

IL_0025:
	{
		G_B2_0 = ((int32_t)1676394157);
	}

IL_002a:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)1676394156))))
		{
			case 0:
			{
				goto IL_006b;
			}
			case 1:
			{
				goto IL_0056;
			}
			case 2:
			{
				goto IL_0025;
			}
			case 3:
			{
				goto IL_00c4;
			}
			case 4:
			{
				goto IL_00ae;
			}
			case 5:
			{
				goto IL_0084;
			}
			case 6:
			{
				goto IL_00fc;
			}
		}
	}
	{
		goto IL_00fc;
	}

IL_0056:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(p0));
		ControllerMap_set_enabled_m4A347FEEC7D6FF51515C88121C4F2A0437B472A8((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(p0), (bool)1, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)1676394159);
		goto IL_002a;
	}

IL_006b:
	{
		int32_t L_4 = p1;
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_00c4;
		}
	}
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(p0));
		ControllerMap_set_enabled_m4A347FEEC7D6FF51515C88121C4F2A0437B472A8((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(p0), (bool)0, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)1676394159);
		goto IL_002a;
	}

IL_0084:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_5 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_6 = V_0;
		RuntimeObject * L_7 = p0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_5);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_5, (int32_t)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_8 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		int32_t L_9 = V_0;
		RuntimeObject * L_10 = p0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_8);
		List_1_set_Item_m86D714200344F5D83977C34422287C90AC3DFD80((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_8, (int32_t)L_9, (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)L_10, /*hidden argument*/List_1_set_Item_m86D714200344F5D83977C34422287C90AC3DFD80_RuntimeMethod_var);
		return;
	}
	// Dead block : IL_00a4: ldc.i4 1676394152

IL_00ae:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_11 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		RuntimeObject * L_12 = p0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_11);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17));
		G_B2_0 = ((int32_t)1676394154);
		goto IL_002a;
	}

IL_00c4:
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) < ((int32_t)0)))
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_14 = p1;
		if (L_14)
		{
			goto IL_0084;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_15 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_16 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_15);
		RuntimeObject * L_17 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_1 = (RuntimeObject *)L_17;
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_1));
		bool L_18 = ControllerMap_get_enabled_m454281A8B80828B120C4F7EDF2BC52028AD35C6F((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_1), /*hidden argument*/NULL);
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(p0));
		ControllerMap_set_enabled_m4A347FEEC7D6FF51515C88121C4F2A0437B472A8((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(p0), (bool)L_18, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)1676394153);
		goto IL_002a;
	}

IL_00fc:
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_19 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		RuntimeObject * L_20 = p0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_19);
		List_1_Add_mBB024C6A0C23217B87FABE55861D2532378BEE1B((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_19, (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)L_20, /*hidden argument*/List_1_Add_mBB024C6A0C23217B87FABE55861D2532378BEE1B_RuntimeMethod_var);
		return;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Remove(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void lVPGlCbkNvrVSnFFcfgwEyebHaly_Remove_m9A9E565055D1562B04563FC758F5E360530F24A7_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_Remove_m9A9E565055D1562B04563FC758F5E360530F24A7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = p0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_1 = p0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_2 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2);
		int32_t L_3 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_0038;
		}
	}

IL_0012:
	{
		G_B3_0 = ((int32_t)-1371990552);
	}

IL_0017:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)-1371990551))))
		{
			case 0:
			{
				goto IL_0038;
			}
			case 1:
			{
				goto IL_0030;
			}
			case 2:
			{
				goto IL_0012;
			}
		}
	}
	{
		goto IL_0038;
	}

IL_0030:
	{
		return;
	}
	// Dead block : IL_0031: ldc.i4 -1371990551

IL_0038:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_4 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_5 = p0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_6 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		int32_t L_7 = p0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_6);
		List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_6, (int32_t)L_7, /*hidden argument*/List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5_RuntimeMethod_var);
		return;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::yhpcGPaRrkCsNfCHablLblmduzKL(T)
extern "C" IL2CPP_METHOD_ATTR void lVPGlCbkNvrVSnFFcfgwEyebHaly_yhpcGPaRrkCsNfCHablLblmduzKL_m18EEC4A91A776E5BF91177E6E25CB9860B71456D_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, RuntimeObject * p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_yhpcGPaRrkCsNfCHablLblmduzKL_m18EEC4A91A776E5BF91177E6E25CB9860B71456D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CHECK_PAUSE_POINT;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		RuntimeObject * L_1 = p0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		((  bool (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19));
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_2 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		RuntimeObject * L_3 = p0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_2);
		List_1_Remove_m89A219C1A47D8F7CEDA4FAD3FD848DBCC009D28C((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_2, (ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)L_3, /*hidden argument*/List_1_Remove_m89A219C1A47D8F7CEDA4FAD3FD848DBCC009D28C_RuntimeMethod_var);
		return;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Remove(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void lVPGlCbkNvrVSnFFcfgwEyebHaly_Remove_mAFD5E3D961FAA25E86F20AA6DEF6402EEFDBFDB0_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_Remove_mAFD5E3D961FAA25E86F20AA6DEF6402EEFDBFDB0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	int32_t G_B16_0 = 0;
	int32_t G_B16_1 = 0;
	{
		int32_t L_0 = p0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_1 = p1;
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_00a7;
		}
	}

IL_000b:
	{
		G_B3_0 = ((int32_t)1797494600);
	}

IL_0010:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)1797494602))))
		{
			case 0:
			{
				goto IL_00bf;
			}
			case 1:
			{
				goto IL_00f0;
			}
			case 2:
			{
				goto IL_0044;
			}
			case 3:
			{
				goto IL_0067;
			}
			case 4:
			{
				goto IL_004c;
			}
			case 5:
			{
				goto IL_000b;
			}
			case 6:
			{
				goto IL_00a7;
			}
			case 7:
			{
				goto IL_0060;
			}
			case 8:
			{
				goto IL_0099;
			}
		}
	}
	{
		goto IL_00f0;
	}

IL_0044:
	{
		return;
	}
	// Dead block : IL_0045: ldc.i4 1797494604

IL_004c:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_2 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_3 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2);
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_2 = (RuntimeObject *)L_4;
		G_B3_0 = ((int32_t)1797494601);
		goto IL_0010;
	}

IL_0060:
	{
		G_B3_0 = ((int32_t)1797494603);
		goto IL_0010;
	}

IL_0067:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2));
		int32_t L_5 = ControllerMap_get_layoutId_m1B90CB02DDE635CA339BA98082FD4F9480DF0EF7((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2), /*hidden argument*/NULL);
		int32_t L_6 = p1;
		if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
		{
			goto IL_0099;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_7 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_8 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_9 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		int32_t L_10 = V_0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_9);
		List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_9, (int32_t)L_10, /*hidden argument*/List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5_RuntimeMethod_var);
		G_B3_0 = ((int32_t)1797494594);
		goto IL_0010;
	}

IL_0099:
	{
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)1));
		G_B3_0 = ((int32_t)1797494603);
		goto IL_0010;
	}

IL_00a7:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_12 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_12);
		int32_t L_13 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)1));
		G_B3_0 = ((int32_t)1797494605);
		goto IL_0010;
	}

IL_00bf:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_14 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_15 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_14);
		RuntimeObject * L_16 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_14, (int32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_1 = (RuntimeObject *)L_16;
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_1));
		int32_t L_17 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_1), /*hidden argument*/NULL);
		int32_t L_18 = p0;
		if ((!(((uint32_t)L_17) == ((uint32_t)L_18))))
		{
			goto IL_00e4;
		}
	}
	{
		int32_t L_19 = (int32_t)((int32_t)1797494606);
		G_B16_0 = L_19;
		G_B16_1 = L_19;
		goto IL_00ea;
	}

IL_00e4:
	{
		int32_t L_20 = (int32_t)((int32_t)1797494594);
		G_B16_0 = L_20;
		G_B16_1 = L_20;
	}

IL_00ea:
	{
		G_B3_0 = G_B16_1;
		goto IL_0010;
	}

IL_00f0:
	{
		int32_t L_21 = V_0;
		if ((((int32_t)L_21) >= ((int32_t)0)))
		{
			goto IL_00bf;
		}
	}
	{
		return;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::RemoveById(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void lVPGlCbkNvrVSnFFcfgwEyebHaly_RemoveById_m4AB157EE1AAACECF4CE3F2E7B04AEEF16172A787_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_RemoveById_m4AB157EE1AAACECF4CE3F2E7B04AEEF16172A787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B7_1 = 0;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
		goto IL_0036;
	}

IL_0010:
	{
		G_B2_0 = ((int32_t)1547707813);
	}

IL_0015:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)1547707812))))
		{
			case 0:
			{
				goto IL_0010;
			}
			case 1:
			{
				goto IL_0056;
			}
			case 2:
			{
				goto IL_0092;
			}
			case 3:
			{
				goto IL_0036;
			}
			case 4:
			{
				goto IL_004b;
			}
		}
	}
	{
		goto IL_0092;
	}

IL_0036:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_3 = (int32_t)((int32_t)1547707813);
		G_B7_0 = L_3;
		G_B7_1 = L_3;
		goto IL_0048;
	}

IL_0042:
	{
		int32_t L_4 = (int32_t)((int32_t)1547707814);
		G_B7_0 = L_4;
		G_B7_1 = L_4;
	}

IL_0048:
	{
		G_B2_0 = G_B7_1;
		goto IL_0015;
	}

IL_004b:
	{
		CHECK_PAUSE_POINT;
		int32_t L_5 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)1));
		G_B2_0 = ((int32_t)1547707815);
		goto IL_0015;
	}

IL_0056:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_6 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_7 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_6);
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_1 = (RuntimeObject *)L_8;
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_1));
		int32_t L_9 = ControllerMap_get_id_m6FA8319C62C718FE081C3D490348B1235840E336((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_1), /*hidden argument*/NULL);
		int32_t L_10 = p0;
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_004b;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_11 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_12 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_11);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_13 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		int32_t L_14 = V_0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_13);
		List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_13, (int32_t)L_14, /*hidden argument*/List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5_RuntimeMethod_var);
		G_B2_0 = ((int32_t)1547707808);
		goto IL_0015;
	}

IL_0092:
	{
		return;
	}
}
// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::IndexOf(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_IndexOf_m1F5C0FF549EE4D9768153E071F276224B4796FF3_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		V_0 = (int32_t)0;
	}

IL_0002:
	{
		G_B2_0 = ((int32_t)-658447088);
	}

IL_0007:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-658447087))))
		{
			case 0:
			{
				goto IL_0045;
			}
			case 1:
			{
				goto IL_0059;
			}
			case 2:
			{
				goto IL_0028;
			}
			case 3:
			{
				goto IL_0060;
			}
			case 4:
			{
				goto IL_0002;
			}
		}
	}
	{
		goto IL_0060;
	}

IL_0028:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_1));
		int32_t L_0 = ControllerMap_get_id_m6FA8319C62C718FE081C3D490348B1235840E336((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_1), /*hidden argument*/NULL);
		int32_t L_1 = p0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_2 = V_0;
		return L_2;
	}

IL_003a:
	{
		int32_t L_3 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
		G_B2_0 = ((int32_t)-658447086);
		goto IL_0007;
	}

IL_0045:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_4 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_5 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4);
		RuntimeObject * L_6 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_1 = (RuntimeObject *)L_6;
		G_B2_0 = ((int32_t)-658447085);
		goto IL_0007;
	}

IL_0059:
	{
		G_B2_0 = ((int32_t)-658447086);
		goto IL_0007;
	}

IL_0060:
	{
		int32_t L_7 = V_0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_8 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_8);
		int32_t L_9 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0045;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::IndexOf(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_IndexOf_mA70F2710F8350B51F260A24E6F3EE1DD3B9BE3F7_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = p0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		int32_t L_1 = p1;
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_008f;
		}
	}

IL_000e:
	{
		G_B3_0 = ((int32_t)2005931128);
	}

IL_0013:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)2005931133))))
		{
			case 0:
			{
				goto IL_0080;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_006c;
			}
			case 3:
			{
				goto IL_009b;
			}
			case 4:
			{
				goto IL_000e;
			}
			case 5:
			{
				goto IL_008d;
			}
		}
	}
	{
		goto IL_009b;
	}

IL_0038:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_1));
		int32_t L_2 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_1), /*hidden argument*/NULL);
		int32_t L_3 = p0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0082;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_4 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_5 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4);
		RuntimeObject * L_6 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_2 = (RuntimeObject *)L_6;
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2));
		int32_t L_7 = ControllerMap_get_layoutId_m1B90CB02DDE635CA339BA98082FD4F9480DF0EF7((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2), /*hidden argument*/NULL);
		int32_t L_8 = p1;
		if ((!(((uint32_t)L_7) == ((uint32_t)L_8))))
		{
			goto IL_0082;
		}
	}
	{
		G_B3_0 = ((int32_t)2005931133);
		goto IL_0013;
	}

IL_006c:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_9 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_10 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_9);
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_1 = (RuntimeObject *)L_11;
		G_B3_0 = ((int32_t)2005931132);
		goto IL_0013;
	}

IL_0080:
	{
		int32_t L_12 = V_0;
		return L_12;
	}

IL_0082:
	{
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
		G_B3_0 = ((int32_t)2005931134);
		goto IL_0013;
	}

IL_008d:
	{
		return (-1);
	}

IL_008f:
	{
		V_0 = (int32_t)0;
		G_B3_0 = ((int32_t)2005931134);
		goto IL_0013;
	}

IL_009b:
	{
		int32_t L_14 = V_0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_15 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_15);
		int32_t L_16 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_006c;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Contains(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool lVPGlCbkNvrVSnFFcfgwEyebHaly_Contains_mB2C07E6F2EAC2290D90C47836FD2A7056F353879_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int32_t L_0 = p0;
		NullCheck((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this);
		int32_t L_1 = ((  int32_t (*) (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20)->methodPointer)((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20));
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		return (bool)0;
	}

IL_000c:
	{
		return (bool)1;
	}
}
// System.Boolean lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Contains(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool lVPGlCbkNvrVSnFFcfgwEyebHaly_Contains_mF85C09849D9BAB72466A88C09099BEFFAD8EE7A6_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		int32_t L_0 = p0;
		int32_t L_1 = p1;
		NullCheck((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this);
		int32_t L_2 = ((  int32_t (*) (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16)->methodPointer)((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16));
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		return (bool)1;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Clear(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void lVPGlCbkNvrVSnFFcfgwEyebHaly_Clear_m17040078739B7A2EE787FDC6B7FD81FF4BAD9F70_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, bool p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_Clear_m17040078739B7A2EE787FDC6B7FD81FF4BAD9F70_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * V_2 = NULL;
	RuntimeObject * V_3 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B9_1 = 0;
	{
		bool L_0 = p0;
		if (L_0)
		{
			goto IL_00cb;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_1 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_1);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21));
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_2 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_2);
		List_1_Clear_m88D5862BAF30335E7FEA486544F28FA5CECFF6DE((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_2, /*hidden argument*/List_1_Clear_m88D5862BAF30335E7FEA486544F28FA5CECFF6DE_RuntimeMethod_var);
	}

IL_001c:
	{
		G_B3_0 = ((int32_t)671644862);
	}

IL_0021:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)671644863))))
		{
			case 0:
			{
				goto IL_009b;
			}
			case 1:
			{
				goto IL_00e3;
			}
			case 2:
			{
				goto IL_0087;
			}
			case 3:
			{
				goto IL_00cb;
			}
			case 4:
			{
				goto IL_0051;
			}
			case 5:
			{
				goto IL_00ee;
			}
			case 6:
			{
				goto IL_001c;
			}
			case 7:
			{
				goto IL_00bd;
			}
		}
	}
	{
		goto IL_00ee;
	}

IL_0051:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		int32_t L_3 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), /*hidden argument*/NULL);
		V_1 = (int32_t)L_3;
		IL2CPP_RUNTIME_CLASS_INIT(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_il2cpp_TypeInfo_var);
		MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * L_4 = ReInput_get_mapping_m57CBC64F23235AA736D4C057ED17A85421F00D5C(/*hidden argument*/NULL);
		int32_t L_5 = V_1;
		NullCheck((MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 *)L_4);
		InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * L_6 = MappingHelper_GetMapCategory_mBC62128856B61C97B7AD00A9739EABA209EEFDD0((MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 *)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_2 = (InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 *)L_6;
		InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * L_7 = V_2;
		if (!L_7)
		{
			goto IL_009b;
		}
	}
	{
		InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * L_8 = V_2;
		NullCheck((InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 *)L_8);
		bool L_9 = InputCategory_get_userAssignable_m6F4B367A66B6F94277FB20F851F2280FB6059B64((InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 *)L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_10 = (int32_t)((int32_t)671644856);
		G_B9_0 = L_10;
		G_B9_1 = L_10;
		goto IL_0084;
	}

IL_007e:
	{
		int32_t L_11 = (int32_t)((int32_t)671644863);
		G_B9_0 = L_11;
		G_B9_1 = L_11;
	}

IL_0084:
	{
		G_B3_0 = G_B9_1;
		goto IL_0021;
	}

IL_0087:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_12 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_13 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_12);
		RuntimeObject * L_14 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_12, (int32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_3 = (RuntimeObject *)L_14;
		G_B3_0 = ((int32_t)671644859);
		goto IL_0021;
	}

IL_009b:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_15 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_16 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_15);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_17 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		int32_t L_18 = V_0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_17);
		List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_17, (int32_t)L_18, /*hidden argument*/List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5_RuntimeMethod_var);
		G_B3_0 = ((int32_t)671644856);
		goto IL_0021;
	}

IL_00bd:
	{
		int32_t L_19 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1));
		G_B3_0 = ((int32_t)671644858);
		goto IL_0021;
	}

IL_00cb:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_20 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_20);
		int32_t L_21 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_21, (int32_t)1));
		G_B3_0 = ((int32_t)671644858);
		goto IL_0021;
	}

IL_00e3:
	{
		return;
	}
	// Dead block : IL_00e4: ldc.i4 671644860

IL_00ee:
	{
		int32_t L_22 = V_0;
		if ((((int32_t)L_22) >= ((int32_t)0)))
		{
			goto IL_0087;
		}
	}
	{
		return;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::Clear(System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void lVPGlCbkNvrVSnFFcfgwEyebHaly_Clear_m03E91053683751E1C80B4D40F3901A426A586ADC_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, bool p1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_Clear_m03E91053683751E1C80B4D40F3901A426A586ADC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	RuntimeObject * V_3 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	int32_t G_B14_0 = 0;
	int32_t G_B14_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_il2cpp_TypeInfo_var);
		MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * L_0 = ReInput_get_mapping_m57CBC64F23235AA736D4C057ED17A85421F00D5C(/*hidden argument*/NULL);
		int32_t L_1 = p0;
		NullCheck((MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 *)L_0);
		InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * L_2 = MappingHelper_GetMapCategory_mBC62128856B61C97B7AD00A9739EABA209EEFDD0((MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 *)L_0, (int32_t)L_1, /*hidden argument*/NULL);
		V_0 = (InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 *)L_2;
		InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * L_3 = V_0;
		if (L_3)
		{
			goto IL_00dd;
		}
	}

IL_0012:
	{
		G_B2_0 = ((int32_t)470002511);
	}

IL_0017:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)470002510))))
		{
			case 0:
			{
				goto IL_0012;
			}
			case 1:
			{
				goto IL_0082;
			}
			case 2:
			{
				goto IL_00f3;
			}
			case 3:
			{
				goto IL_006e;
			}
			case 4:
			{
				goto IL_0091;
			}
			case 5:
			{
				goto IL_004f;
			}
			case 6:
			{
				goto IL_00b7;
			}
			case 7:
			{
				goto IL_009f;
			}
			case 8:
			{
				goto IL_008a;
			}
			case 9:
			{
				goto IL_00dd;
			}
		}
	}
	{
		goto IL_00f3;
	}

IL_004f:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_4 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_5 = V_1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_6 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		int32_t L_7 = V_1;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_6);
		List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_6, (int32_t)L_7, /*hidden argument*/List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5_RuntimeMethod_var);
		G_B2_0 = ((int32_t)470002506);
		goto IL_0017;
	}

IL_006e:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_8 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_9 = V_1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_8);
		RuntimeObject * L_10 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_3 = (RuntimeObject *)L_10;
		G_B2_0 = ((int32_t)470002504);
		goto IL_0017;
	}

IL_0082:
	{
		return;
	}
	// Dead block : IL_0083: ldc.i4 470002503

IL_008a:
	{
		G_B2_0 = ((int32_t)470002508);
		goto IL_0017;
	}

IL_0091:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)1));
		G_B2_0 = ((int32_t)470002508);
		goto IL_0017;
	}

IL_009f:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_12 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_12);
		int32_t L_13 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		V_1 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)1));
		G_B2_0 = ((int32_t)470002502);
		goto IL_0017;
	}

IL_00b7:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		int32_t L_14 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), /*hidden argument*/NULL);
		V_2 = (int32_t)L_14;
		int32_t L_15 = p0;
		int32_t L_16 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_00d1;
		}
	}
	{
		int32_t L_17 = (int32_t)((int32_t)470002507);
		G_B14_0 = L_17;
		G_B14_1 = L_17;
		goto IL_00d7;
	}

IL_00d1:
	{
		int32_t L_18 = (int32_t)((int32_t)470002506);
		G_B14_0 = L_18;
		G_B14_1 = L_18;
	}

IL_00d7:
	{
		G_B2_0 = G_B14_1;
		goto IL_0017;
	}

IL_00dd:
	{
		bool L_19 = p1;
		if (!L_19)
		{
			goto IL_009f;
		}
	}
	{
		InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * L_20 = V_0;
		NullCheck((InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 *)L_20);
		bool L_21 = InputCategory_get_userAssignable_m6F4B367A66B6F94277FB20F851F2280FB6059B64((InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 *)L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_009f;
		}
	}
	{
		return;
	}
	// Dead block : IL_00e9: ldc.i4 470002505

IL_00f3:
	{
		int32_t L_22 = V_1;
		if ((((int32_t)L_22) >= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		return;
	}
}
// System.Void lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::ClearByLayout(System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void lVPGlCbkNvrVSnFFcfgwEyebHaly_ClearByLayout_mEE74790663D60300420AEA46949EFD03F6EDE8B0_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, bool p1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (lVPGlCbkNvrVSnFFcfgwEyebHaly_ClearByLayout_mEE74790663D60300420AEA46949EFD03F6EDE8B0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * V_2 = NULL;
	RuntimeObject * V_3 = NULL;
	RuntimeObject * V_4 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B11_1 = 0;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
		goto IL_00ec;
	}

IL_0013:
	{
		G_B2_0 = ((int32_t)-1053357467);
	}

IL_0018:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-1053357468))))
		{
			case 0:
			{
				goto IL_0044;
			}
			case 1:
			{
				goto IL_00bf;
			}
			case 2:
			{
				goto IL_0013;
			}
			case 3:
			{
				goto IL_00ec;
			}
			case 4:
			{
				goto IL_0057;
			}
			case 5:
			{
				goto IL_00d6;
			}
			case 6:
			{
				goto IL_00b1;
			}
		}
	}
	{
		goto IL_00ec;
	}

IL_0044:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_2 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_3 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		G_B2_0 = ((int32_t)-1053357471);
		goto IL_0018;
	}

IL_0057:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		int32_t L_4 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), /*hidden argument*/NULL);
		V_1 = (int32_t)L_4;
		int32_t L_5 = p0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_6 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_7 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_6);
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_4 = (RuntimeObject *)L_8;
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_4));
		int32_t L_9 = ControllerMap_get_layoutId_m1B90CB02DDE635CA339BA98082FD4F9480DF0EF7((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_4), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)L_9))))
		{
			goto IL_00b1;
		}
	}
	{
		bool L_10 = p1;
		if (!L_10)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_il2cpp_TypeInfo_var);
		MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * L_11 = ReInput_get_mapping_m57CBC64F23235AA736D4C057ED17A85421F00D5C(/*hidden argument*/NULL);
		int32_t L_12 = V_1;
		NullCheck((MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 *)L_11);
		InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * L_13 = MappingHelper_GetMapCategory_mBC62128856B61C97B7AD00A9739EABA209EEFDD0((MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 *)L_11, (int32_t)L_12, /*hidden argument*/NULL);
		V_2 = (InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 *)L_13;
		InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * L_14 = V_2;
		if (!L_14)
		{
			goto IL_0044;
		}
	}
	{
		InputMapCategory_t3A6CAA2D6E69E45E734A026F26B816788D175602 * L_15 = V_2;
		NullCheck((InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 *)L_15);
		bool L_16 = InputCategory_get_userAssignable_m6F4B367A66B6F94277FB20F851F2280FB6059B64((InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 *)L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_17 = (int32_t)((int32_t)-1053357470);
		G_B11_0 = L_17;
		G_B11_1 = L_17;
		goto IL_00ab;
	}

IL_00a5:
	{
		int32_t L_18 = (int32_t)((int32_t)-1053357468);
		G_B11_0 = L_18;
		G_B11_1 = L_18;
	}

IL_00ab:
	{
		G_B2_0 = G_B11_1;
		goto IL_0018;
	}

IL_00b1:
	{
		int32_t L_19 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1));
		G_B2_0 = ((int32_t)-1053357465);
		goto IL_0018;
	}

IL_00bf:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_20 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_21 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_20);
		RuntimeObject * L_22 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_20, (int32_t)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_3 = (RuntimeObject *)L_22;
		G_B2_0 = ((int32_t)-1053357472);
		goto IL_0018;
	}

IL_00d6:
	{
		List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E * L_23 = (List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)__this->get_BdaanHoIMYiyKBPrxEPAAvUoiXiO_0();
		int32_t L_24 = V_0;
		NullCheck((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_23);
		List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5((List_1_t4C65ABDD2ABD8E706F31D12EFF7B7308079F212E *)L_23, (int32_t)L_24, /*hidden argument*/List_1_RemoveAt_m7016ACAC4680B5067B4C91DF77A292D8E8D427A5_RuntimeMethod_var);
		G_B2_0 = ((int32_t)-1053357470);
		goto IL_0018;
	}

IL_00ec:
	{
		int32_t L_25 = V_0;
		if ((((int32_t)L_25) >= ((int32_t)0)))
		{
			goto IL_00bf;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* lVPGlCbkNvrVSnFFcfgwEyebHaly_GetEnumerator_m8A04E00DC841C8AA4A3637A9F4E113AAFF799A3F_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, const RuntimeMethod* method)
{
	iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA * V_0 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA * L_0 = (iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 22));
		((  void (*) (iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23)->methodPointer)(L_0, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23));
		V_0 = (iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA *)L_0;
	}

IL_0007:
	{
		G_B2_0 = ((int32_t)-1616299482);
	}

IL_000c:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-1616299481))))
		{
			case 0:
			{
				goto IL_0033;
			}
			case 1:
			{
				goto IL_0025;
			}
			case 2:
			{
				goto IL_0007;
			}
		}
	}
	{
		goto IL_0033;
	}

IL_0025:
	{
		iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_DKaFTWTMhFeLQHDExnDMRxZfmROL_2(__this);
		G_B2_0 = ((int32_t)-1616299481);
		goto IL_000c;
	}

IL_0033:
	{
		iFUZfyBAetyFZkyXRrMrwIZFTkH_tB1D4A5F184F33D6667AD999D908D3D6F5C6DCDDA * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* lVPGlCbkNvrVSnFFcfgwEyebHaly_System_Collections_IEnumerable_GetEnumerator_m90A658B4BEC3298D1457DFFBEF4B2A6290AFE9C6_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, const RuntimeMethod* method)
{
	CHECK_PAUSE_POINT;
	{
		NullCheck((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24)->methodPointer)((lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24));
		return L_0;
	}
}
// T lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::nUjXrKrGUaKkAXqkzFzZwepjOrS(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * lVPGlCbkNvrVSnFFcfgwEyebHaly_nUjXrKrGUaKkAXqkzFzZwepjOrS_m92CAD94355852B33A831220BAFE9F2E4CF23C7BB_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	RuntimeObject * V_3 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B3_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B12_1 = 0;
	{
		int32_t L_0 = p0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
	}

IL_000f:
	{
		G_B3_0 = ((int32_t)-1293424780);
	}

IL_0014:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B3_0^(int32_t)((int32_t)-1293424782))))
		{
			case 0:
			{
				goto IL_0065;
			}
			case 1:
			{
				goto IL_0099;
			}
			case 2:
			{
				goto IL_003d;
			}
			case 3:
			{
				goto IL_006c;
			}
			case 4:
			{
				goto IL_000f;
			}
			case 5:
			{
				goto IL_00b0;
			}
			case 6:
			{
				goto IL_008b;
			}
		}
	}
	{
		goto IL_00b0;
	}

IL_003d:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2));
		int32_t L_1 = ControllerMap_get_id_m6FA8319C62C718FE081C3D490348B1235840E336((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2), /*hidden argument*/NULL);
		int32_t L_2 = p0;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_005a;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_3 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_4 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_3);
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return L_5;
	}

IL_005a:
	{
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
		G_B3_0 = ((int32_t)-1293424783);
		goto IL_0014;
	}

IL_0065:
	{
		G_B3_0 = ((int32_t)-1293424783);
		goto IL_0014;
	}

IL_006c:
	{
		int32_t L_7 = V_0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_8 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_8);
		int32_t L_9 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		if ((((int32_t)L_7) >= ((int32_t)L_9)))
		{
			goto IL_0082;
		}
	}
	{
		int32_t L_10 = (int32_t)((int32_t)-1293424781);
		G_B12_0 = L_10;
		G_B12_1 = L_10;
		goto IL_0088;
	}

IL_0082:
	{
		int32_t L_11 = (int32_t)((int32_t)-1293424777);
		G_B12_0 = L_11;
		G_B12_1 = L_11;
	}

IL_0088:
	{
		G_B3_0 = G_B12_1;
		goto IL_0014;
	}

IL_008b:
	{
		RuntimeObject * L_12 = V_1;
		return L_12;
	}

IL_008d:
	{
		V_0 = (int32_t)0;
		G_B3_0 = ((int32_t)-1293424782);
		goto IL_0014;
	}

IL_0099:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_13 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_14 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_13);
		RuntimeObject * L_15 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_13, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_2 = (RuntimeObject *)L_15;
		G_B3_0 = ((int32_t)-1293424784);
		goto IL_0014;
	}

IL_00b0:
	{
		il2cpp_codegen_initobj((&V_3), sizeof(RuntimeObject *));
		RuntimeObject * L_16 = V_3;
		return L_16;
	}
}
// T lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::nUjXrKrGUaKkAXqkzFzZwepjOrS(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * lVPGlCbkNvrVSnFFcfgwEyebHaly_nUjXrKrGUaKkAXqkzFzZwepjOrS_m794661C96E6774B970DC399306045D678CEA540B_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	RuntimeObject * V_3 = NULL;
	RuntimeObject * V_4 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	int32_t G_B14_0 = 0;
	int32_t G_B14_1 = 0;
	{
		int32_t L_0 = p0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_005d;
		}
	}

IL_0004:
	{
		G_B2_0 = ((int32_t)1131576419);
	}

IL_0009:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)1131576418))))
		{
			case 0:
			{
				goto IL_0004;
			}
			case 1:
			{
				goto IL_0070;
			}
			case 2:
			{
				goto IL_007b;
			}
			case 3:
			{
				goto IL_0035;
			}
			case 4:
			{
				goto IL_00d1;
			}
			case 5:
			{
				goto IL_005d;
			}
			case 6:
			{
				goto IL_009d;
			}
		}
	}
	{
		goto IL_00d1;
	}

IL_0035:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		int32_t L_1 = ControllerMap_get_layoutId_m1B90CB02DDE635CA339BA98082FD4F9480DF0EF7((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), /*hidden argument*/NULL);
		int32_t L_2 = p1;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0052;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_3 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_4 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_3);
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return L_5;
	}

IL_0052:
	{
		CHECK_PAUSE_POINT;
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
		G_B2_0 = ((int32_t)1131576416);
		goto IL_0009;
	}

IL_005d:
	{
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		RuntimeObject * L_7 = V_1;
		return L_7;
	}

IL_0067:
	{
		CHECK_PAUSE_POINT;
		V_0 = (int32_t)0;
		G_B2_0 = ((int32_t)1131576416);
		goto IL_0009;
	}

IL_0070:
	{
		int32_t L_8 = p1;
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_0067;
		}
	}
	{
		G_B2_0 = ((int32_t)1131576423);
		goto IL_0009;
	}

IL_007b:
	{
		int32_t L_9 = V_0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_10 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		if ((((int32_t)L_9) >= ((int32_t)L_11)))
		{
			goto IL_0091;
		}
	}
	{
		int32_t L_12 = (int32_t)((int32_t)1131576420);
		G_B14_0 = L_12;
		G_B14_1 = L_12;
		goto IL_0097;
	}

IL_0091:
	{
		int32_t L_13 = (int32_t)((int32_t)1131576422);
		G_B14_0 = L_13;
		G_B14_1 = L_13;
	}

IL_0097:
	{
		G_B2_0 = G_B14_1;
		goto IL_0009;
	}

IL_009d:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_14 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_15 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_14);
		RuntimeObject * L_16 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_14, (int32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_2 = (RuntimeObject *)L_16;
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2));
		int32_t L_17 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2), /*hidden argument*/NULL);
		int32_t L_18 = p0;
		if ((!(((uint32_t)L_17) == ((uint32_t)L_18))))
		{
			goto IL_0052;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_19 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_20 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_19);
		RuntimeObject * L_21 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_19, (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_3 = (RuntimeObject *)L_21;
		G_B2_0 = ((int32_t)1131576417);
		goto IL_0009;
	}

IL_00d1:
	{
		il2cpp_codegen_initobj((&V_4), sizeof(RuntimeObject *));
		RuntimeObject * L_22 = V_4;
		return L_22;
	}
}
// T lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::HwvbcpoFcTeNNfyZQvVJixFiFrYK(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * lVPGlCbkNvrVSnFFcfgwEyebHaly_HwvbcpoFcTeNNfyZQvVJixFiFrYK_m9B88438D395EFC939BDA62F80D7BA8AA7AE82B13_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_0062;
	}

IL_0004:
	{
		G_B2_0 = ((int32_t)-1098238765);
	}

IL_0009:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-1098238766))))
		{
			case 0:
			{
				goto IL_0004;
			}
			case 1:
			{
				goto IL_0026;
			}
			case 2:
			{
				goto IL_0062;
			}
			case 3:
			{
				goto IL_003a;
			}
		}
	}
	{
		goto IL_0062;
	}

IL_0026:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_1 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_1 = (RuntimeObject *)L_2;
		G_B2_0 = ((int32_t)-1098238767);
		goto IL_0009;
	}

IL_003a:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_1));
		int32_t L_3 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_1), /*hidden argument*/NULL);
		int32_t L_4 = p0;
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_0057;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_5 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_6 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_5);
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return L_7;
	}

IL_0057:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		G_B2_0 = ((int32_t)-1098238768);
		goto IL_0009;
	}

IL_0062:
	{
		int32_t L_9 = V_0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_10 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0026;
		}
	}
	{
		il2cpp_codegen_initobj((&V_2), sizeof(RuntimeObject *));
		RuntimeObject * L_12 = V_2;
		return L_12;
	}
}
// System.Boolean lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::ContainsMapInCategory(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool lVPGlCbkNvrVSnFFcfgwEyebHaly_ContainsMapInCategory_m0AAA2F6C8EE19612EB5C2F57BE92163275FC7874_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, int32_t p0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		int32_t L_0 = p0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}

IL_0004:
	{
		G_B2_0 = ((int32_t)-1224924746);
	}

IL_0009:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-1224924745))))
		{
			case 0:
			{
				goto IL_002a;
			}
			case 1:
			{
				goto IL_005b;
			}
			case 2:
			{
				goto IL_0072;
			}
			case 3:
			{
				goto IL_0004;
			}
			case 4:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_0072;
	}

IL_002a:
	{
		return (bool)1;
	}

IL_002c:
	{
		CHECK_PAUSE_POINT;
		int32_t L_1 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)1));
		G_B2_0 = ((int32_t)-1224924747);
		goto IL_0009;
	}

IL_0037:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_2 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_3 = V_1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2);
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_2 = (RuntimeObject *)L_4;
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2));
		int32_t L_5 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_2), /*hidden argument*/NULL);
		int32_t L_6 = p0;
		if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
		{
			goto IL_002c;
		}
	}
	{
		G_B2_0 = ((int32_t)-1224924745);
		goto IL_0009;
	}

IL_005b:
	{
		return (bool)0;
	}

IL_005d:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_7 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7);
		int32_t L_8 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		V_0 = (int32_t)L_8;
		V_1 = (int32_t)0;
		G_B2_0 = ((int32_t)-1224924747);
		goto IL_0009;
	}

IL_0072:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = V_0;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0037;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::SetEnabledAll(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR int32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_SetEnabledAll_m8CD00B2674098C0523708FAF0BE4376CAEDBA961_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, bool p0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	RuntimeObject * V_3 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		V_0 = (int32_t)L_1;
		V_1 = (int32_t)0;
		V_2 = (int32_t)0;
		goto IL_0088;
	}

IL_0012:
	{
		G_B2_0 = ((int32_t)-1131159659);
	}

IL_0017:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-1131159657))))
		{
			case 0:
			{
				goto IL_0088;
			}
			case 1:
			{
				goto IL_0057;
			}
			case 2:
			{
				goto IL_0038;
			}
			case 3:
			{
				goto IL_004c;
			}
			case 4:
			{
				goto IL_0012;
			}
		}
	}
	{
		goto IL_0088;
	}

IL_0038:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_2 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_3 = V_2;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2);
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_3 = (RuntimeObject *)L_4;
		G_B2_0 = ((int32_t)-1131159658);
		goto IL_0017;
	}

IL_004c:
	{
		CHECK_PAUSE_POINT;
		int32_t L_5 = V_2;
		V_2 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
		G_B2_0 = ((int32_t)-1131159657);
		goto IL_0017;
	}

IL_0057:
	{
		RuntimeObject * L_6 = V_3;
		if (!L_6)
		{
			goto IL_004c;
		}
	}
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		bool L_7 = ControllerMap_get_enabled_m454281A8B80828B120C4F7EDF2BC52028AD35C6F((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), /*hidden argument*/NULL);
		bool L_8 = p0;
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_004c;
		}
	}
	{
		bool L_9 = p0;
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		ControllerMap_set_enabled_m4A347FEEC7D6FF51515C88121C4F2A0437B472A8((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), (bool)L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
		G_B2_0 = ((int32_t)-1131159660);
		goto IL_0017;
	}

IL_0088:
	{
		int32_t L_11 = V_2;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_13 = V_1;
		return L_13;
	}
}
// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::SetEnabledByCategory(System.Boolean,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_SetEnabledByCategory_mFCF8974382E921472D69DB814921DA69F29CBF7F_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, bool p0, int32_t p1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	RuntimeObject * V_3 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B11_1 = 0;
	int32_t G_B17_0 = 0;
	int32_t G_B17_1 = 0;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		V_0 = (int32_t)L_1;
	}

IL_000c:
	{
		G_B2_0 = ((int32_t)918559462);
	}

IL_0011:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)918559463))))
		{
			case 0:
			{
				goto IL_0075;
			}
			case 1:
			{
				goto IL_00a9;
			}
			case 2:
			{
				goto IL_00b7;
			}
			case 3:
			{
				goto IL_000c;
			}
			case 4:
			{
				goto IL_00e5;
			}
			case 5:
			{
				goto IL_006a;
			}
			case 6:
			{
				goto IL_0045;
			}
			case 7:
			{
				goto IL_0080;
			}
			case 8:
			{
				goto IL_00c1;
			}
		}
	}
	{
		goto IL_00e5;
	}

IL_0045:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		bool L_2 = ControllerMap_get_enabled_m454281A8B80828B120C4F7EDF2BC52028AD35C6F((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), /*hidden argument*/NULL);
		bool L_3 = p0;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_006a;
		}
	}
	{
		bool L_4 = p0;
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		ControllerMap_set_enabled_m4A347FEEC7D6FF51515C88121C4F2A0437B472A8((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), (bool)L_4, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)918559463);
		goto IL_0011;
	}

IL_006a:
	{
		int32_t L_5 = V_2;
		V_2 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
		G_B2_0 = ((int32_t)918559459);
		goto IL_0011;
	}

IL_0075:
	{
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
		G_B2_0 = ((int32_t)918559458);
		goto IL_0011;
	}

IL_0080:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_7 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_8 = V_2;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7);
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_3 = (RuntimeObject *)L_9;
		RuntimeObject * L_10 = V_3;
		if (L_10)
		{
			goto IL_009d;
		}
	}
	{
		int32_t L_11 = (int32_t)((int32_t)918559458);
		G_B11_0 = L_11;
		G_B11_1 = L_11;
		goto IL_00a3;
	}

IL_009d:
	{
		int32_t L_12 = (int32_t)((int32_t)918559471);
		G_B11_0 = L_12;
		G_B11_1 = L_12;
	}

IL_00a3:
	{
		G_B2_0 = G_B11_1;
		goto IL_0011;
	}

IL_00a9:
	{
		V_1 = (int32_t)0;
		V_2 = (int32_t)0;
		G_B2_0 = ((int32_t)918559461);
		goto IL_0011;
	}

IL_00b7:
	{
		G_B2_0 = ((int32_t)918559459);
		goto IL_0011;
	}

IL_00c1:
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		int32_t L_13 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), /*hidden argument*/NULL);
		int32_t L_14 = p1;
		if ((((int32_t)L_13) == ((int32_t)L_14)))
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_15 = (int32_t)((int32_t)918559458);
		G_B17_0 = L_15;
		G_B17_1 = L_15;
		goto IL_00df;
	}

IL_00d9:
	{
		int32_t L_16 = (int32_t)((int32_t)918559457);
		G_B17_0 = L_16;
		G_B17_1 = L_16;
	}

IL_00df:
	{
		G_B2_0 = G_B17_1;
		goto IL_0011;
	}

IL_00e5:
	{
		int32_t L_17 = V_2;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_19 = V_1;
		return L_19;
	}
}
// System.Int32 lVPGlCbkNvrVSnFFcfgwEyebHaly<System.Object>::SetEnabledByCategory(System.Boolean,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t lVPGlCbkNvrVSnFFcfgwEyebHaly_SetEnabledByCategory_mA2B089FF4F5B77169954F29B1B8A94DF6A2C7DC8_gshared (lVPGlCbkNvrVSnFFcfgwEyebHaly_t3E25F5263D0F87BC11AF4918E81A3244E39E5642 * __this, bool p0, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	RuntimeObject * V_3 = NULL;
	CHECK_PAUSE_POINT;
	int32_t G_B2_0 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B11_1 = 0;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		V_0 = (int32_t)L_1;
		V_1 = (int32_t)0;
	}

IL_000e:
	{
		G_B2_0 = ((int32_t)-1254479209);
	}

IL_0013:
	{
		CHECK_PAUSE_POINT;
		switch (((int32_t)((int32_t)G_B2_0^(int32_t)((int32_t)-1254479213))))
		{
			case 0:
			{
				goto IL_000e;
			}
			case 1:
			{
				goto IL_00b9;
			}
			case 2:
			{
				goto IL_009d;
			}
			case 3:
			{
				goto IL_00c7;
			}
			case 4:
			{
				goto IL_003b;
			}
			case 5:
			{
				goto IL_0044;
			}
		}
	}
	{
		goto IL_00c7;
	}

IL_003b:
	{
		V_2 = (int32_t)0;
		G_B2_0 = ((int32_t)-1254479216);
		goto IL_0013;
	}

IL_0044:
	{
		CHECK_PAUSE_POINT;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_2 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_HXrZyhBzDfszTlMEUEDAshPIFVI_4();
		int32_t L_3 = V_2;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2);
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		V_3 = (RuntimeObject *)L_4;
		RuntimeObject * L_5 = V_3;
		if (!L_5)
		{
			goto IL_00b9;
		}
	}
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		int32_t L_6 = ControllerMap_get_categoryId_m1B05E24F82D8DED46B2BAFBCD9D8629328CFBA2B((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), /*hidden argument*/NULL);
		int32_t L_7 = p1;
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_00b9;
		}
	}
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		int32_t L_8 = ControllerMap_get_layoutId_m1B90CB02DDE635CA339BA98082FD4F9480DF0EF7((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), /*hidden argument*/NULL);
		int32_t L_9 = p2;
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_00b9;
		}
	}
	{
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		bool L_10 = ControllerMap_get_enabled_m454281A8B80828B120C4F7EDF2BC52028AD35C6F((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), /*hidden argument*/NULL);
		bool L_11 = p0;
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_0091;
		}
	}
	{
		int32_t L_12 = (int32_t)((int32_t)-1254479214);
		G_B11_0 = L_12;
		G_B11_1 = L_12;
		goto IL_0097;
	}

IL_0091:
	{
		int32_t L_13 = (int32_t)((int32_t)-1254479215);
		G_B11_0 = L_13;
		G_B11_1 = L_13;
	}

IL_0097:
	{
		G_B2_0 = G_B11_1;
		goto IL_0013;
	}

IL_009d:
	{
		bool L_14 = p0;
		NullCheck((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3));
		ControllerMap_set_enabled_m4A347FEEC7D6FF51515C88121C4F2A0437B472A8((ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB *)(V_3), (bool)L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
		G_B2_0 = ((int32_t)-1254479214);
		goto IL_0013;
	}

IL_00b9:
	{
		int32_t L_16 = V_2;
		V_2 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
		G_B2_0 = ((int32_t)-1254479216);
		goto IL_0013;
	}

IL_00c7:
	{
		int32_t L_17 = V_2;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_19 = V_1;
		return L_19;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
