﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Rewired.ComponentControls.ComponentControl
struct ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96;
// Rewired.ComponentControls.ComponentController
struct ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE;
// Rewired.ComponentControls.CustomController/CreateCustomControllerSettings
struct CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7;
// Rewired.ComponentControls.Data.CustomControllerElementSelector
struct CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B;
// Rewired.ComponentControls.Data.CustomControllerElementTarget
struct CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A;
// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForBoolean
struct CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54;
// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat
struct CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4;
// Rewired.ComponentControls.Data.CustomControllerSelector
struct CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1;
// Rewired.ComponentControls.IComponentController
struct IComponentController_tFF9EB6121EED599D2594C0E3CD4EDD1D19412D1B;
// Rewired.ComponentControls.TouchButton
struct TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F;
// Rewired.ComponentControls.TouchButton/AxisValueChangedEventHandler
struct AxisValueChangedEventHandler_t622BC4F8C94A13837769CEFE1FD12CC18B926791;
// Rewired.ComponentControls.TouchButton/ButtonDownEventHandler
struct ButtonDownEventHandler_tD3FB726C5A3E17A89FA7992F2D0D0422286B7E7D;
// Rewired.ComponentControls.TouchButton/ButtonUpEventHandler
struct ButtonUpEventHandler_t529887EE63C2C416911C6C64F8D230008EB73339;
// Rewired.ComponentControls.TouchButton/ButtonValueChangedEventHandler
struct ButtonValueChangedEventHandler_tB3FD14B60CDD8A2E3B8D8B26F951B056495B5A50;
// Rewired.ComponentControls.TouchInteractable
struct TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8;
// Rewired.ComponentControls.TouchInteractable/InteractionStateTransitionArgs
struct InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42;
// Rewired.ComponentControls.TouchInteractable/InteractionStateTransitionEventHandler
struct InteractionStateTransitionEventHandler_tC1D00C17A8A6547002894B4718BD039679204D36;
// Rewired.ComponentControls.TouchInteractable/VisibilityChangedEventHandler
struct VisibilityChangedEventHandler_t3D6E9B1BC6606FEC892CE393EDD4D9ED89DE9D28;
// Rewired.ComponentControls.TouchJoystick
struct TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D;
// Rewired.ComponentControls.TouchJoystick/TapEventHandler
struct TapEventHandler_tE705770344A52DD95176406B4B00E5EE8E525B2F;
// Rewired.ComponentControls.TouchJoystick/TouchEndedEventHandler
struct TouchEndedEventHandler_t05BD8DD2BB21689F364B8300E6A1F266093E111C;
// Rewired.ComponentControls.TouchJoystick/TouchStartedEventHandler
struct TouchStartedEventHandler_tC39C61D509D6C9452386D0D05B7782F35841D4AB;
// Rewired.ComponentControls.TouchJoystick/ValueChangedEventHandler
struct ValueChangedEventHandler_t739FAF55A04B824B9182F47969314C660604524B;
// Rewired.ComponentControls.TouchPad/IftMAPzfkVAPWSFrTxsXPBYDanRJ
struct IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA;
// Rewired.ComponentControls.TouchPad/IftMAPzfkVAPWSFrTxsXPBYDanRJ/YcVzboeRjDknSZWvRpWNyVnfSFz[]
struct YcVzboeRjDknSZWvRpWNyVnfSFzU5BU5D_t68B3459EC9CC177F5B9658DB9A1B945DFD0F8FBB;
// Rewired.ComponentControls.TouchPad/PressDownEventHandler
struct PressDownEventHandler_tADC629B1DD822026CE2E06E3CDA7FB6D4E495F8D;
// Rewired.ComponentControls.TouchPad/PressUpEventHandler
struct PressUpEventHandler_t4125670E8C4E9682843D4F1D030ACF428FD397EC;
// Rewired.ComponentControls.TouchPad/TapEventHandler
struct TapEventHandler_t33FC3FB8B60BF60104F3569F86657ADDB6BC0944;
// Rewired.ComponentControls.TouchPad/ValueChangedEventHandler
struct ValueChangedEventHandler_t5D816B57AB2EEC7ACA0BE8CFB398EE5EDD7E32C4;
// Rewired.ComponentControls.TouchRegion
struct TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49;
// Rewired.ComponentControls.TouchRegion/CUBRkEgPtmqloePiGjPGzvftMEz
struct CUBRkEgPtmqloePiGjPGzvftMEz_t62B6522D584CD2A21FCD80D3C3CAED97E95E8D13;
// Rewired.ComponentControls.TouchRegion/DbjAHtCsNuwTcsSMKOqHaDEgpfX
struct DbjAHtCsNuwTcsSMKOqHaDEgpfX_t1824811CE094ED8DE5F842353B1C13B05116152D;
// Rewired.ComponentControls.TouchRegion/SpNSxRoDgiUWSAIfKMmMQPNAbEH
struct SpNSxRoDgiUWSAIfKMmMQPNAbEH_tC64B3B02AA7E79A9CAE0BA33FE4F96AC5E5405A1;
// Rewired.ComponentControls.TouchRegion/gWVGylYAAhiNGOqYxKsBnzSeJJp
struct gWVGylYAAhiNGOqYxKsBnzSeJJp_t698C187A9425952910BAA66C4096E5CECF9C45C4;
// Rewired.ComponentControls.TouchRegion/itdXQhVbgPjNuvzzTkqLUeNdGDO
struct itdXQhVbgPjNuvzzTkqLUeNdGDO_t9686109095347771CDEF55C86FA148EFB32C1144;
// Rewired.ComponentControls.TouchRegion/jOGBgBryHTPCrwAWbtpjcyBVWgH
struct jOGBgBryHTPCrwAWbtpjcyBVWgH_t60095498B6581E7071A1B57B00B2727697AE5892;
// Rewired.ComponentControls.TouchRegion/pSrmmveUVOWIyFBwbhMaEmScisy
struct pSrmmveUVOWIyFBwbhMaEmScisy_t4B47DB2624A04C0C6B1093B289477A5A3506100B;
// Rewired.Components.PlayerController/AxisValueChangedHandler
struct AxisValueChangedHandler_t26F8AD336CA0516BA16CD70E0E0BE16BE4DC474D;
// Rewired.Components.PlayerController/ButtonStateChangedHandler
struct ButtonStateChangedHandler_t0531BB1117C004D092524C45A5BD445D5B15AFEC;
// Rewired.Components.PlayerController/ElementWithSourceInfo[]
struct ElementWithSourceInfoU5BU5D_t6197EBBBA7C755C6B4B97FF30FB7CDDB0C065DEE;
// Rewired.Components.PlayerController/EnabledStateChangedHandler
struct EnabledStateChangedHandler_t7892BA4CD24039FCF244401A350BF2C9367DAF43;
// Rewired.Components.PlayerMouse/ScreenPositionChangedHandler
struct ScreenPositionChangedHandler_t29C4BC8B3DB9744150268C83109151A8DAA7B2F6;
// Rewired.InputManager_Base
struct InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701;
// Rewired.Internal.StandaloneAxis
struct StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E;
// Rewired.Internal.StandaloneAxis2D
struct StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A;
// Rewired.PlayerController
struct PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01;
// Rewired.Utils.Interfaces.IRegistrar`1<Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator>
struct IRegistrar_1_tC247C6AF3FF39C5916D836E3BBCADAEBB8560A13;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Rewired.ComponentControls.TouchButton/CRqCjFCalakqbykartaanqOTwPEL>
struct Action_1_t4E78B000408BF7DAE9233B13DB3098949BB3E61B;
// System.Action`1<Rewired.ComponentControls.TouchJoystick/NHoUMtvQEGOtTovxNMKxiWCTcIN>
struct Action_1_tD0A6E282EB4EEB9F7D6D9FE4F7B149BAEE433F68;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF;
// System.Collections.Generic.List`1<Rewired.ComponentControls.CustomController/InputEvent>
struct List_1_tC3916F1B7F67B72C4C10D463D531CC7944331D43;
// System.Collections.Generic.List`1<Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator>
struct List_1_tAFD7C206B4FF8059B628B709228AFB4E6822DC68;
// System.Collections.Generic.List`1<Rewired.ComponentControls.IComponentControl>
struct List_1_tF377D1440AF55B94061969B893DABAEE87259413;
// System.Collections.Generic.List`1<Rewired.Components.PlayerController/ElementInfo>
struct List_1_t53455F9948EB8237F0F60F04F490DFDD57FBB62C;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t64BA96BFC713F221050385E91C868CE455C245D6;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Func`1<UnityEngine.Vector3>
struct Func_1_t603AE29B135DE2A9508D20577C136EE9B04F680D;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.Font
struct Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// erAqfwCNbAAvnTADjtOsmRVrVxW
struct erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4;
// gQxvLRqJyPpkclzINkoEPJOqGeY[]
struct gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/EventFunction`2<Rewired.ComponentControls.TouchInteractable/IInteractionStateTransitionHandler,Rewired.ComponentControls.TouchInteractable/InteractionStateTransitionArgs>
struct EventFunction_2_t79336E855EAE2B72B776659EF35769F0D0DA8D8A;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/EventFunction`2<Rewired.ComponentControls.TouchJoystick/IStickPositionChangedHandler,UnityEngine.Vector2>
struct EventFunction_2_t4604C1D8FE3FF11B06EC0FF84C1EEED688E6DFEC;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/EventFunction`2<Rewired.ComponentControls.TouchJoystick/IValueChangedHandler,UnityEngine.Vector2>
struct EventFunction_2_t0A48F4F300E24FBDB69326E87FD1939D5A9C1AF0;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/HierarchyEventHelper`2<Rewired.ComponentControls.TouchInteractable/IInteractionStateTransitionHandler,Rewired.ComponentControls.TouchInteractable/InteractionStateTransitionArgs>
struct HierarchyEventHelper_2_tA4972CA50CF1E3EC0959842D99260B261010D354;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/HierarchyEventHelper`2<Rewired.ComponentControls.TouchJoystick/IStickPositionChangedHandler,UnityEngine.Vector2>
struct HierarchyEventHelper_2_tDD149D49CCBE8F055ADC78F7DDF4FE4B5321FD8A;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/HierarchyEventHelper`2<Rewired.ComponentControls.TouchJoystick/IValueChangedHandler,UnityEngine.Vector2>
struct HierarchyEventHelper_2_tE08473D90510E3FE69E11A9293B95BB4D002A65E;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/HierarchyEventHelper`2<Rewired.UI.IVisibilityChangedHandler,System.Boolean>
struct HierarchyEventHelper_2_tF3ABAFBB34E9604A7251ECA9F7EC094F96DBCC52;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef AHFDTZBLGUJHJSRYBXSSRLACYGYV_T264B4C72BD56C85505397A34F5F3392FB9846011_H
#define AHFDTZBLGUJHJSRYBXSSRLACYGYV_T264B4C72BD56C85505397A34F5F3392FB9846011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.ComponentControl_AHFdtZblGUjhJsRybxSsRlACygYv
struct  AHFdtZblGUjhJsRybxSsRlACygYv_t264B4C72BD56C85505397A34F5F3392FB9846011  : public RuntimeObject
{
public:
	// System.Object Rewired.ComponentControls.ComponentControl_AHFdtZblGUjhJsRybxSsRlACygYv::NOArrsNdfKDShNFftfbPqYDzhpq
	RuntimeObject * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ComponentControls.ComponentControl_AHFdtZblGUjhJsRybxSsRlACygYv::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// Rewired.ComponentControls.ComponentControl Rewired.ComponentControls.ComponentControl_AHFdtZblGUjhJsRybxSsRlACygYv::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(AHFdtZblGUjhJsRybxSsRlACygYv_t264B4C72BD56C85505397A34F5F3392FB9846011, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline RuntimeObject * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline RuntimeObject ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(RuntimeObject * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(AHFdtZblGUjhJsRybxSsRlACygYv_t264B4C72BD56C85505397A34F5F3392FB9846011, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() { return static_cast<int32_t>(offsetof(AHFdtZblGUjhJsRybxSsRlACygYv_t264B4C72BD56C85505397A34F5F3392FB9846011, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2)); }
	inline ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2; }
	inline ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_2; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_2(ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_2 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AHFDTZBLGUJHJSRYBXSSRLACYGYV_T264B4C72BD56C85505397A34F5F3392FB9846011_H
#ifndef IBTCEJKOWGZKDVZENYNTFCLUAKWH_T291AF8978C0C4B1EFEEE5D7F0665C169CE9D8C94_H
#define IBTCEJKOWGZKDVZENYNTFCLUAKWH_T291AF8978C0C4B1EFEEE5D7F0665C169CE9D8C94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.ComponentController_IbtcEJkOwgzKDVZeNYnTFCLUAkwh
struct  IbtcEJkOwgzKDVZeNYnTFCLUAkwh_t291AF8978C0C4B1EFEEE5D7F0665C169CE9D8C94  : public RuntimeObject
{
public:
	// System.Object Rewired.ComponentControls.ComponentController_IbtcEJkOwgzKDVZeNYnTFCLUAkwh::NOArrsNdfKDShNFftfbPqYDzhpq
	RuntimeObject * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ComponentControls.ComponentController_IbtcEJkOwgzKDVZeNYnTFCLUAkwh::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// Rewired.ComponentControls.ComponentController Rewired.ComponentControls.ComponentController_IbtcEJkOwgzKDVZeNYnTFCLUAkwh::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(IbtcEJkOwgzKDVZeNYnTFCLUAkwh_t291AF8978C0C4B1EFEEE5D7F0665C169CE9D8C94, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline RuntimeObject * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline RuntimeObject ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(RuntimeObject * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(IbtcEJkOwgzKDVZeNYnTFCLUAkwh_t291AF8978C0C4B1EFEEE5D7F0665C169CE9D8C94, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() { return static_cast<int32_t>(offsetof(IbtcEJkOwgzKDVZeNYnTFCLUAkwh_t291AF8978C0C4B1EFEEE5D7F0665C169CE9D8C94, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2)); }
	inline ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2; }
	inline ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_2; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_2(ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_2 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IBTCEJKOWGZKDVZENYNTFCLUAKWH_T291AF8978C0C4B1EFEEE5D7F0665C169CE9D8C94_H
#ifndef CREATECUSTOMCONTROLLERSETTINGS_T6156FE7402633CF3BD5BA37B832FA547671F99E7_H
#define CREATECUSTOMCONTROLLERSETTINGS_T6156FE7402633CF3BD5BA37B832FA547671F99E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.CustomController_CreateCustomControllerSettings
struct  CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7  : public RuntimeObject
{
public:
	// System.Boolean Rewired.ComponentControls.CustomController_CreateCustomControllerSettings::_createCustomController
	bool ____createCustomController_0;
	// System.Int32 Rewired.ComponentControls.CustomController_CreateCustomControllerSettings::_customControllerSourceId
	int32_t ____customControllerSourceId_1;
	// System.Int32 Rewired.ComponentControls.CustomController_CreateCustomControllerSettings::_assignToPlayerId
	int32_t ____assignToPlayerId_2;
	// System.Boolean Rewired.ComponentControls.CustomController_CreateCustomControllerSettings::_destroyCustomController
	bool ____destroyCustomController_3;

public:
	inline static int32_t get_offset_of__createCustomController_0() { return static_cast<int32_t>(offsetof(CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7, ____createCustomController_0)); }
	inline bool get__createCustomController_0() const { return ____createCustomController_0; }
	inline bool* get_address_of__createCustomController_0() { return &____createCustomController_0; }
	inline void set__createCustomController_0(bool value)
	{
		____createCustomController_0 = value;
	}

	inline static int32_t get_offset_of__customControllerSourceId_1() { return static_cast<int32_t>(offsetof(CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7, ____customControllerSourceId_1)); }
	inline int32_t get__customControllerSourceId_1() const { return ____customControllerSourceId_1; }
	inline int32_t* get_address_of__customControllerSourceId_1() { return &____customControllerSourceId_1; }
	inline void set__customControllerSourceId_1(int32_t value)
	{
		____customControllerSourceId_1 = value;
	}

	inline static int32_t get_offset_of__assignToPlayerId_2() { return static_cast<int32_t>(offsetof(CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7, ____assignToPlayerId_2)); }
	inline int32_t get__assignToPlayerId_2() const { return ____assignToPlayerId_2; }
	inline int32_t* get_address_of__assignToPlayerId_2() { return &____assignToPlayerId_2; }
	inline void set__assignToPlayerId_2(int32_t value)
	{
		____assignToPlayerId_2 = value;
	}

	inline static int32_t get_offset_of__destroyCustomController_3() { return static_cast<int32_t>(offsetof(CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7, ____destroyCustomController_3)); }
	inline bool get__destroyCustomController_3() const { return ____destroyCustomController_3; }
	inline bool* get_address_of__destroyCustomController_3() { return &____destroyCustomController_3; }
	inline void set__destroyCustomController_3(bool value)
	{
		____destroyCustomController_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECUSTOMCONTROLLERSETTINGS_T6156FE7402633CF3BD5BA37B832FA547671F99E7_H
#ifndef CUSTOMCONTROLLERELEMENTTARGETSET_T5FE69FC5AAC8E3A8CD9935FEDBA55C4CB1EA091A_H
#define CUSTOMCONTROLLERELEMENTTARGETSET_T5FE69FC5AAC8E3A8CD9935FEDBA55C4CB1EA091A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Data.CustomControllerElementTargetSet
struct  CustomControllerElementTargetSet_t5FE69FC5AAC8E3A8CD9935FEDBA55C4CB1EA091A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLERELEMENTTARGETSET_T5FE69FC5AAC8E3A8CD9935FEDBA55C4CB1EA091A_H
#ifndef CUSTOMCONTROLLERSELECTOR_T28D12653D04C68A24DC36D72DACA77D35D486FE1_H
#define CUSTOMCONTROLLERSELECTOR_T28D12653D04C68A24DC36D72DACA77D35D486FE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Data.CustomControllerSelector
struct  CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1  : public RuntimeObject
{
public:
	// System.Boolean Rewired.ComponentControls.Data.CustomControllerSelector::_findUsingSourceId
	bool ____findUsingSourceId_0;
	// System.Int32 Rewired.ComponentControls.Data.CustomControllerSelector::_sourceId
	int32_t ____sourceId_1;
	// System.Boolean Rewired.ComponentControls.Data.CustomControllerSelector::_findUsingTag
	bool ____findUsingTag_2;
	// System.String Rewired.ComponentControls.Data.CustomControllerSelector::_tag
	String_t* ____tag_3;
	// System.Boolean Rewired.ComponentControls.Data.CustomControllerSelector::_findInPlayer
	bool ____findInPlayer_4;
	// System.Int32 Rewired.ComponentControls.Data.CustomControllerSelector::_playerId
	int32_t ____playerId_5;

public:
	inline static int32_t get_offset_of__findUsingSourceId_0() { return static_cast<int32_t>(offsetof(CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1, ____findUsingSourceId_0)); }
	inline bool get__findUsingSourceId_0() const { return ____findUsingSourceId_0; }
	inline bool* get_address_of__findUsingSourceId_0() { return &____findUsingSourceId_0; }
	inline void set__findUsingSourceId_0(bool value)
	{
		____findUsingSourceId_0 = value;
	}

	inline static int32_t get_offset_of__sourceId_1() { return static_cast<int32_t>(offsetof(CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1, ____sourceId_1)); }
	inline int32_t get__sourceId_1() const { return ____sourceId_1; }
	inline int32_t* get_address_of__sourceId_1() { return &____sourceId_1; }
	inline void set__sourceId_1(int32_t value)
	{
		____sourceId_1 = value;
	}

	inline static int32_t get_offset_of__findUsingTag_2() { return static_cast<int32_t>(offsetof(CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1, ____findUsingTag_2)); }
	inline bool get__findUsingTag_2() const { return ____findUsingTag_2; }
	inline bool* get_address_of__findUsingTag_2() { return &____findUsingTag_2; }
	inline void set__findUsingTag_2(bool value)
	{
		____findUsingTag_2 = value;
	}

	inline static int32_t get_offset_of__tag_3() { return static_cast<int32_t>(offsetof(CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1, ____tag_3)); }
	inline String_t* get__tag_3() const { return ____tag_3; }
	inline String_t** get_address_of__tag_3() { return &____tag_3; }
	inline void set__tag_3(String_t* value)
	{
		____tag_3 = value;
		Il2CppCodeGenWriteBarrier((&____tag_3), value);
	}

	inline static int32_t get_offset_of__findInPlayer_4() { return static_cast<int32_t>(offsetof(CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1, ____findInPlayer_4)); }
	inline bool get__findInPlayer_4() const { return ____findInPlayer_4; }
	inline bool* get_address_of__findInPlayer_4() { return &____findInPlayer_4; }
	inline void set__findInPlayer_4(bool value)
	{
		____findInPlayer_4 = value;
	}

	inline static int32_t get_offset_of__playerId_5() { return static_cast<int32_t>(offsetof(CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1, ____playerId_5)); }
	inline int32_t get__playerId_5() const { return ____playerId_5; }
	inline int32_t* get_address_of__playerId_5() { return &____playerId_5; }
	inline void set__playerId_5(int32_t value)
	{
		____playerId_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLERSELECTOR_T28D12653D04C68A24DC36D72DACA77D35D486FE1_H
#ifndef IFTMAPZFKVAPWSFRTXSXPBYDANRJ_TD8E54C1EA7EFF9F6094CC38366CD4557D57227DA_H
#define IFTMAPZFKVAPWSFRTXSXPBYDANRJ_TD8E54C1EA7EFF9F6094CC38366CD4557D57227DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchPad_IftMAPzfkVAPWSFrTxsXPBYDanRJ
struct  IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA  : public RuntimeObject
{
public:
	// System.Int32 Rewired.ComponentControls.TouchPad_IftMAPzfkVAPWSFrTxsXPBYDanRJ::HybpbdhcfSQOhFpilWKDvPgatce
	int32_t ___HybpbdhcfSQOhFpilWKDvPgatce_0;
	// Rewired.ComponentControls.TouchPad_IftMAPzfkVAPWSFrTxsXPBYDanRJ_YcVzboeRjDknSZWvRpWNyVnfSFz[] Rewired.ComponentControls.TouchPad_IftMAPzfkVAPWSFrTxsXPBYDanRJ::muHfgZrAoceOXsMhCljOQSVcJGk
	YcVzboeRjDknSZWvRpWNyVnfSFzU5BU5D_t68B3459EC9CC177F5B9658DB9A1B945DFD0F8FBB* ___muHfgZrAoceOXsMhCljOQSVcJGk_1;
	// System.Int32 Rewired.ComponentControls.TouchPad_IftMAPzfkVAPWSFrTxsXPBYDanRJ::uiElHIMsJhOUGwxstGSvtQgdJHm
	int32_t ___uiElHIMsJhOUGwxstGSvtQgdJHm_2;

public:
	inline static int32_t get_offset_of_HybpbdhcfSQOhFpilWKDvPgatce_0() { return static_cast<int32_t>(offsetof(IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA, ___HybpbdhcfSQOhFpilWKDvPgatce_0)); }
	inline int32_t get_HybpbdhcfSQOhFpilWKDvPgatce_0() const { return ___HybpbdhcfSQOhFpilWKDvPgatce_0; }
	inline int32_t* get_address_of_HybpbdhcfSQOhFpilWKDvPgatce_0() { return &___HybpbdhcfSQOhFpilWKDvPgatce_0; }
	inline void set_HybpbdhcfSQOhFpilWKDvPgatce_0(int32_t value)
	{
		___HybpbdhcfSQOhFpilWKDvPgatce_0 = value;
	}

	inline static int32_t get_offset_of_muHfgZrAoceOXsMhCljOQSVcJGk_1() { return static_cast<int32_t>(offsetof(IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA, ___muHfgZrAoceOXsMhCljOQSVcJGk_1)); }
	inline YcVzboeRjDknSZWvRpWNyVnfSFzU5BU5D_t68B3459EC9CC177F5B9658DB9A1B945DFD0F8FBB* get_muHfgZrAoceOXsMhCljOQSVcJGk_1() const { return ___muHfgZrAoceOXsMhCljOQSVcJGk_1; }
	inline YcVzboeRjDknSZWvRpWNyVnfSFzU5BU5D_t68B3459EC9CC177F5B9658DB9A1B945DFD0F8FBB** get_address_of_muHfgZrAoceOXsMhCljOQSVcJGk_1() { return &___muHfgZrAoceOXsMhCljOQSVcJGk_1; }
	inline void set_muHfgZrAoceOXsMhCljOQSVcJGk_1(YcVzboeRjDknSZWvRpWNyVnfSFzU5BU5D_t68B3459EC9CC177F5B9658DB9A1B945DFD0F8FBB* value)
	{
		___muHfgZrAoceOXsMhCljOQSVcJGk_1 = value;
		Il2CppCodeGenWriteBarrier((&___muHfgZrAoceOXsMhCljOQSVcJGk_1), value);
	}

	inline static int32_t get_offset_of_uiElHIMsJhOUGwxstGSvtQgdJHm_2() { return static_cast<int32_t>(offsetof(IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA, ___uiElHIMsJhOUGwxstGSvtQgdJHm_2)); }
	inline int32_t get_uiElHIMsJhOUGwxstGSvtQgdJHm_2() const { return ___uiElHIMsJhOUGwxstGSvtQgdJHm_2; }
	inline int32_t* get_address_of_uiElHIMsJhOUGwxstGSvtQgdJHm_2() { return &___uiElHIMsJhOUGwxstGSvtQgdJHm_2; }
	inline void set_uiElHIMsJhOUGwxstGSvtQgdJHm_2(int32_t value)
	{
		___uiElHIMsJhOUGwxstGSvtQgdJHm_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IFTMAPZFKVAPWSFRTXSXPBYDANRJ_TD8E54C1EA7EFF9F6094CC38366CD4557D57227DA_H
#ifndef YCVZBOERJDKNSZWVRPWNYVNFSFZ_TEF9716D98D3BE93F829A32A7DD90A5AC04BCED8C_H
#define YCVZBOERJDKNSZWVRPWNYVNFSFZ_TEF9716D98D3BE93F829A32A7DD90A5AC04BCED8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchPad_IftMAPzfkVAPWSFrTxsXPBYDanRJ_YcVzboeRjDknSZWvRpWNyVnfSFz
struct  YcVzboeRjDknSZWvRpWNyVnfSFz_tEF9716D98D3BE93F829A32A7DD90A5AC04BCED8C  : public RuntimeObject
{
public:
	// System.Single Rewired.ComponentControls.TouchPad_IftMAPzfkVAPWSFrTxsXPBYDanRJ_YcVzboeRjDknSZWvRpWNyVnfSFz::IPHGhnvmvweeXFsNSmpniuYFNHcM
	float ___IPHGhnvmvweeXFsNSmpniuYFNHcM_0;
	// System.Single Rewired.ComponentControls.TouchPad_IftMAPzfkVAPWSFrTxsXPBYDanRJ_YcVzboeRjDknSZWvRpWNyVnfSFz::ycICvmDyXClaVDHBtBHHgVBlmCs
	float ___ycICvmDyXClaVDHBtBHHgVBlmCs_1;
	// System.UInt32 Rewired.ComponentControls.TouchPad_IftMAPzfkVAPWSFrTxsXPBYDanRJ_YcVzboeRjDknSZWvRpWNyVnfSFz::RwULBJGzOBCTZGSEDQvzZoRYikI
	uint32_t ___RwULBJGzOBCTZGSEDQvzZoRYikI_2;

public:
	inline static int32_t get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_0() { return static_cast<int32_t>(offsetof(YcVzboeRjDknSZWvRpWNyVnfSFz_tEF9716D98D3BE93F829A32A7DD90A5AC04BCED8C, ___IPHGhnvmvweeXFsNSmpniuYFNHcM_0)); }
	inline float get_IPHGhnvmvweeXFsNSmpniuYFNHcM_0() const { return ___IPHGhnvmvweeXFsNSmpniuYFNHcM_0; }
	inline float* get_address_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_0() { return &___IPHGhnvmvweeXFsNSmpniuYFNHcM_0; }
	inline void set_IPHGhnvmvweeXFsNSmpniuYFNHcM_0(float value)
	{
		___IPHGhnvmvweeXFsNSmpniuYFNHcM_0 = value;
	}

	inline static int32_t get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_1() { return static_cast<int32_t>(offsetof(YcVzboeRjDknSZWvRpWNyVnfSFz_tEF9716D98D3BE93F829A32A7DD90A5AC04BCED8C, ___ycICvmDyXClaVDHBtBHHgVBlmCs_1)); }
	inline float get_ycICvmDyXClaVDHBtBHHgVBlmCs_1() const { return ___ycICvmDyXClaVDHBtBHHgVBlmCs_1; }
	inline float* get_address_of_ycICvmDyXClaVDHBtBHHgVBlmCs_1() { return &___ycICvmDyXClaVDHBtBHHgVBlmCs_1; }
	inline void set_ycICvmDyXClaVDHBtBHHgVBlmCs_1(float value)
	{
		___ycICvmDyXClaVDHBtBHHgVBlmCs_1 = value;
	}

	inline static int32_t get_offset_of_RwULBJGzOBCTZGSEDQvzZoRYikI_2() { return static_cast<int32_t>(offsetof(YcVzboeRjDknSZWvRpWNyVnfSFz_tEF9716D98D3BE93F829A32A7DD90A5AC04BCED8C, ___RwULBJGzOBCTZGSEDQvzZoRYikI_2)); }
	inline uint32_t get_RwULBJGzOBCTZGSEDQvzZoRYikI_2() const { return ___RwULBJGzOBCTZGSEDQvzZoRYikI_2; }
	inline uint32_t* get_address_of_RwULBJGzOBCTZGSEDQvzZoRYikI_2() { return &___RwULBJGzOBCTZGSEDQvzZoRYikI_2; }
	inline void set_RwULBJGzOBCTZGSEDQvzZoRYikI_2(uint32_t value)
	{
		___RwULBJGzOBCTZGSEDQvzZoRYikI_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YCVZBOERJDKNSZWVRPWNYVNFSFZ_TEF9716D98D3BE93F829A32A7DD90A5AC04BCED8C_H
#ifndef KEYBOARD_TF30826194FADE14B54A1B21EE260E4A572AFAC70_H
#define KEYBOARD_TF30826194FADE14B54A1B21EE260E4A572AFAC70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ThreadSafeUnityInput_Keyboard
struct  Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70  : public RuntimeObject
{
public:
	// System.Int32 Rewired.ThreadSafeUnityInput_Keyboard::uPLqzGXVtmvPTALbArYgcyquMVm
	int32_t ___uPLqzGXVtmvPTALbArYgcyquMVm_9;
	// System.Int32[] Rewired.ThreadSafeUnityInput_Keyboard::ubaErReAqjXCjFIOlvvEVepdvAK
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ubaErReAqjXCjFIOlvvEVepdvAK_10;
	// System.Boolean[] Rewired.ThreadSafeUnityInput_Keyboard::mJnuxAXBqddLsUBdtgPYHBVNIjI
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___mJnuxAXBqddLsUBdtgPYHBVNIjI_11;
	// System.Boolean Rewired.ThreadSafeUnityInput_Keyboard::eWTjKkaJVjCZqrsQHQXMtJYuNhD
	bool ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_12;
	// System.Int32 Rewired.ThreadSafeUnityInput_Keyboard::sWuSAvTOAkQhUHhzaEDbcPtXwDB
	int32_t ___sWuSAvTOAkQhUHhzaEDbcPtXwDB_13;
	// System.Boolean Rewired.ThreadSafeUnityInput_Keyboard::uFBUjqesoWEDylgvLnqGzSXhYov
	bool ___uFBUjqesoWEDylgvLnqGzSXhYov_14;

public:
	inline static int32_t get_offset_of_uPLqzGXVtmvPTALbArYgcyquMVm_9() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70, ___uPLqzGXVtmvPTALbArYgcyquMVm_9)); }
	inline int32_t get_uPLqzGXVtmvPTALbArYgcyquMVm_9() const { return ___uPLqzGXVtmvPTALbArYgcyquMVm_9; }
	inline int32_t* get_address_of_uPLqzGXVtmvPTALbArYgcyquMVm_9() { return &___uPLqzGXVtmvPTALbArYgcyquMVm_9; }
	inline void set_uPLqzGXVtmvPTALbArYgcyquMVm_9(int32_t value)
	{
		___uPLqzGXVtmvPTALbArYgcyquMVm_9 = value;
	}

	inline static int32_t get_offset_of_ubaErReAqjXCjFIOlvvEVepdvAK_10() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70, ___ubaErReAqjXCjFIOlvvEVepdvAK_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ubaErReAqjXCjFIOlvvEVepdvAK_10() const { return ___ubaErReAqjXCjFIOlvvEVepdvAK_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ubaErReAqjXCjFIOlvvEVepdvAK_10() { return &___ubaErReAqjXCjFIOlvvEVepdvAK_10; }
	inline void set_ubaErReAqjXCjFIOlvvEVepdvAK_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ubaErReAqjXCjFIOlvvEVepdvAK_10 = value;
		Il2CppCodeGenWriteBarrier((&___ubaErReAqjXCjFIOlvvEVepdvAK_10), value);
	}

	inline static int32_t get_offset_of_mJnuxAXBqddLsUBdtgPYHBVNIjI_11() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70, ___mJnuxAXBqddLsUBdtgPYHBVNIjI_11)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_mJnuxAXBqddLsUBdtgPYHBVNIjI_11() const { return ___mJnuxAXBqddLsUBdtgPYHBVNIjI_11; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_mJnuxAXBqddLsUBdtgPYHBVNIjI_11() { return &___mJnuxAXBqddLsUBdtgPYHBVNIjI_11; }
	inline void set_mJnuxAXBqddLsUBdtgPYHBVNIjI_11(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___mJnuxAXBqddLsUBdtgPYHBVNIjI_11 = value;
		Il2CppCodeGenWriteBarrier((&___mJnuxAXBqddLsUBdtgPYHBVNIjI_11), value);
	}

	inline static int32_t get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_12() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70, ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_12)); }
	inline bool get_eWTjKkaJVjCZqrsQHQXMtJYuNhD_12() const { return ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_12; }
	inline bool* get_address_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_12() { return &___eWTjKkaJVjCZqrsQHQXMtJYuNhD_12; }
	inline void set_eWTjKkaJVjCZqrsQHQXMtJYuNhD_12(bool value)
	{
		___eWTjKkaJVjCZqrsQHQXMtJYuNhD_12 = value;
	}

	inline static int32_t get_offset_of_sWuSAvTOAkQhUHhzaEDbcPtXwDB_13() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70, ___sWuSAvTOAkQhUHhzaEDbcPtXwDB_13)); }
	inline int32_t get_sWuSAvTOAkQhUHhzaEDbcPtXwDB_13() const { return ___sWuSAvTOAkQhUHhzaEDbcPtXwDB_13; }
	inline int32_t* get_address_of_sWuSAvTOAkQhUHhzaEDbcPtXwDB_13() { return &___sWuSAvTOAkQhUHhzaEDbcPtXwDB_13; }
	inline void set_sWuSAvTOAkQhUHhzaEDbcPtXwDB_13(int32_t value)
	{
		___sWuSAvTOAkQhUHhzaEDbcPtXwDB_13 = value;
	}

	inline static int32_t get_offset_of_uFBUjqesoWEDylgvLnqGzSXhYov_14() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70, ___uFBUjqesoWEDylgvLnqGzSXhYov_14)); }
	inline bool get_uFBUjqesoWEDylgvLnqGzSXhYov_14() const { return ___uFBUjqesoWEDylgvLnqGzSXhYov_14; }
	inline bool* get_address_of_uFBUjqesoWEDylgvLnqGzSXhYov_14() { return &___uFBUjqesoWEDylgvLnqGzSXhYov_14; }
	inline void set_uFBUjqesoWEDylgvLnqGzSXhYov_14(bool value)
	{
		___uFBUjqesoWEDylgvLnqGzSXhYov_14 = value;
	}
};

struct Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields
{
public:
	// System.Int32 Rewired.ThreadSafeUnityInput_Keyboard::keyValueIndex_Escape
	int32_t ___keyValueIndex_Escape_1;
	// System.Int32 Rewired.ThreadSafeUnityInput_Keyboard::keyValueIndex_Menu
	int32_t ___keyValueIndex_Menu_2;
	// System.Int32 Rewired.ThreadSafeUnityInput_Keyboard::keyValueIndex_F2
	int32_t ___keyValueIndex_F2_3;
	// System.Int32 Rewired.ThreadSafeUnityInput_Keyboard::keyValueIndex_UpArrow
	int32_t ___keyValueIndex_UpArrow_4;
	// System.Int32 Rewired.ThreadSafeUnityInput_Keyboard::keyValueIndex_RightArrow
	int32_t ___keyValueIndex_RightArrow_5;
	// System.Int32 Rewired.ThreadSafeUnityInput_Keyboard::keyValueIndex_DownArrow
	int32_t ___keyValueIndex_DownArrow_6;
	// System.Int32 Rewired.ThreadSafeUnityInput_Keyboard::keyValueIndex_LeftArrow
	int32_t ___keyValueIndex_LeftArrow_7;
	// System.Int32[] Rewired.ThreadSafeUnityInput_Keyboard::dKLIekLYibeGYzfFWuESydKiWWV
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___dKLIekLYibeGYzfFWuESydKiWWV_8;

public:
	inline static int32_t get_offset_of_keyValueIndex_Escape_1() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields, ___keyValueIndex_Escape_1)); }
	inline int32_t get_keyValueIndex_Escape_1() const { return ___keyValueIndex_Escape_1; }
	inline int32_t* get_address_of_keyValueIndex_Escape_1() { return &___keyValueIndex_Escape_1; }
	inline void set_keyValueIndex_Escape_1(int32_t value)
	{
		___keyValueIndex_Escape_1 = value;
	}

	inline static int32_t get_offset_of_keyValueIndex_Menu_2() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields, ___keyValueIndex_Menu_2)); }
	inline int32_t get_keyValueIndex_Menu_2() const { return ___keyValueIndex_Menu_2; }
	inline int32_t* get_address_of_keyValueIndex_Menu_2() { return &___keyValueIndex_Menu_2; }
	inline void set_keyValueIndex_Menu_2(int32_t value)
	{
		___keyValueIndex_Menu_2 = value;
	}

	inline static int32_t get_offset_of_keyValueIndex_F2_3() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields, ___keyValueIndex_F2_3)); }
	inline int32_t get_keyValueIndex_F2_3() const { return ___keyValueIndex_F2_3; }
	inline int32_t* get_address_of_keyValueIndex_F2_3() { return &___keyValueIndex_F2_3; }
	inline void set_keyValueIndex_F2_3(int32_t value)
	{
		___keyValueIndex_F2_3 = value;
	}

	inline static int32_t get_offset_of_keyValueIndex_UpArrow_4() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields, ___keyValueIndex_UpArrow_4)); }
	inline int32_t get_keyValueIndex_UpArrow_4() const { return ___keyValueIndex_UpArrow_4; }
	inline int32_t* get_address_of_keyValueIndex_UpArrow_4() { return &___keyValueIndex_UpArrow_4; }
	inline void set_keyValueIndex_UpArrow_4(int32_t value)
	{
		___keyValueIndex_UpArrow_4 = value;
	}

	inline static int32_t get_offset_of_keyValueIndex_RightArrow_5() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields, ___keyValueIndex_RightArrow_5)); }
	inline int32_t get_keyValueIndex_RightArrow_5() const { return ___keyValueIndex_RightArrow_5; }
	inline int32_t* get_address_of_keyValueIndex_RightArrow_5() { return &___keyValueIndex_RightArrow_5; }
	inline void set_keyValueIndex_RightArrow_5(int32_t value)
	{
		___keyValueIndex_RightArrow_5 = value;
	}

	inline static int32_t get_offset_of_keyValueIndex_DownArrow_6() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields, ___keyValueIndex_DownArrow_6)); }
	inline int32_t get_keyValueIndex_DownArrow_6() const { return ___keyValueIndex_DownArrow_6; }
	inline int32_t* get_address_of_keyValueIndex_DownArrow_6() { return &___keyValueIndex_DownArrow_6; }
	inline void set_keyValueIndex_DownArrow_6(int32_t value)
	{
		___keyValueIndex_DownArrow_6 = value;
	}

	inline static int32_t get_offset_of_keyValueIndex_LeftArrow_7() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields, ___keyValueIndex_LeftArrow_7)); }
	inline int32_t get_keyValueIndex_LeftArrow_7() const { return ___keyValueIndex_LeftArrow_7; }
	inline int32_t* get_address_of_keyValueIndex_LeftArrow_7() { return &___keyValueIndex_LeftArrow_7; }
	inline void set_keyValueIndex_LeftArrow_7(int32_t value)
	{
		___keyValueIndex_LeftArrow_7 = value;
	}

	inline static int32_t get_offset_of_dKLIekLYibeGYzfFWuESydKiWWV_8() { return static_cast<int32_t>(offsetof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields, ___dKLIekLYibeGYzfFWuESydKiWWV_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_dKLIekLYibeGYzfFWuESydKiWWV_8() const { return ___dKLIekLYibeGYzfFWuESydKiWWV_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_dKLIekLYibeGYzfFWuESydKiWWV_8() { return &___dKLIekLYibeGYzfFWuESydKiWWV_8; }
	inline void set_dKLIekLYibeGYzfFWuESydKiWWV_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___dKLIekLYibeGYzfFWuESydKiWWV_8 = value;
		Il2CppCodeGenWriteBarrier((&___dKLIekLYibeGYzfFWuESydKiWWV_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARD_TF30826194FADE14B54A1B21EE260E4A572AFAC70_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef CTEARMJZKQAJNMQJCVTSOASRPEZ_T7A1BE12CFF3F4D23E893037C183E9BE7F45D910E_H
#define CTEARMJZKQAJNMQJCVTSOASRPEZ_T7A1BE12CFF3F4D23E893037C183E9BE7F45D910E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// cteaRMjZkQaJnmQJcVTSoaSRpeZ
struct  cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E  : public RuntimeObject
{
public:
	// gQxvLRqJyPpkclzINkoEPJOqGeY[] cteaRMjZkQaJnmQJcVTSoaSRpeZ::AToGpwTZfcvfXzMlTfKzGBuSBgl
	gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495* ___AToGpwTZfcvfXzMlTfKzGBuSBgl_0;
	// gQxvLRqJyPpkclzINkoEPJOqGeY[] cteaRMjZkQaJnmQJcVTSoaSRpeZ::lGeWrhwdeiuBPdUWuRrvXnmGcKs
	gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495* ___lGeWrhwdeiuBPdUWuRrvXnmGcKs_1;
	// gQxvLRqJyPpkclzINkoEPJOqGeY[] cteaRMjZkQaJnmQJcVTSoaSRpeZ::plyGQmxYKBmkxENtzEuLgCvVCrVr
	gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495* ___plyGQmxYKBmkxENtzEuLgCvVCrVr_2;
	// gQxvLRqJyPpkclzINkoEPJOqGeY[] cteaRMjZkQaJnmQJcVTSoaSRpeZ::jjvwgcrQiCfhWMGXqaTYYuZLQV
	gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495* ___jjvwgcrQiCfhWMGXqaTYYuZLQV_3;

public:
	inline static int32_t get_offset_of_AToGpwTZfcvfXzMlTfKzGBuSBgl_0() { return static_cast<int32_t>(offsetof(cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E, ___AToGpwTZfcvfXzMlTfKzGBuSBgl_0)); }
	inline gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495* get_AToGpwTZfcvfXzMlTfKzGBuSBgl_0() const { return ___AToGpwTZfcvfXzMlTfKzGBuSBgl_0; }
	inline gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495** get_address_of_AToGpwTZfcvfXzMlTfKzGBuSBgl_0() { return &___AToGpwTZfcvfXzMlTfKzGBuSBgl_0; }
	inline void set_AToGpwTZfcvfXzMlTfKzGBuSBgl_0(gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495* value)
	{
		___AToGpwTZfcvfXzMlTfKzGBuSBgl_0 = value;
		Il2CppCodeGenWriteBarrier((&___AToGpwTZfcvfXzMlTfKzGBuSBgl_0), value);
	}

	inline static int32_t get_offset_of_lGeWrhwdeiuBPdUWuRrvXnmGcKs_1() { return static_cast<int32_t>(offsetof(cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E, ___lGeWrhwdeiuBPdUWuRrvXnmGcKs_1)); }
	inline gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495* get_lGeWrhwdeiuBPdUWuRrvXnmGcKs_1() const { return ___lGeWrhwdeiuBPdUWuRrvXnmGcKs_1; }
	inline gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495** get_address_of_lGeWrhwdeiuBPdUWuRrvXnmGcKs_1() { return &___lGeWrhwdeiuBPdUWuRrvXnmGcKs_1; }
	inline void set_lGeWrhwdeiuBPdUWuRrvXnmGcKs_1(gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495* value)
	{
		___lGeWrhwdeiuBPdUWuRrvXnmGcKs_1 = value;
		Il2CppCodeGenWriteBarrier((&___lGeWrhwdeiuBPdUWuRrvXnmGcKs_1), value);
	}

	inline static int32_t get_offset_of_plyGQmxYKBmkxENtzEuLgCvVCrVr_2() { return static_cast<int32_t>(offsetof(cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E, ___plyGQmxYKBmkxENtzEuLgCvVCrVr_2)); }
	inline gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495* get_plyGQmxYKBmkxENtzEuLgCvVCrVr_2() const { return ___plyGQmxYKBmkxENtzEuLgCvVCrVr_2; }
	inline gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495** get_address_of_plyGQmxYKBmkxENtzEuLgCvVCrVr_2() { return &___plyGQmxYKBmkxENtzEuLgCvVCrVr_2; }
	inline void set_plyGQmxYKBmkxENtzEuLgCvVCrVr_2(gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495* value)
	{
		___plyGQmxYKBmkxENtzEuLgCvVCrVr_2 = value;
		Il2CppCodeGenWriteBarrier((&___plyGQmxYKBmkxENtzEuLgCvVCrVr_2), value);
	}

	inline static int32_t get_offset_of_jjvwgcrQiCfhWMGXqaTYYuZLQV_3() { return static_cast<int32_t>(offsetof(cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E, ___jjvwgcrQiCfhWMGXqaTYYuZLQV_3)); }
	inline gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495* get_jjvwgcrQiCfhWMGXqaTYYuZLQV_3() const { return ___jjvwgcrQiCfhWMGXqaTYYuZLQV_3; }
	inline gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495** get_address_of_jjvwgcrQiCfhWMGXqaTYYuZLQV_3() { return &___jjvwgcrQiCfhWMGXqaTYYuZLQV_3; }
	inline void set_jjvwgcrQiCfhWMGXqaTYYuZLQV_3(gQxvLRqJyPpkclzINkoEPJOqGeYU5BU5D_tFA3026256C29A1B519DEC3A1A9CB6390F52F9495* value)
	{
		___jjvwgcrQiCfhWMGXqaTYYuZLQV_3 = value;
		Il2CppCodeGenWriteBarrier((&___jjvwgcrQiCfhWMGXqaTYYuZLQV_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTEARMJZKQAJNMQJCVTSOASRPEZ_T7A1BE12CFF3F4D23E893037C183E9BE7F45D910E_H
#ifndef CUSTOMCONTROLLERELEMENTTARGETSETFORBOOLEAN_T50F1A918D17080055396560627233760C1202B54_H
#define CUSTOMCONTROLLERELEMENTTARGETSETFORBOOLEAN_T50F1A918D17080055396560627233760C1202B54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForBoolean
struct  CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54  : public CustomControllerElementTargetSet_t5FE69FC5AAC8E3A8CD9935FEDBA55C4CB1EA091A
{
public:
	// Rewired.ComponentControls.Data.CustomControllerElementTarget Rewired.ComponentControls.Data.CustomControllerElementTargetSetForBoolean::_target
	CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A * ____target_1;

public:
	inline static int32_t get_offset_of__target_1() { return static_cast<int32_t>(offsetof(CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54, ____target_1)); }
	inline CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A * get__target_1() const { return ____target_1; }
	inline CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A ** get_address_of__target_1() { return &____target_1; }
	inline void set__target_1(CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A * value)
	{
		____target_1 = value;
		Il2CppCodeGenWriteBarrier((&____target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLERELEMENTTARGETSETFORBOOLEAN_T50F1A918D17080055396560627233760C1202B54_H
#ifndef CUSTOMCONTROLLERELEMENTTARGETSETFORFLOAT_T18EA66B22A9C8629E223232293BB2854CD1806D4_H
#define CUSTOMCONTROLLERELEMENTTARGETSETFORFLOAT_T18EA66B22A9C8629E223232293BB2854CD1806D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat
struct  CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4  : public CustomControllerElementTargetSet_t5FE69FC5AAC8E3A8CD9935FEDBA55C4CB1EA091A
{
public:
	// System.Boolean Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat::_splitValue
	bool ____splitValue_0;
	// Rewired.ComponentControls.Data.CustomControllerElementTarget Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat::_target
	CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A * ____target_1;
	// Rewired.ComponentControls.Data.CustomControllerElementTarget Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat::_positiveTarget
	CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A * ____positiveTarget_2;
	// Rewired.ComponentControls.Data.CustomControllerElementTarget Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat::_negativeTarget
	CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A * ____negativeTarget_3;

public:
	inline static int32_t get_offset_of__splitValue_0() { return static_cast<int32_t>(offsetof(CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4, ____splitValue_0)); }
	inline bool get__splitValue_0() const { return ____splitValue_0; }
	inline bool* get_address_of__splitValue_0() { return &____splitValue_0; }
	inline void set__splitValue_0(bool value)
	{
		____splitValue_0 = value;
	}

	inline static int32_t get_offset_of__target_1() { return static_cast<int32_t>(offsetof(CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4, ____target_1)); }
	inline CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A * get__target_1() const { return ____target_1; }
	inline CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A ** get_address_of__target_1() { return &____target_1; }
	inline void set__target_1(CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A * value)
	{
		____target_1 = value;
		Il2CppCodeGenWriteBarrier((&____target_1), value);
	}

	inline static int32_t get_offset_of__positiveTarget_2() { return static_cast<int32_t>(offsetof(CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4, ____positiveTarget_2)); }
	inline CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A * get__positiveTarget_2() const { return ____positiveTarget_2; }
	inline CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A ** get_address_of__positiveTarget_2() { return &____positiveTarget_2; }
	inline void set__positiveTarget_2(CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A * value)
	{
		____positiveTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&____positiveTarget_2), value);
	}

	inline static int32_t get_offset_of__negativeTarget_3() { return static_cast<int32_t>(offsetof(CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4, ____negativeTarget_3)); }
	inline CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A * get__negativeTarget_3() const { return ____negativeTarget_3; }
	inline CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A ** get_address_of__negativeTarget_3() { return &____negativeTarget_3; }
	inline void set__negativeTarget_3(CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A * value)
	{
		____negativeTarget_3 = value;
		Il2CppCodeGenWriteBarrier((&____negativeTarget_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLERELEMENTTARGETSETFORFLOAT_T18EA66B22A9C8629E223232293BB2854CD1806D4_H
#ifndef UIPIVOT_T80B7F0A2AF176A00B9F3DFDBF2CBA21753452CD8_H
#define UIPIVOT_T80B7F0A2AF176A00B9F3DFDBF2CBA21753452CD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.UIPivot
struct  UIPivot_t80B7F0A2AF176A00B9F3DFDBF2CBA21753452CD8 
{
public:
	// System.Single Rewired.UI.UIPivot::min
	float ___min_0;
	// System.Single Rewired.UI.UIPivot::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(UIPivot_t80B7F0A2AF176A00B9F3DFDBF2CBA21753452CD8, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(UIPivot_t80B7F0A2AF176A00B9F3DFDBF2CBA21753452CD8, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPIVOT_T80B7F0A2AF176A00B9F3DFDBF2CBA21753452CD8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#define UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifndef UNITYEVENT_1_T6745AD00086C2C8EA8A79896ABF0755C791D24F3_H
#define UNITYEVENT_1_T6745AD00086C2C8EA8A79896ABF0755C791D24F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Rewired.ComponentControls.TouchInteractable_InteractionStateTransitionArgs>
struct  UnityEvent_1_t6745AD00086C2C8EA8A79896ABF0755C791D24F3  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t6745AD00086C2C8EA8A79896ABF0755C791D24F3, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T6745AD00086C2C8EA8A79896ABF0755C791D24F3_H
#ifndef UNITYEVENT_1_T6FE5C79FD433599728A9AA732E588823AB88FDB5_H
#define UNITYEVENT_1_T6FE5C79FD433599728A9AA732E588823AB88FDB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T6FE5C79FD433599728A9AA732E588823AB88FDB5_H
#ifndef UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#define UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifndef UNITYEVENT_1_TA1BCA02C90947D3EC114C212B470D46C9FBAE511_H
#define UNITYEVENT_1_TA1BCA02C90947D3EC114C212B470D46C9FBAE511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.PointerEventData>
struct  UnityEvent_1_tA1BCA02C90947D3EC114C212B470D46C9FBAE511  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_tA1BCA02C90947D3EC114C212B470D46C9FBAE511, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TA1BCA02C90947D3EC114C212B470D46C9FBAE511_H
#ifndef UNITYEVENT_1_T88E036FD5956DB491BCC160FA57EF4F9584042B9_H
#define UNITYEVENT_1_T88E036FD5956DB491BCC160FA57EF4F9584042B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T88E036FD5956DB491BCC160FA57EF4F9584042B9_H
#ifndef UNITYEVENT_2_TABD337424A0CF2C1045CEC7C53EC87B1B148F551_H
#define UNITYEVENT_2_TABD337424A0CF2C1045CEC7C53EC87B1B148F551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Int32,System.Boolean>
struct  UnityEvent_2_tABD337424A0CF2C1045CEC7C53EC87B1B148F551  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_tABD337424A0CF2C1045CEC7C53EC87B1B148F551, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_TABD337424A0CF2C1045CEC7C53EC87B1B148F551_H
#ifndef UNITYEVENT_2_T3032939B848435863162B30B27D47065AE55DA6E_H
#define UNITYEVENT_2_T3032939B848435863162B30B27D47065AE55DA6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Int32,System.Single>
struct  UnityEvent_2_t3032939B848435863162B30B27D47065AE55DA6E  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t3032939B848435863162B30B27D47065AE55DA6E, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T3032939B848435863162B30B27D47065AE55DA6E_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#define SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_HighlightedSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_PressedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_DisabledSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#define AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisCoordinateMode
struct  AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868 
{
public:
	// System.Int32 Rewired.AxisCoordinateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifndef ELEMENTTYPE_T4F552D509AC9C2D4FCD592F75CCAA294DA689BCC_H
#define ELEMENTTYPE_T4F552D509AC9C2D4FCD592F75CCAA294DA689BCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Data.CustomControllerElementSelector_ElementType
struct  ElementType_t4F552D509AC9C2D4FCD592F75CCAA294DA689BCC 
{
public:
	// System.Int32 Rewired.ComponentControls.Data.CustomControllerElementSelector_ElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementType_t4F552D509AC9C2D4FCD592F75CCAA294DA689BCC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTTYPE_T4F552D509AC9C2D4FCD592F75CCAA294DA689BCC_H
#ifndef SELECTORTYPE_T0A167C494778CC074B1CF92F6D624B489D934E44_H
#define SELECTORTYPE_T0A167C494778CC074B1CF92F6D624B489D934E44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Data.CustomControllerElementSelector_SelectorType
struct  SelectorType_t0A167C494778CC074B1CF92F6D624B489D934E44 
{
public:
	// System.Int32 Rewired.ComponentControls.Data.CustomControllerElementSelector_SelectorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectorType_t0A167C494778CC074B1CF92F6D624B489D934E44, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTORTYPE_T0A167C494778CC074B1CF92F6D624B489D934E44_H
#ifndef VALUERANGE_T87A04DAE61E9550A2F8D39BCDD5225FD6317EE2B_H
#define VALUERANGE_T87A04DAE61E9550A2F8D39BCDD5225FD6317EE2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Data.CustomControllerElementTarget_ValueRange
struct  ValueRange_t87A04DAE61E9550A2F8D39BCDD5225FD6317EE2B 
{
public:
	// System.Int32 Rewired.ComponentControls.Data.CustomControllerElementTarget_ValueRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ValueRange_t87A04DAE61E9550A2F8D39BCDD5225FD6317EE2B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUERANGE_T87A04DAE61E9550A2F8D39BCDD5225FD6317EE2B_H
#ifndef ROTATIONAXIS_T7534A0B5F153076E2CE1EBCAA5A9A636A093BE92_H
#define ROTATIONAXIS_T7534A0B5F153076E2CE1EBCAA5A9A636A093BE92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Effects.RotateAroundAxis_RotationAxis
struct  RotationAxis_t7534A0B5F153076E2CE1EBCAA5A9A636A093BE92 
{
public:
	// System.Int32 Rewired.ComponentControls.Effects.RotateAroundAxis_RotationAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotationAxis_t7534A0B5F153076E2CE1EBCAA5A9A636A093BE92, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONAXIS_T7534A0B5F153076E2CE1EBCAA5A9A636A093BE92_H
#ifndef SPEED_T302FF2DB0BAF962B98E90BF0901B05A0B0072F92_H
#define SPEED_T302FF2DB0BAF962B98E90BF0901B05A0B0072F92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Effects.RotateAroundAxis_Speed
struct  Speed_t302FF2DB0BAF962B98E90BF0901B05A0B0072F92 
{
public:
	// System.Int32 Rewired.ComponentControls.Effects.RotateAroundAxis_Speed::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Speed_t302FF2DB0BAF962B98E90BF0901B05A0B0072F92, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEED_T302FF2DB0BAF962B98E90BF0901B05A0B0072F92_H
#ifndef TILTDIRECTION_T845536D7B05018F819DC404C83D5288B2C5F9F98_H
#define TILTDIRECTION_T845536D7B05018F819DC404C83D5288B2C5F9F98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TiltControl_TiltDirection
struct  TiltDirection_t845536D7B05018F819DC404C83D5288B2C5F9F98 
{
public:
	// System.Int32 Rewired.ComponentControls.TiltControl_TiltDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TiltDirection_t845536D7B05018F819DC404C83D5288B2C5F9F98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTDIRECTION_T845536D7B05018F819DC404C83D5288B2C5F9F98_H
#ifndef AXISVALUECHANGEDEVENTHANDLER_T622BC4F8C94A13837769CEFE1FD12CC18B926791_H
#define AXISVALUECHANGEDEVENTHANDLER_T622BC4F8C94A13837769CEFE1FD12CC18B926791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchButton_AxisValueChangedEventHandler
struct  AxisValueChangedEventHandler_t622BC4F8C94A13837769CEFE1FD12CC18B926791  : public UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISVALUECHANGEDEVENTHANDLER_T622BC4F8C94A13837769CEFE1FD12CC18B926791_H
#ifndef BUTTONDOWNEVENTHANDLER_TD3FB726C5A3E17A89FA7992F2D0D0422286B7E7D_H
#define BUTTONDOWNEVENTHANDLER_TD3FB726C5A3E17A89FA7992F2D0D0422286B7E7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchButton_ButtonDownEventHandler
struct  ButtonDownEventHandler_tD3FB726C5A3E17A89FA7992F2D0D0422286B7E7D  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONDOWNEVENTHANDLER_TD3FB726C5A3E17A89FA7992F2D0D0422286B7E7D_H
#ifndef BUTTONTYPE_TCA2796A69EDE4183905636CE5738CD1D06F9E39B_H
#define BUTTONTYPE_TCA2796A69EDE4183905636CE5738CD1D06F9E39B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchButton_ButtonType
struct  ButtonType_tCA2796A69EDE4183905636CE5738CD1D06F9E39B 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchButton_ButtonType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonType_tCA2796A69EDE4183905636CE5738CD1D06F9E39B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONTYPE_TCA2796A69EDE4183905636CE5738CD1D06F9E39B_H
#ifndef BUTTONUPEVENTHANDLER_T529887EE63C2C416911C6C64F8D230008EB73339_H
#define BUTTONUPEVENTHANDLER_T529887EE63C2C416911C6C64F8D230008EB73339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchButton_ButtonUpEventHandler
struct  ButtonUpEventHandler_t529887EE63C2C416911C6C64F8D230008EB73339  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONUPEVENTHANDLER_T529887EE63C2C416911C6C64F8D230008EB73339_H
#ifndef BUTTONVALUECHANGEDEVENTHANDLER_TB3FD14B60CDD8A2E3B8D8B26F951B056495B5A50_H
#define BUTTONVALUECHANGEDEVENTHANDLER_TB3FD14B60CDD8A2E3B8D8B26F951B056495B5A50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchButton_ButtonValueChangedEventHandler
struct  ButtonValueChangedEventHandler_tB3FD14B60CDD8A2E3B8D8B26F951B056495B5A50  : public UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONVALUECHANGEDEVENTHANDLER_TB3FD14B60CDD8A2E3B8D8B26F951B056495B5A50_H
#ifndef CRQCJFCALAKQBYKARTAANQOTWPEL_TB97B494B677C52047A13AF2D1089D10B855F70CA_H
#define CRQCJFCALAKQBYKARTAANQOTWPEL_TB97B494B677C52047A13AF2D1089D10B855F70CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchButton_CRqCjFCalakqbykartaanqOTwPEL
struct  CRqCjFCalakqbykartaanqOTwPEL_tB97B494B677C52047A13AF2D1089D10B855F70CA 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchButton_CRqCjFCalakqbykartaanqOTwPEL::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CRqCjFCalakqbykartaanqOTwPEL_tB97B494B677C52047A13AF2D1089D10B855F70CA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRQCJFCALAKQBYKARTAANQOTWPEL_TB97B494B677C52047A13AF2D1089D10B855F70CA_H
#ifndef IUGSMPNEKJYQLHIYODPFOTTCQPE_TA1AFA539D16AD8DC6479AF8DDA006C37B0A2D43A_H
#define IUGSMPNEKJYQLHIYODPFOTTCQPE_TA1AFA539D16AD8DC6479AF8DDA006C37B0A2D43A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchButton_IUgSMPnekjYQlhIyodPFoTtCQpE
struct  IUgSMPnekjYQlhIyodPFoTtCQpE_tA1AFA539D16AD8DC6479AF8DDA006C37B0A2D43A 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchButton_IUgSMPnekjYQlhIyodPFoTtCQpE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IUgSMPnekjYQlhIyodPFoTtCQpE_tA1AFA539D16AD8DC6479AF8DDA006C37B0A2D43A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IUGSMPNEKJYQLHIYODPFOTTCQPE_TA1AFA539D16AD8DC6479AF8DDA006C37B0A2D43A_H
#ifndef INTERACTIONSTATE_TF692B7740730EF1B1F625EEDA7610B14938DFED2_H
#define INTERACTIONSTATE_TF692B7740730EF1B1F625EEDA7610B14938DFED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchInteractable_InteractionState
struct  InteractionState_tF692B7740730EF1B1F625EEDA7610B14938DFED2 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchInteractable_InteractionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractionState_tF692B7740730EF1B1F625EEDA7610B14938DFED2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSTATE_TF692B7740730EF1B1F625EEDA7610B14938DFED2_H
#ifndef INTERACTIONSTATETRANSITIONEVENTHANDLER_TC1D00C17A8A6547002894B4718BD039679204D36_H
#define INTERACTIONSTATETRANSITIONEVENTHANDLER_TC1D00C17A8A6547002894B4718BD039679204D36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchInteractable_InteractionStateTransitionEventHandler
struct  InteractionStateTransitionEventHandler_tC1D00C17A8A6547002894B4718BD039679204D36  : public UnityEvent_1_t6745AD00086C2C8EA8A79896ABF0755C791D24F3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSTATETRANSITIONEVENTHANDLER_TC1D00C17A8A6547002894B4718BD039679204D36_H
#ifndef MOUSEBUTTONFLAGS_TC652D180E7354654F1CDBB43ECEA0B09A7ADC8C6_H
#define MOUSEBUTTONFLAGS_TC652D180E7354654F1CDBB43ECEA0B09A7ADC8C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchInteractable_MouseButtonFlags
struct  MouseButtonFlags_tC652D180E7354654F1CDBB43ECEA0B09A7ADC8C6 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchInteractable_MouseButtonFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MouseButtonFlags_tC652D180E7354654F1CDBB43ECEA0B09A7ADC8C6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONFLAGS_TC652D180E7354654F1CDBB43ECEA0B09A7ADC8C6_H
#ifndef TRANSITIONTYPEFLAGS_T3AEF78427B33BF979944184C868F9C98B6F39662_H
#define TRANSITIONTYPEFLAGS_T3AEF78427B33BF979944184C868F9C98B6F39662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchInteractable_TransitionTypeFlags
struct  TransitionTypeFlags_t3AEF78427B33BF979944184C868F9C98B6F39662 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchInteractable_TransitionTypeFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransitionTypeFlags_t3AEF78427B33BF979944184C868F9C98B6F39662, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITIONTYPEFLAGS_T3AEF78427B33BF979944184C868F9C98B6F39662_H
#ifndef VISIBILITYCHANGEDEVENTHANDLER_T3D6E9B1BC6606FEC892CE393EDD4D9ED89DE9D28_H
#define VISIBILITYCHANGEDEVENTHANDLER_T3D6E9B1BC6606FEC892CE393EDD4D9ED89DE9D28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchInteractable_VisibilityChangedEventHandler
struct  VisibilityChangedEventHandler_t3D6E9B1BC6606FEC892CE393EDD4D9ED89DE9D28  : public UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITYCHANGEDEVENTHANDLER_T3D6E9B1BC6606FEC892CE393EDD4D9ED89DE9D28_H
#ifndef AXISDIRECTION_TCD8E4443AA3EE2AE12039BCD2D74D246C1390C74_H
#define AXISDIRECTION_TCD8E4443AA3EE2AE12039BCD2D74D246C1390C74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick_AxisDirection
struct  AxisDirection_tCD8E4443AA3EE2AE12039BCD2D74D246C1390C74 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchJoystick_AxisDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisDirection_tCD8E4443AA3EE2AE12039BCD2D74D246C1390C74, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISDIRECTION_TCD8E4443AA3EE2AE12039BCD2D74D246C1390C74_H
#ifndef GQMWMWTRNQTHMSUCWNHLOWMZATS_T8BD164B75F6D445AD35941ECB547B6C428C71D18_H
#define GQMWMWTRNQTHMSUCWNHLOWMZATS_T8BD164B75F6D445AD35941ECB547B6C428C71D18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick_GQMwmwtRNqThmSuCwNhLowMzATs
struct  GQMwmwtRNqThmSuCwNhLowMzATs_t8BD164B75F6D445AD35941ECB547B6C428C71D18 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchJoystick_GQMwmwtRNqThmSuCwNhLowMzATs::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GQMwmwtRNqThmSuCwNhLowMzATs_t8BD164B75F6D445AD35941ECB547B6C428C71D18, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GQMWMWTRNQTHMSUCWNHLOWMZATS_T8BD164B75F6D445AD35941ECB547B6C428C71D18_H
#ifndef JOYSTICKMODE_T74D57E8CEC39EA951D6113585329E61E9F5613D8_H
#define JOYSTICKMODE_T74D57E8CEC39EA951D6113585329E61E9F5613D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick_JoystickMode
struct  JoystickMode_t74D57E8CEC39EA951D6113585329E61E9F5613D8 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchJoystick_JoystickMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JoystickMode_t74D57E8CEC39EA951D6113585329E61E9F5613D8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKMODE_T74D57E8CEC39EA951D6113585329E61E9F5613D8_H
#ifndef NHOUMTVQEGOTTOVXNMKXIWCTCIN_T380A42EB78513E9246FEC0675D2974000BA9A147_H
#define NHOUMTVQEGOTTOVXNMKXIWCTCIN_T380A42EB78513E9246FEC0675D2974000BA9A147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick_NHoUMtvQEGOtTovxNMKxiWCTcIN
struct  NHoUMtvQEGOtTovxNMKxiWCTcIN_t380A42EB78513E9246FEC0675D2974000BA9A147 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchJoystick_NHoUMtvQEGOtTovxNMKxiWCTcIN::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NHoUMtvQEGOtTovxNMKxiWCTcIN_t380A42EB78513E9246FEC0675D2974000BA9A147, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NHOUMTVQEGOTTOVXNMKXIWCTCIN_T380A42EB78513E9246FEC0675D2974000BA9A147_H
#ifndef SNAPDIRECTIONS_TE4DF36DCA8FBCE6CA238E4F0B0995544F8852A65_H
#define SNAPDIRECTIONS_TE4DF36DCA8FBCE6CA238E4F0B0995544F8852A65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick_SnapDirections
struct  SnapDirections_tE4DF36DCA8FBCE6CA238E4F0B0995544F8852A65 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchJoystick_SnapDirections::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SnapDirections_tE4DF36DCA8FBCE6CA238E4F0B0995544F8852A65, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SNAPDIRECTIONS_TE4DF36DCA8FBCE6CA238E4F0B0995544F8852A65_H
#ifndef STICKBOUNDS_TC34F0212ECA58EB32784789FCF7DA246E08ED230_H
#define STICKBOUNDS_TC34F0212ECA58EB32784789FCF7DA246E08ED230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick_StickBounds
struct  StickBounds_tC34F0212ECA58EB32784789FCF7DA246E08ED230 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchJoystick_StickBounds::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StickBounds_tC34F0212ECA58EB32784789FCF7DA246E08ED230, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STICKBOUNDS_TC34F0212ECA58EB32784789FCF7DA246E08ED230_H
#ifndef STICKPOSITIONCHANGEDEVENTHANDLER_TD962BE6C58EEDC9CA9949ACBF5F61EDC922BF395_H
#define STICKPOSITIONCHANGEDEVENTHANDLER_TD962BE6C58EEDC9CA9949ACBF5F61EDC922BF395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick_StickPositionChangedEventHandler
struct  StickPositionChangedEventHandler_tD962BE6C58EEDC9CA9949ACBF5F61EDC922BF395  : public UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STICKPOSITIONCHANGEDEVENTHANDLER_TD962BE6C58EEDC9CA9949ACBF5F61EDC922BF395_H
#ifndef TAPEVENTHANDLER_TE705770344A52DD95176406B4B00E5EE8E525B2F_H
#define TAPEVENTHANDLER_TE705770344A52DD95176406B4B00E5EE8E525B2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick_TapEventHandler
struct  TapEventHandler_tE705770344A52DD95176406B4B00E5EE8E525B2F  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPEVENTHANDLER_TE705770344A52DD95176406B4B00E5EE8E525B2F_H
#ifndef TOUCHENDEDEVENTHANDLER_T05BD8DD2BB21689F364B8300E6A1F266093E111C_H
#define TOUCHENDEDEVENTHANDLER_T05BD8DD2BB21689F364B8300E6A1F266093E111C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick_TouchEndedEventHandler
struct  TouchEndedEventHandler_t05BD8DD2BB21689F364B8300E6A1F266093E111C  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHENDEDEVENTHANDLER_T05BD8DD2BB21689F364B8300E6A1F266093E111C_H
#ifndef TOUCHSTARTEDEVENTHANDLER_TC39C61D509D6C9452386D0D05B7782F35841D4AB_H
#define TOUCHSTARTEDEVENTHANDLER_TC39C61D509D6C9452386D0D05B7782F35841D4AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick_TouchStartedEventHandler
struct  TouchStartedEventHandler_tC39C61D509D6C9452386D0D05B7782F35841D4AB  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSTARTEDEVENTHANDLER_TC39C61D509D6C9452386D0D05B7782F35841D4AB_H
#ifndef VALUECHANGEDEVENTHANDLER_T739FAF55A04B824B9182F47969314C660604524B_H
#define VALUECHANGEDEVENTHANDLER_T739FAF55A04B824B9182F47969314C660604524B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick_ValueChangedEventHandler
struct  ValueChangedEventHandler_t739FAF55A04B824B9182F47969314C660604524B  : public UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECHANGEDEVENTHANDLER_T739FAF55A04B824B9182F47969314C660604524B_H
#ifndef AXISDIRECTION_TDFDCCB740A5EA7151F47AB1DE1F1360010912247_H
#define AXISDIRECTION_TDFDCCB740A5EA7151F47AB1DE1F1360010912247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchPad_AxisDirection
struct  AxisDirection_tDFDCCB740A5EA7151F47AB1DE1F1360010912247 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchPad_AxisDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisDirection_tDFDCCB740A5EA7151F47AB1DE1F1360010912247, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISDIRECTION_TDFDCCB740A5EA7151F47AB1DE1F1360010912247_H
#ifndef PRESSDOWNEVENTHANDLER_TADC629B1DD822026CE2E06E3CDA7FB6D4E495F8D_H
#define PRESSDOWNEVENTHANDLER_TADC629B1DD822026CE2E06E3CDA7FB6D4E495F8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchPad_PressDownEventHandler
struct  PressDownEventHandler_tADC629B1DD822026CE2E06E3CDA7FB6D4E495F8D  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESSDOWNEVENTHANDLER_TADC629B1DD822026CE2E06E3CDA7FB6D4E495F8D_H
#ifndef PRESSUPEVENTHANDLER_T4125670E8C4E9682843D4F1D030ACF428FD397EC_H
#define PRESSUPEVENTHANDLER_T4125670E8C4E9682843D4F1D030ACF428FD397EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchPad_PressUpEventHandler
struct  PressUpEventHandler_t4125670E8C4E9682843D4F1D030ACF428FD397EC  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESSUPEVENTHANDLER_T4125670E8C4E9682843D4F1D030ACF428FD397EC_H
#ifndef TAPEVENTHANDLER_T33FC3FB8B60BF60104F3569F86657ADDB6BC0944_H
#define TAPEVENTHANDLER_T33FC3FB8B60BF60104F3569F86657ADDB6BC0944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchPad_TapEventHandler
struct  TapEventHandler_t33FC3FB8B60BF60104F3569F86657ADDB6BC0944  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPEVENTHANDLER_T33FC3FB8B60BF60104F3569F86657ADDB6BC0944_H
#ifndef TOUCHPADMODE_T686F891A5C5D94E96BB5A41CBBA163893A673E06_H
#define TOUCHPADMODE_T686F891A5C5D94E96BB5A41CBBA163893A673E06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchPad_TouchPadMode
struct  TouchPadMode_t686F891A5C5D94E96BB5A41CBBA163893A673E06 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchPad_TouchPadMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPadMode_t686F891A5C5D94E96BB5A41CBBA163893A673E06, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPADMODE_T686F891A5C5D94E96BB5A41CBBA163893A673E06_H
#ifndef VALUECHANGEDEVENTHANDLER_T5D816B57AB2EEC7ACA0BE8CFB398EE5EDD7E32C4_H
#define VALUECHANGEDEVENTHANDLER_T5D816B57AB2EEC7ACA0BE8CFB398EE5EDD7E32C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchPad_ValueChangedEventHandler
struct  ValueChangedEventHandler_t5D816B57AB2EEC7ACA0BE8CFB398EE5EDD7E32C4  : public UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECHANGEDEVENTHANDLER_T5D816B57AB2EEC7ACA0BE8CFB398EE5EDD7E32C4_H
#ifndef VALUEFORMAT_TD974E10B7AD1AC0E4E312035DE1D83F3BA01E16D_H
#define VALUEFORMAT_TD974E10B7AD1AC0E4E312035DE1D83F3BA01E16D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchPad_ValueFormat
struct  ValueFormat_tD974E10B7AD1AC0E4E312035DE1D83F3BA01E16D 
{
public:
	// System.Int32 Rewired.ComponentControls.TouchPad_ValueFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ValueFormat_tD974E10B7AD1AC0E4E312035DE1D83F3BA01E16D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEFORMAT_TD974E10B7AD1AC0E4E312035DE1D83F3BA01E16D_H
#ifndef CUBRKEGPTMQLOEPIGJPGZVFTMEZ_T62B6522D584CD2A21FCD80D3C3CAED97E95E8D13_H
#define CUBRKEGPTMQLOEPIGJPGZVFTMEZ_T62B6522D584CD2A21FCD80D3C3CAED97E95E8D13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchRegion_CUBRkEgPtmqloePiGjPGzvftMEz
struct  CUBRkEgPtmqloePiGjPGzvftMEz_t62B6522D584CD2A21FCD80D3C3CAED97E95E8D13  : public UnityEvent_1_tA1BCA02C90947D3EC114C212B470D46C9FBAE511
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBRKEGPTMQLOEPIGJPGZVFTMEZ_T62B6522D584CD2A21FCD80D3C3CAED97E95E8D13_H
#ifndef DBJAHTCSNUWTCSSMKOQHADEGPFX_T1824811CE094ED8DE5F842353B1C13B05116152D_H
#define DBJAHTCSNUWTCSSMKOQHADEGPFX_T1824811CE094ED8DE5F842353B1C13B05116152D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchRegion_DbjAHtCsNuwTcsSMKOqHaDEgpfX
struct  DbjAHtCsNuwTcsSMKOqHaDEgpfX_t1824811CE094ED8DE5F842353B1C13B05116152D  : public UnityEvent_1_tA1BCA02C90947D3EC114C212B470D46C9FBAE511
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DBJAHTCSNUWTCSSMKOQHADEGPFX_T1824811CE094ED8DE5F842353B1C13B05116152D_H
#ifndef SPNSXRODGIUWSAIFKMMMQPNABEH_TC64B3B02AA7E79A9CAE0BA33FE4F96AC5E5405A1_H
#define SPNSXRODGIUWSAIFKMMMQPNABEH_TC64B3B02AA7E79A9CAE0BA33FE4F96AC5E5405A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchRegion_SpNSxRoDgiUWSAIfKMmMQPNAbEH
struct  SpNSxRoDgiUWSAIfKMmMQPNAbEH_tC64B3B02AA7E79A9CAE0BA33FE4F96AC5E5405A1  : public UnityEvent_1_tA1BCA02C90947D3EC114C212B470D46C9FBAE511
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPNSXRODGIUWSAIFKMMMQPNABEH_TC64B3B02AA7E79A9CAE0BA33FE4F96AC5E5405A1_H
#ifndef GWVGYLYAAHINGOQYXKSBNZSEJJP_T698C187A9425952910BAA66C4096E5CECF9C45C4_H
#define GWVGYLYAAHINGOQYXKSBNZSEJJP_T698C187A9425952910BAA66C4096E5CECF9C45C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchRegion_gWVGylYAAhiNGOqYxKsBnzSeJJp
struct  gWVGylYAAhiNGOqYxKsBnzSeJJp_t698C187A9425952910BAA66C4096E5CECF9C45C4  : public UnityEvent_1_tA1BCA02C90947D3EC114C212B470D46C9FBAE511
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GWVGYLYAAHINGOQYXKSBNZSEJJP_T698C187A9425952910BAA66C4096E5CECF9C45C4_H
#ifndef ITDXQHVBGPJNUVZZTKQLUENDGDO_T9686109095347771CDEF55C86FA148EFB32C1144_H
#define ITDXQHVBGPJNUVZZTKQLUENDGDO_T9686109095347771CDEF55C86FA148EFB32C1144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchRegion_itdXQhVbgPjNuvzzTkqLUeNdGDO
struct  itdXQhVbgPjNuvzzTkqLUeNdGDO_t9686109095347771CDEF55C86FA148EFB32C1144  : public UnityEvent_1_tA1BCA02C90947D3EC114C212B470D46C9FBAE511
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITDXQHVBGPJNUVZZTKQLUENDGDO_T9686109095347771CDEF55C86FA148EFB32C1144_H
#ifndef JOGBGBRYHTPCRWAWBTPJCYBVWGH_T60095498B6581E7071A1B57B00B2727697AE5892_H
#define JOGBGBRYHTPCRWAWBTPJCYBVWGH_T60095498B6581E7071A1B57B00B2727697AE5892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchRegion_jOGBgBryHTPCrwAWbtpjcyBVWgH
struct  jOGBgBryHTPCrwAWbtpjcyBVWgH_t60095498B6581E7071A1B57B00B2727697AE5892  : public UnityEvent_1_tA1BCA02C90947D3EC114C212B470D46C9FBAE511
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOGBGBRYHTPCRWAWBTPJCYBVWGH_T60095498B6581E7071A1B57B00B2727697AE5892_H
#ifndef PSRMMVEUVOWIYFBWBHMAEMSCISY_T4B47DB2624A04C0C6B1093B289477A5A3506100B_H
#define PSRMMVEUVOWIYFBWBHMAEMSCISY_T4B47DB2624A04C0C6B1093B289477A5A3506100B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchRegion_pSrmmveUVOWIyFBwbhMaEmScisy
struct  pSrmmveUVOWIyFBwbhMaEmScisy_t4B47DB2624A04C0C6B1093B289477A5A3506100B  : public UnityEvent_1_tA1BCA02C90947D3EC114C212B470D46C9FBAE511
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PSRMMVEUVOWIYFBWBHMAEMSCISY_T4B47DB2624A04C0C6B1093B289477A5A3506100B_H
#ifndef AXISVALUECHANGEDHANDLER_T26F8AD336CA0516BA16CD70E0E0BE16BE4DC474D_H
#define AXISVALUECHANGEDHANDLER_T26F8AD336CA0516BA16CD70E0E0BE16BE4DC474D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Components.PlayerController_AxisValueChangedHandler
struct  AxisValueChangedHandler_t26F8AD336CA0516BA16CD70E0E0BE16BE4DC474D  : public UnityEvent_2_t3032939B848435863162B30B27D47065AE55DA6E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISVALUECHANGEDHANDLER_T26F8AD336CA0516BA16CD70E0E0BE16BE4DC474D_H
#ifndef BUTTONSTATECHANGEDHANDLER_T0531BB1117C004D092524C45A5BD445D5B15AFEC_H
#define BUTTONSTATECHANGEDHANDLER_T0531BB1117C004D092524C45A5BD445D5B15AFEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Components.PlayerController_ButtonStateChangedHandler
struct  ButtonStateChangedHandler_t0531BB1117C004D092524C45A5BD445D5B15AFEC  : public UnityEvent_2_tABD337424A0CF2C1045CEC7C53EC87B1B148F551
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATECHANGEDHANDLER_T0531BB1117C004D092524C45A5BD445D5B15AFEC_H
#ifndef ENABLEDSTATECHANGEDHANDLER_T7892BA4CD24039FCF244401A350BF2C9367DAF43_H
#define ENABLEDSTATECHANGEDHANDLER_T7892BA4CD24039FCF244401A350BF2C9367DAF43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Components.PlayerController_EnabledStateChangedHandler
struct  EnabledStateChangedHandler_t7892BA4CD24039FCF244401A350BF2C9367DAF43  : public UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEDSTATECHANGEDHANDLER_T7892BA4CD24039FCF244401A350BF2C9367DAF43_H
#ifndef SCREENPOSITIONCHANGEDHANDLER_T29C4BC8B3DB9744150268C83109151A8DAA7B2F6_H
#define SCREENPOSITIONCHANGEDHANDLER_T29C4BC8B3DB9744150268C83109151A8DAA7B2F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Components.PlayerMouse_ScreenPositionChangedHandler
struct  ScreenPositionChangedHandler_t29C4BC8B3DB9744150268C83109151A8DAA7B2F6  : public UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENPOSITIONCHANGEDHANDLER_T29C4BC8B3DB9744150268C83109151A8DAA7B2F6_H
#ifndef UPDATELOOPSETTING_T813FEF786E2423CBB40457E67ABB4712DA865CA5_H
#define UPDATELOOPSETTING_T813FEF786E2423CBB40457E67ABB4712DA865CA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Config.UpdateLoopSetting
struct  UpdateLoopSetting_t813FEF786E2423CBB40457E67ABB4712DA865CA5 
{
public:
	// System.Int32 Rewired.Config.UpdateLoopSetting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateLoopSetting_t813FEF786E2423CBB40457E67ABB4712DA865CA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATELOOPSETTING_T813FEF786E2423CBB40457E67ABB4712DA865CA5_H
#ifndef TYPE_TAC1607CC90FDFD13003AF557B815C0332390D80C_H
#define TYPE_TAC1607CC90FDFD13003AF557B815C0332390D80C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Element_Type
struct  Type_tAC1607CC90FDFD13003AF557B815C0332390D80C 
{
public:
	// System.Int32 Rewired.PlayerController_Element_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tAC1607CC90FDFD13003AF557B815C0332390D80C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_TAC1607CC90FDFD13003AF557B815C0332390D80C_H
#ifndef TYPEWITHSOURCE_T18AB19C95A16F87755AB2F248CEB6A5E507E7667_H
#define TYPEWITHSOURCE_T18AB19C95A16F87755AB2F248CEB6A5E507E7667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Element_TypeWithSource
struct  TypeWithSource_t18AB19C95A16F87755AB2F248CEB6A5E507E7667 
{
public:
	// System.Int32 Rewired.PlayerController_Element_TypeWithSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeWithSource_t18AB19C95A16F87755AB2F248CEB6A5E507E7667, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEWITHSOURCE_T18AB19C95A16F87755AB2F248CEB6A5E507E7667_H
#ifndef MOVEMENTAREAUNIT_T25F2042B52F33CFAE132EB793F341081375940C1_H
#define MOVEMENTAREAUNIT_T25F2042B52F33CFAE132EB793F341081375940C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerMouse_MovementAreaUnit
struct  MovementAreaUnit_t25F2042B52F33CFAE132EB793F341081375940C1 
{
public:
	// System.Int32 Rewired.PlayerMouse_MovementAreaUnit::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MovementAreaUnit_t25F2042B52F33CFAE132EB793F341081375940C1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTAREAUNIT_T25F2042B52F33CFAE132EB793F341081375940C1_H
#ifndef POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#define POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Pole
struct  Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C 
{
public:
	// System.Int32 Rewired.Pole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifndef MOUSE_TF43362A0D8B9780699E590990FCA13DB7BE0DFE9_H
#define MOUSE_TF43362A0D8B9780699E590990FCA13DB7BE0DFE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ThreadSafeUnityInput_Mouse
struct  Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9  : public RuntimeObject
{
public:
	// System.Boolean[] Rewired.ThreadSafeUnityInput_Mouse::QjgkqxyMBXGPcdjZSGsOcXzPxUl
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_2;
	// System.Single[] Rewired.ThreadSafeUnityInput_Mouse::xwGjMDxksyYaKvqOwHfPiiWNyUq
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___xwGjMDxksyYaKvqOwHfPiiWNyUq_3;
	// System.Int32 Rewired.ThreadSafeUnityInput_Mouse::sWuSAvTOAkQhUHhzaEDbcPtXwDB
	int32_t ___sWuSAvTOAkQhUHhzaEDbcPtXwDB_4;
	// UnityEngine.Vector3 Rewired.ThreadSafeUnityInput_Mouse::kZqclMjeSxqHcUjffDBtNDosmXf
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___kZqclMjeSxqHcUjffDBtNDosmXf_5;
	// System.Boolean Rewired.ThreadSafeUnityInput_Mouse::vsYPZQUSwLnGSZrMOhSMXUSVmhx
	bool ___vsYPZQUSwLnGSZrMOhSMXUSVmhx_6;

public:
	inline static int32_t get_offset_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_2() { return static_cast<int32_t>(offsetof(Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9, ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_2)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_QjgkqxyMBXGPcdjZSGsOcXzPxUl_2() const { return ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_2; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_2() { return &___QjgkqxyMBXGPcdjZSGsOcXzPxUl_2; }
	inline void set_QjgkqxyMBXGPcdjZSGsOcXzPxUl_2(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___QjgkqxyMBXGPcdjZSGsOcXzPxUl_2 = value;
		Il2CppCodeGenWriteBarrier((&___QjgkqxyMBXGPcdjZSGsOcXzPxUl_2), value);
	}

	inline static int32_t get_offset_of_xwGjMDxksyYaKvqOwHfPiiWNyUq_3() { return static_cast<int32_t>(offsetof(Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9, ___xwGjMDxksyYaKvqOwHfPiiWNyUq_3)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_xwGjMDxksyYaKvqOwHfPiiWNyUq_3() const { return ___xwGjMDxksyYaKvqOwHfPiiWNyUq_3; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_xwGjMDxksyYaKvqOwHfPiiWNyUq_3() { return &___xwGjMDxksyYaKvqOwHfPiiWNyUq_3; }
	inline void set_xwGjMDxksyYaKvqOwHfPiiWNyUq_3(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___xwGjMDxksyYaKvqOwHfPiiWNyUq_3 = value;
		Il2CppCodeGenWriteBarrier((&___xwGjMDxksyYaKvqOwHfPiiWNyUq_3), value);
	}

	inline static int32_t get_offset_of_sWuSAvTOAkQhUHhzaEDbcPtXwDB_4() { return static_cast<int32_t>(offsetof(Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9, ___sWuSAvTOAkQhUHhzaEDbcPtXwDB_4)); }
	inline int32_t get_sWuSAvTOAkQhUHhzaEDbcPtXwDB_4() const { return ___sWuSAvTOAkQhUHhzaEDbcPtXwDB_4; }
	inline int32_t* get_address_of_sWuSAvTOAkQhUHhzaEDbcPtXwDB_4() { return &___sWuSAvTOAkQhUHhzaEDbcPtXwDB_4; }
	inline void set_sWuSAvTOAkQhUHhzaEDbcPtXwDB_4(int32_t value)
	{
		___sWuSAvTOAkQhUHhzaEDbcPtXwDB_4 = value;
	}

	inline static int32_t get_offset_of_kZqclMjeSxqHcUjffDBtNDosmXf_5() { return static_cast<int32_t>(offsetof(Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9, ___kZqclMjeSxqHcUjffDBtNDosmXf_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_kZqclMjeSxqHcUjffDBtNDosmXf_5() const { return ___kZqclMjeSxqHcUjffDBtNDosmXf_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_kZqclMjeSxqHcUjffDBtNDosmXf_5() { return &___kZqclMjeSxqHcUjffDBtNDosmXf_5; }
	inline void set_kZqclMjeSxqHcUjffDBtNDosmXf_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___kZqclMjeSxqHcUjffDBtNDosmXf_5 = value;
	}

	inline static int32_t get_offset_of_vsYPZQUSwLnGSZrMOhSMXUSVmhx_6() { return static_cast<int32_t>(offsetof(Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9, ___vsYPZQUSwLnGSZrMOhSMXUSVmhx_6)); }
	inline bool get_vsYPZQUSwLnGSZrMOhSMXUSVmhx_6() const { return ___vsYPZQUSwLnGSZrMOhSMXUSVmhx_6; }
	inline bool* get_address_of_vsYPZQUSwLnGSZrMOhSMXUSVmhx_6() { return &___vsYPZQUSwLnGSZrMOhSMXUSVmhx_6; }
	inline void set_vsYPZQUSwLnGSZrMOhSMXUSVmhx_6(bool value)
	{
		___vsYPZQUSwLnGSZrMOhSMXUSVmhx_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSE_TF43362A0D8B9780699E590990FCA13DB7BE0DFE9_H
#ifndef UIANCHOR_T42FBDDDE4E7588246ADA3B042CA1DF9AB59DCEE7_H
#define UIANCHOR_T42FBDDDE4E7588246ADA3B042CA1DF9AB59DCEE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.UIAnchor
struct  UIAnchor_t42FBDDDE4E7588246ADA3B042CA1DF9AB59DCEE7 
{
public:
	// UnityEngine.Vector2 Rewired.UI.UIAnchor::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 Rewired.UI.UIAnchor::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(UIAnchor_t42FBDDDE4E7588246ADA3B042CA1DF9AB59DCEE7, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(UIAnchor_t42FBDDDE4E7588246ADA3B042CA1DF9AB59DCEE7, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIANCHOR_T42FBDDDE4E7588246ADA3B042CA1DF9AB59DCEE7_H
#ifndef POSITIONTYPE_T62802278F69FE3A2C9F12AB1ABB78E6AE93F00E4_H
#define POSITIONTYPE_T62802278F69FE3A2C9F12AB1ABB78E6AE93F00E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.UI.PositionType
struct  PositionType_t62802278F69FE3A2C9F12AB1ABB78E6AE93F00E4 
{
public:
	// System.Int32 Rewired.Utils.UI.PositionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PositionType_t62802278F69FE3A2C9F12AB1ABB78E6AE93F00E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONTYPE_T62802278F69FE3A2C9F12AB1ABB78E6AE93F00E4_H
#ifndef FONTSTYLE_T273973EBB1F40C2381F6D60AB957149DE5720CF3_H
#define FONTSTYLE_T273973EBB1F40C2381F6D60AB957149DE5720CF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FontStyle
struct  FontStyle_t273973EBB1F40C2381F6D60AB957149DE5720CF3 
{
public:
	// System.Int32 UnityEngine.FontStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyle_t273973EBB1F40C2381F6D60AB957149DE5720CF3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLE_T273973EBB1F40C2381F6D60AB957149DE5720CF3_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef SPACE_T0F622BF939B7A47E0F9632CE968F7D72FC63AF58_H
#define SPACE_T0F622BF939B7A47E0F9632CE968F7D72FC63AF58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t0F622BF939B7A47E0F9632CE968F7D72FC63AF58 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Space_t0F622BF939B7A47E0F9632CE968F7D72FC63AF58, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T0F622BF939B7A47E0F9632CE968F7D72FC63AF58_H
#ifndef TEXTALIGNMENT_TA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD_H
#define TEXTALIGNMENT_TA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAlignment
struct  TextAlignment_tA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD 
{
public:
	// System.Int32 UnityEngine.TextAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignment_tA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENT_TA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD_H
#ifndef TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#define TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifndef COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#define COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_NormalColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_HighlightedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_PressedColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_DisabledColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifndef INPUTEVENT_T0FF27E0A76BCCAA672F1BBC888C0284F1E7201B6_H
#define INPUTEVENT_T0FF27E0A76BCCAA672F1BBC888C0284F1E7201B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.CustomController_InputEvent
struct  InputEvent_t0FF27E0A76BCCAA672F1BBC888C0284F1E7201B6 
{
public:
	// Rewired.ComponentControls.Data.CustomControllerElementSelector_ElementType Rewired.ComponentControls.CustomController_InputEvent::elementType
	int32_t ___elementType_0;
	// System.Int32 Rewired.ComponentControls.CustomController_InputEvent::elementIndex
	int32_t ___elementIndex_1;
	// System.Single Rewired.ComponentControls.CustomController_InputEvent::value
	float ___value_2;

public:
	inline static int32_t get_offset_of_elementType_0() { return static_cast<int32_t>(offsetof(InputEvent_t0FF27E0A76BCCAA672F1BBC888C0284F1E7201B6, ___elementType_0)); }
	inline int32_t get_elementType_0() const { return ___elementType_0; }
	inline int32_t* get_address_of_elementType_0() { return &___elementType_0; }
	inline void set_elementType_0(int32_t value)
	{
		___elementType_0 = value;
	}

	inline static int32_t get_offset_of_elementIndex_1() { return static_cast<int32_t>(offsetof(InputEvent_t0FF27E0A76BCCAA672F1BBC888C0284F1E7201B6, ___elementIndex_1)); }
	inline int32_t get_elementIndex_1() const { return ___elementIndex_1; }
	inline int32_t* get_address_of_elementIndex_1() { return &___elementIndex_1; }
	inline void set_elementIndex_1(int32_t value)
	{
		___elementIndex_1 = value;
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(InputEvent_t0FF27E0A76BCCAA672F1BBC888C0284F1E7201B6, ___value_2)); }
	inline float get_value_2() const { return ___value_2; }
	inline float* get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(float value)
	{
		___value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTEVENT_T0FF27E0A76BCCAA672F1BBC888C0284F1E7201B6_H
#ifndef CUSTOMCONTROLLERELEMENTSELECTOR_TCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B_H
#define CUSTOMCONTROLLERELEMENTSELECTOR_TCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Data.CustomControllerElementSelector
struct  CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B  : public RuntimeObject
{
public:
	// Rewired.ComponentControls.Data.CustomControllerElementSelector_ElementType Rewired.ComponentControls.Data.CustomControllerElementSelector::_elementType
	int32_t ____elementType_0;
	// Rewired.ComponentControls.Data.CustomControllerElementSelector_SelectorType Rewired.ComponentControls.Data.CustomControllerElementSelector::_selectorType
	int32_t ____selectorType_1;
	// System.String Rewired.ComponentControls.Data.CustomControllerElementSelector::_elementName
	String_t* ____elementName_2;
	// System.Int32 Rewired.ComponentControls.Data.CustomControllerElementSelector::_elementIndex
	int32_t ____elementIndex_3;
	// System.Int32 Rewired.ComponentControls.Data.CustomControllerElementSelector::_elementId
	int32_t ____elementId_4;
	// System.Int32 Rewired.ComponentControls.Data.CustomControllerElementSelector::zhRZwMfsKoTPHpSKjbUhuMVqTIV
	int32_t ___zhRZwMfsKoTPHpSKjbUhuMVqTIV_5;
	// System.Int32 Rewired.ComponentControls.Data.CustomControllerElementSelector::xLIaLwWCCNhQfFvghLhibpbwidU
	int32_t ___xLIaLwWCCNhQfFvghLhibpbwidU_6;

public:
	inline static int32_t get_offset_of__elementType_0() { return static_cast<int32_t>(offsetof(CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B, ____elementType_0)); }
	inline int32_t get__elementType_0() const { return ____elementType_0; }
	inline int32_t* get_address_of__elementType_0() { return &____elementType_0; }
	inline void set__elementType_0(int32_t value)
	{
		____elementType_0 = value;
	}

	inline static int32_t get_offset_of__selectorType_1() { return static_cast<int32_t>(offsetof(CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B, ____selectorType_1)); }
	inline int32_t get__selectorType_1() const { return ____selectorType_1; }
	inline int32_t* get_address_of__selectorType_1() { return &____selectorType_1; }
	inline void set__selectorType_1(int32_t value)
	{
		____selectorType_1 = value;
	}

	inline static int32_t get_offset_of__elementName_2() { return static_cast<int32_t>(offsetof(CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B, ____elementName_2)); }
	inline String_t* get__elementName_2() const { return ____elementName_2; }
	inline String_t** get_address_of__elementName_2() { return &____elementName_2; }
	inline void set__elementName_2(String_t* value)
	{
		____elementName_2 = value;
		Il2CppCodeGenWriteBarrier((&____elementName_2), value);
	}

	inline static int32_t get_offset_of__elementIndex_3() { return static_cast<int32_t>(offsetof(CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B, ____elementIndex_3)); }
	inline int32_t get__elementIndex_3() const { return ____elementIndex_3; }
	inline int32_t* get_address_of__elementIndex_3() { return &____elementIndex_3; }
	inline void set__elementIndex_3(int32_t value)
	{
		____elementIndex_3 = value;
	}

	inline static int32_t get_offset_of__elementId_4() { return static_cast<int32_t>(offsetof(CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B, ____elementId_4)); }
	inline int32_t get__elementId_4() const { return ____elementId_4; }
	inline int32_t* get_address_of__elementId_4() { return &____elementId_4; }
	inline void set__elementId_4(int32_t value)
	{
		____elementId_4 = value;
	}

	inline static int32_t get_offset_of_zhRZwMfsKoTPHpSKjbUhuMVqTIV_5() { return static_cast<int32_t>(offsetof(CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B, ___zhRZwMfsKoTPHpSKjbUhuMVqTIV_5)); }
	inline int32_t get_zhRZwMfsKoTPHpSKjbUhuMVqTIV_5() const { return ___zhRZwMfsKoTPHpSKjbUhuMVqTIV_5; }
	inline int32_t* get_address_of_zhRZwMfsKoTPHpSKjbUhuMVqTIV_5() { return &___zhRZwMfsKoTPHpSKjbUhuMVqTIV_5; }
	inline void set_zhRZwMfsKoTPHpSKjbUhuMVqTIV_5(int32_t value)
	{
		___zhRZwMfsKoTPHpSKjbUhuMVqTIV_5 = value;
	}

	inline static int32_t get_offset_of_xLIaLwWCCNhQfFvghLhibpbwidU_6() { return static_cast<int32_t>(offsetof(CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B, ___xLIaLwWCCNhQfFvghLhibpbwidU_6)); }
	inline int32_t get_xLIaLwWCCNhQfFvghLhibpbwidU_6() const { return ___xLIaLwWCCNhQfFvghLhibpbwidU_6; }
	inline int32_t* get_address_of_xLIaLwWCCNhQfFvghLhibpbwidU_6() { return &___xLIaLwWCCNhQfFvghLhibpbwidU_6; }
	inline void set_xLIaLwWCCNhQfFvghLhibpbwidU_6(int32_t value)
	{
		___xLIaLwWCCNhQfFvghLhibpbwidU_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLERELEMENTSELECTOR_TCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B_H
#ifndef CUSTOMCONTROLLERELEMENTTARGET_TA12266513574F9C756EF59EFE415F92CB79CCC7A_H
#define CUSTOMCONTROLLERELEMENTTARGET_TA12266513574F9C756EF59EFE415F92CB79CCC7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Data.CustomControllerElementTarget
struct  CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A  : public RuntimeObject
{
public:
	// Rewired.ComponentControls.Data.CustomControllerElementSelector Rewired.ComponentControls.Data.CustomControllerElementTarget::_element
	CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B * ____element_0;
	// Rewired.ComponentControls.Data.CustomControllerElementTarget_ValueRange Rewired.ComponentControls.Data.CustomControllerElementTarget::_valueRange
	int32_t ____valueRange_1;
	// Rewired.Pole Rewired.ComponentControls.Data.CustomControllerElementTarget::_valueContribution
	int32_t ____valueContribution_2;
	// System.Boolean Rewired.ComponentControls.Data.CustomControllerElementTarget::_invert
	bool ____invert_3;

public:
	inline static int32_t get_offset_of__element_0() { return static_cast<int32_t>(offsetof(CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A, ____element_0)); }
	inline CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B * get__element_0() const { return ____element_0; }
	inline CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B ** get_address_of__element_0() { return &____element_0; }
	inline void set__element_0(CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B * value)
	{
		____element_0 = value;
		Il2CppCodeGenWriteBarrier((&____element_0), value);
	}

	inline static int32_t get_offset_of__valueRange_1() { return static_cast<int32_t>(offsetof(CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A, ____valueRange_1)); }
	inline int32_t get__valueRange_1() const { return ____valueRange_1; }
	inline int32_t* get_address_of__valueRange_1() { return &____valueRange_1; }
	inline void set__valueRange_1(int32_t value)
	{
		____valueRange_1 = value;
	}

	inline static int32_t get_offset_of__valueContribution_2() { return static_cast<int32_t>(offsetof(CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A, ____valueContribution_2)); }
	inline int32_t get__valueContribution_2() const { return ____valueContribution_2; }
	inline int32_t* get_address_of__valueContribution_2() { return &____valueContribution_2; }
	inline void set__valueContribution_2(int32_t value)
	{
		____valueContribution_2 = value;
	}

	inline static int32_t get_offset_of__invert_3() { return static_cast<int32_t>(offsetof(CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A, ____invert_3)); }
	inline bool get__invert_3() const { return ____invert_3; }
	inline bool* get_address_of__invert_3() { return &____invert_3; }
	inline void set__invert_3(bool value)
	{
		____invert_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLERELEMENTTARGET_TA12266513574F9C756EF59EFE415F92CB79CCC7A_H
#ifndef ADKISTKLJQZAHJSAYVBTGATOWFP_T536F7A7A261C53ED39A2F5468663D7D8FFABD906_H
#define ADKISTKLJQZAHJSAYVBTGATOWFP_T536F7A7A261C53ED39A2F5468663D7D8FFABD906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP
struct  ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906  : public RuntimeObject
{
public:
	// System.Object Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP::NOArrsNdfKDShNFftfbPqYDzhpq
	RuntimeObject * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// Rewired.ComponentControls.TouchButton Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP::DKaFTWTMhFeLQHDExnDMRxZfmROL
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2;
	// UnityEngine.Vector2 Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP::PLYNlNwVGPITrOPQzvkfxJINxqV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___PLYNlNwVGPITrOPQzvkfxJINxqV_3;
	// Rewired.Utils.UI.PositionType Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP::RSjsEfDGFnIyjaMrbqVhgtKxPqNf
	int32_t ___RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4;
	// System.Single Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP::QFegQkgOGzZhlzwAWRdvkhXoHBk
	float ___QFegQkgOGzZhlzwAWRdvkhXoHBk_5;
	// Rewired.ComponentControls.TouchButton_CRqCjFCalakqbykartaanqOTwPEL Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP::iBiwqHkhtevrrrpOZhDdNCZduXg
	int32_t ___iBiwqHkhtevrrrpOZhDdNCZduXg_6;
	// UnityEngine.RectTransform Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP::rZWxSZSGyMGOGFWhVovIOcyLTvmI
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rZWxSZSGyMGOGFWhVovIOcyLTvmI_7;
	// UnityEngine.Vector2 Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP::MgCqBsRGtBCAjzmghqbspYwrQUy
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___MgCqBsRGtBCAjzmghqbspYwrQUy_8;
	// System.Single Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP::PtnufnJQeeWcbSnrMVerFHKuNkj
	float ___PtnufnJQeeWcbSnrMVerFHKuNkj_9;
	// System.Single Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP::sHEiejKjFwBVUBvFilDHmOsyaMhI
	float ___sHEiejKjFwBVUBvFilDHmOsyaMhI_10;
	// System.Single Rewired.ComponentControls.TouchButton_ADKisTklJqZAHJSayvbTgaTOwfP::squPontnRWBsKFStIzRLzrGxIch
	float ___squPontnRWBsKFStIzRLzrGxIch_11;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline RuntimeObject * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline RuntimeObject ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(RuntimeObject * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() { return static_cast<int32_t>(offsetof(ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2)); }
	inline TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2; }
	inline TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_2; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_2(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_2 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_2), value);
	}

	inline static int32_t get_offset_of_PLYNlNwVGPITrOPQzvkfxJINxqV_3() { return static_cast<int32_t>(offsetof(ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906, ___PLYNlNwVGPITrOPQzvkfxJINxqV_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_PLYNlNwVGPITrOPQzvkfxJINxqV_3() const { return ___PLYNlNwVGPITrOPQzvkfxJINxqV_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_PLYNlNwVGPITrOPQzvkfxJINxqV_3() { return &___PLYNlNwVGPITrOPQzvkfxJINxqV_3; }
	inline void set_PLYNlNwVGPITrOPQzvkfxJINxqV_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___PLYNlNwVGPITrOPQzvkfxJINxqV_3 = value;
	}

	inline static int32_t get_offset_of_RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4() { return static_cast<int32_t>(offsetof(ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906, ___RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4)); }
	inline int32_t get_RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4() const { return ___RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4; }
	inline int32_t* get_address_of_RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4() { return &___RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4; }
	inline void set_RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4(int32_t value)
	{
		___RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4 = value;
	}

	inline static int32_t get_offset_of_QFegQkgOGzZhlzwAWRdvkhXoHBk_5() { return static_cast<int32_t>(offsetof(ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906, ___QFegQkgOGzZhlzwAWRdvkhXoHBk_5)); }
	inline float get_QFegQkgOGzZhlzwAWRdvkhXoHBk_5() const { return ___QFegQkgOGzZhlzwAWRdvkhXoHBk_5; }
	inline float* get_address_of_QFegQkgOGzZhlzwAWRdvkhXoHBk_5() { return &___QFegQkgOGzZhlzwAWRdvkhXoHBk_5; }
	inline void set_QFegQkgOGzZhlzwAWRdvkhXoHBk_5(float value)
	{
		___QFegQkgOGzZhlzwAWRdvkhXoHBk_5 = value;
	}

	inline static int32_t get_offset_of_iBiwqHkhtevrrrpOZhDdNCZduXg_6() { return static_cast<int32_t>(offsetof(ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906, ___iBiwqHkhtevrrrpOZhDdNCZduXg_6)); }
	inline int32_t get_iBiwqHkhtevrrrpOZhDdNCZduXg_6() const { return ___iBiwqHkhtevrrrpOZhDdNCZduXg_6; }
	inline int32_t* get_address_of_iBiwqHkhtevrrrpOZhDdNCZduXg_6() { return &___iBiwqHkhtevrrrpOZhDdNCZduXg_6; }
	inline void set_iBiwqHkhtevrrrpOZhDdNCZduXg_6(int32_t value)
	{
		___iBiwqHkhtevrrrpOZhDdNCZduXg_6 = value;
	}

	inline static int32_t get_offset_of_rZWxSZSGyMGOGFWhVovIOcyLTvmI_7() { return static_cast<int32_t>(offsetof(ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906, ___rZWxSZSGyMGOGFWhVovIOcyLTvmI_7)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rZWxSZSGyMGOGFWhVovIOcyLTvmI_7() const { return ___rZWxSZSGyMGOGFWhVovIOcyLTvmI_7; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rZWxSZSGyMGOGFWhVovIOcyLTvmI_7() { return &___rZWxSZSGyMGOGFWhVovIOcyLTvmI_7; }
	inline void set_rZWxSZSGyMGOGFWhVovIOcyLTvmI_7(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rZWxSZSGyMGOGFWhVovIOcyLTvmI_7 = value;
		Il2CppCodeGenWriteBarrier((&___rZWxSZSGyMGOGFWhVovIOcyLTvmI_7), value);
	}

	inline static int32_t get_offset_of_MgCqBsRGtBCAjzmghqbspYwrQUy_8() { return static_cast<int32_t>(offsetof(ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906, ___MgCqBsRGtBCAjzmghqbspYwrQUy_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_MgCqBsRGtBCAjzmghqbspYwrQUy_8() const { return ___MgCqBsRGtBCAjzmghqbspYwrQUy_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_MgCqBsRGtBCAjzmghqbspYwrQUy_8() { return &___MgCqBsRGtBCAjzmghqbspYwrQUy_8; }
	inline void set_MgCqBsRGtBCAjzmghqbspYwrQUy_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___MgCqBsRGtBCAjzmghqbspYwrQUy_8 = value;
	}

	inline static int32_t get_offset_of_PtnufnJQeeWcbSnrMVerFHKuNkj_9() { return static_cast<int32_t>(offsetof(ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906, ___PtnufnJQeeWcbSnrMVerFHKuNkj_9)); }
	inline float get_PtnufnJQeeWcbSnrMVerFHKuNkj_9() const { return ___PtnufnJQeeWcbSnrMVerFHKuNkj_9; }
	inline float* get_address_of_PtnufnJQeeWcbSnrMVerFHKuNkj_9() { return &___PtnufnJQeeWcbSnrMVerFHKuNkj_9; }
	inline void set_PtnufnJQeeWcbSnrMVerFHKuNkj_9(float value)
	{
		___PtnufnJQeeWcbSnrMVerFHKuNkj_9 = value;
	}

	inline static int32_t get_offset_of_sHEiejKjFwBVUBvFilDHmOsyaMhI_10() { return static_cast<int32_t>(offsetof(ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906, ___sHEiejKjFwBVUBvFilDHmOsyaMhI_10)); }
	inline float get_sHEiejKjFwBVUBvFilDHmOsyaMhI_10() const { return ___sHEiejKjFwBVUBvFilDHmOsyaMhI_10; }
	inline float* get_address_of_sHEiejKjFwBVUBvFilDHmOsyaMhI_10() { return &___sHEiejKjFwBVUBvFilDHmOsyaMhI_10; }
	inline void set_sHEiejKjFwBVUBvFilDHmOsyaMhI_10(float value)
	{
		___sHEiejKjFwBVUBvFilDHmOsyaMhI_10 = value;
	}

	inline static int32_t get_offset_of_squPontnRWBsKFStIzRLzrGxIch_11() { return static_cast<int32_t>(offsetof(ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906, ___squPontnRWBsKFStIzRLzrGxIch_11)); }
	inline float get_squPontnRWBsKFStIzRLzrGxIch_11() const { return ___squPontnRWBsKFStIzRLzrGxIch_11; }
	inline float* get_address_of_squPontnRWBsKFStIzRLzrGxIch_11() { return &___squPontnRWBsKFStIzRLzrGxIch_11; }
	inline void set_squPontnRWBsKFStIzRLzrGxIch_11(float value)
	{
		___squPontnRWBsKFStIzRLzrGxIch_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADKISTKLJQZAHJSAYVBTGATOWFP_T536F7A7A261C53ED39A2F5468663D7D8FFABD906_H
#ifndef INTERACTIONSTATETRANSITIONARGS_T67500A98203849C0A877FD6DB1AC3A0A9487AB42_H
#define INTERACTIONSTATETRANSITIONARGS_T67500A98203849C0A877FD6DB1AC3A0A9487AB42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchInteractable_InteractionStateTransitionArgs
struct  InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42  : public RuntimeObject
{
public:
	// Rewired.ComponentControls.TouchInteractable Rewired.ComponentControls.TouchInteractable_InteractionStateTransitionArgs::mBBOWyREJBaBXFyzagpvatnxwNu
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8 * ___mBBOWyREJBaBXFyzagpvatnxwNu_0;
	// Rewired.ComponentControls.TouchInteractable_InteractionState Rewired.ComponentControls.TouchInteractable_InteractionStateTransitionArgs::UBXHFojKQfGbvszJFuVFvDRPgNg
	int32_t ___UBXHFojKQfGbvszJFuVFvDRPgNg_1;
	// System.Single Rewired.ComponentControls.TouchInteractable_InteractionStateTransitionArgs::xbBSqLtNsieZdHwmQDzzBoxOPYLS
	float ___xbBSqLtNsieZdHwmQDzzBoxOPYLS_2;

public:
	inline static int32_t get_offset_of_mBBOWyREJBaBXFyzagpvatnxwNu_0() { return static_cast<int32_t>(offsetof(InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42, ___mBBOWyREJBaBXFyzagpvatnxwNu_0)); }
	inline TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8 * get_mBBOWyREJBaBXFyzagpvatnxwNu_0() const { return ___mBBOWyREJBaBXFyzagpvatnxwNu_0; }
	inline TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8 ** get_address_of_mBBOWyREJBaBXFyzagpvatnxwNu_0() { return &___mBBOWyREJBaBXFyzagpvatnxwNu_0; }
	inline void set_mBBOWyREJBaBXFyzagpvatnxwNu_0(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8 * value)
	{
		___mBBOWyREJBaBXFyzagpvatnxwNu_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBBOWyREJBaBXFyzagpvatnxwNu_0), value);
	}

	inline static int32_t get_offset_of_UBXHFojKQfGbvszJFuVFvDRPgNg_1() { return static_cast<int32_t>(offsetof(InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42, ___UBXHFojKQfGbvszJFuVFvDRPgNg_1)); }
	inline int32_t get_UBXHFojKQfGbvszJFuVFvDRPgNg_1() const { return ___UBXHFojKQfGbvszJFuVFvDRPgNg_1; }
	inline int32_t* get_address_of_UBXHFojKQfGbvszJFuVFvDRPgNg_1() { return &___UBXHFojKQfGbvszJFuVFvDRPgNg_1; }
	inline void set_UBXHFojKQfGbvszJFuVFvDRPgNg_1(int32_t value)
	{
		___UBXHFojKQfGbvszJFuVFvDRPgNg_1 = value;
	}

	inline static int32_t get_offset_of_xbBSqLtNsieZdHwmQDzzBoxOPYLS_2() { return static_cast<int32_t>(offsetof(InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42, ___xbBSqLtNsieZdHwmQDzzBoxOPYLS_2)); }
	inline float get_xbBSqLtNsieZdHwmQDzzBoxOPYLS_2() const { return ___xbBSqLtNsieZdHwmQDzzBoxOPYLS_2; }
	inline float* get_address_of_xbBSqLtNsieZdHwmQDzzBoxOPYLS_2() { return &___xbBSqLtNsieZdHwmQDzzBoxOPYLS_2; }
	inline void set_xbBSqLtNsieZdHwmQDzzBoxOPYLS_2(float value)
	{
		___xbBSqLtNsieZdHwmQDzzBoxOPYLS_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSTATETRANSITIONARGS_T67500A98203849C0A877FD6DB1AC3A0A9487AB42_H
#ifndef HHWKZZQYZTTVNYPTXLBINDUIQPV_TE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA_H
#define HHWKZZQYZTTVNYPTXLBINDUIQPV_TE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv
struct  hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA  : public RuntimeObject
{
public:
	// System.Object Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv::NOArrsNdfKDShNFftfbPqYDzhpq
	RuntimeObject * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// Rewired.ComponentControls.TouchJoystick Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv::DKaFTWTMhFeLQHDExnDMRxZfmROL
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2;
	// UnityEngine.Vector2 Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv::PLYNlNwVGPITrOPQzvkfxJINxqV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___PLYNlNwVGPITrOPQzvkfxJINxqV_3;
	// Rewired.Utils.UI.PositionType Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv::RSjsEfDGFnIyjaMrbqVhgtKxPqNf
	int32_t ___RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4;
	// System.Single Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv::QFegQkgOGzZhlzwAWRdvkhXoHBk
	float ___QFegQkgOGzZhlzwAWRdvkhXoHBk_5;
	// Rewired.ComponentControls.TouchJoystick_NHoUMtvQEGOtTovxNMKxiWCTcIN Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv::iBiwqHkhtevrrrpOZhDdNCZduXg
	int32_t ___iBiwqHkhtevrrrpOZhDdNCZduXg_6;
	// UnityEngine.RectTransform Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv::hQTchMQmvLwphsByczpEoMsVTpL
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___hQTchMQmvLwphsByczpEoMsVTpL_7;
	// UnityEngine.Vector2 Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv::XnqfsYIpjWuutJvTBiiaElBjIyR
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___XnqfsYIpjWuutJvTBiiaElBjIyR_8;
	// System.Single Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv::nByUllSKMZtbQGGNAQuuoTgeyIy
	float ___nByUllSKMZtbQGGNAQuuoTgeyIy_9;
	// System.Single Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv::KrYbtDRftEBOhUhmcodyXyGJFIK
	float ___KrYbtDRftEBOhUhmcodyXyGJFIK_10;
	// System.Single Rewired.ComponentControls.TouchJoystick_hhWKZzQYZtTVNyptxlbindUIQPv::yZYDvdhLYwOFahFGGIYphvTazJj
	float ___yZYDvdhLYwOFahFGGIYphvTazJj_11;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline RuntimeObject * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline RuntimeObject ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(RuntimeObject * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() { return static_cast<int32_t>(offsetof(hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2)); }
	inline TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_2; }
	inline TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_2; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_2(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_2 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_2), value);
	}

	inline static int32_t get_offset_of_PLYNlNwVGPITrOPQzvkfxJINxqV_3() { return static_cast<int32_t>(offsetof(hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA, ___PLYNlNwVGPITrOPQzvkfxJINxqV_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_PLYNlNwVGPITrOPQzvkfxJINxqV_3() const { return ___PLYNlNwVGPITrOPQzvkfxJINxqV_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_PLYNlNwVGPITrOPQzvkfxJINxqV_3() { return &___PLYNlNwVGPITrOPQzvkfxJINxqV_3; }
	inline void set_PLYNlNwVGPITrOPQzvkfxJINxqV_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___PLYNlNwVGPITrOPQzvkfxJINxqV_3 = value;
	}

	inline static int32_t get_offset_of_RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4() { return static_cast<int32_t>(offsetof(hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA, ___RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4)); }
	inline int32_t get_RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4() const { return ___RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4; }
	inline int32_t* get_address_of_RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4() { return &___RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4; }
	inline void set_RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4(int32_t value)
	{
		___RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4 = value;
	}

	inline static int32_t get_offset_of_QFegQkgOGzZhlzwAWRdvkhXoHBk_5() { return static_cast<int32_t>(offsetof(hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA, ___QFegQkgOGzZhlzwAWRdvkhXoHBk_5)); }
	inline float get_QFegQkgOGzZhlzwAWRdvkhXoHBk_5() const { return ___QFegQkgOGzZhlzwAWRdvkhXoHBk_5; }
	inline float* get_address_of_QFegQkgOGzZhlzwAWRdvkhXoHBk_5() { return &___QFegQkgOGzZhlzwAWRdvkhXoHBk_5; }
	inline void set_QFegQkgOGzZhlzwAWRdvkhXoHBk_5(float value)
	{
		___QFegQkgOGzZhlzwAWRdvkhXoHBk_5 = value;
	}

	inline static int32_t get_offset_of_iBiwqHkhtevrrrpOZhDdNCZduXg_6() { return static_cast<int32_t>(offsetof(hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA, ___iBiwqHkhtevrrrpOZhDdNCZduXg_6)); }
	inline int32_t get_iBiwqHkhtevrrrpOZhDdNCZduXg_6() const { return ___iBiwqHkhtevrrrpOZhDdNCZduXg_6; }
	inline int32_t* get_address_of_iBiwqHkhtevrrrpOZhDdNCZduXg_6() { return &___iBiwqHkhtevrrrpOZhDdNCZduXg_6; }
	inline void set_iBiwqHkhtevrrrpOZhDdNCZduXg_6(int32_t value)
	{
		___iBiwqHkhtevrrrpOZhDdNCZduXg_6 = value;
	}

	inline static int32_t get_offset_of_hQTchMQmvLwphsByczpEoMsVTpL_7() { return static_cast<int32_t>(offsetof(hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA, ___hQTchMQmvLwphsByczpEoMsVTpL_7)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_hQTchMQmvLwphsByczpEoMsVTpL_7() const { return ___hQTchMQmvLwphsByczpEoMsVTpL_7; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_hQTchMQmvLwphsByczpEoMsVTpL_7() { return &___hQTchMQmvLwphsByczpEoMsVTpL_7; }
	inline void set_hQTchMQmvLwphsByczpEoMsVTpL_7(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___hQTchMQmvLwphsByczpEoMsVTpL_7 = value;
		Il2CppCodeGenWriteBarrier((&___hQTchMQmvLwphsByczpEoMsVTpL_7), value);
	}

	inline static int32_t get_offset_of_XnqfsYIpjWuutJvTBiiaElBjIyR_8() { return static_cast<int32_t>(offsetof(hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA, ___XnqfsYIpjWuutJvTBiiaElBjIyR_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_XnqfsYIpjWuutJvTBiiaElBjIyR_8() const { return ___XnqfsYIpjWuutJvTBiiaElBjIyR_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_XnqfsYIpjWuutJvTBiiaElBjIyR_8() { return &___XnqfsYIpjWuutJvTBiiaElBjIyR_8; }
	inline void set_XnqfsYIpjWuutJvTBiiaElBjIyR_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___XnqfsYIpjWuutJvTBiiaElBjIyR_8 = value;
	}

	inline static int32_t get_offset_of_nByUllSKMZtbQGGNAQuuoTgeyIy_9() { return static_cast<int32_t>(offsetof(hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA, ___nByUllSKMZtbQGGNAQuuoTgeyIy_9)); }
	inline float get_nByUllSKMZtbQGGNAQuuoTgeyIy_9() const { return ___nByUllSKMZtbQGGNAQuuoTgeyIy_9; }
	inline float* get_address_of_nByUllSKMZtbQGGNAQuuoTgeyIy_9() { return &___nByUllSKMZtbQGGNAQuuoTgeyIy_9; }
	inline void set_nByUllSKMZtbQGGNAQuuoTgeyIy_9(float value)
	{
		___nByUllSKMZtbQGGNAQuuoTgeyIy_9 = value;
	}

	inline static int32_t get_offset_of_KrYbtDRftEBOhUhmcodyXyGJFIK_10() { return static_cast<int32_t>(offsetof(hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA, ___KrYbtDRftEBOhUhmcodyXyGJFIK_10)); }
	inline float get_KrYbtDRftEBOhUhmcodyXyGJFIK_10() const { return ___KrYbtDRftEBOhUhmcodyXyGJFIK_10; }
	inline float* get_address_of_KrYbtDRftEBOhUhmcodyXyGJFIK_10() { return &___KrYbtDRftEBOhUhmcodyXyGJFIK_10; }
	inline void set_KrYbtDRftEBOhUhmcodyXyGJFIK_10(float value)
	{
		___KrYbtDRftEBOhUhmcodyXyGJFIK_10 = value;
	}

	inline static int32_t get_offset_of_yZYDvdhLYwOFahFGGIYphvTazJj_11() { return static_cast<int32_t>(offsetof(hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA, ___yZYDvdhLYwOFahFGGIYphvTazJj_11)); }
	inline float get_yZYDvdhLYwOFahFGGIYphvTazJj_11() const { return ___yZYDvdhLYwOFahFGGIYphvTazJj_11; }
	inline float* get_address_of_yZYDvdhLYwOFahFGGIYphvTazJj_11() { return &___yZYDvdhLYwOFahFGGIYphvTazJj_11; }
	inline void set_yZYDvdhLYwOFahFGGIYphvTazJj_11(float value)
	{
		___yZYDvdhLYwOFahFGGIYphvTazJj_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HHWKZZQYZTTVNYPTXLBINDUIQPV_TE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA_H
#ifndef ELEMENTINFO_T0269D1F95FEF139A73F40327FE7B457D4253F1C7_H
#define ELEMENTINFO_T0269D1F95FEF139A73F40327FE7B457D4253F1C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Components.PlayerController_ElementInfo
struct  ElementInfo_t0269D1F95FEF139A73F40327FE7B457D4253F1C7  : public RuntimeObject
{
public:
	// System.String Rewired.Components.PlayerController_ElementInfo::_name
	String_t* ____name_0;
	// Rewired.PlayerController_Element_Type Rewired.Components.PlayerController_ElementInfo::_elementType
	int32_t ____elementType_1;
	// System.Boolean Rewired.Components.PlayerController_ElementInfo::_enabled
	bool ____enabled_2;
	// Rewired.Components.PlayerController_ElementWithSourceInfo[] Rewired.Components.PlayerController_ElementInfo::_elements
	ElementWithSourceInfoU5BU5D_t6197EBBBA7C755C6B4B97FF30FB7CDDB0C065DEE* ____elements_3;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(ElementInfo_t0269D1F95FEF139A73F40327FE7B457D4253F1C7, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__elementType_1() { return static_cast<int32_t>(offsetof(ElementInfo_t0269D1F95FEF139A73F40327FE7B457D4253F1C7, ____elementType_1)); }
	inline int32_t get__elementType_1() const { return ____elementType_1; }
	inline int32_t* get_address_of__elementType_1() { return &____elementType_1; }
	inline void set__elementType_1(int32_t value)
	{
		____elementType_1 = value;
	}

	inline static int32_t get_offset_of__enabled_2() { return static_cast<int32_t>(offsetof(ElementInfo_t0269D1F95FEF139A73F40327FE7B457D4253F1C7, ____enabled_2)); }
	inline bool get__enabled_2() const { return ____enabled_2; }
	inline bool* get_address_of__enabled_2() { return &____enabled_2; }
	inline void set__enabled_2(bool value)
	{
		____enabled_2 = value;
	}

	inline static int32_t get_offset_of__elements_3() { return static_cast<int32_t>(offsetof(ElementInfo_t0269D1F95FEF139A73F40327FE7B457D4253F1C7, ____elements_3)); }
	inline ElementWithSourceInfoU5BU5D_t6197EBBBA7C755C6B4B97FF30FB7CDDB0C065DEE* get__elements_3() const { return ____elements_3; }
	inline ElementWithSourceInfoU5BU5D_t6197EBBBA7C755C6B4B97FF30FB7CDDB0C065DEE** get_address_of__elements_3() { return &____elements_3; }
	inline void set__elements_3(ElementWithSourceInfoU5BU5D_t6197EBBBA7C755C6B4B97FF30FB7CDDB0C065DEE* value)
	{
		____elements_3 = value;
		Il2CppCodeGenWriteBarrier((&____elements_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTINFO_T0269D1F95FEF139A73F40327FE7B457D4253F1C7_H
#ifndef ELEMENTWITHSOURCEINFO_TE98141C8E9A10E18ED90DB84BC264F43DAB73A05_H
#define ELEMENTWITHSOURCEINFO_TE98141C8E9A10E18ED90DB84BC264F43DAB73A05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Components.PlayerController_ElementWithSourceInfo
struct  ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05  : public RuntimeObject
{
public:
	// System.String Rewired.Components.PlayerController_ElementWithSourceInfo::_name
	String_t* ____name_0;
	// Rewired.PlayerController_Element_TypeWithSource Rewired.Components.PlayerController_ElementWithSourceInfo::_elementType
	int32_t ____elementType_1;
	// System.Boolean Rewired.Components.PlayerController_ElementWithSourceInfo::_enabled
	bool ____enabled_2;
	// System.Int32 Rewired.Components.PlayerController_ElementWithSourceInfo::_actionId
	int32_t ____actionId_3;
	// Rewired.AxisCoordinateMode Rewired.Components.PlayerController_ElementWithSourceInfo::_coordinateMode
	int32_t ____coordinateMode_4;
	// System.Single Rewired.Components.PlayerController_ElementWithSourceInfo::_absoluteToRelativeSensitivity
	float ____absoluteToRelativeSensitivity_5;
	// System.Single Rewired.Components.PlayerController_ElementWithSourceInfo::_repeatRate
	float ____repeatRate_6;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__elementType_1() { return static_cast<int32_t>(offsetof(ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05, ____elementType_1)); }
	inline int32_t get__elementType_1() const { return ____elementType_1; }
	inline int32_t* get_address_of__elementType_1() { return &____elementType_1; }
	inline void set__elementType_1(int32_t value)
	{
		____elementType_1 = value;
	}

	inline static int32_t get_offset_of__enabled_2() { return static_cast<int32_t>(offsetof(ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05, ____enabled_2)); }
	inline bool get__enabled_2() const { return ____enabled_2; }
	inline bool* get_address_of__enabled_2() { return &____enabled_2; }
	inline void set__enabled_2(bool value)
	{
		____enabled_2 = value;
	}

	inline static int32_t get_offset_of__actionId_3() { return static_cast<int32_t>(offsetof(ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05, ____actionId_3)); }
	inline int32_t get__actionId_3() const { return ____actionId_3; }
	inline int32_t* get_address_of__actionId_3() { return &____actionId_3; }
	inline void set__actionId_3(int32_t value)
	{
		____actionId_3 = value;
	}

	inline static int32_t get_offset_of__coordinateMode_4() { return static_cast<int32_t>(offsetof(ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05, ____coordinateMode_4)); }
	inline int32_t get__coordinateMode_4() const { return ____coordinateMode_4; }
	inline int32_t* get_address_of__coordinateMode_4() { return &____coordinateMode_4; }
	inline void set__coordinateMode_4(int32_t value)
	{
		____coordinateMode_4 = value;
	}

	inline static int32_t get_offset_of__absoluteToRelativeSensitivity_5() { return static_cast<int32_t>(offsetof(ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05, ____absoluteToRelativeSensitivity_5)); }
	inline float get__absoluteToRelativeSensitivity_5() const { return ____absoluteToRelativeSensitivity_5; }
	inline float* get_address_of__absoluteToRelativeSensitivity_5() { return &____absoluteToRelativeSensitivity_5; }
	inline void set__absoluteToRelativeSensitivity_5(float value)
	{
		____absoluteToRelativeSensitivity_5 = value;
	}

	inline static int32_t get_offset_of__repeatRate_6() { return static_cast<int32_t>(offsetof(ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05, ____repeatRate_6)); }
	inline float get__repeatRate_6() const { return ____repeatRate_6; }
	inline float* get_address_of__repeatRate_6() { return &____repeatRate_6; }
	inline void set__repeatRate_6(float value)
	{
		____repeatRate_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTWITHSOURCEINFO_TE98141C8E9A10E18ED90DB84BC264F43DAB73A05_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef COMPONENTCONTROL_T712783BFAB42EB8BDA290EA3CCE00DA2392EFA96_H
#define COMPONENTCONTROL_T712783BFAB42EB8BDA290EA3CCE00DA2392EFA96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.ComponentControl
struct  ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.ComponentControls.IComponentController Rewired.ComponentControls.ComponentControl::_controller
	RuntimeObject* ____controller_4;
	// System.Boolean Rewired.ComponentControls.ComponentControl::oGbqxjxXGgvgXNLPUMbdLzEDsmT
	bool ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5;
	// System.Boolean Rewired.ComponentControls.ComponentControl::XzPgldmZGPgZguvaiWrsKWnbdEP
	bool ___XzPgldmZGPgZguvaiWrsKWnbdEP_6;
	// System.Int32 Rewired.ComponentControls.ComponentControl::_lastUpdateFrame
	int32_t ____lastUpdateFrame_7;

public:
	inline static int32_t get_offset_of__controller_4() { return static_cast<int32_t>(offsetof(ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96, ____controller_4)); }
	inline RuntimeObject* get__controller_4() const { return ____controller_4; }
	inline RuntimeObject** get_address_of__controller_4() { return &____controller_4; }
	inline void set__controller_4(RuntimeObject* value)
	{
		____controller_4 = value;
		Il2CppCodeGenWriteBarrier((&____controller_4), value);
	}

	inline static int32_t get_offset_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5() { return static_cast<int32_t>(offsetof(ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96, ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5)); }
	inline bool get_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5() const { return ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5; }
	inline bool* get_address_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5() { return &___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5; }
	inline void set_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5(bool value)
	{
		___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5 = value;
	}

	inline static int32_t get_offset_of_XzPgldmZGPgZguvaiWrsKWnbdEP_6() { return static_cast<int32_t>(offsetof(ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96, ___XzPgldmZGPgZguvaiWrsKWnbdEP_6)); }
	inline bool get_XzPgldmZGPgZguvaiWrsKWnbdEP_6() const { return ___XzPgldmZGPgZguvaiWrsKWnbdEP_6; }
	inline bool* get_address_of_XzPgldmZGPgZguvaiWrsKWnbdEP_6() { return &___XzPgldmZGPgZguvaiWrsKWnbdEP_6; }
	inline void set_XzPgldmZGPgZguvaiWrsKWnbdEP_6(bool value)
	{
		___XzPgldmZGPgZguvaiWrsKWnbdEP_6 = value;
	}

	inline static int32_t get_offset_of__lastUpdateFrame_7() { return static_cast<int32_t>(offsetof(ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96, ____lastUpdateFrame_7)); }
	inline int32_t get__lastUpdateFrame_7() const { return ____lastUpdateFrame_7; }
	inline int32_t* get_address_of__lastUpdateFrame_7() { return &____lastUpdateFrame_7; }
	inline void set__lastUpdateFrame_7(int32_t value)
	{
		____lastUpdateFrame_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTCONTROL_T712783BFAB42EB8BDA290EA3CCE00DA2392EFA96_H
#ifndef COMPONENTCONTROLLER_TEC306FC31850782AF316BE7AB0B90B424DC954DE_H
#define COMPONENTCONTROLLER_TEC306FC31850782AF316BE7AB0B90B424DC954DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.ComponentController
struct  ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Rewired.ComponentControls.ComponentController::oGbqxjxXGgvgXNLPUMbdLzEDsmT
	bool ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_4;
	// System.Boolean Rewired.ComponentControls.ComponentController::XzPgldmZGPgZguvaiWrsKWnbdEP
	bool ___XzPgldmZGPgZguvaiWrsKWnbdEP_5;
	// System.Collections.Generic.List`1<Rewired.ComponentControls.IComponentControl> Rewired.ComponentControls.ComponentController::_controls
	List_1_tF377D1440AF55B94061969B893DABAEE87259413 * ____controls_6;

public:
	inline static int32_t get_offset_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_4() { return static_cast<int32_t>(offsetof(ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE, ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_4)); }
	inline bool get_oGbqxjxXGgvgXNLPUMbdLzEDsmT_4() const { return ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_4; }
	inline bool* get_address_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_4() { return &___oGbqxjxXGgvgXNLPUMbdLzEDsmT_4; }
	inline void set_oGbqxjxXGgvgXNLPUMbdLzEDsmT_4(bool value)
	{
		___oGbqxjxXGgvgXNLPUMbdLzEDsmT_4 = value;
	}

	inline static int32_t get_offset_of_XzPgldmZGPgZguvaiWrsKWnbdEP_5() { return static_cast<int32_t>(offsetof(ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE, ___XzPgldmZGPgZguvaiWrsKWnbdEP_5)); }
	inline bool get_XzPgldmZGPgZguvaiWrsKWnbdEP_5() const { return ___XzPgldmZGPgZguvaiWrsKWnbdEP_5; }
	inline bool* get_address_of_XzPgldmZGPgZguvaiWrsKWnbdEP_5() { return &___XzPgldmZGPgZguvaiWrsKWnbdEP_5; }
	inline void set_XzPgldmZGPgZguvaiWrsKWnbdEP_5(bool value)
	{
		___XzPgldmZGPgZguvaiWrsKWnbdEP_5 = value;
	}

	inline static int32_t get_offset_of__controls_6() { return static_cast<int32_t>(offsetof(ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE, ____controls_6)); }
	inline List_1_tF377D1440AF55B94061969B893DABAEE87259413 * get__controls_6() const { return ____controls_6; }
	inline List_1_tF377D1440AF55B94061969B893DABAEE87259413 ** get_address_of__controls_6() { return &____controls_6; }
	inline void set__controls_6(List_1_tF377D1440AF55B94061969B893DABAEE87259413 * value)
	{
		____controls_6 = value;
		Il2CppCodeGenWriteBarrier((&____controls_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTCONTROLLER_TEC306FC31850782AF316BE7AB0B90B424DC954DE_H
#ifndef ROTATEAROUNDAXIS_T8BDEC8749D2C59079DDE5BDF99810BEE630D244E_H
#define ROTATEAROUNDAXIS_T8BDEC8749D2C59079DDE5BDF99810BEE630D244E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Effects.RotateAroundAxis
struct  RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.ComponentControls.Effects.RotateAroundAxis_Speed Rewired.ComponentControls.Effects.RotateAroundAxis::_speed
	int32_t ____speed_4;
	// System.Single Rewired.ComponentControls.Effects.RotateAroundAxis::_slowRotationSpeed
	float ____slowRotationSpeed_5;
	// System.Single Rewired.ComponentControls.Effects.RotateAroundAxis::_fastRotationSpeed
	float ____fastRotationSpeed_6;
	// Rewired.ComponentControls.Effects.RotateAroundAxis_RotationAxis Rewired.ComponentControls.Effects.RotateAroundAxis::_rotateAroundAxis
	int32_t ____rotateAroundAxis_7;
	// UnityEngine.Space Rewired.ComponentControls.Effects.RotateAroundAxis::_relativeTo
	int32_t ____relativeTo_8;
	// System.Boolean Rewired.ComponentControls.Effects.RotateAroundAxis::_reverse
	bool ____reverse_9;

public:
	inline static int32_t get_offset_of__speed_4() { return static_cast<int32_t>(offsetof(RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E, ____speed_4)); }
	inline int32_t get__speed_4() const { return ____speed_4; }
	inline int32_t* get_address_of__speed_4() { return &____speed_4; }
	inline void set__speed_4(int32_t value)
	{
		____speed_4 = value;
	}

	inline static int32_t get_offset_of__slowRotationSpeed_5() { return static_cast<int32_t>(offsetof(RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E, ____slowRotationSpeed_5)); }
	inline float get__slowRotationSpeed_5() const { return ____slowRotationSpeed_5; }
	inline float* get_address_of__slowRotationSpeed_5() { return &____slowRotationSpeed_5; }
	inline void set__slowRotationSpeed_5(float value)
	{
		____slowRotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of__fastRotationSpeed_6() { return static_cast<int32_t>(offsetof(RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E, ____fastRotationSpeed_6)); }
	inline float get__fastRotationSpeed_6() const { return ____fastRotationSpeed_6; }
	inline float* get_address_of__fastRotationSpeed_6() { return &____fastRotationSpeed_6; }
	inline void set__fastRotationSpeed_6(float value)
	{
		____fastRotationSpeed_6 = value;
	}

	inline static int32_t get_offset_of__rotateAroundAxis_7() { return static_cast<int32_t>(offsetof(RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E, ____rotateAroundAxis_7)); }
	inline int32_t get__rotateAroundAxis_7() const { return ____rotateAroundAxis_7; }
	inline int32_t* get_address_of__rotateAroundAxis_7() { return &____rotateAroundAxis_7; }
	inline void set__rotateAroundAxis_7(int32_t value)
	{
		____rotateAroundAxis_7 = value;
	}

	inline static int32_t get_offset_of__relativeTo_8() { return static_cast<int32_t>(offsetof(RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E, ____relativeTo_8)); }
	inline int32_t get__relativeTo_8() const { return ____relativeTo_8; }
	inline int32_t* get_address_of__relativeTo_8() { return &____relativeTo_8; }
	inline void set__relativeTo_8(int32_t value)
	{
		____relativeTo_8 = value;
	}

	inline static int32_t get_offset_of__reverse_9() { return static_cast<int32_t>(offsetof(RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E, ____reverse_9)); }
	inline bool get__reverse_9() const { return ____reverse_9; }
	inline bool* get_address_of__reverse_9() { return &____reverse_9; }
	inline void set__reverse_9(bool value)
	{
		____reverse_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEAROUNDAXIS_T8BDEC8749D2C59079DDE5BDF99810BEE630D244E_H
#ifndef TOUCHINTERACTABLETRANSITIONER_T35BA166309A7F0BAFB408D3C94D7FC841F150B39_H
#define TOUCHINTERACTABLETRANSITIONER_T35BA166309A7F0BAFB408D3C94D7FC841F150B39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Effects.TouchInteractableTransitioner
struct  TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Rewired.ComponentControls.Effects.TouchInteractableTransitioner::_visible
	bool ____visible_4;
	// Rewired.ComponentControls.TouchInteractable_TransitionTypeFlags Rewired.ComponentControls.Effects.TouchInteractableTransitioner::_transitionType
	int32_t ____transitionType_5;
	// UnityEngine.UI.ColorBlock Rewired.ComponentControls.Effects.TouchInteractableTransitioner::_transitionColorTint
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ____transitionColorTint_6;
	// UnityEngine.UI.SpriteState Rewired.ComponentControls.Effects.TouchInteractableTransitioner::_transitionSpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ____transitionSpriteState_7;
	// UnityEngine.UI.AnimationTriggers Rewired.ComponentControls.Effects.TouchInteractableTransitioner::_transitionAnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ____transitionAnimationTriggers_8;
	// UnityEngine.UI.Graphic Rewired.ComponentControls.Effects.TouchInteractableTransitioner::_targetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ____targetGraphic_9;
	// System.Boolean Rewired.ComponentControls.Effects.TouchInteractableTransitioner::_syncFadeDurationWithTransitionEvent
	bool ____syncFadeDurationWithTransitionEvent_10;
	// System.Boolean Rewired.ComponentControls.Effects.TouchInteractableTransitioner::_syncColorTintWithTransitionEvent
	bool ____syncColorTintWithTransitionEvent_11;
	// Rewired.ComponentControls.TouchInteractable_InteractionState Rewired.ComponentControls.Effects.TouchInteractableTransitioner::PqtQcSjSeTVUFsVUypYQJPZpbSs
	int32_t ___PqtQcSjSeTVUFsVUypYQJPZpbSs_12;

public:
	inline static int32_t get_offset_of__visible_4() { return static_cast<int32_t>(offsetof(TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39, ____visible_4)); }
	inline bool get__visible_4() const { return ____visible_4; }
	inline bool* get_address_of__visible_4() { return &____visible_4; }
	inline void set__visible_4(bool value)
	{
		____visible_4 = value;
	}

	inline static int32_t get_offset_of__transitionType_5() { return static_cast<int32_t>(offsetof(TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39, ____transitionType_5)); }
	inline int32_t get__transitionType_5() const { return ____transitionType_5; }
	inline int32_t* get_address_of__transitionType_5() { return &____transitionType_5; }
	inline void set__transitionType_5(int32_t value)
	{
		____transitionType_5 = value;
	}

	inline static int32_t get_offset_of__transitionColorTint_6() { return static_cast<int32_t>(offsetof(TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39, ____transitionColorTint_6)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get__transitionColorTint_6() const { return ____transitionColorTint_6; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of__transitionColorTint_6() { return &____transitionColorTint_6; }
	inline void set__transitionColorTint_6(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		____transitionColorTint_6 = value;
	}

	inline static int32_t get_offset_of__transitionSpriteState_7() { return static_cast<int32_t>(offsetof(TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39, ____transitionSpriteState_7)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get__transitionSpriteState_7() const { return ____transitionSpriteState_7; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of__transitionSpriteState_7() { return &____transitionSpriteState_7; }
	inline void set__transitionSpriteState_7(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		____transitionSpriteState_7 = value;
	}

	inline static int32_t get_offset_of__transitionAnimationTriggers_8() { return static_cast<int32_t>(offsetof(TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39, ____transitionAnimationTriggers_8)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get__transitionAnimationTriggers_8() const { return ____transitionAnimationTriggers_8; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of__transitionAnimationTriggers_8() { return &____transitionAnimationTriggers_8; }
	inline void set__transitionAnimationTriggers_8(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		____transitionAnimationTriggers_8 = value;
		Il2CppCodeGenWriteBarrier((&____transitionAnimationTriggers_8), value);
	}

	inline static int32_t get_offset_of__targetGraphic_9() { return static_cast<int32_t>(offsetof(TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39, ____targetGraphic_9)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get__targetGraphic_9() const { return ____targetGraphic_9; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of__targetGraphic_9() { return &____targetGraphic_9; }
	inline void set__targetGraphic_9(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		____targetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&____targetGraphic_9), value);
	}

	inline static int32_t get_offset_of__syncFadeDurationWithTransitionEvent_10() { return static_cast<int32_t>(offsetof(TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39, ____syncFadeDurationWithTransitionEvent_10)); }
	inline bool get__syncFadeDurationWithTransitionEvent_10() const { return ____syncFadeDurationWithTransitionEvent_10; }
	inline bool* get_address_of__syncFadeDurationWithTransitionEvent_10() { return &____syncFadeDurationWithTransitionEvent_10; }
	inline void set__syncFadeDurationWithTransitionEvent_10(bool value)
	{
		____syncFadeDurationWithTransitionEvent_10 = value;
	}

	inline static int32_t get_offset_of__syncColorTintWithTransitionEvent_11() { return static_cast<int32_t>(offsetof(TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39, ____syncColorTintWithTransitionEvent_11)); }
	inline bool get__syncColorTintWithTransitionEvent_11() const { return ____syncColorTintWithTransitionEvent_11; }
	inline bool* get_address_of__syncColorTintWithTransitionEvent_11() { return &____syncColorTintWithTransitionEvent_11; }
	inline void set__syncColorTintWithTransitionEvent_11(bool value)
	{
		____syncColorTintWithTransitionEvent_11 = value;
	}

	inline static int32_t get_offset_of_PqtQcSjSeTVUFsVUypYQJPZpbSs_12() { return static_cast<int32_t>(offsetof(TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39, ___PqtQcSjSeTVUFsVUypYQJPZpbSs_12)); }
	inline int32_t get_PqtQcSjSeTVUFsVUypYQJPZpbSs_12() const { return ___PqtQcSjSeTVUFsVUypYQJPZpbSs_12; }
	inline int32_t* get_address_of_PqtQcSjSeTVUFsVUypYQJPZpbSs_12() { return &___PqtQcSjSeTVUFsVUypYQJPZpbSs_12; }
	inline void set_PqtQcSjSeTVUFsVUypYQJPZpbSs_12(int32_t value)
	{
		___PqtQcSjSeTVUFsVUypYQJPZpbSs_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHINTERACTABLETRANSITIONER_T35BA166309A7F0BAFB408D3C94D7FC841F150B39_H
#ifndef TOUCHJOYSTICKANGLEINDICATOR_TBDE7E5ED4BADAA97015FE8FB45943F451313BD38_H
#define TOUCHJOYSTICKANGLEINDICATOR_TBDE7E5ED4BADAA97015FE8FB45943F451313BD38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator
struct  TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::_visible
	bool ____visible_4;
	// System.Boolean Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::_targetAngleFromRotation
	bool ____targetAngleFromRotation_5;
	// System.Single Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::_targetAngle
	float ____targetAngle_6;
	// System.Boolean Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::_fadeWithValue
	bool ____fadeWithValue_7;
	// System.Boolean Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::_fadeWithAngle
	bool ____fadeWithAngle_8;
	// System.Single Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::_fadeRange
	float ____fadeRange_9;
	// UnityEngine.Color Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::_activeColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____activeColor_10;
	// UnityEngine.Color Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::_normalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____normalColor_11;
	// UnityEngine.UI.Image Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::rKKdbmHeiCCQMaJgmJUEKzGjGBy
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___rKKdbmHeiCCQMaJgmJUEKzGjGBy_12;
	// UnityEngine.RectTransform Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::EaMOsLhjtTYBBHEPpOpEyoAPAdK
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___EaMOsLhjtTYBBHEPpOpEyoAPAdK_13;
	// UnityEngine.Vector2 Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::oLCXrVyPtTkSlhIIeWGCYgYYtsl
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oLCXrVyPtTkSlhIIeWGCYgYYtsl_14;
	// System.Boolean Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::UVHSsikRXlSwbjZfTYqbFsbelOK
	bool ___UVHSsikRXlSwbjZfTYqbFsbelOK_15;
	// Rewired.Utils.Interfaces.IRegistrar`1<Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator> Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator::nVsrZvdBAogFFZcvCTKaJhDldWc
	RuntimeObject* ___nVsrZvdBAogFFZcvCTKaJhDldWc_16;

public:
	inline static int32_t get_offset_of__visible_4() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ____visible_4)); }
	inline bool get__visible_4() const { return ____visible_4; }
	inline bool* get_address_of__visible_4() { return &____visible_4; }
	inline void set__visible_4(bool value)
	{
		____visible_4 = value;
	}

	inline static int32_t get_offset_of__targetAngleFromRotation_5() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ____targetAngleFromRotation_5)); }
	inline bool get__targetAngleFromRotation_5() const { return ____targetAngleFromRotation_5; }
	inline bool* get_address_of__targetAngleFromRotation_5() { return &____targetAngleFromRotation_5; }
	inline void set__targetAngleFromRotation_5(bool value)
	{
		____targetAngleFromRotation_5 = value;
	}

	inline static int32_t get_offset_of__targetAngle_6() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ____targetAngle_6)); }
	inline float get__targetAngle_6() const { return ____targetAngle_6; }
	inline float* get_address_of__targetAngle_6() { return &____targetAngle_6; }
	inline void set__targetAngle_6(float value)
	{
		____targetAngle_6 = value;
	}

	inline static int32_t get_offset_of__fadeWithValue_7() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ____fadeWithValue_7)); }
	inline bool get__fadeWithValue_7() const { return ____fadeWithValue_7; }
	inline bool* get_address_of__fadeWithValue_7() { return &____fadeWithValue_7; }
	inline void set__fadeWithValue_7(bool value)
	{
		____fadeWithValue_7 = value;
	}

	inline static int32_t get_offset_of__fadeWithAngle_8() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ____fadeWithAngle_8)); }
	inline bool get__fadeWithAngle_8() const { return ____fadeWithAngle_8; }
	inline bool* get_address_of__fadeWithAngle_8() { return &____fadeWithAngle_8; }
	inline void set__fadeWithAngle_8(bool value)
	{
		____fadeWithAngle_8 = value;
	}

	inline static int32_t get_offset_of__fadeRange_9() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ____fadeRange_9)); }
	inline float get__fadeRange_9() const { return ____fadeRange_9; }
	inline float* get_address_of__fadeRange_9() { return &____fadeRange_9; }
	inline void set__fadeRange_9(float value)
	{
		____fadeRange_9 = value;
	}

	inline static int32_t get_offset_of__activeColor_10() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ____activeColor_10)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__activeColor_10() const { return ____activeColor_10; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__activeColor_10() { return &____activeColor_10; }
	inline void set__activeColor_10(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____activeColor_10 = value;
	}

	inline static int32_t get_offset_of__normalColor_11() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ____normalColor_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__normalColor_11() const { return ____normalColor_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__normalColor_11() { return &____normalColor_11; }
	inline void set__normalColor_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____normalColor_11 = value;
	}

	inline static int32_t get_offset_of_rKKdbmHeiCCQMaJgmJUEKzGjGBy_12() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ___rKKdbmHeiCCQMaJgmJUEKzGjGBy_12)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_rKKdbmHeiCCQMaJgmJUEKzGjGBy_12() const { return ___rKKdbmHeiCCQMaJgmJUEKzGjGBy_12; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_rKKdbmHeiCCQMaJgmJUEKzGjGBy_12() { return &___rKKdbmHeiCCQMaJgmJUEKzGjGBy_12; }
	inline void set_rKKdbmHeiCCQMaJgmJUEKzGjGBy_12(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___rKKdbmHeiCCQMaJgmJUEKzGjGBy_12 = value;
		Il2CppCodeGenWriteBarrier((&___rKKdbmHeiCCQMaJgmJUEKzGjGBy_12), value);
	}

	inline static int32_t get_offset_of_EaMOsLhjtTYBBHEPpOpEyoAPAdK_13() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ___EaMOsLhjtTYBBHEPpOpEyoAPAdK_13)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_EaMOsLhjtTYBBHEPpOpEyoAPAdK_13() const { return ___EaMOsLhjtTYBBHEPpOpEyoAPAdK_13; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_EaMOsLhjtTYBBHEPpOpEyoAPAdK_13() { return &___EaMOsLhjtTYBBHEPpOpEyoAPAdK_13; }
	inline void set_EaMOsLhjtTYBBHEPpOpEyoAPAdK_13(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___EaMOsLhjtTYBBHEPpOpEyoAPAdK_13 = value;
		Il2CppCodeGenWriteBarrier((&___EaMOsLhjtTYBBHEPpOpEyoAPAdK_13), value);
	}

	inline static int32_t get_offset_of_oLCXrVyPtTkSlhIIeWGCYgYYtsl_14() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ___oLCXrVyPtTkSlhIIeWGCYgYYtsl_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oLCXrVyPtTkSlhIIeWGCYgYYtsl_14() const { return ___oLCXrVyPtTkSlhIIeWGCYgYYtsl_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oLCXrVyPtTkSlhIIeWGCYgYYtsl_14() { return &___oLCXrVyPtTkSlhIIeWGCYgYYtsl_14; }
	inline void set_oLCXrVyPtTkSlhIIeWGCYgYYtsl_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oLCXrVyPtTkSlhIIeWGCYgYYtsl_14 = value;
	}

	inline static int32_t get_offset_of_UVHSsikRXlSwbjZfTYqbFsbelOK_15() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ___UVHSsikRXlSwbjZfTYqbFsbelOK_15)); }
	inline bool get_UVHSsikRXlSwbjZfTYqbFsbelOK_15() const { return ___UVHSsikRXlSwbjZfTYqbFsbelOK_15; }
	inline bool* get_address_of_UVHSsikRXlSwbjZfTYqbFsbelOK_15() { return &___UVHSsikRXlSwbjZfTYqbFsbelOK_15; }
	inline void set_UVHSsikRXlSwbjZfTYqbFsbelOK_15(bool value)
	{
		___UVHSsikRXlSwbjZfTYqbFsbelOK_15 = value;
	}

	inline static int32_t get_offset_of_nVsrZvdBAogFFZcvCTKaJhDldWc_16() { return static_cast<int32_t>(offsetof(TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38, ___nVsrZvdBAogFFZcvCTKaJhDldWc_16)); }
	inline RuntimeObject* get_nVsrZvdBAogFFZcvCTKaJhDldWc_16() const { return ___nVsrZvdBAogFFZcvCTKaJhDldWc_16; }
	inline RuntimeObject** get_address_of_nVsrZvdBAogFFZcvCTKaJhDldWc_16() { return &___nVsrZvdBAogFFZcvCTKaJhDldWc_16; }
	inline void set_nVsrZvdBAogFFZcvCTKaJhDldWc_16(RuntimeObject* value)
	{
		___nVsrZvdBAogFFZcvCTKaJhDldWc_16 = value;
		Il2CppCodeGenWriteBarrier((&___nVsrZvdBAogFFZcvCTKaJhDldWc_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHJOYSTICKANGLEINDICATOR_TBDE7E5ED4BADAA97015FE8FB45943F451313BD38_H
#ifndef TOUCHJOYSTICKRADIALINDICATOR_T67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE_H
#define TOUCHJOYSTICKRADIALINDICATOR_T67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.Effects.TouchJoystickRadialIndicator
struct  TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Rewired.ComponentControls.Effects.TouchJoystickRadialIndicator::_scale
	bool ____scale_4;
	// System.Boolean Rewired.ComponentControls.Effects.TouchJoystickRadialIndicator::_preserveSpriteAspectRatio
	bool ____preserveSpriteAspectRatio_5;
	// System.Single Rewired.ComponentControls.Effects.TouchJoystickRadialIndicator::_scaleRatio
	float ____scaleRatio_6;
	// System.Single Rewired.ComponentControls.Effects.TouchJoystickRadialIndicator::_aspectRatioX
	float ____aspectRatioX_7;
	// System.Single Rewired.ComponentControls.Effects.TouchJoystickRadialIndicator::_aspectRatioY
	float ____aspectRatioY_8;
	// System.Single Rewired.ComponentControls.Effects.TouchJoystickRadialIndicator::_offset
	float ____offset_9;
	// UnityEngine.RectTransform Rewired.ComponentControls.Effects.TouchJoystickRadialIndicator::EaMOsLhjtTYBBHEPpOpEyoAPAdK
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___EaMOsLhjtTYBBHEPpOpEyoAPAdK_11;
	// System.Collections.Generic.List`1<Rewired.ComponentControls.Effects.TouchJoystickAngleIndicator> Rewired.ComponentControls.Effects.TouchJoystickRadialIndicator::WGFHKIeFWLGlqMVqbtJnErXsASW
	List_1_tAFD7C206B4FF8059B628B709228AFB4E6822DC68 * ___WGFHKIeFWLGlqMVqbtJnErXsASW_12;

public:
	inline static int32_t get_offset_of__scale_4() { return static_cast<int32_t>(offsetof(TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE, ____scale_4)); }
	inline bool get__scale_4() const { return ____scale_4; }
	inline bool* get_address_of__scale_4() { return &____scale_4; }
	inline void set__scale_4(bool value)
	{
		____scale_4 = value;
	}

	inline static int32_t get_offset_of__preserveSpriteAspectRatio_5() { return static_cast<int32_t>(offsetof(TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE, ____preserveSpriteAspectRatio_5)); }
	inline bool get__preserveSpriteAspectRatio_5() const { return ____preserveSpriteAspectRatio_5; }
	inline bool* get_address_of__preserveSpriteAspectRatio_5() { return &____preserveSpriteAspectRatio_5; }
	inline void set__preserveSpriteAspectRatio_5(bool value)
	{
		____preserveSpriteAspectRatio_5 = value;
	}

	inline static int32_t get_offset_of__scaleRatio_6() { return static_cast<int32_t>(offsetof(TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE, ____scaleRatio_6)); }
	inline float get__scaleRatio_6() const { return ____scaleRatio_6; }
	inline float* get_address_of__scaleRatio_6() { return &____scaleRatio_6; }
	inline void set__scaleRatio_6(float value)
	{
		____scaleRatio_6 = value;
	}

	inline static int32_t get_offset_of__aspectRatioX_7() { return static_cast<int32_t>(offsetof(TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE, ____aspectRatioX_7)); }
	inline float get__aspectRatioX_7() const { return ____aspectRatioX_7; }
	inline float* get_address_of__aspectRatioX_7() { return &____aspectRatioX_7; }
	inline void set__aspectRatioX_7(float value)
	{
		____aspectRatioX_7 = value;
	}

	inline static int32_t get_offset_of__aspectRatioY_8() { return static_cast<int32_t>(offsetof(TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE, ____aspectRatioY_8)); }
	inline float get__aspectRatioY_8() const { return ____aspectRatioY_8; }
	inline float* get_address_of__aspectRatioY_8() { return &____aspectRatioY_8; }
	inline void set__aspectRatioY_8(float value)
	{
		____aspectRatioY_8 = value;
	}

	inline static int32_t get_offset_of__offset_9() { return static_cast<int32_t>(offsetof(TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE, ____offset_9)); }
	inline float get__offset_9() const { return ____offset_9; }
	inline float* get_address_of__offset_9() { return &____offset_9; }
	inline void set__offset_9(float value)
	{
		____offset_9 = value;
	}

	inline static int32_t get_offset_of_EaMOsLhjtTYBBHEPpOpEyoAPAdK_11() { return static_cast<int32_t>(offsetof(TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE, ___EaMOsLhjtTYBBHEPpOpEyoAPAdK_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_EaMOsLhjtTYBBHEPpOpEyoAPAdK_11() const { return ___EaMOsLhjtTYBBHEPpOpEyoAPAdK_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_EaMOsLhjtTYBBHEPpOpEyoAPAdK_11() { return &___EaMOsLhjtTYBBHEPpOpEyoAPAdK_11; }
	inline void set_EaMOsLhjtTYBBHEPpOpEyoAPAdK_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___EaMOsLhjtTYBBHEPpOpEyoAPAdK_11 = value;
		Il2CppCodeGenWriteBarrier((&___EaMOsLhjtTYBBHEPpOpEyoAPAdK_11), value);
	}

	inline static int32_t get_offset_of_WGFHKIeFWLGlqMVqbtJnErXsASW_12() { return static_cast<int32_t>(offsetof(TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE, ___WGFHKIeFWLGlqMVqbtJnErXsASW_12)); }
	inline List_1_tAFD7C206B4FF8059B628B709228AFB4E6822DC68 * get_WGFHKIeFWLGlqMVqbtJnErXsASW_12() const { return ___WGFHKIeFWLGlqMVqbtJnErXsASW_12; }
	inline List_1_tAFD7C206B4FF8059B628B709228AFB4E6822DC68 ** get_address_of_WGFHKIeFWLGlqMVqbtJnErXsASW_12() { return &___WGFHKIeFWLGlqMVqbtJnErXsASW_12; }
	inline void set_WGFHKIeFWLGlqMVqbtJnErXsASW_12(List_1_tAFD7C206B4FF8059B628B709228AFB4E6822DC68 * value)
	{
		___WGFHKIeFWLGlqMVqbtJnErXsASW_12 = value;
		Il2CppCodeGenWriteBarrier((&___WGFHKIeFWLGlqMVqbtJnErXsASW_12), value);
	}
};

struct TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE_StaticFields
{
public:
	// UnityEngine.Vector2 Rewired.ComponentControls.Effects.TouchJoystickRadialIndicator::YqFjPGpeuVROMhanOhJFCnjeIPlk
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___YqFjPGpeuVROMhanOhJFCnjeIPlk_10;

public:
	inline static int32_t get_offset_of_YqFjPGpeuVROMhanOhJFCnjeIPlk_10() { return static_cast<int32_t>(offsetof(TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE_StaticFields, ___YqFjPGpeuVROMhanOhJFCnjeIPlk_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_YqFjPGpeuVROMhanOhJFCnjeIPlk_10() const { return ___YqFjPGpeuVROMhanOhJFCnjeIPlk_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_YqFjPGpeuVROMhanOhJFCnjeIPlk_10() { return &___YqFjPGpeuVROMhanOhJFCnjeIPlk_10; }
	inline void set_YqFjPGpeuVROMhanOhJFCnjeIPlk_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___YqFjPGpeuVROMhanOhJFCnjeIPlk_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHJOYSTICKRADIALINDICATOR_T67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE_H
#ifndef COMPONENTWRAPPER_1_TA70617D66AF23B03198171AF8040B86543ED0C99_H
#define COMPONENTWRAPPER_1_TA70617D66AF23B03198171AF8040B86543ED0C99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Components.ComponentWrapper`1<Rewired.PlayerController>
struct  ComponentWrapper_1_tA70617D66AF23B03198171AF8040B86543ED0C99  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// T Rewired.Components.ComponentWrapper`1::gkqUnrSMeLSOlffXjmHcoYDAlQT
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01 * ___gkqUnrSMeLSOlffXjmHcoYDAlQT_4;
	// System.Boolean Rewired.Components.ComponentWrapper`1::oGbqxjxXGgvgXNLPUMbdLzEDsmT
	bool ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5;

public:
	inline static int32_t get_offset_of_gkqUnrSMeLSOlffXjmHcoYDAlQT_4() { return static_cast<int32_t>(offsetof(ComponentWrapper_1_tA70617D66AF23B03198171AF8040B86543ED0C99, ___gkqUnrSMeLSOlffXjmHcoYDAlQT_4)); }
	inline PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01 * get_gkqUnrSMeLSOlffXjmHcoYDAlQT_4() const { return ___gkqUnrSMeLSOlffXjmHcoYDAlQT_4; }
	inline PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01 ** get_address_of_gkqUnrSMeLSOlffXjmHcoYDAlQT_4() { return &___gkqUnrSMeLSOlffXjmHcoYDAlQT_4; }
	inline void set_gkqUnrSMeLSOlffXjmHcoYDAlQT_4(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01 * value)
	{
		___gkqUnrSMeLSOlffXjmHcoYDAlQT_4 = value;
		Il2CppCodeGenWriteBarrier((&___gkqUnrSMeLSOlffXjmHcoYDAlQT_4), value);
	}

	inline static int32_t get_offset_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5() { return static_cast<int32_t>(offsetof(ComponentWrapper_1_tA70617D66AF23B03198171AF8040B86543ED0C99, ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5)); }
	inline bool get_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5() const { return ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5; }
	inline bool* get_address_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5() { return &___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5; }
	inline void set_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5(bool value)
	{
		___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTWRAPPER_1_TA70617D66AF23B03198171AF8040B86543ED0C99_H
#ifndef GUITEXT_T84576ACF2926E50EB99E8E3EAB1737B0E96F1920_H
#define GUITEXT_T84576ACF2926E50EB99E8E3EAB1737B0E96F1920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Internal.GUIText
struct  GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Rewired.Internal.GUIText::fxPyosdzuUVfIoTnoatkzaIYYJl
	String_t* ___fxPyosdzuUVfIoTnoatkzaIYYJl_4;
	// UnityEngine.GUIStyle Rewired.Internal.GUIText::WnjAhrAihdtxFulBRrgxcITudfz
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___WnjAhrAihdtxFulBRrgxcITudfz_5;
	// UnityEngine.TextAnchor Rewired.Internal.GUIText::iKscsEulwuetlYvVYaPeaFTNJSZg
	int32_t ___iKscsEulwuetlYvVYaPeaFTNJSZg_6;
	// UnityEngine.TextAlignment Rewired.Internal.GUIText::fFvHsAznMvcgKhHLgxHxQEdYLHGA
	int32_t ___fFvHsAznMvcgKhHLgxHxQEdYLHGA_7;
	// System.Single Rewired.Internal.GUIText::nVBbSBJrzDVLjxPpLnFWRJrqpnda
	float ___nVBbSBJrzDVLjxPpLnFWRJrqpnda_8;
	// UnityEngine.Font Rewired.Internal.GUIText::njfbYhoZSXSBcINfTcGIzZCBwGA
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ___njfbYhoZSXSBcINfTcGIzZCBwGA_9;
	// System.Int32 Rewired.Internal.GUIText::ReexRVDwIAoRObWPqAOJHgFFIFv
	int32_t ___ReexRVDwIAoRObWPqAOJHgFFIFv_10;
	// UnityEngine.FontStyle Rewired.Internal.GUIText::XlWchbacyFmSpmbRcmxDYFgMIbEY
	int32_t ___XlWchbacyFmSpmbRcmxDYFgMIbEY_11;
	// UnityEngine.Color Rewired.Internal.GUIText::XwkVKMsVgfUYwrPiIOQInqbsgTC
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___XwkVKMsVgfUYwrPiIOQInqbsgTC_12;
	// UnityEngine.Vector2 Rewired.Internal.GUIText::_pixelOffset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____pixelOffset_13;
	// System.Boolean Rewired.Internal.GUIText::_useUnityUI
	bool ____useUnityUI_14;
	// System.Boolean Rewired.Internal.GUIText::NMtBtkcgqEpJeDczKXGWnquCbUp
	bool ___NMtBtkcgqEpJeDczKXGWnquCbUp_15;
	// System.Boolean Rewired.Internal.GUIText::mZHeAdFbfsOSZjQmUHTYdAFVBkW
	bool ___mZHeAdFbfsOSZjQmUHTYdAFVBkW_16;
	// System.Boolean Rewired.Internal.GUIText::qIicGepqBUspmCvklMSTGPkPgKW
	bool ___qIicGepqBUspmCvklMSTGPkPgKW_17;
	// System.Boolean Rewired.Internal.GUIText::AXWGqqdBpbPPSPNwXyPAeBjKIme
	bool ___AXWGqqdBpbPPSPNwXyPAeBjKIme_18;
	// System.Boolean Rewired.Internal.GUIText::MDOeNGhMUWnxCcHRFQHentJOMIzC
	bool ___MDOeNGhMUWnxCcHRFQHentJOMIzC_19;
	// System.Boolean Rewired.Internal.GUIText::WaaJhTkGlowlywsKGOEygWgGAaRl
	bool ___WaaJhTkGlowlywsKGOEygWgGAaRl_20;
	// System.Boolean Rewired.Internal.GUIText::EFUqwAyKlRDHWdJuqghmZEOuYIp
	bool ___EFUqwAyKlRDHWdJuqghmZEOuYIp_21;
	// UnityEngine.UI.Text Rewired.Internal.GUIText::oDCGxHSOOZReIUovZMkPhxDnBKL
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___oDCGxHSOOZReIUovZMkPhxDnBKL_22;
	// System.Boolean Rewired.Internal.GUIText::uqDbFpLJXfOjUFlulTwwZNfYWKm
	bool ___uqDbFpLJXfOjUFlulTwwZNfYWKm_23;
	// System.Boolean Rewired.Internal.GUIText::FwzTBmzDMooWVymcTdymLOTmSVP
	bool ___FwzTBmzDMooWVymcTdymLOTmSVP_24;

public:
	inline static int32_t get_offset_of_fxPyosdzuUVfIoTnoatkzaIYYJl_4() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___fxPyosdzuUVfIoTnoatkzaIYYJl_4)); }
	inline String_t* get_fxPyosdzuUVfIoTnoatkzaIYYJl_4() const { return ___fxPyosdzuUVfIoTnoatkzaIYYJl_4; }
	inline String_t** get_address_of_fxPyosdzuUVfIoTnoatkzaIYYJl_4() { return &___fxPyosdzuUVfIoTnoatkzaIYYJl_4; }
	inline void set_fxPyosdzuUVfIoTnoatkzaIYYJl_4(String_t* value)
	{
		___fxPyosdzuUVfIoTnoatkzaIYYJl_4 = value;
		Il2CppCodeGenWriteBarrier((&___fxPyosdzuUVfIoTnoatkzaIYYJl_4), value);
	}

	inline static int32_t get_offset_of_WnjAhrAihdtxFulBRrgxcITudfz_5() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___WnjAhrAihdtxFulBRrgxcITudfz_5)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_WnjAhrAihdtxFulBRrgxcITudfz_5() const { return ___WnjAhrAihdtxFulBRrgxcITudfz_5; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_WnjAhrAihdtxFulBRrgxcITudfz_5() { return &___WnjAhrAihdtxFulBRrgxcITudfz_5; }
	inline void set_WnjAhrAihdtxFulBRrgxcITudfz_5(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___WnjAhrAihdtxFulBRrgxcITudfz_5 = value;
		Il2CppCodeGenWriteBarrier((&___WnjAhrAihdtxFulBRrgxcITudfz_5), value);
	}

	inline static int32_t get_offset_of_iKscsEulwuetlYvVYaPeaFTNJSZg_6() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___iKscsEulwuetlYvVYaPeaFTNJSZg_6)); }
	inline int32_t get_iKscsEulwuetlYvVYaPeaFTNJSZg_6() const { return ___iKscsEulwuetlYvVYaPeaFTNJSZg_6; }
	inline int32_t* get_address_of_iKscsEulwuetlYvVYaPeaFTNJSZg_6() { return &___iKscsEulwuetlYvVYaPeaFTNJSZg_6; }
	inline void set_iKscsEulwuetlYvVYaPeaFTNJSZg_6(int32_t value)
	{
		___iKscsEulwuetlYvVYaPeaFTNJSZg_6 = value;
	}

	inline static int32_t get_offset_of_fFvHsAznMvcgKhHLgxHxQEdYLHGA_7() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___fFvHsAznMvcgKhHLgxHxQEdYLHGA_7)); }
	inline int32_t get_fFvHsAznMvcgKhHLgxHxQEdYLHGA_7() const { return ___fFvHsAznMvcgKhHLgxHxQEdYLHGA_7; }
	inline int32_t* get_address_of_fFvHsAznMvcgKhHLgxHxQEdYLHGA_7() { return &___fFvHsAznMvcgKhHLgxHxQEdYLHGA_7; }
	inline void set_fFvHsAznMvcgKhHLgxHxQEdYLHGA_7(int32_t value)
	{
		___fFvHsAznMvcgKhHLgxHxQEdYLHGA_7 = value;
	}

	inline static int32_t get_offset_of_nVBbSBJrzDVLjxPpLnFWRJrqpnda_8() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___nVBbSBJrzDVLjxPpLnFWRJrqpnda_8)); }
	inline float get_nVBbSBJrzDVLjxPpLnFWRJrqpnda_8() const { return ___nVBbSBJrzDVLjxPpLnFWRJrqpnda_8; }
	inline float* get_address_of_nVBbSBJrzDVLjxPpLnFWRJrqpnda_8() { return &___nVBbSBJrzDVLjxPpLnFWRJrqpnda_8; }
	inline void set_nVBbSBJrzDVLjxPpLnFWRJrqpnda_8(float value)
	{
		___nVBbSBJrzDVLjxPpLnFWRJrqpnda_8 = value;
	}

	inline static int32_t get_offset_of_njfbYhoZSXSBcINfTcGIzZCBwGA_9() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___njfbYhoZSXSBcINfTcGIzZCBwGA_9)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get_njfbYhoZSXSBcINfTcGIzZCBwGA_9() const { return ___njfbYhoZSXSBcINfTcGIzZCBwGA_9; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of_njfbYhoZSXSBcINfTcGIzZCBwGA_9() { return &___njfbYhoZSXSBcINfTcGIzZCBwGA_9; }
	inline void set_njfbYhoZSXSBcINfTcGIzZCBwGA_9(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		___njfbYhoZSXSBcINfTcGIzZCBwGA_9 = value;
		Il2CppCodeGenWriteBarrier((&___njfbYhoZSXSBcINfTcGIzZCBwGA_9), value);
	}

	inline static int32_t get_offset_of_ReexRVDwIAoRObWPqAOJHgFFIFv_10() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___ReexRVDwIAoRObWPqAOJHgFFIFv_10)); }
	inline int32_t get_ReexRVDwIAoRObWPqAOJHgFFIFv_10() const { return ___ReexRVDwIAoRObWPqAOJHgFFIFv_10; }
	inline int32_t* get_address_of_ReexRVDwIAoRObWPqAOJHgFFIFv_10() { return &___ReexRVDwIAoRObWPqAOJHgFFIFv_10; }
	inline void set_ReexRVDwIAoRObWPqAOJHgFFIFv_10(int32_t value)
	{
		___ReexRVDwIAoRObWPqAOJHgFFIFv_10 = value;
	}

	inline static int32_t get_offset_of_XlWchbacyFmSpmbRcmxDYFgMIbEY_11() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___XlWchbacyFmSpmbRcmxDYFgMIbEY_11)); }
	inline int32_t get_XlWchbacyFmSpmbRcmxDYFgMIbEY_11() const { return ___XlWchbacyFmSpmbRcmxDYFgMIbEY_11; }
	inline int32_t* get_address_of_XlWchbacyFmSpmbRcmxDYFgMIbEY_11() { return &___XlWchbacyFmSpmbRcmxDYFgMIbEY_11; }
	inline void set_XlWchbacyFmSpmbRcmxDYFgMIbEY_11(int32_t value)
	{
		___XlWchbacyFmSpmbRcmxDYFgMIbEY_11 = value;
	}

	inline static int32_t get_offset_of_XwkVKMsVgfUYwrPiIOQInqbsgTC_12() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___XwkVKMsVgfUYwrPiIOQInqbsgTC_12)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_XwkVKMsVgfUYwrPiIOQInqbsgTC_12() const { return ___XwkVKMsVgfUYwrPiIOQInqbsgTC_12; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_XwkVKMsVgfUYwrPiIOQInqbsgTC_12() { return &___XwkVKMsVgfUYwrPiIOQInqbsgTC_12; }
	inline void set_XwkVKMsVgfUYwrPiIOQInqbsgTC_12(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___XwkVKMsVgfUYwrPiIOQInqbsgTC_12 = value;
	}

	inline static int32_t get_offset_of__pixelOffset_13() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ____pixelOffset_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__pixelOffset_13() const { return ____pixelOffset_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__pixelOffset_13() { return &____pixelOffset_13; }
	inline void set__pixelOffset_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____pixelOffset_13 = value;
	}

	inline static int32_t get_offset_of__useUnityUI_14() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ____useUnityUI_14)); }
	inline bool get__useUnityUI_14() const { return ____useUnityUI_14; }
	inline bool* get_address_of__useUnityUI_14() { return &____useUnityUI_14; }
	inline void set__useUnityUI_14(bool value)
	{
		____useUnityUI_14 = value;
	}

	inline static int32_t get_offset_of_NMtBtkcgqEpJeDczKXGWnquCbUp_15() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___NMtBtkcgqEpJeDczKXGWnquCbUp_15)); }
	inline bool get_NMtBtkcgqEpJeDczKXGWnquCbUp_15() const { return ___NMtBtkcgqEpJeDczKXGWnquCbUp_15; }
	inline bool* get_address_of_NMtBtkcgqEpJeDczKXGWnquCbUp_15() { return &___NMtBtkcgqEpJeDczKXGWnquCbUp_15; }
	inline void set_NMtBtkcgqEpJeDczKXGWnquCbUp_15(bool value)
	{
		___NMtBtkcgqEpJeDczKXGWnquCbUp_15 = value;
	}

	inline static int32_t get_offset_of_mZHeAdFbfsOSZjQmUHTYdAFVBkW_16() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___mZHeAdFbfsOSZjQmUHTYdAFVBkW_16)); }
	inline bool get_mZHeAdFbfsOSZjQmUHTYdAFVBkW_16() const { return ___mZHeAdFbfsOSZjQmUHTYdAFVBkW_16; }
	inline bool* get_address_of_mZHeAdFbfsOSZjQmUHTYdAFVBkW_16() { return &___mZHeAdFbfsOSZjQmUHTYdAFVBkW_16; }
	inline void set_mZHeAdFbfsOSZjQmUHTYdAFVBkW_16(bool value)
	{
		___mZHeAdFbfsOSZjQmUHTYdAFVBkW_16 = value;
	}

	inline static int32_t get_offset_of_qIicGepqBUspmCvklMSTGPkPgKW_17() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___qIicGepqBUspmCvklMSTGPkPgKW_17)); }
	inline bool get_qIicGepqBUspmCvklMSTGPkPgKW_17() const { return ___qIicGepqBUspmCvklMSTGPkPgKW_17; }
	inline bool* get_address_of_qIicGepqBUspmCvklMSTGPkPgKW_17() { return &___qIicGepqBUspmCvklMSTGPkPgKW_17; }
	inline void set_qIicGepqBUspmCvklMSTGPkPgKW_17(bool value)
	{
		___qIicGepqBUspmCvklMSTGPkPgKW_17 = value;
	}

	inline static int32_t get_offset_of_AXWGqqdBpbPPSPNwXyPAeBjKIme_18() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___AXWGqqdBpbPPSPNwXyPAeBjKIme_18)); }
	inline bool get_AXWGqqdBpbPPSPNwXyPAeBjKIme_18() const { return ___AXWGqqdBpbPPSPNwXyPAeBjKIme_18; }
	inline bool* get_address_of_AXWGqqdBpbPPSPNwXyPAeBjKIme_18() { return &___AXWGqqdBpbPPSPNwXyPAeBjKIme_18; }
	inline void set_AXWGqqdBpbPPSPNwXyPAeBjKIme_18(bool value)
	{
		___AXWGqqdBpbPPSPNwXyPAeBjKIme_18 = value;
	}

	inline static int32_t get_offset_of_MDOeNGhMUWnxCcHRFQHentJOMIzC_19() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___MDOeNGhMUWnxCcHRFQHentJOMIzC_19)); }
	inline bool get_MDOeNGhMUWnxCcHRFQHentJOMIzC_19() const { return ___MDOeNGhMUWnxCcHRFQHentJOMIzC_19; }
	inline bool* get_address_of_MDOeNGhMUWnxCcHRFQHentJOMIzC_19() { return &___MDOeNGhMUWnxCcHRFQHentJOMIzC_19; }
	inline void set_MDOeNGhMUWnxCcHRFQHentJOMIzC_19(bool value)
	{
		___MDOeNGhMUWnxCcHRFQHentJOMIzC_19 = value;
	}

	inline static int32_t get_offset_of_WaaJhTkGlowlywsKGOEygWgGAaRl_20() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___WaaJhTkGlowlywsKGOEygWgGAaRl_20)); }
	inline bool get_WaaJhTkGlowlywsKGOEygWgGAaRl_20() const { return ___WaaJhTkGlowlywsKGOEygWgGAaRl_20; }
	inline bool* get_address_of_WaaJhTkGlowlywsKGOEygWgGAaRl_20() { return &___WaaJhTkGlowlywsKGOEygWgGAaRl_20; }
	inline void set_WaaJhTkGlowlywsKGOEygWgGAaRl_20(bool value)
	{
		___WaaJhTkGlowlywsKGOEygWgGAaRl_20 = value;
	}

	inline static int32_t get_offset_of_EFUqwAyKlRDHWdJuqghmZEOuYIp_21() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___EFUqwAyKlRDHWdJuqghmZEOuYIp_21)); }
	inline bool get_EFUqwAyKlRDHWdJuqghmZEOuYIp_21() const { return ___EFUqwAyKlRDHWdJuqghmZEOuYIp_21; }
	inline bool* get_address_of_EFUqwAyKlRDHWdJuqghmZEOuYIp_21() { return &___EFUqwAyKlRDHWdJuqghmZEOuYIp_21; }
	inline void set_EFUqwAyKlRDHWdJuqghmZEOuYIp_21(bool value)
	{
		___EFUqwAyKlRDHWdJuqghmZEOuYIp_21 = value;
	}

	inline static int32_t get_offset_of_oDCGxHSOOZReIUovZMkPhxDnBKL_22() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___oDCGxHSOOZReIUovZMkPhxDnBKL_22)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_oDCGxHSOOZReIUovZMkPhxDnBKL_22() const { return ___oDCGxHSOOZReIUovZMkPhxDnBKL_22; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_oDCGxHSOOZReIUovZMkPhxDnBKL_22() { return &___oDCGxHSOOZReIUovZMkPhxDnBKL_22; }
	inline void set_oDCGxHSOOZReIUovZMkPhxDnBKL_22(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___oDCGxHSOOZReIUovZMkPhxDnBKL_22 = value;
		Il2CppCodeGenWriteBarrier((&___oDCGxHSOOZReIUovZMkPhxDnBKL_22), value);
	}

	inline static int32_t get_offset_of_uqDbFpLJXfOjUFlulTwwZNfYWKm_23() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___uqDbFpLJXfOjUFlulTwwZNfYWKm_23)); }
	inline bool get_uqDbFpLJXfOjUFlulTwwZNfYWKm_23() const { return ___uqDbFpLJXfOjUFlulTwwZNfYWKm_23; }
	inline bool* get_address_of_uqDbFpLJXfOjUFlulTwwZNfYWKm_23() { return &___uqDbFpLJXfOjUFlulTwwZNfYWKm_23; }
	inline void set_uqDbFpLJXfOjUFlulTwwZNfYWKm_23(bool value)
	{
		___uqDbFpLJXfOjUFlulTwwZNfYWKm_23 = value;
	}

	inline static int32_t get_offset_of_FwzTBmzDMooWVymcTdymLOTmSVP_24() { return static_cast<int32_t>(offsetof(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920, ___FwzTBmzDMooWVymcTdymLOTmSVP_24)); }
	inline bool get_FwzTBmzDMooWVymcTdymLOTmSVP_24() const { return ___FwzTBmzDMooWVymcTdymLOTmSVP_24; }
	inline bool* get_address_of_FwzTBmzDMooWVymcTdymLOTmSVP_24() { return &___FwzTBmzDMooWVymcTdymLOTmSVP_24; }
	inline void set_FwzTBmzDMooWVymcTdymLOTmSVP_24(bool value)
	{
		___FwzTBmzDMooWVymcTdymLOTmSVP_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITEXT_T84576ACF2926E50EB99E8E3EAB1737B0E96F1920_H
#ifndef CUSTOMCONTROLLER_TDA75F822EE3D9041FE381092FE88F41FE6AB7CBF_H
#define CUSTOMCONTROLLER_TDA75F822EE3D9041FE381092FE88F41FE6AB7CBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.CustomController
struct  CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF  : public ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE
{
public:
	// Rewired.InputManager_Base Rewired.ComponentControls.CustomController::_rewiredInputManager
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * ____rewiredInputManager_7;
	// Rewired.ComponentControls.Data.CustomControllerSelector Rewired.ComponentControls.CustomController::_customControllerSelector
	CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1 * ____customControllerSelector_8;
	// Rewired.ComponentControls.CustomController_CreateCustomControllerSettings Rewired.ComponentControls.CustomController::_createCustomControllerSettings
	CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7 * ____createCustomControllerSettings_9;
	// System.Collections.Generic.List`1<Rewired.ComponentControls.CustomController_InputEvent> Rewired.ComponentControls.CustomController::_inputEvents
	List_1_tC3916F1B7F67B72C4C10D463D531CC7944331D43 * ____inputEvents_10;
	// System.Int32 Rewired.ComponentControls.CustomController::_createdCustomControllerId
	int32_t ____createdCustomControllerId_11;
	// System.Action Rewired.ComponentControls.CustomController::_InputSourceUpdateEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____InputSourceUpdateEvent_12;

public:
	inline static int32_t get_offset_of__rewiredInputManager_7() { return static_cast<int32_t>(offsetof(CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF, ____rewiredInputManager_7)); }
	inline InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * get__rewiredInputManager_7() const { return ____rewiredInputManager_7; }
	inline InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 ** get_address_of__rewiredInputManager_7() { return &____rewiredInputManager_7; }
	inline void set__rewiredInputManager_7(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * value)
	{
		____rewiredInputManager_7 = value;
		Il2CppCodeGenWriteBarrier((&____rewiredInputManager_7), value);
	}

	inline static int32_t get_offset_of__customControllerSelector_8() { return static_cast<int32_t>(offsetof(CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF, ____customControllerSelector_8)); }
	inline CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1 * get__customControllerSelector_8() const { return ____customControllerSelector_8; }
	inline CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1 ** get_address_of__customControllerSelector_8() { return &____customControllerSelector_8; }
	inline void set__customControllerSelector_8(CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1 * value)
	{
		____customControllerSelector_8 = value;
		Il2CppCodeGenWriteBarrier((&____customControllerSelector_8), value);
	}

	inline static int32_t get_offset_of__createCustomControllerSettings_9() { return static_cast<int32_t>(offsetof(CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF, ____createCustomControllerSettings_9)); }
	inline CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7 * get__createCustomControllerSettings_9() const { return ____createCustomControllerSettings_9; }
	inline CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7 ** get_address_of__createCustomControllerSettings_9() { return &____createCustomControllerSettings_9; }
	inline void set__createCustomControllerSettings_9(CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7 * value)
	{
		____createCustomControllerSettings_9 = value;
		Il2CppCodeGenWriteBarrier((&____createCustomControllerSettings_9), value);
	}

	inline static int32_t get_offset_of__inputEvents_10() { return static_cast<int32_t>(offsetof(CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF, ____inputEvents_10)); }
	inline List_1_tC3916F1B7F67B72C4C10D463D531CC7944331D43 * get__inputEvents_10() const { return ____inputEvents_10; }
	inline List_1_tC3916F1B7F67B72C4C10D463D531CC7944331D43 ** get_address_of__inputEvents_10() { return &____inputEvents_10; }
	inline void set__inputEvents_10(List_1_tC3916F1B7F67B72C4C10D463D531CC7944331D43 * value)
	{
		____inputEvents_10 = value;
		Il2CppCodeGenWriteBarrier((&____inputEvents_10), value);
	}

	inline static int32_t get_offset_of__createdCustomControllerId_11() { return static_cast<int32_t>(offsetof(CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF, ____createdCustomControllerId_11)); }
	inline int32_t get__createdCustomControllerId_11() const { return ____createdCustomControllerId_11; }
	inline int32_t* get_address_of__createdCustomControllerId_11() { return &____createdCustomControllerId_11; }
	inline void set__createdCustomControllerId_11(int32_t value)
	{
		____createdCustomControllerId_11 = value;
	}

	inline static int32_t get_offset_of__InputSourceUpdateEvent_12() { return static_cast<int32_t>(offsetof(CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF, ____InputSourceUpdateEvent_12)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__InputSourceUpdateEvent_12() const { return ____InputSourceUpdateEvent_12; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__InputSourceUpdateEvent_12() { return &____InputSourceUpdateEvent_12; }
	inline void set__InputSourceUpdateEvent_12(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____InputSourceUpdateEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&____InputSourceUpdateEvent_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLER_TDA75F822EE3D9041FE381092FE88F41FE6AB7CBF_H
#ifndef CUSTOMCONTROLLERCONTROL_T210E15B9A7BE79A48909114F34D9BBDF75E42835_H
#define CUSTOMCONTROLLERCONTROL_T210E15B9A7BE79A48909114F34D9BBDF75E42835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.CustomControllerControl
struct  CustomControllerControl_t210E15B9A7BE79A48909114F34D9BBDF75E42835  : public ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLERCONTROL_T210E15B9A7BE79A48909114F34D9BBDF75E42835_H
#ifndef PLAYERCONTROLLER_TF6C76D76E8F199EF74F4568EB7E9ADD18A62600A_H
#define PLAYERCONTROLLER_TF6C76D76E8F199EF74F4568EB7E9ADD18A62600A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Components.PlayerController
struct  PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A  : public ComponentWrapper_1_tA70617D66AF23B03198171AF8040B86543ED0C99
{
public:
	// Rewired.InputManager_Base Rewired.Components.PlayerController::_rewiredInputManager
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * ____rewiredInputManager_6;
	// System.Int32 Rewired.Components.PlayerController::_playerId
	int32_t ____playerId_7;
	// System.Collections.Generic.List`1<Rewired.Components.PlayerController_ElementInfo> Rewired.Components.PlayerController::_elements
	List_1_t53455F9948EB8237F0F60F04F490DFDD57FBB62C * ____elements_8;
	// Rewired.Components.PlayerController_ButtonStateChangedHandler Rewired.Components.PlayerController::_onButtonStateChanged
	ButtonStateChangedHandler_t0531BB1117C004D092524C45A5BD445D5B15AFEC * ____onButtonStateChanged_9;
	// Rewired.Components.PlayerController_AxisValueChangedHandler Rewired.Components.PlayerController::_onAxisValueChanged
	AxisValueChangedHandler_t26F8AD336CA0516BA16CD70E0E0BE16BE4DC474D * ____onAxisValueChanged_10;
	// Rewired.Components.PlayerController_EnabledStateChangedHandler Rewired.Components.PlayerController::_onEnabledStateChanged
	EnabledStateChangedHandler_t7892BA4CD24039FCF244401A350BF2C9367DAF43 * ____onEnabledStateChanged_11;

public:
	inline static int32_t get_offset_of__rewiredInputManager_6() { return static_cast<int32_t>(offsetof(PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A, ____rewiredInputManager_6)); }
	inline InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * get__rewiredInputManager_6() const { return ____rewiredInputManager_6; }
	inline InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 ** get_address_of__rewiredInputManager_6() { return &____rewiredInputManager_6; }
	inline void set__rewiredInputManager_6(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * value)
	{
		____rewiredInputManager_6 = value;
		Il2CppCodeGenWriteBarrier((&____rewiredInputManager_6), value);
	}

	inline static int32_t get_offset_of__playerId_7() { return static_cast<int32_t>(offsetof(PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A, ____playerId_7)); }
	inline int32_t get__playerId_7() const { return ____playerId_7; }
	inline int32_t* get_address_of__playerId_7() { return &____playerId_7; }
	inline void set__playerId_7(int32_t value)
	{
		____playerId_7 = value;
	}

	inline static int32_t get_offset_of__elements_8() { return static_cast<int32_t>(offsetof(PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A, ____elements_8)); }
	inline List_1_t53455F9948EB8237F0F60F04F490DFDD57FBB62C * get__elements_8() const { return ____elements_8; }
	inline List_1_t53455F9948EB8237F0F60F04F490DFDD57FBB62C ** get_address_of__elements_8() { return &____elements_8; }
	inline void set__elements_8(List_1_t53455F9948EB8237F0F60F04F490DFDD57FBB62C * value)
	{
		____elements_8 = value;
		Il2CppCodeGenWriteBarrier((&____elements_8), value);
	}

	inline static int32_t get_offset_of__onButtonStateChanged_9() { return static_cast<int32_t>(offsetof(PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A, ____onButtonStateChanged_9)); }
	inline ButtonStateChangedHandler_t0531BB1117C004D092524C45A5BD445D5B15AFEC * get__onButtonStateChanged_9() const { return ____onButtonStateChanged_9; }
	inline ButtonStateChangedHandler_t0531BB1117C004D092524C45A5BD445D5B15AFEC ** get_address_of__onButtonStateChanged_9() { return &____onButtonStateChanged_9; }
	inline void set__onButtonStateChanged_9(ButtonStateChangedHandler_t0531BB1117C004D092524C45A5BD445D5B15AFEC * value)
	{
		____onButtonStateChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&____onButtonStateChanged_9), value);
	}

	inline static int32_t get_offset_of__onAxisValueChanged_10() { return static_cast<int32_t>(offsetof(PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A, ____onAxisValueChanged_10)); }
	inline AxisValueChangedHandler_t26F8AD336CA0516BA16CD70E0E0BE16BE4DC474D * get__onAxisValueChanged_10() const { return ____onAxisValueChanged_10; }
	inline AxisValueChangedHandler_t26F8AD336CA0516BA16CD70E0E0BE16BE4DC474D ** get_address_of__onAxisValueChanged_10() { return &____onAxisValueChanged_10; }
	inline void set__onAxisValueChanged_10(AxisValueChangedHandler_t26F8AD336CA0516BA16CD70E0E0BE16BE4DC474D * value)
	{
		____onAxisValueChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&____onAxisValueChanged_10), value);
	}

	inline static int32_t get_offset_of__onEnabledStateChanged_11() { return static_cast<int32_t>(offsetof(PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A, ____onEnabledStateChanged_11)); }
	inline EnabledStateChangedHandler_t7892BA4CD24039FCF244401A350BF2C9367DAF43 * get__onEnabledStateChanged_11() const { return ____onEnabledStateChanged_11; }
	inline EnabledStateChangedHandler_t7892BA4CD24039FCF244401A350BF2C9367DAF43 ** get_address_of__onEnabledStateChanged_11() { return &____onEnabledStateChanged_11; }
	inline void set__onEnabledStateChanged_11(EnabledStateChangedHandler_t7892BA4CD24039FCF244401A350BF2C9367DAF43 * value)
	{
		____onEnabledStateChanged_11 = value;
		Il2CppCodeGenWriteBarrier((&____onEnabledStateChanged_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_TF6C76D76E8F199EF74F4568EB7E9ADD18A62600A_H
#ifndef TILTCONTROL_TD1F85A001E3238AC0969DF1FCC743EEE613235EF_H
#define TILTCONTROL_TD1F85A001E3238AC0969DF1FCC743EEE613235EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TiltControl
struct  TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF  : public CustomControllerControl_t210E15B9A7BE79A48909114F34D9BBDF75E42835
{
public:
	// Rewired.ComponentControls.TiltControl_TiltDirection Rewired.ComponentControls.TiltControl::_allowedTiltDirections
	int32_t ____allowedTiltDirections_10;
	// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat Rewired.ComponentControls.TiltControl::_horizontalTiltCustomControllerElement
	CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * ____horizontalTiltCustomControllerElement_11;
	// System.Single Rewired.ComponentControls.TiltControl::_horizontalTiltLimit
	float ____horizontalTiltLimit_12;
	// System.Single Rewired.ComponentControls.TiltControl::_horizontalRestAngle
	float ____horizontalRestAngle_13;
	// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat Rewired.ComponentControls.TiltControl::_forwardTiltCustomControllerElement
	CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * ____forwardTiltCustomControllerElement_14;
	// System.Single Rewired.ComponentControls.TiltControl::_forwardTiltLimit
	float ____forwardTiltLimit_15;
	// System.Single Rewired.ComponentControls.TiltControl::_forwardRestAngle
	float ____forwardRestAngle_16;
	// Rewired.Internal.StandaloneAxis2D Rewired.ComponentControls.TiltControl::_axis2D
	StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A * ____axis2D_17;
	// System.Boolean Rewired.ComponentControls.TiltControl::_useHAxis
	bool ____useHAxis_18;
	// System.Boolean Rewired.ComponentControls.TiltControl::_useFAxis
	bool ____useFAxis_19;
	// System.Func`1<UnityEngine.Vector3> Rewired.ComponentControls.TiltControl::_getAccelerationValue
	Func_1_t603AE29B135DE2A9508D20577C136EE9B04F680D * ____getAccelerationValue_20;

public:
	inline static int32_t get_offset_of__allowedTiltDirections_10() { return static_cast<int32_t>(offsetof(TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF, ____allowedTiltDirections_10)); }
	inline int32_t get__allowedTiltDirections_10() const { return ____allowedTiltDirections_10; }
	inline int32_t* get_address_of__allowedTiltDirections_10() { return &____allowedTiltDirections_10; }
	inline void set__allowedTiltDirections_10(int32_t value)
	{
		____allowedTiltDirections_10 = value;
	}

	inline static int32_t get_offset_of__horizontalTiltCustomControllerElement_11() { return static_cast<int32_t>(offsetof(TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF, ____horizontalTiltCustomControllerElement_11)); }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * get__horizontalTiltCustomControllerElement_11() const { return ____horizontalTiltCustomControllerElement_11; }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 ** get_address_of__horizontalTiltCustomControllerElement_11() { return &____horizontalTiltCustomControllerElement_11; }
	inline void set__horizontalTiltCustomControllerElement_11(CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * value)
	{
		____horizontalTiltCustomControllerElement_11 = value;
		Il2CppCodeGenWriteBarrier((&____horizontalTiltCustomControllerElement_11), value);
	}

	inline static int32_t get_offset_of__horizontalTiltLimit_12() { return static_cast<int32_t>(offsetof(TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF, ____horizontalTiltLimit_12)); }
	inline float get__horizontalTiltLimit_12() const { return ____horizontalTiltLimit_12; }
	inline float* get_address_of__horizontalTiltLimit_12() { return &____horizontalTiltLimit_12; }
	inline void set__horizontalTiltLimit_12(float value)
	{
		____horizontalTiltLimit_12 = value;
	}

	inline static int32_t get_offset_of__horizontalRestAngle_13() { return static_cast<int32_t>(offsetof(TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF, ____horizontalRestAngle_13)); }
	inline float get__horizontalRestAngle_13() const { return ____horizontalRestAngle_13; }
	inline float* get_address_of__horizontalRestAngle_13() { return &____horizontalRestAngle_13; }
	inline void set__horizontalRestAngle_13(float value)
	{
		____horizontalRestAngle_13 = value;
	}

	inline static int32_t get_offset_of__forwardTiltCustomControllerElement_14() { return static_cast<int32_t>(offsetof(TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF, ____forwardTiltCustomControllerElement_14)); }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * get__forwardTiltCustomControllerElement_14() const { return ____forwardTiltCustomControllerElement_14; }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 ** get_address_of__forwardTiltCustomControllerElement_14() { return &____forwardTiltCustomControllerElement_14; }
	inline void set__forwardTiltCustomControllerElement_14(CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * value)
	{
		____forwardTiltCustomControllerElement_14 = value;
		Il2CppCodeGenWriteBarrier((&____forwardTiltCustomControllerElement_14), value);
	}

	inline static int32_t get_offset_of__forwardTiltLimit_15() { return static_cast<int32_t>(offsetof(TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF, ____forwardTiltLimit_15)); }
	inline float get__forwardTiltLimit_15() const { return ____forwardTiltLimit_15; }
	inline float* get_address_of__forwardTiltLimit_15() { return &____forwardTiltLimit_15; }
	inline void set__forwardTiltLimit_15(float value)
	{
		____forwardTiltLimit_15 = value;
	}

	inline static int32_t get_offset_of__forwardRestAngle_16() { return static_cast<int32_t>(offsetof(TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF, ____forwardRestAngle_16)); }
	inline float get__forwardRestAngle_16() const { return ____forwardRestAngle_16; }
	inline float* get_address_of__forwardRestAngle_16() { return &____forwardRestAngle_16; }
	inline void set__forwardRestAngle_16(float value)
	{
		____forwardRestAngle_16 = value;
	}

	inline static int32_t get_offset_of__axis2D_17() { return static_cast<int32_t>(offsetof(TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF, ____axis2D_17)); }
	inline StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A * get__axis2D_17() const { return ____axis2D_17; }
	inline StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A ** get_address_of__axis2D_17() { return &____axis2D_17; }
	inline void set__axis2D_17(StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A * value)
	{
		____axis2D_17 = value;
		Il2CppCodeGenWriteBarrier((&____axis2D_17), value);
	}

	inline static int32_t get_offset_of__useHAxis_18() { return static_cast<int32_t>(offsetof(TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF, ____useHAxis_18)); }
	inline bool get__useHAxis_18() const { return ____useHAxis_18; }
	inline bool* get_address_of__useHAxis_18() { return &____useHAxis_18; }
	inline void set__useHAxis_18(bool value)
	{
		____useHAxis_18 = value;
	}

	inline static int32_t get_offset_of__useFAxis_19() { return static_cast<int32_t>(offsetof(TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF, ____useFAxis_19)); }
	inline bool get__useFAxis_19() const { return ____useFAxis_19; }
	inline bool* get_address_of__useFAxis_19() { return &____useFAxis_19; }
	inline void set__useFAxis_19(bool value)
	{
		____useFAxis_19 = value;
	}

	inline static int32_t get_offset_of__getAccelerationValue_20() { return static_cast<int32_t>(offsetof(TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF, ____getAccelerationValue_20)); }
	inline Func_1_t603AE29B135DE2A9508D20577C136EE9B04F680D * get__getAccelerationValue_20() const { return ____getAccelerationValue_20; }
	inline Func_1_t603AE29B135DE2A9508D20577C136EE9B04F680D ** get_address_of__getAccelerationValue_20() { return &____getAccelerationValue_20; }
	inline void set__getAccelerationValue_20(Func_1_t603AE29B135DE2A9508D20577C136EE9B04F680D * value)
	{
		____getAccelerationValue_20 = value;
		Il2CppCodeGenWriteBarrier((&____getAccelerationValue_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTCONTROL_TD1F85A001E3238AC0969DF1FCC743EEE613235EF_H
#ifndef TOUCHCONTROL_T9BF86126F1D194D4D0E1A508ABDA37784EF365CC_H
#define TOUCHCONTROL_T9BF86126F1D194D4D0E1A508ABDA37784EF365CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchControl
struct  TouchControl_t9BF86126F1D194D4D0E1A508ABDA37784EF365CC  : public CustomControllerControl_t210E15B9A7BE79A48909114F34D9BBDF75E42835
{
public:
	// UnityEngine.Canvas Rewired.ComponentControls.TouchControl::_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ____canvas_8;
	// UnityEngine.RectTransform Rewired.ComponentControls.TouchControl::__rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * _____rectTransform_9;

public:
	inline static int32_t get_offset_of__canvas_8() { return static_cast<int32_t>(offsetof(TouchControl_t9BF86126F1D194D4D0E1A508ABDA37784EF365CC, ____canvas_8)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get__canvas_8() const { return ____canvas_8; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of__canvas_8() { return &____canvas_8; }
	inline void set__canvas_8(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		____canvas_8 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_8), value);
	}

	inline static int32_t get_offset_of___rectTransform_9() { return static_cast<int32_t>(offsetof(TouchControl_t9BF86126F1D194D4D0E1A508ABDA37784EF365CC, _____rectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get___rectTransform_9() const { return _____rectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of___rectTransform_9() { return &_____rectTransform_9; }
	inline void set___rectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		_____rectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&_____rectTransform_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCONTROL_T9BF86126F1D194D4D0E1A508ABDA37784EF365CC_H
#ifndef TOUCHCONTROLLER_T524B2E08B5946E6CD833758471F80C3B35473C3A_H
#define TOUCHCONTROLLER_T524B2E08B5946E6CD833758471F80C3B35473C3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchController
struct  TouchController_t524B2E08B5946E6CD833758471F80C3B35473C3A  : public CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF
{
public:
	// System.Boolean Rewired.ComponentControls.TouchController::_disableMouseInputWhenEnabled
	bool ____disableMouseInputWhenEnabled_13;
	// System.Boolean Rewired.ComponentControls.TouchController::_useCustomController
	bool ____useCustomController_14;

public:
	inline static int32_t get_offset_of__disableMouseInputWhenEnabled_13() { return static_cast<int32_t>(offsetof(TouchController_t524B2E08B5946E6CD833758471F80C3B35473C3A, ____disableMouseInputWhenEnabled_13)); }
	inline bool get__disableMouseInputWhenEnabled_13() const { return ____disableMouseInputWhenEnabled_13; }
	inline bool* get_address_of__disableMouseInputWhenEnabled_13() { return &____disableMouseInputWhenEnabled_13; }
	inline void set__disableMouseInputWhenEnabled_13(bool value)
	{
		____disableMouseInputWhenEnabled_13 = value;
	}

	inline static int32_t get_offset_of__useCustomController_14() { return static_cast<int32_t>(offsetof(TouchController_t524B2E08B5946E6CD833758471F80C3B35473C3A, ____useCustomController_14)); }
	inline bool get__useCustomController_14() const { return ____useCustomController_14; }
	inline bool* get_address_of__useCustomController_14() { return &____useCustomController_14; }
	inline void set__useCustomController_14(bool value)
	{
		____useCustomController_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCONTROLLER_T524B2E08B5946E6CD833758471F80C3B35473C3A_H
#ifndef PLAYERMOUSE_T503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23_H
#define PLAYERMOUSE_T503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Components.PlayerMouse
struct  PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23  : public PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A
{
public:
	// System.Boolean Rewired.Components.PlayerMouse::_defaultToCenter
	bool ____defaultToCenter_12;
	// System.Single Rewired.Components.PlayerMouse::_pointerSpeed
	float ____pointerSpeed_13;
	// System.Boolean Rewired.Components.PlayerMouse::_useHardwarePointerPosition
	bool ____useHardwarePointerPosition_14;
	// UnityEngine.Rect Rewired.Components.PlayerMouse::_movementArea
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ____movementArea_15;
	// Rewired.PlayerMouse_MovementAreaUnit Rewired.Components.PlayerMouse::_movementAreaUnit
	int32_t ____movementAreaUnit_16;
	// Rewired.Components.PlayerMouse_ScreenPositionChangedHandler Rewired.Components.PlayerMouse::_onScreenPositionChanged
	ScreenPositionChangedHandler_t29C4BC8B3DB9744150268C83109151A8DAA7B2F6 * ____onScreenPositionChanged_17;

public:
	inline static int32_t get_offset_of__defaultToCenter_12() { return static_cast<int32_t>(offsetof(PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23, ____defaultToCenter_12)); }
	inline bool get__defaultToCenter_12() const { return ____defaultToCenter_12; }
	inline bool* get_address_of__defaultToCenter_12() { return &____defaultToCenter_12; }
	inline void set__defaultToCenter_12(bool value)
	{
		____defaultToCenter_12 = value;
	}

	inline static int32_t get_offset_of__pointerSpeed_13() { return static_cast<int32_t>(offsetof(PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23, ____pointerSpeed_13)); }
	inline float get__pointerSpeed_13() const { return ____pointerSpeed_13; }
	inline float* get_address_of__pointerSpeed_13() { return &____pointerSpeed_13; }
	inline void set__pointerSpeed_13(float value)
	{
		____pointerSpeed_13 = value;
	}

	inline static int32_t get_offset_of__useHardwarePointerPosition_14() { return static_cast<int32_t>(offsetof(PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23, ____useHardwarePointerPosition_14)); }
	inline bool get__useHardwarePointerPosition_14() const { return ____useHardwarePointerPosition_14; }
	inline bool* get_address_of__useHardwarePointerPosition_14() { return &____useHardwarePointerPosition_14; }
	inline void set__useHardwarePointerPosition_14(bool value)
	{
		____useHardwarePointerPosition_14 = value;
	}

	inline static int32_t get_offset_of__movementArea_15() { return static_cast<int32_t>(offsetof(PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23, ____movementArea_15)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get__movementArea_15() const { return ____movementArea_15; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of__movementArea_15() { return &____movementArea_15; }
	inline void set__movementArea_15(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		____movementArea_15 = value;
	}

	inline static int32_t get_offset_of__movementAreaUnit_16() { return static_cast<int32_t>(offsetof(PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23, ____movementAreaUnit_16)); }
	inline int32_t get__movementAreaUnit_16() const { return ____movementAreaUnit_16; }
	inline int32_t* get_address_of__movementAreaUnit_16() { return &____movementAreaUnit_16; }
	inline void set__movementAreaUnit_16(int32_t value)
	{
		____movementAreaUnit_16 = value;
	}

	inline static int32_t get_offset_of__onScreenPositionChanged_17() { return static_cast<int32_t>(offsetof(PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23, ____onScreenPositionChanged_17)); }
	inline ScreenPositionChangedHandler_t29C4BC8B3DB9744150268C83109151A8DAA7B2F6 * get__onScreenPositionChanged_17() const { return ____onScreenPositionChanged_17; }
	inline ScreenPositionChangedHandler_t29C4BC8B3DB9744150268C83109151A8DAA7B2F6 ** get_address_of__onScreenPositionChanged_17() { return &____onScreenPositionChanged_17; }
	inline void set__onScreenPositionChanged_17(ScreenPositionChangedHandler_t29C4BC8B3DB9744150268C83109151A8DAA7B2F6 * value)
	{
		____onScreenPositionChanged_17 = value;
		Il2CppCodeGenWriteBarrier((&____onScreenPositionChanged_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMOUSE_T503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23_H
#ifndef TOUCHINTERACTABLE_TD9DE3E6594439924A77020122C094D639E4BF8B8_H
#define TOUCHINTERACTABLE_TD9DE3E6594439924A77020122C094D639E4BF8B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchInteractable
struct  TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8  : public TouchControl_t9BF86126F1D194D4D0E1A508ABDA37784EF365CC
{
public:
	// System.Boolean Rewired.ComponentControls.TouchInteractable::_interactable
	bool ____interactable_15;
	// System.Boolean Rewired.ComponentControls.TouchInteractable::_visible
	bool ____visible_16;
	// System.Boolean Rewired.ComponentControls.TouchInteractable::_hideWhenIdle
	bool ____hideWhenIdle_17;
	// Rewired.ComponentControls.TouchInteractable_MouseButtonFlags Rewired.ComponentControls.TouchInteractable::_allowedMouseButtons
	int32_t ____allowedMouseButtons_18;
	// Rewired.ComponentControls.TouchInteractable_TransitionTypeFlags Rewired.ComponentControls.TouchInteractable::_transitionType
	int32_t ____transitionType_19;
	// UnityEngine.UI.ColorBlock Rewired.ComponentControls.TouchInteractable::_transitionColorTint
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ____transitionColorTint_20;
	// UnityEngine.UI.SpriteState Rewired.ComponentControls.TouchInteractable::_transitionSpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ____transitionSpriteState_21;
	// UnityEngine.UI.AnimationTriggers Rewired.ComponentControls.TouchInteractable::_transitionAnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ____transitionAnimationTriggers_22;
	// UnityEngine.UI.Graphic Rewired.ComponentControls.TouchInteractable::_targetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ____targetGraphic_23;
	// Rewired.ComponentControls.TouchInteractable_InteractionStateTransitionEventHandler Rewired.ComponentControls.TouchInteractable::_onInteractionStateTransition
	InteractionStateTransitionEventHandler_tC1D00C17A8A6547002894B4718BD039679204D36 * ____onInteractionStateTransition_24;
	// Rewired.ComponentControls.TouchInteractable_VisibilityChangedEventHandler Rewired.ComponentControls.TouchInteractable::_onVisibilityChanged
	VisibilityChangedEventHandler_t3D6E9B1BC6606FEC892CE393EDD4D9ED89DE9D28 * ____onVisibilityChanged_25;
	// UnityEngine.Events.UnityEvent Rewired.ComponentControls.TouchInteractable::_onInteractionStateChangedToNormal
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ____onInteractionStateChangedToNormal_26;
	// UnityEngine.Events.UnityEvent Rewired.ComponentControls.TouchInteractable::_onInteractionStateChangedToHighlighted
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ____onInteractionStateChangedToHighlighted_27;
	// UnityEngine.Events.UnityEvent Rewired.ComponentControls.TouchInteractable::_onInteractionStateChangedToPressed
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ____onInteractionStateChangedToPressed_28;
	// UnityEngine.Events.UnityEvent Rewired.ComponentControls.TouchInteractable::_onInteractionStateChangedToDisabled
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ____onInteractionStateChangedToDisabled_29;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> Rewired.ComponentControls.TouchInteractable::_canvasGroupCache
	List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * ____canvasGroupCache_30;
	// System.Boolean Rewired.ComponentControls.TouchInteractable::_groupsAllowInteraction
	bool ____groupsAllowInteraction_31;
	// Rewired.ComponentControls.TouchInteractable_InteractionState Rewired.ComponentControls.TouchInteractable::_interactionState
	int32_t ____interactionState_32;
	// System.Boolean Rewired.ComponentControls.TouchInteractable::pPGULjslgtAiohyNMfEalDGgOVl
	bool ___pPGULjslgtAiohyNMfEalDGgOVl_33;
	// System.Boolean Rewired.ComponentControls.TouchInteractable::UsFAvRrPreZngsJiAjbwyGLdcub
	bool ___UsFAvRrPreZngsJiAjbwyGLdcub_34;
	// System.Boolean Rewired.ComponentControls.TouchInteractable::_varWatch_visible
	bool ____varWatch_visible_35;
	// System.Boolean Rewired.ComponentControls.TouchInteractable::_varWatch_interactable
	bool ____varWatch_interactable_36;
	// System.Boolean Rewired.ComponentControls.TouchInteractable::_allowSendingEvents
	bool ____allowSendingEvents_37;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<Rewired.UI.IVisibilityChangedHandler,System.Boolean> Rewired.ComponentControls.TouchInteractable::__hierarchyVisibilityChangedHandlers
	HierarchyEventHelper_2_tF3ABAFBB34E9604A7251ECA9F7EC094F96DBCC52 * _____hierarchyVisibilityChangedHandlers_39;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<Rewired.ComponentControls.TouchInteractable_IInteractionStateTransitionHandler,Rewired.ComponentControls.TouchInteractable_InteractionStateTransitionArgs> Rewired.ComponentControls.TouchInteractable::__hierarchyInteractionStateTransitionHandlers
	HierarchyEventHelper_2_tA4972CA50CF1E3EC0959842D99260B261010D354 * _____hierarchyInteractionStateTransitionHandlers_40;

public:
	inline static int32_t get_offset_of__interactable_15() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____interactable_15)); }
	inline bool get__interactable_15() const { return ____interactable_15; }
	inline bool* get_address_of__interactable_15() { return &____interactable_15; }
	inline void set__interactable_15(bool value)
	{
		____interactable_15 = value;
	}

	inline static int32_t get_offset_of__visible_16() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____visible_16)); }
	inline bool get__visible_16() const { return ____visible_16; }
	inline bool* get_address_of__visible_16() { return &____visible_16; }
	inline void set__visible_16(bool value)
	{
		____visible_16 = value;
	}

	inline static int32_t get_offset_of__hideWhenIdle_17() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____hideWhenIdle_17)); }
	inline bool get__hideWhenIdle_17() const { return ____hideWhenIdle_17; }
	inline bool* get_address_of__hideWhenIdle_17() { return &____hideWhenIdle_17; }
	inline void set__hideWhenIdle_17(bool value)
	{
		____hideWhenIdle_17 = value;
	}

	inline static int32_t get_offset_of__allowedMouseButtons_18() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____allowedMouseButtons_18)); }
	inline int32_t get__allowedMouseButtons_18() const { return ____allowedMouseButtons_18; }
	inline int32_t* get_address_of__allowedMouseButtons_18() { return &____allowedMouseButtons_18; }
	inline void set__allowedMouseButtons_18(int32_t value)
	{
		____allowedMouseButtons_18 = value;
	}

	inline static int32_t get_offset_of__transitionType_19() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____transitionType_19)); }
	inline int32_t get__transitionType_19() const { return ____transitionType_19; }
	inline int32_t* get_address_of__transitionType_19() { return &____transitionType_19; }
	inline void set__transitionType_19(int32_t value)
	{
		____transitionType_19 = value;
	}

	inline static int32_t get_offset_of__transitionColorTint_20() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____transitionColorTint_20)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get__transitionColorTint_20() const { return ____transitionColorTint_20; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of__transitionColorTint_20() { return &____transitionColorTint_20; }
	inline void set__transitionColorTint_20(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		____transitionColorTint_20 = value;
	}

	inline static int32_t get_offset_of__transitionSpriteState_21() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____transitionSpriteState_21)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get__transitionSpriteState_21() const { return ____transitionSpriteState_21; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of__transitionSpriteState_21() { return &____transitionSpriteState_21; }
	inline void set__transitionSpriteState_21(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		____transitionSpriteState_21 = value;
	}

	inline static int32_t get_offset_of__transitionAnimationTriggers_22() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____transitionAnimationTriggers_22)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get__transitionAnimationTriggers_22() const { return ____transitionAnimationTriggers_22; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of__transitionAnimationTriggers_22() { return &____transitionAnimationTriggers_22; }
	inline void set__transitionAnimationTriggers_22(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		____transitionAnimationTriggers_22 = value;
		Il2CppCodeGenWriteBarrier((&____transitionAnimationTriggers_22), value);
	}

	inline static int32_t get_offset_of__targetGraphic_23() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____targetGraphic_23)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get__targetGraphic_23() const { return ____targetGraphic_23; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of__targetGraphic_23() { return &____targetGraphic_23; }
	inline void set__targetGraphic_23(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		____targetGraphic_23 = value;
		Il2CppCodeGenWriteBarrier((&____targetGraphic_23), value);
	}

	inline static int32_t get_offset_of__onInteractionStateTransition_24() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____onInteractionStateTransition_24)); }
	inline InteractionStateTransitionEventHandler_tC1D00C17A8A6547002894B4718BD039679204D36 * get__onInteractionStateTransition_24() const { return ____onInteractionStateTransition_24; }
	inline InteractionStateTransitionEventHandler_tC1D00C17A8A6547002894B4718BD039679204D36 ** get_address_of__onInteractionStateTransition_24() { return &____onInteractionStateTransition_24; }
	inline void set__onInteractionStateTransition_24(InteractionStateTransitionEventHandler_tC1D00C17A8A6547002894B4718BD039679204D36 * value)
	{
		____onInteractionStateTransition_24 = value;
		Il2CppCodeGenWriteBarrier((&____onInteractionStateTransition_24), value);
	}

	inline static int32_t get_offset_of__onVisibilityChanged_25() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____onVisibilityChanged_25)); }
	inline VisibilityChangedEventHandler_t3D6E9B1BC6606FEC892CE393EDD4D9ED89DE9D28 * get__onVisibilityChanged_25() const { return ____onVisibilityChanged_25; }
	inline VisibilityChangedEventHandler_t3D6E9B1BC6606FEC892CE393EDD4D9ED89DE9D28 ** get_address_of__onVisibilityChanged_25() { return &____onVisibilityChanged_25; }
	inline void set__onVisibilityChanged_25(VisibilityChangedEventHandler_t3D6E9B1BC6606FEC892CE393EDD4D9ED89DE9D28 * value)
	{
		____onVisibilityChanged_25 = value;
		Il2CppCodeGenWriteBarrier((&____onVisibilityChanged_25), value);
	}

	inline static int32_t get_offset_of__onInteractionStateChangedToNormal_26() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____onInteractionStateChangedToNormal_26)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get__onInteractionStateChangedToNormal_26() const { return ____onInteractionStateChangedToNormal_26; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of__onInteractionStateChangedToNormal_26() { return &____onInteractionStateChangedToNormal_26; }
	inline void set__onInteractionStateChangedToNormal_26(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		____onInteractionStateChangedToNormal_26 = value;
		Il2CppCodeGenWriteBarrier((&____onInteractionStateChangedToNormal_26), value);
	}

	inline static int32_t get_offset_of__onInteractionStateChangedToHighlighted_27() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____onInteractionStateChangedToHighlighted_27)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get__onInteractionStateChangedToHighlighted_27() const { return ____onInteractionStateChangedToHighlighted_27; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of__onInteractionStateChangedToHighlighted_27() { return &____onInteractionStateChangedToHighlighted_27; }
	inline void set__onInteractionStateChangedToHighlighted_27(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		____onInteractionStateChangedToHighlighted_27 = value;
		Il2CppCodeGenWriteBarrier((&____onInteractionStateChangedToHighlighted_27), value);
	}

	inline static int32_t get_offset_of__onInteractionStateChangedToPressed_28() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____onInteractionStateChangedToPressed_28)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get__onInteractionStateChangedToPressed_28() const { return ____onInteractionStateChangedToPressed_28; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of__onInteractionStateChangedToPressed_28() { return &____onInteractionStateChangedToPressed_28; }
	inline void set__onInteractionStateChangedToPressed_28(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		____onInteractionStateChangedToPressed_28 = value;
		Il2CppCodeGenWriteBarrier((&____onInteractionStateChangedToPressed_28), value);
	}

	inline static int32_t get_offset_of__onInteractionStateChangedToDisabled_29() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____onInteractionStateChangedToDisabled_29)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get__onInteractionStateChangedToDisabled_29() const { return ____onInteractionStateChangedToDisabled_29; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of__onInteractionStateChangedToDisabled_29() { return &____onInteractionStateChangedToDisabled_29; }
	inline void set__onInteractionStateChangedToDisabled_29(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		____onInteractionStateChangedToDisabled_29 = value;
		Il2CppCodeGenWriteBarrier((&____onInteractionStateChangedToDisabled_29), value);
	}

	inline static int32_t get_offset_of__canvasGroupCache_30() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____canvasGroupCache_30)); }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * get__canvasGroupCache_30() const { return ____canvasGroupCache_30; }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 ** get_address_of__canvasGroupCache_30() { return &____canvasGroupCache_30; }
	inline void set__canvasGroupCache_30(List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * value)
	{
		____canvasGroupCache_30 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroupCache_30), value);
	}

	inline static int32_t get_offset_of__groupsAllowInteraction_31() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____groupsAllowInteraction_31)); }
	inline bool get__groupsAllowInteraction_31() const { return ____groupsAllowInteraction_31; }
	inline bool* get_address_of__groupsAllowInteraction_31() { return &____groupsAllowInteraction_31; }
	inline void set__groupsAllowInteraction_31(bool value)
	{
		____groupsAllowInteraction_31 = value;
	}

	inline static int32_t get_offset_of__interactionState_32() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____interactionState_32)); }
	inline int32_t get__interactionState_32() const { return ____interactionState_32; }
	inline int32_t* get_address_of__interactionState_32() { return &____interactionState_32; }
	inline void set__interactionState_32(int32_t value)
	{
		____interactionState_32 = value;
	}

	inline static int32_t get_offset_of_pPGULjslgtAiohyNMfEalDGgOVl_33() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ___pPGULjslgtAiohyNMfEalDGgOVl_33)); }
	inline bool get_pPGULjslgtAiohyNMfEalDGgOVl_33() const { return ___pPGULjslgtAiohyNMfEalDGgOVl_33; }
	inline bool* get_address_of_pPGULjslgtAiohyNMfEalDGgOVl_33() { return &___pPGULjslgtAiohyNMfEalDGgOVl_33; }
	inline void set_pPGULjslgtAiohyNMfEalDGgOVl_33(bool value)
	{
		___pPGULjslgtAiohyNMfEalDGgOVl_33 = value;
	}

	inline static int32_t get_offset_of_UsFAvRrPreZngsJiAjbwyGLdcub_34() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ___UsFAvRrPreZngsJiAjbwyGLdcub_34)); }
	inline bool get_UsFAvRrPreZngsJiAjbwyGLdcub_34() const { return ___UsFAvRrPreZngsJiAjbwyGLdcub_34; }
	inline bool* get_address_of_UsFAvRrPreZngsJiAjbwyGLdcub_34() { return &___UsFAvRrPreZngsJiAjbwyGLdcub_34; }
	inline void set_UsFAvRrPreZngsJiAjbwyGLdcub_34(bool value)
	{
		___UsFAvRrPreZngsJiAjbwyGLdcub_34 = value;
	}

	inline static int32_t get_offset_of__varWatch_visible_35() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____varWatch_visible_35)); }
	inline bool get__varWatch_visible_35() const { return ____varWatch_visible_35; }
	inline bool* get_address_of__varWatch_visible_35() { return &____varWatch_visible_35; }
	inline void set__varWatch_visible_35(bool value)
	{
		____varWatch_visible_35 = value;
	}

	inline static int32_t get_offset_of__varWatch_interactable_36() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____varWatch_interactable_36)); }
	inline bool get__varWatch_interactable_36() const { return ____varWatch_interactable_36; }
	inline bool* get_address_of__varWatch_interactable_36() { return &____varWatch_interactable_36; }
	inline void set__varWatch_interactable_36(bool value)
	{
		____varWatch_interactable_36 = value;
	}

	inline static int32_t get_offset_of__allowSendingEvents_37() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, ____allowSendingEvents_37)); }
	inline bool get__allowSendingEvents_37() const { return ____allowSendingEvents_37; }
	inline bool* get_address_of__allowSendingEvents_37() { return &____allowSendingEvents_37; }
	inline void set__allowSendingEvents_37(bool value)
	{
		____allowSendingEvents_37 = value;
	}

	inline static int32_t get_offset_of___hierarchyVisibilityChangedHandlers_39() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, _____hierarchyVisibilityChangedHandlers_39)); }
	inline HierarchyEventHelper_2_tF3ABAFBB34E9604A7251ECA9F7EC094F96DBCC52 * get___hierarchyVisibilityChangedHandlers_39() const { return _____hierarchyVisibilityChangedHandlers_39; }
	inline HierarchyEventHelper_2_tF3ABAFBB34E9604A7251ECA9F7EC094F96DBCC52 ** get_address_of___hierarchyVisibilityChangedHandlers_39() { return &_____hierarchyVisibilityChangedHandlers_39; }
	inline void set___hierarchyVisibilityChangedHandlers_39(HierarchyEventHelper_2_tF3ABAFBB34E9604A7251ECA9F7EC094F96DBCC52 * value)
	{
		_____hierarchyVisibilityChangedHandlers_39 = value;
		Il2CppCodeGenWriteBarrier((&_____hierarchyVisibilityChangedHandlers_39), value);
	}

	inline static int32_t get_offset_of___hierarchyInteractionStateTransitionHandlers_40() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8, _____hierarchyInteractionStateTransitionHandlers_40)); }
	inline HierarchyEventHelper_2_tA4972CA50CF1E3EC0959842D99260B261010D354 * get___hierarchyInteractionStateTransitionHandlers_40() const { return _____hierarchyInteractionStateTransitionHandlers_40; }
	inline HierarchyEventHelper_2_tA4972CA50CF1E3EC0959842D99260B261010D354 ** get_address_of___hierarchyInteractionStateTransitionHandlers_40() { return &_____hierarchyInteractionStateTransitionHandlers_40; }
	inline void set___hierarchyInteractionStateTransitionHandlers_40(HierarchyEventHelper_2_tA4972CA50CF1E3EC0959842D99260B261010D354 * value)
	{
		_____hierarchyInteractionStateTransitionHandlers_40 = value;
		Il2CppCodeGenWriteBarrier((&_____hierarchyInteractionStateTransitionHandlers_40), value);
	}
};

struct TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8_StaticFields
{
public:
	// Rewired.ComponentControls.TouchInteractable_InteractionStateTransitionArgs Rewired.ComponentControls.TouchInteractable::_transitionArgs
	InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42 * ____transitionArgs_38;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<Rewired.ComponentControls.TouchInteractable_IInteractionStateTransitionHandler,Rewired.ComponentControls.TouchInteractable_InteractionStateTransitionArgs> Rewired.ComponentControls.TouchInteractable::__interactionStateTransitionHandlerDelegate
	EventFunction_2_t79336E855EAE2B72B776659EF35769F0D0DA8D8A * _____interactionStateTransitionHandlerDelegate_41;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<Rewired.ComponentControls.TouchInteractable_IInteractionStateTransitionHandler,Rewired.ComponentControls.TouchInteractable_InteractionStateTransitionArgs> Rewired.ComponentControls.TouchInteractable::CSU24<>9__CachedAnonymousMethodDelegate4
	EventFunction_2_t79336E855EAE2B72B776659EF35769F0D0DA8D8A * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_42;

public:
	inline static int32_t get_offset_of__transitionArgs_38() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8_StaticFields, ____transitionArgs_38)); }
	inline InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42 * get__transitionArgs_38() const { return ____transitionArgs_38; }
	inline InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42 ** get_address_of__transitionArgs_38() { return &____transitionArgs_38; }
	inline void set__transitionArgs_38(InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42 * value)
	{
		____transitionArgs_38 = value;
		Il2CppCodeGenWriteBarrier((&____transitionArgs_38), value);
	}

	inline static int32_t get_offset_of___interactionStateTransitionHandlerDelegate_41() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8_StaticFields, _____interactionStateTransitionHandlerDelegate_41)); }
	inline EventFunction_2_t79336E855EAE2B72B776659EF35769F0D0DA8D8A * get___interactionStateTransitionHandlerDelegate_41() const { return _____interactionStateTransitionHandlerDelegate_41; }
	inline EventFunction_2_t79336E855EAE2B72B776659EF35769F0D0DA8D8A ** get_address_of___interactionStateTransitionHandlerDelegate_41() { return &_____interactionStateTransitionHandlerDelegate_41; }
	inline void set___interactionStateTransitionHandlerDelegate_41(EventFunction_2_t79336E855EAE2B72B776659EF35769F0D0DA8D8A * value)
	{
		_____interactionStateTransitionHandlerDelegate_41 = value;
		Il2CppCodeGenWriteBarrier((&_____interactionStateTransitionHandlerDelegate_41), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate4_42() { return static_cast<int32_t>(offsetof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_42)); }
	inline EventFunction_2_t79336E855EAE2B72B776659EF35769F0D0DA8D8A * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate4_42() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_42; }
	inline EventFunction_2_t79336E855EAE2B72B776659EF35769F0D0DA8D8A ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate4_42() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_42; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate4_42(EventFunction_2_t79336E855EAE2B72B776659EF35769F0D0DA8D8A * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_42 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHINTERACTABLE_TD9DE3E6594439924A77020122C094D639E4BF8B8_H
#ifndef TOUCHBUTTON_T192BE56ABF72197355626DC4A937D764B01D6D7F_H
#define TOUCHBUTTON_T192BE56ABF72197355626DC4A937D764B01D6D7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchButton
struct  TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F  : public TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8
{
public:
	// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat Rewired.ComponentControls.TouchButton::_targetCustomControllerElement
	CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * ____targetCustomControllerElement_44;
	// Rewired.ComponentControls.TouchButton_ButtonType Rewired.ComponentControls.TouchButton::_buttonType
	int32_t ____buttonType_45;
	// System.Boolean Rewired.ComponentControls.TouchButton::_activateOnSwipeIn
	bool ____activateOnSwipeIn_46;
	// System.Boolean Rewired.ComponentControls.TouchButton::_stayActiveOnSwipeOut
	bool ____stayActiveOnSwipeOut_47;
	// System.Boolean Rewired.ComponentControls.TouchButton::_useDigitalAxisSimulation
	bool ____useDigitalAxisSimulation_48;
	// System.Single Rewired.ComponentControls.TouchButton::_digitalAxisGravity
	float ____digitalAxisGravity_49;
	// System.Single Rewired.ComponentControls.TouchButton::_digitalAxisSensitivity
	float ____digitalAxisSensitivity_50;
	// Rewired.Internal.StandaloneAxis Rewired.ComponentControls.TouchButton::_axis
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E * ____axis_51;
	// Rewired.ComponentControls.TouchRegion Rewired.ComponentControls.TouchButton::_touchRegion
	TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 * ____touchRegion_52;
	// System.Boolean Rewired.ComponentControls.TouchButton::_useTouchRegionOnly
	bool ____useTouchRegionOnly_53;
	// System.Boolean Rewired.ComponentControls.TouchButton::_moveToTouchPosition
	bool ____moveToTouchPosition_54;
	// System.Boolean Rewired.ComponentControls.TouchButton::_returnOnRelease
	bool ____returnOnRelease_55;
	// System.Boolean Rewired.ComponentControls.TouchButton::_followTouchPosition
	bool ____followTouchPosition_56;
	// System.Boolean Rewired.ComponentControls.TouchButton::_animateOnMoveToTouch
	bool ____animateOnMoveToTouch_57;
	// System.Single Rewired.ComponentControls.TouchButton::_moveToTouchSpeed
	float ____moveToTouchSpeed_58;
	// System.Boolean Rewired.ComponentControls.TouchButton::_animateOnReturn
	bool ____animateOnReturn_59;
	// System.Single Rewired.ComponentControls.TouchButton::_returnSpeed
	float ____returnSpeed_60;
	// System.Boolean Rewired.ComponentControls.TouchButton::_manageRaycasting
	bool ____manageRaycasting_61;
	// System.Single Rewired.ComponentControls.TouchButton::EiwtMLppfCtuooXjvdHygyGrsHS
	float ___EiwtMLppfCtuooXjvdHygyGrsHS_62;
	// System.Single Rewired.ComponentControls.TouchButton::JHUTNNOcuztILjpjkavvMXajFRV
	float ___JHUTNNOcuztILjpjkavvMXajFRV_63;
	// Rewired.ComponentControls.TouchRegion Rewired.ComponentControls.TouchButton::FcORQGMEvzohtgyCwrysLogmieu
	TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 * ___FcORQGMEvzohtgyCwrysLogmieu_64;
	// UnityEngine.Vector2 Rewired.ComponentControls.TouchButton::nysomDCBRIvDcmoXFQHdLQznHNr
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___nysomDCBRIvDcmoXFQHdLQznHNr_65;
	// System.Boolean Rewired.ComponentControls.TouchButton::reSQzxHMdzbyhGwOPrSROZjFOBx
	bool ___reSQzxHMdzbyhGwOPrSROZjFOBx_66;
	// System.Boolean Rewired.ComponentControls.TouchButton::uaVpGuIOaSatQKbOxUPLMpimWG
	bool ___uaVpGuIOaSatQKbOxUPLMpimWG_67;
	// Rewired.ComponentControls.TouchButton_CRqCjFCalakqbykartaanqOTwPEL Rewired.ComponentControls.TouchButton::mucIMzrdRGVGfPFucsRzAxGACqM
	int32_t ___mucIMzrdRGVGfPFucsRzAxGACqM_68;
	// System.Int32 Rewired.ComponentControls.TouchButton::kKSSGIYcShDBXbkfYwidHsiqbRh
	int32_t ___kKSSGIYcShDBXbkfYwidHsiqbRh_69;
	// System.Int32 Rewired.ComponentControls.TouchButton::VKAtGkSQLhkGGQSRuCzXdQVELuq
	int32_t ___VKAtGkSQLhkGGQSRuCzXdQVELuq_70;
	// System.Boolean Rewired.ComponentControls.TouchButton::UsFAvRrPreZngsJiAjbwyGLdcub
	bool ___UsFAvRrPreZngsJiAjbwyGLdcub_71;
	// System.Boolean Rewired.ComponentControls.TouchButton::pPGULjslgtAiohyNMfEalDGgOVl
	bool ___pPGULjslgtAiohyNMfEalDGgOVl_72;
	// System.Collections.IEnumerator Rewired.ComponentControls.TouchButton::HgstqelxyDTEvxPqOrKBCHhdKVB
	RuntimeObject* ___HgstqelxyDTEvxPqOrKBCHhdKVB_73;
	// erAqfwCNbAAvnTADjtOsmRVrVxW Rewired.ComponentControls.TouchButton::VUcbNyhUNSjVjmdmpXAHQiBoHeYO
	erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4 * ___VUcbNyhUNSjVjmdmpXAHQiBoHeYO_74;
	// System.Action`1<Rewired.ComponentControls.TouchButton_CRqCjFCalakqbykartaanqOTwPEL> Rewired.ComponentControls.TouchButton::bQOHtIPuejRPzOXhHfAkcDmJfTT
	Action_1_t4E78B000408BF7DAE9233B13DB3098949BB3E61B * ___bQOHtIPuejRPzOXhHfAkcDmJfTT_75;
	// System.Action`1<Rewired.ComponentControls.TouchButton_CRqCjFCalakqbykartaanqOTwPEL> Rewired.ComponentControls.TouchButton::AQDzRNACmkHZMcdFfzmRWfTaKBg
	Action_1_t4E78B000408BF7DAE9233B13DB3098949BB3E61B * ___AQDzRNACmkHZMcdFfzmRWfTaKBg_76;
	// Rewired.ComponentControls.TouchButton_AxisValueChangedEventHandler Rewired.ComponentControls.TouchButton::_onAxisValueChanged
	AxisValueChangedEventHandler_t622BC4F8C94A13837769CEFE1FD12CC18B926791 * ____onAxisValueChanged_77;
	// Rewired.ComponentControls.TouchButton_ButtonValueChangedEventHandler Rewired.ComponentControls.TouchButton::_onButtonValueChanged
	ButtonValueChangedEventHandler_tB3FD14B60CDD8A2E3B8D8B26F951B056495B5A50 * ____onButtonValueChanged_78;
	// Rewired.ComponentControls.TouchButton_ButtonDownEventHandler Rewired.ComponentControls.TouchButton::_onButtonDown
	ButtonDownEventHandler_tD3FB726C5A3E17A89FA7992F2D0D0422286B7E7D * ____onButtonDown_79;
	// Rewired.ComponentControls.TouchButton_ButtonUpEventHandler Rewired.ComponentControls.TouchButton::_onButtonUp
	ButtonUpEventHandler_t529887EE63C2C416911C6C64F8D230008EB73339 * ____onButtonUp_80;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> Rewired.ComponentControls.TouchButton::SbJiaOqkhEQicEKkypVosjZLBXA
	Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF * ___SbJiaOqkhEQicEKkypVosjZLBXA_81;

public:
	inline static int32_t get_offset_of__targetCustomControllerElement_44() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____targetCustomControllerElement_44)); }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * get__targetCustomControllerElement_44() const { return ____targetCustomControllerElement_44; }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 ** get_address_of__targetCustomControllerElement_44() { return &____targetCustomControllerElement_44; }
	inline void set__targetCustomControllerElement_44(CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * value)
	{
		____targetCustomControllerElement_44 = value;
		Il2CppCodeGenWriteBarrier((&____targetCustomControllerElement_44), value);
	}

	inline static int32_t get_offset_of__buttonType_45() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____buttonType_45)); }
	inline int32_t get__buttonType_45() const { return ____buttonType_45; }
	inline int32_t* get_address_of__buttonType_45() { return &____buttonType_45; }
	inline void set__buttonType_45(int32_t value)
	{
		____buttonType_45 = value;
	}

	inline static int32_t get_offset_of__activateOnSwipeIn_46() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____activateOnSwipeIn_46)); }
	inline bool get__activateOnSwipeIn_46() const { return ____activateOnSwipeIn_46; }
	inline bool* get_address_of__activateOnSwipeIn_46() { return &____activateOnSwipeIn_46; }
	inline void set__activateOnSwipeIn_46(bool value)
	{
		____activateOnSwipeIn_46 = value;
	}

	inline static int32_t get_offset_of__stayActiveOnSwipeOut_47() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____stayActiveOnSwipeOut_47)); }
	inline bool get__stayActiveOnSwipeOut_47() const { return ____stayActiveOnSwipeOut_47; }
	inline bool* get_address_of__stayActiveOnSwipeOut_47() { return &____stayActiveOnSwipeOut_47; }
	inline void set__stayActiveOnSwipeOut_47(bool value)
	{
		____stayActiveOnSwipeOut_47 = value;
	}

	inline static int32_t get_offset_of__useDigitalAxisSimulation_48() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____useDigitalAxisSimulation_48)); }
	inline bool get__useDigitalAxisSimulation_48() const { return ____useDigitalAxisSimulation_48; }
	inline bool* get_address_of__useDigitalAxisSimulation_48() { return &____useDigitalAxisSimulation_48; }
	inline void set__useDigitalAxisSimulation_48(bool value)
	{
		____useDigitalAxisSimulation_48 = value;
	}

	inline static int32_t get_offset_of__digitalAxisGravity_49() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____digitalAxisGravity_49)); }
	inline float get__digitalAxisGravity_49() const { return ____digitalAxisGravity_49; }
	inline float* get_address_of__digitalAxisGravity_49() { return &____digitalAxisGravity_49; }
	inline void set__digitalAxisGravity_49(float value)
	{
		____digitalAxisGravity_49 = value;
	}

	inline static int32_t get_offset_of__digitalAxisSensitivity_50() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____digitalAxisSensitivity_50)); }
	inline float get__digitalAxisSensitivity_50() const { return ____digitalAxisSensitivity_50; }
	inline float* get_address_of__digitalAxisSensitivity_50() { return &____digitalAxisSensitivity_50; }
	inline void set__digitalAxisSensitivity_50(float value)
	{
		____digitalAxisSensitivity_50 = value;
	}

	inline static int32_t get_offset_of__axis_51() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____axis_51)); }
	inline StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E * get__axis_51() const { return ____axis_51; }
	inline StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E ** get_address_of__axis_51() { return &____axis_51; }
	inline void set__axis_51(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E * value)
	{
		____axis_51 = value;
		Il2CppCodeGenWriteBarrier((&____axis_51), value);
	}

	inline static int32_t get_offset_of__touchRegion_52() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____touchRegion_52)); }
	inline TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 * get__touchRegion_52() const { return ____touchRegion_52; }
	inline TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 ** get_address_of__touchRegion_52() { return &____touchRegion_52; }
	inline void set__touchRegion_52(TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 * value)
	{
		____touchRegion_52 = value;
		Il2CppCodeGenWriteBarrier((&____touchRegion_52), value);
	}

	inline static int32_t get_offset_of__useTouchRegionOnly_53() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____useTouchRegionOnly_53)); }
	inline bool get__useTouchRegionOnly_53() const { return ____useTouchRegionOnly_53; }
	inline bool* get_address_of__useTouchRegionOnly_53() { return &____useTouchRegionOnly_53; }
	inline void set__useTouchRegionOnly_53(bool value)
	{
		____useTouchRegionOnly_53 = value;
	}

	inline static int32_t get_offset_of__moveToTouchPosition_54() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____moveToTouchPosition_54)); }
	inline bool get__moveToTouchPosition_54() const { return ____moveToTouchPosition_54; }
	inline bool* get_address_of__moveToTouchPosition_54() { return &____moveToTouchPosition_54; }
	inline void set__moveToTouchPosition_54(bool value)
	{
		____moveToTouchPosition_54 = value;
	}

	inline static int32_t get_offset_of__returnOnRelease_55() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____returnOnRelease_55)); }
	inline bool get__returnOnRelease_55() const { return ____returnOnRelease_55; }
	inline bool* get_address_of__returnOnRelease_55() { return &____returnOnRelease_55; }
	inline void set__returnOnRelease_55(bool value)
	{
		____returnOnRelease_55 = value;
	}

	inline static int32_t get_offset_of__followTouchPosition_56() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____followTouchPosition_56)); }
	inline bool get__followTouchPosition_56() const { return ____followTouchPosition_56; }
	inline bool* get_address_of__followTouchPosition_56() { return &____followTouchPosition_56; }
	inline void set__followTouchPosition_56(bool value)
	{
		____followTouchPosition_56 = value;
	}

	inline static int32_t get_offset_of__animateOnMoveToTouch_57() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____animateOnMoveToTouch_57)); }
	inline bool get__animateOnMoveToTouch_57() const { return ____animateOnMoveToTouch_57; }
	inline bool* get_address_of__animateOnMoveToTouch_57() { return &____animateOnMoveToTouch_57; }
	inline void set__animateOnMoveToTouch_57(bool value)
	{
		____animateOnMoveToTouch_57 = value;
	}

	inline static int32_t get_offset_of__moveToTouchSpeed_58() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____moveToTouchSpeed_58)); }
	inline float get__moveToTouchSpeed_58() const { return ____moveToTouchSpeed_58; }
	inline float* get_address_of__moveToTouchSpeed_58() { return &____moveToTouchSpeed_58; }
	inline void set__moveToTouchSpeed_58(float value)
	{
		____moveToTouchSpeed_58 = value;
	}

	inline static int32_t get_offset_of__animateOnReturn_59() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____animateOnReturn_59)); }
	inline bool get__animateOnReturn_59() const { return ____animateOnReturn_59; }
	inline bool* get_address_of__animateOnReturn_59() { return &____animateOnReturn_59; }
	inline void set__animateOnReturn_59(bool value)
	{
		____animateOnReturn_59 = value;
	}

	inline static int32_t get_offset_of__returnSpeed_60() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____returnSpeed_60)); }
	inline float get__returnSpeed_60() const { return ____returnSpeed_60; }
	inline float* get_address_of__returnSpeed_60() { return &____returnSpeed_60; }
	inline void set__returnSpeed_60(float value)
	{
		____returnSpeed_60 = value;
	}

	inline static int32_t get_offset_of__manageRaycasting_61() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____manageRaycasting_61)); }
	inline bool get__manageRaycasting_61() const { return ____manageRaycasting_61; }
	inline bool* get_address_of__manageRaycasting_61() { return &____manageRaycasting_61; }
	inline void set__manageRaycasting_61(bool value)
	{
		____manageRaycasting_61 = value;
	}

	inline static int32_t get_offset_of_EiwtMLppfCtuooXjvdHygyGrsHS_62() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___EiwtMLppfCtuooXjvdHygyGrsHS_62)); }
	inline float get_EiwtMLppfCtuooXjvdHygyGrsHS_62() const { return ___EiwtMLppfCtuooXjvdHygyGrsHS_62; }
	inline float* get_address_of_EiwtMLppfCtuooXjvdHygyGrsHS_62() { return &___EiwtMLppfCtuooXjvdHygyGrsHS_62; }
	inline void set_EiwtMLppfCtuooXjvdHygyGrsHS_62(float value)
	{
		___EiwtMLppfCtuooXjvdHygyGrsHS_62 = value;
	}

	inline static int32_t get_offset_of_JHUTNNOcuztILjpjkavvMXajFRV_63() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___JHUTNNOcuztILjpjkavvMXajFRV_63)); }
	inline float get_JHUTNNOcuztILjpjkavvMXajFRV_63() const { return ___JHUTNNOcuztILjpjkavvMXajFRV_63; }
	inline float* get_address_of_JHUTNNOcuztILjpjkavvMXajFRV_63() { return &___JHUTNNOcuztILjpjkavvMXajFRV_63; }
	inline void set_JHUTNNOcuztILjpjkavvMXajFRV_63(float value)
	{
		___JHUTNNOcuztILjpjkavvMXajFRV_63 = value;
	}

	inline static int32_t get_offset_of_FcORQGMEvzohtgyCwrysLogmieu_64() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___FcORQGMEvzohtgyCwrysLogmieu_64)); }
	inline TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 * get_FcORQGMEvzohtgyCwrysLogmieu_64() const { return ___FcORQGMEvzohtgyCwrysLogmieu_64; }
	inline TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 ** get_address_of_FcORQGMEvzohtgyCwrysLogmieu_64() { return &___FcORQGMEvzohtgyCwrysLogmieu_64; }
	inline void set_FcORQGMEvzohtgyCwrysLogmieu_64(TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 * value)
	{
		___FcORQGMEvzohtgyCwrysLogmieu_64 = value;
		Il2CppCodeGenWriteBarrier((&___FcORQGMEvzohtgyCwrysLogmieu_64), value);
	}

	inline static int32_t get_offset_of_nysomDCBRIvDcmoXFQHdLQznHNr_65() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___nysomDCBRIvDcmoXFQHdLQznHNr_65)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_nysomDCBRIvDcmoXFQHdLQznHNr_65() const { return ___nysomDCBRIvDcmoXFQHdLQznHNr_65; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_nysomDCBRIvDcmoXFQHdLQznHNr_65() { return &___nysomDCBRIvDcmoXFQHdLQznHNr_65; }
	inline void set_nysomDCBRIvDcmoXFQHdLQznHNr_65(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___nysomDCBRIvDcmoXFQHdLQznHNr_65 = value;
	}

	inline static int32_t get_offset_of_reSQzxHMdzbyhGwOPrSROZjFOBx_66() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___reSQzxHMdzbyhGwOPrSROZjFOBx_66)); }
	inline bool get_reSQzxHMdzbyhGwOPrSROZjFOBx_66() const { return ___reSQzxHMdzbyhGwOPrSROZjFOBx_66; }
	inline bool* get_address_of_reSQzxHMdzbyhGwOPrSROZjFOBx_66() { return &___reSQzxHMdzbyhGwOPrSROZjFOBx_66; }
	inline void set_reSQzxHMdzbyhGwOPrSROZjFOBx_66(bool value)
	{
		___reSQzxHMdzbyhGwOPrSROZjFOBx_66 = value;
	}

	inline static int32_t get_offset_of_uaVpGuIOaSatQKbOxUPLMpimWG_67() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___uaVpGuIOaSatQKbOxUPLMpimWG_67)); }
	inline bool get_uaVpGuIOaSatQKbOxUPLMpimWG_67() const { return ___uaVpGuIOaSatQKbOxUPLMpimWG_67; }
	inline bool* get_address_of_uaVpGuIOaSatQKbOxUPLMpimWG_67() { return &___uaVpGuIOaSatQKbOxUPLMpimWG_67; }
	inline void set_uaVpGuIOaSatQKbOxUPLMpimWG_67(bool value)
	{
		___uaVpGuIOaSatQKbOxUPLMpimWG_67 = value;
	}

	inline static int32_t get_offset_of_mucIMzrdRGVGfPFucsRzAxGACqM_68() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___mucIMzrdRGVGfPFucsRzAxGACqM_68)); }
	inline int32_t get_mucIMzrdRGVGfPFucsRzAxGACqM_68() const { return ___mucIMzrdRGVGfPFucsRzAxGACqM_68; }
	inline int32_t* get_address_of_mucIMzrdRGVGfPFucsRzAxGACqM_68() { return &___mucIMzrdRGVGfPFucsRzAxGACqM_68; }
	inline void set_mucIMzrdRGVGfPFucsRzAxGACqM_68(int32_t value)
	{
		___mucIMzrdRGVGfPFucsRzAxGACqM_68 = value;
	}

	inline static int32_t get_offset_of_kKSSGIYcShDBXbkfYwidHsiqbRh_69() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___kKSSGIYcShDBXbkfYwidHsiqbRh_69)); }
	inline int32_t get_kKSSGIYcShDBXbkfYwidHsiqbRh_69() const { return ___kKSSGIYcShDBXbkfYwidHsiqbRh_69; }
	inline int32_t* get_address_of_kKSSGIYcShDBXbkfYwidHsiqbRh_69() { return &___kKSSGIYcShDBXbkfYwidHsiqbRh_69; }
	inline void set_kKSSGIYcShDBXbkfYwidHsiqbRh_69(int32_t value)
	{
		___kKSSGIYcShDBXbkfYwidHsiqbRh_69 = value;
	}

	inline static int32_t get_offset_of_VKAtGkSQLhkGGQSRuCzXdQVELuq_70() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___VKAtGkSQLhkGGQSRuCzXdQVELuq_70)); }
	inline int32_t get_VKAtGkSQLhkGGQSRuCzXdQVELuq_70() const { return ___VKAtGkSQLhkGGQSRuCzXdQVELuq_70; }
	inline int32_t* get_address_of_VKAtGkSQLhkGGQSRuCzXdQVELuq_70() { return &___VKAtGkSQLhkGGQSRuCzXdQVELuq_70; }
	inline void set_VKAtGkSQLhkGGQSRuCzXdQVELuq_70(int32_t value)
	{
		___VKAtGkSQLhkGGQSRuCzXdQVELuq_70 = value;
	}

	inline static int32_t get_offset_of_UsFAvRrPreZngsJiAjbwyGLdcub_71() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___UsFAvRrPreZngsJiAjbwyGLdcub_71)); }
	inline bool get_UsFAvRrPreZngsJiAjbwyGLdcub_71() const { return ___UsFAvRrPreZngsJiAjbwyGLdcub_71; }
	inline bool* get_address_of_UsFAvRrPreZngsJiAjbwyGLdcub_71() { return &___UsFAvRrPreZngsJiAjbwyGLdcub_71; }
	inline void set_UsFAvRrPreZngsJiAjbwyGLdcub_71(bool value)
	{
		___UsFAvRrPreZngsJiAjbwyGLdcub_71 = value;
	}

	inline static int32_t get_offset_of_pPGULjslgtAiohyNMfEalDGgOVl_72() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___pPGULjslgtAiohyNMfEalDGgOVl_72)); }
	inline bool get_pPGULjslgtAiohyNMfEalDGgOVl_72() const { return ___pPGULjslgtAiohyNMfEalDGgOVl_72; }
	inline bool* get_address_of_pPGULjslgtAiohyNMfEalDGgOVl_72() { return &___pPGULjslgtAiohyNMfEalDGgOVl_72; }
	inline void set_pPGULjslgtAiohyNMfEalDGgOVl_72(bool value)
	{
		___pPGULjslgtAiohyNMfEalDGgOVl_72 = value;
	}

	inline static int32_t get_offset_of_HgstqelxyDTEvxPqOrKBCHhdKVB_73() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___HgstqelxyDTEvxPqOrKBCHhdKVB_73)); }
	inline RuntimeObject* get_HgstqelxyDTEvxPqOrKBCHhdKVB_73() const { return ___HgstqelxyDTEvxPqOrKBCHhdKVB_73; }
	inline RuntimeObject** get_address_of_HgstqelxyDTEvxPqOrKBCHhdKVB_73() { return &___HgstqelxyDTEvxPqOrKBCHhdKVB_73; }
	inline void set_HgstqelxyDTEvxPqOrKBCHhdKVB_73(RuntimeObject* value)
	{
		___HgstqelxyDTEvxPqOrKBCHhdKVB_73 = value;
		Il2CppCodeGenWriteBarrier((&___HgstqelxyDTEvxPqOrKBCHhdKVB_73), value);
	}

	inline static int32_t get_offset_of_VUcbNyhUNSjVjmdmpXAHQiBoHeYO_74() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___VUcbNyhUNSjVjmdmpXAHQiBoHeYO_74)); }
	inline erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4 * get_VUcbNyhUNSjVjmdmpXAHQiBoHeYO_74() const { return ___VUcbNyhUNSjVjmdmpXAHQiBoHeYO_74; }
	inline erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4 ** get_address_of_VUcbNyhUNSjVjmdmpXAHQiBoHeYO_74() { return &___VUcbNyhUNSjVjmdmpXAHQiBoHeYO_74; }
	inline void set_VUcbNyhUNSjVjmdmpXAHQiBoHeYO_74(erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4 * value)
	{
		___VUcbNyhUNSjVjmdmpXAHQiBoHeYO_74 = value;
		Il2CppCodeGenWriteBarrier((&___VUcbNyhUNSjVjmdmpXAHQiBoHeYO_74), value);
	}

	inline static int32_t get_offset_of_bQOHtIPuejRPzOXhHfAkcDmJfTT_75() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___bQOHtIPuejRPzOXhHfAkcDmJfTT_75)); }
	inline Action_1_t4E78B000408BF7DAE9233B13DB3098949BB3E61B * get_bQOHtIPuejRPzOXhHfAkcDmJfTT_75() const { return ___bQOHtIPuejRPzOXhHfAkcDmJfTT_75; }
	inline Action_1_t4E78B000408BF7DAE9233B13DB3098949BB3E61B ** get_address_of_bQOHtIPuejRPzOXhHfAkcDmJfTT_75() { return &___bQOHtIPuejRPzOXhHfAkcDmJfTT_75; }
	inline void set_bQOHtIPuejRPzOXhHfAkcDmJfTT_75(Action_1_t4E78B000408BF7DAE9233B13DB3098949BB3E61B * value)
	{
		___bQOHtIPuejRPzOXhHfAkcDmJfTT_75 = value;
		Il2CppCodeGenWriteBarrier((&___bQOHtIPuejRPzOXhHfAkcDmJfTT_75), value);
	}

	inline static int32_t get_offset_of_AQDzRNACmkHZMcdFfzmRWfTaKBg_76() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___AQDzRNACmkHZMcdFfzmRWfTaKBg_76)); }
	inline Action_1_t4E78B000408BF7DAE9233B13DB3098949BB3E61B * get_AQDzRNACmkHZMcdFfzmRWfTaKBg_76() const { return ___AQDzRNACmkHZMcdFfzmRWfTaKBg_76; }
	inline Action_1_t4E78B000408BF7DAE9233B13DB3098949BB3E61B ** get_address_of_AQDzRNACmkHZMcdFfzmRWfTaKBg_76() { return &___AQDzRNACmkHZMcdFfzmRWfTaKBg_76; }
	inline void set_AQDzRNACmkHZMcdFfzmRWfTaKBg_76(Action_1_t4E78B000408BF7DAE9233B13DB3098949BB3E61B * value)
	{
		___AQDzRNACmkHZMcdFfzmRWfTaKBg_76 = value;
		Il2CppCodeGenWriteBarrier((&___AQDzRNACmkHZMcdFfzmRWfTaKBg_76), value);
	}

	inline static int32_t get_offset_of__onAxisValueChanged_77() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____onAxisValueChanged_77)); }
	inline AxisValueChangedEventHandler_t622BC4F8C94A13837769CEFE1FD12CC18B926791 * get__onAxisValueChanged_77() const { return ____onAxisValueChanged_77; }
	inline AxisValueChangedEventHandler_t622BC4F8C94A13837769CEFE1FD12CC18B926791 ** get_address_of__onAxisValueChanged_77() { return &____onAxisValueChanged_77; }
	inline void set__onAxisValueChanged_77(AxisValueChangedEventHandler_t622BC4F8C94A13837769CEFE1FD12CC18B926791 * value)
	{
		____onAxisValueChanged_77 = value;
		Il2CppCodeGenWriteBarrier((&____onAxisValueChanged_77), value);
	}

	inline static int32_t get_offset_of__onButtonValueChanged_78() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____onButtonValueChanged_78)); }
	inline ButtonValueChangedEventHandler_tB3FD14B60CDD8A2E3B8D8B26F951B056495B5A50 * get__onButtonValueChanged_78() const { return ____onButtonValueChanged_78; }
	inline ButtonValueChangedEventHandler_tB3FD14B60CDD8A2E3B8D8B26F951B056495B5A50 ** get_address_of__onButtonValueChanged_78() { return &____onButtonValueChanged_78; }
	inline void set__onButtonValueChanged_78(ButtonValueChangedEventHandler_tB3FD14B60CDD8A2E3B8D8B26F951B056495B5A50 * value)
	{
		____onButtonValueChanged_78 = value;
		Il2CppCodeGenWriteBarrier((&____onButtonValueChanged_78), value);
	}

	inline static int32_t get_offset_of__onButtonDown_79() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____onButtonDown_79)); }
	inline ButtonDownEventHandler_tD3FB726C5A3E17A89FA7992F2D0D0422286B7E7D * get__onButtonDown_79() const { return ____onButtonDown_79; }
	inline ButtonDownEventHandler_tD3FB726C5A3E17A89FA7992F2D0D0422286B7E7D ** get_address_of__onButtonDown_79() { return &____onButtonDown_79; }
	inline void set__onButtonDown_79(ButtonDownEventHandler_tD3FB726C5A3E17A89FA7992F2D0D0422286B7E7D * value)
	{
		____onButtonDown_79 = value;
		Il2CppCodeGenWriteBarrier((&____onButtonDown_79), value);
	}

	inline static int32_t get_offset_of__onButtonUp_80() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ____onButtonUp_80)); }
	inline ButtonUpEventHandler_t529887EE63C2C416911C6C64F8D230008EB73339 * get__onButtonUp_80() const { return ____onButtonUp_80; }
	inline ButtonUpEventHandler_t529887EE63C2C416911C6C64F8D230008EB73339 ** get_address_of__onButtonUp_80() { return &____onButtonUp_80; }
	inline void set__onButtonUp_80(ButtonUpEventHandler_t529887EE63C2C416911C6C64F8D230008EB73339 * value)
	{
		____onButtonUp_80 = value;
		Il2CppCodeGenWriteBarrier((&____onButtonUp_80), value);
	}

	inline static int32_t get_offset_of_SbJiaOqkhEQicEKkypVosjZLBXA_81() { return static_cast<int32_t>(offsetof(TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F, ___SbJiaOqkhEQicEKkypVosjZLBXA_81)); }
	inline Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF * get_SbJiaOqkhEQicEKkypVosjZLBXA_81() const { return ___SbJiaOqkhEQicEKkypVosjZLBXA_81; }
	inline Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF ** get_address_of_SbJiaOqkhEQicEKkypVosjZLBXA_81() { return &___SbJiaOqkhEQicEKkypVosjZLBXA_81; }
	inline void set_SbJiaOqkhEQicEKkypVosjZLBXA_81(Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF * value)
	{
		___SbJiaOqkhEQicEKkypVosjZLBXA_81 = value;
		Il2CppCodeGenWriteBarrier((&___SbJiaOqkhEQicEKkypVosjZLBXA_81), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHBUTTON_T192BE56ABF72197355626DC4A937D764B01D6D7F_H
#ifndef TOUCHJOYSTICK_T7D003265D5469EE86A9E1609771F8362EE1F703D_H
#define TOUCHJOYSTICK_T7D003265D5469EE86A9E1609771F8362EE1F703D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchJoystick
struct  TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D  : public TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8
{
public:
	// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat Rewired.ComponentControls.TouchJoystick::_horizontalAxisCustomControllerElement
	CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * ____horizontalAxisCustomControllerElement_44;
	// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat Rewired.ComponentControls.TouchJoystick::_verticalAxisCustomControllerElement
	CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * ____verticalAxisCustomControllerElement_45;
	// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForBoolean Rewired.ComponentControls.TouchJoystick::_tapCustomControllerElement
	CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54 * ____tapCustomControllerElement_46;
	// UnityEngine.RectTransform Rewired.ComponentControls.TouchJoystick::_stickTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____stickTransform_47;
	// Rewired.ComponentControls.TouchJoystick_JoystickMode Rewired.ComponentControls.TouchJoystick::_joystickMode
	int32_t ____joystickMode_48;
	// System.Single Rewired.ComponentControls.TouchJoystick::_digitalModeDeadZone
	float ____digitalModeDeadZone_49;
	// System.Single Rewired.ComponentControls.TouchJoystick::_stickRange
	float ____stickRange_50;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_scaleStickRange
	bool ____scaleStickRange_51;
	// Rewired.ComponentControls.TouchJoystick_StickBounds Rewired.ComponentControls.TouchJoystick::_stickBounds
	int32_t ____stickBounds_52;
	// Rewired.ComponentControls.TouchJoystick_AxisDirection Rewired.ComponentControls.TouchJoystick::_axesToUse
	int32_t ____axesToUse_53;
	// Rewired.ComponentControls.TouchJoystick_SnapDirections Rewired.ComponentControls.TouchJoystick::_snapDirections
	int32_t ____snapDirections_54;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_snapStickToTouch
	bool ____snapStickToTouch_55;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_centerStickOnRelease
	bool ____centerStickOnRelease_56;
	// Rewired.Internal.StandaloneAxis2D Rewired.ComponentControls.TouchJoystick::_axis2D
	StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A * ____axis2D_57;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_activateOnSwipeIn
	bool ____activateOnSwipeIn_58;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_stayActiveOnSwipeOut
	bool ____stayActiveOnSwipeOut_59;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_allowTap
	bool ____allowTap_60;
	// System.Single Rewired.ComponentControls.TouchJoystick::_tapTimeout
	float ____tapTimeout_61;
	// System.Int32 Rewired.ComponentControls.TouchJoystick::_tapDistanceLimit
	int32_t ____tapDistanceLimit_62;
	// Rewired.ComponentControls.TouchRegion Rewired.ComponentControls.TouchJoystick::_touchRegion
	TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 * ____touchRegion_63;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_useTouchRegionOnly
	bool ____useTouchRegionOnly_64;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_moveToTouchPosition
	bool ____moveToTouchPosition_65;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_returnOnRelease
	bool ____returnOnRelease_66;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_followTouchPosition
	bool ____followTouchPosition_67;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_animateOnMoveToTouch
	bool ____animateOnMoveToTouch_68;
	// System.Single Rewired.ComponentControls.TouchJoystick::_moveToTouchSpeed
	float ____moveToTouchSpeed_69;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_animateOnReturn
	bool ____animateOnReturn_70;
	// System.Single Rewired.ComponentControls.TouchJoystick::_returnSpeed
	float ____returnSpeed_71;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_manageRaycasting
	bool ____manageRaycasting_72;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_useXAxis
	bool ____useXAxis_73;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_useYAxis
	bool ____useYAxis_74;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<Rewired.ComponentControls.TouchJoystick_IValueChangedHandler,UnityEngine.Vector2> Rewired.ComponentControls.TouchJoystick::_hierarchyValueChangedHandlers
	HierarchyEventHelper_2_tE08473D90510E3FE69E11A9293B95BB4D002A65E * ____hierarchyValueChangedHandlers_75;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_HierarchyEventHelper`2<Rewired.ComponentControls.TouchJoystick_IStickPositionChangedHandler,UnityEngine.Vector2> Rewired.ComponentControls.TouchJoystick::_hierarchyStickPositionChangedHandlers
	HierarchyEventHelper_2_tDD149D49CCBE8F055ADC78F7DDF4FE4B5321FD8A * ____hierarchyStickPositionChangedHandlers_76;
	// Rewired.ComponentControls.TouchRegion Rewired.ComponentControls.TouchJoystick::_workingTouchRegion
	TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 * ____workingTouchRegion_77;
	// UnityEngine.Vector2 Rewired.ComponentControls.TouchJoystick::_origAnchoredPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____origAnchoredPosition_78;
	// UnityEngine.Vector2 Rewired.ComponentControls.TouchJoystick::_origStickAnchoredPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____origStickAnchoredPosition_79;
	// UnityEngine.Vector2 Rewired.ComponentControls.TouchJoystick::_lastPressAnchoredPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____lastPressAnchoredPosition_80;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_isMoving
	bool ____isMoving_81;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_isMovedFromDefaultPosition
	bool ____isMovedFromDefaultPosition_82;
	// Rewired.ComponentControls.TouchJoystick_NHoUMtvQEGOtTovxNMKxiWCTcIN Rewired.ComponentControls.TouchJoystick::_moveDirection
	int32_t ____moveDirection_83;
	// System.Int32 Rewired.ComponentControls.TouchJoystick::_pointerId
	int32_t ____pointerId_84;
	// System.Int32 Rewired.ComponentControls.TouchJoystick::_realMousePointerId
	int32_t ____realMousePointerId_85;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::UsFAvRrPreZngsJiAjbwyGLdcub
	bool ___UsFAvRrPreZngsJiAjbwyGLdcub_86;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::pPGULjslgtAiohyNMfEalDGgOVl
	bool ___pPGULjslgtAiohyNMfEalDGgOVl_87;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_pointerDownIsFake
	bool ____pointerDownIsFake_88;
	// UnityEngine.Vector2 Rewired.ComponentControls.TouchJoystick::_lastPressStartingValue
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____lastPressStartingValue_89;
	// Rewired.ComponentControls.TouchJoystick_GQMwmwtRNqThmSuCwNhLowMzATs Rewired.ComponentControls.TouchJoystick::_lastClaimSource
	int32_t ____lastClaimSource_90;
	// System.Single Rewired.ComponentControls.TouchJoystick::_touchStartTime
	float ____touchStartTime_91;
	// UnityEngine.Vector2 Rewired.ComponentControls.TouchJoystick::_touchStartPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____touchStartPosition_92;
	// System.Collections.IEnumerator Rewired.ComponentControls.TouchJoystick::_coroutineMove
	RuntimeObject* ____coroutineMove_93;
	// erAqfwCNbAAvnTADjtOsmRVrVxW Rewired.ComponentControls.TouchJoystick::_imageRaycastHelper
	erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4 * ____imageRaycastHelper_94;
	// System.Int32 Rewired.ComponentControls.TouchJoystick::_calculatedStickRange_lastUpdatedFrame
	int32_t ____calculatedStickRange_lastUpdatedFrame_95;
	// System.Int32 Rewired.ComponentControls.TouchJoystick::_lastTapFrame
	int32_t ____lastTapFrame_96;
	// System.Boolean Rewired.ComponentControls.TouchJoystick::_isEligibleForTap
	bool ____isEligibleForTap_97;
	// System.Single Rewired.ComponentControls.TouchJoystick::__calculatedStickRange_cachedValue
	float _____calculatedStickRange_cachedValue_98;
	// System.Action`1<Rewired.ComponentControls.TouchJoystick_NHoUMtvQEGOtTovxNMKxiWCTcIN> Rewired.ComponentControls.TouchJoystick::__moveStartedDelegate
	Action_1_tD0A6E282EB4EEB9F7D6D9FE4F7B149BAEE433F68 * _____moveStartedDelegate_99;
	// System.Action`1<Rewired.ComponentControls.TouchJoystick_NHoUMtvQEGOtTovxNMKxiWCTcIN> Rewired.ComponentControls.TouchJoystick::__moveEndedDelegate
	Action_1_tD0A6E282EB4EEB9F7D6D9FE4F7B149BAEE433F68 * _____moveEndedDelegate_100;
	// Rewired.ComponentControls.TouchJoystick_ValueChangedEventHandler Rewired.ComponentControls.TouchJoystick::_onValueChanged
	ValueChangedEventHandler_t739FAF55A04B824B9182F47969314C660604524B * ____onValueChanged_101;
	// Rewired.ComponentControls.TouchJoystick_ValueChangedEventHandler Rewired.ComponentControls.TouchJoystick::_onStickPositionChanged
	ValueChangedEventHandler_t739FAF55A04B824B9182F47969314C660604524B * ____onStickPositionChanged_102;
	// Rewired.ComponentControls.TouchJoystick_TouchStartedEventHandler Rewired.ComponentControls.TouchJoystick::_onTouchStarted
	TouchStartedEventHandler_tC39C61D509D6C9452386D0D05B7782F35841D4AB * ____onTouchStarted_103;
	// Rewired.ComponentControls.TouchJoystick_TouchEndedEventHandler Rewired.ComponentControls.TouchJoystick::_onTouchEnded
	TouchEndedEventHandler_t05BD8DD2BB21689F364B8300E6A1F266093E111C * ____onTouchEnded_104;
	// Rewired.ComponentControls.TouchJoystick_TapEventHandler Rewired.ComponentControls.TouchJoystick::_onTap
	TapEventHandler_tE705770344A52DD95176406B4B00E5EE8E525B2F * ____onTap_105;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> Rewired.ComponentControls.TouchJoystick::__fakePointerEventData
	Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF * _____fakePointerEventData_106;

public:
	inline static int32_t get_offset_of__horizontalAxisCustomControllerElement_44() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____horizontalAxisCustomControllerElement_44)); }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * get__horizontalAxisCustomControllerElement_44() const { return ____horizontalAxisCustomControllerElement_44; }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 ** get_address_of__horizontalAxisCustomControllerElement_44() { return &____horizontalAxisCustomControllerElement_44; }
	inline void set__horizontalAxisCustomControllerElement_44(CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * value)
	{
		____horizontalAxisCustomControllerElement_44 = value;
		Il2CppCodeGenWriteBarrier((&____horizontalAxisCustomControllerElement_44), value);
	}

	inline static int32_t get_offset_of__verticalAxisCustomControllerElement_45() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____verticalAxisCustomControllerElement_45)); }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * get__verticalAxisCustomControllerElement_45() const { return ____verticalAxisCustomControllerElement_45; }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 ** get_address_of__verticalAxisCustomControllerElement_45() { return &____verticalAxisCustomControllerElement_45; }
	inline void set__verticalAxisCustomControllerElement_45(CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * value)
	{
		____verticalAxisCustomControllerElement_45 = value;
		Il2CppCodeGenWriteBarrier((&____verticalAxisCustomControllerElement_45), value);
	}

	inline static int32_t get_offset_of__tapCustomControllerElement_46() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____tapCustomControllerElement_46)); }
	inline CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54 * get__tapCustomControllerElement_46() const { return ____tapCustomControllerElement_46; }
	inline CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54 ** get_address_of__tapCustomControllerElement_46() { return &____tapCustomControllerElement_46; }
	inline void set__tapCustomControllerElement_46(CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54 * value)
	{
		____tapCustomControllerElement_46 = value;
		Il2CppCodeGenWriteBarrier((&____tapCustomControllerElement_46), value);
	}

	inline static int32_t get_offset_of__stickTransform_47() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____stickTransform_47)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__stickTransform_47() const { return ____stickTransform_47; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__stickTransform_47() { return &____stickTransform_47; }
	inline void set__stickTransform_47(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____stickTransform_47 = value;
		Il2CppCodeGenWriteBarrier((&____stickTransform_47), value);
	}

	inline static int32_t get_offset_of__joystickMode_48() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____joystickMode_48)); }
	inline int32_t get__joystickMode_48() const { return ____joystickMode_48; }
	inline int32_t* get_address_of__joystickMode_48() { return &____joystickMode_48; }
	inline void set__joystickMode_48(int32_t value)
	{
		____joystickMode_48 = value;
	}

	inline static int32_t get_offset_of__digitalModeDeadZone_49() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____digitalModeDeadZone_49)); }
	inline float get__digitalModeDeadZone_49() const { return ____digitalModeDeadZone_49; }
	inline float* get_address_of__digitalModeDeadZone_49() { return &____digitalModeDeadZone_49; }
	inline void set__digitalModeDeadZone_49(float value)
	{
		____digitalModeDeadZone_49 = value;
	}

	inline static int32_t get_offset_of__stickRange_50() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____stickRange_50)); }
	inline float get__stickRange_50() const { return ____stickRange_50; }
	inline float* get_address_of__stickRange_50() { return &____stickRange_50; }
	inline void set__stickRange_50(float value)
	{
		____stickRange_50 = value;
	}

	inline static int32_t get_offset_of__scaleStickRange_51() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____scaleStickRange_51)); }
	inline bool get__scaleStickRange_51() const { return ____scaleStickRange_51; }
	inline bool* get_address_of__scaleStickRange_51() { return &____scaleStickRange_51; }
	inline void set__scaleStickRange_51(bool value)
	{
		____scaleStickRange_51 = value;
	}

	inline static int32_t get_offset_of__stickBounds_52() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____stickBounds_52)); }
	inline int32_t get__stickBounds_52() const { return ____stickBounds_52; }
	inline int32_t* get_address_of__stickBounds_52() { return &____stickBounds_52; }
	inline void set__stickBounds_52(int32_t value)
	{
		____stickBounds_52 = value;
	}

	inline static int32_t get_offset_of__axesToUse_53() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____axesToUse_53)); }
	inline int32_t get__axesToUse_53() const { return ____axesToUse_53; }
	inline int32_t* get_address_of__axesToUse_53() { return &____axesToUse_53; }
	inline void set__axesToUse_53(int32_t value)
	{
		____axesToUse_53 = value;
	}

	inline static int32_t get_offset_of__snapDirections_54() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____snapDirections_54)); }
	inline int32_t get__snapDirections_54() const { return ____snapDirections_54; }
	inline int32_t* get_address_of__snapDirections_54() { return &____snapDirections_54; }
	inline void set__snapDirections_54(int32_t value)
	{
		____snapDirections_54 = value;
	}

	inline static int32_t get_offset_of__snapStickToTouch_55() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____snapStickToTouch_55)); }
	inline bool get__snapStickToTouch_55() const { return ____snapStickToTouch_55; }
	inline bool* get_address_of__snapStickToTouch_55() { return &____snapStickToTouch_55; }
	inline void set__snapStickToTouch_55(bool value)
	{
		____snapStickToTouch_55 = value;
	}

	inline static int32_t get_offset_of__centerStickOnRelease_56() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____centerStickOnRelease_56)); }
	inline bool get__centerStickOnRelease_56() const { return ____centerStickOnRelease_56; }
	inline bool* get_address_of__centerStickOnRelease_56() { return &____centerStickOnRelease_56; }
	inline void set__centerStickOnRelease_56(bool value)
	{
		____centerStickOnRelease_56 = value;
	}

	inline static int32_t get_offset_of__axis2D_57() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____axis2D_57)); }
	inline StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A * get__axis2D_57() const { return ____axis2D_57; }
	inline StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A ** get_address_of__axis2D_57() { return &____axis2D_57; }
	inline void set__axis2D_57(StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A * value)
	{
		____axis2D_57 = value;
		Il2CppCodeGenWriteBarrier((&____axis2D_57), value);
	}

	inline static int32_t get_offset_of__activateOnSwipeIn_58() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____activateOnSwipeIn_58)); }
	inline bool get__activateOnSwipeIn_58() const { return ____activateOnSwipeIn_58; }
	inline bool* get_address_of__activateOnSwipeIn_58() { return &____activateOnSwipeIn_58; }
	inline void set__activateOnSwipeIn_58(bool value)
	{
		____activateOnSwipeIn_58 = value;
	}

	inline static int32_t get_offset_of__stayActiveOnSwipeOut_59() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____stayActiveOnSwipeOut_59)); }
	inline bool get__stayActiveOnSwipeOut_59() const { return ____stayActiveOnSwipeOut_59; }
	inline bool* get_address_of__stayActiveOnSwipeOut_59() { return &____stayActiveOnSwipeOut_59; }
	inline void set__stayActiveOnSwipeOut_59(bool value)
	{
		____stayActiveOnSwipeOut_59 = value;
	}

	inline static int32_t get_offset_of__allowTap_60() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____allowTap_60)); }
	inline bool get__allowTap_60() const { return ____allowTap_60; }
	inline bool* get_address_of__allowTap_60() { return &____allowTap_60; }
	inline void set__allowTap_60(bool value)
	{
		____allowTap_60 = value;
	}

	inline static int32_t get_offset_of__tapTimeout_61() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____tapTimeout_61)); }
	inline float get__tapTimeout_61() const { return ____tapTimeout_61; }
	inline float* get_address_of__tapTimeout_61() { return &____tapTimeout_61; }
	inline void set__tapTimeout_61(float value)
	{
		____tapTimeout_61 = value;
	}

	inline static int32_t get_offset_of__tapDistanceLimit_62() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____tapDistanceLimit_62)); }
	inline int32_t get__tapDistanceLimit_62() const { return ____tapDistanceLimit_62; }
	inline int32_t* get_address_of__tapDistanceLimit_62() { return &____tapDistanceLimit_62; }
	inline void set__tapDistanceLimit_62(int32_t value)
	{
		____tapDistanceLimit_62 = value;
	}

	inline static int32_t get_offset_of__touchRegion_63() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____touchRegion_63)); }
	inline TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 * get__touchRegion_63() const { return ____touchRegion_63; }
	inline TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 ** get_address_of__touchRegion_63() { return &____touchRegion_63; }
	inline void set__touchRegion_63(TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 * value)
	{
		____touchRegion_63 = value;
		Il2CppCodeGenWriteBarrier((&____touchRegion_63), value);
	}

	inline static int32_t get_offset_of__useTouchRegionOnly_64() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____useTouchRegionOnly_64)); }
	inline bool get__useTouchRegionOnly_64() const { return ____useTouchRegionOnly_64; }
	inline bool* get_address_of__useTouchRegionOnly_64() { return &____useTouchRegionOnly_64; }
	inline void set__useTouchRegionOnly_64(bool value)
	{
		____useTouchRegionOnly_64 = value;
	}

	inline static int32_t get_offset_of__moveToTouchPosition_65() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____moveToTouchPosition_65)); }
	inline bool get__moveToTouchPosition_65() const { return ____moveToTouchPosition_65; }
	inline bool* get_address_of__moveToTouchPosition_65() { return &____moveToTouchPosition_65; }
	inline void set__moveToTouchPosition_65(bool value)
	{
		____moveToTouchPosition_65 = value;
	}

	inline static int32_t get_offset_of__returnOnRelease_66() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____returnOnRelease_66)); }
	inline bool get__returnOnRelease_66() const { return ____returnOnRelease_66; }
	inline bool* get_address_of__returnOnRelease_66() { return &____returnOnRelease_66; }
	inline void set__returnOnRelease_66(bool value)
	{
		____returnOnRelease_66 = value;
	}

	inline static int32_t get_offset_of__followTouchPosition_67() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____followTouchPosition_67)); }
	inline bool get__followTouchPosition_67() const { return ____followTouchPosition_67; }
	inline bool* get_address_of__followTouchPosition_67() { return &____followTouchPosition_67; }
	inline void set__followTouchPosition_67(bool value)
	{
		____followTouchPosition_67 = value;
	}

	inline static int32_t get_offset_of__animateOnMoveToTouch_68() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____animateOnMoveToTouch_68)); }
	inline bool get__animateOnMoveToTouch_68() const { return ____animateOnMoveToTouch_68; }
	inline bool* get_address_of__animateOnMoveToTouch_68() { return &____animateOnMoveToTouch_68; }
	inline void set__animateOnMoveToTouch_68(bool value)
	{
		____animateOnMoveToTouch_68 = value;
	}

	inline static int32_t get_offset_of__moveToTouchSpeed_69() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____moveToTouchSpeed_69)); }
	inline float get__moveToTouchSpeed_69() const { return ____moveToTouchSpeed_69; }
	inline float* get_address_of__moveToTouchSpeed_69() { return &____moveToTouchSpeed_69; }
	inline void set__moveToTouchSpeed_69(float value)
	{
		____moveToTouchSpeed_69 = value;
	}

	inline static int32_t get_offset_of__animateOnReturn_70() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____animateOnReturn_70)); }
	inline bool get__animateOnReturn_70() const { return ____animateOnReturn_70; }
	inline bool* get_address_of__animateOnReturn_70() { return &____animateOnReturn_70; }
	inline void set__animateOnReturn_70(bool value)
	{
		____animateOnReturn_70 = value;
	}

	inline static int32_t get_offset_of__returnSpeed_71() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____returnSpeed_71)); }
	inline float get__returnSpeed_71() const { return ____returnSpeed_71; }
	inline float* get_address_of__returnSpeed_71() { return &____returnSpeed_71; }
	inline void set__returnSpeed_71(float value)
	{
		____returnSpeed_71 = value;
	}

	inline static int32_t get_offset_of__manageRaycasting_72() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____manageRaycasting_72)); }
	inline bool get__manageRaycasting_72() const { return ____manageRaycasting_72; }
	inline bool* get_address_of__manageRaycasting_72() { return &____manageRaycasting_72; }
	inline void set__manageRaycasting_72(bool value)
	{
		____manageRaycasting_72 = value;
	}

	inline static int32_t get_offset_of__useXAxis_73() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____useXAxis_73)); }
	inline bool get__useXAxis_73() const { return ____useXAxis_73; }
	inline bool* get_address_of__useXAxis_73() { return &____useXAxis_73; }
	inline void set__useXAxis_73(bool value)
	{
		____useXAxis_73 = value;
	}

	inline static int32_t get_offset_of__useYAxis_74() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____useYAxis_74)); }
	inline bool get__useYAxis_74() const { return ____useYAxis_74; }
	inline bool* get_address_of__useYAxis_74() { return &____useYAxis_74; }
	inline void set__useYAxis_74(bool value)
	{
		____useYAxis_74 = value;
	}

	inline static int32_t get_offset_of__hierarchyValueChangedHandlers_75() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____hierarchyValueChangedHandlers_75)); }
	inline HierarchyEventHelper_2_tE08473D90510E3FE69E11A9293B95BB4D002A65E * get__hierarchyValueChangedHandlers_75() const { return ____hierarchyValueChangedHandlers_75; }
	inline HierarchyEventHelper_2_tE08473D90510E3FE69E11A9293B95BB4D002A65E ** get_address_of__hierarchyValueChangedHandlers_75() { return &____hierarchyValueChangedHandlers_75; }
	inline void set__hierarchyValueChangedHandlers_75(HierarchyEventHelper_2_tE08473D90510E3FE69E11A9293B95BB4D002A65E * value)
	{
		____hierarchyValueChangedHandlers_75 = value;
		Il2CppCodeGenWriteBarrier((&____hierarchyValueChangedHandlers_75), value);
	}

	inline static int32_t get_offset_of__hierarchyStickPositionChangedHandlers_76() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____hierarchyStickPositionChangedHandlers_76)); }
	inline HierarchyEventHelper_2_tDD149D49CCBE8F055ADC78F7DDF4FE4B5321FD8A * get__hierarchyStickPositionChangedHandlers_76() const { return ____hierarchyStickPositionChangedHandlers_76; }
	inline HierarchyEventHelper_2_tDD149D49CCBE8F055ADC78F7DDF4FE4B5321FD8A ** get_address_of__hierarchyStickPositionChangedHandlers_76() { return &____hierarchyStickPositionChangedHandlers_76; }
	inline void set__hierarchyStickPositionChangedHandlers_76(HierarchyEventHelper_2_tDD149D49CCBE8F055ADC78F7DDF4FE4B5321FD8A * value)
	{
		____hierarchyStickPositionChangedHandlers_76 = value;
		Il2CppCodeGenWriteBarrier((&____hierarchyStickPositionChangedHandlers_76), value);
	}

	inline static int32_t get_offset_of__workingTouchRegion_77() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____workingTouchRegion_77)); }
	inline TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 * get__workingTouchRegion_77() const { return ____workingTouchRegion_77; }
	inline TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 ** get_address_of__workingTouchRegion_77() { return &____workingTouchRegion_77; }
	inline void set__workingTouchRegion_77(TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49 * value)
	{
		____workingTouchRegion_77 = value;
		Il2CppCodeGenWriteBarrier((&____workingTouchRegion_77), value);
	}

	inline static int32_t get_offset_of__origAnchoredPosition_78() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____origAnchoredPosition_78)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__origAnchoredPosition_78() const { return ____origAnchoredPosition_78; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__origAnchoredPosition_78() { return &____origAnchoredPosition_78; }
	inline void set__origAnchoredPosition_78(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____origAnchoredPosition_78 = value;
	}

	inline static int32_t get_offset_of__origStickAnchoredPosition_79() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____origStickAnchoredPosition_79)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__origStickAnchoredPosition_79() const { return ____origStickAnchoredPosition_79; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__origStickAnchoredPosition_79() { return &____origStickAnchoredPosition_79; }
	inline void set__origStickAnchoredPosition_79(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____origStickAnchoredPosition_79 = value;
	}

	inline static int32_t get_offset_of__lastPressAnchoredPosition_80() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____lastPressAnchoredPosition_80)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__lastPressAnchoredPosition_80() const { return ____lastPressAnchoredPosition_80; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__lastPressAnchoredPosition_80() { return &____lastPressAnchoredPosition_80; }
	inline void set__lastPressAnchoredPosition_80(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____lastPressAnchoredPosition_80 = value;
	}

	inline static int32_t get_offset_of__isMoving_81() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____isMoving_81)); }
	inline bool get__isMoving_81() const { return ____isMoving_81; }
	inline bool* get_address_of__isMoving_81() { return &____isMoving_81; }
	inline void set__isMoving_81(bool value)
	{
		____isMoving_81 = value;
	}

	inline static int32_t get_offset_of__isMovedFromDefaultPosition_82() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____isMovedFromDefaultPosition_82)); }
	inline bool get__isMovedFromDefaultPosition_82() const { return ____isMovedFromDefaultPosition_82; }
	inline bool* get_address_of__isMovedFromDefaultPosition_82() { return &____isMovedFromDefaultPosition_82; }
	inline void set__isMovedFromDefaultPosition_82(bool value)
	{
		____isMovedFromDefaultPosition_82 = value;
	}

	inline static int32_t get_offset_of__moveDirection_83() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____moveDirection_83)); }
	inline int32_t get__moveDirection_83() const { return ____moveDirection_83; }
	inline int32_t* get_address_of__moveDirection_83() { return &____moveDirection_83; }
	inline void set__moveDirection_83(int32_t value)
	{
		____moveDirection_83 = value;
	}

	inline static int32_t get_offset_of__pointerId_84() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____pointerId_84)); }
	inline int32_t get__pointerId_84() const { return ____pointerId_84; }
	inline int32_t* get_address_of__pointerId_84() { return &____pointerId_84; }
	inline void set__pointerId_84(int32_t value)
	{
		____pointerId_84 = value;
	}

	inline static int32_t get_offset_of__realMousePointerId_85() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____realMousePointerId_85)); }
	inline int32_t get__realMousePointerId_85() const { return ____realMousePointerId_85; }
	inline int32_t* get_address_of__realMousePointerId_85() { return &____realMousePointerId_85; }
	inline void set__realMousePointerId_85(int32_t value)
	{
		____realMousePointerId_85 = value;
	}

	inline static int32_t get_offset_of_UsFAvRrPreZngsJiAjbwyGLdcub_86() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ___UsFAvRrPreZngsJiAjbwyGLdcub_86)); }
	inline bool get_UsFAvRrPreZngsJiAjbwyGLdcub_86() const { return ___UsFAvRrPreZngsJiAjbwyGLdcub_86; }
	inline bool* get_address_of_UsFAvRrPreZngsJiAjbwyGLdcub_86() { return &___UsFAvRrPreZngsJiAjbwyGLdcub_86; }
	inline void set_UsFAvRrPreZngsJiAjbwyGLdcub_86(bool value)
	{
		___UsFAvRrPreZngsJiAjbwyGLdcub_86 = value;
	}

	inline static int32_t get_offset_of_pPGULjslgtAiohyNMfEalDGgOVl_87() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ___pPGULjslgtAiohyNMfEalDGgOVl_87)); }
	inline bool get_pPGULjslgtAiohyNMfEalDGgOVl_87() const { return ___pPGULjslgtAiohyNMfEalDGgOVl_87; }
	inline bool* get_address_of_pPGULjslgtAiohyNMfEalDGgOVl_87() { return &___pPGULjslgtAiohyNMfEalDGgOVl_87; }
	inline void set_pPGULjslgtAiohyNMfEalDGgOVl_87(bool value)
	{
		___pPGULjslgtAiohyNMfEalDGgOVl_87 = value;
	}

	inline static int32_t get_offset_of__pointerDownIsFake_88() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____pointerDownIsFake_88)); }
	inline bool get__pointerDownIsFake_88() const { return ____pointerDownIsFake_88; }
	inline bool* get_address_of__pointerDownIsFake_88() { return &____pointerDownIsFake_88; }
	inline void set__pointerDownIsFake_88(bool value)
	{
		____pointerDownIsFake_88 = value;
	}

	inline static int32_t get_offset_of__lastPressStartingValue_89() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____lastPressStartingValue_89)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__lastPressStartingValue_89() const { return ____lastPressStartingValue_89; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__lastPressStartingValue_89() { return &____lastPressStartingValue_89; }
	inline void set__lastPressStartingValue_89(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____lastPressStartingValue_89 = value;
	}

	inline static int32_t get_offset_of__lastClaimSource_90() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____lastClaimSource_90)); }
	inline int32_t get__lastClaimSource_90() const { return ____lastClaimSource_90; }
	inline int32_t* get_address_of__lastClaimSource_90() { return &____lastClaimSource_90; }
	inline void set__lastClaimSource_90(int32_t value)
	{
		____lastClaimSource_90 = value;
	}

	inline static int32_t get_offset_of__touchStartTime_91() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____touchStartTime_91)); }
	inline float get__touchStartTime_91() const { return ____touchStartTime_91; }
	inline float* get_address_of__touchStartTime_91() { return &____touchStartTime_91; }
	inline void set__touchStartTime_91(float value)
	{
		____touchStartTime_91 = value;
	}

	inline static int32_t get_offset_of__touchStartPosition_92() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____touchStartPosition_92)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__touchStartPosition_92() const { return ____touchStartPosition_92; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__touchStartPosition_92() { return &____touchStartPosition_92; }
	inline void set__touchStartPosition_92(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____touchStartPosition_92 = value;
	}

	inline static int32_t get_offset_of__coroutineMove_93() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____coroutineMove_93)); }
	inline RuntimeObject* get__coroutineMove_93() const { return ____coroutineMove_93; }
	inline RuntimeObject** get_address_of__coroutineMove_93() { return &____coroutineMove_93; }
	inline void set__coroutineMove_93(RuntimeObject* value)
	{
		____coroutineMove_93 = value;
		Il2CppCodeGenWriteBarrier((&____coroutineMove_93), value);
	}

	inline static int32_t get_offset_of__imageRaycastHelper_94() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____imageRaycastHelper_94)); }
	inline erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4 * get__imageRaycastHelper_94() const { return ____imageRaycastHelper_94; }
	inline erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4 ** get_address_of__imageRaycastHelper_94() { return &____imageRaycastHelper_94; }
	inline void set__imageRaycastHelper_94(erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4 * value)
	{
		____imageRaycastHelper_94 = value;
		Il2CppCodeGenWriteBarrier((&____imageRaycastHelper_94), value);
	}

	inline static int32_t get_offset_of__calculatedStickRange_lastUpdatedFrame_95() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____calculatedStickRange_lastUpdatedFrame_95)); }
	inline int32_t get__calculatedStickRange_lastUpdatedFrame_95() const { return ____calculatedStickRange_lastUpdatedFrame_95; }
	inline int32_t* get_address_of__calculatedStickRange_lastUpdatedFrame_95() { return &____calculatedStickRange_lastUpdatedFrame_95; }
	inline void set__calculatedStickRange_lastUpdatedFrame_95(int32_t value)
	{
		____calculatedStickRange_lastUpdatedFrame_95 = value;
	}

	inline static int32_t get_offset_of__lastTapFrame_96() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____lastTapFrame_96)); }
	inline int32_t get__lastTapFrame_96() const { return ____lastTapFrame_96; }
	inline int32_t* get_address_of__lastTapFrame_96() { return &____lastTapFrame_96; }
	inline void set__lastTapFrame_96(int32_t value)
	{
		____lastTapFrame_96 = value;
	}

	inline static int32_t get_offset_of__isEligibleForTap_97() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____isEligibleForTap_97)); }
	inline bool get__isEligibleForTap_97() const { return ____isEligibleForTap_97; }
	inline bool* get_address_of__isEligibleForTap_97() { return &____isEligibleForTap_97; }
	inline void set__isEligibleForTap_97(bool value)
	{
		____isEligibleForTap_97 = value;
	}

	inline static int32_t get_offset_of___calculatedStickRange_cachedValue_98() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, _____calculatedStickRange_cachedValue_98)); }
	inline float get___calculatedStickRange_cachedValue_98() const { return _____calculatedStickRange_cachedValue_98; }
	inline float* get_address_of___calculatedStickRange_cachedValue_98() { return &_____calculatedStickRange_cachedValue_98; }
	inline void set___calculatedStickRange_cachedValue_98(float value)
	{
		_____calculatedStickRange_cachedValue_98 = value;
	}

	inline static int32_t get_offset_of___moveStartedDelegate_99() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, _____moveStartedDelegate_99)); }
	inline Action_1_tD0A6E282EB4EEB9F7D6D9FE4F7B149BAEE433F68 * get___moveStartedDelegate_99() const { return _____moveStartedDelegate_99; }
	inline Action_1_tD0A6E282EB4EEB9F7D6D9FE4F7B149BAEE433F68 ** get_address_of___moveStartedDelegate_99() { return &_____moveStartedDelegate_99; }
	inline void set___moveStartedDelegate_99(Action_1_tD0A6E282EB4EEB9F7D6D9FE4F7B149BAEE433F68 * value)
	{
		_____moveStartedDelegate_99 = value;
		Il2CppCodeGenWriteBarrier((&_____moveStartedDelegate_99), value);
	}

	inline static int32_t get_offset_of___moveEndedDelegate_100() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, _____moveEndedDelegate_100)); }
	inline Action_1_tD0A6E282EB4EEB9F7D6D9FE4F7B149BAEE433F68 * get___moveEndedDelegate_100() const { return _____moveEndedDelegate_100; }
	inline Action_1_tD0A6E282EB4EEB9F7D6D9FE4F7B149BAEE433F68 ** get_address_of___moveEndedDelegate_100() { return &_____moveEndedDelegate_100; }
	inline void set___moveEndedDelegate_100(Action_1_tD0A6E282EB4EEB9F7D6D9FE4F7B149BAEE433F68 * value)
	{
		_____moveEndedDelegate_100 = value;
		Il2CppCodeGenWriteBarrier((&_____moveEndedDelegate_100), value);
	}

	inline static int32_t get_offset_of__onValueChanged_101() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____onValueChanged_101)); }
	inline ValueChangedEventHandler_t739FAF55A04B824B9182F47969314C660604524B * get__onValueChanged_101() const { return ____onValueChanged_101; }
	inline ValueChangedEventHandler_t739FAF55A04B824B9182F47969314C660604524B ** get_address_of__onValueChanged_101() { return &____onValueChanged_101; }
	inline void set__onValueChanged_101(ValueChangedEventHandler_t739FAF55A04B824B9182F47969314C660604524B * value)
	{
		____onValueChanged_101 = value;
		Il2CppCodeGenWriteBarrier((&____onValueChanged_101), value);
	}

	inline static int32_t get_offset_of__onStickPositionChanged_102() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____onStickPositionChanged_102)); }
	inline ValueChangedEventHandler_t739FAF55A04B824B9182F47969314C660604524B * get__onStickPositionChanged_102() const { return ____onStickPositionChanged_102; }
	inline ValueChangedEventHandler_t739FAF55A04B824B9182F47969314C660604524B ** get_address_of__onStickPositionChanged_102() { return &____onStickPositionChanged_102; }
	inline void set__onStickPositionChanged_102(ValueChangedEventHandler_t739FAF55A04B824B9182F47969314C660604524B * value)
	{
		____onStickPositionChanged_102 = value;
		Il2CppCodeGenWriteBarrier((&____onStickPositionChanged_102), value);
	}

	inline static int32_t get_offset_of__onTouchStarted_103() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____onTouchStarted_103)); }
	inline TouchStartedEventHandler_tC39C61D509D6C9452386D0D05B7782F35841D4AB * get__onTouchStarted_103() const { return ____onTouchStarted_103; }
	inline TouchStartedEventHandler_tC39C61D509D6C9452386D0D05B7782F35841D4AB ** get_address_of__onTouchStarted_103() { return &____onTouchStarted_103; }
	inline void set__onTouchStarted_103(TouchStartedEventHandler_tC39C61D509D6C9452386D0D05B7782F35841D4AB * value)
	{
		____onTouchStarted_103 = value;
		Il2CppCodeGenWriteBarrier((&____onTouchStarted_103), value);
	}

	inline static int32_t get_offset_of__onTouchEnded_104() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____onTouchEnded_104)); }
	inline TouchEndedEventHandler_t05BD8DD2BB21689F364B8300E6A1F266093E111C * get__onTouchEnded_104() const { return ____onTouchEnded_104; }
	inline TouchEndedEventHandler_t05BD8DD2BB21689F364B8300E6A1F266093E111C ** get_address_of__onTouchEnded_104() { return &____onTouchEnded_104; }
	inline void set__onTouchEnded_104(TouchEndedEventHandler_t05BD8DD2BB21689F364B8300E6A1F266093E111C * value)
	{
		____onTouchEnded_104 = value;
		Il2CppCodeGenWriteBarrier((&____onTouchEnded_104), value);
	}

	inline static int32_t get_offset_of__onTap_105() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, ____onTap_105)); }
	inline TapEventHandler_tE705770344A52DD95176406B4B00E5EE8E525B2F * get__onTap_105() const { return ____onTap_105; }
	inline TapEventHandler_tE705770344A52DD95176406B4B00E5EE8E525B2F ** get_address_of__onTap_105() { return &____onTap_105; }
	inline void set__onTap_105(TapEventHandler_tE705770344A52DD95176406B4B00E5EE8E525B2F * value)
	{
		____onTap_105 = value;
		Il2CppCodeGenWriteBarrier((&____onTap_105), value);
	}

	inline static int32_t get_offset_of___fakePointerEventData_106() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D, _____fakePointerEventData_106)); }
	inline Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF * get___fakePointerEventData_106() const { return _____fakePointerEventData_106; }
	inline Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF ** get_address_of___fakePointerEventData_106() { return &_____fakePointerEventData_106; }
	inline void set___fakePointerEventData_106(Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF * value)
	{
		_____fakePointerEventData_106 = value;
		Il2CppCodeGenWriteBarrier((&_____fakePointerEventData_106), value);
	}
};

struct TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D_StaticFields
{
public:
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<Rewired.ComponentControls.TouchJoystick_IValueChangedHandler,UnityEngine.Vector2> Rewired.ComponentControls.TouchJoystick::__valueChangedHandlerDelegate
	EventFunction_2_t0A48F4F300E24FBDB69326E87FD1939D5A9C1AF0 * _____valueChangedHandlerDelegate_107;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<Rewired.ComponentControls.TouchJoystick_IStickPositionChangedHandler,UnityEngine.Vector2> Rewired.ComponentControls.TouchJoystick::__stickPositionChangedHandlerDelegate
	EventFunction_2_t4604C1D8FE3FF11B06EC0FF84C1EEED688E6DFEC * _____stickPositionChangedHandlerDelegate_108;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<Rewired.ComponentControls.TouchJoystick_IValueChangedHandler,UnityEngine.Vector2> Rewired.ComponentControls.TouchJoystick::CSU24<>9__CachedAnonymousMethodDelegate8
	EventFunction_2_t0A48F4F300E24FBDB69326E87FD1939D5A9C1AF0 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate8_109;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<Rewired.ComponentControls.TouchJoystick_IStickPositionChangedHandler,UnityEngine.Vector2> Rewired.ComponentControls.TouchJoystick::CSU24<>9__CachedAnonymousMethodDelegatea
	EventFunction_2_t4604C1D8FE3FF11B06EC0FF84C1EEED688E6DFEC * ___CSU24U3CU3E9__CachedAnonymousMethodDelegatea_110;

public:
	inline static int32_t get_offset_of___valueChangedHandlerDelegate_107() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D_StaticFields, _____valueChangedHandlerDelegate_107)); }
	inline EventFunction_2_t0A48F4F300E24FBDB69326E87FD1939D5A9C1AF0 * get___valueChangedHandlerDelegate_107() const { return _____valueChangedHandlerDelegate_107; }
	inline EventFunction_2_t0A48F4F300E24FBDB69326E87FD1939D5A9C1AF0 ** get_address_of___valueChangedHandlerDelegate_107() { return &_____valueChangedHandlerDelegate_107; }
	inline void set___valueChangedHandlerDelegate_107(EventFunction_2_t0A48F4F300E24FBDB69326E87FD1939D5A9C1AF0 * value)
	{
		_____valueChangedHandlerDelegate_107 = value;
		Il2CppCodeGenWriteBarrier((&_____valueChangedHandlerDelegate_107), value);
	}

	inline static int32_t get_offset_of___stickPositionChangedHandlerDelegate_108() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D_StaticFields, _____stickPositionChangedHandlerDelegate_108)); }
	inline EventFunction_2_t4604C1D8FE3FF11B06EC0FF84C1EEED688E6DFEC * get___stickPositionChangedHandlerDelegate_108() const { return _____stickPositionChangedHandlerDelegate_108; }
	inline EventFunction_2_t4604C1D8FE3FF11B06EC0FF84C1EEED688E6DFEC ** get_address_of___stickPositionChangedHandlerDelegate_108() { return &_____stickPositionChangedHandlerDelegate_108; }
	inline void set___stickPositionChangedHandlerDelegate_108(EventFunction_2_t4604C1D8FE3FF11B06EC0FF84C1EEED688E6DFEC * value)
	{
		_____stickPositionChangedHandlerDelegate_108 = value;
		Il2CppCodeGenWriteBarrier((&_____stickPositionChangedHandlerDelegate_108), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate8_109() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate8_109)); }
	inline EventFunction_2_t0A48F4F300E24FBDB69326E87FD1939D5A9C1AF0 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate8_109() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate8_109; }
	inline EventFunction_2_t0A48F4F300E24FBDB69326E87FD1939D5A9C1AF0 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate8_109() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate8_109; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate8_109(EventFunction_2_t0A48F4F300E24FBDB69326E87FD1939D5A9C1AF0 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate8_109 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegate8_109), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatea_110() { return static_cast<int32_t>(offsetof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegatea_110)); }
	inline EventFunction_2_t4604C1D8FE3FF11B06EC0FF84C1EEED688E6DFEC * get_CSU24U3CU3E9__CachedAnonymousMethodDelegatea_110() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegatea_110; }
	inline EventFunction_2_t4604C1D8FE3FF11B06EC0FF84C1EEED688E6DFEC ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatea_110() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegatea_110; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegatea_110(EventFunction_2_t4604C1D8FE3FF11B06EC0FF84C1EEED688E6DFEC * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegatea_110 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E9__CachedAnonymousMethodDelegatea_110), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHJOYSTICK_T7D003265D5469EE86A9E1609771F8362EE1F703D_H
#ifndef TOUCHPAD_T129C582C0CDA2EEE97C17F8A94F61930086089C9_H
#define TOUCHPAD_T129C582C0CDA2EEE97C17F8A94F61930086089C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchPad
struct  TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9  : public TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8
{
public:
	// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat Rewired.ComponentControls.TouchPad::_horizontalAxisCustomControllerElement
	CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * ____horizontalAxisCustomControllerElement_44;
	// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForFloat Rewired.ComponentControls.TouchPad::_verticalAxisCustomControllerElement
	CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * ____verticalAxisCustomControllerElement_45;
	// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForBoolean Rewired.ComponentControls.TouchPad::_tapCustomControllerElement
	CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54 * ____tapCustomControllerElement_46;
	// Rewired.ComponentControls.Data.CustomControllerElementTargetSetForBoolean Rewired.ComponentControls.TouchPad::_pressCustomControllerElement
	CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54 * ____pressCustomControllerElement_47;
	// Rewired.ComponentControls.TouchPad_AxisDirection Rewired.ComponentControls.TouchPad::_axesToUse
	int32_t ____axesToUse_48;
	// Rewired.ComponentControls.TouchPad_TouchPadMode Rewired.ComponentControls.TouchPad::_touchPadMode
	int32_t ____touchPadMode_49;
	// Rewired.ComponentControls.TouchPad_ValueFormat Rewired.ComponentControls.TouchPad::_valueFormat
	int32_t ____valueFormat_50;
	// System.Boolean Rewired.ComponentControls.TouchPad::_useInertia
	bool ____useInertia_51;
	// System.Single Rewired.ComponentControls.TouchPad::_inertiaFriction
	float ____inertiaFriction_52;
	// System.Boolean Rewired.ComponentControls.TouchPad::_activateOnSwipeIn
	bool ____activateOnSwipeIn_53;
	// System.Boolean Rewired.ComponentControls.TouchPad::_stayActiveOnSwipeOut
	bool ____stayActiveOnSwipeOut_54;
	// System.Boolean Rewired.ComponentControls.TouchPad::_allowTap
	bool ____allowTap_55;
	// System.Single Rewired.ComponentControls.TouchPad::_tapTimeout
	float ____tapTimeout_56;
	// System.Int32 Rewired.ComponentControls.TouchPad::_tapDistanceLimit
	int32_t ____tapDistanceLimit_57;
	// System.Boolean Rewired.ComponentControls.TouchPad::_allowPress
	bool ____allowPress_58;
	// System.Single Rewired.ComponentControls.TouchPad::_pressStartDelay
	float ____pressStartDelay_59;
	// System.Int32 Rewired.ComponentControls.TouchPad::_pressDistanceLimit
	int32_t ____pressDistanceLimit_60;
	// System.Boolean Rewired.ComponentControls.TouchPad::_hideAtRuntime
	bool ____hideAtRuntime_61;
	// Rewired.Internal.StandaloneAxis2D Rewired.ComponentControls.TouchPad::_axis2D
	StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A * ____axis2D_62;
	// Rewired.ComponentControls.TouchPad_ValueChangedEventHandler Rewired.ComponentControls.TouchPad::_onValueChanged
	ValueChangedEventHandler_t5D816B57AB2EEC7ACA0BE8CFB398EE5EDD7E32C4 * ____onValueChanged_63;
	// Rewired.ComponentControls.TouchPad_TapEventHandler Rewired.ComponentControls.TouchPad::_onTap
	TapEventHandler_t33FC3FB8B60BF60104F3569F86657ADDB6BC0944 * ____onTap_64;
	// Rewired.ComponentControls.TouchPad_PressDownEventHandler Rewired.ComponentControls.TouchPad::_onPressDown
	PressDownEventHandler_tADC629B1DD822026CE2E06E3CDA7FB6D4E495F8D * ____onPressDown_65;
	// Rewired.ComponentControls.TouchPad_PressUpEventHandler Rewired.ComponentControls.TouchPad::_onPressUp
	PressUpEventHandler_t4125670E8C4E9682843D4F1D030ACF428FD397EC * ____onPressUp_66;
	// System.Boolean Rewired.ComponentControls.TouchPad::_useXAxis
	bool ____useXAxis_67;
	// System.Boolean Rewired.ComponentControls.TouchPad::_useYAxis
	bool ____useYAxis_68;
	// System.Int32 Rewired.ComponentControls.TouchPad::_pointerId
	int32_t ____pointerId_69;
	// System.Int32 Rewired.ComponentControls.TouchPad::_realMousePointerId
	int32_t ____realMousePointerId_70;
	// System.Boolean Rewired.ComponentControls.TouchPad::UsFAvRrPreZngsJiAjbwyGLdcub
	bool ___UsFAvRrPreZngsJiAjbwyGLdcub_71;
	// System.Boolean Rewired.ComponentControls.TouchPad::pPGULjslgtAiohyNMfEalDGgOVl
	bool ___pPGULjslgtAiohyNMfEalDGgOVl_72;
	// System.Boolean Rewired.ComponentControls.TouchPad::_pointerDownIsFake
	bool ____pointerDownIsFake_73;
	// UnityEngine.Vector2 Rewired.ComponentControls.TouchPad::_touchStartPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____touchStartPosition_74;
	// System.Single Rewired.ComponentControls.TouchPad::_touchStartTime
	float ____touchStartTime_75;
	// UnityEngine.Vector3 Rewired.ComponentControls.TouchPad::_currentCenter
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ____currentCenter_76;
	// UnityEngine.Vector2 Rewired.ComponentControls.TouchPad::_previousTouchPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____previousTouchPosition_77;
	// System.Int32 Rewired.ComponentControls.TouchPad::_lastTapFrame
	int32_t ____lastTapFrame_78;
	// System.Boolean Rewired.ComponentControls.TouchPad::_isEligibleForTap
	bool ____isEligibleForTap_79;
	// System.Boolean Rewired.ComponentControls.TouchPad::_isEligibleForPress
	bool ____isEligibleForPress_80;
	// System.Boolean Rewired.ComponentControls.TouchPad::_pressValue
	bool ____pressValue_81;
	// Rewired.ComponentControls.TouchPad_IftMAPzfkVAPWSFrTxsXPBYDanRJ Rewired.ComponentControls.TouchPad::_smoothDelta
	IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA * ____smoothDelta_82;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> Rewired.ComponentControls.TouchPad::__fakePointerEventData
	Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF * _____fakePointerEventData_83;

public:
	inline static int32_t get_offset_of__horizontalAxisCustomControllerElement_44() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____horizontalAxisCustomControllerElement_44)); }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * get__horizontalAxisCustomControllerElement_44() const { return ____horizontalAxisCustomControllerElement_44; }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 ** get_address_of__horizontalAxisCustomControllerElement_44() { return &____horizontalAxisCustomControllerElement_44; }
	inline void set__horizontalAxisCustomControllerElement_44(CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * value)
	{
		____horizontalAxisCustomControllerElement_44 = value;
		Il2CppCodeGenWriteBarrier((&____horizontalAxisCustomControllerElement_44), value);
	}

	inline static int32_t get_offset_of__verticalAxisCustomControllerElement_45() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____verticalAxisCustomControllerElement_45)); }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * get__verticalAxisCustomControllerElement_45() const { return ____verticalAxisCustomControllerElement_45; }
	inline CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 ** get_address_of__verticalAxisCustomControllerElement_45() { return &____verticalAxisCustomControllerElement_45; }
	inline void set__verticalAxisCustomControllerElement_45(CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4 * value)
	{
		____verticalAxisCustomControllerElement_45 = value;
		Il2CppCodeGenWriteBarrier((&____verticalAxisCustomControllerElement_45), value);
	}

	inline static int32_t get_offset_of__tapCustomControllerElement_46() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____tapCustomControllerElement_46)); }
	inline CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54 * get__tapCustomControllerElement_46() const { return ____tapCustomControllerElement_46; }
	inline CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54 ** get_address_of__tapCustomControllerElement_46() { return &____tapCustomControllerElement_46; }
	inline void set__tapCustomControllerElement_46(CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54 * value)
	{
		____tapCustomControllerElement_46 = value;
		Il2CppCodeGenWriteBarrier((&____tapCustomControllerElement_46), value);
	}

	inline static int32_t get_offset_of__pressCustomControllerElement_47() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____pressCustomControllerElement_47)); }
	inline CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54 * get__pressCustomControllerElement_47() const { return ____pressCustomControllerElement_47; }
	inline CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54 ** get_address_of__pressCustomControllerElement_47() { return &____pressCustomControllerElement_47; }
	inline void set__pressCustomControllerElement_47(CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54 * value)
	{
		____pressCustomControllerElement_47 = value;
		Il2CppCodeGenWriteBarrier((&____pressCustomControllerElement_47), value);
	}

	inline static int32_t get_offset_of__axesToUse_48() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____axesToUse_48)); }
	inline int32_t get__axesToUse_48() const { return ____axesToUse_48; }
	inline int32_t* get_address_of__axesToUse_48() { return &____axesToUse_48; }
	inline void set__axesToUse_48(int32_t value)
	{
		____axesToUse_48 = value;
	}

	inline static int32_t get_offset_of__touchPadMode_49() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____touchPadMode_49)); }
	inline int32_t get__touchPadMode_49() const { return ____touchPadMode_49; }
	inline int32_t* get_address_of__touchPadMode_49() { return &____touchPadMode_49; }
	inline void set__touchPadMode_49(int32_t value)
	{
		____touchPadMode_49 = value;
	}

	inline static int32_t get_offset_of__valueFormat_50() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____valueFormat_50)); }
	inline int32_t get__valueFormat_50() const { return ____valueFormat_50; }
	inline int32_t* get_address_of__valueFormat_50() { return &____valueFormat_50; }
	inline void set__valueFormat_50(int32_t value)
	{
		____valueFormat_50 = value;
	}

	inline static int32_t get_offset_of__useInertia_51() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____useInertia_51)); }
	inline bool get__useInertia_51() const { return ____useInertia_51; }
	inline bool* get_address_of__useInertia_51() { return &____useInertia_51; }
	inline void set__useInertia_51(bool value)
	{
		____useInertia_51 = value;
	}

	inline static int32_t get_offset_of__inertiaFriction_52() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____inertiaFriction_52)); }
	inline float get__inertiaFriction_52() const { return ____inertiaFriction_52; }
	inline float* get_address_of__inertiaFriction_52() { return &____inertiaFriction_52; }
	inline void set__inertiaFriction_52(float value)
	{
		____inertiaFriction_52 = value;
	}

	inline static int32_t get_offset_of__activateOnSwipeIn_53() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____activateOnSwipeIn_53)); }
	inline bool get__activateOnSwipeIn_53() const { return ____activateOnSwipeIn_53; }
	inline bool* get_address_of__activateOnSwipeIn_53() { return &____activateOnSwipeIn_53; }
	inline void set__activateOnSwipeIn_53(bool value)
	{
		____activateOnSwipeIn_53 = value;
	}

	inline static int32_t get_offset_of__stayActiveOnSwipeOut_54() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____stayActiveOnSwipeOut_54)); }
	inline bool get__stayActiveOnSwipeOut_54() const { return ____stayActiveOnSwipeOut_54; }
	inline bool* get_address_of__stayActiveOnSwipeOut_54() { return &____stayActiveOnSwipeOut_54; }
	inline void set__stayActiveOnSwipeOut_54(bool value)
	{
		____stayActiveOnSwipeOut_54 = value;
	}

	inline static int32_t get_offset_of__allowTap_55() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____allowTap_55)); }
	inline bool get__allowTap_55() const { return ____allowTap_55; }
	inline bool* get_address_of__allowTap_55() { return &____allowTap_55; }
	inline void set__allowTap_55(bool value)
	{
		____allowTap_55 = value;
	}

	inline static int32_t get_offset_of__tapTimeout_56() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____tapTimeout_56)); }
	inline float get__tapTimeout_56() const { return ____tapTimeout_56; }
	inline float* get_address_of__tapTimeout_56() { return &____tapTimeout_56; }
	inline void set__tapTimeout_56(float value)
	{
		____tapTimeout_56 = value;
	}

	inline static int32_t get_offset_of__tapDistanceLimit_57() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____tapDistanceLimit_57)); }
	inline int32_t get__tapDistanceLimit_57() const { return ____tapDistanceLimit_57; }
	inline int32_t* get_address_of__tapDistanceLimit_57() { return &____tapDistanceLimit_57; }
	inline void set__tapDistanceLimit_57(int32_t value)
	{
		____tapDistanceLimit_57 = value;
	}

	inline static int32_t get_offset_of__allowPress_58() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____allowPress_58)); }
	inline bool get__allowPress_58() const { return ____allowPress_58; }
	inline bool* get_address_of__allowPress_58() { return &____allowPress_58; }
	inline void set__allowPress_58(bool value)
	{
		____allowPress_58 = value;
	}

	inline static int32_t get_offset_of__pressStartDelay_59() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____pressStartDelay_59)); }
	inline float get__pressStartDelay_59() const { return ____pressStartDelay_59; }
	inline float* get_address_of__pressStartDelay_59() { return &____pressStartDelay_59; }
	inline void set__pressStartDelay_59(float value)
	{
		____pressStartDelay_59 = value;
	}

	inline static int32_t get_offset_of__pressDistanceLimit_60() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____pressDistanceLimit_60)); }
	inline int32_t get__pressDistanceLimit_60() const { return ____pressDistanceLimit_60; }
	inline int32_t* get_address_of__pressDistanceLimit_60() { return &____pressDistanceLimit_60; }
	inline void set__pressDistanceLimit_60(int32_t value)
	{
		____pressDistanceLimit_60 = value;
	}

	inline static int32_t get_offset_of__hideAtRuntime_61() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____hideAtRuntime_61)); }
	inline bool get__hideAtRuntime_61() const { return ____hideAtRuntime_61; }
	inline bool* get_address_of__hideAtRuntime_61() { return &____hideAtRuntime_61; }
	inline void set__hideAtRuntime_61(bool value)
	{
		____hideAtRuntime_61 = value;
	}

	inline static int32_t get_offset_of__axis2D_62() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____axis2D_62)); }
	inline StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A * get__axis2D_62() const { return ____axis2D_62; }
	inline StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A ** get_address_of__axis2D_62() { return &____axis2D_62; }
	inline void set__axis2D_62(StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A * value)
	{
		____axis2D_62 = value;
		Il2CppCodeGenWriteBarrier((&____axis2D_62), value);
	}

	inline static int32_t get_offset_of__onValueChanged_63() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____onValueChanged_63)); }
	inline ValueChangedEventHandler_t5D816B57AB2EEC7ACA0BE8CFB398EE5EDD7E32C4 * get__onValueChanged_63() const { return ____onValueChanged_63; }
	inline ValueChangedEventHandler_t5D816B57AB2EEC7ACA0BE8CFB398EE5EDD7E32C4 ** get_address_of__onValueChanged_63() { return &____onValueChanged_63; }
	inline void set__onValueChanged_63(ValueChangedEventHandler_t5D816B57AB2EEC7ACA0BE8CFB398EE5EDD7E32C4 * value)
	{
		____onValueChanged_63 = value;
		Il2CppCodeGenWriteBarrier((&____onValueChanged_63), value);
	}

	inline static int32_t get_offset_of__onTap_64() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____onTap_64)); }
	inline TapEventHandler_t33FC3FB8B60BF60104F3569F86657ADDB6BC0944 * get__onTap_64() const { return ____onTap_64; }
	inline TapEventHandler_t33FC3FB8B60BF60104F3569F86657ADDB6BC0944 ** get_address_of__onTap_64() { return &____onTap_64; }
	inline void set__onTap_64(TapEventHandler_t33FC3FB8B60BF60104F3569F86657ADDB6BC0944 * value)
	{
		____onTap_64 = value;
		Il2CppCodeGenWriteBarrier((&____onTap_64), value);
	}

	inline static int32_t get_offset_of__onPressDown_65() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____onPressDown_65)); }
	inline PressDownEventHandler_tADC629B1DD822026CE2E06E3CDA7FB6D4E495F8D * get__onPressDown_65() const { return ____onPressDown_65; }
	inline PressDownEventHandler_tADC629B1DD822026CE2E06E3CDA7FB6D4E495F8D ** get_address_of__onPressDown_65() { return &____onPressDown_65; }
	inline void set__onPressDown_65(PressDownEventHandler_tADC629B1DD822026CE2E06E3CDA7FB6D4E495F8D * value)
	{
		____onPressDown_65 = value;
		Il2CppCodeGenWriteBarrier((&____onPressDown_65), value);
	}

	inline static int32_t get_offset_of__onPressUp_66() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____onPressUp_66)); }
	inline PressUpEventHandler_t4125670E8C4E9682843D4F1D030ACF428FD397EC * get__onPressUp_66() const { return ____onPressUp_66; }
	inline PressUpEventHandler_t4125670E8C4E9682843D4F1D030ACF428FD397EC ** get_address_of__onPressUp_66() { return &____onPressUp_66; }
	inline void set__onPressUp_66(PressUpEventHandler_t4125670E8C4E9682843D4F1D030ACF428FD397EC * value)
	{
		____onPressUp_66 = value;
		Il2CppCodeGenWriteBarrier((&____onPressUp_66), value);
	}

	inline static int32_t get_offset_of__useXAxis_67() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____useXAxis_67)); }
	inline bool get__useXAxis_67() const { return ____useXAxis_67; }
	inline bool* get_address_of__useXAxis_67() { return &____useXAxis_67; }
	inline void set__useXAxis_67(bool value)
	{
		____useXAxis_67 = value;
	}

	inline static int32_t get_offset_of__useYAxis_68() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____useYAxis_68)); }
	inline bool get__useYAxis_68() const { return ____useYAxis_68; }
	inline bool* get_address_of__useYAxis_68() { return &____useYAxis_68; }
	inline void set__useYAxis_68(bool value)
	{
		____useYAxis_68 = value;
	}

	inline static int32_t get_offset_of__pointerId_69() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____pointerId_69)); }
	inline int32_t get__pointerId_69() const { return ____pointerId_69; }
	inline int32_t* get_address_of__pointerId_69() { return &____pointerId_69; }
	inline void set__pointerId_69(int32_t value)
	{
		____pointerId_69 = value;
	}

	inline static int32_t get_offset_of__realMousePointerId_70() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____realMousePointerId_70)); }
	inline int32_t get__realMousePointerId_70() const { return ____realMousePointerId_70; }
	inline int32_t* get_address_of__realMousePointerId_70() { return &____realMousePointerId_70; }
	inline void set__realMousePointerId_70(int32_t value)
	{
		____realMousePointerId_70 = value;
	}

	inline static int32_t get_offset_of_UsFAvRrPreZngsJiAjbwyGLdcub_71() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ___UsFAvRrPreZngsJiAjbwyGLdcub_71)); }
	inline bool get_UsFAvRrPreZngsJiAjbwyGLdcub_71() const { return ___UsFAvRrPreZngsJiAjbwyGLdcub_71; }
	inline bool* get_address_of_UsFAvRrPreZngsJiAjbwyGLdcub_71() { return &___UsFAvRrPreZngsJiAjbwyGLdcub_71; }
	inline void set_UsFAvRrPreZngsJiAjbwyGLdcub_71(bool value)
	{
		___UsFAvRrPreZngsJiAjbwyGLdcub_71 = value;
	}

	inline static int32_t get_offset_of_pPGULjslgtAiohyNMfEalDGgOVl_72() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ___pPGULjslgtAiohyNMfEalDGgOVl_72)); }
	inline bool get_pPGULjslgtAiohyNMfEalDGgOVl_72() const { return ___pPGULjslgtAiohyNMfEalDGgOVl_72; }
	inline bool* get_address_of_pPGULjslgtAiohyNMfEalDGgOVl_72() { return &___pPGULjslgtAiohyNMfEalDGgOVl_72; }
	inline void set_pPGULjslgtAiohyNMfEalDGgOVl_72(bool value)
	{
		___pPGULjslgtAiohyNMfEalDGgOVl_72 = value;
	}

	inline static int32_t get_offset_of__pointerDownIsFake_73() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____pointerDownIsFake_73)); }
	inline bool get__pointerDownIsFake_73() const { return ____pointerDownIsFake_73; }
	inline bool* get_address_of__pointerDownIsFake_73() { return &____pointerDownIsFake_73; }
	inline void set__pointerDownIsFake_73(bool value)
	{
		____pointerDownIsFake_73 = value;
	}

	inline static int32_t get_offset_of__touchStartPosition_74() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____touchStartPosition_74)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__touchStartPosition_74() const { return ____touchStartPosition_74; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__touchStartPosition_74() { return &____touchStartPosition_74; }
	inline void set__touchStartPosition_74(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____touchStartPosition_74 = value;
	}

	inline static int32_t get_offset_of__touchStartTime_75() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____touchStartTime_75)); }
	inline float get__touchStartTime_75() const { return ____touchStartTime_75; }
	inline float* get_address_of__touchStartTime_75() { return &____touchStartTime_75; }
	inline void set__touchStartTime_75(float value)
	{
		____touchStartTime_75 = value;
	}

	inline static int32_t get_offset_of__currentCenter_76() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____currentCenter_76)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get__currentCenter_76() const { return ____currentCenter_76; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of__currentCenter_76() { return &____currentCenter_76; }
	inline void set__currentCenter_76(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		____currentCenter_76 = value;
	}

	inline static int32_t get_offset_of__previousTouchPosition_77() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____previousTouchPosition_77)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__previousTouchPosition_77() const { return ____previousTouchPosition_77; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__previousTouchPosition_77() { return &____previousTouchPosition_77; }
	inline void set__previousTouchPosition_77(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____previousTouchPosition_77 = value;
	}

	inline static int32_t get_offset_of__lastTapFrame_78() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____lastTapFrame_78)); }
	inline int32_t get__lastTapFrame_78() const { return ____lastTapFrame_78; }
	inline int32_t* get_address_of__lastTapFrame_78() { return &____lastTapFrame_78; }
	inline void set__lastTapFrame_78(int32_t value)
	{
		____lastTapFrame_78 = value;
	}

	inline static int32_t get_offset_of__isEligibleForTap_79() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____isEligibleForTap_79)); }
	inline bool get__isEligibleForTap_79() const { return ____isEligibleForTap_79; }
	inline bool* get_address_of__isEligibleForTap_79() { return &____isEligibleForTap_79; }
	inline void set__isEligibleForTap_79(bool value)
	{
		____isEligibleForTap_79 = value;
	}

	inline static int32_t get_offset_of__isEligibleForPress_80() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____isEligibleForPress_80)); }
	inline bool get__isEligibleForPress_80() const { return ____isEligibleForPress_80; }
	inline bool* get_address_of__isEligibleForPress_80() { return &____isEligibleForPress_80; }
	inline void set__isEligibleForPress_80(bool value)
	{
		____isEligibleForPress_80 = value;
	}

	inline static int32_t get_offset_of__pressValue_81() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____pressValue_81)); }
	inline bool get__pressValue_81() const { return ____pressValue_81; }
	inline bool* get_address_of__pressValue_81() { return &____pressValue_81; }
	inline void set__pressValue_81(bool value)
	{
		____pressValue_81 = value;
	}

	inline static int32_t get_offset_of__smoothDelta_82() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, ____smoothDelta_82)); }
	inline IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA * get__smoothDelta_82() const { return ____smoothDelta_82; }
	inline IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA ** get_address_of__smoothDelta_82() { return &____smoothDelta_82; }
	inline void set__smoothDelta_82(IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA * value)
	{
		____smoothDelta_82 = value;
		Il2CppCodeGenWriteBarrier((&____smoothDelta_82), value);
	}

	inline static int32_t get_offset_of___fakePointerEventData_83() { return static_cast<int32_t>(offsetof(TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9, _____fakePointerEventData_83)); }
	inline Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF * get___fakePointerEventData_83() const { return _____fakePointerEventData_83; }
	inline Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF ** get_address_of___fakePointerEventData_83() { return &_____fakePointerEventData_83; }
	inline void set___fakePointerEventData_83(Dictionary_2_t4DD8490EB900C82E89E3C456A8DA6A741801BDEF * value)
	{
		_____fakePointerEventData_83 = value;
		Il2CppCodeGenWriteBarrier((&_____fakePointerEventData_83), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_T129C582C0CDA2EEE97C17F8A94F61930086089C9_H
#ifndef TOUCHREGION_TF116E3AF6B3D62ECC010A44548C846348638DF49_H
#define TOUCHREGION_TF116E3AF6B3D62ECC010A44548C846348638DF49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ComponentControls.TouchRegion
struct  TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49  : public TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8
{
public:
	// System.Boolean Rewired.ComponentControls.TouchRegion::_hideAtRuntime
	bool ____hideAtRuntime_43;
	// Rewired.ComponentControls.TouchRegion_gWVGylYAAhiNGOqYxKsBnzSeJJp Rewired.ComponentControls.TouchRegion::_onPointerDown
	gWVGylYAAhiNGOqYxKsBnzSeJJp_t698C187A9425952910BAA66C4096E5CECF9C45C4 * ____onPointerDown_44;
	// Rewired.ComponentControls.TouchRegion_jOGBgBryHTPCrwAWbtpjcyBVWgH Rewired.ComponentControls.TouchRegion::_onPointerUp
	jOGBgBryHTPCrwAWbtpjcyBVWgH_t60095498B6581E7071A1B57B00B2727697AE5892 * ____onPointerUp_45;
	// Rewired.ComponentControls.TouchRegion_CUBRkEgPtmqloePiGjPGzvftMEz Rewired.ComponentControls.TouchRegion::_onPointerEnter
	CUBRkEgPtmqloePiGjPGzvftMEz_t62B6522D584CD2A21FCD80D3C3CAED97E95E8D13 * ____onPointerEnter_46;
	// Rewired.ComponentControls.TouchRegion_itdXQhVbgPjNuvzzTkqLUeNdGDO Rewired.ComponentControls.TouchRegion::_onPointerExit
	itdXQhVbgPjNuvzzTkqLUeNdGDO_t9686109095347771CDEF55C86FA148EFB32C1144 * ____onPointerExit_47;
	// Rewired.ComponentControls.TouchRegion_pSrmmveUVOWIyFBwbhMaEmScisy Rewired.ComponentControls.TouchRegion::_onBeginDrag
	pSrmmveUVOWIyFBwbhMaEmScisy_t4B47DB2624A04C0C6B1093B289477A5A3506100B * ____onBeginDrag_48;
	// Rewired.ComponentControls.TouchRegion_DbjAHtCsNuwTcsSMKOqHaDEgpfX Rewired.ComponentControls.TouchRegion::_onDrag
	DbjAHtCsNuwTcsSMKOqHaDEgpfX_t1824811CE094ED8DE5F842353B1C13B05116152D * ____onDrag_49;
	// Rewired.ComponentControls.TouchRegion_SpNSxRoDgiUWSAIfKMmMQPNAbEH Rewired.ComponentControls.TouchRegion::_onEndDrag
	SpNSxRoDgiUWSAIfKMmMQPNAbEH_tC64B3B02AA7E79A9CAE0BA33FE4F96AC5E5405A1 * ____onEndDrag_50;

public:
	inline static int32_t get_offset_of__hideAtRuntime_43() { return static_cast<int32_t>(offsetof(TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49, ____hideAtRuntime_43)); }
	inline bool get__hideAtRuntime_43() const { return ____hideAtRuntime_43; }
	inline bool* get_address_of__hideAtRuntime_43() { return &____hideAtRuntime_43; }
	inline void set__hideAtRuntime_43(bool value)
	{
		____hideAtRuntime_43 = value;
	}

	inline static int32_t get_offset_of__onPointerDown_44() { return static_cast<int32_t>(offsetof(TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49, ____onPointerDown_44)); }
	inline gWVGylYAAhiNGOqYxKsBnzSeJJp_t698C187A9425952910BAA66C4096E5CECF9C45C4 * get__onPointerDown_44() const { return ____onPointerDown_44; }
	inline gWVGylYAAhiNGOqYxKsBnzSeJJp_t698C187A9425952910BAA66C4096E5CECF9C45C4 ** get_address_of__onPointerDown_44() { return &____onPointerDown_44; }
	inline void set__onPointerDown_44(gWVGylYAAhiNGOqYxKsBnzSeJJp_t698C187A9425952910BAA66C4096E5CECF9C45C4 * value)
	{
		____onPointerDown_44 = value;
		Il2CppCodeGenWriteBarrier((&____onPointerDown_44), value);
	}

	inline static int32_t get_offset_of__onPointerUp_45() { return static_cast<int32_t>(offsetof(TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49, ____onPointerUp_45)); }
	inline jOGBgBryHTPCrwAWbtpjcyBVWgH_t60095498B6581E7071A1B57B00B2727697AE5892 * get__onPointerUp_45() const { return ____onPointerUp_45; }
	inline jOGBgBryHTPCrwAWbtpjcyBVWgH_t60095498B6581E7071A1B57B00B2727697AE5892 ** get_address_of__onPointerUp_45() { return &____onPointerUp_45; }
	inline void set__onPointerUp_45(jOGBgBryHTPCrwAWbtpjcyBVWgH_t60095498B6581E7071A1B57B00B2727697AE5892 * value)
	{
		____onPointerUp_45 = value;
		Il2CppCodeGenWriteBarrier((&____onPointerUp_45), value);
	}

	inline static int32_t get_offset_of__onPointerEnter_46() { return static_cast<int32_t>(offsetof(TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49, ____onPointerEnter_46)); }
	inline CUBRkEgPtmqloePiGjPGzvftMEz_t62B6522D584CD2A21FCD80D3C3CAED97E95E8D13 * get__onPointerEnter_46() const { return ____onPointerEnter_46; }
	inline CUBRkEgPtmqloePiGjPGzvftMEz_t62B6522D584CD2A21FCD80D3C3CAED97E95E8D13 ** get_address_of__onPointerEnter_46() { return &____onPointerEnter_46; }
	inline void set__onPointerEnter_46(CUBRkEgPtmqloePiGjPGzvftMEz_t62B6522D584CD2A21FCD80D3C3CAED97E95E8D13 * value)
	{
		____onPointerEnter_46 = value;
		Il2CppCodeGenWriteBarrier((&____onPointerEnter_46), value);
	}

	inline static int32_t get_offset_of__onPointerExit_47() { return static_cast<int32_t>(offsetof(TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49, ____onPointerExit_47)); }
	inline itdXQhVbgPjNuvzzTkqLUeNdGDO_t9686109095347771CDEF55C86FA148EFB32C1144 * get__onPointerExit_47() const { return ____onPointerExit_47; }
	inline itdXQhVbgPjNuvzzTkqLUeNdGDO_t9686109095347771CDEF55C86FA148EFB32C1144 ** get_address_of__onPointerExit_47() { return &____onPointerExit_47; }
	inline void set__onPointerExit_47(itdXQhVbgPjNuvzzTkqLUeNdGDO_t9686109095347771CDEF55C86FA148EFB32C1144 * value)
	{
		____onPointerExit_47 = value;
		Il2CppCodeGenWriteBarrier((&____onPointerExit_47), value);
	}

	inline static int32_t get_offset_of__onBeginDrag_48() { return static_cast<int32_t>(offsetof(TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49, ____onBeginDrag_48)); }
	inline pSrmmveUVOWIyFBwbhMaEmScisy_t4B47DB2624A04C0C6B1093B289477A5A3506100B * get__onBeginDrag_48() const { return ____onBeginDrag_48; }
	inline pSrmmveUVOWIyFBwbhMaEmScisy_t4B47DB2624A04C0C6B1093B289477A5A3506100B ** get_address_of__onBeginDrag_48() { return &____onBeginDrag_48; }
	inline void set__onBeginDrag_48(pSrmmveUVOWIyFBwbhMaEmScisy_t4B47DB2624A04C0C6B1093B289477A5A3506100B * value)
	{
		____onBeginDrag_48 = value;
		Il2CppCodeGenWriteBarrier((&____onBeginDrag_48), value);
	}

	inline static int32_t get_offset_of__onDrag_49() { return static_cast<int32_t>(offsetof(TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49, ____onDrag_49)); }
	inline DbjAHtCsNuwTcsSMKOqHaDEgpfX_t1824811CE094ED8DE5F842353B1C13B05116152D * get__onDrag_49() const { return ____onDrag_49; }
	inline DbjAHtCsNuwTcsSMKOqHaDEgpfX_t1824811CE094ED8DE5F842353B1C13B05116152D ** get_address_of__onDrag_49() { return &____onDrag_49; }
	inline void set__onDrag_49(DbjAHtCsNuwTcsSMKOqHaDEgpfX_t1824811CE094ED8DE5F842353B1C13B05116152D * value)
	{
		____onDrag_49 = value;
		Il2CppCodeGenWriteBarrier((&____onDrag_49), value);
	}

	inline static int32_t get_offset_of__onEndDrag_50() { return static_cast<int32_t>(offsetof(TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49, ____onEndDrag_50)); }
	inline SpNSxRoDgiUWSAIfKMmMQPNAbEH_tC64B3B02AA7E79A9CAE0BA33FE4F96AC5E5405A1 * get__onEndDrag_50() const { return ____onEndDrag_50; }
	inline SpNSxRoDgiUWSAIfKMmMQPNAbEH_tC64B3B02AA7E79A9CAE0BA33FE4F96AC5E5405A1 ** get_address_of__onEndDrag_50() { return &____onEndDrag_50; }
	inline void set__onEndDrag_50(SpNSxRoDgiUWSAIfKMmMQPNAbEH_tC64B3B02AA7E79A9CAE0BA33FE4F96AC5E5405A1 * value)
	{
		____onEndDrag_50 = value;
		Il2CppCodeGenWriteBarrier((&____onEndDrag_50), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHREGION_TF116E3AF6B3D62ECC010A44548C846348638DF49_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70), -1, sizeof(Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3900[15] = 
{
	0,
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields::get_offset_of_keyValueIndex_Escape_1(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields::get_offset_of_keyValueIndex_Menu_2(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields::get_offset_of_keyValueIndex_F2_3(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields::get_offset_of_keyValueIndex_UpArrow_4(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields::get_offset_of_keyValueIndex_RightArrow_5(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields::get_offset_of_keyValueIndex_DownArrow_6(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields::get_offset_of_keyValueIndex_LeftArrow_7(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70_StaticFields::get_offset_of_dKLIekLYibeGYzfFWuESydKiWWV_8(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70::get_offset_of_uPLqzGXVtmvPTALbArYgcyquMVm_9(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70::get_offset_of_ubaErReAqjXCjFIOlvvEVepdvAK_10(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70::get_offset_of_mJnuxAXBqddLsUBdtgPYHBVNIjI_11(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70::get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_12(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70::get_offset_of_sWuSAvTOAkQhUHhzaEDbcPtXwDB_13(),
	Keyboard_tF30826194FADE14B54A1B21EE260E4A572AFAC70::get_offset_of_uFBUjqesoWEDylgvLnqGzSXhYov_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3901[7] = 
{
	0,
	0,
	Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9::get_offset_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_2(),
	Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9::get_offset_of_xwGjMDxksyYaKvqOwHfPiiWNyUq_3(),
	Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9::get_offset_of_sWuSAvTOAkQhUHhzaEDbcPtXwDB_4(),
	Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9::get_offset_of_kZqclMjeSxqHcUjffDBtNDosmXf_5(),
	Mouse_tF43362A0D8B9780699E590990FCA13DB7BE0DFE9::get_offset_of_vsYPZQUSwLnGSZrMOhSMXUSVmhx_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3902[4] = 
{
	cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E::get_offset_of_AToGpwTZfcvfXzMlTfKzGBuSBgl_0(),
	cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E::get_offset_of_lGeWrhwdeiuBPdUWuRrvXnmGcKs_1(),
	cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E::get_offset_of_plyGQmxYKBmkxENtzEuLgCvVCrVr_2(),
	cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E::get_offset_of_jjvwgcrQiCfhWMGXqaTYYuZLQV_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { sizeof (UIAnchor_t42FBDDDE4E7588246ADA3B042CA1DF9AB59DCEE7)+ sizeof (RuntimeObject), sizeof(UIAnchor_t42FBDDDE4E7588246ADA3B042CA1DF9AB59DCEE7 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3903[2] = 
{
	UIAnchor_t42FBDDDE4E7588246ADA3B042CA1DF9AB59DCEE7::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIAnchor_t42FBDDDE4E7588246ADA3B042CA1DF9AB59DCEE7::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { sizeof (UIPivot_t80B7F0A2AF176A00B9F3DFDBF2CBA21753452CD8)+ sizeof (RuntimeObject), sizeof(UIPivot_t80B7F0A2AF176A00B9F3DFDBF2CBA21753452CD8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3904[2] = 
{
	UIPivot_t80B7F0A2AF176A00B9F3DFDBF2CBA21753452CD8::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIPivot_t80B7F0A2AF176A00B9F3DFDBF2CBA21753452CD8::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { sizeof (GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3905[21] = 
{
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_fxPyosdzuUVfIoTnoatkzaIYYJl_4(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_WnjAhrAihdtxFulBRrgxcITudfz_5(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_iKscsEulwuetlYvVYaPeaFTNJSZg_6(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_fFvHsAznMvcgKhHLgxHxQEdYLHGA_7(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_nVBbSBJrzDVLjxPpLnFWRJrqpnda_8(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_njfbYhoZSXSBcINfTcGIzZCBwGA_9(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_ReexRVDwIAoRObWPqAOJHgFFIFv_10(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_XlWchbacyFmSpmbRcmxDYFgMIbEY_11(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_XwkVKMsVgfUYwrPiIOQInqbsgTC_12(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of__pixelOffset_13(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of__useUnityUI_14(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_NMtBtkcgqEpJeDczKXGWnquCbUp_15(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_mZHeAdFbfsOSZjQmUHTYdAFVBkW_16(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_qIicGepqBUspmCvklMSTGPkPgKW_17(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_AXWGqqdBpbPPSPNwXyPAeBjKIme_18(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_MDOeNGhMUWnxCcHRFQHentJOMIzC_19(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_WaaJhTkGlowlywsKGOEygWgGAaRl_20(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_EFUqwAyKlRDHWdJuqghmZEOuYIp_21(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_oDCGxHSOOZReIUovZMkPhxDnBKL_22(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_uqDbFpLJXfOjUFlulTwwZNfYWKm_23(),
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920::get_offset_of_FwzTBmzDMooWVymcTdymLOTmSVP_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { sizeof (ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3907[4] = 
{
	ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96::get_offset_of__controller_4(),
	ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96::get_offset_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5(),
	ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96::get_offset_of_XzPgldmZGPgZguvaiWrsKWnbdEP_6(),
	ComponentControl_t712783BFAB42EB8BDA290EA3CCE00DA2392EFA96::get_offset_of__lastUpdateFrame_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { sizeof (AHFdtZblGUjhJsRybxSsRlACygYv_t264B4C72BD56C85505397A34F5F3392FB9846011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3908[3] = 
{
	AHFdtZblGUjhJsRybxSsRlACygYv_t264B4C72BD56C85505397A34F5F3392FB9846011::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	AHFdtZblGUjhJsRybxSsRlACygYv_t264B4C72BD56C85505397A34F5F3392FB9846011::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	AHFdtZblGUjhJsRybxSsRlACygYv_t264B4C72BD56C85505397A34F5F3392FB9846011::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { sizeof (ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3911[3] = 
{
	ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE::get_offset_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_4(),
	ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE::get_offset_of_XzPgldmZGPgZguvaiWrsKWnbdEP_5(),
	ComponentController_tEC306FC31850782AF316BE7AB0B90B424DC954DE::get_offset_of__controls_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { sizeof (IbtcEJkOwgzKDVZeNYnTFCLUAkwh_t291AF8978C0C4B1EFEEE5D7F0665C169CE9D8C94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3912[3] = 
{
	IbtcEJkOwgzKDVZeNYnTFCLUAkwh_t291AF8978C0C4B1EFEEE5D7F0665C169CE9D8C94::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	IbtcEJkOwgzKDVZeNYnTFCLUAkwh_t291AF8978C0C4B1EFEEE5D7F0665C169CE9D8C94::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	IbtcEJkOwgzKDVZeNYnTFCLUAkwh_t291AF8978C0C4B1EFEEE5D7F0665C169CE9D8C94::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { sizeof (CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3913[6] = 
{
	CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF::get_offset_of__rewiredInputManager_7(),
	CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF::get_offset_of__customControllerSelector_8(),
	CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF::get_offset_of__createCustomControllerSettings_9(),
	CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF::get_offset_of__inputEvents_10(),
	CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF::get_offset_of__createdCustomControllerId_11(),
	CustomController_tDA75F822EE3D9041FE381092FE88F41FE6AB7CBF::get_offset_of__InputSourceUpdateEvent_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3914[4] = 
{
	CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7::get_offset_of__createCustomController_0(),
	CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7::get_offset_of__customControllerSourceId_1(),
	CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7::get_offset_of__assignToPlayerId_2(),
	CreateCustomControllerSettings_t6156FE7402633CF3BD5BA37B832FA547671F99E7::get_offset_of__destroyCustomController_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (InputEvent_t0FF27E0A76BCCAA672F1BBC888C0284F1E7201B6)+ sizeof (RuntimeObject), sizeof(InputEvent_t0FF27E0A76BCCAA672F1BBC888C0284F1E7201B6 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3915[3] = 
{
	InputEvent_t0FF27E0A76BCCAA672F1BBC888C0284F1E7201B6::get_offset_of_elementType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputEvent_t0FF27E0A76BCCAA672F1BBC888C0284F1E7201B6::get_offset_of_elementIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputEvent_t0FF27E0A76BCCAA672F1BBC888C0284F1E7201B6::get_offset_of_value_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (CustomControllerControl_t210E15B9A7BE79A48909114F34D9BBDF75E42835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3917[4] = 
{
	CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A::get_offset_of__element_0(),
	CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A::get_offset_of__valueRange_1(),
	CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A::get_offset_of__valueContribution_2(),
	CustomControllerElementTarget_tA12266513574F9C756EF59EFE415F92CB79CCC7A::get_offset_of__invert_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (ValueRange_t87A04DAE61E9550A2F8D39BCDD5225FD6317EE2B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3918[4] = 
{
	ValueRange_t87A04DAE61E9550A2F8D39BCDD5225FD6317EE2B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (CustomControllerElementTargetSet_t5FE69FC5AAC8E3A8CD9935FEDBA55C4CB1EA091A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3920[2] = 
{
	0,
	CustomControllerElementTargetSetForBoolean_t50F1A918D17080055396560627233760C1202B54::get_offset_of__target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3921[4] = 
{
	CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4::get_offset_of__splitValue_0(),
	CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4::get_offset_of__target_1(),
	CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4::get_offset_of__positiveTarget_2(),
	CustomControllerElementTargetSetForFloat_t18EA66B22A9C8629E223232293BB2854CD1806D4::get_offset_of__negativeTarget_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3922[7] = 
{
	CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B::get_offset_of__elementType_0(),
	CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B::get_offset_of__selectorType_1(),
	CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B::get_offset_of__elementName_2(),
	CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B::get_offset_of__elementIndex_3(),
	CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B::get_offset_of__elementId_4(),
	CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B::get_offset_of_zhRZwMfsKoTPHpSKjbUhuMVqTIV_5(),
	CustomControllerElementSelector_tCDC321CFCB6F71A4CE4134E2FB64118B4078DB6B::get_offset_of_xLIaLwWCCNhQfFvghLhibpbwidU_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (ElementType_t4F552D509AC9C2D4FCD592F75CCAA294DA689BCC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3923[3] = 
{
	ElementType_t4F552D509AC9C2D4FCD592F75CCAA294DA689BCC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (SelectorType_t0A167C494778CC074B1CF92F6D624B489D934E44)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3924[4] = 
{
	SelectorType_t0A167C494778CC074B1CF92F6D624B489D934E44::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3925[6] = 
{
	CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1::get_offset_of__findUsingSourceId_0(),
	CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1::get_offset_of__sourceId_1(),
	CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1::get_offset_of__findUsingTag_2(),
	CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1::get_offset_of__tag_3(),
	CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1::get_offset_of__findInPlayer_4(),
	CustomControllerSelector_t28D12653D04C68A24DC36D72DACA77D35D486FE1::get_offset_of__playerId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3926[13] = 
{
	0,
	0,
	TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF::get_offset_of__allowedTiltDirections_10(),
	TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF::get_offset_of__horizontalTiltCustomControllerElement_11(),
	TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF::get_offset_of__horizontalTiltLimit_12(),
	TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF::get_offset_of__horizontalRestAngle_13(),
	TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF::get_offset_of__forwardTiltCustomControllerElement_14(),
	TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF::get_offset_of__forwardTiltLimit_15(),
	TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF::get_offset_of__forwardRestAngle_16(),
	TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF::get_offset_of__axis2D_17(),
	TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF::get_offset_of__useHAxis_18(),
	TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF::get_offset_of__useFAxis_19(),
	TiltControl_tD1F85A001E3238AC0969DF1FCC743EEE613235EF::get_offset_of__getAccelerationValue_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { sizeof (TiltDirection_t845536D7B05018F819DC404C83D5288B2C5F9F98)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3927[4] = 
{
	TiltDirection_t845536D7B05018F819DC404C83D5288B2C5F9F98::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3928[6] = 
{
	RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E::get_offset_of__speed_4(),
	RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E::get_offset_of__slowRotationSpeed_5(),
	RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E::get_offset_of__fastRotationSpeed_6(),
	RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E::get_offset_of__rotateAroundAxis_7(),
	RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E::get_offset_of__relativeTo_8(),
	RotateAroundAxis_t8BDEC8749D2C59079DDE5BDF99810BEE630D244E::get_offset_of__reverse_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (Speed_t302FF2DB0BAF962B98E90BF0901B05A0B0072F92)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3929[4] = 
{
	Speed_t302FF2DB0BAF962B98E90BF0901B05A0B0072F92::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (RotationAxis_t7534A0B5F153076E2CE1EBCAA5A9A636A093BE92)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3930[4] = 
{
	RotationAxis_t7534A0B5F153076E2CE1EBCAA5A9A636A093BE92::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { sizeof (TouchControl_t9BF86126F1D194D4D0E1A508ABDA37784EF365CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3932[2] = 
{
	TouchControl_t9BF86126F1D194D4D0E1A508ABDA37784EF365CC::get_offset_of__canvas_8(),
	TouchControl_t9BF86126F1D194D4D0E1A508ABDA37784EF365CC::get_offset_of___rectTransform_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { sizeof (TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8), -1, sizeof(TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3933[33] = 
{
	0,
	0,
	0,
	0,
	0,
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__interactable_15(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__visible_16(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__hideWhenIdle_17(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__allowedMouseButtons_18(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__transitionType_19(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__transitionColorTint_20(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__transitionSpriteState_21(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__transitionAnimationTriggers_22(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__targetGraphic_23(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__onInteractionStateTransition_24(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__onVisibilityChanged_25(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__onInteractionStateChangedToNormal_26(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__onInteractionStateChangedToHighlighted_27(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__onInteractionStateChangedToPressed_28(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__onInteractionStateChangedToDisabled_29(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__canvasGroupCache_30(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__groupsAllowInteraction_31(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__interactionState_32(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of_pPGULjslgtAiohyNMfEalDGgOVl_33(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of_UsFAvRrPreZngsJiAjbwyGLdcub_34(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__varWatch_visible_35(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__varWatch_interactable_36(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of__allowSendingEvents_37(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8_StaticFields::get_offset_of__transitionArgs_38(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of___hierarchyVisibilityChangedHandlers_39(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8::get_offset_of___hierarchyInteractionStateTransitionHandlers_40(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8_StaticFields::get_offset_of___interactionStateTransitionHandlerDelegate_41(),
	TouchInteractable_tD9DE3E6594439924A77020122C094D639E4BF8B8_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate4_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { sizeof (InteractionState_tF692B7740730EF1B1F625EEDA7610B14938DFED2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3934[5] = 
{
	InteractionState_tF692B7740730EF1B1F625EEDA7610B14938DFED2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { sizeof (TransitionTypeFlags_t3AEF78427B33BF979944184C868F9C98B6F39662)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3935[5] = 
{
	TransitionTypeFlags_t3AEF78427B33BF979944184C868F9C98B6F39662::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { sizeof (MouseButtonFlags_tC652D180E7354654F1CDBB43ECEA0B09A7ADC8C6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3936[6] = 
{
	MouseButtonFlags_tC652D180E7354654F1CDBB43ECEA0B09A7ADC8C6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { sizeof (InteractionStateTransitionEventHandler_tC1D00C17A8A6547002894B4718BD039679204D36), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { sizeof (VisibilityChangedEventHandler_t3D6E9B1BC6606FEC892CE393EDD4D9ED89DE9D28), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { sizeof (InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3939[3] = 
{
	InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42::get_offset_of_mBBOWyREJBaBXFyzagpvatnxwNu_0(),
	InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42::get_offset_of_UBXHFojKQfGbvszJFuVFvDRPgNg_1(),
	InteractionStateTransitionArgs_t67500A98203849C0A877FD6DB1AC3A0A9487AB42::get_offset_of_xbBSqLtNsieZdHwmQDzzBoxOPYLS_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { sizeof (TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3941[9] = 
{
	TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39::get_offset_of__visible_4(),
	TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39::get_offset_of__transitionType_5(),
	TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39::get_offset_of__transitionColorTint_6(),
	TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39::get_offset_of__transitionSpriteState_7(),
	TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39::get_offset_of__transitionAnimationTriggers_8(),
	TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39::get_offset_of__targetGraphic_9(),
	TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39::get_offset_of__syncFadeDurationWithTransitionEvent_10(),
	TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39::get_offset_of__syncColorTintWithTransitionEvent_11(),
	TouchInteractableTransitioner_t35BA166309A7F0BAFB408D3C94D7FC841F150B39::get_offset_of_PqtQcSjSeTVUFsVUypYQJPZpbSs_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { sizeof (TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D), -1, sizeof(TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3942[68] = 
{
	0,
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__horizontalAxisCustomControllerElement_44(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__verticalAxisCustomControllerElement_45(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__tapCustomControllerElement_46(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__stickTransform_47(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__joystickMode_48(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__digitalModeDeadZone_49(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__stickRange_50(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__scaleStickRange_51(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__stickBounds_52(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__axesToUse_53(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__snapDirections_54(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__snapStickToTouch_55(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__centerStickOnRelease_56(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__axis2D_57(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__activateOnSwipeIn_58(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__stayActiveOnSwipeOut_59(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__allowTap_60(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__tapTimeout_61(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__tapDistanceLimit_62(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__touchRegion_63(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__useTouchRegionOnly_64(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__moveToTouchPosition_65(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__returnOnRelease_66(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__followTouchPosition_67(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__animateOnMoveToTouch_68(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__moveToTouchSpeed_69(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__animateOnReturn_70(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__returnSpeed_71(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__manageRaycasting_72(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__useXAxis_73(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__useYAxis_74(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__hierarchyValueChangedHandlers_75(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__hierarchyStickPositionChangedHandlers_76(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__workingTouchRegion_77(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__origAnchoredPosition_78(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__origStickAnchoredPosition_79(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__lastPressAnchoredPosition_80(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__isMoving_81(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__isMovedFromDefaultPosition_82(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__moveDirection_83(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__pointerId_84(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__realMousePointerId_85(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of_UsFAvRrPreZngsJiAjbwyGLdcub_86(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of_pPGULjslgtAiohyNMfEalDGgOVl_87(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__pointerDownIsFake_88(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__lastPressStartingValue_89(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__lastClaimSource_90(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__touchStartTime_91(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__touchStartPosition_92(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__coroutineMove_93(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__imageRaycastHelper_94(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__calculatedStickRange_lastUpdatedFrame_95(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__lastTapFrame_96(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__isEligibleForTap_97(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of___calculatedStickRange_cachedValue_98(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of___moveStartedDelegate_99(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of___moveEndedDelegate_100(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__onValueChanged_101(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__onStickPositionChanged_102(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__onTouchStarted_103(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__onTouchEnded_104(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of__onTap_105(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D::get_offset_of___fakePointerEventData_106(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D_StaticFields::get_offset_of___valueChangedHandlerDelegate_107(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D_StaticFields::get_offset_of___stickPositionChangedHandlerDelegate_108(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate8_109(),
	TouchJoystick_t7D003265D5469EE86A9E1609771F8362EE1F703D_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatea_110(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { sizeof (AxisDirection_tCD8E4443AA3EE2AE12039BCD2D74D246C1390C74)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3943[4] = 
{
	AxisDirection_tCD8E4443AA3EE2AE12039BCD2D74D246C1390C74::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { sizeof (JoystickMode_t74D57E8CEC39EA951D6113585329E61E9F5613D8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3944[3] = 
{
	JoystickMode_t74D57E8CEC39EA951D6113585329E61E9F5613D8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { sizeof (SnapDirections_tE4DF36DCA8FBCE6CA238E4F0B0995544F8852A65)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3945[7] = 
{
	SnapDirections_tE4DF36DCA8FBCE6CA238E4F0B0995544F8852A65::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { sizeof (NHoUMtvQEGOtTovxNMKxiWCTcIN_t380A42EB78513E9246FEC0675D2974000BA9A147)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3946[4] = 
{
	NHoUMtvQEGOtTovxNMKxiWCTcIN_t380A42EB78513E9246FEC0675D2974000BA9A147::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (GQMwmwtRNqThmSuCwNhLowMzATs_t8BD164B75F6D445AD35941ECB547B6C428C71D18)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3947[3] = 
{
	GQMwmwtRNqThmSuCwNhLowMzATs_t8BD164B75F6D445AD35941ECB547B6C428C71D18::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { sizeof (StickBounds_tC34F0212ECA58EB32784789FCF7DA246E08ED230)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3948[3] = 
{
	StickBounds_tC34F0212ECA58EB32784789FCF7DA246E08ED230::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { sizeof (ValueChangedEventHandler_t739FAF55A04B824B9182F47969314C660604524B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { sizeof (StickPositionChangedEventHandler_tD962BE6C58EEDC9CA9949ACBF5F61EDC922BF395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { sizeof (TapEventHandler_tE705770344A52DD95176406B4B00E5EE8E525B2F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (TouchStartedEventHandler_tC39C61D509D6C9452386D0D05B7782F35841D4AB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { sizeof (TouchEndedEventHandler_t05BD8DD2BB21689F364B8300E6A1F266093E111C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3956[12] = 
{
	hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2(),
	hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA::get_offset_of_PLYNlNwVGPITrOPQzvkfxJINxqV_3(),
	hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA::get_offset_of_RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4(),
	hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA::get_offset_of_QFegQkgOGzZhlzwAWRdvkhXoHBk_5(),
	hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA::get_offset_of_iBiwqHkhtevrrrpOZhDdNCZduXg_6(),
	hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA::get_offset_of_hQTchMQmvLwphsByczpEoMsVTpL_7(),
	hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA::get_offset_of_XnqfsYIpjWuutJvTBiiaElBjIyR_8(),
	hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA::get_offset_of_nByUllSKMZtbQGGNAQuuoTgeyIy_9(),
	hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA::get_offset_of_KrYbtDRftEBOhUhmcodyXyGJFIK_10(),
	hhWKZzQYZtTVNyptxlbindUIQPv_tE8D06CFDB7F2B24A0A79FD3DAB67E48AD0B0CDDA::get_offset_of_yZYDvdhLYwOFahFGGIYphvTazJj_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3957[13] = 
{
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of__visible_4(),
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of__targetAngleFromRotation_5(),
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of__targetAngle_6(),
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of__fadeWithValue_7(),
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of__fadeWithAngle_8(),
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of__fadeRange_9(),
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of__activeColor_10(),
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of__normalColor_11(),
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of_rKKdbmHeiCCQMaJgmJUEKzGjGBy_12(),
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of_EaMOsLhjtTYBBHEPpOpEyoAPAdK_13(),
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of_oLCXrVyPtTkSlhIIeWGCYgYYtsl_14(),
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of_UVHSsikRXlSwbjZfTYqbFsbelOK_15(),
	TouchJoystickAngleIndicator_tBDE7E5ED4BADAA97015FE8FB45943F451313BD38::get_offset_of_nVsrZvdBAogFFZcvCTKaJhDldWc_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE), -1, sizeof(TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3958[9] = 
{
	TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE::get_offset_of__scale_4(),
	TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE::get_offset_of__preserveSpriteAspectRatio_5(),
	TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE::get_offset_of__scaleRatio_6(),
	TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE::get_offset_of__aspectRatioX_7(),
	TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE::get_offset_of__aspectRatioY_8(),
	TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE::get_offset_of__offset_9(),
	TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE_StaticFields::get_offset_of_YqFjPGpeuVROMhanOhJFCnjeIPlk_10(),
	TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE::get_offset_of_EaMOsLhjtTYBBHEPpOpEyoAPAdK_11(),
	TouchJoystickRadialIndicator_t67A5B7A8B5D2B042D1B0C4A37A7983FB445AF3AE::get_offset_of_WGFHKIeFWLGlqMVqbtJnErXsASW_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3959[39] = 
{
	0,
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__targetCustomControllerElement_44(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__buttonType_45(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__activateOnSwipeIn_46(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__stayActiveOnSwipeOut_47(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__useDigitalAxisSimulation_48(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__digitalAxisGravity_49(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__digitalAxisSensitivity_50(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__axis_51(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__touchRegion_52(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__useTouchRegionOnly_53(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__moveToTouchPosition_54(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__returnOnRelease_55(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__followTouchPosition_56(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__animateOnMoveToTouch_57(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__moveToTouchSpeed_58(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__animateOnReturn_59(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__returnSpeed_60(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__manageRaycasting_61(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_EiwtMLppfCtuooXjvdHygyGrsHS_62(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_JHUTNNOcuztILjpjkavvMXajFRV_63(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_FcORQGMEvzohtgyCwrysLogmieu_64(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_nysomDCBRIvDcmoXFQHdLQznHNr_65(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_reSQzxHMdzbyhGwOPrSROZjFOBx_66(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_uaVpGuIOaSatQKbOxUPLMpimWG_67(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_mucIMzrdRGVGfPFucsRzAxGACqM_68(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_kKSSGIYcShDBXbkfYwidHsiqbRh_69(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_VKAtGkSQLhkGGQSRuCzXdQVELuq_70(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_UsFAvRrPreZngsJiAjbwyGLdcub_71(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_pPGULjslgtAiohyNMfEalDGgOVl_72(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_HgstqelxyDTEvxPqOrKBCHhdKVB_73(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_VUcbNyhUNSjVjmdmpXAHQiBoHeYO_74(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_bQOHtIPuejRPzOXhHfAkcDmJfTT_75(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_AQDzRNACmkHZMcdFfzmRWfTaKBg_76(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__onAxisValueChanged_77(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__onButtonValueChanged_78(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__onButtonDown_79(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of__onButtonUp_80(),
	TouchButton_t192BE56ABF72197355626DC4A937D764B01D6D7F::get_offset_of_SbJiaOqkhEQicEKkypVosjZLBXA_81(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { sizeof (ButtonType_tCA2796A69EDE4183905636CE5738CD1D06F9E39B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3960[3] = 
{
	ButtonType_tCA2796A69EDE4183905636CE5738CD1D06F9E39B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (CRqCjFCalakqbykartaanqOTwPEL_tB97B494B677C52047A13AF2D1089D10B855F70CA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3961[4] = 
{
	CRqCjFCalakqbykartaanqOTwPEL_tB97B494B677C52047A13AF2D1089D10B855F70CA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (IUgSMPnekjYQlhIyodPFoTtCQpE_tA1AFA539D16AD8DC6479AF8DDA006C37B0A2D43A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3962[3] = 
{
	IUgSMPnekjYQlhIyodPFoTtCQpE_tA1AFA539D16AD8DC6479AF8DDA006C37B0A2D43A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (AxisValueChangedEventHandler_t622BC4F8C94A13837769CEFE1FD12CC18B926791), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (ButtonValueChangedEventHandler_tB3FD14B60CDD8A2E3B8D8B26F951B056495B5A50), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { sizeof (ButtonDownEventHandler_tD3FB726C5A3E17A89FA7992F2D0D0422286B7E7D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { sizeof (ButtonUpEventHandler_t529887EE63C2C416911C6C64F8D230008EB73339), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { sizeof (ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3967[12] = 
{
	ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_2(),
	ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906::get_offset_of_PLYNlNwVGPITrOPQzvkfxJINxqV_3(),
	ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906::get_offset_of_RSjsEfDGFnIyjaMrbqVhgtKxPqNf_4(),
	ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906::get_offset_of_QFegQkgOGzZhlzwAWRdvkhXoHBk_5(),
	ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906::get_offset_of_iBiwqHkhtevrrrpOZhDdNCZduXg_6(),
	ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906::get_offset_of_rZWxSZSGyMGOGFWhVovIOcyLTvmI_7(),
	ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906::get_offset_of_MgCqBsRGtBCAjzmghqbspYwrQUy_8(),
	ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906::get_offset_of_PtnufnJQeeWcbSnrMVerFHKuNkj_9(),
	ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906::get_offset_of_sHEiejKjFwBVUBvFilDHmOsyaMhI_10(),
	ADKisTklJqZAHJSayvbTgaTOwfP_t536F7A7A261C53ED39A2F5468663D7D8FFABD906::get_offset_of_squPontnRWBsKFStIzRLzrGxIch_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { sizeof (TouchController_t524B2E08B5946E6CD833758471F80C3B35473C3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3968[2] = 
{
	TouchController_t524B2E08B5946E6CD833758471F80C3B35473C3A::get_offset_of__disableMouseInputWhenEnabled_13(),
	TouchController_t524B2E08B5946E6CD833758471F80C3B35473C3A::get_offset_of__useCustomController_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { sizeof (TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3969[41] = 
{
	0,
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__horizontalAxisCustomControllerElement_44(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__verticalAxisCustomControllerElement_45(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__tapCustomControllerElement_46(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__pressCustomControllerElement_47(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__axesToUse_48(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__touchPadMode_49(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__valueFormat_50(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__useInertia_51(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__inertiaFriction_52(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__activateOnSwipeIn_53(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__stayActiveOnSwipeOut_54(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__allowTap_55(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__tapTimeout_56(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__tapDistanceLimit_57(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__allowPress_58(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__pressStartDelay_59(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__pressDistanceLimit_60(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__hideAtRuntime_61(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__axis2D_62(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__onValueChanged_63(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__onTap_64(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__onPressDown_65(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__onPressUp_66(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__useXAxis_67(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__useYAxis_68(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__pointerId_69(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__realMousePointerId_70(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of_UsFAvRrPreZngsJiAjbwyGLdcub_71(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of_pPGULjslgtAiohyNMfEalDGgOVl_72(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__pointerDownIsFake_73(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__touchStartPosition_74(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__touchStartTime_75(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__currentCenter_76(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__previousTouchPosition_77(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__lastTapFrame_78(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__isEligibleForTap_79(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__isEligibleForPress_80(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__pressValue_81(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of__smoothDelta_82(),
	TouchPad_t129C582C0CDA2EEE97C17F8A94F61930086089C9::get_offset_of___fakePointerEventData_83(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (AxisDirection_tDFDCCB740A5EA7151F47AB1DE1F1360010912247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3970[4] = 
{
	AxisDirection_tDFDCCB740A5EA7151F47AB1DE1F1360010912247::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { sizeof (TouchPadMode_t686F891A5C5D94E96BB5A41CBBA163893A673E06)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3971[5] = 
{
	TouchPadMode_t686F891A5C5D94E96BB5A41CBBA163893A673E06::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { sizeof (ValueFormat_tD974E10B7AD1AC0E4E312035DE1D83F3BA01E16D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3972[5] = 
{
	ValueFormat_tD974E10B7AD1AC0E4E312035DE1D83F3BA01E16D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { sizeof (IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3973[3] = 
{
	IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA::get_offset_of_HybpbdhcfSQOhFpilWKDvPgatce_0(),
	IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA::get_offset_of_muHfgZrAoceOXsMhCljOQSVcJGk_1(),
	IftMAPzfkVAPWSFrTxsXPBYDanRJ_tD8E54C1EA7EFF9F6094CC38366CD4557D57227DA::get_offset_of_uiElHIMsJhOUGwxstGSvtQgdJHm_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (YcVzboeRjDknSZWvRpWNyVnfSFz_tEF9716D98D3BE93F829A32A7DD90A5AC04BCED8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3974[3] = 
{
	YcVzboeRjDknSZWvRpWNyVnfSFz_tEF9716D98D3BE93F829A32A7DD90A5AC04BCED8C::get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_0(),
	YcVzboeRjDknSZWvRpWNyVnfSFz_tEF9716D98D3BE93F829A32A7DD90A5AC04BCED8C::get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_1(),
	YcVzboeRjDknSZWvRpWNyVnfSFz_tEF9716D98D3BE93F829A32A7DD90A5AC04BCED8C::get_offset_of_RwULBJGzOBCTZGSEDQvzZoRYikI_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (ValueChangedEventHandler_t5D816B57AB2EEC7ACA0BE8CFB398EE5EDD7E32C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (TapEventHandler_t33FC3FB8B60BF60104F3569F86657ADDB6BC0944), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (PressDownEventHandler_tADC629B1DD822026CE2E06E3CDA7FB6D4E495F8D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (PressUpEventHandler_t4125670E8C4E9682843D4F1D030ACF428FD397EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3979[8] = 
{
	TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49::get_offset_of__hideAtRuntime_43(),
	TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49::get_offset_of__onPointerDown_44(),
	TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49::get_offset_of__onPointerUp_45(),
	TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49::get_offset_of__onPointerEnter_46(),
	TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49::get_offset_of__onPointerExit_47(),
	TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49::get_offset_of__onBeginDrag_48(),
	TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49::get_offset_of__onDrag_49(),
	TouchRegion_tF116E3AF6B3D62ECC010A44548C846348638DF49::get_offset_of__onEndDrag_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (gWVGylYAAhiNGOqYxKsBnzSeJJp_t698C187A9425952910BAA66C4096E5CECF9C45C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { sizeof (jOGBgBryHTPCrwAWbtpjcyBVWgH_t60095498B6581E7071A1B57B00B2727697AE5892), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { sizeof (CUBRkEgPtmqloePiGjPGzvftMEz_t62B6522D584CD2A21FCD80D3C3CAED97E95E8D13), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (itdXQhVbgPjNuvzzTkqLUeNdGDO_t9686109095347771CDEF55C86FA148EFB32C1144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (pSrmmveUVOWIyFBwbhMaEmScisy_t4B47DB2624A04C0C6B1093B289477A5A3506100B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { sizeof (DbjAHtCsNuwTcsSMKOqHaDEgpfX_t1824811CE094ED8DE5F842353B1C13B05116152D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { sizeof (SpNSxRoDgiUWSAIfKMmMQPNAbEH_tC64B3B02AA7E79A9CAE0BA33FE4F96AC5E5405A1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3987[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989 = { sizeof (PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3989[6] = 
{
	PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A::get_offset_of__rewiredInputManager_6(),
	PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A::get_offset_of__playerId_7(),
	PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A::get_offset_of__elements_8(),
	PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A::get_offset_of__onButtonStateChanged_9(),
	PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A::get_offset_of__onAxisValueChanged_10(),
	PlayerController_tF6C76D76E8F199EF74F4568EB7E9ADD18A62600A::get_offset_of__onEnabledStateChanged_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990 = { sizeof (ButtonStateChangedHandler_t0531BB1117C004D092524C45A5BD445D5B15AFEC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991 = { sizeof (AxisValueChangedHandler_t26F8AD336CA0516BA16CD70E0E0BE16BE4DC474D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992 = { sizeof (EnabledStateChangedHandler_t7892BA4CD24039FCF244401A350BF2C9367DAF43), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993 = { sizeof (ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3993[7] = 
{
	ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05::get_offset_of__name_0(),
	ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05::get_offset_of__elementType_1(),
	ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05::get_offset_of__enabled_2(),
	ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05::get_offset_of__actionId_3(),
	ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05::get_offset_of__coordinateMode_4(),
	ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05::get_offset_of__absoluteToRelativeSensitivity_5(),
	ElementWithSourceInfo_tE98141C8E9A10E18ED90DB84BC264F43DAB73A05::get_offset_of__repeatRate_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994 = { sizeof (ElementInfo_t0269D1F95FEF139A73F40327FE7B457D4253F1C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3994[4] = 
{
	ElementInfo_t0269D1F95FEF139A73F40327FE7B457D4253F1C7::get_offset_of__name_0(),
	ElementInfo_t0269D1F95FEF139A73F40327FE7B457D4253F1C7::get_offset_of__elementType_1(),
	ElementInfo_t0269D1F95FEF139A73F40327FE7B457D4253F1C7::get_offset_of__enabled_2(),
	ElementInfo_t0269D1F95FEF139A73F40327FE7B457D4253F1C7::get_offset_of__elements_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997 = { sizeof (PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3997[6] = 
{
	PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23::get_offset_of__defaultToCenter_12(),
	PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23::get_offset_of__pointerSpeed_13(),
	PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23::get_offset_of__useHardwarePointerPosition_14(),
	PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23::get_offset_of__movementArea_15(),
	PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23::get_offset_of__movementAreaUnit_16(),
	PlayerMouse_t503EB79D61B2B0E2FF6444E3EDD72F31F5E15B23::get_offset_of__onScreenPositionChanged_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998 = { sizeof (ScreenPositionChangedHandler_t29C4BC8B3DB9744150268C83109151A8DAA7B2F6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999 = { sizeof (UpdateLoopSetting_t813FEF786E2423CBB40457E67ABB4712DA865CA5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3999[5] = 
{
	UpdateLoopSetting_t813FEF786E2423CBB40457E67ABB4712DA865CA5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
