﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Rewired.Controller/Extension
struct Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD;
// Rewired.ControllerElementIdentifier
struct ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66;
// Rewired.ControllerElementIdentifier[]
struct ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE;
// Rewired.Data.ConfigVars/EditorVars
struct EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF;
// Rewired.Data.ConfigVars/PlatformVars
struct PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA;
// Rewired.Data.ConfigVars/PlatformVars_WindowsStandalone
struct PlatformVars_WindowsStandalone_t1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A;
// Rewired.Data.ConfigVars/PlatformVars_WindowsUWP
struct PlatformVars_WindowsUWP_t3F16819E5BF554DD60A4D1E2464E38C88A9D73D8;
// Rewired.Data.CustomController_Editor
struct CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E;
// Rewired.Data.Mapping.ActionCategoryMap
struct ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F;
// Rewired.Data.Mapping.ActionCategoryMap/Entry
struct Entry_t87C36EB768B9C1EA06D2C2FC073B650711241DAC;
// Rewired.Data.Mapping.AxisCalibrationInfo
struct AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5;
// Rewired.Data.Mapping.CustomCalculation
struct CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038;
// Rewired.Data.Mapping.HardwareAxisInfo
struct HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4;
// Rewired.Data.Mapping.HardwareButtonInfo
struct HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273;
// Rewired.Data.Mapping.HardwareJoystickMap
struct HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB;
// Rewired.Data.Mapping.HardwareJoystickMap/AxisCalibrationInfoEntry[]
struct AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7;
// Rewired.Data.Mapping.HardwareJoystickMap/CompoundElement[]
struct CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform
struct Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_DirectInput
struct Platform_DirectInput_t8384F48EB39207EF4B476BF85EEE95C48F113399;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_DirectInput_Base
struct Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_DirectInput_Base/Axis[]
struct AxisU5BU5D_tA03336768F545EBCBA07811F87E1AFA987F0C606;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_DirectInput_Base/Button[]
struct ButtonU5BU5D_tE48299B02DCCB4EF830F20647DD07A24F584B3E6;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_DirectInput_Base/Elements
struct Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_DirectInput_Base[]
struct Platform_DirectInput_BaseU5BU5D_tC48FB06612E7272F985A1F10E6951190ABF30EEE;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Fallback
struct Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_InternalDriver
struct Platform_InternalDriver_t8A5A88BC6C79D793F1E687A676A4EC95C644007F;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Linux
struct Platform_Linux_t5C50A8A935B1AE1D30D6F7912DA10AEAFFD53EB5;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_NintendoSwitch
struct Platform_NintendoSwitch_t2696C2B97DA28B7AFFF2A0AA8CE74563C99F99C8;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_OSX
struct Platform_OSX_t03A46AF339888AC6E808D1D31A7467AD14FCA357;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Ouya
struct Platform_Ouya_t6E1DA960C43511902B179E0E6746B8230AE60092;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_PS4
struct Platform_PS4_t5B4B7E5A8EF60DFB5E1F1CE6756ADD5BD5FB4FA1;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawInput
struct Platform_RawInput_tFA8A4734C93ADCCF4CE7C2E3947003AB875E5FBD;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawInput_Base/Elements
struct Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawOrDirectInput/Axis_Base
struct Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawOrDirectInput/Button_Base
struct Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawOrDirectInput/CustomCalculationSourceData[]
struct CustomCalculationSourceDataU5BU5D_t034AAEDA41E4C78D3D656BBE7D542F430FDB5CC9;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawOrDirectInput/MatchingCriteria
struct MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawOrDirectInput/MatchingCriteria/ElementCount[]
struct ElementCountU5BU5D_tF8971F7C10BBD3C570DFBE16A697C21D0A2DAE5B;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_SDL2
struct Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WebGL
struct Platform_WebGL_t7C026B328916B42D37B442AE084EABCB320F318A;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WindowsUWP
struct Platform_WindowsUWP_t79A50D7432962C9DD8B8CDFFAFC55B27BB32C06A;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XInput
struct Platform_XInput_t44E2E6F0FD4F4A22A7FB6C031984FFC5F82BDC16;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XboxOne
struct Platform_XboxOne_tF5ACC52C21E7D086DBC6B842E36075C1582AA2C4;
// Rewired.Data.Mapping.HardwareJoystickMap[]
struct HardwareJoystickMapU5BU5D_t3DA1886163A97C877786AB4C7C60EF14C46B0FD4;
// Rewired.Data.Mapping.HardwareJoystickTemplateMap[]
struct HardwareJoystickTemplateMapU5BU5D_t57BC6D30D553A657F72600B324770F807FDDECAB;
// Rewired.JoystickType[]
struct JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA;
// Rewired.Platforms.Custom.CustomInputSource/Axis[]
struct AxisU5BU5D_tD22230ADED67D8E860DD8F4226514D79EDE55BEE;
// Rewired.Platforms.Custom.CustomInputSource/Button[]
struct ButtonU5BU5D_t181B5EB5ACB98CA3C44C3FB3DF613259B353B43A;
// Rewired.PlayerController
struct PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01;
// Rewired.PlayerController/Axis/Definition
struct Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9;
// Rewired.Utils.Classes.Data.AList`1<Rewired.PlayerController/Axis>
struct AList_1_t9C15FC8D8FA03BDBF2DD16B78F757675F8E2BEBF;
// Rewired.Utils.Classes.Data.AList`1<Rewired.PlayerController/Button>
struct AList_1_tF031382831FF2B6183255F62CB24A361ACB88878;
// Rewired.Utils.Classes.Data.AList`1<Rewired.PlayerController/Element>
struct AList_1_t3F27923E7ADC225A9A272E929A0E4F01AF6316AC;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t28DE0166228B92F520FE7ADC71034A2C4009E0F5;
// System.Action`2<Rewired.Platforms.Platform,System.Object>
struct Action_2_tB2B96E4A5FB6DFD583C51ED8D9B9AE1C03B7965D;
// System.Action`2<System.Int32,System.Boolean>
struct Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E;
// System.Action`2<System.Int32,System.Single>
struct Action_2_t634E33D1E916177D11D3008C7BCF5A07ADECEA10;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Data.ConfigVars/DadcqoCjLXmahnTWBdNQJAWfNLqB>
struct Dictionary_2_tA052622BA982CD488FA11E7207A3A0230C399552;
// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Data.ConfigVars/EtiwYEyEvxhGdDZwPhVpQNWkBYX>
struct Dictionary_2_t2E49D98A4B76F81C22CA318194D3185163B54A89;
// System.Collections.Generic.ICollection`1<Rewired.PlayerController/Element/Definition>
struct ICollection_1_t61758A9DBBA60DE454452A75BFD94EC288674415;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t8BA8E0A3600A88AC1F3656FA795732039D920E4B;
// System.Collections.Generic.IList`1<Rewired.Data.Mapping.HardwareJoystickMap/Platform>
struct IList_1_tA9EB101BC5366E66024BFF7EC33964460C2850B4;
// System.Collections.Generic.List`1<Rewired.ControllerElementIdentifier>
struct List_1_t4D4CD18041A17FC7805220DF92DF87F2FE40ED1C;
// System.Collections.Generic.List`1<Rewired.ControllerTemplateActionElementMap>
struct List_1_t6C42FA67B6C0B4EF47E07906C577B234282D2171;
// System.Collections.Generic.List`1<Rewired.Data.CustomController_Editor/Axis>
struct List_1_t780589CD1E23A6E1051C3ED014CD54D57B46B34B;
// System.Collections.Generic.List`1<Rewired.Data.CustomController_Editor/Button>
struct List_1_tF154A2939A16D295695D2DFAACA1A83DB0A2D531;
// System.Collections.Generic.List`1<Rewired.Data.Mapping.ActionCategoryMap/Entry>
struct List_1_tAE81ACAEB31146DF9658E07FCEE561860901D114;
// System.Collections.Generic.List`1<Rewired.Platforms.Custom.CustomInputSource/Joystick>
struct List_1_tE74CF06E458D02B7DFF8A66064E48B59E372F1F2;
// System.Collections.Generic.List`1<Rewired.PlayerController/Element/dvlZSFCjnFxtoufPKLchsiMXNQc>
struct List_1_t1BAF2A63302AA70F47F2BB1F8ADC76463CEBC1D1;
// System.Collections.Generic.List`1<Rewired.PlayerController/Element>
struct List_1_t928662206E2177819B177E4EA1888910BA369578;
// System.Collections.Generic.List`1<Rewired.Utils.Classes.Data.TypeWrapper>
struct List_1_tE69A2AD31EE2358B14DC2FF01FA302D8FCF220CC;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.ControllerTemplateActionElementMap>
struct ReadOnlyCollection_1_tBA01D97DD95AB800F00D61982D160EEEA23DB50E;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Platforms.Custom.CustomInputSource/Axis>
struct ReadOnlyCollection_1_t4829C3B53F3992A9308E9687500F009CA89848AC;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Platforms.Custom.CustomInputSource/Button>
struct ReadOnlyCollection_1_t8D1C0984E4131D0AD8A662731FDECC171DD80B5F;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Platforms.Custom.CustomInputSource/Joystick>
struct ReadOnlyCollection_1_t3725131578433E1E4A019736E9F58AB979C5A48D;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.PlayerController/Axis>
struct ReadOnlyCollection_1_t41212E3E590E84A0579889CC0C7E5FFE22DEFC8F;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.PlayerController/Button>
struct ReadOnlyCollection_1_t05EECEFFDFD4BA4204B837F165F3EFB261C27C52;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.PlayerController/Element>
struct ReadOnlyCollection_1_tCB1B9579FC6781EA29B0BA088B7B6D67A6494C4C;
// System.Func`1<Rewired.Data.ConfigVars/PlatformVars>
struct Func_1_t04F7F7CE71B3BD573D381237377FC5BC6251348F;
// System.Func`2<Rewired.Platforms.Platform,System.Object>
struct Func_2_t6E71B9A886B9A137734D96A014812A046CAA9A56;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Predicate`1<Rewired.PlayerController/Axis>
struct Predicate_1_tF4A9FED2002F5F108CC39D461644FF7FA88E4C55;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DADCQOCJLXMAHNTWBDNQJAWFNLQB_TEBDD11410F726E43CEAB433D38E6283D8AC4670B_H
#define DADCQOCJLXMAHNTWBDNQJAWFNLQB_TEBDD11410F726E43CEAB433D38E6283D8AC4670B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ConfigVars_DadcqoCjLXmahnTWBdNQJAWfNLqB
struct  DadcqoCjLXmahnTWBdNQJAWfNLqB_tEBDD11410F726E43CEAB433D38E6283D8AC4670B  : public RuntimeObject
{
public:
	// System.Func`2<Rewired.Platforms.Platform,System.Object> Rewired.Data.ConfigVars_DadcqoCjLXmahnTWBdNQJAWfNLqB::ImAEWBFqgIbvkLGhFYjFCwIBMTg
	Func_2_t6E71B9A886B9A137734D96A014812A046CAA9A56 * ___ImAEWBFqgIbvkLGhFYjFCwIBMTg_0;
	// System.Action`2<Rewired.Platforms.Platform,System.Object> Rewired.Data.ConfigVars_DadcqoCjLXmahnTWBdNQJAWfNLqB::WjjfnLCmwGPtFyAxWAdOloiFApm
	Action_2_tB2B96E4A5FB6DFD583C51ED8D9B9AE1C03B7965D * ___WjjfnLCmwGPtFyAxWAdOloiFApm_1;

public:
	inline static int32_t get_offset_of_ImAEWBFqgIbvkLGhFYjFCwIBMTg_0() { return static_cast<int32_t>(offsetof(DadcqoCjLXmahnTWBdNQJAWfNLqB_tEBDD11410F726E43CEAB433D38E6283D8AC4670B, ___ImAEWBFqgIbvkLGhFYjFCwIBMTg_0)); }
	inline Func_2_t6E71B9A886B9A137734D96A014812A046CAA9A56 * get_ImAEWBFqgIbvkLGhFYjFCwIBMTg_0() const { return ___ImAEWBFqgIbvkLGhFYjFCwIBMTg_0; }
	inline Func_2_t6E71B9A886B9A137734D96A014812A046CAA9A56 ** get_address_of_ImAEWBFqgIbvkLGhFYjFCwIBMTg_0() { return &___ImAEWBFqgIbvkLGhFYjFCwIBMTg_0; }
	inline void set_ImAEWBFqgIbvkLGhFYjFCwIBMTg_0(Func_2_t6E71B9A886B9A137734D96A014812A046CAA9A56 * value)
	{
		___ImAEWBFqgIbvkLGhFYjFCwIBMTg_0 = value;
		Il2CppCodeGenWriteBarrier((&___ImAEWBFqgIbvkLGhFYjFCwIBMTg_0), value);
	}

	inline static int32_t get_offset_of_WjjfnLCmwGPtFyAxWAdOloiFApm_1() { return static_cast<int32_t>(offsetof(DadcqoCjLXmahnTWBdNQJAWfNLqB_tEBDD11410F726E43CEAB433D38E6283D8AC4670B, ___WjjfnLCmwGPtFyAxWAdOloiFApm_1)); }
	inline Action_2_tB2B96E4A5FB6DFD583C51ED8D9B9AE1C03B7965D * get_WjjfnLCmwGPtFyAxWAdOloiFApm_1() const { return ___WjjfnLCmwGPtFyAxWAdOloiFApm_1; }
	inline Action_2_tB2B96E4A5FB6DFD583C51ED8D9B9AE1C03B7965D ** get_address_of_WjjfnLCmwGPtFyAxWAdOloiFApm_1() { return &___WjjfnLCmwGPtFyAxWAdOloiFApm_1; }
	inline void set_WjjfnLCmwGPtFyAxWAdOloiFApm_1(Action_2_tB2B96E4A5FB6DFD583C51ED8D9B9AE1C03B7965D * value)
	{
		___WjjfnLCmwGPtFyAxWAdOloiFApm_1 = value;
		Il2CppCodeGenWriteBarrier((&___WjjfnLCmwGPtFyAxWAdOloiFApm_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DADCQOCJLXMAHNTWBDNQJAWFNLQB_TEBDD11410F726E43CEAB433D38E6283D8AC4670B_H
#ifndef EDITORVARS_T2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF_H
#define EDITORVARS_T2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ConfigVars_EditorVars
struct  EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_useParentClass
	bool ___exportConsts_useParentClass_0;
	// System.String Rewired.Data.ConfigVars_EditorVars::exportConsts_parentClassName
	String_t* ___exportConsts_parentClassName_1;
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_useNamespace
	bool ___exportConsts_useNamespace_2;
	// System.String Rewired.Data.ConfigVars_EditorVars::exportConsts_namespace
	String_t* ___exportConsts_namespace_3;
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_actions
	bool ___exportConsts_actions_4;
	// System.String Rewired.Data.ConfigVars_EditorVars::exportConsts_actionsClassName
	String_t* ___exportConsts_actionsClassName_5;
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_actionsIncludeActionCategory
	bool ___exportConsts_actionsIncludeActionCategory_6;
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_actionsCreateClassesForActionCategories
	bool ___exportConsts_actionsCreateClassesForActionCategories_7;
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_mapCategories
	bool ___exportConsts_mapCategories_8;
	// System.String Rewired.Data.ConfigVars_EditorVars::exportConsts_mapCategoriesClassName
	String_t* ___exportConsts_mapCategoriesClassName_9;
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_layouts
	bool ___exportConsts_layouts_10;
	// System.String Rewired.Data.ConfigVars_EditorVars::exportConsts_layoutsClassName
	String_t* ___exportConsts_layoutsClassName_11;
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_players
	bool ___exportConsts_players_12;
	// System.String Rewired.Data.ConfigVars_EditorVars::exportConsts_playersClassName
	String_t* ___exportConsts_playersClassName_13;
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_inputBehaviors
	bool ___exportConsts_inputBehaviors_14;
	// System.String Rewired.Data.ConfigVars_EditorVars::exportConsts_inputBehaviorsClassName
	String_t* ___exportConsts_inputBehaviorsClassName_15;
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_customControllers
	bool ___exportConsts_customControllers_16;
	// System.String Rewired.Data.ConfigVars_EditorVars::exportConsts_customControllersClassName
	String_t* ___exportConsts_customControllersClassName_17;
	// System.String Rewired.Data.ConfigVars_EditorVars::exportConsts_customControllersAxesClassName
	String_t* ___exportConsts_customControllersAxesClassName_18;
	// System.String Rewired.Data.ConfigVars_EditorVars::exportConsts_customControllersButtonsClassName
	String_t* ___exportConsts_customControllersButtonsClassName_19;
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_layoutManagerRuleSets
	bool ___exportConsts_layoutManagerRuleSets_20;
	// System.String Rewired.Data.ConfigVars_EditorVars::exportConsts_layoutManagerRuleSetsClassName
	String_t* ___exportConsts_layoutManagerRuleSetsClassName_21;
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_mapEnablerRuleSets
	bool ___exportConsts_mapEnablerRuleSets_22;
	// System.String Rewired.Data.ConfigVars_EditorVars::exportConsts_mapEnablerRuleSetsClassName
	String_t* ___exportConsts_mapEnablerRuleSetsClassName_23;
	// System.Boolean Rewired.Data.ConfigVars_EditorVars::exportConsts_allCapsConstantNames
	bool ___exportConsts_allCapsConstantNames_24;

public:
	inline static int32_t get_offset_of_exportConsts_useParentClass_0() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_useParentClass_0)); }
	inline bool get_exportConsts_useParentClass_0() const { return ___exportConsts_useParentClass_0; }
	inline bool* get_address_of_exportConsts_useParentClass_0() { return &___exportConsts_useParentClass_0; }
	inline void set_exportConsts_useParentClass_0(bool value)
	{
		___exportConsts_useParentClass_0 = value;
	}

	inline static int32_t get_offset_of_exportConsts_parentClassName_1() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_parentClassName_1)); }
	inline String_t* get_exportConsts_parentClassName_1() const { return ___exportConsts_parentClassName_1; }
	inline String_t** get_address_of_exportConsts_parentClassName_1() { return &___exportConsts_parentClassName_1; }
	inline void set_exportConsts_parentClassName_1(String_t* value)
	{
		___exportConsts_parentClassName_1 = value;
		Il2CppCodeGenWriteBarrier((&___exportConsts_parentClassName_1), value);
	}

	inline static int32_t get_offset_of_exportConsts_useNamespace_2() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_useNamespace_2)); }
	inline bool get_exportConsts_useNamespace_2() const { return ___exportConsts_useNamespace_2; }
	inline bool* get_address_of_exportConsts_useNamespace_2() { return &___exportConsts_useNamespace_2; }
	inline void set_exportConsts_useNamespace_2(bool value)
	{
		___exportConsts_useNamespace_2 = value;
	}

	inline static int32_t get_offset_of_exportConsts_namespace_3() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_namespace_3)); }
	inline String_t* get_exportConsts_namespace_3() const { return ___exportConsts_namespace_3; }
	inline String_t** get_address_of_exportConsts_namespace_3() { return &___exportConsts_namespace_3; }
	inline void set_exportConsts_namespace_3(String_t* value)
	{
		___exportConsts_namespace_3 = value;
		Il2CppCodeGenWriteBarrier((&___exportConsts_namespace_3), value);
	}

	inline static int32_t get_offset_of_exportConsts_actions_4() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_actions_4)); }
	inline bool get_exportConsts_actions_4() const { return ___exportConsts_actions_4; }
	inline bool* get_address_of_exportConsts_actions_4() { return &___exportConsts_actions_4; }
	inline void set_exportConsts_actions_4(bool value)
	{
		___exportConsts_actions_4 = value;
	}

	inline static int32_t get_offset_of_exportConsts_actionsClassName_5() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_actionsClassName_5)); }
	inline String_t* get_exportConsts_actionsClassName_5() const { return ___exportConsts_actionsClassName_5; }
	inline String_t** get_address_of_exportConsts_actionsClassName_5() { return &___exportConsts_actionsClassName_5; }
	inline void set_exportConsts_actionsClassName_5(String_t* value)
	{
		___exportConsts_actionsClassName_5 = value;
		Il2CppCodeGenWriteBarrier((&___exportConsts_actionsClassName_5), value);
	}

	inline static int32_t get_offset_of_exportConsts_actionsIncludeActionCategory_6() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_actionsIncludeActionCategory_6)); }
	inline bool get_exportConsts_actionsIncludeActionCategory_6() const { return ___exportConsts_actionsIncludeActionCategory_6; }
	inline bool* get_address_of_exportConsts_actionsIncludeActionCategory_6() { return &___exportConsts_actionsIncludeActionCategory_6; }
	inline void set_exportConsts_actionsIncludeActionCategory_6(bool value)
	{
		___exportConsts_actionsIncludeActionCategory_6 = value;
	}

	inline static int32_t get_offset_of_exportConsts_actionsCreateClassesForActionCategories_7() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_actionsCreateClassesForActionCategories_7)); }
	inline bool get_exportConsts_actionsCreateClassesForActionCategories_7() const { return ___exportConsts_actionsCreateClassesForActionCategories_7; }
	inline bool* get_address_of_exportConsts_actionsCreateClassesForActionCategories_7() { return &___exportConsts_actionsCreateClassesForActionCategories_7; }
	inline void set_exportConsts_actionsCreateClassesForActionCategories_7(bool value)
	{
		___exportConsts_actionsCreateClassesForActionCategories_7 = value;
	}

	inline static int32_t get_offset_of_exportConsts_mapCategories_8() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_mapCategories_8)); }
	inline bool get_exportConsts_mapCategories_8() const { return ___exportConsts_mapCategories_8; }
	inline bool* get_address_of_exportConsts_mapCategories_8() { return &___exportConsts_mapCategories_8; }
	inline void set_exportConsts_mapCategories_8(bool value)
	{
		___exportConsts_mapCategories_8 = value;
	}

	inline static int32_t get_offset_of_exportConsts_mapCategoriesClassName_9() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_mapCategoriesClassName_9)); }
	inline String_t* get_exportConsts_mapCategoriesClassName_9() const { return ___exportConsts_mapCategoriesClassName_9; }
	inline String_t** get_address_of_exportConsts_mapCategoriesClassName_9() { return &___exportConsts_mapCategoriesClassName_9; }
	inline void set_exportConsts_mapCategoriesClassName_9(String_t* value)
	{
		___exportConsts_mapCategoriesClassName_9 = value;
		Il2CppCodeGenWriteBarrier((&___exportConsts_mapCategoriesClassName_9), value);
	}

	inline static int32_t get_offset_of_exportConsts_layouts_10() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_layouts_10)); }
	inline bool get_exportConsts_layouts_10() const { return ___exportConsts_layouts_10; }
	inline bool* get_address_of_exportConsts_layouts_10() { return &___exportConsts_layouts_10; }
	inline void set_exportConsts_layouts_10(bool value)
	{
		___exportConsts_layouts_10 = value;
	}

	inline static int32_t get_offset_of_exportConsts_layoutsClassName_11() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_layoutsClassName_11)); }
	inline String_t* get_exportConsts_layoutsClassName_11() const { return ___exportConsts_layoutsClassName_11; }
	inline String_t** get_address_of_exportConsts_layoutsClassName_11() { return &___exportConsts_layoutsClassName_11; }
	inline void set_exportConsts_layoutsClassName_11(String_t* value)
	{
		___exportConsts_layoutsClassName_11 = value;
		Il2CppCodeGenWriteBarrier((&___exportConsts_layoutsClassName_11), value);
	}

	inline static int32_t get_offset_of_exportConsts_players_12() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_players_12)); }
	inline bool get_exportConsts_players_12() const { return ___exportConsts_players_12; }
	inline bool* get_address_of_exportConsts_players_12() { return &___exportConsts_players_12; }
	inline void set_exportConsts_players_12(bool value)
	{
		___exportConsts_players_12 = value;
	}

	inline static int32_t get_offset_of_exportConsts_playersClassName_13() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_playersClassName_13)); }
	inline String_t* get_exportConsts_playersClassName_13() const { return ___exportConsts_playersClassName_13; }
	inline String_t** get_address_of_exportConsts_playersClassName_13() { return &___exportConsts_playersClassName_13; }
	inline void set_exportConsts_playersClassName_13(String_t* value)
	{
		___exportConsts_playersClassName_13 = value;
		Il2CppCodeGenWriteBarrier((&___exportConsts_playersClassName_13), value);
	}

	inline static int32_t get_offset_of_exportConsts_inputBehaviors_14() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_inputBehaviors_14)); }
	inline bool get_exportConsts_inputBehaviors_14() const { return ___exportConsts_inputBehaviors_14; }
	inline bool* get_address_of_exportConsts_inputBehaviors_14() { return &___exportConsts_inputBehaviors_14; }
	inline void set_exportConsts_inputBehaviors_14(bool value)
	{
		___exportConsts_inputBehaviors_14 = value;
	}

	inline static int32_t get_offset_of_exportConsts_inputBehaviorsClassName_15() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_inputBehaviorsClassName_15)); }
	inline String_t* get_exportConsts_inputBehaviorsClassName_15() const { return ___exportConsts_inputBehaviorsClassName_15; }
	inline String_t** get_address_of_exportConsts_inputBehaviorsClassName_15() { return &___exportConsts_inputBehaviorsClassName_15; }
	inline void set_exportConsts_inputBehaviorsClassName_15(String_t* value)
	{
		___exportConsts_inputBehaviorsClassName_15 = value;
		Il2CppCodeGenWriteBarrier((&___exportConsts_inputBehaviorsClassName_15), value);
	}

	inline static int32_t get_offset_of_exportConsts_customControllers_16() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_customControllers_16)); }
	inline bool get_exportConsts_customControllers_16() const { return ___exportConsts_customControllers_16; }
	inline bool* get_address_of_exportConsts_customControllers_16() { return &___exportConsts_customControllers_16; }
	inline void set_exportConsts_customControllers_16(bool value)
	{
		___exportConsts_customControllers_16 = value;
	}

	inline static int32_t get_offset_of_exportConsts_customControllersClassName_17() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_customControllersClassName_17)); }
	inline String_t* get_exportConsts_customControllersClassName_17() const { return ___exportConsts_customControllersClassName_17; }
	inline String_t** get_address_of_exportConsts_customControllersClassName_17() { return &___exportConsts_customControllersClassName_17; }
	inline void set_exportConsts_customControllersClassName_17(String_t* value)
	{
		___exportConsts_customControllersClassName_17 = value;
		Il2CppCodeGenWriteBarrier((&___exportConsts_customControllersClassName_17), value);
	}

	inline static int32_t get_offset_of_exportConsts_customControllersAxesClassName_18() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_customControllersAxesClassName_18)); }
	inline String_t* get_exportConsts_customControllersAxesClassName_18() const { return ___exportConsts_customControllersAxesClassName_18; }
	inline String_t** get_address_of_exportConsts_customControllersAxesClassName_18() { return &___exportConsts_customControllersAxesClassName_18; }
	inline void set_exportConsts_customControllersAxesClassName_18(String_t* value)
	{
		___exportConsts_customControllersAxesClassName_18 = value;
		Il2CppCodeGenWriteBarrier((&___exportConsts_customControllersAxesClassName_18), value);
	}

	inline static int32_t get_offset_of_exportConsts_customControllersButtonsClassName_19() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_customControllersButtonsClassName_19)); }
	inline String_t* get_exportConsts_customControllersButtonsClassName_19() const { return ___exportConsts_customControllersButtonsClassName_19; }
	inline String_t** get_address_of_exportConsts_customControllersButtonsClassName_19() { return &___exportConsts_customControllersButtonsClassName_19; }
	inline void set_exportConsts_customControllersButtonsClassName_19(String_t* value)
	{
		___exportConsts_customControllersButtonsClassName_19 = value;
		Il2CppCodeGenWriteBarrier((&___exportConsts_customControllersButtonsClassName_19), value);
	}

	inline static int32_t get_offset_of_exportConsts_layoutManagerRuleSets_20() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_layoutManagerRuleSets_20)); }
	inline bool get_exportConsts_layoutManagerRuleSets_20() const { return ___exportConsts_layoutManagerRuleSets_20; }
	inline bool* get_address_of_exportConsts_layoutManagerRuleSets_20() { return &___exportConsts_layoutManagerRuleSets_20; }
	inline void set_exportConsts_layoutManagerRuleSets_20(bool value)
	{
		___exportConsts_layoutManagerRuleSets_20 = value;
	}

	inline static int32_t get_offset_of_exportConsts_layoutManagerRuleSetsClassName_21() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_layoutManagerRuleSetsClassName_21)); }
	inline String_t* get_exportConsts_layoutManagerRuleSetsClassName_21() const { return ___exportConsts_layoutManagerRuleSetsClassName_21; }
	inline String_t** get_address_of_exportConsts_layoutManagerRuleSetsClassName_21() { return &___exportConsts_layoutManagerRuleSetsClassName_21; }
	inline void set_exportConsts_layoutManagerRuleSetsClassName_21(String_t* value)
	{
		___exportConsts_layoutManagerRuleSetsClassName_21 = value;
		Il2CppCodeGenWriteBarrier((&___exportConsts_layoutManagerRuleSetsClassName_21), value);
	}

	inline static int32_t get_offset_of_exportConsts_mapEnablerRuleSets_22() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_mapEnablerRuleSets_22)); }
	inline bool get_exportConsts_mapEnablerRuleSets_22() const { return ___exportConsts_mapEnablerRuleSets_22; }
	inline bool* get_address_of_exportConsts_mapEnablerRuleSets_22() { return &___exportConsts_mapEnablerRuleSets_22; }
	inline void set_exportConsts_mapEnablerRuleSets_22(bool value)
	{
		___exportConsts_mapEnablerRuleSets_22 = value;
	}

	inline static int32_t get_offset_of_exportConsts_mapEnablerRuleSetsClassName_23() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_mapEnablerRuleSetsClassName_23)); }
	inline String_t* get_exportConsts_mapEnablerRuleSetsClassName_23() const { return ___exportConsts_mapEnablerRuleSetsClassName_23; }
	inline String_t** get_address_of_exportConsts_mapEnablerRuleSetsClassName_23() { return &___exportConsts_mapEnablerRuleSetsClassName_23; }
	inline void set_exportConsts_mapEnablerRuleSetsClassName_23(String_t* value)
	{
		___exportConsts_mapEnablerRuleSetsClassName_23 = value;
		Il2CppCodeGenWriteBarrier((&___exportConsts_mapEnablerRuleSetsClassName_23), value);
	}

	inline static int32_t get_offset_of_exportConsts_allCapsConstantNames_24() { return static_cast<int32_t>(offsetof(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF, ___exportConsts_allCapsConstantNames_24)); }
	inline bool get_exportConsts_allCapsConstantNames_24() const { return ___exportConsts_allCapsConstantNames_24; }
	inline bool* get_address_of_exportConsts_allCapsConstantNames_24() { return &___exportConsts_allCapsConstantNames_24; }
	inline void set_exportConsts_allCapsConstantNames_24(bool value)
	{
		___exportConsts_allCapsConstantNames_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORVARS_T2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF_H
#ifndef ETIWYEYEVXHGDDZWPHVPQNWKBYX_T9ED19CF8345F9FD567470585E1F5E2F949935321_H
#define ETIWYEYEVXHGDDZWPHVPQNWKBYX_T9ED19CF8345F9FD567470585E1F5E2F949935321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ConfigVars_EtiwYEyEvxhGdDZwPhVpQNWkBYX
struct  EtiwYEyEvxhGdDZwPhVpQNWkBYX_t9ED19CF8345F9FD567470585E1F5E2F949935321  : public RuntimeObject
{
public:
	// System.Func`1<Rewired.Data.ConfigVars_PlatformVars> Rewired.Data.ConfigVars_EtiwYEyEvxhGdDZwPhVpQNWkBYX::ImAEWBFqgIbvkLGhFYjFCwIBMTg
	Func_1_t04F7F7CE71B3BD573D381237377FC5BC6251348F * ___ImAEWBFqgIbvkLGhFYjFCwIBMTg_0;
	// System.String Rewired.Data.ConfigVars_EtiwYEyEvxhGdDZwPhVpQNWkBYX::uxGkAcyjrmYqmmrwmUKivgesJVl
	String_t* ___uxGkAcyjrmYqmmrwmUKivgesJVl_1;

public:
	inline static int32_t get_offset_of_ImAEWBFqgIbvkLGhFYjFCwIBMTg_0() { return static_cast<int32_t>(offsetof(EtiwYEyEvxhGdDZwPhVpQNWkBYX_t9ED19CF8345F9FD567470585E1F5E2F949935321, ___ImAEWBFqgIbvkLGhFYjFCwIBMTg_0)); }
	inline Func_1_t04F7F7CE71B3BD573D381237377FC5BC6251348F * get_ImAEWBFqgIbvkLGhFYjFCwIBMTg_0() const { return ___ImAEWBFqgIbvkLGhFYjFCwIBMTg_0; }
	inline Func_1_t04F7F7CE71B3BD573D381237377FC5BC6251348F ** get_address_of_ImAEWBFqgIbvkLGhFYjFCwIBMTg_0() { return &___ImAEWBFqgIbvkLGhFYjFCwIBMTg_0; }
	inline void set_ImAEWBFqgIbvkLGhFYjFCwIBMTg_0(Func_1_t04F7F7CE71B3BD573D381237377FC5BC6251348F * value)
	{
		___ImAEWBFqgIbvkLGhFYjFCwIBMTg_0 = value;
		Il2CppCodeGenWriteBarrier((&___ImAEWBFqgIbvkLGhFYjFCwIBMTg_0), value);
	}

	inline static int32_t get_offset_of_uxGkAcyjrmYqmmrwmUKivgesJVl_1() { return static_cast<int32_t>(offsetof(EtiwYEyEvxhGdDZwPhVpQNWkBYX_t9ED19CF8345F9FD567470585E1F5E2F949935321, ___uxGkAcyjrmYqmmrwmUKivgesJVl_1)); }
	inline String_t* get_uxGkAcyjrmYqmmrwmUKivgesJVl_1() const { return ___uxGkAcyjrmYqmmrwmUKivgesJVl_1; }
	inline String_t** get_address_of_uxGkAcyjrmYqmmrwmUKivgesJVl_1() { return &___uxGkAcyjrmYqmmrwmUKivgesJVl_1; }
	inline void set_uxGkAcyjrmYqmmrwmUKivgesJVl_1(String_t* value)
	{
		___uxGkAcyjrmYqmmrwmUKivgesJVl_1 = value;
		Il2CppCodeGenWriteBarrier((&___uxGkAcyjrmYqmmrwmUKivgesJVl_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETIWYEYEVXHGDDZWPHVPQNWKBYX_T9ED19CF8345F9FD567470585E1F5E2F949935321_H
#ifndef PLATFORMVARS_T9E023462BBB87694D35C6951AF767B22A20FEFFA_H
#define PLATFORMVARS_T9E023462BBB87694D35C6951AF767B22A20FEFFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ConfigVars_PlatformVars
struct  PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Data.ConfigVars_PlatformVars::disableKeyboard
	bool ___disableKeyboard_0;
	// System.Boolean Rewired.Data.ConfigVars_PlatformVars::ignoreInputWhenAppNotInFocus
	bool ___ignoreInputWhenAppNotInFocus_1;

public:
	inline static int32_t get_offset_of_disableKeyboard_0() { return static_cast<int32_t>(offsetof(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA, ___disableKeyboard_0)); }
	inline bool get_disableKeyboard_0() const { return ___disableKeyboard_0; }
	inline bool* get_address_of_disableKeyboard_0() { return &___disableKeyboard_0; }
	inline void set_disableKeyboard_0(bool value)
	{
		___disableKeyboard_0 = value;
	}

	inline static int32_t get_offset_of_ignoreInputWhenAppNotInFocus_1() { return static_cast<int32_t>(offsetof(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA, ___ignoreInputWhenAppNotInFocus_1)); }
	inline bool get_ignoreInputWhenAppNotInFocus_1() const { return ___ignoreInputWhenAppNotInFocus_1; }
	inline bool* get_address_of_ignoreInputWhenAppNotInFocus_1() { return &___ignoreInputWhenAppNotInFocus_1; }
	inline void set_ignoreInputWhenAppNotInFocus_1(bool value)
	{
		___ignoreInputWhenAppNotInFocus_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMVARS_T9E023462BBB87694D35C6951AF767B22A20FEFFA_H
#ifndef CUSTOMCONTROLLER_EDITOR_T3C1F05B017977306A26C6D50DCBC737FD4B7BD1E_H
#define CUSTOMCONTROLLER_EDITOR_T3C1F05B017977306A26C6D50DCBC737FD4B7BD1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.CustomController_Editor
struct  CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E  : public RuntimeObject
{
public:
	// System.String Rewired.Data.CustomController_Editor::_name
	String_t* ____name_0;
	// System.String Rewired.Data.CustomController_Editor::_descriptiveName
	String_t* ____descriptiveName_1;
	// System.Int32 Rewired.Data.CustomController_Editor::_id
	int32_t ____id_2;
	// System.String Rewired.Data.CustomController_Editor::_typeGuidString
	String_t* ____typeGuidString_3;
	// System.Collections.Generic.List`1<Rewired.ControllerElementIdentifier> Rewired.Data.CustomController_Editor::_elementIdentifiers
	List_1_t4D4CD18041A17FC7805220DF92DF87F2FE40ED1C * ____elementIdentifiers_4;
	// System.Collections.Generic.List`1<Rewired.Data.CustomController_Editor_Axis> Rewired.Data.CustomController_Editor::_axes
	List_1_t780589CD1E23A6E1051C3ED014CD54D57B46B34B * ____axes_5;
	// System.Collections.Generic.List`1<Rewired.Data.CustomController_Editor_Button> Rewired.Data.CustomController_Editor::_buttons
	List_1_tF154A2939A16D295695D2DFAACA1A83DB0A2D531 * ____buttons_6;
	// System.Int32 Rewired.Data.CustomController_Editor::_elementIdentifierIdCounter
	int32_t ____elementIdentifierIdCounter_7;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__descriptiveName_1() { return static_cast<int32_t>(offsetof(CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E, ____descriptiveName_1)); }
	inline String_t* get__descriptiveName_1() const { return ____descriptiveName_1; }
	inline String_t** get_address_of__descriptiveName_1() { return &____descriptiveName_1; }
	inline void set__descriptiveName_1(String_t* value)
	{
		____descriptiveName_1 = value;
		Il2CppCodeGenWriteBarrier((&____descriptiveName_1), value);
	}

	inline static int32_t get_offset_of__id_2() { return static_cast<int32_t>(offsetof(CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E, ____id_2)); }
	inline int32_t get__id_2() const { return ____id_2; }
	inline int32_t* get_address_of__id_2() { return &____id_2; }
	inline void set__id_2(int32_t value)
	{
		____id_2 = value;
	}

	inline static int32_t get_offset_of__typeGuidString_3() { return static_cast<int32_t>(offsetof(CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E, ____typeGuidString_3)); }
	inline String_t* get__typeGuidString_3() const { return ____typeGuidString_3; }
	inline String_t** get_address_of__typeGuidString_3() { return &____typeGuidString_3; }
	inline void set__typeGuidString_3(String_t* value)
	{
		____typeGuidString_3 = value;
		Il2CppCodeGenWriteBarrier((&____typeGuidString_3), value);
	}

	inline static int32_t get_offset_of__elementIdentifiers_4() { return static_cast<int32_t>(offsetof(CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E, ____elementIdentifiers_4)); }
	inline List_1_t4D4CD18041A17FC7805220DF92DF87F2FE40ED1C * get__elementIdentifiers_4() const { return ____elementIdentifiers_4; }
	inline List_1_t4D4CD18041A17FC7805220DF92DF87F2FE40ED1C ** get_address_of__elementIdentifiers_4() { return &____elementIdentifiers_4; }
	inline void set__elementIdentifiers_4(List_1_t4D4CD18041A17FC7805220DF92DF87F2FE40ED1C * value)
	{
		____elementIdentifiers_4 = value;
		Il2CppCodeGenWriteBarrier((&____elementIdentifiers_4), value);
	}

	inline static int32_t get_offset_of__axes_5() { return static_cast<int32_t>(offsetof(CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E, ____axes_5)); }
	inline List_1_t780589CD1E23A6E1051C3ED014CD54D57B46B34B * get__axes_5() const { return ____axes_5; }
	inline List_1_t780589CD1E23A6E1051C3ED014CD54D57B46B34B ** get_address_of__axes_5() { return &____axes_5; }
	inline void set__axes_5(List_1_t780589CD1E23A6E1051C3ED014CD54D57B46B34B * value)
	{
		____axes_5 = value;
		Il2CppCodeGenWriteBarrier((&____axes_5), value);
	}

	inline static int32_t get_offset_of__buttons_6() { return static_cast<int32_t>(offsetof(CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E, ____buttons_6)); }
	inline List_1_tF154A2939A16D295695D2DFAACA1A83DB0A2D531 * get__buttons_6() const { return ____buttons_6; }
	inline List_1_tF154A2939A16D295695D2DFAACA1A83DB0A2D531 ** get_address_of__buttons_6() { return &____buttons_6; }
	inline void set__buttons_6(List_1_tF154A2939A16D295695D2DFAACA1A83DB0A2D531 * value)
	{
		____buttons_6 = value;
		Il2CppCodeGenWriteBarrier((&____buttons_6), value);
	}

	inline static int32_t get_offset_of__elementIdentifierIdCounter_7() { return static_cast<int32_t>(offsetof(CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E, ____elementIdentifierIdCounter_7)); }
	inline int32_t get__elementIdentifierIdCounter_7() const { return ____elementIdentifierIdCounter_7; }
	inline int32_t* get_address_of__elementIdentifierIdCounter_7() { return &____elementIdentifierIdCounter_7; }
	inline void set__elementIdentifierIdCounter_7(int32_t value)
	{
		____elementIdentifierIdCounter_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLER_EDITOR_T3C1F05B017977306A26C6D50DCBC737FD4B7BD1E_H
#ifndef ELEMENT_T4C80356DECB9FF4357706FB02B4C85C71AC29D12_H
#define ELEMENT_T4C80356DECB9FF4357706FB02B4C85C71AC29D12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.CustomController_Editor_Element
struct  Element_t4C80356DECB9FF4357706FB02B4C85C71AC29D12  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.CustomController_Editor_Element::elementIdentifierId
	int32_t ___elementIdentifierId_0;
	// System.String Rewired.Data.CustomController_Editor_Element::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_elementIdentifierId_0() { return static_cast<int32_t>(offsetof(Element_t4C80356DECB9FF4357706FB02B4C85C71AC29D12, ___elementIdentifierId_0)); }
	inline int32_t get_elementIdentifierId_0() const { return ___elementIdentifierId_0; }
	inline int32_t* get_address_of_elementIdentifierId_0() { return &___elementIdentifierId_0; }
	inline void set_elementIdentifierId_0(int32_t value)
	{
		___elementIdentifierId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Element_t4C80356DECB9FF4357706FB02B4C85C71AC29D12, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T4C80356DECB9FF4357706FB02B4C85C71AC29D12_H
#ifndef HFFTEQVRDYKNPNJMKTQLXCUKADL_T4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1_H
#define HFFTEQVRDYKNPNJMKTQLXCUKADL_T4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.CustomController_Editor_HFfTEQVrDyKNPNJmkTqlxcUKadL
struct  HFfTEQVrDyKNPNJmkTqlxcUKadL_t4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1  : public RuntimeObject
{
public:
	// Rewired.ControllerElementIdentifier Rewired.Data.CustomController_Editor_HFfTEQVrDyKNPNJmkTqlxcUKadL::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.CustomController_Editor_HFfTEQVrDyKNPNJmkTqlxcUKadL::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.CustomController_Editor_HFfTEQVrDyKNPNJmkTqlxcUKadL::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.CustomController_Editor Rewired.Data.CustomController_Editor_HFfTEQVrDyKNPNJmkTqlxcUKadL::DKaFTWTMhFeLQHDExnDMRxZfmROL
	CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.CustomController_Editor_HFfTEQVrDyKNPNJmkTqlxcUKadL::okMFupfyoBNZDyKMyvAtwGIUiAd
	int32_t ___okMFupfyoBNZDyKMyvAtwGIUiAd_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(HFfTEQVrDyKNPNJmkTqlxcUKadL_t4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(HFfTEQVrDyKNPNJmkTqlxcUKadL_t4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(HFfTEQVrDyKNPNJmkTqlxcUKadL_t4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(HFfTEQVrDyKNPNJmkTqlxcUKadL_t4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4() { return static_cast<int32_t>(offsetof(HFfTEQVrDyKNPNJmkTqlxcUKadL_t4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1, ___okMFupfyoBNZDyKMyvAtwGIUiAd_4)); }
	inline int32_t get_okMFupfyoBNZDyKMyvAtwGIUiAd_4() const { return ___okMFupfyoBNZDyKMyvAtwGIUiAd_4; }
	inline int32_t* get_address_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4() { return &___okMFupfyoBNZDyKMyvAtwGIUiAd_4; }
	inline void set_okMFupfyoBNZDyKMyvAtwGIUiAd_4(int32_t value)
	{
		___okMFupfyoBNZDyKMyvAtwGIUiAd_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HFFTEQVRDYKNPNJMKTQLXCUKADL_T4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1_H
#ifndef ACTIONCATEGORYMAP_T7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F_H
#define ACTIONCATEGORYMAP_T7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ActionCategoryMap
struct  ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Rewired.Data.Mapping.ActionCategoryMap_Entry> Rewired.Data.Mapping.ActionCategoryMap::list
	List_1_tAE81ACAEB31146DF9658E07FCEE561860901D114 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F, ___list_0)); }
	inline List_1_tAE81ACAEB31146DF9658E07FCEE561860901D114 * get_list_0() const { return ___list_0; }
	inline List_1_tAE81ACAEB31146DF9658E07FCEE561860901D114 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tAE81ACAEB31146DF9658E07FCEE561860901D114 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONCATEGORYMAP_T7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F_H
#ifndef ENTRY_T87C36EB768B9C1EA06D2C2FC073B650711241DAC_H
#define ENTRY_T87C36EB768B9C1EA06D2C2FC073B650711241DAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ActionCategoryMap_Entry
struct  Entry_t87C36EB768B9C1EA06D2C2FC073B650711241DAC  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.ActionCategoryMap_Entry::categoryId
	int32_t ___categoryId_0;
	// System.Collections.Generic.List`1<System.Int32> Rewired.Data.Mapping.ActionCategoryMap_Entry::actionIds
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___actionIds_1;

public:
	inline static int32_t get_offset_of_categoryId_0() { return static_cast<int32_t>(offsetof(Entry_t87C36EB768B9C1EA06D2C2FC073B650711241DAC, ___categoryId_0)); }
	inline int32_t get_categoryId_0() const { return ___categoryId_0; }
	inline int32_t* get_address_of_categoryId_0() { return &___categoryId_0; }
	inline void set_categoryId_0(int32_t value)
	{
		___categoryId_0 = value;
	}

	inline static int32_t get_offset_of_actionIds_1() { return static_cast<int32_t>(offsetof(Entry_t87C36EB768B9C1EA06D2C2FC073B650711241DAC, ___actionIds_1)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_actionIds_1() const { return ___actionIds_1; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_actionIds_1() { return &___actionIds_1; }
	inline void set_actionIds_1(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___actionIds_1 = value;
		Il2CppCodeGenWriteBarrier((&___actionIds_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T87C36EB768B9C1EA06D2C2FC073B650711241DAC_H
#ifndef MJCVRWENRDTCMLNJDIDVAICKGXDG_T73CF85D3572153DF8DB71136ECF91EA684355ABA_H
#define MJCVRWENRDTCMLNJDIDVAICKGXDG_T73CF85D3572153DF8DB71136ECF91EA684355ABA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ActionCategoryMap_Entry_MJcVRwenrdtcMlnjDIdvaiCKGxDg
struct  MJcVRwenrdtcMlnjDIdvaiCKGxDg_t73CF85D3572153DF8DB71136ECF91EA684355ABA  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.ActionCategoryMap_Entry_MJcVRwenrdtcMlnjDIdvaiCKGxDg::NOArrsNdfKDShNFftfbPqYDzhpq
	int32_t ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.ActionCategoryMap_Entry_MJcVRwenrdtcMlnjDIdvaiCKGxDg::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.ActionCategoryMap_Entry_MJcVRwenrdtcMlnjDIdvaiCKGxDg::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.ActionCategoryMap_Entry Rewired.Data.Mapping.ActionCategoryMap_Entry_MJcVRwenrdtcMlnjDIdvaiCKGxDg::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Entry_t87C36EB768B9C1EA06D2C2FC073B650711241DAC * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.ActionCategoryMap_Entry_MJcVRwenrdtcMlnjDIdvaiCKGxDg::CXbAsCcpaRocwlxePSqDsZRxJTuA
	int32_t ___CXbAsCcpaRocwlxePSqDsZRxJTuA_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(MJcVRwenrdtcMlnjDIdvaiCKGxDg_t73CF85D3572153DF8DB71136ECF91EA684355ABA, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline int32_t get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline int32_t* get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(int32_t value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(MJcVRwenrdtcMlnjDIdvaiCKGxDg_t73CF85D3572153DF8DB71136ECF91EA684355ABA, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(MJcVRwenrdtcMlnjDIdvaiCKGxDg_t73CF85D3572153DF8DB71136ECF91EA684355ABA, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(MJcVRwenrdtcMlnjDIdvaiCKGxDg_t73CF85D3572153DF8DB71136ECF91EA684355ABA, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Entry_t87C36EB768B9C1EA06D2C2FC073B650711241DAC * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Entry_t87C36EB768B9C1EA06D2C2FC073B650711241DAC ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Entry_t87C36EB768B9C1EA06D2C2FC073B650711241DAC * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_CXbAsCcpaRocwlxePSqDsZRxJTuA_4() { return static_cast<int32_t>(offsetof(MJcVRwenrdtcMlnjDIdvaiCKGxDg_t73CF85D3572153DF8DB71136ECF91EA684355ABA, ___CXbAsCcpaRocwlxePSqDsZRxJTuA_4)); }
	inline int32_t get_CXbAsCcpaRocwlxePSqDsZRxJTuA_4() const { return ___CXbAsCcpaRocwlxePSqDsZRxJTuA_4; }
	inline int32_t* get_address_of_CXbAsCcpaRocwlxePSqDsZRxJTuA_4() { return &___CXbAsCcpaRocwlxePSqDsZRxJTuA_4; }
	inline void set_CXbAsCcpaRocwlxePSqDsZRxJTuA_4(int32_t value)
	{
		___CXbAsCcpaRocwlxePSqDsZRxJTuA_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MJCVRWENRDTCMLNJDIDVAICKGXDG_T73CF85D3572153DF8DB71136ECF91EA684355ABA_H
#ifndef UEFGDVYWKKBMQZHTBDFHTGTDMZI_TCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F_H
#define UEFGDVYWKKBMQZHTBDFHTGTDMZI_TCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ActionCategoryMap_uEfGdvywKKBmqZHtBdfHTGtDMzI
struct  uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.ActionCategoryMap_uEfGdvywKKBmqZHtBdfHTGtDMzI::NOArrsNdfKDShNFftfbPqYDzhpq
	int32_t ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.ActionCategoryMap_uEfGdvywKKBmqZHtBdfHTGtDMzI::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.ActionCategoryMap_uEfGdvywKKBmqZHtBdfHTGtDMzI::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.ActionCategoryMap Rewired.Data.Mapping.ActionCategoryMap_uEfGdvywKKBmqZHtBdfHTGtDMzI::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.ActionCategoryMap_uEfGdvywKKBmqZHtBdfHTGtDMzI::OoFswlQbqBhHqGOCooflwYCYmYWF
	int32_t ___OoFswlQbqBhHqGOCooflwYCYmYWF_4;
	// System.Int32 Rewired.Data.Mapping.ActionCategoryMap_uEfGdvywKKBmqZHtBdfHTGtDMzI::pyCASHftKaJjucJBlmeeWQrdKuRk
	int32_t ___pyCASHftKaJjucJBlmeeWQrdKuRk_5;
	// System.Int32 Rewired.Data.Mapping.ActionCategoryMap_uEfGdvywKKBmqZHtBdfHTGtDMzI::bFEubZafxTexBBhbDuHMMulYJfbH
	int32_t ___bFEubZafxTexBBhbDuHMMulYJfbH_6;
	// System.Int32 Rewired.Data.Mapping.ActionCategoryMap_uEfGdvywKKBmqZHtBdfHTGtDMzI::IdhQMvrODBHtZqpemtLdtJCrDcUe
	int32_t ___IdhQMvrODBHtZqpemtLdtJCrDcUe_7;
	// System.Collections.Generic.IEnumerator`1<System.Int32> Rewired.Data.Mapping.ActionCategoryMap_uEfGdvywKKBmqZHtBdfHTGtDMzI::zCSdpqWqddovuMgwXAspBmdHeqjU
	RuntimeObject* ___zCSdpqWqddovuMgwXAspBmdHeqjU_8;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline int32_t get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline int32_t* get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(int32_t value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_OoFswlQbqBhHqGOCooflwYCYmYWF_4() { return static_cast<int32_t>(offsetof(uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F, ___OoFswlQbqBhHqGOCooflwYCYmYWF_4)); }
	inline int32_t get_OoFswlQbqBhHqGOCooflwYCYmYWF_4() const { return ___OoFswlQbqBhHqGOCooflwYCYmYWF_4; }
	inline int32_t* get_address_of_OoFswlQbqBhHqGOCooflwYCYmYWF_4() { return &___OoFswlQbqBhHqGOCooflwYCYmYWF_4; }
	inline void set_OoFswlQbqBhHqGOCooflwYCYmYWF_4(int32_t value)
	{
		___OoFswlQbqBhHqGOCooflwYCYmYWF_4 = value;
	}

	inline static int32_t get_offset_of_pyCASHftKaJjucJBlmeeWQrdKuRk_5() { return static_cast<int32_t>(offsetof(uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F, ___pyCASHftKaJjucJBlmeeWQrdKuRk_5)); }
	inline int32_t get_pyCASHftKaJjucJBlmeeWQrdKuRk_5() const { return ___pyCASHftKaJjucJBlmeeWQrdKuRk_5; }
	inline int32_t* get_address_of_pyCASHftKaJjucJBlmeeWQrdKuRk_5() { return &___pyCASHftKaJjucJBlmeeWQrdKuRk_5; }
	inline void set_pyCASHftKaJjucJBlmeeWQrdKuRk_5(int32_t value)
	{
		___pyCASHftKaJjucJBlmeeWQrdKuRk_5 = value;
	}

	inline static int32_t get_offset_of_bFEubZafxTexBBhbDuHMMulYJfbH_6() { return static_cast<int32_t>(offsetof(uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F, ___bFEubZafxTexBBhbDuHMMulYJfbH_6)); }
	inline int32_t get_bFEubZafxTexBBhbDuHMMulYJfbH_6() const { return ___bFEubZafxTexBBhbDuHMMulYJfbH_6; }
	inline int32_t* get_address_of_bFEubZafxTexBBhbDuHMMulYJfbH_6() { return &___bFEubZafxTexBBhbDuHMMulYJfbH_6; }
	inline void set_bFEubZafxTexBBhbDuHMMulYJfbH_6(int32_t value)
	{
		___bFEubZafxTexBBhbDuHMMulYJfbH_6 = value;
	}

	inline static int32_t get_offset_of_IdhQMvrODBHtZqpemtLdtJCrDcUe_7() { return static_cast<int32_t>(offsetof(uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F, ___IdhQMvrODBHtZqpemtLdtJCrDcUe_7)); }
	inline int32_t get_IdhQMvrODBHtZqpemtLdtJCrDcUe_7() const { return ___IdhQMvrODBHtZqpemtLdtJCrDcUe_7; }
	inline int32_t* get_address_of_IdhQMvrODBHtZqpemtLdtJCrDcUe_7() { return &___IdhQMvrODBHtZqpemtLdtJCrDcUe_7; }
	inline void set_IdhQMvrODBHtZqpemtLdtJCrDcUe_7(int32_t value)
	{
		___IdhQMvrODBHtZqpemtLdtJCrDcUe_7 = value;
	}

	inline static int32_t get_offset_of_zCSdpqWqddovuMgwXAspBmdHeqjU_8() { return static_cast<int32_t>(offsetof(uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F, ___zCSdpqWqddovuMgwXAspBmdHeqjU_8)); }
	inline RuntimeObject* get_zCSdpqWqddovuMgwXAspBmdHeqjU_8() const { return ___zCSdpqWqddovuMgwXAspBmdHeqjU_8; }
	inline RuntimeObject** get_address_of_zCSdpqWqddovuMgwXAspBmdHeqjU_8() { return &___zCSdpqWqddovuMgwXAspBmdHeqjU_8; }
	inline void set_zCSdpqWqddovuMgwXAspBmdHeqjU_8(RuntimeObject* value)
	{
		___zCSdpqWqddovuMgwXAspBmdHeqjU_8 = value;
		Il2CppCodeGenWriteBarrier((&___zCSdpqWqddovuMgwXAspBmdHeqjU_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UEFGDVYWKKBMQZHTBDFHTGTDMZI_TCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F_H
#ifndef ELEMENTS_BASE_T646A0ACACC7BEB9CE31C70CB74E59EF0312756F0_H
#define ELEMENTS_BASE_T646A0ACACC7BEB9CE31C70CB74E59EF0312756F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Elements_Base
struct  Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_BASE_T646A0ACACC7BEB9CE31C70CB74E59EF0312756F0_H
#ifndef MATCHINGCRITERIA_BASE_T9475B14364CDF4EB6C2585C9F29E9394954F3065_H
#define MATCHINGCRITERIA_BASE_T9475B14364CDF4EB6C2585C9F29E9394954F3065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base
struct  MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base::axisCount
	int32_t ___axisCount_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base::buttonCount
	int32_t ___buttonCount_1;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base::disabled
	bool ___disabled_2;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base::tag
	String_t* ___tag_3;

public:
	inline static int32_t get_offset_of_axisCount_0() { return static_cast<int32_t>(offsetof(MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065, ___axisCount_0)); }
	inline int32_t get_axisCount_0() const { return ___axisCount_0; }
	inline int32_t* get_address_of_axisCount_0() { return &___axisCount_0; }
	inline void set_axisCount_0(int32_t value)
	{
		___axisCount_0 = value;
	}

	inline static int32_t get_offset_of_buttonCount_1() { return static_cast<int32_t>(offsetof(MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065, ___buttonCount_1)); }
	inline int32_t get_buttonCount_1() const { return ___buttonCount_1; }
	inline int32_t* get_address_of_buttonCount_1() { return &___buttonCount_1; }
	inline void set_buttonCount_1(int32_t value)
	{
		___buttonCount_1 = value;
	}

	inline static int32_t get_offset_of_disabled_2() { return static_cast<int32_t>(offsetof(MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065, ___disabled_2)); }
	inline bool get_disabled_2() const { return ___disabled_2; }
	inline bool* get_address_of_disabled_2() { return &___disabled_2; }
	inline void set_disabled_2(bool value)
	{
		___disabled_2 = value;
	}

	inline static int32_t get_offset_of_tag_3() { return static_cast<int32_t>(offsetof(MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065, ___tag_3)); }
	inline String_t* get_tag_3() const { return ___tag_3; }
	inline String_t** get_address_of_tag_3() { return &___tag_3; }
	inline void set_tag_3(String_t* value)
	{
		___tag_3 = value;
		Il2CppCodeGenWriteBarrier((&___tag_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_BASE_T9475B14364CDF4EB6C2585C9F29E9394954F3065_H
#ifndef ELEMENTCOUNT_BASE_T73ADB57E490B282EC131BA5EAF3AFABDD618ECBC_H
#define ELEMENTCOUNT_BASE_T73ADB57E490B282EC131BA5EAF3AFABDD618ECBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base_ElementCount_Base
struct  ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base_ElementCount_Base::axisCount
	int32_t ___axisCount_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base_ElementCount_Base::buttonCount
	int32_t ___buttonCount_1;

public:
	inline static int32_t get_offset_of_axisCount_0() { return static_cast<int32_t>(offsetof(ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC, ___axisCount_0)); }
	inline int32_t get_axisCount_0() const { return ___axisCount_0; }
	inline int32_t* get_address_of_axisCount_0() { return &___axisCount_0; }
	inline void set_axisCount_0(int32_t value)
	{
		___axisCount_0 = value;
	}

	inline static int32_t get_offset_of_buttonCount_1() { return static_cast<int32_t>(offsetof(ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC, ___buttonCount_1)); }
	inline int32_t get_buttonCount_1() const { return ___buttonCount_1; }
	inline int32_t* get_address_of_buttonCount_1() { return &___buttonCount_1; }
	inline void set_buttonCount_1(int32_t value)
	{
		___buttonCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTCOUNT_BASE_T73ADB57E490B282EC131BA5EAF3AFABDD618ECBC_H
#ifndef PLATFORM_T0E28F5AD2665BAE68E81D3FF477CA1E6E1574789_H
#define PLATFORM_T0E28F5AD2665BAE68E81D3FF477CA1E6E1574789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform
struct  Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789  : public RuntimeObject
{
public:
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_Platform::description
	String_t* ___description_0;

public:
	inline static int32_t get_offset_of_description_0() { return static_cast<int32_t>(offsetof(Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789, ___description_0)); }
	inline String_t* get_description_0() const { return ___description_0; }
	inline String_t** get_address_of_description_0() { return &___description_0; }
	inline void set_description_0(String_t* value)
	{
		___description_0 = value;
		Il2CppCodeGenWriteBarrier((&___description_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T0E28F5AD2665BAE68E81D3FF477CA1E6E1574789_H
#ifndef RWBYTEFAGBVRLOMJBKPFTZWRFCD_TB1A300544AD09F3B8EB122374ADFC6161066E5BE_H
#define RWBYTEFAGBVRLOMJBKPFTZWRFCD_TB1A300544AD09F3B8EB122374ADFC6161066E5BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_rwbYTEfAGbvRLOMJbkPFTZWRFcD
struct  rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform Rewired.Data.Mapping.HardwareJoystickMap_Platform_rwbYTEfAGbvRLOMJbkPFTZWRFcD::NOArrsNdfKDShNFftfbPqYDzhpq
	Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_rwbYTEfAGbvRLOMJbkPFTZWRFcD::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_rwbYTEfAGbvRLOMJbkPFTZWRFcD::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform Rewired.Data.Mapping.HardwareJoystickMap_Platform_rwbYTEfAGbvRLOMJbkPFTZWRFcD::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Collections.Generic.IList`1<Rewired.Data.Mapping.HardwareJoystickMap_Platform> Rewired.Data.Mapping.HardwareJoystickMap_Platform_rwbYTEfAGbvRLOMJbkPFTZWRFcD::DNPeIdvKJFFEBSUOvaipMiRUthG
	RuntimeObject* ___DNPeIdvKJFFEBSUOvaipMiRUthG_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_rwbYTEfAGbvRLOMJbkPFTZWRFcD::sllTPBsbpmDyVEIgPNnVpQiDoIoV
	int32_t ___sllTPBsbpmDyVEIgPNnVpQiDoIoV_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_DNPeIdvKJFFEBSUOvaipMiRUthG_4() { return static_cast<int32_t>(offsetof(rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE, ___DNPeIdvKJFFEBSUOvaipMiRUthG_4)); }
	inline RuntimeObject* get_DNPeIdvKJFFEBSUOvaipMiRUthG_4() const { return ___DNPeIdvKJFFEBSUOvaipMiRUthG_4; }
	inline RuntimeObject** get_address_of_DNPeIdvKJFFEBSUOvaipMiRUthG_4() { return &___DNPeIdvKJFFEBSUOvaipMiRUthG_4; }
	inline void set_DNPeIdvKJFFEBSUOvaipMiRUthG_4(RuntimeObject* value)
	{
		___DNPeIdvKJFFEBSUOvaipMiRUthG_4 = value;
		Il2CppCodeGenWriteBarrier((&___DNPeIdvKJFFEBSUOvaipMiRUthG_4), value);
	}

	inline static int32_t get_offset_of_sllTPBsbpmDyVEIgPNnVpQiDoIoV_5() { return static_cast<int32_t>(offsetof(rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE, ___sllTPBsbpmDyVEIgPNnVpQiDoIoV_5)); }
	inline int32_t get_sllTPBsbpmDyVEIgPNnVpQiDoIoV_5() const { return ___sllTPBsbpmDyVEIgPNnVpQiDoIoV_5; }
	inline int32_t* get_address_of_sllTPBsbpmDyVEIgPNnVpQiDoIoV_5() { return &___sllTPBsbpmDyVEIgPNnVpQiDoIoV_5; }
	inline void set_sllTPBsbpmDyVEIgPNnVpQiDoIoV_5(int32_t value)
	{
		___sllTPBsbpmDyVEIgPNnVpQiDoIoV_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RWBYTEFAGBVRLOMJBKPFTZWRFCD_TB1A300544AD09F3B8EB122374ADFC6161066E5BE_H
#ifndef DARHVSBIUOHHKRDYDFGMIMAKZSDI_T35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630_H
#define DARHVSBIUOHHKRDYDFGMIMAKZSDI_T35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_DArhVSBIuOHHKRdyDFgmiMakZSDI
struct  DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_DArhVSBIuOHHKRdyDFgmiMakZSDI::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_DArhVSBIuOHHKRdyDFgmiMakZSDI::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_DArhVSBIuOHHKRdyDFgmiMakZSDI::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_DArhVSBIuOHHKRdyDFgmiMakZSDI::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_DArhVSBIuOHHKRdyDFgmiMakZSDI::cHJcLuiUAnRXodxmLILWuKXgXcOO
	int32_t ___cHJcLuiUAnRXodxmLILWuKXgXcOO_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_DArhVSBIuOHHKRdyDFgmiMakZSDI::ZRfecGkoINAkZyMuJGesYKGoGOmG
	int32_t ___ZRfecGkoINAkZyMuJGesYKGoGOmG_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_cHJcLuiUAnRXodxmLILWuKXgXcOO_4() { return static_cast<int32_t>(offsetof(DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630, ___cHJcLuiUAnRXodxmLILWuKXgXcOO_4)); }
	inline int32_t get_cHJcLuiUAnRXodxmLILWuKXgXcOO_4() const { return ___cHJcLuiUAnRXodxmLILWuKXgXcOO_4; }
	inline int32_t* get_address_of_cHJcLuiUAnRXodxmLILWuKXgXcOO_4() { return &___cHJcLuiUAnRXodxmLILWuKXgXcOO_4; }
	inline void set_cHJcLuiUAnRXodxmLILWuKXgXcOO_4(int32_t value)
	{
		___cHJcLuiUAnRXodxmLILWuKXgXcOO_4 = value;
	}

	inline static int32_t get_offset_of_ZRfecGkoINAkZyMuJGesYKGoGOmG_5() { return static_cast<int32_t>(offsetof(DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630, ___ZRfecGkoINAkZyMuJGesYKGoGOmG_5)); }
	inline int32_t get_ZRfecGkoINAkZyMuJGesYKGoGOmG_5() const { return ___ZRfecGkoINAkZyMuJGesYKGoGOmG_5; }
	inline int32_t* get_address_of_ZRfecGkoINAkZyMuJGesYKGoGOmG_5() { return &___ZRfecGkoINAkZyMuJGesYKGoGOmG_5; }
	inline void set_ZRfecGkoINAkZyMuJGesYKGoGOmG_5(int32_t value)
	{
		___ZRfecGkoINAkZyMuJGesYKGoGOmG_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DARHVSBIUOHHKRDYDFGMIMAKZSDI_T35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630_H
#ifndef WQMAKTEPTIJIVHEBVUTMBALIERX_T7A72BA7412CF7C38E78EE69AB15373C0AE8B9777_H
#define WQMAKTEPTIJIVHEBVUTMBALIERX_T7A72BA7412CF7C38E78EE69AB15373C0AE8B9777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements_WqmaKTePtijIvhEbVutMbAlIErx
struct  WqmaKTePtijIvhEbVutMbAlIErx_t7A72BA7412CF7C38E78EE69AB15373C0AE8B9777  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements_WqmaKTePtijIvhEbVutMbAlIErx::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements_WqmaKTePtijIvhEbVutMbAlIErx::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements_WqmaKTePtijIvhEbVutMbAlIErx::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements_WqmaKTePtijIvhEbVutMbAlIErx::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements_WqmaKTePtijIvhEbVutMbAlIErx::ZwruVSrOjQwKqPazqaarhnUAxKPR
	int32_t ___ZwruVSrOjQwKqPazqaarhnUAxKPR_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(WqmaKTePtijIvhEbVutMbAlIErx_t7A72BA7412CF7C38E78EE69AB15373C0AE8B9777, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(WqmaKTePtijIvhEbVutMbAlIErx_t7A72BA7412CF7C38E78EE69AB15373C0AE8B9777, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(WqmaKTePtijIvhEbVutMbAlIErx_t7A72BA7412CF7C38E78EE69AB15373C0AE8B9777, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(WqmaKTePtijIvhEbVutMbAlIErx_t7A72BA7412CF7C38E78EE69AB15373C0AE8B9777, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_ZwruVSrOjQwKqPazqaarhnUAxKPR_4() { return static_cast<int32_t>(offsetof(WqmaKTePtijIvhEbVutMbAlIErx_t7A72BA7412CF7C38E78EE69AB15373C0AE8B9777, ___ZwruVSrOjQwKqPazqaarhnUAxKPR_4)); }
	inline int32_t get_ZwruVSrOjQwKqPazqaarhnUAxKPR_4() const { return ___ZwruVSrOjQwKqPazqaarhnUAxKPR_4; }
	inline int32_t* get_address_of_ZwruVSrOjQwKqPazqaarhnUAxKPR_4() { return &___ZwruVSrOjQwKqPazqaarhnUAxKPR_4; }
	inline void set_ZwruVSrOjQwKqPazqaarhnUAxKPR_4(int32_t value)
	{
		___ZwruVSrOjQwKqPazqaarhnUAxKPR_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WQMAKTEPTIJIVHEBVUTMBALIERX_T7A72BA7412CF7C38E78EE69AB15373C0AE8B9777_H
#ifndef UISVFGTXFKODCIGBUCWYXSORARFH_TD0A2A74AE5E4500854EF6E705670E7E18C59A232_H
#define UISVFGTXFKODCIGBUCWYXSORARFH_TD0A2A74AE5E4500854EF6E705670E7E18C59A232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements_uIsVFgtXfKodcIgBUCwYxSoRaRFh
struct  uIsVFgtXfKodcIgBUCwYxSoRaRFh_tD0A2A74AE5E4500854EF6E705670E7E18C59A232  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements_uIsVFgtXfKodcIgBUCwYxSoRaRFh::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements_uIsVFgtXfKodcIgBUCwYxSoRaRFh::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements_uIsVFgtXfKodcIgBUCwYxSoRaRFh::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements_uIsVFgtXfKodcIgBUCwYxSoRaRFh::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements_uIsVFgtXfKodcIgBUCwYxSoRaRFh::eLAERvBUfLIWiIfMBoNRaRMTEvp
	int32_t ___eLAERvBUfLIWiIfMBoNRaRMTEvp_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(uIsVFgtXfKodcIgBUCwYxSoRaRFh_tD0A2A74AE5E4500854EF6E705670E7E18C59A232, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(uIsVFgtXfKodcIgBUCwYxSoRaRFh_tD0A2A74AE5E4500854EF6E705670E7E18C59A232, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(uIsVFgtXfKodcIgBUCwYxSoRaRFh_tD0A2A74AE5E4500854EF6E705670E7E18C59A232, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(uIsVFgtXfKodcIgBUCwYxSoRaRFh_tD0A2A74AE5E4500854EF6E705670E7E18C59A232, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_eLAERvBUfLIWiIfMBoNRaRMTEvp_4() { return static_cast<int32_t>(offsetof(uIsVFgtXfKodcIgBUCwYxSoRaRFh_tD0A2A74AE5E4500854EF6E705670E7E18C59A232, ___eLAERvBUfLIWiIfMBoNRaRMTEvp_4)); }
	inline int32_t get_eLAERvBUfLIWiIfMBoNRaRMTEvp_4() const { return ___eLAERvBUfLIWiIfMBoNRaRMTEvp_4; }
	inline int32_t* get_address_of_eLAERvBUfLIWiIfMBoNRaRMTEvp_4() { return &___eLAERvBUfLIWiIfMBoNRaRMTEvp_4; }
	inline void set_eLAERvBUfLIWiIfMBoNRaRMTEvp_4(int32_t value)
	{
		___eLAERvBUfLIWiIfMBoNRaRMTEvp_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISVFGTXFKODCIGBUCWYXSORARFH_TD0A2A74AE5E4500854EF6E705670E7E18C59A232_H
#ifndef FYKVHJSRSALDQMIZEHJHYSQMVSZ_TA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651_H
#define FYKVHJSRSALDQMIZEHJHYSQMVSZ_TA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_FykVHJSRsAldQmIZEhJHYSqmVsz
struct  FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_FykVHJSRsAldQmIZEhJHYSqmVsz::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_FykVHJSRsAldQmIZEhJHYSqmVsz::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_FykVHJSRsAldQmIZEhJHYSqmVsz::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_FykVHJSRsAldQmIZEhJHYSqmVsz::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_FykVHJSRsAldQmIZEhJHYSqmVsz::rbzmaknNVQXmtceEExhcrkqjdXx
	int32_t ___rbzmaknNVQXmtceEExhcrkqjdXx_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_FykVHJSRsAldQmIZEhJHYSqmVsz::bbylexxHkiPVNjmmyFKFxUlOenA
	int32_t ___bbylexxHkiPVNjmmyFKFxUlOenA_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_rbzmaknNVQXmtceEExhcrkqjdXx_4() { return static_cast<int32_t>(offsetof(FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651, ___rbzmaknNVQXmtceEExhcrkqjdXx_4)); }
	inline int32_t get_rbzmaknNVQXmtceEExhcrkqjdXx_4() const { return ___rbzmaknNVQXmtceEExhcrkqjdXx_4; }
	inline int32_t* get_address_of_rbzmaknNVQXmtceEExhcrkqjdXx_4() { return &___rbzmaknNVQXmtceEExhcrkqjdXx_4; }
	inline void set_rbzmaknNVQXmtceEExhcrkqjdXx_4(int32_t value)
	{
		___rbzmaknNVQXmtceEExhcrkqjdXx_4 = value;
	}

	inline static int32_t get_offset_of_bbylexxHkiPVNjmmyFKFxUlOenA_5() { return static_cast<int32_t>(offsetof(FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651, ___bbylexxHkiPVNjmmyFKFxUlOenA_5)); }
	inline int32_t get_bbylexxHkiPVNjmmyFKFxUlOenA_5() const { return ___bbylexxHkiPVNjmmyFKFxUlOenA_5; }
	inline int32_t* get_address_of_bbylexxHkiPVNjmmyFKFxUlOenA_5() { return &___bbylexxHkiPVNjmmyFKFxUlOenA_5; }
	inline void set_bbylexxHkiPVNjmmyFKFxUlOenA_5(int32_t value)
	{
		___bbylexxHkiPVNjmmyFKFxUlOenA_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FYKVHJSRSALDQMIZEHJHYSQMVSZ_TA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651_H
#ifndef ELEMENT_T4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B_H
#define ELEMENT_T4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Element
struct  Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.CustomCalculation Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Element::customCalculation
	CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * ___customCalculation_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Element::customCalculationSourceData
	CustomCalculationSourceDataU5BU5D_t034AAEDA41E4C78D3D656BBE7D542F430FDB5CC9* ___customCalculationSourceData_1;

public:
	inline static int32_t get_offset_of_customCalculation_0() { return static_cast<int32_t>(offsetof(Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B, ___customCalculation_0)); }
	inline CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * get_customCalculation_0() const { return ___customCalculation_0; }
	inline CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 ** get_address_of_customCalculation_0() { return &___customCalculation_0; }
	inline void set_customCalculation_0(CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * value)
	{
		___customCalculation_0 = value;
		Il2CppCodeGenWriteBarrier((&___customCalculation_0), value);
	}

	inline static int32_t get_offset_of_customCalculationSourceData_1() { return static_cast<int32_t>(offsetof(Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B, ___customCalculationSourceData_1)); }
	inline CustomCalculationSourceDataU5BU5D_t034AAEDA41E4C78D3D656BBE7D542F430FDB5CC9* get_customCalculationSourceData_1() const { return ___customCalculationSourceData_1; }
	inline CustomCalculationSourceDataU5BU5D_t034AAEDA41E4C78D3D656BBE7D542F430FDB5CC9** get_address_of_customCalculationSourceData_1() { return &___customCalculationSourceData_1; }
	inline void set_customCalculationSourceData_1(CustomCalculationSourceDataU5BU5D_t034AAEDA41E4C78D3D656BBE7D542F430FDB5CC9* value)
	{
		___customCalculationSourceData_1 = value;
		Il2CppCodeGenWriteBarrier((&___customCalculationSourceData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B_H
#ifndef VIDPID_TD48EA2DDFAF98EAD7C13F962D730E15A6D13D6B2_H
#define VIDPID_TD48EA2DDFAF98EAD7C13F962D730E15A6D13D6B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_VidPid
struct  VidPid_tD48EA2DDFAF98EAD7C13F962D730E15A6D13D6B2  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_VidPid::vendorId
	int32_t ___vendorId_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_VidPid::productId
	int32_t ___productId_1;

public:
	inline static int32_t get_offset_of_vendorId_0() { return static_cast<int32_t>(offsetof(VidPid_tD48EA2DDFAF98EAD7C13F962D730E15A6D13D6B2, ___vendorId_0)); }
	inline int32_t get_vendorId_0() const { return ___vendorId_0; }
	inline int32_t* get_address_of_vendorId_0() { return &___vendorId_0; }
	inline void set_vendorId_0(int32_t value)
	{
		___vendorId_0 = value;
	}

	inline static int32_t get_offset_of_productId_1() { return static_cast<int32_t>(offsetof(VidPid_tD48EA2DDFAF98EAD7C13F962D730E15A6D13D6B2, ___productId_1)); }
	inline int32_t get_productId_1() const { return ___productId_1; }
	inline int32_t* get_address_of_productId_1() { return &___productId_1; }
	inline void set_productId_1(int32_t value)
	{
		___productId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDPID_TD48EA2DDFAF98EAD7C13F962D730E15A6D13D6B2_H
#ifndef CONTROLLER_TFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B_H
#define CONTROLLER_TFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Custom.CustomInputSource_Controller
struct  Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Platforms.Custom.CustomInputSource_Controller::_isConnected
	bool ____isConnected_0;
	// System.String Rewired.Platforms.Custom.CustomInputSource_Controller::_deviceName
	String_t* ____deviceName_1;
	// System.String Rewired.Platforms.Custom.CustomInputSource_Controller::_customName
	String_t* ____customName_2;

public:
	inline static int32_t get_offset_of__isConnected_0() { return static_cast<int32_t>(offsetof(Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B, ____isConnected_0)); }
	inline bool get__isConnected_0() const { return ____isConnected_0; }
	inline bool* get_address_of__isConnected_0() { return &____isConnected_0; }
	inline void set__isConnected_0(bool value)
	{
		____isConnected_0 = value;
	}

	inline static int32_t get_offset_of__deviceName_1() { return static_cast<int32_t>(offsetof(Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B, ____deviceName_1)); }
	inline String_t* get__deviceName_1() const { return ____deviceName_1; }
	inline String_t** get_address_of__deviceName_1() { return &____deviceName_1; }
	inline void set__deviceName_1(String_t* value)
	{
		____deviceName_1 = value;
		Il2CppCodeGenWriteBarrier((&____deviceName_1), value);
	}

	inline static int32_t get_offset_of__customName_2() { return static_cast<int32_t>(offsetof(Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B, ____customName_2)); }
	inline String_t* get__customName_2() const { return ____customName_2; }
	inline String_t** get_address_of__customName_2() { return &____customName_2; }
	inline void set__customName_2(String_t* value)
	{
		____customName_2 = value;
		Il2CppCodeGenWriteBarrier((&____customName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLER_TFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B_H
#ifndef ELEMENT_T67DF1A6C6914A798218BD19B5517004855DF149F_H
#define ELEMENT_T67DF1A6C6914A798218BD19B5517004855DF149F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Custom.CustomInputSource_Element
struct  Element_t67DF1A6C6914A798218BD19B5517004855DF149F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T67DF1A6C6914A798218BD19B5517004855DF149F_H
#ifndef PLAYERCONTROLLER_T85008A2D3BCF71A0667B37FD2A3C77505D8FBA01_H
#define PLAYERCONTROLLER_T85008A2D3BCF71A0667B37FD2A3C77505D8FBA01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController
struct  PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01  : public RuntimeObject
{
public:
	// System.Int32 Rewired.PlayerController::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_0;
	// System.Boolean Rewired.PlayerController::eWTjKkaJVjCZqrsQHQXMtJYuNhD
	bool ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_1;
	// System.Int32 Rewired.PlayerController::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	// Rewired.Utils.Classes.Data.AList`1<Rewired.PlayerController_Element> Rewired.PlayerController::tqrCjCDiVROjgZQMTxdrZcCDZcA
	AList_1_t3F27923E7ADC225A9A272E929A0E4F01AF6316AC * ___tqrCjCDiVROjgZQMTxdrZcCDZcA_3;
	// Rewired.Utils.Classes.Data.AList`1<Rewired.PlayerController_Button> Rewired.PlayerController::QjgkqxyMBXGPcdjZSGsOcXzPxUl
	AList_1_tF031382831FF2B6183255F62CB24A361ACB88878 * ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_4;
	// Rewired.Utils.Classes.Data.AList`1<Rewired.PlayerController_Axis> Rewired.PlayerController::ewtEGQKoSuqZGbCfKTwTvaVIdwwn
	AList_1_t9C15FC8D8FA03BDBF2DD16B78F757675F8E2BEBF * ___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.PlayerController_Element> Rewired.PlayerController::HeiaDLCywuXsOrgGQhDKAJyFIubw
	ReadOnlyCollection_1_tCB1B9579FC6781EA29B0BA088B7B6D67A6494C4C * ___HeiaDLCywuXsOrgGQhDKAJyFIubw_6;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.PlayerController_Button> Rewired.PlayerController::xToauoyvRfSQtEIgZmpTaDMqGuB
	ReadOnlyCollection_1_t05EECEFFDFD4BA4204B837F165F3EFB261C27C52 * ___xToauoyvRfSQtEIgZmpTaDMqGuB_7;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.PlayerController_Axis> Rewired.PlayerController::ofSgeiiRBYviNkjEOCsJhBMuHgr
	ReadOnlyCollection_1_t41212E3E590E84A0579889CC0C7E5FFE22DEFC8F * ___ofSgeiiRBYviNkjEOCsJhBMuHgr_8;
	// System.Collections.Generic.List`1<Rewired.PlayerController_Element_dvlZSFCjnFxtoufPKLchsiMXNQc> Rewired.PlayerController::XLgAOntniFEhppktIYlmoPiBltW
	List_1_t1BAF2A63302AA70F47F2BB1F8ADC76463CEBC1D1 * ___XLgAOntniFEhppktIYlmoPiBltW_9;
	// System.Action`2<System.Int32,System.Boolean> Rewired.PlayerController::juknZgAgMCiSIxOQoGMldcYZSBp
	Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E * ___juknZgAgMCiSIxOQoGMldcYZSBp_10;
	// System.Action`2<System.Int32,System.Single> Rewired.PlayerController::KUlSPPdLfEKOBhsvlUicZMIWLRE
	Action_2_t634E33D1E916177D11D3008C7BCF5A07ADECEA10 * ___KUlSPPdLfEKOBhsvlUicZMIWLRE_11;
	// System.Action`1<System.Boolean> Rewired.PlayerController::yQeGjgudbMdGxktFDlTRKRPYNpwO
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___yQeGjgudbMdGxktFDlTRKRPYNpwO_12;

public:
	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_0() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_0)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_0() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_0; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_0() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_0; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_0(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_0 = value;
	}

	inline static int32_t get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_1() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_1)); }
	inline bool get_eWTjKkaJVjCZqrsQHQXMtJYuNhD_1() const { return ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_1; }
	inline bool* get_address_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_1() { return &___eWTjKkaJVjCZqrsQHQXMtJYuNhD_1; }
	inline void set_eWTjKkaJVjCZqrsQHQXMtJYuNhD_1(bool value)
	{
		___eWTjKkaJVjCZqrsQHQXMtJYuNhD_1 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___FCIziTxDWAnyHATWwZeMEmiXvdc_2)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_2() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_2(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_2 = value;
	}

	inline static int32_t get_offset_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_3() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___tqrCjCDiVROjgZQMTxdrZcCDZcA_3)); }
	inline AList_1_t3F27923E7ADC225A9A272E929A0E4F01AF6316AC * get_tqrCjCDiVROjgZQMTxdrZcCDZcA_3() const { return ___tqrCjCDiVROjgZQMTxdrZcCDZcA_3; }
	inline AList_1_t3F27923E7ADC225A9A272E929A0E4F01AF6316AC ** get_address_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_3() { return &___tqrCjCDiVROjgZQMTxdrZcCDZcA_3; }
	inline void set_tqrCjCDiVROjgZQMTxdrZcCDZcA_3(AList_1_t3F27923E7ADC225A9A272E929A0E4F01AF6316AC * value)
	{
		___tqrCjCDiVROjgZQMTxdrZcCDZcA_3 = value;
		Il2CppCodeGenWriteBarrier((&___tqrCjCDiVROjgZQMTxdrZcCDZcA_3), value);
	}

	inline static int32_t get_offset_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_4() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_4)); }
	inline AList_1_tF031382831FF2B6183255F62CB24A361ACB88878 * get_QjgkqxyMBXGPcdjZSGsOcXzPxUl_4() const { return ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_4; }
	inline AList_1_tF031382831FF2B6183255F62CB24A361ACB88878 ** get_address_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_4() { return &___QjgkqxyMBXGPcdjZSGsOcXzPxUl_4; }
	inline void set_QjgkqxyMBXGPcdjZSGsOcXzPxUl_4(AList_1_tF031382831FF2B6183255F62CB24A361ACB88878 * value)
	{
		___QjgkqxyMBXGPcdjZSGsOcXzPxUl_4 = value;
		Il2CppCodeGenWriteBarrier((&___QjgkqxyMBXGPcdjZSGsOcXzPxUl_4), value);
	}

	inline static int32_t get_offset_of_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5)); }
	inline AList_1_t9C15FC8D8FA03BDBF2DD16B78F757675F8E2BEBF * get_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5() const { return ___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5; }
	inline AList_1_t9C15FC8D8FA03BDBF2DD16B78F757675F8E2BEBF ** get_address_of_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5() { return &___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5; }
	inline void set_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5(AList_1_t9C15FC8D8FA03BDBF2DD16B78F757675F8E2BEBF * value)
	{
		___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5 = value;
		Il2CppCodeGenWriteBarrier((&___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5), value);
	}

	inline static int32_t get_offset_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_6() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___HeiaDLCywuXsOrgGQhDKAJyFIubw_6)); }
	inline ReadOnlyCollection_1_tCB1B9579FC6781EA29B0BA088B7B6D67A6494C4C * get_HeiaDLCywuXsOrgGQhDKAJyFIubw_6() const { return ___HeiaDLCywuXsOrgGQhDKAJyFIubw_6; }
	inline ReadOnlyCollection_1_tCB1B9579FC6781EA29B0BA088B7B6D67A6494C4C ** get_address_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_6() { return &___HeiaDLCywuXsOrgGQhDKAJyFIubw_6; }
	inline void set_HeiaDLCywuXsOrgGQhDKAJyFIubw_6(ReadOnlyCollection_1_tCB1B9579FC6781EA29B0BA088B7B6D67A6494C4C * value)
	{
		___HeiaDLCywuXsOrgGQhDKAJyFIubw_6 = value;
		Il2CppCodeGenWriteBarrier((&___HeiaDLCywuXsOrgGQhDKAJyFIubw_6), value);
	}

	inline static int32_t get_offset_of_xToauoyvRfSQtEIgZmpTaDMqGuB_7() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___xToauoyvRfSQtEIgZmpTaDMqGuB_7)); }
	inline ReadOnlyCollection_1_t05EECEFFDFD4BA4204B837F165F3EFB261C27C52 * get_xToauoyvRfSQtEIgZmpTaDMqGuB_7() const { return ___xToauoyvRfSQtEIgZmpTaDMqGuB_7; }
	inline ReadOnlyCollection_1_t05EECEFFDFD4BA4204B837F165F3EFB261C27C52 ** get_address_of_xToauoyvRfSQtEIgZmpTaDMqGuB_7() { return &___xToauoyvRfSQtEIgZmpTaDMqGuB_7; }
	inline void set_xToauoyvRfSQtEIgZmpTaDMqGuB_7(ReadOnlyCollection_1_t05EECEFFDFD4BA4204B837F165F3EFB261C27C52 * value)
	{
		___xToauoyvRfSQtEIgZmpTaDMqGuB_7 = value;
		Il2CppCodeGenWriteBarrier((&___xToauoyvRfSQtEIgZmpTaDMqGuB_7), value);
	}

	inline static int32_t get_offset_of_ofSgeiiRBYviNkjEOCsJhBMuHgr_8() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___ofSgeiiRBYviNkjEOCsJhBMuHgr_8)); }
	inline ReadOnlyCollection_1_t41212E3E590E84A0579889CC0C7E5FFE22DEFC8F * get_ofSgeiiRBYviNkjEOCsJhBMuHgr_8() const { return ___ofSgeiiRBYviNkjEOCsJhBMuHgr_8; }
	inline ReadOnlyCollection_1_t41212E3E590E84A0579889CC0C7E5FFE22DEFC8F ** get_address_of_ofSgeiiRBYviNkjEOCsJhBMuHgr_8() { return &___ofSgeiiRBYviNkjEOCsJhBMuHgr_8; }
	inline void set_ofSgeiiRBYviNkjEOCsJhBMuHgr_8(ReadOnlyCollection_1_t41212E3E590E84A0579889CC0C7E5FFE22DEFC8F * value)
	{
		___ofSgeiiRBYviNkjEOCsJhBMuHgr_8 = value;
		Il2CppCodeGenWriteBarrier((&___ofSgeiiRBYviNkjEOCsJhBMuHgr_8), value);
	}

	inline static int32_t get_offset_of_XLgAOntniFEhppktIYlmoPiBltW_9() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___XLgAOntniFEhppktIYlmoPiBltW_9)); }
	inline List_1_t1BAF2A63302AA70F47F2BB1F8ADC76463CEBC1D1 * get_XLgAOntniFEhppktIYlmoPiBltW_9() const { return ___XLgAOntniFEhppktIYlmoPiBltW_9; }
	inline List_1_t1BAF2A63302AA70F47F2BB1F8ADC76463CEBC1D1 ** get_address_of_XLgAOntniFEhppktIYlmoPiBltW_9() { return &___XLgAOntniFEhppktIYlmoPiBltW_9; }
	inline void set_XLgAOntniFEhppktIYlmoPiBltW_9(List_1_t1BAF2A63302AA70F47F2BB1F8ADC76463CEBC1D1 * value)
	{
		___XLgAOntniFEhppktIYlmoPiBltW_9 = value;
		Il2CppCodeGenWriteBarrier((&___XLgAOntniFEhppktIYlmoPiBltW_9), value);
	}

	inline static int32_t get_offset_of_juknZgAgMCiSIxOQoGMldcYZSBp_10() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___juknZgAgMCiSIxOQoGMldcYZSBp_10)); }
	inline Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E * get_juknZgAgMCiSIxOQoGMldcYZSBp_10() const { return ___juknZgAgMCiSIxOQoGMldcYZSBp_10; }
	inline Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E ** get_address_of_juknZgAgMCiSIxOQoGMldcYZSBp_10() { return &___juknZgAgMCiSIxOQoGMldcYZSBp_10; }
	inline void set_juknZgAgMCiSIxOQoGMldcYZSBp_10(Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E * value)
	{
		___juknZgAgMCiSIxOQoGMldcYZSBp_10 = value;
		Il2CppCodeGenWriteBarrier((&___juknZgAgMCiSIxOQoGMldcYZSBp_10), value);
	}

	inline static int32_t get_offset_of_KUlSPPdLfEKOBhsvlUicZMIWLRE_11() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___KUlSPPdLfEKOBhsvlUicZMIWLRE_11)); }
	inline Action_2_t634E33D1E916177D11D3008C7BCF5A07ADECEA10 * get_KUlSPPdLfEKOBhsvlUicZMIWLRE_11() const { return ___KUlSPPdLfEKOBhsvlUicZMIWLRE_11; }
	inline Action_2_t634E33D1E916177D11D3008C7BCF5A07ADECEA10 ** get_address_of_KUlSPPdLfEKOBhsvlUicZMIWLRE_11() { return &___KUlSPPdLfEKOBhsvlUicZMIWLRE_11; }
	inline void set_KUlSPPdLfEKOBhsvlUicZMIWLRE_11(Action_2_t634E33D1E916177D11D3008C7BCF5A07ADECEA10 * value)
	{
		___KUlSPPdLfEKOBhsvlUicZMIWLRE_11 = value;
		Il2CppCodeGenWriteBarrier((&___KUlSPPdLfEKOBhsvlUicZMIWLRE_11), value);
	}

	inline static int32_t get_offset_of_yQeGjgudbMdGxktFDlTRKRPYNpwO_12() { return static_cast<int32_t>(offsetof(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01, ___yQeGjgudbMdGxktFDlTRKRPYNpwO_12)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_yQeGjgudbMdGxktFDlTRKRPYNpwO_12() const { return ___yQeGjgudbMdGxktFDlTRKRPYNpwO_12; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_yQeGjgudbMdGxktFDlTRKRPYNpwO_12() { return &___yQeGjgudbMdGxktFDlTRKRPYNpwO_12; }
	inline void set_yQeGjgudbMdGxktFDlTRKRPYNpwO_12(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___yQeGjgudbMdGxktFDlTRKRPYNpwO_12 = value;
		Il2CppCodeGenWriteBarrier((&___yQeGjgudbMdGxktFDlTRKRPYNpwO_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T85008A2D3BCF71A0667B37FD2A3C77505D8FBA01_H
#ifndef DEFINITION_TCF297A490F8E07E75C2E1F36D9335A2625E0ED46_H
#define DEFINITION_TCF297A490F8E07E75C2E1F36D9335A2625E0ED46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Definition
struct  Definition_tCF297A490F8E07E75C2E1F36D9335A2625E0ED46  : public RuntimeObject
{
public:
	// System.Boolean Rewired.PlayerController_Definition::enabled
	bool ___enabled_0;
	// System.Int32 Rewired.PlayerController_Definition::playerId
	int32_t ___playerId_1;
	// System.Collections.Generic.ICollection`1<Rewired.PlayerController_Element_Definition> Rewired.PlayerController_Definition::elements
	RuntimeObject* ___elements_2;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(Definition_tCF297A490F8E07E75C2E1F36D9335A2625E0ED46, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_playerId_1() { return static_cast<int32_t>(offsetof(Definition_tCF297A490F8E07E75C2E1F36D9335A2625E0ED46, ___playerId_1)); }
	inline int32_t get_playerId_1() const { return ___playerId_1; }
	inline int32_t* get_address_of_playerId_1() { return &___playerId_1; }
	inline void set_playerId_1(int32_t value)
	{
		___playerId_1 = value;
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Definition_tCF297A490F8E07E75C2E1F36D9335A2625E0ED46, ___elements_2)); }
	inline RuntimeObject* get_elements_2() const { return ___elements_2; }
	inline RuntimeObject** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(RuntimeObject* value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITION_TCF297A490F8E07E75C2E1F36D9335A2625E0ED46_H
#ifndef ELEMENT_T94B07EAAA40D9789D1ED340A48413B7827D559A9_H
#define ELEMENT_T94B07EAAA40D9789D1ED340A48413B7827D559A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Element
struct  Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9  : public RuntimeObject
{
public:
	// Rewired.PlayerController Rewired.PlayerController_Element::waEhkaNJermXjlpTevBkHdlvdCD
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01 * ___waEhkaNJermXjlpTevBkHdlvdCD_1;
	// System.Boolean Rewired.PlayerController_Element::BQHRGRekfDXytGxwUCilBePQmzf
	bool ___BQHRGRekfDXytGxwUCilBePQmzf_2;
	// System.Boolean Rewired.PlayerController_Element::eWTjKkaJVjCZqrsQHQXMtJYuNhD
	bool ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_3;
	// System.String Rewired.PlayerController_Element::xzFIhZhMHNeQYGDUhfMfvvqdZQds
	String_t* ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_4;

public:
	inline static int32_t get_offset_of_waEhkaNJermXjlpTevBkHdlvdCD_1() { return static_cast<int32_t>(offsetof(Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9, ___waEhkaNJermXjlpTevBkHdlvdCD_1)); }
	inline PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01 * get_waEhkaNJermXjlpTevBkHdlvdCD_1() const { return ___waEhkaNJermXjlpTevBkHdlvdCD_1; }
	inline PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01 ** get_address_of_waEhkaNJermXjlpTevBkHdlvdCD_1() { return &___waEhkaNJermXjlpTevBkHdlvdCD_1; }
	inline void set_waEhkaNJermXjlpTevBkHdlvdCD_1(PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01 * value)
	{
		___waEhkaNJermXjlpTevBkHdlvdCD_1 = value;
		Il2CppCodeGenWriteBarrier((&___waEhkaNJermXjlpTevBkHdlvdCD_1), value);
	}

	inline static int32_t get_offset_of_BQHRGRekfDXytGxwUCilBePQmzf_2() { return static_cast<int32_t>(offsetof(Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9, ___BQHRGRekfDXytGxwUCilBePQmzf_2)); }
	inline bool get_BQHRGRekfDXytGxwUCilBePQmzf_2() const { return ___BQHRGRekfDXytGxwUCilBePQmzf_2; }
	inline bool* get_address_of_BQHRGRekfDXytGxwUCilBePQmzf_2() { return &___BQHRGRekfDXytGxwUCilBePQmzf_2; }
	inline void set_BQHRGRekfDXytGxwUCilBePQmzf_2(bool value)
	{
		___BQHRGRekfDXytGxwUCilBePQmzf_2 = value;
	}

	inline static int32_t get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_3() { return static_cast<int32_t>(offsetof(Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9, ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_3)); }
	inline bool get_eWTjKkaJVjCZqrsQHQXMtJYuNhD_3() const { return ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_3; }
	inline bool* get_address_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_3() { return &___eWTjKkaJVjCZqrsQHQXMtJYuNhD_3; }
	inline void set_eWTjKkaJVjCZqrsQHQXMtJYuNhD_3(bool value)
	{
		___eWTjKkaJVjCZqrsQHQXMtJYuNhD_3 = value;
	}

	inline static int32_t get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_4() { return static_cast<int32_t>(offsetof(Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9, ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_4)); }
	inline String_t* get_xzFIhZhMHNeQYGDUhfMfvvqdZQds_4() const { return ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_4; }
	inline String_t** get_address_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_4() { return &___xzFIhZhMHNeQYGDUhfMfvvqdZQds_4; }
	inline void set_xzFIhZhMHNeQYGDUhfMfvvqdZQds_4(String_t* value)
	{
		___xzFIhZhMHNeQYGDUhfMfvvqdZQds_4 = value;
		Il2CppCodeGenWriteBarrier((&___xzFIhZhMHNeQYGDUhfMfvvqdZQds_4), value);
	}
};

struct Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9_StaticFields
{
public:
	// System.Int32[] Rewired.PlayerController_Element::cBnZNmEwJpeWkutStcRxxMWgNUb
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cBnZNmEwJpeWkutStcRxxMWgNUb_5;
	// System.Int32[] Rewired.PlayerController_Element::sUhzqBbdlaMdFyXGMgVdBJRYgTAC
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___sUhzqBbdlaMdFyXGMgVdBJRYgTAC_6;

public:
	inline static int32_t get_offset_of_cBnZNmEwJpeWkutStcRxxMWgNUb_5() { return static_cast<int32_t>(offsetof(Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9_StaticFields, ___cBnZNmEwJpeWkutStcRxxMWgNUb_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cBnZNmEwJpeWkutStcRxxMWgNUb_5() const { return ___cBnZNmEwJpeWkutStcRxxMWgNUb_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cBnZNmEwJpeWkutStcRxxMWgNUb_5() { return &___cBnZNmEwJpeWkutStcRxxMWgNUb_5; }
	inline void set_cBnZNmEwJpeWkutStcRxxMWgNUb_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cBnZNmEwJpeWkutStcRxxMWgNUb_5 = value;
		Il2CppCodeGenWriteBarrier((&___cBnZNmEwJpeWkutStcRxxMWgNUb_5), value);
	}

	inline static int32_t get_offset_of_sUhzqBbdlaMdFyXGMgVdBJRYgTAC_6() { return static_cast<int32_t>(offsetof(Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9_StaticFields, ___sUhzqBbdlaMdFyXGMgVdBJRYgTAC_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_sUhzqBbdlaMdFyXGMgVdBJRYgTAC_6() const { return ___sUhzqBbdlaMdFyXGMgVdBJRYgTAC_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_sUhzqBbdlaMdFyXGMgVdBJRYgTAC_6() { return &___sUhzqBbdlaMdFyXGMgVdBJRYgTAC_6; }
	inline void set_sUhzqBbdlaMdFyXGMgVdBJRYgTAC_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___sUhzqBbdlaMdFyXGMgVdBJRYgTAC_6 = value;
		Il2CppCodeGenWriteBarrier((&___sUhzqBbdlaMdFyXGMgVdBJRYgTAC_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T94B07EAAA40D9789D1ED340A48413B7827D559A9_H
#ifndef DEFINITION_T438F4A94815CBEA643160ADCB2B76E9A3F053347_H
#define DEFINITION_T438F4A94815CBEA643160ADCB2B76E9A3F053347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Element_Definition
struct  Definition_t438F4A94815CBEA643160ADCB2B76E9A3F053347  : public RuntimeObject
{
public:
	// System.Boolean Rewired.PlayerController_Element_Definition::enabled
	bool ___enabled_0;
	// System.String Rewired.PlayerController_Element_Definition::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(Definition_t438F4A94815CBEA643160ADCB2B76E9A3F053347, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Definition_t438F4A94815CBEA643160ADCB2B76E9A3F053347, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITION_T438F4A94815CBEA643160ADCB2B76E9A3F053347_H
#ifndef FACTORY_T1C741BFACE6E45692011FFFEAD1C24E13DF9F709_H
#define FACTORY_T1C741BFACE6E45692011FFFEAD1C24E13DF9F709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Factory
struct  Factory_t1C741BFACE6E45692011FFFEAD1C24E13DF9F709  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORY_T1C741BFACE6E45692011FFFEAD1C24E13DF9F709_H
#ifndef FACTORY_TE6CF86402AE42CF34950BD2785B1F852FDFAC2CB_H
#define FACTORY_TE6CF86402AE42CF34950BD2785B1F852FDFAC2CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerMouse_Factory
struct  Factory_tE6CF86402AE42CF34950BD2785B1F852FDFAC2CB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORY_TE6CF86402AE42CF34950BD2785B1F852FDFAC2CB_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef PLATFORMVARS_WINDOWSSTANDALONE_T1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A_H
#define PLATFORMVARS_WINDOWSSTANDALONE_T1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ConfigVars_PlatformVars_WindowsStandalone
struct  PlatformVars_WindowsStandalone_t1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A  : public PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA
{
public:
	// System.Boolean Rewired.Data.ConfigVars_PlatformVars_WindowsStandalone::useNativeKeyboard
	bool ___useNativeKeyboard_2;
	// System.Int32 Rewired.Data.ConfigVars_PlatformVars_WindowsStandalone::joystickRefreshRate
	int32_t ___joystickRefreshRate_3;

public:
	inline static int32_t get_offset_of_useNativeKeyboard_2() { return static_cast<int32_t>(offsetof(PlatformVars_WindowsStandalone_t1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A, ___useNativeKeyboard_2)); }
	inline bool get_useNativeKeyboard_2() const { return ___useNativeKeyboard_2; }
	inline bool* get_address_of_useNativeKeyboard_2() { return &___useNativeKeyboard_2; }
	inline void set_useNativeKeyboard_2(bool value)
	{
		___useNativeKeyboard_2 = value;
	}

	inline static int32_t get_offset_of_joystickRefreshRate_3() { return static_cast<int32_t>(offsetof(PlatformVars_WindowsStandalone_t1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A, ___joystickRefreshRate_3)); }
	inline int32_t get_joystickRefreshRate_3() const { return ___joystickRefreshRate_3; }
	inline int32_t* get_address_of_joystickRefreshRate_3() { return &___joystickRefreshRate_3; }
	inline void set_joystickRefreshRate_3(int32_t value)
	{
		___joystickRefreshRate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMVARS_WINDOWSSTANDALONE_T1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A_H
#ifndef PLATFORMVARS_WINDOWSUWP_T3F16819E5BF554DD60A4D1E2464E38C88A9D73D8_H
#define PLATFORMVARS_WINDOWSUWP_T3F16819E5BF554DD60A4D1E2464E38C88A9D73D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ConfigVars_PlatformVars_WindowsUWP
struct  PlatformVars_WindowsUWP_t3F16819E5BF554DD60A4D1E2464E38C88A9D73D8  : public PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA
{
public:
	// System.Boolean Rewired.Data.ConfigVars_PlatformVars_WindowsUWP::useGamepadAPI
	bool ___useGamepadAPI_2;
	// System.Boolean Rewired.Data.ConfigVars_PlatformVars_WindowsUWP::useHIDAPI
	bool ___useHIDAPI_3;

public:
	inline static int32_t get_offset_of_useGamepadAPI_2() { return static_cast<int32_t>(offsetof(PlatformVars_WindowsUWP_t3F16819E5BF554DD60A4D1E2464E38C88A9D73D8, ___useGamepadAPI_2)); }
	inline bool get_useGamepadAPI_2() const { return ___useGamepadAPI_2; }
	inline bool* get_address_of_useGamepadAPI_2() { return &___useGamepadAPI_2; }
	inline void set_useGamepadAPI_2(bool value)
	{
		___useGamepadAPI_2 = value;
	}

	inline static int32_t get_offset_of_useHIDAPI_3() { return static_cast<int32_t>(offsetof(PlatformVars_WindowsUWP_t3F16819E5BF554DD60A4D1E2464E38C88A9D73D8, ___useHIDAPI_3)); }
	inline bool get_useHIDAPI_3() const { return ___useHIDAPI_3; }
	inline bool* get_address_of_useHIDAPI_3() { return &___useHIDAPI_3; }
	inline void set_useHIDAPI_3(bool value)
	{
		___useHIDAPI_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMVARS_WINDOWSUWP_T3F16819E5BF554DD60A4D1E2464E38C88A9D73D8_H
#ifndef BUTTON_T037E5D372364FAE233617DE537143C0FF59B05BA_H
#define BUTTON_T037E5D372364FAE233617DE537143C0FF59B05BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.CustomController_Editor_Button
struct  Button_t037E5D372364FAE233617DE537143C0FF59B05BA  : public Element_t4C80356DECB9FF4357706FB02B4C85C71AC29D12
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T037E5D372364FAE233617DE537143C0FF59B05BA_H
#ifndef PLATFORM_RAWORDIRECTINPUT_T66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC_H
#define PLATFORM_RAWORDIRECTINPUT_T66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput
struct  Platform_RawOrDirectInput_t66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC  : public Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput::matchingCriteria
	MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3 * ___matchingCriteria_1;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_RawOrDirectInput_t66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC, ___matchingCriteria_1)); }
	inline MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3 * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3 ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3 * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_RAWORDIRECTINPUT_T66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC_H
#ifndef ELEMENTS_PLATFORM_BASE_TAA1373033B7FB6EA7E7C0298F2380C7D3EE67CE5_H
#define ELEMENTS_PLATFORM_BASE_TAA1373033B7FB6EA7E7C0298F2380C7D3EE67CE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Elements_Platform_Base
struct  Elements_Platform_Base_tAA1373033B7FB6EA7E7C0298F2380C7D3EE67CE5  : public Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_PLATFORM_BASE_TAA1373033B7FB6EA7E7C0298F2380C7D3EE67CE5_H
#ifndef ELEMENTCOUNT_TA6EACD850F29B9F522B95E98217FCE9EF4B77A18_H
#define ELEMENTCOUNT_TA6EACD850F29B9F522B95E98217FCE9EF4B77A18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria_ElementCount
struct  ElementCount_tA6EACD850F29B9F522B95E98217FCE9EF4B77A18  : public ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria_ElementCount::hatCount
	int32_t ___hatCount_2;

public:
	inline static int32_t get_offset_of_hatCount_2() { return static_cast<int32_t>(offsetof(ElementCount_tA6EACD850F29B9F522B95E98217FCE9EF4B77A18, ___hatCount_2)); }
	inline int32_t get_hatCount_2() const { return ___hatCount_2; }
	inline int32_t* get_address_of_hatCount_2() { return &___hatCount_2; }
	inline void set_hatCount_2(int32_t value)
	{
		___hatCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTCOUNT_TA6EACD850F29B9F522B95E98217FCE9EF4B77A18_H
#ifndef AXIS_T62C4BB066E0FE5BEBABD43DF24B48B56ED399FC9_H
#define AXIS_T62C4BB066E0FE5BEBABD43DF24B48B56ED399FC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Custom.CustomInputSource_Axis
struct  Axis_t62C4BB066E0FE5BEBABD43DF24B48B56ED399FC9  : public Element_t67DF1A6C6914A798218BD19B5517004855DF149F
{
public:
	// System.Single Rewired.Platforms.Custom.CustomInputSource_Axis::value
	float ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Axis_t62C4BB066E0FE5BEBABD43DF24B48B56ED399FC9, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T62C4BB066E0FE5BEBABD43DF24B48B56ED399FC9_H
#ifndef BUTTON_T9063D0415294673AB9137F72C897049AE5DDE41C_H
#define BUTTON_T9063D0415294673AB9137F72C897049AE5DDE41C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Custom.CustomInputSource_Button
struct  Button_t9063D0415294673AB9137F72C897049AE5DDE41C  : public Element_t67DF1A6C6914A798218BD19B5517004855DF149F
{
public:
	// System.Boolean Rewired.Platforms.Custom.CustomInputSource_Button::value
	bool ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Button_t9063D0415294673AB9137F72C897049AE5DDE41C, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T9063D0415294673AB9137F72C897049AE5DDE41C_H
#ifndef COMPOUNDELEMENT_TA56F63F47F096A626ADF63799EEF977B5E4E632E_H
#define COMPOUNDELEMENT_TA56F63F47F096A626ADF63799EEF977B5E4E632E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_CompoundElement
struct  CompoundElement_tA56F63F47F096A626ADF63799EEF977B5E4E632E  : public Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9
{
public:
	// System.Collections.Generic.List`1<Rewired.PlayerController_Element> Rewired.PlayerController_CompoundElement::tqrCjCDiVROjgZQMTxdrZcCDZcA
	List_1_t928662206E2177819B177E4EA1888910BA369578 * ___tqrCjCDiVROjgZQMTxdrZcCDZcA_7;

public:
	inline static int32_t get_offset_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_7() { return static_cast<int32_t>(offsetof(CompoundElement_tA56F63F47F096A626ADF63799EEF977B5E4E632E, ___tqrCjCDiVROjgZQMTxdrZcCDZcA_7)); }
	inline List_1_t928662206E2177819B177E4EA1888910BA369578 * get_tqrCjCDiVROjgZQMTxdrZcCDZcA_7() const { return ___tqrCjCDiVROjgZQMTxdrZcCDZcA_7; }
	inline List_1_t928662206E2177819B177E4EA1888910BA369578 ** get_address_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_7() { return &___tqrCjCDiVROjgZQMTxdrZcCDZcA_7; }
	inline void set_tqrCjCDiVROjgZQMTxdrZcCDZcA_7(List_1_t928662206E2177819B177E4EA1888910BA369578 * value)
	{
		___tqrCjCDiVROjgZQMTxdrZcCDZcA_7 = value;
		Il2CppCodeGenWriteBarrier((&___tqrCjCDiVROjgZQMTxdrZcCDZcA_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDELEMENT_TA56F63F47F096A626ADF63799EEF977B5E4E632E_H
#ifndef DEFINITION_T3A8A60468526C12F075318D337B6FA439005CA39_H
#define DEFINITION_T3A8A60468526C12F075318D337B6FA439005CA39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_CompoundElement_Definition
struct  Definition_t3A8A60468526C12F075318D337B6FA439005CA39  : public Definition_t438F4A94815CBEA643160ADCB2B76E9A3F053347
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITION_T3A8A60468526C12F075318D337B6FA439005CA39_H
#ifndef ELEMENTWITHSOURCE_T69752C9D210EDEE80C6FD0F291364B0681FDB4ED_H
#define ELEMENTWITHSOURCE_T69752C9D210EDEE80C6FD0F291364B0681FDB4ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_ElementWithSource
struct  ElementWithSource_t69752C9D210EDEE80C6FD0F291364B0681FDB4ED  : public Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9
{
public:
	// System.Int32 Rewired.PlayerController_ElementWithSource::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_8;

public:
	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_8() { return static_cast<int32_t>(offsetof(ElementWithSource_t69752C9D210EDEE80C6FD0F291364B0681FDB4ED, ___LmADKPFcmVAiabETiHqQTGSgvmcg_8)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_8() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_8; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_8() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_8; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_8(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTWITHSOURCE_T69752C9D210EDEE80C6FD0F291364B0681FDB4ED_H
#ifndef DEFINITION_T18A5B017F634EC0B2DB9B29ADB9294C16C71F6BA_H
#define DEFINITION_T18A5B017F634EC0B2DB9B29ADB9294C16C71F6BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_ElementWithSource_Definition
struct  Definition_t18A5B017F634EC0B2DB9B29ADB9294C16C71F6BA  : public Definition_t438F4A94815CBEA643160ADCB2B76E9A3F053347
{
public:
	// System.Int32 Rewired.PlayerController_ElementWithSource_Definition::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_2;

public:
	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_2() { return static_cast<int32_t>(offsetof(Definition_t18A5B017F634EC0B2DB9B29ADB9294C16C71F6BA, ___LmADKPFcmVAiabETiHqQTGSgvmcg_2)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_2() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_2; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_2() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_2; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_2(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITION_T18A5B017F634EC0B2DB9B29ADB9294C16C71F6BA_H
#ifndef SCREENRECT_T3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14_H
#define SCREENRECT_T3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.ScreenRect
struct  ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14 
{
public:
	// System.Single Rewired.Utils.Classes.Data.ScreenRect::xMin
	float ___xMin_0;
	// System.Single Rewired.Utils.Classes.Data.ScreenRect::yMin
	float ___yMin_1;
	// System.Single Rewired.Utils.Classes.Data.ScreenRect::width
	float ___width_2;
	// System.Single Rewired.Utils.Classes.Data.ScreenRect::height
	float ___height_3;

public:
	inline static int32_t get_offset_of_xMin_0() { return static_cast<int32_t>(offsetof(ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14, ___xMin_0)); }
	inline float get_xMin_0() const { return ___xMin_0; }
	inline float* get_address_of_xMin_0() { return &___xMin_0; }
	inline void set_xMin_0(float value)
	{
		___xMin_0 = value;
	}

	inline static int32_t get_offset_of_yMin_1() { return static_cast<int32_t>(offsetof(ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14, ___yMin_1)); }
	inline float get_yMin_1() const { return ___yMin_1; }
	inline float* get_address_of_yMin_1() { return &___yMin_1; }
	inline void set_yMin_1(float value)
	{
		___yMin_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14, ___width_2)); }
	inline float get_width_2() const { return ___width_2; }
	inline float* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(float value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14, ___height_3)); }
	inline float get_height_3() const { return ___height_3; }
	inline float* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(float value)
	{
		___height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENRECT_T3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#define NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int64>
struct  Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 
{
public:
	// T System.Nullable`1::value
	int64_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifndef TYDFJIILEUXIFZMYRYPSYWMPMWA_T8340A7D91096EED258E5C91C215B70F279460FE8_H
#define TYDFJIILEUXIFZMYRYPSYWMPMWA_T8340A7D91096EED258E5C91C215B70F279460FE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TYDfjiilEUxIfZMyRypsywmpmwA
struct  TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte TYDfjiilEUxIfZMyRypsywmpmwA::wTYLYalPavcctjXZrASJNkapMaJ
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.SByte TYDfjiilEUxIfZMyRypsywmpmwA::MJrfBrxoBTvDrzGeBrTAImwmNOZ
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Char TYDfjiilEUxIfZMyRypsywmpmwA::euLduoAiVMxRQGLWMCGBXUDrCKr
			Il2CppChar ___euLduoAiVMxRQGLWMCGBXUDrCKr_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppChar ___euLduoAiVMxRQGLWMCGBXUDrCKr_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int16 TYDfjiilEUxIfZMyRypsywmpmwA::BlWgkqEeUMGgBlbpbsWvFckFbDv
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt16 TYDfjiilEUxIfZMyRypsywmpmwA::WBbwvCOIPVbAJbjWvqCbJElmQLtd
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 TYDfjiilEUxIfZMyRypsywmpmwA::lxdbJzaQWeBlRYInsjMuuXhyCggl
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt32 TYDfjiilEUxIfZMyRypsywmpmwA::evDemCrkcBEYeHncrlFUdsIyAKPa
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int64 TYDfjiilEUxIfZMyRypsywmpmwA::BUoNfFowxTfWVzlGeZVAXYsTHeZ
			int64_t ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt64 TYDfjiilEUxIfZMyRypsywmpmwA::UgJnYbycrRGWLWQVxOsnvCHCOdO
			uint64_t ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint64_t ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single TYDfjiilEUxIfZMyRypsywmpmwA::mUqspDOssdvKUAQvOxwRVdbooCb
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Double TYDfjiilEUxIfZMyRypsywmpmwA::niPvDktNKWYPHmgqlUoZPBssHhI
			double ___niPvDktNKWYPHmgqlUoZPBssHhI_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___niPvDktNKWYPHmgqlUoZPBssHhI_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Boolean TYDfjiilEUxIfZMyRypsywmpmwA::HqWkThUinZjstdlLHSIImaHNnAis
			bool ___HqWkThUinZjstdlLHSIImaHNnAis_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			bool ___HqWkThUinZjstdlLHSIImaHNnAis_11_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_wTYLYalPavcctjXZrASJNkapMaJ_0() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___wTYLYalPavcctjXZrASJNkapMaJ_0)); }
	inline uint8_t get_wTYLYalPavcctjXZrASJNkapMaJ_0() const { return ___wTYLYalPavcctjXZrASJNkapMaJ_0; }
	inline uint8_t* get_address_of_wTYLYalPavcctjXZrASJNkapMaJ_0() { return &___wTYLYalPavcctjXZrASJNkapMaJ_0; }
	inline void set_wTYLYalPavcctjXZrASJNkapMaJ_0(uint8_t value)
	{
		___wTYLYalPavcctjXZrASJNkapMaJ_0 = value;
	}

	inline static int32_t get_offset_of_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1)); }
	inline int8_t get_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() const { return ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1; }
	inline int8_t* get_address_of_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() { return &___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1; }
	inline void set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1(int8_t value)
	{
		___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1 = value;
	}

	inline static int32_t get_offset_of_euLduoAiVMxRQGLWMCGBXUDrCKr_2() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___euLduoAiVMxRQGLWMCGBXUDrCKr_2)); }
	inline Il2CppChar get_euLduoAiVMxRQGLWMCGBXUDrCKr_2() const { return ___euLduoAiVMxRQGLWMCGBXUDrCKr_2; }
	inline Il2CppChar* get_address_of_euLduoAiVMxRQGLWMCGBXUDrCKr_2() { return &___euLduoAiVMxRQGLWMCGBXUDrCKr_2; }
	inline void set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(Il2CppChar value)
	{
		___euLduoAiVMxRQGLWMCGBXUDrCKr_2 = value;
	}

	inline static int32_t get_offset_of_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3)); }
	inline int16_t get_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() const { return ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3; }
	inline int16_t* get_address_of_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() { return &___BlWgkqEeUMGgBlbpbsWvFckFbDv_3; }
	inline void set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3(int16_t value)
	{
		___BlWgkqEeUMGgBlbpbsWvFckFbDv_3 = value;
	}

	inline static int32_t get_offset_of_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4)); }
	inline uint16_t get_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() const { return ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4; }
	inline uint16_t* get_address_of_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() { return &___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4; }
	inline void set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4(uint16_t value)
	{
		___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4 = value;
	}

	inline static int32_t get_offset_of_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5)); }
	inline int32_t get_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() const { return ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5; }
	inline int32_t* get_address_of_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() { return &___lxdbJzaQWeBlRYInsjMuuXhyCggl_5; }
	inline void set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(int32_t value)
	{
		___lxdbJzaQWeBlRYInsjMuuXhyCggl_5 = value;
	}

	inline static int32_t get_offset_of_evDemCrkcBEYeHncrlFUdsIyAKPa_6() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___evDemCrkcBEYeHncrlFUdsIyAKPa_6)); }
	inline uint32_t get_evDemCrkcBEYeHncrlFUdsIyAKPa_6() const { return ___evDemCrkcBEYeHncrlFUdsIyAKPa_6; }
	inline uint32_t* get_address_of_evDemCrkcBEYeHncrlFUdsIyAKPa_6() { return &___evDemCrkcBEYeHncrlFUdsIyAKPa_6; }
	inline void set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(uint32_t value)
	{
		___evDemCrkcBEYeHncrlFUdsIyAKPa_6 = value;
	}

	inline static int32_t get_offset_of_BUoNfFowxTfWVzlGeZVAXYsTHeZ_7() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7)); }
	inline int64_t get_BUoNfFowxTfWVzlGeZVAXYsTHeZ_7() const { return ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7; }
	inline int64_t* get_address_of_BUoNfFowxTfWVzlGeZVAXYsTHeZ_7() { return &___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7; }
	inline void set_BUoNfFowxTfWVzlGeZVAXYsTHeZ_7(int64_t value)
	{
		___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7 = value;
	}

	inline static int32_t get_offset_of_UgJnYbycrRGWLWQVxOsnvCHCOdO_8() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8)); }
	inline uint64_t get_UgJnYbycrRGWLWQVxOsnvCHCOdO_8() const { return ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8; }
	inline uint64_t* get_address_of_UgJnYbycrRGWLWQVxOsnvCHCOdO_8() { return &___UgJnYbycrRGWLWQVxOsnvCHCOdO_8; }
	inline void set_UgJnYbycrRGWLWQVxOsnvCHCOdO_8(uint64_t value)
	{
		___UgJnYbycrRGWLWQVxOsnvCHCOdO_8 = value;
	}

	inline static int32_t get_offset_of_mUqspDOssdvKUAQvOxwRVdbooCb_9() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___mUqspDOssdvKUAQvOxwRVdbooCb_9)); }
	inline float get_mUqspDOssdvKUAQvOxwRVdbooCb_9() const { return ___mUqspDOssdvKUAQvOxwRVdbooCb_9; }
	inline float* get_address_of_mUqspDOssdvKUAQvOxwRVdbooCb_9() { return &___mUqspDOssdvKUAQvOxwRVdbooCb_9; }
	inline void set_mUqspDOssdvKUAQvOxwRVdbooCb_9(float value)
	{
		___mUqspDOssdvKUAQvOxwRVdbooCb_9 = value;
	}

	inline static int32_t get_offset_of_niPvDktNKWYPHmgqlUoZPBssHhI_10() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___niPvDktNKWYPHmgqlUoZPBssHhI_10)); }
	inline double get_niPvDktNKWYPHmgqlUoZPBssHhI_10() const { return ___niPvDktNKWYPHmgqlUoZPBssHhI_10; }
	inline double* get_address_of_niPvDktNKWYPHmgqlUoZPBssHhI_10() { return &___niPvDktNKWYPHmgqlUoZPBssHhI_10; }
	inline void set_niPvDktNKWYPHmgqlUoZPBssHhI_10(double value)
	{
		___niPvDktNKWYPHmgqlUoZPBssHhI_10 = value;
	}

	inline static int32_t get_offset_of_HqWkThUinZjstdlLHSIImaHNnAis_11() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___HqWkThUinZjstdlLHSIImaHNnAis_11)); }
	inline bool get_HqWkThUinZjstdlLHSIImaHNnAis_11() const { return ___HqWkThUinZjstdlLHSIImaHNnAis_11; }
	inline bool* get_address_of_HqWkThUinZjstdlLHSIImaHNnAis_11() { return &___HqWkThUinZjstdlLHSIImaHNnAis_11; }
	inline void set_HqWkThUinZjstdlLHSIImaHNnAis_11(bool value)
	{
		___HqWkThUinZjstdlLHSIImaHNnAis_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TYDfjiilEUxIfZMyRypsywmpmwA
struct TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int64_t ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint64_t ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint64_t ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			double ___niPvDktNKWYPHmgqlUoZPBssHhI_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___niPvDktNKWYPHmgqlUoZPBssHhI_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_11_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of TYDfjiilEUxIfZMyRypsywmpmwA
struct TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int64_t ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint64_t ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint64_t ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			double ___niPvDktNKWYPHmgqlUoZPBssHhI_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___niPvDktNKWYPHmgqlUoZPBssHhI_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_11_forAlignmentOnly;
		};
	};
};
#endif // TYDFJIILEUXIFZMYRYPSYWMPMWA_T8340A7D91096EED258E5C91C215B70F279460FE8_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#define AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisCoordinateMode
struct  AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868 
{
public:
	// System.Int32 Rewired.AxisCoordinateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifndef AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#define AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisRange
struct  AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652 
{
public:
	// System.Int32 Rewired.AxisRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifndef AXISSENSITIVITY2DTYPE_T0406339027EB595B27EA4C5E9A2A3F7F67FEF36B_H
#define AXISSENSITIVITY2DTYPE_T0406339027EB595B27EA4C5E9A2A3F7F67FEF36B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisSensitivity2DType
struct  AxisSensitivity2DType_t0406339027EB595B27EA4C5E9A2A3F7F67FEF36B 
{
public:
	// System.Int32 Rewired.AxisSensitivity2DType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisSensitivity2DType_t0406339027EB595B27EA4C5E9A2A3F7F67FEF36B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSENSITIVITY2DTYPE_T0406339027EB595B27EA4C5E9A2A3F7F67FEF36B_H
#ifndef AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#define AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisSensitivityType
struct  AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A 
{
public:
	// System.Int32 Rewired.AxisSensitivityType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#ifndef COMPOUNDCONTROLLERELEMENTTYPE_T38C73D05390CC2625D0760EB81BACFDEC36EB6B2_H
#define COMPOUNDCONTROLLERELEMENTTYPE_T38C73D05390CC2625D0760EB81BACFDEC36EB6B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.CompoundControllerElementType
struct  CompoundControllerElementType_t38C73D05390CC2625D0760EB81BACFDEC36EB6B2 
{
public:
	// System.Int32 Rewired.CompoundControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompoundControllerElementType_t38C73D05390CC2625D0760EB81BACFDEC36EB6B2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDCONTROLLERELEMENTTYPE_T38C73D05390CC2625D0760EB81BACFDEC36EB6B2_H
#ifndef LOGLEVELFLAGS_TD2231B5E28D4ADA489FAE89122340FE9D6755981_H
#define LOGLEVELFLAGS_TD2231B5E28D4ADA489FAE89122340FE9D6755981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Config.LogLevelFlags
struct  LogLevelFlags_tD2231B5E28D4ADA489FAE89122340FE9D6755981 
{
public:
	// System.Int32 Rewired.Config.LogLevelFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevelFlags_tD2231B5E28D4ADA489FAE89122340FE9D6755981, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVELFLAGS_TD2231B5E28D4ADA489FAE89122340FE9D6755981_H
#ifndef THROTTLECALIBRATIONMODE_T9454AA7B39A1EE99DD4B92B4E3A15AE78CD52EF9_H
#define THROTTLECALIBRATIONMODE_T9454AA7B39A1EE99DD4B92B4E3A15AE78CD52EF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Config.ThrottleCalibrationMode
struct  ThrottleCalibrationMode_t9454AA7B39A1EE99DD4B92B4E3A15AE78CD52EF9 
{
public:
	// System.Int32 Rewired.Config.ThrottleCalibrationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ThrottleCalibrationMode_t9454AA7B39A1EE99DD4B92B4E3A15AE78CD52EF9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THROTTLECALIBRATIONMODE_T9454AA7B39A1EE99DD4B92B4E3A15AE78CD52EF9_H
#ifndef UPDATELOOPSETTING_T813FEF786E2423CBB40457E67ABB4712DA865CA5_H
#define UPDATELOOPSETTING_T813FEF786E2423CBB40457E67ABB4712DA865CA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Config.UpdateLoopSetting
struct  UpdateLoopSetting_t813FEF786E2423CBB40457E67ABB4712DA865CA5 
{
public:
	// System.Int32 Rewired.Config.UpdateLoopSetting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateLoopSetting_t813FEF786E2423CBB40457E67ABB4712DA865CA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATELOOPSETTING_T813FEF786E2423CBB40457E67ABB4712DA865CA5_H
#ifndef CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#define CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerElementType
struct  ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA 
{
public:
	// System.Int32 Rewired.ControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifndef CONTROLLERTEMPLATEELEMENTSOURCETYPE_T9E3AF7FB9F717238623D389BE374AADB8C92066B_H
#define CONTROLLERTEMPLATEELEMENTSOURCETYPE_T9E3AF7FB9F717238623D389BE374AADB8C92066B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplateElementSourceType
struct  ControllerTemplateElementSourceType_t9E3AF7FB9F717238623D389BE374AADB8C92066B 
{
public:
	// System.Int32 Rewired.ControllerTemplateElementSourceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerTemplateElementSourceType_t9E3AF7FB9F717238623D389BE374AADB8C92066B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEELEMENTSOURCETYPE_T9E3AF7FB9F717238623D389BE374AADB8C92066B_H
#ifndef CONTROLLERTEMPLATEELEMENTTYPE_T64649CE1F95852B8211119A5D65EE4ACDA0C49D2_H
#define CONTROLLERTEMPLATEELEMENTTYPE_T64649CE1F95852B8211119A5D65EE4ACDA0C49D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplateElementType
struct  ControllerTemplateElementType_t64649CE1F95852B8211119A5D65EE4ACDA0C49D2 
{
public:
	// System.Int32 Rewired.ControllerTemplateElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerTemplateElementType_t64649CE1F95852B8211119A5D65EE4ACDA0C49D2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEELEMENTTYPE_T64649CE1F95852B8211119A5D65EE4ACDA0C49D2_H
#ifndef CONTROLLERTEMPLATEMAP_T5A4633F87A6A8691841F86C0584242C3CAB9666C_H
#define CONTROLLERTEMPLATEMAP_T5A4633F87A6A8691841F86C0584242C3CAB9666C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplateMap
struct  ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C  : public RuntimeObject
{
public:
	// System.Int32 Rewired.ControllerTemplateMap::_reInputId
	int32_t ____reInputId_0;
	// System.Int32 Rewired.ControllerTemplateMap::_id
	int32_t ____id_1;
	// System.Guid Rewired.ControllerTemplateMap::_templateTypeGuid
	Guid_t  ____templateTypeGuid_2;
	// System.Collections.Generic.List`1<Rewired.ControllerTemplateActionElementMap> Rewired.ControllerTemplateMap::_elementMaps
	List_1_t6C42FA67B6C0B4EF47E07906C577B234282D2171 * ____elementMaps_3;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.ControllerTemplateActionElementMap> Rewired.ControllerTemplateMap::_elementMaps_readOnly
	ReadOnlyCollection_1_tBA01D97DD95AB800F00D61982D160EEEA23DB50E * ____elementMaps_readOnly_4;
	// System.Boolean Rewired.ControllerTemplateMap::_enabled
	bool ____enabled_5;
	// System.Int32 Rewired.ControllerTemplateMap::_categoryId
	int32_t ____categoryId_6;
	// System.Int32 Rewired.ControllerTemplateMap::_layoutId
	int32_t ____layoutId_7;
	// System.Int32 Rewired.ControllerTemplateMap::_sourceMapId
	int32_t ____sourceMapId_8;

public:
	inline static int32_t get_offset_of__reInputId_0() { return static_cast<int32_t>(offsetof(ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C, ____reInputId_0)); }
	inline int32_t get__reInputId_0() const { return ____reInputId_0; }
	inline int32_t* get_address_of__reInputId_0() { return &____reInputId_0; }
	inline void set__reInputId_0(int32_t value)
	{
		____reInputId_0 = value;
	}

	inline static int32_t get_offset_of__id_1() { return static_cast<int32_t>(offsetof(ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C, ____id_1)); }
	inline int32_t get__id_1() const { return ____id_1; }
	inline int32_t* get_address_of__id_1() { return &____id_1; }
	inline void set__id_1(int32_t value)
	{
		____id_1 = value;
	}

	inline static int32_t get_offset_of__templateTypeGuid_2() { return static_cast<int32_t>(offsetof(ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C, ____templateTypeGuid_2)); }
	inline Guid_t  get__templateTypeGuid_2() const { return ____templateTypeGuid_2; }
	inline Guid_t * get_address_of__templateTypeGuid_2() { return &____templateTypeGuid_2; }
	inline void set__templateTypeGuid_2(Guid_t  value)
	{
		____templateTypeGuid_2 = value;
	}

	inline static int32_t get_offset_of__elementMaps_3() { return static_cast<int32_t>(offsetof(ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C, ____elementMaps_3)); }
	inline List_1_t6C42FA67B6C0B4EF47E07906C577B234282D2171 * get__elementMaps_3() const { return ____elementMaps_3; }
	inline List_1_t6C42FA67B6C0B4EF47E07906C577B234282D2171 ** get_address_of__elementMaps_3() { return &____elementMaps_3; }
	inline void set__elementMaps_3(List_1_t6C42FA67B6C0B4EF47E07906C577B234282D2171 * value)
	{
		____elementMaps_3 = value;
		Il2CppCodeGenWriteBarrier((&____elementMaps_3), value);
	}

	inline static int32_t get_offset_of__elementMaps_readOnly_4() { return static_cast<int32_t>(offsetof(ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C, ____elementMaps_readOnly_4)); }
	inline ReadOnlyCollection_1_tBA01D97DD95AB800F00D61982D160EEEA23DB50E * get__elementMaps_readOnly_4() const { return ____elementMaps_readOnly_4; }
	inline ReadOnlyCollection_1_tBA01D97DD95AB800F00D61982D160EEEA23DB50E ** get_address_of__elementMaps_readOnly_4() { return &____elementMaps_readOnly_4; }
	inline void set__elementMaps_readOnly_4(ReadOnlyCollection_1_tBA01D97DD95AB800F00D61982D160EEEA23DB50E * value)
	{
		____elementMaps_readOnly_4 = value;
		Il2CppCodeGenWriteBarrier((&____elementMaps_readOnly_4), value);
	}

	inline static int32_t get_offset_of__enabled_5() { return static_cast<int32_t>(offsetof(ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C, ____enabled_5)); }
	inline bool get__enabled_5() const { return ____enabled_5; }
	inline bool* get_address_of__enabled_5() { return &____enabled_5; }
	inline void set__enabled_5(bool value)
	{
		____enabled_5 = value;
	}

	inline static int32_t get_offset_of__categoryId_6() { return static_cast<int32_t>(offsetof(ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C, ____categoryId_6)); }
	inline int32_t get__categoryId_6() const { return ____categoryId_6; }
	inline int32_t* get_address_of__categoryId_6() { return &____categoryId_6; }
	inline void set__categoryId_6(int32_t value)
	{
		____categoryId_6 = value;
	}

	inline static int32_t get_offset_of__layoutId_7() { return static_cast<int32_t>(offsetof(ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C, ____layoutId_7)); }
	inline int32_t get__layoutId_7() const { return ____layoutId_7; }
	inline int32_t* get_address_of__layoutId_7() { return &____layoutId_7; }
	inline void set__layoutId_7(int32_t value)
	{
		____layoutId_7 = value;
	}

	inline static int32_t get_offset_of__sourceMapId_8() { return static_cast<int32_t>(offsetof(ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C, ____sourceMapId_8)); }
	inline int32_t get__sourceMapId_8() const { return ____sourceMapId_8; }
	inline int32_t* get_address_of__sourceMapId_8() { return &____sourceMapId_8; }
	inline void set__sourceMapId_8(int32_t value)
	{
		____sourceMapId_8 = value;
	}
};

struct ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C_StaticFields
{
public:
	// System.Int32 Rewired.ControllerTemplateMap::__idCounter
	int32_t _____idCounter_9;

public:
	inline static int32_t get_offset_of___idCounter_9() { return static_cast<int32_t>(offsetof(ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C_StaticFields, _____idCounter_9)); }
	inline int32_t get___idCounter_9() const { return _____idCounter_9; }
	inline int32_t* get_address_of___idCounter_9() { return &_____idCounter_9; }
	inline void set___idCounter_9(int32_t value)
	{
		_____idCounter_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEMAP_T5A4633F87A6A8691841F86C0584242C3CAB9666C_H
#ifndef ALLPLATFORMVAR_T7BEC1C3A2BD20549C4FAB7A17E51CBE9CB6006B3_H
#define ALLPLATFORMVAR_T7BEC1C3A2BD20549C4FAB7A17E51CBE9CB6006B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ConfigVars_AllPlatformVar
struct  AllPlatformVar_t7BEC1C3A2BD20549C4FAB7A17E51CBE9CB6006B3 
{
public:
	// System.Int32 Rewired.Data.ConfigVars_AllPlatformVar::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AllPlatformVar_t7BEC1C3A2BD20549C4FAB7A17E51CBE9CB6006B3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLPLATFORMVAR_T7BEC1C3A2BD20549C4FAB7A17E51CBE9CB6006B3_H
#ifndef ALTERNATEAXISCALIBRATIONTYPE_T13E810E9252891A41BDB4417F660AE3C9EEA526F_H
#define ALTERNATEAXISCALIBRATIONTYPE_T13E810E9252891A41BDB4417F660AE3C9EEA526F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.AlternateAxisCalibrationType
struct  AlternateAxisCalibrationType_t13E810E9252891A41BDB4417F660AE3C9EEA526F 
{
public:
	// System.Int32 Rewired.Data.Mapping.AlternateAxisCalibrationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlternateAxisCalibrationType_t13E810E9252891A41BDB4417F660AE3C9EEA526F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALTERNATEAXISCALIBRATIONTYPE_T13E810E9252891A41BDB4417F660AE3C9EEA526F_H
#ifndef AXISCALIBRATIONTYPE_TE13AFD356AB96E532C90470EC4A227DEF86B598C_H
#define AXISCALIBRATIONTYPE_TE13AFD356AB96E532C90470EC4A227DEF86B598C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.AxisCalibrationType
struct  AxisCalibrationType_tE13AFD356AB96E532C90470EC4A227DEF86B598C 
{
public:
	// System.Int32 Rewired.Data.Mapping.AxisCalibrationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisCalibrationType_tE13AFD356AB96E532C90470EC4A227DEF86B598C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCALIBRATIONTYPE_TE13AFD356AB96E532C90470EC4A227DEF86B598C_H
#ifndef AXISDIRECTION_TE4953CDA51E7D3BD480078C8768EBCD213C2EF49_H
#define AXISDIRECTION_TE4953CDA51E7D3BD480078C8768EBCD213C2EF49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.AxisDirection
struct  AxisDirection_tE4953CDA51E7D3BD480078C8768EBCD213C2EF49 
{
public:
	// System.Int32 Rewired.Data.Mapping.AxisDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisDirection_tE4953CDA51E7D3BD480078C8768EBCD213C2EF49, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISDIRECTION_TE4953CDA51E7D3BD480078C8768EBCD213C2EF49_H
#ifndef CALCULATIONTYPE_T9EE6947FA506F4C2E9393F694C0395D4FA172D2D_H
#define CALCULATIONTYPE_T9EE6947FA506F4C2E9393F694C0395D4FA172D2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.CustomCalculation_Accelerometer_CalculationType
struct  CalculationType_t9EE6947FA506F4C2E9393F694C0395D4FA172D2D 
{
public:
	// System.Int32 Rewired.Data.Mapping.CustomCalculation_Accelerometer_CalculationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CalculationType_t9EE6947FA506F4C2E9393F694C0395D4FA172D2D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALCULATIONTYPE_T9EE6947FA506F4C2E9393F694C0395D4FA172D2D_H
#ifndef INPUTTYPE_TE659D1D29467BD6075DF11BB007F36E08C7F1890_H
#define INPUTTYPE_TE659D1D29467BD6075DF11BB007F36E08C7F1890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.CustomCalculation_Accelerometer_InputType
struct  InputType_tE659D1D29467BD6075DF11BB007F36E08C7F1890 
{
public:
	// System.Int32 Rewired.Data.Mapping.CustomCalculation_Accelerometer_InputType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputType_tE659D1D29467BD6075DF11BB007F36E08C7F1890, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_TE659D1D29467BD6075DF11BB007F36E08C7F1890_H
#ifndef OUTPUTTYPE_T5373E3E8E93DFB10C4A5039895A10EB56D77A660_H
#define OUTPUTTYPE_T5373E3E8E93DFB10C4A5039895A10EB56D77A660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.CustomCalculation_Accelerometer_OutputType
struct  OutputType_t5373E3E8E93DFB10C4A5039895A10EB56D77A660 
{
public:
	// System.Int32 Rewired.Data.Mapping.CustomCalculation_Accelerometer_OutputType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OutputType_t5373E3E8E93DFB10C4A5039895A10EB56D77A660, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTTYPE_T5373E3E8E93DFB10C4A5039895A10EB56D77A660_H
#ifndef COMPARISONTYPE_T513ED3BC6BF03CCF7D6DBE670F8C16A90982C6C1_H
#define COMPARISONTYPE_T513ED3BC6BF03CCF7D6DBE670F8C16A90982C6C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.CustomCalculation_CompareElementValues_ComparisonType
struct  ComparisonType_t513ED3BC6BF03CCF7D6DBE670F8C16A90982C6C1 
{
public:
	// System.Int32 Rewired.Data.Mapping.CustomCalculation_CompareElementValues_ComparisonType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ComparisonType_t513ED3BC6BF03CCF7D6DBE670F8C16A90982C6C1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARISONTYPE_T513ED3BC6BF03CCF7D6DBE670F8C16A90982C6C1_H
#ifndef MODE_T67DA2117A0FBBBACA752DD1E441832AF9B10D952_H
#define MODE_T67DA2117A0FBBBACA752DD1E441832AF9B10D952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.CustomCalculation_LogitechGRacingWheelPedals_Mode
struct  Mode_t67DA2117A0FBBBACA752DD1E441832AF9B10D952 
{
public:
	// System.Int32 Rewired.Data.Mapping.CustomCalculation_LogitechGRacingWheelPedals_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t67DA2117A0FBBBACA752DD1E441832AF9B10D952, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T67DA2117A0FBBBACA752DD1E441832AF9B10D952_H
#ifndef HARDWAREELEMENTSOURCETYPEWITHHAT_TDC84EB6512EDD5A1BB8A15FFF31201CC0E318520_H
#define HARDWAREELEMENTSOURCETYPEWITHHAT_TDC84EB6512EDD5A1BB8A15FFF31201CC0E318520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat
struct  HardwareElementSourceTypeWithHat_tDC84EB6512EDD5A1BB8A15FFF31201CC0E318520 
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareElementSourceTypeWithHat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HardwareElementSourceTypeWithHat_tDC84EB6512EDD5A1BB8A15FFF31201CC0E318520, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWAREELEMENTSOURCETYPEWITHHAT_TDC84EB6512EDD5A1BB8A15FFF31201CC0E318520_H
#ifndef PLATFORM_DIRECTINPUT_BASE_T5F001050D12A98E75D98A40FACDFAF46998FFC5B_H
#define PLATFORM_DIRECTINPUT_BASE_T5F001050D12A98E75D98A40FACDFAF46998FFC5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base
struct  Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B  : public Platform_RawOrDirectInput_t66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base::elements
	Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7 * ___elements_2;

public:
	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B, ___elements_2)); }
	inline Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7 * get_elements_2() const { return ___elements_2; }
	inline Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7 ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7 * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_DIRECTINPUT_BASE_T5F001050D12A98E75D98A40FACDFAF46998FFC5B_H
#ifndef ELEMENTS_T3F40D6D8530AD00F16775409ED3EA957C3E150B7_H
#define ELEMENTS_T3F40D6D8530AD00F16775409ED3EA957C3E150B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements
struct  Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7  : public Elements_Platform_Base_tAA1373033B7FB6EA7E7C0298F2380C7D3EE67CE5
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements::axes
	AxisU5BU5D_tA03336768F545EBCBA07811F87E1AFA987F0C606* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Elements::buttons
	ButtonU5BU5D_tE48299B02DCCB4EF830F20647DD07A24F584B3E6* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7, ___axes_0)); }
	inline AxisU5BU5D_tA03336768F545EBCBA07811F87E1AFA987F0C606* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_tA03336768F545EBCBA07811F87E1AFA987F0C606** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_tA03336768F545EBCBA07811F87E1AFA987F0C606* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7, ___buttons_1)); }
	inline ButtonU5BU5D_tE48299B02DCCB4EF830F20647DD07A24F584B3E6* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_tE48299B02DCCB4EF830F20647DD07A24F584B3E6** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_tE48299B02DCCB4EF830F20647DD07A24F584B3E6* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_T3F40D6D8530AD00F16775409ED3EA957C3E150B7_H
#ifndef PLATFORM_RAWINPUT_BASE_TA0DFFECAF98212B21416DE596B7011684B6F5ADB_H
#define PLATFORM_RAWINPUT_BASE_TA0DFFECAF98212B21416DE596B7011684B6F5ADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base
struct  Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB  : public Platform_RawOrDirectInput_t66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base::elements
	Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 * ___elements_2;

public:
	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB, ___elements_2)); }
	inline Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 * get_elements_2() const { return ___elements_2; }
	inline Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_RAWINPUT_BASE_TA0DFFECAF98212B21416DE596B7011684B6F5ADB_H
#ifndef DEVICETYPE_TA1B2CDA0C670F6AF83BC2ECFE58D81275EF2B6DC_H
#define DEVICETYPE_TA1B2CDA0C670F6AF83BC2ECFE58D81275EF2B6DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_DeviceType
struct  DeviceType_tA1B2CDA0C670F6AF83BC2ECFE58D81275EF2B6DC 
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_DeviceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeviceType_tA1B2CDA0C670F6AF83BC2ECFE58D81275EF2B6DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETYPE_TA1B2CDA0C670F6AF83BC2ECFE58D81275EF2B6DC_H
#ifndef HATDIRECTION_T8B3787E56706986A258EEBCF16F989D2075785BA_H
#define HATDIRECTION_T8B3787E56706986A258EEBCF16F989D2075785BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HatDirection
struct  HatDirection_t8B3787E56706986A258EEBCF16F989D2075785BA 
{
public:
	// System.Int32 Rewired.Data.Mapping.HatDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HatDirection_t8B3787E56706986A258EEBCF16F989D2075785BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HATDIRECTION_T8B3787E56706986A258EEBCF16F989D2075785BA_H
#ifndef HATTYPE_T83B584B8E763A7B6924539BA31ECF4BD99E395F3_H
#define HATTYPE_T83B584B8E763A7B6924539BA31ECF4BD99E395F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HatType
struct  HatType_t83B584B8E763A7B6924539BA31ECF4BD99E395F3 
{
public:
	// System.Int32 Rewired.Data.Mapping.HatType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HatType_t83B584B8E763A7B6924539BA31ECF4BD99E395F3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HATTYPE_T83B584B8E763A7B6924539BA31ECF4BD99E395F3_H
#ifndef DEADZONE2DTYPE_TBD26291A70136F11C062AAAC6A6257423809B586_H
#define DEADZONE2DTYPE_TBD26291A70136F11C062AAAC6A6257423809B586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.DeadZone2DType
struct  DeadZone2DType_tBD26291A70136F11C062AAAC6A6257423809B586 
{
public:
	// System.Int32 Rewired.DeadZone2DType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeadZone2DType_tBD26291A70136F11C062AAAC6A6257423809B586, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEADZONE2DTYPE_TBD26291A70136F11C062AAAC6A6257423809B586_H
#ifndef INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#define INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSource
struct  InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B 
{
public:
	// System.Int32 Rewired.InputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifndef JOYSTICK_T6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09_H
#define JOYSTICK_T6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Custom.CustomInputSource_Joystick
struct  Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09  : public Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B
{
public:
	// System.Nullable`1<System.Int64> Rewired.Platforms.Custom.CustomInputSource_Joystick::kNCtRZYNhfpgNEjWCvYWWaJdsPw
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___kNCtRZYNhfpgNEjWCvYWWaJdsPw_3;
	// System.Int32 Rewired.Platforms.Custom.CustomInputSource_Joystick::pVMgxVlhOWBbMBjIelvXUbEMRHTh
	int32_t ___pVMgxVlhOWBbMBjIelvXUbEMRHTh_4;
	// Rewired.Platforms.Custom.CustomInputSource_Axis[] Rewired.Platforms.Custom.CustomInputSource_Joystick::ewtEGQKoSuqZGbCfKTwTvaVIdwwn
	AxisU5BU5D_tD22230ADED67D8E860DD8F4226514D79EDE55BEE* ___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5;
	// Rewired.Platforms.Custom.CustomInputSource_Button[] Rewired.Platforms.Custom.CustomInputSource_Joystick::QjgkqxyMBXGPcdjZSGsOcXzPxUl
	ButtonU5BU5D_t181B5EB5ACB98CA3C44C3FB3DF613259B353B43A* ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_6;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Platforms.Custom.CustomInputSource_Axis> Rewired.Platforms.Custom.CustomInputSource_Joystick::ofSgeiiRBYviNkjEOCsJhBMuHgr
	ReadOnlyCollection_1_t4829C3B53F3992A9308E9687500F009CA89848AC * ___ofSgeiiRBYviNkjEOCsJhBMuHgr_7;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Platforms.Custom.CustomInputSource_Button> Rewired.Platforms.Custom.CustomInputSource_Joystick::xToauoyvRfSQtEIgZmpTaDMqGuB
	ReadOnlyCollection_1_t8D1C0984E4131D0AD8A662731FDECC171DD80B5F * ___xToauoyvRfSQtEIgZmpTaDMqGuB_8;
	// System.Boolean Rewired.Platforms.Custom.CustomInputSource_Joystick::OWbAvMhWYnpGDbwOiHPyDnDXREx
	bool ___OWbAvMhWYnpGDbwOiHPyDnDXREx_9;
	// Rewired.Controller_Extension Rewired.Platforms.Custom.CustomInputSource_Joystick::keBYOCobzoABWhYylPhjApVSLXnN
	Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * ___keBYOCobzoABWhYylPhjApVSLXnN_10;

public:
	inline static int32_t get_offset_of_kNCtRZYNhfpgNEjWCvYWWaJdsPw_3() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___kNCtRZYNhfpgNEjWCvYWWaJdsPw_3)); }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  get_kNCtRZYNhfpgNEjWCvYWWaJdsPw_3() const { return ___kNCtRZYNhfpgNEjWCvYWWaJdsPw_3; }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 * get_address_of_kNCtRZYNhfpgNEjWCvYWWaJdsPw_3() { return &___kNCtRZYNhfpgNEjWCvYWWaJdsPw_3; }
	inline void set_kNCtRZYNhfpgNEjWCvYWWaJdsPw_3(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  value)
	{
		___kNCtRZYNhfpgNEjWCvYWWaJdsPw_3 = value;
	}

	inline static int32_t get_offset_of_pVMgxVlhOWBbMBjIelvXUbEMRHTh_4() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___pVMgxVlhOWBbMBjIelvXUbEMRHTh_4)); }
	inline int32_t get_pVMgxVlhOWBbMBjIelvXUbEMRHTh_4() const { return ___pVMgxVlhOWBbMBjIelvXUbEMRHTh_4; }
	inline int32_t* get_address_of_pVMgxVlhOWBbMBjIelvXUbEMRHTh_4() { return &___pVMgxVlhOWBbMBjIelvXUbEMRHTh_4; }
	inline void set_pVMgxVlhOWBbMBjIelvXUbEMRHTh_4(int32_t value)
	{
		___pVMgxVlhOWBbMBjIelvXUbEMRHTh_4 = value;
	}

	inline static int32_t get_offset_of_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5)); }
	inline AxisU5BU5D_tD22230ADED67D8E860DD8F4226514D79EDE55BEE* get_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5() const { return ___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5; }
	inline AxisU5BU5D_tD22230ADED67D8E860DD8F4226514D79EDE55BEE** get_address_of_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5() { return &___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5; }
	inline void set_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5(AxisU5BU5D_tD22230ADED67D8E860DD8F4226514D79EDE55BEE* value)
	{
		___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5 = value;
		Il2CppCodeGenWriteBarrier((&___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5), value);
	}

	inline static int32_t get_offset_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_6() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_6)); }
	inline ButtonU5BU5D_t181B5EB5ACB98CA3C44C3FB3DF613259B353B43A* get_QjgkqxyMBXGPcdjZSGsOcXzPxUl_6() const { return ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_6; }
	inline ButtonU5BU5D_t181B5EB5ACB98CA3C44C3FB3DF613259B353B43A** get_address_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_6() { return &___QjgkqxyMBXGPcdjZSGsOcXzPxUl_6; }
	inline void set_QjgkqxyMBXGPcdjZSGsOcXzPxUl_6(ButtonU5BU5D_t181B5EB5ACB98CA3C44C3FB3DF613259B353B43A* value)
	{
		___QjgkqxyMBXGPcdjZSGsOcXzPxUl_6 = value;
		Il2CppCodeGenWriteBarrier((&___QjgkqxyMBXGPcdjZSGsOcXzPxUl_6), value);
	}

	inline static int32_t get_offset_of_ofSgeiiRBYviNkjEOCsJhBMuHgr_7() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___ofSgeiiRBYviNkjEOCsJhBMuHgr_7)); }
	inline ReadOnlyCollection_1_t4829C3B53F3992A9308E9687500F009CA89848AC * get_ofSgeiiRBYviNkjEOCsJhBMuHgr_7() const { return ___ofSgeiiRBYviNkjEOCsJhBMuHgr_7; }
	inline ReadOnlyCollection_1_t4829C3B53F3992A9308E9687500F009CA89848AC ** get_address_of_ofSgeiiRBYviNkjEOCsJhBMuHgr_7() { return &___ofSgeiiRBYviNkjEOCsJhBMuHgr_7; }
	inline void set_ofSgeiiRBYviNkjEOCsJhBMuHgr_7(ReadOnlyCollection_1_t4829C3B53F3992A9308E9687500F009CA89848AC * value)
	{
		___ofSgeiiRBYviNkjEOCsJhBMuHgr_7 = value;
		Il2CppCodeGenWriteBarrier((&___ofSgeiiRBYviNkjEOCsJhBMuHgr_7), value);
	}

	inline static int32_t get_offset_of_xToauoyvRfSQtEIgZmpTaDMqGuB_8() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___xToauoyvRfSQtEIgZmpTaDMqGuB_8)); }
	inline ReadOnlyCollection_1_t8D1C0984E4131D0AD8A662731FDECC171DD80B5F * get_xToauoyvRfSQtEIgZmpTaDMqGuB_8() const { return ___xToauoyvRfSQtEIgZmpTaDMqGuB_8; }
	inline ReadOnlyCollection_1_t8D1C0984E4131D0AD8A662731FDECC171DD80B5F ** get_address_of_xToauoyvRfSQtEIgZmpTaDMqGuB_8() { return &___xToauoyvRfSQtEIgZmpTaDMqGuB_8; }
	inline void set_xToauoyvRfSQtEIgZmpTaDMqGuB_8(ReadOnlyCollection_1_t8D1C0984E4131D0AD8A662731FDECC171DD80B5F * value)
	{
		___xToauoyvRfSQtEIgZmpTaDMqGuB_8 = value;
		Il2CppCodeGenWriteBarrier((&___xToauoyvRfSQtEIgZmpTaDMqGuB_8), value);
	}

	inline static int32_t get_offset_of_OWbAvMhWYnpGDbwOiHPyDnDXREx_9() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___OWbAvMhWYnpGDbwOiHPyDnDXREx_9)); }
	inline bool get_OWbAvMhWYnpGDbwOiHPyDnDXREx_9() const { return ___OWbAvMhWYnpGDbwOiHPyDnDXREx_9; }
	inline bool* get_address_of_OWbAvMhWYnpGDbwOiHPyDnDXREx_9() { return &___OWbAvMhWYnpGDbwOiHPyDnDXREx_9; }
	inline void set_OWbAvMhWYnpGDbwOiHPyDnDXREx_9(bool value)
	{
		___OWbAvMhWYnpGDbwOiHPyDnDXREx_9 = value;
	}

	inline static int32_t get_offset_of_keBYOCobzoABWhYylPhjApVSLXnN_10() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___keBYOCobzoABWhYylPhjApVSLXnN_10)); }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * get_keBYOCobzoABWhYylPhjApVSLXnN_10() const { return ___keBYOCobzoABWhYylPhjApVSLXnN_10; }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD ** get_address_of_keBYOCobzoABWhYylPhjApVSLXnN_10() { return &___keBYOCobzoABWhYylPhjApVSLXnN_10; }
	inline void set_keBYOCobzoABWhYylPhjApVSLXnN_10(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * value)
	{
		___keBYOCobzoABWhYylPhjApVSLXnN_10 = value;
		Il2CppCodeGenWriteBarrier((&___keBYOCobzoABWhYylPhjApVSLXnN_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09_H
#ifndef LINUXSTANDALONEPRIMARYINPUTSOURCE_TCD04EC6568123793A33EDD120E970E68B321AAF7_H
#define LINUXSTANDALONEPRIMARYINPUTSOURCE_TCD04EC6568123793A33EDD120E970E68B321AAF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.LinuxStandalonePrimaryInputSource
struct  LinuxStandalonePrimaryInputSource_tCD04EC6568123793A33EDD120E970E68B321AAF7 
{
public:
	// System.Int32 Rewired.Platforms.LinuxStandalonePrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LinuxStandalonePrimaryInputSource_tCD04EC6568123793A33EDD120E970E68B321AAF7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINUXSTANDALONEPRIMARYINPUTSOURCE_TCD04EC6568123793A33EDD120E970E68B321AAF7_H
#ifndef OSXSTANDALONEPRIMARYINPUTSOURCE_TDA7CCA1D9759DDC562E7A934D2B65E0FFFAE88A1_H
#define OSXSTANDALONEPRIMARYINPUTSOURCE_TDA7CCA1D9759DDC562E7A934D2B65E0FFFAE88A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.OSXStandalonePrimaryInputSource
struct  OSXStandalonePrimaryInputSource_tDA7CCA1D9759DDC562E7A934D2B65E0FFFAE88A1 
{
public:
	// System.Int32 Rewired.Platforms.OSXStandalonePrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OSXStandalonePrimaryInputSource_tDA7CCA1D9759DDC562E7A934D2B65E0FFFAE88A1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSXSTANDALONEPRIMARYINPUTSOURCE_TDA7CCA1D9759DDC562E7A934D2B65E0FFFAE88A1_H
#ifndef PS4PRIMARYINPUTSOURCE_T88E3A7EC68E12E57E89FB6FA08E707B2F19942CA_H
#define PS4PRIMARYINPUTSOURCE_T88E3A7EC68E12E57E89FB6FA08E707B2F19942CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4PrimaryInputSource
struct  PS4PrimaryInputSource_t88E3A7EC68E12E57E89FB6FA08E707B2F19942CA 
{
public:
	// System.Int32 Rewired.Platforms.PS4PrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PS4PrimaryInputSource_t88E3A7EC68E12E57E89FB6FA08E707B2F19942CA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PS4PRIMARYINPUTSOURCE_T88E3A7EC68E12E57E89FB6FA08E707B2F19942CA_H
#ifndef WEBGLPRIMARYINPUTSOURCE_TCB5C5C878C66CD363BD72A800E74A4A285A9CD07_H
#define WEBGLPRIMARYINPUTSOURCE_TCB5C5C878C66CD363BD72A800E74A4A285A9CD07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebGLPrimaryInputSource
struct  WebGLPrimaryInputSource_tCB5C5C878C66CD363BD72A800E74A4A285A9CD07 
{
public:
	// System.Int32 Rewired.Platforms.WebGLPrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebGLPrimaryInputSource_tCB5C5C878C66CD363BD72A800E74A4A285A9CD07, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBGLPRIMARYINPUTSOURCE_TCB5C5C878C66CD363BD72A800E74A4A285A9CD07_H
#ifndef WINDOWSSTANDALONEPRIMARYINPUTSOURCE_T58DDCF841E98644C97531C71AB22F4411187AF54_H
#define WINDOWSSTANDALONEPRIMARYINPUTSOURCE_T58DDCF841E98644C97531C71AB22F4411187AF54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WindowsStandalonePrimaryInputSource
struct  WindowsStandalonePrimaryInputSource_t58DDCF841E98644C97531C71AB22F4411187AF54 
{
public:
	// System.Int32 Rewired.Platforms.WindowsStandalonePrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WindowsStandalonePrimaryInputSource_t58DDCF841E98644C97531C71AB22F4411187AF54, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWSSTANDALONEPRIMARYINPUTSOURCE_T58DDCF841E98644C97531C71AB22F4411187AF54_H
#ifndef WINDOWSUWPPRIMARYINPUTSOURCE_TF6B5835E6B61328E5EF2EC8AD43C6C5B83974F66_H
#define WINDOWSUWPPRIMARYINPUTSOURCE_TF6B5835E6B61328E5EF2EC8AD43C6C5B83974F66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WindowsUWPPrimaryInputSource
struct  WindowsUWPPrimaryInputSource_tF6B5835E6B61328E5EF2EC8AD43C6C5B83974F66 
{
public:
	// System.Int32 Rewired.Platforms.WindowsUWPPrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WindowsUWPPrimaryInputSource_tF6B5835E6B61328E5EF2EC8AD43C6C5B83974F66, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWSUWPPRIMARYINPUTSOURCE_TF6B5835E6B61328E5EF2EC8AD43C6C5B83974F66_H
#ifndef XBOXONEPRIMARYINPUTSOURCE_T41A15179DDA452C363479AB56320EBEA7103B38E_H
#define XBOXONEPRIMARYINPUTSOURCE_T41A15179DDA452C363479AB56320EBEA7103B38E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.XboxOnePrimaryInputSource
struct  XboxOnePrimaryInputSource_t41A15179DDA452C363479AB56320EBEA7103B38E 
{
public:
	// System.Int32 Rewired.Platforms.XboxOnePrimaryInputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XboxOnePrimaryInputSource_t41A15179DDA452C363479AB56320EBEA7103B38E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XBOXONEPRIMARYINPUTSOURCE_T41A15179DDA452C363479AB56320EBEA7103B38E_H
#ifndef AXIS2D_TA62A3EAF575DEE91FF86DCEE3B2075AFA54705B7_H
#define AXIS2D_TA62A3EAF575DEE91FF86DCEE3B2075AFA54705B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Axis2D
struct  Axis2D_tA62A3EAF575DEE91FF86DCEE3B2075AFA54705B7  : public CompoundElement_tA56F63F47F096A626ADF63799EEF977B5E4E632E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS2D_TA62A3EAF575DEE91FF86DCEE3B2075AFA54705B7_H
#ifndef DEFINITION_TABC9A67DA4AE30F7D1070DD41541C2F97780F86B_H
#define DEFINITION_TABC9A67DA4AE30F7D1070DD41541C2F97780F86B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Axis2D_Definition
struct  Definition_tABC9A67DA4AE30F7D1070DD41541C2F97780F86B  : public Definition_t3A8A60468526C12F075318D337B6FA439005CA39
{
public:
	// Rewired.PlayerController_Axis_Definition Rewired.PlayerController_Axis2D_Definition::kOXOgojqZxOWXJWVYHpqCqJXQBb
	Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9 * ___kOXOgojqZxOWXJWVYHpqCqJXQBb_2;
	// Rewired.PlayerController_Axis_Definition Rewired.PlayerController_Axis2D_Definition::qMQJsNmTJihTirHMJKUghCLgmDa
	Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9 * ___qMQJsNmTJihTirHMJKUghCLgmDa_3;

public:
	inline static int32_t get_offset_of_kOXOgojqZxOWXJWVYHpqCqJXQBb_2() { return static_cast<int32_t>(offsetof(Definition_tABC9A67DA4AE30F7D1070DD41541C2F97780F86B, ___kOXOgojqZxOWXJWVYHpqCqJXQBb_2)); }
	inline Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9 * get_kOXOgojqZxOWXJWVYHpqCqJXQBb_2() const { return ___kOXOgojqZxOWXJWVYHpqCqJXQBb_2; }
	inline Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9 ** get_address_of_kOXOgojqZxOWXJWVYHpqCqJXQBb_2() { return &___kOXOgojqZxOWXJWVYHpqCqJXQBb_2; }
	inline void set_kOXOgojqZxOWXJWVYHpqCqJXQBb_2(Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9 * value)
	{
		___kOXOgojqZxOWXJWVYHpqCqJXQBb_2 = value;
		Il2CppCodeGenWriteBarrier((&___kOXOgojqZxOWXJWVYHpqCqJXQBb_2), value);
	}

	inline static int32_t get_offset_of_qMQJsNmTJihTirHMJKUghCLgmDa_3() { return static_cast<int32_t>(offsetof(Definition_tABC9A67DA4AE30F7D1070DD41541C2F97780F86B, ___qMQJsNmTJihTirHMJKUghCLgmDa_3)); }
	inline Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9 * get_qMQJsNmTJihTirHMJKUghCLgmDa_3() const { return ___qMQJsNmTJihTirHMJKUghCLgmDa_3; }
	inline Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9 ** get_address_of_qMQJsNmTJihTirHMJKUghCLgmDa_3() { return &___qMQJsNmTJihTirHMJKUghCLgmDa_3; }
	inline void set_qMQJsNmTJihTirHMJKUghCLgmDa_3(Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9 * value)
	{
		___qMQJsNmTJihTirHMJKUghCLgmDa_3 = value;
		Il2CppCodeGenWriteBarrier((&___qMQJsNmTJihTirHMJKUghCLgmDa_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITION_TABC9A67DA4AE30F7D1070DD41541C2F97780F86B_H
#ifndef BUTTON_T66C2A534EFD269FA571DA08D484624525061CB9A_H
#define BUTTON_T66C2A534EFD269FA571DA08D484624525061CB9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Button
struct  Button_t66C2A534EFD269FA571DA08D484624525061CB9A  : public ElementWithSource_t69752C9D210EDEE80C6FD0F291364B0681FDB4ED
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T66C2A534EFD269FA571DA08D484624525061CB9A_H
#ifndef DEFINITION_T41DF50915C398BCAAB784454ED9B1D7CCFDED5DA_H
#define DEFINITION_T41DF50915C398BCAAB784454ED9B1D7CCFDED5DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Button_Definition
struct  Definition_t41DF50915C398BCAAB784454ED9B1D7CCFDED5DA  : public Definition_t18A5B017F634EC0B2DB9B29ADB9294C16C71F6BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITION_T41DF50915C398BCAAB784454ED9B1D7CCFDED5DA_H
#ifndef COMPOUNDTYPES_T02641D39033795CA47618FFBED3164C68954742D_H
#define COMPOUNDTYPES_T02641D39033795CA47618FFBED3164C68954742D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Element_CompoundTypes
struct  CompoundTypes_t02641D39033795CA47618FFBED3164C68954742D 
{
public:
	// System.Int32 Rewired.PlayerController_Element_CompoundTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompoundTypes_t02641D39033795CA47618FFBED3164C68954742D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDTYPES_T02641D39033795CA47618FFBED3164C68954742D_H
#ifndef TYPE_TAC1607CC90FDFD13003AF557B815C0332390D80C_H
#define TYPE_TAC1607CC90FDFD13003AF557B815C0332390D80C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Element_Type
struct  Type_tAC1607CC90FDFD13003AF557B815C0332390D80C 
{
public:
	// System.Int32 Rewired.PlayerController_Element_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tAC1607CC90FDFD13003AF557B815C0332390D80C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_TAC1607CC90FDFD13003AF557B815C0332390D80C_H
#ifndef TYPEWITHSOURCE_T18AB19C95A16F87755AB2F248CEB6A5E507E7667_H
#define TYPEWITHSOURCE_T18AB19C95A16F87755AB2F248CEB6A5E507E7667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Element_TypeWithSource
struct  TypeWithSource_t18AB19C95A16F87755AB2F248CEB6A5E507E7667 
{
public:
	// System.Int32 Rewired.PlayerController_Element_TypeWithSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeWithSource_t18AB19C95A16F87755AB2F248CEB6A5E507E7667, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEWITHSOURCE_T18AB19C95A16F87755AB2F248CEB6A5E507E7667_H
#ifndef MOVEMENTAREAUNIT_T25F2042B52F33CFAE132EB793F341081375940C1_H
#define MOVEMENTAREAUNIT_T25F2042B52F33CFAE132EB793F341081375940C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerMouse_MovementAreaUnit
struct  MovementAreaUnit_t25F2042B52F33CFAE132EB793F341081375940C1 
{
public:
	// System.Int32 Rewired.PlayerMouse_MovementAreaUnit::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MovementAreaUnit_t25F2042B52F33CFAE132EB793F341081375940C1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTAREAUNIT_T25F2042B52F33CFAE132EB793F341081375940C1_H
#ifndef POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#define POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Pole
struct  Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C 
{
public:
	// System.Int32 Rewired.Pole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifndef DATATYPE_T2657C86FEC483E9329AD21CA3C61E9F50DA0A034_H
#define DATATYPE_T2657C86FEC483E9329AD21CA3C61E9F50DA0A034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.TypeWrapper_DataType
struct  DataType_t2657C86FEC483E9329AD21CA3C61E9F50DA0A034 
{
public:
	// System.Int32 Rewired.Utils.Classes.Data.TypeWrapper_DataType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataType_t2657C86FEC483E9329AD21CA3C61E9F50DA0A034, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_T2657C86FEC483E9329AD21CA3C61E9F50DA0A034_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef CONFIGVARS_TD3FE004E260819ED3EEDD720972407A874379B41_H
#define CONFIGVARS_TD3FE004E260819ED3EEDD720972407A874379B41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ConfigVars
struct  ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41  : public RuntimeObject
{
public:
	// Rewired.Config.UpdateLoopSetting Rewired.Data.ConfigVars::updateLoop
	int32_t ___updateLoop_0;
	// System.Boolean Rewired.Data.ConfigVars::alwaysUseUnityInput
	bool ___alwaysUseUnityInput_1;
	// Rewired.Platforms.WindowsStandalonePrimaryInputSource Rewired.Data.ConfigVars::windowsStandalonePrimaryInputSource
	int32_t ___windowsStandalonePrimaryInputSource_2;
	// Rewired.Platforms.OSXStandalonePrimaryInputSource Rewired.Data.ConfigVars::osx_primaryInputSource
	int32_t ___osx_primaryInputSource_3;
	// Rewired.Platforms.LinuxStandalonePrimaryInputSource Rewired.Data.ConfigVars::linux_primaryInputSource
	int32_t ___linux_primaryInputSource_4;
	// Rewired.Platforms.WindowsUWPPrimaryInputSource Rewired.Data.ConfigVars::windowsUWP_primaryInputSource
	int32_t ___windowsUWP_primaryInputSource_5;
	// Rewired.Platforms.XboxOnePrimaryInputSource Rewired.Data.ConfigVars::xboxOne_primaryInputSource
	int32_t ___xboxOne_primaryInputSource_6;
	// Rewired.Platforms.PS4PrimaryInputSource Rewired.Data.ConfigVars::ps4_primaryInputSource
	int32_t ___ps4_primaryInputSource_7;
	// Rewired.Platforms.WebGLPrimaryInputSource Rewired.Data.ConfigVars::webGL_primaryInputSource
	int32_t ___webGL_primaryInputSource_8;
	// System.Boolean Rewired.Data.ConfigVars::useXInput
	bool ___useXInput_9;
	// System.Boolean Rewired.Data.ConfigVars::useNativeMouse
	bool ___useNativeMouse_10;
	// System.Boolean Rewired.Data.ConfigVars::useEnhancedDeviceSupport
	bool ___useEnhancedDeviceSupport_11;
	// System.Boolean Rewired.Data.ConfigVars::windowsStandalone_useSteamRawInputControllerWorkaround
	bool ___windowsStandalone_useSteamRawInputControllerWorkaround_12;
	// System.Boolean Rewired.Data.ConfigVars::osxStandalone_useEnhancedDeviceSupport
	bool ___osxStandalone_useEnhancedDeviceSupport_13;
	// System.Boolean Rewired.Data.ConfigVars::android_supportUnknownGamepads
	bool ___android_supportUnknownGamepads_14;
	// System.Boolean Rewired.Data.ConfigVars::ps4_assignJoysticksByPS4JoyId
	bool ___ps4_assignJoysticksByPS4JoyId_15;
	// System.Boolean Rewired.Data.ConfigVars::useSteamControllerSupport
	bool ___useSteamControllerSupport_16;
	// System.Boolean Rewired.Data.ConfigVars::logToScreen
	bool ___logToScreen_17;
	// System.Boolean Rewired.Data.ConfigVars::runInEditMode
	bool ___runInEditMode_18;
	// System.Boolean Rewired.Data.ConfigVars::allowInputInEditorSceneView
	bool ___allowInputInEditorSceneView_19;
	// Rewired.Data.ConfigVars_PlatformVars_WindowsStandalone Rewired.Data.ConfigVars::platformVars_windowsStandalone
	PlatformVars_WindowsStandalone_t1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A * ___platformVars_windowsStandalone_20;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_linuxStandalone
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_linuxStandalone_21;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_osxStandalone
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_osxStandalone_22;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_windows8Store
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_windows8Store_23;
	// Rewired.Data.ConfigVars_PlatformVars_WindowsUWP Rewired.Data.ConfigVars::platformVars_windowsUWP
	PlatformVars_WindowsUWP_t3F16819E5BF554DD60A4D1E2464E38C88A9D73D8 * ___platformVars_windowsUWP_24;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_iOS
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_iOS_25;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_tvOS
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_tvOS_26;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_android
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_android_27;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_ps3
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_ps3_28;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_ps4
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_ps4_29;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_psVita
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_psVita_30;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_xbox360
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_xbox360_31;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_xboxOne
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_xboxOne_32;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_wii
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_wii_33;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_wiiu
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_wiiu_34;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_switch
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_switch_35;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_webGL
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_webGL_36;
	// Rewired.Data.ConfigVars_PlatformVars Rewired.Data.ConfigVars::platformVars_unknown
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * ___platformVars_unknown_37;
	// System.Int32 Rewired.Data.ConfigVars::maxJoysticksPerPlayer
	int32_t ___maxJoysticksPerPlayer_38;
	// System.Boolean Rewired.Data.ConfigVars::autoAssignJoysticks
	bool ___autoAssignJoysticks_39;
	// System.Boolean Rewired.Data.ConfigVars::assignJoysticksToPlayingPlayersOnly
	bool ___assignJoysticksToPlayingPlayersOnly_40;
	// System.Boolean Rewired.Data.ConfigVars::distributeJoysticksEvenly
	bool ___distributeJoysticksEvenly_41;
	// System.Boolean Rewired.Data.ConfigVars::reassignJoystickToPreviousOwnerOnReconnect
	bool ___reassignJoystickToPreviousOwnerOnReconnect_42;
	// Rewired.DeadZone2DType Rewired.Data.ConfigVars::defaultJoystickAxis2DDeadZoneType
	int32_t ___defaultJoystickAxis2DDeadZoneType_43;
	// Rewired.AxisSensitivity2DType Rewired.Data.ConfigVars::defaultJoystickAxis2DSensitivityType
	int32_t ___defaultJoystickAxis2DSensitivityType_44;
	// Rewired.AxisSensitivityType Rewired.Data.ConfigVars::defaultAxisSensitivityType
	int32_t ___defaultAxisSensitivityType_45;
	// System.Boolean Rewired.Data.ConfigVars::force4WayHats
	bool ___force4WayHats_46;
	// Rewired.Config.ThrottleCalibrationMode Rewired.Data.ConfigVars::throttleCalibrationMode
	int32_t ___throttleCalibrationMode_47;
	// System.Boolean Rewired.Data.ConfigVars::activateActionButtonsOnNegativeValue
	bool ___activateActionButtonsOnNegativeValue_48;
	// System.Boolean Rewired.Data.ConfigVars::deferControllerConnectedEventsOnStart
	bool ___deferControllerConnectedEventsOnStart_49;
	// Rewired.Config.LogLevelFlags Rewired.Data.ConfigVars::logLevel
	int32_t ___logLevel_50;
	// Rewired.Data.ConfigVars_EditorVars Rewired.Data.ConfigVars::editorSettings
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF * ___editorSettings_51;
	// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Data.ConfigVars_EtiwYEyEvxhGdDZwPhVpQNWkBYX> Rewired.Data.ConfigVars::__platformVarsDict
	Dictionary_2_t2E49D98A4B76F81C22CA318194D3185163B54A89 * _____platformVarsDict_52;
	// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Data.ConfigVars_DadcqoCjLXmahnTWBdNQJAWfNLqB> Rewired.Data.ConfigVars::__getSetPlatformVariableDict
	Dictionary_2_tA052622BA982CD488FA11E7207A3A0230C399552 * _____getSetPlatformVariableDict_53;

public:
	inline static int32_t get_offset_of_updateLoop_0() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___updateLoop_0)); }
	inline int32_t get_updateLoop_0() const { return ___updateLoop_0; }
	inline int32_t* get_address_of_updateLoop_0() { return &___updateLoop_0; }
	inline void set_updateLoop_0(int32_t value)
	{
		___updateLoop_0 = value;
	}

	inline static int32_t get_offset_of_alwaysUseUnityInput_1() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___alwaysUseUnityInput_1)); }
	inline bool get_alwaysUseUnityInput_1() const { return ___alwaysUseUnityInput_1; }
	inline bool* get_address_of_alwaysUseUnityInput_1() { return &___alwaysUseUnityInput_1; }
	inline void set_alwaysUseUnityInput_1(bool value)
	{
		___alwaysUseUnityInput_1 = value;
	}

	inline static int32_t get_offset_of_windowsStandalonePrimaryInputSource_2() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___windowsStandalonePrimaryInputSource_2)); }
	inline int32_t get_windowsStandalonePrimaryInputSource_2() const { return ___windowsStandalonePrimaryInputSource_2; }
	inline int32_t* get_address_of_windowsStandalonePrimaryInputSource_2() { return &___windowsStandalonePrimaryInputSource_2; }
	inline void set_windowsStandalonePrimaryInputSource_2(int32_t value)
	{
		___windowsStandalonePrimaryInputSource_2 = value;
	}

	inline static int32_t get_offset_of_osx_primaryInputSource_3() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___osx_primaryInputSource_3)); }
	inline int32_t get_osx_primaryInputSource_3() const { return ___osx_primaryInputSource_3; }
	inline int32_t* get_address_of_osx_primaryInputSource_3() { return &___osx_primaryInputSource_3; }
	inline void set_osx_primaryInputSource_3(int32_t value)
	{
		___osx_primaryInputSource_3 = value;
	}

	inline static int32_t get_offset_of_linux_primaryInputSource_4() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___linux_primaryInputSource_4)); }
	inline int32_t get_linux_primaryInputSource_4() const { return ___linux_primaryInputSource_4; }
	inline int32_t* get_address_of_linux_primaryInputSource_4() { return &___linux_primaryInputSource_4; }
	inline void set_linux_primaryInputSource_4(int32_t value)
	{
		___linux_primaryInputSource_4 = value;
	}

	inline static int32_t get_offset_of_windowsUWP_primaryInputSource_5() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___windowsUWP_primaryInputSource_5)); }
	inline int32_t get_windowsUWP_primaryInputSource_5() const { return ___windowsUWP_primaryInputSource_5; }
	inline int32_t* get_address_of_windowsUWP_primaryInputSource_5() { return &___windowsUWP_primaryInputSource_5; }
	inline void set_windowsUWP_primaryInputSource_5(int32_t value)
	{
		___windowsUWP_primaryInputSource_5 = value;
	}

	inline static int32_t get_offset_of_xboxOne_primaryInputSource_6() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___xboxOne_primaryInputSource_6)); }
	inline int32_t get_xboxOne_primaryInputSource_6() const { return ___xboxOne_primaryInputSource_6; }
	inline int32_t* get_address_of_xboxOne_primaryInputSource_6() { return &___xboxOne_primaryInputSource_6; }
	inline void set_xboxOne_primaryInputSource_6(int32_t value)
	{
		___xboxOne_primaryInputSource_6 = value;
	}

	inline static int32_t get_offset_of_ps4_primaryInputSource_7() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___ps4_primaryInputSource_7)); }
	inline int32_t get_ps4_primaryInputSource_7() const { return ___ps4_primaryInputSource_7; }
	inline int32_t* get_address_of_ps4_primaryInputSource_7() { return &___ps4_primaryInputSource_7; }
	inline void set_ps4_primaryInputSource_7(int32_t value)
	{
		___ps4_primaryInputSource_7 = value;
	}

	inline static int32_t get_offset_of_webGL_primaryInputSource_8() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___webGL_primaryInputSource_8)); }
	inline int32_t get_webGL_primaryInputSource_8() const { return ___webGL_primaryInputSource_8; }
	inline int32_t* get_address_of_webGL_primaryInputSource_8() { return &___webGL_primaryInputSource_8; }
	inline void set_webGL_primaryInputSource_8(int32_t value)
	{
		___webGL_primaryInputSource_8 = value;
	}

	inline static int32_t get_offset_of_useXInput_9() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___useXInput_9)); }
	inline bool get_useXInput_9() const { return ___useXInput_9; }
	inline bool* get_address_of_useXInput_9() { return &___useXInput_9; }
	inline void set_useXInput_9(bool value)
	{
		___useXInput_9 = value;
	}

	inline static int32_t get_offset_of_useNativeMouse_10() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___useNativeMouse_10)); }
	inline bool get_useNativeMouse_10() const { return ___useNativeMouse_10; }
	inline bool* get_address_of_useNativeMouse_10() { return &___useNativeMouse_10; }
	inline void set_useNativeMouse_10(bool value)
	{
		___useNativeMouse_10 = value;
	}

	inline static int32_t get_offset_of_useEnhancedDeviceSupport_11() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___useEnhancedDeviceSupport_11)); }
	inline bool get_useEnhancedDeviceSupport_11() const { return ___useEnhancedDeviceSupport_11; }
	inline bool* get_address_of_useEnhancedDeviceSupport_11() { return &___useEnhancedDeviceSupport_11; }
	inline void set_useEnhancedDeviceSupport_11(bool value)
	{
		___useEnhancedDeviceSupport_11 = value;
	}

	inline static int32_t get_offset_of_windowsStandalone_useSteamRawInputControllerWorkaround_12() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___windowsStandalone_useSteamRawInputControllerWorkaround_12)); }
	inline bool get_windowsStandalone_useSteamRawInputControllerWorkaround_12() const { return ___windowsStandalone_useSteamRawInputControllerWorkaround_12; }
	inline bool* get_address_of_windowsStandalone_useSteamRawInputControllerWorkaround_12() { return &___windowsStandalone_useSteamRawInputControllerWorkaround_12; }
	inline void set_windowsStandalone_useSteamRawInputControllerWorkaround_12(bool value)
	{
		___windowsStandalone_useSteamRawInputControllerWorkaround_12 = value;
	}

	inline static int32_t get_offset_of_osxStandalone_useEnhancedDeviceSupport_13() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___osxStandalone_useEnhancedDeviceSupport_13)); }
	inline bool get_osxStandalone_useEnhancedDeviceSupport_13() const { return ___osxStandalone_useEnhancedDeviceSupport_13; }
	inline bool* get_address_of_osxStandalone_useEnhancedDeviceSupport_13() { return &___osxStandalone_useEnhancedDeviceSupport_13; }
	inline void set_osxStandalone_useEnhancedDeviceSupport_13(bool value)
	{
		___osxStandalone_useEnhancedDeviceSupport_13 = value;
	}

	inline static int32_t get_offset_of_android_supportUnknownGamepads_14() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___android_supportUnknownGamepads_14)); }
	inline bool get_android_supportUnknownGamepads_14() const { return ___android_supportUnknownGamepads_14; }
	inline bool* get_address_of_android_supportUnknownGamepads_14() { return &___android_supportUnknownGamepads_14; }
	inline void set_android_supportUnknownGamepads_14(bool value)
	{
		___android_supportUnknownGamepads_14 = value;
	}

	inline static int32_t get_offset_of_ps4_assignJoysticksByPS4JoyId_15() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___ps4_assignJoysticksByPS4JoyId_15)); }
	inline bool get_ps4_assignJoysticksByPS4JoyId_15() const { return ___ps4_assignJoysticksByPS4JoyId_15; }
	inline bool* get_address_of_ps4_assignJoysticksByPS4JoyId_15() { return &___ps4_assignJoysticksByPS4JoyId_15; }
	inline void set_ps4_assignJoysticksByPS4JoyId_15(bool value)
	{
		___ps4_assignJoysticksByPS4JoyId_15 = value;
	}

	inline static int32_t get_offset_of_useSteamControllerSupport_16() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___useSteamControllerSupport_16)); }
	inline bool get_useSteamControllerSupport_16() const { return ___useSteamControllerSupport_16; }
	inline bool* get_address_of_useSteamControllerSupport_16() { return &___useSteamControllerSupport_16; }
	inline void set_useSteamControllerSupport_16(bool value)
	{
		___useSteamControllerSupport_16 = value;
	}

	inline static int32_t get_offset_of_logToScreen_17() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___logToScreen_17)); }
	inline bool get_logToScreen_17() const { return ___logToScreen_17; }
	inline bool* get_address_of_logToScreen_17() { return &___logToScreen_17; }
	inline void set_logToScreen_17(bool value)
	{
		___logToScreen_17 = value;
	}

	inline static int32_t get_offset_of_runInEditMode_18() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___runInEditMode_18)); }
	inline bool get_runInEditMode_18() const { return ___runInEditMode_18; }
	inline bool* get_address_of_runInEditMode_18() { return &___runInEditMode_18; }
	inline void set_runInEditMode_18(bool value)
	{
		___runInEditMode_18 = value;
	}

	inline static int32_t get_offset_of_allowInputInEditorSceneView_19() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___allowInputInEditorSceneView_19)); }
	inline bool get_allowInputInEditorSceneView_19() const { return ___allowInputInEditorSceneView_19; }
	inline bool* get_address_of_allowInputInEditorSceneView_19() { return &___allowInputInEditorSceneView_19; }
	inline void set_allowInputInEditorSceneView_19(bool value)
	{
		___allowInputInEditorSceneView_19 = value;
	}

	inline static int32_t get_offset_of_platformVars_windowsStandalone_20() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_windowsStandalone_20)); }
	inline PlatformVars_WindowsStandalone_t1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A * get_platformVars_windowsStandalone_20() const { return ___platformVars_windowsStandalone_20; }
	inline PlatformVars_WindowsStandalone_t1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A ** get_address_of_platformVars_windowsStandalone_20() { return &___platformVars_windowsStandalone_20; }
	inline void set_platformVars_windowsStandalone_20(PlatformVars_WindowsStandalone_t1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A * value)
	{
		___platformVars_windowsStandalone_20 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_windowsStandalone_20), value);
	}

	inline static int32_t get_offset_of_platformVars_linuxStandalone_21() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_linuxStandalone_21)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_linuxStandalone_21() const { return ___platformVars_linuxStandalone_21; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_linuxStandalone_21() { return &___platformVars_linuxStandalone_21; }
	inline void set_platformVars_linuxStandalone_21(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_linuxStandalone_21 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_linuxStandalone_21), value);
	}

	inline static int32_t get_offset_of_platformVars_osxStandalone_22() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_osxStandalone_22)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_osxStandalone_22() const { return ___platformVars_osxStandalone_22; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_osxStandalone_22() { return &___platformVars_osxStandalone_22; }
	inline void set_platformVars_osxStandalone_22(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_osxStandalone_22 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_osxStandalone_22), value);
	}

	inline static int32_t get_offset_of_platformVars_windows8Store_23() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_windows8Store_23)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_windows8Store_23() const { return ___platformVars_windows8Store_23; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_windows8Store_23() { return &___platformVars_windows8Store_23; }
	inline void set_platformVars_windows8Store_23(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_windows8Store_23 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_windows8Store_23), value);
	}

	inline static int32_t get_offset_of_platformVars_windowsUWP_24() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_windowsUWP_24)); }
	inline PlatformVars_WindowsUWP_t3F16819E5BF554DD60A4D1E2464E38C88A9D73D8 * get_platformVars_windowsUWP_24() const { return ___platformVars_windowsUWP_24; }
	inline PlatformVars_WindowsUWP_t3F16819E5BF554DD60A4D1E2464E38C88A9D73D8 ** get_address_of_platformVars_windowsUWP_24() { return &___platformVars_windowsUWP_24; }
	inline void set_platformVars_windowsUWP_24(PlatformVars_WindowsUWP_t3F16819E5BF554DD60A4D1E2464E38C88A9D73D8 * value)
	{
		___platformVars_windowsUWP_24 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_windowsUWP_24), value);
	}

	inline static int32_t get_offset_of_platformVars_iOS_25() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_iOS_25)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_iOS_25() const { return ___platformVars_iOS_25; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_iOS_25() { return &___platformVars_iOS_25; }
	inline void set_platformVars_iOS_25(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_iOS_25 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_iOS_25), value);
	}

	inline static int32_t get_offset_of_platformVars_tvOS_26() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_tvOS_26)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_tvOS_26() const { return ___platformVars_tvOS_26; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_tvOS_26() { return &___platformVars_tvOS_26; }
	inline void set_platformVars_tvOS_26(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_tvOS_26 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_tvOS_26), value);
	}

	inline static int32_t get_offset_of_platformVars_android_27() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_android_27)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_android_27() const { return ___platformVars_android_27; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_android_27() { return &___platformVars_android_27; }
	inline void set_platformVars_android_27(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_android_27 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_android_27), value);
	}

	inline static int32_t get_offset_of_platformVars_ps3_28() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_ps3_28)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_ps3_28() const { return ___platformVars_ps3_28; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_ps3_28() { return &___platformVars_ps3_28; }
	inline void set_platformVars_ps3_28(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_ps3_28 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_ps3_28), value);
	}

	inline static int32_t get_offset_of_platformVars_ps4_29() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_ps4_29)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_ps4_29() const { return ___platformVars_ps4_29; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_ps4_29() { return &___platformVars_ps4_29; }
	inline void set_platformVars_ps4_29(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_ps4_29 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_ps4_29), value);
	}

	inline static int32_t get_offset_of_platformVars_psVita_30() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_psVita_30)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_psVita_30() const { return ___platformVars_psVita_30; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_psVita_30() { return &___platformVars_psVita_30; }
	inline void set_platformVars_psVita_30(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_psVita_30 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_psVita_30), value);
	}

	inline static int32_t get_offset_of_platformVars_xbox360_31() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_xbox360_31)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_xbox360_31() const { return ___platformVars_xbox360_31; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_xbox360_31() { return &___platformVars_xbox360_31; }
	inline void set_platformVars_xbox360_31(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_xbox360_31 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_xbox360_31), value);
	}

	inline static int32_t get_offset_of_platformVars_xboxOne_32() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_xboxOne_32)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_xboxOne_32() const { return ___platformVars_xboxOne_32; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_xboxOne_32() { return &___platformVars_xboxOne_32; }
	inline void set_platformVars_xboxOne_32(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_xboxOne_32 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_xboxOne_32), value);
	}

	inline static int32_t get_offset_of_platformVars_wii_33() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_wii_33)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_wii_33() const { return ___platformVars_wii_33; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_wii_33() { return &___platformVars_wii_33; }
	inline void set_platformVars_wii_33(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_wii_33 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_wii_33), value);
	}

	inline static int32_t get_offset_of_platformVars_wiiu_34() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_wiiu_34)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_wiiu_34() const { return ___platformVars_wiiu_34; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_wiiu_34() { return &___platformVars_wiiu_34; }
	inline void set_platformVars_wiiu_34(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_wiiu_34 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_wiiu_34), value);
	}

	inline static int32_t get_offset_of_platformVars_switch_35() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_switch_35)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_switch_35() const { return ___platformVars_switch_35; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_switch_35() { return &___platformVars_switch_35; }
	inline void set_platformVars_switch_35(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_switch_35 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_switch_35), value);
	}

	inline static int32_t get_offset_of_platformVars_webGL_36() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_webGL_36)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_webGL_36() const { return ___platformVars_webGL_36; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_webGL_36() { return &___platformVars_webGL_36; }
	inline void set_platformVars_webGL_36(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_webGL_36 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_webGL_36), value);
	}

	inline static int32_t get_offset_of_platformVars_unknown_37() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___platformVars_unknown_37)); }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * get_platformVars_unknown_37() const { return ___platformVars_unknown_37; }
	inline PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA ** get_address_of_platformVars_unknown_37() { return &___platformVars_unknown_37; }
	inline void set_platformVars_unknown_37(PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA * value)
	{
		___platformVars_unknown_37 = value;
		Il2CppCodeGenWriteBarrier((&___platformVars_unknown_37), value);
	}

	inline static int32_t get_offset_of_maxJoysticksPerPlayer_38() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___maxJoysticksPerPlayer_38)); }
	inline int32_t get_maxJoysticksPerPlayer_38() const { return ___maxJoysticksPerPlayer_38; }
	inline int32_t* get_address_of_maxJoysticksPerPlayer_38() { return &___maxJoysticksPerPlayer_38; }
	inline void set_maxJoysticksPerPlayer_38(int32_t value)
	{
		___maxJoysticksPerPlayer_38 = value;
	}

	inline static int32_t get_offset_of_autoAssignJoysticks_39() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___autoAssignJoysticks_39)); }
	inline bool get_autoAssignJoysticks_39() const { return ___autoAssignJoysticks_39; }
	inline bool* get_address_of_autoAssignJoysticks_39() { return &___autoAssignJoysticks_39; }
	inline void set_autoAssignJoysticks_39(bool value)
	{
		___autoAssignJoysticks_39 = value;
	}

	inline static int32_t get_offset_of_assignJoysticksToPlayingPlayersOnly_40() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___assignJoysticksToPlayingPlayersOnly_40)); }
	inline bool get_assignJoysticksToPlayingPlayersOnly_40() const { return ___assignJoysticksToPlayingPlayersOnly_40; }
	inline bool* get_address_of_assignJoysticksToPlayingPlayersOnly_40() { return &___assignJoysticksToPlayingPlayersOnly_40; }
	inline void set_assignJoysticksToPlayingPlayersOnly_40(bool value)
	{
		___assignJoysticksToPlayingPlayersOnly_40 = value;
	}

	inline static int32_t get_offset_of_distributeJoysticksEvenly_41() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___distributeJoysticksEvenly_41)); }
	inline bool get_distributeJoysticksEvenly_41() const { return ___distributeJoysticksEvenly_41; }
	inline bool* get_address_of_distributeJoysticksEvenly_41() { return &___distributeJoysticksEvenly_41; }
	inline void set_distributeJoysticksEvenly_41(bool value)
	{
		___distributeJoysticksEvenly_41 = value;
	}

	inline static int32_t get_offset_of_reassignJoystickToPreviousOwnerOnReconnect_42() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___reassignJoystickToPreviousOwnerOnReconnect_42)); }
	inline bool get_reassignJoystickToPreviousOwnerOnReconnect_42() const { return ___reassignJoystickToPreviousOwnerOnReconnect_42; }
	inline bool* get_address_of_reassignJoystickToPreviousOwnerOnReconnect_42() { return &___reassignJoystickToPreviousOwnerOnReconnect_42; }
	inline void set_reassignJoystickToPreviousOwnerOnReconnect_42(bool value)
	{
		___reassignJoystickToPreviousOwnerOnReconnect_42 = value;
	}

	inline static int32_t get_offset_of_defaultJoystickAxis2DDeadZoneType_43() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___defaultJoystickAxis2DDeadZoneType_43)); }
	inline int32_t get_defaultJoystickAxis2DDeadZoneType_43() const { return ___defaultJoystickAxis2DDeadZoneType_43; }
	inline int32_t* get_address_of_defaultJoystickAxis2DDeadZoneType_43() { return &___defaultJoystickAxis2DDeadZoneType_43; }
	inline void set_defaultJoystickAxis2DDeadZoneType_43(int32_t value)
	{
		___defaultJoystickAxis2DDeadZoneType_43 = value;
	}

	inline static int32_t get_offset_of_defaultJoystickAxis2DSensitivityType_44() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___defaultJoystickAxis2DSensitivityType_44)); }
	inline int32_t get_defaultJoystickAxis2DSensitivityType_44() const { return ___defaultJoystickAxis2DSensitivityType_44; }
	inline int32_t* get_address_of_defaultJoystickAxis2DSensitivityType_44() { return &___defaultJoystickAxis2DSensitivityType_44; }
	inline void set_defaultJoystickAxis2DSensitivityType_44(int32_t value)
	{
		___defaultJoystickAxis2DSensitivityType_44 = value;
	}

	inline static int32_t get_offset_of_defaultAxisSensitivityType_45() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___defaultAxisSensitivityType_45)); }
	inline int32_t get_defaultAxisSensitivityType_45() const { return ___defaultAxisSensitivityType_45; }
	inline int32_t* get_address_of_defaultAxisSensitivityType_45() { return &___defaultAxisSensitivityType_45; }
	inline void set_defaultAxisSensitivityType_45(int32_t value)
	{
		___defaultAxisSensitivityType_45 = value;
	}

	inline static int32_t get_offset_of_force4WayHats_46() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___force4WayHats_46)); }
	inline bool get_force4WayHats_46() const { return ___force4WayHats_46; }
	inline bool* get_address_of_force4WayHats_46() { return &___force4WayHats_46; }
	inline void set_force4WayHats_46(bool value)
	{
		___force4WayHats_46 = value;
	}

	inline static int32_t get_offset_of_throttleCalibrationMode_47() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___throttleCalibrationMode_47)); }
	inline int32_t get_throttleCalibrationMode_47() const { return ___throttleCalibrationMode_47; }
	inline int32_t* get_address_of_throttleCalibrationMode_47() { return &___throttleCalibrationMode_47; }
	inline void set_throttleCalibrationMode_47(int32_t value)
	{
		___throttleCalibrationMode_47 = value;
	}

	inline static int32_t get_offset_of_activateActionButtonsOnNegativeValue_48() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___activateActionButtonsOnNegativeValue_48)); }
	inline bool get_activateActionButtonsOnNegativeValue_48() const { return ___activateActionButtonsOnNegativeValue_48; }
	inline bool* get_address_of_activateActionButtonsOnNegativeValue_48() { return &___activateActionButtonsOnNegativeValue_48; }
	inline void set_activateActionButtonsOnNegativeValue_48(bool value)
	{
		___activateActionButtonsOnNegativeValue_48 = value;
	}

	inline static int32_t get_offset_of_deferControllerConnectedEventsOnStart_49() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___deferControllerConnectedEventsOnStart_49)); }
	inline bool get_deferControllerConnectedEventsOnStart_49() const { return ___deferControllerConnectedEventsOnStart_49; }
	inline bool* get_address_of_deferControllerConnectedEventsOnStart_49() { return &___deferControllerConnectedEventsOnStart_49; }
	inline void set_deferControllerConnectedEventsOnStart_49(bool value)
	{
		___deferControllerConnectedEventsOnStart_49 = value;
	}

	inline static int32_t get_offset_of_logLevel_50() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___logLevel_50)); }
	inline int32_t get_logLevel_50() const { return ___logLevel_50; }
	inline int32_t* get_address_of_logLevel_50() { return &___logLevel_50; }
	inline void set_logLevel_50(int32_t value)
	{
		___logLevel_50 = value;
	}

	inline static int32_t get_offset_of_editorSettings_51() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, ___editorSettings_51)); }
	inline EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF * get_editorSettings_51() const { return ___editorSettings_51; }
	inline EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF ** get_address_of_editorSettings_51() { return &___editorSettings_51; }
	inline void set_editorSettings_51(EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF * value)
	{
		___editorSettings_51 = value;
		Il2CppCodeGenWriteBarrier((&___editorSettings_51), value);
	}

	inline static int32_t get_offset_of___platformVarsDict_52() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, _____platformVarsDict_52)); }
	inline Dictionary_2_t2E49D98A4B76F81C22CA318194D3185163B54A89 * get___platformVarsDict_52() const { return _____platformVarsDict_52; }
	inline Dictionary_2_t2E49D98A4B76F81C22CA318194D3185163B54A89 ** get_address_of___platformVarsDict_52() { return &_____platformVarsDict_52; }
	inline void set___platformVarsDict_52(Dictionary_2_t2E49D98A4B76F81C22CA318194D3185163B54A89 * value)
	{
		_____platformVarsDict_52 = value;
		Il2CppCodeGenWriteBarrier((&_____platformVarsDict_52), value);
	}

	inline static int32_t get_offset_of___getSetPlatformVariableDict_53() { return static_cast<int32_t>(offsetof(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41, _____getSetPlatformVariableDict_53)); }
	inline Dictionary_2_tA052622BA982CD488FA11E7207A3A0230C399552 * get___getSetPlatformVariableDict_53() const { return _____getSetPlatformVariableDict_53; }
	inline Dictionary_2_tA052622BA982CD488FA11E7207A3A0230C399552 ** get_address_of___getSetPlatformVariableDict_53() { return &_____getSetPlatformVariableDict_53; }
	inline void set___getSetPlatformVariableDict_53(Dictionary_2_tA052622BA982CD488FA11E7207A3A0230C399552 * value)
	{
		_____getSetPlatformVariableDict_53 = value;
		Il2CppCodeGenWriteBarrier((&_____getSetPlatformVariableDict_53), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGVARS_TD3FE004E260819ED3EEDD720972407A874379B41_H
#ifndef AXIS_T504AF754E9EB12A17FB176318BADD88841A38B7E_H
#define AXIS_T504AF754E9EB12A17FB176318BADD88841A38B7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.CustomController_Editor_Axis
struct  Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E  : public Element_t4C80356DECB9FF4357706FB02B4C85C71AC29D12
{
public:
	// Rewired.AxisRange Rewired.Data.CustomController_Editor_Axis::range
	int32_t ___range_2;
	// System.Boolean Rewired.Data.CustomController_Editor_Axis::invert
	bool ___invert_3;
	// System.Single Rewired.Data.CustomController_Editor_Axis::deadZone
	float ___deadZone_4;
	// System.Single Rewired.Data.CustomController_Editor_Axis::zero
	float ___zero_5;
	// System.Single Rewired.Data.CustomController_Editor_Axis::min
	float ___min_6;
	// System.Single Rewired.Data.CustomController_Editor_Axis::max
	float ___max_7;
	// System.Boolean Rewired.Data.CustomController_Editor_Axis::doNotCalibrateRange
	bool ___doNotCalibrateRange_8;
	// Rewired.AxisSensitivityType Rewired.Data.CustomController_Editor_Axis::sensitivityType
	int32_t ___sensitivityType_9;
	// System.Single Rewired.Data.CustomController_Editor_Axis::sensitivity
	float ___sensitivity_10;
	// UnityEngine.AnimationCurve Rewired.Data.CustomController_Editor_Axis::sensitivityCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___sensitivityCurve_11;
	// Rewired.Data.Mapping.HardwareAxisInfo Rewired.Data.CustomController_Editor_Axis::axisInfo
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * ___axisInfo_12;

public:
	inline static int32_t get_offset_of_range_2() { return static_cast<int32_t>(offsetof(Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E, ___range_2)); }
	inline int32_t get_range_2() const { return ___range_2; }
	inline int32_t* get_address_of_range_2() { return &___range_2; }
	inline void set_range_2(int32_t value)
	{
		___range_2 = value;
	}

	inline static int32_t get_offset_of_invert_3() { return static_cast<int32_t>(offsetof(Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E, ___invert_3)); }
	inline bool get_invert_3() const { return ___invert_3; }
	inline bool* get_address_of_invert_3() { return &___invert_3; }
	inline void set_invert_3(bool value)
	{
		___invert_3 = value;
	}

	inline static int32_t get_offset_of_deadZone_4() { return static_cast<int32_t>(offsetof(Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E, ___deadZone_4)); }
	inline float get_deadZone_4() const { return ___deadZone_4; }
	inline float* get_address_of_deadZone_4() { return &___deadZone_4; }
	inline void set_deadZone_4(float value)
	{
		___deadZone_4 = value;
	}

	inline static int32_t get_offset_of_zero_5() { return static_cast<int32_t>(offsetof(Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E, ___zero_5)); }
	inline float get_zero_5() const { return ___zero_5; }
	inline float* get_address_of_zero_5() { return &___zero_5; }
	inline void set_zero_5(float value)
	{
		___zero_5 = value;
	}

	inline static int32_t get_offset_of_min_6() { return static_cast<int32_t>(offsetof(Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E, ___min_6)); }
	inline float get_min_6() const { return ___min_6; }
	inline float* get_address_of_min_6() { return &___min_6; }
	inline void set_min_6(float value)
	{
		___min_6 = value;
	}

	inline static int32_t get_offset_of_max_7() { return static_cast<int32_t>(offsetof(Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E, ___max_7)); }
	inline float get_max_7() const { return ___max_7; }
	inline float* get_address_of_max_7() { return &___max_7; }
	inline void set_max_7(float value)
	{
		___max_7 = value;
	}

	inline static int32_t get_offset_of_doNotCalibrateRange_8() { return static_cast<int32_t>(offsetof(Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E, ___doNotCalibrateRange_8)); }
	inline bool get_doNotCalibrateRange_8() const { return ___doNotCalibrateRange_8; }
	inline bool* get_address_of_doNotCalibrateRange_8() { return &___doNotCalibrateRange_8; }
	inline void set_doNotCalibrateRange_8(bool value)
	{
		___doNotCalibrateRange_8 = value;
	}

	inline static int32_t get_offset_of_sensitivityType_9() { return static_cast<int32_t>(offsetof(Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E, ___sensitivityType_9)); }
	inline int32_t get_sensitivityType_9() const { return ___sensitivityType_9; }
	inline int32_t* get_address_of_sensitivityType_9() { return &___sensitivityType_9; }
	inline void set_sensitivityType_9(int32_t value)
	{
		___sensitivityType_9 = value;
	}

	inline static int32_t get_offset_of_sensitivity_10() { return static_cast<int32_t>(offsetof(Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E, ___sensitivity_10)); }
	inline float get_sensitivity_10() const { return ___sensitivity_10; }
	inline float* get_address_of_sensitivity_10() { return &___sensitivity_10; }
	inline void set_sensitivity_10(float value)
	{
		___sensitivity_10 = value;
	}

	inline static int32_t get_offset_of_sensitivityCurve_11() { return static_cast<int32_t>(offsetof(Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E, ___sensitivityCurve_11)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_sensitivityCurve_11() const { return ___sensitivityCurve_11; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_sensitivityCurve_11() { return &___sensitivityCurve_11; }
	inline void set_sensitivityCurve_11(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___sensitivityCurve_11 = value;
		Il2CppCodeGenWriteBarrier((&___sensitivityCurve_11), value);
	}

	inline static int32_t get_offset_of_axisInfo_12() { return static_cast<int32_t>(offsetof(Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E, ___axisInfo_12)); }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * get_axisInfo_12() const { return ___axisInfo_12; }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 ** get_address_of_axisInfo_12() { return &___axisInfo_12; }
	inline void set_axisInfo_12(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * value)
	{
		___axisInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___axisInfo_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T504AF754E9EB12A17FB176318BADD88841A38B7E_H
#ifndef AXISCALIBRATIONINFOENTRY_T84CE6AFB327D1C49A7AD0ED2B62289CC94DBC239_H
#define AXISCALIBRATIONINFOENTRY_T84CE6AFB327D1C49A7AD0ED2B62289CC94DBC239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry
struct  AxisCalibrationInfoEntry_t84CE6AFB327D1C49A7AD0ED2B62289CC94DBC239  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.AlternateAxisCalibrationType Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry::key
	int32_t ___key_0;
	// Rewired.Data.Mapping.AxisCalibrationInfo Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry::calibration
	AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5 * ___calibration_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(AxisCalibrationInfoEntry_t84CE6AFB327D1C49A7AD0ED2B62289CC94DBC239, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_calibration_1() { return static_cast<int32_t>(offsetof(AxisCalibrationInfoEntry_t84CE6AFB327D1C49A7AD0ED2B62289CC94DBC239, ___calibration_1)); }
	inline AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5 * get_calibration_1() const { return ___calibration_1; }
	inline AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5 ** get_address_of_calibration_1() { return &___calibration_1; }
	inline void set_calibration_1(AxisCalibrationInfo_t2FBCD3ED5FAD745689CC7FE7F2B74CBA497B42E5 * value)
	{
		___calibration_1 = value;
		Il2CppCodeGenWriteBarrier((&___calibration_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCALIBRATIONINFOENTRY_T84CE6AFB327D1C49A7AD0ED2B62289CC94DBC239_H
#ifndef COMPOUNDELEMENT_TC9DDF922221721DEC6AF01C86AD61E299430E978_H
#define COMPOUNDELEMENT_TC9DDF922221721DEC6AF01C86AD61E299430E978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_CompoundElement
struct  CompoundElement_tC9DDF922221721DEC6AF01C86AD61E299430E978  : public RuntimeObject
{
public:
	// Rewired.CompoundControllerElementType Rewired.Data.Mapping.HardwareJoystickMap_CompoundElement::type
	int32_t ___type_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_CompoundElement::elementIdentifier
	int32_t ___elementIdentifier_1;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_CompoundElement::componentElementIdentifiers
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___componentElementIdentifiers_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(CompoundElement_tC9DDF922221721DEC6AF01C86AD61E299430E978, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_elementIdentifier_1() { return static_cast<int32_t>(offsetof(CompoundElement_tC9DDF922221721DEC6AF01C86AD61E299430E978, ___elementIdentifier_1)); }
	inline int32_t get_elementIdentifier_1() const { return ___elementIdentifier_1; }
	inline int32_t* get_address_of_elementIdentifier_1() { return &___elementIdentifier_1; }
	inline void set_elementIdentifier_1(int32_t value)
	{
		___elementIdentifier_1 = value;
	}

	inline static int32_t get_offset_of_componentElementIdentifiers_2() { return static_cast<int32_t>(offsetof(CompoundElement_tC9DDF922221721DEC6AF01C86AD61E299430E978, ___componentElementIdentifiers_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_componentElementIdentifiers_2() const { return ___componentElementIdentifiers_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_componentElementIdentifiers_2() { return &___componentElementIdentifiers_2; }
	inline void set_componentElementIdentifiers_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___componentElementIdentifiers_2 = value;
		Il2CppCodeGenWriteBarrier((&___componentElementIdentifiers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDELEMENT_TC9DDF922221721DEC6AF01C86AD61E299430E978_H
#ifndef PLATFORM_DIRECTINPUT_T8384F48EB39207EF4B476BF85EEE95C48F113399_H
#define PLATFORM_DIRECTINPUT_T8384F48EB39207EF4B476BF85EEE95C48F113399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput
struct  Platform_DirectInput_t8384F48EB39207EF4B476BF85EEE95C48F113399  : public Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput::variants
	Platform_DirectInput_BaseU5BU5D_tC48FB06612E7272F985A1F10E6951190ABF30EEE* ___variants_3;

public:
	inline static int32_t get_offset_of_variants_3() { return static_cast<int32_t>(offsetof(Platform_DirectInput_t8384F48EB39207EF4B476BF85EEE95C48F113399, ___variants_3)); }
	inline Platform_DirectInput_BaseU5BU5D_tC48FB06612E7272F985A1F10E6951190ABF30EEE* get_variants_3() const { return ___variants_3; }
	inline Platform_DirectInput_BaseU5BU5D_tC48FB06612E7272F985A1F10E6951190ABF30EEE** get_address_of_variants_3() { return &___variants_3; }
	inline void set_variants_3(Platform_DirectInput_BaseU5BU5D_tC48FB06612E7272F985A1F10E6951190ABF30EEE* value)
	{
		___variants_3 = value;
		Il2CppCodeGenWriteBarrier((&___variants_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_DIRECTINPUT_T8384F48EB39207EF4B476BF85EEE95C48F113399_H
#ifndef AXIS_BASE_TDCD5E6CC6636C4ECA93D8DDE3302423259E63623_H
#define AXIS_BASE_TDCD5E6CC6636C4ECA93D8DDE3302423259E63623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base
struct  Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623  : public Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::elementIdentifier
	int32_t ___elementIdentifier_2;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceType
	int32_t ___sourceType_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceAxis
	int32_t ___sourceAxis_4;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceAxisRange
	int32_t ___sourceAxisRange_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::invert
	bool ___invert_6;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::axisDeadZone
	float ___axisDeadZone_7;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::calibrateAxis
	bool ___calibrateAxis_8;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::axisZero
	float ___axisZero_9;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::axisMin
	float ___axisMin_10;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::axisMax
	float ___axisMax_11;
	// Rewired.Data.Mapping.HardwareAxisInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::axisInfo
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * ___axisInfo_12;
	// Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::alternateCalibrations
	AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* ___alternateCalibrations_13;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceButton
	int32_t ___sourceButton_14;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::buttonAxisContribution
	int32_t ___buttonAxisContribution_15;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceHat
	int32_t ___sourceHat_16;
	// Rewired.Data.Mapping.AxisDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceHatDirection
	int32_t ___sourceHatDirection_17;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceHatRange
	int32_t ___sourceHatRange_18;

public:
	inline static int32_t get_offset_of_elementIdentifier_2() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___elementIdentifier_2)); }
	inline int32_t get_elementIdentifier_2() const { return ___elementIdentifier_2; }
	inline int32_t* get_address_of_elementIdentifier_2() { return &___elementIdentifier_2; }
	inline void set_elementIdentifier_2(int32_t value)
	{
		___elementIdentifier_2 = value;
	}

	inline static int32_t get_offset_of_sourceType_3() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceType_3)); }
	inline int32_t get_sourceType_3() const { return ___sourceType_3; }
	inline int32_t* get_address_of_sourceType_3() { return &___sourceType_3; }
	inline void set_sourceType_3(int32_t value)
	{
		___sourceType_3 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_4() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceAxis_4)); }
	inline int32_t get_sourceAxis_4() const { return ___sourceAxis_4; }
	inline int32_t* get_address_of_sourceAxis_4() { return &___sourceAxis_4; }
	inline void set_sourceAxis_4(int32_t value)
	{
		___sourceAxis_4 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_5() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceAxisRange_5)); }
	inline int32_t get_sourceAxisRange_5() const { return ___sourceAxisRange_5; }
	inline int32_t* get_address_of_sourceAxisRange_5() { return &___sourceAxisRange_5; }
	inline void set_sourceAxisRange_5(int32_t value)
	{
		___sourceAxisRange_5 = value;
	}

	inline static int32_t get_offset_of_invert_6() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___invert_6)); }
	inline bool get_invert_6() const { return ___invert_6; }
	inline bool* get_address_of_invert_6() { return &___invert_6; }
	inline void set_invert_6(bool value)
	{
		___invert_6 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_7() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___axisDeadZone_7)); }
	inline float get_axisDeadZone_7() const { return ___axisDeadZone_7; }
	inline float* get_address_of_axisDeadZone_7() { return &___axisDeadZone_7; }
	inline void set_axisDeadZone_7(float value)
	{
		___axisDeadZone_7 = value;
	}

	inline static int32_t get_offset_of_calibrateAxis_8() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___calibrateAxis_8)); }
	inline bool get_calibrateAxis_8() const { return ___calibrateAxis_8; }
	inline bool* get_address_of_calibrateAxis_8() { return &___calibrateAxis_8; }
	inline void set_calibrateAxis_8(bool value)
	{
		___calibrateAxis_8 = value;
	}

	inline static int32_t get_offset_of_axisZero_9() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___axisZero_9)); }
	inline float get_axisZero_9() const { return ___axisZero_9; }
	inline float* get_address_of_axisZero_9() { return &___axisZero_9; }
	inline void set_axisZero_9(float value)
	{
		___axisZero_9 = value;
	}

	inline static int32_t get_offset_of_axisMin_10() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___axisMin_10)); }
	inline float get_axisMin_10() const { return ___axisMin_10; }
	inline float* get_address_of_axisMin_10() { return &___axisMin_10; }
	inline void set_axisMin_10(float value)
	{
		___axisMin_10 = value;
	}

	inline static int32_t get_offset_of_axisMax_11() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___axisMax_11)); }
	inline float get_axisMax_11() const { return ___axisMax_11; }
	inline float* get_address_of_axisMax_11() { return &___axisMax_11; }
	inline void set_axisMax_11(float value)
	{
		___axisMax_11 = value;
	}

	inline static int32_t get_offset_of_axisInfo_12() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___axisInfo_12)); }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * get_axisInfo_12() const { return ___axisInfo_12; }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 ** get_address_of_axisInfo_12() { return &___axisInfo_12; }
	inline void set_axisInfo_12(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * value)
	{
		___axisInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___axisInfo_12), value);
	}

	inline static int32_t get_offset_of_alternateCalibrations_13() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___alternateCalibrations_13)); }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* get_alternateCalibrations_13() const { return ___alternateCalibrations_13; }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7** get_address_of_alternateCalibrations_13() { return &___alternateCalibrations_13; }
	inline void set_alternateCalibrations_13(AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* value)
	{
		___alternateCalibrations_13 = value;
		Il2CppCodeGenWriteBarrier((&___alternateCalibrations_13), value);
	}

	inline static int32_t get_offset_of_sourceButton_14() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceButton_14)); }
	inline int32_t get_sourceButton_14() const { return ___sourceButton_14; }
	inline int32_t* get_address_of_sourceButton_14() { return &___sourceButton_14; }
	inline void set_sourceButton_14(int32_t value)
	{
		___sourceButton_14 = value;
	}

	inline static int32_t get_offset_of_buttonAxisContribution_15() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___buttonAxisContribution_15)); }
	inline int32_t get_buttonAxisContribution_15() const { return ___buttonAxisContribution_15; }
	inline int32_t* get_address_of_buttonAxisContribution_15() { return &___buttonAxisContribution_15; }
	inline void set_buttonAxisContribution_15(int32_t value)
	{
		___buttonAxisContribution_15 = value;
	}

	inline static int32_t get_offset_of_sourceHat_16() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceHat_16)); }
	inline int32_t get_sourceHat_16() const { return ___sourceHat_16; }
	inline int32_t* get_address_of_sourceHat_16() { return &___sourceHat_16; }
	inline void set_sourceHat_16(int32_t value)
	{
		___sourceHat_16 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_17() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceHatDirection_17)); }
	inline int32_t get_sourceHatDirection_17() const { return ___sourceHatDirection_17; }
	inline int32_t* get_address_of_sourceHatDirection_17() { return &___sourceHatDirection_17; }
	inline void set_sourceHatDirection_17(int32_t value)
	{
		___sourceHatDirection_17 = value;
	}

	inline static int32_t get_offset_of_sourceHatRange_18() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceHatRange_18)); }
	inline int32_t get_sourceHatRange_18() const { return ___sourceHatRange_18; }
	inline int32_t* get_address_of_sourceHatRange_18() { return &___sourceHatRange_18; }
	inline void set_sourceHatRange_18(int32_t value)
	{
		___sourceHatRange_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_BASE_TDCD5E6CC6636C4ECA93D8DDE3302423259E63623_H
#ifndef BUTTON_BASE_TCA15C13FD1596A13A31E6ECCD63A077476969024_H
#define BUTTON_BASE_TCA15C13FD1596A13A31E6ECCD63A077476969024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base
struct  Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024  : public Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::elementIdentifier
	int32_t ___elementIdentifier_2;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceType
	int32_t ___sourceType_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceButton
	int32_t ___sourceButton_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceAxis
	int32_t ___sourceAxis_5;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceAxisPole
	int32_t ___sourceAxisPole_6;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::axisDeadZone
	float ___axisDeadZone_7;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceHat
	int32_t ___sourceHat_8;
	// Rewired.Data.Mapping.HatType Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceHatType
	int32_t ___sourceHatType_9;
	// Rewired.Data.Mapping.HatDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceHatDirection
	int32_t ___sourceHatDirection_10;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::requireMultipleButtons
	bool ___requireMultipleButtons_11;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::requiredButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___requiredButtons_12;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::ignoreIfButtonsActive
	bool ___ignoreIfButtonsActive_13;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::ignoreIfButtonsActiveButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ignoreIfButtonsActiveButtons_14;
	// Rewired.Data.Mapping.HardwareButtonInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::buttonInfo
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * ___buttonInfo_15;

public:
	inline static int32_t get_offset_of_elementIdentifier_2() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___elementIdentifier_2)); }
	inline int32_t get_elementIdentifier_2() const { return ___elementIdentifier_2; }
	inline int32_t* get_address_of_elementIdentifier_2() { return &___elementIdentifier_2; }
	inline void set_elementIdentifier_2(int32_t value)
	{
		___elementIdentifier_2 = value;
	}

	inline static int32_t get_offset_of_sourceType_3() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceType_3)); }
	inline int32_t get_sourceType_3() const { return ___sourceType_3; }
	inline int32_t* get_address_of_sourceType_3() { return &___sourceType_3; }
	inline void set_sourceType_3(int32_t value)
	{
		___sourceType_3 = value;
	}

	inline static int32_t get_offset_of_sourceButton_4() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceButton_4)); }
	inline int32_t get_sourceButton_4() const { return ___sourceButton_4; }
	inline int32_t* get_address_of_sourceButton_4() { return &___sourceButton_4; }
	inline void set_sourceButton_4(int32_t value)
	{
		___sourceButton_4 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_5() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceAxis_5)); }
	inline int32_t get_sourceAxis_5() const { return ___sourceAxis_5; }
	inline int32_t* get_address_of_sourceAxis_5() { return &___sourceAxis_5; }
	inline void set_sourceAxis_5(int32_t value)
	{
		___sourceAxis_5 = value;
	}

	inline static int32_t get_offset_of_sourceAxisPole_6() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceAxisPole_6)); }
	inline int32_t get_sourceAxisPole_6() const { return ___sourceAxisPole_6; }
	inline int32_t* get_address_of_sourceAxisPole_6() { return &___sourceAxisPole_6; }
	inline void set_sourceAxisPole_6(int32_t value)
	{
		___sourceAxisPole_6 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_7() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___axisDeadZone_7)); }
	inline float get_axisDeadZone_7() const { return ___axisDeadZone_7; }
	inline float* get_address_of_axisDeadZone_7() { return &___axisDeadZone_7; }
	inline void set_axisDeadZone_7(float value)
	{
		___axisDeadZone_7 = value;
	}

	inline static int32_t get_offset_of_sourceHat_8() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceHat_8)); }
	inline int32_t get_sourceHat_8() const { return ___sourceHat_8; }
	inline int32_t* get_address_of_sourceHat_8() { return &___sourceHat_8; }
	inline void set_sourceHat_8(int32_t value)
	{
		___sourceHat_8 = value;
	}

	inline static int32_t get_offset_of_sourceHatType_9() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceHatType_9)); }
	inline int32_t get_sourceHatType_9() const { return ___sourceHatType_9; }
	inline int32_t* get_address_of_sourceHatType_9() { return &___sourceHatType_9; }
	inline void set_sourceHatType_9(int32_t value)
	{
		___sourceHatType_9 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_10() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceHatDirection_10)); }
	inline int32_t get_sourceHatDirection_10() const { return ___sourceHatDirection_10; }
	inline int32_t* get_address_of_sourceHatDirection_10() { return &___sourceHatDirection_10; }
	inline void set_sourceHatDirection_10(int32_t value)
	{
		___sourceHatDirection_10 = value;
	}

	inline static int32_t get_offset_of_requireMultipleButtons_11() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___requireMultipleButtons_11)); }
	inline bool get_requireMultipleButtons_11() const { return ___requireMultipleButtons_11; }
	inline bool* get_address_of_requireMultipleButtons_11() { return &___requireMultipleButtons_11; }
	inline void set_requireMultipleButtons_11(bool value)
	{
		___requireMultipleButtons_11 = value;
	}

	inline static int32_t get_offset_of_requiredButtons_12() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___requiredButtons_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_requiredButtons_12() const { return ___requiredButtons_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_requiredButtons_12() { return &___requiredButtons_12; }
	inline void set_requiredButtons_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___requiredButtons_12 = value;
		Il2CppCodeGenWriteBarrier((&___requiredButtons_12), value);
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActive_13() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___ignoreIfButtonsActive_13)); }
	inline bool get_ignoreIfButtonsActive_13() const { return ___ignoreIfButtonsActive_13; }
	inline bool* get_address_of_ignoreIfButtonsActive_13() { return &___ignoreIfButtonsActive_13; }
	inline void set_ignoreIfButtonsActive_13(bool value)
	{
		___ignoreIfButtonsActive_13 = value;
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActiveButtons_14() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___ignoreIfButtonsActiveButtons_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ignoreIfButtonsActiveButtons_14() const { return ___ignoreIfButtonsActiveButtons_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ignoreIfButtonsActiveButtons_14() { return &___ignoreIfButtonsActiveButtons_14; }
	inline void set_ignoreIfButtonsActiveButtons_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ignoreIfButtonsActiveButtons_14 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreIfButtonsActiveButtons_14), value);
	}

	inline static int32_t get_offset_of_buttonInfo_15() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___buttonInfo_15)); }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * get_buttonInfo_15() const { return ___buttonInfo_15; }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 ** get_address_of_buttonInfo_15() { return &___buttonInfo_15; }
	inline void set_buttonInfo_15(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * value)
	{
		___buttonInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___buttonInfo_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_BASE_TCA15C13FD1596A13A31E6ECCD63A077476969024_H
#ifndef CUSTOMCALCULATIONSOURCEDATA_T5A88147FA27E7351766E5D928E4C8D384C48DA32_H
#define CUSTOMCALCULATIONSOURCEDATA_T5A88147FA27E7351766E5D928E4C8D384C48DA32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData
struct  CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData::sourceType
	int32_t ___sourceType_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData::sourceAxis
	int32_t ___sourceAxis_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData::sourceButton
	int32_t ___sourceButton_2;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData::sourceOtherAxis
	int32_t ___sourceOtherAxis_3;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData::sourceAxisRange
	int32_t ___sourceAxisRange_4;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData::axisDeadZone
	float ___axisDeadZone_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData::invert
	bool ___invert_6;
	// Rewired.Data.Mapping.AxisCalibrationType Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData::axisCalibrationType
	int32_t ___axisCalibrationType_7;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData::axisZero
	float ___axisZero_8;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData::axisMin
	float ___axisMin_9;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData::axisMax
	float ___axisMax_10;

public:
	inline static int32_t get_offset_of_sourceType_0() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32, ___sourceType_0)); }
	inline int32_t get_sourceType_0() const { return ___sourceType_0; }
	inline int32_t* get_address_of_sourceType_0() { return &___sourceType_0; }
	inline void set_sourceType_0(int32_t value)
	{
		___sourceType_0 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_1() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32, ___sourceAxis_1)); }
	inline int32_t get_sourceAxis_1() const { return ___sourceAxis_1; }
	inline int32_t* get_address_of_sourceAxis_1() { return &___sourceAxis_1; }
	inline void set_sourceAxis_1(int32_t value)
	{
		___sourceAxis_1 = value;
	}

	inline static int32_t get_offset_of_sourceButton_2() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32, ___sourceButton_2)); }
	inline int32_t get_sourceButton_2() const { return ___sourceButton_2; }
	inline int32_t* get_address_of_sourceButton_2() { return &___sourceButton_2; }
	inline void set_sourceButton_2(int32_t value)
	{
		___sourceButton_2 = value;
	}

	inline static int32_t get_offset_of_sourceOtherAxis_3() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32, ___sourceOtherAxis_3)); }
	inline int32_t get_sourceOtherAxis_3() const { return ___sourceOtherAxis_3; }
	inline int32_t* get_address_of_sourceOtherAxis_3() { return &___sourceOtherAxis_3; }
	inline void set_sourceOtherAxis_3(int32_t value)
	{
		___sourceOtherAxis_3 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_4() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32, ___sourceAxisRange_4)); }
	inline int32_t get_sourceAxisRange_4() const { return ___sourceAxisRange_4; }
	inline int32_t* get_address_of_sourceAxisRange_4() { return &___sourceAxisRange_4; }
	inline void set_sourceAxisRange_4(int32_t value)
	{
		___sourceAxisRange_4 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_5() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32, ___axisDeadZone_5)); }
	inline float get_axisDeadZone_5() const { return ___axisDeadZone_5; }
	inline float* get_address_of_axisDeadZone_5() { return &___axisDeadZone_5; }
	inline void set_axisDeadZone_5(float value)
	{
		___axisDeadZone_5 = value;
	}

	inline static int32_t get_offset_of_invert_6() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32, ___invert_6)); }
	inline bool get_invert_6() const { return ___invert_6; }
	inline bool* get_address_of_invert_6() { return &___invert_6; }
	inline void set_invert_6(bool value)
	{
		___invert_6 = value;
	}

	inline static int32_t get_offset_of_axisCalibrationType_7() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32, ___axisCalibrationType_7)); }
	inline int32_t get_axisCalibrationType_7() const { return ___axisCalibrationType_7; }
	inline int32_t* get_address_of_axisCalibrationType_7() { return &___axisCalibrationType_7; }
	inline void set_axisCalibrationType_7(int32_t value)
	{
		___axisCalibrationType_7 = value;
	}

	inline static int32_t get_offset_of_axisZero_8() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32, ___axisZero_8)); }
	inline float get_axisZero_8() const { return ___axisZero_8; }
	inline float* get_address_of_axisZero_8() { return &___axisZero_8; }
	inline void set_axisZero_8(float value)
	{
		___axisZero_8 = value;
	}

	inline static int32_t get_offset_of_axisMin_9() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32, ___axisMin_9)); }
	inline float get_axisMin_9() const { return ___axisMin_9; }
	inline float* get_address_of_axisMin_9() { return &___axisMin_9; }
	inline void set_axisMin_9(float value)
	{
		___axisMin_9 = value;
	}

	inline static int32_t get_offset_of_axisMax_10() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32, ___axisMax_10)); }
	inline float get_axisMax_10() const { return ___axisMax_10; }
	inline float* get_address_of_axisMax_10() { return &___axisMax_10; }
	inline void set_axisMax_10(float value)
	{
		___axisMax_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCALCULATIONSOURCEDATA_T5A88147FA27E7351766E5D928E4C8D384C48DA32_H
#ifndef MATCHINGCRITERIA_T22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3_H
#define MATCHINGCRITERIA_T22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria
struct  MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3  : public MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria::hatCount
	int32_t ___hatCount_4;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria_ElementCount[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria::alternateElementCounts
	ElementCountU5BU5D_tF8971F7C10BBD3C570DFBE16A697C21D0A2DAE5B* ___alternateElementCounts_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria::productName_useRegex
	bool ___productName_useRegex_6;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria::productName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productName_7;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria::productGUID
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productGUID_8;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria::productId
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___productId_9;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_DeviceType Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria::deviceType
	int32_t ___deviceType_10;

public:
	inline static int32_t get_offset_of_hatCount_4() { return static_cast<int32_t>(offsetof(MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3, ___hatCount_4)); }
	inline int32_t get_hatCount_4() const { return ___hatCount_4; }
	inline int32_t* get_address_of_hatCount_4() { return &___hatCount_4; }
	inline void set_hatCount_4(int32_t value)
	{
		___hatCount_4 = value;
	}

	inline static int32_t get_offset_of_alternateElementCounts_5() { return static_cast<int32_t>(offsetof(MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3, ___alternateElementCounts_5)); }
	inline ElementCountU5BU5D_tF8971F7C10BBD3C570DFBE16A697C21D0A2DAE5B* get_alternateElementCounts_5() const { return ___alternateElementCounts_5; }
	inline ElementCountU5BU5D_tF8971F7C10BBD3C570DFBE16A697C21D0A2DAE5B** get_address_of_alternateElementCounts_5() { return &___alternateElementCounts_5; }
	inline void set_alternateElementCounts_5(ElementCountU5BU5D_tF8971F7C10BBD3C570DFBE16A697C21D0A2DAE5B* value)
	{
		___alternateElementCounts_5 = value;
		Il2CppCodeGenWriteBarrier((&___alternateElementCounts_5), value);
	}

	inline static int32_t get_offset_of_productName_useRegex_6() { return static_cast<int32_t>(offsetof(MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3, ___productName_useRegex_6)); }
	inline bool get_productName_useRegex_6() const { return ___productName_useRegex_6; }
	inline bool* get_address_of_productName_useRegex_6() { return &___productName_useRegex_6; }
	inline void set_productName_useRegex_6(bool value)
	{
		___productName_useRegex_6 = value;
	}

	inline static int32_t get_offset_of_productName_7() { return static_cast<int32_t>(offsetof(MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3, ___productName_7)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productName_7() const { return ___productName_7; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productName_7() { return &___productName_7; }
	inline void set_productName_7(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productName_7 = value;
		Il2CppCodeGenWriteBarrier((&___productName_7), value);
	}

	inline static int32_t get_offset_of_productGUID_8() { return static_cast<int32_t>(offsetof(MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3, ___productGUID_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productGUID_8() const { return ___productGUID_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productGUID_8() { return &___productGUID_8; }
	inline void set_productGUID_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productGUID_8 = value;
		Il2CppCodeGenWriteBarrier((&___productGUID_8), value);
	}

	inline static int32_t get_offset_of_productId_9() { return static_cast<int32_t>(offsetof(MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3, ___productId_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_productId_9() const { return ___productId_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_productId_9() { return &___productId_9; }
	inline void set_productId_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___productId_9 = value;
		Il2CppCodeGenWriteBarrier((&___productId_9), value);
	}

	inline static int32_t get_offset_of_deviceType_10() { return static_cast<int32_t>(offsetof(MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3, ___deviceType_10)); }
	inline int32_t get_deviceType_10() const { return ___deviceType_10; }
	inline int32_t* get_address_of_deviceType_10() { return &___deviceType_10; }
	inline void set_deviceType_10(int32_t value)
	{
		___deviceType_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_T22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3_H
#ifndef CUSTOMINPUTSOURCE_T845A0FA665A9B8FFE97AD4828D69AECDF618DE24_H
#define CUSTOMINPUTSOURCE_T845A0FA665A9B8FFE97AD4828D69AECDF618DE24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Custom.CustomInputSource
struct  CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24  : public RuntimeObject
{
public:
	// Rewired.InputSource Rewired.Platforms.Custom.CustomInputSource::ytEpmEgckhdtBXUMvGmTeXChJTg
	int32_t ___ytEpmEgckhdtBXUMvGmTeXChJTg_0;
	// System.Collections.Generic.List`1<Rewired.Platforms.Custom.CustomInputSource_Joystick> Rewired.Platforms.Custom.CustomInputSource::jiyuiOHkrKbDzzGVtQSJtFPdiuXH
	List_1_tE74CF06E458D02B7DFF8A66064E48B59E372F1F2 * ___jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Platforms.Custom.CustomInputSource_Joystick> Rewired.Platforms.Custom.CustomInputSource::GlRIkUvAeSsFrGZYYfdWdAiahHaH
	ReadOnlyCollection_1_t3725131578433E1E4A019736E9F58AB979C5A48D * ___GlRIkUvAeSsFrGZYYfdWdAiahHaH_2;
	// System.Boolean Rewired.Platforms.Custom.CustomInputSource::OxqcyLtiNnnQZjHLJHrubHVLORyY
	bool ___OxqcyLtiNnnQZjHLJHrubHVLORyY_3;
	// System.Action Rewired.Platforms.Custom.CustomInputSource::dSCdvTfRJafJNqFqCvsZWnmZGkuH
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___dSCdvTfRJafJNqFqCvsZWnmZGkuH_4;
	// System.Action Rewired.Platforms.Custom.CustomInputSource::WtXxfgwPkvBzrhvGFrBKnZbGqaC
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___WtXxfgwPkvBzrhvGFrBKnZbGqaC_5;
	// System.Boolean Rewired.Platforms.Custom.CustomInputSource::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_6;

public:
	inline static int32_t get_offset_of_ytEpmEgckhdtBXUMvGmTeXChJTg_0() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___ytEpmEgckhdtBXUMvGmTeXChJTg_0)); }
	inline int32_t get_ytEpmEgckhdtBXUMvGmTeXChJTg_0() const { return ___ytEpmEgckhdtBXUMvGmTeXChJTg_0; }
	inline int32_t* get_address_of_ytEpmEgckhdtBXUMvGmTeXChJTg_0() { return &___ytEpmEgckhdtBXUMvGmTeXChJTg_0; }
	inline void set_ytEpmEgckhdtBXUMvGmTeXChJTg_0(int32_t value)
	{
		___ytEpmEgckhdtBXUMvGmTeXChJTg_0 = value;
	}

	inline static int32_t get_offset_of_jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1)); }
	inline List_1_tE74CF06E458D02B7DFF8A66064E48B59E372F1F2 * get_jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1() const { return ___jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1; }
	inline List_1_tE74CF06E458D02B7DFF8A66064E48B59E372F1F2 ** get_address_of_jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1() { return &___jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1; }
	inline void set_jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1(List_1_tE74CF06E458D02B7DFF8A66064E48B59E372F1F2 * value)
	{
		___jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1 = value;
		Il2CppCodeGenWriteBarrier((&___jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1), value);
	}

	inline static int32_t get_offset_of_GlRIkUvAeSsFrGZYYfdWdAiahHaH_2() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___GlRIkUvAeSsFrGZYYfdWdAiahHaH_2)); }
	inline ReadOnlyCollection_1_t3725131578433E1E4A019736E9F58AB979C5A48D * get_GlRIkUvAeSsFrGZYYfdWdAiahHaH_2() const { return ___GlRIkUvAeSsFrGZYYfdWdAiahHaH_2; }
	inline ReadOnlyCollection_1_t3725131578433E1E4A019736E9F58AB979C5A48D ** get_address_of_GlRIkUvAeSsFrGZYYfdWdAiahHaH_2() { return &___GlRIkUvAeSsFrGZYYfdWdAiahHaH_2; }
	inline void set_GlRIkUvAeSsFrGZYYfdWdAiahHaH_2(ReadOnlyCollection_1_t3725131578433E1E4A019736E9F58AB979C5A48D * value)
	{
		___GlRIkUvAeSsFrGZYYfdWdAiahHaH_2 = value;
		Il2CppCodeGenWriteBarrier((&___GlRIkUvAeSsFrGZYYfdWdAiahHaH_2), value);
	}

	inline static int32_t get_offset_of_OxqcyLtiNnnQZjHLJHrubHVLORyY_3() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___OxqcyLtiNnnQZjHLJHrubHVLORyY_3)); }
	inline bool get_OxqcyLtiNnnQZjHLJHrubHVLORyY_3() const { return ___OxqcyLtiNnnQZjHLJHrubHVLORyY_3; }
	inline bool* get_address_of_OxqcyLtiNnnQZjHLJHrubHVLORyY_3() { return &___OxqcyLtiNnnQZjHLJHrubHVLORyY_3; }
	inline void set_OxqcyLtiNnnQZjHLJHrubHVLORyY_3(bool value)
	{
		___OxqcyLtiNnnQZjHLJHrubHVLORyY_3 = value;
	}

	inline static int32_t get_offset_of_dSCdvTfRJafJNqFqCvsZWnmZGkuH_4() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___dSCdvTfRJafJNqFqCvsZWnmZGkuH_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_dSCdvTfRJafJNqFqCvsZWnmZGkuH_4() const { return ___dSCdvTfRJafJNqFqCvsZWnmZGkuH_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_dSCdvTfRJafJNqFqCvsZWnmZGkuH_4() { return &___dSCdvTfRJafJNqFqCvsZWnmZGkuH_4; }
	inline void set_dSCdvTfRJafJNqFqCvsZWnmZGkuH_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___dSCdvTfRJafJNqFqCvsZWnmZGkuH_4 = value;
		Il2CppCodeGenWriteBarrier((&___dSCdvTfRJafJNqFqCvsZWnmZGkuH_4), value);
	}

	inline static int32_t get_offset_of_WtXxfgwPkvBzrhvGFrBKnZbGqaC_5() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___WtXxfgwPkvBzrhvGFrBKnZbGqaC_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_WtXxfgwPkvBzrhvGFrBKnZbGqaC_5() const { return ___WtXxfgwPkvBzrhvGFrBKnZbGqaC_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_WtXxfgwPkvBzrhvGFrBKnZbGqaC_5() { return &___WtXxfgwPkvBzrhvGFrBKnZbGqaC_5; }
	inline void set_WtXxfgwPkvBzrhvGFrBKnZbGqaC_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___WtXxfgwPkvBzrhvGFrBKnZbGqaC_5 = value;
		Il2CppCodeGenWriteBarrier((&___WtXxfgwPkvBzrhvGFrBKnZbGqaC_5), value);
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_6() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___SeCUoinDywZmqZDHRKupOdOaTke_6)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_6() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_6; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_6() { return &___SeCUoinDywZmqZDHRKupOdOaTke_6; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_6(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMINPUTSOURCE_T845A0FA665A9B8FFE97AD4828D69AECDF618DE24_H
#ifndef AXIS_T99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A_H
#define AXIS_T99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Axis
struct  Axis_t99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A  : public ElementWithSource_t69752C9D210EDEE80C6FD0F291364B0681FDB4ED
{
public:
	// System.Single Rewired.PlayerController_Axis::IiRfbnSvbaGlqxPQfWikqkfabCN
	float ___IiRfbnSvbaGlqxPQfWikqkfabCN_11;
	// Rewired.AxisCoordinateMode Rewired.PlayerController_Axis::KoJFJTyAFTzWKOYBGqPtFhoxzoa
	int32_t ___KoJFJTyAFTzWKOYBGqPtFhoxzoa_12;
	// System.Boolean Rewired.PlayerController_Axis::inaxtTsfdEOFKtbeAbFuayoHJCRB
	bool ___inaxtTsfdEOFKtbeAbFuayoHJCRB_13;

public:
	inline static int32_t get_offset_of_IiRfbnSvbaGlqxPQfWikqkfabCN_11() { return static_cast<int32_t>(offsetof(Axis_t99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A, ___IiRfbnSvbaGlqxPQfWikqkfabCN_11)); }
	inline float get_IiRfbnSvbaGlqxPQfWikqkfabCN_11() const { return ___IiRfbnSvbaGlqxPQfWikqkfabCN_11; }
	inline float* get_address_of_IiRfbnSvbaGlqxPQfWikqkfabCN_11() { return &___IiRfbnSvbaGlqxPQfWikqkfabCN_11; }
	inline void set_IiRfbnSvbaGlqxPQfWikqkfabCN_11(float value)
	{
		___IiRfbnSvbaGlqxPQfWikqkfabCN_11 = value;
	}

	inline static int32_t get_offset_of_KoJFJTyAFTzWKOYBGqPtFhoxzoa_12() { return static_cast<int32_t>(offsetof(Axis_t99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A, ___KoJFJTyAFTzWKOYBGqPtFhoxzoa_12)); }
	inline int32_t get_KoJFJTyAFTzWKOYBGqPtFhoxzoa_12() const { return ___KoJFJTyAFTzWKOYBGqPtFhoxzoa_12; }
	inline int32_t* get_address_of_KoJFJTyAFTzWKOYBGqPtFhoxzoa_12() { return &___KoJFJTyAFTzWKOYBGqPtFhoxzoa_12; }
	inline void set_KoJFJTyAFTzWKOYBGqPtFhoxzoa_12(int32_t value)
	{
		___KoJFJTyAFTzWKOYBGqPtFhoxzoa_12 = value;
	}

	inline static int32_t get_offset_of_inaxtTsfdEOFKtbeAbFuayoHJCRB_13() { return static_cast<int32_t>(offsetof(Axis_t99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A, ___inaxtTsfdEOFKtbeAbFuayoHJCRB_13)); }
	inline bool get_inaxtTsfdEOFKtbeAbFuayoHJCRB_13() const { return ___inaxtTsfdEOFKtbeAbFuayoHJCRB_13; }
	inline bool* get_address_of_inaxtTsfdEOFKtbeAbFuayoHJCRB_13() { return &___inaxtTsfdEOFKtbeAbFuayoHJCRB_13; }
	inline void set_inaxtTsfdEOFKtbeAbFuayoHJCRB_13(bool value)
	{
		___inaxtTsfdEOFKtbeAbFuayoHJCRB_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A_H
#ifndef DEFINITION_T10130017A93C5FDA0255FF4008B240CF1BB7A7A9_H
#define DEFINITION_T10130017A93C5FDA0255FF4008B240CF1BB7A7A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Axis_Definition
struct  Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9  : public Definition_t18A5B017F634EC0B2DB9B29ADB9294C16C71F6BA
{
public:
	// Rewired.AxisCoordinateMode Rewired.PlayerController_Axis_Definition::coordinateMode
	int32_t ___coordinateMode_3;
	// System.Single Rewired.PlayerController_Axis_Definition::absoluteToRelativeSensitivity
	float ___absoluteToRelativeSensitivity_4;

public:
	inline static int32_t get_offset_of_coordinateMode_3() { return static_cast<int32_t>(offsetof(Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9, ___coordinateMode_3)); }
	inline int32_t get_coordinateMode_3() const { return ___coordinateMode_3; }
	inline int32_t* get_address_of_coordinateMode_3() { return &___coordinateMode_3; }
	inline void set_coordinateMode_3(int32_t value)
	{
		___coordinateMode_3 = value;
	}

	inline static int32_t get_offset_of_absoluteToRelativeSensitivity_4() { return static_cast<int32_t>(offsetof(Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9, ___absoluteToRelativeSensitivity_4)); }
	inline float get_absoluteToRelativeSensitivity_4() const { return ___absoluteToRelativeSensitivity_4; }
	inline float* get_address_of_absoluteToRelativeSensitivity_4() { return &___absoluteToRelativeSensitivity_4; }
	inline void set_absoluteToRelativeSensitivity_4(float value)
	{
		___absoluteToRelativeSensitivity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITION_T10130017A93C5FDA0255FF4008B240CF1BB7A7A9_H
#ifndef DVLZSFCJNFXTOUFPKLCHSIMXNQC_TEB40E5A227CB3E8F0D35981E8A306F32243E416E_H
#define DVLZSFCJNFXTOUFPKLCHSIMXNQC_TEB40E5A227CB3E8F0D35981E8A306F32243E416E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_Element_dvlZSFCjnFxtoufPKLchsiMXNQc
struct  dvlZSFCjnFxtoufPKLchsiMXNQc_tEB40E5A227CB3E8F0D35981E8A306F32243E416E 
{
public:
	// Rewired.ControllerElementType Rewired.PlayerController_Element_dvlZSFCjnFxtoufPKLchsiMXNQc::EDrPGdciFGATRHQnPSEzqJMwhNu
	int32_t ___EDrPGdciFGATRHQnPSEzqJMwhNu_0;
	// System.Int32 Rewired.PlayerController_Element_dvlZSFCjnFxtoufPKLchsiMXNQc::BrVbOYLWwCQWNcRRlBBgwzbuWsi
	int32_t ___BrVbOYLWwCQWNcRRlBBgwzbuWsi_1;
	// System.Single Rewired.PlayerController_Element_dvlZSFCjnFxtoufPKLchsiMXNQc::ujWJxEJcSVHSdnQYrtHnXDWQpnZ
	float ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_2;

public:
	inline static int32_t get_offset_of_EDrPGdciFGATRHQnPSEzqJMwhNu_0() { return static_cast<int32_t>(offsetof(dvlZSFCjnFxtoufPKLchsiMXNQc_tEB40E5A227CB3E8F0D35981E8A306F32243E416E, ___EDrPGdciFGATRHQnPSEzqJMwhNu_0)); }
	inline int32_t get_EDrPGdciFGATRHQnPSEzqJMwhNu_0() const { return ___EDrPGdciFGATRHQnPSEzqJMwhNu_0; }
	inline int32_t* get_address_of_EDrPGdciFGATRHQnPSEzqJMwhNu_0() { return &___EDrPGdciFGATRHQnPSEzqJMwhNu_0; }
	inline void set_EDrPGdciFGATRHQnPSEzqJMwhNu_0(int32_t value)
	{
		___EDrPGdciFGATRHQnPSEzqJMwhNu_0 = value;
	}

	inline static int32_t get_offset_of_BrVbOYLWwCQWNcRRlBBgwzbuWsi_1() { return static_cast<int32_t>(offsetof(dvlZSFCjnFxtoufPKLchsiMXNQc_tEB40E5A227CB3E8F0D35981E8A306F32243E416E, ___BrVbOYLWwCQWNcRRlBBgwzbuWsi_1)); }
	inline int32_t get_BrVbOYLWwCQWNcRRlBBgwzbuWsi_1() const { return ___BrVbOYLWwCQWNcRRlBBgwzbuWsi_1; }
	inline int32_t* get_address_of_BrVbOYLWwCQWNcRRlBBgwzbuWsi_1() { return &___BrVbOYLWwCQWNcRRlBBgwzbuWsi_1; }
	inline void set_BrVbOYLWwCQWNcRRlBBgwzbuWsi_1(int32_t value)
	{
		___BrVbOYLWwCQWNcRRlBBgwzbuWsi_1 = value;
	}

	inline static int32_t get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_2() { return static_cast<int32_t>(offsetof(dvlZSFCjnFxtoufPKLchsiMXNQc_tEB40E5A227CB3E8F0D35981E8A306F32243E416E, ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_2)); }
	inline float get_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_2() const { return ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_2; }
	inline float* get_address_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_2() { return &___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_2; }
	inline void set_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_2(float value)
	{
		___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DVLZSFCJNFXTOUFPKLCHSIMXNQC_TEB40E5A227CB3E8F0D35981E8A306F32243E416E_H
#ifndef MOUSEAXIS2D_TDA0430D22FE9F3F7D591E689F3403A018214C41F_H
#define MOUSEAXIS2D_TDA0430D22FE9F3F7D591E689F3403A018214C41F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_MouseAxis2D
struct  MouseAxis2D_tDA0430D22FE9F3F7D591E689F3403A018214C41F  : public Axis2D_tA62A3EAF575DEE91FF86DCEE3B2075AFA54705B7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEAXIS2D_TDA0430D22FE9F3F7D591E689F3403A018214C41F_H
#ifndef DEFINITION_T46F903B3012455FA952C56CCD90032CCC23D2E73_H
#define DEFINITION_T46F903B3012455FA952C56CCD90032CCC23D2E73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_MouseAxis2D_Definition
struct  Definition_t46F903B3012455FA952C56CCD90032CCC23D2E73  : public Definition_tABC9A67DA4AE30F7D1070DD41541C2F97780F86B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITION_T46F903B3012455FA952C56CCD90032CCC23D2E73_H
#ifndef MOUSEWHEEL_T96F0FF773D68A8E93DE6E731AF4A45FA4BD9845A_H
#define MOUSEWHEEL_T96F0FF773D68A8E93DE6E731AF4A45FA4BD9845A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_MouseWheel
struct  MouseWheel_t96F0FF773D68A8E93DE6E731AF4A45FA4BD9845A  : public Axis2D_tA62A3EAF575DEE91FF86DCEE3B2075AFA54705B7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEWHEEL_T96F0FF773D68A8E93DE6E731AF4A45FA4BD9845A_H
#ifndef DEFINITION_TD95B80BE030BC14FA5244466336EEAFE33A2F4C9_H
#define DEFINITION_TD95B80BE030BC14FA5244466336EEAFE33A2F4C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_MouseWheel_Definition
struct  Definition_tD95B80BE030BC14FA5244466336EEAFE33A2F4C9  : public Definition_tABC9A67DA4AE30F7D1070DD41541C2F97780F86B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITION_TD95B80BE030BC14FA5244466336EEAFE33A2F4C9_H
#ifndef PLAYERMOUSE_TFE3F2427C1D7E8AE42A497332E5D40F903252B3D_H
#define PLAYERMOUSE_TFE3F2427C1D7E8AE42A497332E5D40F903252B3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerMouse
struct  PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D  : public PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01
{
public:
	// System.Int32 Rewired.PlayerMouse::bkySQZIzPEjvMtqmVQIflsRZLcC
	int32_t ___bkySQZIzPEjvMtqmVQIflsRZLcC_29;
	// System.Int32 Rewired.PlayerMouse::ZdUUEykXEadJhwdAvxRLuFOjdlo
	int32_t ___ZdUUEykXEadJhwdAvxRLuFOjdlo_30;
	// System.Int32 Rewired.PlayerMouse::xSdFlWJnlQKpmDxLjLiJVHILHHQb
	int32_t ___xSdFlWJnlQKpmDxLjLiJVHILHHQb_31;
	// System.Int32 Rewired.PlayerMouse::zakNtTwPoqfQTgRmwJMlFSKDayyY
	int32_t ___zakNtTwPoqfQTgRmwJMlFSKDayyY_32;
	// System.Int32 Rewired.PlayerMouse::TGDHdlRSoTbnNveTXeJyCfhOQHVw
	int32_t ___TGDHdlRSoTbnNveTXeJyCfhOQHVw_33;
	// System.Int32 Rewired.PlayerMouse::ASvelPdARqNeHoGFRujyrySfHzZ
	int32_t ___ASvelPdARqNeHoGFRujyrySfHzZ_34;
	// System.Boolean Rewired.PlayerMouse::vIRfZylUMCaIJegxbePAceWChvkz
	bool ___vIRfZylUMCaIJegxbePAceWChvkz_35;
	// UnityEngine.Vector2 Rewired.PlayerMouse::ygfOxayKzOSmtRmOHCLqarVOYDQN
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___ygfOxayKzOSmtRmOHCLqarVOYDQN_36;
	// UnityEngine.Vector2 Rewired.PlayerMouse::zobOISDZaPowFanRheDQywSrYck
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zobOISDZaPowFanRheDQywSrYck_37;
	// UnityEngine.Vector2 Rewired.PlayerMouse::DWbngFJqKIdtWDGFHVEKwNRZdLxe
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___DWbngFJqKIdtWDGFHVEKwNRZdLxe_38;
	// UnityEngine.Vector2 Rewired.PlayerMouse::NraenLBVpMrWzcDEoclzjffxsOC
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___NraenLBVpMrWzcDEoclzjffxsOC_39;
	// UnityEngine.Vector2 Rewired.PlayerMouse::YfaIpXgcpqghFASYjZjjzMtnuoBK
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___YfaIpXgcpqghFASYjZjjzMtnuoBK_40;
	// System.Single Rewired.PlayerMouse::EANPkZSmkxVcEcmVTARGUefVdrP
	float ___EANPkZSmkxVcEcmVTARGUefVdrP_41;
	// System.Boolean Rewired.PlayerMouse::BePwMGWomgmSkblQmXdzPGicbYd
	bool ___BePwMGWomgmSkblQmXdzPGicbYd_42;
	// System.Action`1<UnityEngine.Vector2> Rewired.PlayerMouse::zTdQBKFzjclSAMvLChomKafLIaI
	Action_1_t28DE0166228B92F520FE7ADC71034A2C4009E0F5 * ___zTdQBKFzjclSAMvLChomKafLIaI_43;
	// System.Boolean Rewired.PlayerMouse::ufodZjARVBvOArvlATqqfpzRNEQ
	bool ___ufodZjARVBvOArvlATqqfpzRNEQ_44;
	// Rewired.Utils.Classes.Data.ScreenRect Rewired.PlayerMouse::hXyTkhcAPGjVzSrSRDwkAYLwdmCa
	ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14  ___hXyTkhcAPGjVzSrSRDwkAYLwdmCa_45;
	// Rewired.PlayerMouse_MovementAreaUnit Rewired.PlayerMouse::EsWfQirqStgFmlaejyHRdJihNXP
	int32_t ___EsWfQirqStgFmlaejyHRdJihNXP_46;

public:
	inline static int32_t get_offset_of_bkySQZIzPEjvMtqmVQIflsRZLcC_29() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___bkySQZIzPEjvMtqmVQIflsRZLcC_29)); }
	inline int32_t get_bkySQZIzPEjvMtqmVQIflsRZLcC_29() const { return ___bkySQZIzPEjvMtqmVQIflsRZLcC_29; }
	inline int32_t* get_address_of_bkySQZIzPEjvMtqmVQIflsRZLcC_29() { return &___bkySQZIzPEjvMtqmVQIflsRZLcC_29; }
	inline void set_bkySQZIzPEjvMtqmVQIflsRZLcC_29(int32_t value)
	{
		___bkySQZIzPEjvMtqmVQIflsRZLcC_29 = value;
	}

	inline static int32_t get_offset_of_ZdUUEykXEadJhwdAvxRLuFOjdlo_30() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___ZdUUEykXEadJhwdAvxRLuFOjdlo_30)); }
	inline int32_t get_ZdUUEykXEadJhwdAvxRLuFOjdlo_30() const { return ___ZdUUEykXEadJhwdAvxRLuFOjdlo_30; }
	inline int32_t* get_address_of_ZdUUEykXEadJhwdAvxRLuFOjdlo_30() { return &___ZdUUEykXEadJhwdAvxRLuFOjdlo_30; }
	inline void set_ZdUUEykXEadJhwdAvxRLuFOjdlo_30(int32_t value)
	{
		___ZdUUEykXEadJhwdAvxRLuFOjdlo_30 = value;
	}

	inline static int32_t get_offset_of_xSdFlWJnlQKpmDxLjLiJVHILHHQb_31() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___xSdFlWJnlQKpmDxLjLiJVHILHHQb_31)); }
	inline int32_t get_xSdFlWJnlQKpmDxLjLiJVHILHHQb_31() const { return ___xSdFlWJnlQKpmDxLjLiJVHILHHQb_31; }
	inline int32_t* get_address_of_xSdFlWJnlQKpmDxLjLiJVHILHHQb_31() { return &___xSdFlWJnlQKpmDxLjLiJVHILHHQb_31; }
	inline void set_xSdFlWJnlQKpmDxLjLiJVHILHHQb_31(int32_t value)
	{
		___xSdFlWJnlQKpmDxLjLiJVHILHHQb_31 = value;
	}

	inline static int32_t get_offset_of_zakNtTwPoqfQTgRmwJMlFSKDayyY_32() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___zakNtTwPoqfQTgRmwJMlFSKDayyY_32)); }
	inline int32_t get_zakNtTwPoqfQTgRmwJMlFSKDayyY_32() const { return ___zakNtTwPoqfQTgRmwJMlFSKDayyY_32; }
	inline int32_t* get_address_of_zakNtTwPoqfQTgRmwJMlFSKDayyY_32() { return &___zakNtTwPoqfQTgRmwJMlFSKDayyY_32; }
	inline void set_zakNtTwPoqfQTgRmwJMlFSKDayyY_32(int32_t value)
	{
		___zakNtTwPoqfQTgRmwJMlFSKDayyY_32 = value;
	}

	inline static int32_t get_offset_of_TGDHdlRSoTbnNveTXeJyCfhOQHVw_33() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___TGDHdlRSoTbnNveTXeJyCfhOQHVw_33)); }
	inline int32_t get_TGDHdlRSoTbnNveTXeJyCfhOQHVw_33() const { return ___TGDHdlRSoTbnNveTXeJyCfhOQHVw_33; }
	inline int32_t* get_address_of_TGDHdlRSoTbnNveTXeJyCfhOQHVw_33() { return &___TGDHdlRSoTbnNveTXeJyCfhOQHVw_33; }
	inline void set_TGDHdlRSoTbnNveTXeJyCfhOQHVw_33(int32_t value)
	{
		___TGDHdlRSoTbnNveTXeJyCfhOQHVw_33 = value;
	}

	inline static int32_t get_offset_of_ASvelPdARqNeHoGFRujyrySfHzZ_34() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___ASvelPdARqNeHoGFRujyrySfHzZ_34)); }
	inline int32_t get_ASvelPdARqNeHoGFRujyrySfHzZ_34() const { return ___ASvelPdARqNeHoGFRujyrySfHzZ_34; }
	inline int32_t* get_address_of_ASvelPdARqNeHoGFRujyrySfHzZ_34() { return &___ASvelPdARqNeHoGFRujyrySfHzZ_34; }
	inline void set_ASvelPdARqNeHoGFRujyrySfHzZ_34(int32_t value)
	{
		___ASvelPdARqNeHoGFRujyrySfHzZ_34 = value;
	}

	inline static int32_t get_offset_of_vIRfZylUMCaIJegxbePAceWChvkz_35() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___vIRfZylUMCaIJegxbePAceWChvkz_35)); }
	inline bool get_vIRfZylUMCaIJegxbePAceWChvkz_35() const { return ___vIRfZylUMCaIJegxbePAceWChvkz_35; }
	inline bool* get_address_of_vIRfZylUMCaIJegxbePAceWChvkz_35() { return &___vIRfZylUMCaIJegxbePAceWChvkz_35; }
	inline void set_vIRfZylUMCaIJegxbePAceWChvkz_35(bool value)
	{
		___vIRfZylUMCaIJegxbePAceWChvkz_35 = value;
	}

	inline static int32_t get_offset_of_ygfOxayKzOSmtRmOHCLqarVOYDQN_36() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___ygfOxayKzOSmtRmOHCLqarVOYDQN_36)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_ygfOxayKzOSmtRmOHCLqarVOYDQN_36() const { return ___ygfOxayKzOSmtRmOHCLqarVOYDQN_36; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_ygfOxayKzOSmtRmOHCLqarVOYDQN_36() { return &___ygfOxayKzOSmtRmOHCLqarVOYDQN_36; }
	inline void set_ygfOxayKzOSmtRmOHCLqarVOYDQN_36(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___ygfOxayKzOSmtRmOHCLqarVOYDQN_36 = value;
	}

	inline static int32_t get_offset_of_zobOISDZaPowFanRheDQywSrYck_37() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___zobOISDZaPowFanRheDQywSrYck_37)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zobOISDZaPowFanRheDQywSrYck_37() const { return ___zobOISDZaPowFanRheDQywSrYck_37; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zobOISDZaPowFanRheDQywSrYck_37() { return &___zobOISDZaPowFanRheDQywSrYck_37; }
	inline void set_zobOISDZaPowFanRheDQywSrYck_37(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zobOISDZaPowFanRheDQywSrYck_37 = value;
	}

	inline static int32_t get_offset_of_DWbngFJqKIdtWDGFHVEKwNRZdLxe_38() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___DWbngFJqKIdtWDGFHVEKwNRZdLxe_38)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_DWbngFJqKIdtWDGFHVEKwNRZdLxe_38() const { return ___DWbngFJqKIdtWDGFHVEKwNRZdLxe_38; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_DWbngFJqKIdtWDGFHVEKwNRZdLxe_38() { return &___DWbngFJqKIdtWDGFHVEKwNRZdLxe_38; }
	inline void set_DWbngFJqKIdtWDGFHVEKwNRZdLxe_38(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___DWbngFJqKIdtWDGFHVEKwNRZdLxe_38 = value;
	}

	inline static int32_t get_offset_of_NraenLBVpMrWzcDEoclzjffxsOC_39() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___NraenLBVpMrWzcDEoclzjffxsOC_39)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_NraenLBVpMrWzcDEoclzjffxsOC_39() const { return ___NraenLBVpMrWzcDEoclzjffxsOC_39; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_NraenLBVpMrWzcDEoclzjffxsOC_39() { return &___NraenLBVpMrWzcDEoclzjffxsOC_39; }
	inline void set_NraenLBVpMrWzcDEoclzjffxsOC_39(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___NraenLBVpMrWzcDEoclzjffxsOC_39 = value;
	}

	inline static int32_t get_offset_of_YfaIpXgcpqghFASYjZjjzMtnuoBK_40() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___YfaIpXgcpqghFASYjZjjzMtnuoBK_40)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_YfaIpXgcpqghFASYjZjjzMtnuoBK_40() const { return ___YfaIpXgcpqghFASYjZjjzMtnuoBK_40; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_YfaIpXgcpqghFASYjZjjzMtnuoBK_40() { return &___YfaIpXgcpqghFASYjZjjzMtnuoBK_40; }
	inline void set_YfaIpXgcpqghFASYjZjjzMtnuoBK_40(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___YfaIpXgcpqghFASYjZjjzMtnuoBK_40 = value;
	}

	inline static int32_t get_offset_of_EANPkZSmkxVcEcmVTARGUefVdrP_41() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___EANPkZSmkxVcEcmVTARGUefVdrP_41)); }
	inline float get_EANPkZSmkxVcEcmVTARGUefVdrP_41() const { return ___EANPkZSmkxVcEcmVTARGUefVdrP_41; }
	inline float* get_address_of_EANPkZSmkxVcEcmVTARGUefVdrP_41() { return &___EANPkZSmkxVcEcmVTARGUefVdrP_41; }
	inline void set_EANPkZSmkxVcEcmVTARGUefVdrP_41(float value)
	{
		___EANPkZSmkxVcEcmVTARGUefVdrP_41 = value;
	}

	inline static int32_t get_offset_of_BePwMGWomgmSkblQmXdzPGicbYd_42() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___BePwMGWomgmSkblQmXdzPGicbYd_42)); }
	inline bool get_BePwMGWomgmSkblQmXdzPGicbYd_42() const { return ___BePwMGWomgmSkblQmXdzPGicbYd_42; }
	inline bool* get_address_of_BePwMGWomgmSkblQmXdzPGicbYd_42() { return &___BePwMGWomgmSkblQmXdzPGicbYd_42; }
	inline void set_BePwMGWomgmSkblQmXdzPGicbYd_42(bool value)
	{
		___BePwMGWomgmSkblQmXdzPGicbYd_42 = value;
	}

	inline static int32_t get_offset_of_zTdQBKFzjclSAMvLChomKafLIaI_43() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___zTdQBKFzjclSAMvLChomKafLIaI_43)); }
	inline Action_1_t28DE0166228B92F520FE7ADC71034A2C4009E0F5 * get_zTdQBKFzjclSAMvLChomKafLIaI_43() const { return ___zTdQBKFzjclSAMvLChomKafLIaI_43; }
	inline Action_1_t28DE0166228B92F520FE7ADC71034A2C4009E0F5 ** get_address_of_zTdQBKFzjclSAMvLChomKafLIaI_43() { return &___zTdQBKFzjclSAMvLChomKafLIaI_43; }
	inline void set_zTdQBKFzjclSAMvLChomKafLIaI_43(Action_1_t28DE0166228B92F520FE7ADC71034A2C4009E0F5 * value)
	{
		___zTdQBKFzjclSAMvLChomKafLIaI_43 = value;
		Il2CppCodeGenWriteBarrier((&___zTdQBKFzjclSAMvLChomKafLIaI_43), value);
	}

	inline static int32_t get_offset_of_ufodZjARVBvOArvlATqqfpzRNEQ_44() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___ufodZjARVBvOArvlATqqfpzRNEQ_44)); }
	inline bool get_ufodZjARVBvOArvlATqqfpzRNEQ_44() const { return ___ufodZjARVBvOArvlATqqfpzRNEQ_44; }
	inline bool* get_address_of_ufodZjARVBvOArvlATqqfpzRNEQ_44() { return &___ufodZjARVBvOArvlATqqfpzRNEQ_44; }
	inline void set_ufodZjARVBvOArvlATqqfpzRNEQ_44(bool value)
	{
		___ufodZjARVBvOArvlATqqfpzRNEQ_44 = value;
	}

	inline static int32_t get_offset_of_hXyTkhcAPGjVzSrSRDwkAYLwdmCa_45() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___hXyTkhcAPGjVzSrSRDwkAYLwdmCa_45)); }
	inline ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14  get_hXyTkhcAPGjVzSrSRDwkAYLwdmCa_45() const { return ___hXyTkhcAPGjVzSrSRDwkAYLwdmCa_45; }
	inline ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14 * get_address_of_hXyTkhcAPGjVzSrSRDwkAYLwdmCa_45() { return &___hXyTkhcAPGjVzSrSRDwkAYLwdmCa_45; }
	inline void set_hXyTkhcAPGjVzSrSRDwkAYLwdmCa_45(ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14  value)
	{
		___hXyTkhcAPGjVzSrSRDwkAYLwdmCa_45 = value;
	}

	inline static int32_t get_offset_of_EsWfQirqStgFmlaejyHRdJihNXP_46() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D, ___EsWfQirqStgFmlaejyHRdJihNXP_46)); }
	inline int32_t get_EsWfQirqStgFmlaejyHRdJihNXP_46() const { return ___EsWfQirqStgFmlaejyHRdJihNXP_46; }
	inline int32_t* get_address_of_EsWfQirqStgFmlaejyHRdJihNXP_46() { return &___EsWfQirqStgFmlaejyHRdJihNXP_46; }
	inline void set_EsWfQirqStgFmlaejyHRdJihNXP_46(int32_t value)
	{
		___EsWfQirqStgFmlaejyHRdJihNXP_46 = value;
	}
};

struct PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D_StaticFields
{
public:
	// Rewired.Utils.Classes.Data.ScreenRect Rewired.PlayerMouse::dWDDIjnMxVawYKNHxusdBGeHHGfa
	ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14  ___dWDDIjnMxVawYKNHxusdBGeHHGfa_28;
	// System.Predicate`1<Rewired.PlayerController_Axis> Rewired.PlayerMouse::APrqfxcqXfOigsaYgstdZUzQbTD
	Predicate_1_tF4A9FED2002F5F108CC39D461644FF7FA88E4C55 * ___APrqfxcqXfOigsaYgstdZUzQbTD_47;
	// System.Predicate`1<Rewired.PlayerController_Axis> Rewired.PlayerMouse::wzuTgIXkBigvyhwmUgzxbJRwaLDH
	Predicate_1_tF4A9FED2002F5F108CC39D461644FF7FA88E4C55 * ___wzuTgIXkBigvyhwmUgzxbJRwaLDH_48;

public:
	inline static int32_t get_offset_of_dWDDIjnMxVawYKNHxusdBGeHHGfa_28() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D_StaticFields, ___dWDDIjnMxVawYKNHxusdBGeHHGfa_28)); }
	inline ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14  get_dWDDIjnMxVawYKNHxusdBGeHHGfa_28() const { return ___dWDDIjnMxVawYKNHxusdBGeHHGfa_28; }
	inline ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14 * get_address_of_dWDDIjnMxVawYKNHxusdBGeHHGfa_28() { return &___dWDDIjnMxVawYKNHxusdBGeHHGfa_28; }
	inline void set_dWDDIjnMxVawYKNHxusdBGeHHGfa_28(ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14  value)
	{
		___dWDDIjnMxVawYKNHxusdBGeHHGfa_28 = value;
	}

	inline static int32_t get_offset_of_APrqfxcqXfOigsaYgstdZUzQbTD_47() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D_StaticFields, ___APrqfxcqXfOigsaYgstdZUzQbTD_47)); }
	inline Predicate_1_tF4A9FED2002F5F108CC39D461644FF7FA88E4C55 * get_APrqfxcqXfOigsaYgstdZUzQbTD_47() const { return ___APrqfxcqXfOigsaYgstdZUzQbTD_47; }
	inline Predicate_1_tF4A9FED2002F5F108CC39D461644FF7FA88E4C55 ** get_address_of_APrqfxcqXfOigsaYgstdZUzQbTD_47() { return &___APrqfxcqXfOigsaYgstdZUzQbTD_47; }
	inline void set_APrqfxcqXfOigsaYgstdZUzQbTD_47(Predicate_1_tF4A9FED2002F5F108CC39D461644FF7FA88E4C55 * value)
	{
		___APrqfxcqXfOigsaYgstdZUzQbTD_47 = value;
		Il2CppCodeGenWriteBarrier((&___APrqfxcqXfOigsaYgstdZUzQbTD_47), value);
	}

	inline static int32_t get_offset_of_wzuTgIXkBigvyhwmUgzxbJRwaLDH_48() { return static_cast<int32_t>(offsetof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D_StaticFields, ___wzuTgIXkBigvyhwmUgzxbJRwaLDH_48)); }
	inline Predicate_1_tF4A9FED2002F5F108CC39D461644FF7FA88E4C55 * get_wzuTgIXkBigvyhwmUgzxbJRwaLDH_48() const { return ___wzuTgIXkBigvyhwmUgzxbJRwaLDH_48; }
	inline Predicate_1_tF4A9FED2002F5F108CC39D461644FF7FA88E4C55 ** get_address_of_wzuTgIXkBigvyhwmUgzxbJRwaLDH_48() { return &___wzuTgIXkBigvyhwmUgzxbJRwaLDH_48; }
	inline void set_wzuTgIXkBigvyhwmUgzxbJRwaLDH_48(Predicate_1_tF4A9FED2002F5F108CC39D461644FF7FA88E4C55 * value)
	{
		___wzuTgIXkBigvyhwmUgzxbJRwaLDH_48 = value;
		Il2CppCodeGenWriteBarrier((&___wzuTgIXkBigvyhwmUgzxbJRwaLDH_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMOUSE_TFE3F2427C1D7E8AE42A497332E5D40F903252B3D_H
#ifndef DEFINITION_TF82F454FDB865FB8E647E7C9783284702FC65165_H
#define DEFINITION_TF82F454FDB865FB8E647E7C9783284702FC65165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerMouse_Definition
struct  Definition_tF82F454FDB865FB8E647E7C9783284702FC65165  : public Definition_tCF297A490F8E07E75C2E1F36D9335A2625E0ED46
{
public:
	// System.Boolean Rewired.PlayerMouse_Definition::defaultToCenter
	bool ___defaultToCenter_3;
	// Rewired.Utils.Classes.Data.ScreenRect Rewired.PlayerMouse_Definition::movementArea
	ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14  ___movementArea_4;
	// Rewired.PlayerMouse_MovementAreaUnit Rewired.PlayerMouse_Definition::movementAreaUnit
	int32_t ___movementAreaUnit_5;
	// System.Single Rewired.PlayerMouse_Definition::pointerSpeed
	float ___pointerSpeed_6;
	// System.Boolean Rewired.PlayerMouse_Definition::useHardwarePointerPosition
	bool ___useHardwarePointerPosition_7;

public:
	inline static int32_t get_offset_of_defaultToCenter_3() { return static_cast<int32_t>(offsetof(Definition_tF82F454FDB865FB8E647E7C9783284702FC65165, ___defaultToCenter_3)); }
	inline bool get_defaultToCenter_3() const { return ___defaultToCenter_3; }
	inline bool* get_address_of_defaultToCenter_3() { return &___defaultToCenter_3; }
	inline void set_defaultToCenter_3(bool value)
	{
		___defaultToCenter_3 = value;
	}

	inline static int32_t get_offset_of_movementArea_4() { return static_cast<int32_t>(offsetof(Definition_tF82F454FDB865FB8E647E7C9783284702FC65165, ___movementArea_4)); }
	inline ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14  get_movementArea_4() const { return ___movementArea_4; }
	inline ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14 * get_address_of_movementArea_4() { return &___movementArea_4; }
	inline void set_movementArea_4(ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14  value)
	{
		___movementArea_4 = value;
	}

	inline static int32_t get_offset_of_movementAreaUnit_5() { return static_cast<int32_t>(offsetof(Definition_tF82F454FDB865FB8E647E7C9783284702FC65165, ___movementAreaUnit_5)); }
	inline int32_t get_movementAreaUnit_5() const { return ___movementAreaUnit_5; }
	inline int32_t* get_address_of_movementAreaUnit_5() { return &___movementAreaUnit_5; }
	inline void set_movementAreaUnit_5(int32_t value)
	{
		___movementAreaUnit_5 = value;
	}

	inline static int32_t get_offset_of_pointerSpeed_6() { return static_cast<int32_t>(offsetof(Definition_tF82F454FDB865FB8E647E7C9783284702FC65165, ___pointerSpeed_6)); }
	inline float get_pointerSpeed_6() const { return ___pointerSpeed_6; }
	inline float* get_address_of_pointerSpeed_6() { return &___pointerSpeed_6; }
	inline void set_pointerSpeed_6(float value)
	{
		___pointerSpeed_6 = value;
	}

	inline static int32_t get_offset_of_useHardwarePointerPosition_7() { return static_cast<int32_t>(offsetof(Definition_tF82F454FDB865FB8E647E7C9783284702FC65165, ___useHardwarePointerPosition_7)); }
	inline bool get_useHardwarePointerPosition_7() const { return ___useHardwarePointerPosition_7; }
	inline bool* get_address_of_useHardwarePointerPosition_7() { return &___useHardwarePointerPosition_7; }
	inline void set_useHardwarePointerPosition_7(bool value)
	{
		___useHardwarePointerPosition_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITION_TF82F454FDB865FB8E647E7C9783284702FC65165_H
#ifndef TYPEWRAPPER_T788D13306712518A6288256C82C759738CC41C29_H
#define TYPEWRAPPER_T788D13306712518A6288256C82C759738CC41C29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.TypeWrapper
struct  TypeWrapper_t788D13306712518A6288256C82C759738CC41C29 
{
public:
	// Rewired.Utils.Classes.Data.TypeWrapper_DataType Rewired.Utils.Classes.Data.TypeWrapper::type
	int32_t ___type_0;
	// TYDfjiilEUxIfZMyRypsywmpmwA Rewired.Utils.Classes.Data.TypeWrapper::GcsCrWwEUNPgiZhGNANyAaKmIeHi
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8  ___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1;
	// System.String Rewired.Utils.Classes.Data.TypeWrapper::kQVmJpOPJsrLDZRAwRmSCbrkGjJE
	String_t* ___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2;
	// System.Object Rewired.Utils.Classes.Data.TypeWrapper::VnmoosDgcCbSdCuvMIbrnMSABaLs
	RuntimeObject * ___VnmoosDgcCbSdCuvMIbrnMSABaLs_3;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TypeWrapper_t788D13306712518A6288256C82C759738CC41C29, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_GcsCrWwEUNPgiZhGNANyAaKmIeHi_1() { return static_cast<int32_t>(offsetof(TypeWrapper_t788D13306712518A6288256C82C759738CC41C29, ___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1)); }
	inline TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8  get_GcsCrWwEUNPgiZhGNANyAaKmIeHi_1() const { return ___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1; }
	inline TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8 * get_address_of_GcsCrWwEUNPgiZhGNANyAaKmIeHi_1() { return &___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1; }
	inline void set_GcsCrWwEUNPgiZhGNANyAaKmIeHi_1(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8  value)
	{
		___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1 = value;
	}

	inline static int32_t get_offset_of_kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2() { return static_cast<int32_t>(offsetof(TypeWrapper_t788D13306712518A6288256C82C759738CC41C29, ___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2)); }
	inline String_t* get_kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2() const { return ___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2; }
	inline String_t** get_address_of_kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2() { return &___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2; }
	inline void set_kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2(String_t* value)
	{
		___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2 = value;
		Il2CppCodeGenWriteBarrier((&___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2), value);
	}

	inline static int32_t get_offset_of_VnmoosDgcCbSdCuvMIbrnMSABaLs_3() { return static_cast<int32_t>(offsetof(TypeWrapper_t788D13306712518A6288256C82C759738CC41C29, ___VnmoosDgcCbSdCuvMIbrnMSABaLs_3)); }
	inline RuntimeObject * get_VnmoosDgcCbSdCuvMIbrnMSABaLs_3() const { return ___VnmoosDgcCbSdCuvMIbrnMSABaLs_3; }
	inline RuntimeObject ** get_address_of_VnmoosDgcCbSdCuvMIbrnMSABaLs_3() { return &___VnmoosDgcCbSdCuvMIbrnMSABaLs_3; }
	inline void set_VnmoosDgcCbSdCuvMIbrnMSABaLs_3(RuntimeObject * value)
	{
		___VnmoosDgcCbSdCuvMIbrnMSABaLs_3 = value;
		Il2CppCodeGenWriteBarrier((&___VnmoosDgcCbSdCuvMIbrnMSABaLs_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.Utils.Classes.Data.TypeWrapper
struct TypeWrapper_t788D13306712518A6288256C82C759738CC41C29_marshaled_pinvoke
{
	int32_t ___type_0;
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8_marshaled_pinvoke ___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1;
	char* ___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2;
	Il2CppIUnknown* ___VnmoosDgcCbSdCuvMIbrnMSABaLs_3;
};
// Native definition for COM marshalling of Rewired.Utils.Classes.Data.TypeWrapper
struct TypeWrapper_t788D13306712518A6288256C82C759738CC41C29_marshaled_com
{
	int32_t ___type_0;
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8_marshaled_com ___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1;
	Il2CppChar* ___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2;
	Il2CppIUnknown* ___VnmoosDgcCbSdCuvMIbrnMSABaLs_3;
};
#endif // TYPEWRAPPER_T788D13306712518A6288256C82C759738CC41C29_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef CONTROLLERDATAFILES_T233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5_H
#define CONTROLLERDATAFILES_T233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ControllerDataFiles
struct  ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap Rewired.Data.ControllerDataFiles::defaultHardwareJoystickMap
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * ___defaultHardwareJoystickMap_4;
	// Rewired.Data.Mapping.HardwareJoystickMap[] Rewired.Data.ControllerDataFiles::hardwareJoystickMaps
	HardwareJoystickMapU5BU5D_t3DA1886163A97C877786AB4C7C60EF14C46B0FD4* ___hardwareJoystickMaps_5;
	// Rewired.Data.Mapping.HardwareJoystickTemplateMap[] Rewired.Data.ControllerDataFiles::joystickTemplates
	HardwareJoystickTemplateMapU5BU5D_t57BC6D30D553A657F72600B324770F807FDDECAB* ___joystickTemplates_6;
	// System.Boolean Rewired.Data.ControllerDataFiles::kvSBqhmkckXimqKmlOMbbiFcgwF
	bool ___kvSBqhmkckXimqKmlOMbbiFcgwF_7;

public:
	inline static int32_t get_offset_of_defaultHardwareJoystickMap_4() { return static_cast<int32_t>(offsetof(ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5, ___defaultHardwareJoystickMap_4)); }
	inline HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * get_defaultHardwareJoystickMap_4() const { return ___defaultHardwareJoystickMap_4; }
	inline HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB ** get_address_of_defaultHardwareJoystickMap_4() { return &___defaultHardwareJoystickMap_4; }
	inline void set_defaultHardwareJoystickMap_4(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB * value)
	{
		___defaultHardwareJoystickMap_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultHardwareJoystickMap_4), value);
	}

	inline static int32_t get_offset_of_hardwareJoystickMaps_5() { return static_cast<int32_t>(offsetof(ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5, ___hardwareJoystickMaps_5)); }
	inline HardwareJoystickMapU5BU5D_t3DA1886163A97C877786AB4C7C60EF14C46B0FD4* get_hardwareJoystickMaps_5() const { return ___hardwareJoystickMaps_5; }
	inline HardwareJoystickMapU5BU5D_t3DA1886163A97C877786AB4C7C60EF14C46B0FD4** get_address_of_hardwareJoystickMaps_5() { return &___hardwareJoystickMaps_5; }
	inline void set_hardwareJoystickMaps_5(HardwareJoystickMapU5BU5D_t3DA1886163A97C877786AB4C7C60EF14C46B0FD4* value)
	{
		___hardwareJoystickMaps_5 = value;
		Il2CppCodeGenWriteBarrier((&___hardwareJoystickMaps_5), value);
	}

	inline static int32_t get_offset_of_joystickTemplates_6() { return static_cast<int32_t>(offsetof(ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5, ___joystickTemplates_6)); }
	inline HardwareJoystickTemplateMapU5BU5D_t57BC6D30D553A657F72600B324770F807FDDECAB* get_joystickTemplates_6() const { return ___joystickTemplates_6; }
	inline HardwareJoystickTemplateMapU5BU5D_t57BC6D30D553A657F72600B324770F807FDDECAB** get_address_of_joystickTemplates_6() { return &___joystickTemplates_6; }
	inline void set_joystickTemplates_6(HardwareJoystickTemplateMapU5BU5D_t57BC6D30D553A657F72600B324770F807FDDECAB* value)
	{
		___joystickTemplates_6 = value;
		Il2CppCodeGenWriteBarrier((&___joystickTemplates_6), value);
	}

	inline static int32_t get_offset_of_kvSBqhmkckXimqKmlOMbbiFcgwF_7() { return static_cast<int32_t>(offsetof(ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5, ___kvSBqhmkckXimqKmlOMbbiFcgwF_7)); }
	inline bool get_kvSBqhmkckXimqKmlOMbbiFcgwF_7() const { return ___kvSBqhmkckXimqKmlOMbbiFcgwF_7; }
	inline bool* get_address_of_kvSBqhmkckXimqKmlOMbbiFcgwF_7() { return &___kvSBqhmkckXimqKmlOMbbiFcgwF_7; }
	inline void set_kvSBqhmkckXimqKmlOMbbiFcgwF_7(bool value)
	{
		___kvSBqhmkckXimqKmlOMbbiFcgwF_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERDATAFILES_T233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5_H
#ifndef EDITORSETTINGS_TAF090EB782C1024E7E0A8D475184757BC1A5CA2A_H
#define EDITORSETTINGS_TAF090EB782C1024E7E0A8D475184757BC1A5CA2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.EditorSettings
struct  EditorSettings_tAF090EB782C1024E7E0A8D475184757BC1A5CA2A  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Int32 Rewired.Data.EditorSettings::programVersion1
	int32_t ___programVersion1_4;
	// System.Int32 Rewired.Data.EditorSettings::programVersion2
	int32_t ___programVersion2_5;
	// System.Int32 Rewired.Data.EditorSettings::programVersion3
	int32_t ___programVersion3_6;
	// System.Int32 Rewired.Data.EditorSettings::programVersion4
	int32_t ___programVersion4_7;
	// System.Int32 Rewired.Data.EditorSettings::dataVersion
	int32_t ___dataVersion_8;

public:
	inline static int32_t get_offset_of_programVersion1_4() { return static_cast<int32_t>(offsetof(EditorSettings_tAF090EB782C1024E7E0A8D475184757BC1A5CA2A, ___programVersion1_4)); }
	inline int32_t get_programVersion1_4() const { return ___programVersion1_4; }
	inline int32_t* get_address_of_programVersion1_4() { return &___programVersion1_4; }
	inline void set_programVersion1_4(int32_t value)
	{
		___programVersion1_4 = value;
	}

	inline static int32_t get_offset_of_programVersion2_5() { return static_cast<int32_t>(offsetof(EditorSettings_tAF090EB782C1024E7E0A8D475184757BC1A5CA2A, ___programVersion2_5)); }
	inline int32_t get_programVersion2_5() const { return ___programVersion2_5; }
	inline int32_t* get_address_of_programVersion2_5() { return &___programVersion2_5; }
	inline void set_programVersion2_5(int32_t value)
	{
		___programVersion2_5 = value;
	}

	inline static int32_t get_offset_of_programVersion3_6() { return static_cast<int32_t>(offsetof(EditorSettings_tAF090EB782C1024E7E0A8D475184757BC1A5CA2A, ___programVersion3_6)); }
	inline int32_t get_programVersion3_6() const { return ___programVersion3_6; }
	inline int32_t* get_address_of_programVersion3_6() { return &___programVersion3_6; }
	inline void set_programVersion3_6(int32_t value)
	{
		___programVersion3_6 = value;
	}

	inline static int32_t get_offset_of_programVersion4_7() { return static_cast<int32_t>(offsetof(EditorSettings_tAF090EB782C1024E7E0A8D475184757BC1A5CA2A, ___programVersion4_7)); }
	inline int32_t get_programVersion4_7() const { return ___programVersion4_7; }
	inline int32_t* get_address_of_programVersion4_7() { return &___programVersion4_7; }
	inline void set_programVersion4_7(int32_t value)
	{
		___programVersion4_7 = value;
	}

	inline static int32_t get_offset_of_dataVersion_8() { return static_cast<int32_t>(offsetof(EditorSettings_tAF090EB782C1024E7E0A8D475184757BC1A5CA2A, ___dataVersion_8)); }
	inline int32_t get_dataVersion_8() const { return ___dataVersion_8; }
	inline int32_t* get_address_of_dataVersion_8() { return &___dataVersion_8; }
	inline void set_dataVersion_8(int32_t value)
	{
		___dataVersion_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORSETTINGS_TAF090EB782C1024E7E0A8D475184757BC1A5CA2A_H
#ifndef HARDWAREJOYSTICKMAP_TEC3597190BA5C54AF4F8849D274BE558ADB284FB_H
#define HARDWAREJOYSTICKMAP_TEC3597190BA5C54AF4F8849D274BE558ADB284FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap
struct  HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String Rewired.Data.Mapping.HardwareJoystickMap::controllerName
	String_t* ___controllerName_4;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap::editorControllerName
	String_t* ___editorControllerName_5;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap::description
	String_t* ___description_6;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap::controllerGuid
	String_t* ___controllerGuid_7;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap::templateGuids
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___templateGuids_8;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap::hideInLists
	bool ___hideInLists_9;
	// Rewired.JoystickType[] Rewired.Data.Mapping.HardwareJoystickMap::joystickTypes
	JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA* ___joystickTypes_10;
	// Rewired.ControllerElementIdentifier[] Rewired.Data.Mapping.HardwareJoystickMap::elementIdentifiers
	ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* ___elementIdentifiers_11;
	// Rewired.Data.Mapping.HardwareJoystickMap_CompoundElement[] Rewired.Data.Mapping.HardwareJoystickMap::compoundElements
	CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C* ___compoundElements_12;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput Rewired.Data.Mapping.HardwareJoystickMap::directInput
	Platform_DirectInput_t8384F48EB39207EF4B476BF85EEE95C48F113399 * ___directInput_13;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput Rewired.Data.Mapping.HardwareJoystickMap::rawInput
	Platform_RawInput_tFA8A4734C93ADCCF4CE7C2E3947003AB875E5FBD * ___rawInput_14;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput Rewired.Data.Mapping.HardwareJoystickMap::xInput
	Platform_XInput_t44E2E6F0FD4F4A22A7FB6C031984FFC5F82BDC16 * ___xInput_15;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX Rewired.Data.Mapping.HardwareJoystickMap::osx
	Platform_OSX_t03A46AF339888AC6E808D1D31A7467AD14FCA357 * ___osx_16;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux Rewired.Data.Mapping.HardwareJoystickMap::linux
	Platform_Linux_t5C50A8A935B1AE1D30D6F7912DA10AEAFFD53EB5 * ___linux_17;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP Rewired.Data.Mapping.HardwareJoystickMap::windowsUWP
	Platform_WindowsUWP_t79A50D7432962C9DD8B8CDFFAFC55B27BB32C06A * ___windowsUWP_18;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_Windows
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_Windows_19;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_WindowsUWP
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_WindowsUWP_20;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_OSX
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_OSX_21;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_Linux
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_Linux_22;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_Linux_PreConfigured
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_Linux_PreConfigured_23;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_Android
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_Android_24;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_iOS
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_iOS_25;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_Blackberry
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_Blackberry_26;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_WindowsPhone8
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_WindowsPhone8_27;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_XBox360
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_XBox360_28;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_XBoxOne
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_XBoxOne_29;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_PS3
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_PS3_30;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_PS4
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_PS4_31;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_PSM
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_PSM_32;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_PSVita
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_PSVita_33;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_Wii
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_Wii_34;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_WiiU
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_WiiU_35;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_AmazonFireTV
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_AmazonFireTV_36;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback Rewired.Data.Mapping.HardwareJoystickMap::fallback_RazerForgeTV
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * ___fallback_RazerForgeTV_37;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WebGL Rewired.Data.Mapping.HardwareJoystickMap::webGL
	Platform_WebGL_t7C026B328916B42D37B442AE084EABCB320F318A * ___webGL_38;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya Rewired.Data.Mapping.HardwareJoystickMap::ouya
	Platform_Ouya_t6E1DA960C43511902B179E0E6746B8230AE60092 * ___ouya_39;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne Rewired.Data.Mapping.HardwareJoystickMap::xboxOne
	Platform_XboxOne_tF5ACC52C21E7D086DBC6B842E36075C1582AA2C4 * ___xboxOne_40;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4 Rewired.Data.Mapping.HardwareJoystickMap::ps4
	Platform_PS4_t5B4B7E5A8EF60DFB5E1F1CE6756ADD5BD5FB4FA1 * ___ps4_41;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch Rewired.Data.Mapping.HardwareJoystickMap::nintendoSwitch
	Platform_NintendoSwitch_t2696C2B97DA28B7AFFF2A0AA8CE74563C99F99C8 * ___nintendoSwitch_42;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_InternalDriver Rewired.Data.Mapping.HardwareJoystickMap::internalDriver
	Platform_InternalDriver_t8A5A88BC6C79D793F1E687A676A4EC95C644007F * ___internalDriver_43;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2 Rewired.Data.Mapping.HardwareJoystickMap::sdl2_Linux
	Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A * ___sdl2_Linux_44;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2 Rewired.Data.Mapping.HardwareJoystickMap::sdl2_Windows
	Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A * ___sdl2_Windows_45;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_SDL2 Rewired.Data.Mapping.HardwareJoystickMap::sdl2_OSX
	Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A * ___sdl2_OSX_46;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap::elementIdentifierIdCounter
	int32_t ___elementIdentifierIdCounter_47;

public:
	inline static int32_t get_offset_of_controllerName_4() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___controllerName_4)); }
	inline String_t* get_controllerName_4() const { return ___controllerName_4; }
	inline String_t** get_address_of_controllerName_4() { return &___controllerName_4; }
	inline void set_controllerName_4(String_t* value)
	{
		___controllerName_4 = value;
		Il2CppCodeGenWriteBarrier((&___controllerName_4), value);
	}

	inline static int32_t get_offset_of_editorControllerName_5() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___editorControllerName_5)); }
	inline String_t* get_editorControllerName_5() const { return ___editorControllerName_5; }
	inline String_t** get_address_of_editorControllerName_5() { return &___editorControllerName_5; }
	inline void set_editorControllerName_5(String_t* value)
	{
		___editorControllerName_5 = value;
		Il2CppCodeGenWriteBarrier((&___editorControllerName_5), value);
	}

	inline static int32_t get_offset_of_description_6() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___description_6)); }
	inline String_t* get_description_6() const { return ___description_6; }
	inline String_t** get_address_of_description_6() { return &___description_6; }
	inline void set_description_6(String_t* value)
	{
		___description_6 = value;
		Il2CppCodeGenWriteBarrier((&___description_6), value);
	}

	inline static int32_t get_offset_of_controllerGuid_7() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___controllerGuid_7)); }
	inline String_t* get_controllerGuid_7() const { return ___controllerGuid_7; }
	inline String_t** get_address_of_controllerGuid_7() { return &___controllerGuid_7; }
	inline void set_controllerGuid_7(String_t* value)
	{
		___controllerGuid_7 = value;
		Il2CppCodeGenWriteBarrier((&___controllerGuid_7), value);
	}

	inline static int32_t get_offset_of_templateGuids_8() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___templateGuids_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_templateGuids_8() const { return ___templateGuids_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_templateGuids_8() { return &___templateGuids_8; }
	inline void set_templateGuids_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___templateGuids_8 = value;
		Il2CppCodeGenWriteBarrier((&___templateGuids_8), value);
	}

	inline static int32_t get_offset_of_hideInLists_9() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___hideInLists_9)); }
	inline bool get_hideInLists_9() const { return ___hideInLists_9; }
	inline bool* get_address_of_hideInLists_9() { return &___hideInLists_9; }
	inline void set_hideInLists_9(bool value)
	{
		___hideInLists_9 = value;
	}

	inline static int32_t get_offset_of_joystickTypes_10() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___joystickTypes_10)); }
	inline JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA* get_joystickTypes_10() const { return ___joystickTypes_10; }
	inline JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA** get_address_of_joystickTypes_10() { return &___joystickTypes_10; }
	inline void set_joystickTypes_10(JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA* value)
	{
		___joystickTypes_10 = value;
		Il2CppCodeGenWriteBarrier((&___joystickTypes_10), value);
	}

	inline static int32_t get_offset_of_elementIdentifiers_11() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___elementIdentifiers_11)); }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* get_elementIdentifiers_11() const { return ___elementIdentifiers_11; }
	inline ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE** get_address_of_elementIdentifiers_11() { return &___elementIdentifiers_11; }
	inline void set_elementIdentifiers_11(ControllerElementIdentifierU5BU5D_tA8BA3AEF17F1DDBADB44C5BE04B7F34917625DEE* value)
	{
		___elementIdentifiers_11 = value;
		Il2CppCodeGenWriteBarrier((&___elementIdentifiers_11), value);
	}

	inline static int32_t get_offset_of_compoundElements_12() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___compoundElements_12)); }
	inline CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C* get_compoundElements_12() const { return ___compoundElements_12; }
	inline CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C** get_address_of_compoundElements_12() { return &___compoundElements_12; }
	inline void set_compoundElements_12(CompoundElementU5BU5D_t99A80F4322F7288CB752671D33337B32F7A0E79C* value)
	{
		___compoundElements_12 = value;
		Il2CppCodeGenWriteBarrier((&___compoundElements_12), value);
	}

	inline static int32_t get_offset_of_directInput_13() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___directInput_13)); }
	inline Platform_DirectInput_t8384F48EB39207EF4B476BF85EEE95C48F113399 * get_directInput_13() const { return ___directInput_13; }
	inline Platform_DirectInput_t8384F48EB39207EF4B476BF85EEE95C48F113399 ** get_address_of_directInput_13() { return &___directInput_13; }
	inline void set_directInput_13(Platform_DirectInput_t8384F48EB39207EF4B476BF85EEE95C48F113399 * value)
	{
		___directInput_13 = value;
		Il2CppCodeGenWriteBarrier((&___directInput_13), value);
	}

	inline static int32_t get_offset_of_rawInput_14() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___rawInput_14)); }
	inline Platform_RawInput_tFA8A4734C93ADCCF4CE7C2E3947003AB875E5FBD * get_rawInput_14() const { return ___rawInput_14; }
	inline Platform_RawInput_tFA8A4734C93ADCCF4CE7C2E3947003AB875E5FBD ** get_address_of_rawInput_14() { return &___rawInput_14; }
	inline void set_rawInput_14(Platform_RawInput_tFA8A4734C93ADCCF4CE7C2E3947003AB875E5FBD * value)
	{
		___rawInput_14 = value;
		Il2CppCodeGenWriteBarrier((&___rawInput_14), value);
	}

	inline static int32_t get_offset_of_xInput_15() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___xInput_15)); }
	inline Platform_XInput_t44E2E6F0FD4F4A22A7FB6C031984FFC5F82BDC16 * get_xInput_15() const { return ___xInput_15; }
	inline Platform_XInput_t44E2E6F0FD4F4A22A7FB6C031984FFC5F82BDC16 ** get_address_of_xInput_15() { return &___xInput_15; }
	inline void set_xInput_15(Platform_XInput_t44E2E6F0FD4F4A22A7FB6C031984FFC5F82BDC16 * value)
	{
		___xInput_15 = value;
		Il2CppCodeGenWriteBarrier((&___xInput_15), value);
	}

	inline static int32_t get_offset_of_osx_16() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___osx_16)); }
	inline Platform_OSX_t03A46AF339888AC6E808D1D31A7467AD14FCA357 * get_osx_16() const { return ___osx_16; }
	inline Platform_OSX_t03A46AF339888AC6E808D1D31A7467AD14FCA357 ** get_address_of_osx_16() { return &___osx_16; }
	inline void set_osx_16(Platform_OSX_t03A46AF339888AC6E808D1D31A7467AD14FCA357 * value)
	{
		___osx_16 = value;
		Il2CppCodeGenWriteBarrier((&___osx_16), value);
	}

	inline static int32_t get_offset_of_linux_17() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___linux_17)); }
	inline Platform_Linux_t5C50A8A935B1AE1D30D6F7912DA10AEAFFD53EB5 * get_linux_17() const { return ___linux_17; }
	inline Platform_Linux_t5C50A8A935B1AE1D30D6F7912DA10AEAFFD53EB5 ** get_address_of_linux_17() { return &___linux_17; }
	inline void set_linux_17(Platform_Linux_t5C50A8A935B1AE1D30D6F7912DA10AEAFFD53EB5 * value)
	{
		___linux_17 = value;
		Il2CppCodeGenWriteBarrier((&___linux_17), value);
	}

	inline static int32_t get_offset_of_windowsUWP_18() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___windowsUWP_18)); }
	inline Platform_WindowsUWP_t79A50D7432962C9DD8B8CDFFAFC55B27BB32C06A * get_windowsUWP_18() const { return ___windowsUWP_18; }
	inline Platform_WindowsUWP_t79A50D7432962C9DD8B8CDFFAFC55B27BB32C06A ** get_address_of_windowsUWP_18() { return &___windowsUWP_18; }
	inline void set_windowsUWP_18(Platform_WindowsUWP_t79A50D7432962C9DD8B8CDFFAFC55B27BB32C06A * value)
	{
		___windowsUWP_18 = value;
		Il2CppCodeGenWriteBarrier((&___windowsUWP_18), value);
	}

	inline static int32_t get_offset_of_fallback_Windows_19() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_Windows_19)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_Windows_19() const { return ___fallback_Windows_19; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_Windows_19() { return &___fallback_Windows_19; }
	inline void set_fallback_Windows_19(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_Windows_19 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_Windows_19), value);
	}

	inline static int32_t get_offset_of_fallback_WindowsUWP_20() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_WindowsUWP_20)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_WindowsUWP_20() const { return ___fallback_WindowsUWP_20; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_WindowsUWP_20() { return &___fallback_WindowsUWP_20; }
	inline void set_fallback_WindowsUWP_20(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_WindowsUWP_20 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_WindowsUWP_20), value);
	}

	inline static int32_t get_offset_of_fallback_OSX_21() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_OSX_21)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_OSX_21() const { return ___fallback_OSX_21; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_OSX_21() { return &___fallback_OSX_21; }
	inline void set_fallback_OSX_21(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_OSX_21 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_OSX_21), value);
	}

	inline static int32_t get_offset_of_fallback_Linux_22() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_Linux_22)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_Linux_22() const { return ___fallback_Linux_22; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_Linux_22() { return &___fallback_Linux_22; }
	inline void set_fallback_Linux_22(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_Linux_22 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_Linux_22), value);
	}

	inline static int32_t get_offset_of_fallback_Linux_PreConfigured_23() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_Linux_PreConfigured_23)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_Linux_PreConfigured_23() const { return ___fallback_Linux_PreConfigured_23; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_Linux_PreConfigured_23() { return &___fallback_Linux_PreConfigured_23; }
	inline void set_fallback_Linux_PreConfigured_23(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_Linux_PreConfigured_23 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_Linux_PreConfigured_23), value);
	}

	inline static int32_t get_offset_of_fallback_Android_24() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_Android_24)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_Android_24() const { return ___fallback_Android_24; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_Android_24() { return &___fallback_Android_24; }
	inline void set_fallback_Android_24(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_Android_24 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_Android_24), value);
	}

	inline static int32_t get_offset_of_fallback_iOS_25() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_iOS_25)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_iOS_25() const { return ___fallback_iOS_25; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_iOS_25() { return &___fallback_iOS_25; }
	inline void set_fallback_iOS_25(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_iOS_25 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_iOS_25), value);
	}

	inline static int32_t get_offset_of_fallback_Blackberry_26() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_Blackberry_26)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_Blackberry_26() const { return ___fallback_Blackberry_26; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_Blackberry_26() { return &___fallback_Blackberry_26; }
	inline void set_fallback_Blackberry_26(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_Blackberry_26 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_Blackberry_26), value);
	}

	inline static int32_t get_offset_of_fallback_WindowsPhone8_27() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_WindowsPhone8_27)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_WindowsPhone8_27() const { return ___fallback_WindowsPhone8_27; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_WindowsPhone8_27() { return &___fallback_WindowsPhone8_27; }
	inline void set_fallback_WindowsPhone8_27(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_WindowsPhone8_27 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_WindowsPhone8_27), value);
	}

	inline static int32_t get_offset_of_fallback_XBox360_28() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_XBox360_28)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_XBox360_28() const { return ___fallback_XBox360_28; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_XBox360_28() { return &___fallback_XBox360_28; }
	inline void set_fallback_XBox360_28(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_XBox360_28 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_XBox360_28), value);
	}

	inline static int32_t get_offset_of_fallback_XBoxOne_29() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_XBoxOne_29)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_XBoxOne_29() const { return ___fallback_XBoxOne_29; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_XBoxOne_29() { return &___fallback_XBoxOne_29; }
	inline void set_fallback_XBoxOne_29(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_XBoxOne_29 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_XBoxOne_29), value);
	}

	inline static int32_t get_offset_of_fallback_PS3_30() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_PS3_30)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_PS3_30() const { return ___fallback_PS3_30; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_PS3_30() { return &___fallback_PS3_30; }
	inline void set_fallback_PS3_30(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_PS3_30 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_PS3_30), value);
	}

	inline static int32_t get_offset_of_fallback_PS4_31() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_PS4_31)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_PS4_31() const { return ___fallback_PS4_31; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_PS4_31() { return &___fallback_PS4_31; }
	inline void set_fallback_PS4_31(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_PS4_31 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_PS4_31), value);
	}

	inline static int32_t get_offset_of_fallback_PSM_32() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_PSM_32)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_PSM_32() const { return ___fallback_PSM_32; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_PSM_32() { return &___fallback_PSM_32; }
	inline void set_fallback_PSM_32(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_PSM_32 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_PSM_32), value);
	}

	inline static int32_t get_offset_of_fallback_PSVita_33() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_PSVita_33)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_PSVita_33() const { return ___fallback_PSVita_33; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_PSVita_33() { return &___fallback_PSVita_33; }
	inline void set_fallback_PSVita_33(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_PSVita_33 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_PSVita_33), value);
	}

	inline static int32_t get_offset_of_fallback_Wii_34() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_Wii_34)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_Wii_34() const { return ___fallback_Wii_34; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_Wii_34() { return &___fallback_Wii_34; }
	inline void set_fallback_Wii_34(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_Wii_34 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_Wii_34), value);
	}

	inline static int32_t get_offset_of_fallback_WiiU_35() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_WiiU_35)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_WiiU_35() const { return ___fallback_WiiU_35; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_WiiU_35() { return &___fallback_WiiU_35; }
	inline void set_fallback_WiiU_35(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_WiiU_35 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_WiiU_35), value);
	}

	inline static int32_t get_offset_of_fallback_AmazonFireTV_36() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_AmazonFireTV_36)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_AmazonFireTV_36() const { return ___fallback_AmazonFireTV_36; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_AmazonFireTV_36() { return &___fallback_AmazonFireTV_36; }
	inline void set_fallback_AmazonFireTV_36(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_AmazonFireTV_36 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_AmazonFireTV_36), value);
	}

	inline static int32_t get_offset_of_fallback_RazerForgeTV_37() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___fallback_RazerForgeTV_37)); }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * get_fallback_RazerForgeTV_37() const { return ___fallback_RazerForgeTV_37; }
	inline Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 ** get_address_of_fallback_RazerForgeTV_37() { return &___fallback_RazerForgeTV_37; }
	inline void set_fallback_RazerForgeTV_37(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313 * value)
	{
		___fallback_RazerForgeTV_37 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_RazerForgeTV_37), value);
	}

	inline static int32_t get_offset_of_webGL_38() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___webGL_38)); }
	inline Platform_WebGL_t7C026B328916B42D37B442AE084EABCB320F318A * get_webGL_38() const { return ___webGL_38; }
	inline Platform_WebGL_t7C026B328916B42D37B442AE084EABCB320F318A ** get_address_of_webGL_38() { return &___webGL_38; }
	inline void set_webGL_38(Platform_WebGL_t7C026B328916B42D37B442AE084EABCB320F318A * value)
	{
		___webGL_38 = value;
		Il2CppCodeGenWriteBarrier((&___webGL_38), value);
	}

	inline static int32_t get_offset_of_ouya_39() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___ouya_39)); }
	inline Platform_Ouya_t6E1DA960C43511902B179E0E6746B8230AE60092 * get_ouya_39() const { return ___ouya_39; }
	inline Platform_Ouya_t6E1DA960C43511902B179E0E6746B8230AE60092 ** get_address_of_ouya_39() { return &___ouya_39; }
	inline void set_ouya_39(Platform_Ouya_t6E1DA960C43511902B179E0E6746B8230AE60092 * value)
	{
		___ouya_39 = value;
		Il2CppCodeGenWriteBarrier((&___ouya_39), value);
	}

	inline static int32_t get_offset_of_xboxOne_40() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___xboxOne_40)); }
	inline Platform_XboxOne_tF5ACC52C21E7D086DBC6B842E36075C1582AA2C4 * get_xboxOne_40() const { return ___xboxOne_40; }
	inline Platform_XboxOne_tF5ACC52C21E7D086DBC6B842E36075C1582AA2C4 ** get_address_of_xboxOne_40() { return &___xboxOne_40; }
	inline void set_xboxOne_40(Platform_XboxOne_tF5ACC52C21E7D086DBC6B842E36075C1582AA2C4 * value)
	{
		___xboxOne_40 = value;
		Il2CppCodeGenWriteBarrier((&___xboxOne_40), value);
	}

	inline static int32_t get_offset_of_ps4_41() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___ps4_41)); }
	inline Platform_PS4_t5B4B7E5A8EF60DFB5E1F1CE6756ADD5BD5FB4FA1 * get_ps4_41() const { return ___ps4_41; }
	inline Platform_PS4_t5B4B7E5A8EF60DFB5E1F1CE6756ADD5BD5FB4FA1 ** get_address_of_ps4_41() { return &___ps4_41; }
	inline void set_ps4_41(Platform_PS4_t5B4B7E5A8EF60DFB5E1F1CE6756ADD5BD5FB4FA1 * value)
	{
		___ps4_41 = value;
		Il2CppCodeGenWriteBarrier((&___ps4_41), value);
	}

	inline static int32_t get_offset_of_nintendoSwitch_42() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___nintendoSwitch_42)); }
	inline Platform_NintendoSwitch_t2696C2B97DA28B7AFFF2A0AA8CE74563C99F99C8 * get_nintendoSwitch_42() const { return ___nintendoSwitch_42; }
	inline Platform_NintendoSwitch_t2696C2B97DA28B7AFFF2A0AA8CE74563C99F99C8 ** get_address_of_nintendoSwitch_42() { return &___nintendoSwitch_42; }
	inline void set_nintendoSwitch_42(Platform_NintendoSwitch_t2696C2B97DA28B7AFFF2A0AA8CE74563C99F99C8 * value)
	{
		___nintendoSwitch_42 = value;
		Il2CppCodeGenWriteBarrier((&___nintendoSwitch_42), value);
	}

	inline static int32_t get_offset_of_internalDriver_43() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___internalDriver_43)); }
	inline Platform_InternalDriver_t8A5A88BC6C79D793F1E687A676A4EC95C644007F * get_internalDriver_43() const { return ___internalDriver_43; }
	inline Platform_InternalDriver_t8A5A88BC6C79D793F1E687A676A4EC95C644007F ** get_address_of_internalDriver_43() { return &___internalDriver_43; }
	inline void set_internalDriver_43(Platform_InternalDriver_t8A5A88BC6C79D793F1E687A676A4EC95C644007F * value)
	{
		___internalDriver_43 = value;
		Il2CppCodeGenWriteBarrier((&___internalDriver_43), value);
	}

	inline static int32_t get_offset_of_sdl2_Linux_44() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___sdl2_Linux_44)); }
	inline Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A * get_sdl2_Linux_44() const { return ___sdl2_Linux_44; }
	inline Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A ** get_address_of_sdl2_Linux_44() { return &___sdl2_Linux_44; }
	inline void set_sdl2_Linux_44(Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A * value)
	{
		___sdl2_Linux_44 = value;
		Il2CppCodeGenWriteBarrier((&___sdl2_Linux_44), value);
	}

	inline static int32_t get_offset_of_sdl2_Windows_45() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___sdl2_Windows_45)); }
	inline Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A * get_sdl2_Windows_45() const { return ___sdl2_Windows_45; }
	inline Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A ** get_address_of_sdl2_Windows_45() { return &___sdl2_Windows_45; }
	inline void set_sdl2_Windows_45(Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A * value)
	{
		___sdl2_Windows_45 = value;
		Il2CppCodeGenWriteBarrier((&___sdl2_Windows_45), value);
	}

	inline static int32_t get_offset_of_sdl2_OSX_46() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___sdl2_OSX_46)); }
	inline Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A * get_sdl2_OSX_46() const { return ___sdl2_OSX_46; }
	inline Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A ** get_address_of_sdl2_OSX_46() { return &___sdl2_OSX_46; }
	inline void set_sdl2_OSX_46(Platform_SDL2_t5339C1F7963CAC4185CB1400C3EA0174D7A7B80A * value)
	{
		___sdl2_OSX_46 = value;
		Il2CppCodeGenWriteBarrier((&___sdl2_OSX_46), value);
	}

	inline static int32_t get_offset_of_elementIdentifierIdCounter_47() { return static_cast<int32_t>(offsetof(HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB, ___elementIdentifierIdCounter_47)); }
	inline int32_t get_elementIdentifierIdCounter_47() const { return ___elementIdentifierIdCounter_47; }
	inline int32_t* get_address_of_elementIdentifierIdCounter_47() { return &___elementIdentifierIdCounter_47; }
	inline void set_elementIdentifierIdCounter_47(int32_t value)
	{
		___elementIdentifierIdCounter_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWAREJOYSTICKMAP_TEC3597190BA5C54AF4F8849D274BE558ADB284FB_H
#ifndef AXIS_TDF7D3B4A808B8EC21B426BA447F16A9656C5E80C_H
#define AXIS_TDF7D3B4A808B8EC21B426BA447F16A9656C5E80C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Axis
struct  Axis_tDF7D3B4A808B8EC21B426BA447F16A9656C5E80C  : public Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_TDF7D3B4A808B8EC21B426BA447F16A9656C5E80C_H
#ifndef BUTTON_TB6F1D153DD9EDACC05ED5C32D8D1EF258B329266_H
#define BUTTON_TB6F1D153DD9EDACC05ED5C32D8D1EF258B329266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_DirectInput_Base_Button
struct  Button_tB6F1D153DD9EDACC05ED5C32D8D1EF258B329266  : public Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_TB6F1D153DD9EDACC05ED5C32D8D1EF258B329266_H
#ifndef MOUSEAXIS_T60D6DDDC2AAF95DB273A010BE949D8365076A8DF_H
#define MOUSEAXIS_T60D6DDDC2AAF95DB273A010BE949D8365076A8DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_MouseAxis
struct  MouseAxis_t60D6DDDC2AAF95DB273A010BE949D8365076A8DF  : public Axis_t99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEAXIS_T60D6DDDC2AAF95DB273A010BE949D8365076A8DF_H
#ifndef DEFINITION_T0C037D21BC8BDFEDEA1FA85DE2E3CF081F3EB242_H
#define DEFINITION_T0C037D21BC8BDFEDEA1FA85DE2E3CF081F3EB242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_MouseAxis_Definition
struct  Definition_t0C037D21BC8BDFEDEA1FA85DE2E3CF081F3EB242  : public Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITION_T0C037D21BC8BDFEDEA1FA85DE2E3CF081F3EB242_H
#ifndef MOUSEWHEELAXIS_TE34ABA4811B899C43F609891C924BD3797CE21A1_H
#define MOUSEWHEELAXIS_TE34ABA4811B899C43F609891C924BD3797CE21A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_MouseWheelAxis
struct  MouseWheelAxis_tE34ABA4811B899C43F609891C924BD3797CE21A1  : public Axis_t99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A
{
public:
	// System.Single Rewired.PlayerController_MouseWheelAxis::UpkvflgKnqIYwagopCDVqQIjcSUd
	float ___UpkvflgKnqIYwagopCDVqQIjcSUd_17;
	// System.Single Rewired.PlayerController_MouseWheelAxis::eYPMVuyFLSfzqcuiinQTgcTexKRJ
	float ___eYPMVuyFLSfzqcuiinQTgcTexKRJ_18;
	// System.Single Rewired.PlayerController_MouseWheelAxis::mvKHSWNtYxtrXAPVpClHLUktCzV
	float ___mvKHSWNtYxtrXAPVpClHLUktCzV_19;

public:
	inline static int32_t get_offset_of_UpkvflgKnqIYwagopCDVqQIjcSUd_17() { return static_cast<int32_t>(offsetof(MouseWheelAxis_tE34ABA4811B899C43F609891C924BD3797CE21A1, ___UpkvflgKnqIYwagopCDVqQIjcSUd_17)); }
	inline float get_UpkvflgKnqIYwagopCDVqQIjcSUd_17() const { return ___UpkvflgKnqIYwagopCDVqQIjcSUd_17; }
	inline float* get_address_of_UpkvflgKnqIYwagopCDVqQIjcSUd_17() { return &___UpkvflgKnqIYwagopCDVqQIjcSUd_17; }
	inline void set_UpkvflgKnqIYwagopCDVqQIjcSUd_17(float value)
	{
		___UpkvflgKnqIYwagopCDVqQIjcSUd_17 = value;
	}

	inline static int32_t get_offset_of_eYPMVuyFLSfzqcuiinQTgcTexKRJ_18() { return static_cast<int32_t>(offsetof(MouseWheelAxis_tE34ABA4811B899C43F609891C924BD3797CE21A1, ___eYPMVuyFLSfzqcuiinQTgcTexKRJ_18)); }
	inline float get_eYPMVuyFLSfzqcuiinQTgcTexKRJ_18() const { return ___eYPMVuyFLSfzqcuiinQTgcTexKRJ_18; }
	inline float* get_address_of_eYPMVuyFLSfzqcuiinQTgcTexKRJ_18() { return &___eYPMVuyFLSfzqcuiinQTgcTexKRJ_18; }
	inline void set_eYPMVuyFLSfzqcuiinQTgcTexKRJ_18(float value)
	{
		___eYPMVuyFLSfzqcuiinQTgcTexKRJ_18 = value;
	}

	inline static int32_t get_offset_of_mvKHSWNtYxtrXAPVpClHLUktCzV_19() { return static_cast<int32_t>(offsetof(MouseWheelAxis_tE34ABA4811B899C43F609891C924BD3797CE21A1, ___mvKHSWNtYxtrXAPVpClHLUktCzV_19)); }
	inline float get_mvKHSWNtYxtrXAPVpClHLUktCzV_19() const { return ___mvKHSWNtYxtrXAPVpClHLUktCzV_19; }
	inline float* get_address_of_mvKHSWNtYxtrXAPVpClHLUktCzV_19() { return &___mvKHSWNtYxtrXAPVpClHLUktCzV_19; }
	inline void set_mvKHSWNtYxtrXAPVpClHLUktCzV_19(float value)
	{
		___mvKHSWNtYxtrXAPVpClHLUktCzV_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEWHEELAXIS_TE34ABA4811B899C43F609891C924BD3797CE21A1_H
#ifndef DEFINITION_T8556A31E68641FE8480951FC1733CBA187B0C7F7_H
#define DEFINITION_T8556A31E68641FE8480951FC1733CBA187B0C7F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlayerController_MouseWheelAxis_Definition
struct  Definition_t8556A31E68641FE8480951FC1733CBA187B0C7F7  : public Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9
{
public:
	// System.Single Rewired.PlayerController_MouseWheelAxis_Definition::repeatRate
	float ___repeatRate_5;

public:
	inline static int32_t get_offset_of_repeatRate_5() { return static_cast<int32_t>(offsetof(Definition_t8556A31E68641FE8480951FC1733CBA187B0C7F7, ___repeatRate_5)); }
	inline float get_repeatRate_5() const { return ___repeatRate_5; }
	inline float* get_address_of_repeatRate_5() { return &___repeatRate_5; }
	inline void set_repeatRate_5(float value)
	{
		___repeatRate_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINITION_T8556A31E68641FE8480951FC1733CBA187B0C7F7_H
#ifndef SERIALIZEDMETHOD_T620A6C7138FEC59B91B3CF7B58C47B50C50B082E_H
#define SERIALIZEDMETHOD_T620A6C7138FEC59B91B3CF7B58C47B50C50B082E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.SerializedMethod
struct  SerializedMethod_t620A6C7138FEC59B91B3CF7B58C47B50C50B082E  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Boolean Rewired.Utils.Classes.SerializedMethod::vclPlLKqlxHWseSEJOiyCtEkWhpb
	bool ___vclPlLKqlxHWseSEJOiyCtEkWhpb_5;
	// System.Collections.Generic.List`1<Rewired.Utils.Classes.Data.TypeWrapper> Rewired.Utils.Classes.SerializedMethod::_data
	List_1_tE69A2AD31EE2358B14DC2FF01FA302D8FCF220CC * ____data_6;
	// Rewired.Utils.Classes.Data.TypeWrapper Rewired.Utils.Classes.SerializedMethod::_result
	TypeWrapper_t788D13306712518A6288256C82C759738CC41C29  ____result_7;
	// System.Boolean Rewired.Utils.Classes.SerializedMethod::_resultIsValid
	bool ____resultIsValid_8;

public:
	inline static int32_t get_offset_of_vclPlLKqlxHWseSEJOiyCtEkWhpb_5() { return static_cast<int32_t>(offsetof(SerializedMethod_t620A6C7138FEC59B91B3CF7B58C47B50C50B082E, ___vclPlLKqlxHWseSEJOiyCtEkWhpb_5)); }
	inline bool get_vclPlLKqlxHWseSEJOiyCtEkWhpb_5() const { return ___vclPlLKqlxHWseSEJOiyCtEkWhpb_5; }
	inline bool* get_address_of_vclPlLKqlxHWseSEJOiyCtEkWhpb_5() { return &___vclPlLKqlxHWseSEJOiyCtEkWhpb_5; }
	inline void set_vclPlLKqlxHWseSEJOiyCtEkWhpb_5(bool value)
	{
		___vclPlLKqlxHWseSEJOiyCtEkWhpb_5 = value;
	}

	inline static int32_t get_offset_of__data_6() { return static_cast<int32_t>(offsetof(SerializedMethod_t620A6C7138FEC59B91B3CF7B58C47B50C50B082E, ____data_6)); }
	inline List_1_tE69A2AD31EE2358B14DC2FF01FA302D8FCF220CC * get__data_6() const { return ____data_6; }
	inline List_1_tE69A2AD31EE2358B14DC2FF01FA302D8FCF220CC ** get_address_of__data_6() { return &____data_6; }
	inline void set__data_6(List_1_tE69A2AD31EE2358B14DC2FF01FA302D8FCF220CC * value)
	{
		____data_6 = value;
		Il2CppCodeGenWriteBarrier((&____data_6), value);
	}

	inline static int32_t get_offset_of__result_7() { return static_cast<int32_t>(offsetof(SerializedMethod_t620A6C7138FEC59B91B3CF7B58C47B50C50B082E, ____result_7)); }
	inline TypeWrapper_t788D13306712518A6288256C82C759738CC41C29  get__result_7() const { return ____result_7; }
	inline TypeWrapper_t788D13306712518A6288256C82C759738CC41C29 * get_address_of__result_7() { return &____result_7; }
	inline void set__result_7(TypeWrapper_t788D13306712518A6288256C82C759738CC41C29  value)
	{
		____result_7 = value;
	}

	inline static int32_t get_offset_of__resultIsValid_8() { return static_cast<int32_t>(offsetof(SerializedMethod_t620A6C7138FEC59B91B3CF7B58C47B50C50B082E, ____resultIsValid_8)); }
	inline bool get__resultIsValid_8() const { return ____resultIsValid_8; }
	inline bool* get_address_of__resultIsValid_8() { return &____resultIsValid_8; }
	inline void set__resultIsValid_8(bool value)
	{
		____resultIsValid_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDMETHOD_T620A6C7138FEC59B91B3CF7B58C47B50C50B082E_H
#ifndef CUSTOMCALCULATION_T9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038_H
#define CUSTOMCALCULATION_T9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.CustomCalculation
struct  CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038  : public SerializedMethod_t620A6C7138FEC59B91B3CF7B58C47B50C50B082E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCALCULATION_T9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038_H
#ifndef CUSTOMCALCULATION_ACCELEROMETER_TD6A0A03F922F3200D9E9C7ED0D8DAFF2590AD79E_H
#define CUSTOMCALCULATION_ACCELEROMETER_TD6A0A03F922F3200D9E9C7ED0D8DAFF2590AD79E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.CustomCalculation_Accelerometer
struct  CustomCalculation_Accelerometer_tD6A0A03F922F3200D9E9C7ED0D8DAFF2590AD79E  : public CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038
{
public:
	// Rewired.Data.Mapping.CustomCalculation_Accelerometer_CalculationType Rewired.Data.Mapping.CustomCalculation_Accelerometer::_calculationType
	int32_t ____calculationType_9;
	// Rewired.Data.Mapping.CustomCalculation_Accelerometer_InputType Rewired.Data.Mapping.CustomCalculation_Accelerometer::_inputType
	int32_t ____inputType_10;
	// Rewired.Data.Mapping.CustomCalculation_Accelerometer_OutputType Rewired.Data.Mapping.CustomCalculation_Accelerometer::_outputType
	int32_t ____outputType_11;

public:
	inline static int32_t get_offset_of__calculationType_9() { return static_cast<int32_t>(offsetof(CustomCalculation_Accelerometer_tD6A0A03F922F3200D9E9C7ED0D8DAFF2590AD79E, ____calculationType_9)); }
	inline int32_t get__calculationType_9() const { return ____calculationType_9; }
	inline int32_t* get_address_of__calculationType_9() { return &____calculationType_9; }
	inline void set__calculationType_9(int32_t value)
	{
		____calculationType_9 = value;
	}

	inline static int32_t get_offset_of__inputType_10() { return static_cast<int32_t>(offsetof(CustomCalculation_Accelerometer_tD6A0A03F922F3200D9E9C7ED0D8DAFF2590AD79E, ____inputType_10)); }
	inline int32_t get__inputType_10() const { return ____inputType_10; }
	inline int32_t* get_address_of__inputType_10() { return &____inputType_10; }
	inline void set__inputType_10(int32_t value)
	{
		____inputType_10 = value;
	}

	inline static int32_t get_offset_of__outputType_11() { return static_cast<int32_t>(offsetof(CustomCalculation_Accelerometer_tD6A0A03F922F3200D9E9C7ED0D8DAFF2590AD79E, ____outputType_11)); }
	inline int32_t get__outputType_11() const { return ____outputType_11; }
	inline int32_t* get_address_of__outputType_11() { return &____outputType_11; }
	inline void set__outputType_11(int32_t value)
	{
		____outputType_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCALCULATION_ACCELEROMETER_TD6A0A03F922F3200D9E9C7ED0D8DAFF2590AD79E_H
#ifndef CUSTOMCALCULATION_COMPAREELEMENTVALUES_TA210ED02BC946094240D1C2EAB44E66AECEBF233_H
#define CUSTOMCALCULATION_COMPAREELEMENTVALUES_TA210ED02BC946094240D1C2EAB44E66AECEBF233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.CustomCalculation_CompareElementValues
struct  CustomCalculation_CompareElementValues_tA210ED02BC946094240D1C2EAB44E66AECEBF233  : public CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038
{
public:
	// Rewired.Data.Mapping.CustomCalculation_CompareElementValues_ComparisonType Rewired.Data.Mapping.CustomCalculation_CompareElementValues::_comparisonType
	int32_t ____comparisonType_10;

public:
	inline static int32_t get_offset_of__comparisonType_10() { return static_cast<int32_t>(offsetof(CustomCalculation_CompareElementValues_tA210ED02BC946094240D1C2EAB44E66AECEBF233, ____comparisonType_10)); }
	inline int32_t get__comparisonType_10() const { return ____comparisonType_10; }
	inline int32_t* get_address_of__comparisonType_10() { return &____comparisonType_10; }
	inline void set__comparisonType_10(int32_t value)
	{
		____comparisonType_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCALCULATION_COMPAREELEMENTVALUES_TA210ED02BC946094240D1C2EAB44E66AECEBF233_H
#ifndef CUSTOMCALCULATION_FIRSTNONZERO_TB93097EB7844491CF4FC62A47C2CB29F1B949FCC_H
#define CUSTOMCALCULATION_FIRSTNONZERO_TB93097EB7844491CF4FC62A47C2CB29F1B949FCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.CustomCalculation_FirstNonZero
struct  CustomCalculation_FirstNonZero_tB93097EB7844491CF4FC62A47C2CB29F1B949FCC  : public CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCALCULATION_FIRSTNONZERO_TB93097EB7844491CF4FC62A47C2CB29F1B949FCC_H
#ifndef CUSTOMCALCULATION_LOGITECHGRACINGWHEELPEDALS_T1C7D5D7E69764D67E1DD654AF2157EE8D6078188_H
#define CUSTOMCALCULATION_LOGITECHGRACINGWHEELPEDALS_T1C7D5D7E69764D67E1DD654AF2157EE8D6078188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.CustomCalculation_LogitechGRacingWheelPedals
struct  CustomCalculation_LogitechGRacingWheelPedals_t1C7D5D7E69764D67E1DD654AF2157EE8D6078188  : public CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038
{
public:
	// Rewired.Data.Mapping.CustomCalculation_LogitechGRacingWheelPedals_Mode Rewired.Data.Mapping.CustomCalculation_LogitechGRacingWheelPedals::uNIbyeAgwAlSAubgsaEThHpZDsfN
	int32_t ___uNIbyeAgwAlSAubgsaEThHpZDsfN_11;

public:
	inline static int32_t get_offset_of_uNIbyeAgwAlSAubgsaEThHpZDsfN_11() { return static_cast<int32_t>(offsetof(CustomCalculation_LogitechGRacingWheelPedals_t1C7D5D7E69764D67E1DD654AF2157EE8D6078188, ___uNIbyeAgwAlSAubgsaEThHpZDsfN_11)); }
	inline int32_t get_uNIbyeAgwAlSAubgsaEThHpZDsfN_11() const { return ___uNIbyeAgwAlSAubgsaEThHpZDsfN_11; }
	inline int32_t* get_address_of_uNIbyeAgwAlSAubgsaEThHpZDsfN_11() { return &___uNIbyeAgwAlSAubgsaEThHpZDsfN_11; }
	inline void set_uNIbyeAgwAlSAubgsaEThHpZDsfN_11(int32_t value)
	{
		___uNIbyeAgwAlSAubgsaEThHpZDsfN_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCALCULATION_LOGITECHGRACINGWHEELPEDALS_T1C7D5D7E69764D67E1DD654AF2157EE8D6078188_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4100 = { sizeof (ControllerTemplateElementType_t64649CE1F95852B8211119A5D65EE4ACDA0C49D2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4100[10] = 
{
	ControllerTemplateElementType_t64649CE1F95852B8211119A5D65EE4ACDA0C49D2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4101 = { sizeof (ControllerTemplateElementSourceType_t9E3AF7FB9F717238623D389BE374AADB8C92066B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4101[3] = 
{
	ControllerTemplateElementSourceType_t9E3AF7FB9F717238623D389BE374AADB8C92066B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4102 = { sizeof (ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C), -1, sizeof(ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4102[10] = 
{
	ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C::get_offset_of__reInputId_0(),
	ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C::get_offset_of__id_1(),
	ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C::get_offset_of__templateTypeGuid_2(),
	ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C::get_offset_of__elementMaps_3(),
	ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C::get_offset_of__elementMaps_readOnly_4(),
	ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C::get_offset_of__enabled_5(),
	ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C::get_offset_of__categoryId_6(),
	ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C::get_offset_of__layoutId_7(),
	ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C::get_offset_of__sourceMapId_8(),
	ControllerTemplateMap_t5A4633F87A6A8691841F86C0584242C3CAB9666C_StaticFields::get_offset_of___idCounter_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4103 = { sizeof (PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4103[13] = 
{
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_0(),
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_1(),
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2(),
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_3(),
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_4(),
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5(),
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_6(),
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_xToauoyvRfSQtEIgZmpTaDMqGuB_7(),
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_ofSgeiiRBYviNkjEOCsJhBMuHgr_8(),
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_XLgAOntniFEhppktIYlmoPiBltW_9(),
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_juknZgAgMCiSIxOQoGMldcYZSBp_10(),
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_KUlSPPdLfEKOBhsvlUicZMIWLRE_11(),
	PlayerController_t85008A2D3BCF71A0667B37FD2A3C77505D8FBA01::get_offset_of_yQeGjgudbMdGxktFDlTRKRPYNpwO_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4104 = { sizeof (Definition_tCF297A490F8E07E75C2E1F36D9335A2625E0ED46), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4104[3] = 
{
	Definition_tCF297A490F8E07E75C2E1F36D9335A2625E0ED46::get_offset_of_enabled_0(),
	Definition_tCF297A490F8E07E75C2E1F36D9335A2625E0ED46::get_offset_of_playerId_1(),
	Definition_tCF297A490F8E07E75C2E1F36D9335A2625E0ED46::get_offset_of_elements_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4105 = { sizeof (Factory_t1C741BFACE6E45692011FFFEAD1C24E13DF9F709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4106 = { sizeof (Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9), -1, sizeof(Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4106[7] = 
{
	0,
	Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9::get_offset_of_waEhkaNJermXjlpTevBkHdlvdCD_1(),
	Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9::get_offset_of_BQHRGRekfDXytGxwUCilBePQmzf_2(),
	Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9::get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_3(),
	Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9::get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_4(),
	Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9_StaticFields::get_offset_of_cBnZNmEwJpeWkutStcRxxMWgNUb_5(),
	Element_t94B07EAAA40D9789D1ED340A48413B7827D559A9_StaticFields::get_offset_of_sUhzqBbdlaMdFyXGMgVdBJRYgTAC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4107 = { sizeof (Type_tAC1607CC90FDFD13003AF557B815C0332390D80C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4107[8] = 
{
	Type_tAC1607CC90FDFD13003AF557B815C0332390D80C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4108 = { sizeof (TypeWithSource_t18AB19C95A16F87755AB2F248CEB6A5E507E7667)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4108[5] = 
{
	TypeWithSource_t18AB19C95A16F87755AB2F248CEB6A5E507E7667::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4109 = { sizeof (CompoundTypes_t02641D39033795CA47618FFBED3164C68954742D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4109[4] = 
{
	CompoundTypes_t02641D39033795CA47618FFBED3164C68954742D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4110 = { sizeof (Definition_t438F4A94815CBEA643160ADCB2B76E9A3F053347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4110[2] = 
{
	Definition_t438F4A94815CBEA643160ADCB2B76E9A3F053347::get_offset_of_enabled_0(),
	Definition_t438F4A94815CBEA643160ADCB2B76E9A3F053347::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4111 = { sizeof (dvlZSFCjnFxtoufPKLchsiMXNQc_tEB40E5A227CB3E8F0D35981E8A306F32243E416E)+ sizeof (RuntimeObject), sizeof(dvlZSFCjnFxtoufPKLchsiMXNQc_tEB40E5A227CB3E8F0D35981E8A306F32243E416E ), 0, 0 };
extern const int32_t g_FieldOffsetTable4111[3] = 
{
	dvlZSFCjnFxtoufPKLchsiMXNQc_tEB40E5A227CB3E8F0D35981E8A306F32243E416E::get_offset_of_EDrPGdciFGATRHQnPSEzqJMwhNu_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	dvlZSFCjnFxtoufPKLchsiMXNQc_tEB40E5A227CB3E8F0D35981E8A306F32243E416E::get_offset_of_BrVbOYLWwCQWNcRRlBBgwzbuWsi_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	dvlZSFCjnFxtoufPKLchsiMXNQc_tEB40E5A227CB3E8F0D35981E8A306F32243E416E::get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4112 = { sizeof (ElementWithSource_t69752C9D210EDEE80C6FD0F291364B0681FDB4ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4112[2] = 
{
	0,
	ElementWithSource_t69752C9D210EDEE80C6FD0F291364B0681FDB4ED::get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4113 = { sizeof (Definition_t18A5B017F634EC0B2DB9B29ADB9294C16C71F6BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4113[1] = 
{
	Definition_t18A5B017F634EC0B2DB9B29ADB9294C16C71F6BA::get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4114 = { sizeof (Axis_t99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4114[5] = 
{
	0,
	0,
	Axis_t99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A::get_offset_of_IiRfbnSvbaGlqxPQfWikqkfabCN_11(),
	Axis_t99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A::get_offset_of_KoJFJTyAFTzWKOYBGqPtFhoxzoa_12(),
	Axis_t99918A07F9B5BC9E90B7D89FD34181A6D8B06A2A::get_offset_of_inaxtTsfdEOFKtbeAbFuayoHJCRB_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4115 = { sizeof (Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4115[2] = 
{
	Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9::get_offset_of_coordinateMode_3(),
	Definition_t10130017A93C5FDA0255FF4008B240CF1BB7A7A9::get_offset_of_absoluteToRelativeSensitivity_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4116 = { sizeof (MouseAxis_t60D6DDDC2AAF95DB273A010BE949D8365076A8DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4116[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4117 = { sizeof (Definition_t0C037D21BC8BDFEDEA1FA85DE2E3CF081F3EB242), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4118 = { sizeof (CompoundElement_tA56F63F47F096A626ADF63799EEF977B5E4E632E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4118[1] = 
{
	CompoundElement_tA56F63F47F096A626ADF63799EEF977B5E4E632E::get_offset_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4119 = { sizeof (Definition_t3A8A60468526C12F075318D337B6FA439005CA39), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4120 = { sizeof (Axis2D_tA62A3EAF575DEE91FF86DCEE3B2075AFA54705B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4120[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4121 = { sizeof (Definition_tABC9A67DA4AE30F7D1070DD41541C2F97780F86B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4121[2] = 
{
	Definition_tABC9A67DA4AE30F7D1070DD41541C2F97780F86B::get_offset_of_kOXOgojqZxOWXJWVYHpqCqJXQBb_2(),
	Definition_tABC9A67DA4AE30F7D1070DD41541C2F97780F86B::get_offset_of_qMQJsNmTJihTirHMJKUghCLgmDa_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4122 = { sizeof (MouseAxis2D_tDA0430D22FE9F3F7D591E689F3403A018214C41F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4123 = { sizeof (Definition_t46F903B3012455FA952C56CCD90032CCC23D2E73), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4124 = { sizeof (Button_t66C2A534EFD269FA571DA08D484624525061CB9A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4125 = { sizeof (Definition_t41DF50915C398BCAAB784454ED9B1D7CCFDED5DA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4126 = { sizeof (MouseWheel_t96F0FF773D68A8E93DE6E731AF4A45FA4BD9845A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4127 = { sizeof (Definition_tD95B80BE030BC14FA5244466336EEAFE33A2F4C9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4128 = { sizeof (MouseWheelAxis_tE34ABA4811B899C43F609891C924BD3797CE21A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4128[6] = 
{
	0,
	0,
	0,
	MouseWheelAxis_tE34ABA4811B899C43F609891C924BD3797CE21A1::get_offset_of_UpkvflgKnqIYwagopCDVqQIjcSUd_17(),
	MouseWheelAxis_tE34ABA4811B899C43F609891C924BD3797CE21A1::get_offset_of_eYPMVuyFLSfzqcuiinQTgcTexKRJ_18(),
	MouseWheelAxis_tE34ABA4811B899C43F609891C924BD3797CE21A1::get_offset_of_mvKHSWNtYxtrXAPVpClHLUktCzV_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4129 = { sizeof (Definition_t8556A31E68641FE8480951FC1733CBA187B0C7F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4129[1] = 
{
	Definition_t8556A31E68641FE8480951FC1733CBA187B0C7F7::get_offset_of_repeatRate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4130 = { sizeof (PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D), -1, sizeof(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4130[36] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D_StaticFields::get_offset_of_dWDDIjnMxVawYKNHxusdBGeHHGfa_28(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_bkySQZIzPEjvMtqmVQIflsRZLcC_29(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_ZdUUEykXEadJhwdAvxRLuFOjdlo_30(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_xSdFlWJnlQKpmDxLjLiJVHILHHQb_31(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_zakNtTwPoqfQTgRmwJMlFSKDayyY_32(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_TGDHdlRSoTbnNveTXeJyCfhOQHVw_33(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_ASvelPdARqNeHoGFRujyrySfHzZ_34(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_vIRfZylUMCaIJegxbePAceWChvkz_35(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_ygfOxayKzOSmtRmOHCLqarVOYDQN_36(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_zobOISDZaPowFanRheDQywSrYck_37(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_DWbngFJqKIdtWDGFHVEKwNRZdLxe_38(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_NraenLBVpMrWzcDEoclzjffxsOC_39(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_YfaIpXgcpqghFASYjZjjzMtnuoBK_40(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_EANPkZSmkxVcEcmVTARGUefVdrP_41(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_BePwMGWomgmSkblQmXdzPGicbYd_42(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_zTdQBKFzjclSAMvLChomKafLIaI_43(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_ufodZjARVBvOArvlATqqfpzRNEQ_44(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_hXyTkhcAPGjVzSrSRDwkAYLwdmCa_45(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D::get_offset_of_EsWfQirqStgFmlaejyHRdJihNXP_46(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D_StaticFields::get_offset_of_APrqfxcqXfOigsaYgstdZUzQbTD_47(),
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D_StaticFields::get_offset_of_wzuTgIXkBigvyhwmUgzxbJRwaLDH_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4131 = { sizeof (Definition_tF82F454FDB865FB8E647E7C9783284702FC65165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4131[5] = 
{
	Definition_tF82F454FDB865FB8E647E7C9783284702FC65165::get_offset_of_defaultToCenter_3(),
	Definition_tF82F454FDB865FB8E647E7C9783284702FC65165::get_offset_of_movementArea_4(),
	Definition_tF82F454FDB865FB8E647E7C9783284702FC65165::get_offset_of_movementAreaUnit_5(),
	Definition_tF82F454FDB865FB8E647E7C9783284702FC65165::get_offset_of_pointerSpeed_6(),
	Definition_tF82F454FDB865FB8E647E7C9783284702FC65165::get_offset_of_useHardwarePointerPosition_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4132 = { sizeof (Factory_tE6CF86402AE42CF34950BD2785B1F852FDFAC2CB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4133 = { sizeof (MovementAreaUnit_t25F2042B52F33CFAE132EB793F341081375940C1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4133[3] = 
{
	MovementAreaUnit_t25F2042B52F33CFAE132EB793F341081375940C1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4134 = { sizeof (CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4134[7] = 
{
	CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24::get_offset_of_ytEpmEgckhdtBXUMvGmTeXChJTg_0(),
	CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24::get_offset_of_jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1(),
	CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24::get_offset_of_GlRIkUvAeSsFrGZYYfdWdAiahHaH_2(),
	CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24::get_offset_of_OxqcyLtiNnnQZjHLJHrubHVLORyY_3(),
	CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24::get_offset_of_dSCdvTfRJafJNqFqCvsZWnmZGkuH_4(),
	CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24::get_offset_of_WtXxfgwPkvBzrhvGFrBKnZbGqaC_5(),
	CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4135 = { sizeof (Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4135[3] = 
{
	Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B::get_offset_of__isConnected_0(),
	Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B::get_offset_of__deviceName_1(),
	Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B::get_offset_of__customName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4136 = { sizeof (Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4136[8] = 
{
	Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09::get_offset_of_kNCtRZYNhfpgNEjWCvYWWaJdsPw_3(),
	Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09::get_offset_of_pVMgxVlhOWBbMBjIelvXUbEMRHTh_4(),
	Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09::get_offset_of_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5(),
	Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09::get_offset_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_6(),
	Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09::get_offset_of_ofSgeiiRBYviNkjEOCsJhBMuHgr_7(),
	Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09::get_offset_of_xToauoyvRfSQtEIgZmpTaDMqGuB_8(),
	Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09::get_offset_of_OWbAvMhWYnpGDbwOiHPyDnDXREx_9(),
	Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09::get_offset_of_keBYOCobzoABWhYylPhjApVSLXnN_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4137 = { sizeof (Element_t67DF1A6C6914A798218BD19B5517004855DF149F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4138 = { sizeof (Axis_t62C4BB066E0FE5BEBABD43DF24B48B56ED399FC9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4138[1] = 
{
	Axis_t62C4BB066E0FE5BEBABD43DF24B48B56ED399FC9::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4139 = { sizeof (Button_t9063D0415294673AB9137F72C897049AE5DDE41C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4139[1] = 
{
	Button_t9063D0415294673AB9137F72C897049AE5DDE41C::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4140 = { sizeof (ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4140[54] = 
{
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_updateLoop_0(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_alwaysUseUnityInput_1(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_windowsStandalonePrimaryInputSource_2(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_osx_primaryInputSource_3(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_linux_primaryInputSource_4(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_windowsUWP_primaryInputSource_5(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_xboxOne_primaryInputSource_6(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_ps4_primaryInputSource_7(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_webGL_primaryInputSource_8(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_useXInput_9(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_useNativeMouse_10(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_useEnhancedDeviceSupport_11(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_windowsStandalone_useSteamRawInputControllerWorkaround_12(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_osxStandalone_useEnhancedDeviceSupport_13(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_android_supportUnknownGamepads_14(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_ps4_assignJoysticksByPS4JoyId_15(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_useSteamControllerSupport_16(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_logToScreen_17(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_runInEditMode_18(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_allowInputInEditorSceneView_19(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_windowsStandalone_20(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_linuxStandalone_21(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_osxStandalone_22(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_windows8Store_23(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_windowsUWP_24(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_iOS_25(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_tvOS_26(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_android_27(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_ps3_28(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_ps4_29(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_psVita_30(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_xbox360_31(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_xboxOne_32(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_wii_33(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_wiiu_34(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_switch_35(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_webGL_36(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_platformVars_unknown_37(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_maxJoysticksPerPlayer_38(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_autoAssignJoysticks_39(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_assignJoysticksToPlayingPlayersOnly_40(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_distributeJoysticksEvenly_41(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_reassignJoystickToPreviousOwnerOnReconnect_42(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_defaultJoystickAxis2DDeadZoneType_43(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_defaultJoystickAxis2DSensitivityType_44(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_defaultAxisSensitivityType_45(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_force4WayHats_46(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_throttleCalibrationMode_47(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_activateActionButtonsOnNegativeValue_48(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_deferControllerConnectedEventsOnStart_49(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_logLevel_50(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of_editorSettings_51(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of___platformVarsDict_52(),
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41::get_offset_of___getSetPlatformVariableDict_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4141 = { sizeof (PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4141[2] = 
{
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA::get_offset_of_disableKeyboard_0(),
	PlatformVars_t9E023462BBB87694D35C6951AF767B22A20FEFFA::get_offset_of_ignoreInputWhenAppNotInFocus_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4142 = { sizeof (PlatformVars_WindowsStandalone_t1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4142[2] = 
{
	PlatformVars_WindowsStandalone_t1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A::get_offset_of_useNativeKeyboard_2(),
	PlatformVars_WindowsStandalone_t1FC20AA12CA7CEA4D6EF0D4CE37A13D87448856A::get_offset_of_joystickRefreshRate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4143 = { sizeof (PlatformVars_WindowsUWP_t3F16819E5BF554DD60A4D1E2464E38C88A9D73D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4143[2] = 
{
	PlatformVars_WindowsUWP_t3F16819E5BF554DD60A4D1E2464E38C88A9D73D8::get_offset_of_useGamepadAPI_2(),
	PlatformVars_WindowsUWP_t3F16819E5BF554DD60A4D1E2464E38C88A9D73D8::get_offset_of_useHIDAPI_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4144 = { sizeof (EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4144[25] = 
{
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_useParentClass_0(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_parentClassName_1(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_useNamespace_2(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_namespace_3(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_actions_4(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_actionsClassName_5(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_actionsIncludeActionCategory_6(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_actionsCreateClassesForActionCategories_7(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_mapCategories_8(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_mapCategoriesClassName_9(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_layouts_10(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_layoutsClassName_11(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_players_12(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_playersClassName_13(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_inputBehaviors_14(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_inputBehaviorsClassName_15(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_customControllers_16(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_customControllersClassName_17(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_customControllersAxesClassName_18(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_customControllersButtonsClassName_19(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_layoutManagerRuleSets_20(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_layoutManagerRuleSetsClassName_21(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_mapEnablerRuleSets_22(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_mapEnablerRuleSetsClassName_23(),
	EditorVars_t2288ACEE73A33C9572CBA4CD0A34DD00A3D7F8DF::get_offset_of_exportConsts_allCapsConstantNames_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4145 = { sizeof (EtiwYEyEvxhGdDZwPhVpQNWkBYX_t9ED19CF8345F9FD567470585E1F5E2F949935321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4145[2] = 
{
	EtiwYEyEvxhGdDZwPhVpQNWkBYX_t9ED19CF8345F9FD567470585E1F5E2F949935321::get_offset_of_ImAEWBFqgIbvkLGhFYjFCwIBMTg_0(),
	EtiwYEyEvxhGdDZwPhVpQNWkBYX_t9ED19CF8345F9FD567470585E1F5E2F949935321::get_offset_of_uxGkAcyjrmYqmmrwmUKivgesJVl_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4146 = { sizeof (DadcqoCjLXmahnTWBdNQJAWfNLqB_tEBDD11410F726E43CEAB433D38E6283D8AC4670B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4146[2] = 
{
	DadcqoCjLXmahnTWBdNQJAWfNLqB_tEBDD11410F726E43CEAB433D38E6283D8AC4670B::get_offset_of_ImAEWBFqgIbvkLGhFYjFCwIBMTg_0(),
	DadcqoCjLXmahnTWBdNQJAWfNLqB_tEBDD11410F726E43CEAB433D38E6283D8AC4670B::get_offset_of_WjjfnLCmwGPtFyAxWAdOloiFApm_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4147 = { sizeof (AllPlatformVar_t7BEC1C3A2BD20549C4FAB7A17E51CBE9CB6006B3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4147[3] = 
{
	AllPlatformVar_t7BEC1C3A2BD20549C4FAB7A17E51CBE9CB6006B3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4148 = { sizeof (ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4148[4] = 
{
	ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5::get_offset_of_defaultHardwareJoystickMap_4(),
	ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5::get_offset_of_hardwareJoystickMaps_5(),
	ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5::get_offset_of_joystickTemplates_6(),
	ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5::get_offset_of_kvSBqhmkckXimqKmlOMbbiFcgwF_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4149 = { sizeof (CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4149[8] = 
{
	CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E::get_offset_of__name_0(),
	CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E::get_offset_of__descriptiveName_1(),
	CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E::get_offset_of__id_2(),
	CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E::get_offset_of__typeGuidString_3(),
	CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E::get_offset_of__elementIdentifiers_4(),
	CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E::get_offset_of__axes_5(),
	CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E::get_offset_of__buttons_6(),
	CustomController_Editor_t3C1F05B017977306A26C6D50DCBC737FD4B7BD1E::get_offset_of__elementIdentifierIdCounter_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4150 = { sizeof (Element_t4C80356DECB9FF4357706FB02B4C85C71AC29D12), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4150[2] = 
{
	Element_t4C80356DECB9FF4357706FB02B4C85C71AC29D12::get_offset_of_elementIdentifierId_0(),
	Element_t4C80356DECB9FF4357706FB02B4C85C71AC29D12::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4151 = { sizeof (Button_t037E5D372364FAE233617DE537143C0FF59B05BA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4152 = { sizeof (Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4152[11] = 
{
	Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E::get_offset_of_range_2(),
	Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E::get_offset_of_invert_3(),
	Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E::get_offset_of_deadZone_4(),
	Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E::get_offset_of_zero_5(),
	Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E::get_offset_of_min_6(),
	Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E::get_offset_of_max_7(),
	Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E::get_offset_of_doNotCalibrateRange_8(),
	Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E::get_offset_of_sensitivityType_9(),
	Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E::get_offset_of_sensitivity_10(),
	Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E::get_offset_of_sensitivityCurve_11(),
	Axis_t504AF754E9EB12A17FB176318BADD88841A38B7E::get_offset_of_axisInfo_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4153 = { sizeof (HFfTEQVrDyKNPNJmkTqlxcUKadL_t4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4153[5] = 
{
	HFfTEQVrDyKNPNJmkTqlxcUKadL_t4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	HFfTEQVrDyKNPNJmkTqlxcUKadL_t4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	HFfTEQVrDyKNPNJmkTqlxcUKadL_t4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	HFfTEQVrDyKNPNJmkTqlxcUKadL_t4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	HFfTEQVrDyKNPNJmkTqlxcUKadL_t4DA69928A38A3CBE9460BFF443DBF0D1E0A48FA1::get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4154 = { sizeof (EditorSettings_tAF090EB782C1024E7E0A8D475184757BC1A5CA2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4154[5] = 
{
	EditorSettings_tAF090EB782C1024E7E0A8D475184757BC1A5CA2A::get_offset_of_programVersion1_4(),
	EditorSettings_tAF090EB782C1024E7E0A8D475184757BC1A5CA2A::get_offset_of_programVersion2_5(),
	EditorSettings_tAF090EB782C1024E7E0A8D475184757BC1A5CA2A::get_offset_of_programVersion3_6(),
	EditorSettings_tAF090EB782C1024E7E0A8D475184757BC1A5CA2A::get_offset_of_programVersion4_7(),
	EditorSettings_tAF090EB782C1024E7E0A8D475184757BC1A5CA2A::get_offset_of_dataVersion_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4155 = { sizeof (ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4155[1] = 
{
	ActionCategoryMap_t7E746A715F5EA1B7B0BFCC48B29000FCB5F9B20F::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4156 = { sizeof (Entry_t87C36EB768B9C1EA06D2C2FC073B650711241DAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4156[2] = 
{
	Entry_t87C36EB768B9C1EA06D2C2FC073B650711241DAC::get_offset_of_categoryId_0(),
	Entry_t87C36EB768B9C1EA06D2C2FC073B650711241DAC::get_offset_of_actionIds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4157 = { sizeof (MJcVRwenrdtcMlnjDIdvaiCKGxDg_t73CF85D3572153DF8DB71136ECF91EA684355ABA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4157[5] = 
{
	MJcVRwenrdtcMlnjDIdvaiCKGxDg_t73CF85D3572153DF8DB71136ECF91EA684355ABA::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	MJcVRwenrdtcMlnjDIdvaiCKGxDg_t73CF85D3572153DF8DB71136ECF91EA684355ABA::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	MJcVRwenrdtcMlnjDIdvaiCKGxDg_t73CF85D3572153DF8DB71136ECF91EA684355ABA::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	MJcVRwenrdtcMlnjDIdvaiCKGxDg_t73CF85D3572153DF8DB71136ECF91EA684355ABA::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	MJcVRwenrdtcMlnjDIdvaiCKGxDg_t73CF85D3572153DF8DB71136ECF91EA684355ABA::get_offset_of_CXbAsCcpaRocwlxePSqDsZRxJTuA_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4158 = { sizeof (uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4158[9] = 
{
	uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F::get_offset_of_OoFswlQbqBhHqGOCooflwYCYmYWF_4(),
	uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F::get_offset_of_pyCASHftKaJjucJBlmeeWQrdKuRk_5(),
	uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F::get_offset_of_bFEubZafxTexBBhbDuHMMulYJfbH_6(),
	uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F::get_offset_of_IdhQMvrODBHtZqpemtLdtJCrDcUe_7(),
	uEfGdvywKKBmqZHtBdfHTGtDMzI_tCDF48FF5DF4D381EDCF25557BBEB8A27C7950A3F::get_offset_of_zCSdpqWqddovuMgwXAspBmdHeqjU_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4159 = { sizeof (SerializedMethod_t620A6C7138FEC59B91B3CF7B58C47B50C50B082E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4159[5] = 
{
	0,
	SerializedMethod_t620A6C7138FEC59B91B3CF7B58C47B50C50B082E::get_offset_of_vclPlLKqlxHWseSEJOiyCtEkWhpb_5(),
	SerializedMethod_t620A6C7138FEC59B91B3CF7B58C47B50C50B082E::get_offset_of__data_6(),
	SerializedMethod_t620A6C7138FEC59B91B3CF7B58C47B50C50B082E::get_offset_of__result_7(),
	SerializedMethod_t620A6C7138FEC59B91B3CF7B58C47B50C50B082E::get_offset_of__resultIsValid_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4160 = { sizeof (CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4161 = { sizeof (CustomCalculation_Accelerometer_tD6A0A03F922F3200D9E9C7ED0D8DAFF2590AD79E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4161[3] = 
{
	CustomCalculation_Accelerometer_tD6A0A03F922F3200D9E9C7ED0D8DAFF2590AD79E::get_offset_of__calculationType_9(),
	CustomCalculation_Accelerometer_tD6A0A03F922F3200D9E9C7ED0D8DAFF2590AD79E::get_offset_of__inputType_10(),
	CustomCalculation_Accelerometer_tD6A0A03F922F3200D9E9C7ED0D8DAFF2590AD79E::get_offset_of__outputType_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4162 = { sizeof (CalculationType_t9EE6947FA506F4C2E9393F694C0395D4FA172D2D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4162[3] = 
{
	CalculationType_t9EE6947FA506F4C2E9393F694C0395D4FA172D2D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4163 = { sizeof (OutputType_t5373E3E8E93DFB10C4A5039895A10EB56D77A660)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4163[3] = 
{
	OutputType_t5373E3E8E93DFB10C4A5039895A10EB56D77A660::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4164 = { sizeof (InputType_tE659D1D29467BD6075DF11BB007F36E08C7F1890)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4164[4] = 
{
	InputType_tE659D1D29467BD6075DF11BB007F36E08C7F1890::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4165 = { sizeof (CustomCalculation_CompareElementValues_tA210ED02BC946094240D1C2EAB44E66AECEBF233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4165[2] = 
{
	0,
	CustomCalculation_CompareElementValues_tA210ED02BC946094240D1C2EAB44E66AECEBF233::get_offset_of__comparisonType_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4166 = { sizeof (ComparisonType_t513ED3BC6BF03CCF7D6DBE670F8C16A90982C6C1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4166[5] = 
{
	ComparisonType_t513ED3BC6BF03CCF7D6DBE670F8C16A90982C6C1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4167 = { sizeof (CustomCalculation_FirstNonZero_tB93097EB7844491CF4FC62A47C2CB29F1B949FCC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4167[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4168 = { sizeof (CustomCalculation_LogitechGRacingWheelPedals_t1C7D5D7E69764D67E1DD654AF2157EE8D6078188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4168[3] = 
{
	0,
	0,
	CustomCalculation_LogitechGRacingWheelPedals_t1C7D5D7E69764D67E1DD654AF2157EE8D6078188::get_offset_of_uNIbyeAgwAlSAubgsaEThHpZDsfN_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4169 = { sizeof (Mode_t67DA2117A0FBBBACA752DD1E441832AF9B10D952)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4169[3] = 
{
	Mode_t67DA2117A0FBBBACA752DD1E441832AF9B10D952::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4170 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4171 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4172 = { sizeof (HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4172[44] = 
{
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_controllerName_4(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_editorControllerName_5(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_description_6(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_controllerGuid_7(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_templateGuids_8(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_hideInLists_9(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_joystickTypes_10(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_elementIdentifiers_11(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_compoundElements_12(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_directInput_13(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_rawInput_14(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_xInput_15(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_osx_16(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_linux_17(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_windowsUWP_18(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_Windows_19(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_WindowsUWP_20(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_OSX_21(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_Linux_22(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_Linux_PreConfigured_23(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_Android_24(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_iOS_25(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_Blackberry_26(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_WindowsPhone8_27(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_XBox360_28(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_XBoxOne_29(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_PS3_30(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_PS4_31(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_PSM_32(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_PSVita_33(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_Wii_34(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_WiiU_35(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_AmazonFireTV_36(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_fallback_RazerForgeTV_37(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_webGL_38(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_ouya_39(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_xboxOne_40(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_ps4_41(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_nintendoSwitch_42(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_internalDriver_43(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_sdl2_Linux_44(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_sdl2_Windows_45(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_sdl2_OSX_46(),
	HardwareJoystickMap_tEC3597190BA5C54AF4F8849D274BE558ADB284FB::get_offset_of_elementIdentifierIdCounter_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4173 = { sizeof (Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4173[1] = 
{
	Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789::get_offset_of_description_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4174 = { sizeof (rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4174[6] = 
{
	rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE::get_offset_of_DNPeIdvKJFFEBSUOvaipMiRUthG_4(),
	rwbYTEfAGbvRLOMJbkPFTZWRFcD_tB1A300544AD09F3B8EB122374ADFC6161066E5BE::get_offset_of_sllTPBsbpmDyVEIgPNnVpQiDoIoV_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4175 = { sizeof (Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4176 = { sizeof (MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4176[4] = 
{
	MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065::get_offset_of_axisCount_0(),
	MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065::get_offset_of_buttonCount_1(),
	MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065::get_offset_of_disabled_2(),
	MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065::get_offset_of_tag_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4177 = { sizeof (ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4177[2] = 
{
	ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC::get_offset_of_axisCount_0(),
	ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC::get_offset_of_buttonCount_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4178 = { sizeof (CompoundElement_tC9DDF922221721DEC6AF01C86AD61E299430E978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4178[3] = 
{
	CompoundElement_tC9DDF922221721DEC6AF01C86AD61E299430E978::get_offset_of_type_0(),
	CompoundElement_tC9DDF922221721DEC6AF01C86AD61E299430E978::get_offset_of_elementIdentifier_1(),
	CompoundElement_tC9DDF922221721DEC6AF01C86AD61E299430E978::get_offset_of_componentElementIdentifiers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4179 = { sizeof (VidPid_tD48EA2DDFAF98EAD7C13F962D730E15A6D13D6B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4179[2] = 
{
	VidPid_tD48EA2DDFAF98EAD7C13F962D730E15A6D13D6B2::get_offset_of_vendorId_0(),
	VidPid_tD48EA2DDFAF98EAD7C13F962D730E15A6D13D6B2::get_offset_of_productId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4180 = { sizeof (AxisCalibrationInfoEntry_t84CE6AFB327D1C49A7AD0ED2B62289CC94DBC239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4180[2] = 
{
	AxisCalibrationInfoEntry_t84CE6AFB327D1C49A7AD0ED2B62289CC94DBC239::get_offset_of_key_0(),
	AxisCalibrationInfoEntry_t84CE6AFB327D1C49A7AD0ED2B62289CC94DBC239::get_offset_of_calibration_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4181 = { sizeof (Platform_RawOrDirectInput_t66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4181[1] = 
{
	Platform_RawOrDirectInput_t66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC::get_offset_of_matchingCriteria_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4182 = { sizeof (MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4182[7] = 
{
	MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3::get_offset_of_hatCount_4(),
	MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3::get_offset_of_alternateElementCounts_5(),
	MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3::get_offset_of_productName_useRegex_6(),
	MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3::get_offset_of_productName_7(),
	MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3::get_offset_of_productGUID_8(),
	MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3::get_offset_of_productId_9(),
	MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3::get_offset_of_deviceType_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4183 = { sizeof (ElementCount_tA6EACD850F29B9F522B95E98217FCE9EF4B77A18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4183[1] = 
{
	ElementCount_tA6EACD850F29B9F522B95E98217FCE9EF4B77A18::get_offset_of_hatCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4184 = { sizeof (Elements_Platform_Base_tAA1373033B7FB6EA7E7C0298F2380C7D3EE67CE5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4185 = { sizeof (CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4185[11] = 
{
	CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32::get_offset_of_sourceType_0(),
	CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32::get_offset_of_sourceAxis_1(),
	CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32::get_offset_of_sourceButton_2(),
	CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32::get_offset_of_sourceOtherAxis_3(),
	CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32::get_offset_of_sourceAxisRange_4(),
	CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32::get_offset_of_axisDeadZone_5(),
	CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32::get_offset_of_invert_6(),
	CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32::get_offset_of_axisCalibrationType_7(),
	CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32::get_offset_of_axisZero_8(),
	CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32::get_offset_of_axisMin_9(),
	CustomCalculationSourceData_t5A88147FA27E7351766E5D928E4C8D384C48DA32::get_offset_of_axisMax_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4186 = { sizeof (Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4186[2] = 
{
	Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B::get_offset_of_customCalculation_0(),
	Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B::get_offset_of_customCalculationSourceData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4187 = { sizeof (Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4187[14] = 
{
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_elementIdentifier_2(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_sourceType_3(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_sourceButton_4(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_sourceAxis_5(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_sourceAxisPole_6(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_axisDeadZone_7(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_sourceHat_8(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_sourceHatType_9(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_sourceHatDirection_10(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_requireMultipleButtons_11(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_requiredButtons_12(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_ignoreIfButtonsActive_13(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_ignoreIfButtonsActiveButtons_14(),
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024::get_offset_of_buttonInfo_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4188 = { sizeof (Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4188[17] = 
{
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_elementIdentifier_2(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_sourceType_3(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_sourceAxis_4(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_sourceAxisRange_5(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_invert_6(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_axisDeadZone_7(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_calibrateAxis_8(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_axisZero_9(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_axisMin_10(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_axisMax_11(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_axisInfo_12(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_alternateCalibrations_13(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_sourceButton_14(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_buttonAxisContribution_15(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_sourceHat_16(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_sourceHatDirection_17(),
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623::get_offset_of_sourceHatRange_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4189 = { sizeof (DeviceType_tA1B2CDA0C670F6AF83BC2ECFE58D81275EF2B6DC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4189[14] = 
{
	DeviceType_tA1B2CDA0C670F6AF83BC2ECFE58D81275EF2B6DC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4190 = { sizeof (Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4190[1] = 
{
	Platform_DirectInput_Base_t5F001050D12A98E75D98A40FACDFAF46998FFC5B::get_offset_of_elements_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4191 = { sizeof (Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4191[2] = 
{
	Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7::get_offset_of_axes_0(),
	Elements_t3F40D6D8530AD00F16775409ED3EA957C3E150B7::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4192 = { sizeof (uIsVFgtXfKodcIgBUCwYxSoRaRFh_tD0A2A74AE5E4500854EF6E705670E7E18C59A232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4192[5] = 
{
	uIsVFgtXfKodcIgBUCwYxSoRaRFh_tD0A2A74AE5E4500854EF6E705670E7E18C59A232::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	uIsVFgtXfKodcIgBUCwYxSoRaRFh_tD0A2A74AE5E4500854EF6E705670E7E18C59A232::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	uIsVFgtXfKodcIgBUCwYxSoRaRFh_tD0A2A74AE5E4500854EF6E705670E7E18C59A232::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	uIsVFgtXfKodcIgBUCwYxSoRaRFh_tD0A2A74AE5E4500854EF6E705670E7E18C59A232::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	uIsVFgtXfKodcIgBUCwYxSoRaRFh_tD0A2A74AE5E4500854EF6E705670E7E18C59A232::get_offset_of_eLAERvBUfLIWiIfMBoNRaRMTEvp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4193 = { sizeof (WqmaKTePtijIvhEbVutMbAlIErx_t7A72BA7412CF7C38E78EE69AB15373C0AE8B9777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4193[5] = 
{
	WqmaKTePtijIvhEbVutMbAlIErx_t7A72BA7412CF7C38E78EE69AB15373C0AE8B9777::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	WqmaKTePtijIvhEbVutMbAlIErx_t7A72BA7412CF7C38E78EE69AB15373C0AE8B9777::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	WqmaKTePtijIvhEbVutMbAlIErx_t7A72BA7412CF7C38E78EE69AB15373C0AE8B9777::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	WqmaKTePtijIvhEbVutMbAlIErx_t7A72BA7412CF7C38E78EE69AB15373C0AE8B9777::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	WqmaKTePtijIvhEbVutMbAlIErx_t7A72BA7412CF7C38E78EE69AB15373C0AE8B9777::get_offset_of_ZwruVSrOjQwKqPazqaarhnUAxKPR_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4194 = { sizeof (Button_tB6F1D153DD9EDACC05ED5C32D8D1EF258B329266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4195 = { sizeof (Axis_tDF7D3B4A808B8EC21B426BA447F16A9656C5E80C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4196 = { sizeof (DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4196[6] = 
{
	DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630::get_offset_of_cHJcLuiUAnRXodxmLILWuKXgXcOO_4(),
	DArhVSBIuOHHKRdyDFgmiMakZSDI_t35D60AD0E7CEBD5BF12FC36C5A6BF9C90132E630::get_offset_of_ZRfecGkoINAkZyMuJGesYKGoGOmG_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4197 = { sizeof (FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4197[6] = 
{
	FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651::get_offset_of_rbzmaknNVQXmtceEExhcrkqjdXx_4(),
	FykVHJSRsAldQmIZEhJHYSqmVsz_tA95DB6008D0DFB3D3E01C0DAEA0DC457FC488651::get_offset_of_bbylexxHkiPVNjmmyFKFxUlOenA_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4198 = { sizeof (Platform_DirectInput_t8384F48EB39207EF4B476BF85EEE95C48F113399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4198[1] = 
{
	Platform_DirectInput_t8384F48EB39207EF4B476BF85EEE95C48F113399::get_offset_of_variants_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4199 = { sizeof (Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4199[1] = 
{
	Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB::get_offset_of_elements_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
