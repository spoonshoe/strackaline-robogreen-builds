﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// EaCqncmrbxTySfotaFWfqDQVLIS/FOBdOrKrxGfXvkOuUmdtNzedDCs/VsbAISQkdJoqOdmXONtnUYGDNdA[]
struct VsbAISQkdJoqOdmXONtnUYGDNdAU5BU5D_t659F3F12A92C612FBA1B07A8AB7D0F8058CB65F5;
// Rewired.Utils.Classes.Data.ADictionary`2<System.Type,System.Collections.Generic.List`1<System.Collections.IList>>
struct ADictionary_2_t18CC0D77B50ADC14D160940E03DC5A92EF759931;
// Rewired.Utils.Classes.Data.ADictionary`2<System.Type,System.Collections.Generic.List`1<System.Object>>
struct ADictionary_2_t0634D596F2E2DFE76DA880E0F73742FC7D6EDE12;
// Rewired.Utils.Classes.Data.IndexedDictionary`2<System.String,Rewired.Utils.Classes.Data.SerializedObject/Entry>
struct IndexedDictionary_2_tEB2A1CD4B3B24D918E51CE4813AC81DDB5A46D27;
// Rewired.Utils.Classes.Data.NativeBuffer
struct NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11;
// Rewired.Utils.Classes.Data.SerializedObject/XmlDocument/Element
struct Element_tB976891F5774B30930B5751F45DAC9A21D669254;
// Rewired.Utils.Classes.Data.SerializedObject/XmlInfo
struct XmlInfo_t1D129B900590213A92E045C337F63C7A2D1A2FBB;
// Rewired.Utils.GUITools/JYlRBGumenhRDRKYwPQSnpRSbox
struct JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8;
// System.Action`1<System.Exception>
struct Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC;
// System.Action`2<System.Object,System.Action>
struct Action_2_tDDE800AB741C3BECFFDA85992A2EF0436DF8604E;
// System.Action`2<System.Text.StringBuilder,System.Object>
struct Action_2_t0C40F2023DD772A0F3511D237BE444DEC1344C44;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,erAqfwCNbAAvnTADjtOsmRVrVxW/XxmVNqBKBGNcpIbRBzMhemMCRnu>
struct Dictionary_2_t96F61893C984671CB734D4C257EF7F9458A429E5;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.FieldInfo>>
struct Dictionary_2_tF19B322732A3F59097E35488888753E75A98508B;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.PropertyInfo>>
struct Dictionary_2_t85E880FBBD1F7D6DF932C42144E9B40E2B7FCFE9;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Rewired.Utils.Classes.Data.SerializedObject/Entry>>
struct IEnumerator_1_t567DE1B197A1F34C9B8CECD8DCF312F0532FD84D;
// System.Collections.Generic.List`1<Rewired.Utils.Classes.Data.SerializedObject/XmlDocument/Element>
struct List_1_tD303BF18C474D111E3A63E9BCED1882E91F62D15;
// System.Collections.Generic.List`1<Rewired.Utils.Classes.Data.SerializedObject/XmlInfo/XmlAttribute>
struct List_1_t9671A9C9A8A89A10CA13CEE5E706700AD7AF23F4;
// System.Collections.Generic.List`1<Rewired.Utils.SafeDelegate`1/XHqyovzaINqHymgNffvhSMRXkfh<System.Action>>
struct List_1_t7A77DFF4E032CCBB8F2A4E9630724EFEFCF48417;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.String>>
struct Stack_1_t4EF598C6952C314843BCAE9E9E1AC9B7262E7999;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D;
// System.Func`2<System.Reflection.FieldInfo,System.String>
struct Func_2_t1EB5033BF4C5996F1CBD82840960676EBDD8049C;
// System.Func`2<System.Reflection.PropertyInfo,System.Boolean>
struct Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C;
// System.Func`2<System.Reflection.PropertyInfo,System.String>
struct Func_2_t1E4225A498AC4D60FC503C2A063C30CA478ACF5A;
// System.Func`3<Rewired.Utils.Classes.Data.Float2x,Rewired.Utils.Classes.Data.Float2x,Rewired.Utils.Classes.Data.Float2x>
struct Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB;
// System.Func`3<Rewired.Utils.Classes.Data.Float3x,Rewired.Utils.Classes.Data.Float3x,Rewired.Utils.Classes.Data.Float3x>
struct Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6;
// System.Func`3<Rewired.Utils.Classes.Data.Float4x,Rewired.Utils.Classes.Data.Float4x,Rewired.Utils.Classes.Data.Float4x>
struct Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// hgArHCrBiBgmkkaWqlNqbKsRVyk/EventFunction`2<Rewired.UI.IVisibilityChangedHandler,System.Boolean>
struct EventFunction_2_tB340856DCEC8183671035C694D756F834AA71712;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DYLVOGVHEENJYEEULYTOEUQYNKK_T3555EB6FDDAFCFBCF107BB6A84AF952343983AA3_H
#define DYLVOGVHEENJYEEULYTOEUQYNKK_T3555EB6FDDAFCFBCF107BB6A84AF952343983AA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DylvOGVHEEnJyEeuLyTOeUqyNkk
struct  DylvOGVHEEnJyEeuLyTOeUqyNkk_t3555EB6FDDAFCFBCF107BB6A84AF952343983AA3  : public RuntimeObject
{
public:

public:
};

struct DylvOGVHEEnJyEeuLyTOeUqyNkk_t3555EB6FDDAFCFBCF107BB6A84AF952343983AA3_StaticFields
{
public:
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<Rewired.UI.IVisibilityChangedHandler,System.Boolean> DylvOGVHEEnJyEeuLyTOeUqyNkk::NOMeqcZMOxtewAfrBZxUglAtxAy
	EventFunction_2_tB340856DCEC8183671035C694D756F834AA71712 * ___NOMeqcZMOxtewAfrBZxUglAtxAy_0;
	// hgArHCrBiBgmkkaWqlNqbKsRVyk_EventFunction`2<Rewired.UI.IVisibilityChangedHandler,System.Boolean> DylvOGVHEEnJyEeuLyTOeUqyNkk::wKZsRNpOhQctCfJnvfAdTetobSIx
	EventFunction_2_tB340856DCEC8183671035C694D756F834AA71712 * ___wKZsRNpOhQctCfJnvfAdTetobSIx_1;

public:
	inline static int32_t get_offset_of_NOMeqcZMOxtewAfrBZxUglAtxAy_0() { return static_cast<int32_t>(offsetof(DylvOGVHEEnJyEeuLyTOeUqyNkk_t3555EB6FDDAFCFBCF107BB6A84AF952343983AA3_StaticFields, ___NOMeqcZMOxtewAfrBZxUglAtxAy_0)); }
	inline EventFunction_2_tB340856DCEC8183671035C694D756F834AA71712 * get_NOMeqcZMOxtewAfrBZxUglAtxAy_0() const { return ___NOMeqcZMOxtewAfrBZxUglAtxAy_0; }
	inline EventFunction_2_tB340856DCEC8183671035C694D756F834AA71712 ** get_address_of_NOMeqcZMOxtewAfrBZxUglAtxAy_0() { return &___NOMeqcZMOxtewAfrBZxUglAtxAy_0; }
	inline void set_NOMeqcZMOxtewAfrBZxUglAtxAy_0(EventFunction_2_tB340856DCEC8183671035C694D756F834AA71712 * value)
	{
		___NOMeqcZMOxtewAfrBZxUglAtxAy_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOMeqcZMOxtewAfrBZxUglAtxAy_0), value);
	}

	inline static int32_t get_offset_of_wKZsRNpOhQctCfJnvfAdTetobSIx_1() { return static_cast<int32_t>(offsetof(DylvOGVHEEnJyEeuLyTOeUqyNkk_t3555EB6FDDAFCFBCF107BB6A84AF952343983AA3_StaticFields, ___wKZsRNpOhQctCfJnvfAdTetobSIx_1)); }
	inline EventFunction_2_tB340856DCEC8183671035C694D756F834AA71712 * get_wKZsRNpOhQctCfJnvfAdTetobSIx_1() const { return ___wKZsRNpOhQctCfJnvfAdTetobSIx_1; }
	inline EventFunction_2_tB340856DCEC8183671035C694D756F834AA71712 ** get_address_of_wKZsRNpOhQctCfJnvfAdTetobSIx_1() { return &___wKZsRNpOhQctCfJnvfAdTetobSIx_1; }
	inline void set_wKZsRNpOhQctCfJnvfAdTetobSIx_1(EventFunction_2_tB340856DCEC8183671035C694D756F834AA71712 * value)
	{
		___wKZsRNpOhQctCfJnvfAdTetobSIx_1 = value;
		Il2CppCodeGenWriteBarrier((&___wKZsRNpOhQctCfJnvfAdTetobSIx_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYLVOGVHEENJYEEULYTOEUQYNKK_T3555EB6FDDAFCFBCF107BB6A84AF952343983AA3_H
#ifndef GMJETTZNICGHDDZQLFGDMVUFCNPJ_T199215CBE67B0D8F7F016D8F9F9EBAD4E24E0F63_H
#define GMJETTZNICGHDDZQLFGDMVUFCNPJ_T199215CBE67B0D8F7F016D8F9F9EBAD4E24E0F63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_GMJeTtZnicgHDDzqLfgDMVufCNPJ
struct  GMJeTtZnicgHDDzqLfgDMVufCNPJ_t199215CBE67B0D8F7F016D8F9F9EBAD4E24E0F63  : public RuntimeObject
{
public:
	// System.Int32 EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_GMJeTtZnicgHDDzqLfgDMVufCNPJ::iDGWEoZdokSjNmaJhbGKJlCcWrm
	int32_t ___iDGWEoZdokSjNmaJhbGKJlCcWrm_0;
	// System.Single EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_GMJeTtZnicgHDDzqLfgDMVufCNPJ::mvKHSWNtYxtrXAPVpClHLUktCzV
	float ___mvKHSWNtYxtrXAPVpClHLUktCzV_1;
	// System.Single EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_GMJeTtZnicgHDDzqLfgDMVufCNPJ::JNQgVTiKijygYJnTzRQNWJvQSMGQ
	float ___JNQgVTiKijygYJnTzRQNWJvQSMGQ_2;

public:
	inline static int32_t get_offset_of_iDGWEoZdokSjNmaJhbGKJlCcWrm_0() { return static_cast<int32_t>(offsetof(GMJeTtZnicgHDDzqLfgDMVufCNPJ_t199215CBE67B0D8F7F016D8F9F9EBAD4E24E0F63, ___iDGWEoZdokSjNmaJhbGKJlCcWrm_0)); }
	inline int32_t get_iDGWEoZdokSjNmaJhbGKJlCcWrm_0() const { return ___iDGWEoZdokSjNmaJhbGKJlCcWrm_0; }
	inline int32_t* get_address_of_iDGWEoZdokSjNmaJhbGKJlCcWrm_0() { return &___iDGWEoZdokSjNmaJhbGKJlCcWrm_0; }
	inline void set_iDGWEoZdokSjNmaJhbGKJlCcWrm_0(int32_t value)
	{
		___iDGWEoZdokSjNmaJhbGKJlCcWrm_0 = value;
	}

	inline static int32_t get_offset_of_mvKHSWNtYxtrXAPVpClHLUktCzV_1() { return static_cast<int32_t>(offsetof(GMJeTtZnicgHDDzqLfgDMVufCNPJ_t199215CBE67B0D8F7F016D8F9F9EBAD4E24E0F63, ___mvKHSWNtYxtrXAPVpClHLUktCzV_1)); }
	inline float get_mvKHSWNtYxtrXAPVpClHLUktCzV_1() const { return ___mvKHSWNtYxtrXAPVpClHLUktCzV_1; }
	inline float* get_address_of_mvKHSWNtYxtrXAPVpClHLUktCzV_1() { return &___mvKHSWNtYxtrXAPVpClHLUktCzV_1; }
	inline void set_mvKHSWNtYxtrXAPVpClHLUktCzV_1(float value)
	{
		___mvKHSWNtYxtrXAPVpClHLUktCzV_1 = value;
	}

	inline static int32_t get_offset_of_JNQgVTiKijygYJnTzRQNWJvQSMGQ_2() { return static_cast<int32_t>(offsetof(GMJeTtZnicgHDDzqLfgDMVufCNPJ_t199215CBE67B0D8F7F016D8F9F9EBAD4E24E0F63, ___JNQgVTiKijygYJnTzRQNWJvQSMGQ_2)); }
	inline float get_JNQgVTiKijygYJnTzRQNWJvQSMGQ_2() const { return ___JNQgVTiKijygYJnTzRQNWJvQSMGQ_2; }
	inline float* get_address_of_JNQgVTiKijygYJnTzRQNWJvQSMGQ_2() { return &___JNQgVTiKijygYJnTzRQNWJvQSMGQ_2; }
	inline void set_JNQgVTiKijygYJnTzRQNWJvQSMGQ_2(float value)
	{
		___JNQgVTiKijygYJnTzRQNWJvQSMGQ_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GMJETTZNICGHDDZQLFGDMVUFCNPJ_T199215CBE67B0D8F7F016D8F9F9EBAD4E24E0F63_H
#ifndef TSEOKKCYYURGUDCBLASTHZYWFSU_T835CAF85D62B6B5E478ECC574C9864E2115F0575_H
#define TSEOKKCYYURGUDCBLASTHZYWFSU_T835CAF85D62B6B5E478ECC574C9864E2115F0575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_TsEOKKcYyURGUdCblaSthzyWFsU
struct  TsEOKKcYyURGUdCblaSthzyWFsU_t835CAF85D62B6B5E478ECC574C9864E2115F0575  : public RuntimeObject
{
public:
	// EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_VsbAISQkdJoqOdmXONtnUYGDNdA[] EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_TsEOKKcYyURGUdCblaSthzyWFsU::jygrDMKtEfPwNxIkfLKMOCmUyrS
	VsbAISQkdJoqOdmXONtnUYGDNdAU5BU5D_t659F3F12A92C612FBA1B07A8AB7D0F8058CB65F5* ___jygrDMKtEfPwNxIkfLKMOCmUyrS_0;

public:
	inline static int32_t get_offset_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_0() { return static_cast<int32_t>(offsetof(TsEOKKcYyURGUdCblaSthzyWFsU_t835CAF85D62B6B5E478ECC574C9864E2115F0575, ___jygrDMKtEfPwNxIkfLKMOCmUyrS_0)); }
	inline VsbAISQkdJoqOdmXONtnUYGDNdAU5BU5D_t659F3F12A92C612FBA1B07A8AB7D0F8058CB65F5* get_jygrDMKtEfPwNxIkfLKMOCmUyrS_0() const { return ___jygrDMKtEfPwNxIkfLKMOCmUyrS_0; }
	inline VsbAISQkdJoqOdmXONtnUYGDNdAU5BU5D_t659F3F12A92C612FBA1B07A8AB7D0F8058CB65F5** get_address_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_0() { return &___jygrDMKtEfPwNxIkfLKMOCmUyrS_0; }
	inline void set_jygrDMKtEfPwNxIkfLKMOCmUyrS_0(VsbAISQkdJoqOdmXONtnUYGDNdAU5BU5D_t659F3F12A92C612FBA1B07A8AB7D0F8058CB65F5* value)
	{
		___jygrDMKtEfPwNxIkfLKMOCmUyrS_0 = value;
		Il2CppCodeGenWriteBarrier((&___jygrDMKtEfPwNxIkfLKMOCmUyrS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TSEOKKCYYURGUDCBLASTHZYWFSU_T835CAF85D62B6B5E478ECC574C9864E2115F0575_H
#ifndef VSBAISQKDJOQODMXONTNUYGDNDA_T04872FAF3937D80A144C647F42EF5437114B38E5_H
#define VSBAISQKDJOQODMXONTNUYGDNDA_T04872FAF3937D80A144C647F42EF5437114B38E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_VsbAISQkdJoqOdmXONtnUYGDNdA
struct  VsbAISQkdJoqOdmXONtnUYGDNdA_t04872FAF3937D80A144C647F42EF5437114B38E5  : public RuntimeObject
{
public:
	// System.Boolean EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_VsbAISQkdJoqOdmXONtnUYGDNdA::mvKHSWNtYxtrXAPVpClHLUktCzV
	bool ___mvKHSWNtYxtrXAPVpClHLUktCzV_0;
	// System.Boolean EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_VsbAISQkdJoqOdmXONtnUYGDNdA::XtHNtnfhgeCKrJTxdJCwGrOeGslH
	bool ___XtHNtnfhgeCKrJTxdJCwGrOeGslH_1;

public:
	inline static int32_t get_offset_of_mvKHSWNtYxtrXAPVpClHLUktCzV_0() { return static_cast<int32_t>(offsetof(VsbAISQkdJoqOdmXONtnUYGDNdA_t04872FAF3937D80A144C647F42EF5437114B38E5, ___mvKHSWNtYxtrXAPVpClHLUktCzV_0)); }
	inline bool get_mvKHSWNtYxtrXAPVpClHLUktCzV_0() const { return ___mvKHSWNtYxtrXAPVpClHLUktCzV_0; }
	inline bool* get_address_of_mvKHSWNtYxtrXAPVpClHLUktCzV_0() { return &___mvKHSWNtYxtrXAPVpClHLUktCzV_0; }
	inline void set_mvKHSWNtYxtrXAPVpClHLUktCzV_0(bool value)
	{
		___mvKHSWNtYxtrXAPVpClHLUktCzV_0 = value;
	}

	inline static int32_t get_offset_of_XtHNtnfhgeCKrJTxdJCwGrOeGslH_1() { return static_cast<int32_t>(offsetof(VsbAISQkdJoqOdmXONtnUYGDNdA_t04872FAF3937D80A144C647F42EF5437114B38E5, ___XtHNtnfhgeCKrJTxdJCwGrOeGslH_1)); }
	inline bool get_XtHNtnfhgeCKrJTxdJCwGrOeGslH_1() const { return ___XtHNtnfhgeCKrJTxdJCwGrOeGslH_1; }
	inline bool* get_address_of_XtHNtnfhgeCKrJTxdJCwGrOeGslH_1() { return &___XtHNtnfhgeCKrJTxdJCwGrOeGslH_1; }
	inline void set_XtHNtnfhgeCKrJTxdJCwGrOeGslH_1(bool value)
	{
		___XtHNtnfhgeCKrJTxdJCwGrOeGslH_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VSBAISQKDJOQODMXONTNUYGDNDA_T04872FAF3937D80A144C647F42EF5437114B38E5_H
#ifndef ARRAYTOOLS_T02E7DC3CE31992C415030A64F304F67C14BDAFC9_H
#define ARRAYTOOLS_T02E7DC3CE31992C415030A64F304F67C14BDAFC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.ArrayTools
struct  ArrayTools_t02E7DC3CE31992C415030A64F304F67C14BDAFC9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYTOOLS_T02E7DC3CE31992C415030A64F304F67C14BDAFC9_H
#ifndef BITTOOLS_TEB61A0CF35B6D9E3489D10AE4F2F3E720908B65B_H
#define BITTOOLS_TEB61A0CF35B6D9E3489D10AE4F2F3E720908B65B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.BitTools
struct  BitTools_tEB61A0CF35B6D9E3489D10AE4F2F3E720908B65B  : public RuntimeObject
{
public:

public:
};

struct BitTools_tEB61A0CF35B6D9E3489D10AE4F2F3E720908B65B_StaticFields
{
public:
	// System.Byte[] Rewired.Utils.BitTools::SVLlPnSWrsGeIkjrbNXnCHMAlbA
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___SVLlPnSWrsGeIkjrbNXnCHMAlbA_0;

public:
	inline static int32_t get_offset_of_SVLlPnSWrsGeIkjrbNXnCHMAlbA_0() { return static_cast<int32_t>(offsetof(BitTools_tEB61A0CF35B6D9E3489D10AE4F2F3E720908B65B_StaticFields, ___SVLlPnSWrsGeIkjrbNXnCHMAlbA_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_SVLlPnSWrsGeIkjrbNXnCHMAlbA_0() const { return ___SVLlPnSWrsGeIkjrbNXnCHMAlbA_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_SVLlPnSWrsGeIkjrbNXnCHMAlbA_0() { return &___SVLlPnSWrsGeIkjrbNXnCHMAlbA_0; }
	inline void set_SVLlPnSWrsGeIkjrbNXnCHMAlbA_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___SVLlPnSWrsGeIkjrbNXnCHMAlbA_0 = value;
		Il2CppCodeGenWriteBarrier((&___SVLlPnSWrsGeIkjrbNXnCHMAlbA_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITTOOLS_TEB61A0CF35B6D9E3489D10AE4F2F3E720908B65B_H
#ifndef INTPADDING_T5EA5D15351CA442F8C3B721C29A6FD5398080D06_H
#define INTPADDING_T5EA5D15351CA442F8C3B721C29A6FD5398080D06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.IntPadding
struct  IntPadding_t5EA5D15351CA442F8C3B721C29A6FD5398080D06  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Utils.Classes.Data.IntPadding::top
	int32_t ___top_0;
	// System.Int32 Rewired.Utils.Classes.Data.IntPadding::right
	int32_t ___right_1;
	// System.Int32 Rewired.Utils.Classes.Data.IntPadding::bottom
	int32_t ___bottom_2;
	// System.Int32 Rewired.Utils.Classes.Data.IntPadding::left
	int32_t ___left_3;

public:
	inline static int32_t get_offset_of_top_0() { return static_cast<int32_t>(offsetof(IntPadding_t5EA5D15351CA442F8C3B721C29A6FD5398080D06, ___top_0)); }
	inline int32_t get_top_0() const { return ___top_0; }
	inline int32_t* get_address_of_top_0() { return &___top_0; }
	inline void set_top_0(int32_t value)
	{
		___top_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(IntPadding_t5EA5D15351CA442F8C3B721C29A6FD5398080D06, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}

	inline static int32_t get_offset_of_bottom_2() { return static_cast<int32_t>(offsetof(IntPadding_t5EA5D15351CA442F8C3B721C29A6FD5398080D06, ___bottom_2)); }
	inline int32_t get_bottom_2() const { return ___bottom_2; }
	inline int32_t* get_address_of_bottom_2() { return &___bottom_2; }
	inline void set_bottom_2(int32_t value)
	{
		___bottom_2 = value;
	}

	inline static int32_t get_offset_of_left_3() { return static_cast<int32_t>(offsetof(IntPadding_t5EA5D15351CA442F8C3B721C29A6FD5398080D06, ___left_3)); }
	inline int32_t get_left_3() const { return ___left_3; }
	inline int32_t* get_address_of_left_3() { return &___left_3; }
	inline void set_left_3(int32_t value)
	{
		___left_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPADDING_T5EA5D15351CA442F8C3B721C29A6FD5398080D06_H
#ifndef INTRECT_TD854461BA870122515A5E50047F967810C37D57D_H
#define INTRECT_TD854461BA870122515A5E50047F967810C37D57D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.IntRect
struct  IntRect_tD854461BA870122515A5E50047F967810C37D57D  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Utils.Classes.Data.IntRect::x
	int32_t ___x_0;
	// System.Int32 Rewired.Utils.Classes.Data.IntRect::y
	int32_t ___y_1;
	// System.Int32 Rewired.Utils.Classes.Data.IntRect::width
	int32_t ___width_2;
	// System.Int32 Rewired.Utils.Classes.Data.IntRect::height
	int32_t ___height_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(IntRect_tD854461BA870122515A5E50047F967810C37D57D, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(IntRect_tD854461BA870122515A5E50047F967810C37D57D, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(IntRect_tD854461BA870122515A5E50047F967810C37D57D, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(IntRect_tD854461BA870122515A5E50047F967810C37D57D, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTRECT_TD854461BA870122515A5E50047F967810C37D57D_H
#ifndef INTVECTOR2_T0FE90043F56F76F399543387D0775EB6A20234DC_H
#define INTVECTOR2_T0FE90043F56F76F399543387D0775EB6A20234DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.IntVector2
struct  IntVector2_t0FE90043F56F76F399543387D0775EB6A20234DC  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Utils.Classes.Data.IntVector2::x
	int32_t ___x_0;
	// System.Int32 Rewired.Utils.Classes.Data.IntVector2::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(IntVector2_t0FE90043F56F76F399543387D0775EB6A20234DC, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(IntVector2_t0FE90043F56F76F399543387D0775EB6A20234DC, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTVECTOR2_T0FE90043F56F76F399543387D0775EB6A20234DC_H
#ifndef INTVECTOR3_TF98FAA9B1105CA2DEF567A7C889B4381C874625D_H
#define INTVECTOR3_TF98FAA9B1105CA2DEF567A7C889B4381C874625D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.IntVector3
struct  IntVector3_tF98FAA9B1105CA2DEF567A7C889B4381C874625D  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Utils.Classes.Data.IntVector3::x
	int32_t ___x_0;
	// System.Int32 Rewired.Utils.Classes.Data.IntVector3::y
	int32_t ___y_1;
	// System.Int32 Rewired.Utils.Classes.Data.IntVector3::z
	int32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(IntVector3_tF98FAA9B1105CA2DEF567A7C889B4381C874625D, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(IntVector3_tF98FAA9B1105CA2DEF567A7C889B4381C874625D, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(IntVector3_tF98FAA9B1105CA2DEF567A7C889B4381C874625D, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTVECTOR3_TF98FAA9B1105CA2DEF567A7C889B4381C874625D_H
#ifndef INTVECTOR4_T03EE51B365B356E37758C3B9655F9E0D1097ABA1_H
#define INTVECTOR4_T03EE51B365B356E37758C3B9655F9E0D1097ABA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.IntVector4
struct  IntVector4_t03EE51B365B356E37758C3B9655F9E0D1097ABA1  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Utils.Classes.Data.IntVector4::x
	int32_t ___x_0;
	// System.Int32 Rewired.Utils.Classes.Data.IntVector4::y
	int32_t ___y_1;
	// System.Int32 Rewired.Utils.Classes.Data.IntVector4::z
	int32_t ___z_2;
	// System.Int32 Rewired.Utils.Classes.Data.IntVector4::q
	int32_t ___q_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(IntVector4_t03EE51B365B356E37758C3B9655F9E0D1097ABA1, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(IntVector4_t03EE51B365B356E37758C3B9655F9E0D1097ABA1, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(IntVector4_t03EE51B365B356E37758C3B9655F9E0D1097ABA1, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_q_3() { return static_cast<int32_t>(offsetof(IntVector4_t03EE51B365B356E37758C3B9655F9E0D1097ABA1, ___q_3)); }
	inline int32_t get_q_3() const { return ___q_3; }
	inline int32_t* get_address_of_q_3() { return &___q_3; }
	inline void set_q_3(int32_t value)
	{
		___q_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTVECTOR4_T03EE51B365B356E37758C3B9655F9E0D1097ABA1_H
#ifndef NATIVERINGBUFFER_T48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E_H
#define NATIVERINGBUFFER_T48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.NativeRingBuffer
struct  NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E  : public RuntimeObject
{
public:
	// Rewired.Utils.Classes.Data.NativeBuffer Rewired.Utils.Classes.Data.NativeRingBuffer::gWYcSreEUttuvqTHqKoRwtWQvHb
	NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * ___gWYcSreEUttuvqTHqKoRwtWQvHb_0;
	// System.Int32 Rewired.Utils.Classes.Data.NativeRingBuffer::sBiHWicpYhmNgEiaLGeoNmCayd
	int32_t ___sBiHWicpYhmNgEiaLGeoNmCayd_1;
	// System.Int64 Rewired.Utils.Classes.Data.NativeRingBuffer::XWfuhHzLXoGfsuCDzbUXDcLiMFsN
	int64_t ___XWfuhHzLXoGfsuCDzbUXDcLiMFsN_2;
	// System.Int64 Rewired.Utils.Classes.Data.NativeRingBuffer::UbdmeRQrglqqkmOaVIKVDgtTcHy
	int64_t ___UbdmeRQrglqqkmOaVIKVDgtTcHy_3;
	// System.Int32 Rewired.Utils.Classes.Data.NativeRingBuffer::MqUXNSXDPPfjAskTnFXiaRaylNl
	int32_t ___MqUXNSXDPPfjAskTnFXiaRaylNl_4;
	// System.Boolean Rewired.Utils.Classes.Data.NativeRingBuffer::kVZynhRxuCAZFbglRgJlGkfKQuU
	bool ___kVZynhRxuCAZFbglRgJlGkfKQuU_5;
	// System.UInt32 Rewired.Utils.Classes.Data.NativeRingBuffer::jzHVvuRZBKMkHyMhCGOheBeOJtl
	uint32_t ___jzHVvuRZBKMkHyMhCGOheBeOJtl_6;
	// System.Boolean Rewired.Utils.Classes.Data.NativeRingBuffer::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_7;

public:
	inline static int32_t get_offset_of_gWYcSreEUttuvqTHqKoRwtWQvHb_0() { return static_cast<int32_t>(offsetof(NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E, ___gWYcSreEUttuvqTHqKoRwtWQvHb_0)); }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * get_gWYcSreEUttuvqTHqKoRwtWQvHb_0() const { return ___gWYcSreEUttuvqTHqKoRwtWQvHb_0; }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 ** get_address_of_gWYcSreEUttuvqTHqKoRwtWQvHb_0() { return &___gWYcSreEUttuvqTHqKoRwtWQvHb_0; }
	inline void set_gWYcSreEUttuvqTHqKoRwtWQvHb_0(NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * value)
	{
		___gWYcSreEUttuvqTHqKoRwtWQvHb_0 = value;
		Il2CppCodeGenWriteBarrier((&___gWYcSreEUttuvqTHqKoRwtWQvHb_0), value);
	}

	inline static int32_t get_offset_of_sBiHWicpYhmNgEiaLGeoNmCayd_1() { return static_cast<int32_t>(offsetof(NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E, ___sBiHWicpYhmNgEiaLGeoNmCayd_1)); }
	inline int32_t get_sBiHWicpYhmNgEiaLGeoNmCayd_1() const { return ___sBiHWicpYhmNgEiaLGeoNmCayd_1; }
	inline int32_t* get_address_of_sBiHWicpYhmNgEiaLGeoNmCayd_1() { return &___sBiHWicpYhmNgEiaLGeoNmCayd_1; }
	inline void set_sBiHWicpYhmNgEiaLGeoNmCayd_1(int32_t value)
	{
		___sBiHWicpYhmNgEiaLGeoNmCayd_1 = value;
	}

	inline static int32_t get_offset_of_XWfuhHzLXoGfsuCDzbUXDcLiMFsN_2() { return static_cast<int32_t>(offsetof(NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E, ___XWfuhHzLXoGfsuCDzbUXDcLiMFsN_2)); }
	inline int64_t get_XWfuhHzLXoGfsuCDzbUXDcLiMFsN_2() const { return ___XWfuhHzLXoGfsuCDzbUXDcLiMFsN_2; }
	inline int64_t* get_address_of_XWfuhHzLXoGfsuCDzbUXDcLiMFsN_2() { return &___XWfuhHzLXoGfsuCDzbUXDcLiMFsN_2; }
	inline void set_XWfuhHzLXoGfsuCDzbUXDcLiMFsN_2(int64_t value)
	{
		___XWfuhHzLXoGfsuCDzbUXDcLiMFsN_2 = value;
	}

	inline static int32_t get_offset_of_UbdmeRQrglqqkmOaVIKVDgtTcHy_3() { return static_cast<int32_t>(offsetof(NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E, ___UbdmeRQrglqqkmOaVIKVDgtTcHy_3)); }
	inline int64_t get_UbdmeRQrglqqkmOaVIKVDgtTcHy_3() const { return ___UbdmeRQrglqqkmOaVIKVDgtTcHy_3; }
	inline int64_t* get_address_of_UbdmeRQrglqqkmOaVIKVDgtTcHy_3() { return &___UbdmeRQrglqqkmOaVIKVDgtTcHy_3; }
	inline void set_UbdmeRQrglqqkmOaVIKVDgtTcHy_3(int64_t value)
	{
		___UbdmeRQrglqqkmOaVIKVDgtTcHy_3 = value;
	}

	inline static int32_t get_offset_of_MqUXNSXDPPfjAskTnFXiaRaylNl_4() { return static_cast<int32_t>(offsetof(NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E, ___MqUXNSXDPPfjAskTnFXiaRaylNl_4)); }
	inline int32_t get_MqUXNSXDPPfjAskTnFXiaRaylNl_4() const { return ___MqUXNSXDPPfjAskTnFXiaRaylNl_4; }
	inline int32_t* get_address_of_MqUXNSXDPPfjAskTnFXiaRaylNl_4() { return &___MqUXNSXDPPfjAskTnFXiaRaylNl_4; }
	inline void set_MqUXNSXDPPfjAskTnFXiaRaylNl_4(int32_t value)
	{
		___MqUXNSXDPPfjAskTnFXiaRaylNl_4 = value;
	}

	inline static int32_t get_offset_of_kVZynhRxuCAZFbglRgJlGkfKQuU_5() { return static_cast<int32_t>(offsetof(NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E, ___kVZynhRxuCAZFbglRgJlGkfKQuU_5)); }
	inline bool get_kVZynhRxuCAZFbglRgJlGkfKQuU_5() const { return ___kVZynhRxuCAZFbglRgJlGkfKQuU_5; }
	inline bool* get_address_of_kVZynhRxuCAZFbglRgJlGkfKQuU_5() { return &___kVZynhRxuCAZFbglRgJlGkfKQuU_5; }
	inline void set_kVZynhRxuCAZFbglRgJlGkfKQuU_5(bool value)
	{
		___kVZynhRxuCAZFbglRgJlGkfKQuU_5 = value;
	}

	inline static int32_t get_offset_of_jzHVvuRZBKMkHyMhCGOheBeOJtl_6() { return static_cast<int32_t>(offsetof(NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E, ___jzHVvuRZBKMkHyMhCGOheBeOJtl_6)); }
	inline uint32_t get_jzHVvuRZBKMkHyMhCGOheBeOJtl_6() const { return ___jzHVvuRZBKMkHyMhCGOheBeOJtl_6; }
	inline uint32_t* get_address_of_jzHVvuRZBKMkHyMhCGOheBeOJtl_6() { return &___jzHVvuRZBKMkHyMhCGOheBeOJtl_6; }
	inline void set_jzHVvuRZBKMkHyMhCGOheBeOJtl_6(uint32_t value)
	{
		___jzHVvuRZBKMkHyMhCGOheBeOJtl_6 = value;
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_7() { return static_cast<int32_t>(offsetof(NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E, ___SeCUoinDywZmqZDHRKupOdOaTke_7)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_7() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_7; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_7() { return &___SeCUoinDywZmqZDHRKupOdOaTke_7; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_7(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVERINGBUFFER_T48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E_H
#ifndef SERIALIZABLEULONG_T33164346204E6ACFE4136BC2B4A0D5F9941FADEF_H
#define SERIALIZABLEULONG_T33164346204E6ACFE4136BC2B4A0D5F9941FADEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.SerializableULong
struct  SerializableULong_t33164346204E6ACFE4136BC2B4A0D5F9941FADEF  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Utils.Classes.Data.SerializableULong::ulong_32BitLow
	int32_t ___ulong_32BitLow_0;
	// System.Int32 Rewired.Utils.Classes.Data.SerializableULong::ulong_32BitHigh
	int32_t ___ulong_32BitHigh_1;

public:
	inline static int32_t get_offset_of_ulong_32BitLow_0() { return static_cast<int32_t>(offsetof(SerializableULong_t33164346204E6ACFE4136BC2B4A0D5F9941FADEF, ___ulong_32BitLow_0)); }
	inline int32_t get_ulong_32BitLow_0() const { return ___ulong_32BitLow_0; }
	inline int32_t* get_address_of_ulong_32BitLow_0() { return &___ulong_32BitLow_0; }
	inline void set_ulong_32BitLow_0(int32_t value)
	{
		___ulong_32BitLow_0 = value;
	}

	inline static int32_t get_offset_of_ulong_32BitHigh_1() { return static_cast<int32_t>(offsetof(SerializableULong_t33164346204E6ACFE4136BC2B4A0D5F9941FADEF, ___ulong_32BitHigh_1)); }
	inline int32_t get_ulong_32BitHigh_1() const { return ___ulong_32BitHigh_1; }
	inline int32_t* get_address_of_ulong_32BitHigh_1() { return &___ulong_32BitHigh_1; }
	inline void set_ulong_32BitHigh_1(int32_t value)
	{
		___ulong_32BitHigh_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEULONG_T33164346204E6ACFE4136BC2B4A0D5F9941FADEF_H
#ifndef XMLDOCUMENT_TEB80CCD80CA82521A35545CB41596F7A54C65BE3_H
#define XMLDOCUMENT_TEB80CCD80CA82521A35545CB41596F7A54C65BE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.SerializedObject_XmlDocument
struct  XmlDocument_tEB80CCD80CA82521A35545CB41596F7A54C65BE3  : public RuntimeObject
{
public:
	// Rewired.Utils.Classes.Data.SerializedObject_XmlDocument_Element Rewired.Utils.Classes.Data.SerializedObject_XmlDocument::_root
	Element_tB976891F5774B30930B5751F45DAC9A21D669254 * ____root_0;

public:
	inline static int32_t get_offset_of__root_0() { return static_cast<int32_t>(offsetof(XmlDocument_tEB80CCD80CA82521A35545CB41596F7A54C65BE3, ____root_0)); }
	inline Element_tB976891F5774B30930B5751F45DAC9A21D669254 * get__root_0() const { return ____root_0; }
	inline Element_tB976891F5774B30930B5751F45DAC9A21D669254 ** get_address_of__root_0() { return &____root_0; }
	inline void set__root_0(Element_tB976891F5774B30930B5751F45DAC9A21D669254 * value)
	{
		____root_0 = value;
		Il2CppCodeGenWriteBarrier((&____root_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENT_TEB80CCD80CA82521A35545CB41596F7A54C65BE3_H
#ifndef ELEMENT_TB976891F5774B30930B5751F45DAC9A21D669254_H
#define ELEMENT_TB976891F5774B30930B5751F45DAC9A21D669254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.SerializedObject_XmlDocument_Element
struct  Element_tB976891F5774B30930B5751F45DAC9A21D669254  : public RuntimeObject
{
public:
	// System.String Rewired.Utils.Classes.Data.SerializedObject_XmlDocument_Element::name
	String_t* ___name_0;
	// Rewired.Utils.Classes.Data.SerializedObject_XmlDocument_Element Rewired.Utils.Classes.Data.SerializedObject_XmlDocument_Element::parent
	Element_tB976891F5774B30930B5751F45DAC9A21D669254 * ___parent_1;
	// System.String Rewired.Utils.Classes.Data.SerializedObject_XmlDocument_Element::content
	String_t* ___content_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Rewired.Utils.Classes.Data.SerializedObject_XmlDocument_Element::attributes
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___attributes_3;
	// System.Collections.Generic.List`1<Rewired.Utils.Classes.Data.SerializedObject_XmlDocument_Element> Rewired.Utils.Classes.Data.SerializedObject_XmlDocument_Element::children
	List_1_tD303BF18C474D111E3A63E9BCED1882E91F62D15 * ___children_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Element_tB976891F5774B30930B5751F45DAC9A21D669254, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(Element_tB976891F5774B30930B5751F45DAC9A21D669254, ___parent_1)); }
	inline Element_tB976891F5774B30930B5751F45DAC9A21D669254 * get_parent_1() const { return ___parent_1; }
	inline Element_tB976891F5774B30930B5751F45DAC9A21D669254 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(Element_tB976891F5774B30930B5751F45DAC9A21D669254 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier((&___parent_1), value);
	}

	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(Element_tB976891F5774B30930B5751F45DAC9A21D669254, ___content_2)); }
	inline String_t* get_content_2() const { return ___content_2; }
	inline String_t** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(String_t* value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}

	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(Element_tB976891F5774B30930B5751F45DAC9A21D669254, ___attributes_3)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_attributes_3() const { return ___attributes_3; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___attributes_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_3), value);
	}

	inline static int32_t get_offset_of_children_4() { return static_cast<int32_t>(offsetof(Element_tB976891F5774B30930B5751F45DAC9A21D669254, ___children_4)); }
	inline List_1_tD303BF18C474D111E3A63E9BCED1882E91F62D15 * get_children_4() const { return ___children_4; }
	inline List_1_tD303BF18C474D111E3A63E9BCED1882E91F62D15 ** get_address_of_children_4() { return &___children_4; }
	inline void set_children_4(List_1_tD303BF18C474D111E3A63E9BCED1882E91F62D15 * value)
	{
		___children_4 = value;
		Il2CppCodeGenWriteBarrier((&___children_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_TB976891F5774B30930B5751F45DAC9A21D669254_H
#ifndef XMLINFO_T1D129B900590213A92E045C337F63C7A2D1A2FBB_H
#define XMLINFO_T1D129B900590213A92E045C337F63C7A2D1A2FBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.SerializedObject_XmlInfo
struct  XmlInfo_t1D129B900590213A92E045C337F63C7A2D1A2FBB  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Rewired.Utils.Classes.Data.SerializedObject_XmlInfo_XmlAttribute> Rewired.Utils.Classes.Data.SerializedObject_XmlInfo::fLfjvbAsBBJCTCqKzKXkmvWdjlNO
	List_1_t9671A9C9A8A89A10CA13CEE5E706700AD7AF23F4 * ___fLfjvbAsBBJCTCqKzKXkmvWdjlNO_0;

public:
	inline static int32_t get_offset_of_fLfjvbAsBBJCTCqKzKXkmvWdjlNO_0() { return static_cast<int32_t>(offsetof(XmlInfo_t1D129B900590213A92E045C337F63C7A2D1A2FBB, ___fLfjvbAsBBJCTCqKzKXkmvWdjlNO_0)); }
	inline List_1_t9671A9C9A8A89A10CA13CEE5E706700AD7AF23F4 * get_fLfjvbAsBBJCTCqKzKXkmvWdjlNO_0() const { return ___fLfjvbAsBBJCTCqKzKXkmvWdjlNO_0; }
	inline List_1_t9671A9C9A8A89A10CA13CEE5E706700AD7AF23F4 ** get_address_of_fLfjvbAsBBJCTCqKzKXkmvWdjlNO_0() { return &___fLfjvbAsBBJCTCqKzKXkmvWdjlNO_0; }
	inline void set_fLfjvbAsBBJCTCqKzKXkmvWdjlNO_0(List_1_t9671A9C9A8A89A10CA13CEE5E706700AD7AF23F4 * value)
	{
		___fLfjvbAsBBJCTCqKzKXkmvWdjlNO_0 = value;
		Il2CppCodeGenWriteBarrier((&___fLfjvbAsBBJCTCqKzKXkmvWdjlNO_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLINFO_T1D129B900590213A92E045C337F63C7A2D1A2FBB_H
#ifndef XMLATTRIBUTE_T654C37181E9CB42791E326F78F30E4352E521DC4_H
#define XMLATTRIBUTE_T654C37181E9CB42791E326F78F30E4352E521DC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.SerializedObject_XmlInfo_XmlAttribute
struct  XmlAttribute_t654C37181E9CB42791E326F78F30E4352E521DC4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTE_T654C37181E9CB42791E326F78F30E4352E521DC4_H
#ifndef COLLECTIONTOOLS_TD23FBCB2643F1CBA7D62E771DB0897933AC9A22E_H
#define COLLECTIONTOOLS_TD23FBCB2643F1CBA7D62E771DB0897933AC9A22E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.CollectionTools
struct  CollectionTools_tD23FBCB2643F1CBA7D62E771DB0897933AC9A22E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONTOOLS_TD23FBCB2643F1CBA7D62E771DB0897933AC9A22E_H
#ifndef ENUMTOOLS_T755C614986ADC18A4AF7B958B1D857B57FD601D4_H
#define ENUMTOOLS_T755C614986ADC18A4AF7B958B1D857B57FD601D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.EnumTools
struct  EnumTools_t755C614986ADC18A4AF7B958B1D857B57FD601D4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMTOOLS_T755C614986ADC18A4AF7B958B1D857B57FD601D4_H
#ifndef EXTENSIONMETHODS_T84BE5B0F3959CB2C3F5C52B7D2D86F4D40F61DF2_H
#define EXTENSIONMETHODS_T84BE5B0F3959CB2C3F5C52B7D2D86F4D40F61DF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.ExtensionMethods
struct  ExtensionMethods_t84BE5B0F3959CB2C3F5C52B7D2D86F4D40F61DF2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONMETHODS_T84BE5B0F3959CB2C3F5C52B7D2D86F4D40F61DF2_H
#ifndef GUITOOLS_T151239CEF509B8F55E5BB9BF9083DFD0FE728D5F_H
#define GUITOOLS_T151239CEF509B8F55E5BB9BF9083DFD0FE728D5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.GUITools
struct  GUITools_t151239CEF509B8F55E5BB9BF9083DFD0FE728D5F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITOOLS_T151239CEF509B8F55E5BB9BF9083DFD0FE728D5F_H
#ifndef JYLRBGUMENHRDRKYWPQSNPRSBOX_T421A3ADF936383A24ABF7D179E497851B293E4D8_H
#define JYLRBGUMENHRDRKYWPQSNPRSBOX_T421A3ADF936383A24ABF7D179E497851B293E4D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.GUITools_JYlRBGumenhRDRKYwPQSnpRSbox
struct  JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D Rewired.Utils.GUITools_JYlRBGumenhRDRKYwPQSnpRSbox::EWjdtQBmywIsBumQtMhYWMahyq
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___EWjdtQBmywIsBumQtMhYWMahyq_0;
	// UnityEngine.Texture2D Rewired.Utils.GUITools_JYlRBGumenhRDRKYwPQSnpRSbox::fikNlDQpHfvQauiZfOtstCPtqXx
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___fikNlDQpHfvQauiZfOtstCPtqXx_1;
	// System.Boolean Rewired.Utils.GUITools_JYlRBGumenhRDRKYwPQSnpRSbox::wvGzQhLdOBNwcvVnZKxLivQDPWK
	bool ___wvGzQhLdOBNwcvVnZKxLivQDPWK_2;

public:
	inline static int32_t get_offset_of_EWjdtQBmywIsBumQtMhYWMahyq_0() { return static_cast<int32_t>(offsetof(JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8, ___EWjdtQBmywIsBumQtMhYWMahyq_0)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_EWjdtQBmywIsBumQtMhYWMahyq_0() const { return ___EWjdtQBmywIsBumQtMhYWMahyq_0; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_EWjdtQBmywIsBumQtMhYWMahyq_0() { return &___EWjdtQBmywIsBumQtMhYWMahyq_0; }
	inline void set_EWjdtQBmywIsBumQtMhYWMahyq_0(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___EWjdtQBmywIsBumQtMhYWMahyq_0 = value;
		Il2CppCodeGenWriteBarrier((&___EWjdtQBmywIsBumQtMhYWMahyq_0), value);
	}

	inline static int32_t get_offset_of_fikNlDQpHfvQauiZfOtstCPtqXx_1() { return static_cast<int32_t>(offsetof(JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8, ___fikNlDQpHfvQauiZfOtstCPtqXx_1)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_fikNlDQpHfvQauiZfOtstCPtqXx_1() const { return ___fikNlDQpHfvQauiZfOtstCPtqXx_1; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_fikNlDQpHfvQauiZfOtstCPtqXx_1() { return &___fikNlDQpHfvQauiZfOtstCPtqXx_1; }
	inline void set_fikNlDQpHfvQauiZfOtstCPtqXx_1(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___fikNlDQpHfvQauiZfOtstCPtqXx_1 = value;
		Il2CppCodeGenWriteBarrier((&___fikNlDQpHfvQauiZfOtstCPtqXx_1), value);
	}

	inline static int32_t get_offset_of_wvGzQhLdOBNwcvVnZKxLivQDPWK_2() { return static_cast<int32_t>(offsetof(JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8, ___wvGzQhLdOBNwcvVnZKxLivQDPWK_2)); }
	inline bool get_wvGzQhLdOBNwcvVnZKxLivQDPWK_2() const { return ___wvGzQhLdOBNwcvVnZKxLivQDPWK_2; }
	inline bool* get_address_of_wvGzQhLdOBNwcvVnZKxLivQDPWK_2() { return &___wvGzQhLdOBNwcvVnZKxLivQDPWK_2; }
	inline void set_wvGzQhLdOBNwcvVnZKxLivQDPWK_2(bool value)
	{
		___wvGzQhLdOBNwcvVnZKxLivQDPWK_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JYLRBGUMENHRDRKYWPQSNPRSBOX_T421A3ADF936383A24ABF7D179E497851B293E4D8_H
#ifndef INPUTTOOLS_T9682C38ED402090BA6DBDD740CD85A0471DD459B_H
#define INPUTTOOLS_T9682C38ED402090BA6DBDD740CD85A0471DD459B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.InputTools
struct  InputTools_t9682C38ED402090BA6DBDD740CD85A0471DD459B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTOOLS_T9682C38ED402090BA6DBDD740CD85A0471DD459B_H
#ifndef JSONPARSER_T4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_H
#define JSONPARSER_T4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Libraries.TinyJson.JsonParser
struct  JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8  : public RuntimeObject
{
public:

public:
};

struct JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.String>> Rewired.Utils.Libraries.TinyJson.JsonParser::splitArrayPool
	Stack_1_t4EF598C6952C314843BCAE9E9E1AC9B7262E7999 * ___splitArrayPool_0;
	// System.Text.StringBuilder Rewired.Utils.Libraries.TinyJson.JsonParser::KWmHqVWzTXhrCMwnLCcIUmyQlOp
	StringBuilder_t * ___KWmHqVWzTXhrCMwnLCcIUmyQlOp_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.FieldInfo>> Rewired.Utils.Libraries.TinyJson.JsonParser::LQGJEklSmntrktcsXahnHwtrcTh
	Dictionary_2_tF19B322732A3F59097E35488888753E75A98508B * ___LQGJEklSmntrktcsXahnHwtrcTh_2;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.PropertyInfo>> Rewired.Utils.Libraries.TinyJson.JsonParser::nBBUxyulOPuAQzbmJBCPkpmkUWsj
	Dictionary_2_t85E880FBBD1F7D6DF932C42144E9B40E2B7FCFE9 * ___nBBUxyulOPuAQzbmJBCPkpmkUWsj_3;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Rewired.Utils.Libraries.TinyJson.JsonParser::APrqfxcqXfOigsaYgstdZUzQbTD
	Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * ___APrqfxcqXfOigsaYgstdZUzQbTD_4;
	// System.Func`2<System.Reflection.FieldInfo,System.String> Rewired.Utils.Libraries.TinyJson.JsonParser::wzuTgIXkBigvyhwmUgzxbJRwaLDH
	Func_2_t1EB5033BF4C5996F1CBD82840960676EBDD8049C * ___wzuTgIXkBigvyhwmUgzxbJRwaLDH_5;
	// System.Func`2<System.Reflection.PropertyInfo,System.Boolean> Rewired.Utils.Libraries.TinyJson.JsonParser::JHDbWtnHlVZLvnPftaKEZPlIjTx
	Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * ___JHDbWtnHlVZLvnPftaKEZPlIjTx_6;
	// System.Func`2<System.Reflection.PropertyInfo,System.String> Rewired.Utils.Libraries.TinyJson.JsonParser::xzAVWYnEbnPJfpBopUAUVcTTRAy
	Func_2_t1E4225A498AC4D60FC503C2A063C30CA478ACF5A * ___xzAVWYnEbnPJfpBopUAUVcTTRAy_7;

public:
	inline static int32_t get_offset_of_splitArrayPool_0() { return static_cast<int32_t>(offsetof(JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields, ___splitArrayPool_0)); }
	inline Stack_1_t4EF598C6952C314843BCAE9E9E1AC9B7262E7999 * get_splitArrayPool_0() const { return ___splitArrayPool_0; }
	inline Stack_1_t4EF598C6952C314843BCAE9E9E1AC9B7262E7999 ** get_address_of_splitArrayPool_0() { return &___splitArrayPool_0; }
	inline void set_splitArrayPool_0(Stack_1_t4EF598C6952C314843BCAE9E9E1AC9B7262E7999 * value)
	{
		___splitArrayPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___splitArrayPool_0), value);
	}

	inline static int32_t get_offset_of_KWmHqVWzTXhrCMwnLCcIUmyQlOp_1() { return static_cast<int32_t>(offsetof(JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields, ___KWmHqVWzTXhrCMwnLCcIUmyQlOp_1)); }
	inline StringBuilder_t * get_KWmHqVWzTXhrCMwnLCcIUmyQlOp_1() const { return ___KWmHqVWzTXhrCMwnLCcIUmyQlOp_1; }
	inline StringBuilder_t ** get_address_of_KWmHqVWzTXhrCMwnLCcIUmyQlOp_1() { return &___KWmHqVWzTXhrCMwnLCcIUmyQlOp_1; }
	inline void set_KWmHqVWzTXhrCMwnLCcIUmyQlOp_1(StringBuilder_t * value)
	{
		___KWmHqVWzTXhrCMwnLCcIUmyQlOp_1 = value;
		Il2CppCodeGenWriteBarrier((&___KWmHqVWzTXhrCMwnLCcIUmyQlOp_1), value);
	}

	inline static int32_t get_offset_of_LQGJEklSmntrktcsXahnHwtrcTh_2() { return static_cast<int32_t>(offsetof(JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields, ___LQGJEklSmntrktcsXahnHwtrcTh_2)); }
	inline Dictionary_2_tF19B322732A3F59097E35488888753E75A98508B * get_LQGJEklSmntrktcsXahnHwtrcTh_2() const { return ___LQGJEklSmntrktcsXahnHwtrcTh_2; }
	inline Dictionary_2_tF19B322732A3F59097E35488888753E75A98508B ** get_address_of_LQGJEklSmntrktcsXahnHwtrcTh_2() { return &___LQGJEklSmntrktcsXahnHwtrcTh_2; }
	inline void set_LQGJEklSmntrktcsXahnHwtrcTh_2(Dictionary_2_tF19B322732A3F59097E35488888753E75A98508B * value)
	{
		___LQGJEklSmntrktcsXahnHwtrcTh_2 = value;
		Il2CppCodeGenWriteBarrier((&___LQGJEklSmntrktcsXahnHwtrcTh_2), value);
	}

	inline static int32_t get_offset_of_nBBUxyulOPuAQzbmJBCPkpmkUWsj_3() { return static_cast<int32_t>(offsetof(JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields, ___nBBUxyulOPuAQzbmJBCPkpmkUWsj_3)); }
	inline Dictionary_2_t85E880FBBD1F7D6DF932C42144E9B40E2B7FCFE9 * get_nBBUxyulOPuAQzbmJBCPkpmkUWsj_3() const { return ___nBBUxyulOPuAQzbmJBCPkpmkUWsj_3; }
	inline Dictionary_2_t85E880FBBD1F7D6DF932C42144E9B40E2B7FCFE9 ** get_address_of_nBBUxyulOPuAQzbmJBCPkpmkUWsj_3() { return &___nBBUxyulOPuAQzbmJBCPkpmkUWsj_3; }
	inline void set_nBBUxyulOPuAQzbmJBCPkpmkUWsj_3(Dictionary_2_t85E880FBBD1F7D6DF932C42144E9B40E2B7FCFE9 * value)
	{
		___nBBUxyulOPuAQzbmJBCPkpmkUWsj_3 = value;
		Il2CppCodeGenWriteBarrier((&___nBBUxyulOPuAQzbmJBCPkpmkUWsj_3), value);
	}

	inline static int32_t get_offset_of_APrqfxcqXfOigsaYgstdZUzQbTD_4() { return static_cast<int32_t>(offsetof(JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields, ___APrqfxcqXfOigsaYgstdZUzQbTD_4)); }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * get_APrqfxcqXfOigsaYgstdZUzQbTD_4() const { return ___APrqfxcqXfOigsaYgstdZUzQbTD_4; }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D ** get_address_of_APrqfxcqXfOigsaYgstdZUzQbTD_4() { return &___APrqfxcqXfOigsaYgstdZUzQbTD_4; }
	inline void set_APrqfxcqXfOigsaYgstdZUzQbTD_4(Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * value)
	{
		___APrqfxcqXfOigsaYgstdZUzQbTD_4 = value;
		Il2CppCodeGenWriteBarrier((&___APrqfxcqXfOigsaYgstdZUzQbTD_4), value);
	}

	inline static int32_t get_offset_of_wzuTgIXkBigvyhwmUgzxbJRwaLDH_5() { return static_cast<int32_t>(offsetof(JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields, ___wzuTgIXkBigvyhwmUgzxbJRwaLDH_5)); }
	inline Func_2_t1EB5033BF4C5996F1CBD82840960676EBDD8049C * get_wzuTgIXkBigvyhwmUgzxbJRwaLDH_5() const { return ___wzuTgIXkBigvyhwmUgzxbJRwaLDH_5; }
	inline Func_2_t1EB5033BF4C5996F1CBD82840960676EBDD8049C ** get_address_of_wzuTgIXkBigvyhwmUgzxbJRwaLDH_5() { return &___wzuTgIXkBigvyhwmUgzxbJRwaLDH_5; }
	inline void set_wzuTgIXkBigvyhwmUgzxbJRwaLDH_5(Func_2_t1EB5033BF4C5996F1CBD82840960676EBDD8049C * value)
	{
		___wzuTgIXkBigvyhwmUgzxbJRwaLDH_5 = value;
		Il2CppCodeGenWriteBarrier((&___wzuTgIXkBigvyhwmUgzxbJRwaLDH_5), value);
	}

	inline static int32_t get_offset_of_JHDbWtnHlVZLvnPftaKEZPlIjTx_6() { return static_cast<int32_t>(offsetof(JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields, ___JHDbWtnHlVZLvnPftaKEZPlIjTx_6)); }
	inline Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * get_JHDbWtnHlVZLvnPftaKEZPlIjTx_6() const { return ___JHDbWtnHlVZLvnPftaKEZPlIjTx_6; }
	inline Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C ** get_address_of_JHDbWtnHlVZLvnPftaKEZPlIjTx_6() { return &___JHDbWtnHlVZLvnPftaKEZPlIjTx_6; }
	inline void set_JHDbWtnHlVZLvnPftaKEZPlIjTx_6(Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * value)
	{
		___JHDbWtnHlVZLvnPftaKEZPlIjTx_6 = value;
		Il2CppCodeGenWriteBarrier((&___JHDbWtnHlVZLvnPftaKEZPlIjTx_6), value);
	}

	inline static int32_t get_offset_of_xzAVWYnEbnPJfpBopUAUVcTTRAy_7() { return static_cast<int32_t>(offsetof(JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields, ___xzAVWYnEbnPJfpBopUAUVcTTRAy_7)); }
	inline Func_2_t1E4225A498AC4D60FC503C2A063C30CA478ACF5A * get_xzAVWYnEbnPJfpBopUAUVcTTRAy_7() const { return ___xzAVWYnEbnPJfpBopUAUVcTTRAy_7; }
	inline Func_2_t1E4225A498AC4D60FC503C2A063C30CA478ACF5A ** get_address_of_xzAVWYnEbnPJfpBopUAUVcTTRAy_7() { return &___xzAVWYnEbnPJfpBopUAUVcTTRAy_7; }
	inline void set_xzAVWYnEbnPJfpBopUAUVcTTRAy_7(Func_2_t1E4225A498AC4D60FC503C2A063C30CA478ACF5A * value)
	{
		___xzAVWYnEbnPJfpBopUAUVcTTRAy_7 = value;
		Il2CppCodeGenWriteBarrier((&___xzAVWYnEbnPJfpBopUAUVcTTRAy_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPARSER_T4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_H
#ifndef JSONTOOLS_TDED13E7D75774FB5C228D7AE7F9006973F9AF0DC_H
#define JSONTOOLS_TDED13E7D75774FB5C228D7AE7F9006973F9AF0DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Libraries.TinyJson.JsonTools
struct  JsonTools_tDED13E7D75774FB5C228D7AE7F9006973F9AF0DC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOOLS_TDED13E7D75774FB5C228D7AE7F9006973F9AF0DC_H
#ifndef JSONWRITER_T89F979F122D2D97D0748B0D4EA7CB9A6C4FE867F_H
#define JSONWRITER_T89F979F122D2D97D0748B0D4EA7CB9A6C4FE867F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Libraries.TinyJson.JsonWriter
struct  JsonWriter_t89F979F122D2D97D0748B0D4EA7CB9A6C4FE867F  : public RuntimeObject
{
public:

public:
};

struct JsonWriter_t89F979F122D2D97D0748B0D4EA7CB9A6C4FE867F_StaticFields
{
public:
	// System.Action`2<System.Text.StringBuilder,System.Object> Rewired.Utils.Libraries.TinyJson.JsonWriter::pzweyUhGzpYFZIjeZCshaHZTBEkZ
	Action_2_t0C40F2023DD772A0F3511D237BE444DEC1344C44 * ___pzweyUhGzpYFZIjeZCshaHZTBEkZ_0;

public:
	inline static int32_t get_offset_of_pzweyUhGzpYFZIjeZCshaHZTBEkZ_0() { return static_cast<int32_t>(offsetof(JsonWriter_t89F979F122D2D97D0748B0D4EA7CB9A6C4FE867F_StaticFields, ___pzweyUhGzpYFZIjeZCshaHZTBEkZ_0)); }
	inline Action_2_t0C40F2023DD772A0F3511D237BE444DEC1344C44 * get_pzweyUhGzpYFZIjeZCshaHZTBEkZ_0() const { return ___pzweyUhGzpYFZIjeZCshaHZTBEkZ_0; }
	inline Action_2_t0C40F2023DD772A0F3511D237BE444DEC1344C44 ** get_address_of_pzweyUhGzpYFZIjeZCshaHZTBEkZ_0() { return &___pzweyUhGzpYFZIjeZCshaHZTBEkZ_0; }
	inline void set_pzweyUhGzpYFZIjeZCshaHZTBEkZ_0(Action_2_t0C40F2023DD772A0F3511D237BE444DEC1344C44 * value)
	{
		___pzweyUhGzpYFZIjeZCshaHZTBEkZ_0 = value;
		Il2CppCodeGenWriteBarrier((&___pzweyUhGzpYFZIjeZCshaHZTBEkZ_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T89F979F122D2D97D0748B0D4EA7CB9A6C4FE867F_H
#ifndef LISTTOOLS_TD14BE95825F9C16D0533E58B79EEB05ABD91F1B8_H
#define LISTTOOLS_TD14BE95825F9C16D0533E58B79EEB05ABD91F1B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.ListTools
struct  ListTools_tD14BE95825F9C16D0533E58B79EEB05ABD91F1B8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTTOOLS_TD14BE95825F9C16D0533E58B79EEB05ABD91F1B8_H
#ifndef MATHTOOLS_T63ABC37176B73299DB82FAB4F057063FD72DE9CA_H
#define MATHTOOLS_T63ABC37176B73299DB82FAB4F057063FD72DE9CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.MathTools
struct  MathTools_t63ABC37176B73299DB82FAB4F057063FD72DE9CA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHTOOLS_T63ABC37176B73299DB82FAB4F057063FD72DE9CA_H
#ifndef MISCTOOLS_T69EF30FD8289334C1C95BFCCCE668C607CD984B5_H
#define MISCTOOLS_T69EF30FD8289334C1C95BFCCCE668C607CD984B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.MiscTools
struct  MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5  : public RuntimeObject
{
public:

public:
};

struct MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields
{
public:
	// System.UInt32 Rewired.Utils.MiscTools::PilgZOgMdQkvLRQJTMdkLwdNSxcu
	uint32_t ___PilgZOgMdQkvLRQJTMdkLwdNSxcu_0;
	// System.UInt32 Rewired.Utils.MiscTools::MPNnPbKljEMTmQdcfVDEytIoERK
	uint32_t ___MPNnPbKljEMTmQdcfVDEytIoERK_1;
	// System.UInt32 Rewired.Utils.MiscTools::gFOCzooUWrVFdddyaCYWaBvPueX
	uint32_t ___gFOCzooUWrVFdddyaCYWaBvPueX_2;
	// System.Int32 Rewired.Utils.MiscTools::UAPhrSUGzfDxNANVwlkYYHdikQqB
	int32_t ___UAPhrSUGzfDxNANVwlkYYHdikQqB_3;
	// System.Int32 Rewired.Utils.MiscTools::MzBGEnkBVgiayhACZNrLHMLYXYh
	int32_t ___MzBGEnkBVgiayhACZNrLHMLYXYh_4;
	// System.Int32 Rewired.Utils.MiscTools::mAVtJlKnKLxuYqEZjOxyITXxDFE
	int32_t ___mAVtJlKnKLxuYqEZjOxyITXxDFE_5;

public:
	inline static int32_t get_offset_of_PilgZOgMdQkvLRQJTMdkLwdNSxcu_0() { return static_cast<int32_t>(offsetof(MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields, ___PilgZOgMdQkvLRQJTMdkLwdNSxcu_0)); }
	inline uint32_t get_PilgZOgMdQkvLRQJTMdkLwdNSxcu_0() const { return ___PilgZOgMdQkvLRQJTMdkLwdNSxcu_0; }
	inline uint32_t* get_address_of_PilgZOgMdQkvLRQJTMdkLwdNSxcu_0() { return &___PilgZOgMdQkvLRQJTMdkLwdNSxcu_0; }
	inline void set_PilgZOgMdQkvLRQJTMdkLwdNSxcu_0(uint32_t value)
	{
		___PilgZOgMdQkvLRQJTMdkLwdNSxcu_0 = value;
	}

	inline static int32_t get_offset_of_MPNnPbKljEMTmQdcfVDEytIoERK_1() { return static_cast<int32_t>(offsetof(MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields, ___MPNnPbKljEMTmQdcfVDEytIoERK_1)); }
	inline uint32_t get_MPNnPbKljEMTmQdcfVDEytIoERK_1() const { return ___MPNnPbKljEMTmQdcfVDEytIoERK_1; }
	inline uint32_t* get_address_of_MPNnPbKljEMTmQdcfVDEytIoERK_1() { return &___MPNnPbKljEMTmQdcfVDEytIoERK_1; }
	inline void set_MPNnPbKljEMTmQdcfVDEytIoERK_1(uint32_t value)
	{
		___MPNnPbKljEMTmQdcfVDEytIoERK_1 = value;
	}

	inline static int32_t get_offset_of_gFOCzooUWrVFdddyaCYWaBvPueX_2() { return static_cast<int32_t>(offsetof(MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields, ___gFOCzooUWrVFdddyaCYWaBvPueX_2)); }
	inline uint32_t get_gFOCzooUWrVFdddyaCYWaBvPueX_2() const { return ___gFOCzooUWrVFdddyaCYWaBvPueX_2; }
	inline uint32_t* get_address_of_gFOCzooUWrVFdddyaCYWaBvPueX_2() { return &___gFOCzooUWrVFdddyaCYWaBvPueX_2; }
	inline void set_gFOCzooUWrVFdddyaCYWaBvPueX_2(uint32_t value)
	{
		___gFOCzooUWrVFdddyaCYWaBvPueX_2 = value;
	}

	inline static int32_t get_offset_of_UAPhrSUGzfDxNANVwlkYYHdikQqB_3() { return static_cast<int32_t>(offsetof(MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields, ___UAPhrSUGzfDxNANVwlkYYHdikQqB_3)); }
	inline int32_t get_UAPhrSUGzfDxNANVwlkYYHdikQqB_3() const { return ___UAPhrSUGzfDxNANVwlkYYHdikQqB_3; }
	inline int32_t* get_address_of_UAPhrSUGzfDxNANVwlkYYHdikQqB_3() { return &___UAPhrSUGzfDxNANVwlkYYHdikQqB_3; }
	inline void set_UAPhrSUGzfDxNANVwlkYYHdikQqB_3(int32_t value)
	{
		___UAPhrSUGzfDxNANVwlkYYHdikQqB_3 = value;
	}

	inline static int32_t get_offset_of_MzBGEnkBVgiayhACZNrLHMLYXYh_4() { return static_cast<int32_t>(offsetof(MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields, ___MzBGEnkBVgiayhACZNrLHMLYXYh_4)); }
	inline int32_t get_MzBGEnkBVgiayhACZNrLHMLYXYh_4() const { return ___MzBGEnkBVgiayhACZNrLHMLYXYh_4; }
	inline int32_t* get_address_of_MzBGEnkBVgiayhACZNrLHMLYXYh_4() { return &___MzBGEnkBVgiayhACZNrLHMLYXYh_4; }
	inline void set_MzBGEnkBVgiayhACZNrLHMLYXYh_4(int32_t value)
	{
		___MzBGEnkBVgiayhACZNrLHMLYXYh_4 = value;
	}

	inline static int32_t get_offset_of_mAVtJlKnKLxuYqEZjOxyITXxDFE_5() { return static_cast<int32_t>(offsetof(MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields, ___mAVtJlKnKLxuYqEZjOxyITXxDFE_5)); }
	inline int32_t get_mAVtJlKnKLxuYqEZjOxyITXxDFE_5() const { return ___mAVtJlKnKLxuYqEZjOxyITXxDFE_5; }
	inline int32_t* get_address_of_mAVtJlKnKLxuYqEZjOxyITXxDFE_5() { return &___mAVtJlKnKLxuYqEZjOxyITXxDFE_5; }
	inline void set_mAVtJlKnKLxuYqEZjOxyITXxDFE_5(int32_t value)
	{
		___mAVtJlKnKLxuYqEZjOxyITXxDFE_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCTOOLS_T69EF30FD8289334C1C95BFCCCE668C607CD984B5_H
#ifndef NATIVETOOLS_T02DA8C9296FF32AE2A10A7374941F56D189135CD_H
#define NATIVETOOLS_T02DA8C9296FF32AE2A10A7374941F56D189135CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.NativeTools
struct  NativeTools_t02DA8C9296FF32AE2A10A7374941F56D189135CD  : public RuntimeObject
{
public:

public:
};

struct NativeTools_t02DA8C9296FF32AE2A10A7374941F56D189135CD_StaticFields
{
public:
	// System.Byte[] Rewired.Utils.NativeTools::wlpWfLZnkDmuKDulwuFevFQQsOu
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___wlpWfLZnkDmuKDulwuFevFQQsOu_0;

public:
	inline static int32_t get_offset_of_wlpWfLZnkDmuKDulwuFevFQQsOu_0() { return static_cast<int32_t>(offsetof(NativeTools_t02DA8C9296FF32AE2A10A7374941F56D189135CD_StaticFields, ___wlpWfLZnkDmuKDulwuFevFQQsOu_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_wlpWfLZnkDmuKDulwuFevFQQsOu_0() const { return ___wlpWfLZnkDmuKDulwuFevFQQsOu_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_wlpWfLZnkDmuKDulwuFevFQQsOu_0() { return &___wlpWfLZnkDmuKDulwuFevFQQsOu_0; }
	inline void set_wlpWfLZnkDmuKDulwuFevFQQsOu_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___wlpWfLZnkDmuKDulwuFevFQQsOu_0 = value;
		Il2CppCodeGenWriteBarrier((&___wlpWfLZnkDmuKDulwuFevFQQsOu_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVETOOLS_T02DA8C9296FF32AE2A10A7374941F56D189135CD_H
#ifndef REFLECTIONTOOLS_TC70FA2C798DE540AF05D03972E0EF9DE435AD1E9_H
#define REFLECTIONTOOLS_TC70FA2C798DE540AF05D03972E0EF9DE435AD1E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.ReflectionTools
struct  ReflectionTools_tC70FA2C798DE540AF05D03972E0EF9DE435AD1E9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONTOOLS_TC70FA2C798DE540AF05D03972E0EF9DE435AD1E9_H
#ifndef SAFEDELEGATE_T0297C74E3DC510F138FAB3D8C38128AEB0AA6506_H
#define SAFEDELEGATE_T0297C74E3DC510F138FAB3D8C38128AEB0AA6506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.SafeDelegate
struct  SafeDelegate_t0297C74E3DC510F138FAB3D8C38128AEB0AA6506  : public RuntimeObject
{
public:

public:
};

struct SafeDelegate_t0297C74E3DC510F138FAB3D8C38128AEB0AA6506_StaticFields
{
public:
	// System.Action`1<System.Exception> Rewired.Utils.SafeDelegate::ptPzBqHgnzLwCGDGNDvdnOmXLZc
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___ptPzBqHgnzLwCGDGNDvdnOmXLZc_0;

public:
	inline static int32_t get_offset_of_ptPzBqHgnzLwCGDGNDvdnOmXLZc_0() { return static_cast<int32_t>(offsetof(SafeDelegate_t0297C74E3DC510F138FAB3D8C38128AEB0AA6506_StaticFields, ___ptPzBqHgnzLwCGDGNDvdnOmXLZc_0)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_ptPzBqHgnzLwCGDGNDvdnOmXLZc_0() const { return ___ptPzBqHgnzLwCGDGNDvdnOmXLZc_0; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_ptPzBqHgnzLwCGDGNDvdnOmXLZc_0() { return &___ptPzBqHgnzLwCGDGNDvdnOmXLZc_0; }
	inline void set_ptPzBqHgnzLwCGDGNDvdnOmXLZc_0(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___ptPzBqHgnzLwCGDGNDvdnOmXLZc_0 = value;
		Il2CppCodeGenWriteBarrier((&___ptPzBqHgnzLwCGDGNDvdnOmXLZc_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEDELEGATE_T0297C74E3DC510F138FAB3D8C38128AEB0AA6506_H
#ifndef SERIALIZATIONTOOLS_T27FE4DC60DE61922AB7B56E79801C4719094FCA1_H
#define SERIALIZATIONTOOLS_T27FE4DC60DE61922AB7B56E79801C4719094FCA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.SerializationTools
struct  SerializationTools_t27FE4DC60DE61922AB7B56E79801C4719094FCA1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONTOOLS_T27FE4DC60DE61922AB7B56E79801C4719094FCA1_H
#ifndef STRINGTOOLS_T9CD9E74F8892509E42E19E040353881CBFA52D3C_H
#define STRINGTOOLS_T9CD9E74F8892509E42E19E040353881CBFA52D3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.StringTools
struct  StringTools_t9CD9E74F8892509E42E19E040353881CBFA52D3C  : public RuntimeObject
{
public:

public:
};

struct StringTools_t9CD9E74F8892509E42E19E040353881CBFA52D3C_StaticFields
{
public:
	// System.String Rewired.Utils.StringTools::uNJiAceKjYQFOnPGlpGqdnWHAyqA
	String_t* ___uNJiAceKjYQFOnPGlpGqdnWHAyqA_0;

public:
	inline static int32_t get_offset_of_uNJiAceKjYQFOnPGlpGqdnWHAyqA_0() { return static_cast<int32_t>(offsetof(StringTools_t9CD9E74F8892509E42E19E040353881CBFA52D3C_StaticFields, ___uNJiAceKjYQFOnPGlpGqdnWHAyqA_0)); }
	inline String_t* get_uNJiAceKjYQFOnPGlpGqdnWHAyqA_0() const { return ___uNJiAceKjYQFOnPGlpGqdnWHAyqA_0; }
	inline String_t** get_address_of_uNJiAceKjYQFOnPGlpGqdnWHAyqA_0() { return &___uNJiAceKjYQFOnPGlpGqdnWHAyqA_0; }
	inline void set_uNJiAceKjYQFOnPGlpGqdnWHAyqA_0(String_t* value)
	{
		___uNJiAceKjYQFOnPGlpGqdnWHAyqA_0 = value;
		Il2CppCodeGenWriteBarrier((&___uNJiAceKjYQFOnPGlpGqdnWHAyqA_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGTOOLS_T9CD9E74F8892509E42E19E040353881CBFA52D3C_H
#ifndef SYSTEMINFO_TCA2E859BC362FF7E45275F546C88750CB0BCE4D1_H
#define SYSTEMINFO_TCA2E859BC362FF7E45275F546C88750CB0BCE4D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.SystemInfo
struct  SystemInfo_tCA2E859BC362FF7E45275F546C88750CB0BCE4D1  : public RuntimeObject
{
public:

public:
};

struct SystemInfo_tCA2E859BC362FF7E45275F546C88750CB0BCE4D1_StaticFields
{
public:
	// System.Boolean Rewired.Utils.SystemInfo::is64Bit
	bool ___is64Bit_0;

public:
	inline static int32_t get_offset_of_is64Bit_0() { return static_cast<int32_t>(offsetof(SystemInfo_tCA2E859BC362FF7E45275F546C88750CB0BCE4D1_StaticFields, ___is64Bit_0)); }
	inline bool get_is64Bit_0() const { return ___is64Bit_0; }
	inline bool* get_address_of_is64Bit_0() { return &___is64Bit_0; }
	inline void set_is64Bit_0(bool value)
	{
		___is64Bit_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMINFO_TCA2E859BC362FF7E45275F546C88750CB0BCE4D1_H
#ifndef TEMPLISTPOOL_TD0311D26DDB7CA4D65DDA12E515D92B625142012_H
#define TEMPLISTPOOL_TD0311D26DDB7CA4D65DDA12E515D92B625142012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.TempListPool
struct  TempListPool_tD0311D26DDB7CA4D65DDA12E515D92B625142012  : public RuntimeObject
{
public:

public:
};

struct TempListPool_tD0311D26DDB7CA4D65DDA12E515D92B625142012_StaticFields
{
public:
	// Rewired.Utils.Classes.Data.ADictionary`2<System.Type,System.Collections.Generic.List`1<System.Collections.IList>> Rewired.Utils.TempListPool::pDlOHdwCqMQnSZVglpsIlLSNKfE
	ADictionary_2_t18CC0D77B50ADC14D160940E03DC5A92EF759931 * ___pDlOHdwCqMQnSZVglpsIlLSNKfE_2;

public:
	inline static int32_t get_offset_of_pDlOHdwCqMQnSZVglpsIlLSNKfE_2() { return static_cast<int32_t>(offsetof(TempListPool_tD0311D26DDB7CA4D65DDA12E515D92B625142012_StaticFields, ___pDlOHdwCqMQnSZVglpsIlLSNKfE_2)); }
	inline ADictionary_2_t18CC0D77B50ADC14D160940E03DC5A92EF759931 * get_pDlOHdwCqMQnSZVglpsIlLSNKfE_2() const { return ___pDlOHdwCqMQnSZVglpsIlLSNKfE_2; }
	inline ADictionary_2_t18CC0D77B50ADC14D160940E03DC5A92EF759931 ** get_address_of_pDlOHdwCqMQnSZVglpsIlLSNKfE_2() { return &___pDlOHdwCqMQnSZVglpsIlLSNKfE_2; }
	inline void set_pDlOHdwCqMQnSZVglpsIlLSNKfE_2(ADictionary_2_t18CC0D77B50ADC14D160940E03DC5A92EF759931 * value)
	{
		___pDlOHdwCqMQnSZVglpsIlLSNKfE_2 = value;
		Il2CppCodeGenWriteBarrier((&___pDlOHdwCqMQnSZVglpsIlLSNKfE_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEMPLISTPOOL_TD0311D26DDB7CA4D65DDA12E515D92B625142012_H
#ifndef VCTPRMGNXMABYWCYYMGEZBUCMCO_TF93C64F9612F5016E5B47BB42CF6449960D32BC7_H
#define VCTPRMGNXMABYWCYYMGEZBUCMCO_TF93C64F9612F5016E5B47BB42CF6449960D32BC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.TempListPool_vCtprMGnXmaBYwCYymGezBucMCO
struct  vCtprMGnXmaBYwCYymGezBucMCO_tF93C64F9612F5016E5B47BB42CF6449960D32BC7  : public RuntimeObject
{
public:

public:
};

struct vCtprMGnXmaBYwCYymGezBucMCO_tF93C64F9612F5016E5B47BB42CF6449960D32BC7_StaticFields
{
public:
	// Rewired.Utils.Classes.Data.ADictionary`2<System.Type,System.Collections.Generic.List`1<System.Object>> Rewired.Utils.TempListPool_vCtprMGnXmaBYwCYymGezBucMCO::rkXXtFzuqnQzeNiXkbZrpScYwkq
	ADictionary_2_t0634D596F2E2DFE76DA880E0F73742FC7D6EDE12 * ___rkXXtFzuqnQzeNiXkbZrpScYwkq_0;

public:
	inline static int32_t get_offset_of_rkXXtFzuqnQzeNiXkbZrpScYwkq_0() { return static_cast<int32_t>(offsetof(vCtprMGnXmaBYwCYymGezBucMCO_tF93C64F9612F5016E5B47BB42CF6449960D32BC7_StaticFields, ___rkXXtFzuqnQzeNiXkbZrpScYwkq_0)); }
	inline ADictionary_2_t0634D596F2E2DFE76DA880E0F73742FC7D6EDE12 * get_rkXXtFzuqnQzeNiXkbZrpScYwkq_0() const { return ___rkXXtFzuqnQzeNiXkbZrpScYwkq_0; }
	inline ADictionary_2_t0634D596F2E2DFE76DA880E0F73742FC7D6EDE12 ** get_address_of_rkXXtFzuqnQzeNiXkbZrpScYwkq_0() { return &___rkXXtFzuqnQzeNiXkbZrpScYwkq_0; }
	inline void set_rkXXtFzuqnQzeNiXkbZrpScYwkq_0(ADictionary_2_t0634D596F2E2DFE76DA880E0F73742FC7D6EDE12 * value)
	{
		___rkXXtFzuqnQzeNiXkbZrpScYwkq_0 = value;
		Il2CppCodeGenWriteBarrier((&___rkXXtFzuqnQzeNiXkbZrpScYwkq_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VCTPRMGNXMABYWCYYMGEZBUCMCO_TF93C64F9612F5016E5B47BB42CF6449960D32BC7_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef XXMVNQBKBGNCPIBRBZMHEMMCRNU_T28904CA63EAACBE256A9F029FAAE570922071D2A_H
#define XXMVNQBKBGNCPIBRBZMHEMMCRNU_T28904CA63EAACBE256A9F029FAAE570922071D2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// erAqfwCNbAAvnTADjtOsmRVrVxW_XxmVNqBKBGNcpIbRBzMhemMCRnu
struct  XxmVNqBKBGNcpIbRBzMhemMCRnu_t28904CA63EAACBE256A9F029FAAE570922071D2A  : public RuntimeObject
{
public:
	// System.Boolean erAqfwCNbAAvnTADjtOsmRVrVxW_XxmVNqBKBGNcpIbRBzMhemMCRnu::FwmafwnPPXAtBqTArlYOKFIyJoe
	bool ___FwmafwnPPXAtBqTArlYOKFIyJoe_0;
	// System.Boolean erAqfwCNbAAvnTADjtOsmRVrVxW_XxmVNqBKBGNcpIbRBzMhemMCRnu::UZbYDMRHqoMYGDwwWIwoKVeBvKy
	bool ___UZbYDMRHqoMYGDwwWIwoKVeBvKy_1;
	// System.Boolean erAqfwCNbAAvnTADjtOsmRVrVxW_XxmVNqBKBGNcpIbRBzMhemMCRnu::ODdrsnIXVvtMCTXbvFEXFLAJgJr
	bool ___ODdrsnIXVvtMCTXbvFEXFLAJgJr_2;

public:
	inline static int32_t get_offset_of_FwmafwnPPXAtBqTArlYOKFIyJoe_0() { return static_cast<int32_t>(offsetof(XxmVNqBKBGNcpIbRBzMhemMCRnu_t28904CA63EAACBE256A9F029FAAE570922071D2A, ___FwmafwnPPXAtBqTArlYOKFIyJoe_0)); }
	inline bool get_FwmafwnPPXAtBqTArlYOKFIyJoe_0() const { return ___FwmafwnPPXAtBqTArlYOKFIyJoe_0; }
	inline bool* get_address_of_FwmafwnPPXAtBqTArlYOKFIyJoe_0() { return &___FwmafwnPPXAtBqTArlYOKFIyJoe_0; }
	inline void set_FwmafwnPPXAtBqTArlYOKFIyJoe_0(bool value)
	{
		___FwmafwnPPXAtBqTArlYOKFIyJoe_0 = value;
	}

	inline static int32_t get_offset_of_UZbYDMRHqoMYGDwwWIwoKVeBvKy_1() { return static_cast<int32_t>(offsetof(XxmVNqBKBGNcpIbRBzMhemMCRnu_t28904CA63EAACBE256A9F029FAAE570922071D2A, ___UZbYDMRHqoMYGDwwWIwoKVeBvKy_1)); }
	inline bool get_UZbYDMRHqoMYGDwwWIwoKVeBvKy_1() const { return ___UZbYDMRHqoMYGDwwWIwoKVeBvKy_1; }
	inline bool* get_address_of_UZbYDMRHqoMYGDwwWIwoKVeBvKy_1() { return &___UZbYDMRHqoMYGDwwWIwoKVeBvKy_1; }
	inline void set_UZbYDMRHqoMYGDwwWIwoKVeBvKy_1(bool value)
	{
		___UZbYDMRHqoMYGDwwWIwoKVeBvKy_1 = value;
	}

	inline static int32_t get_offset_of_ODdrsnIXVvtMCTXbvFEXFLAJgJr_2() { return static_cast<int32_t>(offsetof(XxmVNqBKBGNcpIbRBzMhemMCRnu_t28904CA63EAACBE256A9F029FAAE570922071D2A, ___ODdrsnIXVvtMCTXbvFEXFLAJgJr_2)); }
	inline bool get_ODdrsnIXVvtMCTXbvFEXFLAJgJr_2() const { return ___ODdrsnIXVvtMCTXbvFEXFLAJgJr_2; }
	inline bool* get_address_of_ODdrsnIXVvtMCTXbvFEXFLAJgJr_2() { return &___ODdrsnIXVvtMCTXbvFEXFLAJgJr_2; }
	inline void set_ODdrsnIXVvtMCTXbvFEXFLAJgJr_2(bool value)
	{
		___ODdrsnIXVvtMCTXbvFEXFLAJgJr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XXMVNQBKBGNCPIBRBZMHEMMCRNU_T28904CA63EAACBE256A9F029FAAE570922071D2A_H
#ifndef HGARHCRBIBGMKKAWQLNQBKSRVYK_T41256CAA11A811B0C96971EF489A5FAB76506B0A_H
#define HGARHCRBIBGMKKAWQLNQBKSRVYK_T41256CAA11A811B0C96971EF489A5FAB76506B0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// hgArHCrBiBgmkkaWqlNqbKsRVyk
struct  hgArHCrBiBgmkkaWqlNqbKsRVyk_t41256CAA11A811B0C96971EF489A5FAB76506B0A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HGARHCRBIBGMKKAWQLNQBKSRVYK_T41256CAA11A811B0C96971EF489A5FAB76506B0A_H
#ifndef OBOBAJTXXZOWBMFHJAOIYGLMGZKA_TD4FB95EE6B78B67A70D059DE6AADCAD15649E3A4_H
#define OBOBAJTXXZOWBMFHJAOIYGLMGZKA_TD4FB95EE6B78B67A70D059DE6AADCAD15649E3A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// oboBAJTxxzoWBMFHJAOIyglMGzKA
struct  oboBAJTxxzoWBMFHJAOIyglMGzKA_tD4FB95EE6B78B67A70D059DE6AADCAD15649E3A4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBOBAJTXXZOWBMFHJAOIYGLMGZKA_TD4FB95EE6B78B67A70D059DE6AADCAD15649E3A4_H
#ifndef FLOAT2X_TBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_H
#define FLOAT2X_TBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.Float2x
struct  Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3 
{
public:
	// System.Single Rewired.Utils.Classes.Data.Float2x::a
	float ___a_1;
	// System.Single Rewired.Utils.Classes.Data.Float2x::b
	float ___b_2;

public:
	inline static int32_t get_offset_of_a_1() { return static_cast<int32_t>(offsetof(Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3, ___a_1)); }
	inline float get_a_1() const { return ___a_1; }
	inline float* get_address_of_a_1() { return &___a_1; }
	inline void set_a_1(float value)
	{
		___a_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}
};

struct Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_StaticFields
{
public:
	// System.Func`3<Rewired.Utils.Classes.Data.Float2x,Rewired.Utils.Classes.Data.Float2x,Rewired.Utils.Classes.Data.Float2x> Rewired.Utils.Classes.Data.Float2x::_additionDelegate
	Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB * ____additionDelegate_3;
	// System.Func`3<Rewired.Utils.Classes.Data.Float2x,Rewired.Utils.Classes.Data.Float2x,Rewired.Utils.Classes.Data.Float2x> Rewired.Utils.Classes.Data.Float2x::_subtractionDelegate
	Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB * ____subtractionDelegate_4;
	// System.Func`3<Rewired.Utils.Classes.Data.Float2x,Rewired.Utils.Classes.Data.Float2x,Rewired.Utils.Classes.Data.Float2x> Rewired.Utils.Classes.Data.Float2x::_multiplicationDelegate
	Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB * ____multiplicationDelegate_5;
	// System.Func`3<Rewired.Utils.Classes.Data.Float2x,Rewired.Utils.Classes.Data.Float2x,Rewired.Utils.Classes.Data.Float2x> Rewired.Utils.Classes.Data.Float2x::_divisionDelegate
	Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB * ____divisionDelegate_6;

public:
	inline static int32_t get_offset_of__additionDelegate_3() { return static_cast<int32_t>(offsetof(Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_StaticFields, ____additionDelegate_3)); }
	inline Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB * get__additionDelegate_3() const { return ____additionDelegate_3; }
	inline Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB ** get_address_of__additionDelegate_3() { return &____additionDelegate_3; }
	inline void set__additionDelegate_3(Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB * value)
	{
		____additionDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((&____additionDelegate_3), value);
	}

	inline static int32_t get_offset_of__subtractionDelegate_4() { return static_cast<int32_t>(offsetof(Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_StaticFields, ____subtractionDelegate_4)); }
	inline Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB * get__subtractionDelegate_4() const { return ____subtractionDelegate_4; }
	inline Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB ** get_address_of__subtractionDelegate_4() { return &____subtractionDelegate_4; }
	inline void set__subtractionDelegate_4(Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB * value)
	{
		____subtractionDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((&____subtractionDelegate_4), value);
	}

	inline static int32_t get_offset_of__multiplicationDelegate_5() { return static_cast<int32_t>(offsetof(Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_StaticFields, ____multiplicationDelegate_5)); }
	inline Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB * get__multiplicationDelegate_5() const { return ____multiplicationDelegate_5; }
	inline Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB ** get_address_of__multiplicationDelegate_5() { return &____multiplicationDelegate_5; }
	inline void set__multiplicationDelegate_5(Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB * value)
	{
		____multiplicationDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((&____multiplicationDelegate_5), value);
	}

	inline static int32_t get_offset_of__divisionDelegate_6() { return static_cast<int32_t>(offsetof(Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_StaticFields, ____divisionDelegate_6)); }
	inline Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB * get__divisionDelegate_6() const { return ____divisionDelegate_6; }
	inline Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB ** get_address_of__divisionDelegate_6() { return &____divisionDelegate_6; }
	inline void set__divisionDelegate_6(Func_3_t32EEF0C151A91874B6E84A5B071D5D5BA33DBECB * value)
	{
		____divisionDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((&____divisionDelegate_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOAT2X_TBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_H
#ifndef FLOAT3X_T6E72B93448627376705F47B6FB2119DA743E6B8C_H
#define FLOAT3X_T6E72B93448627376705F47B6FB2119DA743E6B8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.Float3x
struct  Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C 
{
public:
	// System.Single Rewired.Utils.Classes.Data.Float3x::a
	float ___a_1;
	// System.Single Rewired.Utils.Classes.Data.Float3x::b
	float ___b_2;
	// System.Single Rewired.Utils.Classes.Data.Float3x::c
	float ___c_3;

public:
	inline static int32_t get_offset_of_a_1() { return static_cast<int32_t>(offsetof(Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C, ___a_1)); }
	inline float get_a_1() const { return ___a_1; }
	inline float* get_address_of_a_1() { return &___a_1; }
	inline void set_a_1(float value)
	{
		___a_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_c_3() { return static_cast<int32_t>(offsetof(Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C, ___c_3)); }
	inline float get_c_3() const { return ___c_3; }
	inline float* get_address_of_c_3() { return &___c_3; }
	inline void set_c_3(float value)
	{
		___c_3 = value;
	}
};

struct Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C_StaticFields
{
public:
	// System.Func`3<Rewired.Utils.Classes.Data.Float3x,Rewired.Utils.Classes.Data.Float3x,Rewired.Utils.Classes.Data.Float3x> Rewired.Utils.Classes.Data.Float3x::_additionDelegate
	Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 * ____additionDelegate_4;
	// System.Func`3<Rewired.Utils.Classes.Data.Float3x,Rewired.Utils.Classes.Data.Float3x,Rewired.Utils.Classes.Data.Float3x> Rewired.Utils.Classes.Data.Float3x::_subtractionDelegate
	Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 * ____subtractionDelegate_5;
	// System.Func`3<Rewired.Utils.Classes.Data.Float3x,Rewired.Utils.Classes.Data.Float3x,Rewired.Utils.Classes.Data.Float3x> Rewired.Utils.Classes.Data.Float3x::_multiplicationDelegate
	Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 * ____multiplicationDelegate_6;
	// System.Func`3<Rewired.Utils.Classes.Data.Float3x,Rewired.Utils.Classes.Data.Float3x,Rewired.Utils.Classes.Data.Float3x> Rewired.Utils.Classes.Data.Float3x::_divisionDelegate
	Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 * ____divisionDelegate_7;

public:
	inline static int32_t get_offset_of__additionDelegate_4() { return static_cast<int32_t>(offsetof(Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C_StaticFields, ____additionDelegate_4)); }
	inline Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 * get__additionDelegate_4() const { return ____additionDelegate_4; }
	inline Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 ** get_address_of__additionDelegate_4() { return &____additionDelegate_4; }
	inline void set__additionDelegate_4(Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 * value)
	{
		____additionDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((&____additionDelegate_4), value);
	}

	inline static int32_t get_offset_of__subtractionDelegate_5() { return static_cast<int32_t>(offsetof(Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C_StaticFields, ____subtractionDelegate_5)); }
	inline Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 * get__subtractionDelegate_5() const { return ____subtractionDelegate_5; }
	inline Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 ** get_address_of__subtractionDelegate_5() { return &____subtractionDelegate_5; }
	inline void set__subtractionDelegate_5(Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 * value)
	{
		____subtractionDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((&____subtractionDelegate_5), value);
	}

	inline static int32_t get_offset_of__multiplicationDelegate_6() { return static_cast<int32_t>(offsetof(Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C_StaticFields, ____multiplicationDelegate_6)); }
	inline Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 * get__multiplicationDelegate_6() const { return ____multiplicationDelegate_6; }
	inline Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 ** get_address_of__multiplicationDelegate_6() { return &____multiplicationDelegate_6; }
	inline void set__multiplicationDelegate_6(Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 * value)
	{
		____multiplicationDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((&____multiplicationDelegate_6), value);
	}

	inline static int32_t get_offset_of__divisionDelegate_7() { return static_cast<int32_t>(offsetof(Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C_StaticFields, ____divisionDelegate_7)); }
	inline Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 * get__divisionDelegate_7() const { return ____divisionDelegate_7; }
	inline Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 ** get_address_of__divisionDelegate_7() { return &____divisionDelegate_7; }
	inline void set__divisionDelegate_7(Func_3_tEEFB17F4D00C447253FAA24E2C7865EBBDDC26A6 * value)
	{
		____divisionDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((&____divisionDelegate_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOAT3X_T6E72B93448627376705F47B6FB2119DA743E6B8C_H
#ifndef FLOAT4X_T0A5708BC5B27ABD79D27C864E666BF0B01119BC7_H
#define FLOAT4X_T0A5708BC5B27ABD79D27C864E666BF0B01119BC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.Float4x
struct  Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7 
{
public:
	// System.Single Rewired.Utils.Classes.Data.Float4x::a
	float ___a_1;
	// System.Single Rewired.Utils.Classes.Data.Float4x::b
	float ___b_2;
	// System.Single Rewired.Utils.Classes.Data.Float4x::c
	float ___c_3;
	// System.Single Rewired.Utils.Classes.Data.Float4x::d
	float ___d_4;

public:
	inline static int32_t get_offset_of_a_1() { return static_cast<int32_t>(offsetof(Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7, ___a_1)); }
	inline float get_a_1() const { return ___a_1; }
	inline float* get_address_of_a_1() { return &___a_1; }
	inline void set_a_1(float value)
	{
		___a_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_c_3() { return static_cast<int32_t>(offsetof(Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7, ___c_3)); }
	inline float get_c_3() const { return ___c_3; }
	inline float* get_address_of_c_3() { return &___c_3; }
	inline void set_c_3(float value)
	{
		___c_3 = value;
	}

	inline static int32_t get_offset_of_d_4() { return static_cast<int32_t>(offsetof(Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7, ___d_4)); }
	inline float get_d_4() const { return ___d_4; }
	inline float* get_address_of_d_4() { return &___d_4; }
	inline void set_d_4(float value)
	{
		___d_4 = value;
	}
};

struct Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7_StaticFields
{
public:
	// System.Func`3<Rewired.Utils.Classes.Data.Float4x,Rewired.Utils.Classes.Data.Float4x,Rewired.Utils.Classes.Data.Float4x> Rewired.Utils.Classes.Data.Float4x::_additionDelegate
	Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 * ____additionDelegate_5;
	// System.Func`3<Rewired.Utils.Classes.Data.Float4x,Rewired.Utils.Classes.Data.Float4x,Rewired.Utils.Classes.Data.Float4x> Rewired.Utils.Classes.Data.Float4x::_subtractionDelegate
	Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 * ____subtractionDelegate_6;
	// System.Func`3<Rewired.Utils.Classes.Data.Float4x,Rewired.Utils.Classes.Data.Float4x,Rewired.Utils.Classes.Data.Float4x> Rewired.Utils.Classes.Data.Float4x::_multiplicationDelegate
	Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 * ____multiplicationDelegate_7;
	// System.Func`3<Rewired.Utils.Classes.Data.Float4x,Rewired.Utils.Classes.Data.Float4x,Rewired.Utils.Classes.Data.Float4x> Rewired.Utils.Classes.Data.Float4x::_divisionDelegate
	Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 * ____divisionDelegate_8;

public:
	inline static int32_t get_offset_of__additionDelegate_5() { return static_cast<int32_t>(offsetof(Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7_StaticFields, ____additionDelegate_5)); }
	inline Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 * get__additionDelegate_5() const { return ____additionDelegate_5; }
	inline Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 ** get_address_of__additionDelegate_5() { return &____additionDelegate_5; }
	inline void set__additionDelegate_5(Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 * value)
	{
		____additionDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((&____additionDelegate_5), value);
	}

	inline static int32_t get_offset_of__subtractionDelegate_6() { return static_cast<int32_t>(offsetof(Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7_StaticFields, ____subtractionDelegate_6)); }
	inline Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 * get__subtractionDelegate_6() const { return ____subtractionDelegate_6; }
	inline Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 ** get_address_of__subtractionDelegate_6() { return &____subtractionDelegate_6; }
	inline void set__subtractionDelegate_6(Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 * value)
	{
		____subtractionDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((&____subtractionDelegate_6), value);
	}

	inline static int32_t get_offset_of__multiplicationDelegate_7() { return static_cast<int32_t>(offsetof(Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7_StaticFields, ____multiplicationDelegate_7)); }
	inline Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 * get__multiplicationDelegate_7() const { return ____multiplicationDelegate_7; }
	inline Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 ** get_address_of__multiplicationDelegate_7() { return &____multiplicationDelegate_7; }
	inline void set__multiplicationDelegate_7(Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 * value)
	{
		____multiplicationDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((&____multiplicationDelegate_7), value);
	}

	inline static int32_t get_offset_of__divisionDelegate_8() { return static_cast<int32_t>(offsetof(Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7_StaticFields, ____divisionDelegate_8)); }
	inline Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 * get__divisionDelegate_8() const { return ____divisionDelegate_8; }
	inline Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 ** get_address_of__divisionDelegate_8() { return &____divisionDelegate_8; }
	inline void set__divisionDelegate_8(Func_3_t654F3FDD4A08C82B407464923BD8823A6EAEFA30 * value)
	{
		____divisionDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((&____divisionDelegate_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOAT4X_T0A5708BC5B27ABD79D27C864E666BF0B01119BC7_H
#ifndef SCREENRECT_T3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14_H
#define SCREENRECT_T3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.ScreenRect
struct  ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14 
{
public:
	// System.Single Rewired.Utils.Classes.Data.ScreenRect::xMin
	float ___xMin_0;
	// System.Single Rewired.Utils.Classes.Data.ScreenRect::yMin
	float ___yMin_1;
	// System.Single Rewired.Utils.Classes.Data.ScreenRect::width
	float ___width_2;
	// System.Single Rewired.Utils.Classes.Data.ScreenRect::height
	float ___height_3;

public:
	inline static int32_t get_offset_of_xMin_0() { return static_cast<int32_t>(offsetof(ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14, ___xMin_0)); }
	inline float get_xMin_0() const { return ___xMin_0; }
	inline float* get_address_of_xMin_0() { return &___xMin_0; }
	inline void set_xMin_0(float value)
	{
		___xMin_0 = value;
	}

	inline static int32_t get_offset_of_yMin_1() { return static_cast<int32_t>(offsetof(ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14, ___yMin_1)); }
	inline float get_yMin_1() const { return ___yMin_1; }
	inline float* get_address_of_yMin_1() { return &___yMin_1; }
	inline void set_yMin_1(float value)
	{
		___yMin_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14, ___width_2)); }
	inline float get_width_2() const { return ___width_2; }
	inline float* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(float value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14, ___height_3)); }
	inline float get_height_3() const { return ___height_3; }
	inline float* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(float value)
	{
		___height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENRECT_T3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14_H
#ifndef XMLSTRINGATTRIBUTE_T457BC831FBBFB17CD3F7F610F5337F282E3E6159_H
#define XMLSTRINGATTRIBUTE_T457BC831FBBFB17CD3F7F610F5337F282E3E6159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.SerializedObject_XmlInfo_XmlStringAttribute
struct  XmlStringAttribute_t457BC831FBBFB17CD3F7F610F5337F282E3E6159  : public XmlAttribute_t654C37181E9CB42791E326F78F30E4352E521DC4
{
public:
	// System.String Rewired.Utils.Classes.Data.SerializedObject_XmlInfo_XmlStringAttribute::prefix
	String_t* ___prefix_0;
	// System.String Rewired.Utils.Classes.Data.SerializedObject_XmlInfo_XmlStringAttribute::localName
	String_t* ___localName_1;
	// System.String Rewired.Utils.Classes.Data.SerializedObject_XmlInfo_XmlStringAttribute::ns
	String_t* ___ns_2;
	// System.String Rewired.Utils.Classes.Data.SerializedObject_XmlInfo_XmlStringAttribute::value
	String_t* ___value_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(XmlStringAttribute_t457BC831FBBFB17CD3F7F610F5337F282E3E6159, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_localName_1() { return static_cast<int32_t>(offsetof(XmlStringAttribute_t457BC831FBBFB17CD3F7F610F5337F282E3E6159, ___localName_1)); }
	inline String_t* get_localName_1() const { return ___localName_1; }
	inline String_t** get_address_of_localName_1() { return &___localName_1; }
	inline void set_localName_1(String_t* value)
	{
		___localName_1 = value;
		Il2CppCodeGenWriteBarrier((&___localName_1), value);
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlStringAttribute_t457BC831FBBFB17CD3F7F610F5337F282E3E6159, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((&___ns_2), value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(XmlStringAttribute_t457BC831FBBFB17CD3F7F610F5337F282E3E6159, ___value_3)); }
	inline String_t* get_value_3() const { return ___value_3; }
	inline String_t** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(String_t* value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSTRINGATTRIBUTE_T457BC831FBBFB17CD3F7F610F5337F282E3E6159_H
#ifndef DONOTSERIALIZEATTRIBUTE_TE1E9808B80F5508B06D839021C51B7AA79288F4E_H
#define DONOTSERIALIZEATTRIBUTE_TE1E9808B80F5508B06D839021C51B7AA79288F4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Libraries.TinyJson.DoNotSerializeAttribute
struct  DoNotSerializeAttribute_tE1E9808B80F5508B06D839021C51B7AA79288F4E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONOTSERIALIZEATTRIBUTE_TE1E9808B80F5508B06D839021C51B7AA79288F4E_H
#ifndef SERIALIZEATTRIBUTE_TF250E6F5010E384B303C2E6395DC56C5098A3DB4_H
#define SERIALIZEATTRIBUTE_TF250E6F5010E384B303C2E6395DC56C5098A3DB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Libraries.TinyJson.SerializeAttribute
struct  SerializeAttribute_tF250E6F5010E384B303C2E6395DC56C5098A3DB4  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Rewired.Utils.Libraries.TinyJson.SerializeAttribute::Name
	String_t* ___Name_0;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(SerializeAttribute_tF250E6F5010E384B303C2E6395DC56C5098A3DB4, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEATTRIBUTE_TF250E6F5010E384B303C2E6395DC56C5098A3DB4_H
#ifndef SAFEDELEGATE_1_T6610170CD17271905495F29D060B38C3BCCCC4F3_H
#define SAFEDELEGATE_1_T6610170CD17271905495F29D060B38C3BCCCC4F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.SafeDelegate`1<System.Action>
struct  SafeDelegate_1_t6610170CD17271905495F29D060B38C3BCCCC4F3  : public SafeDelegate_t0297C74E3DC510F138FAB3D8C38128AEB0AA6506
{
public:
	// System.Action`1<System.Exception> Rewired.Utils.SafeDelegate`1::MjpiiejKmpwBmypnUzxaDeHcnJN
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___MjpiiejKmpwBmypnUzxaDeHcnJN_1;
	// System.Collections.Generic.List`1<Rewired.Utils.SafeDelegate`1_XHqyovzaINqHymgNffvhSMRXkfh<T>> Rewired.Utils.SafeDelegate`1::sAbTIEHjrJhtWCxZrVCCwXzcsld
	List_1_t7A77DFF4E032CCBB8F2A4E9630724EFEFCF48417 * ___sAbTIEHjrJhtWCxZrVCCwXzcsld_2;
	// System.Collections.Generic.List`1<Rewired.Utils.SafeDelegate`1_XHqyovzaINqHymgNffvhSMRXkfh<T>> Rewired.Utils.SafeDelegate`1::vrirGqtYJysYFbtOFHFgUFGrebpB
	List_1_t7A77DFF4E032CCBB8F2A4E9630724EFEFCF48417 * ___vrirGqtYJysYFbtOFHFgUFGrebpB_3;

public:
	inline static int32_t get_offset_of_MjpiiejKmpwBmypnUzxaDeHcnJN_1() { return static_cast<int32_t>(offsetof(SafeDelegate_1_t6610170CD17271905495F29D060B38C3BCCCC4F3, ___MjpiiejKmpwBmypnUzxaDeHcnJN_1)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_MjpiiejKmpwBmypnUzxaDeHcnJN_1() const { return ___MjpiiejKmpwBmypnUzxaDeHcnJN_1; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_MjpiiejKmpwBmypnUzxaDeHcnJN_1() { return &___MjpiiejKmpwBmypnUzxaDeHcnJN_1; }
	inline void set_MjpiiejKmpwBmypnUzxaDeHcnJN_1(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___MjpiiejKmpwBmypnUzxaDeHcnJN_1 = value;
		Il2CppCodeGenWriteBarrier((&___MjpiiejKmpwBmypnUzxaDeHcnJN_1), value);
	}

	inline static int32_t get_offset_of_sAbTIEHjrJhtWCxZrVCCwXzcsld_2() { return static_cast<int32_t>(offsetof(SafeDelegate_1_t6610170CD17271905495F29D060B38C3BCCCC4F3, ___sAbTIEHjrJhtWCxZrVCCwXzcsld_2)); }
	inline List_1_t7A77DFF4E032CCBB8F2A4E9630724EFEFCF48417 * get_sAbTIEHjrJhtWCxZrVCCwXzcsld_2() const { return ___sAbTIEHjrJhtWCxZrVCCwXzcsld_2; }
	inline List_1_t7A77DFF4E032CCBB8F2A4E9630724EFEFCF48417 ** get_address_of_sAbTIEHjrJhtWCxZrVCCwXzcsld_2() { return &___sAbTIEHjrJhtWCxZrVCCwXzcsld_2; }
	inline void set_sAbTIEHjrJhtWCxZrVCCwXzcsld_2(List_1_t7A77DFF4E032CCBB8F2A4E9630724EFEFCF48417 * value)
	{
		___sAbTIEHjrJhtWCxZrVCCwXzcsld_2 = value;
		Il2CppCodeGenWriteBarrier((&___sAbTIEHjrJhtWCxZrVCCwXzcsld_2), value);
	}

	inline static int32_t get_offset_of_vrirGqtYJysYFbtOFHFgUFGrebpB_3() { return static_cast<int32_t>(offsetof(SafeDelegate_1_t6610170CD17271905495F29D060B38C3BCCCC4F3, ___vrirGqtYJysYFbtOFHFgUFGrebpB_3)); }
	inline List_1_t7A77DFF4E032CCBB8F2A4E9630724EFEFCF48417 * get_vrirGqtYJysYFbtOFHFgUFGrebpB_3() const { return ___vrirGqtYJysYFbtOFHFgUFGrebpB_3; }
	inline List_1_t7A77DFF4E032CCBB8F2A4E9630724EFEFCF48417 ** get_address_of_vrirGqtYJysYFbtOFHFgUFGrebpB_3() { return &___vrirGqtYJysYFbtOFHFgUFGrebpB_3; }
	inline void set_vrirGqtYJysYFbtOFHFgUFGrebpB_3(List_1_t7A77DFF4E032CCBB8F2A4E9630724EFEFCF48417 * value)
	{
		___vrirGqtYJysYFbtOFHFgUFGrebpB_3 = value;
		Il2CppCodeGenWriteBarrier((&___vrirGqtYJysYFbtOFHFgUFGrebpB_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEDELEGATE_1_T6610170CD17271905495F29D060B38C3BCCCC4F3_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TYDFJIILEUXIFZMYRYPSYWMPMWA_T8340A7D91096EED258E5C91C215B70F279460FE8_H
#define TYDFJIILEUXIFZMYRYPSYWMPMWA_T8340A7D91096EED258E5C91C215B70F279460FE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TYDfjiilEUxIfZMyRypsywmpmwA
struct  TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte TYDfjiilEUxIfZMyRypsywmpmwA::wTYLYalPavcctjXZrASJNkapMaJ
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.SByte TYDfjiilEUxIfZMyRypsywmpmwA::MJrfBrxoBTvDrzGeBrTAImwmNOZ
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Char TYDfjiilEUxIfZMyRypsywmpmwA::euLduoAiVMxRQGLWMCGBXUDrCKr
			Il2CppChar ___euLduoAiVMxRQGLWMCGBXUDrCKr_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppChar ___euLduoAiVMxRQGLWMCGBXUDrCKr_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int16 TYDfjiilEUxIfZMyRypsywmpmwA::BlWgkqEeUMGgBlbpbsWvFckFbDv
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt16 TYDfjiilEUxIfZMyRypsywmpmwA::WBbwvCOIPVbAJbjWvqCbJElmQLtd
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 TYDfjiilEUxIfZMyRypsywmpmwA::lxdbJzaQWeBlRYInsjMuuXhyCggl
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt32 TYDfjiilEUxIfZMyRypsywmpmwA::evDemCrkcBEYeHncrlFUdsIyAKPa
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int64 TYDfjiilEUxIfZMyRypsywmpmwA::BUoNfFowxTfWVzlGeZVAXYsTHeZ
			int64_t ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt64 TYDfjiilEUxIfZMyRypsywmpmwA::UgJnYbycrRGWLWQVxOsnvCHCOdO
			uint64_t ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint64_t ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single TYDfjiilEUxIfZMyRypsywmpmwA::mUqspDOssdvKUAQvOxwRVdbooCb
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Double TYDfjiilEUxIfZMyRypsywmpmwA::niPvDktNKWYPHmgqlUoZPBssHhI
			double ___niPvDktNKWYPHmgqlUoZPBssHhI_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___niPvDktNKWYPHmgqlUoZPBssHhI_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Boolean TYDfjiilEUxIfZMyRypsywmpmwA::HqWkThUinZjstdlLHSIImaHNnAis
			bool ___HqWkThUinZjstdlLHSIImaHNnAis_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			bool ___HqWkThUinZjstdlLHSIImaHNnAis_11_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_wTYLYalPavcctjXZrASJNkapMaJ_0() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___wTYLYalPavcctjXZrASJNkapMaJ_0)); }
	inline uint8_t get_wTYLYalPavcctjXZrASJNkapMaJ_0() const { return ___wTYLYalPavcctjXZrASJNkapMaJ_0; }
	inline uint8_t* get_address_of_wTYLYalPavcctjXZrASJNkapMaJ_0() { return &___wTYLYalPavcctjXZrASJNkapMaJ_0; }
	inline void set_wTYLYalPavcctjXZrASJNkapMaJ_0(uint8_t value)
	{
		___wTYLYalPavcctjXZrASJNkapMaJ_0 = value;
	}

	inline static int32_t get_offset_of_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1)); }
	inline int8_t get_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() const { return ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1; }
	inline int8_t* get_address_of_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() { return &___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1; }
	inline void set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1(int8_t value)
	{
		___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1 = value;
	}

	inline static int32_t get_offset_of_euLduoAiVMxRQGLWMCGBXUDrCKr_2() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___euLduoAiVMxRQGLWMCGBXUDrCKr_2)); }
	inline Il2CppChar get_euLduoAiVMxRQGLWMCGBXUDrCKr_2() const { return ___euLduoAiVMxRQGLWMCGBXUDrCKr_2; }
	inline Il2CppChar* get_address_of_euLduoAiVMxRQGLWMCGBXUDrCKr_2() { return &___euLduoAiVMxRQGLWMCGBXUDrCKr_2; }
	inline void set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(Il2CppChar value)
	{
		___euLduoAiVMxRQGLWMCGBXUDrCKr_2 = value;
	}

	inline static int32_t get_offset_of_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3)); }
	inline int16_t get_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() const { return ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3; }
	inline int16_t* get_address_of_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() { return &___BlWgkqEeUMGgBlbpbsWvFckFbDv_3; }
	inline void set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3(int16_t value)
	{
		___BlWgkqEeUMGgBlbpbsWvFckFbDv_3 = value;
	}

	inline static int32_t get_offset_of_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4)); }
	inline uint16_t get_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() const { return ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4; }
	inline uint16_t* get_address_of_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() { return &___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4; }
	inline void set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4(uint16_t value)
	{
		___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4 = value;
	}

	inline static int32_t get_offset_of_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5)); }
	inline int32_t get_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() const { return ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5; }
	inline int32_t* get_address_of_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() { return &___lxdbJzaQWeBlRYInsjMuuXhyCggl_5; }
	inline void set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(int32_t value)
	{
		___lxdbJzaQWeBlRYInsjMuuXhyCggl_5 = value;
	}

	inline static int32_t get_offset_of_evDemCrkcBEYeHncrlFUdsIyAKPa_6() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___evDemCrkcBEYeHncrlFUdsIyAKPa_6)); }
	inline uint32_t get_evDemCrkcBEYeHncrlFUdsIyAKPa_6() const { return ___evDemCrkcBEYeHncrlFUdsIyAKPa_6; }
	inline uint32_t* get_address_of_evDemCrkcBEYeHncrlFUdsIyAKPa_6() { return &___evDemCrkcBEYeHncrlFUdsIyAKPa_6; }
	inline void set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(uint32_t value)
	{
		___evDemCrkcBEYeHncrlFUdsIyAKPa_6 = value;
	}

	inline static int32_t get_offset_of_BUoNfFowxTfWVzlGeZVAXYsTHeZ_7() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7)); }
	inline int64_t get_BUoNfFowxTfWVzlGeZVAXYsTHeZ_7() const { return ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7; }
	inline int64_t* get_address_of_BUoNfFowxTfWVzlGeZVAXYsTHeZ_7() { return &___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7; }
	inline void set_BUoNfFowxTfWVzlGeZVAXYsTHeZ_7(int64_t value)
	{
		___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7 = value;
	}

	inline static int32_t get_offset_of_UgJnYbycrRGWLWQVxOsnvCHCOdO_8() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8)); }
	inline uint64_t get_UgJnYbycrRGWLWQVxOsnvCHCOdO_8() const { return ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8; }
	inline uint64_t* get_address_of_UgJnYbycrRGWLWQVxOsnvCHCOdO_8() { return &___UgJnYbycrRGWLWQVxOsnvCHCOdO_8; }
	inline void set_UgJnYbycrRGWLWQVxOsnvCHCOdO_8(uint64_t value)
	{
		___UgJnYbycrRGWLWQVxOsnvCHCOdO_8 = value;
	}

	inline static int32_t get_offset_of_mUqspDOssdvKUAQvOxwRVdbooCb_9() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___mUqspDOssdvKUAQvOxwRVdbooCb_9)); }
	inline float get_mUqspDOssdvKUAQvOxwRVdbooCb_9() const { return ___mUqspDOssdvKUAQvOxwRVdbooCb_9; }
	inline float* get_address_of_mUqspDOssdvKUAQvOxwRVdbooCb_9() { return &___mUqspDOssdvKUAQvOxwRVdbooCb_9; }
	inline void set_mUqspDOssdvKUAQvOxwRVdbooCb_9(float value)
	{
		___mUqspDOssdvKUAQvOxwRVdbooCb_9 = value;
	}

	inline static int32_t get_offset_of_niPvDktNKWYPHmgqlUoZPBssHhI_10() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___niPvDktNKWYPHmgqlUoZPBssHhI_10)); }
	inline double get_niPvDktNKWYPHmgqlUoZPBssHhI_10() const { return ___niPvDktNKWYPHmgqlUoZPBssHhI_10; }
	inline double* get_address_of_niPvDktNKWYPHmgqlUoZPBssHhI_10() { return &___niPvDktNKWYPHmgqlUoZPBssHhI_10; }
	inline void set_niPvDktNKWYPHmgqlUoZPBssHhI_10(double value)
	{
		___niPvDktNKWYPHmgqlUoZPBssHhI_10 = value;
	}

	inline static int32_t get_offset_of_HqWkThUinZjstdlLHSIImaHNnAis_11() { return static_cast<int32_t>(offsetof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8, ___HqWkThUinZjstdlLHSIImaHNnAis_11)); }
	inline bool get_HqWkThUinZjstdlLHSIImaHNnAis_11() const { return ___HqWkThUinZjstdlLHSIImaHNnAis_11; }
	inline bool* get_address_of_HqWkThUinZjstdlLHSIImaHNnAis_11() { return &___HqWkThUinZjstdlLHSIImaHNnAis_11; }
	inline void set_HqWkThUinZjstdlLHSIImaHNnAis_11(bool value)
	{
		___HqWkThUinZjstdlLHSIImaHNnAis_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TYDfjiilEUxIfZMyRypsywmpmwA
struct TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int64_t ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint64_t ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint64_t ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			double ___niPvDktNKWYPHmgqlUoZPBssHhI_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___niPvDktNKWYPHmgqlUoZPBssHhI_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_11_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of TYDfjiilEUxIfZMyRypsywmpmwA
struct TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int64_t ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___BUoNfFowxTfWVzlGeZVAXYsTHeZ_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint64_t ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint64_t ___UgJnYbycrRGWLWQVxOsnvCHCOdO_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			double ___niPvDktNKWYPHmgqlUoZPBssHhI_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___niPvDktNKWYPHmgqlUoZPBssHhI_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_11_forAlignmentOnly;
		};
	};
};
#endif // TYDFJIILEUXIFZMYRYPSYWMPMWA_T8340A7D91096EED258E5C91C215B70F279460FE8_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef UZCGPPJQBITKWKQVAYRGZBIZQLX_TA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_H
#define UZCGPPJQBITKWKQVAYRGZBIZQLX_TA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// uzcGPPJqbITKwkQVaYRGzbIZqLX
struct  uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte uzcGPPJqbITKwkQVaYRGzbIZqLX::wTYLYalPavcctjXZrASJNkapMaJ
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.SByte uzcGPPJqbITKwkQVaYRGzbIZqLX::MJrfBrxoBTvDrzGeBrTAImwmNOZ
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Char uzcGPPJqbITKwkQVaYRGzbIZqLX::euLduoAiVMxRQGLWMCGBXUDrCKr
			Il2CppChar ___euLduoAiVMxRQGLWMCGBXUDrCKr_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppChar ___euLduoAiVMxRQGLWMCGBXUDrCKr_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int16 uzcGPPJqbITKwkQVaYRGzbIZqLX::BlWgkqEeUMGgBlbpbsWvFckFbDv
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt16 uzcGPPJqbITKwkQVaYRGzbIZqLX::WBbwvCOIPVbAJbjWvqCbJElmQLtd
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 uzcGPPJqbITKwkQVaYRGzbIZqLX::lxdbJzaQWeBlRYInsjMuuXhyCggl
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt32 uzcGPPJqbITKwkQVaYRGzbIZqLX::evDemCrkcBEYeHncrlFUdsIyAKPa
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single uzcGPPJqbITKwkQVaYRGzbIZqLX::mUqspDOssdvKUAQvOxwRVdbooCb
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Boolean uzcGPPJqbITKwkQVaYRGzbIZqLX::HqWkThUinZjstdlLHSIImaHNnAis
			bool ___HqWkThUinZjstdlLHSIImaHNnAis_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			bool ___HqWkThUinZjstdlLHSIImaHNnAis_8_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_wTYLYalPavcctjXZrASJNkapMaJ_0() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___wTYLYalPavcctjXZrASJNkapMaJ_0)); }
	inline uint8_t get_wTYLYalPavcctjXZrASJNkapMaJ_0() const { return ___wTYLYalPavcctjXZrASJNkapMaJ_0; }
	inline uint8_t* get_address_of_wTYLYalPavcctjXZrASJNkapMaJ_0() { return &___wTYLYalPavcctjXZrASJNkapMaJ_0; }
	inline void set_wTYLYalPavcctjXZrASJNkapMaJ_0(uint8_t value)
	{
		___wTYLYalPavcctjXZrASJNkapMaJ_0 = value;
	}

	inline static int32_t get_offset_of_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1)); }
	inline int8_t get_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() const { return ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1; }
	inline int8_t* get_address_of_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() { return &___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1; }
	inline void set_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1(int8_t value)
	{
		___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1 = value;
	}

	inline static int32_t get_offset_of_euLduoAiVMxRQGLWMCGBXUDrCKr_2() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___euLduoAiVMxRQGLWMCGBXUDrCKr_2)); }
	inline Il2CppChar get_euLduoAiVMxRQGLWMCGBXUDrCKr_2() const { return ___euLduoAiVMxRQGLWMCGBXUDrCKr_2; }
	inline Il2CppChar* get_address_of_euLduoAiVMxRQGLWMCGBXUDrCKr_2() { return &___euLduoAiVMxRQGLWMCGBXUDrCKr_2; }
	inline void set_euLduoAiVMxRQGLWMCGBXUDrCKr_2(Il2CppChar value)
	{
		___euLduoAiVMxRQGLWMCGBXUDrCKr_2 = value;
	}

	inline static int32_t get_offset_of_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3)); }
	inline int16_t get_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() const { return ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3; }
	inline int16_t* get_address_of_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() { return &___BlWgkqEeUMGgBlbpbsWvFckFbDv_3; }
	inline void set_BlWgkqEeUMGgBlbpbsWvFckFbDv_3(int16_t value)
	{
		___BlWgkqEeUMGgBlbpbsWvFckFbDv_3 = value;
	}

	inline static int32_t get_offset_of_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4)); }
	inline uint16_t get_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() const { return ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4; }
	inline uint16_t* get_address_of_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() { return &___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4; }
	inline void set_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4(uint16_t value)
	{
		___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4 = value;
	}

	inline static int32_t get_offset_of_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5)); }
	inline int32_t get_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() const { return ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5; }
	inline int32_t* get_address_of_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() { return &___lxdbJzaQWeBlRYInsjMuuXhyCggl_5; }
	inline void set_lxdbJzaQWeBlRYInsjMuuXhyCggl_5(int32_t value)
	{
		___lxdbJzaQWeBlRYInsjMuuXhyCggl_5 = value;
	}

	inline static int32_t get_offset_of_evDemCrkcBEYeHncrlFUdsIyAKPa_6() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___evDemCrkcBEYeHncrlFUdsIyAKPa_6)); }
	inline uint32_t get_evDemCrkcBEYeHncrlFUdsIyAKPa_6() const { return ___evDemCrkcBEYeHncrlFUdsIyAKPa_6; }
	inline uint32_t* get_address_of_evDemCrkcBEYeHncrlFUdsIyAKPa_6() { return &___evDemCrkcBEYeHncrlFUdsIyAKPa_6; }
	inline void set_evDemCrkcBEYeHncrlFUdsIyAKPa_6(uint32_t value)
	{
		___evDemCrkcBEYeHncrlFUdsIyAKPa_6 = value;
	}

	inline static int32_t get_offset_of_mUqspDOssdvKUAQvOxwRVdbooCb_7() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___mUqspDOssdvKUAQvOxwRVdbooCb_7)); }
	inline float get_mUqspDOssdvKUAQvOxwRVdbooCb_7() const { return ___mUqspDOssdvKUAQvOxwRVdbooCb_7; }
	inline float* get_address_of_mUqspDOssdvKUAQvOxwRVdbooCb_7() { return &___mUqspDOssdvKUAQvOxwRVdbooCb_7; }
	inline void set_mUqspDOssdvKUAQvOxwRVdbooCb_7(float value)
	{
		___mUqspDOssdvKUAQvOxwRVdbooCb_7 = value;
	}

	inline static int32_t get_offset_of_HqWkThUinZjstdlLHSIImaHNnAis_8() { return static_cast<int32_t>(offsetof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491, ___HqWkThUinZjstdlLHSIImaHNnAis_8)); }
	inline bool get_HqWkThUinZjstdlLHSIImaHNnAis_8() const { return ___HqWkThUinZjstdlLHSIImaHNnAis_8; }
	inline bool* get_address_of_HqWkThUinZjstdlLHSIImaHNnAis_8() { return &___HqWkThUinZjstdlLHSIImaHNnAis_8; }
	inline void set_HqWkThUinZjstdlLHSIImaHNnAis_8(bool value)
	{
		___HqWkThUinZjstdlLHSIImaHNnAis_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of uzcGPPJqbITKwkQVaYRGzbIZqLX
struct uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_8_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of uzcGPPJqbITKwkQVaYRGzbIZqLX
struct uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___wTYLYalPavcctjXZrASJNkapMaJ_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int8_t ___MJrfBrxoBTvDrzGeBrTAImwmNOZ_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___euLduoAiVMxRQGLWMCGBXUDrCKr_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int16_t ___BlWgkqEeUMGgBlbpbsWvFckFbDv_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint16_t ___WBbwvCOIPVbAJbjWvqCbJElmQLtd_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___lxdbJzaQWeBlRYInsjMuuXhyCggl_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___evDemCrkcBEYeHncrlFUdsIyAKPa_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___mUqspDOssdvKUAQvOxwRVdbooCb_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___HqWkThUinZjstdlLHSIImaHNnAis_8_forAlignmentOnly;
		};
	};
};
#endif // UZCGPPJQBITKWKQVAYRGZBIZQLX_TA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_H
#ifndef AUSYJAOISXHKQULAWKDPQIPFVEN_TAA4C6596664B7369017D86D66D65E380AF5816A6_H
#define AUSYJAOISXHKQULAWKDPQIPFVEN_TAA4C6596664B7369017D86D66D65E380AF5816A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AUsyJaOISXhkQuLAWkdpqiPFvEn
struct  AUsyJaOISXhkQuLAWkdpqiPFvEn_tAA4C6596664B7369017D86D66D65E380AF5816A6 
{
public:
	// System.Int32 AUsyJaOISXhkQuLAWkdpqiPFvEn::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AUsyJaOISXhkQuLAWkdpqiPFvEn_tAA4C6596664B7369017D86D66D65E380AF5816A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUSYJAOISXHKQULAWKDPQIPFVEN_TAA4C6596664B7369017D86D66D65E380AF5816A6_H
#ifndef CCQITDALCOQZRIQYWQAPKWYGGXU_T7346E65EC9FD6CD5F470036AD3966026DCBF04D4_H
#define CCQITDALCOQZRIQYWQAPKWYGGXU_T7346E65EC9FD6CD5F470036AD3966026DCBF04D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCqITdAlCOqZriQyWqApkwYgGXU
struct  CCqITdAlCOqZriQyWqApkwYgGXU_t7346E65EC9FD6CD5F470036AD3966026DCBF04D4 
{
public:
	// System.Int32 CCqITdAlCOqZriQyWqApkwYgGXU::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CCqITdAlCOqZriQyWqApkwYgGXU_t7346E65EC9FD6CD5F470036AD3966026DCBF04D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCQITDALCOQZRIQYWQAPKWYGGXU_T7346E65EC9FD6CD5F470036AD3966026DCBF04D4_H
#ifndef NATIVEBUFFER_T8286D68043C990040A4F881140AB359A283A0E11_H
#define NATIVEBUFFER_T8286D68043C990040A4F881140AB359A283A0E11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.NativeBuffer
struct  NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11  : public RuntimeObject
{
public:
	// System.IntPtr Rewired.Utils.Classes.Data.NativeBuffer::DwHPghupeLEibLNuynKBvXsAFRu
	intptr_t ___DwHPghupeLEibLNuynKBvXsAFRu_0;
	// System.Int32 Rewired.Utils.Classes.Data.NativeBuffer::rUkckUyMTwDbZpaLuzsRIZNNDjA
	int32_t ___rUkckUyMTwDbZpaLuzsRIZNNDjA_1;
	// System.Boolean Rewired.Utils.Classes.Data.NativeBuffer::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_2;

public:
	inline static int32_t get_offset_of_DwHPghupeLEibLNuynKBvXsAFRu_0() { return static_cast<int32_t>(offsetof(NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11, ___DwHPghupeLEibLNuynKBvXsAFRu_0)); }
	inline intptr_t get_DwHPghupeLEibLNuynKBvXsAFRu_0() const { return ___DwHPghupeLEibLNuynKBvXsAFRu_0; }
	inline intptr_t* get_address_of_DwHPghupeLEibLNuynKBvXsAFRu_0() { return &___DwHPghupeLEibLNuynKBvXsAFRu_0; }
	inline void set_DwHPghupeLEibLNuynKBvXsAFRu_0(intptr_t value)
	{
		___DwHPghupeLEibLNuynKBvXsAFRu_0 = value;
	}

	inline static int32_t get_offset_of_rUkckUyMTwDbZpaLuzsRIZNNDjA_1() { return static_cast<int32_t>(offsetof(NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11, ___rUkckUyMTwDbZpaLuzsRIZNNDjA_1)); }
	inline int32_t get_rUkckUyMTwDbZpaLuzsRIZNNDjA_1() const { return ___rUkckUyMTwDbZpaLuzsRIZNNDjA_1; }
	inline int32_t* get_address_of_rUkckUyMTwDbZpaLuzsRIZNNDjA_1() { return &___rUkckUyMTwDbZpaLuzsRIZNNDjA_1; }
	inline void set_rUkckUyMTwDbZpaLuzsRIZNNDjA_1(int32_t value)
	{
		___rUkckUyMTwDbZpaLuzsRIZNNDjA_1 = value;
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_2() { return static_cast<int32_t>(offsetof(NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11, ___SeCUoinDywZmqZDHRKupOdOaTke_2)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_2() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_2; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_2() { return &___SeCUoinDywZmqZDHRKupOdOaTke_2; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_2(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEBUFFER_T8286D68043C990040A4F881140AB359A283A0E11_H
#ifndef FIELDOPTIONS_TAAD78715F738EBEB767BBD930664CDC03B476D91_H
#define FIELDOPTIONS_TAAD78715F738EBEB767BBD930664CDC03B476D91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.SerializedObject_FieldOptions
struct  FieldOptions_tAAD78715F738EBEB767BBD930664CDC03B476D91 
{
public:
	// System.Int32 Rewired.Utils.Classes.Data.SerializedObject_FieldOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FieldOptions_tAAD78715F738EBEB767BBD930664CDC03B476D91, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDOPTIONS_TAAD78715F738EBEB767BBD930664CDC03B476D91_H
#ifndef OBJECTTYPE_TA60C02C4004643CFFBAF8E23AC4B372437A08882_H
#define OBJECTTYPE_TA60C02C4004643CFFBAF8E23AC4B372437A08882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.SerializedObject_ObjectType
struct  ObjectType_tA60C02C4004643CFFBAF8E23AC4B372437A08882 
{
public:
	// System.Int32 Rewired.Utils.Classes.Data.SerializedObject_ObjectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectType_tA60C02C4004643CFFBAF8E23AC4B372437A08882, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTYPE_TA60C02C4004643CFFBAF8E23AC4B372437A08882_H
#ifndef DATATYPE_T2657C86FEC483E9329AD21CA3C61E9F50DA0A034_H
#define DATATYPE_T2657C86FEC483E9329AD21CA3C61E9F50DA0A034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.TypeWrapper_DataType
struct  DataType_t2657C86FEC483E9329AD21CA3C61E9F50DA0A034 
{
public:
	// System.Int32 Rewired.Utils.Classes.Data.TypeWrapper_DataType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataType_t2657C86FEC483E9329AD21CA3C61E9F50DA0A034, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_T2657C86FEC483E9329AD21CA3C61E9F50DA0A034_H
#ifndef SOLID_TC45AF417F066B6914095A87319241F9BBEF41E14_H
#define SOLID_TC45AF417F066B6914095A87319241F9BBEF41E14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.GUITools_Solid
struct  Solid_tC45AF417F066B6914095A87319241F9BBEF41E14  : public RuntimeObject
{
public:

public:
};

struct Solid_tC45AF417F066B6914095A87319241F9BBEF41E14_StaticFields
{
public:
	// System.Boolean Rewired.Utils.GUITools_Solid::hjfDXkmBkoLRKhseAxMUSlYXpkD
	bool ___hjfDXkmBkoLRKhseAxMUSlYXpkD_0;
	// UnityEngine.Texture2D Rewired.Utils.GUITools_Solid::bZQjuQuxDGYqIDZEXoBDBRHmDAU
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___bZQjuQuxDGYqIDZEXoBDBRHmDAU_1;
	// UnityEngine.Color Rewired.Utils.GUITools_Solid::jkmuVmxgfLllNdZKkbFTcPlNIBL
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___jkmuVmxgfLllNdZKkbFTcPlNIBL_2;

public:
	inline static int32_t get_offset_of_hjfDXkmBkoLRKhseAxMUSlYXpkD_0() { return static_cast<int32_t>(offsetof(Solid_tC45AF417F066B6914095A87319241F9BBEF41E14_StaticFields, ___hjfDXkmBkoLRKhseAxMUSlYXpkD_0)); }
	inline bool get_hjfDXkmBkoLRKhseAxMUSlYXpkD_0() const { return ___hjfDXkmBkoLRKhseAxMUSlYXpkD_0; }
	inline bool* get_address_of_hjfDXkmBkoLRKhseAxMUSlYXpkD_0() { return &___hjfDXkmBkoLRKhseAxMUSlYXpkD_0; }
	inline void set_hjfDXkmBkoLRKhseAxMUSlYXpkD_0(bool value)
	{
		___hjfDXkmBkoLRKhseAxMUSlYXpkD_0 = value;
	}

	inline static int32_t get_offset_of_bZQjuQuxDGYqIDZEXoBDBRHmDAU_1() { return static_cast<int32_t>(offsetof(Solid_tC45AF417F066B6914095A87319241F9BBEF41E14_StaticFields, ___bZQjuQuxDGYqIDZEXoBDBRHmDAU_1)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_bZQjuQuxDGYqIDZEXoBDBRHmDAU_1() const { return ___bZQjuQuxDGYqIDZEXoBDBRHmDAU_1; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_bZQjuQuxDGYqIDZEXoBDBRHmDAU_1() { return &___bZQjuQuxDGYqIDZEXoBDBRHmDAU_1; }
	inline void set_bZQjuQuxDGYqIDZEXoBDBRHmDAU_1(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___bZQjuQuxDGYqIDZEXoBDBRHmDAU_1 = value;
		Il2CppCodeGenWriteBarrier((&___bZQjuQuxDGYqIDZEXoBDBRHmDAU_1), value);
	}

	inline static int32_t get_offset_of_jkmuVmxgfLllNdZKkbFTcPlNIBL_2() { return static_cast<int32_t>(offsetof(Solid_tC45AF417F066B6914095A87319241F9BBEF41E14_StaticFields, ___jkmuVmxgfLllNdZKkbFTcPlNIBL_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_jkmuVmxgfLllNdZKkbFTcPlNIBL_2() const { return ___jkmuVmxgfLllNdZKkbFTcPlNIBL_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_jkmuVmxgfLllNdZKkbFTcPlNIBL_2() { return &___jkmuVmxgfLllNdZKkbFTcPlNIBL_2; }
	inline void set_jkmuVmxgfLllNdZKkbFTcPlNIBL_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___jkmuVmxgfLllNdZKkbFTcPlNIBL_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLID_TC45AF417F066B6914095A87319241F9BBEF41E14_H
#ifndef KDKKADIXKDHONXUJOUPGPNSQWMX_T268E557DA1791F4E969FD6E968C6414B96535C7E_H
#define KDKKADIXKDHONXUJOUPGPNSQWMX_T268E557DA1791F4E969FD6E968C6414B96535C7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.GUITools_kdkKadIxKDHONXuJOUpgpNSQwMx
struct  kdkKadIxKDHONXuJOUpgpNSQwMx_t268E557DA1791F4E969FD6E968C6414B96535C7E  : public RuntimeObject
{
public:
	// Rewired.Utils.GUITools_JYlRBGumenhRDRKYwPQSnpRSbox Rewired.Utils.GUITools_kdkKadIxKDHONXuJOUpgpNSQwMx::HAoGLOThHUkGKWpXvIYqRUzJSPk
	JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8 * ___HAoGLOThHUkGKWpXvIYqRUzJSPk_0;
	// UnityEngine.Rect Rewired.Utils.GUITools_kdkKadIxKDHONXuJOUpgpNSQwMx::BFPjbuOicsnWQIMpJOSsuGryCMb
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___BFPjbuOicsnWQIMpJOSsuGryCMb_1;
	// System.Single Rewired.Utils.GUITools_kdkKadIxKDHONXuJOUpgpNSQwMx::MkIfaGHGtZJFUiSCrMIUqejbPuaz
	float ___MkIfaGHGtZJFUiSCrMIUqejbPuaz_2;

public:
	inline static int32_t get_offset_of_HAoGLOThHUkGKWpXvIYqRUzJSPk_0() { return static_cast<int32_t>(offsetof(kdkKadIxKDHONXuJOUpgpNSQwMx_t268E557DA1791F4E969FD6E968C6414B96535C7E, ___HAoGLOThHUkGKWpXvIYqRUzJSPk_0)); }
	inline JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8 * get_HAoGLOThHUkGKWpXvIYqRUzJSPk_0() const { return ___HAoGLOThHUkGKWpXvIYqRUzJSPk_0; }
	inline JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8 ** get_address_of_HAoGLOThHUkGKWpXvIYqRUzJSPk_0() { return &___HAoGLOThHUkGKWpXvIYqRUzJSPk_0; }
	inline void set_HAoGLOThHUkGKWpXvIYqRUzJSPk_0(JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8 * value)
	{
		___HAoGLOThHUkGKWpXvIYqRUzJSPk_0 = value;
		Il2CppCodeGenWriteBarrier((&___HAoGLOThHUkGKWpXvIYqRUzJSPk_0), value);
	}

	inline static int32_t get_offset_of_BFPjbuOicsnWQIMpJOSsuGryCMb_1() { return static_cast<int32_t>(offsetof(kdkKadIxKDHONXuJOUpgpNSQwMx_t268E557DA1791F4E969FD6E968C6414B96535C7E, ___BFPjbuOicsnWQIMpJOSsuGryCMb_1)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_BFPjbuOicsnWQIMpJOSsuGryCMb_1() const { return ___BFPjbuOicsnWQIMpJOSsuGryCMb_1; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_BFPjbuOicsnWQIMpJOSsuGryCMb_1() { return &___BFPjbuOicsnWQIMpJOSsuGryCMb_1; }
	inline void set_BFPjbuOicsnWQIMpJOSsuGryCMb_1(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___BFPjbuOicsnWQIMpJOSsuGryCMb_1 = value;
	}

	inline static int32_t get_offset_of_MkIfaGHGtZJFUiSCrMIUqejbPuaz_2() { return static_cast<int32_t>(offsetof(kdkKadIxKDHONXuJOUpgpNSQwMx_t268E557DA1791F4E969FD6E968C6414B96535C7E, ___MkIfaGHGtZJFUiSCrMIUqejbPuaz_2)); }
	inline float get_MkIfaGHGtZJFUiSCrMIUqejbPuaz_2() const { return ___MkIfaGHGtZJFUiSCrMIUqejbPuaz_2; }
	inline float* get_address_of_MkIfaGHGtZJFUiSCrMIUqejbPuaz_2() { return &___MkIfaGHGtZJFUiSCrMIUqejbPuaz_2; }
	inline void set_MkIfaGHGtZJFUiSCrMIUqejbPuaz_2(float value)
	{
		___MkIfaGHGtZJFUiSCrMIUqejbPuaz_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KDKKADIXKDHONXUJOUPGPNSQWMX_T268E557DA1791F4E969FD6E968C6414B96535C7E_H
#ifndef BINDINGFLAGS_T295F6C7880FE259CEB8BA62B1DB438E16F565B6F_H
#define BINDINGFLAGS_T295F6C7880FE259CEB8BA62B1DB438E16F565B6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.ReflectionTools_BindingFlags
struct  BindingFlags_t295F6C7880FE259CEB8BA62B1DB438E16F565B6F 
{
public:
	// System.Int32 Rewired.Utils.ReflectionTools_BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_t295F6C7880FE259CEB8BA62B1DB438E16F565B6F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T295F6C7880FE259CEB8BA62B1DB438E16F565B6F_H
#ifndef SAFEACTION_TF141A9EE03213597DA8CA5527AAE121779DC69AD_H
#define SAFEACTION_TF141A9EE03213597DA8CA5527AAE121779DC69AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.SafeAction
struct  SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD  : public SafeDelegate_1_t6610170CD17271905495F29D060B38C3BCCCC4F3
{
public:

public:
};

struct SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD_StaticFields
{
public:
	// System.Action`2<System.Object,System.Action> Rewired.Utils.SafeAction::cfQeUDtgvyowVkXIfVFhZYHWfYC
	Action_2_tDDE800AB741C3BECFFDA85992A2EF0436DF8604E * ___cfQeUDtgvyowVkXIfVFhZYHWfYC_4;

public:
	inline static int32_t get_offset_of_cfQeUDtgvyowVkXIfVFhZYHWfYC_4() { return static_cast<int32_t>(offsetof(SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD_StaticFields, ___cfQeUDtgvyowVkXIfVFhZYHWfYC_4)); }
	inline Action_2_tDDE800AB741C3BECFFDA85992A2EF0436DF8604E * get_cfQeUDtgvyowVkXIfVFhZYHWfYC_4() const { return ___cfQeUDtgvyowVkXIfVFhZYHWfYC_4; }
	inline Action_2_tDDE800AB741C3BECFFDA85992A2EF0436DF8604E ** get_address_of_cfQeUDtgvyowVkXIfVFhZYHWfYC_4() { return &___cfQeUDtgvyowVkXIfVFhZYHWfYC_4; }
	inline void set_cfQeUDtgvyowVkXIfVFhZYHWfYC_4(Action_2_tDDE800AB741C3BECFFDA85992A2EF0436DF8604E * value)
	{
		___cfQeUDtgvyowVkXIfVFhZYHWfYC_4 = value;
		Il2CppCodeGenWriteBarrier((&___cfQeUDtgvyowVkXIfVFhZYHWfYC_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEACTION_TF141A9EE03213597DA8CA5527AAE121779DC69AD_H
#ifndef POSITIONTYPE_T62802278F69FE3A2C9F12AB1ABB78E6AE93F00E4_H
#define POSITIONTYPE_T62802278F69FE3A2C9F12AB1ABB78E6AE93F00E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.UI.PositionType
struct  PositionType_t62802278F69FE3A2C9F12AB1ABB78E6AE93F00E4 
{
public:
	// System.Int32 Rewired.Utils.UI.PositionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PositionType_t62802278F69FE3A2C9F12AB1ABB78E6AE93F00E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONTYPE_T62802278F69FE3A2C9F12AB1ABB78E6AE93F00E4_H
#ifndef BFBFPYEWFWLITMGEVWGPRFNMLDSZ_T52685C2269402DA0FBB7B8B704C0C7CCECAD64DC_H
#define BFBFPYEWFWLITMGEVWGPRFNMLDSZ_T52685C2269402DA0FBB7B8B704C0C7CCECAD64DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bFbfpYeWFWLitmgEvWgpRfNmLDSZ
struct  bFbfpYeWFWLitmgEvWgpRfNmLDSZ_t52685C2269402DA0FBB7B8B704C0C7CCECAD64DC 
{
public:
	// System.Int32 bFbfpYeWFWLitmgEvWgpRfNmLDSZ::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(bFbfpYeWFWLitmgEvWgpRfNmLDSZ_t52685C2269402DA0FBB7B8B704C0C7CCECAD64DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BFBFPYEWFWLITMGEVWGPRFNMLDSZ_T52685C2269402DA0FBB7B8B704C0C7CCECAD64DC_H
#ifndef MCDMDFPRCWEHADVRZCXVIWIIIXU_TFE05AFBCFF7352FA4DAB43AD510D75263887B21C_H
#define MCDMDFPRCWEHADVRZCXVIWIIIXU_TFE05AFBCFF7352FA4DAB43AD510D75263887B21C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// erAqfwCNbAAvnTADjtOsmRVrVxW_MCDmDFprCwEHaDvrZcxViWIIIXu
struct  MCDmDFprCwEHaDvrZcxViWIIIXu_tFE05AFBCFF7352FA4DAB43AD510D75263887B21C 
{
public:
	// System.Int32 erAqfwCNbAAvnTADjtOsmRVrVxW_MCDmDFprCwEHaDvrZcxViWIIIXu::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MCDmDFprCwEHaDvrZcxViWIIIXu_tFE05AFBCFF7352FA4DAB43AD510D75263887B21C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MCDMDFPRCWEHADVRZCXVIWIIIXU_TFE05AFBCFF7352FA4DAB43AD510D75263887B21C_H
#ifndef XHNVLCKSUDXVFJOOBCKJTHSIFWDB_T1D62E3F1CF98F64BBE43F320A063B46FEB0CBCD5_H
#define XHNVLCKSUDXVFJOOBCKJTHSIFWDB_T1D62E3F1CF98F64BBE43F320A063B46FEB0CBCD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// xHnvlcKSudxVFJOObckJtHsIfwDB
struct  xHnvlcKSudxVFJOObckJtHsIfwDB_t1D62E3F1CF98F64BBE43F320A063B46FEB0CBCD5 
{
public:
	// System.Int32 xHnvlcKSudxVFJOObckJtHsIfwDB::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(xHnvlcKSudxVFJOObckJtHsIfwDB_t1D62E3F1CF98F64BBE43F320A063B46FEB0CBCD5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XHNVLCKSUDXVFJOOBCKJTHSIFWDB_T1D62E3F1CF98F64BBE43F320A063B46FEB0CBCD5_H
#ifndef SERIALIZEDOBJECT_T6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_H
#define SERIALIZEDOBJECT_T6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.SerializedObject
struct  SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC  : public RuntimeObject
{
public:
	// Rewired.Utils.Classes.Data.IndexedDictionary`2<System.String,Rewired.Utils.Classes.Data.SerializedObject_Entry> Rewired.Utils.Classes.Data.SerializedObject::RTZlMdFsgbDqnbwpCQWxLQtoCuAK
	IndexedDictionary_2_tEB2A1CD4B3B24D918E51CE4813AC81DDB5A46D27 * ___RTZlMdFsgbDqnbwpCQWxLQtoCuAK_0;
	// Rewired.Utils.Classes.Data.SerializedObject_XmlInfo Rewired.Utils.Classes.Data.SerializedObject::JszCsfGdrEMwgDCGJhJThIDTBSWW
	XmlInfo_t1D129B900590213A92E045C337F63C7A2D1A2FBB * ___JszCsfGdrEMwgDCGJhJThIDTBSWW_1;
	// System.Type Rewired.Utils.Classes.Data.SerializedObject::DzAgXbAsFcjwinjPLOBYLcFrhIr
	Type_t * ___DzAgXbAsFcjwinjPLOBYLcFrhIr_2;
	// Rewired.Utils.Classes.Data.SerializedObject_ObjectType Rewired.Utils.Classes.Data.SerializedObject::GBbJzqokGyItHeUcMtAlHxUeqtQ
	int32_t ___GBbJzqokGyItHeUcMtAlHxUeqtQ_3;

public:
	inline static int32_t get_offset_of_RTZlMdFsgbDqnbwpCQWxLQtoCuAK_0() { return static_cast<int32_t>(offsetof(SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC, ___RTZlMdFsgbDqnbwpCQWxLQtoCuAK_0)); }
	inline IndexedDictionary_2_tEB2A1CD4B3B24D918E51CE4813AC81DDB5A46D27 * get_RTZlMdFsgbDqnbwpCQWxLQtoCuAK_0() const { return ___RTZlMdFsgbDqnbwpCQWxLQtoCuAK_0; }
	inline IndexedDictionary_2_tEB2A1CD4B3B24D918E51CE4813AC81DDB5A46D27 ** get_address_of_RTZlMdFsgbDqnbwpCQWxLQtoCuAK_0() { return &___RTZlMdFsgbDqnbwpCQWxLQtoCuAK_0; }
	inline void set_RTZlMdFsgbDqnbwpCQWxLQtoCuAK_0(IndexedDictionary_2_tEB2A1CD4B3B24D918E51CE4813AC81DDB5A46D27 * value)
	{
		___RTZlMdFsgbDqnbwpCQWxLQtoCuAK_0 = value;
		Il2CppCodeGenWriteBarrier((&___RTZlMdFsgbDqnbwpCQWxLQtoCuAK_0), value);
	}

	inline static int32_t get_offset_of_JszCsfGdrEMwgDCGJhJThIDTBSWW_1() { return static_cast<int32_t>(offsetof(SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC, ___JszCsfGdrEMwgDCGJhJThIDTBSWW_1)); }
	inline XmlInfo_t1D129B900590213A92E045C337F63C7A2D1A2FBB * get_JszCsfGdrEMwgDCGJhJThIDTBSWW_1() const { return ___JszCsfGdrEMwgDCGJhJThIDTBSWW_1; }
	inline XmlInfo_t1D129B900590213A92E045C337F63C7A2D1A2FBB ** get_address_of_JszCsfGdrEMwgDCGJhJThIDTBSWW_1() { return &___JszCsfGdrEMwgDCGJhJThIDTBSWW_1; }
	inline void set_JszCsfGdrEMwgDCGJhJThIDTBSWW_1(XmlInfo_t1D129B900590213A92E045C337F63C7A2D1A2FBB * value)
	{
		___JszCsfGdrEMwgDCGJhJThIDTBSWW_1 = value;
		Il2CppCodeGenWriteBarrier((&___JszCsfGdrEMwgDCGJhJThIDTBSWW_1), value);
	}

	inline static int32_t get_offset_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_2() { return static_cast<int32_t>(offsetof(SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC, ___DzAgXbAsFcjwinjPLOBYLcFrhIr_2)); }
	inline Type_t * get_DzAgXbAsFcjwinjPLOBYLcFrhIr_2() const { return ___DzAgXbAsFcjwinjPLOBYLcFrhIr_2; }
	inline Type_t ** get_address_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_2() { return &___DzAgXbAsFcjwinjPLOBYLcFrhIr_2; }
	inline void set_DzAgXbAsFcjwinjPLOBYLcFrhIr_2(Type_t * value)
	{
		___DzAgXbAsFcjwinjPLOBYLcFrhIr_2 = value;
		Il2CppCodeGenWriteBarrier((&___DzAgXbAsFcjwinjPLOBYLcFrhIr_2), value);
	}

	inline static int32_t get_offset_of_GBbJzqokGyItHeUcMtAlHxUeqtQ_3() { return static_cast<int32_t>(offsetof(SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC, ___GBbJzqokGyItHeUcMtAlHxUeqtQ_3)); }
	inline int32_t get_GBbJzqokGyItHeUcMtAlHxUeqtQ_3() const { return ___GBbJzqokGyItHeUcMtAlHxUeqtQ_3; }
	inline int32_t* get_address_of_GBbJzqokGyItHeUcMtAlHxUeqtQ_3() { return &___GBbJzqokGyItHeUcMtAlHxUeqtQ_3; }
	inline void set_GBbJzqokGyItHeUcMtAlHxUeqtQ_3(int32_t value)
	{
		___GBbJzqokGyItHeUcMtAlHxUeqtQ_3 = value;
	}
};

struct SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.FieldInfo>> Rewired.Utils.Classes.Data.SerializedObject::LQGJEklSmntrktcsXahnHwtrcTh
	Dictionary_2_tF19B322732A3F59097E35488888753E75A98508B * ___LQGJEklSmntrktcsXahnHwtrcTh_4;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.PropertyInfo>> Rewired.Utils.Classes.Data.SerializedObject::nBBUxyulOPuAQzbmJBCPkpmkUWsj
	Dictionary_2_t85E880FBBD1F7D6DF932C42144E9B40E2B7FCFE9 * ___nBBUxyulOPuAQzbmJBCPkpmkUWsj_5;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Rewired.Utils.Classes.Data.SerializedObject::APrqfxcqXfOigsaYgstdZUzQbTD
	Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * ___APrqfxcqXfOigsaYgstdZUzQbTD_6;
	// System.Func`2<System.Reflection.FieldInfo,System.String> Rewired.Utils.Classes.Data.SerializedObject::wzuTgIXkBigvyhwmUgzxbJRwaLDH
	Func_2_t1EB5033BF4C5996F1CBD82840960676EBDD8049C * ___wzuTgIXkBigvyhwmUgzxbJRwaLDH_7;
	// System.Func`2<System.Reflection.PropertyInfo,System.Boolean> Rewired.Utils.Classes.Data.SerializedObject::JHDbWtnHlVZLvnPftaKEZPlIjTx
	Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * ___JHDbWtnHlVZLvnPftaKEZPlIjTx_8;
	// System.Func`2<System.Reflection.PropertyInfo,System.String> Rewired.Utils.Classes.Data.SerializedObject::xzAVWYnEbnPJfpBopUAUVcTTRAy
	Func_2_t1E4225A498AC4D60FC503C2A063C30CA478ACF5A * ___xzAVWYnEbnPJfpBopUAUVcTTRAy_9;

public:
	inline static int32_t get_offset_of_LQGJEklSmntrktcsXahnHwtrcTh_4() { return static_cast<int32_t>(offsetof(SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields, ___LQGJEklSmntrktcsXahnHwtrcTh_4)); }
	inline Dictionary_2_tF19B322732A3F59097E35488888753E75A98508B * get_LQGJEklSmntrktcsXahnHwtrcTh_4() const { return ___LQGJEklSmntrktcsXahnHwtrcTh_4; }
	inline Dictionary_2_tF19B322732A3F59097E35488888753E75A98508B ** get_address_of_LQGJEklSmntrktcsXahnHwtrcTh_4() { return &___LQGJEklSmntrktcsXahnHwtrcTh_4; }
	inline void set_LQGJEklSmntrktcsXahnHwtrcTh_4(Dictionary_2_tF19B322732A3F59097E35488888753E75A98508B * value)
	{
		___LQGJEklSmntrktcsXahnHwtrcTh_4 = value;
		Il2CppCodeGenWriteBarrier((&___LQGJEklSmntrktcsXahnHwtrcTh_4), value);
	}

	inline static int32_t get_offset_of_nBBUxyulOPuAQzbmJBCPkpmkUWsj_5() { return static_cast<int32_t>(offsetof(SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields, ___nBBUxyulOPuAQzbmJBCPkpmkUWsj_5)); }
	inline Dictionary_2_t85E880FBBD1F7D6DF932C42144E9B40E2B7FCFE9 * get_nBBUxyulOPuAQzbmJBCPkpmkUWsj_5() const { return ___nBBUxyulOPuAQzbmJBCPkpmkUWsj_5; }
	inline Dictionary_2_t85E880FBBD1F7D6DF932C42144E9B40E2B7FCFE9 ** get_address_of_nBBUxyulOPuAQzbmJBCPkpmkUWsj_5() { return &___nBBUxyulOPuAQzbmJBCPkpmkUWsj_5; }
	inline void set_nBBUxyulOPuAQzbmJBCPkpmkUWsj_5(Dictionary_2_t85E880FBBD1F7D6DF932C42144E9B40E2B7FCFE9 * value)
	{
		___nBBUxyulOPuAQzbmJBCPkpmkUWsj_5 = value;
		Il2CppCodeGenWriteBarrier((&___nBBUxyulOPuAQzbmJBCPkpmkUWsj_5), value);
	}

	inline static int32_t get_offset_of_APrqfxcqXfOigsaYgstdZUzQbTD_6() { return static_cast<int32_t>(offsetof(SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields, ___APrqfxcqXfOigsaYgstdZUzQbTD_6)); }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * get_APrqfxcqXfOigsaYgstdZUzQbTD_6() const { return ___APrqfxcqXfOigsaYgstdZUzQbTD_6; }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D ** get_address_of_APrqfxcqXfOigsaYgstdZUzQbTD_6() { return &___APrqfxcqXfOigsaYgstdZUzQbTD_6; }
	inline void set_APrqfxcqXfOigsaYgstdZUzQbTD_6(Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * value)
	{
		___APrqfxcqXfOigsaYgstdZUzQbTD_6 = value;
		Il2CppCodeGenWriteBarrier((&___APrqfxcqXfOigsaYgstdZUzQbTD_6), value);
	}

	inline static int32_t get_offset_of_wzuTgIXkBigvyhwmUgzxbJRwaLDH_7() { return static_cast<int32_t>(offsetof(SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields, ___wzuTgIXkBigvyhwmUgzxbJRwaLDH_7)); }
	inline Func_2_t1EB5033BF4C5996F1CBD82840960676EBDD8049C * get_wzuTgIXkBigvyhwmUgzxbJRwaLDH_7() const { return ___wzuTgIXkBigvyhwmUgzxbJRwaLDH_7; }
	inline Func_2_t1EB5033BF4C5996F1CBD82840960676EBDD8049C ** get_address_of_wzuTgIXkBigvyhwmUgzxbJRwaLDH_7() { return &___wzuTgIXkBigvyhwmUgzxbJRwaLDH_7; }
	inline void set_wzuTgIXkBigvyhwmUgzxbJRwaLDH_7(Func_2_t1EB5033BF4C5996F1CBD82840960676EBDD8049C * value)
	{
		___wzuTgIXkBigvyhwmUgzxbJRwaLDH_7 = value;
		Il2CppCodeGenWriteBarrier((&___wzuTgIXkBigvyhwmUgzxbJRwaLDH_7), value);
	}

	inline static int32_t get_offset_of_JHDbWtnHlVZLvnPftaKEZPlIjTx_8() { return static_cast<int32_t>(offsetof(SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields, ___JHDbWtnHlVZLvnPftaKEZPlIjTx_8)); }
	inline Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * get_JHDbWtnHlVZLvnPftaKEZPlIjTx_8() const { return ___JHDbWtnHlVZLvnPftaKEZPlIjTx_8; }
	inline Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C ** get_address_of_JHDbWtnHlVZLvnPftaKEZPlIjTx_8() { return &___JHDbWtnHlVZLvnPftaKEZPlIjTx_8; }
	inline void set_JHDbWtnHlVZLvnPftaKEZPlIjTx_8(Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * value)
	{
		___JHDbWtnHlVZLvnPftaKEZPlIjTx_8 = value;
		Il2CppCodeGenWriteBarrier((&___JHDbWtnHlVZLvnPftaKEZPlIjTx_8), value);
	}

	inline static int32_t get_offset_of_xzAVWYnEbnPJfpBopUAUVcTTRAy_9() { return static_cast<int32_t>(offsetof(SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields, ___xzAVWYnEbnPJfpBopUAUVcTTRAy_9)); }
	inline Func_2_t1E4225A498AC4D60FC503C2A063C30CA478ACF5A * get_xzAVWYnEbnPJfpBopUAUVcTTRAy_9() const { return ___xzAVWYnEbnPJfpBopUAUVcTTRAy_9; }
	inline Func_2_t1E4225A498AC4D60FC503C2A063C30CA478ACF5A ** get_address_of_xzAVWYnEbnPJfpBopUAUVcTTRAy_9() { return &___xzAVWYnEbnPJfpBopUAUVcTTRAy_9; }
	inline void set_xzAVWYnEbnPJfpBopUAUVcTTRAy_9(Func_2_t1E4225A498AC4D60FC503C2A063C30CA478ACF5A * value)
	{
		___xzAVWYnEbnPJfpBopUAUVcTTRAy_9 = value;
		Il2CppCodeGenWriteBarrier((&___xzAVWYnEbnPJfpBopUAUVcTTRAy_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDOBJECT_T6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_H
#ifndef ENTRY_T8FF2DBC68F3363571693FF374CEB5BA8E152D147_H
#define ENTRY_T8FF2DBC68F3363571693FF374CEB5BA8E152D147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.SerializedObject_Entry
struct  Entry_t8FF2DBC68F3363571693FF374CEB5BA8E152D147 
{
public:
	// System.Type Rewired.Utils.Classes.Data.SerializedObject_Entry::type
	Type_t * ___type_0;
	// System.Object Rewired.Utils.Classes.Data.SerializedObject_Entry::value
	RuntimeObject * ___value_1;
	// Rewired.Utils.Classes.Data.SerializedObject_FieldOptions Rewired.Utils.Classes.Data.SerializedObject_Entry::options
	int32_t ___options_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Entry_t8FF2DBC68F3363571693FF374CEB5BA8E152D147, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(Entry_t8FF2DBC68F3363571693FF374CEB5BA8E152D147, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}

	inline static int32_t get_offset_of_options_2() { return static_cast<int32_t>(offsetof(Entry_t8FF2DBC68F3363571693FF374CEB5BA8E152D147, ___options_2)); }
	inline int32_t get_options_2() const { return ___options_2; }
	inline int32_t* get_address_of_options_2() { return &___options_2; }
	inline void set_options_2(int32_t value)
	{
		___options_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.Utils.Classes.Data.SerializedObject/Entry
struct Entry_t8FF2DBC68F3363571693FF374CEB5BA8E152D147_marshaled_pinvoke
{
	Type_t * ___type_0;
	Il2CppIUnknown* ___value_1;
	int32_t ___options_2;
};
// Native definition for COM marshalling of Rewired.Utils.Classes.Data.SerializedObject/Entry
struct Entry_t8FF2DBC68F3363571693FF374CEB5BA8E152D147_marshaled_com
{
	Type_t * ___type_0;
	Il2CppIUnknown* ___value_1;
	int32_t ___options_2;
};
#endif // ENTRY_T8FF2DBC68F3363571693FF374CEB5BA8E152D147_H
#ifndef FIELD_T95D6D0022F563F78348F8B1EA435E0A35ACFC297_H
#define FIELD_T95D6D0022F563F78348F8B1EA435E0A35ACFC297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.SerializedObject_Field
struct  Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297 
{
public:
	// System.String Rewired.Utils.Classes.Data.SerializedObject_Field::name
	String_t* ___name_0;
	// System.Object Rewired.Utils.Classes.Data.SerializedObject_Field::value
	RuntimeObject * ___value_1;
	// System.Type Rewired.Utils.Classes.Data.SerializedObject_Field::type
	Type_t * ___type_2;
	// Rewired.Utils.Classes.Data.SerializedObject_FieldOptions Rewired.Utils.Classes.Data.SerializedObject_Field::options
	int32_t ___options_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297, ___type_2)); }
	inline Type_t * get_type_2() const { return ___type_2; }
	inline Type_t ** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(Type_t * value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier((&___type_2), value);
	}

	inline static int32_t get_offset_of_options_3() { return static_cast<int32_t>(offsetof(Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297, ___options_3)); }
	inline int32_t get_options_3() const { return ___options_3; }
	inline int32_t* get_address_of_options_3() { return &___options_3; }
	inline void set_options_3(int32_t value)
	{
		___options_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.Utils.Classes.Data.SerializedObject/Field
struct Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297_marshaled_pinvoke
{
	char* ___name_0;
	Il2CppIUnknown* ___value_1;
	Type_t * ___type_2;
	int32_t ___options_3;
};
// Native definition for COM marshalling of Rewired.Utils.Classes.Data.SerializedObject/Field
struct Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppIUnknown* ___value_1;
	Type_t * ___type_2;
	int32_t ___options_3;
};
#endif // FIELD_T95D6D0022F563F78348F8B1EA435E0A35ACFC297_H
#ifndef TYPEWRAPPER_T788D13306712518A6288256C82C759738CC41C29_H
#define TYPEWRAPPER_T788D13306712518A6288256C82C759738CC41C29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.TypeWrapper
struct  TypeWrapper_t788D13306712518A6288256C82C759738CC41C29 
{
public:
	// Rewired.Utils.Classes.Data.TypeWrapper_DataType Rewired.Utils.Classes.Data.TypeWrapper::type
	int32_t ___type_0;
	// TYDfjiilEUxIfZMyRypsywmpmwA Rewired.Utils.Classes.Data.TypeWrapper::GcsCrWwEUNPgiZhGNANyAaKmIeHi
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8  ___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1;
	// System.String Rewired.Utils.Classes.Data.TypeWrapper::kQVmJpOPJsrLDZRAwRmSCbrkGjJE
	String_t* ___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2;
	// System.Object Rewired.Utils.Classes.Data.TypeWrapper::VnmoosDgcCbSdCuvMIbrnMSABaLs
	RuntimeObject * ___VnmoosDgcCbSdCuvMIbrnMSABaLs_3;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TypeWrapper_t788D13306712518A6288256C82C759738CC41C29, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_GcsCrWwEUNPgiZhGNANyAaKmIeHi_1() { return static_cast<int32_t>(offsetof(TypeWrapper_t788D13306712518A6288256C82C759738CC41C29, ___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1)); }
	inline TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8  get_GcsCrWwEUNPgiZhGNANyAaKmIeHi_1() const { return ___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1; }
	inline TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8 * get_address_of_GcsCrWwEUNPgiZhGNANyAaKmIeHi_1() { return &___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1; }
	inline void set_GcsCrWwEUNPgiZhGNANyAaKmIeHi_1(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8  value)
	{
		___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1 = value;
	}

	inline static int32_t get_offset_of_kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2() { return static_cast<int32_t>(offsetof(TypeWrapper_t788D13306712518A6288256C82C759738CC41C29, ___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2)); }
	inline String_t* get_kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2() const { return ___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2; }
	inline String_t** get_address_of_kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2() { return &___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2; }
	inline void set_kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2(String_t* value)
	{
		___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2 = value;
		Il2CppCodeGenWriteBarrier((&___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2), value);
	}

	inline static int32_t get_offset_of_VnmoosDgcCbSdCuvMIbrnMSABaLs_3() { return static_cast<int32_t>(offsetof(TypeWrapper_t788D13306712518A6288256C82C759738CC41C29, ___VnmoosDgcCbSdCuvMIbrnMSABaLs_3)); }
	inline RuntimeObject * get_VnmoosDgcCbSdCuvMIbrnMSABaLs_3() const { return ___VnmoosDgcCbSdCuvMIbrnMSABaLs_3; }
	inline RuntimeObject ** get_address_of_VnmoosDgcCbSdCuvMIbrnMSABaLs_3() { return &___VnmoosDgcCbSdCuvMIbrnMSABaLs_3; }
	inline void set_VnmoosDgcCbSdCuvMIbrnMSABaLs_3(RuntimeObject * value)
	{
		___VnmoosDgcCbSdCuvMIbrnMSABaLs_3 = value;
		Il2CppCodeGenWriteBarrier((&___VnmoosDgcCbSdCuvMIbrnMSABaLs_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.Utils.Classes.Data.TypeWrapper
struct TypeWrapper_t788D13306712518A6288256C82C759738CC41C29_marshaled_pinvoke
{
	int32_t ___type_0;
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8_marshaled_pinvoke ___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1;
	char* ___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2;
	Il2CppIUnknown* ___VnmoosDgcCbSdCuvMIbrnMSABaLs_3;
};
// Native definition for COM marshalling of Rewired.Utils.Classes.Data.TypeWrapper
struct TypeWrapper_t788D13306712518A6288256C82C759738CC41C29_marshaled_com
{
	int32_t ___type_0;
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8_marshaled_com ___GcsCrWwEUNPgiZhGNANyAaKmIeHi_1;
	Il2CppChar* ___kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2;
	Il2CppIUnknown* ___VnmoosDgcCbSdCuvMIbrnMSABaLs_3;
};
#endif // TYPEWRAPPER_T788D13306712518A6288256C82C759738CC41C29_H
#ifndef ERAQFWCNBAAVNTADJTOSMRVRVXW_TCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4_H
#define ERAQFWCNBAAVNTADJTOSMRVRVXW_TCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// erAqfwCNbAAvnTADjtOsmRVrVxW
struct  erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,erAqfwCNbAAvnTADjtOsmRVrVxW_XxmVNqBKBGNcpIbRBzMhemMCRnu> erAqfwCNbAAvnTADjtOsmRVrVxW::TvnOSqQNJsYQfEBzSupypcovrbP
	Dictionary_2_t96F61893C984671CB734D4C257EF7F9458A429E5 * ___TvnOSqQNJsYQfEBzSupypcovrbP_0;
	// erAqfwCNbAAvnTADjtOsmRVrVxW_MCDmDFprCwEHaDvrZcxViWIIIXu erAqfwCNbAAvnTADjtOsmRVrVxW::wAvHpKuziBJSknRXebWXhyUtdWg
	int32_t ___wAvHpKuziBJSknRXebWXhyUtdWg_1;

public:
	inline static int32_t get_offset_of_TvnOSqQNJsYQfEBzSupypcovrbP_0() { return static_cast<int32_t>(offsetof(erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4, ___TvnOSqQNJsYQfEBzSupypcovrbP_0)); }
	inline Dictionary_2_t96F61893C984671CB734D4C257EF7F9458A429E5 * get_TvnOSqQNJsYQfEBzSupypcovrbP_0() const { return ___TvnOSqQNJsYQfEBzSupypcovrbP_0; }
	inline Dictionary_2_t96F61893C984671CB734D4C257EF7F9458A429E5 ** get_address_of_TvnOSqQNJsYQfEBzSupypcovrbP_0() { return &___TvnOSqQNJsYQfEBzSupypcovrbP_0; }
	inline void set_TvnOSqQNJsYQfEBzSupypcovrbP_0(Dictionary_2_t96F61893C984671CB734D4C257EF7F9458A429E5 * value)
	{
		___TvnOSqQNJsYQfEBzSupypcovrbP_0 = value;
		Il2CppCodeGenWriteBarrier((&___TvnOSqQNJsYQfEBzSupypcovrbP_0), value);
	}

	inline static int32_t get_offset_of_wAvHpKuziBJSknRXebWXhyUtdWg_1() { return static_cast<int32_t>(offsetof(erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4, ___wAvHpKuziBJSknRXebWXhyUtdWg_1)); }
	inline int32_t get_wAvHpKuziBJSknRXebWXhyUtdWg_1() const { return ___wAvHpKuziBJSknRXebWXhyUtdWg_1; }
	inline int32_t* get_address_of_wAvHpKuziBJSknRXebWXhyUtdWg_1() { return &___wAvHpKuziBJSknRXebWXhyUtdWg_1; }
	inline void set_wAvHpKuziBJSknRXebWXhyUtdWg_1(int32_t value)
	{
		___wAvHpKuziBJSknRXebWXhyUtdWg_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERAQFWCNBAAVNTADJTOSMRVRVXW_TCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4_H
#ifndef ENUMERATOR_T692F35CBB5D794EE13127B28687B27012A8B9CE5_H
#define ENUMERATOR_T692F35CBB5D794EE13127B28687B27012A8B9CE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.SerializedObject_Enumerator
struct  Enumerator_t692F35CBB5D794EE13127B28687B27012A8B9CE5 
{
public:
	// Rewired.Utils.Classes.Data.IndexedDictionary`2<System.String,Rewired.Utils.Classes.Data.SerializedObject_Entry> Rewired.Utils.Classes.Data.SerializedObject_Enumerator::mpQsQGNdoKAwJwXPzsVPtDdrMBq
	IndexedDictionary_2_tEB2A1CD4B3B24D918E51CE4813AC81DDB5A46D27 * ___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0;
	// Rewired.Utils.Classes.Data.SerializedObject_Field Rewired.Utils.Classes.Data.SerializedObject_Enumerator::YaDAnfimULzphKnSejtwEcfSlkEJ
	Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297  ___YaDAnfimULzphKnSejtwEcfSlkEJ_1;
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Rewired.Utils.Classes.Data.SerializedObject_Entry>> Rewired.Utils.Classes.Data.SerializedObject_Enumerator::fMjmkhBkUGvJxCtQrfUacKtHlFW
	RuntimeObject* ___fMjmkhBkUGvJxCtQrfUacKtHlFW_2;

public:
	inline static int32_t get_offset_of_mpQsQGNdoKAwJwXPzsVPtDdrMBq_0() { return static_cast<int32_t>(offsetof(Enumerator_t692F35CBB5D794EE13127B28687B27012A8B9CE5, ___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0)); }
	inline IndexedDictionary_2_tEB2A1CD4B3B24D918E51CE4813AC81DDB5A46D27 * get_mpQsQGNdoKAwJwXPzsVPtDdrMBq_0() const { return ___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0; }
	inline IndexedDictionary_2_tEB2A1CD4B3B24D918E51CE4813AC81DDB5A46D27 ** get_address_of_mpQsQGNdoKAwJwXPzsVPtDdrMBq_0() { return &___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0; }
	inline void set_mpQsQGNdoKAwJwXPzsVPtDdrMBq_0(IndexedDictionary_2_tEB2A1CD4B3B24D918E51CE4813AC81DDB5A46D27 * value)
	{
		___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0 = value;
		Il2CppCodeGenWriteBarrier((&___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0), value);
	}

	inline static int32_t get_offset_of_YaDAnfimULzphKnSejtwEcfSlkEJ_1() { return static_cast<int32_t>(offsetof(Enumerator_t692F35CBB5D794EE13127B28687B27012A8B9CE5, ___YaDAnfimULzphKnSejtwEcfSlkEJ_1)); }
	inline Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297  get_YaDAnfimULzphKnSejtwEcfSlkEJ_1() const { return ___YaDAnfimULzphKnSejtwEcfSlkEJ_1; }
	inline Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297 * get_address_of_YaDAnfimULzphKnSejtwEcfSlkEJ_1() { return &___YaDAnfimULzphKnSejtwEcfSlkEJ_1; }
	inline void set_YaDAnfimULzphKnSejtwEcfSlkEJ_1(Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297  value)
	{
		___YaDAnfimULzphKnSejtwEcfSlkEJ_1 = value;
	}

	inline static int32_t get_offset_of_fMjmkhBkUGvJxCtQrfUacKtHlFW_2() { return static_cast<int32_t>(offsetof(Enumerator_t692F35CBB5D794EE13127B28687B27012A8B9CE5, ___fMjmkhBkUGvJxCtQrfUacKtHlFW_2)); }
	inline RuntimeObject* get_fMjmkhBkUGvJxCtQrfUacKtHlFW_2() const { return ___fMjmkhBkUGvJxCtQrfUacKtHlFW_2; }
	inline RuntimeObject** get_address_of_fMjmkhBkUGvJxCtQrfUacKtHlFW_2() { return &___fMjmkhBkUGvJxCtQrfUacKtHlFW_2; }
	inline void set_fMjmkhBkUGvJxCtQrfUacKtHlFW_2(RuntimeObject* value)
	{
		___fMjmkhBkUGvJxCtQrfUacKtHlFW_2 = value;
		Il2CppCodeGenWriteBarrier((&___fMjmkhBkUGvJxCtQrfUacKtHlFW_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.Utils.Classes.Data.SerializedObject/Enumerator
struct Enumerator_t692F35CBB5D794EE13127B28687B27012A8B9CE5_marshaled_pinvoke
{
	IndexedDictionary_2_tEB2A1CD4B3B24D918E51CE4813AC81DDB5A46D27 * ___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0;
	Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297_marshaled_pinvoke ___YaDAnfimULzphKnSejtwEcfSlkEJ_1;
	RuntimeObject* ___fMjmkhBkUGvJxCtQrfUacKtHlFW_2;
};
// Native definition for COM marshalling of Rewired.Utils.Classes.Data.SerializedObject/Enumerator
struct Enumerator_t692F35CBB5D794EE13127B28687B27012A8B9CE5_marshaled_com
{
	IndexedDictionary_2_tEB2A1CD4B3B24D918E51CE4813AC81DDB5A46D27 * ___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0;
	Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297_marshaled_com ___YaDAnfimULzphKnSejtwEcfSlkEJ_1;
	RuntimeObject* ___fMjmkhBkUGvJxCtQrfUacKtHlFW_2;
};
#endif // ENUMERATOR_T692F35CBB5D794EE13127B28687B27012A8B9CE5_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4900 = { sizeof (TsEOKKcYyURGUdCblaSthzyWFsU_t835CAF85D62B6B5E478ECC574C9864E2115F0575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4900[1] = 
{
	TsEOKKcYyURGUdCblaSthzyWFsU_t835CAF85D62B6B5E478ECC574C9864E2115F0575::get_offset_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4901 = { sizeof (VsbAISQkdJoqOdmXONtnUYGDNdA_t04872FAF3937D80A144C647F42EF5437114B38E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4901[2] = 
{
	VsbAISQkdJoqOdmXONtnUYGDNdA_t04872FAF3937D80A144C647F42EF5437114B38E5::get_offset_of_mvKHSWNtYxtrXAPVpClHLUktCzV_0(),
	VsbAISQkdJoqOdmXONtnUYGDNdA_t04872FAF3937D80A144C647F42EF5437114B38E5::get_offset_of_XtHNtnfhgeCKrJTxdJCwGrOeGslH_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4902 = { sizeof (GMJeTtZnicgHDDzqLfgDMVufCNPJ_t199215CBE67B0D8F7F016D8F9F9EBAD4E24E0F63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4902[3] = 
{
	GMJeTtZnicgHDDzqLfgDMVufCNPJ_t199215CBE67B0D8F7F016D8F9F9EBAD4E24E0F63::get_offset_of_iDGWEoZdokSjNmaJhbGKJlCcWrm_0(),
	GMJeTtZnicgHDDzqLfgDMVufCNPJ_t199215CBE67B0D8F7F016D8F9F9EBAD4E24E0F63::get_offset_of_mvKHSWNtYxtrXAPVpClHLUktCzV_1(),
	GMJeTtZnicgHDDzqLfgDMVufCNPJ_t199215CBE67B0D8F7F016D8F9F9EBAD4E24E0F63::get_offset_of_JNQgVTiKijygYJnTzRQNWJvQSMGQ_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4903 = { sizeof (erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4903[2] = 
{
	erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4::get_offset_of_TvnOSqQNJsYQfEBzSupypcovrbP_0(),
	erAqfwCNbAAvnTADjtOsmRVrVxW_tCDDB53EF9CAEB6A9074FEE09B92E92697CF956E4::get_offset_of_wAvHpKuziBJSknRXebWXhyUtdWg_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4904 = { sizeof (MCDmDFprCwEHaDvrZcxViWIIIXu_tFE05AFBCFF7352FA4DAB43AD510D75263887B21C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4904[4] = 
{
	MCDmDFprCwEHaDvrZcxViWIIIXu_tFE05AFBCFF7352FA4DAB43AD510D75263887B21C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4905 = { sizeof (XxmVNqBKBGNcpIbRBzMhemMCRnu_t28904CA63EAACBE256A9F029FAAE570922071D2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4905[3] = 
{
	XxmVNqBKBGNcpIbRBzMhemMCRnu_t28904CA63EAACBE256A9F029FAAE570922071D2A::get_offset_of_FwmafwnPPXAtBqTArlYOKFIyJoe_0(),
	XxmVNqBKBGNcpIbRBzMhemMCRnu_t28904CA63EAACBE256A9F029FAAE570922071D2A::get_offset_of_UZbYDMRHqoMYGDwwWIwoKVeBvKy_1(),
	XxmVNqBKBGNcpIbRBzMhemMCRnu_t28904CA63EAACBE256A9F029FAAE570922071D2A::get_offset_of_ODdrsnIXVvtMCTXbvFEXFLAJgJr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4906 = { sizeof (oboBAJTxxzoWBMFHJAOIyglMGzKA_tD4FB95EE6B78B67A70D059DE6AADCAD15649E3A4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4907 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4907[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4908 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4908[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4909 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4909[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4910 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4910[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4911 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4911[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4912 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4912[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4913 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4913[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4914 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4914[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4915 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4915[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4916 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4916[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4917 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4917[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4918 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4918[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4919 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4919[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4920 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4920[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4921 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4921[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4922 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4922[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4923 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4923[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4924 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4924[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4925 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4925[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4926 = { sizeof (SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC), -1, sizeof(SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4926[10] = 
{
	SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC::get_offset_of_RTZlMdFsgbDqnbwpCQWxLQtoCuAK_0(),
	SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC::get_offset_of_JszCsfGdrEMwgDCGJhJThIDTBSWW_1(),
	SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC::get_offset_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_2(),
	SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC::get_offset_of_GBbJzqokGyItHeUcMtAlHxUeqtQ_3(),
	SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields::get_offset_of_LQGJEklSmntrktcsXahnHwtrcTh_4(),
	SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields::get_offset_of_nBBUxyulOPuAQzbmJBCPkpmkUWsj_5(),
	SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields::get_offset_of_APrqfxcqXfOigsaYgstdZUzQbTD_6(),
	SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields::get_offset_of_wzuTgIXkBigvyhwmUgzxbJRwaLDH_7(),
	SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields::get_offset_of_JHDbWtnHlVZLvnPftaKEZPlIjTx_8(),
	SerializedObject_t6DF5261493C113C3E7BA779CBC342F5D4CFA22EC_StaticFields::get_offset_of_xzAVWYnEbnPJfpBopUAUVcTTRAy_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4927 = { sizeof (ObjectType_tA60C02C4004643CFFBAF8E23AC4B372437A08882)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4927[3] = 
{
	ObjectType_tA60C02C4004643CFFBAF8E23AC4B372437A08882::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4928 = { sizeof (FieldOptions_tAAD78715F738EBEB767BBD930664CDC03B476D91)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4928[3] = 
{
	FieldOptions_tAAD78715F738EBEB767BBD930664CDC03B476D91::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4929 = { sizeof (Entry_t8FF2DBC68F3363571693FF374CEB5BA8E152D147)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4929[3] = 
{
	Entry_t8FF2DBC68F3363571693FF374CEB5BA8E152D147::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Entry_t8FF2DBC68F3363571693FF374CEB5BA8E152D147::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Entry_t8FF2DBC68F3363571693FF374CEB5BA8E152D147::get_offset_of_options_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4930 = { sizeof (Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4930[4] = 
{
	Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297::get_offset_of_type_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Field_t95D6D0022F563F78348F8B1EA435E0A35ACFC297::get_offset_of_options_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4931 = { sizeof (XmlInfo_t1D129B900590213A92E045C337F63C7A2D1A2FBB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4931[1] = 
{
	XmlInfo_t1D129B900590213A92E045C337F63C7A2D1A2FBB::get_offset_of_fLfjvbAsBBJCTCqKzKXkmvWdjlNO_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4932 = { sizeof (XmlAttribute_t654C37181E9CB42791E326F78F30E4352E521DC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4933 = { sizeof (XmlStringAttribute_t457BC831FBBFB17CD3F7F610F5337F282E3E6159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4933[4] = 
{
	XmlStringAttribute_t457BC831FBBFB17CD3F7F610F5337F282E3E6159::get_offset_of_prefix_0(),
	XmlStringAttribute_t457BC831FBBFB17CD3F7F610F5337F282E3E6159::get_offset_of_localName_1(),
	XmlStringAttribute_t457BC831FBBFB17CD3F7F610F5337F282E3E6159::get_offset_of_ns_2(),
	XmlStringAttribute_t457BC831FBBFB17CD3F7F610F5337F282E3E6159::get_offset_of_value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4934 = { sizeof (Enumerator_t692F35CBB5D794EE13127B28687B27012A8B9CE5)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4934[3] = 
{
	Enumerator_t692F35CBB5D794EE13127B28687B27012A8B9CE5::get_offset_of_mpQsQGNdoKAwJwXPzsVPtDdrMBq_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t692F35CBB5D794EE13127B28687B27012A8B9CE5::get_offset_of_YaDAnfimULzphKnSejtwEcfSlkEJ_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t692F35CBB5D794EE13127B28687B27012A8B9CE5::get_offset_of_fMjmkhBkUGvJxCtQrfUacKtHlFW_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4935 = { sizeof (XmlDocument_tEB80CCD80CA82521A35545CB41596F7A54C65BE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4935[1] = 
{
	XmlDocument_tEB80CCD80CA82521A35545CB41596F7A54C65BE3::get_offset_of__root_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4936 = { sizeof (Element_tB976891F5774B30930B5751F45DAC9A21D669254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4936[5] = 
{
	Element_tB976891F5774B30930B5751F45DAC9A21D669254::get_offset_of_name_0(),
	Element_tB976891F5774B30930B5751F45DAC9A21D669254::get_offset_of_parent_1(),
	Element_tB976891F5774B30930B5751F45DAC9A21D669254::get_offset_of_content_2(),
	Element_tB976891F5774B30930B5751F45DAC9A21D669254::get_offset_of_attributes_3(),
	Element_tB976891F5774B30930B5751F45DAC9A21D669254::get_offset_of_children_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4937 = { sizeof (NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4937[3] = 
{
	NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11::get_offset_of_DwHPghupeLEibLNuynKBvXsAFRu_0(),
	NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11::get_offset_of_rUkckUyMTwDbZpaLuzsRIZNNDjA_1(),
	NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4938 = { sizeof (NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4938[8] = 
{
	NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E::get_offset_of_gWYcSreEUttuvqTHqKoRwtWQvHb_0(),
	NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E::get_offset_of_sBiHWicpYhmNgEiaLGeoNmCayd_1(),
	NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E::get_offset_of_XWfuhHzLXoGfsuCDzbUXDcLiMFsN_2(),
	NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E::get_offset_of_UbdmeRQrglqqkmOaVIKVDgtTcHy_3(),
	NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E::get_offset_of_MqUXNSXDPPfjAskTnFXiaRaylNl_4(),
	NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E::get_offset_of_kVZynhRxuCAZFbglRgJlGkfKQuU_5(),
	NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E::get_offset_of_jzHVvuRZBKMkHyMhCGOheBeOJtl_6(),
	NativeRingBuffer_t48CF3D10318B9DE882D29FA0C9E00FBEC2173F6E::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4939 = { sizeof (TypeWrapper_t788D13306712518A6288256C82C759738CC41C29)+ sizeof (RuntimeObject), sizeof(TypeWrapper_t788D13306712518A6288256C82C759738CC41C29_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4939[4] = 
{
	TypeWrapper_t788D13306712518A6288256C82C759738CC41C29::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TypeWrapper_t788D13306712518A6288256C82C759738CC41C29::get_offset_of_GcsCrWwEUNPgiZhGNANyAaKmIeHi_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TypeWrapper_t788D13306712518A6288256C82C759738CC41C29::get_offset_of_kQVmJpOPJsrLDZRAwRmSCbrkGjJE_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TypeWrapper_t788D13306712518A6288256C82C759738CC41C29::get_offset_of_VnmoosDgcCbSdCuvMIbrnMSABaLs_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4940 = { sizeof (DataType_t2657C86FEC483E9329AD21CA3C61E9F50DA0A034)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4940[16] = 
{
	DataType_t2657C86FEC483E9329AD21CA3C61E9F50DA0A034::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4941 = { sizeof (IntVector2_t0FE90043F56F76F399543387D0775EB6A20234DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4941[2] = 
{
	IntVector2_t0FE90043F56F76F399543387D0775EB6A20234DC::get_offset_of_x_0(),
	IntVector2_t0FE90043F56F76F399543387D0775EB6A20234DC::get_offset_of_y_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4942 = { sizeof (IntVector3_tF98FAA9B1105CA2DEF567A7C889B4381C874625D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4942[3] = 
{
	IntVector3_tF98FAA9B1105CA2DEF567A7C889B4381C874625D::get_offset_of_x_0(),
	IntVector3_tF98FAA9B1105CA2DEF567A7C889B4381C874625D::get_offset_of_y_1(),
	IntVector3_tF98FAA9B1105CA2DEF567A7C889B4381C874625D::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4943 = { sizeof (IntVector4_t03EE51B365B356E37758C3B9655F9E0D1097ABA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4943[4] = 
{
	IntVector4_t03EE51B365B356E37758C3B9655F9E0D1097ABA1::get_offset_of_x_0(),
	IntVector4_t03EE51B365B356E37758C3B9655F9E0D1097ABA1::get_offset_of_y_1(),
	IntVector4_t03EE51B365B356E37758C3B9655F9E0D1097ABA1::get_offset_of_z_2(),
	IntVector4_t03EE51B365B356E37758C3B9655F9E0D1097ABA1::get_offset_of_q_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4944 = { sizeof (IntRect_tD854461BA870122515A5E50047F967810C37D57D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4944[4] = 
{
	IntRect_tD854461BA870122515A5E50047F967810C37D57D::get_offset_of_x_0(),
	IntRect_tD854461BA870122515A5E50047F967810C37D57D::get_offset_of_y_1(),
	IntRect_tD854461BA870122515A5E50047F967810C37D57D::get_offset_of_width_2(),
	IntRect_tD854461BA870122515A5E50047F967810C37D57D::get_offset_of_height_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4945 = { sizeof (IntPadding_t5EA5D15351CA442F8C3B721C29A6FD5398080D06), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4945[4] = 
{
	IntPadding_t5EA5D15351CA442F8C3B721C29A6FD5398080D06::get_offset_of_top_0(),
	IntPadding_t5EA5D15351CA442F8C3B721C29A6FD5398080D06::get_offset_of_right_1(),
	IntPadding_t5EA5D15351CA442F8C3B721C29A6FD5398080D06::get_offset_of_bottom_2(),
	IntPadding_t5EA5D15351CA442F8C3B721C29A6FD5398080D06::get_offset_of_left_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4946 = { sizeof (SerializableULong_t33164346204E6ACFE4136BC2B4A0D5F9941FADEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4946[2] = 
{
	SerializableULong_t33164346204E6ACFE4136BC2B4A0D5F9941FADEF::get_offset_of_ulong_32BitLow_0(),
	SerializableULong_t33164346204E6ACFE4136BC2B4A0D5F9941FADEF::get_offset_of_ulong_32BitHigh_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4947 = { sizeof (TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8)+ sizeof (RuntimeObject), sizeof(TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4947[12] = 
{
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8::get_offset_of_wTYLYalPavcctjXZrASJNkapMaJ_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8::get_offset_of_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8::get_offset_of_euLduoAiVMxRQGLWMCGBXUDrCKr_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8::get_offset_of_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8::get_offset_of_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8::get_offset_of_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8::get_offset_of_evDemCrkcBEYeHncrlFUdsIyAKPa_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8::get_offset_of_BUoNfFowxTfWVzlGeZVAXYsTHeZ_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8::get_offset_of_UgJnYbycrRGWLWQVxOsnvCHCOdO_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8::get_offset_of_mUqspDOssdvKUAQvOxwRVdbooCb_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8::get_offset_of_niPvDktNKWYPHmgqlUoZPBssHhI_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TYDfjiilEUxIfZMyRypsywmpmwA_t8340A7D91096EED258E5C91C215B70F279460FE8::get_offset_of_HqWkThUinZjstdlLHSIImaHNnAis_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4948 = { sizeof (uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491)+ sizeof (RuntimeObject), sizeof(uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4948[9] = 
{
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491::get_offset_of_wTYLYalPavcctjXZrASJNkapMaJ_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491::get_offset_of_MJrfBrxoBTvDrzGeBrTAImwmNOZ_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491::get_offset_of_euLduoAiVMxRQGLWMCGBXUDrCKr_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491::get_offset_of_BlWgkqEeUMGgBlbpbsWvFckFbDv_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491::get_offset_of_WBbwvCOIPVbAJbjWvqCbJElmQLtd_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491::get_offset_of_lxdbJzaQWeBlRYInsjMuuXhyCggl_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491::get_offset_of_evDemCrkcBEYeHncrlFUdsIyAKPa_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491::get_offset_of_mUqspDOssdvKUAQvOxwRVdbooCb_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	uzcGPPJqbITKwkQVaYRGzbIZqLX_tA6F1CE74B6FB294E3A75E926E8C11EBAD3955491::get_offset_of_HqWkThUinZjstdlLHSIImaHNnAis_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4949 = { sizeof (Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3)+ sizeof (RuntimeObject), sizeof(Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3 ), sizeof(Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4949[7] = 
{
	0,
	Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3::get_offset_of_a_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3::get_offset_of_b_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_StaticFields::get_offset_of__additionDelegate_3(),
	Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_StaticFields::get_offset_of__subtractionDelegate_4(),
	Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_StaticFields::get_offset_of__multiplicationDelegate_5(),
	Float2x_tBF5E909D9FC6BC9E8AB8826517198277D14DD8F3_StaticFields::get_offset_of__divisionDelegate_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4950 = { sizeof (Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C)+ sizeof (RuntimeObject), sizeof(Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C ), sizeof(Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4950[8] = 
{
	0,
	Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C::get_offset_of_a_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C::get_offset_of_b_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C::get_offset_of_c_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C_StaticFields::get_offset_of__additionDelegate_4(),
	Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C_StaticFields::get_offset_of__subtractionDelegate_5(),
	Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C_StaticFields::get_offset_of__multiplicationDelegate_6(),
	Float3x_t6E72B93448627376705F47B6FB2119DA743E6B8C_StaticFields::get_offset_of__divisionDelegate_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4951 = { sizeof (Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7)+ sizeof (RuntimeObject), sizeof(Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7 ), sizeof(Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4951[9] = 
{
	0,
	Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7::get_offset_of_a_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7::get_offset_of_b_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7::get_offset_of_c_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7::get_offset_of_d_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7_StaticFields::get_offset_of__additionDelegate_5(),
	Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7_StaticFields::get_offset_of__subtractionDelegate_6(),
	Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7_StaticFields::get_offset_of__multiplicationDelegate_7(),
	Float4x_t0A5708BC5B27ABD79D27C864E666BF0B01119BC7_StaticFields::get_offset_of__divisionDelegate_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4952 = { sizeof (ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14)+ sizeof (RuntimeObject), sizeof(ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4952[4] = 
{
	ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14::get_offset_of_xMin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14::get_offset_of_yMin_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14::get_offset_of_width_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ScreenRect_t3AB475F73A1FA7EC1B7ABB759D1CE7F73CCCCD14::get_offset_of_height_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4953 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4953[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4954 = { sizeof (CCqITdAlCOqZriQyWqApkwYgGXU_t7346E65EC9FD6CD5F470036AD3966026DCBF04D4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4954[4] = 
{
	CCqITdAlCOqZriQyWqApkwYgGXU_t7346E65EC9FD6CD5F470036AD3966026DCBF04D4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4955 = { sizeof (AUsyJaOISXhkQuLAWkdpqiPFvEn_tAA4C6596664B7369017D86D66D65E380AF5816A6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4955[4] = 
{
	AUsyJaOISXhkQuLAWkdpqiPFvEn_tAA4C6596664B7369017D86D66D65E380AF5816A6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4956 = { sizeof (bFbfpYeWFWLitmgEvWgpRfNmLDSZ_t52685C2269402DA0FBB7B8B704C0C7CCECAD64DC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4956[6] = 
{
	bFbfpYeWFWLitmgEvWgpRfNmLDSZ_t52685C2269402DA0FBB7B8B704C0C7CCECAD64DC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4957 = { sizeof (xHnvlcKSudxVFJOObckJtHsIfwDB_t1D62E3F1CF98F64BBE43F320A063B46FEB0CBCD5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4957[3] = 
{
	xHnvlcKSudxVFJOObckJtHsIfwDB_t1D62E3F1CF98F64BBE43F320A063B46FEB0CBCD5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4958 = { sizeof (PositionType_t62802278F69FE3A2C9F12AB1ABB78E6AE93F00E4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4958[4] = 
{
	PositionType_t62802278F69FE3A2C9F12AB1ABB78E6AE93F00E4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4959 = { sizeof (hgArHCrBiBgmkkaWqlNqbKsRVyk_t41256CAA11A811B0C96971EF489A5FAB76506B0A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4960 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4961 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4961[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4962 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4962[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4963 = { sizeof (DylvOGVHEEnJyEeuLyTOeUqyNkk_t3555EB6FDDAFCFBCF107BB6A84AF952343983AA3), -1, sizeof(DylvOGVHEEnJyEeuLyTOeUqyNkk_t3555EB6FDDAFCFBCF107BB6A84AF952343983AA3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4963[2] = 
{
	DylvOGVHEEnJyEeuLyTOeUqyNkk_t3555EB6FDDAFCFBCF107BB6A84AF952343983AA3_StaticFields::get_offset_of_NOMeqcZMOxtewAfrBZxUglAtxAy_0(),
	DylvOGVHEEnJyEeuLyTOeUqyNkk_t3555EB6FDDAFCFBCF107BB6A84AF952343983AA3_StaticFields::get_offset_of_wKZsRNpOhQctCfJnvfAdTetobSIx_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4964 = { sizeof (ExtensionMethods_t84BE5B0F3959CB2C3F5C52B7D2D86F4D40F61DF2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4965 = { sizeof (SerializeAttribute_tF250E6F5010E384B303C2E6395DC56C5098A3DB4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4965[1] = 
{
	SerializeAttribute_tF250E6F5010E384B303C2E6395DC56C5098A3DB4::get_offset_of_Name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4966 = { sizeof (DoNotSerializeAttribute_tE1E9808B80F5508B06D839021C51B7AA79288F4E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4967 = { sizeof (JsonTools_tDED13E7D75774FB5C228D7AE7F9006973F9AF0DC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4968 = { sizeof (JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8), -1, sizeof(JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4968[8] = 
{
	JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields::get_offset_of_splitArrayPool_0(),
	JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields::get_offset_of_KWmHqVWzTXhrCMwnLCcIUmyQlOp_1(),
	JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields::get_offset_of_LQGJEklSmntrktcsXahnHwtrcTh_2(),
	JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields::get_offset_of_nBBUxyulOPuAQzbmJBCPkpmkUWsj_3(),
	JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields::get_offset_of_APrqfxcqXfOigsaYgstdZUzQbTD_4(),
	JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields::get_offset_of_wzuTgIXkBigvyhwmUgzxbJRwaLDH_5(),
	JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields::get_offset_of_JHDbWtnHlVZLvnPftaKEZPlIjTx_6(),
	JsonParser_t4BBDF2576F9D7AA5F21590E6D1809FC294042DF8_StaticFields::get_offset_of_xzAVWYnEbnPJfpBopUAUVcTTRAy_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4969 = { sizeof (JsonWriter_t89F979F122D2D97D0748B0D4EA7CB9A6C4FE867F), -1, sizeof(JsonWriter_t89F979F122D2D97D0748B0D4EA7CB9A6C4FE867F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4969[1] = 
{
	JsonWriter_t89F979F122D2D97D0748B0D4EA7CB9A6C4FE867F_StaticFields::get_offset_of_pzweyUhGzpYFZIjeZCshaHZTBEkZ_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4970 = { sizeof (SafeDelegate_t0297C74E3DC510F138FAB3D8C38128AEB0AA6506), -1, sizeof(SafeDelegate_t0297C74E3DC510F138FAB3D8C38128AEB0AA6506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4970[1] = 
{
	SafeDelegate_t0297C74E3DC510F138FAB3D8C38128AEB0AA6506_StaticFields::get_offset_of_ptPzBqHgnzLwCGDGNDvdnOmXLZc_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4971 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4971[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4972 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4972[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4973 = { sizeof (SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD), -1, sizeof(SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4973[1] = 
{
	SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD_StaticFields::get_offset_of_cfQeUDtgvyowVkXIfVFhZYHWfYC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4974 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4974[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4975 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4975[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4976 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4976[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4977 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4977[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4978 = { sizeof (SystemInfo_tCA2E859BC362FF7E45275F546C88750CB0BCE4D1), -1, sizeof(SystemInfo_tCA2E859BC362FF7E45275F546C88750CB0BCE4D1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4978[1] = 
{
	SystemInfo_tCA2E859BC362FF7E45275F546C88750CB0BCE4D1_StaticFields::get_offset_of_is64Bit_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4979 = { sizeof (TempListPool_tD0311D26DDB7CA4D65DDA12E515D92B625142012), -1, sizeof(TempListPool_tD0311D26DDB7CA4D65DDA12E515D92B625142012_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4979[3] = 
{
	0,
	0,
	TempListPool_tD0311D26DDB7CA4D65DDA12E515D92B625142012_StaticFields::get_offset_of_pDlOHdwCqMQnSZVglpsIlLSNKfE_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4980 = { sizeof (vCtprMGnXmaBYwCYymGezBucMCO_tF93C64F9612F5016E5B47BB42CF6449960D32BC7), -1, sizeof(vCtprMGnXmaBYwCYymGezBucMCO_tF93C64F9612F5016E5B47BB42CF6449960D32BC7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4980[1] = 
{
	vCtprMGnXmaBYwCYymGezBucMCO_tF93C64F9612F5016E5B47BB42CF6449960D32BC7_StaticFields::get_offset_of_rkXXtFzuqnQzeNiXkbZrpScYwkq_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4981 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4982 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4982[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4983 = { sizeof (ArrayTools_t02E7DC3CE31992C415030A64F304F67C14BDAFC9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4984 = { sizeof (BitTools_tEB61A0CF35B6D9E3489D10AE4F2F3E720908B65B), -1, sizeof(BitTools_tEB61A0CF35B6D9E3489D10AE4F2F3E720908B65B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4984[1] = 
{
	BitTools_tEB61A0CF35B6D9E3489D10AE4F2F3E720908B65B_StaticFields::get_offset_of_SVLlPnSWrsGeIkjrbNXnCHMAlbA_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4985 = { sizeof (CollectionTools_tD23FBCB2643F1CBA7D62E771DB0897933AC9A22E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4986 = { sizeof (EnumTools_t755C614986ADC18A4AF7B958B1D857B57FD601D4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4987 = { sizeof (GUITools_t151239CEF509B8F55E5BB9BF9083DFD0FE728D5F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4988 = { sizeof (Solid_tC45AF417F066B6914095A87319241F9BBEF41E14), -1, sizeof(Solid_tC45AF417F066B6914095A87319241F9BBEF41E14_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4988[3] = 
{
	Solid_tC45AF417F066B6914095A87319241F9BBEF41E14_StaticFields::get_offset_of_hjfDXkmBkoLRKhseAxMUSlYXpkD_0(),
	Solid_tC45AF417F066B6914095A87319241F9BBEF41E14_StaticFields::get_offset_of_bZQjuQuxDGYqIDZEXoBDBRHmDAU_1(),
	Solid_tC45AF417F066B6914095A87319241F9BBEF41E14_StaticFields::get_offset_of_jkmuVmxgfLllNdZKkbFTcPlNIBL_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4989 = { sizeof (kdkKadIxKDHONXuJOUpgpNSQwMx_t268E557DA1791F4E969FD6E968C6414B96535C7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4989[3] = 
{
	kdkKadIxKDHONXuJOUpgpNSQwMx_t268E557DA1791F4E969FD6E968C6414B96535C7E::get_offset_of_HAoGLOThHUkGKWpXvIYqRUzJSPk_0(),
	kdkKadIxKDHONXuJOUpgpNSQwMx_t268E557DA1791F4E969FD6E968C6414B96535C7E::get_offset_of_BFPjbuOicsnWQIMpJOSsuGryCMb_1(),
	kdkKadIxKDHONXuJOUpgpNSQwMx_t268E557DA1791F4E969FD6E968C6414B96535C7E::get_offset_of_MkIfaGHGtZJFUiSCrMIUqejbPuaz_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4990 = { sizeof (JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4990[3] = 
{
	JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8::get_offset_of_EWjdtQBmywIsBumQtMhYWMahyq_0(),
	JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8::get_offset_of_fikNlDQpHfvQauiZfOtstCPtqXx_1(),
	JYlRBGumenhRDRKYwPQSnpRSbox_t421A3ADF936383A24ABF7D179E497851B293E4D8::get_offset_of_wvGzQhLdOBNwcvVnZKxLivQDPWK_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4991 = { sizeof (InputTools_t9682C38ED402090BA6DBDD740CD85A0471DD459B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4992 = { sizeof (ListTools_tD14BE95825F9C16D0533E58B79EEB05ABD91F1B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4993 = { sizeof (MathTools_t63ABC37176B73299DB82FAB4F057063FD72DE9CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4993[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4994 = { sizeof (MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5), -1, sizeof(MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4994[6] = 
{
	MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields::get_offset_of_PilgZOgMdQkvLRQJTMdkLwdNSxcu_0(),
	MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields::get_offset_of_MPNnPbKljEMTmQdcfVDEytIoERK_1(),
	MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields::get_offset_of_gFOCzooUWrVFdddyaCYWaBvPueX_2(),
	MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields::get_offset_of_UAPhrSUGzfDxNANVwlkYYHdikQqB_3(),
	MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields::get_offset_of_MzBGEnkBVgiayhACZNrLHMLYXYh_4(),
	MiscTools_t69EF30FD8289334C1C95BFCCCE668C607CD984B5_StaticFields::get_offset_of_mAVtJlKnKLxuYqEZjOxyITXxDFE_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4995 = { sizeof (NativeTools_t02DA8C9296FF32AE2A10A7374941F56D189135CD), -1, sizeof(NativeTools_t02DA8C9296FF32AE2A10A7374941F56D189135CD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4995[1] = 
{
	NativeTools_t02DA8C9296FF32AE2A10A7374941F56D189135CD_StaticFields::get_offset_of_wlpWfLZnkDmuKDulwuFevFQQsOu_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4996 = { sizeof (ReflectionTools_tC70FA2C798DE540AF05D03972E0EF9DE435AD1E9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4997 = { sizeof (BindingFlags_t295F6C7880FE259CEB8BA62B1DB438E16F565B6F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4997[8] = 
{
	BindingFlags_t295F6C7880FE259CEB8BA62B1DB438E16F565B6F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4998 = { sizeof (SerializationTools_t27FE4DC60DE61922AB7B56E79801C4719094FCA1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4999 = { sizeof (StringTools_t9CD9E74F8892509E42E19E040353881CBFA52D3C), -1, sizeof(StringTools_t9CD9E74F8892509E42E19E040353881CBFA52D3C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4999[1] = 
{
	StringTools_t9CD9E74F8892509E42E19E040353881CBFA52D3C_StaticFields::get_offset_of_uNJiAceKjYQFOnPGlpGqdnWHAyqA_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
