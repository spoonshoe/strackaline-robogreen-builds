﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// AhZETLxKRDTxPbFmIsnmalofYne<Rewired.ActiveControllerChangedDelegate>
struct AhZETLxKRDTxPbFmIsnmalofYne_tD42028288CF7D973DC8FE62C4DC9B2FC5D81FF1B;
// AhZETLxKRDTxPbFmIsnmalofYne<Rewired.PlayerActiveControllerChangedDelegate>
struct AhZETLxKRDTxPbFmIsnmalofYne_t8042A016871DA80AEAADC294117467652BDAA49B;
// AhZETLxKRDTxPbFmIsnmalofYne<Rewired.PlayerActiveControllerChangedDelegate>[]
struct AhZETLxKRDTxPbFmIsnmalofYneU5BU5D_t6C46DF09F6A0C2E4EC91CA0DD52639756080E32A;
// DvbYkiORxIPlVcnxRmcWLbPwAdhB/sSSwLdgRIGhAHubEfzNAExVFpqE
struct sSSwLdgRIGhAHubEfzNAExVFpqE_tEC4C43E6DFB8DA01667D46FA8A9F981B8871D642;
// EhbFMnIgrveIkvxMjDinIUjSQTaf
struct EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5;
// GmNQnYIPchcrVIxASPikMTNzgbC
struct GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99;
// GmNQnYIPchcrVIxASPikMTNzgbC[]
struct GmNQnYIPchcrVIxASPikMTNzgbCU5BU5D_tB2E5A22F809105ED175CB8ED6BCBE9305D54182A;
// LkmsImmStVEvsBvZKxyBsAPXXJ
struct LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA;
// OmePuLXqingOShAfdEjGZVdOnDpG/EAJRBzBYjjtwooLSURpauuDghJn
struct EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94;
// OmePuLXqingOShAfdEjGZVdOnDpG/EAJRBzBYjjtwooLSURpauuDghJn/zMiZeuUVPippzZoUlAuLXMyxVeJ
struct zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5;
// OmePuLXqingOShAfdEjGZVdOnDpG/WjDjjZjmTrLgvroIUzlAqRnRCmzJ
struct WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99;
// OmePuLXqingOShAfdEjGZVdOnDpG/WjDjjZjmTrLgvroIUzlAqRnRCmzJ/LBaYaJDRjCvogHekEOTUEmcDnEB
struct LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A;
// OmePuLXqingOShAfdEjGZVdOnDpG/WjDjjZjmTrLgvroIUzlAqRnRCmzJ/LBaYaJDRjCvogHekEOTUEmcDnEB[]
struct LBaYaJDRjCvogHekEOTUEmcDnEBU5BU5D_t142412F855ACBF17A1129FCCC70F569A96B78A2F;
// OmePuLXqingOShAfdEjGZVdOnDpG[0...,0...]
struct OmePuLXqingOShAfdEjGZVdOnDpGU5BU2CU5D_t719ED9D943C79F90B998E2C43220749CED0C555C;
// OmePuLXqingOShAfdEjGZVdOnDpG[]
struct OmePuLXqingOShAfdEjGZVdOnDpGU5BU5D_t6371EF4B41790887302C0A070D79235FCFE476D2;
// Rewired.Axis2DCalibration
struct Axis2DCalibration_t4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1;
// Rewired.AxisCalibration
struct AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F;
// Rewired.AxisCalibration[]
struct AxisCalibrationU5BU5D_t3B1B9A305126F8F79AFD0CD2105AC2B33A92F692;
// Rewired.ButtonStateRecorder
struct ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148;
// Rewired.CalibrationMap
struct CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3;
// Rewired.Controller/Extension
struct Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD;
// Rewired.ControllerMap
struct ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB;
// Rewired.ControllerMapEnabler/ixNifobzbtiwSHPyWPtxRGELpf
struct ixNifobzbtiwSHPyWPtxRGELpf_t6742005DEB9045D54B12821D97E544A8E0DAF484;
// Rewired.ControllerSetSelector
struct ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD;
// Rewired.CustomController
struct CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A;
// Rewired.Data.ConfigVars
struct ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41;
// Rewired.Data.ControllerDataFiles
struct ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5;
// Rewired.Data.UserData
struct UserData_t23CF13BC0883AADA4A3457583033292E49A77D48;
// Rewired.HID.HIDAccelerometer[]
struct HIDAccelerometerU5BU5D_t0058D027862A38EBC89C348C2064488E2AF120DD;
// Rewired.HID.HIDAxis[]
struct HIDAxisU5BU5D_t8BF0E8BC7B27B705A2C2DDE04581B7DB595B50ED;
// Rewired.HID.HIDButton[]
struct HIDButtonU5BU5D_t622EE36EF01D50FF9EC401595CEC229A78056946;
// Rewired.HID.HIDControllerElement/HIDInfo
struct HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366;
// Rewired.HID.HIDControllerElementWithDataSet/CVaCXXbjFHbTGydqcOoiGPdeykAL
struct CVaCXXbjFHbTGydqcOoiGPdeykAL_tD7DD1276581C9E6FDF35379998DD5CEE6A2567D5;
// Rewired.HID.HIDControllerElementWithDataSet/CVaCXXbjFHbTGydqcOoiGPdeykAL[]
struct CVaCXXbjFHbTGydqcOoiGPdeykALU5BU5D_tA5D1BE9E5CF82A3FA8C7A2EB120E3B7E80A1039A;
// Rewired.HID.HIDControllerElementWithDataSet/wgqSTfYHDsMNygvPFQwICLAnDKO
struct wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5;
// Rewired.HID.HIDGyroscope[]
struct HIDGyroscopeU5BU5D_t4220A1DEFB7DA209C05DB9369B9AC6A947D39A0F;
// Rewired.HID.HIDHat[]
struct HIDHatU5BU5D_t8BD3C3729A1AF48C2DA5988763E684048ECACCBA;
// Rewired.HID.HIDLight[]
struct HIDLightU5BU5D_tD12625439187D085105DB06372883AAE5C325155;
// Rewired.HID.HIDTouchpad/TouchData[]
struct TouchDataU5BU5D_tFC196EF962568BF4100D8AD3E86E59E35926F03D;
// Rewired.HID.HIDTouchpad/TouchpadInfo
struct TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F;
// Rewired.HID.HIDTouchpad[]
struct HIDTouchpadU5BU5D_tB00C7596EE4CC3F689B9300029FCB9FC829D8BC7;
// Rewired.HID.HIDVibrationMotor[]
struct HIDVibrationMotorU5BU5D_tD42C2F405AC939F9EB00DABF5C37FD04B7E91966;
// Rewired.HID.HidOutputReportHandler/WriteReportDelegate
struct WriteReportDelegate_tE0510A5AB2468221BF3ECDD4A176FE7F3A71706F;
// Rewired.HID.HidOutputReportHandler/qQwWjhhBpjGwyXouxBiErDakyMc
struct qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2;
// Rewired.HID.SpecialDevices/xgTeshlOrQNHJRhpOldNrbEuIDD[]
struct xgTeshlOrQNHJRhpOldNrbEuIDDU5BU5D_t2AF95DE51A490AC9408AD0758B7D4CFF60DA8E6F;
// Rewired.HardwareControllerMap_Game
struct HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3;
// Rewired.HardwareJoystickMap_InputManager
struct HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0;
// Rewired.InputAction
struct InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA;
// Rewired.InputAction[]
struct InputActionU5BU5D_tD157E02666906BCC0381D6204DB99218559DD6F3;
// Rewired.InputBehavior
struct InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B;
// Rewired.InputManager_Base
struct InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701;
// Rewired.InputManagers.CustomInputManager/igstrunnCuMHZFMjGQSDGcyfZnU
struct igstrunnCuMHZFMjGQSDGcyfZnU_t8BA0D7526D539420345A84552152540A82348119;
// Rewired.Interfaces.IUnifiedKeyboardSource
struct IUnifiedKeyboardSource_t94183BB51C9B2A7CA8011D4C731C2CB013B5E591;
// Rewired.Interfaces.IUnifiedMouseSource
struct IUnifiedMouseSource_tF1958F0193D41ADC2F469AE84F413647B71B31FB;
// Rewired.Internal.StandaloneAxis
struct StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E;
// Rewired.Internal.StandaloneAxis/AxisValueChangedEventHandler
struct AxisValueChangedEventHandler_t14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237;
// Rewired.Internal.StandaloneAxis/ButtonDownEventHandler
struct ButtonDownEventHandler_tDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE;
// Rewired.Internal.StandaloneAxis/ButtonUpEventHandler
struct ButtonUpEventHandler_t7D7868DC7B0024CD5CC804C9D9BA28C302222FB9;
// Rewired.Internal.StandaloneAxis/ButtonValueChangedEventHandler
struct ButtonValueChangedEventHandler_tD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5;
// Rewired.Internal.StandaloneAxis2D/ValueChangedEventHandler
struct ValueChangedEventHandler_t4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7;
// Rewired.Keyboard
struct Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004;
// Rewired.Mouse
struct Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3;
// Rewired.PlatformInputManager
struct PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A;
// Rewired.Platforms.Custom.CustomInputSource
struct CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24;
// Rewired.Platforms.Custom.CustomInputSource/Joystick
struct Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09;
// Rewired.Player
struct Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F;
// Rewired.Player[]
struct PlayerU5BU5D_tA2EBA2C2C08EFF96D0FEA66EC1C366F3945AD33F;
// Rewired.UnityInputHelper/lYuJuZsYJJgZnLsElVIsjMUXGeV[]
struct lYuJuZsYJJgZnLsElVIsjMUXGeVU5BU5D_t0A7F75C3B268C533E1A3BE7243A800301FC78006;
// Rewired.UpdateLoopDataSet`1<Rewired.UnityUnifiedMouseSource/hJvPEYlJFiWOITXXSJHwonxNnUv>
struct UpdateLoopDataSet_1_t6CA24B93FD5588022AC0CDD55D805BDDD51F2096;
// Rewired.UpdateLoopDataSet`1<iDabdpqyvlEAXmjzfkEiOCBPdMuc/deXVkUCQurzFRSBNargAFcePsbp>
struct UpdateLoopDataSet_1_t344F79FFB18FB5709B0825099571D8D3A83C0473;
// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,OmePuLXqingOShAfdEjGZVdOnDpG/EAJRBzBYjjtwooLSURpauuDghJn/zMiZeuUVPippzZoUlAuLXMyxVeJ>
struct ADictionary_2_t1D194B1E00D38C772F722BC937ECB8D1EB16125C;
// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,Rewired.InputBehavior>
struct ADictionary_2_t5D2F9858F224DDD739AF42F21ABDAD714A682FA2;
// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,lUbzJtkApcIkohMrHhJAmcDImRRR/JLPbgmCslxcSncPKJybubLvESakA>
struct ADictionary_2_tBF46E6C105A19DB0AC69B62AF4603EA0D774ECAB;
// Rewired.Utils.Classes.Data.ADictionary`2<System.String,nkGLVKXmQVVuJNEogVVmAwwQOJl/tFGjMRkSnzMdvjlUOfGbmVBOoWy>
struct ADictionary_2_tB857704A7EC12DF048058CE5530F82C7C1FF3253;
// Rewired.Utils.Classes.Data.ExpandableArray_DataContainer`1<Rewired.HID.HIDGyroscope/hoiXDorKAcXnjheVDxCUSVqObdh>
struct ExpandableArray_DataContainer_1_tE100BB3581A1C6D56FD572CFE6FEBB136AC77E7C;
// Rewired.Utils.Classes.Data.ExpandableArray_DataContainer`1<SoMwFHWdfFmaNnPQuSGWlSuWHDi/mtCbAGdYHzRqiPnOmbyDgpWTYHdl/MVPpNOKkKsBHVbOLknceWHXOeUB>
struct ExpandableArray_DataContainer_1_t07F3C761135F4A055596287551ABF7675C077873;
// Rewired.Utils.Classes.Data.NativeBuffer
struct NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11;
// Rewired.Utils.Classes.Utility.ObjectPool`1<iDabdpqyvlEAXmjzfkEiOCBPdMuc>
struct ObjectPool_1_tB0F74D18847376089F85915E3CE5B7A8CD181D2C;
// Rewired.Utils.Classes.Utility.ThreadHelper
struct ThreadHelper_t4B3E611CF122FA6B00FFF5BE3B6CCAE8399F35CF;
// Rewired.Utils.Classes.Utility.TimerAbs
struct TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429;
// SoMwFHWdfFmaNnPQuSGWlSuWHDi
struct SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6;
// SoMwFHWdfFmaNnPQuSGWlSuWHDi/mtCbAGdYHzRqiPnOmbyDgpWTYHdl
struct mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F;
// SoMwFHWdfFmaNnPQuSGWlSuWHDi/mtCbAGdYHzRqiPnOmbyDgpWTYHdl[]
struct mtCbAGdYHzRqiPnOmbyDgpWTYHdlU5BU5D_tB58DCAF360AEA76C17C02C816B58FDE76058A165;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Rewired.BridgedController>
struct Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5;
// System.Action`1<Rewired.ControllerDisconnectedEventArgs>
struct Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0;
// System.Action`1<Rewired.ControllerStatusChangedEventArgs>
struct Action_1_t42630A9FFF8BE0214466A318ED987BB54B016B80;
// System.Action`1<Rewired.HID.OutputReport>
struct Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832;
// System.Action`1<Rewired.UpdateControllerInfoEventArgs>
struct Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3;
// System.Action`1<iDabdpqyvlEAXmjzfkEiOCBPdMuc>
struct Action_1_t2D38188C06D89FCBF7A89B8A484072049DA402D3;
// System.Action`2<Rewired.ControllerType,System.Int32>
struct Action_2_t11BE1B5AD80237AA8DDBE0EF6610F8369C77941E;
// System.Action`2<Rewired.Utils.Classes.Data.NativeBuffer,Rewired.HID.HIDTouchpad/TouchData[]>
struct Action_2_t7A013AB8DF7D757FD04ED42C96A7751D8A139398;
// System.Action`2<System.Byte[],System.Single[]>
struct Action_2_t6348996BC3E0C8743F528E4FA9E5118FC95A1FEB;
// System.Action`2<System.Int32,Rewired.ControllerDataUpdater>
struct Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A;
// System.Action`3<System.Boolean,System.Int32,System.Int32>
struct Action_3_tCC0C451189B08E60B355D3B8F0030B4E812C0A1D;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Data.Mapping.AxisCalibrationInfo>
struct Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03;
// System.Collections.Generic.IList`1<Rewired.AxisCalibration>
struct IList_1_t04731700188653DCBC190A7C04FFDE7ABE92F786;
// System.Collections.Generic.IList`1<Rewired.CustomController>
struct IList_1_t72F8D0A343B835033A661FC40376E4E35F64327B;
// System.Collections.Generic.IList`1<Rewired.InputBehavior>
struct IList_1_tE69D8FDDF240FFBEE8DD59F7381C2DFA26A34F5C;
// System.Collections.Generic.IList`1<Rewired.Joystick>
struct IList_1_tF47B0A47132A7F677E03201FFE653320541BBA20;
// System.Collections.Generic.IList`1<Rewired.Player>
struct IList_1_t6E46C98ED98DF6EC33AA741543131318B0F5A06A;
// System.Collections.Generic.List`1<DvbYkiORxIPlVcnxRmcWLbPwAdhB/jzMnORPzOilIJZDTJTcDzOnMbEdi>
struct List_1_t3531148A82CAFAF7C56CC59256C74BD2CA5CB26F;
// System.Collections.Generic.List`1<DvbYkiORxIPlVcnxRmcWLbPwAdhB/sSSwLdgRIGhAHubEfzNAExVFpqE/bUptQGxKYmtFEzDAvGXbiJIbkczf>
struct List_1_t872743667E9882BCD5C4B7BD4F792103DDE63C2E;
// System.Collections.Generic.List`1<Rewired.Controller>
struct List_1_t20C185E0480AE6CA25B3B6AD936AA34CDE1EC518;
// System.Collections.Generic.List`1<Rewired.ControllerMapEnabler/Rule>
struct List_1_t93FFD47C81E2F494B000EF49C186820C085FCA43;
// System.Collections.Generic.List`1<Rewired.ControllerMapEnabler/RuleSet>
struct List_1_tCE05C39A3E7EF93981D38C62BD754C0D392F09EC;
// System.Collections.Generic.List`1<Rewired.CustomController>
struct List_1_tB037A3809838545DA54CE039436BC1BF87578A9B;
// System.Collections.Generic.List`1<Rewired.InputActionSourceData>
struct List_1_t44209413D8D2B694B5619AA3630215F6729C4528;
// System.Collections.Generic.List`1<Rewired.InputBehavior>
struct List_1_t00493746A9C09D2A4E213DD073FD95F9A4FAA144;
// System.Collections.Generic.List`1<Rewired.InputManagers.CustomInputManager/OSbOCXhMGybBoudblkgMAExKfJb>
struct List_1_t68D5DD05DB0C6C03A43191A3A25FD11A787C15BD;
// System.Collections.Generic.List`1<Rewired.InputManagers.CustomInputManager/igstrunnCuMHZFMjGQSDGcyfZnU/SAPqXzqjGZlmsJaBMWgsmLJhEOu>
struct List_1_t76A8E92538DC15148CD293B8684B84AD860EDA6C;
// System.Collections.Generic.List`1<Rewired.Joystick>
struct List_1_tA72AFBAA71AD07F0E9CDE70C7F4744366B2E2D8B;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.Queue`1<Rewired.HID.HIDTouchpad/TouchData>
struct Queue_1_tCA0F05DEAE19C30F80931FD49F701E7FD431028A;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller>
struct ReadOnlyCollection_1_t037BB93EEC8E496E6B7901A805B53290BC747353;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.InputAction>
struct ReadOnlyCollection_1_tB6849D2EAAF58FF4A5ADFB4DE2B3C5B577594DCC;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.InputActionSourceData>
struct ReadOnlyCollection_1_t341D5CFD873149C1CE3509FD2066B741CBCE8044;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`1<Rewired.UnityUnifiedMouseSource/hJvPEYlJFiWOITXXSJHwonxNnUv>
struct Func_1_tCB096340A0ECB0571AFD6D0FAA95AC42D108DD98;
// System.Func`1<System.Int32>
struct Func_1_t30631A63BE46FE93700939B764202D360449FE30;
// System.Func`1<System.Single>
struct Func_1_tA655F82C43D9C03F75E783D0DF2629CF9676E735;
// System.Func`1<iDabdpqyvlEAXmjzfkEiOCBPdMuc/deXVkUCQurzFRSBNargAFcePsbp>
struct Func_1_t0D5025F33D441612826295FBD43351ED3FA668AF;
// System.Func`1<iDabdpqyvlEAXmjzfkEiOCBPdMuc>
struct Func_1_tFCBCFB47A5A1ECA4BFAE4B15DD3E01CB5A3CFB4B;
// System.Func`2<Rewired.BridgedControllerHWInfo,Rewired.HardwareJoystickMap_InputManager>
struct Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF;
// System.Func`2<Rewired.HID.OutputReport,System.Boolean>
struct Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// XRsvvJyCXrJjMmEnAyacvMwIrT
struct XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3;
// iDabdpqyvlEAXmjzfkEiOCBPdMuc[]
struct iDabdpqyvlEAXmjzfkEiOCBPdMucU5BU5D_tAEED0569CDC25A1F22C37A514772B6A59E6DCD58;
// iNmAhOSEGUcqVFxmGFasdFHfdnr
struct iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C;
// iNmAhOSEGUcqVFxmGFasdFHfdnr[]
struct iNmAhOSEGUcqVFxmGFasdFHfdnrU5BU5D_tBC2F37DBA40E9A0ADD343BF2EB391385C7D5FA69;
// jufVWXOncnQzwwsvxneDefLrtLg[]
struct jufVWXOncnQzwwsvxneDefLrtLgU5BU5D_tDE07F616F33597DE5FCF94F1B020CBA6C5345F09;
// lUbzJtkApcIkohMrHhJAmcDImRRR
struct lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE;
// nkGLVKXmQVVuJNEogVVmAwwQOJl
struct nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22;
// nkGLVKXmQVVuJNEogVVmAwwQOJl/tFGjMRkSnzMdvjlUOfGbmVBOoWy[]
struct tFGjMRkSnzMdvjlUOfGbmVBOoWyU5BU5D_tD4F19941C97928551C285DB3314FAF0F7571D942;
// rIcvLcWCuQghgaaiofvKjSvndJX
struct rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224;
// xvUPNQnWmcdqDjulQTyAIyRAxwG
struct xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032;

struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_com;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SSSWLDGRIGHAHUBEFZNAEXVFPQE_TEC4C43E6DFB8DA01667D46FA8A9F981B8871D642_H
#define SSSWLDGRIGHAHUBEFZNAEXVFPQE_TEC4C43E6DFB8DA01667D46FA8A9F981B8871D642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DvbYkiORxIPlVcnxRmcWLbPwAdhB_sSSwLdgRIGhAHubEfzNAExVFpqE
struct  sSSwLdgRIGhAHubEfzNAExVFpqE_tEC4C43E6DFB8DA01667D46FA8A9F981B8871D642  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<DvbYkiORxIPlVcnxRmcWLbPwAdhB_sSSwLdgRIGhAHubEfzNAExVFpqE_bUptQGxKYmtFEzDAvGXbiJIbkczf> DvbYkiORxIPlVcnxRmcWLbPwAdhB_sSSwLdgRIGhAHubEfzNAExVFpqE::AHOFIsQodiSzMcHMSUpdlsGdKfH
	List_1_t872743667E9882BCD5C4B7BD4F792103DDE63C2E * ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0;

public:
	inline static int32_t get_offset_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() { return static_cast<int32_t>(offsetof(sSSwLdgRIGhAHubEfzNAExVFpqE_tEC4C43E6DFB8DA01667D46FA8A9F981B8871D642, ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0)); }
	inline List_1_t872743667E9882BCD5C4B7BD4F792103DDE63C2E * get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() const { return ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0; }
	inline List_1_t872743667E9882BCD5C4B7BD4F792103DDE63C2E ** get_address_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() { return &___AHOFIsQodiSzMcHMSUpdlsGdKfH_0; }
	inline void set_AHOFIsQodiSzMcHMSUpdlsGdKfH_0(List_1_t872743667E9882BCD5C4B7BD4F792103DDE63C2E * value)
	{
		___AHOFIsQodiSzMcHMSUpdlsGdKfH_0 = value;
		Il2CppCodeGenWriteBarrier((&___AHOFIsQodiSzMcHMSUpdlsGdKfH_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSSWLDGRIGHAHUBEFZNAEXVFPQE_TEC4C43E6DFB8DA01667D46FA8A9F981B8871D642_H
#ifndef BUPTQGXKYMTFEZDAVGXBIJIBKCZF_T6A0E7CFEFE93C50BF31EE4989A128A205B1015C1_H
#define BUPTQGXKYMTFEZDAVGXBIJIBKCZF_T6A0E7CFEFE93C50BF31EE4989A128A205B1015C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DvbYkiORxIPlVcnxRmcWLbPwAdhB_sSSwLdgRIGhAHubEfzNAExVFpqE_bUptQGxKYmtFEzDAvGXbiJIbkczf
struct  bUptQGxKYmtFEzDAvGXbiJIbkczf_t6A0E7CFEFE93C50BF31EE4989A128A205B1015C1  : public RuntimeObject
{
public:
	// System.Int32 DvbYkiORxIPlVcnxRmcWLbPwAdhB_sSSwLdgRIGhAHubEfzNAExVFpqE_bUptQGxKYmtFEzDAvGXbiJIbkczf::nvorGNbxnIKRTEQakQRxoDaDbkF
	int32_t ___nvorGNbxnIKRTEQakQRxoDaDbkF_0;
	// System.Int32 DvbYkiORxIPlVcnxRmcWLbPwAdhB_sSSwLdgRIGhAHubEfzNAExVFpqE_bUptQGxKYmtFEzDAvGXbiJIbkczf::ynRWyQCwQkkFzvADNXNoLIVFuMQ
	int32_t ___ynRWyQCwQkkFzvADNXNoLIVFuMQ_1;
	// System.String DvbYkiORxIPlVcnxRmcWLbPwAdhB_sSSwLdgRIGhAHubEfzNAExVFpqE_bUptQGxKYmtFEzDAvGXbiJIbkczf::lXiIYlzqhDMJpyJyatjvSnwcuzH
	String_t* ___lXiIYlzqhDMJpyJyatjvSnwcuzH_2;
	// System.Int32 DvbYkiORxIPlVcnxRmcWLbPwAdhB_sSSwLdgRIGhAHubEfzNAExVFpqE_bUptQGxKYmtFEzDAvGXbiJIbkczf::fYYSPmKTkyNnvcLPwwaCPYkcWqv
	int32_t ___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3;

public:
	inline static int32_t get_offset_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0() { return static_cast<int32_t>(offsetof(bUptQGxKYmtFEzDAvGXbiJIbkczf_t6A0E7CFEFE93C50BF31EE4989A128A205B1015C1, ___nvorGNbxnIKRTEQakQRxoDaDbkF_0)); }
	inline int32_t get_nvorGNbxnIKRTEQakQRxoDaDbkF_0() const { return ___nvorGNbxnIKRTEQakQRxoDaDbkF_0; }
	inline int32_t* get_address_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0() { return &___nvorGNbxnIKRTEQakQRxoDaDbkF_0; }
	inline void set_nvorGNbxnIKRTEQakQRxoDaDbkF_0(int32_t value)
	{
		___nvorGNbxnIKRTEQakQRxoDaDbkF_0 = value;
	}

	inline static int32_t get_offset_of_ynRWyQCwQkkFzvADNXNoLIVFuMQ_1() { return static_cast<int32_t>(offsetof(bUptQGxKYmtFEzDAvGXbiJIbkczf_t6A0E7CFEFE93C50BF31EE4989A128A205B1015C1, ___ynRWyQCwQkkFzvADNXNoLIVFuMQ_1)); }
	inline int32_t get_ynRWyQCwQkkFzvADNXNoLIVFuMQ_1() const { return ___ynRWyQCwQkkFzvADNXNoLIVFuMQ_1; }
	inline int32_t* get_address_of_ynRWyQCwQkkFzvADNXNoLIVFuMQ_1() { return &___ynRWyQCwQkkFzvADNXNoLIVFuMQ_1; }
	inline void set_ynRWyQCwQkkFzvADNXNoLIVFuMQ_1(int32_t value)
	{
		___ynRWyQCwQkkFzvADNXNoLIVFuMQ_1 = value;
	}

	inline static int32_t get_offset_of_lXiIYlzqhDMJpyJyatjvSnwcuzH_2() { return static_cast<int32_t>(offsetof(bUptQGxKYmtFEzDAvGXbiJIbkczf_t6A0E7CFEFE93C50BF31EE4989A128A205B1015C1, ___lXiIYlzqhDMJpyJyatjvSnwcuzH_2)); }
	inline String_t* get_lXiIYlzqhDMJpyJyatjvSnwcuzH_2() const { return ___lXiIYlzqhDMJpyJyatjvSnwcuzH_2; }
	inline String_t** get_address_of_lXiIYlzqhDMJpyJyatjvSnwcuzH_2() { return &___lXiIYlzqhDMJpyJyatjvSnwcuzH_2; }
	inline void set_lXiIYlzqhDMJpyJyatjvSnwcuzH_2(String_t* value)
	{
		___lXiIYlzqhDMJpyJyatjvSnwcuzH_2 = value;
		Il2CppCodeGenWriteBarrier((&___lXiIYlzqhDMJpyJyatjvSnwcuzH_2), value);
	}

	inline static int32_t get_offset_of_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3() { return static_cast<int32_t>(offsetof(bUptQGxKYmtFEzDAvGXbiJIbkczf_t6A0E7CFEFE93C50BF31EE4989A128A205B1015C1, ___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3)); }
	inline int32_t get_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3() const { return ___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3; }
	inline int32_t* get_address_of_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3() { return &___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3; }
	inline void set_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3(int32_t value)
	{
		___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUPTQGXKYMTFEZDAVGXBIJIBKCZF_T6A0E7CFEFE93C50BF31EE4989A128A205B1015C1_H
#ifndef LKMSIMMSTVEVSBVZKXYBSAPXXJ_T77053E43086638C76E5D5A1B8C41882906E619FA_H
#define LKMSIMMSTVEVSBVZKXYBSAPXXJ_T77053E43086638C76E5D5A1B8C41882906E619FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LkmsImmStVEvsBvZKxyBsAPXXJ
struct  LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA  : public RuntimeObject
{
public:
	// System.Int32 LkmsImmStVEvsBvZKxyBsAPXXJ::egbgDzhjndOTKCklEgspYpnNGmim
	int32_t ___egbgDzhjndOTKCklEgspYpnNGmim_0;
	// System.Int32 LkmsImmStVEvsBvZKxyBsAPXXJ::anPcJjMsMDSJwvtMEoZwvAUataJ
	int32_t ___anPcJjMsMDSJwvtMEoZwvAUataJ_1;
	// Rewired.Player LkmsImmStVEvsBvZKxyBsAPXXJ::loWjoBuDbZXrdQdTsQFIrOmbiYA
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___loWjoBuDbZXrdQdTsQFIrOmbiYA_2;
	// Rewired.Player[] LkmsImmStVEvsBvZKxyBsAPXXJ::PhOrdQVRimdzfuIpLkOsoIGzHIeD
	PlayerU5BU5D_tA2EBA2C2C08EFF96D0FEA66EC1C366F3945AD33F* ___PhOrdQVRimdzfuIpLkOsoIGzHIeD_3;
	// Rewired.Player[] LkmsImmStVEvsBvZKxyBsAPXXJ::IjcqxMjNVxBsZYkDWgLiYmCBJsi
	PlayerU5BU5D_tA2EBA2C2C08EFF96D0FEA66EC1C366F3945AD33F* ___IjcqxMjNVxBsZYkDWgLiYmCBJsi_4;
	// System.Collections.Generic.IList`1<Rewired.Player> LkmsImmStVEvsBvZKxyBsAPXXJ::EcJHmTtpwPjcbSwyMcKCsDWZObj
	RuntimeObject* ___EcJHmTtpwPjcbSwyMcKCsDWZObj_5;
	// System.Collections.Generic.IList`1<Rewired.Player> LkmsImmStVEvsBvZKxyBsAPXXJ::ZvHntwlAZZfXbFvKCBDFrhulswnd
	RuntimeObject* ___ZvHntwlAZZfXbFvKCBDFrhulswnd_6;
	// Rewired.Data.ConfigVars LkmsImmStVEvsBvZKxyBsAPXXJ::bsdXVIEsRZlaTBlTrOAhsApnYKn
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * ___bsdXVIEsRZlaTBlTrOAhsApnYKn_7;
	// System.Boolean LkmsImmStVEvsBvZKxyBsAPXXJ::ffNWJUuxPxuFMIZszOmCdhFAkUp
	bool ___ffNWJUuxPxuFMIZszOmCdhFAkUp_8;

public:
	inline static int32_t get_offset_of_egbgDzhjndOTKCklEgspYpnNGmim_0() { return static_cast<int32_t>(offsetof(LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA, ___egbgDzhjndOTKCklEgspYpnNGmim_0)); }
	inline int32_t get_egbgDzhjndOTKCklEgspYpnNGmim_0() const { return ___egbgDzhjndOTKCklEgspYpnNGmim_0; }
	inline int32_t* get_address_of_egbgDzhjndOTKCklEgspYpnNGmim_0() { return &___egbgDzhjndOTKCklEgspYpnNGmim_0; }
	inline void set_egbgDzhjndOTKCklEgspYpnNGmim_0(int32_t value)
	{
		___egbgDzhjndOTKCklEgspYpnNGmim_0 = value;
	}

	inline static int32_t get_offset_of_anPcJjMsMDSJwvtMEoZwvAUataJ_1() { return static_cast<int32_t>(offsetof(LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA, ___anPcJjMsMDSJwvtMEoZwvAUataJ_1)); }
	inline int32_t get_anPcJjMsMDSJwvtMEoZwvAUataJ_1() const { return ___anPcJjMsMDSJwvtMEoZwvAUataJ_1; }
	inline int32_t* get_address_of_anPcJjMsMDSJwvtMEoZwvAUataJ_1() { return &___anPcJjMsMDSJwvtMEoZwvAUataJ_1; }
	inline void set_anPcJjMsMDSJwvtMEoZwvAUataJ_1(int32_t value)
	{
		___anPcJjMsMDSJwvtMEoZwvAUataJ_1 = value;
	}

	inline static int32_t get_offset_of_loWjoBuDbZXrdQdTsQFIrOmbiYA_2() { return static_cast<int32_t>(offsetof(LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA, ___loWjoBuDbZXrdQdTsQFIrOmbiYA_2)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_loWjoBuDbZXrdQdTsQFIrOmbiYA_2() const { return ___loWjoBuDbZXrdQdTsQFIrOmbiYA_2; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_loWjoBuDbZXrdQdTsQFIrOmbiYA_2() { return &___loWjoBuDbZXrdQdTsQFIrOmbiYA_2; }
	inline void set_loWjoBuDbZXrdQdTsQFIrOmbiYA_2(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___loWjoBuDbZXrdQdTsQFIrOmbiYA_2 = value;
		Il2CppCodeGenWriteBarrier((&___loWjoBuDbZXrdQdTsQFIrOmbiYA_2), value);
	}

	inline static int32_t get_offset_of_PhOrdQVRimdzfuIpLkOsoIGzHIeD_3() { return static_cast<int32_t>(offsetof(LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA, ___PhOrdQVRimdzfuIpLkOsoIGzHIeD_3)); }
	inline PlayerU5BU5D_tA2EBA2C2C08EFF96D0FEA66EC1C366F3945AD33F* get_PhOrdQVRimdzfuIpLkOsoIGzHIeD_3() const { return ___PhOrdQVRimdzfuIpLkOsoIGzHIeD_3; }
	inline PlayerU5BU5D_tA2EBA2C2C08EFF96D0FEA66EC1C366F3945AD33F** get_address_of_PhOrdQVRimdzfuIpLkOsoIGzHIeD_3() { return &___PhOrdQVRimdzfuIpLkOsoIGzHIeD_3; }
	inline void set_PhOrdQVRimdzfuIpLkOsoIGzHIeD_3(PlayerU5BU5D_tA2EBA2C2C08EFF96D0FEA66EC1C366F3945AD33F* value)
	{
		___PhOrdQVRimdzfuIpLkOsoIGzHIeD_3 = value;
		Il2CppCodeGenWriteBarrier((&___PhOrdQVRimdzfuIpLkOsoIGzHIeD_3), value);
	}

	inline static int32_t get_offset_of_IjcqxMjNVxBsZYkDWgLiYmCBJsi_4() { return static_cast<int32_t>(offsetof(LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA, ___IjcqxMjNVxBsZYkDWgLiYmCBJsi_4)); }
	inline PlayerU5BU5D_tA2EBA2C2C08EFF96D0FEA66EC1C366F3945AD33F* get_IjcqxMjNVxBsZYkDWgLiYmCBJsi_4() const { return ___IjcqxMjNVxBsZYkDWgLiYmCBJsi_4; }
	inline PlayerU5BU5D_tA2EBA2C2C08EFF96D0FEA66EC1C366F3945AD33F** get_address_of_IjcqxMjNVxBsZYkDWgLiYmCBJsi_4() { return &___IjcqxMjNVxBsZYkDWgLiYmCBJsi_4; }
	inline void set_IjcqxMjNVxBsZYkDWgLiYmCBJsi_4(PlayerU5BU5D_tA2EBA2C2C08EFF96D0FEA66EC1C366F3945AD33F* value)
	{
		___IjcqxMjNVxBsZYkDWgLiYmCBJsi_4 = value;
		Il2CppCodeGenWriteBarrier((&___IjcqxMjNVxBsZYkDWgLiYmCBJsi_4), value);
	}

	inline static int32_t get_offset_of_EcJHmTtpwPjcbSwyMcKCsDWZObj_5() { return static_cast<int32_t>(offsetof(LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA, ___EcJHmTtpwPjcbSwyMcKCsDWZObj_5)); }
	inline RuntimeObject* get_EcJHmTtpwPjcbSwyMcKCsDWZObj_5() const { return ___EcJHmTtpwPjcbSwyMcKCsDWZObj_5; }
	inline RuntimeObject** get_address_of_EcJHmTtpwPjcbSwyMcKCsDWZObj_5() { return &___EcJHmTtpwPjcbSwyMcKCsDWZObj_5; }
	inline void set_EcJHmTtpwPjcbSwyMcKCsDWZObj_5(RuntimeObject* value)
	{
		___EcJHmTtpwPjcbSwyMcKCsDWZObj_5 = value;
		Il2CppCodeGenWriteBarrier((&___EcJHmTtpwPjcbSwyMcKCsDWZObj_5), value);
	}

	inline static int32_t get_offset_of_ZvHntwlAZZfXbFvKCBDFrhulswnd_6() { return static_cast<int32_t>(offsetof(LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA, ___ZvHntwlAZZfXbFvKCBDFrhulswnd_6)); }
	inline RuntimeObject* get_ZvHntwlAZZfXbFvKCBDFrhulswnd_6() const { return ___ZvHntwlAZZfXbFvKCBDFrhulswnd_6; }
	inline RuntimeObject** get_address_of_ZvHntwlAZZfXbFvKCBDFrhulswnd_6() { return &___ZvHntwlAZZfXbFvKCBDFrhulswnd_6; }
	inline void set_ZvHntwlAZZfXbFvKCBDFrhulswnd_6(RuntimeObject* value)
	{
		___ZvHntwlAZZfXbFvKCBDFrhulswnd_6 = value;
		Il2CppCodeGenWriteBarrier((&___ZvHntwlAZZfXbFvKCBDFrhulswnd_6), value);
	}

	inline static int32_t get_offset_of_bsdXVIEsRZlaTBlTrOAhsApnYKn_7() { return static_cast<int32_t>(offsetof(LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA, ___bsdXVIEsRZlaTBlTrOAhsApnYKn_7)); }
	inline ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * get_bsdXVIEsRZlaTBlTrOAhsApnYKn_7() const { return ___bsdXVIEsRZlaTBlTrOAhsApnYKn_7; }
	inline ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 ** get_address_of_bsdXVIEsRZlaTBlTrOAhsApnYKn_7() { return &___bsdXVIEsRZlaTBlTrOAhsApnYKn_7; }
	inline void set_bsdXVIEsRZlaTBlTrOAhsApnYKn_7(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * value)
	{
		___bsdXVIEsRZlaTBlTrOAhsApnYKn_7 = value;
		Il2CppCodeGenWriteBarrier((&___bsdXVIEsRZlaTBlTrOAhsApnYKn_7), value);
	}

	inline static int32_t get_offset_of_ffNWJUuxPxuFMIZszOmCdhFAkUp_8() { return static_cast<int32_t>(offsetof(LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA, ___ffNWJUuxPxuFMIZszOmCdhFAkUp_8)); }
	inline bool get_ffNWJUuxPxuFMIZszOmCdhFAkUp_8() const { return ___ffNWJUuxPxuFMIZszOmCdhFAkUp_8; }
	inline bool* get_address_of_ffNWJUuxPxuFMIZszOmCdhFAkUp_8() { return &___ffNWJUuxPxuFMIZszOmCdhFAkUp_8; }
	inline void set_ffNWJUuxPxuFMIZszOmCdhFAkUp_8(bool value)
	{
		___ffNWJUuxPxuFMIZszOmCdhFAkUp_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LKMSIMMSTVEVSBVZKXYBSAPXXJ_T77053E43086638C76E5D5A1B8C41882906E619FA_H
#ifndef WJDJJZJMTRLGVROIUZLAQRNRCMZJ_T8846D339F090A739301F627234D2FE3F00045B99_H
#define WJDJJZJMTRLGVROIUZLAQRNRCMZJ_T8846D339F090A739301F627234D2FE3F00045B99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ
struct  WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99  : public RuntimeObject
{
public:
	// OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB[] OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ::XzwvNxyYkXcaiidxwJGvQNwBigT
	LBaYaJDRjCvogHekEOTUEmcDnEBU5BU5D_t142412F855ACBF17A1129FCCC70F569A96B78A2F* ___XzwvNxyYkXcaiidxwJGvQNwBigT_0;
	// System.Int32[] OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ::bJGqRCGyLwMdbyXFtRqWyHanjhF
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___bJGqRCGyLwMdbyXFtRqWyHanjhF_1;
	// System.Int32 OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ::uiElHIMsJhOUGwxstGSvtQgdJHm
	int32_t ___uiElHIMsJhOUGwxstGSvtQgdJHm_2;
	// OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ::YaDAnfimULzphKnSejtwEcfSlkEJ
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A * ___YaDAnfimULzphKnSejtwEcfSlkEJ_3;

public:
	inline static int32_t get_offset_of_XzwvNxyYkXcaiidxwJGvQNwBigT_0() { return static_cast<int32_t>(offsetof(WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99, ___XzwvNxyYkXcaiidxwJGvQNwBigT_0)); }
	inline LBaYaJDRjCvogHekEOTUEmcDnEBU5BU5D_t142412F855ACBF17A1129FCCC70F569A96B78A2F* get_XzwvNxyYkXcaiidxwJGvQNwBigT_0() const { return ___XzwvNxyYkXcaiidxwJGvQNwBigT_0; }
	inline LBaYaJDRjCvogHekEOTUEmcDnEBU5BU5D_t142412F855ACBF17A1129FCCC70F569A96B78A2F** get_address_of_XzwvNxyYkXcaiidxwJGvQNwBigT_0() { return &___XzwvNxyYkXcaiidxwJGvQNwBigT_0; }
	inline void set_XzwvNxyYkXcaiidxwJGvQNwBigT_0(LBaYaJDRjCvogHekEOTUEmcDnEBU5BU5D_t142412F855ACBF17A1129FCCC70F569A96B78A2F* value)
	{
		___XzwvNxyYkXcaiidxwJGvQNwBigT_0 = value;
		Il2CppCodeGenWriteBarrier((&___XzwvNxyYkXcaiidxwJGvQNwBigT_0), value);
	}

	inline static int32_t get_offset_of_bJGqRCGyLwMdbyXFtRqWyHanjhF_1() { return static_cast<int32_t>(offsetof(WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99, ___bJGqRCGyLwMdbyXFtRqWyHanjhF_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_bJGqRCGyLwMdbyXFtRqWyHanjhF_1() const { return ___bJGqRCGyLwMdbyXFtRqWyHanjhF_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_bJGqRCGyLwMdbyXFtRqWyHanjhF_1() { return &___bJGqRCGyLwMdbyXFtRqWyHanjhF_1; }
	inline void set_bJGqRCGyLwMdbyXFtRqWyHanjhF_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___bJGqRCGyLwMdbyXFtRqWyHanjhF_1 = value;
		Il2CppCodeGenWriteBarrier((&___bJGqRCGyLwMdbyXFtRqWyHanjhF_1), value);
	}

	inline static int32_t get_offset_of_uiElHIMsJhOUGwxstGSvtQgdJHm_2() { return static_cast<int32_t>(offsetof(WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99, ___uiElHIMsJhOUGwxstGSvtQgdJHm_2)); }
	inline int32_t get_uiElHIMsJhOUGwxstGSvtQgdJHm_2() const { return ___uiElHIMsJhOUGwxstGSvtQgdJHm_2; }
	inline int32_t* get_address_of_uiElHIMsJhOUGwxstGSvtQgdJHm_2() { return &___uiElHIMsJhOUGwxstGSvtQgdJHm_2; }
	inline void set_uiElHIMsJhOUGwxstGSvtQgdJHm_2(int32_t value)
	{
		___uiElHIMsJhOUGwxstGSvtQgdJHm_2 = value;
	}

	inline static int32_t get_offset_of_YaDAnfimULzphKnSejtwEcfSlkEJ_3() { return static_cast<int32_t>(offsetof(WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99, ___YaDAnfimULzphKnSejtwEcfSlkEJ_3)); }
	inline LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A * get_YaDAnfimULzphKnSejtwEcfSlkEJ_3() const { return ___YaDAnfimULzphKnSejtwEcfSlkEJ_3; }
	inline LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A ** get_address_of_YaDAnfimULzphKnSejtwEcfSlkEJ_3() { return &___YaDAnfimULzphKnSejtwEcfSlkEJ_3; }
	inline void set_YaDAnfimULzphKnSejtwEcfSlkEJ_3(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A * value)
	{
		___YaDAnfimULzphKnSejtwEcfSlkEJ_3 = value;
		Il2CppCodeGenWriteBarrier((&___YaDAnfimULzphKnSejtwEcfSlkEJ_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WJDJJZJMTRLGVROIUZLAQRNRCMZJ_T8846D339F090A739301F627234D2FE3F00045B99_H
#ifndef CALIBRATIONMAP_T52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3_H
#define CALIBRATIONMAP_T52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.CalibrationMap
struct  CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3  : public RuntimeObject
{
public:
	// Rewired.AxisCalibration[] Rewired.CalibrationMap::ewtEGQKoSuqZGbCfKTwTvaVIdwwn
	AxisCalibrationU5BU5D_t3B1B9A305126F8F79AFD0CD2105AC2B33A92F692* ___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_0;
	// System.Collections.Generic.IList`1<Rewired.AxisCalibration> Rewired.CalibrationMap::AHeRqytYRGxoDmzkycBJFsLjKkW
	RuntimeObject* ___AHeRqytYRGxoDmzkycBJFsLjKkW_1;
	// System.Int32 Rewired.CalibrationMap::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_2;

public:
	inline static int32_t get_offset_of_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_0() { return static_cast<int32_t>(offsetof(CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3, ___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_0)); }
	inline AxisCalibrationU5BU5D_t3B1B9A305126F8F79AFD0CD2105AC2B33A92F692* get_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_0() const { return ___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_0; }
	inline AxisCalibrationU5BU5D_t3B1B9A305126F8F79AFD0CD2105AC2B33A92F692** get_address_of_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_0() { return &___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_0; }
	inline void set_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_0(AxisCalibrationU5BU5D_t3B1B9A305126F8F79AFD0CD2105AC2B33A92F692* value)
	{
		___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_0 = value;
		Il2CppCodeGenWriteBarrier((&___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_0), value);
	}

	inline static int32_t get_offset_of_AHeRqytYRGxoDmzkycBJFsLjKkW_1() { return static_cast<int32_t>(offsetof(CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3, ___AHeRqytYRGxoDmzkycBJFsLjKkW_1)); }
	inline RuntimeObject* get_AHeRqytYRGxoDmzkycBJFsLjKkW_1() const { return ___AHeRqytYRGxoDmzkycBJFsLjKkW_1; }
	inline RuntimeObject** get_address_of_AHeRqytYRGxoDmzkycBJFsLjKkW_1() { return &___AHeRqytYRGxoDmzkycBJFsLjKkW_1; }
	inline void set_AHeRqytYRGxoDmzkycBJFsLjKkW_1(RuntimeObject* value)
	{
		___AHeRqytYRGxoDmzkycBJFsLjKkW_1 = value;
		Il2CppCodeGenWriteBarrier((&___AHeRqytYRGxoDmzkycBJFsLjKkW_1), value);
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_2() { return static_cast<int32_t>(offsetof(CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_2)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_2() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_2; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_2() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_2; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_2(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALIBRATIONMAP_T52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3_H
#ifndef CONTROLLERMAPENABLER_T72022B3413E35CE64277E9F8F6F64236C8F0148D_H
#define CONTROLLERMAPENABLER_T72022B3413E35CE64277E9F8F6F64236C8F0148D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapEnabler
struct  ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D  : public RuntimeObject
{
public:
	// System.Boolean Rewired.ControllerMapEnabler::eWTjKkaJVjCZqrsQHQXMtJYuNhD
	bool ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_0;
	// Rewired.Player Rewired.ControllerMapEnabler::VZYtnnAqBdPNaSltSWGWGTTbFPN
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___VZYtnnAqBdPNaSltSWGWGTTbFPN_1;
	// Rewired.ControllerMapEnabler_ixNifobzbtiwSHPyWPtxRGELpf Rewired.ControllerMapEnabler::HEDjmuguOtJTvuChkRgbhJaBjAU
	ixNifobzbtiwSHPyWPtxRGELpf_t6742005DEB9045D54B12821D97E544A8E0DAF484 * ___HEDjmuguOtJTvuChkRgbhJaBjAU_2;
	// System.Int32 Rewired.ControllerMapEnabler::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_3;
	// System.Collections.Generic.List`1<Rewired.ControllerMapEnabler_RuleSet> Rewired.ControllerMapEnabler::MOUTEkpzvrLxSPrledaFXJEDTx
	List_1_tCE05C39A3E7EF93981D38C62BD754C0D392F09EC * ___MOUTEkpzvrLxSPrledaFXJEDTx_4;

public:
	inline static int32_t get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_0() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D, ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_0)); }
	inline bool get_eWTjKkaJVjCZqrsQHQXMtJYuNhD_0() const { return ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_0; }
	inline bool* get_address_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_0() { return &___eWTjKkaJVjCZqrsQHQXMtJYuNhD_0; }
	inline void set_eWTjKkaJVjCZqrsQHQXMtJYuNhD_0(bool value)
	{
		___eWTjKkaJVjCZqrsQHQXMtJYuNhD_0 = value;
	}

	inline static int32_t get_offset_of_VZYtnnAqBdPNaSltSWGWGTTbFPN_1() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D, ___VZYtnnAqBdPNaSltSWGWGTTbFPN_1)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_VZYtnnAqBdPNaSltSWGWGTTbFPN_1() const { return ___VZYtnnAqBdPNaSltSWGWGTTbFPN_1; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_VZYtnnAqBdPNaSltSWGWGTTbFPN_1() { return &___VZYtnnAqBdPNaSltSWGWGTTbFPN_1; }
	inline void set_VZYtnnAqBdPNaSltSWGWGTTbFPN_1(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___VZYtnnAqBdPNaSltSWGWGTTbFPN_1 = value;
		Il2CppCodeGenWriteBarrier((&___VZYtnnAqBdPNaSltSWGWGTTbFPN_1), value);
	}

	inline static int32_t get_offset_of_HEDjmuguOtJTvuChkRgbhJaBjAU_2() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D, ___HEDjmuguOtJTvuChkRgbhJaBjAU_2)); }
	inline ixNifobzbtiwSHPyWPtxRGELpf_t6742005DEB9045D54B12821D97E544A8E0DAF484 * get_HEDjmuguOtJTvuChkRgbhJaBjAU_2() const { return ___HEDjmuguOtJTvuChkRgbhJaBjAU_2; }
	inline ixNifobzbtiwSHPyWPtxRGELpf_t6742005DEB9045D54B12821D97E544A8E0DAF484 ** get_address_of_HEDjmuguOtJTvuChkRgbhJaBjAU_2() { return &___HEDjmuguOtJTvuChkRgbhJaBjAU_2; }
	inline void set_HEDjmuguOtJTvuChkRgbhJaBjAU_2(ixNifobzbtiwSHPyWPtxRGELpf_t6742005DEB9045D54B12821D97E544A8E0DAF484 * value)
	{
		___HEDjmuguOtJTvuChkRgbhJaBjAU_2 = value;
		Il2CppCodeGenWriteBarrier((&___HEDjmuguOtJTvuChkRgbhJaBjAU_2), value);
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_3() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_3)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_3() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_3; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_3() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_3; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_3(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_3 = value;
	}

	inline static int32_t get_offset_of_MOUTEkpzvrLxSPrledaFXJEDTx_4() { return static_cast<int32_t>(offsetof(ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D, ___MOUTEkpzvrLxSPrledaFXJEDTx_4)); }
	inline List_1_tCE05C39A3E7EF93981D38C62BD754C0D392F09EC * get_MOUTEkpzvrLxSPrledaFXJEDTx_4() const { return ___MOUTEkpzvrLxSPrledaFXJEDTx_4; }
	inline List_1_tCE05C39A3E7EF93981D38C62BD754C0D392F09EC ** get_address_of_MOUTEkpzvrLxSPrledaFXJEDTx_4() { return &___MOUTEkpzvrLxSPrledaFXJEDTx_4; }
	inline void set_MOUTEkpzvrLxSPrledaFXJEDTx_4(List_1_tCE05C39A3E7EF93981D38C62BD754C0D392F09EC * value)
	{
		___MOUTEkpzvrLxSPrledaFXJEDTx_4 = value;
		Il2CppCodeGenWriteBarrier((&___MOUTEkpzvrLxSPrledaFXJEDTx_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMAPENABLER_T72022B3413E35CE64277E9F8F6F64236C8F0148D_H
#ifndef RULE_T80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB_H
#define RULE_T80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapEnabler_Rule
struct  Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB  : public RuntimeObject
{
public:
	// System.String Rewired.ControllerMapEnabler_Rule::_tag
	String_t* ____tag_0;
	// System.Boolean Rewired.ControllerMapEnabler_Rule::_enable
	bool ____enable_1;
	// System.Int32[] Rewired.ControllerMapEnabler_Rule::_categoryIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____categoryIds_2;
	// System.Int32[] Rewired.ControllerMapEnabler_Rule::_layoutIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____layoutIds_3;
	// Rewired.ControllerSetSelector Rewired.ControllerMapEnabler_Rule::_controllerSetSelector
	ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD * ____controllerSetSelector_4;
	// System.String[] Rewired.ControllerMapEnabler_Rule::_preInitCategoryNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____preInitCategoryNames_5;
	// System.String[] Rewired.ControllerMapEnabler_Rule::_preInitLayoutNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____preInitLayoutNames_6;

public:
	inline static int32_t get_offset_of__tag_0() { return static_cast<int32_t>(offsetof(Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB, ____tag_0)); }
	inline String_t* get__tag_0() const { return ____tag_0; }
	inline String_t** get_address_of__tag_0() { return &____tag_0; }
	inline void set__tag_0(String_t* value)
	{
		____tag_0 = value;
		Il2CppCodeGenWriteBarrier((&____tag_0), value);
	}

	inline static int32_t get_offset_of__enable_1() { return static_cast<int32_t>(offsetof(Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB, ____enable_1)); }
	inline bool get__enable_1() const { return ____enable_1; }
	inline bool* get_address_of__enable_1() { return &____enable_1; }
	inline void set__enable_1(bool value)
	{
		____enable_1 = value;
	}

	inline static int32_t get_offset_of__categoryIds_2() { return static_cast<int32_t>(offsetof(Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB, ____categoryIds_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__categoryIds_2() const { return ____categoryIds_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__categoryIds_2() { return &____categoryIds_2; }
	inline void set__categoryIds_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____categoryIds_2 = value;
		Il2CppCodeGenWriteBarrier((&____categoryIds_2), value);
	}

	inline static int32_t get_offset_of__layoutIds_3() { return static_cast<int32_t>(offsetof(Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB, ____layoutIds_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__layoutIds_3() const { return ____layoutIds_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__layoutIds_3() { return &____layoutIds_3; }
	inline void set__layoutIds_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____layoutIds_3 = value;
		Il2CppCodeGenWriteBarrier((&____layoutIds_3), value);
	}

	inline static int32_t get_offset_of__controllerSetSelector_4() { return static_cast<int32_t>(offsetof(Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB, ____controllerSetSelector_4)); }
	inline ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD * get__controllerSetSelector_4() const { return ____controllerSetSelector_4; }
	inline ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD ** get_address_of__controllerSetSelector_4() { return &____controllerSetSelector_4; }
	inline void set__controllerSetSelector_4(ControllerSetSelector_tA23A5CBB83617E78B6AC0116E102C782BCB9ADCD * value)
	{
		____controllerSetSelector_4 = value;
		Il2CppCodeGenWriteBarrier((&____controllerSetSelector_4), value);
	}

	inline static int32_t get_offset_of__preInitCategoryNames_5() { return static_cast<int32_t>(offsetof(Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB, ____preInitCategoryNames_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__preInitCategoryNames_5() const { return ____preInitCategoryNames_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__preInitCategoryNames_5() { return &____preInitCategoryNames_5; }
	inline void set__preInitCategoryNames_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____preInitCategoryNames_5 = value;
		Il2CppCodeGenWriteBarrier((&____preInitCategoryNames_5), value);
	}

	inline static int32_t get_offset_of__preInitLayoutNames_6() { return static_cast<int32_t>(offsetof(Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB, ____preInitLayoutNames_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__preInitLayoutNames_6() const { return ____preInitLayoutNames_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__preInitLayoutNames_6() { return &____preInitLayoutNames_6; }
	inline void set__preInitLayoutNames_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____preInitLayoutNames_6 = value;
		Il2CppCodeGenWriteBarrier((&____preInitLayoutNames_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RULE_T80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB_H
#ifndef RULESET_TC0E87F2AC099155968FF378786CE9483F6B47E6C_H
#define RULESET_TC0E87F2AC099155968FF378786CE9483F6B47E6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapEnabler_RuleSet
struct  RuleSet_tC0E87F2AC099155968FF378786CE9483F6B47E6C  : public RuntimeObject
{
public:
	// System.Boolean Rewired.ControllerMapEnabler_RuleSet::_enabled
	bool ____enabled_1;
	// System.String Rewired.ControllerMapEnabler_RuleSet::_tag
	String_t* ____tag_2;
	// System.Collections.Generic.List`1<Rewired.ControllerMapEnabler_Rule> Rewired.ControllerMapEnabler_RuleSet::_rules
	List_1_t93FFD47C81E2F494B000EF49C186820C085FCA43 * ____rules_3;

public:
	inline static int32_t get_offset_of__enabled_1() { return static_cast<int32_t>(offsetof(RuleSet_tC0E87F2AC099155968FF378786CE9483F6B47E6C, ____enabled_1)); }
	inline bool get__enabled_1() const { return ____enabled_1; }
	inline bool* get_address_of__enabled_1() { return &____enabled_1; }
	inline void set__enabled_1(bool value)
	{
		____enabled_1 = value;
	}

	inline static int32_t get_offset_of__tag_2() { return static_cast<int32_t>(offsetof(RuleSet_tC0E87F2AC099155968FF378786CE9483F6B47E6C, ____tag_2)); }
	inline String_t* get__tag_2() const { return ____tag_2; }
	inline String_t** get_address_of__tag_2() { return &____tag_2; }
	inline void set__tag_2(String_t* value)
	{
		____tag_2 = value;
		Il2CppCodeGenWriteBarrier((&____tag_2), value);
	}

	inline static int32_t get_offset_of__rules_3() { return static_cast<int32_t>(offsetof(RuleSet_tC0E87F2AC099155968FF378786CE9483F6B47E6C, ____rules_3)); }
	inline List_1_t93FFD47C81E2F494B000EF49C186820C085FCA43 * get__rules_3() const { return ____rules_3; }
	inline List_1_t93FFD47C81E2F494B000EF49C186820C085FCA43 ** get_address_of__rules_3() { return &____rules_3; }
	inline void set__rules_3(List_1_t93FFD47C81E2F494B000EF49C186820C085FCA43 * value)
	{
		____rules_3 = value;
		Il2CppCodeGenWriteBarrier((&____rules_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RULESET_TC0E87F2AC099155968FF378786CE9483F6B47E6C_H
#ifndef IXNIFOBZBTIWSHPYWPTXRGELPF_T6742005DEB9045D54B12821D97E544A8E0DAF484_H
#define IXNIFOBZBTIWSHPYWPTXRGELPF_T6742005DEB9045D54B12821D97E544A8E0DAF484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerMapEnabler_ixNifobzbtiwSHPyWPtxRGELpf
struct  ixNifobzbtiwSHPyWPtxRGELpf_t6742005DEB9045D54B12821D97E544A8E0DAF484  : public RuntimeObject
{
public:
	// System.Boolean Rewired.ControllerMapEnabler_ixNifobzbtiwSHPyWPtxRGELpf::mgHlmSwiXWBcWiZgWIYuggfBYfpv
	bool ___mgHlmSwiXWBcWiZgWIYuggfBYfpv_0;
	// jufVWXOncnQzwwsvxneDefLrtLg[] Rewired.ControllerMapEnabler_ixNifobzbtiwSHPyWPtxRGELpf::kXPzYCvJwftDdckxbGhbBRgTrNKq
	jufVWXOncnQzwwsvxneDefLrtLgU5BU5D_tDE07F616F33597DE5FCF94F1B020CBA6C5345F09* ___kXPzYCvJwftDdckxbGhbBRgTrNKq_1;

public:
	inline static int32_t get_offset_of_mgHlmSwiXWBcWiZgWIYuggfBYfpv_0() { return static_cast<int32_t>(offsetof(ixNifobzbtiwSHPyWPtxRGELpf_t6742005DEB9045D54B12821D97E544A8E0DAF484, ___mgHlmSwiXWBcWiZgWIYuggfBYfpv_0)); }
	inline bool get_mgHlmSwiXWBcWiZgWIYuggfBYfpv_0() const { return ___mgHlmSwiXWBcWiZgWIYuggfBYfpv_0; }
	inline bool* get_address_of_mgHlmSwiXWBcWiZgWIYuggfBYfpv_0() { return &___mgHlmSwiXWBcWiZgWIYuggfBYfpv_0; }
	inline void set_mgHlmSwiXWBcWiZgWIYuggfBYfpv_0(bool value)
	{
		___mgHlmSwiXWBcWiZgWIYuggfBYfpv_0 = value;
	}

	inline static int32_t get_offset_of_kXPzYCvJwftDdckxbGhbBRgTrNKq_1() { return static_cast<int32_t>(offsetof(ixNifobzbtiwSHPyWPtxRGELpf_t6742005DEB9045D54B12821D97E544A8E0DAF484, ___kXPzYCvJwftDdckxbGhbBRgTrNKq_1)); }
	inline jufVWXOncnQzwwsvxneDefLrtLgU5BU5D_tDE07F616F33597DE5FCF94F1B020CBA6C5345F09* get_kXPzYCvJwftDdckxbGhbBRgTrNKq_1() const { return ___kXPzYCvJwftDdckxbGhbBRgTrNKq_1; }
	inline jufVWXOncnQzwwsvxneDefLrtLgU5BU5D_tDE07F616F33597DE5FCF94F1B020CBA6C5345F09** get_address_of_kXPzYCvJwftDdckxbGhbBRgTrNKq_1() { return &___kXPzYCvJwftDdckxbGhbBRgTrNKq_1; }
	inline void set_kXPzYCvJwftDdckxbGhbBRgTrNKq_1(jufVWXOncnQzwwsvxneDefLrtLgU5BU5D_tDE07F616F33597DE5FCF94F1B020CBA6C5345F09* value)
	{
		___kXPzYCvJwftDdckxbGhbBRgTrNKq_1 = value;
		Il2CppCodeGenWriteBarrier((&___kXPzYCvJwftDdckxbGhbBRgTrNKq_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IXNIFOBZBTIWSHPYWPTXRGELPF_T6742005DEB9045D54B12821D97E544A8E0DAF484_H
#ifndef HIDDEVICEDRIVER_T58509E517EE4CB9F0364323BB01CD6A39705AFFC_H
#define HIDDEVICEDRIVER_T58509E517EE4CB9F0364323BB01CD6A39705AFFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.Drivers.HIDDeviceDriver
struct  HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC  : public RuntimeObject
{
public:
	// Rewired.HID.HIDAxis[] Rewired.HID.Drivers.HIDDeviceDriver::axes
	HIDAxisU5BU5D_t8BF0E8BC7B27B705A2C2DDE04581B7DB595B50ED* ___axes_0;
	// Rewired.HID.HIDButton[] Rewired.HID.Drivers.HIDDeviceDriver::buttons
	HIDButtonU5BU5D_t622EE36EF01D50FF9EC401595CEC229A78056946* ___buttons_1;
	// Rewired.HID.HIDHat[] Rewired.HID.Drivers.HIDDeviceDriver::hats
	HIDHatU5BU5D_t8BD3C3729A1AF48C2DA5988763E684048ECACCBA* ___hats_2;
	// Rewired.HID.HIDAccelerometer[] Rewired.HID.Drivers.HIDDeviceDriver::accelerometers
	HIDAccelerometerU5BU5D_t0058D027862A38EBC89C348C2064488E2AF120DD* ___accelerometers_3;
	// Rewired.HID.HIDGyroscope[] Rewired.HID.Drivers.HIDDeviceDriver::gyroscopes
	HIDGyroscopeU5BU5D_t4220A1DEFB7DA209C05DB9369B9AC6A947D39A0F* ___gyroscopes_4;
	// Rewired.HID.HIDTouchpad[] Rewired.HID.Drivers.HIDDeviceDriver::touchpads
	HIDTouchpadU5BU5D_tB00C7596EE4CC3F689B9300029FCB9FC829D8BC7* ___touchpads_5;
	// Rewired.HID.HIDVibrationMotor[] Rewired.HID.Drivers.HIDDeviceDriver::vibrationMotors
	HIDVibrationMotorU5BU5D_tD42C2F405AC939F9EB00DABF5C37FD04B7E91966* ___vibrationMotors_6;
	// Rewired.HID.HIDLight[] Rewired.HID.Drivers.HIDDeviceDriver::lights
	HIDLightU5BU5D_tD12625439187D085105DB06372883AAE5C325155* ___lights_7;
	// System.Boolean Rewired.HID.Drivers.HIDDeviceDriver::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_8;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC, ___axes_0)); }
	inline HIDAxisU5BU5D_t8BF0E8BC7B27B705A2C2DDE04581B7DB595B50ED* get_axes_0() const { return ___axes_0; }
	inline HIDAxisU5BU5D_t8BF0E8BC7B27B705A2C2DDE04581B7DB595B50ED** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(HIDAxisU5BU5D_t8BF0E8BC7B27B705A2C2DDE04581B7DB595B50ED* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC, ___buttons_1)); }
	inline HIDButtonU5BU5D_t622EE36EF01D50FF9EC401595CEC229A78056946* get_buttons_1() const { return ___buttons_1; }
	inline HIDButtonU5BU5D_t622EE36EF01D50FF9EC401595CEC229A78056946** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(HIDButtonU5BU5D_t622EE36EF01D50FF9EC401595CEC229A78056946* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}

	inline static int32_t get_offset_of_hats_2() { return static_cast<int32_t>(offsetof(HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC, ___hats_2)); }
	inline HIDHatU5BU5D_t8BD3C3729A1AF48C2DA5988763E684048ECACCBA* get_hats_2() const { return ___hats_2; }
	inline HIDHatU5BU5D_t8BD3C3729A1AF48C2DA5988763E684048ECACCBA** get_address_of_hats_2() { return &___hats_2; }
	inline void set_hats_2(HIDHatU5BU5D_t8BD3C3729A1AF48C2DA5988763E684048ECACCBA* value)
	{
		___hats_2 = value;
		Il2CppCodeGenWriteBarrier((&___hats_2), value);
	}

	inline static int32_t get_offset_of_accelerometers_3() { return static_cast<int32_t>(offsetof(HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC, ___accelerometers_3)); }
	inline HIDAccelerometerU5BU5D_t0058D027862A38EBC89C348C2064488E2AF120DD* get_accelerometers_3() const { return ___accelerometers_3; }
	inline HIDAccelerometerU5BU5D_t0058D027862A38EBC89C348C2064488E2AF120DD** get_address_of_accelerometers_3() { return &___accelerometers_3; }
	inline void set_accelerometers_3(HIDAccelerometerU5BU5D_t0058D027862A38EBC89C348C2064488E2AF120DD* value)
	{
		___accelerometers_3 = value;
		Il2CppCodeGenWriteBarrier((&___accelerometers_3), value);
	}

	inline static int32_t get_offset_of_gyroscopes_4() { return static_cast<int32_t>(offsetof(HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC, ___gyroscopes_4)); }
	inline HIDGyroscopeU5BU5D_t4220A1DEFB7DA209C05DB9369B9AC6A947D39A0F* get_gyroscopes_4() const { return ___gyroscopes_4; }
	inline HIDGyroscopeU5BU5D_t4220A1DEFB7DA209C05DB9369B9AC6A947D39A0F** get_address_of_gyroscopes_4() { return &___gyroscopes_4; }
	inline void set_gyroscopes_4(HIDGyroscopeU5BU5D_t4220A1DEFB7DA209C05DB9369B9AC6A947D39A0F* value)
	{
		___gyroscopes_4 = value;
		Il2CppCodeGenWriteBarrier((&___gyroscopes_4), value);
	}

	inline static int32_t get_offset_of_touchpads_5() { return static_cast<int32_t>(offsetof(HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC, ___touchpads_5)); }
	inline HIDTouchpadU5BU5D_tB00C7596EE4CC3F689B9300029FCB9FC829D8BC7* get_touchpads_5() const { return ___touchpads_5; }
	inline HIDTouchpadU5BU5D_tB00C7596EE4CC3F689B9300029FCB9FC829D8BC7** get_address_of_touchpads_5() { return &___touchpads_5; }
	inline void set_touchpads_5(HIDTouchpadU5BU5D_tB00C7596EE4CC3F689B9300029FCB9FC829D8BC7* value)
	{
		___touchpads_5 = value;
		Il2CppCodeGenWriteBarrier((&___touchpads_5), value);
	}

	inline static int32_t get_offset_of_vibrationMotors_6() { return static_cast<int32_t>(offsetof(HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC, ___vibrationMotors_6)); }
	inline HIDVibrationMotorU5BU5D_tD42C2F405AC939F9EB00DABF5C37FD04B7E91966* get_vibrationMotors_6() const { return ___vibrationMotors_6; }
	inline HIDVibrationMotorU5BU5D_tD42C2F405AC939F9EB00DABF5C37FD04B7E91966** get_address_of_vibrationMotors_6() { return &___vibrationMotors_6; }
	inline void set_vibrationMotors_6(HIDVibrationMotorU5BU5D_tD42C2F405AC939F9EB00DABF5C37FD04B7E91966* value)
	{
		___vibrationMotors_6 = value;
		Il2CppCodeGenWriteBarrier((&___vibrationMotors_6), value);
	}

	inline static int32_t get_offset_of_lights_7() { return static_cast<int32_t>(offsetof(HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC, ___lights_7)); }
	inline HIDLightU5BU5D_tD12625439187D085105DB06372883AAE5C325155* get_lights_7() const { return ___lights_7; }
	inline HIDLightU5BU5D_tD12625439187D085105DB06372883AAE5C325155** get_address_of_lights_7() { return &___lights_7; }
	inline void set_lights_7(HIDLightU5BU5D_tD12625439187D085105DB06372883AAE5C325155* value)
	{
		___lights_7 = value;
		Il2CppCodeGenWriteBarrier((&___lights_7), value);
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_8() { return static_cast<int32_t>(offsetof(HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC, ___SeCUoinDywZmqZDHRKupOdOaTke_8)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_8() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_8; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_8() { return &___SeCUoinDywZmqZDHRKupOdOaTke_8; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_8(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDDEVICEDRIVER_T58509E517EE4CB9F0364323BB01CD6A39705AFFC_H
#ifndef HIDCONTROLLERELEMENT_TC98B3FAE2975F27917258ACE9B578318D1EAF7E0_H
#define HIDCONTROLLERELEMENT_TC98B3FAE2975F27917258ACE9B578318D1EAF7E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDControllerElement
struct  HIDControllerElement_tC98B3FAE2975F27917258ACE9B578318D1EAF7E0  : public RuntimeObject
{
public:
	// System.Byte Rewired.HID.HIDControllerElement::reportId
	uint8_t ___reportId_0;
	// Rewired.HID.HIDControllerElement_HIDInfo Rewired.HID.HIDControllerElement::hidInfo
	HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366 * ___hidInfo_1;

public:
	inline static int32_t get_offset_of_reportId_0() { return static_cast<int32_t>(offsetof(HIDControllerElement_tC98B3FAE2975F27917258ACE9B578318D1EAF7E0, ___reportId_0)); }
	inline uint8_t get_reportId_0() const { return ___reportId_0; }
	inline uint8_t* get_address_of_reportId_0() { return &___reportId_0; }
	inline void set_reportId_0(uint8_t value)
	{
		___reportId_0 = value;
	}

	inline static int32_t get_offset_of_hidInfo_1() { return static_cast<int32_t>(offsetof(HIDControllerElement_tC98B3FAE2975F27917258ACE9B578318D1EAF7E0, ___hidInfo_1)); }
	inline HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366 * get_hidInfo_1() const { return ___hidInfo_1; }
	inline HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366 ** get_address_of_hidInfo_1() { return &___hidInfo_1; }
	inline void set_hidInfo_1(HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366 * value)
	{
		___hidInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___hidInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDCONTROLLERELEMENT_TC98B3FAE2975F27917258ACE9B578318D1EAF7E0_H
#ifndef HIDINFO_TC6AFF24A777D4DFEA56D99BCF63C31CE90E21366_H
#define HIDINFO_TC6AFF24A777D4DFEA56D99BCF63C31CE90E21366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDControllerElement_HIDInfo
struct  HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366  : public RuntimeObject
{
public:
	// System.UInt16 Rewired.HID.HIDControllerElement_HIDInfo::usagePage
	uint16_t ___usagePage_0;
	// System.UInt16 Rewired.HID.HIDControllerElement_HIDInfo::usage
	uint16_t ___usage_1;
	// System.Int32 Rewired.HID.HIDControllerElement_HIDInfo::dataIndex
	int32_t ___dataIndex_2;
	// System.Int32 Rewired.HID.HIDControllerElement_HIDInfo::bitSize
	int32_t ___bitSize_3;
	// System.Int32 Rewired.HID.HIDControllerElement_HIDInfo::logicalMin
	int32_t ___logicalMin_4;
	// System.Int32 Rewired.HID.HIDControllerElement_HIDInfo::logicalMax
	int32_t ___logicalMax_5;
	// System.Int32 Rewired.HID.HIDControllerElement_HIDInfo::physicalMin
	int32_t ___physicalMin_6;
	// System.Int32 Rewired.HID.HIDControllerElement_HIDInfo::physicalMax
	int32_t ___physicalMax_7;
	// System.UInt32 Rewired.HID.HIDControllerElement_HIDInfo::units
	uint32_t ___units_8;
	// System.UInt32 Rewired.HID.HIDControllerElement_HIDInfo::unitsExp
	uint32_t ___unitsExp_9;

public:
	inline static int32_t get_offset_of_usagePage_0() { return static_cast<int32_t>(offsetof(HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366, ___usagePage_0)); }
	inline uint16_t get_usagePage_0() const { return ___usagePage_0; }
	inline uint16_t* get_address_of_usagePage_0() { return &___usagePage_0; }
	inline void set_usagePage_0(uint16_t value)
	{
		___usagePage_0 = value;
	}

	inline static int32_t get_offset_of_usage_1() { return static_cast<int32_t>(offsetof(HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366, ___usage_1)); }
	inline uint16_t get_usage_1() const { return ___usage_1; }
	inline uint16_t* get_address_of_usage_1() { return &___usage_1; }
	inline void set_usage_1(uint16_t value)
	{
		___usage_1 = value;
	}

	inline static int32_t get_offset_of_dataIndex_2() { return static_cast<int32_t>(offsetof(HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366, ___dataIndex_2)); }
	inline int32_t get_dataIndex_2() const { return ___dataIndex_2; }
	inline int32_t* get_address_of_dataIndex_2() { return &___dataIndex_2; }
	inline void set_dataIndex_2(int32_t value)
	{
		___dataIndex_2 = value;
	}

	inline static int32_t get_offset_of_bitSize_3() { return static_cast<int32_t>(offsetof(HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366, ___bitSize_3)); }
	inline int32_t get_bitSize_3() const { return ___bitSize_3; }
	inline int32_t* get_address_of_bitSize_3() { return &___bitSize_3; }
	inline void set_bitSize_3(int32_t value)
	{
		___bitSize_3 = value;
	}

	inline static int32_t get_offset_of_logicalMin_4() { return static_cast<int32_t>(offsetof(HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366, ___logicalMin_4)); }
	inline int32_t get_logicalMin_4() const { return ___logicalMin_4; }
	inline int32_t* get_address_of_logicalMin_4() { return &___logicalMin_4; }
	inline void set_logicalMin_4(int32_t value)
	{
		___logicalMin_4 = value;
	}

	inline static int32_t get_offset_of_logicalMax_5() { return static_cast<int32_t>(offsetof(HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366, ___logicalMax_5)); }
	inline int32_t get_logicalMax_5() const { return ___logicalMax_5; }
	inline int32_t* get_address_of_logicalMax_5() { return &___logicalMax_5; }
	inline void set_logicalMax_5(int32_t value)
	{
		___logicalMax_5 = value;
	}

	inline static int32_t get_offset_of_physicalMin_6() { return static_cast<int32_t>(offsetof(HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366, ___physicalMin_6)); }
	inline int32_t get_physicalMin_6() const { return ___physicalMin_6; }
	inline int32_t* get_address_of_physicalMin_6() { return &___physicalMin_6; }
	inline void set_physicalMin_6(int32_t value)
	{
		___physicalMin_6 = value;
	}

	inline static int32_t get_offset_of_physicalMax_7() { return static_cast<int32_t>(offsetof(HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366, ___physicalMax_7)); }
	inline int32_t get_physicalMax_7() const { return ___physicalMax_7; }
	inline int32_t* get_address_of_physicalMax_7() { return &___physicalMax_7; }
	inline void set_physicalMax_7(int32_t value)
	{
		___physicalMax_7 = value;
	}

	inline static int32_t get_offset_of_units_8() { return static_cast<int32_t>(offsetof(HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366, ___units_8)); }
	inline uint32_t get_units_8() const { return ___units_8; }
	inline uint32_t* get_address_of_units_8() { return &___units_8; }
	inline void set_units_8(uint32_t value)
	{
		___units_8 = value;
	}

	inline static int32_t get_offset_of_unitsExp_9() { return static_cast<int32_t>(offsetof(HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366, ___unitsExp_9)); }
	inline uint32_t get_unitsExp_9() const { return ___unitsExp_9; }
	inline uint32_t* get_address_of_unitsExp_9() { return &___unitsExp_9; }
	inline void set_unitsExp_9(uint32_t value)
	{
		___unitsExp_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDINFO_TC6AFF24A777D4DFEA56D99BCF63C31CE90E21366_H
#ifndef WGQSTFYHDSMNYGVPFQWICLANDKO_T8BAD9E0D116BF67A723C851334BA553EA028C7E5_H
#define WGQSTFYHDSMNYGVPFQWICLANDKO_T8BAD9E0D116BF67A723C851334BA553EA028C7E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDControllerElementWithDataSet_wgqSTfYHDsMNygvPFQwICLAnDKO
struct  wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5  : public RuntimeObject
{
public:
	// System.Int32 Rewired.HID.HIDControllerElementWithDataSet_wgqSTfYHDsMNygvPFQwICLAnDKO::dufrXbHCNDnCDAkzaZIcSkKuBqZ
	int32_t ___dufrXbHCNDnCDAkzaZIcSkKuBqZ_0;
	// System.Int32[] Rewired.HID.HIDControllerElementWithDataSet_wgqSTfYHDsMNygvPFQwICLAnDKO::UtyHrsDgBPEcHKmyNNRLLGUMtoub
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___UtyHrsDgBPEcHKmyNNRLLGUMtoub_1;
	// Rewired.HID.HIDControllerElementWithDataSet_CVaCXXbjFHbTGydqcOoiGPdeykAL[] Rewired.HID.HIDControllerElementWithDataSet_wgqSTfYHDsMNygvPFQwICLAnDKO::XzwvNxyYkXcaiidxwJGvQNwBigT
	CVaCXXbjFHbTGydqcOoiGPdeykALU5BU5D_tA5D1BE9E5CF82A3FA8C7A2EB120E3B7E80A1039A* ___XzwvNxyYkXcaiidxwJGvQNwBigT_2;
	// Rewired.HID.HIDControllerElementWithDataSet_CVaCXXbjFHbTGydqcOoiGPdeykAL Rewired.HID.HIDControllerElementWithDataSet_wgqSTfYHDsMNygvPFQwICLAnDKO::YaDAnfimULzphKnSejtwEcfSlkEJ
	CVaCXXbjFHbTGydqcOoiGPdeykAL_tD7DD1276581C9E6FDF35379998DD5CEE6A2567D5 * ___YaDAnfimULzphKnSejtwEcfSlkEJ_3;
	// System.Int32 Rewired.HID.HIDControllerElementWithDataSet_wgqSTfYHDsMNygvPFQwICLAnDKO::uiElHIMsJhOUGwxstGSvtQgdJHm
	int32_t ___uiElHIMsJhOUGwxstGSvtQgdJHm_4;
	// System.Int32 Rewired.HID.HIDControllerElementWithDataSet_wgqSTfYHDsMNygvPFQwICLAnDKO::mitExlcuToNzViGEORWHsMktptx
	int32_t ___mitExlcuToNzViGEORWHsMktptx_5;
	// System.Boolean Rewired.HID.HIDControllerElementWithDataSet_wgqSTfYHDsMNygvPFQwICLAnDKO::ffNWJUuxPxuFMIZszOmCdhFAkUp
	bool ___ffNWJUuxPxuFMIZszOmCdhFAkUp_6;

public:
	inline static int32_t get_offset_of_dufrXbHCNDnCDAkzaZIcSkKuBqZ_0() { return static_cast<int32_t>(offsetof(wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5, ___dufrXbHCNDnCDAkzaZIcSkKuBqZ_0)); }
	inline int32_t get_dufrXbHCNDnCDAkzaZIcSkKuBqZ_0() const { return ___dufrXbHCNDnCDAkzaZIcSkKuBqZ_0; }
	inline int32_t* get_address_of_dufrXbHCNDnCDAkzaZIcSkKuBqZ_0() { return &___dufrXbHCNDnCDAkzaZIcSkKuBqZ_0; }
	inline void set_dufrXbHCNDnCDAkzaZIcSkKuBqZ_0(int32_t value)
	{
		___dufrXbHCNDnCDAkzaZIcSkKuBqZ_0 = value;
	}

	inline static int32_t get_offset_of_UtyHrsDgBPEcHKmyNNRLLGUMtoub_1() { return static_cast<int32_t>(offsetof(wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5, ___UtyHrsDgBPEcHKmyNNRLLGUMtoub_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_UtyHrsDgBPEcHKmyNNRLLGUMtoub_1() const { return ___UtyHrsDgBPEcHKmyNNRLLGUMtoub_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_UtyHrsDgBPEcHKmyNNRLLGUMtoub_1() { return &___UtyHrsDgBPEcHKmyNNRLLGUMtoub_1; }
	inline void set_UtyHrsDgBPEcHKmyNNRLLGUMtoub_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___UtyHrsDgBPEcHKmyNNRLLGUMtoub_1 = value;
		Il2CppCodeGenWriteBarrier((&___UtyHrsDgBPEcHKmyNNRLLGUMtoub_1), value);
	}

	inline static int32_t get_offset_of_XzwvNxyYkXcaiidxwJGvQNwBigT_2() { return static_cast<int32_t>(offsetof(wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5, ___XzwvNxyYkXcaiidxwJGvQNwBigT_2)); }
	inline CVaCXXbjFHbTGydqcOoiGPdeykALU5BU5D_tA5D1BE9E5CF82A3FA8C7A2EB120E3B7E80A1039A* get_XzwvNxyYkXcaiidxwJGvQNwBigT_2() const { return ___XzwvNxyYkXcaiidxwJGvQNwBigT_2; }
	inline CVaCXXbjFHbTGydqcOoiGPdeykALU5BU5D_tA5D1BE9E5CF82A3FA8C7A2EB120E3B7E80A1039A** get_address_of_XzwvNxyYkXcaiidxwJGvQNwBigT_2() { return &___XzwvNxyYkXcaiidxwJGvQNwBigT_2; }
	inline void set_XzwvNxyYkXcaiidxwJGvQNwBigT_2(CVaCXXbjFHbTGydqcOoiGPdeykALU5BU5D_tA5D1BE9E5CF82A3FA8C7A2EB120E3B7E80A1039A* value)
	{
		___XzwvNxyYkXcaiidxwJGvQNwBigT_2 = value;
		Il2CppCodeGenWriteBarrier((&___XzwvNxyYkXcaiidxwJGvQNwBigT_2), value);
	}

	inline static int32_t get_offset_of_YaDAnfimULzphKnSejtwEcfSlkEJ_3() { return static_cast<int32_t>(offsetof(wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5, ___YaDAnfimULzphKnSejtwEcfSlkEJ_3)); }
	inline CVaCXXbjFHbTGydqcOoiGPdeykAL_tD7DD1276581C9E6FDF35379998DD5CEE6A2567D5 * get_YaDAnfimULzphKnSejtwEcfSlkEJ_3() const { return ___YaDAnfimULzphKnSejtwEcfSlkEJ_3; }
	inline CVaCXXbjFHbTGydqcOoiGPdeykAL_tD7DD1276581C9E6FDF35379998DD5CEE6A2567D5 ** get_address_of_YaDAnfimULzphKnSejtwEcfSlkEJ_3() { return &___YaDAnfimULzphKnSejtwEcfSlkEJ_3; }
	inline void set_YaDAnfimULzphKnSejtwEcfSlkEJ_3(CVaCXXbjFHbTGydqcOoiGPdeykAL_tD7DD1276581C9E6FDF35379998DD5CEE6A2567D5 * value)
	{
		___YaDAnfimULzphKnSejtwEcfSlkEJ_3 = value;
		Il2CppCodeGenWriteBarrier((&___YaDAnfimULzphKnSejtwEcfSlkEJ_3), value);
	}

	inline static int32_t get_offset_of_uiElHIMsJhOUGwxstGSvtQgdJHm_4() { return static_cast<int32_t>(offsetof(wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5, ___uiElHIMsJhOUGwxstGSvtQgdJHm_4)); }
	inline int32_t get_uiElHIMsJhOUGwxstGSvtQgdJHm_4() const { return ___uiElHIMsJhOUGwxstGSvtQgdJHm_4; }
	inline int32_t* get_address_of_uiElHIMsJhOUGwxstGSvtQgdJHm_4() { return &___uiElHIMsJhOUGwxstGSvtQgdJHm_4; }
	inline void set_uiElHIMsJhOUGwxstGSvtQgdJHm_4(int32_t value)
	{
		___uiElHIMsJhOUGwxstGSvtQgdJHm_4 = value;
	}

	inline static int32_t get_offset_of_mitExlcuToNzViGEORWHsMktptx_5() { return static_cast<int32_t>(offsetof(wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5, ___mitExlcuToNzViGEORWHsMktptx_5)); }
	inline int32_t get_mitExlcuToNzViGEORWHsMktptx_5() const { return ___mitExlcuToNzViGEORWHsMktptx_5; }
	inline int32_t* get_address_of_mitExlcuToNzViGEORWHsMktptx_5() { return &___mitExlcuToNzViGEORWHsMktptx_5; }
	inline void set_mitExlcuToNzViGEORWHsMktptx_5(int32_t value)
	{
		___mitExlcuToNzViGEORWHsMktptx_5 = value;
	}

	inline static int32_t get_offset_of_ffNWJUuxPxuFMIZszOmCdhFAkUp_6() { return static_cast<int32_t>(offsetof(wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5, ___ffNWJUuxPxuFMIZszOmCdhFAkUp_6)); }
	inline bool get_ffNWJUuxPxuFMIZszOmCdhFAkUp_6() const { return ___ffNWJUuxPxuFMIZszOmCdhFAkUp_6; }
	inline bool* get_address_of_ffNWJUuxPxuFMIZszOmCdhFAkUp_6() { return &___ffNWJUuxPxuFMIZszOmCdhFAkUp_6; }
	inline void set_ffNWJUuxPxuFMIZszOmCdhFAkUp_6(bool value)
	{
		___ffNWJUuxPxuFMIZszOmCdhFAkUp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WGQSTFYHDSMNYGVPFQWICLANDKO_T8BAD9E0D116BF67A723C851334BA553EA028C7E5_H
#ifndef HIDLIGHT_T660171518264B44E52EC67A0A99260D58A3EF792_H
#define HIDLIGHT_T660171518264B44E52EC67A0A99260D58A3EF792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDLight
struct  HIDLight_t660171518264B44E52EC67A0A99260D58A3EF792  : public RuntimeObject
{
public:
	// System.Byte Rewired.HID.HIDLight::QCkHJtJyNQQdBeNwHiKkWzyBkcy
	uint8_t ___QCkHJtJyNQQdBeNwHiKkWzyBkcy_0;
	// System.Byte Rewired.HID.HIDLight::cZtjlqxSiTJhueALMVKEKENfssc
	uint8_t ___cZtjlqxSiTJhueALMVKEKENfssc_1;
	// System.Byte Rewired.HID.HIDLight::NIfzXOfUmcMwYJBnCegHlLvZrWm
	uint8_t ___NIfzXOfUmcMwYJBnCegHlLvZrWm_2;
	// System.Action Rewired.HID.HIDLight::ylgqmkRBsacEGDjEnylhZJSqJLM
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ylgqmkRBsacEGDjEnylhZJSqJLM_3;

public:
	inline static int32_t get_offset_of_QCkHJtJyNQQdBeNwHiKkWzyBkcy_0() { return static_cast<int32_t>(offsetof(HIDLight_t660171518264B44E52EC67A0A99260D58A3EF792, ___QCkHJtJyNQQdBeNwHiKkWzyBkcy_0)); }
	inline uint8_t get_QCkHJtJyNQQdBeNwHiKkWzyBkcy_0() const { return ___QCkHJtJyNQQdBeNwHiKkWzyBkcy_0; }
	inline uint8_t* get_address_of_QCkHJtJyNQQdBeNwHiKkWzyBkcy_0() { return &___QCkHJtJyNQQdBeNwHiKkWzyBkcy_0; }
	inline void set_QCkHJtJyNQQdBeNwHiKkWzyBkcy_0(uint8_t value)
	{
		___QCkHJtJyNQQdBeNwHiKkWzyBkcy_0 = value;
	}

	inline static int32_t get_offset_of_cZtjlqxSiTJhueALMVKEKENfssc_1() { return static_cast<int32_t>(offsetof(HIDLight_t660171518264B44E52EC67A0A99260D58A3EF792, ___cZtjlqxSiTJhueALMVKEKENfssc_1)); }
	inline uint8_t get_cZtjlqxSiTJhueALMVKEKENfssc_1() const { return ___cZtjlqxSiTJhueALMVKEKENfssc_1; }
	inline uint8_t* get_address_of_cZtjlqxSiTJhueALMVKEKENfssc_1() { return &___cZtjlqxSiTJhueALMVKEKENfssc_1; }
	inline void set_cZtjlqxSiTJhueALMVKEKENfssc_1(uint8_t value)
	{
		___cZtjlqxSiTJhueALMVKEKENfssc_1 = value;
	}

	inline static int32_t get_offset_of_NIfzXOfUmcMwYJBnCegHlLvZrWm_2() { return static_cast<int32_t>(offsetof(HIDLight_t660171518264B44E52EC67A0A99260D58A3EF792, ___NIfzXOfUmcMwYJBnCegHlLvZrWm_2)); }
	inline uint8_t get_NIfzXOfUmcMwYJBnCegHlLvZrWm_2() const { return ___NIfzXOfUmcMwYJBnCegHlLvZrWm_2; }
	inline uint8_t* get_address_of_NIfzXOfUmcMwYJBnCegHlLvZrWm_2() { return &___NIfzXOfUmcMwYJBnCegHlLvZrWm_2; }
	inline void set_NIfzXOfUmcMwYJBnCegHlLvZrWm_2(uint8_t value)
	{
		___NIfzXOfUmcMwYJBnCegHlLvZrWm_2 = value;
	}

	inline static int32_t get_offset_of_ylgqmkRBsacEGDjEnylhZJSqJLM_3() { return static_cast<int32_t>(offsetof(HIDLight_t660171518264B44E52EC67A0A99260D58A3EF792, ___ylgqmkRBsacEGDjEnylhZJSqJLM_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ylgqmkRBsacEGDjEnylhZJSqJLM_3() const { return ___ylgqmkRBsacEGDjEnylhZJSqJLM_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ylgqmkRBsacEGDjEnylhZJSqJLM_3() { return &___ylgqmkRBsacEGDjEnylhZJSqJLM_3; }
	inline void set_ylgqmkRBsacEGDjEnylhZJSqJLM_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ylgqmkRBsacEGDjEnylhZJSqJLM_3 = value;
		Il2CppCodeGenWriteBarrier((&___ylgqmkRBsacEGDjEnylhZJSqJLM_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDLIGHT_T660171518264B44E52EC67A0A99260D58A3EF792_H
#ifndef TOUCHPADINFO_TA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F_H
#define TOUCHPADINFO_TA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDTouchpad_TouchpadInfo
struct  TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F  : public RuntimeObject
{
public:
	// System.Int32 Rewired.HID.HIDTouchpad_TouchpadInfo::maxTouches
	int32_t ___maxTouches_0;
	// System.Int32 Rewired.HID.HIDTouchpad_TouchpadInfo::minX
	int32_t ___minX_1;
	// System.Int32 Rewired.HID.HIDTouchpad_TouchpadInfo::maxX
	int32_t ___maxX_2;
	// System.Int32 Rewired.HID.HIDTouchpad_TouchpadInfo::minY
	int32_t ___minY_3;
	// System.Int32 Rewired.HID.HIDTouchpad_TouchpadInfo::maxY
	int32_t ___maxY_4;
	// System.Boolean Rewired.HID.HIDTouchpad_TouchpadInfo::invertY
	bool ___invertY_5;
	// System.Boolean Rewired.HID.HIDTouchpad_TouchpadInfo::reverseY
	bool ___reverseY_6;

public:
	inline static int32_t get_offset_of_maxTouches_0() { return static_cast<int32_t>(offsetof(TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F, ___maxTouches_0)); }
	inline int32_t get_maxTouches_0() const { return ___maxTouches_0; }
	inline int32_t* get_address_of_maxTouches_0() { return &___maxTouches_0; }
	inline void set_maxTouches_0(int32_t value)
	{
		___maxTouches_0 = value;
	}

	inline static int32_t get_offset_of_minX_1() { return static_cast<int32_t>(offsetof(TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F, ___minX_1)); }
	inline int32_t get_minX_1() const { return ___minX_1; }
	inline int32_t* get_address_of_minX_1() { return &___minX_1; }
	inline void set_minX_1(int32_t value)
	{
		___minX_1 = value;
	}

	inline static int32_t get_offset_of_maxX_2() { return static_cast<int32_t>(offsetof(TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F, ___maxX_2)); }
	inline int32_t get_maxX_2() const { return ___maxX_2; }
	inline int32_t* get_address_of_maxX_2() { return &___maxX_2; }
	inline void set_maxX_2(int32_t value)
	{
		___maxX_2 = value;
	}

	inline static int32_t get_offset_of_minY_3() { return static_cast<int32_t>(offsetof(TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F, ___minY_3)); }
	inline int32_t get_minY_3() const { return ___minY_3; }
	inline int32_t* get_address_of_minY_3() { return &___minY_3; }
	inline void set_minY_3(int32_t value)
	{
		___minY_3 = value;
	}

	inline static int32_t get_offset_of_maxY_4() { return static_cast<int32_t>(offsetof(TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F, ___maxY_4)); }
	inline int32_t get_maxY_4() const { return ___maxY_4; }
	inline int32_t* get_address_of_maxY_4() { return &___maxY_4; }
	inline void set_maxY_4(int32_t value)
	{
		___maxY_4 = value;
	}

	inline static int32_t get_offset_of_invertY_5() { return static_cast<int32_t>(offsetof(TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F, ___invertY_5)); }
	inline bool get_invertY_5() const { return ___invertY_5; }
	inline bool* get_address_of_invertY_5() { return &___invertY_5; }
	inline void set_invertY_5(bool value)
	{
		___invertY_5 = value;
	}

	inline static int32_t get_offset_of_reverseY_6() { return static_cast<int32_t>(offsetof(TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F, ___reverseY_6)); }
	inline bool get_reverseY_6() const { return ___reverseY_6; }
	inline bool* get_address_of_reverseY_6() { return &___reverseY_6; }
	inline void set_reverseY_6(bool value)
	{
		___reverseY_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPADINFO_TA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F_H
#ifndef HIDVIBRATIONMOTOR_TD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD_H
#define HIDVIBRATIONMOTOR_TD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDVibrationMotor
struct  HIDVibrationMotor_tD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD  : public RuntimeObject
{
public:
	// System.Int32 Rewired.HID.HIDVibrationMotor::RjVwQUrdIaflqLncuxqWYkoeksF
	int32_t ___RjVwQUrdIaflqLncuxqWYkoeksF_0;
	// System.Int32 Rewired.HID.HIDVibrationMotor::IkjGTcZHBbXyWdZhHrngxaAVUov
	int32_t ___IkjGTcZHBbXyWdZhHrngxaAVUov_1;
	// System.Int32 Rewired.HID.HIDVibrationMotor::WPgfnXCmbruqQhVPUePGTNIbeIs
	int32_t ___WPgfnXCmbruqQhVPUePGTNIbeIs_2;
	// System.Action Rewired.HID.HIDVibrationMotor::ylgqmkRBsacEGDjEnylhZJSqJLM
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ylgqmkRBsacEGDjEnylhZJSqJLM_3;

public:
	inline static int32_t get_offset_of_RjVwQUrdIaflqLncuxqWYkoeksF_0() { return static_cast<int32_t>(offsetof(HIDVibrationMotor_tD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD, ___RjVwQUrdIaflqLncuxqWYkoeksF_0)); }
	inline int32_t get_RjVwQUrdIaflqLncuxqWYkoeksF_0() const { return ___RjVwQUrdIaflqLncuxqWYkoeksF_0; }
	inline int32_t* get_address_of_RjVwQUrdIaflqLncuxqWYkoeksF_0() { return &___RjVwQUrdIaflqLncuxqWYkoeksF_0; }
	inline void set_RjVwQUrdIaflqLncuxqWYkoeksF_0(int32_t value)
	{
		___RjVwQUrdIaflqLncuxqWYkoeksF_0 = value;
	}

	inline static int32_t get_offset_of_IkjGTcZHBbXyWdZhHrngxaAVUov_1() { return static_cast<int32_t>(offsetof(HIDVibrationMotor_tD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD, ___IkjGTcZHBbXyWdZhHrngxaAVUov_1)); }
	inline int32_t get_IkjGTcZHBbXyWdZhHrngxaAVUov_1() const { return ___IkjGTcZHBbXyWdZhHrngxaAVUov_1; }
	inline int32_t* get_address_of_IkjGTcZHBbXyWdZhHrngxaAVUov_1() { return &___IkjGTcZHBbXyWdZhHrngxaAVUov_1; }
	inline void set_IkjGTcZHBbXyWdZhHrngxaAVUov_1(int32_t value)
	{
		___IkjGTcZHBbXyWdZhHrngxaAVUov_1 = value;
	}

	inline static int32_t get_offset_of_WPgfnXCmbruqQhVPUePGTNIbeIs_2() { return static_cast<int32_t>(offsetof(HIDVibrationMotor_tD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD, ___WPgfnXCmbruqQhVPUePGTNIbeIs_2)); }
	inline int32_t get_WPgfnXCmbruqQhVPUePGTNIbeIs_2() const { return ___WPgfnXCmbruqQhVPUePGTNIbeIs_2; }
	inline int32_t* get_address_of_WPgfnXCmbruqQhVPUePGTNIbeIs_2() { return &___WPgfnXCmbruqQhVPUePGTNIbeIs_2; }
	inline void set_WPgfnXCmbruqQhVPUePGTNIbeIs_2(int32_t value)
	{
		___WPgfnXCmbruqQhVPUePGTNIbeIs_2 = value;
	}

	inline static int32_t get_offset_of_ylgqmkRBsacEGDjEnylhZJSqJLM_3() { return static_cast<int32_t>(offsetof(HIDVibrationMotor_tD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD, ___ylgqmkRBsacEGDjEnylhZJSqJLM_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ylgqmkRBsacEGDjEnylhZJSqJLM_3() const { return ___ylgqmkRBsacEGDjEnylhZJSqJLM_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ylgqmkRBsacEGDjEnylhZJSqJLM_3() { return &___ylgqmkRBsacEGDjEnylhZJSqJLM_3; }
	inline void set_ylgqmkRBsacEGDjEnylhZJSqJLM_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ylgqmkRBsacEGDjEnylhZJSqJLM_3 = value;
		Il2CppCodeGenWriteBarrier((&___ylgqmkRBsacEGDjEnylhZJSqJLM_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDVIBRATIONMOTOR_TD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD_H
#ifndef HIDASYNCSTATE_TC164BD9FBE8C9F0429BDF795DCADBBABD1D3DF3F_H
#define HIDASYNCSTATE_TC164BD9FBE8C9F0429BDF795DCADBBABD1D3DF3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HidAsyncState
struct  HidAsyncState_tC164BD9FBE8C9F0429BDF795DCADBBABD1D3DF3F  : public RuntimeObject
{
public:
	// System.Object Rewired.HID.HidAsyncState::sPTpDqQdEBUDvQzULhTTSiouVDp
	RuntimeObject * ___sPTpDqQdEBUDvQzULhTTSiouVDp_0;
	// System.Object Rewired.HID.HidAsyncState::EzfDOGOPpWwwmlcesauoDQIXFNqR
	RuntimeObject * ___EzfDOGOPpWwwmlcesauoDQIXFNqR_1;

public:
	inline static int32_t get_offset_of_sPTpDqQdEBUDvQzULhTTSiouVDp_0() { return static_cast<int32_t>(offsetof(HidAsyncState_tC164BD9FBE8C9F0429BDF795DCADBBABD1D3DF3F, ___sPTpDqQdEBUDvQzULhTTSiouVDp_0)); }
	inline RuntimeObject * get_sPTpDqQdEBUDvQzULhTTSiouVDp_0() const { return ___sPTpDqQdEBUDvQzULhTTSiouVDp_0; }
	inline RuntimeObject ** get_address_of_sPTpDqQdEBUDvQzULhTTSiouVDp_0() { return &___sPTpDqQdEBUDvQzULhTTSiouVDp_0; }
	inline void set_sPTpDqQdEBUDvQzULhTTSiouVDp_0(RuntimeObject * value)
	{
		___sPTpDqQdEBUDvQzULhTTSiouVDp_0 = value;
		Il2CppCodeGenWriteBarrier((&___sPTpDqQdEBUDvQzULhTTSiouVDp_0), value);
	}

	inline static int32_t get_offset_of_EzfDOGOPpWwwmlcesauoDQIXFNqR_1() { return static_cast<int32_t>(offsetof(HidAsyncState_tC164BD9FBE8C9F0429BDF795DCADBBABD1D3DF3F, ___EzfDOGOPpWwwmlcesauoDQIXFNqR_1)); }
	inline RuntimeObject * get_EzfDOGOPpWwwmlcesauoDQIXFNqR_1() const { return ___EzfDOGOPpWwwmlcesauoDQIXFNqR_1; }
	inline RuntimeObject ** get_address_of_EzfDOGOPpWwwmlcesauoDQIXFNqR_1() { return &___EzfDOGOPpWwwmlcesauoDQIXFNqR_1; }
	inline void set_EzfDOGOPpWwwmlcesauoDQIXFNqR_1(RuntimeObject * value)
	{
		___EzfDOGOPpWwwmlcesauoDQIXFNqR_1 = value;
		Il2CppCodeGenWriteBarrier((&___EzfDOGOPpWwwmlcesauoDQIXFNqR_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDASYNCSTATE_TC164BD9FBE8C9F0429BDF795DCADBBABD1D3DF3F_H
#ifndef HIDOUTPUTREPORTHANDLER_TDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1_H
#define HIDOUTPUTREPORTHANDLER_TDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HidOutputReportHandler
struct  HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1  : public RuntimeObject
{
public:
	// Rewired.Utils.Classes.Utility.ThreadHelper Rewired.HID.HidOutputReportHandler::YCIAjsdTgJBhSKEobujeDTVToefg
	ThreadHelper_t4B3E611CF122FA6B00FFF5BE3B6CCAE8399F35CF * ___YCIAjsdTgJBhSKEobujeDTVToefg_3;
	// Rewired.HID.HidOutputReportHandler_qQwWjhhBpjGwyXouxBiErDakyMc Rewired.HID.HidOutputReportHandler::gWYcSreEUttuvqTHqKoRwtWQvHb
	qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2 * ___gWYcSreEUttuvqTHqKoRwtWQvHb_4;
	// Rewired.HID.HidOutputReportHandler_qQwWjhhBpjGwyXouxBiErDakyMc Rewired.HID.HidOutputReportHandler::AcueBFpbGaWvgcHGsyajhWTFHb
	qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2 * ___AcueBFpbGaWvgcHGsyajhWTFHb_5;
	// System.Boolean Rewired.HID.HidOutputReportHandler::yWQMhXTZSDJlTYAWMbZbErNHEywF
	bool ___yWQMhXTZSDJlTYAWMbZbErNHEywF_6;
	// System.Boolean Rewired.HID.HidOutputReportHandler::oGbqxjxXGgvgXNLPUMbdLzEDsmT
	bool ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_7;
	// System.Object Rewired.HID.HidOutputReportHandler::sAfeizOAktnwQKxVxmsSWsUMLvc
	RuntimeObject * ___sAfeizOAktnwQKxVxmsSWsUMLvc_8;
	// Rewired.HID.HidOutputReportHandler_WriteReportDelegate Rewired.HID.HidOutputReportHandler::miTAQrjTIXETROKSlTwwkjkJlreA
	WriteReportDelegate_tE0510A5AB2468221BF3ECDD4A176FE7F3A71706F * ___miTAQrjTIXETROKSlTwwkjkJlreA_9;
	// System.Boolean Rewired.HID.HidOutputReportHandler::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_10;

public:
	inline static int32_t get_offset_of_YCIAjsdTgJBhSKEobujeDTVToefg_3() { return static_cast<int32_t>(offsetof(HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1, ___YCIAjsdTgJBhSKEobujeDTVToefg_3)); }
	inline ThreadHelper_t4B3E611CF122FA6B00FFF5BE3B6CCAE8399F35CF * get_YCIAjsdTgJBhSKEobujeDTVToefg_3() const { return ___YCIAjsdTgJBhSKEobujeDTVToefg_3; }
	inline ThreadHelper_t4B3E611CF122FA6B00FFF5BE3B6CCAE8399F35CF ** get_address_of_YCIAjsdTgJBhSKEobujeDTVToefg_3() { return &___YCIAjsdTgJBhSKEobujeDTVToefg_3; }
	inline void set_YCIAjsdTgJBhSKEobujeDTVToefg_3(ThreadHelper_t4B3E611CF122FA6B00FFF5BE3B6CCAE8399F35CF * value)
	{
		___YCIAjsdTgJBhSKEobujeDTVToefg_3 = value;
		Il2CppCodeGenWriteBarrier((&___YCIAjsdTgJBhSKEobujeDTVToefg_3), value);
	}

	inline static int32_t get_offset_of_gWYcSreEUttuvqTHqKoRwtWQvHb_4() { return static_cast<int32_t>(offsetof(HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1, ___gWYcSreEUttuvqTHqKoRwtWQvHb_4)); }
	inline qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2 * get_gWYcSreEUttuvqTHqKoRwtWQvHb_4() const { return ___gWYcSreEUttuvqTHqKoRwtWQvHb_4; }
	inline qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2 ** get_address_of_gWYcSreEUttuvqTHqKoRwtWQvHb_4() { return &___gWYcSreEUttuvqTHqKoRwtWQvHb_4; }
	inline void set_gWYcSreEUttuvqTHqKoRwtWQvHb_4(qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2 * value)
	{
		___gWYcSreEUttuvqTHqKoRwtWQvHb_4 = value;
		Il2CppCodeGenWriteBarrier((&___gWYcSreEUttuvqTHqKoRwtWQvHb_4), value);
	}

	inline static int32_t get_offset_of_AcueBFpbGaWvgcHGsyajhWTFHb_5() { return static_cast<int32_t>(offsetof(HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1, ___AcueBFpbGaWvgcHGsyajhWTFHb_5)); }
	inline qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2 * get_AcueBFpbGaWvgcHGsyajhWTFHb_5() const { return ___AcueBFpbGaWvgcHGsyajhWTFHb_5; }
	inline qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2 ** get_address_of_AcueBFpbGaWvgcHGsyajhWTFHb_5() { return &___AcueBFpbGaWvgcHGsyajhWTFHb_5; }
	inline void set_AcueBFpbGaWvgcHGsyajhWTFHb_5(qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2 * value)
	{
		___AcueBFpbGaWvgcHGsyajhWTFHb_5 = value;
		Il2CppCodeGenWriteBarrier((&___AcueBFpbGaWvgcHGsyajhWTFHb_5), value);
	}

	inline static int32_t get_offset_of_yWQMhXTZSDJlTYAWMbZbErNHEywF_6() { return static_cast<int32_t>(offsetof(HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1, ___yWQMhXTZSDJlTYAWMbZbErNHEywF_6)); }
	inline bool get_yWQMhXTZSDJlTYAWMbZbErNHEywF_6() const { return ___yWQMhXTZSDJlTYAWMbZbErNHEywF_6; }
	inline bool* get_address_of_yWQMhXTZSDJlTYAWMbZbErNHEywF_6() { return &___yWQMhXTZSDJlTYAWMbZbErNHEywF_6; }
	inline void set_yWQMhXTZSDJlTYAWMbZbErNHEywF_6(bool value)
	{
		___yWQMhXTZSDJlTYAWMbZbErNHEywF_6 = value;
	}

	inline static int32_t get_offset_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_7() { return static_cast<int32_t>(offsetof(HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1, ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_7)); }
	inline bool get_oGbqxjxXGgvgXNLPUMbdLzEDsmT_7() const { return ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_7; }
	inline bool* get_address_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_7() { return &___oGbqxjxXGgvgXNLPUMbdLzEDsmT_7; }
	inline void set_oGbqxjxXGgvgXNLPUMbdLzEDsmT_7(bool value)
	{
		___oGbqxjxXGgvgXNLPUMbdLzEDsmT_7 = value;
	}

	inline static int32_t get_offset_of_sAfeizOAktnwQKxVxmsSWsUMLvc_8() { return static_cast<int32_t>(offsetof(HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1, ___sAfeizOAktnwQKxVxmsSWsUMLvc_8)); }
	inline RuntimeObject * get_sAfeizOAktnwQKxVxmsSWsUMLvc_8() const { return ___sAfeizOAktnwQKxVxmsSWsUMLvc_8; }
	inline RuntimeObject ** get_address_of_sAfeizOAktnwQKxVxmsSWsUMLvc_8() { return &___sAfeizOAktnwQKxVxmsSWsUMLvc_8; }
	inline void set_sAfeizOAktnwQKxVxmsSWsUMLvc_8(RuntimeObject * value)
	{
		___sAfeizOAktnwQKxVxmsSWsUMLvc_8 = value;
		Il2CppCodeGenWriteBarrier((&___sAfeizOAktnwQKxVxmsSWsUMLvc_8), value);
	}

	inline static int32_t get_offset_of_miTAQrjTIXETROKSlTwwkjkJlreA_9() { return static_cast<int32_t>(offsetof(HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1, ___miTAQrjTIXETROKSlTwwkjkJlreA_9)); }
	inline WriteReportDelegate_tE0510A5AB2468221BF3ECDD4A176FE7F3A71706F * get_miTAQrjTIXETROKSlTwwkjkJlreA_9() const { return ___miTAQrjTIXETROKSlTwwkjkJlreA_9; }
	inline WriteReportDelegate_tE0510A5AB2468221BF3ECDD4A176FE7F3A71706F ** get_address_of_miTAQrjTIXETROKSlTwwkjkJlreA_9() { return &___miTAQrjTIXETROKSlTwwkjkJlreA_9; }
	inline void set_miTAQrjTIXETROKSlTwwkjkJlreA_9(WriteReportDelegate_tE0510A5AB2468221BF3ECDD4A176FE7F3A71706F * value)
	{
		___miTAQrjTIXETROKSlTwwkjkJlreA_9 = value;
		Il2CppCodeGenWriteBarrier((&___miTAQrjTIXETROKSlTwwkjkJlreA_9), value);
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_10() { return static_cast<int32_t>(offsetof(HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1, ___SeCUoinDywZmqZDHRKupOdOaTke_10)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_10() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_10; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_10() { return &___SeCUoinDywZmqZDHRKupOdOaTke_10; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_10(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDOUTPUTREPORTHANDLER_TDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1_H
#ifndef SPECIALDEVICES_T7FD926F80A8BFC55C5B0D654087F1E1969608160_H
#define SPECIALDEVICES_T7FD926F80A8BFC55C5B0D654087F1E1969608160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.SpecialDevices
struct  SpecialDevices_t7FD926F80A8BFC55C5B0D654087F1E1969608160  : public RuntimeObject
{
public:

public:
};

struct SpecialDevices_t7FD926F80A8BFC55C5B0D654087F1E1969608160_StaticFields
{
public:
	// Rewired.HID.SpecialDevices_xgTeshlOrQNHJRhpOldNrbEuIDD[] Rewired.HID.SpecialDevices::OiwDpoVrGMJDTSsgSenCIKLVmQA
	xgTeshlOrQNHJRhpOldNrbEuIDDU5BU5D_t2AF95DE51A490AC9408AD0758B7D4CFF60DA8E6F* ___OiwDpoVrGMJDTSsgSenCIKLVmQA_1;

public:
	inline static int32_t get_offset_of_OiwDpoVrGMJDTSsgSenCIKLVmQA_1() { return static_cast<int32_t>(offsetof(SpecialDevices_t7FD926F80A8BFC55C5B0D654087F1E1969608160_StaticFields, ___OiwDpoVrGMJDTSsgSenCIKLVmQA_1)); }
	inline xgTeshlOrQNHJRhpOldNrbEuIDDU5BU5D_t2AF95DE51A490AC9408AD0758B7D4CFF60DA8E6F* get_OiwDpoVrGMJDTSsgSenCIKLVmQA_1() const { return ___OiwDpoVrGMJDTSsgSenCIKLVmQA_1; }
	inline xgTeshlOrQNHJRhpOldNrbEuIDDU5BU5D_t2AF95DE51A490AC9408AD0758B7D4CFF60DA8E6F** get_address_of_OiwDpoVrGMJDTSsgSenCIKLVmQA_1() { return &___OiwDpoVrGMJDTSsgSenCIKLVmQA_1; }
	inline void set_OiwDpoVrGMJDTSsgSenCIKLVmQA_1(xgTeshlOrQNHJRhpOldNrbEuIDDU5BU5D_t2AF95DE51A490AC9408AD0758B7D4CFF60DA8E6F* value)
	{
		___OiwDpoVrGMJDTSsgSenCIKLVmQA_1 = value;
		Il2CppCodeGenWriteBarrier((&___OiwDpoVrGMJDTSsgSenCIKLVmQA_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALDEVICES_T7FD926F80A8BFC55C5B0D654087F1E1969608160_H
#ifndef XGTESHLORQNHJRHPOLDNRBEUIDD_TF0D0E27B41450747E58631E8D6AEF6CC74733B69_H
#define XGTESHLORQNHJRHPOLDNRBEUIDD_TF0D0E27B41450747E58631E8D6AEF6CC74733B69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.SpecialDevices_xgTeshlOrQNHJRhpOldNrbEuIDD
struct  xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69  : public RuntimeObject
{
public:
	// System.UInt16 Rewired.HID.SpecialDevices_xgTeshlOrQNHJRhpOldNrbEuIDD::qNNdzuExLuNfrdyOzaNcsNVrpIQc
	uint16_t ___qNNdzuExLuNfrdyOzaNcsNVrpIQc_0;
	// System.UInt16 Rewired.HID.SpecialDevices_xgTeshlOrQNHJRhpOldNrbEuIDD::KclFaRHPVyrRHSfCxDIVgvgADJgN
	uint16_t ___KclFaRHPVyrRHSfCxDIVgvgADJgN_1;
	// System.String Rewired.HID.SpecialDevices_xgTeshlOrQNHJRhpOldNrbEuIDD::hkAHAbBESJASJeGsvHpkANiUnas
	String_t* ___hkAHAbBESJASJeGsvHpkANiUnas_2;
	// System.Boolean Rewired.HID.SpecialDevices_xgTeshlOrQNHJRhpOldNrbEuIDD::vVbUoUJKTYYgJmwHgAyrHHnVNrB
	bool ___vVbUoUJKTYYgJmwHgAyrHHnVNrB_3;
	// System.Int32 Rewired.HID.SpecialDevices_xgTeshlOrQNHJRhpOldNrbEuIDD::GwkaAVhZdVBugFUdUPGxbbAwtRY
	int32_t ___GwkaAVhZdVBugFUdUPGxbbAwtRY_4;
	// System.Int32 Rewired.HID.SpecialDevices_xgTeshlOrQNHJRhpOldNrbEuIDD::NrTqLQBJEpjtlDuUGPfGaBOGvpp
	int32_t ___NrTqLQBJEpjtlDuUGPfGaBOGvpp_5;
	// System.Int32 Rewired.HID.SpecialDevices_xgTeshlOrQNHJRhpOldNrbEuIDD::NmDDIiBdffXLHnURuiGdFYLeXJWi
	int32_t ___NmDDIiBdffXLHnURuiGdFYLeXJWi_6;
	// System.Single Rewired.HID.SpecialDevices_xgTeshlOrQNHJRhpOldNrbEuIDD::XRELSQboAmhkEqjogcpwBWhsogP
	float ___XRELSQboAmhkEqjogcpwBWhsogP_7;

public:
	inline static int32_t get_offset_of_qNNdzuExLuNfrdyOzaNcsNVrpIQc_0() { return static_cast<int32_t>(offsetof(xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69, ___qNNdzuExLuNfrdyOzaNcsNVrpIQc_0)); }
	inline uint16_t get_qNNdzuExLuNfrdyOzaNcsNVrpIQc_0() const { return ___qNNdzuExLuNfrdyOzaNcsNVrpIQc_0; }
	inline uint16_t* get_address_of_qNNdzuExLuNfrdyOzaNcsNVrpIQc_0() { return &___qNNdzuExLuNfrdyOzaNcsNVrpIQc_0; }
	inline void set_qNNdzuExLuNfrdyOzaNcsNVrpIQc_0(uint16_t value)
	{
		___qNNdzuExLuNfrdyOzaNcsNVrpIQc_0 = value;
	}

	inline static int32_t get_offset_of_KclFaRHPVyrRHSfCxDIVgvgADJgN_1() { return static_cast<int32_t>(offsetof(xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69, ___KclFaRHPVyrRHSfCxDIVgvgADJgN_1)); }
	inline uint16_t get_KclFaRHPVyrRHSfCxDIVgvgADJgN_1() const { return ___KclFaRHPVyrRHSfCxDIVgvgADJgN_1; }
	inline uint16_t* get_address_of_KclFaRHPVyrRHSfCxDIVgvgADJgN_1() { return &___KclFaRHPVyrRHSfCxDIVgvgADJgN_1; }
	inline void set_KclFaRHPVyrRHSfCxDIVgvgADJgN_1(uint16_t value)
	{
		___KclFaRHPVyrRHSfCxDIVgvgADJgN_1 = value;
	}

	inline static int32_t get_offset_of_hkAHAbBESJASJeGsvHpkANiUnas_2() { return static_cast<int32_t>(offsetof(xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69, ___hkAHAbBESJASJeGsvHpkANiUnas_2)); }
	inline String_t* get_hkAHAbBESJASJeGsvHpkANiUnas_2() const { return ___hkAHAbBESJASJeGsvHpkANiUnas_2; }
	inline String_t** get_address_of_hkAHAbBESJASJeGsvHpkANiUnas_2() { return &___hkAHAbBESJASJeGsvHpkANiUnas_2; }
	inline void set_hkAHAbBESJASJeGsvHpkANiUnas_2(String_t* value)
	{
		___hkAHAbBESJASJeGsvHpkANiUnas_2 = value;
		Il2CppCodeGenWriteBarrier((&___hkAHAbBESJASJeGsvHpkANiUnas_2), value);
	}

	inline static int32_t get_offset_of_vVbUoUJKTYYgJmwHgAyrHHnVNrB_3() { return static_cast<int32_t>(offsetof(xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69, ___vVbUoUJKTYYgJmwHgAyrHHnVNrB_3)); }
	inline bool get_vVbUoUJKTYYgJmwHgAyrHHnVNrB_3() const { return ___vVbUoUJKTYYgJmwHgAyrHHnVNrB_3; }
	inline bool* get_address_of_vVbUoUJKTYYgJmwHgAyrHHnVNrB_3() { return &___vVbUoUJKTYYgJmwHgAyrHHnVNrB_3; }
	inline void set_vVbUoUJKTYYgJmwHgAyrHHnVNrB_3(bool value)
	{
		___vVbUoUJKTYYgJmwHgAyrHHnVNrB_3 = value;
	}

	inline static int32_t get_offset_of_GwkaAVhZdVBugFUdUPGxbbAwtRY_4() { return static_cast<int32_t>(offsetof(xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69, ___GwkaAVhZdVBugFUdUPGxbbAwtRY_4)); }
	inline int32_t get_GwkaAVhZdVBugFUdUPGxbbAwtRY_4() const { return ___GwkaAVhZdVBugFUdUPGxbbAwtRY_4; }
	inline int32_t* get_address_of_GwkaAVhZdVBugFUdUPGxbbAwtRY_4() { return &___GwkaAVhZdVBugFUdUPGxbbAwtRY_4; }
	inline void set_GwkaAVhZdVBugFUdUPGxbbAwtRY_4(int32_t value)
	{
		___GwkaAVhZdVBugFUdUPGxbbAwtRY_4 = value;
	}

	inline static int32_t get_offset_of_NrTqLQBJEpjtlDuUGPfGaBOGvpp_5() { return static_cast<int32_t>(offsetof(xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69, ___NrTqLQBJEpjtlDuUGPfGaBOGvpp_5)); }
	inline int32_t get_NrTqLQBJEpjtlDuUGPfGaBOGvpp_5() const { return ___NrTqLQBJEpjtlDuUGPfGaBOGvpp_5; }
	inline int32_t* get_address_of_NrTqLQBJEpjtlDuUGPfGaBOGvpp_5() { return &___NrTqLQBJEpjtlDuUGPfGaBOGvpp_5; }
	inline void set_NrTqLQBJEpjtlDuUGPfGaBOGvpp_5(int32_t value)
	{
		___NrTqLQBJEpjtlDuUGPfGaBOGvpp_5 = value;
	}

	inline static int32_t get_offset_of_NmDDIiBdffXLHnURuiGdFYLeXJWi_6() { return static_cast<int32_t>(offsetof(xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69, ___NmDDIiBdffXLHnURuiGdFYLeXJWi_6)); }
	inline int32_t get_NmDDIiBdffXLHnURuiGdFYLeXJWi_6() const { return ___NmDDIiBdffXLHnURuiGdFYLeXJWi_6; }
	inline int32_t* get_address_of_NmDDIiBdffXLHnURuiGdFYLeXJWi_6() { return &___NmDDIiBdffXLHnURuiGdFYLeXJWi_6; }
	inline void set_NmDDIiBdffXLHnURuiGdFYLeXJWi_6(int32_t value)
	{
		___NmDDIiBdffXLHnURuiGdFYLeXJWi_6 = value;
	}

	inline static int32_t get_offset_of_XRELSQboAmhkEqjogcpwBWhsogP_7() { return static_cast<int32_t>(offsetof(xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69, ___XRELSQboAmhkEqjogcpwBWhsogP_7)); }
	inline float get_XRELSQboAmhkEqjogcpwBWhsogP_7() const { return ___XRELSQboAmhkEqjogcpwBWhsogP_7; }
	inline float* get_address_of_XRELSQboAmhkEqjogcpwBWhsogP_7() { return &___XRELSQboAmhkEqjogcpwBWhsogP_7; }
	inline void set_XRELSQboAmhkEqjogcpwBWhsogP_7(float value)
	{
		___XRELSQboAmhkEqjogcpwBWhsogP_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XGTESHLORQNHJRHPOLDNRBEUIDD_TF0D0E27B41450747E58631E8D6AEF6CC74733B69_H
#ifndef IGSTRUNNCUMHZFMJGQSDGCYFZNU_T8BA0D7526D539420345A84552152540A82348119_H
#define IGSTRUNNCUMHZFMJGQSDGCYFZNU_T8BA0D7526D539420345A84552152540A82348119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU
struct  igstrunnCuMHZFMjGQSDGcyfZnU_t8BA0D7526D539420345A84552152540A82348119  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU_SAPqXzqjGZlmsJaBMWgsmLJhEOu> Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU::AHOFIsQodiSzMcHMSUpdlsGdKfH
	List_1_t76A8E92538DC15148CD293B8684B84AD860EDA6C * ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0;

public:
	inline static int32_t get_offset_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() { return static_cast<int32_t>(offsetof(igstrunnCuMHZFMjGQSDGcyfZnU_t8BA0D7526D539420345A84552152540A82348119, ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0)); }
	inline List_1_t76A8E92538DC15148CD293B8684B84AD860EDA6C * get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() const { return ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0; }
	inline List_1_t76A8E92538DC15148CD293B8684B84AD860EDA6C ** get_address_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() { return &___AHOFIsQodiSzMcHMSUpdlsGdKfH_0; }
	inline void set_AHOFIsQodiSzMcHMSUpdlsGdKfH_0(List_1_t76A8E92538DC15148CD293B8684B84AD860EDA6C * value)
	{
		___AHOFIsQodiSzMcHMSUpdlsGdKfH_0 = value;
		Il2CppCodeGenWriteBarrier((&___AHOFIsQodiSzMcHMSUpdlsGdKfH_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGSTRUNNCUMHZFMJGQSDGCYFZNU_T8BA0D7526D539420345A84552152540A82348119_H
#ifndef STANDALONEAXIS_TA87F652134EED1F82C20E8D5492BAA7A03A84B0E_H
#define STANDALONEAXIS_TA87F652134EED1F82C20E8D5492BAA7A03A84B0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Internal.StandaloneAxis
struct  StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E  : public RuntimeObject
{
public:
	// System.Single Rewired.Internal.StandaloneAxis::_buttonActivationThreshold
	float ____buttonActivationThreshold_0;
	// Rewired.AxisCalibration Rewired.Internal.StandaloneAxis::_calibration
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F * ____calibration_1;
	// System.Single Rewired.Internal.StandaloneAxis::_valueRaw
	float ____valueRaw_2;
	// System.Single Rewired.Internal.StandaloneAxis::_valueRawPrev
	float ____valueRawPrev_3;
	// Rewired.Internal.StandaloneAxis_AxisValueChangedEventHandler Rewired.Internal.StandaloneAxis::KUlSPPdLfEKOBhsvlUicZMIWLRE
	AxisValueChangedEventHandler_t14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237 * ___KUlSPPdLfEKOBhsvlUicZMIWLRE_4;
	// Rewired.Internal.StandaloneAxis_AxisValueChangedEventHandler Rewired.Internal.StandaloneAxis::mGhyHlStIdvIiyOnXQeuafDvcjng
	AxisValueChangedEventHandler_t14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237 * ___mGhyHlStIdvIiyOnXQeuafDvcjng_5;
	// Rewired.Internal.StandaloneAxis_ButtonDownEventHandler Rewired.Internal.StandaloneAxis::xKHiNcdbLaKdvcxFiGwrKuIRfUU
	ButtonDownEventHandler_tDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE * ___xKHiNcdbLaKdvcxFiGwrKuIRfUU_6;
	// Rewired.Internal.StandaloneAxis_ButtonUpEventHandler Rewired.Internal.StandaloneAxis::DEgkhrYBjiHUxeODZhwJYEMpWwu
	ButtonUpEventHandler_t7D7868DC7B0024CD5CC804C9D9BA28C302222FB9 * ___DEgkhrYBjiHUxeODZhwJYEMpWwu_7;
	// Rewired.Internal.StandaloneAxis_ButtonValueChangedEventHandler Rewired.Internal.StandaloneAxis::PIDWGAUFGQIIdjKDIBekmKewfmzb
	ButtonValueChangedEventHandler_tD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5 * ___PIDWGAUFGQIIdjKDIBekmKewfmzb_8;
	// Rewired.Internal.StandaloneAxis_ButtonDownEventHandler Rewired.Internal.StandaloneAxis::LiXxUKIllyDqWfhinmsMtlvlqxZ
	ButtonDownEventHandler_tDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE * ___LiXxUKIllyDqWfhinmsMtlvlqxZ_9;
	// Rewired.Internal.StandaloneAxis_ButtonUpEventHandler Rewired.Internal.StandaloneAxis::WuAAKQyCOFjlKqFbRJifGpYdQfP
	ButtonUpEventHandler_t7D7868DC7B0024CD5CC804C9D9BA28C302222FB9 * ___WuAAKQyCOFjlKqFbRJifGpYdQfP_10;
	// Rewired.Internal.StandaloneAxis_ButtonValueChangedEventHandler Rewired.Internal.StandaloneAxis::SBrWOewdLfDOaaQtOSpeIoHHxxX
	ButtonValueChangedEventHandler_tD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5 * ___SBrWOewdLfDOaaQtOSpeIoHHxxX_11;

public:
	inline static int32_t get_offset_of__buttonActivationThreshold_0() { return static_cast<int32_t>(offsetof(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E, ____buttonActivationThreshold_0)); }
	inline float get__buttonActivationThreshold_0() const { return ____buttonActivationThreshold_0; }
	inline float* get_address_of__buttonActivationThreshold_0() { return &____buttonActivationThreshold_0; }
	inline void set__buttonActivationThreshold_0(float value)
	{
		____buttonActivationThreshold_0 = value;
	}

	inline static int32_t get_offset_of__calibration_1() { return static_cast<int32_t>(offsetof(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E, ____calibration_1)); }
	inline AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F * get__calibration_1() const { return ____calibration_1; }
	inline AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F ** get_address_of__calibration_1() { return &____calibration_1; }
	inline void set__calibration_1(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F * value)
	{
		____calibration_1 = value;
		Il2CppCodeGenWriteBarrier((&____calibration_1), value);
	}

	inline static int32_t get_offset_of__valueRaw_2() { return static_cast<int32_t>(offsetof(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E, ____valueRaw_2)); }
	inline float get__valueRaw_2() const { return ____valueRaw_2; }
	inline float* get_address_of__valueRaw_2() { return &____valueRaw_2; }
	inline void set__valueRaw_2(float value)
	{
		____valueRaw_2 = value;
	}

	inline static int32_t get_offset_of__valueRawPrev_3() { return static_cast<int32_t>(offsetof(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E, ____valueRawPrev_3)); }
	inline float get__valueRawPrev_3() const { return ____valueRawPrev_3; }
	inline float* get_address_of__valueRawPrev_3() { return &____valueRawPrev_3; }
	inline void set__valueRawPrev_3(float value)
	{
		____valueRawPrev_3 = value;
	}

	inline static int32_t get_offset_of_KUlSPPdLfEKOBhsvlUicZMIWLRE_4() { return static_cast<int32_t>(offsetof(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E, ___KUlSPPdLfEKOBhsvlUicZMIWLRE_4)); }
	inline AxisValueChangedEventHandler_t14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237 * get_KUlSPPdLfEKOBhsvlUicZMIWLRE_4() const { return ___KUlSPPdLfEKOBhsvlUicZMIWLRE_4; }
	inline AxisValueChangedEventHandler_t14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237 ** get_address_of_KUlSPPdLfEKOBhsvlUicZMIWLRE_4() { return &___KUlSPPdLfEKOBhsvlUicZMIWLRE_4; }
	inline void set_KUlSPPdLfEKOBhsvlUicZMIWLRE_4(AxisValueChangedEventHandler_t14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237 * value)
	{
		___KUlSPPdLfEKOBhsvlUicZMIWLRE_4 = value;
		Il2CppCodeGenWriteBarrier((&___KUlSPPdLfEKOBhsvlUicZMIWLRE_4), value);
	}

	inline static int32_t get_offset_of_mGhyHlStIdvIiyOnXQeuafDvcjng_5() { return static_cast<int32_t>(offsetof(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E, ___mGhyHlStIdvIiyOnXQeuafDvcjng_5)); }
	inline AxisValueChangedEventHandler_t14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237 * get_mGhyHlStIdvIiyOnXQeuafDvcjng_5() const { return ___mGhyHlStIdvIiyOnXQeuafDvcjng_5; }
	inline AxisValueChangedEventHandler_t14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237 ** get_address_of_mGhyHlStIdvIiyOnXQeuafDvcjng_5() { return &___mGhyHlStIdvIiyOnXQeuafDvcjng_5; }
	inline void set_mGhyHlStIdvIiyOnXQeuafDvcjng_5(AxisValueChangedEventHandler_t14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237 * value)
	{
		___mGhyHlStIdvIiyOnXQeuafDvcjng_5 = value;
		Il2CppCodeGenWriteBarrier((&___mGhyHlStIdvIiyOnXQeuafDvcjng_5), value);
	}

	inline static int32_t get_offset_of_xKHiNcdbLaKdvcxFiGwrKuIRfUU_6() { return static_cast<int32_t>(offsetof(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E, ___xKHiNcdbLaKdvcxFiGwrKuIRfUU_6)); }
	inline ButtonDownEventHandler_tDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE * get_xKHiNcdbLaKdvcxFiGwrKuIRfUU_6() const { return ___xKHiNcdbLaKdvcxFiGwrKuIRfUU_6; }
	inline ButtonDownEventHandler_tDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE ** get_address_of_xKHiNcdbLaKdvcxFiGwrKuIRfUU_6() { return &___xKHiNcdbLaKdvcxFiGwrKuIRfUU_6; }
	inline void set_xKHiNcdbLaKdvcxFiGwrKuIRfUU_6(ButtonDownEventHandler_tDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE * value)
	{
		___xKHiNcdbLaKdvcxFiGwrKuIRfUU_6 = value;
		Il2CppCodeGenWriteBarrier((&___xKHiNcdbLaKdvcxFiGwrKuIRfUU_6), value);
	}

	inline static int32_t get_offset_of_DEgkhrYBjiHUxeODZhwJYEMpWwu_7() { return static_cast<int32_t>(offsetof(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E, ___DEgkhrYBjiHUxeODZhwJYEMpWwu_7)); }
	inline ButtonUpEventHandler_t7D7868DC7B0024CD5CC804C9D9BA28C302222FB9 * get_DEgkhrYBjiHUxeODZhwJYEMpWwu_7() const { return ___DEgkhrYBjiHUxeODZhwJYEMpWwu_7; }
	inline ButtonUpEventHandler_t7D7868DC7B0024CD5CC804C9D9BA28C302222FB9 ** get_address_of_DEgkhrYBjiHUxeODZhwJYEMpWwu_7() { return &___DEgkhrYBjiHUxeODZhwJYEMpWwu_7; }
	inline void set_DEgkhrYBjiHUxeODZhwJYEMpWwu_7(ButtonUpEventHandler_t7D7868DC7B0024CD5CC804C9D9BA28C302222FB9 * value)
	{
		___DEgkhrYBjiHUxeODZhwJYEMpWwu_7 = value;
		Il2CppCodeGenWriteBarrier((&___DEgkhrYBjiHUxeODZhwJYEMpWwu_7), value);
	}

	inline static int32_t get_offset_of_PIDWGAUFGQIIdjKDIBekmKewfmzb_8() { return static_cast<int32_t>(offsetof(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E, ___PIDWGAUFGQIIdjKDIBekmKewfmzb_8)); }
	inline ButtonValueChangedEventHandler_tD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5 * get_PIDWGAUFGQIIdjKDIBekmKewfmzb_8() const { return ___PIDWGAUFGQIIdjKDIBekmKewfmzb_8; }
	inline ButtonValueChangedEventHandler_tD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5 ** get_address_of_PIDWGAUFGQIIdjKDIBekmKewfmzb_8() { return &___PIDWGAUFGQIIdjKDIBekmKewfmzb_8; }
	inline void set_PIDWGAUFGQIIdjKDIBekmKewfmzb_8(ButtonValueChangedEventHandler_tD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5 * value)
	{
		___PIDWGAUFGQIIdjKDIBekmKewfmzb_8 = value;
		Il2CppCodeGenWriteBarrier((&___PIDWGAUFGQIIdjKDIBekmKewfmzb_8), value);
	}

	inline static int32_t get_offset_of_LiXxUKIllyDqWfhinmsMtlvlqxZ_9() { return static_cast<int32_t>(offsetof(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E, ___LiXxUKIllyDqWfhinmsMtlvlqxZ_9)); }
	inline ButtonDownEventHandler_tDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE * get_LiXxUKIllyDqWfhinmsMtlvlqxZ_9() const { return ___LiXxUKIllyDqWfhinmsMtlvlqxZ_9; }
	inline ButtonDownEventHandler_tDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE ** get_address_of_LiXxUKIllyDqWfhinmsMtlvlqxZ_9() { return &___LiXxUKIllyDqWfhinmsMtlvlqxZ_9; }
	inline void set_LiXxUKIllyDqWfhinmsMtlvlqxZ_9(ButtonDownEventHandler_tDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE * value)
	{
		___LiXxUKIllyDqWfhinmsMtlvlqxZ_9 = value;
		Il2CppCodeGenWriteBarrier((&___LiXxUKIllyDqWfhinmsMtlvlqxZ_9), value);
	}

	inline static int32_t get_offset_of_WuAAKQyCOFjlKqFbRJifGpYdQfP_10() { return static_cast<int32_t>(offsetof(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E, ___WuAAKQyCOFjlKqFbRJifGpYdQfP_10)); }
	inline ButtonUpEventHandler_t7D7868DC7B0024CD5CC804C9D9BA28C302222FB9 * get_WuAAKQyCOFjlKqFbRJifGpYdQfP_10() const { return ___WuAAKQyCOFjlKqFbRJifGpYdQfP_10; }
	inline ButtonUpEventHandler_t7D7868DC7B0024CD5CC804C9D9BA28C302222FB9 ** get_address_of_WuAAKQyCOFjlKqFbRJifGpYdQfP_10() { return &___WuAAKQyCOFjlKqFbRJifGpYdQfP_10; }
	inline void set_WuAAKQyCOFjlKqFbRJifGpYdQfP_10(ButtonUpEventHandler_t7D7868DC7B0024CD5CC804C9D9BA28C302222FB9 * value)
	{
		___WuAAKQyCOFjlKqFbRJifGpYdQfP_10 = value;
		Il2CppCodeGenWriteBarrier((&___WuAAKQyCOFjlKqFbRJifGpYdQfP_10), value);
	}

	inline static int32_t get_offset_of_SBrWOewdLfDOaaQtOSpeIoHHxxX_11() { return static_cast<int32_t>(offsetof(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E, ___SBrWOewdLfDOaaQtOSpeIoHHxxX_11)); }
	inline ButtonValueChangedEventHandler_tD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5 * get_SBrWOewdLfDOaaQtOSpeIoHHxxX_11() const { return ___SBrWOewdLfDOaaQtOSpeIoHHxxX_11; }
	inline ButtonValueChangedEventHandler_tD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5 ** get_address_of_SBrWOewdLfDOaaQtOSpeIoHHxxX_11() { return &___SBrWOewdLfDOaaQtOSpeIoHHxxX_11; }
	inline void set_SBrWOewdLfDOaaQtOSpeIoHHxxX_11(ButtonValueChangedEventHandler_tD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5 * value)
	{
		___SBrWOewdLfDOaaQtOSpeIoHHxxX_11 = value;
		Il2CppCodeGenWriteBarrier((&___SBrWOewdLfDOaaQtOSpeIoHHxxX_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEAXIS_TA87F652134EED1F82C20E8D5492BAA7A03A84B0E_H
#ifndef STANDALONEAXIS2D_T6B514C46E4E8BD60077ACF59F158467F2B1C942A_H
#define STANDALONEAXIS2D_T6B514C46E4E8BD60077ACF59F158467F2B1C942A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Internal.StandaloneAxis2D
struct  StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A  : public RuntimeObject
{
public:
	// Rewired.Axis2DCalibration Rewired.Internal.StandaloneAxis2D::_calibration
	Axis2DCalibration_t4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1 * ____calibration_0;
	// Rewired.Internal.StandaloneAxis Rewired.Internal.StandaloneAxis2D::_xAxis
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E * ____xAxis_1;
	// Rewired.Internal.StandaloneAxis Rewired.Internal.StandaloneAxis2D::_yAxis
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E * ____yAxis_2;
	// System.Boolean Rewired.Internal.StandaloneAxis2D::_allowEvents
	bool ____allowEvents_3;
	// Rewired.Internal.StandaloneAxis2D_ValueChangedEventHandler Rewired.Internal.StandaloneAxis2D::_ValueChangedEvent
	ValueChangedEventHandler_t4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7 * ____ValueChangedEvent_4;
	// Rewired.Internal.StandaloneAxis2D_ValueChangedEventHandler Rewired.Internal.StandaloneAxis2D::_RawValueChangedEvent
	ValueChangedEventHandler_t4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7 * ____RawValueChangedEvent_5;

public:
	inline static int32_t get_offset_of__calibration_0() { return static_cast<int32_t>(offsetof(StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A, ____calibration_0)); }
	inline Axis2DCalibration_t4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1 * get__calibration_0() const { return ____calibration_0; }
	inline Axis2DCalibration_t4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1 ** get_address_of__calibration_0() { return &____calibration_0; }
	inline void set__calibration_0(Axis2DCalibration_t4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1 * value)
	{
		____calibration_0 = value;
		Il2CppCodeGenWriteBarrier((&____calibration_0), value);
	}

	inline static int32_t get_offset_of__xAxis_1() { return static_cast<int32_t>(offsetof(StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A, ____xAxis_1)); }
	inline StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E * get__xAxis_1() const { return ____xAxis_1; }
	inline StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E ** get_address_of__xAxis_1() { return &____xAxis_1; }
	inline void set__xAxis_1(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E * value)
	{
		____xAxis_1 = value;
		Il2CppCodeGenWriteBarrier((&____xAxis_1), value);
	}

	inline static int32_t get_offset_of__yAxis_2() { return static_cast<int32_t>(offsetof(StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A, ____yAxis_2)); }
	inline StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E * get__yAxis_2() const { return ____yAxis_2; }
	inline StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E ** get_address_of__yAxis_2() { return &____yAxis_2; }
	inline void set__yAxis_2(StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E * value)
	{
		____yAxis_2 = value;
		Il2CppCodeGenWriteBarrier((&____yAxis_2), value);
	}

	inline static int32_t get_offset_of__allowEvents_3() { return static_cast<int32_t>(offsetof(StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A, ____allowEvents_3)); }
	inline bool get__allowEvents_3() const { return ____allowEvents_3; }
	inline bool* get_address_of__allowEvents_3() { return &____allowEvents_3; }
	inline void set__allowEvents_3(bool value)
	{
		____allowEvents_3 = value;
	}

	inline static int32_t get_offset_of__ValueChangedEvent_4() { return static_cast<int32_t>(offsetof(StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A, ____ValueChangedEvent_4)); }
	inline ValueChangedEventHandler_t4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7 * get__ValueChangedEvent_4() const { return ____ValueChangedEvent_4; }
	inline ValueChangedEventHandler_t4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7 ** get_address_of__ValueChangedEvent_4() { return &____ValueChangedEvent_4; }
	inline void set__ValueChangedEvent_4(ValueChangedEventHandler_t4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7 * value)
	{
		____ValueChangedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&____ValueChangedEvent_4), value);
	}

	inline static int32_t get_offset_of__RawValueChangedEvent_5() { return static_cast<int32_t>(offsetof(StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A, ____RawValueChangedEvent_5)); }
	inline ValueChangedEventHandler_t4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7 * get__RawValueChangedEvent_5() const { return ____RawValueChangedEvent_5; }
	inline ValueChangedEventHandler_t4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7 ** get_address_of__RawValueChangedEvent_5() { return &____RawValueChangedEvent_5; }
	inline void set__RawValueChangedEvent_5(ValueChangedEventHandler_t4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7 * value)
	{
		____RawValueChangedEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&____RawValueChangedEvent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEAXIS2D_T6B514C46E4E8BD60077ACF59F158467F2B1C942A_H
#ifndef PLATFORMINPUTMANAGER_TF891982F1ED83426874318B2057900C730AAA62A_H
#define PLATFORMINPUTMANAGER_TF891982F1ED83426874318B2057900C730AAA62A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlatformInputManager
struct  PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.BridgedController> Rewired.PlatformInputManager::_DeviceConnectedEvent
	Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5 * ____DeviceConnectedEvent_0;
	// System.Action`1<Rewired.ControllerDisconnectedEventArgs> Rewired.PlatformInputManager::_DeviceDisconnectedEvent
	Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0 * ____DeviceDisconnectedEvent_1;
	// System.Action`1<Rewired.UpdateControllerInfoEventArgs> Rewired.PlatformInputManager::_UpdateControllerInfoEvent
	Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3 * ____UpdateControllerInfoEvent_2;
	// System.Action Rewired.PlatformInputManager::_SystemDeviceConnectedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____SystemDeviceConnectedEvent_3;
	// System.Action Rewired.PlatformInputManager::_SystemDeviceDisconnectedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____SystemDeviceDisconnectedEvent_4;

public:
	inline static int32_t get_offset_of__DeviceConnectedEvent_0() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____DeviceConnectedEvent_0)); }
	inline Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5 * get__DeviceConnectedEvent_0() const { return ____DeviceConnectedEvent_0; }
	inline Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5 ** get_address_of__DeviceConnectedEvent_0() { return &____DeviceConnectedEvent_0; }
	inline void set__DeviceConnectedEvent_0(Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5 * value)
	{
		____DeviceConnectedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____DeviceConnectedEvent_0), value);
	}

	inline static int32_t get_offset_of__DeviceDisconnectedEvent_1() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____DeviceDisconnectedEvent_1)); }
	inline Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0 * get__DeviceDisconnectedEvent_1() const { return ____DeviceDisconnectedEvent_1; }
	inline Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0 ** get_address_of__DeviceDisconnectedEvent_1() { return &____DeviceDisconnectedEvent_1; }
	inline void set__DeviceDisconnectedEvent_1(Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0 * value)
	{
		____DeviceDisconnectedEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&____DeviceDisconnectedEvent_1), value);
	}

	inline static int32_t get_offset_of__UpdateControllerInfoEvent_2() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____UpdateControllerInfoEvent_2)); }
	inline Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3 * get__UpdateControllerInfoEvent_2() const { return ____UpdateControllerInfoEvent_2; }
	inline Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3 ** get_address_of__UpdateControllerInfoEvent_2() { return &____UpdateControllerInfoEvent_2; }
	inline void set__UpdateControllerInfoEvent_2(Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3 * value)
	{
		____UpdateControllerInfoEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateControllerInfoEvent_2), value);
	}

	inline static int32_t get_offset_of__SystemDeviceConnectedEvent_3() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____SystemDeviceConnectedEvent_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__SystemDeviceConnectedEvent_3() const { return ____SystemDeviceConnectedEvent_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__SystemDeviceConnectedEvent_3() { return &____SystemDeviceConnectedEvent_3; }
	inline void set__SystemDeviceConnectedEvent_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____SystemDeviceConnectedEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&____SystemDeviceConnectedEvent_3), value);
	}

	inline static int32_t get_offset_of__SystemDeviceDisconnectedEvent_4() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____SystemDeviceDisconnectedEvent_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__SystemDeviceDisconnectedEvent_4() const { return ____SystemDeviceDisconnectedEvent_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__SystemDeviceDisconnectedEvent_4() { return &____SystemDeviceDisconnectedEvent_4; }
	inline void set__SystemDeviceDisconnectedEvent_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____SystemDeviceDisconnectedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&____SystemDeviceDisconnectedEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMINPUTMANAGER_TF891982F1ED83426874318B2057900C730AAA62A_H
#ifndef UNITYINPUTHELPER_T3D91912A1788E7F4F2971FA4E168665E00E04636_H
#define UNITYINPUTHELPER_T3D91912A1788E7F4F2971FA4E168665E00E04636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UnityInputHelper
struct  UnityInputHelper_t3D91912A1788E7F4F2971FA4E168665E00E04636  : public RuntimeObject
{
public:

public:
};

struct UnityInputHelper_t3D91912A1788E7F4F2971FA4E168665E00E04636_StaticFields
{
public:
	// Rewired.UnityInputHelper_lYuJuZsYJJgZnLsElVIsjMUXGeV[] Rewired.UnityInputHelper::lZlHpVlqScCxOayHMQQNLIJUPtMA
	lYuJuZsYJJgZnLsElVIsjMUXGeVU5BU5D_t0A7F75C3B268C533E1A3BE7243A800301FC78006* ___lZlHpVlqScCxOayHMQQNLIJUPtMA_0;

public:
	inline static int32_t get_offset_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_0() { return static_cast<int32_t>(offsetof(UnityInputHelper_t3D91912A1788E7F4F2971FA4E168665E00E04636_StaticFields, ___lZlHpVlqScCxOayHMQQNLIJUPtMA_0)); }
	inline lYuJuZsYJJgZnLsElVIsjMUXGeVU5BU5D_t0A7F75C3B268C533E1A3BE7243A800301FC78006* get_lZlHpVlqScCxOayHMQQNLIJUPtMA_0() const { return ___lZlHpVlqScCxOayHMQQNLIJUPtMA_0; }
	inline lYuJuZsYJJgZnLsElVIsjMUXGeVU5BU5D_t0A7F75C3B268C533E1A3BE7243A800301FC78006** get_address_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_0() { return &___lZlHpVlqScCxOayHMQQNLIJUPtMA_0; }
	inline void set_lZlHpVlqScCxOayHMQQNLIJUPtMA_0(lYuJuZsYJJgZnLsElVIsjMUXGeVU5BU5D_t0A7F75C3B268C533E1A3BE7243A800301FC78006* value)
	{
		___lZlHpVlqScCxOayHMQQNLIJUPtMA_0 = value;
		Il2CppCodeGenWriteBarrier((&___lZlHpVlqScCxOayHMQQNLIJUPtMA_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYINPUTHELPER_T3D91912A1788E7F4F2971FA4E168665E00E04636_H
#ifndef LYUJUZSYJJGZNLSELVISJMUXGEV_T5FFDD188949D9675A8ECC4F400C5FA1A0F0CCEA9_H
#define LYUJUZSYJJGZNLSELVISJMUXGEV_T5FFDD188949D9675A8ECC4F400C5FA1A0F0CCEA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UnityInputHelper_lYuJuZsYJJgZnLsElVIsjMUXGeV
struct  lYuJuZsYJJgZnLsElVIsjMUXGeV_t5FFDD188949D9675A8ECC4F400C5FA1A0F0CCEA9  : public RuntimeObject
{
public:
	// System.String[] Rewired.UnityInputHelper_lYuJuZsYJJgZnLsElVIsjMUXGeV::ojmYnkCHKzVQFExRGawmjxOAwip
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___ojmYnkCHKzVQFExRGawmjxOAwip_0;
	// System.String[] Rewired.UnityInputHelper_lYuJuZsYJJgZnLsElVIsjMUXGeV::jygrDMKtEfPwNxIkfLKMOCmUyrS
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___jygrDMKtEfPwNxIkfLKMOCmUyrS_1;

public:
	inline static int32_t get_offset_of_ojmYnkCHKzVQFExRGawmjxOAwip_0() { return static_cast<int32_t>(offsetof(lYuJuZsYJJgZnLsElVIsjMUXGeV_t5FFDD188949D9675A8ECC4F400C5FA1A0F0CCEA9, ___ojmYnkCHKzVQFExRGawmjxOAwip_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_ojmYnkCHKzVQFExRGawmjxOAwip_0() const { return ___ojmYnkCHKzVQFExRGawmjxOAwip_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_ojmYnkCHKzVQFExRGawmjxOAwip_0() { return &___ojmYnkCHKzVQFExRGawmjxOAwip_0; }
	inline void set_ojmYnkCHKzVQFExRGawmjxOAwip_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___ojmYnkCHKzVQFExRGawmjxOAwip_0 = value;
		Il2CppCodeGenWriteBarrier((&___ojmYnkCHKzVQFExRGawmjxOAwip_0), value);
	}

	inline static int32_t get_offset_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_1() { return static_cast<int32_t>(offsetof(lYuJuZsYJJgZnLsElVIsjMUXGeV_t5FFDD188949D9675A8ECC4F400C5FA1A0F0CCEA9, ___jygrDMKtEfPwNxIkfLKMOCmUyrS_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_jygrDMKtEfPwNxIkfLKMOCmUyrS_1() const { return ___jygrDMKtEfPwNxIkfLKMOCmUyrS_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_1() { return &___jygrDMKtEfPwNxIkfLKMOCmUyrS_1; }
	inline void set_jygrDMKtEfPwNxIkfLKMOCmUyrS_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___jygrDMKtEfPwNxIkfLKMOCmUyrS_1 = value;
		Il2CppCodeGenWriteBarrier((&___jygrDMKtEfPwNxIkfLKMOCmUyrS_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LYUJUZSYJJGZNLSELVISJMUXGEV_T5FFDD188949D9675A8ECC4F400C5FA1A0F0CCEA9_H
#ifndef UNITYUNIFIEDKEYBOARDSOURCE_T360607795D1E16597E86DCF369EA3EBB2C2C78DD_H
#define UNITYUNIFIEDKEYBOARDSOURCE_T360607795D1E16597E86DCF369EA3EBB2C2C78DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UnityUnifiedKeyboardSource
struct  UnityUnifiedKeyboardSource_t360607795D1E16597E86DCF369EA3EBB2C2C78DD  : public RuntimeObject
{
public:
	// System.Boolean Rewired.UnityUnifiedKeyboardSource::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_2;

public:
	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_2() { return static_cast<int32_t>(offsetof(UnityUnifiedKeyboardSource_t360607795D1E16597E86DCF369EA3EBB2C2C78DD, ___SeCUoinDywZmqZDHRKupOdOaTke_2)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_2() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_2; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_2() { return &___SeCUoinDywZmqZDHRKupOdOaTke_2; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_2(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_2 = value;
	}
};

struct UnityUnifiedKeyboardSource_t360607795D1E16597E86DCF369EA3EBB2C2C78DD_StaticFields
{
public:
	// Rewired.HardwareControllerMap_Game Rewired.UnityUnifiedKeyboardSource::ZDxZczTtwYyKCbAxUkBLSReseUH
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * ___ZDxZczTtwYyKCbAxUkBLSReseUH_1;

public:
	inline static int32_t get_offset_of_ZDxZczTtwYyKCbAxUkBLSReseUH_1() { return static_cast<int32_t>(offsetof(UnityUnifiedKeyboardSource_t360607795D1E16597E86DCF369EA3EBB2C2C78DD_StaticFields, ___ZDxZczTtwYyKCbAxUkBLSReseUH_1)); }
	inline HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * get_ZDxZczTtwYyKCbAxUkBLSReseUH_1() const { return ___ZDxZczTtwYyKCbAxUkBLSReseUH_1; }
	inline HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 ** get_address_of_ZDxZczTtwYyKCbAxUkBLSReseUH_1() { return &___ZDxZczTtwYyKCbAxUkBLSReseUH_1; }
	inline void set_ZDxZczTtwYyKCbAxUkBLSReseUH_1(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * value)
	{
		___ZDxZczTtwYyKCbAxUkBLSReseUH_1 = value;
		Il2CppCodeGenWriteBarrier((&___ZDxZczTtwYyKCbAxUkBLSReseUH_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYUNIFIEDKEYBOARDSOURCE_T360607795D1E16597E86DCF369EA3EBB2C2C78DD_H
#ifndef UNITYUNIFIEDMOUSESOURCE_T190B342D704A3698587DE5CC8FDB3B8B4D4839C5_H
#define UNITYUNIFIEDMOUSESOURCE_T190B342D704A3698587DE5CC8FDB3B8B4D4839C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UnityUnifiedMouseSource
struct  UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5  : public RuntimeObject
{
public:
	// Rewired.UpdateLoopDataSet`1<Rewired.UnityUnifiedMouseSource_hJvPEYlJFiWOITXXSJHwonxNnUv> Rewired.UnityUnifiedMouseSource::PwPjYSFGGMniIMfHkZVcHaYxjMi
	UpdateLoopDataSet_1_t6CA24B93FD5588022AC0CDD55D805BDDD51F2096 * ___PwPjYSFGGMniIMfHkZVcHaYxjMi_1;
	// System.Single[] Rewired.UnityUnifiedMouseSource::ITEosTYotauoYUkEMfXpdEGBpoIi
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___ITEosTYotauoYUkEMfXpdEGBpoIi_2;
	// System.Boolean[] Rewired.UnityUnifiedMouseSource::iVJAyfgBNrLbCgocuKyKOjvhJjT
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___iVJAyfgBNrLbCgocuKyKOjvhJjT_3;
	// System.Boolean Rewired.UnityUnifiedMouseSource::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_4;

public:
	inline static int32_t get_offset_of_PwPjYSFGGMniIMfHkZVcHaYxjMi_1() { return static_cast<int32_t>(offsetof(UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5, ___PwPjYSFGGMniIMfHkZVcHaYxjMi_1)); }
	inline UpdateLoopDataSet_1_t6CA24B93FD5588022AC0CDD55D805BDDD51F2096 * get_PwPjYSFGGMniIMfHkZVcHaYxjMi_1() const { return ___PwPjYSFGGMniIMfHkZVcHaYxjMi_1; }
	inline UpdateLoopDataSet_1_t6CA24B93FD5588022AC0CDD55D805BDDD51F2096 ** get_address_of_PwPjYSFGGMniIMfHkZVcHaYxjMi_1() { return &___PwPjYSFGGMniIMfHkZVcHaYxjMi_1; }
	inline void set_PwPjYSFGGMniIMfHkZVcHaYxjMi_1(UpdateLoopDataSet_1_t6CA24B93FD5588022AC0CDD55D805BDDD51F2096 * value)
	{
		___PwPjYSFGGMniIMfHkZVcHaYxjMi_1 = value;
		Il2CppCodeGenWriteBarrier((&___PwPjYSFGGMniIMfHkZVcHaYxjMi_1), value);
	}

	inline static int32_t get_offset_of_ITEosTYotauoYUkEMfXpdEGBpoIi_2() { return static_cast<int32_t>(offsetof(UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5, ___ITEosTYotauoYUkEMfXpdEGBpoIi_2)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_ITEosTYotauoYUkEMfXpdEGBpoIi_2() const { return ___ITEosTYotauoYUkEMfXpdEGBpoIi_2; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_ITEosTYotauoYUkEMfXpdEGBpoIi_2() { return &___ITEosTYotauoYUkEMfXpdEGBpoIi_2; }
	inline void set_ITEosTYotauoYUkEMfXpdEGBpoIi_2(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___ITEosTYotauoYUkEMfXpdEGBpoIi_2 = value;
		Il2CppCodeGenWriteBarrier((&___ITEosTYotauoYUkEMfXpdEGBpoIi_2), value);
	}

	inline static int32_t get_offset_of_iVJAyfgBNrLbCgocuKyKOjvhJjT_3() { return static_cast<int32_t>(offsetof(UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5, ___iVJAyfgBNrLbCgocuKyKOjvhJjT_3)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_iVJAyfgBNrLbCgocuKyKOjvhJjT_3() const { return ___iVJAyfgBNrLbCgocuKyKOjvhJjT_3; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_iVJAyfgBNrLbCgocuKyKOjvhJjT_3() { return &___iVJAyfgBNrLbCgocuKyKOjvhJjT_3; }
	inline void set_iVJAyfgBNrLbCgocuKyKOjvhJjT_3(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___iVJAyfgBNrLbCgocuKyKOjvhJjT_3 = value;
		Il2CppCodeGenWriteBarrier((&___iVJAyfgBNrLbCgocuKyKOjvhJjT_3), value);
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_4() { return static_cast<int32_t>(offsetof(UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5, ___SeCUoinDywZmqZDHRKupOdOaTke_4)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_4() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_4; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_4() { return &___SeCUoinDywZmqZDHRKupOdOaTke_4; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_4(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_4 = value;
	}
};

struct UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5_StaticFields
{
public:
	// Rewired.HardwareControllerMap_Game Rewired.UnityUnifiedMouseSource::ZDxZczTtwYyKCbAxUkBLSReseUH
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * ___ZDxZczTtwYyKCbAxUkBLSReseUH_0;
	// System.Func`1<Rewired.UnityUnifiedMouseSource_hJvPEYlJFiWOITXXSJHwonxNnUv> Rewired.UnityUnifiedMouseSource::wKZsRNpOhQctCfJnvfAdTetobSIx
	Func_1_tCB096340A0ECB0571AFD6D0FAA95AC42D108DD98 * ___wKZsRNpOhQctCfJnvfAdTetobSIx_5;

public:
	inline static int32_t get_offset_of_ZDxZczTtwYyKCbAxUkBLSReseUH_0() { return static_cast<int32_t>(offsetof(UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5_StaticFields, ___ZDxZczTtwYyKCbAxUkBLSReseUH_0)); }
	inline HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * get_ZDxZczTtwYyKCbAxUkBLSReseUH_0() const { return ___ZDxZczTtwYyKCbAxUkBLSReseUH_0; }
	inline HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 ** get_address_of_ZDxZczTtwYyKCbAxUkBLSReseUH_0() { return &___ZDxZczTtwYyKCbAxUkBLSReseUH_0; }
	inline void set_ZDxZczTtwYyKCbAxUkBLSReseUH_0(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * value)
	{
		___ZDxZczTtwYyKCbAxUkBLSReseUH_0 = value;
		Il2CppCodeGenWriteBarrier((&___ZDxZczTtwYyKCbAxUkBLSReseUH_0), value);
	}

	inline static int32_t get_offset_of_wKZsRNpOhQctCfJnvfAdTetobSIx_5() { return static_cast<int32_t>(offsetof(UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5_StaticFields, ___wKZsRNpOhQctCfJnvfAdTetobSIx_5)); }
	inline Func_1_tCB096340A0ECB0571AFD6D0FAA95AC42D108DD98 * get_wKZsRNpOhQctCfJnvfAdTetobSIx_5() const { return ___wKZsRNpOhQctCfJnvfAdTetobSIx_5; }
	inline Func_1_tCB096340A0ECB0571AFD6D0FAA95AC42D108DD98 ** get_address_of_wKZsRNpOhQctCfJnvfAdTetobSIx_5() { return &___wKZsRNpOhQctCfJnvfAdTetobSIx_5; }
	inline void set_wKZsRNpOhQctCfJnvfAdTetobSIx_5(Func_1_tCB096340A0ECB0571AFD6D0FAA95AC42D108DD98 * value)
	{
		___wKZsRNpOhQctCfJnvfAdTetobSIx_5 = value;
		Il2CppCodeGenWriteBarrier((&___wKZsRNpOhQctCfJnvfAdTetobSIx_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYUNIFIEDMOUSESOURCE_T190B342D704A3698587DE5CC8FDB3B8B4D4839C5_H
#ifndef HJVPEYLJFIWOITXXSJHWONXNNUV_TCE253F5A620DDCC3B6E224B3CD0BD31047F9BB3A_H
#define HJVPEYLJFIWOITXXSJHWONXNNUV_TCE253F5A620DDCC3B6E224B3CD0BD31047F9BB3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UnityUnifiedMouseSource_hJvPEYlJFiWOITXXSJHwonxNnUv
struct  hJvPEYlJFiWOITXXSJHwonxNnUv_tCE253F5A620DDCC3B6E224B3CD0BD31047F9BB3A  : public RuntimeObject
{
public:
	// System.Single[] Rewired.UnityUnifiedMouseSource_hJvPEYlJFiWOITXXSJHwonxNnUv::ITEosTYotauoYUkEMfXpdEGBpoIi
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___ITEosTYotauoYUkEMfXpdEGBpoIi_0;
	// System.Boolean[] Rewired.UnityUnifiedMouseSource_hJvPEYlJFiWOITXXSJHwonxNnUv::iVJAyfgBNrLbCgocuKyKOjvhJjT
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___iVJAyfgBNrLbCgocuKyKOjvhJjT_1;

public:
	inline static int32_t get_offset_of_ITEosTYotauoYUkEMfXpdEGBpoIi_0() { return static_cast<int32_t>(offsetof(hJvPEYlJFiWOITXXSJHwonxNnUv_tCE253F5A620DDCC3B6E224B3CD0BD31047F9BB3A, ___ITEosTYotauoYUkEMfXpdEGBpoIi_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_ITEosTYotauoYUkEMfXpdEGBpoIi_0() const { return ___ITEosTYotauoYUkEMfXpdEGBpoIi_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_ITEosTYotauoYUkEMfXpdEGBpoIi_0() { return &___ITEosTYotauoYUkEMfXpdEGBpoIi_0; }
	inline void set_ITEosTYotauoYUkEMfXpdEGBpoIi_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___ITEosTYotauoYUkEMfXpdEGBpoIi_0 = value;
		Il2CppCodeGenWriteBarrier((&___ITEosTYotauoYUkEMfXpdEGBpoIi_0), value);
	}

	inline static int32_t get_offset_of_iVJAyfgBNrLbCgocuKyKOjvhJjT_1() { return static_cast<int32_t>(offsetof(hJvPEYlJFiWOITXXSJHwonxNnUv_tCE253F5A620DDCC3B6E224B3CD0BD31047F9BB3A, ___iVJAyfgBNrLbCgocuKyKOjvhJjT_1)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_iVJAyfgBNrLbCgocuKyKOjvhJjT_1() const { return ___iVJAyfgBNrLbCgocuKyKOjvhJjT_1; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_iVJAyfgBNrLbCgocuKyKOjvhJjT_1() { return &___iVJAyfgBNrLbCgocuKyKOjvhJjT_1; }
	inline void set_iVJAyfgBNrLbCgocuKyKOjvhJjT_1(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___iVJAyfgBNrLbCgocuKyKOjvhJjT_1 = value;
		Il2CppCodeGenWriteBarrier((&___iVJAyfgBNrLbCgocuKyKOjvhJjT_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HJVPEYLJFIWOITXXSJHWONXNNUV_TCE253F5A620DDCC3B6E224B3CD0BD31047F9BB3A_H
#ifndef SHHHWMRREBDKLQDSPHHCPGLTOQH_T526B73E627059D9BB097766207D6D3B243833459_H
#define SHHHWMRREBDKLQDSPHHCPGLTOQH_T526B73E627059D9BB097766207D6D3B243833459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SHhhwmrrEBdklqDsPHHCPGltOQH
struct  SHhhwmrrEBdklqDsPHHCPGltOQH_t526B73E627059D9BB097766207D6D3B243833459  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHHHWMRREBDKLQDSPHHCPGLTOQH_T526B73E627059D9BB097766207D6D3B243833459_H
#ifndef SNWFMEAUUAAYDZPHHXGGFLMYKIG_T6CD465E16931B16F59BEC3F4D9308A7155AA8DA5_H
#define SNWFMEAUUAAYDZPHHXGGFLMYKIG_T6CD465E16931B16F59BEC3F4D9308A7155AA8DA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SNWfMeAUUAAydzPHHxGGFlmYkig
struct  SNWfMeAUUAAydzPHHxGGFlmYkig_t6CD465E16931B16F59BEC3F4D9308A7155AA8DA5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SNWFMEAUUAAYDZPHHXGGFLMYKIG_T6CD465E16931B16F59BEC3F4D9308A7155AA8DA5_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef IDABDPQYVLEAXMJZFKEIOCBPDMUC_T7E16D5BBE8507AE86374AD4C25BE551F8033B28B_H
#define IDABDPQYVLEAXMJZFKEIOCBPDMUC_T7E16D5BBE8507AE86374AD4C25BE551F8033B28B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iDabdpqyvlEAXmjzfkEiOCBPdMuc
struct  iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B  : public RuntimeObject
{
public:
	// System.Int32 iDabdpqyvlEAXmjzfkEiOCBPdMuc::QVmtnTUxrGwnkEUluvTIVeAJedsI
	int32_t ___QVmtnTUxrGwnkEUluvTIVeAJedsI_5;
	// Rewired.UpdateLoopDataSet`1<iDabdpqyvlEAXmjzfkEiOCBPdMuc_deXVkUCQurzFRSBNargAFcePsbp> iDabdpqyvlEAXmjzfkEiOCBPdMuc::HWmOewXpuebOsZDStBejWuBUGqK
	UpdateLoopDataSet_1_t344F79FFB18FB5709B0825099571D8D3A83C0473 * ___HWmOewXpuebOsZDStBejWuBUGqK_6;

public:
	inline static int32_t get_offset_of_QVmtnTUxrGwnkEUluvTIVeAJedsI_5() { return static_cast<int32_t>(offsetof(iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B, ___QVmtnTUxrGwnkEUluvTIVeAJedsI_5)); }
	inline int32_t get_QVmtnTUxrGwnkEUluvTIVeAJedsI_5() const { return ___QVmtnTUxrGwnkEUluvTIVeAJedsI_5; }
	inline int32_t* get_address_of_QVmtnTUxrGwnkEUluvTIVeAJedsI_5() { return &___QVmtnTUxrGwnkEUluvTIVeAJedsI_5; }
	inline void set_QVmtnTUxrGwnkEUluvTIVeAJedsI_5(int32_t value)
	{
		___QVmtnTUxrGwnkEUluvTIVeAJedsI_5 = value;
	}

	inline static int32_t get_offset_of_HWmOewXpuebOsZDStBejWuBUGqK_6() { return static_cast<int32_t>(offsetof(iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B, ___HWmOewXpuebOsZDStBejWuBUGqK_6)); }
	inline UpdateLoopDataSet_1_t344F79FFB18FB5709B0825099571D8D3A83C0473 * get_HWmOewXpuebOsZDStBejWuBUGqK_6() const { return ___HWmOewXpuebOsZDStBejWuBUGqK_6; }
	inline UpdateLoopDataSet_1_t344F79FFB18FB5709B0825099571D8D3A83C0473 ** get_address_of_HWmOewXpuebOsZDStBejWuBUGqK_6() { return &___HWmOewXpuebOsZDStBejWuBUGqK_6; }
	inline void set_HWmOewXpuebOsZDStBejWuBUGqK_6(UpdateLoopDataSet_1_t344F79FFB18FB5709B0825099571D8D3A83C0473 * value)
	{
		___HWmOewXpuebOsZDStBejWuBUGqK_6 = value;
		Il2CppCodeGenWriteBarrier((&___HWmOewXpuebOsZDStBejWuBUGqK_6), value);
	}
};

struct iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields
{
public:
	// Rewired.Utils.Classes.Utility.ObjectPool`1<iDabdpqyvlEAXmjzfkEiOCBPdMuc> iDabdpqyvlEAXmjzfkEiOCBPdMuc::NZUXTnMHDkwJgNxVMtXOIRsHDGN
	ObjectPool_1_tB0F74D18847376089F85915E3CE5B7A8CD181D2C * ___NZUXTnMHDkwJgNxVMtXOIRsHDGN_2;
	// iDabdpqyvlEAXmjzfkEiOCBPdMuc[] iDabdpqyvlEAXmjzfkEiOCBPdMuc::PtVcZnVLLfsPRoQGwAAZntklgED
	iDabdpqyvlEAXmjzfkEiOCBPdMucU5BU5D_tAEED0569CDC25A1F22C37A514772B6A59E6DCD58* ___PtVcZnVLLfsPRoQGwAAZntklgED_3;
	// System.Int32 iDabdpqyvlEAXmjzfkEiOCBPdMuc::KXtDRnIjAQGAjYaGjSzcpROKhQZ
	int32_t ___KXtDRnIjAQGAjYaGjSzcpROKhQZ_4;
	// System.Func`1<iDabdpqyvlEAXmjzfkEiOCBPdMuc> iDabdpqyvlEAXmjzfkEiOCBPdMuc::xuKcWqHPyZZLojTegosyfuffNaVL
	Func_1_tFCBCFB47A5A1ECA4BFAE4B15DD3E01CB5A3CFB4B * ___xuKcWqHPyZZLojTegosyfuffNaVL_7;
	// System.Action`1<iDabdpqyvlEAXmjzfkEiOCBPdMuc> iDabdpqyvlEAXmjzfkEiOCBPdMuc::xgFcMzbcKwfNhKMwwFcokhPKEXIb
	Action_1_t2D38188C06D89FCBF7A89B8A484072049DA402D3 * ___xgFcMzbcKwfNhKMwwFcokhPKEXIb_8;
	// System.Func`1<iDabdpqyvlEAXmjzfkEiOCBPdMuc_deXVkUCQurzFRSBNargAFcePsbp> iDabdpqyvlEAXmjzfkEiOCBPdMuc::wzuTgIXkBigvyhwmUgzxbJRwaLDH
	Func_1_t0D5025F33D441612826295FBD43351ED3FA668AF * ___wzuTgIXkBigvyhwmUgzxbJRwaLDH_9;

public:
	inline static int32_t get_offset_of_NZUXTnMHDkwJgNxVMtXOIRsHDGN_2() { return static_cast<int32_t>(offsetof(iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields, ___NZUXTnMHDkwJgNxVMtXOIRsHDGN_2)); }
	inline ObjectPool_1_tB0F74D18847376089F85915E3CE5B7A8CD181D2C * get_NZUXTnMHDkwJgNxVMtXOIRsHDGN_2() const { return ___NZUXTnMHDkwJgNxVMtXOIRsHDGN_2; }
	inline ObjectPool_1_tB0F74D18847376089F85915E3CE5B7A8CD181D2C ** get_address_of_NZUXTnMHDkwJgNxVMtXOIRsHDGN_2() { return &___NZUXTnMHDkwJgNxVMtXOIRsHDGN_2; }
	inline void set_NZUXTnMHDkwJgNxVMtXOIRsHDGN_2(ObjectPool_1_tB0F74D18847376089F85915E3CE5B7A8CD181D2C * value)
	{
		___NZUXTnMHDkwJgNxVMtXOIRsHDGN_2 = value;
		Il2CppCodeGenWriteBarrier((&___NZUXTnMHDkwJgNxVMtXOIRsHDGN_2), value);
	}

	inline static int32_t get_offset_of_PtVcZnVLLfsPRoQGwAAZntklgED_3() { return static_cast<int32_t>(offsetof(iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields, ___PtVcZnVLLfsPRoQGwAAZntklgED_3)); }
	inline iDabdpqyvlEAXmjzfkEiOCBPdMucU5BU5D_tAEED0569CDC25A1F22C37A514772B6A59E6DCD58* get_PtVcZnVLLfsPRoQGwAAZntklgED_3() const { return ___PtVcZnVLLfsPRoQGwAAZntklgED_3; }
	inline iDabdpqyvlEAXmjzfkEiOCBPdMucU5BU5D_tAEED0569CDC25A1F22C37A514772B6A59E6DCD58** get_address_of_PtVcZnVLLfsPRoQGwAAZntklgED_3() { return &___PtVcZnVLLfsPRoQGwAAZntklgED_3; }
	inline void set_PtVcZnVLLfsPRoQGwAAZntklgED_3(iDabdpqyvlEAXmjzfkEiOCBPdMucU5BU5D_tAEED0569CDC25A1F22C37A514772B6A59E6DCD58* value)
	{
		___PtVcZnVLLfsPRoQGwAAZntklgED_3 = value;
		Il2CppCodeGenWriteBarrier((&___PtVcZnVLLfsPRoQGwAAZntklgED_3), value);
	}

	inline static int32_t get_offset_of_KXtDRnIjAQGAjYaGjSzcpROKhQZ_4() { return static_cast<int32_t>(offsetof(iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields, ___KXtDRnIjAQGAjYaGjSzcpROKhQZ_4)); }
	inline int32_t get_KXtDRnIjAQGAjYaGjSzcpROKhQZ_4() const { return ___KXtDRnIjAQGAjYaGjSzcpROKhQZ_4; }
	inline int32_t* get_address_of_KXtDRnIjAQGAjYaGjSzcpROKhQZ_4() { return &___KXtDRnIjAQGAjYaGjSzcpROKhQZ_4; }
	inline void set_KXtDRnIjAQGAjYaGjSzcpROKhQZ_4(int32_t value)
	{
		___KXtDRnIjAQGAjYaGjSzcpROKhQZ_4 = value;
	}

	inline static int32_t get_offset_of_xuKcWqHPyZZLojTegosyfuffNaVL_7() { return static_cast<int32_t>(offsetof(iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields, ___xuKcWqHPyZZLojTegosyfuffNaVL_7)); }
	inline Func_1_tFCBCFB47A5A1ECA4BFAE4B15DD3E01CB5A3CFB4B * get_xuKcWqHPyZZLojTegosyfuffNaVL_7() const { return ___xuKcWqHPyZZLojTegosyfuffNaVL_7; }
	inline Func_1_tFCBCFB47A5A1ECA4BFAE4B15DD3E01CB5A3CFB4B ** get_address_of_xuKcWqHPyZZLojTegosyfuffNaVL_7() { return &___xuKcWqHPyZZLojTegosyfuffNaVL_7; }
	inline void set_xuKcWqHPyZZLojTegosyfuffNaVL_7(Func_1_tFCBCFB47A5A1ECA4BFAE4B15DD3E01CB5A3CFB4B * value)
	{
		___xuKcWqHPyZZLojTegosyfuffNaVL_7 = value;
		Il2CppCodeGenWriteBarrier((&___xuKcWqHPyZZLojTegosyfuffNaVL_7), value);
	}

	inline static int32_t get_offset_of_xgFcMzbcKwfNhKMwwFcokhPKEXIb_8() { return static_cast<int32_t>(offsetof(iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields, ___xgFcMzbcKwfNhKMwwFcokhPKEXIb_8)); }
	inline Action_1_t2D38188C06D89FCBF7A89B8A484072049DA402D3 * get_xgFcMzbcKwfNhKMwwFcokhPKEXIb_8() const { return ___xgFcMzbcKwfNhKMwwFcokhPKEXIb_8; }
	inline Action_1_t2D38188C06D89FCBF7A89B8A484072049DA402D3 ** get_address_of_xgFcMzbcKwfNhKMwwFcokhPKEXIb_8() { return &___xgFcMzbcKwfNhKMwwFcokhPKEXIb_8; }
	inline void set_xgFcMzbcKwfNhKMwwFcokhPKEXIb_8(Action_1_t2D38188C06D89FCBF7A89B8A484072049DA402D3 * value)
	{
		___xgFcMzbcKwfNhKMwwFcokhPKEXIb_8 = value;
		Il2CppCodeGenWriteBarrier((&___xgFcMzbcKwfNhKMwwFcokhPKEXIb_8), value);
	}

	inline static int32_t get_offset_of_wzuTgIXkBigvyhwmUgzxbJRwaLDH_9() { return static_cast<int32_t>(offsetof(iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields, ___wzuTgIXkBigvyhwmUgzxbJRwaLDH_9)); }
	inline Func_1_t0D5025F33D441612826295FBD43351ED3FA668AF * get_wzuTgIXkBigvyhwmUgzxbJRwaLDH_9() const { return ___wzuTgIXkBigvyhwmUgzxbJRwaLDH_9; }
	inline Func_1_t0D5025F33D441612826295FBD43351ED3FA668AF ** get_address_of_wzuTgIXkBigvyhwmUgzxbJRwaLDH_9() { return &___wzuTgIXkBigvyhwmUgzxbJRwaLDH_9; }
	inline void set_wzuTgIXkBigvyhwmUgzxbJRwaLDH_9(Func_1_t0D5025F33D441612826295FBD43351ED3FA668AF * value)
	{
		___wzuTgIXkBigvyhwmUgzxbJRwaLDH_9 = value;
		Il2CppCodeGenWriteBarrier((&___wzuTgIXkBigvyhwmUgzxbJRwaLDH_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDABDPQYVLEAXMJZFKEIOCBPDMUC_T7E16D5BBE8507AE86374AD4C25BE551F8033B28B_H
#ifndef LUBZJTKAPCIKOHMRHHJAMCDIMRRR_TD9D698C83E139CBDDAEC47D478281011C3E60EEE_H
#define LUBZJTKAPCIKOHMRHHJAMCDIMRRR_TD9D698C83E139CBDDAEC47D478281011C3E60EEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// lUbzJtkApcIkohMrHhJAmcDImRRR
struct  lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Rewired.Joystick> lUbzJtkApcIkohMrHhJAmcDImRRR::QgoIWtHsaMGVeBFhqUvbmSmsiRh
	List_1_tA72AFBAA71AD07F0E9CDE70C7F4744366B2E2D8B * ___QgoIWtHsaMGVeBFhqUvbmSmsiRh_0;
	// System.Collections.Generic.List`1<Rewired.Joystick> lUbzJtkApcIkohMrHhJAmcDImRRR::rHKCUSVpCkqbhfZvJGgsrHqdAPs
	List_1_tA72AFBAA71AD07F0E9CDE70C7F4744366B2E2D8B * ___rHKCUSVpCkqbhfZvJGgsrHqdAPs_1;
	// System.Collections.Generic.List`1<Rewired.CustomController> lUbzJtkApcIkohMrHhJAmcDImRRR::dKRJCZQhWFihhgHVLBWkTcBEUEZ
	List_1_tB037A3809838545DA54CE039436BC1BF87578A9B * ___dKRJCZQhWFihhgHVLBWkTcBEUEZ_2;
	// System.Collections.Generic.List`1<Rewired.Controller> lUbzJtkApcIkohMrHhJAmcDImRRR::tvaElgXLUKAHFDoRhflnYxhnyPI
	List_1_t20C185E0480AE6CA25B3B6AD936AA34CDE1EC518 * ___tvaElgXLUKAHFDoRhflnYxhnyPI_3;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller> lUbzJtkApcIkohMrHhJAmcDImRRR::bemErvveeaCaSZFVNunRQfuZPez
	ReadOnlyCollection_1_t037BB93EEC8E496E6B7901A805B53290BC747353 * ___bemErvveeaCaSZFVNunRQfuZPez_4;
	// Rewired.Keyboard lUbzJtkApcIkohMrHhJAmcDImRRR::OSuLPPJEiNhXcAilTdfZMoZXfznt
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * ___OSuLPPJEiNhXcAilTdfZMoZXfznt_5;
	// Rewired.Mouse lUbzJtkApcIkohMrHhJAmcDImRRR::vcFHuVHhWhJcpPiVLFZqonrrcV
	Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3 * ___vcFHuVHhWhJcpPiVLFZqonrrcV_6;
	// Rewired.Data.ConfigVars lUbzJtkApcIkohMrHhJAmcDImRRR::bsdXVIEsRZlaTBlTrOAhsApnYKn
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * ___bsdXVIEsRZlaTBlTrOAhsApnYKn_7;
	// OmePuLXqingOShAfdEjGZVdOnDpG[] lUbzJtkApcIkohMrHhJAmcDImRRR::mCOQtFensPUoYxPTcAQQdFgYYSo
	OmePuLXqingOShAfdEjGZVdOnDpGU5BU5D_t6371EF4B41790887302C0A070D79235FCFE476D2* ___mCOQtFensPUoYxPTcAQQdFgYYSo_8;
	// OmePuLXqingOShAfdEjGZVdOnDpG[] lUbzJtkApcIkohMrHhJAmcDImRRR::zaBpOeSvCieYmNRzYvTnztQTkri
	OmePuLXqingOShAfdEjGZVdOnDpGU5BU5D_t6371EF4B41790887302C0A070D79235FCFE476D2* ___zaBpOeSvCieYmNRzYvTnztQTkri_9;
	// OmePuLXqingOShAfdEjGZVdOnDpG[0...,0...] lUbzJtkApcIkohMrHhJAmcDImRRR::OjQnvtvXVmEgxqUHpwsCCndFmCp
	OmePuLXqingOShAfdEjGZVdOnDpGU5BU2CU5D_t719ED9D943C79F90B998E2C43220749CED0C555C* ___OjQnvtvXVmEgxqUHpwsCCndFmCp_10;
	// SoMwFHWdfFmaNnPQuSGWlSuWHDi lUbzJtkApcIkohMrHhJAmcDImRRR::xbSkPOtNiebOSCgAhOovPqRqXwvm
	SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6 * ___xbSkPOtNiebOSCgAhOovPqRqXwvm_11;
	// iNmAhOSEGUcqVFxmGFasdFHfdnr lUbzJtkApcIkohMrHhJAmcDImRRR::IyEdcVHkuDkafEWHmxaqzzZeFqNW
	iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C * ___IyEdcVHkuDkafEWHmxaqzzZeFqNW_12;
	// iNmAhOSEGUcqVFxmGFasdFHfdnr[] lUbzJtkApcIkohMrHhJAmcDImRRR::HaAKNEMDBYyMhrIoUCHGzFFyJiI
	iNmAhOSEGUcqVFxmGFasdFHfdnrU5BU5D_tBC2F37DBA40E9A0ADD343BF2EB391385C7D5FA69* ___HaAKNEMDBYyMhrIoUCHGzFFyJiI_13;
	// AhZETLxKRDTxPbFmIsnmalofYne<Rewired.ActiveControllerChangedDelegate> lUbzJtkApcIkohMrHhJAmcDImRRR::xkHAaTRrzATFXVORsdQcGVcAyKu
	AhZETLxKRDTxPbFmIsnmalofYne_tD42028288CF7D973DC8FE62C4DC9B2FC5D81FF1B * ___xkHAaTRrzATFXVORsdQcGVcAyKu_14;
	// AhZETLxKRDTxPbFmIsnmalofYne<Rewired.PlayerActiveControllerChangedDelegate> lUbzJtkApcIkohMrHhJAmcDImRRR::pVgKJMsbUYqDnPgywSbnsKwnJSU
	AhZETLxKRDTxPbFmIsnmalofYne_t8042A016871DA80AEAADC294117467652BDAA49B * ___pVgKJMsbUYqDnPgywSbnsKwnJSU_15;
	// AhZETLxKRDTxPbFmIsnmalofYne<Rewired.PlayerActiveControllerChangedDelegate>[] lUbzJtkApcIkohMrHhJAmcDImRRR::baGZLwAXmBfCREgsimWQSbLkIpIj
	AhZETLxKRDTxPbFmIsnmalofYneU5BU5D_t6C46DF09F6A0C2E4EC91CA0DD52639756080E32A* ___baGZLwAXmBfCREgsimWQSbLkIpIj_16;
	// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,lUbzJtkApcIkohMrHhJAmcDImRRR_JLPbgmCslxcSncPKJybubLvESakA> lUbzJtkApcIkohMrHhJAmcDImRRR::vwyzzodFvzaJuDLLaAMUycIZhjD
	ADictionary_2_tBF46E6C105A19DB0AC69B62AF4603EA0D774ECAB * ___vwyzzodFvzaJuDLLaAMUycIZhjD_17;
	// rIcvLcWCuQghgaaiofvKjSvndJX lUbzJtkApcIkohMrHhJAmcDImRRR::stygjDDRGdOuLuSiDJenZijkpLMI
	rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * ___stygjDDRGdOuLuSiDJenZijkpLMI_18;
	// System.Collections.Generic.IList`1<Rewired.Joystick> lUbzJtkApcIkohMrHhJAmcDImRRR::YiuZEsEepQwFmUNRStSPyEzuABi
	RuntimeObject* ___YiuZEsEepQwFmUNRStSPyEzuABi_19;
	// System.Collections.Generic.IList`1<Rewired.CustomController> lUbzJtkApcIkohMrHhJAmcDImRRR::CbqXRwUReIvUcKUTjbTmSBpjpzO
	RuntimeObject* ___CbqXRwUReIvUcKUTjbTmSBpjpzO_20;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR::plOMmYwRyscVwBsKeVnZUOnVRLL
	int32_t ___plOMmYwRyscVwBsKeVnZUOnVRLL_21;
	// System.Boolean lUbzJtkApcIkohMrHhJAmcDImRRR::uFBUjqesoWEDylgvLnqGzSXhYov
	bool ___uFBUjqesoWEDylgvLnqGzSXhYov_22;
	// System.Boolean lUbzJtkApcIkohMrHhJAmcDImRRR::xUzeyjlHpwGUxEwdPtENmOvsKbtN
	bool ___xUzeyjlHpwGUxEwdPtENmOvsKbtN_23;
	// System.Boolean lUbzJtkApcIkohMrHhJAmcDImRRR::jrhqyYlRWCcRxAwNnKcXJyaAtcA
	bool ___jrhqyYlRWCcRxAwNnKcXJyaAtcA_24;
	// Rewired.Interfaces.IUnifiedKeyboardSource lUbzJtkApcIkohMrHhJAmcDImRRR::tpzVLnfsmVSJSCcIXipLHRtetoAv
	RuntimeObject* ___tpzVLnfsmVSJSCcIXipLHRtetoAv_25;
	// Rewired.Interfaces.IUnifiedMouseSource lUbzJtkApcIkohMrHhJAmcDImRRR::OdOCHXeXDFufODzdKwToFSkQCsc
	RuntimeObject* ___OdOCHXeXDFufODzdKwToFSkQCsc_26;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR::KisfUtAbFzkOvdyrnDCIAPLyGpMK
	int32_t ___KisfUtAbFzkOvdyrnDCIAPLyGpMK_27;
	// nkGLVKXmQVVuJNEogVVmAwwQOJl lUbzJtkApcIkohMrHhJAmcDImRRR::IEqlUZsiLAwwHwjaJiWawGshrHQ
	nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22 * ___IEqlUZsiLAwwHwjaJiWawGshrHQ_28;
	// LkmsImmStVEvsBvZKxyBsAPXXJ lUbzJtkApcIkohMrHhJAmcDImRRR::fsKUnkudBKaclGoZoLGqLplnpcq
	LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA * ___fsKUnkudBKaclGoZoLGqLplnpcq_29;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR::IREyufqHKXHZPEQHPEAqenDEAUFv
	int32_t ___IREyufqHKXHZPEQHPEAqenDEAUFv_30;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR::kSRoFZFcwKfkpCQvLgASZpiKDjWp
	int32_t ___kSRoFZFcwKfkpCQvLgASZpiKDjWp_31;
	// System.Action`2<System.Int32,Rewired.ControllerDataUpdater> lUbzJtkApcIkohMrHhJAmcDImRRR::bbAisqwPdLehArffZbUYobsOVJk
	Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A * ___bbAisqwPdLehArffZbUYobsOVJk_32;
	// System.Action`3<System.Boolean,System.Int32,System.Int32> lUbzJtkApcIkohMrHhJAmcDImRRR::dKOMDeTFhcAGvhGXFvxwMQTzTpa
	Action_3_tCC0C451189B08E60B355D3B8F0030B4E812C0A1D * ___dKOMDeTFhcAGvhGXFvxwMQTzTpa_33;
	// System.Action`1<Rewired.ControllerStatusChangedEventArgs> lUbzJtkApcIkohMrHhJAmcDImRRR::kxPEmGNYGoPtVtPrSMMzAAUWENaG
	Action_1_t42630A9FFF8BE0214466A318ED987BB54B016B80 * ___kxPEmGNYGoPtVtPrSMMzAAUWENaG_34;
	// System.Action`2<Rewired.ControllerType,System.Int32> lUbzJtkApcIkohMrHhJAmcDImRRR::pvjIVuFuziOtBNaRGTkkFsOlRDp
	Action_2_t11BE1B5AD80237AA8DDBE0EF6610F8369C77941E * ___pvjIVuFuziOtBNaRGTkkFsOlRDp_35;
	// System.Boolean lUbzJtkApcIkohMrHhJAmcDImRRR::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_36;

public:
	inline static int32_t get_offset_of_QgoIWtHsaMGVeBFhqUvbmSmsiRh_0() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___QgoIWtHsaMGVeBFhqUvbmSmsiRh_0)); }
	inline List_1_tA72AFBAA71AD07F0E9CDE70C7F4744366B2E2D8B * get_QgoIWtHsaMGVeBFhqUvbmSmsiRh_0() const { return ___QgoIWtHsaMGVeBFhqUvbmSmsiRh_0; }
	inline List_1_tA72AFBAA71AD07F0E9CDE70C7F4744366B2E2D8B ** get_address_of_QgoIWtHsaMGVeBFhqUvbmSmsiRh_0() { return &___QgoIWtHsaMGVeBFhqUvbmSmsiRh_0; }
	inline void set_QgoIWtHsaMGVeBFhqUvbmSmsiRh_0(List_1_tA72AFBAA71AD07F0E9CDE70C7F4744366B2E2D8B * value)
	{
		___QgoIWtHsaMGVeBFhqUvbmSmsiRh_0 = value;
		Il2CppCodeGenWriteBarrier((&___QgoIWtHsaMGVeBFhqUvbmSmsiRh_0), value);
	}

	inline static int32_t get_offset_of_rHKCUSVpCkqbhfZvJGgsrHqdAPs_1() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___rHKCUSVpCkqbhfZvJGgsrHqdAPs_1)); }
	inline List_1_tA72AFBAA71AD07F0E9CDE70C7F4744366B2E2D8B * get_rHKCUSVpCkqbhfZvJGgsrHqdAPs_1() const { return ___rHKCUSVpCkqbhfZvJGgsrHqdAPs_1; }
	inline List_1_tA72AFBAA71AD07F0E9CDE70C7F4744366B2E2D8B ** get_address_of_rHKCUSVpCkqbhfZvJGgsrHqdAPs_1() { return &___rHKCUSVpCkqbhfZvJGgsrHqdAPs_1; }
	inline void set_rHKCUSVpCkqbhfZvJGgsrHqdAPs_1(List_1_tA72AFBAA71AD07F0E9CDE70C7F4744366B2E2D8B * value)
	{
		___rHKCUSVpCkqbhfZvJGgsrHqdAPs_1 = value;
		Il2CppCodeGenWriteBarrier((&___rHKCUSVpCkqbhfZvJGgsrHqdAPs_1), value);
	}

	inline static int32_t get_offset_of_dKRJCZQhWFihhgHVLBWkTcBEUEZ_2() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___dKRJCZQhWFihhgHVLBWkTcBEUEZ_2)); }
	inline List_1_tB037A3809838545DA54CE039436BC1BF87578A9B * get_dKRJCZQhWFihhgHVLBWkTcBEUEZ_2() const { return ___dKRJCZQhWFihhgHVLBWkTcBEUEZ_2; }
	inline List_1_tB037A3809838545DA54CE039436BC1BF87578A9B ** get_address_of_dKRJCZQhWFihhgHVLBWkTcBEUEZ_2() { return &___dKRJCZQhWFihhgHVLBWkTcBEUEZ_2; }
	inline void set_dKRJCZQhWFihhgHVLBWkTcBEUEZ_2(List_1_tB037A3809838545DA54CE039436BC1BF87578A9B * value)
	{
		___dKRJCZQhWFihhgHVLBWkTcBEUEZ_2 = value;
		Il2CppCodeGenWriteBarrier((&___dKRJCZQhWFihhgHVLBWkTcBEUEZ_2), value);
	}

	inline static int32_t get_offset_of_tvaElgXLUKAHFDoRhflnYxhnyPI_3() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___tvaElgXLUKAHFDoRhflnYxhnyPI_3)); }
	inline List_1_t20C185E0480AE6CA25B3B6AD936AA34CDE1EC518 * get_tvaElgXLUKAHFDoRhflnYxhnyPI_3() const { return ___tvaElgXLUKAHFDoRhflnYxhnyPI_3; }
	inline List_1_t20C185E0480AE6CA25B3B6AD936AA34CDE1EC518 ** get_address_of_tvaElgXLUKAHFDoRhflnYxhnyPI_3() { return &___tvaElgXLUKAHFDoRhflnYxhnyPI_3; }
	inline void set_tvaElgXLUKAHFDoRhflnYxhnyPI_3(List_1_t20C185E0480AE6CA25B3B6AD936AA34CDE1EC518 * value)
	{
		___tvaElgXLUKAHFDoRhflnYxhnyPI_3 = value;
		Il2CppCodeGenWriteBarrier((&___tvaElgXLUKAHFDoRhflnYxhnyPI_3), value);
	}

	inline static int32_t get_offset_of_bemErvveeaCaSZFVNunRQfuZPez_4() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___bemErvveeaCaSZFVNunRQfuZPez_4)); }
	inline ReadOnlyCollection_1_t037BB93EEC8E496E6B7901A805B53290BC747353 * get_bemErvveeaCaSZFVNunRQfuZPez_4() const { return ___bemErvveeaCaSZFVNunRQfuZPez_4; }
	inline ReadOnlyCollection_1_t037BB93EEC8E496E6B7901A805B53290BC747353 ** get_address_of_bemErvveeaCaSZFVNunRQfuZPez_4() { return &___bemErvveeaCaSZFVNunRQfuZPez_4; }
	inline void set_bemErvveeaCaSZFVNunRQfuZPez_4(ReadOnlyCollection_1_t037BB93EEC8E496E6B7901A805B53290BC747353 * value)
	{
		___bemErvveeaCaSZFVNunRQfuZPez_4 = value;
		Il2CppCodeGenWriteBarrier((&___bemErvveeaCaSZFVNunRQfuZPez_4), value);
	}

	inline static int32_t get_offset_of_OSuLPPJEiNhXcAilTdfZMoZXfznt_5() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___OSuLPPJEiNhXcAilTdfZMoZXfznt_5)); }
	inline Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * get_OSuLPPJEiNhXcAilTdfZMoZXfznt_5() const { return ___OSuLPPJEiNhXcAilTdfZMoZXfznt_5; }
	inline Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 ** get_address_of_OSuLPPJEiNhXcAilTdfZMoZXfznt_5() { return &___OSuLPPJEiNhXcAilTdfZMoZXfznt_5; }
	inline void set_OSuLPPJEiNhXcAilTdfZMoZXfznt_5(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * value)
	{
		___OSuLPPJEiNhXcAilTdfZMoZXfznt_5 = value;
		Il2CppCodeGenWriteBarrier((&___OSuLPPJEiNhXcAilTdfZMoZXfznt_5), value);
	}

	inline static int32_t get_offset_of_vcFHuVHhWhJcpPiVLFZqonrrcV_6() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___vcFHuVHhWhJcpPiVLFZqonrrcV_6)); }
	inline Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3 * get_vcFHuVHhWhJcpPiVLFZqonrrcV_6() const { return ___vcFHuVHhWhJcpPiVLFZqonrrcV_6; }
	inline Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3 ** get_address_of_vcFHuVHhWhJcpPiVLFZqonrrcV_6() { return &___vcFHuVHhWhJcpPiVLFZqonrrcV_6; }
	inline void set_vcFHuVHhWhJcpPiVLFZqonrrcV_6(Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3 * value)
	{
		___vcFHuVHhWhJcpPiVLFZqonrrcV_6 = value;
		Il2CppCodeGenWriteBarrier((&___vcFHuVHhWhJcpPiVLFZqonrrcV_6), value);
	}

	inline static int32_t get_offset_of_bsdXVIEsRZlaTBlTrOAhsApnYKn_7() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___bsdXVIEsRZlaTBlTrOAhsApnYKn_7)); }
	inline ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * get_bsdXVIEsRZlaTBlTrOAhsApnYKn_7() const { return ___bsdXVIEsRZlaTBlTrOAhsApnYKn_7; }
	inline ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 ** get_address_of_bsdXVIEsRZlaTBlTrOAhsApnYKn_7() { return &___bsdXVIEsRZlaTBlTrOAhsApnYKn_7; }
	inline void set_bsdXVIEsRZlaTBlTrOAhsApnYKn_7(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * value)
	{
		___bsdXVIEsRZlaTBlTrOAhsApnYKn_7 = value;
		Il2CppCodeGenWriteBarrier((&___bsdXVIEsRZlaTBlTrOAhsApnYKn_7), value);
	}

	inline static int32_t get_offset_of_mCOQtFensPUoYxPTcAQQdFgYYSo_8() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___mCOQtFensPUoYxPTcAQQdFgYYSo_8)); }
	inline OmePuLXqingOShAfdEjGZVdOnDpGU5BU5D_t6371EF4B41790887302C0A070D79235FCFE476D2* get_mCOQtFensPUoYxPTcAQQdFgYYSo_8() const { return ___mCOQtFensPUoYxPTcAQQdFgYYSo_8; }
	inline OmePuLXqingOShAfdEjGZVdOnDpGU5BU5D_t6371EF4B41790887302C0A070D79235FCFE476D2** get_address_of_mCOQtFensPUoYxPTcAQQdFgYYSo_8() { return &___mCOQtFensPUoYxPTcAQQdFgYYSo_8; }
	inline void set_mCOQtFensPUoYxPTcAQQdFgYYSo_8(OmePuLXqingOShAfdEjGZVdOnDpGU5BU5D_t6371EF4B41790887302C0A070D79235FCFE476D2* value)
	{
		___mCOQtFensPUoYxPTcAQQdFgYYSo_8 = value;
		Il2CppCodeGenWriteBarrier((&___mCOQtFensPUoYxPTcAQQdFgYYSo_8), value);
	}

	inline static int32_t get_offset_of_zaBpOeSvCieYmNRzYvTnztQTkri_9() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___zaBpOeSvCieYmNRzYvTnztQTkri_9)); }
	inline OmePuLXqingOShAfdEjGZVdOnDpGU5BU5D_t6371EF4B41790887302C0A070D79235FCFE476D2* get_zaBpOeSvCieYmNRzYvTnztQTkri_9() const { return ___zaBpOeSvCieYmNRzYvTnztQTkri_9; }
	inline OmePuLXqingOShAfdEjGZVdOnDpGU5BU5D_t6371EF4B41790887302C0A070D79235FCFE476D2** get_address_of_zaBpOeSvCieYmNRzYvTnztQTkri_9() { return &___zaBpOeSvCieYmNRzYvTnztQTkri_9; }
	inline void set_zaBpOeSvCieYmNRzYvTnztQTkri_9(OmePuLXqingOShAfdEjGZVdOnDpGU5BU5D_t6371EF4B41790887302C0A070D79235FCFE476D2* value)
	{
		___zaBpOeSvCieYmNRzYvTnztQTkri_9 = value;
		Il2CppCodeGenWriteBarrier((&___zaBpOeSvCieYmNRzYvTnztQTkri_9), value);
	}

	inline static int32_t get_offset_of_OjQnvtvXVmEgxqUHpwsCCndFmCp_10() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___OjQnvtvXVmEgxqUHpwsCCndFmCp_10)); }
	inline OmePuLXqingOShAfdEjGZVdOnDpGU5BU2CU5D_t719ED9D943C79F90B998E2C43220749CED0C555C* get_OjQnvtvXVmEgxqUHpwsCCndFmCp_10() const { return ___OjQnvtvXVmEgxqUHpwsCCndFmCp_10; }
	inline OmePuLXqingOShAfdEjGZVdOnDpGU5BU2CU5D_t719ED9D943C79F90B998E2C43220749CED0C555C** get_address_of_OjQnvtvXVmEgxqUHpwsCCndFmCp_10() { return &___OjQnvtvXVmEgxqUHpwsCCndFmCp_10; }
	inline void set_OjQnvtvXVmEgxqUHpwsCCndFmCp_10(OmePuLXqingOShAfdEjGZVdOnDpGU5BU2CU5D_t719ED9D943C79F90B998E2C43220749CED0C555C* value)
	{
		___OjQnvtvXVmEgxqUHpwsCCndFmCp_10 = value;
		Il2CppCodeGenWriteBarrier((&___OjQnvtvXVmEgxqUHpwsCCndFmCp_10), value);
	}

	inline static int32_t get_offset_of_xbSkPOtNiebOSCgAhOovPqRqXwvm_11() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___xbSkPOtNiebOSCgAhOovPqRqXwvm_11)); }
	inline SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6 * get_xbSkPOtNiebOSCgAhOovPqRqXwvm_11() const { return ___xbSkPOtNiebOSCgAhOovPqRqXwvm_11; }
	inline SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6 ** get_address_of_xbSkPOtNiebOSCgAhOovPqRqXwvm_11() { return &___xbSkPOtNiebOSCgAhOovPqRqXwvm_11; }
	inline void set_xbSkPOtNiebOSCgAhOovPqRqXwvm_11(SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6 * value)
	{
		___xbSkPOtNiebOSCgAhOovPqRqXwvm_11 = value;
		Il2CppCodeGenWriteBarrier((&___xbSkPOtNiebOSCgAhOovPqRqXwvm_11), value);
	}

	inline static int32_t get_offset_of_IyEdcVHkuDkafEWHmxaqzzZeFqNW_12() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___IyEdcVHkuDkafEWHmxaqzzZeFqNW_12)); }
	inline iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C * get_IyEdcVHkuDkafEWHmxaqzzZeFqNW_12() const { return ___IyEdcVHkuDkafEWHmxaqzzZeFqNW_12; }
	inline iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C ** get_address_of_IyEdcVHkuDkafEWHmxaqzzZeFqNW_12() { return &___IyEdcVHkuDkafEWHmxaqzzZeFqNW_12; }
	inline void set_IyEdcVHkuDkafEWHmxaqzzZeFqNW_12(iNmAhOSEGUcqVFxmGFasdFHfdnr_tCF49ACFBDFBCA5D508FB9D3E5863BC3807F2518C * value)
	{
		___IyEdcVHkuDkafEWHmxaqzzZeFqNW_12 = value;
		Il2CppCodeGenWriteBarrier((&___IyEdcVHkuDkafEWHmxaqzzZeFqNW_12), value);
	}

	inline static int32_t get_offset_of_HaAKNEMDBYyMhrIoUCHGzFFyJiI_13() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___HaAKNEMDBYyMhrIoUCHGzFFyJiI_13)); }
	inline iNmAhOSEGUcqVFxmGFasdFHfdnrU5BU5D_tBC2F37DBA40E9A0ADD343BF2EB391385C7D5FA69* get_HaAKNEMDBYyMhrIoUCHGzFFyJiI_13() const { return ___HaAKNEMDBYyMhrIoUCHGzFFyJiI_13; }
	inline iNmAhOSEGUcqVFxmGFasdFHfdnrU5BU5D_tBC2F37DBA40E9A0ADD343BF2EB391385C7D5FA69** get_address_of_HaAKNEMDBYyMhrIoUCHGzFFyJiI_13() { return &___HaAKNEMDBYyMhrIoUCHGzFFyJiI_13; }
	inline void set_HaAKNEMDBYyMhrIoUCHGzFFyJiI_13(iNmAhOSEGUcqVFxmGFasdFHfdnrU5BU5D_tBC2F37DBA40E9A0ADD343BF2EB391385C7D5FA69* value)
	{
		___HaAKNEMDBYyMhrIoUCHGzFFyJiI_13 = value;
		Il2CppCodeGenWriteBarrier((&___HaAKNEMDBYyMhrIoUCHGzFFyJiI_13), value);
	}

	inline static int32_t get_offset_of_xkHAaTRrzATFXVORsdQcGVcAyKu_14() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___xkHAaTRrzATFXVORsdQcGVcAyKu_14)); }
	inline AhZETLxKRDTxPbFmIsnmalofYne_tD42028288CF7D973DC8FE62C4DC9B2FC5D81FF1B * get_xkHAaTRrzATFXVORsdQcGVcAyKu_14() const { return ___xkHAaTRrzATFXVORsdQcGVcAyKu_14; }
	inline AhZETLxKRDTxPbFmIsnmalofYne_tD42028288CF7D973DC8FE62C4DC9B2FC5D81FF1B ** get_address_of_xkHAaTRrzATFXVORsdQcGVcAyKu_14() { return &___xkHAaTRrzATFXVORsdQcGVcAyKu_14; }
	inline void set_xkHAaTRrzATFXVORsdQcGVcAyKu_14(AhZETLxKRDTxPbFmIsnmalofYne_tD42028288CF7D973DC8FE62C4DC9B2FC5D81FF1B * value)
	{
		___xkHAaTRrzATFXVORsdQcGVcAyKu_14 = value;
		Il2CppCodeGenWriteBarrier((&___xkHAaTRrzATFXVORsdQcGVcAyKu_14), value);
	}

	inline static int32_t get_offset_of_pVgKJMsbUYqDnPgywSbnsKwnJSU_15() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___pVgKJMsbUYqDnPgywSbnsKwnJSU_15)); }
	inline AhZETLxKRDTxPbFmIsnmalofYne_t8042A016871DA80AEAADC294117467652BDAA49B * get_pVgKJMsbUYqDnPgywSbnsKwnJSU_15() const { return ___pVgKJMsbUYqDnPgywSbnsKwnJSU_15; }
	inline AhZETLxKRDTxPbFmIsnmalofYne_t8042A016871DA80AEAADC294117467652BDAA49B ** get_address_of_pVgKJMsbUYqDnPgywSbnsKwnJSU_15() { return &___pVgKJMsbUYqDnPgywSbnsKwnJSU_15; }
	inline void set_pVgKJMsbUYqDnPgywSbnsKwnJSU_15(AhZETLxKRDTxPbFmIsnmalofYne_t8042A016871DA80AEAADC294117467652BDAA49B * value)
	{
		___pVgKJMsbUYqDnPgywSbnsKwnJSU_15 = value;
		Il2CppCodeGenWriteBarrier((&___pVgKJMsbUYqDnPgywSbnsKwnJSU_15), value);
	}

	inline static int32_t get_offset_of_baGZLwAXmBfCREgsimWQSbLkIpIj_16() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___baGZLwAXmBfCREgsimWQSbLkIpIj_16)); }
	inline AhZETLxKRDTxPbFmIsnmalofYneU5BU5D_t6C46DF09F6A0C2E4EC91CA0DD52639756080E32A* get_baGZLwAXmBfCREgsimWQSbLkIpIj_16() const { return ___baGZLwAXmBfCREgsimWQSbLkIpIj_16; }
	inline AhZETLxKRDTxPbFmIsnmalofYneU5BU5D_t6C46DF09F6A0C2E4EC91CA0DD52639756080E32A** get_address_of_baGZLwAXmBfCREgsimWQSbLkIpIj_16() { return &___baGZLwAXmBfCREgsimWQSbLkIpIj_16; }
	inline void set_baGZLwAXmBfCREgsimWQSbLkIpIj_16(AhZETLxKRDTxPbFmIsnmalofYneU5BU5D_t6C46DF09F6A0C2E4EC91CA0DD52639756080E32A* value)
	{
		___baGZLwAXmBfCREgsimWQSbLkIpIj_16 = value;
		Il2CppCodeGenWriteBarrier((&___baGZLwAXmBfCREgsimWQSbLkIpIj_16), value);
	}

	inline static int32_t get_offset_of_vwyzzodFvzaJuDLLaAMUycIZhjD_17() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___vwyzzodFvzaJuDLLaAMUycIZhjD_17)); }
	inline ADictionary_2_tBF46E6C105A19DB0AC69B62AF4603EA0D774ECAB * get_vwyzzodFvzaJuDLLaAMUycIZhjD_17() const { return ___vwyzzodFvzaJuDLLaAMUycIZhjD_17; }
	inline ADictionary_2_tBF46E6C105A19DB0AC69B62AF4603EA0D774ECAB ** get_address_of_vwyzzodFvzaJuDLLaAMUycIZhjD_17() { return &___vwyzzodFvzaJuDLLaAMUycIZhjD_17; }
	inline void set_vwyzzodFvzaJuDLLaAMUycIZhjD_17(ADictionary_2_tBF46E6C105A19DB0AC69B62AF4603EA0D774ECAB * value)
	{
		___vwyzzodFvzaJuDLLaAMUycIZhjD_17 = value;
		Il2CppCodeGenWriteBarrier((&___vwyzzodFvzaJuDLLaAMUycIZhjD_17), value);
	}

	inline static int32_t get_offset_of_stygjDDRGdOuLuSiDJenZijkpLMI_18() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___stygjDDRGdOuLuSiDJenZijkpLMI_18)); }
	inline rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * get_stygjDDRGdOuLuSiDJenZijkpLMI_18() const { return ___stygjDDRGdOuLuSiDJenZijkpLMI_18; }
	inline rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 ** get_address_of_stygjDDRGdOuLuSiDJenZijkpLMI_18() { return &___stygjDDRGdOuLuSiDJenZijkpLMI_18; }
	inline void set_stygjDDRGdOuLuSiDJenZijkpLMI_18(rIcvLcWCuQghgaaiofvKjSvndJX_t194A11E798E6C6497FA7FF7267CD63127C961224 * value)
	{
		___stygjDDRGdOuLuSiDJenZijkpLMI_18 = value;
		Il2CppCodeGenWriteBarrier((&___stygjDDRGdOuLuSiDJenZijkpLMI_18), value);
	}

	inline static int32_t get_offset_of_YiuZEsEepQwFmUNRStSPyEzuABi_19() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___YiuZEsEepQwFmUNRStSPyEzuABi_19)); }
	inline RuntimeObject* get_YiuZEsEepQwFmUNRStSPyEzuABi_19() const { return ___YiuZEsEepQwFmUNRStSPyEzuABi_19; }
	inline RuntimeObject** get_address_of_YiuZEsEepQwFmUNRStSPyEzuABi_19() { return &___YiuZEsEepQwFmUNRStSPyEzuABi_19; }
	inline void set_YiuZEsEepQwFmUNRStSPyEzuABi_19(RuntimeObject* value)
	{
		___YiuZEsEepQwFmUNRStSPyEzuABi_19 = value;
		Il2CppCodeGenWriteBarrier((&___YiuZEsEepQwFmUNRStSPyEzuABi_19), value);
	}

	inline static int32_t get_offset_of_CbqXRwUReIvUcKUTjbTmSBpjpzO_20() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___CbqXRwUReIvUcKUTjbTmSBpjpzO_20)); }
	inline RuntimeObject* get_CbqXRwUReIvUcKUTjbTmSBpjpzO_20() const { return ___CbqXRwUReIvUcKUTjbTmSBpjpzO_20; }
	inline RuntimeObject** get_address_of_CbqXRwUReIvUcKUTjbTmSBpjpzO_20() { return &___CbqXRwUReIvUcKUTjbTmSBpjpzO_20; }
	inline void set_CbqXRwUReIvUcKUTjbTmSBpjpzO_20(RuntimeObject* value)
	{
		___CbqXRwUReIvUcKUTjbTmSBpjpzO_20 = value;
		Il2CppCodeGenWriteBarrier((&___CbqXRwUReIvUcKUTjbTmSBpjpzO_20), value);
	}

	inline static int32_t get_offset_of_plOMmYwRyscVwBsKeVnZUOnVRLL_21() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___plOMmYwRyscVwBsKeVnZUOnVRLL_21)); }
	inline int32_t get_plOMmYwRyscVwBsKeVnZUOnVRLL_21() const { return ___plOMmYwRyscVwBsKeVnZUOnVRLL_21; }
	inline int32_t* get_address_of_plOMmYwRyscVwBsKeVnZUOnVRLL_21() { return &___plOMmYwRyscVwBsKeVnZUOnVRLL_21; }
	inline void set_plOMmYwRyscVwBsKeVnZUOnVRLL_21(int32_t value)
	{
		___plOMmYwRyscVwBsKeVnZUOnVRLL_21 = value;
	}

	inline static int32_t get_offset_of_uFBUjqesoWEDylgvLnqGzSXhYov_22() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___uFBUjqesoWEDylgvLnqGzSXhYov_22)); }
	inline bool get_uFBUjqesoWEDylgvLnqGzSXhYov_22() const { return ___uFBUjqesoWEDylgvLnqGzSXhYov_22; }
	inline bool* get_address_of_uFBUjqesoWEDylgvLnqGzSXhYov_22() { return &___uFBUjqesoWEDylgvLnqGzSXhYov_22; }
	inline void set_uFBUjqesoWEDylgvLnqGzSXhYov_22(bool value)
	{
		___uFBUjqesoWEDylgvLnqGzSXhYov_22 = value;
	}

	inline static int32_t get_offset_of_xUzeyjlHpwGUxEwdPtENmOvsKbtN_23() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___xUzeyjlHpwGUxEwdPtENmOvsKbtN_23)); }
	inline bool get_xUzeyjlHpwGUxEwdPtENmOvsKbtN_23() const { return ___xUzeyjlHpwGUxEwdPtENmOvsKbtN_23; }
	inline bool* get_address_of_xUzeyjlHpwGUxEwdPtENmOvsKbtN_23() { return &___xUzeyjlHpwGUxEwdPtENmOvsKbtN_23; }
	inline void set_xUzeyjlHpwGUxEwdPtENmOvsKbtN_23(bool value)
	{
		___xUzeyjlHpwGUxEwdPtENmOvsKbtN_23 = value;
	}

	inline static int32_t get_offset_of_jrhqyYlRWCcRxAwNnKcXJyaAtcA_24() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___jrhqyYlRWCcRxAwNnKcXJyaAtcA_24)); }
	inline bool get_jrhqyYlRWCcRxAwNnKcXJyaAtcA_24() const { return ___jrhqyYlRWCcRxAwNnKcXJyaAtcA_24; }
	inline bool* get_address_of_jrhqyYlRWCcRxAwNnKcXJyaAtcA_24() { return &___jrhqyYlRWCcRxAwNnKcXJyaAtcA_24; }
	inline void set_jrhqyYlRWCcRxAwNnKcXJyaAtcA_24(bool value)
	{
		___jrhqyYlRWCcRxAwNnKcXJyaAtcA_24 = value;
	}

	inline static int32_t get_offset_of_tpzVLnfsmVSJSCcIXipLHRtetoAv_25() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___tpzVLnfsmVSJSCcIXipLHRtetoAv_25)); }
	inline RuntimeObject* get_tpzVLnfsmVSJSCcIXipLHRtetoAv_25() const { return ___tpzVLnfsmVSJSCcIXipLHRtetoAv_25; }
	inline RuntimeObject** get_address_of_tpzVLnfsmVSJSCcIXipLHRtetoAv_25() { return &___tpzVLnfsmVSJSCcIXipLHRtetoAv_25; }
	inline void set_tpzVLnfsmVSJSCcIXipLHRtetoAv_25(RuntimeObject* value)
	{
		___tpzVLnfsmVSJSCcIXipLHRtetoAv_25 = value;
		Il2CppCodeGenWriteBarrier((&___tpzVLnfsmVSJSCcIXipLHRtetoAv_25), value);
	}

	inline static int32_t get_offset_of_OdOCHXeXDFufODzdKwToFSkQCsc_26() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___OdOCHXeXDFufODzdKwToFSkQCsc_26)); }
	inline RuntimeObject* get_OdOCHXeXDFufODzdKwToFSkQCsc_26() const { return ___OdOCHXeXDFufODzdKwToFSkQCsc_26; }
	inline RuntimeObject** get_address_of_OdOCHXeXDFufODzdKwToFSkQCsc_26() { return &___OdOCHXeXDFufODzdKwToFSkQCsc_26; }
	inline void set_OdOCHXeXDFufODzdKwToFSkQCsc_26(RuntimeObject* value)
	{
		___OdOCHXeXDFufODzdKwToFSkQCsc_26 = value;
		Il2CppCodeGenWriteBarrier((&___OdOCHXeXDFufODzdKwToFSkQCsc_26), value);
	}

	inline static int32_t get_offset_of_KisfUtAbFzkOvdyrnDCIAPLyGpMK_27() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___KisfUtAbFzkOvdyrnDCIAPLyGpMK_27)); }
	inline int32_t get_KisfUtAbFzkOvdyrnDCIAPLyGpMK_27() const { return ___KisfUtAbFzkOvdyrnDCIAPLyGpMK_27; }
	inline int32_t* get_address_of_KisfUtAbFzkOvdyrnDCIAPLyGpMK_27() { return &___KisfUtAbFzkOvdyrnDCIAPLyGpMK_27; }
	inline void set_KisfUtAbFzkOvdyrnDCIAPLyGpMK_27(int32_t value)
	{
		___KisfUtAbFzkOvdyrnDCIAPLyGpMK_27 = value;
	}

	inline static int32_t get_offset_of_IEqlUZsiLAwwHwjaJiWawGshrHQ_28() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___IEqlUZsiLAwwHwjaJiWawGshrHQ_28)); }
	inline nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22 * get_IEqlUZsiLAwwHwjaJiWawGshrHQ_28() const { return ___IEqlUZsiLAwwHwjaJiWawGshrHQ_28; }
	inline nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22 ** get_address_of_IEqlUZsiLAwwHwjaJiWawGshrHQ_28() { return &___IEqlUZsiLAwwHwjaJiWawGshrHQ_28; }
	inline void set_IEqlUZsiLAwwHwjaJiWawGshrHQ_28(nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22 * value)
	{
		___IEqlUZsiLAwwHwjaJiWawGshrHQ_28 = value;
		Il2CppCodeGenWriteBarrier((&___IEqlUZsiLAwwHwjaJiWawGshrHQ_28), value);
	}

	inline static int32_t get_offset_of_fsKUnkudBKaclGoZoLGqLplnpcq_29() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___fsKUnkudBKaclGoZoLGqLplnpcq_29)); }
	inline LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA * get_fsKUnkudBKaclGoZoLGqLplnpcq_29() const { return ___fsKUnkudBKaclGoZoLGqLplnpcq_29; }
	inline LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA ** get_address_of_fsKUnkudBKaclGoZoLGqLplnpcq_29() { return &___fsKUnkudBKaclGoZoLGqLplnpcq_29; }
	inline void set_fsKUnkudBKaclGoZoLGqLplnpcq_29(LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA * value)
	{
		___fsKUnkudBKaclGoZoLGqLplnpcq_29 = value;
		Il2CppCodeGenWriteBarrier((&___fsKUnkudBKaclGoZoLGqLplnpcq_29), value);
	}

	inline static int32_t get_offset_of_IREyufqHKXHZPEQHPEAqenDEAUFv_30() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___IREyufqHKXHZPEQHPEAqenDEAUFv_30)); }
	inline int32_t get_IREyufqHKXHZPEQHPEAqenDEAUFv_30() const { return ___IREyufqHKXHZPEQHPEAqenDEAUFv_30; }
	inline int32_t* get_address_of_IREyufqHKXHZPEQHPEAqenDEAUFv_30() { return &___IREyufqHKXHZPEQHPEAqenDEAUFv_30; }
	inline void set_IREyufqHKXHZPEQHPEAqenDEAUFv_30(int32_t value)
	{
		___IREyufqHKXHZPEQHPEAqenDEAUFv_30 = value;
	}

	inline static int32_t get_offset_of_kSRoFZFcwKfkpCQvLgASZpiKDjWp_31() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___kSRoFZFcwKfkpCQvLgASZpiKDjWp_31)); }
	inline int32_t get_kSRoFZFcwKfkpCQvLgASZpiKDjWp_31() const { return ___kSRoFZFcwKfkpCQvLgASZpiKDjWp_31; }
	inline int32_t* get_address_of_kSRoFZFcwKfkpCQvLgASZpiKDjWp_31() { return &___kSRoFZFcwKfkpCQvLgASZpiKDjWp_31; }
	inline void set_kSRoFZFcwKfkpCQvLgASZpiKDjWp_31(int32_t value)
	{
		___kSRoFZFcwKfkpCQvLgASZpiKDjWp_31 = value;
	}

	inline static int32_t get_offset_of_bbAisqwPdLehArffZbUYobsOVJk_32() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___bbAisqwPdLehArffZbUYobsOVJk_32)); }
	inline Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A * get_bbAisqwPdLehArffZbUYobsOVJk_32() const { return ___bbAisqwPdLehArffZbUYobsOVJk_32; }
	inline Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A ** get_address_of_bbAisqwPdLehArffZbUYobsOVJk_32() { return &___bbAisqwPdLehArffZbUYobsOVJk_32; }
	inline void set_bbAisqwPdLehArffZbUYobsOVJk_32(Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A * value)
	{
		___bbAisqwPdLehArffZbUYobsOVJk_32 = value;
		Il2CppCodeGenWriteBarrier((&___bbAisqwPdLehArffZbUYobsOVJk_32), value);
	}

	inline static int32_t get_offset_of_dKOMDeTFhcAGvhGXFvxwMQTzTpa_33() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___dKOMDeTFhcAGvhGXFvxwMQTzTpa_33)); }
	inline Action_3_tCC0C451189B08E60B355D3B8F0030B4E812C0A1D * get_dKOMDeTFhcAGvhGXFvxwMQTzTpa_33() const { return ___dKOMDeTFhcAGvhGXFvxwMQTzTpa_33; }
	inline Action_3_tCC0C451189B08E60B355D3B8F0030B4E812C0A1D ** get_address_of_dKOMDeTFhcAGvhGXFvxwMQTzTpa_33() { return &___dKOMDeTFhcAGvhGXFvxwMQTzTpa_33; }
	inline void set_dKOMDeTFhcAGvhGXFvxwMQTzTpa_33(Action_3_tCC0C451189B08E60B355D3B8F0030B4E812C0A1D * value)
	{
		___dKOMDeTFhcAGvhGXFvxwMQTzTpa_33 = value;
		Il2CppCodeGenWriteBarrier((&___dKOMDeTFhcAGvhGXFvxwMQTzTpa_33), value);
	}

	inline static int32_t get_offset_of_kxPEmGNYGoPtVtPrSMMzAAUWENaG_34() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___kxPEmGNYGoPtVtPrSMMzAAUWENaG_34)); }
	inline Action_1_t42630A9FFF8BE0214466A318ED987BB54B016B80 * get_kxPEmGNYGoPtVtPrSMMzAAUWENaG_34() const { return ___kxPEmGNYGoPtVtPrSMMzAAUWENaG_34; }
	inline Action_1_t42630A9FFF8BE0214466A318ED987BB54B016B80 ** get_address_of_kxPEmGNYGoPtVtPrSMMzAAUWENaG_34() { return &___kxPEmGNYGoPtVtPrSMMzAAUWENaG_34; }
	inline void set_kxPEmGNYGoPtVtPrSMMzAAUWENaG_34(Action_1_t42630A9FFF8BE0214466A318ED987BB54B016B80 * value)
	{
		___kxPEmGNYGoPtVtPrSMMzAAUWENaG_34 = value;
		Il2CppCodeGenWriteBarrier((&___kxPEmGNYGoPtVtPrSMMzAAUWENaG_34), value);
	}

	inline static int32_t get_offset_of_pvjIVuFuziOtBNaRGTkkFsOlRDp_35() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___pvjIVuFuziOtBNaRGTkkFsOlRDp_35)); }
	inline Action_2_t11BE1B5AD80237AA8DDBE0EF6610F8369C77941E * get_pvjIVuFuziOtBNaRGTkkFsOlRDp_35() const { return ___pvjIVuFuziOtBNaRGTkkFsOlRDp_35; }
	inline Action_2_t11BE1B5AD80237AA8DDBE0EF6610F8369C77941E ** get_address_of_pvjIVuFuziOtBNaRGTkkFsOlRDp_35() { return &___pvjIVuFuziOtBNaRGTkkFsOlRDp_35; }
	inline void set_pvjIVuFuziOtBNaRGTkkFsOlRDp_35(Action_2_t11BE1B5AD80237AA8DDBE0EF6610F8369C77941E * value)
	{
		___pvjIVuFuziOtBNaRGTkkFsOlRDp_35 = value;
		Il2CppCodeGenWriteBarrier((&___pvjIVuFuziOtBNaRGTkkFsOlRDp_35), value);
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_36() { return static_cast<int32_t>(offsetof(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE, ___SeCUoinDywZmqZDHRKupOdOaTke_36)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_36() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_36; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_36() { return &___SeCUoinDywZmqZDHRKupOdOaTke_36; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_36(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUBZJTKAPCIKOHMRHHJAMCDIMRRR_TD9D698C83E139CBDDAEC47D478281011C3E60EEE_H
#ifndef JLPBGMCSLXCSNCPKJYBUBLVESAKA_TD2EB3276EC7B244F65D32F176710DDA90AEA3CC7_H
#define JLPBGMCSLXCSNCPKJYBUBLVESAKA_TD2EB3276EC7B244F65D32F176710DDA90AEA3CC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// lUbzJtkApcIkohMrHhJAmcDImRRR_JLPbgmCslxcSncPKJybubLvESakA
struct  JLPbgmCslxcSncPKJybubLvESakA_tD2EB3276EC7B244F65D32F176710DDA90AEA3CC7  : public RuntimeObject
{
public:
	// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,Rewired.InputBehavior> lUbzJtkApcIkohMrHhJAmcDImRRR_JLPbgmCslxcSncPKJybubLvESakA::mpQsQGNdoKAwJwXPzsVPtDdrMBq
	ADictionary_2_t5D2F9858F224DDD739AF42F21ABDAD714A682FA2 * ___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0;
	// System.Collections.Generic.List`1<Rewired.InputBehavior> lUbzJtkApcIkohMrHhJAmcDImRRR_JLPbgmCslxcSncPKJybubLvESakA::UUQMotrrrdlXgpwOmqCtjHqhvxY
	List_1_t00493746A9C09D2A4E213DD073FD95F9A4FAA144 * ___UUQMotrrrdlXgpwOmqCtjHqhvxY_1;
	// System.Collections.Generic.IList`1<Rewired.InputBehavior> lUbzJtkApcIkohMrHhJAmcDImRRR_JLPbgmCslxcSncPKJybubLvESakA::DDVHsbCpPQKwnQeEtRpjMMeFxLoN
	RuntimeObject* ___DDVHsbCpPQKwnQeEtRpjMMeFxLoN_2;

public:
	inline static int32_t get_offset_of_mpQsQGNdoKAwJwXPzsVPtDdrMBq_0() { return static_cast<int32_t>(offsetof(JLPbgmCslxcSncPKJybubLvESakA_tD2EB3276EC7B244F65D32F176710DDA90AEA3CC7, ___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0)); }
	inline ADictionary_2_t5D2F9858F224DDD739AF42F21ABDAD714A682FA2 * get_mpQsQGNdoKAwJwXPzsVPtDdrMBq_0() const { return ___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0; }
	inline ADictionary_2_t5D2F9858F224DDD739AF42F21ABDAD714A682FA2 ** get_address_of_mpQsQGNdoKAwJwXPzsVPtDdrMBq_0() { return &___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0; }
	inline void set_mpQsQGNdoKAwJwXPzsVPtDdrMBq_0(ADictionary_2_t5D2F9858F224DDD739AF42F21ABDAD714A682FA2 * value)
	{
		___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0 = value;
		Il2CppCodeGenWriteBarrier((&___mpQsQGNdoKAwJwXPzsVPtDdrMBq_0), value);
	}

	inline static int32_t get_offset_of_UUQMotrrrdlXgpwOmqCtjHqhvxY_1() { return static_cast<int32_t>(offsetof(JLPbgmCslxcSncPKJybubLvESakA_tD2EB3276EC7B244F65D32F176710DDA90AEA3CC7, ___UUQMotrrrdlXgpwOmqCtjHqhvxY_1)); }
	inline List_1_t00493746A9C09D2A4E213DD073FD95F9A4FAA144 * get_UUQMotrrrdlXgpwOmqCtjHqhvxY_1() const { return ___UUQMotrrrdlXgpwOmqCtjHqhvxY_1; }
	inline List_1_t00493746A9C09D2A4E213DD073FD95F9A4FAA144 ** get_address_of_UUQMotrrrdlXgpwOmqCtjHqhvxY_1() { return &___UUQMotrrrdlXgpwOmqCtjHqhvxY_1; }
	inline void set_UUQMotrrrdlXgpwOmqCtjHqhvxY_1(List_1_t00493746A9C09D2A4E213DD073FD95F9A4FAA144 * value)
	{
		___UUQMotrrrdlXgpwOmqCtjHqhvxY_1 = value;
		Il2CppCodeGenWriteBarrier((&___UUQMotrrrdlXgpwOmqCtjHqhvxY_1), value);
	}

	inline static int32_t get_offset_of_DDVHsbCpPQKwnQeEtRpjMMeFxLoN_2() { return static_cast<int32_t>(offsetof(JLPbgmCslxcSncPKJybubLvESakA_tD2EB3276EC7B244F65D32F176710DDA90AEA3CC7, ___DDVHsbCpPQKwnQeEtRpjMMeFxLoN_2)); }
	inline RuntimeObject* get_DDVHsbCpPQKwnQeEtRpjMMeFxLoN_2() const { return ___DDVHsbCpPQKwnQeEtRpjMMeFxLoN_2; }
	inline RuntimeObject** get_address_of_DDVHsbCpPQKwnQeEtRpjMMeFxLoN_2() { return &___DDVHsbCpPQKwnQeEtRpjMMeFxLoN_2; }
	inline void set_DDVHsbCpPQKwnQeEtRpjMMeFxLoN_2(RuntimeObject* value)
	{
		___DDVHsbCpPQKwnQeEtRpjMMeFxLoN_2 = value;
		Il2CppCodeGenWriteBarrier((&___DDVHsbCpPQKwnQeEtRpjMMeFxLoN_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JLPBGMCSLXCSNCPKJYBUBLVESAKA_TD2EB3276EC7B244F65D32F176710DDA90AEA3CC7_H
#ifndef KXCJRTBPIEUBVYCPUDWOFJSWIWHC_TBF7DFF727132588BEBBFE526F8A87281EA1003CF_H
#define KXCJRTBPIEUBVYCPUDWOFJSWIWHC_TBF7DFF727132588BEBBFE526F8A87281EA1003CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// lUbzJtkApcIkohMrHhJAmcDImRRR_kXCJrtbPieUBVYcpUDwOFJSwIwHC
struct  kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF  : public RuntimeObject
{
public:
	// Rewired.CustomController lUbzJtkApcIkohMrHhJAmcDImRRR_kXCJrtbPieUBVYcpUDwOFJSwIwHC::NOArrsNdfKDShNFftfbPqYDzhpq
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR_kXCJrtbPieUBVYcpUDwOFJSwIwHC::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR_kXCJrtbPieUBVYcpUDwOFJSwIwHC::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// lUbzJtkApcIkohMrHhJAmcDImRRR lUbzJtkApcIkohMrHhJAmcDImRRR_kXCJrtbPieUBVYcpUDwOFJSwIwHC::DKaFTWTMhFeLQHDExnDMRxZfmROL
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR_kXCJrtbPieUBVYcpUDwOFJSwIwHC::iqBiBbmpNmwUcFGQZprtTGfIrcb
	int32_t ___iqBiBbmpNmwUcFGQZprtTGfIrcb_4;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR_kXCJrtbPieUBVYcpUDwOFJSwIwHC::fsXsmfwBNAZfagSBJBZfiAnrlqch
	int32_t ___fsXsmfwBNAZfagSBJBZfiAnrlqch_5;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR_kXCJrtbPieUBVYcpUDwOFJSwIwHC::HuWurlclvrHdMSaYmLaocIShGBdd
	int32_t ___HuWurlclvrHdMSaYmLaocIShGBdd_6;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR_kXCJrtbPieUBVYcpUDwOFJSwIwHC::vyUAWHyQzNBXmJvIdvpaOQOqBFQk
	int32_t ___vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_iqBiBbmpNmwUcFGQZprtTGfIrcb_4() { return static_cast<int32_t>(offsetof(kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF, ___iqBiBbmpNmwUcFGQZprtTGfIrcb_4)); }
	inline int32_t get_iqBiBbmpNmwUcFGQZprtTGfIrcb_4() const { return ___iqBiBbmpNmwUcFGQZprtTGfIrcb_4; }
	inline int32_t* get_address_of_iqBiBbmpNmwUcFGQZprtTGfIrcb_4() { return &___iqBiBbmpNmwUcFGQZprtTGfIrcb_4; }
	inline void set_iqBiBbmpNmwUcFGQZprtTGfIrcb_4(int32_t value)
	{
		___iqBiBbmpNmwUcFGQZprtTGfIrcb_4 = value;
	}

	inline static int32_t get_offset_of_fsXsmfwBNAZfagSBJBZfiAnrlqch_5() { return static_cast<int32_t>(offsetof(kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF, ___fsXsmfwBNAZfagSBJBZfiAnrlqch_5)); }
	inline int32_t get_fsXsmfwBNAZfagSBJBZfiAnrlqch_5() const { return ___fsXsmfwBNAZfagSBJBZfiAnrlqch_5; }
	inline int32_t* get_address_of_fsXsmfwBNAZfagSBJBZfiAnrlqch_5() { return &___fsXsmfwBNAZfagSBJBZfiAnrlqch_5; }
	inline void set_fsXsmfwBNAZfagSBJBZfiAnrlqch_5(int32_t value)
	{
		___fsXsmfwBNAZfagSBJBZfiAnrlqch_5 = value;
	}

	inline static int32_t get_offset_of_HuWurlclvrHdMSaYmLaocIShGBdd_6() { return static_cast<int32_t>(offsetof(kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF, ___HuWurlclvrHdMSaYmLaocIShGBdd_6)); }
	inline int32_t get_HuWurlclvrHdMSaYmLaocIShGBdd_6() const { return ___HuWurlclvrHdMSaYmLaocIShGBdd_6; }
	inline int32_t* get_address_of_HuWurlclvrHdMSaYmLaocIShGBdd_6() { return &___HuWurlclvrHdMSaYmLaocIShGBdd_6; }
	inline void set_HuWurlclvrHdMSaYmLaocIShGBdd_6(int32_t value)
	{
		___HuWurlclvrHdMSaYmLaocIShGBdd_6 = value;
	}

	inline static int32_t get_offset_of_vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7() { return static_cast<int32_t>(offsetof(kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF, ___vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7)); }
	inline int32_t get_vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7() const { return ___vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7; }
	inline int32_t* get_address_of_vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7() { return &___vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7; }
	inline void set_vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7(int32_t value)
	{
		___vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KXCJRTBPIEUBVYCPUDWOFJSWIWHC_TBF7DFF727132588BEBBFE526F8A87281EA1003CF_H
#ifndef SANNMDWBSGBKNAGSONMTEQVYEQU_TEB6E9B0777FE09C90D929B448A791778B8AB121C_H
#define SANNMDWBSGBKNAGSONMTEQVYEQU_TEB6E9B0777FE09C90D929B448A791778B8AB121C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// lUbzJtkApcIkohMrHhJAmcDImRRR_sAnnMdWBsGBkNagSOnMteQVYEQU
struct  sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C  : public RuntimeObject
{
public:
	// Rewired.CustomController lUbzJtkApcIkohMrHhJAmcDImRRR_sAnnMdWBsGBkNagSOnMteQVYEQU::NOArrsNdfKDShNFftfbPqYDzhpq
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR_sAnnMdWBsGBkNagSOnMteQVYEQU::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR_sAnnMdWBsGBkNagSOnMteQVYEQU::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// lUbzJtkApcIkohMrHhJAmcDImRRR lUbzJtkApcIkohMrHhJAmcDImRRR_sAnnMdWBsGBkNagSOnMteQVYEQU::DKaFTWTMhFeLQHDExnDMRxZfmROL
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.String lUbzJtkApcIkohMrHhJAmcDImRRR_sAnnMdWBsGBkNagSOnMteQVYEQU::ygjTxdCscBQrKEzNUaPpemDmmZg
	String_t* ___ygjTxdCscBQrKEzNUaPpemDmmZg_4;
	// System.String lUbzJtkApcIkohMrHhJAmcDImRRR_sAnnMdWBsGBkNagSOnMteQVYEQU::PcKcMsBLTsjDxArqCnLFFSqoJbI
	String_t* ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR_sAnnMdWBsGBkNagSOnMteQVYEQU::nFbQIufzVdAoNuZfoBqCXfUUOKN
	int32_t ___nFbQIufzVdAoNuZfoBqCXfUUOKN_6;
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR_sAnnMdWBsGBkNagSOnMteQVYEQU::CXbAsCcpaRocwlxePSqDsZRxJTuA
	int32_t ___CXbAsCcpaRocwlxePSqDsZRxJTuA_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4() { return static_cast<int32_t>(offsetof(sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C, ___ygjTxdCscBQrKEzNUaPpemDmmZg_4)); }
	inline String_t* get_ygjTxdCscBQrKEzNUaPpemDmmZg_4() const { return ___ygjTxdCscBQrKEzNUaPpemDmmZg_4; }
	inline String_t** get_address_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4() { return &___ygjTxdCscBQrKEzNUaPpemDmmZg_4; }
	inline void set_ygjTxdCscBQrKEzNUaPpemDmmZg_4(String_t* value)
	{
		___ygjTxdCscBQrKEzNUaPpemDmmZg_4 = value;
		Il2CppCodeGenWriteBarrier((&___ygjTxdCscBQrKEzNUaPpemDmmZg_4), value);
	}

	inline static int32_t get_offset_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() { return static_cast<int32_t>(offsetof(sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C, ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5)); }
	inline String_t* get_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() const { return ___PcKcMsBLTsjDxArqCnLFFSqoJbI_5; }
	inline String_t** get_address_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5() { return &___PcKcMsBLTsjDxArqCnLFFSqoJbI_5; }
	inline void set_PcKcMsBLTsjDxArqCnLFFSqoJbI_5(String_t* value)
	{
		___PcKcMsBLTsjDxArqCnLFFSqoJbI_5 = value;
		Il2CppCodeGenWriteBarrier((&___PcKcMsBLTsjDxArqCnLFFSqoJbI_5), value);
	}

	inline static int32_t get_offset_of_nFbQIufzVdAoNuZfoBqCXfUUOKN_6() { return static_cast<int32_t>(offsetof(sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C, ___nFbQIufzVdAoNuZfoBqCXfUUOKN_6)); }
	inline int32_t get_nFbQIufzVdAoNuZfoBqCXfUUOKN_6() const { return ___nFbQIufzVdAoNuZfoBqCXfUUOKN_6; }
	inline int32_t* get_address_of_nFbQIufzVdAoNuZfoBqCXfUUOKN_6() { return &___nFbQIufzVdAoNuZfoBqCXfUUOKN_6; }
	inline void set_nFbQIufzVdAoNuZfoBqCXfUUOKN_6(int32_t value)
	{
		___nFbQIufzVdAoNuZfoBqCXfUUOKN_6 = value;
	}

	inline static int32_t get_offset_of_CXbAsCcpaRocwlxePSqDsZRxJTuA_7() { return static_cast<int32_t>(offsetof(sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C, ___CXbAsCcpaRocwlxePSqDsZRxJTuA_7)); }
	inline int32_t get_CXbAsCcpaRocwlxePSqDsZRxJTuA_7() const { return ___CXbAsCcpaRocwlxePSqDsZRxJTuA_7; }
	inline int32_t* get_address_of_CXbAsCcpaRocwlxePSqDsZRxJTuA_7() { return &___CXbAsCcpaRocwlxePSqDsZRxJTuA_7; }
	inline void set_CXbAsCcpaRocwlxePSqDsZRxJTuA_7(int32_t value)
	{
		___CXbAsCcpaRocwlxePSqDsZRxJTuA_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SANNMDWBSGBKNAGSONMTEQVYEQU_TEB6E9B0777FE09C90D929B448A791778B8AB121C_H
#ifndef NKGLVKXMQVVUJNEOGVVMAWWQOJL_T8CC5C80A3504882AE9928FFF16668EC435807D22_H
#define NKGLVKXMQVVUJNEOGVVMAWWQOJL_T8CC5C80A3504882AE9928FFF16668EC435807D22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// nkGLVKXmQVVuJNEogVVmAwwQOJl
struct  nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22  : public RuntimeObject
{
public:
	// Rewired.InputAction[] nkGLVKXmQVVuJNEogVVmAwwQOJl::sHcpZMJInjIQFVhudaFOLDXUgfvb
	InputActionU5BU5D_tD157E02666906BCC0381D6204DB99218559DD6F3* ___sHcpZMJInjIQFVhudaFOLDXUgfvb_0;
	// Rewired.Utils.Classes.Data.ADictionary`2<System.String,nkGLVKXmQVVuJNEogVVmAwwQOJl_tFGjMRkSnzMdvjlUOfGbmVBOoWy> nkGLVKXmQVVuJNEogVVmAwwQOJl::BNOJwGEydMJVXSuTJakmKeLIhTQ
	ADictionary_2_tB857704A7EC12DF048058CE5530F82C7C1FF3253 * ___BNOJwGEydMJVXSuTJakmKeLIhTQ_1;
	// nkGLVKXmQVVuJNEogVVmAwwQOJl_tFGjMRkSnzMdvjlUOfGbmVBOoWy[] nkGLVKXmQVVuJNEogVVmAwwQOJl::ZCPKVMwlCoxJXBikDmhLmwDFMdF
	tFGjMRkSnzMdvjlUOfGbmVBOoWyU5BU5D_tD4F19941C97928551C285DB3314FAF0F7571D942* ___ZCPKVMwlCoxJXBikDmhLmwDFMdF_2;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.InputAction> nkGLVKXmQVVuJNEogVVmAwwQOJl::BQwulTWiPxilhhBeLZrXjhMwunr
	ReadOnlyCollection_1_tB6849D2EAAF58FF4A5ADFB4DE2B3C5B577594DCC * ___BQwulTWiPxilhhBeLZrXjhMwunr_3;
	// System.Int32 nkGLVKXmQVVuJNEogVVmAwwQOJl::viKiGUPKnvKiBpsmKMiFyEiTesBf
	int32_t ___viKiGUPKnvKiBpsmKMiFyEiTesBf_4;
	// System.Int32 nkGLVKXmQVVuJNEogVVmAwwQOJl::PdOJVOhXFvBNcgTTFTRrdsGQtwc
	int32_t ___PdOJVOhXFvBNcgTTFTRrdsGQtwc_5;
	// System.Collections.Generic.List`1<System.String> nkGLVKXmQVVuJNEogVVmAwwQOJl::nPxkyYJsDBYIqAAGFFPsCatPbPa
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___nPxkyYJsDBYIqAAGFFPsCatPbPa_6;
	// System.Collections.Generic.List`1<System.Int32> nkGLVKXmQVVuJNEogVVmAwwQOJl::mBQzQCMXDIDEMnkhmuwuMBXYnWa
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___mBQzQCMXDIDEMnkhmuwuMBXYnWa_7;

public:
	inline static int32_t get_offset_of_sHcpZMJInjIQFVhudaFOLDXUgfvb_0() { return static_cast<int32_t>(offsetof(nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22, ___sHcpZMJInjIQFVhudaFOLDXUgfvb_0)); }
	inline InputActionU5BU5D_tD157E02666906BCC0381D6204DB99218559DD6F3* get_sHcpZMJInjIQFVhudaFOLDXUgfvb_0() const { return ___sHcpZMJInjIQFVhudaFOLDXUgfvb_0; }
	inline InputActionU5BU5D_tD157E02666906BCC0381D6204DB99218559DD6F3** get_address_of_sHcpZMJInjIQFVhudaFOLDXUgfvb_0() { return &___sHcpZMJInjIQFVhudaFOLDXUgfvb_0; }
	inline void set_sHcpZMJInjIQFVhudaFOLDXUgfvb_0(InputActionU5BU5D_tD157E02666906BCC0381D6204DB99218559DD6F3* value)
	{
		___sHcpZMJInjIQFVhudaFOLDXUgfvb_0 = value;
		Il2CppCodeGenWriteBarrier((&___sHcpZMJInjIQFVhudaFOLDXUgfvb_0), value);
	}

	inline static int32_t get_offset_of_BNOJwGEydMJVXSuTJakmKeLIhTQ_1() { return static_cast<int32_t>(offsetof(nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22, ___BNOJwGEydMJVXSuTJakmKeLIhTQ_1)); }
	inline ADictionary_2_tB857704A7EC12DF048058CE5530F82C7C1FF3253 * get_BNOJwGEydMJVXSuTJakmKeLIhTQ_1() const { return ___BNOJwGEydMJVXSuTJakmKeLIhTQ_1; }
	inline ADictionary_2_tB857704A7EC12DF048058CE5530F82C7C1FF3253 ** get_address_of_BNOJwGEydMJVXSuTJakmKeLIhTQ_1() { return &___BNOJwGEydMJVXSuTJakmKeLIhTQ_1; }
	inline void set_BNOJwGEydMJVXSuTJakmKeLIhTQ_1(ADictionary_2_tB857704A7EC12DF048058CE5530F82C7C1FF3253 * value)
	{
		___BNOJwGEydMJVXSuTJakmKeLIhTQ_1 = value;
		Il2CppCodeGenWriteBarrier((&___BNOJwGEydMJVXSuTJakmKeLIhTQ_1), value);
	}

	inline static int32_t get_offset_of_ZCPKVMwlCoxJXBikDmhLmwDFMdF_2() { return static_cast<int32_t>(offsetof(nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22, ___ZCPKVMwlCoxJXBikDmhLmwDFMdF_2)); }
	inline tFGjMRkSnzMdvjlUOfGbmVBOoWyU5BU5D_tD4F19941C97928551C285DB3314FAF0F7571D942* get_ZCPKVMwlCoxJXBikDmhLmwDFMdF_2() const { return ___ZCPKVMwlCoxJXBikDmhLmwDFMdF_2; }
	inline tFGjMRkSnzMdvjlUOfGbmVBOoWyU5BU5D_tD4F19941C97928551C285DB3314FAF0F7571D942** get_address_of_ZCPKVMwlCoxJXBikDmhLmwDFMdF_2() { return &___ZCPKVMwlCoxJXBikDmhLmwDFMdF_2; }
	inline void set_ZCPKVMwlCoxJXBikDmhLmwDFMdF_2(tFGjMRkSnzMdvjlUOfGbmVBOoWyU5BU5D_tD4F19941C97928551C285DB3314FAF0F7571D942* value)
	{
		___ZCPKVMwlCoxJXBikDmhLmwDFMdF_2 = value;
		Il2CppCodeGenWriteBarrier((&___ZCPKVMwlCoxJXBikDmhLmwDFMdF_2), value);
	}

	inline static int32_t get_offset_of_BQwulTWiPxilhhBeLZrXjhMwunr_3() { return static_cast<int32_t>(offsetof(nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22, ___BQwulTWiPxilhhBeLZrXjhMwunr_3)); }
	inline ReadOnlyCollection_1_tB6849D2EAAF58FF4A5ADFB4DE2B3C5B577594DCC * get_BQwulTWiPxilhhBeLZrXjhMwunr_3() const { return ___BQwulTWiPxilhhBeLZrXjhMwunr_3; }
	inline ReadOnlyCollection_1_tB6849D2EAAF58FF4A5ADFB4DE2B3C5B577594DCC ** get_address_of_BQwulTWiPxilhhBeLZrXjhMwunr_3() { return &___BQwulTWiPxilhhBeLZrXjhMwunr_3; }
	inline void set_BQwulTWiPxilhhBeLZrXjhMwunr_3(ReadOnlyCollection_1_tB6849D2EAAF58FF4A5ADFB4DE2B3C5B577594DCC * value)
	{
		___BQwulTWiPxilhhBeLZrXjhMwunr_3 = value;
		Il2CppCodeGenWriteBarrier((&___BQwulTWiPxilhhBeLZrXjhMwunr_3), value);
	}

	inline static int32_t get_offset_of_viKiGUPKnvKiBpsmKMiFyEiTesBf_4() { return static_cast<int32_t>(offsetof(nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22, ___viKiGUPKnvKiBpsmKMiFyEiTesBf_4)); }
	inline int32_t get_viKiGUPKnvKiBpsmKMiFyEiTesBf_4() const { return ___viKiGUPKnvKiBpsmKMiFyEiTesBf_4; }
	inline int32_t* get_address_of_viKiGUPKnvKiBpsmKMiFyEiTesBf_4() { return &___viKiGUPKnvKiBpsmKMiFyEiTesBf_4; }
	inline void set_viKiGUPKnvKiBpsmKMiFyEiTesBf_4(int32_t value)
	{
		___viKiGUPKnvKiBpsmKMiFyEiTesBf_4 = value;
	}

	inline static int32_t get_offset_of_PdOJVOhXFvBNcgTTFTRrdsGQtwc_5() { return static_cast<int32_t>(offsetof(nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22, ___PdOJVOhXFvBNcgTTFTRrdsGQtwc_5)); }
	inline int32_t get_PdOJVOhXFvBNcgTTFTRrdsGQtwc_5() const { return ___PdOJVOhXFvBNcgTTFTRrdsGQtwc_5; }
	inline int32_t* get_address_of_PdOJVOhXFvBNcgTTFTRrdsGQtwc_5() { return &___PdOJVOhXFvBNcgTTFTRrdsGQtwc_5; }
	inline void set_PdOJVOhXFvBNcgTTFTRrdsGQtwc_5(int32_t value)
	{
		___PdOJVOhXFvBNcgTTFTRrdsGQtwc_5 = value;
	}

	inline static int32_t get_offset_of_nPxkyYJsDBYIqAAGFFPsCatPbPa_6() { return static_cast<int32_t>(offsetof(nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22, ___nPxkyYJsDBYIqAAGFFPsCatPbPa_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_nPxkyYJsDBYIqAAGFFPsCatPbPa_6() const { return ___nPxkyYJsDBYIqAAGFFPsCatPbPa_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_nPxkyYJsDBYIqAAGFFPsCatPbPa_6() { return &___nPxkyYJsDBYIqAAGFFPsCatPbPa_6; }
	inline void set_nPxkyYJsDBYIqAAGFFPsCatPbPa_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___nPxkyYJsDBYIqAAGFFPsCatPbPa_6 = value;
		Il2CppCodeGenWriteBarrier((&___nPxkyYJsDBYIqAAGFFPsCatPbPa_6), value);
	}

	inline static int32_t get_offset_of_mBQzQCMXDIDEMnkhmuwuMBXYnWa_7() { return static_cast<int32_t>(offsetof(nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22, ___mBQzQCMXDIDEMnkhmuwuMBXYnWa_7)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_mBQzQCMXDIDEMnkhmuwuMBXYnWa_7() const { return ___mBQzQCMXDIDEMnkhmuwuMBXYnWa_7; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_mBQzQCMXDIDEMnkhmuwuMBXYnWa_7() { return &___mBQzQCMXDIDEMnkhmuwuMBXYnWa_7; }
	inline void set_mBQzQCMXDIDEMnkhmuwuMBXYnWa_7(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___mBQzQCMXDIDEMnkhmuwuMBXYnWa_7 = value;
		Il2CppCodeGenWriteBarrier((&___mBQzQCMXDIDEMnkhmuwuMBXYnWa_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NKGLVKXMQVVUJNEOGVVMAWWQOJL_T8CC5C80A3504882AE9928FFF16668EC435807D22_H
#ifndef TFGJMRKSNZMDVJLUOFGBMVBOOWY_T5DC09B766606920055715F138215F62E731B9D61_H
#define TFGJMRKSNZMDVJLUOFGBMVBOOWY_T5DC09B766606920055715F138215F62E731B9D61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// nkGLVKXmQVVuJNEogVVmAwwQOJl_tFGjMRkSnzMdvjlUOfGbmVBOoWy
struct  tFGjMRkSnzMdvjlUOfGbmVBOoWy_t5DC09B766606920055715F138215F62E731B9D61  : public RuntimeObject
{
public:
	// Rewired.InputAction nkGLVKXmQVVuJNEogVVmAwwQOJl_tFGjMRkSnzMdvjlUOfGbmVBOoWy::iexOpLWpDlgqvuDaErqqJlUOkHy
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___iexOpLWpDlgqvuDaErqqJlUOkHy_0;
	// System.Int32 nkGLVKXmQVVuJNEogVVmAwwQOJl_tFGjMRkSnzMdvjlUOfGbmVBOoWy::MlHuhkxTPKDycqbMwTBkBIGihuU
	int32_t ___MlHuhkxTPKDycqbMwTBkBIGihuU_1;
	// System.Int32 nkGLVKXmQVVuJNEogVVmAwwQOJl_tFGjMRkSnzMdvjlUOfGbmVBOoWy::mmqqnntCpwCJvoDhRgRifrXtNvUc
	int32_t ___mmqqnntCpwCJvoDhRgRifrXtNvUc_2;

public:
	inline static int32_t get_offset_of_iexOpLWpDlgqvuDaErqqJlUOkHy_0() { return static_cast<int32_t>(offsetof(tFGjMRkSnzMdvjlUOfGbmVBOoWy_t5DC09B766606920055715F138215F62E731B9D61, ___iexOpLWpDlgqvuDaErqqJlUOkHy_0)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_iexOpLWpDlgqvuDaErqqJlUOkHy_0() const { return ___iexOpLWpDlgqvuDaErqqJlUOkHy_0; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_iexOpLWpDlgqvuDaErqqJlUOkHy_0() { return &___iexOpLWpDlgqvuDaErqqJlUOkHy_0; }
	inline void set_iexOpLWpDlgqvuDaErqqJlUOkHy_0(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___iexOpLWpDlgqvuDaErqqJlUOkHy_0 = value;
		Il2CppCodeGenWriteBarrier((&___iexOpLWpDlgqvuDaErqqJlUOkHy_0), value);
	}

	inline static int32_t get_offset_of_MlHuhkxTPKDycqbMwTBkBIGihuU_1() { return static_cast<int32_t>(offsetof(tFGjMRkSnzMdvjlUOfGbmVBOoWy_t5DC09B766606920055715F138215F62E731B9D61, ___MlHuhkxTPKDycqbMwTBkBIGihuU_1)); }
	inline int32_t get_MlHuhkxTPKDycqbMwTBkBIGihuU_1() const { return ___MlHuhkxTPKDycqbMwTBkBIGihuU_1; }
	inline int32_t* get_address_of_MlHuhkxTPKDycqbMwTBkBIGihuU_1() { return &___MlHuhkxTPKDycqbMwTBkBIGihuU_1; }
	inline void set_MlHuhkxTPKDycqbMwTBkBIGihuU_1(int32_t value)
	{
		___MlHuhkxTPKDycqbMwTBkBIGihuU_1 = value;
	}

	inline static int32_t get_offset_of_mmqqnntCpwCJvoDhRgRifrXtNvUc_2() { return static_cast<int32_t>(offsetof(tFGjMRkSnzMdvjlUOfGbmVBOoWy_t5DC09B766606920055715F138215F62E731B9D61, ___mmqqnntCpwCJvoDhRgRifrXtNvUc_2)); }
	inline int32_t get_mmqqnntCpwCJvoDhRgRifrXtNvUc_2() const { return ___mmqqnntCpwCJvoDhRgRifrXtNvUc_2; }
	inline int32_t* get_address_of_mmqqnntCpwCJvoDhRgRifrXtNvUc_2() { return &___mmqqnntCpwCJvoDhRgRifrXtNvUc_2; }
	inline void set_mmqqnntCpwCJvoDhRgRifrXtNvUc_2(int32_t value)
	{
		___mmqqnntCpwCJvoDhRgRifrXtNvUc_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TFGJMRKSNZMDVJLUOFGBMVBOOWY_T5DC09B766606920055715F138215F62E731B9D61_H
#ifndef CONTROLLERDISCONNECTEDEVENTARGS_TE3F08FB1D1CCD23522D1FE41576E01ECCC2275D5_H
#define CONTROLLERDISCONNECTEDEVENTARGS_TE3F08FB1D1CCD23522D1FE41576E01ECCC2275D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerDisconnectedEventArgs
struct  ControllerDisconnectedEventArgs_tE3F08FB1D1CCD23522D1FE41576E01ECCC2275D5  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Int32 Rewired.ControllerDisconnectedEventArgs::rewiredId
	int32_t ___rewiredId_1;

public:
	inline static int32_t get_offset_of_rewiredId_1() { return static_cast<int32_t>(offsetof(ControllerDisconnectedEventArgs_tE3F08FB1D1CCD23522D1FE41576E01ECCC2275D5, ___rewiredId_1)); }
	inline int32_t get_rewiredId_1() const { return ___rewiredId_1; }
	inline int32_t* get_address_of_rewiredId_1() { return &___rewiredId_1; }
	inline void set_rewiredId_1(int32_t value)
	{
		___rewiredId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERDISCONNECTEDEVENTARGS_TE3F08FB1D1CCD23522D1FE41576E01ECCC2275D5_H
#ifndef HIDACCELEROMETER_TDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76_H
#define HIDACCELEROMETER_TDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDAccelerometer
struct  HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76  : public HIDControllerElement_tC98B3FAE2975F27917258ACE9B578318D1EAF7E0
{
public:
	// System.Single[] Rewired.HID.HIDAccelerometer::rawValue
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___rawValue_2;
	// System.Single Rewired.HID.HIDAccelerometer::timestamp
	float ___timestamp_3;
	// System.Int32 Rewired.HID.HIDAccelerometer::valueLength
	int32_t ___valueLength_4;
	// System.Byte[] Rewired.HID.HIDAccelerometer::NRwCWKPZMBPqSTcLtLnOurBUoWN
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___NRwCWKPZMBPqSTcLtLnOurBUoWN_5;
	// System.Int32 Rewired.HID.HIDAccelerometer::MXKCkdMwCxEJlCsqhAxVfcBDfLde
	int32_t ___MXKCkdMwCxEJlCsqhAxVfcBDfLde_6;
	// System.Int32 Rewired.HID.HIDAccelerometer::AfklhFSjQRdMZXJChQYWNazMHbq
	int32_t ___AfklhFSjQRdMZXJChQYWNazMHbq_7;
	// System.Action`2<System.Byte[],System.Single[]> Rewired.HID.HIDAccelerometer::agqHqDPvTEqPJElFDveRIhPWjClF
	Action_2_t6348996BC3E0C8743F528E4FA9E5118FC95A1FEB * ___agqHqDPvTEqPJElFDveRIhPWjClF_8;

public:
	inline static int32_t get_offset_of_rawValue_2() { return static_cast<int32_t>(offsetof(HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76, ___rawValue_2)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_rawValue_2() const { return ___rawValue_2; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_rawValue_2() { return &___rawValue_2; }
	inline void set_rawValue_2(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___rawValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___rawValue_2), value);
	}

	inline static int32_t get_offset_of_timestamp_3() { return static_cast<int32_t>(offsetof(HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76, ___timestamp_3)); }
	inline float get_timestamp_3() const { return ___timestamp_3; }
	inline float* get_address_of_timestamp_3() { return &___timestamp_3; }
	inline void set_timestamp_3(float value)
	{
		___timestamp_3 = value;
	}

	inline static int32_t get_offset_of_valueLength_4() { return static_cast<int32_t>(offsetof(HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76, ___valueLength_4)); }
	inline int32_t get_valueLength_4() const { return ___valueLength_4; }
	inline int32_t* get_address_of_valueLength_4() { return &___valueLength_4; }
	inline void set_valueLength_4(int32_t value)
	{
		___valueLength_4 = value;
	}

	inline static int32_t get_offset_of_NRwCWKPZMBPqSTcLtLnOurBUoWN_5() { return static_cast<int32_t>(offsetof(HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76, ___NRwCWKPZMBPqSTcLtLnOurBUoWN_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_NRwCWKPZMBPqSTcLtLnOurBUoWN_5() const { return ___NRwCWKPZMBPqSTcLtLnOurBUoWN_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_NRwCWKPZMBPqSTcLtLnOurBUoWN_5() { return &___NRwCWKPZMBPqSTcLtLnOurBUoWN_5; }
	inline void set_NRwCWKPZMBPqSTcLtLnOurBUoWN_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___NRwCWKPZMBPqSTcLtLnOurBUoWN_5 = value;
		Il2CppCodeGenWriteBarrier((&___NRwCWKPZMBPqSTcLtLnOurBUoWN_5), value);
	}

	inline static int32_t get_offset_of_MXKCkdMwCxEJlCsqhAxVfcBDfLde_6() { return static_cast<int32_t>(offsetof(HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76, ___MXKCkdMwCxEJlCsqhAxVfcBDfLde_6)); }
	inline int32_t get_MXKCkdMwCxEJlCsqhAxVfcBDfLde_6() const { return ___MXKCkdMwCxEJlCsqhAxVfcBDfLde_6; }
	inline int32_t* get_address_of_MXKCkdMwCxEJlCsqhAxVfcBDfLde_6() { return &___MXKCkdMwCxEJlCsqhAxVfcBDfLde_6; }
	inline void set_MXKCkdMwCxEJlCsqhAxVfcBDfLde_6(int32_t value)
	{
		___MXKCkdMwCxEJlCsqhAxVfcBDfLde_6 = value;
	}

	inline static int32_t get_offset_of_AfklhFSjQRdMZXJChQYWNazMHbq_7() { return static_cast<int32_t>(offsetof(HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76, ___AfklhFSjQRdMZXJChQYWNazMHbq_7)); }
	inline int32_t get_AfklhFSjQRdMZXJChQYWNazMHbq_7() const { return ___AfklhFSjQRdMZXJChQYWNazMHbq_7; }
	inline int32_t* get_address_of_AfklhFSjQRdMZXJChQYWNazMHbq_7() { return &___AfklhFSjQRdMZXJChQYWNazMHbq_7; }
	inline void set_AfklhFSjQRdMZXJChQYWNazMHbq_7(int32_t value)
	{
		___AfklhFSjQRdMZXJChQYWNazMHbq_7 = value;
	}

	inline static int32_t get_offset_of_agqHqDPvTEqPJElFDveRIhPWjClF_8() { return static_cast<int32_t>(offsetof(HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76, ___agqHqDPvTEqPJElFDveRIhPWjClF_8)); }
	inline Action_2_t6348996BC3E0C8743F528E4FA9E5118FC95A1FEB * get_agqHqDPvTEqPJElFDveRIhPWjClF_8() const { return ___agqHqDPvTEqPJElFDveRIhPWjClF_8; }
	inline Action_2_t6348996BC3E0C8743F528E4FA9E5118FC95A1FEB ** get_address_of_agqHqDPvTEqPJElFDveRIhPWjClF_8() { return &___agqHqDPvTEqPJElFDveRIhPWjClF_8; }
	inline void set_agqHqDPvTEqPJElFDveRIhPWjClF_8(Action_2_t6348996BC3E0C8743F528E4FA9E5118FC95A1FEB * value)
	{
		___agqHqDPvTEqPJElFDveRIhPWjClF_8 = value;
		Il2CppCodeGenWriteBarrier((&___agqHqDPvTEqPJElFDveRIhPWjClF_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDACCELEROMETER_TDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76_H
#ifndef HIDAXIS_TCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D_H
#define HIDAXIS_TCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDAxis
struct  HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D  : public HIDControllerElement_tC98B3FAE2975F27917258ACE9B578318D1EAF7E0
{
public:
	// System.Int32 Rewired.HID.HIDAxis::rawValue
	int32_t ___rawValue_2;
	// System.Single Rewired.HID.HIDAxis::timestamp
	float ___timestamp_3;
	// System.Int32 Rewired.HID.HIDAxis::byteLength
	int32_t ___byteLength_4;
	// System.Int32 Rewired.HID.HIDAxis::startIndex
	int32_t ___startIndex_5;
	// System.Boolean Rewired.HID.HIDAxis::isSigned
	bool ___isSigned_6;
	// System.Int32 Rewired.HID.HIDAxis::minValue
	int32_t ___minValue_7;
	// System.Int32 Rewired.HID.HIDAxis::maxValue
	int32_t ___maxValue_8;
	// System.Int32 Rewired.HID.HIDAxis::zeroValue
	int32_t ___zeroValue_9;

public:
	inline static int32_t get_offset_of_rawValue_2() { return static_cast<int32_t>(offsetof(HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D, ___rawValue_2)); }
	inline int32_t get_rawValue_2() const { return ___rawValue_2; }
	inline int32_t* get_address_of_rawValue_2() { return &___rawValue_2; }
	inline void set_rawValue_2(int32_t value)
	{
		___rawValue_2 = value;
	}

	inline static int32_t get_offset_of_timestamp_3() { return static_cast<int32_t>(offsetof(HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D, ___timestamp_3)); }
	inline float get_timestamp_3() const { return ___timestamp_3; }
	inline float* get_address_of_timestamp_3() { return &___timestamp_3; }
	inline void set_timestamp_3(float value)
	{
		___timestamp_3 = value;
	}

	inline static int32_t get_offset_of_byteLength_4() { return static_cast<int32_t>(offsetof(HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D, ___byteLength_4)); }
	inline int32_t get_byteLength_4() const { return ___byteLength_4; }
	inline int32_t* get_address_of_byteLength_4() { return &___byteLength_4; }
	inline void set_byteLength_4(int32_t value)
	{
		___byteLength_4 = value;
	}

	inline static int32_t get_offset_of_startIndex_5() { return static_cast<int32_t>(offsetof(HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D, ___startIndex_5)); }
	inline int32_t get_startIndex_5() const { return ___startIndex_5; }
	inline int32_t* get_address_of_startIndex_5() { return &___startIndex_5; }
	inline void set_startIndex_5(int32_t value)
	{
		___startIndex_5 = value;
	}

	inline static int32_t get_offset_of_isSigned_6() { return static_cast<int32_t>(offsetof(HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D, ___isSigned_6)); }
	inline bool get_isSigned_6() const { return ___isSigned_6; }
	inline bool* get_address_of_isSigned_6() { return &___isSigned_6; }
	inline void set_isSigned_6(bool value)
	{
		___isSigned_6 = value;
	}

	inline static int32_t get_offset_of_minValue_7() { return static_cast<int32_t>(offsetof(HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D, ___minValue_7)); }
	inline int32_t get_minValue_7() const { return ___minValue_7; }
	inline int32_t* get_address_of_minValue_7() { return &___minValue_7; }
	inline void set_minValue_7(int32_t value)
	{
		___minValue_7 = value;
	}

	inline static int32_t get_offset_of_maxValue_8() { return static_cast<int32_t>(offsetof(HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D, ___maxValue_8)); }
	inline int32_t get_maxValue_8() const { return ___maxValue_8; }
	inline int32_t* get_address_of_maxValue_8() { return &___maxValue_8; }
	inline void set_maxValue_8(int32_t value)
	{
		___maxValue_8 = value;
	}

	inline static int32_t get_offset_of_zeroValue_9() { return static_cast<int32_t>(offsetof(HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D, ___zeroValue_9)); }
	inline int32_t get_zeroValue_9() const { return ___zeroValue_9; }
	inline int32_t* get_address_of_zeroValue_9() { return &___zeroValue_9; }
	inline void set_zeroValue_9(int32_t value)
	{
		___zeroValue_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDAXIS_TCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D_H
#ifndef HIDBUTTON_T6FDD5F213F676E5818EA2F010CF356AB8CC61F33_H
#define HIDBUTTON_T6FDD5F213F676E5818EA2F010CF356AB8CC61F33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDButton
struct  HIDButton_t6FDD5F213F676E5818EA2F010CF356AB8CC61F33  : public HIDControllerElement_tC98B3FAE2975F27917258ACE9B578318D1EAF7E0
{
public:
	// System.Boolean Rewired.HID.HIDButton::rawValue
	bool ___rawValue_2;
	// System.Single Rewired.HID.HIDButton::timestamp
	float ___timestamp_3;

public:
	inline static int32_t get_offset_of_rawValue_2() { return static_cast<int32_t>(offsetof(HIDButton_t6FDD5F213F676E5818EA2F010CF356AB8CC61F33, ___rawValue_2)); }
	inline bool get_rawValue_2() const { return ___rawValue_2; }
	inline bool* get_address_of_rawValue_2() { return &___rawValue_2; }
	inline void set_rawValue_2(bool value)
	{
		___rawValue_2 = value;
	}

	inline static int32_t get_offset_of_timestamp_3() { return static_cast<int32_t>(offsetof(HIDButton_t6FDD5F213F676E5818EA2F010CF356AB8CC61F33, ___timestamp_3)); }
	inline float get_timestamp_3() const { return ___timestamp_3; }
	inline float* get_address_of_timestamp_3() { return &___timestamp_3; }
	inline void set_timestamp_3(float value)
	{
		___timestamp_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDBUTTON_T6FDD5F213F676E5818EA2F010CF356AB8CC61F33_H
#ifndef HIDCONTROLLERELEMENTWITHDATASET_T991E71547369AF5788D0C1935AC633A2A0128775_H
#define HIDCONTROLLERELEMENTWITHDATASET_T991E71547369AF5788D0C1935AC633A2A0128775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDControllerElementWithDataSet
struct  HIDControllerElementWithDataSet_t991E71547369AF5788D0C1935AC633A2A0128775  : public HIDControllerElement_tC98B3FAE2975F27917258ACE9B578318D1EAF7E0
{
public:
	// Rewired.HID.HIDControllerElementWithDataSet_wgqSTfYHDsMNygvPFQwICLAnDKO Rewired.HID.HIDControllerElementWithDataSet::dataSet
	wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5 * ___dataSet_2;

public:
	inline static int32_t get_offset_of_dataSet_2() { return static_cast<int32_t>(offsetof(HIDControllerElementWithDataSet_t991E71547369AF5788D0C1935AC633A2A0128775, ___dataSet_2)); }
	inline wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5 * get_dataSet_2() const { return ___dataSet_2; }
	inline wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5 ** get_address_of_dataSet_2() { return &___dataSet_2; }
	inline void set_dataSet_2(wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5 * value)
	{
		___dataSet_2 = value;
		Il2CppCodeGenWriteBarrier((&___dataSet_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDCONTROLLERELEMENTWITHDATASET_T991E71547369AF5788D0C1935AC633A2A0128775_H
#ifndef AOALGZCLMUAOEIMJGRYDMCRTRTKR_TD17EB85A0DF821B33D140C6E7AA46F2D60BD9AC1_H
#define AOALGZCLMUAOEIMJGRYDMCRTRTKR_TD17EB85A0DF821B33D140C6E7AA46F2D60BD9AC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDGyroscope_AOalgZclMUAOEImjGRYdmCrtRtkR
struct  AOalgZclMUAOEImjGRYdmCrtRtkR_tD17EB85A0DF821B33D140C6E7AA46F2D60BD9AC1  : public wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5
{
public:
	// System.Int32 Rewired.HID.HIDGyroscope_AOalgZclMUAOEImjGRYdmCrtRtkR::LyLzjLOmwSaNWEHoBSRAyumGQSE
	int32_t ___LyLzjLOmwSaNWEHoBSRAyumGQSE_7;
	// System.Int32 Rewired.HID.HIDGyroscope_AOalgZclMUAOEImjGRYdmCrtRtkR::NjyjytGcOPRyPyTXdTHZglaaOxF
	int32_t ___NjyjytGcOPRyPyTXdTHZglaaOxF_8;

public:
	inline static int32_t get_offset_of_LyLzjLOmwSaNWEHoBSRAyumGQSE_7() { return static_cast<int32_t>(offsetof(AOalgZclMUAOEImjGRYdmCrtRtkR_tD17EB85A0DF821B33D140C6E7AA46F2D60BD9AC1, ___LyLzjLOmwSaNWEHoBSRAyumGQSE_7)); }
	inline int32_t get_LyLzjLOmwSaNWEHoBSRAyumGQSE_7() const { return ___LyLzjLOmwSaNWEHoBSRAyumGQSE_7; }
	inline int32_t* get_address_of_LyLzjLOmwSaNWEHoBSRAyumGQSE_7() { return &___LyLzjLOmwSaNWEHoBSRAyumGQSE_7; }
	inline void set_LyLzjLOmwSaNWEHoBSRAyumGQSE_7(int32_t value)
	{
		___LyLzjLOmwSaNWEHoBSRAyumGQSE_7 = value;
	}

	inline static int32_t get_offset_of_NjyjytGcOPRyPyTXdTHZglaaOxF_8() { return static_cast<int32_t>(offsetof(AOalgZclMUAOEImjGRYdmCrtRtkR_tD17EB85A0DF821B33D140C6E7AA46F2D60BD9AC1, ___NjyjytGcOPRyPyTXdTHZglaaOxF_8)); }
	inline int32_t get_NjyjytGcOPRyPyTXdTHZglaaOxF_8() const { return ___NjyjytGcOPRyPyTXdTHZglaaOxF_8; }
	inline int32_t* get_address_of_NjyjytGcOPRyPyTXdTHZglaaOxF_8() { return &___NjyjytGcOPRyPyTXdTHZglaaOxF_8; }
	inline void set_NjyjytGcOPRyPyTXdTHZglaaOxF_8(int32_t value)
	{
		___NjyjytGcOPRyPyTXdTHZglaaOxF_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AOALGZCLMUAOEIMJGRYDMCRTRTKR_TD17EB85A0DF821B33D140C6E7AA46F2D60BD9AC1_H
#ifndef HIDTOUCHPAD_TB50BF65E87F43683093CD5B26D9ED251664532C0_H
#define HIDTOUCHPAD_TB50BF65E87F43683093CD5B26D9ED251664532C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDTouchpad
struct  HIDTouchpad_tB50BF65E87F43683093CD5B26D9ED251664532C0  : public HIDControllerElement_tC98B3FAE2975F27917258ACE9B578318D1EAF7E0
{
public:
	// Rewired.HID.HIDTouchpad_TouchpadInfo Rewired.HID.HIDTouchpad::dyaUNLHBIeUikBzFJeaTDfszbXPG
	TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F * ___dyaUNLHBIeUikBzFJeaTDfszbXPG_2;
	// System.Collections.Generic.Queue`1<Rewired.HID.HIDTouchpad_TouchData> Rewired.HID.HIDTouchpad::nXTEALcSUETkLFyjMScGMCqzyDsh
	Queue_1_tCA0F05DEAE19C30F80931FD49F701E7FD431028A * ___nXTEALcSUETkLFyjMScGMCqzyDsh_3;
	// Rewired.HID.HIDTouchpad_TouchData[] Rewired.HID.HIDTouchpad::QsxvkxJlxUAhMGXKYwoKQkbOURZ
	TouchDataU5BU5D_tFC196EF962568BF4100D8AD3E86E59E35926F03D* ___QsxvkxJlxUAhMGXKYwoKQkbOURZ_4;
	// System.Action`2<Rewired.Utils.Classes.Data.NativeBuffer,Rewired.HID.HIDTouchpad_TouchData[]> Rewired.HID.HIDTouchpad::agqHqDPvTEqPJElFDveRIhPWjClF
	Action_2_t7A013AB8DF7D757FD04ED42C96A7751D8A139398 * ___agqHqDPvTEqPJElFDveRIhPWjClF_5;
	// Rewired.HID.HIDTouchpad_TouchData[] Rewired.HID.HIDTouchpad::values
	TouchDataU5BU5D_tFC196EF962568BF4100D8AD3E86E59E35926F03D* ___values_6;

public:
	inline static int32_t get_offset_of_dyaUNLHBIeUikBzFJeaTDfszbXPG_2() { return static_cast<int32_t>(offsetof(HIDTouchpad_tB50BF65E87F43683093CD5B26D9ED251664532C0, ___dyaUNLHBIeUikBzFJeaTDfszbXPG_2)); }
	inline TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F * get_dyaUNLHBIeUikBzFJeaTDfszbXPG_2() const { return ___dyaUNLHBIeUikBzFJeaTDfszbXPG_2; }
	inline TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F ** get_address_of_dyaUNLHBIeUikBzFJeaTDfszbXPG_2() { return &___dyaUNLHBIeUikBzFJeaTDfszbXPG_2; }
	inline void set_dyaUNLHBIeUikBzFJeaTDfszbXPG_2(TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F * value)
	{
		___dyaUNLHBIeUikBzFJeaTDfszbXPG_2 = value;
		Il2CppCodeGenWriteBarrier((&___dyaUNLHBIeUikBzFJeaTDfszbXPG_2), value);
	}

	inline static int32_t get_offset_of_nXTEALcSUETkLFyjMScGMCqzyDsh_3() { return static_cast<int32_t>(offsetof(HIDTouchpad_tB50BF65E87F43683093CD5B26D9ED251664532C0, ___nXTEALcSUETkLFyjMScGMCqzyDsh_3)); }
	inline Queue_1_tCA0F05DEAE19C30F80931FD49F701E7FD431028A * get_nXTEALcSUETkLFyjMScGMCqzyDsh_3() const { return ___nXTEALcSUETkLFyjMScGMCqzyDsh_3; }
	inline Queue_1_tCA0F05DEAE19C30F80931FD49F701E7FD431028A ** get_address_of_nXTEALcSUETkLFyjMScGMCqzyDsh_3() { return &___nXTEALcSUETkLFyjMScGMCqzyDsh_3; }
	inline void set_nXTEALcSUETkLFyjMScGMCqzyDsh_3(Queue_1_tCA0F05DEAE19C30F80931FD49F701E7FD431028A * value)
	{
		___nXTEALcSUETkLFyjMScGMCqzyDsh_3 = value;
		Il2CppCodeGenWriteBarrier((&___nXTEALcSUETkLFyjMScGMCqzyDsh_3), value);
	}

	inline static int32_t get_offset_of_QsxvkxJlxUAhMGXKYwoKQkbOURZ_4() { return static_cast<int32_t>(offsetof(HIDTouchpad_tB50BF65E87F43683093CD5B26D9ED251664532C0, ___QsxvkxJlxUAhMGXKYwoKQkbOURZ_4)); }
	inline TouchDataU5BU5D_tFC196EF962568BF4100D8AD3E86E59E35926F03D* get_QsxvkxJlxUAhMGXKYwoKQkbOURZ_4() const { return ___QsxvkxJlxUAhMGXKYwoKQkbOURZ_4; }
	inline TouchDataU5BU5D_tFC196EF962568BF4100D8AD3E86E59E35926F03D** get_address_of_QsxvkxJlxUAhMGXKYwoKQkbOURZ_4() { return &___QsxvkxJlxUAhMGXKYwoKQkbOURZ_4; }
	inline void set_QsxvkxJlxUAhMGXKYwoKQkbOURZ_4(TouchDataU5BU5D_tFC196EF962568BF4100D8AD3E86E59E35926F03D* value)
	{
		___QsxvkxJlxUAhMGXKYwoKQkbOURZ_4 = value;
		Il2CppCodeGenWriteBarrier((&___QsxvkxJlxUAhMGXKYwoKQkbOURZ_4), value);
	}

	inline static int32_t get_offset_of_agqHqDPvTEqPJElFDveRIhPWjClF_5() { return static_cast<int32_t>(offsetof(HIDTouchpad_tB50BF65E87F43683093CD5B26D9ED251664532C0, ___agqHqDPvTEqPJElFDveRIhPWjClF_5)); }
	inline Action_2_t7A013AB8DF7D757FD04ED42C96A7751D8A139398 * get_agqHqDPvTEqPJElFDveRIhPWjClF_5() const { return ___agqHqDPvTEqPJElFDveRIhPWjClF_5; }
	inline Action_2_t7A013AB8DF7D757FD04ED42C96A7751D8A139398 ** get_address_of_agqHqDPvTEqPJElFDveRIhPWjClF_5() { return &___agqHqDPvTEqPJElFDveRIhPWjClF_5; }
	inline void set_agqHqDPvTEqPJElFDveRIhPWjClF_5(Action_2_t7A013AB8DF7D757FD04ED42C96A7751D8A139398 * value)
	{
		___agqHqDPvTEqPJElFDveRIhPWjClF_5 = value;
		Il2CppCodeGenWriteBarrier((&___agqHqDPvTEqPJElFDveRIhPWjClF_5), value);
	}

	inline static int32_t get_offset_of_values_6() { return static_cast<int32_t>(offsetof(HIDTouchpad_tB50BF65E87F43683093CD5B26D9ED251664532C0, ___values_6)); }
	inline TouchDataU5BU5D_tFC196EF962568BF4100D8AD3E86E59E35926F03D* get_values_6() const { return ___values_6; }
	inline TouchDataU5BU5D_tFC196EF962568BF4100D8AD3E86E59E35926F03D** get_address_of_values_6() { return &___values_6; }
	inline void set_values_6(TouchDataU5BU5D_tFC196EF962568BF4100D8AD3E86E59E35926F03D* value)
	{
		___values_6 = value;
		Il2CppCodeGenWriteBarrier((&___values_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDTOUCHPAD_TB50BF65E87F43683093CD5B26D9ED251664532C0_H
#ifndef TOUCHDATA_T385EDA338DB337DD4443411767E7AE83BDE51AA7_H
#define TOUCHDATA_T385EDA338DB337DD4443411767E7AE83BDE51AA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDTouchpad_TouchData
struct  TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7 
{
public:
	// System.Int32 Rewired.HID.HIDTouchpad_TouchData::touchId
	int32_t ___touchId_0;
	// System.Single Rewired.HID.HIDTouchpad_TouchData::timeStamp
	float ___timeStamp_1;
	// System.Boolean Rewired.HID.HIDTouchpad_TouchData::isTouching
	bool ___isTouching_2;
	// System.Int32 Rewired.HID.HIDTouchpad_TouchData::positionRawX
	int32_t ___positionRawX_3;
	// System.Int32 Rewired.HID.HIDTouchpad_TouchData::positionRawY
	int32_t ___positionRawY_4;
	// System.Single Rewired.HID.HIDTouchpad_TouchData::positionX
	float ___positionX_5;
	// System.Single Rewired.HID.HIDTouchpad_TouchData::positionY
	float ___positionY_6;
	// System.Int32 Rewired.HID.HIDTouchpad_TouchData::positionAbsX
	int32_t ___positionAbsX_7;
	// System.Int32 Rewired.HID.HIDTouchpad_TouchData::positionAbsY
	int32_t ___positionAbsY_8;

public:
	inline static int32_t get_offset_of_touchId_0() { return static_cast<int32_t>(offsetof(TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7, ___touchId_0)); }
	inline int32_t get_touchId_0() const { return ___touchId_0; }
	inline int32_t* get_address_of_touchId_0() { return &___touchId_0; }
	inline void set_touchId_0(int32_t value)
	{
		___touchId_0 = value;
	}

	inline static int32_t get_offset_of_timeStamp_1() { return static_cast<int32_t>(offsetof(TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7, ___timeStamp_1)); }
	inline float get_timeStamp_1() const { return ___timeStamp_1; }
	inline float* get_address_of_timeStamp_1() { return &___timeStamp_1; }
	inline void set_timeStamp_1(float value)
	{
		___timeStamp_1 = value;
	}

	inline static int32_t get_offset_of_isTouching_2() { return static_cast<int32_t>(offsetof(TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7, ___isTouching_2)); }
	inline bool get_isTouching_2() const { return ___isTouching_2; }
	inline bool* get_address_of_isTouching_2() { return &___isTouching_2; }
	inline void set_isTouching_2(bool value)
	{
		___isTouching_2 = value;
	}

	inline static int32_t get_offset_of_positionRawX_3() { return static_cast<int32_t>(offsetof(TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7, ___positionRawX_3)); }
	inline int32_t get_positionRawX_3() const { return ___positionRawX_3; }
	inline int32_t* get_address_of_positionRawX_3() { return &___positionRawX_3; }
	inline void set_positionRawX_3(int32_t value)
	{
		___positionRawX_3 = value;
	}

	inline static int32_t get_offset_of_positionRawY_4() { return static_cast<int32_t>(offsetof(TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7, ___positionRawY_4)); }
	inline int32_t get_positionRawY_4() const { return ___positionRawY_4; }
	inline int32_t* get_address_of_positionRawY_4() { return &___positionRawY_4; }
	inline void set_positionRawY_4(int32_t value)
	{
		___positionRawY_4 = value;
	}

	inline static int32_t get_offset_of_positionX_5() { return static_cast<int32_t>(offsetof(TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7, ___positionX_5)); }
	inline float get_positionX_5() const { return ___positionX_5; }
	inline float* get_address_of_positionX_5() { return &___positionX_5; }
	inline void set_positionX_5(float value)
	{
		___positionX_5 = value;
	}

	inline static int32_t get_offset_of_positionY_6() { return static_cast<int32_t>(offsetof(TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7, ___positionY_6)); }
	inline float get_positionY_6() const { return ___positionY_6; }
	inline float* get_address_of_positionY_6() { return &___positionY_6; }
	inline void set_positionY_6(float value)
	{
		___positionY_6 = value;
	}

	inline static int32_t get_offset_of_positionAbsX_7() { return static_cast<int32_t>(offsetof(TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7, ___positionAbsX_7)); }
	inline int32_t get_positionAbsX_7() const { return ___positionAbsX_7; }
	inline int32_t* get_address_of_positionAbsX_7() { return &___positionAbsX_7; }
	inline void set_positionAbsX_7(int32_t value)
	{
		___positionAbsX_7 = value;
	}

	inline static int32_t get_offset_of_positionAbsY_8() { return static_cast<int32_t>(offsetof(TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7, ___positionAbsY_8)); }
	inline int32_t get_positionAbsY_8() const { return ___positionAbsY_8; }
	inline int32_t* get_address_of_positionAbsY_8() { return &___positionAbsY_8; }
	inline void set_positionAbsY_8(int32_t value)
	{
		___positionAbsY_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.HID.HIDTouchpad/TouchData
struct TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7_marshaled_pinvoke
{
	int32_t ___touchId_0;
	float ___timeStamp_1;
	int32_t ___isTouching_2;
	int32_t ___positionRawX_3;
	int32_t ___positionRawY_4;
	float ___positionX_5;
	float ___positionY_6;
	int32_t ___positionAbsX_7;
	int32_t ___positionAbsY_8;
};
// Native definition for COM marshalling of Rewired.HID.HIDTouchpad/TouchData
struct TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7_marshaled_com
{
	int32_t ___touchId_0;
	float ___timeStamp_1;
	int32_t ___isTouching_2;
	int32_t ___positionRawX_3;
	int32_t ___positionRawY_4;
	float ___positionX_5;
	float ___positionY_6;
	int32_t ___positionAbsX_7;
	int32_t ___positionAbsY_8;
};
#endif // TOUCHDATA_T385EDA338DB337DD4443411767E7AE83BDE51AA7_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#define NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int64>
struct  Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 
{
public:
	// T System.Nullable`1::value
	int64_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef JZMNORPZOILIJZDTJTCDZONMBEDI_T861B1435A56D8ADF47C2E099EBABBBD104B0E2B8_H
#define JZMNORPZOILIJZDTJTCDZONMBEDI_T861B1435A56D8ADF47C2E099EBABBBD104B0E2B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi
struct  jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8  : public RuntimeObject
{
public:
	// System.Int32 DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::avoCosfGauXluftrTLZWMUMjMSvo
	int32_t ___avoCosfGauXluftrTLZWMUMjMSvo_0;
	// System.Int32 DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::vHhTUeEjpjXweHfcvokTPubqCBh
	int32_t ___vHhTUeEjpjXweHfcvokTPubqCBh_1;
	// System.Int32 DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::pVMgxVlhOWBbMBjIelvXUbEMRHTh
	int32_t ___pVMgxVlhOWBbMBjIelvXUbEMRHTh_2;
	// System.Guid DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::mMvnhxixERRYbVLPXEwyRrtXYwt
	Guid_t  ___mMvnhxixERRYbVLPXEwyRrtXYwt_3;
	// System.String DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::FNogqVfZDudFppBZSIvJRbuRKgJv
	String_t* ___FNogqVfZDudFppBZSIvJRbuRKgJv_4;
	// System.Int32 DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::ynRWyQCwQkkFzvADNXNoLIVFuMQ
	int32_t ___ynRWyQCwQkkFzvADNXNoLIVFuMQ_5;
	// System.String DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::mOTUgFdbiXaaBraSubWzIyhlnpm
	String_t* ___mOTUgFdbiXaaBraSubWzIyhlnpm_6;
	// System.Int32 DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::itTafNyeQoucrbhPkPsSqRewAEI
	int32_t ___itTafNyeQoucrbhPkPsSqRewAEI_7;
	// System.Int32 DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::tOaTpOwzcPDCHdCDIyYaeuASgAM
	int32_t ___tOaTpOwzcPDCHdCDIyYaeuASgAM_8;
	// System.Single[] DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::iKsRvufntavyFjWZbUibgspoftw
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___iKsRvufntavyFjWZbUibgspoftw_9;
	// System.Boolean[] DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::YuSoQESZTtPuhZKVJFBavCSGQsh
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___YuSoQESZTtPuhZKVJFBavCSGQsh_10;
	// System.Boolean[] DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::aXlEqhFZoFpFDaLNqEPWcHalryK
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___aXlEqhFZoFpFDaLNqEPWcHalryK_11;
	// System.Single[] DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::nhzyCKqFLpLsxhCUUDwhedVVBXIa
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___nhzyCKqFLpLsxhCUUDwhedVVBXIa_12;
	// System.Boolean[] DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::MVAyPoAtTKpsQbvLfEuJaoeeEfFH
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___MVAyPoAtTKpsQbvLfEuJaoeeEfFH_13;
	// Rewired.HardwareJoystickMap_InputManager DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::motlnrXwRclwbbdquGWkOEYsYFy
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 * ___motlnrXwRclwbbdquGWkOEYsYFy_14;
	// System.Boolean DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi::uTrFqghzwmUUNLMxoQSRVgfDfsj
	bool ___uTrFqghzwmUUNLMxoQSRVgfDfsj_15;

public:
	inline static int32_t get_offset_of_avoCosfGauXluftrTLZWMUMjMSvo_0() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___avoCosfGauXluftrTLZWMUMjMSvo_0)); }
	inline int32_t get_avoCosfGauXluftrTLZWMUMjMSvo_0() const { return ___avoCosfGauXluftrTLZWMUMjMSvo_0; }
	inline int32_t* get_address_of_avoCosfGauXluftrTLZWMUMjMSvo_0() { return &___avoCosfGauXluftrTLZWMUMjMSvo_0; }
	inline void set_avoCosfGauXluftrTLZWMUMjMSvo_0(int32_t value)
	{
		___avoCosfGauXluftrTLZWMUMjMSvo_0 = value;
	}

	inline static int32_t get_offset_of_vHhTUeEjpjXweHfcvokTPubqCBh_1() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___vHhTUeEjpjXweHfcvokTPubqCBh_1)); }
	inline int32_t get_vHhTUeEjpjXweHfcvokTPubqCBh_1() const { return ___vHhTUeEjpjXweHfcvokTPubqCBh_1; }
	inline int32_t* get_address_of_vHhTUeEjpjXweHfcvokTPubqCBh_1() { return &___vHhTUeEjpjXweHfcvokTPubqCBh_1; }
	inline void set_vHhTUeEjpjXweHfcvokTPubqCBh_1(int32_t value)
	{
		___vHhTUeEjpjXweHfcvokTPubqCBh_1 = value;
	}

	inline static int32_t get_offset_of_pVMgxVlhOWBbMBjIelvXUbEMRHTh_2() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___pVMgxVlhOWBbMBjIelvXUbEMRHTh_2)); }
	inline int32_t get_pVMgxVlhOWBbMBjIelvXUbEMRHTh_2() const { return ___pVMgxVlhOWBbMBjIelvXUbEMRHTh_2; }
	inline int32_t* get_address_of_pVMgxVlhOWBbMBjIelvXUbEMRHTh_2() { return &___pVMgxVlhOWBbMBjIelvXUbEMRHTh_2; }
	inline void set_pVMgxVlhOWBbMBjIelvXUbEMRHTh_2(int32_t value)
	{
		___pVMgxVlhOWBbMBjIelvXUbEMRHTh_2 = value;
	}

	inline static int32_t get_offset_of_mMvnhxixERRYbVLPXEwyRrtXYwt_3() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___mMvnhxixERRYbVLPXEwyRrtXYwt_3)); }
	inline Guid_t  get_mMvnhxixERRYbVLPXEwyRrtXYwt_3() const { return ___mMvnhxixERRYbVLPXEwyRrtXYwt_3; }
	inline Guid_t * get_address_of_mMvnhxixERRYbVLPXEwyRrtXYwt_3() { return &___mMvnhxixERRYbVLPXEwyRrtXYwt_3; }
	inline void set_mMvnhxixERRYbVLPXEwyRrtXYwt_3(Guid_t  value)
	{
		___mMvnhxixERRYbVLPXEwyRrtXYwt_3 = value;
	}

	inline static int32_t get_offset_of_FNogqVfZDudFppBZSIvJRbuRKgJv_4() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___FNogqVfZDudFppBZSIvJRbuRKgJv_4)); }
	inline String_t* get_FNogqVfZDudFppBZSIvJRbuRKgJv_4() const { return ___FNogqVfZDudFppBZSIvJRbuRKgJv_4; }
	inline String_t** get_address_of_FNogqVfZDudFppBZSIvJRbuRKgJv_4() { return &___FNogqVfZDudFppBZSIvJRbuRKgJv_4; }
	inline void set_FNogqVfZDudFppBZSIvJRbuRKgJv_4(String_t* value)
	{
		___FNogqVfZDudFppBZSIvJRbuRKgJv_4 = value;
		Il2CppCodeGenWriteBarrier((&___FNogqVfZDudFppBZSIvJRbuRKgJv_4), value);
	}

	inline static int32_t get_offset_of_ynRWyQCwQkkFzvADNXNoLIVFuMQ_5() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___ynRWyQCwQkkFzvADNXNoLIVFuMQ_5)); }
	inline int32_t get_ynRWyQCwQkkFzvADNXNoLIVFuMQ_5() const { return ___ynRWyQCwQkkFzvADNXNoLIVFuMQ_5; }
	inline int32_t* get_address_of_ynRWyQCwQkkFzvADNXNoLIVFuMQ_5() { return &___ynRWyQCwQkkFzvADNXNoLIVFuMQ_5; }
	inline void set_ynRWyQCwQkkFzvADNXNoLIVFuMQ_5(int32_t value)
	{
		___ynRWyQCwQkkFzvADNXNoLIVFuMQ_5 = value;
	}

	inline static int32_t get_offset_of_mOTUgFdbiXaaBraSubWzIyhlnpm_6() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___mOTUgFdbiXaaBraSubWzIyhlnpm_6)); }
	inline String_t* get_mOTUgFdbiXaaBraSubWzIyhlnpm_6() const { return ___mOTUgFdbiXaaBraSubWzIyhlnpm_6; }
	inline String_t** get_address_of_mOTUgFdbiXaaBraSubWzIyhlnpm_6() { return &___mOTUgFdbiXaaBraSubWzIyhlnpm_6; }
	inline void set_mOTUgFdbiXaaBraSubWzIyhlnpm_6(String_t* value)
	{
		___mOTUgFdbiXaaBraSubWzIyhlnpm_6 = value;
		Il2CppCodeGenWriteBarrier((&___mOTUgFdbiXaaBraSubWzIyhlnpm_6), value);
	}

	inline static int32_t get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_7() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___itTafNyeQoucrbhPkPsSqRewAEI_7)); }
	inline int32_t get_itTafNyeQoucrbhPkPsSqRewAEI_7() const { return ___itTafNyeQoucrbhPkPsSqRewAEI_7; }
	inline int32_t* get_address_of_itTafNyeQoucrbhPkPsSqRewAEI_7() { return &___itTafNyeQoucrbhPkPsSqRewAEI_7; }
	inline void set_itTafNyeQoucrbhPkPsSqRewAEI_7(int32_t value)
	{
		___itTafNyeQoucrbhPkPsSqRewAEI_7 = value;
	}

	inline static int32_t get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_8() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___tOaTpOwzcPDCHdCDIyYaeuASgAM_8)); }
	inline int32_t get_tOaTpOwzcPDCHdCDIyYaeuASgAM_8() const { return ___tOaTpOwzcPDCHdCDIyYaeuASgAM_8; }
	inline int32_t* get_address_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_8() { return &___tOaTpOwzcPDCHdCDIyYaeuASgAM_8; }
	inline void set_tOaTpOwzcPDCHdCDIyYaeuASgAM_8(int32_t value)
	{
		___tOaTpOwzcPDCHdCDIyYaeuASgAM_8 = value;
	}

	inline static int32_t get_offset_of_iKsRvufntavyFjWZbUibgspoftw_9() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___iKsRvufntavyFjWZbUibgspoftw_9)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_iKsRvufntavyFjWZbUibgspoftw_9() const { return ___iKsRvufntavyFjWZbUibgspoftw_9; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_iKsRvufntavyFjWZbUibgspoftw_9() { return &___iKsRvufntavyFjWZbUibgspoftw_9; }
	inline void set_iKsRvufntavyFjWZbUibgspoftw_9(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___iKsRvufntavyFjWZbUibgspoftw_9 = value;
		Il2CppCodeGenWriteBarrier((&___iKsRvufntavyFjWZbUibgspoftw_9), value);
	}

	inline static int32_t get_offset_of_YuSoQESZTtPuhZKVJFBavCSGQsh_10() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___YuSoQESZTtPuhZKVJFBavCSGQsh_10)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_YuSoQESZTtPuhZKVJFBavCSGQsh_10() const { return ___YuSoQESZTtPuhZKVJFBavCSGQsh_10; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_YuSoQESZTtPuhZKVJFBavCSGQsh_10() { return &___YuSoQESZTtPuhZKVJFBavCSGQsh_10; }
	inline void set_YuSoQESZTtPuhZKVJFBavCSGQsh_10(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___YuSoQESZTtPuhZKVJFBavCSGQsh_10 = value;
		Il2CppCodeGenWriteBarrier((&___YuSoQESZTtPuhZKVJFBavCSGQsh_10), value);
	}

	inline static int32_t get_offset_of_aXlEqhFZoFpFDaLNqEPWcHalryK_11() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___aXlEqhFZoFpFDaLNqEPWcHalryK_11)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_aXlEqhFZoFpFDaLNqEPWcHalryK_11() const { return ___aXlEqhFZoFpFDaLNqEPWcHalryK_11; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_aXlEqhFZoFpFDaLNqEPWcHalryK_11() { return &___aXlEqhFZoFpFDaLNqEPWcHalryK_11; }
	inline void set_aXlEqhFZoFpFDaLNqEPWcHalryK_11(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___aXlEqhFZoFpFDaLNqEPWcHalryK_11 = value;
		Il2CppCodeGenWriteBarrier((&___aXlEqhFZoFpFDaLNqEPWcHalryK_11), value);
	}

	inline static int32_t get_offset_of_nhzyCKqFLpLsxhCUUDwhedVVBXIa_12() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___nhzyCKqFLpLsxhCUUDwhedVVBXIa_12)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_nhzyCKqFLpLsxhCUUDwhedVVBXIa_12() const { return ___nhzyCKqFLpLsxhCUUDwhedVVBXIa_12; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_nhzyCKqFLpLsxhCUUDwhedVVBXIa_12() { return &___nhzyCKqFLpLsxhCUUDwhedVVBXIa_12; }
	inline void set_nhzyCKqFLpLsxhCUUDwhedVVBXIa_12(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___nhzyCKqFLpLsxhCUUDwhedVVBXIa_12 = value;
		Il2CppCodeGenWriteBarrier((&___nhzyCKqFLpLsxhCUUDwhedVVBXIa_12), value);
	}

	inline static int32_t get_offset_of_MVAyPoAtTKpsQbvLfEuJaoeeEfFH_13() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___MVAyPoAtTKpsQbvLfEuJaoeeEfFH_13)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_MVAyPoAtTKpsQbvLfEuJaoeeEfFH_13() const { return ___MVAyPoAtTKpsQbvLfEuJaoeeEfFH_13; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_MVAyPoAtTKpsQbvLfEuJaoeeEfFH_13() { return &___MVAyPoAtTKpsQbvLfEuJaoeeEfFH_13; }
	inline void set_MVAyPoAtTKpsQbvLfEuJaoeeEfFH_13(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___MVAyPoAtTKpsQbvLfEuJaoeeEfFH_13 = value;
		Il2CppCodeGenWriteBarrier((&___MVAyPoAtTKpsQbvLfEuJaoeeEfFH_13), value);
	}

	inline static int32_t get_offset_of_motlnrXwRclwbbdquGWkOEYsYFy_14() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___motlnrXwRclwbbdquGWkOEYsYFy_14)); }
	inline HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 * get_motlnrXwRclwbbdquGWkOEYsYFy_14() const { return ___motlnrXwRclwbbdquGWkOEYsYFy_14; }
	inline HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 ** get_address_of_motlnrXwRclwbbdquGWkOEYsYFy_14() { return &___motlnrXwRclwbbdquGWkOEYsYFy_14; }
	inline void set_motlnrXwRclwbbdquGWkOEYsYFy_14(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 * value)
	{
		___motlnrXwRclwbbdquGWkOEYsYFy_14 = value;
		Il2CppCodeGenWriteBarrier((&___motlnrXwRclwbbdquGWkOEYsYFy_14), value);
	}

	inline static int32_t get_offset_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_15() { return static_cast<int32_t>(offsetof(jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8, ___uTrFqghzwmUUNLMxoQSRVgfDfsj_15)); }
	inline bool get_uTrFqghzwmUUNLMxoQSRVgfDfsj_15() const { return ___uTrFqghzwmUUNLMxoQSRVgfDfsj_15; }
	inline bool* get_address_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_15() { return &___uTrFqghzwmUUNLMxoQSRVgfDfsj_15; }
	inline void set_uTrFqghzwmUUNLMxoQSRVgfDfsj_15(bool value)
	{
		___uTrFqghzwmUUNLMxoQSRVgfDfsj_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JZMNORPZOILIJZDTJTCDZONMBEDI_T861B1435A56D8ADF47C2E099EBABBBD104B0E2B8_H
#ifndef RWYFLXYWRUBXVHKTQRHUWTJLFHC_TA9296F8C28B1445F23223BB190C79458BC389738_H
#define RWYFLXYWRUBXVHKTQRHUWTJLFHC_TA9296F8C28B1445F23223BB190C79458BC389738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DvbYkiORxIPlVcnxRmcWLbPwAdhB_sSSwLdgRIGhAHubEfzNAExVFpqE_rWYfLXYWrUBXvHKTQRhUWTjlfhC
struct  rWYfLXYWrUBXvHKTQRhUWTjlfhC_tA9296F8C28B1445F23223BB190C79458BC389738 
{
public:
	// System.Int32 DvbYkiORxIPlVcnxRmcWLbPwAdhB_sSSwLdgRIGhAHubEfzNAExVFpqE_rWYfLXYWrUBXvHKTQRhUWTjlfhC::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(rWYfLXYWrUBXvHKTQRhUWTjlfhC_tA9296F8C28B1445F23223BB190C79458BC389738, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RWYFLXYWRUBXVHKTQRHUWTJLFHC_TA9296F8C28B1445F23223BB190C79458BC389738_H
#ifndef ZMIZEUUVPIPPZZOULAULXMYXVEJ_T1838478BC9B4D5D798C517D8DA250BF017B9FEB5_H
#define ZMIZEUUVPIPPZZOULAULXMYXVEJ_T1838478BC9B4D5D798C517D8DA250BF017B9FEB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OmePuLXqingOShAfdEjGZVdOnDpG_EAJRBzBYjjtwooLSURpauuDghJn_zMiZeuUVPippzZoUlAuLXMyxVeJ
struct  zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 OmePuLXqingOShAfdEjGZVdOnDpG_EAJRBzBYjjtwooLSURpauuDghJn_zMiZeuUVPippzZoUlAuLXMyxVeJ::BcxgghPdhVrGYkkGtFRBBCNhtYPq
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___BcxgghPdhVrGYkkGtFRBBCNhtYPq_0;
	// UnityEngine.Vector3 OmePuLXqingOShAfdEjGZVdOnDpG_EAJRBzBYjjtwooLSURpauuDghJn_zMiZeuUVPippzZoUlAuLXMyxVeJ::TlggNiFPzcPcGmHWHZntSPnyQaj
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___TlggNiFPzcPcGmHWHZntSPnyQaj_1;
	// UnityEngine.Vector3 OmePuLXqingOShAfdEjGZVdOnDpG_EAJRBzBYjjtwooLSURpauuDghJn_zMiZeuUVPippzZoUlAuLXMyxVeJ::uLaCafIsJgBVaUtOsrrFOcozUPA
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___uLaCafIsJgBVaUtOsrrFOcozUPA_2;

public:
	inline static int32_t get_offset_of_BcxgghPdhVrGYkkGtFRBBCNhtYPq_0() { return static_cast<int32_t>(offsetof(zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5, ___BcxgghPdhVrGYkkGtFRBBCNhtYPq_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_BcxgghPdhVrGYkkGtFRBBCNhtYPq_0() const { return ___BcxgghPdhVrGYkkGtFRBBCNhtYPq_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_BcxgghPdhVrGYkkGtFRBBCNhtYPq_0() { return &___BcxgghPdhVrGYkkGtFRBBCNhtYPq_0; }
	inline void set_BcxgghPdhVrGYkkGtFRBBCNhtYPq_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___BcxgghPdhVrGYkkGtFRBBCNhtYPq_0 = value;
	}

	inline static int32_t get_offset_of_TlggNiFPzcPcGmHWHZntSPnyQaj_1() { return static_cast<int32_t>(offsetof(zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5, ___TlggNiFPzcPcGmHWHZntSPnyQaj_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_TlggNiFPzcPcGmHWHZntSPnyQaj_1() const { return ___TlggNiFPzcPcGmHWHZntSPnyQaj_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_TlggNiFPzcPcGmHWHZntSPnyQaj_1() { return &___TlggNiFPzcPcGmHWHZntSPnyQaj_1; }
	inline void set_TlggNiFPzcPcGmHWHZntSPnyQaj_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___TlggNiFPzcPcGmHWHZntSPnyQaj_1 = value;
	}

	inline static int32_t get_offset_of_uLaCafIsJgBVaUtOsrrFOcozUPA_2() { return static_cast<int32_t>(offsetof(zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5, ___uLaCafIsJgBVaUtOsrrFOcozUPA_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_uLaCafIsJgBVaUtOsrrFOcozUPA_2() const { return ___uLaCafIsJgBVaUtOsrrFOcozUPA_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_uLaCafIsJgBVaUtOsrrFOcozUPA_2() { return &___uLaCafIsJgBVaUtOsrrFOcozUPA_2; }
	inline void set_uLaCafIsJgBVaUtOsrrFOcozUPA_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___uLaCafIsJgBVaUtOsrrFOcozUPA_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZMIZEUUVPIPPZZOULAULXMYXVEJ_T1838478BC9B4D5D798C517D8DA250BF017B9FEB5_H
#ifndef CZAXOBKPCTTFHFGFXSKPDDMUAQS_T1317D0DE87A339014626829F75A1491BC984394C_H
#define CZAXOBKPCTTFHFGFXSKPDDMUAQS_T1317D0DE87A339014626829F75A1491BC984394C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OmePuLXqingOShAfdEjGZVdOnDpG_cZaxobkPCTtFHFGFxSkpddMuaQs
struct  cZaxobkPCTtFHFGFxSkpddMuaQs_t1317D0DE87A339014626829F75A1491BC984394C 
{
public:
	// System.Int32 OmePuLXqingOShAfdEjGZVdOnDpG_cZaxobkPCTtFHFGFxSkpddMuaQs::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(cZaxobkPCTtFHFGFxSkpddMuaQs_t1317D0DE87A339014626829F75A1491BC984394C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CZAXOBKPCTTFHFGFXSKPDDMUAQS_T1317D0DE87A339014626829F75A1491BC984394C_H
#ifndef AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#define AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisCoordinateMode
struct  AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868 
{
public:
	// System.Int32 Rewired.AxisCoordinateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisCoordinateMode_t2A084CBA363502B3ECFD5F23446BEC1BA5647868, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCOORDINATEMODE_T2A084CBA363502B3ECFD5F23446BEC1BA5647868_H
#ifndef AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#define AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisRange
struct  AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652 
{
public:
	// System.Int32 Rewired.AxisRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifndef AXISSENSITIVITY2DTYPE_T0406339027EB595B27EA4C5E9A2A3F7F67FEF36B_H
#define AXISSENSITIVITY2DTYPE_T0406339027EB595B27EA4C5E9A2A3F7F67FEF36B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisSensitivity2DType
struct  AxisSensitivity2DType_t0406339027EB595B27EA4C5E9A2A3F7F67FEF36B 
{
public:
	// System.Int32 Rewired.AxisSensitivity2DType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisSensitivity2DType_t0406339027EB595B27EA4C5E9A2A3F7F67FEF36B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSENSITIVITY2DTYPE_T0406339027EB595B27EA4C5E9A2A3F7F67FEF36B_H
#ifndef AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#define AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisSensitivityType
struct  AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A 
{
public:
	// System.Int32 Rewired.AxisSensitivityType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#ifndef BUTTONSTATEFLAGS_TE48083CA4AA8079D0A5006CD787C46510592624A_H
#define BUTTONSTATEFLAGS_TE48083CA4AA8079D0A5006CD787C46510592624A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ButtonStateFlags
struct  ButtonStateFlags_tE48083CA4AA8079D0A5006CD787C46510592624A 
{
public:
	// System.Int32 Rewired.ButtonStateFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStateFlags_tE48083CA4AA8079D0A5006CD787C46510592624A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATEFLAGS_TE48083CA4AA8079D0A5006CD787C46510592624A_H
#ifndef COMPOUNDCONTROLLERELEMENTTYPE_T38C73D05390CC2625D0760EB81BACFDEC36EB6B2_H
#define COMPOUNDCONTROLLERELEMENTTYPE_T38C73D05390CC2625D0760EB81BACFDEC36EB6B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.CompoundControllerElementType
struct  CompoundControllerElementType_t38C73D05390CC2625D0760EB81BACFDEC36EB6B2 
{
public:
	// System.Int32 Rewired.CompoundControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompoundControllerElementType_t38C73D05390CC2625D0760EB81BACFDEC36EB6B2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDCONTROLLERELEMENTTYPE_T38C73D05390CC2625D0760EB81BACFDEC36EB6B2_H
#ifndef UPDATELOOPSETTING_T813FEF786E2423CBB40457E67ABB4712DA865CA5_H
#define UPDATELOOPSETTING_T813FEF786E2423CBB40457E67ABB4712DA865CA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Config.UpdateLoopSetting
struct  UpdateLoopSetting_t813FEF786E2423CBB40457E67ABB4712DA865CA5 
{
public:
	// System.Int32 Rewired.Config.UpdateLoopSetting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateLoopSetting_t813FEF786E2423CBB40457E67ABB4712DA865CA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATELOOPSETTING_T813FEF786E2423CBB40457E67ABB4712DA865CA5_H
#ifndef CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#define CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerElementType
struct  ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA 
{
public:
	// System.Int32 Rewired.ControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifndef CONTROLLERTEMPLATEELEMENTTYPE_T64649CE1F95852B8211119A5D65EE4ACDA0C49D2_H
#define CONTROLLERTEMPLATEELEMENTTYPE_T64649CE1F95852B8211119A5D65EE4ACDA0C49D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplateElementType
struct  ControllerTemplateElementType_t64649CE1F95852B8211119A5D65EE4ACDA0C49D2 
{
public:
	// System.Int32 Rewired.ControllerTemplateElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerTemplateElementType_t64649CE1F95852B8211119A5D65EE4ACDA0C49D2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEELEMENTTYPE_T64649CE1F95852B8211119A5D65EE4ACDA0C49D2_H
#ifndef CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#define CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerType
struct  ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4 
{
public:
	// System.Int32 Rewired.ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifndef ALTERNATEAXISCALIBRATIONTYPE_T13E810E9252891A41BDB4417F660AE3C9EEA526F_H
#define ALTERNATEAXISCALIBRATIONTYPE_T13E810E9252891A41BDB4417F660AE3C9EEA526F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.AlternateAxisCalibrationType
struct  AlternateAxisCalibrationType_t13E810E9252891A41BDB4417F660AE3C9EEA526F 
{
public:
	// System.Int32 Rewired.Data.Mapping.AlternateAxisCalibrationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlternateAxisCalibrationType_t13E810E9252891A41BDB4417F660AE3C9EEA526F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALTERNATEAXISCALIBRATIONTYPE_T13E810E9252891A41BDB4417F660AE3C9EEA526F_H
#ifndef DEADZONE2DTYPE_TBD26291A70136F11C062AAAC6A6257423809B586_H
#define DEADZONE2DTYPE_TBD26291A70136F11C062AAAC6A6257423809B586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.DeadZone2DType
struct  DeadZone2DType_tBD26291A70136F11C062AAAC6A6257423809B586 
{
public:
	// System.Int32 Rewired.DeadZone2DType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeadZone2DType_tBD26291A70136F11C062AAAC6A6257423809B586, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEADZONE2DTYPE_TBD26291A70136F11C062AAAC6A6257423809B586_H
#ifndef DEVICECONNECTIONTYPE_T4A8B5BD929AC202D84804E92063E6F53C7ABDB25_H
#define DEVICECONNECTIONTYPE_T4A8B5BD929AC202D84804E92063E6F53C7ABDB25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.DeviceConnectionType
struct  DeviceConnectionType_t4A8B5BD929AC202D84804E92063E6F53C7ABDB25 
{
public:
	// System.Int32 Rewired.HID.DeviceConnectionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeviceConnectionType_t4A8B5BD929AC202D84804E92063E6F53C7ABDB25, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICECONNECTIONTYPE_T4A8B5BD929AC202D84804E92063E6F53C7ABDB25_H
#ifndef DFGZRWVJTFUSZPHIPRBTLQZNLZK_T5948285F74620B4781E1B062543C1AEB806E7329_H
#define DFGZRWVJTFUSZPHIPRBTLQZNLZK_T5948285F74620B4781E1B062543C1AEB806E7329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.Drivers.DualShock4Driver_dfGZrwvjtfUsZphIPrbTLQZNLZk
struct  dfGZrwvjtfUsZphIPrbTLQZNLZk_t5948285F74620B4781E1B062543C1AEB806E7329 
{
public:
	// System.Int32 Rewired.HID.Drivers.DualShock4Driver_dfGZrwvjtfUsZphIPrbTLQZNLZk::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(dfGZrwvjtfUsZphIPrbTLQZNLZk_t5948285F74620B4781E1B062543C1AEB806E7329, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DFGZRWVJTFUSZPHIPRBTLQZNLZK_T5948285F74620B4781E1B062543C1AEB806E7329_H
#ifndef DNJPJQRXIQTAGDQOHABRWCSNLGZ_TEAAA8E42BD0166A0B52A03C9394D9A67C9143C0E_H
#define DNJPJQRXIQTAGDQOHABRWCSNLGZ_TEAAA8E42BD0166A0B52A03C9394D9A67C9143C0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.Drivers.DualShock4Driver_dnjPjQRXIqTAGDQohaBrWcsnlgz
struct  dnjPjQRXIqTAGDQohaBrWcsnlgz_tEAAA8E42BD0166A0B52A03C9394D9A67C9143C0E 
{
public:
	// System.Int32 Rewired.HID.Drivers.DualShock4Driver_dnjPjQRXIqTAGDQohaBrWcsnlgz::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(dnjPjQRXIqTAGDQohaBrWcsnlgz_tEAAA8E42BD0166A0B52A03C9394D9A67C9143C0E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNJPJQRXIQTAGDQOHABRWCSNLGZ_TEAAA8E42BD0166A0B52A03C9394D9A67C9143C0E_H
#ifndef HZMKPWAHMJTXMHPJEEADSVUQVFN_TD0A3554765214F4827E150DD368FD87254B3B60C_H
#define HZMKPWAHMJTXMHPJEEADSVUQVFN_TD0A3554765214F4827E150DD368FD87254B3B60C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.Drivers.RailDriverDriver_hzMKpWAHmjTXmhpJeEaDSvuQvfn
struct  hzMKpWAHmjTXmhpJeEaDSvuQvfn_tD0A3554765214F4827E150DD368FD87254B3B60C 
{
public:
	// System.Int32 Rewired.HID.Drivers.RailDriverDriver_hzMKpWAHmjTXmhpJeEaDSvuQvfn::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(hzMKpWAHmjTXmhpJeEaDSvuQvfn_tD0A3554765214F4827E150DD368FD87254B3B60C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HZMKPWAHMJTXMHPJEEADSVUQVFN_TD0A3554765214F4827E150DD368FD87254B3B60C_H
#ifndef HIDGYROSCOPE_T1042E70E0081BB80264CF6FDA5E73C4F79250220_H
#define HIDGYROSCOPE_T1042E70E0081BB80264CF6FDA5E73C4F79250220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDGyroscope
struct  HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220  : public HIDControllerElementWithDataSet_t991E71547369AF5788D0C1935AC633A2A0128775
{
public:
	// System.Single Rewired.HID.HIDGyroscope::timestamp
	float ___timestamp_3;
	// System.Single[] Rewired.HID.HIDGyroscope::lastRawValue
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___lastRawValue_4;
	// System.Int32 Rewired.HID.HIDGyroscope::valueLength
	int32_t ___valueLength_5;
	// System.Byte[] Rewired.HID.HIDGyroscope::NRwCWKPZMBPqSTcLtLnOurBUoWN
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___NRwCWKPZMBPqSTcLtLnOurBUoWN_6;
	// System.Single[] Rewired.HID.HIDGyroscope::nfXJRuyZzhrVGDTDLHeNPPEfijfA
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___nfXJRuyZzhrVGDTDLHeNPPEfijfA_7;
	// System.Int32 Rewired.HID.HIDGyroscope::MXKCkdMwCxEJlCsqhAxVfcBDfLde
	int32_t ___MXKCkdMwCxEJlCsqhAxVfcBDfLde_8;
	// System.Int32 Rewired.HID.HIDGyroscope::AfklhFSjQRdMZXJChQYWNazMHbq
	int32_t ___AfklhFSjQRdMZXJChQYWNazMHbq_9;
	// System.Action`2<System.Byte[],System.Single[]> Rewired.HID.HIDGyroscope::agqHqDPvTEqPJElFDveRIhPWjClF
	Action_2_t6348996BC3E0C8743F528E4FA9E5118FC95A1FEB * ___agqHqDPvTEqPJElFDveRIhPWjClF_10;
	// System.Func`1<System.Single> Rewired.HID.HIDGyroscope::jAVLkMiBPrMxvfdCjoUzCmsmHBY
	Func_1_tA655F82C43D9C03F75E783D0DF2629CF9676E735 * ___jAVLkMiBPrMxvfdCjoUzCmsmHBY_11;

public:
	inline static int32_t get_offset_of_timestamp_3() { return static_cast<int32_t>(offsetof(HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220, ___timestamp_3)); }
	inline float get_timestamp_3() const { return ___timestamp_3; }
	inline float* get_address_of_timestamp_3() { return &___timestamp_3; }
	inline void set_timestamp_3(float value)
	{
		___timestamp_3 = value;
	}

	inline static int32_t get_offset_of_lastRawValue_4() { return static_cast<int32_t>(offsetof(HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220, ___lastRawValue_4)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_lastRawValue_4() const { return ___lastRawValue_4; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_lastRawValue_4() { return &___lastRawValue_4; }
	inline void set_lastRawValue_4(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___lastRawValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___lastRawValue_4), value);
	}

	inline static int32_t get_offset_of_valueLength_5() { return static_cast<int32_t>(offsetof(HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220, ___valueLength_5)); }
	inline int32_t get_valueLength_5() const { return ___valueLength_5; }
	inline int32_t* get_address_of_valueLength_5() { return &___valueLength_5; }
	inline void set_valueLength_5(int32_t value)
	{
		___valueLength_5 = value;
	}

	inline static int32_t get_offset_of_NRwCWKPZMBPqSTcLtLnOurBUoWN_6() { return static_cast<int32_t>(offsetof(HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220, ___NRwCWKPZMBPqSTcLtLnOurBUoWN_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_NRwCWKPZMBPqSTcLtLnOurBUoWN_6() const { return ___NRwCWKPZMBPqSTcLtLnOurBUoWN_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_NRwCWKPZMBPqSTcLtLnOurBUoWN_6() { return &___NRwCWKPZMBPqSTcLtLnOurBUoWN_6; }
	inline void set_NRwCWKPZMBPqSTcLtLnOurBUoWN_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___NRwCWKPZMBPqSTcLtLnOurBUoWN_6 = value;
		Il2CppCodeGenWriteBarrier((&___NRwCWKPZMBPqSTcLtLnOurBUoWN_6), value);
	}

	inline static int32_t get_offset_of_nfXJRuyZzhrVGDTDLHeNPPEfijfA_7() { return static_cast<int32_t>(offsetof(HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220, ___nfXJRuyZzhrVGDTDLHeNPPEfijfA_7)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_nfXJRuyZzhrVGDTDLHeNPPEfijfA_7() const { return ___nfXJRuyZzhrVGDTDLHeNPPEfijfA_7; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_nfXJRuyZzhrVGDTDLHeNPPEfijfA_7() { return &___nfXJRuyZzhrVGDTDLHeNPPEfijfA_7; }
	inline void set_nfXJRuyZzhrVGDTDLHeNPPEfijfA_7(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___nfXJRuyZzhrVGDTDLHeNPPEfijfA_7 = value;
		Il2CppCodeGenWriteBarrier((&___nfXJRuyZzhrVGDTDLHeNPPEfijfA_7), value);
	}

	inline static int32_t get_offset_of_MXKCkdMwCxEJlCsqhAxVfcBDfLde_8() { return static_cast<int32_t>(offsetof(HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220, ___MXKCkdMwCxEJlCsqhAxVfcBDfLde_8)); }
	inline int32_t get_MXKCkdMwCxEJlCsqhAxVfcBDfLde_8() const { return ___MXKCkdMwCxEJlCsqhAxVfcBDfLde_8; }
	inline int32_t* get_address_of_MXKCkdMwCxEJlCsqhAxVfcBDfLde_8() { return &___MXKCkdMwCxEJlCsqhAxVfcBDfLde_8; }
	inline void set_MXKCkdMwCxEJlCsqhAxVfcBDfLde_8(int32_t value)
	{
		___MXKCkdMwCxEJlCsqhAxVfcBDfLde_8 = value;
	}

	inline static int32_t get_offset_of_AfklhFSjQRdMZXJChQYWNazMHbq_9() { return static_cast<int32_t>(offsetof(HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220, ___AfklhFSjQRdMZXJChQYWNazMHbq_9)); }
	inline int32_t get_AfklhFSjQRdMZXJChQYWNazMHbq_9() const { return ___AfklhFSjQRdMZXJChQYWNazMHbq_9; }
	inline int32_t* get_address_of_AfklhFSjQRdMZXJChQYWNazMHbq_9() { return &___AfklhFSjQRdMZXJChQYWNazMHbq_9; }
	inline void set_AfklhFSjQRdMZXJChQYWNazMHbq_9(int32_t value)
	{
		___AfklhFSjQRdMZXJChQYWNazMHbq_9 = value;
	}

	inline static int32_t get_offset_of_agqHqDPvTEqPJElFDveRIhPWjClF_10() { return static_cast<int32_t>(offsetof(HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220, ___agqHqDPvTEqPJElFDveRIhPWjClF_10)); }
	inline Action_2_t6348996BC3E0C8743F528E4FA9E5118FC95A1FEB * get_agqHqDPvTEqPJElFDveRIhPWjClF_10() const { return ___agqHqDPvTEqPJElFDveRIhPWjClF_10; }
	inline Action_2_t6348996BC3E0C8743F528E4FA9E5118FC95A1FEB ** get_address_of_agqHqDPvTEqPJElFDveRIhPWjClF_10() { return &___agqHqDPvTEqPJElFDveRIhPWjClF_10; }
	inline void set_agqHqDPvTEqPJElFDveRIhPWjClF_10(Action_2_t6348996BC3E0C8743F528E4FA9E5118FC95A1FEB * value)
	{
		___agqHqDPvTEqPJElFDveRIhPWjClF_10 = value;
		Il2CppCodeGenWriteBarrier((&___agqHqDPvTEqPJElFDveRIhPWjClF_10), value);
	}

	inline static int32_t get_offset_of_jAVLkMiBPrMxvfdCjoUzCmsmHBY_11() { return static_cast<int32_t>(offsetof(HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220, ___jAVLkMiBPrMxvfdCjoUzCmsmHBY_11)); }
	inline Func_1_tA655F82C43D9C03F75E783D0DF2629CF9676E735 * get_jAVLkMiBPrMxvfdCjoUzCmsmHBY_11() const { return ___jAVLkMiBPrMxvfdCjoUzCmsmHBY_11; }
	inline Func_1_tA655F82C43D9C03F75E783D0DF2629CF9676E735 ** get_address_of_jAVLkMiBPrMxvfdCjoUzCmsmHBY_11() { return &___jAVLkMiBPrMxvfdCjoUzCmsmHBY_11; }
	inline void set_jAVLkMiBPrMxvfdCjoUzCmsmHBY_11(Func_1_tA655F82C43D9C03F75E783D0DF2629CF9676E735 * value)
	{
		___jAVLkMiBPrMxvfdCjoUzCmsmHBY_11 = value;
		Il2CppCodeGenWriteBarrier((&___jAVLkMiBPrMxvfdCjoUzCmsmHBY_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDGYROSCOPE_T1042E70E0081BB80264CF6FDA5E73C4F79250220_H
#ifndef HOIXDORKACXNJHEVDXCUSVQOBDH_T2E4948874796211EAC2BEAF1E0D53EC71073A135_H
#define HOIXDORKACXNJHEVDXCUSVQOBDH_T2E4948874796211EAC2BEAF1E0D53EC71073A135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDGyroscope_hoiXDorKAcXnjheVDxCUSVqObdh
struct  hoiXDorKAcXnjheVDxCUSVqObdh_t2E4948874796211EAC2BEAF1E0D53EC71073A135  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Rewired.HID.HIDGyroscope_hoiXDorKAcXnjheVDxCUSVqObdh::HrLhhDFKAQCiJIauDwTQdshRCqV
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___HrLhhDFKAQCiJIauDwTQdshRCqV_0;
	// System.Single Rewired.HID.HIDGyroscope_hoiXDorKAcXnjheVDxCUSVqObdh::qKAHqFHSelUbirCSxInOiDPbgyyc
	float ___qKAHqFHSelUbirCSxInOiDPbgyyc_1;

public:
	inline static int32_t get_offset_of_HrLhhDFKAQCiJIauDwTQdshRCqV_0() { return static_cast<int32_t>(offsetof(hoiXDorKAcXnjheVDxCUSVqObdh_t2E4948874796211EAC2BEAF1E0D53EC71073A135, ___HrLhhDFKAQCiJIauDwTQdshRCqV_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_HrLhhDFKAQCiJIauDwTQdshRCqV_0() const { return ___HrLhhDFKAQCiJIauDwTQdshRCqV_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_HrLhhDFKAQCiJIauDwTQdshRCqV_0() { return &___HrLhhDFKAQCiJIauDwTQdshRCqV_0; }
	inline void set_HrLhhDFKAQCiJIauDwTQdshRCqV_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___HrLhhDFKAQCiJIauDwTQdshRCqV_0 = value;
	}

	inline static int32_t get_offset_of_qKAHqFHSelUbirCSxInOiDPbgyyc_1() { return static_cast<int32_t>(offsetof(hoiXDorKAcXnjheVDxCUSVqObdh_t2E4948874796211EAC2BEAF1E0D53EC71073A135, ___qKAHqFHSelUbirCSxInOiDPbgyyc_1)); }
	inline float get_qKAHqFHSelUbirCSxInOiDPbgyyc_1() const { return ___qKAHqFHSelUbirCSxInOiDPbgyyc_1; }
	inline float* get_address_of_qKAHqFHSelUbirCSxInOiDPbgyyc_1() { return &___qKAHqFHSelUbirCSxInOiDPbgyyc_1; }
	inline void set_qKAHqFHSelUbirCSxInOiDPbgyyc_1(float value)
	{
		___qKAHqFHSelUbirCSxInOiDPbgyyc_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOIXDORKACXNJHEVDXCUSVQOBDH_T2E4948874796211EAC2BEAF1E0D53EC71073A135_H
#ifndef TYPE_T872FDC137DC2F491C6B457F36C652F61641ADFCF_H
#define TYPE_T872FDC137DC2F491C6B457F36C652F61641ADFCF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDHat_Type
struct  Type_t872FDC137DC2F491C6B457F36C652F61641ADFCF 
{
public:
	// System.Int32 Rewired.HID.HIDHat_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t872FDC137DC2F491C6B457F36C652F61641ADFCF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T872FDC137DC2F491C6B457F36C652F61641ADFCF_H
#ifndef OUTPUTREPORTOPTIONS_T8F0DDE8BA800FA9FBE0634A7A5D64439C2BEE3CF_H
#define OUTPUTREPORTOPTIONS_T8F0DDE8BA800FA9FBE0634A7A5D64439C2BEE3CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.OutputReportOptions
struct  OutputReportOptions_t8F0DDE8BA800FA9FBE0634A7A5D64439C2BEE3CF 
{
public:
	// System.Int32 Rewired.HID.OutputReportOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OutputReportOptions_t8F0DDE8BA800FA9FBE0634A7A5D64439C2BEE3CF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTREPORTOPTIONS_T8F0DDE8BA800FA9FBE0634A7A5D64439C2BEE3CF_H
#ifndef EXCEPTIONPOINT_T3DC2DA588E23852E8B0D31F5FF7073AE412F8CA5_H
#define EXCEPTIONPOINT_T3DC2DA588E23852E8B0D31F5FF7073AE412F8CA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputManager_Base_ExceptionPoint
struct  ExceptionPoint_t3DC2DA588E23852E8B0D31F5FF7073AE412F8CA5 
{
public:
	// System.Int32 Rewired.InputManager_Base_ExceptionPoint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExceptionPoint_t3DC2DA588E23852E8B0D31F5FF7073AE412F8CA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONPOINT_T3DC2DA588E23852E8B0D31F5FF7073AE412F8CA5_H
#ifndef SAPQXZQJGZLMSJABMWGSMLJHEOU_T39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4_H
#define SAPQXZQJGZLMSJABMWGSMLJHEOU_T39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU_SAPqXzqjGZlmsJaBMWgsmLJhEOu
struct  SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4  : public RuntimeObject
{
public:
	// System.Int32 Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU_SAPqXzqjGZlmsJaBMWgsmLJhEOu::nvorGNbxnIKRTEQakQRxoDaDbkF
	int32_t ___nvorGNbxnIKRTEQakQRxoDaDbkF_0;
	// System.Nullable`1<System.Int64> Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU_SAPqXzqjGZlmsJaBMWgsmLJhEOu::TbyfgvoLcvCnLzBZpOVXBdPAtwA
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___TbyfgvoLcvCnLzBZpOVXBdPAtwA_1;
	// System.String Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU_SAPqXzqjGZlmsJaBMWgsmLJhEOu::lXiIYlzqhDMJpyJyatjvSnwcuzH
	String_t* ___lXiIYlzqhDMJpyJyatjvSnwcuzH_2;
	// System.Int32 Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU_SAPqXzqjGZlmsJaBMWgsmLJhEOu::fYYSPmKTkyNnvcLPwwaCPYkcWqv
	int32_t ___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3;
	// System.Int32 Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU_SAPqXzqjGZlmsJaBMWgsmLJhEOu::tOaTpOwzcPDCHdCDIyYaeuASgAM
	int32_t ___tOaTpOwzcPDCHdCDIyYaeuASgAM_4;
	// System.Int32 Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU_SAPqXzqjGZlmsJaBMWgsmLJhEOu::itTafNyeQoucrbhPkPsSqRewAEI
	int32_t ___itTafNyeQoucrbhPkPsSqRewAEI_5;

public:
	inline static int32_t get_offset_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0() { return static_cast<int32_t>(offsetof(SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4, ___nvorGNbxnIKRTEQakQRxoDaDbkF_0)); }
	inline int32_t get_nvorGNbxnIKRTEQakQRxoDaDbkF_0() const { return ___nvorGNbxnIKRTEQakQRxoDaDbkF_0; }
	inline int32_t* get_address_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0() { return &___nvorGNbxnIKRTEQakQRxoDaDbkF_0; }
	inline void set_nvorGNbxnIKRTEQakQRxoDaDbkF_0(int32_t value)
	{
		___nvorGNbxnIKRTEQakQRxoDaDbkF_0 = value;
	}

	inline static int32_t get_offset_of_TbyfgvoLcvCnLzBZpOVXBdPAtwA_1() { return static_cast<int32_t>(offsetof(SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4, ___TbyfgvoLcvCnLzBZpOVXBdPAtwA_1)); }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  get_TbyfgvoLcvCnLzBZpOVXBdPAtwA_1() const { return ___TbyfgvoLcvCnLzBZpOVXBdPAtwA_1; }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 * get_address_of_TbyfgvoLcvCnLzBZpOVXBdPAtwA_1() { return &___TbyfgvoLcvCnLzBZpOVXBdPAtwA_1; }
	inline void set_TbyfgvoLcvCnLzBZpOVXBdPAtwA_1(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  value)
	{
		___TbyfgvoLcvCnLzBZpOVXBdPAtwA_1 = value;
	}

	inline static int32_t get_offset_of_lXiIYlzqhDMJpyJyatjvSnwcuzH_2() { return static_cast<int32_t>(offsetof(SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4, ___lXiIYlzqhDMJpyJyatjvSnwcuzH_2)); }
	inline String_t* get_lXiIYlzqhDMJpyJyatjvSnwcuzH_2() const { return ___lXiIYlzqhDMJpyJyatjvSnwcuzH_2; }
	inline String_t** get_address_of_lXiIYlzqhDMJpyJyatjvSnwcuzH_2() { return &___lXiIYlzqhDMJpyJyatjvSnwcuzH_2; }
	inline void set_lXiIYlzqhDMJpyJyatjvSnwcuzH_2(String_t* value)
	{
		___lXiIYlzqhDMJpyJyatjvSnwcuzH_2 = value;
		Il2CppCodeGenWriteBarrier((&___lXiIYlzqhDMJpyJyatjvSnwcuzH_2), value);
	}

	inline static int32_t get_offset_of_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3() { return static_cast<int32_t>(offsetof(SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4, ___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3)); }
	inline int32_t get_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3() const { return ___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3; }
	inline int32_t* get_address_of_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3() { return &___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3; }
	inline void set_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3(int32_t value)
	{
		___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3 = value;
	}

	inline static int32_t get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_4() { return static_cast<int32_t>(offsetof(SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4, ___tOaTpOwzcPDCHdCDIyYaeuASgAM_4)); }
	inline int32_t get_tOaTpOwzcPDCHdCDIyYaeuASgAM_4() const { return ___tOaTpOwzcPDCHdCDIyYaeuASgAM_4; }
	inline int32_t* get_address_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_4() { return &___tOaTpOwzcPDCHdCDIyYaeuASgAM_4; }
	inline void set_tOaTpOwzcPDCHdCDIyYaeuASgAM_4(int32_t value)
	{
		___tOaTpOwzcPDCHdCDIyYaeuASgAM_4 = value;
	}

	inline static int32_t get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_5() { return static_cast<int32_t>(offsetof(SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4, ___itTafNyeQoucrbhPkPsSqRewAEI_5)); }
	inline int32_t get_itTafNyeQoucrbhPkPsSqRewAEI_5() const { return ___itTafNyeQoucrbhPkPsSqRewAEI_5; }
	inline int32_t* get_address_of_itTafNyeQoucrbhPkPsSqRewAEI_5() { return &___itTafNyeQoucrbhPkPsSqRewAEI_5; }
	inline void set_itTafNyeQoucrbhPkPsSqRewAEI_5(int32_t value)
	{
		___itTafNyeQoucrbhPkPsSqRewAEI_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAPQXZQJGZLMSJABMWGSMLJHEOU_T39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4_H
#ifndef AWGCDIGONXUFUEPTNRITXUUZGCK_T9E212A84A1064C41E8047F80A895BAE9D40EB46E_H
#define AWGCDIGONXUFUEPTNRITXUUZGCK_T9E212A84A1064C41E8047F80A895BAE9D40EB46E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU_awgCdigOnXUFUEptnriTXuUzGCk
struct  awgCdigOnXUFUEptnriTXuUzGCk_t9E212A84A1064C41E8047F80A895BAE9D40EB46E 
{
public:
	// System.Int32 Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU_awgCdigOnXUFUEptnriTXuUzGCk::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(awgCdigOnXUFUEptnriTXuUzGCk_t9E212A84A1064C41E8047F80A895BAE9D40EB46E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AWGCDIGONXUFUEPTNRITXUUZGCK_T9E212A84A1064C41E8047F80A895BAE9D40EB46E_H
#ifndef INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#define INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSource
struct  InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B 
{
public:
	// System.Int32 Rewired.InputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifndef KEYBOARDKEYCODE_TD0801C51ECEC814EC673F6E1849B0264EC307854_H
#define KEYBOARDKEYCODE_TD0801C51ECEC814EC673F6E1849B0264EC307854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.KeyboardKeyCode
struct  KeyboardKeyCode_tD0801C51ECEC814EC673F6E1849B0264EC307854 
{
public:
	// System.Int32 Rewired.KeyboardKeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyboardKeyCode_tD0801C51ECEC814EC673F6E1849B0264EC307854, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDKEYCODE_TD0801C51ECEC814EC673F6E1849B0264EC307854_H
#ifndef MODIFIERKEY_T300DA428522CD195A6C4121312A8A2BD4EA7ACE0_H
#define MODIFIERKEY_T300DA428522CD195A6C4121312A8A2BD4EA7ACE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKey
struct  ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0 
{
public:
	// System.Int32 Rewired.ModifierKey::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKey_t300DA428522CD195A6C4121312A8A2BD4EA7ACE0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEY_T300DA428522CD195A6C4121312A8A2BD4EA7ACE0_H
#ifndef MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#define MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKeyFlags
struct  ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651 
{
public:
	// System.Int32 Rewired.ModifierKeyFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifndef EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#define EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.EditorPlatform
struct  EditorPlatform_tBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC 
{
public:
	// System.Int32 Rewired.Platforms.EditorPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EditorPlatform_tBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#ifndef PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#define PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Platform
struct  Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD 
{
public:
	// System.Int32 Rewired.Platforms.Platform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#ifndef SCRIPTINGAPILEVEL_TEB2EB61369C8FB6E95CEC69128BE55B51D53A975_H
#define SCRIPTINGAPILEVEL_TEB2EB61369C8FB6E95CEC69128BE55B51D53A975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.ScriptingAPILevel
struct  ScriptingAPILevel_tEB2EB61369C8FB6E95CEC69128BE55B51D53A975 
{
public:
	// System.Int32 Rewired.Platforms.ScriptingAPILevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScriptingAPILevel_tEB2EB61369C8FB6E95CEC69128BE55B51D53A975, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGAPILEVEL_TEB2EB61369C8FB6E95CEC69128BE55B51D53A975_H
#ifndef SCRIPTINGBACKEND_T82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA_H
#define SCRIPTINGBACKEND_T82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.ScriptingBackend
struct  ScriptingBackend_t82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA 
{
public:
	// System.Int32 Rewired.Platforms.ScriptingBackend::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScriptingBackend_t82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGBACKEND_T82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA_H
#ifndef WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#define WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebplayerPlatform
struct  WebplayerPlatform_tCE2DAFDB23E582591F2C39C73279153D7E0C3A82 
{
public:
	// System.Int32 Rewired.Platforms.WebplayerPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebplayerPlatform_tCE2DAFDB23E582591F2C39C73279153D7E0C3A82, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#ifndef POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#define POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Pole
struct  Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C 
{
public:
	// System.Int32 Rewired.Pole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifndef UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#define UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UpdateLoopType
struct  UpdateLoopType_t407C8154C4B63048CA3DC1A59E6458956BC31F0A 
{
public:
	// System.Int32 Rewired.UpdateLoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateLoopType_t407C8154C4B63048CA3DC1A59E6458956BC31F0A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#ifndef YUPIWDFKBODLPMYXQFAYSICWJMR_TB416753D72DE2ECA22B99511F5EC4BEDC77EA0C8_H
#define YUPIWDFKBODLPMYXQFAYSICWJMR_TB416753D72DE2ECA22B99511F5EC4BEDC77EA0C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl_yUPiWDfKBoDlPMYXQfAysIcWjmR
struct  yUPiWDfKBoDlPMYXQfAysIcWjmR_tB416753D72DE2ECA22B99511F5EC4BEDC77EA0C8 
{
public:
	// System.Int32 SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl_yUPiWDfKBoDlPMYXQfAysIcWjmR::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(yUPiWDfKBoDlPMYXQfAysIcWjmR_tB416753D72DE2ECA22B99511F5EC4BEDC77EA0C8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YUPIWDFKBODLPMYXQFAYSICWJMR_TB416753D72DE2ECA22B99511F5EC4BEDC77EA0C8_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef ANIMATIONCURVE_TD2F265379583AAF1BF8D84F1BB8DB12980FA504C_H
#define ANIMATIONCURVE_TD2F265379583AAF1BF8D84F1BB8DB12980FA504C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_TD2F265379583AAF1BF8D84F1BB8DB12980FA504C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef YAEDWKZBZNXCLWFLPXUGOYUBKET_T030E2C54B6EC34CD15754F1E9F912A67AB6E3EAB_H
#define YAEDWKZBZNXCLWFLPXUGOYUBKET_T030E2C54B6EC34CD15754F1E9F912A67AB6E3EAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iDabdpqyvlEAXmjzfkEiOCBPdMuc_deXVkUCQurzFRSBNargAFcePsbp_yAEDWkzBznxclWFLPxuGoyUBKeT
struct  yAEDWkzBznxclWFLPxuGoyUBKeT_t030E2C54B6EC34CD15754F1E9F912A67AB6E3EAB 
{
public:
	// System.Byte iDabdpqyvlEAXmjzfkEiOCBPdMuc_deXVkUCQurzFRSBNargAFcePsbp_yAEDWkzBznxclWFLPxuGoyUBKeT::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(yAEDWkzBznxclWFLPxuGoyUBKeT_t030E2C54B6EC34CD15754F1E9F912A67AB6E3EAB, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YAEDWKZBZNXCLWFLPXUGOYUBKET_T030E2C54B6EC34CD15754F1E9F912A67AB6E3EAB_H
#ifndef NAMAVBGYJTBIUGBZJFQTWYAROKSE_T0C0CA1B0F694D27E7C4C70B578E574A4BA792125_H
#define NAMAVBGYJTBIUGBZJFQTWYAROKSE_T0C0CA1B0F694D27E7C4C70B578E574A4BA792125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// lUbzJtkApcIkohMrHhJAmcDImRRR_NAMaVbGyjTbiugbZjFqTWyaRoKSe
struct  NAMaVbGyjTbiugbZjFqTWyaRoKSe_t0C0CA1B0F694D27E7C4C70B578E574A4BA792125 
{
public:
	// System.Int32 lUbzJtkApcIkohMrHhJAmcDImRRR_NAMaVbGyjTbiugbZjFqTWyaRoKSe::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NAMaVbGyjTbiugbZjFqTWyaRoKSe_t0C0CA1B0F694D27E7C4C70B578E574A4BA792125, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMAVBGYJTBIUGBZJFQTWYAROKSE_T0C0CA1B0F694D27E7C4C70B578E574A4BA792125_H
#ifndef DVBYKIORXIPLVCNXRMCWLBPWADHB_T25691620CA1CA97FADA23D30620195A339F082A8_H
#define DVBYKIORXIPLVCNXRMCWLBPWADHB_T25691620CA1CA97FADA23D30620195A339F082A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DvbYkiORxIPlVcnxRmcWLbPwAdhB
struct  DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8  : public PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A
{
public:
	// System.Collections.Generic.List`1<DvbYkiORxIPlVcnxRmcWLbPwAdhB_jzMnORPzOilIJZDTJTcDzOnMbEdi> DvbYkiORxIPlVcnxRmcWLbPwAdhB::lZlHpVlqScCxOayHMQQNLIJUPtMA
	List_1_t3531148A82CAFAF7C56CC59256C74BD2CA5CB26F * ___lZlHpVlqScCxOayHMQQNLIJUPtMA_5;
	// System.Int32 DvbYkiORxIPlVcnxRmcWLbPwAdhB::GZolyQbnhDcIglfHmqdnClfPLwP
	int32_t ___GZolyQbnhDcIglfHmqdnClfPLwP_6;
	// DvbYkiORxIPlVcnxRmcWLbPwAdhB_sSSwLdgRIGhAHubEfzNAExVFpqE DvbYkiORxIPlVcnxRmcWLbPwAdhB::iswBkwCyDabLmfxdAYpLbXBWfvHN
	sSSwLdgRIGhAHubEfzNAExVFpqE_tEC4C43E6DFB8DA01667D46FA8A9F981B8871D642 * ___iswBkwCyDabLmfxdAYpLbXBWfvHN_7;
	// System.Boolean DvbYkiORxIPlVcnxRmcWLbPwAdhB::cteekJgqyXKoMIQGdQUhjuJXMhSq
	bool ___cteekJgqyXKoMIQGdQUhjuJXMhSq_8;
	// Rewired.UpdateLoopType DvbYkiORxIPlVcnxRmcWLbPwAdhB::YIgjjtvsNpmLAzEahZSayMqwKAO
	int32_t ___YIgjjtvsNpmLAzEahZSayMqwKAO_9;
	// Rewired.UpdateLoopType DvbYkiORxIPlVcnxRmcWLbPwAdhB::caGFUHzciIUISHXbwfgAhiHUvar
	int32_t ___caGFUHzciIUISHXbwfgAhiHUvar_10;
	// Rewired.Utils.Classes.Utility.TimerAbs DvbYkiORxIPlVcnxRmcWLbPwAdhB::YJmfkQYaPrFhhpPpeaupYcLjhKSG
	TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * ___YJmfkQYaPrFhhpPpeaupYcLjhKSG_11;
	// System.Action`2<System.Int32,Rewired.ControllerDataUpdater> DvbYkiORxIPlVcnxRmcWLbPwAdhB::nEiQhTKZDBYqGTkqzeEgrkzxQan
	Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A * ___nEiQhTKZDBYqGTkqzeEgrkzxQan_12;
	// Rewired.PlatformInputManager DvbYkiORxIPlVcnxRmcWLbPwAdhB::fVwZoOiZNlMKSCdQWxpuPNZKDrf
	PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A * ___fVwZoOiZNlMKSCdQWxpuPNZKDrf_13;
	// Rewired.Interfaces.IUnifiedKeyboardSource DvbYkiORxIPlVcnxRmcWLbPwAdhB::dErtTfMMIryBKEuDkHRJfAiDoNC
	RuntimeObject* ___dErtTfMMIryBKEuDkHRJfAiDoNC_14;
	// Rewired.Interfaces.IUnifiedMouseSource DvbYkiORxIPlVcnxRmcWLbPwAdhB::VwrWeijqNdKIlgwMWGqaMIqSSZC
	RuntimeObject* ___VwrWeijqNdKIlgwMWGqaMIqSSZC_15;
	// System.Boolean DvbYkiORxIPlVcnxRmcWLbPwAdhB::ZhnvtkDwYNjiCyjHcDyrReRJeeR
	bool ___ZhnvtkDwYNjiCyjHcDyrReRJeeR_16;

public:
	inline static int32_t get_offset_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_5() { return static_cast<int32_t>(offsetof(DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8, ___lZlHpVlqScCxOayHMQQNLIJUPtMA_5)); }
	inline List_1_t3531148A82CAFAF7C56CC59256C74BD2CA5CB26F * get_lZlHpVlqScCxOayHMQQNLIJUPtMA_5() const { return ___lZlHpVlqScCxOayHMQQNLIJUPtMA_5; }
	inline List_1_t3531148A82CAFAF7C56CC59256C74BD2CA5CB26F ** get_address_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_5() { return &___lZlHpVlqScCxOayHMQQNLIJUPtMA_5; }
	inline void set_lZlHpVlqScCxOayHMQQNLIJUPtMA_5(List_1_t3531148A82CAFAF7C56CC59256C74BD2CA5CB26F * value)
	{
		___lZlHpVlqScCxOayHMQQNLIJUPtMA_5 = value;
		Il2CppCodeGenWriteBarrier((&___lZlHpVlqScCxOayHMQQNLIJUPtMA_5), value);
	}

	inline static int32_t get_offset_of_GZolyQbnhDcIglfHmqdnClfPLwP_6() { return static_cast<int32_t>(offsetof(DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8, ___GZolyQbnhDcIglfHmqdnClfPLwP_6)); }
	inline int32_t get_GZolyQbnhDcIglfHmqdnClfPLwP_6() const { return ___GZolyQbnhDcIglfHmqdnClfPLwP_6; }
	inline int32_t* get_address_of_GZolyQbnhDcIglfHmqdnClfPLwP_6() { return &___GZolyQbnhDcIglfHmqdnClfPLwP_6; }
	inline void set_GZolyQbnhDcIglfHmqdnClfPLwP_6(int32_t value)
	{
		___GZolyQbnhDcIglfHmqdnClfPLwP_6 = value;
	}

	inline static int32_t get_offset_of_iswBkwCyDabLmfxdAYpLbXBWfvHN_7() { return static_cast<int32_t>(offsetof(DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8, ___iswBkwCyDabLmfxdAYpLbXBWfvHN_7)); }
	inline sSSwLdgRIGhAHubEfzNAExVFpqE_tEC4C43E6DFB8DA01667D46FA8A9F981B8871D642 * get_iswBkwCyDabLmfxdAYpLbXBWfvHN_7() const { return ___iswBkwCyDabLmfxdAYpLbXBWfvHN_7; }
	inline sSSwLdgRIGhAHubEfzNAExVFpqE_tEC4C43E6DFB8DA01667D46FA8A9F981B8871D642 ** get_address_of_iswBkwCyDabLmfxdAYpLbXBWfvHN_7() { return &___iswBkwCyDabLmfxdAYpLbXBWfvHN_7; }
	inline void set_iswBkwCyDabLmfxdAYpLbXBWfvHN_7(sSSwLdgRIGhAHubEfzNAExVFpqE_tEC4C43E6DFB8DA01667D46FA8A9F981B8871D642 * value)
	{
		___iswBkwCyDabLmfxdAYpLbXBWfvHN_7 = value;
		Il2CppCodeGenWriteBarrier((&___iswBkwCyDabLmfxdAYpLbXBWfvHN_7), value);
	}

	inline static int32_t get_offset_of_cteekJgqyXKoMIQGdQUhjuJXMhSq_8() { return static_cast<int32_t>(offsetof(DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8, ___cteekJgqyXKoMIQGdQUhjuJXMhSq_8)); }
	inline bool get_cteekJgqyXKoMIQGdQUhjuJXMhSq_8() const { return ___cteekJgqyXKoMIQGdQUhjuJXMhSq_8; }
	inline bool* get_address_of_cteekJgqyXKoMIQGdQUhjuJXMhSq_8() { return &___cteekJgqyXKoMIQGdQUhjuJXMhSq_8; }
	inline void set_cteekJgqyXKoMIQGdQUhjuJXMhSq_8(bool value)
	{
		___cteekJgqyXKoMIQGdQUhjuJXMhSq_8 = value;
	}

	inline static int32_t get_offset_of_YIgjjtvsNpmLAzEahZSayMqwKAO_9() { return static_cast<int32_t>(offsetof(DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8, ___YIgjjtvsNpmLAzEahZSayMqwKAO_9)); }
	inline int32_t get_YIgjjtvsNpmLAzEahZSayMqwKAO_9() const { return ___YIgjjtvsNpmLAzEahZSayMqwKAO_9; }
	inline int32_t* get_address_of_YIgjjtvsNpmLAzEahZSayMqwKAO_9() { return &___YIgjjtvsNpmLAzEahZSayMqwKAO_9; }
	inline void set_YIgjjtvsNpmLAzEahZSayMqwKAO_9(int32_t value)
	{
		___YIgjjtvsNpmLAzEahZSayMqwKAO_9 = value;
	}

	inline static int32_t get_offset_of_caGFUHzciIUISHXbwfgAhiHUvar_10() { return static_cast<int32_t>(offsetof(DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8, ___caGFUHzciIUISHXbwfgAhiHUvar_10)); }
	inline int32_t get_caGFUHzciIUISHXbwfgAhiHUvar_10() const { return ___caGFUHzciIUISHXbwfgAhiHUvar_10; }
	inline int32_t* get_address_of_caGFUHzciIUISHXbwfgAhiHUvar_10() { return &___caGFUHzciIUISHXbwfgAhiHUvar_10; }
	inline void set_caGFUHzciIUISHXbwfgAhiHUvar_10(int32_t value)
	{
		___caGFUHzciIUISHXbwfgAhiHUvar_10 = value;
	}

	inline static int32_t get_offset_of_YJmfkQYaPrFhhpPpeaupYcLjhKSG_11() { return static_cast<int32_t>(offsetof(DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8, ___YJmfkQYaPrFhhpPpeaupYcLjhKSG_11)); }
	inline TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * get_YJmfkQYaPrFhhpPpeaupYcLjhKSG_11() const { return ___YJmfkQYaPrFhhpPpeaupYcLjhKSG_11; }
	inline TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 ** get_address_of_YJmfkQYaPrFhhpPpeaupYcLjhKSG_11() { return &___YJmfkQYaPrFhhpPpeaupYcLjhKSG_11; }
	inline void set_YJmfkQYaPrFhhpPpeaupYcLjhKSG_11(TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * value)
	{
		___YJmfkQYaPrFhhpPpeaupYcLjhKSG_11 = value;
		Il2CppCodeGenWriteBarrier((&___YJmfkQYaPrFhhpPpeaupYcLjhKSG_11), value);
	}

	inline static int32_t get_offset_of_nEiQhTKZDBYqGTkqzeEgrkzxQan_12() { return static_cast<int32_t>(offsetof(DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8, ___nEiQhTKZDBYqGTkqzeEgrkzxQan_12)); }
	inline Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A * get_nEiQhTKZDBYqGTkqzeEgrkzxQan_12() const { return ___nEiQhTKZDBYqGTkqzeEgrkzxQan_12; }
	inline Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A ** get_address_of_nEiQhTKZDBYqGTkqzeEgrkzxQan_12() { return &___nEiQhTKZDBYqGTkqzeEgrkzxQan_12; }
	inline void set_nEiQhTKZDBYqGTkqzeEgrkzxQan_12(Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A * value)
	{
		___nEiQhTKZDBYqGTkqzeEgrkzxQan_12 = value;
		Il2CppCodeGenWriteBarrier((&___nEiQhTKZDBYqGTkqzeEgrkzxQan_12), value);
	}

	inline static int32_t get_offset_of_fVwZoOiZNlMKSCdQWxpuPNZKDrf_13() { return static_cast<int32_t>(offsetof(DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8, ___fVwZoOiZNlMKSCdQWxpuPNZKDrf_13)); }
	inline PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A * get_fVwZoOiZNlMKSCdQWxpuPNZKDrf_13() const { return ___fVwZoOiZNlMKSCdQWxpuPNZKDrf_13; }
	inline PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A ** get_address_of_fVwZoOiZNlMKSCdQWxpuPNZKDrf_13() { return &___fVwZoOiZNlMKSCdQWxpuPNZKDrf_13; }
	inline void set_fVwZoOiZNlMKSCdQWxpuPNZKDrf_13(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A * value)
	{
		___fVwZoOiZNlMKSCdQWxpuPNZKDrf_13 = value;
		Il2CppCodeGenWriteBarrier((&___fVwZoOiZNlMKSCdQWxpuPNZKDrf_13), value);
	}

	inline static int32_t get_offset_of_dErtTfMMIryBKEuDkHRJfAiDoNC_14() { return static_cast<int32_t>(offsetof(DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8, ___dErtTfMMIryBKEuDkHRJfAiDoNC_14)); }
	inline RuntimeObject* get_dErtTfMMIryBKEuDkHRJfAiDoNC_14() const { return ___dErtTfMMIryBKEuDkHRJfAiDoNC_14; }
	inline RuntimeObject** get_address_of_dErtTfMMIryBKEuDkHRJfAiDoNC_14() { return &___dErtTfMMIryBKEuDkHRJfAiDoNC_14; }
	inline void set_dErtTfMMIryBKEuDkHRJfAiDoNC_14(RuntimeObject* value)
	{
		___dErtTfMMIryBKEuDkHRJfAiDoNC_14 = value;
		Il2CppCodeGenWriteBarrier((&___dErtTfMMIryBKEuDkHRJfAiDoNC_14), value);
	}

	inline static int32_t get_offset_of_VwrWeijqNdKIlgwMWGqaMIqSSZC_15() { return static_cast<int32_t>(offsetof(DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8, ___VwrWeijqNdKIlgwMWGqaMIqSSZC_15)); }
	inline RuntimeObject* get_VwrWeijqNdKIlgwMWGqaMIqSSZC_15() const { return ___VwrWeijqNdKIlgwMWGqaMIqSSZC_15; }
	inline RuntimeObject** get_address_of_VwrWeijqNdKIlgwMWGqaMIqSSZC_15() { return &___VwrWeijqNdKIlgwMWGqaMIqSSZC_15; }
	inline void set_VwrWeijqNdKIlgwMWGqaMIqSSZC_15(RuntimeObject* value)
	{
		___VwrWeijqNdKIlgwMWGqaMIqSSZC_15 = value;
		Il2CppCodeGenWriteBarrier((&___VwrWeijqNdKIlgwMWGqaMIqSSZC_15), value);
	}

	inline static int32_t get_offset_of_ZhnvtkDwYNjiCyjHcDyrReRJeeR_16() { return static_cast<int32_t>(offsetof(DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8, ___ZhnvtkDwYNjiCyjHcDyrReRJeeR_16)); }
	inline bool get_ZhnvtkDwYNjiCyjHcDyrReRJeeR_16() const { return ___ZhnvtkDwYNjiCyjHcDyrReRJeeR_16; }
	inline bool* get_address_of_ZhnvtkDwYNjiCyjHcDyrReRJeeR_16() { return &___ZhnvtkDwYNjiCyjHcDyrReRJeeR_16; }
	inline void set_ZhnvtkDwYNjiCyjHcDyrReRJeeR_16(bool value)
	{
		___ZhnvtkDwYNjiCyjHcDyrReRJeeR_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DVBYKIORXIPLVCNXRMCWLBPWADHB_T25691620CA1CA97FADA23D30620195A339F082A8_H
#ifndef OMEPULXQINGOSHAFDEJGZVDONDPG_T2556EC33AB84FBD2176F62176E5F8A97FE7F8708_H
#define OMEPULXQINGOSHAFDEJGZVDONDPG_T2556EC33AB84FBD2176F62176E5F8A97FE7F8708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OmePuLXqingOShAfdEjGZVdOnDpG
struct  OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708  : public RuntimeObject
{
public:
	// System.String OmePuLXqingOShAfdEjGZVdOnDpG::xzFIhZhMHNeQYGDUhfMfvvqdZQds
	String_t* ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1;
	// System.Int32 OmePuLXqingOShAfdEjGZVdOnDpG::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_2;
	// System.Int32 OmePuLXqingOShAfdEjGZVdOnDpG::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_3;
	// System.Int32 OmePuLXqingOShAfdEjGZVdOnDpG::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_4;
	// Rewired.InputBehavior OmePuLXqingOShAfdEjGZVdOnDpG::kNThXpJRuNjadiuBHlBgpclZYks
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B * ___kNThXpJRuNjadiuBHlBgpclZYks_5;
	// OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ OmePuLXqingOShAfdEjGZVdOnDpG::NSVrbXrLyVPttWtnsqehtpGnXCG
	WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99 * ___NSVrbXrLyVPttWtnsqehtpGnXCG_6;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG::kQtPGxSDMqIDmNwBSXMjhzIdBbA
	float ___kQtPGxSDMqIDmNwBSXMjhzIdBbA_13;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG::KwUMroYKNRAKXmVJWzcwAZRnqTx
	float ___KwUMroYKNRAKXmVJWzcwAZRnqTx_14;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG::aZqkxOCUfsonDTHNWausiXJBOvOu
	float ___aZqkxOCUfsonDTHNWausiXJBOvOu_15;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG::vzFkOfiyuXiBznPLJddntTPCLHp
	float ___vzFkOfiyuXiBznPLJddntTPCLHp_16;
	// Rewired.ButtonStateFlags OmePuLXqingOShAfdEjGZVdOnDpG::LbAAJeTOMjPeFMaCYBaygBzGqKCe
	int32_t ___LbAAJeTOMjPeFMaCYBaygBzGqKCe_17;
	// Rewired.ButtonStateFlags OmePuLXqingOShAfdEjGZVdOnDpG::eupBpCkNZkzzzDHeCmeGOtfktiK
	int32_t ___eupBpCkNZkzzzDHeCmeGOtfktiK_18;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG::GxbsvPonBsCebHUvFmTUGSJKPNy
	float ___GxbsvPonBsCebHUvFmTUGSJKPNy_19;
	// System.Boolean OmePuLXqingOShAfdEjGZVdOnDpG::TkCeQwbtSOAqKaEpBqiGfOMtlHyh
	bool ___TkCeQwbtSOAqKaEpBqiGfOMtlHyh_20;
	// Rewired.AxisCoordinateMode OmePuLXqingOShAfdEjGZVdOnDpG::phClTRYtbLMsxPaWqfWLCoiEcmlf
	int32_t ___phClTRYtbLMsxPaWqfWLCoiEcmlf_21;
	// Rewired.AxisCoordinateMode OmePuLXqingOShAfdEjGZVdOnDpG::dSvBRDQTaBdKBeoGIRyCvWRfmAW
	int32_t ___dSvBRDQTaBdKBeoGIRyCvWRfmAW_22;
	// GmNQnYIPchcrVIxASPikMTNzgbC OmePuLXqingOShAfdEjGZVdOnDpG::NZLeLHoYohvUVkNahXYNolJkMaF
	GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99 * ___NZLeLHoYohvUVkNahXYNolJkMaF_23;
	// System.UInt32 OmePuLXqingOShAfdEjGZVdOnDpG::KCcLZLFnCVvWttVAUFgzikonYgI
	uint32_t ___KCcLZLFnCVvWttVAUFgzikonYgI_24;
	// System.UInt32 OmePuLXqingOShAfdEjGZVdOnDpG::eJLELoqXznZJForjqQNdZWufUik
	uint32_t ___eJLELoqXznZJForjqQNdZWufUik_25;
	// System.Boolean OmePuLXqingOShAfdEjGZVdOnDpG::wettkZCPIyShlpqdnjaNbxAuAvkR
	bool ___wettkZCPIyShlpqdnjaNbxAuAvkR_26;
	// OmePuLXqingOShAfdEjGZVdOnDpG_cZaxobkPCTtFHFGFxSkpddMuaQs OmePuLXqingOShAfdEjGZVdOnDpG::VvDPuKmCDZnivIDnrejTBCxXiIYZ
	int32_t ___VvDPuKmCDZnivIDnrejTBCxXiIYZ_27;
	// System.Int32 OmePuLXqingOShAfdEjGZVdOnDpG::nEWeDBkhKnQxcpyoDgpAVMPOmHD
	int32_t ___nEWeDBkhKnQxcpyoDgpAVMPOmHD_28;
	// GmNQnYIPchcrVIxASPikMTNzgbC[] OmePuLXqingOShAfdEjGZVdOnDpG::MWdTUcwbndAVXjzmXDzdiioSjos
	GmNQnYIPchcrVIxASPikMTNzgbCU5BU5D_tB2E5A22F809105ED175CB8ED6BCBE9305D54182A* ___MWdTUcwbndAVXjzmXDzdiioSjos_29;
	// System.Collections.Generic.List`1<Rewired.InputActionSourceData> OmePuLXqingOShAfdEjGZVdOnDpG::lkENoKGchYvqwItosrqdPkQJuXx
	List_1_t44209413D8D2B694B5619AA3630215F6729C4528 * ___lkENoKGchYvqwItosrqdPkQJuXx_30;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.InputActionSourceData> OmePuLXqingOShAfdEjGZVdOnDpG::YMEVpOuYpVjEtBldKIauEuQBazMc
	ReadOnlyCollection_1_t341D5CFD873149C1CE3509FD2066B741CBCE8044 * ___YMEVpOuYpVjEtBldKIauEuQBazMc_31;
	// System.Boolean OmePuLXqingOShAfdEjGZVdOnDpG::OdSbXBLJoYyBYTxCowWEcLeROjZ
	bool ___OdSbXBLJoYyBYTxCowWEcLeROjZ_32;
	// System.Boolean OmePuLXqingOShAfdEjGZVdOnDpG::JjCmPMTQfjvEUCHkNBeRcjNBieD
	bool ___JjCmPMTQfjvEUCHkNBeRcjNBieD_33;
	// OmePuLXqingOShAfdEjGZVdOnDpG_cZaxobkPCTtFHFGFxSkpddMuaQs OmePuLXqingOShAfdEjGZVdOnDpG::oqVsjNRLeoYJHQGxOeiecQoOuNz
	int32_t ___oqVsjNRLeoYJHQGxOeiecQoOuNz_34;

public:
	inline static int32_t get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1)); }
	inline String_t* get_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1() const { return ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1; }
	inline String_t** get_address_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1() { return &___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1; }
	inline void set_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1(String_t* value)
	{
		___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1 = value;
		Il2CppCodeGenWriteBarrier((&___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1), value);
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_2() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___LmADKPFcmVAiabETiHqQTGSgvmcg_2)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_2() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_2; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_2() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_2; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_2(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_2 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_3() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___FCIziTxDWAnyHATWwZeMEmiXvdc_3)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_3() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_3; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_3() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_3; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_3(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_3 = value;
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_4() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_4)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_4() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_4; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_4() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_4; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_4(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_4 = value;
	}

	inline static int32_t get_offset_of_kNThXpJRuNjadiuBHlBgpclZYks_5() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___kNThXpJRuNjadiuBHlBgpclZYks_5)); }
	inline InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B * get_kNThXpJRuNjadiuBHlBgpclZYks_5() const { return ___kNThXpJRuNjadiuBHlBgpclZYks_5; }
	inline InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B ** get_address_of_kNThXpJRuNjadiuBHlBgpclZYks_5() { return &___kNThXpJRuNjadiuBHlBgpclZYks_5; }
	inline void set_kNThXpJRuNjadiuBHlBgpclZYks_5(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B * value)
	{
		___kNThXpJRuNjadiuBHlBgpclZYks_5 = value;
		Il2CppCodeGenWriteBarrier((&___kNThXpJRuNjadiuBHlBgpclZYks_5), value);
	}

	inline static int32_t get_offset_of_NSVrbXrLyVPttWtnsqehtpGnXCG_6() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___NSVrbXrLyVPttWtnsqehtpGnXCG_6)); }
	inline WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99 * get_NSVrbXrLyVPttWtnsqehtpGnXCG_6() const { return ___NSVrbXrLyVPttWtnsqehtpGnXCG_6; }
	inline WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99 ** get_address_of_NSVrbXrLyVPttWtnsqehtpGnXCG_6() { return &___NSVrbXrLyVPttWtnsqehtpGnXCG_6; }
	inline void set_NSVrbXrLyVPttWtnsqehtpGnXCG_6(WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99 * value)
	{
		___NSVrbXrLyVPttWtnsqehtpGnXCG_6 = value;
		Il2CppCodeGenWriteBarrier((&___NSVrbXrLyVPttWtnsqehtpGnXCG_6), value);
	}

	inline static int32_t get_offset_of_kQtPGxSDMqIDmNwBSXMjhzIdBbA_13() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___kQtPGxSDMqIDmNwBSXMjhzIdBbA_13)); }
	inline float get_kQtPGxSDMqIDmNwBSXMjhzIdBbA_13() const { return ___kQtPGxSDMqIDmNwBSXMjhzIdBbA_13; }
	inline float* get_address_of_kQtPGxSDMqIDmNwBSXMjhzIdBbA_13() { return &___kQtPGxSDMqIDmNwBSXMjhzIdBbA_13; }
	inline void set_kQtPGxSDMqIDmNwBSXMjhzIdBbA_13(float value)
	{
		___kQtPGxSDMqIDmNwBSXMjhzIdBbA_13 = value;
	}

	inline static int32_t get_offset_of_KwUMroYKNRAKXmVJWzcwAZRnqTx_14() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___KwUMroYKNRAKXmVJWzcwAZRnqTx_14)); }
	inline float get_KwUMroYKNRAKXmVJWzcwAZRnqTx_14() const { return ___KwUMroYKNRAKXmVJWzcwAZRnqTx_14; }
	inline float* get_address_of_KwUMroYKNRAKXmVJWzcwAZRnqTx_14() { return &___KwUMroYKNRAKXmVJWzcwAZRnqTx_14; }
	inline void set_KwUMroYKNRAKXmVJWzcwAZRnqTx_14(float value)
	{
		___KwUMroYKNRAKXmVJWzcwAZRnqTx_14 = value;
	}

	inline static int32_t get_offset_of_aZqkxOCUfsonDTHNWausiXJBOvOu_15() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___aZqkxOCUfsonDTHNWausiXJBOvOu_15)); }
	inline float get_aZqkxOCUfsonDTHNWausiXJBOvOu_15() const { return ___aZqkxOCUfsonDTHNWausiXJBOvOu_15; }
	inline float* get_address_of_aZqkxOCUfsonDTHNWausiXJBOvOu_15() { return &___aZqkxOCUfsonDTHNWausiXJBOvOu_15; }
	inline void set_aZqkxOCUfsonDTHNWausiXJBOvOu_15(float value)
	{
		___aZqkxOCUfsonDTHNWausiXJBOvOu_15 = value;
	}

	inline static int32_t get_offset_of_vzFkOfiyuXiBznPLJddntTPCLHp_16() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___vzFkOfiyuXiBznPLJddntTPCLHp_16)); }
	inline float get_vzFkOfiyuXiBznPLJddntTPCLHp_16() const { return ___vzFkOfiyuXiBznPLJddntTPCLHp_16; }
	inline float* get_address_of_vzFkOfiyuXiBznPLJddntTPCLHp_16() { return &___vzFkOfiyuXiBznPLJddntTPCLHp_16; }
	inline void set_vzFkOfiyuXiBznPLJddntTPCLHp_16(float value)
	{
		___vzFkOfiyuXiBznPLJddntTPCLHp_16 = value;
	}

	inline static int32_t get_offset_of_LbAAJeTOMjPeFMaCYBaygBzGqKCe_17() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___LbAAJeTOMjPeFMaCYBaygBzGqKCe_17)); }
	inline int32_t get_LbAAJeTOMjPeFMaCYBaygBzGqKCe_17() const { return ___LbAAJeTOMjPeFMaCYBaygBzGqKCe_17; }
	inline int32_t* get_address_of_LbAAJeTOMjPeFMaCYBaygBzGqKCe_17() { return &___LbAAJeTOMjPeFMaCYBaygBzGqKCe_17; }
	inline void set_LbAAJeTOMjPeFMaCYBaygBzGqKCe_17(int32_t value)
	{
		___LbAAJeTOMjPeFMaCYBaygBzGqKCe_17 = value;
	}

	inline static int32_t get_offset_of_eupBpCkNZkzzzDHeCmeGOtfktiK_18() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___eupBpCkNZkzzzDHeCmeGOtfktiK_18)); }
	inline int32_t get_eupBpCkNZkzzzDHeCmeGOtfktiK_18() const { return ___eupBpCkNZkzzzDHeCmeGOtfktiK_18; }
	inline int32_t* get_address_of_eupBpCkNZkzzzDHeCmeGOtfktiK_18() { return &___eupBpCkNZkzzzDHeCmeGOtfktiK_18; }
	inline void set_eupBpCkNZkzzzDHeCmeGOtfktiK_18(int32_t value)
	{
		___eupBpCkNZkzzzDHeCmeGOtfktiK_18 = value;
	}

	inline static int32_t get_offset_of_GxbsvPonBsCebHUvFmTUGSJKPNy_19() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___GxbsvPonBsCebHUvFmTUGSJKPNy_19)); }
	inline float get_GxbsvPonBsCebHUvFmTUGSJKPNy_19() const { return ___GxbsvPonBsCebHUvFmTUGSJKPNy_19; }
	inline float* get_address_of_GxbsvPonBsCebHUvFmTUGSJKPNy_19() { return &___GxbsvPonBsCebHUvFmTUGSJKPNy_19; }
	inline void set_GxbsvPonBsCebHUvFmTUGSJKPNy_19(float value)
	{
		___GxbsvPonBsCebHUvFmTUGSJKPNy_19 = value;
	}

	inline static int32_t get_offset_of_TkCeQwbtSOAqKaEpBqiGfOMtlHyh_20() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___TkCeQwbtSOAqKaEpBqiGfOMtlHyh_20)); }
	inline bool get_TkCeQwbtSOAqKaEpBqiGfOMtlHyh_20() const { return ___TkCeQwbtSOAqKaEpBqiGfOMtlHyh_20; }
	inline bool* get_address_of_TkCeQwbtSOAqKaEpBqiGfOMtlHyh_20() { return &___TkCeQwbtSOAqKaEpBqiGfOMtlHyh_20; }
	inline void set_TkCeQwbtSOAqKaEpBqiGfOMtlHyh_20(bool value)
	{
		___TkCeQwbtSOAqKaEpBqiGfOMtlHyh_20 = value;
	}

	inline static int32_t get_offset_of_phClTRYtbLMsxPaWqfWLCoiEcmlf_21() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___phClTRYtbLMsxPaWqfWLCoiEcmlf_21)); }
	inline int32_t get_phClTRYtbLMsxPaWqfWLCoiEcmlf_21() const { return ___phClTRYtbLMsxPaWqfWLCoiEcmlf_21; }
	inline int32_t* get_address_of_phClTRYtbLMsxPaWqfWLCoiEcmlf_21() { return &___phClTRYtbLMsxPaWqfWLCoiEcmlf_21; }
	inline void set_phClTRYtbLMsxPaWqfWLCoiEcmlf_21(int32_t value)
	{
		___phClTRYtbLMsxPaWqfWLCoiEcmlf_21 = value;
	}

	inline static int32_t get_offset_of_dSvBRDQTaBdKBeoGIRyCvWRfmAW_22() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___dSvBRDQTaBdKBeoGIRyCvWRfmAW_22)); }
	inline int32_t get_dSvBRDQTaBdKBeoGIRyCvWRfmAW_22() const { return ___dSvBRDQTaBdKBeoGIRyCvWRfmAW_22; }
	inline int32_t* get_address_of_dSvBRDQTaBdKBeoGIRyCvWRfmAW_22() { return &___dSvBRDQTaBdKBeoGIRyCvWRfmAW_22; }
	inline void set_dSvBRDQTaBdKBeoGIRyCvWRfmAW_22(int32_t value)
	{
		___dSvBRDQTaBdKBeoGIRyCvWRfmAW_22 = value;
	}

	inline static int32_t get_offset_of_NZLeLHoYohvUVkNahXYNolJkMaF_23() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___NZLeLHoYohvUVkNahXYNolJkMaF_23)); }
	inline GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99 * get_NZLeLHoYohvUVkNahXYNolJkMaF_23() const { return ___NZLeLHoYohvUVkNahXYNolJkMaF_23; }
	inline GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99 ** get_address_of_NZLeLHoYohvUVkNahXYNolJkMaF_23() { return &___NZLeLHoYohvUVkNahXYNolJkMaF_23; }
	inline void set_NZLeLHoYohvUVkNahXYNolJkMaF_23(GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99 * value)
	{
		___NZLeLHoYohvUVkNahXYNolJkMaF_23 = value;
		Il2CppCodeGenWriteBarrier((&___NZLeLHoYohvUVkNahXYNolJkMaF_23), value);
	}

	inline static int32_t get_offset_of_KCcLZLFnCVvWttVAUFgzikonYgI_24() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___KCcLZLFnCVvWttVAUFgzikonYgI_24)); }
	inline uint32_t get_KCcLZLFnCVvWttVAUFgzikonYgI_24() const { return ___KCcLZLFnCVvWttVAUFgzikonYgI_24; }
	inline uint32_t* get_address_of_KCcLZLFnCVvWttVAUFgzikonYgI_24() { return &___KCcLZLFnCVvWttVAUFgzikonYgI_24; }
	inline void set_KCcLZLFnCVvWttVAUFgzikonYgI_24(uint32_t value)
	{
		___KCcLZLFnCVvWttVAUFgzikonYgI_24 = value;
	}

	inline static int32_t get_offset_of_eJLELoqXznZJForjqQNdZWufUik_25() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___eJLELoqXznZJForjqQNdZWufUik_25)); }
	inline uint32_t get_eJLELoqXznZJForjqQNdZWufUik_25() const { return ___eJLELoqXznZJForjqQNdZWufUik_25; }
	inline uint32_t* get_address_of_eJLELoqXznZJForjqQNdZWufUik_25() { return &___eJLELoqXznZJForjqQNdZWufUik_25; }
	inline void set_eJLELoqXznZJForjqQNdZWufUik_25(uint32_t value)
	{
		___eJLELoqXznZJForjqQNdZWufUik_25 = value;
	}

	inline static int32_t get_offset_of_wettkZCPIyShlpqdnjaNbxAuAvkR_26() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___wettkZCPIyShlpqdnjaNbxAuAvkR_26)); }
	inline bool get_wettkZCPIyShlpqdnjaNbxAuAvkR_26() const { return ___wettkZCPIyShlpqdnjaNbxAuAvkR_26; }
	inline bool* get_address_of_wettkZCPIyShlpqdnjaNbxAuAvkR_26() { return &___wettkZCPIyShlpqdnjaNbxAuAvkR_26; }
	inline void set_wettkZCPIyShlpqdnjaNbxAuAvkR_26(bool value)
	{
		___wettkZCPIyShlpqdnjaNbxAuAvkR_26 = value;
	}

	inline static int32_t get_offset_of_VvDPuKmCDZnivIDnrejTBCxXiIYZ_27() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___VvDPuKmCDZnivIDnrejTBCxXiIYZ_27)); }
	inline int32_t get_VvDPuKmCDZnivIDnrejTBCxXiIYZ_27() const { return ___VvDPuKmCDZnivIDnrejTBCxXiIYZ_27; }
	inline int32_t* get_address_of_VvDPuKmCDZnivIDnrejTBCxXiIYZ_27() { return &___VvDPuKmCDZnivIDnrejTBCxXiIYZ_27; }
	inline void set_VvDPuKmCDZnivIDnrejTBCxXiIYZ_27(int32_t value)
	{
		___VvDPuKmCDZnivIDnrejTBCxXiIYZ_27 = value;
	}

	inline static int32_t get_offset_of_nEWeDBkhKnQxcpyoDgpAVMPOmHD_28() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___nEWeDBkhKnQxcpyoDgpAVMPOmHD_28)); }
	inline int32_t get_nEWeDBkhKnQxcpyoDgpAVMPOmHD_28() const { return ___nEWeDBkhKnQxcpyoDgpAVMPOmHD_28; }
	inline int32_t* get_address_of_nEWeDBkhKnQxcpyoDgpAVMPOmHD_28() { return &___nEWeDBkhKnQxcpyoDgpAVMPOmHD_28; }
	inline void set_nEWeDBkhKnQxcpyoDgpAVMPOmHD_28(int32_t value)
	{
		___nEWeDBkhKnQxcpyoDgpAVMPOmHD_28 = value;
	}

	inline static int32_t get_offset_of_MWdTUcwbndAVXjzmXDzdiioSjos_29() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___MWdTUcwbndAVXjzmXDzdiioSjos_29)); }
	inline GmNQnYIPchcrVIxASPikMTNzgbCU5BU5D_tB2E5A22F809105ED175CB8ED6BCBE9305D54182A* get_MWdTUcwbndAVXjzmXDzdiioSjos_29() const { return ___MWdTUcwbndAVXjzmXDzdiioSjos_29; }
	inline GmNQnYIPchcrVIxASPikMTNzgbCU5BU5D_tB2E5A22F809105ED175CB8ED6BCBE9305D54182A** get_address_of_MWdTUcwbndAVXjzmXDzdiioSjos_29() { return &___MWdTUcwbndAVXjzmXDzdiioSjos_29; }
	inline void set_MWdTUcwbndAVXjzmXDzdiioSjos_29(GmNQnYIPchcrVIxASPikMTNzgbCU5BU5D_tB2E5A22F809105ED175CB8ED6BCBE9305D54182A* value)
	{
		___MWdTUcwbndAVXjzmXDzdiioSjos_29 = value;
		Il2CppCodeGenWriteBarrier((&___MWdTUcwbndAVXjzmXDzdiioSjos_29), value);
	}

	inline static int32_t get_offset_of_lkENoKGchYvqwItosrqdPkQJuXx_30() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___lkENoKGchYvqwItosrqdPkQJuXx_30)); }
	inline List_1_t44209413D8D2B694B5619AA3630215F6729C4528 * get_lkENoKGchYvqwItosrqdPkQJuXx_30() const { return ___lkENoKGchYvqwItosrqdPkQJuXx_30; }
	inline List_1_t44209413D8D2B694B5619AA3630215F6729C4528 ** get_address_of_lkENoKGchYvqwItosrqdPkQJuXx_30() { return &___lkENoKGchYvqwItosrqdPkQJuXx_30; }
	inline void set_lkENoKGchYvqwItosrqdPkQJuXx_30(List_1_t44209413D8D2B694B5619AA3630215F6729C4528 * value)
	{
		___lkENoKGchYvqwItosrqdPkQJuXx_30 = value;
		Il2CppCodeGenWriteBarrier((&___lkENoKGchYvqwItosrqdPkQJuXx_30), value);
	}

	inline static int32_t get_offset_of_YMEVpOuYpVjEtBldKIauEuQBazMc_31() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___YMEVpOuYpVjEtBldKIauEuQBazMc_31)); }
	inline ReadOnlyCollection_1_t341D5CFD873149C1CE3509FD2066B741CBCE8044 * get_YMEVpOuYpVjEtBldKIauEuQBazMc_31() const { return ___YMEVpOuYpVjEtBldKIauEuQBazMc_31; }
	inline ReadOnlyCollection_1_t341D5CFD873149C1CE3509FD2066B741CBCE8044 ** get_address_of_YMEVpOuYpVjEtBldKIauEuQBazMc_31() { return &___YMEVpOuYpVjEtBldKIauEuQBazMc_31; }
	inline void set_YMEVpOuYpVjEtBldKIauEuQBazMc_31(ReadOnlyCollection_1_t341D5CFD873149C1CE3509FD2066B741CBCE8044 * value)
	{
		___YMEVpOuYpVjEtBldKIauEuQBazMc_31 = value;
		Il2CppCodeGenWriteBarrier((&___YMEVpOuYpVjEtBldKIauEuQBazMc_31), value);
	}

	inline static int32_t get_offset_of_OdSbXBLJoYyBYTxCowWEcLeROjZ_32() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___OdSbXBLJoYyBYTxCowWEcLeROjZ_32)); }
	inline bool get_OdSbXBLJoYyBYTxCowWEcLeROjZ_32() const { return ___OdSbXBLJoYyBYTxCowWEcLeROjZ_32; }
	inline bool* get_address_of_OdSbXBLJoYyBYTxCowWEcLeROjZ_32() { return &___OdSbXBLJoYyBYTxCowWEcLeROjZ_32; }
	inline void set_OdSbXBLJoYyBYTxCowWEcLeROjZ_32(bool value)
	{
		___OdSbXBLJoYyBYTxCowWEcLeROjZ_32 = value;
	}

	inline static int32_t get_offset_of_JjCmPMTQfjvEUCHkNBeRcjNBieD_33() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___JjCmPMTQfjvEUCHkNBeRcjNBieD_33)); }
	inline bool get_JjCmPMTQfjvEUCHkNBeRcjNBieD_33() const { return ___JjCmPMTQfjvEUCHkNBeRcjNBieD_33; }
	inline bool* get_address_of_JjCmPMTQfjvEUCHkNBeRcjNBieD_33() { return &___JjCmPMTQfjvEUCHkNBeRcjNBieD_33; }
	inline void set_JjCmPMTQfjvEUCHkNBeRcjNBieD_33(bool value)
	{
		___JjCmPMTQfjvEUCHkNBeRcjNBieD_33 = value;
	}

	inline static int32_t get_offset_of_oqVsjNRLeoYJHQGxOeiecQoOuNz_34() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708, ___oqVsjNRLeoYJHQGxOeiecQoOuNz_34)); }
	inline int32_t get_oqVsjNRLeoYJHQGxOeiecQoOuNz_34() const { return ___oqVsjNRLeoYJHQGxOeiecQoOuNz_34; }
	inline int32_t* get_address_of_oqVsjNRLeoYJHQGxOeiecQoOuNz_34() { return &___oqVsjNRLeoYJHQGxOeiecQoOuNz_34; }
	inline void set_oqVsjNRLeoYJHQGxOeiecQoOuNz_34(int32_t value)
	{
		___oqVsjNRLeoYJHQGxOeiecQoOuNz_34 = value;
	}
};

struct OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields
{
public:
	// Rewired.Data.ConfigVars OmePuLXqingOShAfdEjGZVdOnDpG::AEpGUwNwMAsWjLqSLSkygEagFsW
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * ___AEpGUwNwMAsWjLqSLSkygEagFsW_7;
	// OmePuLXqingOShAfdEjGZVdOnDpG_EAJRBzBYjjtwooLSURpauuDghJn OmePuLXqingOShAfdEjGZVdOnDpG::QUvyLwiKJQNzsCxNpgqsoOLwSsX
	EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94 * ___QUvyLwiKJQNzsCxNpgqsoOLwSsX_8;
	// Rewired.UpdateLoopType OmePuLXqingOShAfdEjGZVdOnDpG::mRQSosFNYcuzgGZwdtMLlTBUFBV
	int32_t ___mRQSosFNYcuzgGZwdtMLlTBUFBV_9;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG::DdAQuNXlVlFtzKHQSEFqDCRtRgt
	float ___DdAQuNXlVlFtzKHQSEFqDCRtRgt_10;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG::oeJpjybqWWdBoUSIPTRbUtisjll
	float ___oeJpjybqWWdBoUSIPTRbUtisjll_11;
	// System.UInt32 OmePuLXqingOShAfdEjGZVdOnDpG::WuuCRjELljHtiCCsZMoeiRWBMSt
	uint32_t ___WuuCRjELljHtiCCsZMoeiRWBMSt_12;
	// xvUPNQnWmcdqDjulQTyAIyRAxwG OmePuLXqingOShAfdEjGZVdOnDpG::PTHUuKTeYDyTLouJwwjwtdtVAGFd
	xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032 * ___PTHUuKTeYDyTLouJwwjwtdtVAGFd_35;

public:
	inline static int32_t get_offset_of_AEpGUwNwMAsWjLqSLSkygEagFsW_7() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields, ___AEpGUwNwMAsWjLqSLSkygEagFsW_7)); }
	inline ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * get_AEpGUwNwMAsWjLqSLSkygEagFsW_7() const { return ___AEpGUwNwMAsWjLqSLSkygEagFsW_7; }
	inline ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 ** get_address_of_AEpGUwNwMAsWjLqSLSkygEagFsW_7() { return &___AEpGUwNwMAsWjLqSLSkygEagFsW_7; }
	inline void set_AEpGUwNwMAsWjLqSLSkygEagFsW_7(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * value)
	{
		___AEpGUwNwMAsWjLqSLSkygEagFsW_7 = value;
		Il2CppCodeGenWriteBarrier((&___AEpGUwNwMAsWjLqSLSkygEagFsW_7), value);
	}

	inline static int32_t get_offset_of_QUvyLwiKJQNzsCxNpgqsoOLwSsX_8() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields, ___QUvyLwiKJQNzsCxNpgqsoOLwSsX_8)); }
	inline EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94 * get_QUvyLwiKJQNzsCxNpgqsoOLwSsX_8() const { return ___QUvyLwiKJQNzsCxNpgqsoOLwSsX_8; }
	inline EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94 ** get_address_of_QUvyLwiKJQNzsCxNpgqsoOLwSsX_8() { return &___QUvyLwiKJQNzsCxNpgqsoOLwSsX_8; }
	inline void set_QUvyLwiKJQNzsCxNpgqsoOLwSsX_8(EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94 * value)
	{
		___QUvyLwiKJQNzsCxNpgqsoOLwSsX_8 = value;
		Il2CppCodeGenWriteBarrier((&___QUvyLwiKJQNzsCxNpgqsoOLwSsX_8), value);
	}

	inline static int32_t get_offset_of_mRQSosFNYcuzgGZwdtMLlTBUFBV_9() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields, ___mRQSosFNYcuzgGZwdtMLlTBUFBV_9)); }
	inline int32_t get_mRQSosFNYcuzgGZwdtMLlTBUFBV_9() const { return ___mRQSosFNYcuzgGZwdtMLlTBUFBV_9; }
	inline int32_t* get_address_of_mRQSosFNYcuzgGZwdtMLlTBUFBV_9() { return &___mRQSosFNYcuzgGZwdtMLlTBUFBV_9; }
	inline void set_mRQSosFNYcuzgGZwdtMLlTBUFBV_9(int32_t value)
	{
		___mRQSosFNYcuzgGZwdtMLlTBUFBV_9 = value;
	}

	inline static int32_t get_offset_of_DdAQuNXlVlFtzKHQSEFqDCRtRgt_10() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields, ___DdAQuNXlVlFtzKHQSEFqDCRtRgt_10)); }
	inline float get_DdAQuNXlVlFtzKHQSEFqDCRtRgt_10() const { return ___DdAQuNXlVlFtzKHQSEFqDCRtRgt_10; }
	inline float* get_address_of_DdAQuNXlVlFtzKHQSEFqDCRtRgt_10() { return &___DdAQuNXlVlFtzKHQSEFqDCRtRgt_10; }
	inline void set_DdAQuNXlVlFtzKHQSEFqDCRtRgt_10(float value)
	{
		___DdAQuNXlVlFtzKHQSEFqDCRtRgt_10 = value;
	}

	inline static int32_t get_offset_of_oeJpjybqWWdBoUSIPTRbUtisjll_11() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields, ___oeJpjybqWWdBoUSIPTRbUtisjll_11)); }
	inline float get_oeJpjybqWWdBoUSIPTRbUtisjll_11() const { return ___oeJpjybqWWdBoUSIPTRbUtisjll_11; }
	inline float* get_address_of_oeJpjybqWWdBoUSIPTRbUtisjll_11() { return &___oeJpjybqWWdBoUSIPTRbUtisjll_11; }
	inline void set_oeJpjybqWWdBoUSIPTRbUtisjll_11(float value)
	{
		___oeJpjybqWWdBoUSIPTRbUtisjll_11 = value;
	}

	inline static int32_t get_offset_of_WuuCRjELljHtiCCsZMoeiRWBMSt_12() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields, ___WuuCRjELljHtiCCsZMoeiRWBMSt_12)); }
	inline uint32_t get_WuuCRjELljHtiCCsZMoeiRWBMSt_12() const { return ___WuuCRjELljHtiCCsZMoeiRWBMSt_12; }
	inline uint32_t* get_address_of_WuuCRjELljHtiCCsZMoeiRWBMSt_12() { return &___WuuCRjELljHtiCCsZMoeiRWBMSt_12; }
	inline void set_WuuCRjELljHtiCCsZMoeiRWBMSt_12(uint32_t value)
	{
		___WuuCRjELljHtiCCsZMoeiRWBMSt_12 = value;
	}

	inline static int32_t get_offset_of_PTHUuKTeYDyTLouJwwjwtdtVAGFd_35() { return static_cast<int32_t>(offsetof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields, ___PTHUuKTeYDyTLouJwwjwtdtVAGFd_35)); }
	inline xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032 * get_PTHUuKTeYDyTLouJwwjwtdtVAGFd_35() const { return ___PTHUuKTeYDyTLouJwwjwtdtVAGFd_35; }
	inline xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032 ** get_address_of_PTHUuKTeYDyTLouJwwjwtdtVAGFd_35() { return &___PTHUuKTeYDyTLouJwwjwtdtVAGFd_35; }
	inline void set_PTHUuKTeYDyTLouJwwjwtdtVAGFd_35(xvUPNQnWmcdqDjulQTyAIyRAxwG_tD9F87B3D114024B9D7527B698A80BE4245E1D032 * value)
	{
		___PTHUuKTeYDyTLouJwwjwtdtVAGFd_35 = value;
		Il2CppCodeGenWriteBarrier((&___PTHUuKTeYDyTLouJwwjwtdtVAGFd_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OMEPULXQINGOSHAFDEJGZVDONDPG_T2556EC33AB84FBD2176F62176E5F8A97FE7F8708_H
#ifndef EAJRBZBYJJTWOOLSURPAUUDGHJN_T985525D4ADBA589270E310CA763D8D21A2304C94_H
#define EAJRBZBYJJTWOOLSURPAUUDGHJN_T985525D4ADBA589270E310CA763D8D21A2304C94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OmePuLXqingOShAfdEjGZVdOnDpG_EAJRBzBYjjtwooLSURpauuDghJn
struct  EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94  : public RuntimeObject
{
public:
	// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,OmePuLXqingOShAfdEjGZVdOnDpG_EAJRBzBYjjtwooLSURpauuDghJn_zMiZeuUVPippzZoUlAuLXMyxVeJ> OmePuLXqingOShAfdEjGZVdOnDpG_EAJRBzBYjjtwooLSURpauuDghJn::HWmOewXpuebOsZDStBejWuBUGqK
	ADictionary_2_t1D194B1E00D38C772F722BC937ECB8D1EB16125C * ___HWmOewXpuebOsZDStBejWuBUGqK_0;
	// OmePuLXqingOShAfdEjGZVdOnDpG_EAJRBzBYjjtwooLSURpauuDghJn_zMiZeuUVPippzZoUlAuLXMyxVeJ OmePuLXqingOShAfdEjGZVdOnDpG_EAJRBzBYjjtwooLSURpauuDghJn::XTGIIIRFvRSEWTCNbDuWdIDniEr
	zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5 * ___XTGIIIRFvRSEWTCNbDuWdIDniEr_1;
	// Rewired.UpdateLoopType OmePuLXqingOShAfdEjGZVdOnDpG_EAJRBzBYjjtwooLSURpauuDghJn::WzMPJskYTcPesnNpifYmyBmyoWg
	int32_t ___WzMPJskYTcPesnNpifYmyBmyoWg_2;

public:
	inline static int32_t get_offset_of_HWmOewXpuebOsZDStBejWuBUGqK_0() { return static_cast<int32_t>(offsetof(EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94, ___HWmOewXpuebOsZDStBejWuBUGqK_0)); }
	inline ADictionary_2_t1D194B1E00D38C772F722BC937ECB8D1EB16125C * get_HWmOewXpuebOsZDStBejWuBUGqK_0() const { return ___HWmOewXpuebOsZDStBejWuBUGqK_0; }
	inline ADictionary_2_t1D194B1E00D38C772F722BC937ECB8D1EB16125C ** get_address_of_HWmOewXpuebOsZDStBejWuBUGqK_0() { return &___HWmOewXpuebOsZDStBejWuBUGqK_0; }
	inline void set_HWmOewXpuebOsZDStBejWuBUGqK_0(ADictionary_2_t1D194B1E00D38C772F722BC937ECB8D1EB16125C * value)
	{
		___HWmOewXpuebOsZDStBejWuBUGqK_0 = value;
		Il2CppCodeGenWriteBarrier((&___HWmOewXpuebOsZDStBejWuBUGqK_0), value);
	}

	inline static int32_t get_offset_of_XTGIIIRFvRSEWTCNbDuWdIDniEr_1() { return static_cast<int32_t>(offsetof(EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94, ___XTGIIIRFvRSEWTCNbDuWdIDniEr_1)); }
	inline zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5 * get_XTGIIIRFvRSEWTCNbDuWdIDniEr_1() const { return ___XTGIIIRFvRSEWTCNbDuWdIDniEr_1; }
	inline zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5 ** get_address_of_XTGIIIRFvRSEWTCNbDuWdIDniEr_1() { return &___XTGIIIRFvRSEWTCNbDuWdIDniEr_1; }
	inline void set_XTGIIIRFvRSEWTCNbDuWdIDniEr_1(zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5 * value)
	{
		___XTGIIIRFvRSEWTCNbDuWdIDniEr_1 = value;
		Il2CppCodeGenWriteBarrier((&___XTGIIIRFvRSEWTCNbDuWdIDniEr_1), value);
	}

	inline static int32_t get_offset_of_WzMPJskYTcPesnNpifYmyBmyoWg_2() { return static_cast<int32_t>(offsetof(EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94, ___WzMPJskYTcPesnNpifYmyBmyoWg_2)); }
	inline int32_t get_WzMPJskYTcPesnNpifYmyBmyoWg_2() const { return ___WzMPJskYTcPesnNpifYmyBmyoWg_2; }
	inline int32_t* get_address_of_WzMPJskYTcPesnNpifYmyBmyoWg_2() { return &___WzMPJskYTcPesnNpifYmyBmyoWg_2; }
	inline void set_WzMPJskYTcPesnNpifYmyBmyoWg_2(int32_t value)
	{
		___WzMPJskYTcPesnNpifYmyBmyoWg_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAJRBZBYJJTWOOLSURPAUUDGHJN_T985525D4ADBA589270E310CA763D8D21A2304C94_H
#ifndef LBAYAJDRJCVOGHEKEOTUEMCDNEB_TE08793447A23877E83B61A730E12F94A0B11B25A_H
#define LBAYAJDRJCVOGHEKEOTUEMCDNEB_TE08793447A23877E83B61A730E12F94A0B11B25A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB
struct  LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A  : public RuntimeObject
{
public:
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::xBYHLUKVdSEjOEPIkUktZiSGzVBE
	float ___xBYHLUKVdSEjOEPIkUktZiSGzVBE_0;
	// Rewired.InputBehavior OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::kNThXpJRuNjadiuBHlBgpclZYks
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B * ___kNThXpJRuNjadiuBHlBgpclZYks_1;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::zjYBUXbgxFkfVKHODHNCeWOHMfQc
	float ___zjYBUXbgxFkfVKHODHNCeWOHMfQc_2;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::CgFpjNWUaxudvYYjWafrhISXfsX
	float ___CgFpjNWUaxudvYYjWafrhISXfsX_3;
	// Rewired.AxisCoordinateMode OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::AzcaYoGJNjQgjFntSQDdTBEQrJJI
	int32_t ___AzcaYoGJNjQgjFntSQDdTBEQrJJI_4;
	// Rewired.AxisCoordinateMode OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::XJzgQnByCGDxnamkpUkReEJkbORp
	int32_t ___XJzgQnByCGDxnamkpUkReEJkbORp_5;
	// Rewired.ButtonStateFlags OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::UWLOvhrjuLGUzrhYiVuOYFsiVgW
	int32_t ___UWLOvhrjuLGUzrhYiVuOYFsiVgW_6;
	// Rewired.ButtonStateFlags OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::jZuVxjpUtIHgWikNwxZtQcMRgGB
	int32_t ___jZuVxjpUtIHgWikNwxZtQcMRgGB_7;
	// Rewired.ButtonStateFlags OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::iMEYBpBPvCQGaPNoMfhjVvkHhrSb
	int32_t ___iMEYBpBPvCQGaPNoMfhjVvkHhrSb_8;
	// Rewired.ButtonStateFlags OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::pWXDxpuOcPfcTJvNeqHftHwISveU
	int32_t ___pWXDxpuOcPfcTJvNeqHftHwISveU_9;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::nnqAvqEbNhGPWaKJhFKBxsKIdBiO
	float ___nnqAvqEbNhGPWaKJhFKBxsKIdBiO_10;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::rGTlPXCBdVBtLOWjFDRxKopXiAE
	float ___rGTlPXCBdVBtLOWjFDRxKopXiAE_11;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::ZIimzTsAoQjJCPHJabxdFjKEAjWE
	float ___ZIimzTsAoQjJCPHJabxdFjKEAjWE_12;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::EDtJGekJemucNcdKBHzoKxKvbHp
	float ___EDtJGekJemucNcdKBHzoKxKvbHp_13;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::AOjNPhmpFfSsfgsHzQEuIbBWMmf
	float ___AOjNPhmpFfSsfgsHzQEuIbBWMmf_14;
	// System.Single OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::pNvLaxscwlwCkBrJwGJrJqUsuPi
	float ___pNvLaxscwlwCkBrJwGJrJqUsuPi_15;
	// EhbFMnIgrveIkvxMjDinIUjSQTaf OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::dMgxPdNCqXlVlcUpUsDbXeutRij
	EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5 * ___dMgxPdNCqXlVlcUpUsDbXeutRij_16;
	// EhbFMnIgrveIkvxMjDinIUjSQTaf OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::xMJoTTkcCHywdQKtIpIJTZAzKDp
	EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5 * ___xMJoTTkcCHywdQKtIpIJTZAzKDp_17;
	// Rewired.ButtonStateRecorder OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::JPerCemNbwwhxjieMcolXzApwdg
	ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148 * ___JPerCemNbwwhxjieMcolXzApwdg_18;
	// Rewired.ButtonStateRecorder OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::UhxJMgpGhYKbuyqOMbfuYlAwhVMB
	ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148 * ___UhxJMgpGhYKbuyqOMbfuYlAwhVMB_19;
	// XRsvvJyCXrJjMmEnAyacvMwIrT OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::pyKYQGkOujslKcEyGgJjLcZosGz
	XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3 * ___pyKYQGkOujslKcEyGgJjLcZosGz_20;
	// XRsvvJyCXrJjMmEnAyacvMwIrT OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::zNndbbdmxoHBurCJgVAnUpeNSbuu
	XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3 * ___zNndbbdmxoHBurCJgVAnUpeNSbuu_21;
	// Rewired.Utils.Classes.Utility.TimerAbs OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::RJVzwOkzMvoymRuaJSGzPuRCILq
	TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * ___RJVzwOkzMvoymRuaJSGzPuRCILq_22;
	// Rewired.Utils.Classes.Utility.TimerAbs OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::kdsPsXHldQcEYrcKmrJXiCQcZZU
	TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * ___kdsPsXHldQcEYrcKmrJXiCQcZZU_23;
	// GmNQnYIPchcrVIxASPikMTNzgbC OmePuLXqingOShAfdEjGZVdOnDpG_WjDjjZjmTrLgvroIUzlAqRnRCmzJ_LBaYaJDRjCvogHekEOTUEmcDnEB::CzufUZdOxzNVUAmhknXMtEeNCg
	GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99 * ___CzufUZdOxzNVUAmhknXMtEeNCg_24;

public:
	inline static int32_t get_offset_of_xBYHLUKVdSEjOEPIkUktZiSGzVBE_0() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___xBYHLUKVdSEjOEPIkUktZiSGzVBE_0)); }
	inline float get_xBYHLUKVdSEjOEPIkUktZiSGzVBE_0() const { return ___xBYHLUKVdSEjOEPIkUktZiSGzVBE_0; }
	inline float* get_address_of_xBYHLUKVdSEjOEPIkUktZiSGzVBE_0() { return &___xBYHLUKVdSEjOEPIkUktZiSGzVBE_0; }
	inline void set_xBYHLUKVdSEjOEPIkUktZiSGzVBE_0(float value)
	{
		___xBYHLUKVdSEjOEPIkUktZiSGzVBE_0 = value;
	}

	inline static int32_t get_offset_of_kNThXpJRuNjadiuBHlBgpclZYks_1() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___kNThXpJRuNjadiuBHlBgpclZYks_1)); }
	inline InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B * get_kNThXpJRuNjadiuBHlBgpclZYks_1() const { return ___kNThXpJRuNjadiuBHlBgpclZYks_1; }
	inline InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B ** get_address_of_kNThXpJRuNjadiuBHlBgpclZYks_1() { return &___kNThXpJRuNjadiuBHlBgpclZYks_1; }
	inline void set_kNThXpJRuNjadiuBHlBgpclZYks_1(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B * value)
	{
		___kNThXpJRuNjadiuBHlBgpclZYks_1 = value;
		Il2CppCodeGenWriteBarrier((&___kNThXpJRuNjadiuBHlBgpclZYks_1), value);
	}

	inline static int32_t get_offset_of_zjYBUXbgxFkfVKHODHNCeWOHMfQc_2() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___zjYBUXbgxFkfVKHODHNCeWOHMfQc_2)); }
	inline float get_zjYBUXbgxFkfVKHODHNCeWOHMfQc_2() const { return ___zjYBUXbgxFkfVKHODHNCeWOHMfQc_2; }
	inline float* get_address_of_zjYBUXbgxFkfVKHODHNCeWOHMfQc_2() { return &___zjYBUXbgxFkfVKHODHNCeWOHMfQc_2; }
	inline void set_zjYBUXbgxFkfVKHODHNCeWOHMfQc_2(float value)
	{
		___zjYBUXbgxFkfVKHODHNCeWOHMfQc_2 = value;
	}

	inline static int32_t get_offset_of_CgFpjNWUaxudvYYjWafrhISXfsX_3() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___CgFpjNWUaxudvYYjWafrhISXfsX_3)); }
	inline float get_CgFpjNWUaxudvYYjWafrhISXfsX_3() const { return ___CgFpjNWUaxudvYYjWafrhISXfsX_3; }
	inline float* get_address_of_CgFpjNWUaxudvYYjWafrhISXfsX_3() { return &___CgFpjNWUaxudvYYjWafrhISXfsX_3; }
	inline void set_CgFpjNWUaxudvYYjWafrhISXfsX_3(float value)
	{
		___CgFpjNWUaxudvYYjWafrhISXfsX_3 = value;
	}

	inline static int32_t get_offset_of_AzcaYoGJNjQgjFntSQDdTBEQrJJI_4() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___AzcaYoGJNjQgjFntSQDdTBEQrJJI_4)); }
	inline int32_t get_AzcaYoGJNjQgjFntSQDdTBEQrJJI_4() const { return ___AzcaYoGJNjQgjFntSQDdTBEQrJJI_4; }
	inline int32_t* get_address_of_AzcaYoGJNjQgjFntSQDdTBEQrJJI_4() { return &___AzcaYoGJNjQgjFntSQDdTBEQrJJI_4; }
	inline void set_AzcaYoGJNjQgjFntSQDdTBEQrJJI_4(int32_t value)
	{
		___AzcaYoGJNjQgjFntSQDdTBEQrJJI_4 = value;
	}

	inline static int32_t get_offset_of_XJzgQnByCGDxnamkpUkReEJkbORp_5() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___XJzgQnByCGDxnamkpUkReEJkbORp_5)); }
	inline int32_t get_XJzgQnByCGDxnamkpUkReEJkbORp_5() const { return ___XJzgQnByCGDxnamkpUkReEJkbORp_5; }
	inline int32_t* get_address_of_XJzgQnByCGDxnamkpUkReEJkbORp_5() { return &___XJzgQnByCGDxnamkpUkReEJkbORp_5; }
	inline void set_XJzgQnByCGDxnamkpUkReEJkbORp_5(int32_t value)
	{
		___XJzgQnByCGDxnamkpUkReEJkbORp_5 = value;
	}

	inline static int32_t get_offset_of_UWLOvhrjuLGUzrhYiVuOYFsiVgW_6() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___UWLOvhrjuLGUzrhYiVuOYFsiVgW_6)); }
	inline int32_t get_UWLOvhrjuLGUzrhYiVuOYFsiVgW_6() const { return ___UWLOvhrjuLGUzrhYiVuOYFsiVgW_6; }
	inline int32_t* get_address_of_UWLOvhrjuLGUzrhYiVuOYFsiVgW_6() { return &___UWLOvhrjuLGUzrhYiVuOYFsiVgW_6; }
	inline void set_UWLOvhrjuLGUzrhYiVuOYFsiVgW_6(int32_t value)
	{
		___UWLOvhrjuLGUzrhYiVuOYFsiVgW_6 = value;
	}

	inline static int32_t get_offset_of_jZuVxjpUtIHgWikNwxZtQcMRgGB_7() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___jZuVxjpUtIHgWikNwxZtQcMRgGB_7)); }
	inline int32_t get_jZuVxjpUtIHgWikNwxZtQcMRgGB_7() const { return ___jZuVxjpUtIHgWikNwxZtQcMRgGB_7; }
	inline int32_t* get_address_of_jZuVxjpUtIHgWikNwxZtQcMRgGB_7() { return &___jZuVxjpUtIHgWikNwxZtQcMRgGB_7; }
	inline void set_jZuVxjpUtIHgWikNwxZtQcMRgGB_7(int32_t value)
	{
		___jZuVxjpUtIHgWikNwxZtQcMRgGB_7 = value;
	}

	inline static int32_t get_offset_of_iMEYBpBPvCQGaPNoMfhjVvkHhrSb_8() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___iMEYBpBPvCQGaPNoMfhjVvkHhrSb_8)); }
	inline int32_t get_iMEYBpBPvCQGaPNoMfhjVvkHhrSb_8() const { return ___iMEYBpBPvCQGaPNoMfhjVvkHhrSb_8; }
	inline int32_t* get_address_of_iMEYBpBPvCQGaPNoMfhjVvkHhrSb_8() { return &___iMEYBpBPvCQGaPNoMfhjVvkHhrSb_8; }
	inline void set_iMEYBpBPvCQGaPNoMfhjVvkHhrSb_8(int32_t value)
	{
		___iMEYBpBPvCQGaPNoMfhjVvkHhrSb_8 = value;
	}

	inline static int32_t get_offset_of_pWXDxpuOcPfcTJvNeqHftHwISveU_9() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___pWXDxpuOcPfcTJvNeqHftHwISveU_9)); }
	inline int32_t get_pWXDxpuOcPfcTJvNeqHftHwISveU_9() const { return ___pWXDxpuOcPfcTJvNeqHftHwISveU_9; }
	inline int32_t* get_address_of_pWXDxpuOcPfcTJvNeqHftHwISveU_9() { return &___pWXDxpuOcPfcTJvNeqHftHwISveU_9; }
	inline void set_pWXDxpuOcPfcTJvNeqHftHwISveU_9(int32_t value)
	{
		___pWXDxpuOcPfcTJvNeqHftHwISveU_9 = value;
	}

	inline static int32_t get_offset_of_nnqAvqEbNhGPWaKJhFKBxsKIdBiO_10() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___nnqAvqEbNhGPWaKJhFKBxsKIdBiO_10)); }
	inline float get_nnqAvqEbNhGPWaKJhFKBxsKIdBiO_10() const { return ___nnqAvqEbNhGPWaKJhFKBxsKIdBiO_10; }
	inline float* get_address_of_nnqAvqEbNhGPWaKJhFKBxsKIdBiO_10() { return &___nnqAvqEbNhGPWaKJhFKBxsKIdBiO_10; }
	inline void set_nnqAvqEbNhGPWaKJhFKBxsKIdBiO_10(float value)
	{
		___nnqAvqEbNhGPWaKJhFKBxsKIdBiO_10 = value;
	}

	inline static int32_t get_offset_of_rGTlPXCBdVBtLOWjFDRxKopXiAE_11() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___rGTlPXCBdVBtLOWjFDRxKopXiAE_11)); }
	inline float get_rGTlPXCBdVBtLOWjFDRxKopXiAE_11() const { return ___rGTlPXCBdVBtLOWjFDRxKopXiAE_11; }
	inline float* get_address_of_rGTlPXCBdVBtLOWjFDRxKopXiAE_11() { return &___rGTlPXCBdVBtLOWjFDRxKopXiAE_11; }
	inline void set_rGTlPXCBdVBtLOWjFDRxKopXiAE_11(float value)
	{
		___rGTlPXCBdVBtLOWjFDRxKopXiAE_11 = value;
	}

	inline static int32_t get_offset_of_ZIimzTsAoQjJCPHJabxdFjKEAjWE_12() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___ZIimzTsAoQjJCPHJabxdFjKEAjWE_12)); }
	inline float get_ZIimzTsAoQjJCPHJabxdFjKEAjWE_12() const { return ___ZIimzTsAoQjJCPHJabxdFjKEAjWE_12; }
	inline float* get_address_of_ZIimzTsAoQjJCPHJabxdFjKEAjWE_12() { return &___ZIimzTsAoQjJCPHJabxdFjKEAjWE_12; }
	inline void set_ZIimzTsAoQjJCPHJabxdFjKEAjWE_12(float value)
	{
		___ZIimzTsAoQjJCPHJabxdFjKEAjWE_12 = value;
	}

	inline static int32_t get_offset_of_EDtJGekJemucNcdKBHzoKxKvbHp_13() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___EDtJGekJemucNcdKBHzoKxKvbHp_13)); }
	inline float get_EDtJGekJemucNcdKBHzoKxKvbHp_13() const { return ___EDtJGekJemucNcdKBHzoKxKvbHp_13; }
	inline float* get_address_of_EDtJGekJemucNcdKBHzoKxKvbHp_13() { return &___EDtJGekJemucNcdKBHzoKxKvbHp_13; }
	inline void set_EDtJGekJemucNcdKBHzoKxKvbHp_13(float value)
	{
		___EDtJGekJemucNcdKBHzoKxKvbHp_13 = value;
	}

	inline static int32_t get_offset_of_AOjNPhmpFfSsfgsHzQEuIbBWMmf_14() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___AOjNPhmpFfSsfgsHzQEuIbBWMmf_14)); }
	inline float get_AOjNPhmpFfSsfgsHzQEuIbBWMmf_14() const { return ___AOjNPhmpFfSsfgsHzQEuIbBWMmf_14; }
	inline float* get_address_of_AOjNPhmpFfSsfgsHzQEuIbBWMmf_14() { return &___AOjNPhmpFfSsfgsHzQEuIbBWMmf_14; }
	inline void set_AOjNPhmpFfSsfgsHzQEuIbBWMmf_14(float value)
	{
		___AOjNPhmpFfSsfgsHzQEuIbBWMmf_14 = value;
	}

	inline static int32_t get_offset_of_pNvLaxscwlwCkBrJwGJrJqUsuPi_15() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___pNvLaxscwlwCkBrJwGJrJqUsuPi_15)); }
	inline float get_pNvLaxscwlwCkBrJwGJrJqUsuPi_15() const { return ___pNvLaxscwlwCkBrJwGJrJqUsuPi_15; }
	inline float* get_address_of_pNvLaxscwlwCkBrJwGJrJqUsuPi_15() { return &___pNvLaxscwlwCkBrJwGJrJqUsuPi_15; }
	inline void set_pNvLaxscwlwCkBrJwGJrJqUsuPi_15(float value)
	{
		___pNvLaxscwlwCkBrJwGJrJqUsuPi_15 = value;
	}

	inline static int32_t get_offset_of_dMgxPdNCqXlVlcUpUsDbXeutRij_16() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___dMgxPdNCqXlVlcUpUsDbXeutRij_16)); }
	inline EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5 * get_dMgxPdNCqXlVlcUpUsDbXeutRij_16() const { return ___dMgxPdNCqXlVlcUpUsDbXeutRij_16; }
	inline EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5 ** get_address_of_dMgxPdNCqXlVlcUpUsDbXeutRij_16() { return &___dMgxPdNCqXlVlcUpUsDbXeutRij_16; }
	inline void set_dMgxPdNCqXlVlcUpUsDbXeutRij_16(EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5 * value)
	{
		___dMgxPdNCqXlVlcUpUsDbXeutRij_16 = value;
		Il2CppCodeGenWriteBarrier((&___dMgxPdNCqXlVlcUpUsDbXeutRij_16), value);
	}

	inline static int32_t get_offset_of_xMJoTTkcCHywdQKtIpIJTZAzKDp_17() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___xMJoTTkcCHywdQKtIpIJTZAzKDp_17)); }
	inline EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5 * get_xMJoTTkcCHywdQKtIpIJTZAzKDp_17() const { return ___xMJoTTkcCHywdQKtIpIJTZAzKDp_17; }
	inline EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5 ** get_address_of_xMJoTTkcCHywdQKtIpIJTZAzKDp_17() { return &___xMJoTTkcCHywdQKtIpIJTZAzKDp_17; }
	inline void set_xMJoTTkcCHywdQKtIpIJTZAzKDp_17(EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5 * value)
	{
		___xMJoTTkcCHywdQKtIpIJTZAzKDp_17 = value;
		Il2CppCodeGenWriteBarrier((&___xMJoTTkcCHywdQKtIpIJTZAzKDp_17), value);
	}

	inline static int32_t get_offset_of_JPerCemNbwwhxjieMcolXzApwdg_18() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___JPerCemNbwwhxjieMcolXzApwdg_18)); }
	inline ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148 * get_JPerCemNbwwhxjieMcolXzApwdg_18() const { return ___JPerCemNbwwhxjieMcolXzApwdg_18; }
	inline ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148 ** get_address_of_JPerCemNbwwhxjieMcolXzApwdg_18() { return &___JPerCemNbwwhxjieMcolXzApwdg_18; }
	inline void set_JPerCemNbwwhxjieMcolXzApwdg_18(ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148 * value)
	{
		___JPerCemNbwwhxjieMcolXzApwdg_18 = value;
		Il2CppCodeGenWriteBarrier((&___JPerCemNbwwhxjieMcolXzApwdg_18), value);
	}

	inline static int32_t get_offset_of_UhxJMgpGhYKbuyqOMbfuYlAwhVMB_19() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___UhxJMgpGhYKbuyqOMbfuYlAwhVMB_19)); }
	inline ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148 * get_UhxJMgpGhYKbuyqOMbfuYlAwhVMB_19() const { return ___UhxJMgpGhYKbuyqOMbfuYlAwhVMB_19; }
	inline ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148 ** get_address_of_UhxJMgpGhYKbuyqOMbfuYlAwhVMB_19() { return &___UhxJMgpGhYKbuyqOMbfuYlAwhVMB_19; }
	inline void set_UhxJMgpGhYKbuyqOMbfuYlAwhVMB_19(ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148 * value)
	{
		___UhxJMgpGhYKbuyqOMbfuYlAwhVMB_19 = value;
		Il2CppCodeGenWriteBarrier((&___UhxJMgpGhYKbuyqOMbfuYlAwhVMB_19), value);
	}

	inline static int32_t get_offset_of_pyKYQGkOujslKcEyGgJjLcZosGz_20() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___pyKYQGkOujslKcEyGgJjLcZosGz_20)); }
	inline XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3 * get_pyKYQGkOujslKcEyGgJjLcZosGz_20() const { return ___pyKYQGkOujslKcEyGgJjLcZosGz_20; }
	inline XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3 ** get_address_of_pyKYQGkOujslKcEyGgJjLcZosGz_20() { return &___pyKYQGkOujslKcEyGgJjLcZosGz_20; }
	inline void set_pyKYQGkOujslKcEyGgJjLcZosGz_20(XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3 * value)
	{
		___pyKYQGkOujslKcEyGgJjLcZosGz_20 = value;
		Il2CppCodeGenWriteBarrier((&___pyKYQGkOujslKcEyGgJjLcZosGz_20), value);
	}

	inline static int32_t get_offset_of_zNndbbdmxoHBurCJgVAnUpeNSbuu_21() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___zNndbbdmxoHBurCJgVAnUpeNSbuu_21)); }
	inline XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3 * get_zNndbbdmxoHBurCJgVAnUpeNSbuu_21() const { return ___zNndbbdmxoHBurCJgVAnUpeNSbuu_21; }
	inline XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3 ** get_address_of_zNndbbdmxoHBurCJgVAnUpeNSbuu_21() { return &___zNndbbdmxoHBurCJgVAnUpeNSbuu_21; }
	inline void set_zNndbbdmxoHBurCJgVAnUpeNSbuu_21(XRsvvJyCXrJjMmEnAyacvMwIrT_t4CDEC5638DF179F76E2B57A0F3222FA6B2DA80D3 * value)
	{
		___zNndbbdmxoHBurCJgVAnUpeNSbuu_21 = value;
		Il2CppCodeGenWriteBarrier((&___zNndbbdmxoHBurCJgVAnUpeNSbuu_21), value);
	}

	inline static int32_t get_offset_of_RJVzwOkzMvoymRuaJSGzPuRCILq_22() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___RJVzwOkzMvoymRuaJSGzPuRCILq_22)); }
	inline TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * get_RJVzwOkzMvoymRuaJSGzPuRCILq_22() const { return ___RJVzwOkzMvoymRuaJSGzPuRCILq_22; }
	inline TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 ** get_address_of_RJVzwOkzMvoymRuaJSGzPuRCILq_22() { return &___RJVzwOkzMvoymRuaJSGzPuRCILq_22; }
	inline void set_RJVzwOkzMvoymRuaJSGzPuRCILq_22(TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * value)
	{
		___RJVzwOkzMvoymRuaJSGzPuRCILq_22 = value;
		Il2CppCodeGenWriteBarrier((&___RJVzwOkzMvoymRuaJSGzPuRCILq_22), value);
	}

	inline static int32_t get_offset_of_kdsPsXHldQcEYrcKmrJXiCQcZZU_23() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___kdsPsXHldQcEYrcKmrJXiCQcZZU_23)); }
	inline TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * get_kdsPsXHldQcEYrcKmrJXiCQcZZU_23() const { return ___kdsPsXHldQcEYrcKmrJXiCQcZZU_23; }
	inline TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 ** get_address_of_kdsPsXHldQcEYrcKmrJXiCQcZZU_23() { return &___kdsPsXHldQcEYrcKmrJXiCQcZZU_23; }
	inline void set_kdsPsXHldQcEYrcKmrJXiCQcZZU_23(TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * value)
	{
		___kdsPsXHldQcEYrcKmrJXiCQcZZU_23 = value;
		Il2CppCodeGenWriteBarrier((&___kdsPsXHldQcEYrcKmrJXiCQcZZU_23), value);
	}

	inline static int32_t get_offset_of_CzufUZdOxzNVUAmhknXMtEeNCg_24() { return static_cast<int32_t>(offsetof(LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A, ___CzufUZdOxzNVUAmhknXMtEeNCg_24)); }
	inline GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99 * get_CzufUZdOxzNVUAmhknXMtEeNCg_24() const { return ___CzufUZdOxzNVUAmhknXMtEeNCg_24; }
	inline GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99 ** get_address_of_CzufUZdOxzNVUAmhknXMtEeNCg_24() { return &___CzufUZdOxzNVUAmhknXMtEeNCg_24; }
	inline void set_CzufUZdOxzNVUAmhknXMtEeNCg_24(GmNQnYIPchcrVIxASPikMTNzgbC_t0972372CDB411C81FC52D05D4415722D62DAAF99 * value)
	{
		___CzufUZdOxzNVUAmhknXMtEeNCg_24 = value;
		Il2CppCodeGenWriteBarrier((&___CzufUZdOxzNVUAmhknXMtEeNCg_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LBAYAJDRJCVOGHEKEOTUEMCDNEB_TE08793447A23877E83B61A730E12F94A0B11B25A_H
#ifndef ACTIONELEMENTMAP_T2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7_H
#define ACTIONELEMENTMAP_T2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ActionElementMap
struct  ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7  : public RuntimeObject
{
public:
	// System.Int32 Rewired.ActionElementMap::_actionCategoryId
	int32_t ____actionCategoryId_0;
	// System.Int32 Rewired.ActionElementMap::_actionId
	int32_t ____actionId_1;
	// Rewired.ControllerElementType Rewired.ActionElementMap::_elementType
	int32_t ____elementType_2;
	// System.Int32 Rewired.ActionElementMap::_elementIdentifierId
	int32_t ____elementIdentifierId_3;
	// Rewired.AxisRange Rewired.ActionElementMap::_axisRange
	int32_t ____axisRange_4;
	// System.Boolean Rewired.ActionElementMap::_invert
	bool ____invert_5;
	// Rewired.Pole Rewired.ActionElementMap::_axisContribution
	int32_t ____axisContribution_6;
	// Rewired.KeyboardKeyCode Rewired.ActionElementMap::_keyboardKeyCode
	int32_t ____keyboardKeyCode_7;
	// Rewired.ModifierKey Rewired.ActionElementMap::_modifierKey1
	int32_t ____modifierKey1_8;
	// Rewired.ModifierKey Rewired.ActionElementMap::_modifierKey2
	int32_t ____modifierKey2_9;
	// Rewired.ModifierKey Rewired.ActionElementMap::_modifierKey3
	int32_t ____modifierKey3_10;
	// Rewired.ControllerMap Rewired.ActionElementMap::uNIXhFwhKcbRMaCTeDSCheRwjLdb
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_11;
	// System.Boolean Rewired.ActionElementMap::eWTjKkaJVjCZqrsQHQXMtJYuNhD
	bool ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_12;
	// System.String Rewired.ActionElementMap::BpnqUtABZEySqFzPQqIktIgZaUw
	String_t* ___BpnqUtABZEySqFzPQqIktIgZaUw_13;
	// System.String Rewired.ActionElementMap::EcYoSgaQzXASycflhjuFGZOdPsvy
	String_t* ___EcYoSgaQzXASycflhjuFGZOdPsvy_14;
	// System.Int32 Rewired.ActionElementMap::JcIOHtlyyDcisHJclHvGjyIzQHK
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_15;
	// System.Int32 Rewired.ActionElementMap::WHCIpuTfwdHNjfJipsoGkINikLt
	int32_t ___WHCIpuTfwdHNjfJipsoGkINikLt_16;

public:
	inline static int32_t get_offset_of__actionCategoryId_0() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ____actionCategoryId_0)); }
	inline int32_t get__actionCategoryId_0() const { return ____actionCategoryId_0; }
	inline int32_t* get_address_of__actionCategoryId_0() { return &____actionCategoryId_0; }
	inline void set__actionCategoryId_0(int32_t value)
	{
		____actionCategoryId_0 = value;
	}

	inline static int32_t get_offset_of__actionId_1() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ____actionId_1)); }
	inline int32_t get__actionId_1() const { return ____actionId_1; }
	inline int32_t* get_address_of__actionId_1() { return &____actionId_1; }
	inline void set__actionId_1(int32_t value)
	{
		____actionId_1 = value;
	}

	inline static int32_t get_offset_of__elementType_2() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ____elementType_2)); }
	inline int32_t get__elementType_2() const { return ____elementType_2; }
	inline int32_t* get_address_of__elementType_2() { return &____elementType_2; }
	inline void set__elementType_2(int32_t value)
	{
		____elementType_2 = value;
	}

	inline static int32_t get_offset_of__elementIdentifierId_3() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ____elementIdentifierId_3)); }
	inline int32_t get__elementIdentifierId_3() const { return ____elementIdentifierId_3; }
	inline int32_t* get_address_of__elementIdentifierId_3() { return &____elementIdentifierId_3; }
	inline void set__elementIdentifierId_3(int32_t value)
	{
		____elementIdentifierId_3 = value;
	}

	inline static int32_t get_offset_of__axisRange_4() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ____axisRange_4)); }
	inline int32_t get__axisRange_4() const { return ____axisRange_4; }
	inline int32_t* get_address_of__axisRange_4() { return &____axisRange_4; }
	inline void set__axisRange_4(int32_t value)
	{
		____axisRange_4 = value;
	}

	inline static int32_t get_offset_of__invert_5() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ____invert_5)); }
	inline bool get__invert_5() const { return ____invert_5; }
	inline bool* get_address_of__invert_5() { return &____invert_5; }
	inline void set__invert_5(bool value)
	{
		____invert_5 = value;
	}

	inline static int32_t get_offset_of__axisContribution_6() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ____axisContribution_6)); }
	inline int32_t get__axisContribution_6() const { return ____axisContribution_6; }
	inline int32_t* get_address_of__axisContribution_6() { return &____axisContribution_6; }
	inline void set__axisContribution_6(int32_t value)
	{
		____axisContribution_6 = value;
	}

	inline static int32_t get_offset_of__keyboardKeyCode_7() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ____keyboardKeyCode_7)); }
	inline int32_t get__keyboardKeyCode_7() const { return ____keyboardKeyCode_7; }
	inline int32_t* get_address_of__keyboardKeyCode_7() { return &____keyboardKeyCode_7; }
	inline void set__keyboardKeyCode_7(int32_t value)
	{
		____keyboardKeyCode_7 = value;
	}

	inline static int32_t get_offset_of__modifierKey1_8() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ____modifierKey1_8)); }
	inline int32_t get__modifierKey1_8() const { return ____modifierKey1_8; }
	inline int32_t* get_address_of__modifierKey1_8() { return &____modifierKey1_8; }
	inline void set__modifierKey1_8(int32_t value)
	{
		____modifierKey1_8 = value;
	}

	inline static int32_t get_offset_of__modifierKey2_9() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ____modifierKey2_9)); }
	inline int32_t get__modifierKey2_9() const { return ____modifierKey2_9; }
	inline int32_t* get_address_of__modifierKey2_9() { return &____modifierKey2_9; }
	inline void set__modifierKey2_9(int32_t value)
	{
		____modifierKey2_9 = value;
	}

	inline static int32_t get_offset_of__modifierKey3_10() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ____modifierKey3_10)); }
	inline int32_t get__modifierKey3_10() const { return ____modifierKey3_10; }
	inline int32_t* get_address_of__modifierKey3_10() { return &____modifierKey3_10; }
	inline void set__modifierKey3_10(int32_t value)
	{
		____modifierKey3_10 = value;
	}

	inline static int32_t get_offset_of_uNIXhFwhKcbRMaCTeDSCheRwjLdb_11() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_11)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_uNIXhFwhKcbRMaCTeDSCheRwjLdb_11() const { return ___uNIXhFwhKcbRMaCTeDSCheRwjLdb_11; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_uNIXhFwhKcbRMaCTeDSCheRwjLdb_11() { return &___uNIXhFwhKcbRMaCTeDSCheRwjLdb_11; }
	inline void set_uNIXhFwhKcbRMaCTeDSCheRwjLdb_11(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___uNIXhFwhKcbRMaCTeDSCheRwjLdb_11 = value;
		Il2CppCodeGenWriteBarrier((&___uNIXhFwhKcbRMaCTeDSCheRwjLdb_11), value);
	}

	inline static int32_t get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_12() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_12)); }
	inline bool get_eWTjKkaJVjCZqrsQHQXMtJYuNhD_12() const { return ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_12; }
	inline bool* get_address_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_12() { return &___eWTjKkaJVjCZqrsQHQXMtJYuNhD_12; }
	inline void set_eWTjKkaJVjCZqrsQHQXMtJYuNhD_12(bool value)
	{
		___eWTjKkaJVjCZqrsQHQXMtJYuNhD_12 = value;
	}

	inline static int32_t get_offset_of_BpnqUtABZEySqFzPQqIktIgZaUw_13() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ___BpnqUtABZEySqFzPQqIktIgZaUw_13)); }
	inline String_t* get_BpnqUtABZEySqFzPQqIktIgZaUw_13() const { return ___BpnqUtABZEySqFzPQqIktIgZaUw_13; }
	inline String_t** get_address_of_BpnqUtABZEySqFzPQqIktIgZaUw_13() { return &___BpnqUtABZEySqFzPQqIktIgZaUw_13; }
	inline void set_BpnqUtABZEySqFzPQqIktIgZaUw_13(String_t* value)
	{
		___BpnqUtABZEySqFzPQqIktIgZaUw_13 = value;
		Il2CppCodeGenWriteBarrier((&___BpnqUtABZEySqFzPQqIktIgZaUw_13), value);
	}

	inline static int32_t get_offset_of_EcYoSgaQzXASycflhjuFGZOdPsvy_14() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ___EcYoSgaQzXASycflhjuFGZOdPsvy_14)); }
	inline String_t* get_EcYoSgaQzXASycflhjuFGZOdPsvy_14() const { return ___EcYoSgaQzXASycflhjuFGZOdPsvy_14; }
	inline String_t** get_address_of_EcYoSgaQzXASycflhjuFGZOdPsvy_14() { return &___EcYoSgaQzXASycflhjuFGZOdPsvy_14; }
	inline void set_EcYoSgaQzXASycflhjuFGZOdPsvy_14(String_t* value)
	{
		___EcYoSgaQzXASycflhjuFGZOdPsvy_14 = value;
		Il2CppCodeGenWriteBarrier((&___EcYoSgaQzXASycflhjuFGZOdPsvy_14), value);
	}

	inline static int32_t get_offset_of_JcIOHtlyyDcisHJclHvGjyIzQHK_15() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ___JcIOHtlyyDcisHJclHvGjyIzQHK_15)); }
	inline int32_t get_JcIOHtlyyDcisHJclHvGjyIzQHK_15() const { return ___JcIOHtlyyDcisHJclHvGjyIzQHK_15; }
	inline int32_t* get_address_of_JcIOHtlyyDcisHJclHvGjyIzQHK_15() { return &___JcIOHtlyyDcisHJclHvGjyIzQHK_15; }
	inline void set_JcIOHtlyyDcisHJclHvGjyIzQHK_15(int32_t value)
	{
		___JcIOHtlyyDcisHJclHvGjyIzQHK_15 = value;
	}

	inline static int32_t get_offset_of_WHCIpuTfwdHNjfJipsoGkINikLt_16() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7, ___WHCIpuTfwdHNjfJipsoGkINikLt_16)); }
	inline int32_t get_WHCIpuTfwdHNjfJipsoGkINikLt_16() const { return ___WHCIpuTfwdHNjfJipsoGkINikLt_16; }
	inline int32_t* get_address_of_WHCIpuTfwdHNjfJipsoGkINikLt_16() { return &___WHCIpuTfwdHNjfJipsoGkINikLt_16; }
	inline void set_WHCIpuTfwdHNjfJipsoGkINikLt_16(int32_t value)
	{
		___WHCIpuTfwdHNjfJipsoGkINikLt_16 = value;
	}
};

struct ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7_StaticFields
{
public:
	// System.Int32 Rewired.ActionElementMap::uidCounter
	int32_t ___uidCounter_17;
	// System.Text.StringBuilder Rewired.ActionElementMap::s_toStringSB
	StringBuilder_t * ___s_toStringSB_18;

public:
	inline static int32_t get_offset_of_uidCounter_17() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7_StaticFields, ___uidCounter_17)); }
	inline int32_t get_uidCounter_17() const { return ___uidCounter_17; }
	inline int32_t* get_address_of_uidCounter_17() { return &___uidCounter_17; }
	inline void set_uidCounter_17(int32_t value)
	{
		___uidCounter_17 = value;
	}

	inline static int32_t get_offset_of_s_toStringSB_18() { return static_cast<int32_t>(offsetof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7_StaticFields, ___s_toStringSB_18)); }
	inline StringBuilder_t * get_s_toStringSB_18() const { return ___s_toStringSB_18; }
	inline StringBuilder_t ** get_address_of_s_toStringSB_18() { return &___s_toStringSB_18; }
	inline void set_s_toStringSB_18(StringBuilder_t * value)
	{
		___s_toStringSB_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_toStringSB_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONELEMENTMAP_T2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7_H
#ifndef AXIS2DCALIBRATION_T4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1_H
#define AXIS2DCALIBRATION_T4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Axis2DCalibration
struct  Axis2DCalibration_t4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1  : public RuntimeObject
{
public:
	// Rewired.DeadZone2DType Rewired.Axis2DCalibration::_deadZoneType
	int32_t ____deadZoneType_0;
	// Rewired.AxisSensitivity2DType Rewired.Axis2DCalibration::_sensitivityType
	int32_t ____sensitivityType_1;

public:
	inline static int32_t get_offset_of__deadZoneType_0() { return static_cast<int32_t>(offsetof(Axis2DCalibration_t4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1, ____deadZoneType_0)); }
	inline int32_t get__deadZoneType_0() const { return ____deadZoneType_0; }
	inline int32_t* get_address_of__deadZoneType_0() { return &____deadZoneType_0; }
	inline void set__deadZoneType_0(int32_t value)
	{
		____deadZoneType_0 = value;
	}

	inline static int32_t get_offset_of__sensitivityType_1() { return static_cast<int32_t>(offsetof(Axis2DCalibration_t4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1, ____sensitivityType_1)); }
	inline int32_t get__sensitivityType_1() const { return ____sensitivityType_1; }
	inline int32_t* get_address_of__sensitivityType_1() { return &____sensitivityType_1; }
	inline void set__sensitivityType_1(int32_t value)
	{
		____sensitivityType_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS2DCALIBRATION_T4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1_H
#ifndef AXISCALIBRATION_T46E34D338183B23046A06AD9D8A558F56EAD9E2F_H
#define AXISCALIBRATION_T46E34D338183B23046A06AD9D8A558F56EAD9E2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisCalibration
struct  AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.AlternateAxisCalibrationType Rewired.AxisCalibration::_calibrationMode
	int32_t ____calibrationMode_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Data.Mapping.AxisCalibrationInfo> Rewired.AxisCalibration::_hardwareCalibrations
	Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * ____hardwareCalibrations_1;
	// System.Boolean Rewired.AxisCalibration::_enabled
	bool ____enabled_2;
	// System.Single Rewired.AxisCalibration::_deadZone
	float ____deadZone_3;
	// System.Single Rewired.AxisCalibration::_calibratedZero
	float ____calibratedZero_4;
	// System.Single Rewired.AxisCalibration::_calibratedMin
	float ____calibratedMin_5;
	// System.Single Rewired.AxisCalibration::_calibratedMax
	float ____calibratedMax_6;
	// System.Boolean Rewired.AxisCalibration::_invert
	bool ____invert_7;
	// Rewired.AxisSensitivityType Rewired.AxisCalibration::_sensitivityType
	int32_t ____sensitivityType_8;
	// System.Single Rewired.AxisCalibration::_sensitivity
	float ____sensitivity_9;
	// UnityEngine.AnimationCurve Rewired.AxisCalibration::_sensitivityCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ____sensitivityCurve_10;
	// System.Boolean Rewired.AxisCalibration::_applyRangeCalibration
	bool ____applyRangeCalibration_11;

public:
	inline static int32_t get_offset_of__calibrationMode_0() { return static_cast<int32_t>(offsetof(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F, ____calibrationMode_0)); }
	inline int32_t get__calibrationMode_0() const { return ____calibrationMode_0; }
	inline int32_t* get_address_of__calibrationMode_0() { return &____calibrationMode_0; }
	inline void set__calibrationMode_0(int32_t value)
	{
		____calibrationMode_0 = value;
	}

	inline static int32_t get_offset_of__hardwareCalibrations_1() { return static_cast<int32_t>(offsetof(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F, ____hardwareCalibrations_1)); }
	inline Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * get__hardwareCalibrations_1() const { return ____hardwareCalibrations_1; }
	inline Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 ** get_address_of__hardwareCalibrations_1() { return &____hardwareCalibrations_1; }
	inline void set__hardwareCalibrations_1(Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * value)
	{
		____hardwareCalibrations_1 = value;
		Il2CppCodeGenWriteBarrier((&____hardwareCalibrations_1), value);
	}

	inline static int32_t get_offset_of__enabled_2() { return static_cast<int32_t>(offsetof(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F, ____enabled_2)); }
	inline bool get__enabled_2() const { return ____enabled_2; }
	inline bool* get_address_of__enabled_2() { return &____enabled_2; }
	inline void set__enabled_2(bool value)
	{
		____enabled_2 = value;
	}

	inline static int32_t get_offset_of__deadZone_3() { return static_cast<int32_t>(offsetof(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F, ____deadZone_3)); }
	inline float get__deadZone_3() const { return ____deadZone_3; }
	inline float* get_address_of__deadZone_3() { return &____deadZone_3; }
	inline void set__deadZone_3(float value)
	{
		____deadZone_3 = value;
	}

	inline static int32_t get_offset_of__calibratedZero_4() { return static_cast<int32_t>(offsetof(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F, ____calibratedZero_4)); }
	inline float get__calibratedZero_4() const { return ____calibratedZero_4; }
	inline float* get_address_of__calibratedZero_4() { return &____calibratedZero_4; }
	inline void set__calibratedZero_4(float value)
	{
		____calibratedZero_4 = value;
	}

	inline static int32_t get_offset_of__calibratedMin_5() { return static_cast<int32_t>(offsetof(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F, ____calibratedMin_5)); }
	inline float get__calibratedMin_5() const { return ____calibratedMin_5; }
	inline float* get_address_of__calibratedMin_5() { return &____calibratedMin_5; }
	inline void set__calibratedMin_5(float value)
	{
		____calibratedMin_5 = value;
	}

	inline static int32_t get_offset_of__calibratedMax_6() { return static_cast<int32_t>(offsetof(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F, ____calibratedMax_6)); }
	inline float get__calibratedMax_6() const { return ____calibratedMax_6; }
	inline float* get_address_of__calibratedMax_6() { return &____calibratedMax_6; }
	inline void set__calibratedMax_6(float value)
	{
		____calibratedMax_6 = value;
	}

	inline static int32_t get_offset_of__invert_7() { return static_cast<int32_t>(offsetof(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F, ____invert_7)); }
	inline bool get__invert_7() const { return ____invert_7; }
	inline bool* get_address_of__invert_7() { return &____invert_7; }
	inline void set__invert_7(bool value)
	{
		____invert_7 = value;
	}

	inline static int32_t get_offset_of__sensitivityType_8() { return static_cast<int32_t>(offsetof(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F, ____sensitivityType_8)); }
	inline int32_t get__sensitivityType_8() const { return ____sensitivityType_8; }
	inline int32_t* get_address_of__sensitivityType_8() { return &____sensitivityType_8; }
	inline void set__sensitivityType_8(int32_t value)
	{
		____sensitivityType_8 = value;
	}

	inline static int32_t get_offset_of__sensitivity_9() { return static_cast<int32_t>(offsetof(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F, ____sensitivity_9)); }
	inline float get__sensitivity_9() const { return ____sensitivity_9; }
	inline float* get_address_of__sensitivity_9() { return &____sensitivity_9; }
	inline void set__sensitivity_9(float value)
	{
		____sensitivity_9 = value;
	}

	inline static int32_t get_offset_of__sensitivityCurve_10() { return static_cast<int32_t>(offsetof(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F, ____sensitivityCurve_10)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get__sensitivityCurve_10() const { return ____sensitivityCurve_10; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of__sensitivityCurve_10() { return &____sensitivityCurve_10; }
	inline void set__sensitivityCurve_10(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		____sensitivityCurve_10 = value;
		Il2CppCodeGenWriteBarrier((&____sensitivityCurve_10), value);
	}

	inline static int32_t get_offset_of__applyRangeCalibration_11() { return static_cast<int32_t>(offsetof(AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F, ____applyRangeCalibration_11)); }
	inline bool get__applyRangeCalibration_11() const { return ____applyRangeCalibration_11; }
	inline bool* get_address_of__applyRangeCalibration_11() { return &____applyRangeCalibration_11; }
	inline void set__applyRangeCalibration_11(bool value)
	{
		____applyRangeCalibration_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCALIBRATION_T46E34D338183B23046A06AD9D8A558F56EAD9E2F_H
#ifndef AXISCALIBRATIONDATA_T2929DB24F1620CF33F77A2431FFB5C3889C31171_H
#define AXISCALIBRATIONDATA_T2929DB24F1620CF33F77A2431FFB5C3889C31171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisCalibrationData
struct  AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171 
{
public:
	// System.Boolean Rewired.AxisCalibrationData::enabled
	bool ___enabled_0;
	// System.Single Rewired.AxisCalibrationData::deadZone
	float ___deadZone_1;
	// System.Single Rewired.AxisCalibrationData::zero
	float ___zero_2;
	// System.Single Rewired.AxisCalibrationData::min
	float ___min_3;
	// System.Single Rewired.AxisCalibrationData::max
	float ___max_4;
	// System.Boolean Rewired.AxisCalibrationData::invert
	bool ___invert_5;
	// Rewired.AxisSensitivityType Rewired.AxisCalibrationData::sensitivityType
	int32_t ___sensitivityType_6;
	// System.Single Rewired.AxisCalibrationData::sensitivity
	float ___sensitivity_7;
	// UnityEngine.AnimationCurve Rewired.AxisCalibrationData::sensitivityCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___sensitivityCurve_8;
	// System.Boolean Rewired.AxisCalibrationData::applyRangeCalibration
	bool ___applyRangeCalibration_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Data.Mapping.AxisCalibrationInfo> Rewired.AxisCalibrationData::calibrations
	Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * ___calibrations_10;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_deadZone_1() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___deadZone_1)); }
	inline float get_deadZone_1() const { return ___deadZone_1; }
	inline float* get_address_of_deadZone_1() { return &___deadZone_1; }
	inline void set_deadZone_1(float value)
	{
		___deadZone_1 = value;
	}

	inline static int32_t get_offset_of_zero_2() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___zero_2)); }
	inline float get_zero_2() const { return ___zero_2; }
	inline float* get_address_of_zero_2() { return &___zero_2; }
	inline void set_zero_2(float value)
	{
		___zero_2 = value;
	}

	inline static int32_t get_offset_of_min_3() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___min_3)); }
	inline float get_min_3() const { return ___min_3; }
	inline float* get_address_of_min_3() { return &___min_3; }
	inline void set_min_3(float value)
	{
		___min_3 = value;
	}

	inline static int32_t get_offset_of_max_4() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___max_4)); }
	inline float get_max_4() const { return ___max_4; }
	inline float* get_address_of_max_4() { return &___max_4; }
	inline void set_max_4(float value)
	{
		___max_4 = value;
	}

	inline static int32_t get_offset_of_invert_5() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___invert_5)); }
	inline bool get_invert_5() const { return ___invert_5; }
	inline bool* get_address_of_invert_5() { return &___invert_5; }
	inline void set_invert_5(bool value)
	{
		___invert_5 = value;
	}

	inline static int32_t get_offset_of_sensitivityType_6() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___sensitivityType_6)); }
	inline int32_t get_sensitivityType_6() const { return ___sensitivityType_6; }
	inline int32_t* get_address_of_sensitivityType_6() { return &___sensitivityType_6; }
	inline void set_sensitivityType_6(int32_t value)
	{
		___sensitivityType_6 = value;
	}

	inline static int32_t get_offset_of_sensitivity_7() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___sensitivity_7)); }
	inline float get_sensitivity_7() const { return ___sensitivity_7; }
	inline float* get_address_of_sensitivity_7() { return &___sensitivity_7; }
	inline void set_sensitivity_7(float value)
	{
		___sensitivity_7 = value;
	}

	inline static int32_t get_offset_of_sensitivityCurve_8() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___sensitivityCurve_8)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_sensitivityCurve_8() const { return ___sensitivityCurve_8; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_sensitivityCurve_8() { return &___sensitivityCurve_8; }
	inline void set_sensitivityCurve_8(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___sensitivityCurve_8 = value;
		Il2CppCodeGenWriteBarrier((&___sensitivityCurve_8), value);
	}

	inline static int32_t get_offset_of_applyRangeCalibration_9() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___applyRangeCalibration_9)); }
	inline bool get_applyRangeCalibration_9() const { return ___applyRangeCalibration_9; }
	inline bool* get_address_of_applyRangeCalibration_9() { return &___applyRangeCalibration_9; }
	inline void set_applyRangeCalibration_9(bool value)
	{
		___applyRangeCalibration_9 = value;
	}

	inline static int32_t get_offset_of_calibrations_10() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___calibrations_10)); }
	inline Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * get_calibrations_10() const { return ___calibrations_10; }
	inline Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 ** get_address_of_calibrations_10() { return &___calibrations_10; }
	inline void set_calibrations_10(Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * value)
	{
		___calibrations_10 = value;
		Il2CppCodeGenWriteBarrier((&___calibrations_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.AxisCalibrationData
struct AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171_marshaled_pinvoke
{
	int32_t ___enabled_0;
	float ___deadZone_1;
	float ___zero_2;
	float ___min_3;
	float ___max_4;
	int32_t ___invert_5;
	int32_t ___sensitivityType_6;
	float ___sensitivity_7;
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_pinvoke ___sensitivityCurve_8;
	int32_t ___applyRangeCalibration_9;
	Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * ___calibrations_10;
};
// Native definition for COM marshalling of Rewired.AxisCalibrationData
struct AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171_marshaled_com
{
	int32_t ___enabled_0;
	float ___deadZone_1;
	float ___zero_2;
	float ___min_3;
	float ___max_4;
	int32_t ___invert_5;
	int32_t ___sensitivityType_6;
	float ___sensitivity_7;
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_com* ___sensitivityCurve_8;
	int32_t ___applyRangeCalibration_9;
	Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * ___calibrations_10;
};
#endif // AXISCALIBRATIONDATA_T2929DB24F1620CF33F77A2431FFB5C3889C31171_H
#ifndef CALIBRATIONMAPSAVEDATA_TE4E68C169ADAF1B228EFF24A8A0301FBB3937C71_H
#define CALIBRATIONMAPSAVEDATA_TE4E68C169ADAF1B228EFF24A8A0301FBB3937C71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.CalibrationMapSaveData
struct  CalibrationMapSaveData_tE4E68C169ADAF1B228EFF24A8A0301FBB3937C71  : public RuntimeObject
{
public:
	// Rewired.CalibrationMap Rewired.CalibrationMapSaveData::kCIxZYCRZqQcvNwpCAQgMRtbDyH
	CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 * ___kCIxZYCRZqQcvNwpCAQgMRtbDyH_0;
	// Rewired.ControllerType Rewired.CalibrationMapSaveData::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1;
	// System.String Rewired.CalibrationMapSaveData::fGtUJMoDYGQBkIHcDCTnfmUopvdN
	String_t* ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_2;

public:
	inline static int32_t get_offset_of_kCIxZYCRZqQcvNwpCAQgMRtbDyH_0() { return static_cast<int32_t>(offsetof(CalibrationMapSaveData_tE4E68C169ADAF1B228EFF24A8A0301FBB3937C71, ___kCIxZYCRZqQcvNwpCAQgMRtbDyH_0)); }
	inline CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 * get_kCIxZYCRZqQcvNwpCAQgMRtbDyH_0() const { return ___kCIxZYCRZqQcvNwpCAQgMRtbDyH_0; }
	inline CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 ** get_address_of_kCIxZYCRZqQcvNwpCAQgMRtbDyH_0() { return &___kCIxZYCRZqQcvNwpCAQgMRtbDyH_0; }
	inline void set_kCIxZYCRZqQcvNwpCAQgMRtbDyH_0(CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 * value)
	{
		___kCIxZYCRZqQcvNwpCAQgMRtbDyH_0 = value;
		Il2CppCodeGenWriteBarrier((&___kCIxZYCRZqQcvNwpCAQgMRtbDyH_0), value);
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() { return static_cast<int32_t>(offsetof(CalibrationMapSaveData_tE4E68C169ADAF1B228EFF24A8A0301FBB3937C71, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1 = value;
	}

	inline static int32_t get_offset_of_fGtUJMoDYGQBkIHcDCTnfmUopvdN_2() { return static_cast<int32_t>(offsetof(CalibrationMapSaveData_tE4E68C169ADAF1B228EFF24A8A0301FBB3937C71, ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_2)); }
	inline String_t* get_fGtUJMoDYGQBkIHcDCTnfmUopvdN_2() const { return ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_2; }
	inline String_t** get_address_of_fGtUJMoDYGQBkIHcDCTnfmUopvdN_2() { return &___fGtUJMoDYGQBkIHcDCTnfmUopvdN_2; }
	inline void set_fGtUJMoDYGQBkIHcDCTnfmUopvdN_2(String_t* value)
	{
		___fGtUJMoDYGQBkIHcDCTnfmUopvdN_2 = value;
		Il2CppCodeGenWriteBarrier((&___fGtUJMoDYGQBkIHcDCTnfmUopvdN_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALIBRATIONMAPSAVEDATA_TE4E68C169ADAF1B228EFF24A8A0301FBB3937C71_H
#ifndef CONTROLLERELEMENTIDENTIFIER_TFC073776F9B708D8AC266037F2C7BA1D3C747C66_H
#define CONTROLLERELEMENTIDENTIFIER_TFC073776F9B708D8AC266037F2C7BA1D3C747C66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerElementIdentifier
struct  ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66  : public RuntimeObject
{
public:
	// System.Int32 Rewired.ControllerElementIdentifier::_id
	int32_t ____id_0;
	// System.String Rewired.ControllerElementIdentifier::_name
	String_t* ____name_1;
	// System.String Rewired.ControllerElementIdentifier::_positiveName
	String_t* ____positiveName_2;
	// System.String Rewired.ControllerElementIdentifier::_negativeName
	String_t* ____negativeName_3;
	// Rewired.ControllerElementType Rewired.ControllerElementIdentifier::_elementType
	int32_t ____elementType_4;
	// Rewired.CompoundControllerElementType Rewired.ControllerElementIdentifier::_compoundElementType
	int32_t ____compoundElementType_5;
	// System.Boolean Rewired.ControllerElementIdentifier::isMappableOnPlatform
	bool ___isMappableOnPlatform_6;
	// System.Boolean Rewired.ControllerElementIdentifier::FiGYLzvVYjaKFXwFzEVgnkASbbH
	bool ___FiGYLzvVYjaKFXwFzEVgnkASbbH_7;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66, ____name_1)); }
	inline String_t* get__name_1() const { return ____name_1; }
	inline String_t** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(String_t* value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier((&____name_1), value);
	}

	inline static int32_t get_offset_of__positiveName_2() { return static_cast<int32_t>(offsetof(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66, ____positiveName_2)); }
	inline String_t* get__positiveName_2() const { return ____positiveName_2; }
	inline String_t** get_address_of__positiveName_2() { return &____positiveName_2; }
	inline void set__positiveName_2(String_t* value)
	{
		____positiveName_2 = value;
		Il2CppCodeGenWriteBarrier((&____positiveName_2), value);
	}

	inline static int32_t get_offset_of__negativeName_3() { return static_cast<int32_t>(offsetof(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66, ____negativeName_3)); }
	inline String_t* get__negativeName_3() const { return ____negativeName_3; }
	inline String_t** get_address_of__negativeName_3() { return &____negativeName_3; }
	inline void set__negativeName_3(String_t* value)
	{
		____negativeName_3 = value;
		Il2CppCodeGenWriteBarrier((&____negativeName_3), value);
	}

	inline static int32_t get_offset_of__elementType_4() { return static_cast<int32_t>(offsetof(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66, ____elementType_4)); }
	inline int32_t get__elementType_4() const { return ____elementType_4; }
	inline int32_t* get_address_of__elementType_4() { return &____elementType_4; }
	inline void set__elementType_4(int32_t value)
	{
		____elementType_4 = value;
	}

	inline static int32_t get_offset_of__compoundElementType_5() { return static_cast<int32_t>(offsetof(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66, ____compoundElementType_5)); }
	inline int32_t get__compoundElementType_5() const { return ____compoundElementType_5; }
	inline int32_t* get_address_of__compoundElementType_5() { return &____compoundElementType_5; }
	inline void set__compoundElementType_5(int32_t value)
	{
		____compoundElementType_5 = value;
	}

	inline static int32_t get_offset_of_isMappableOnPlatform_6() { return static_cast<int32_t>(offsetof(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66, ___isMappableOnPlatform_6)); }
	inline bool get_isMappableOnPlatform_6() const { return ___isMappableOnPlatform_6; }
	inline bool* get_address_of_isMappableOnPlatform_6() { return &___isMappableOnPlatform_6; }
	inline void set_isMappableOnPlatform_6(bool value)
	{
		___isMappableOnPlatform_6 = value;
	}

	inline static int32_t get_offset_of_FiGYLzvVYjaKFXwFzEVgnkASbbH_7() { return static_cast<int32_t>(offsetof(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66, ___FiGYLzvVYjaKFXwFzEVgnkASbbH_7)); }
	inline bool get_FiGYLzvVYjaKFXwFzEVgnkASbbH_7() const { return ___FiGYLzvVYjaKFXwFzEVgnkASbbH_7; }
	inline bool* get_address_of_FiGYLzvVYjaKFXwFzEVgnkASbbH_7() { return &___FiGYLzvVYjaKFXwFzEVgnkASbbH_7; }
	inline void set_FiGYLzvVYjaKFXwFzEVgnkASbbH_7(bool value)
	{
		___FiGYLzvVYjaKFXwFzEVgnkASbbH_7 = value;
	}
};

struct ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66_StaticFields
{
public:
	// Rewired.ControllerElementIdentifier Rewired.ControllerElementIdentifier::TfeYSmUqhkiytsLVYgfweLtJVdB
	ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66 * ___TfeYSmUqhkiytsLVYgfweLtJVdB_8;

public:
	inline static int32_t get_offset_of_TfeYSmUqhkiytsLVYgfweLtJVdB_8() { return static_cast<int32_t>(offsetof(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66_StaticFields, ___TfeYSmUqhkiytsLVYgfweLtJVdB_8)); }
	inline ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66 * get_TfeYSmUqhkiytsLVYgfweLtJVdB_8() const { return ___TfeYSmUqhkiytsLVYgfweLtJVdB_8; }
	inline ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66 ** get_address_of_TfeYSmUqhkiytsLVYgfweLtJVdB_8() { return &___TfeYSmUqhkiytsLVYgfweLtJVdB_8; }
	inline void set_TfeYSmUqhkiytsLVYgfweLtJVdB_8(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66 * value)
	{
		___TfeYSmUqhkiytsLVYgfweLtJVdB_8 = value;
		Il2CppCodeGenWriteBarrier((&___TfeYSmUqhkiytsLVYgfweLtJVdB_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTIDENTIFIER_TFC073776F9B708D8AC266037F2C7BA1D3C747C66_H
#ifndef CONTROLLERTEMPLATEELEMENTIDENTIFIER_TDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40_H
#define CONTROLLERTEMPLATEELEMENTIDENTIFIER_TDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplateElementIdentifier
struct  ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40  : public RuntimeObject
{
public:
	// System.Int32 Rewired.ControllerTemplateElementIdentifier::_id
	int32_t ____id_0;
	// System.String Rewired.ControllerTemplateElementIdentifier::_name
	String_t* ____name_1;
	// System.String Rewired.ControllerTemplateElementIdentifier::_positiveName
	String_t* ____positiveName_2;
	// System.String Rewired.ControllerTemplateElementIdentifier::_negativeName
	String_t* ____negativeName_3;
	// Rewired.ControllerTemplateElementType Rewired.ControllerTemplateElementIdentifier::_elementType
	int32_t ____elementType_4;
	// System.Boolean Rewired.ControllerTemplateElementIdentifier::isMappableOnPlatform
	bool ___isMappableOnPlatform_5;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40, ____name_1)); }
	inline String_t* get__name_1() const { return ____name_1; }
	inline String_t** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(String_t* value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier((&____name_1), value);
	}

	inline static int32_t get_offset_of__positiveName_2() { return static_cast<int32_t>(offsetof(ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40, ____positiveName_2)); }
	inline String_t* get__positiveName_2() const { return ____positiveName_2; }
	inline String_t** get_address_of__positiveName_2() { return &____positiveName_2; }
	inline void set__positiveName_2(String_t* value)
	{
		____positiveName_2 = value;
		Il2CppCodeGenWriteBarrier((&____positiveName_2), value);
	}

	inline static int32_t get_offset_of__negativeName_3() { return static_cast<int32_t>(offsetof(ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40, ____negativeName_3)); }
	inline String_t* get__negativeName_3() const { return ____negativeName_3; }
	inline String_t** get_address_of__negativeName_3() { return &____negativeName_3; }
	inline void set__negativeName_3(String_t* value)
	{
		____negativeName_3 = value;
		Il2CppCodeGenWriteBarrier((&____negativeName_3), value);
	}

	inline static int32_t get_offset_of__elementType_4() { return static_cast<int32_t>(offsetof(ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40, ____elementType_4)); }
	inline int32_t get__elementType_4() const { return ____elementType_4; }
	inline int32_t* get_address_of__elementType_4() { return &____elementType_4; }
	inline void set__elementType_4(int32_t value)
	{
		____elementType_4 = value;
	}

	inline static int32_t get_offset_of_isMappableOnPlatform_5() { return static_cast<int32_t>(offsetof(ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40, ___isMappableOnPlatform_5)); }
	inline bool get_isMappableOnPlatform_5() const { return ___isMappableOnPlatform_5; }
	inline bool* get_address_of_isMappableOnPlatform_5() { return &___isMappableOnPlatform_5; }
	inline void set_isMappableOnPlatform_5(bool value)
	{
		___isMappableOnPlatform_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEELEMENTIDENTIFIER_TDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40_H
#ifndef INITARGS_T4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8_H
#define INITARGS_T4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.Drivers.HIDDeviceDriver_InitArgs
struct  InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8  : public RuntimeObject
{
public:
	// Rewired.Config.UpdateLoopSetting Rewired.HID.Drivers.HIDDeviceDriver_InitArgs::updateLoopSetting
	int32_t ___updateLoopSetting_0;
	// Rewired.HID.DeviceConnectionType Rewired.HID.Drivers.HIDDeviceDriver_InitArgs::connectionType
	int32_t ___connectionType_1;
	// System.Int32 Rewired.HID.Drivers.HIDDeviceDriver_InitArgs::minAxisValue
	int32_t ___minAxisValue_2;
	// System.Int32 Rewired.HID.Drivers.HIDDeviceDriver_InitArgs::maxAxisValue
	int32_t ___maxAxisValue_3;
	// System.Int32 Rewired.HID.Drivers.HIDDeviceDriver_InitArgs::hatZeroValue
	int32_t ___hatZeroValue_4;
	// System.Int32 Rewired.HID.Drivers.HIDDeviceDriver_InitArgs::hatSpan
	int32_t ___hatSpan_5;
	// System.Int32 Rewired.HID.Drivers.HIDDeviceDriver_InitArgs::inputReportLength
	int32_t ___inputReportLength_6;
	// System.Int32 Rewired.HID.Drivers.HIDDeviceDriver_InitArgs::outputReportLength
	int32_t ___outputReportLength_7;
	// System.Func`2<Rewired.HID.OutputReport,System.Boolean> Rewired.HID.Drivers.HIDDeviceDriver_InitArgs::synchronousWriteOutputReportDelegate
	Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8 * ___synchronousWriteOutputReportDelegate_8;
	// System.Action`1<Rewired.HID.OutputReport> Rewired.HID.Drivers.HIDDeviceDriver_InitArgs::asynchronousWriteOutputReportDelegate
	Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832 * ___asynchronousWriteOutputReportDelegate_9;

public:
	inline static int32_t get_offset_of_updateLoopSetting_0() { return static_cast<int32_t>(offsetof(InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8, ___updateLoopSetting_0)); }
	inline int32_t get_updateLoopSetting_0() const { return ___updateLoopSetting_0; }
	inline int32_t* get_address_of_updateLoopSetting_0() { return &___updateLoopSetting_0; }
	inline void set_updateLoopSetting_0(int32_t value)
	{
		___updateLoopSetting_0 = value;
	}

	inline static int32_t get_offset_of_connectionType_1() { return static_cast<int32_t>(offsetof(InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8, ___connectionType_1)); }
	inline int32_t get_connectionType_1() const { return ___connectionType_1; }
	inline int32_t* get_address_of_connectionType_1() { return &___connectionType_1; }
	inline void set_connectionType_1(int32_t value)
	{
		___connectionType_1 = value;
	}

	inline static int32_t get_offset_of_minAxisValue_2() { return static_cast<int32_t>(offsetof(InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8, ___minAxisValue_2)); }
	inline int32_t get_minAxisValue_2() const { return ___minAxisValue_2; }
	inline int32_t* get_address_of_minAxisValue_2() { return &___minAxisValue_2; }
	inline void set_minAxisValue_2(int32_t value)
	{
		___minAxisValue_2 = value;
	}

	inline static int32_t get_offset_of_maxAxisValue_3() { return static_cast<int32_t>(offsetof(InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8, ___maxAxisValue_3)); }
	inline int32_t get_maxAxisValue_3() const { return ___maxAxisValue_3; }
	inline int32_t* get_address_of_maxAxisValue_3() { return &___maxAxisValue_3; }
	inline void set_maxAxisValue_3(int32_t value)
	{
		___maxAxisValue_3 = value;
	}

	inline static int32_t get_offset_of_hatZeroValue_4() { return static_cast<int32_t>(offsetof(InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8, ___hatZeroValue_4)); }
	inline int32_t get_hatZeroValue_4() const { return ___hatZeroValue_4; }
	inline int32_t* get_address_of_hatZeroValue_4() { return &___hatZeroValue_4; }
	inline void set_hatZeroValue_4(int32_t value)
	{
		___hatZeroValue_4 = value;
	}

	inline static int32_t get_offset_of_hatSpan_5() { return static_cast<int32_t>(offsetof(InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8, ___hatSpan_5)); }
	inline int32_t get_hatSpan_5() const { return ___hatSpan_5; }
	inline int32_t* get_address_of_hatSpan_5() { return &___hatSpan_5; }
	inline void set_hatSpan_5(int32_t value)
	{
		___hatSpan_5 = value;
	}

	inline static int32_t get_offset_of_inputReportLength_6() { return static_cast<int32_t>(offsetof(InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8, ___inputReportLength_6)); }
	inline int32_t get_inputReportLength_6() const { return ___inputReportLength_6; }
	inline int32_t* get_address_of_inputReportLength_6() { return &___inputReportLength_6; }
	inline void set_inputReportLength_6(int32_t value)
	{
		___inputReportLength_6 = value;
	}

	inline static int32_t get_offset_of_outputReportLength_7() { return static_cast<int32_t>(offsetof(InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8, ___outputReportLength_7)); }
	inline int32_t get_outputReportLength_7() const { return ___outputReportLength_7; }
	inline int32_t* get_address_of_outputReportLength_7() { return &___outputReportLength_7; }
	inline void set_outputReportLength_7(int32_t value)
	{
		___outputReportLength_7 = value;
	}

	inline static int32_t get_offset_of_synchronousWriteOutputReportDelegate_8() { return static_cast<int32_t>(offsetof(InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8, ___synchronousWriteOutputReportDelegate_8)); }
	inline Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8 * get_synchronousWriteOutputReportDelegate_8() const { return ___synchronousWriteOutputReportDelegate_8; }
	inline Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8 ** get_address_of_synchronousWriteOutputReportDelegate_8() { return &___synchronousWriteOutputReportDelegate_8; }
	inline void set_synchronousWriteOutputReportDelegate_8(Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8 * value)
	{
		___synchronousWriteOutputReportDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((&___synchronousWriteOutputReportDelegate_8), value);
	}

	inline static int32_t get_offset_of_asynchronousWriteOutputReportDelegate_9() { return static_cast<int32_t>(offsetof(InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8, ___asynchronousWriteOutputReportDelegate_9)); }
	inline Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832 * get_asynchronousWriteOutputReportDelegate_9() const { return ___asynchronousWriteOutputReportDelegate_9; }
	inline Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832 ** get_address_of_asynchronousWriteOutputReportDelegate_9() { return &___asynchronousWriteOutputReportDelegate_9; }
	inline void set_asynchronousWriteOutputReportDelegate_9(Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832 * value)
	{
		___asynchronousWriteOutputReportDelegate_9 = value;
		Il2CppCodeGenWriteBarrier((&___asynchronousWriteOutputReportDelegate_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITARGS_T4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8_H
#ifndef CVACXXBJFHBTGYDQCOOIGPDEYKAL_TD7DD1276581C9E6FDF35379998DD5CEE6A2567D5_H
#define CVACXXBJFHBTGYDQCOOIGPDEYKAL_TD7DD1276581C9E6FDF35379998DD5CEE6A2567D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDControllerElementWithDataSet_CVaCXXbjFHbTGydqcOoiGPdeykAL
struct  CVaCXXbjFHbTGydqcOoiGPdeykAL_tD7DD1276581C9E6FDF35379998DD5CEE6A2567D5  : public RuntimeObject
{
public:
	// Rewired.UpdateLoopType Rewired.HID.HIDControllerElementWithDataSet_CVaCXXbjFHbTGydqcOoiGPdeykAL::TWKNVXwJTlddHEpGDbHCUyznGXe
	int32_t ___TWKNVXwJTlddHEpGDbHCUyznGXe_0;

public:
	inline static int32_t get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_0() { return static_cast<int32_t>(offsetof(CVaCXXbjFHbTGydqcOoiGPdeykAL_tD7DD1276581C9E6FDF35379998DD5CEE6A2567D5, ___TWKNVXwJTlddHEpGDbHCUyznGXe_0)); }
	inline int32_t get_TWKNVXwJTlddHEpGDbHCUyznGXe_0() const { return ___TWKNVXwJTlddHEpGDbHCUyznGXe_0; }
	inline int32_t* get_address_of_TWKNVXwJTlddHEpGDbHCUyznGXe_0() { return &___TWKNVXwJTlddHEpGDbHCUyznGXe_0; }
	inline void set_TWKNVXwJTlddHEpGDbHCUyznGXe_0(int32_t value)
	{
		___TWKNVXwJTlddHEpGDbHCUyznGXe_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CVACXXBJFHBTGYDQCOOIGPDEYKAL_TD7DD1276581C9E6FDF35379998DD5CEE6A2567D5_H
#ifndef HIDHAT_T71BDF11807DD41A08195396005D5D890157791FF_H
#define HIDHAT_T71BDF11807DD41A08195396005D5D890157791FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDHat
struct  HIDHat_t71BDF11807DD41A08195396005D5D890157791FF  : public HIDControllerElement_tC98B3FAE2975F27917258ACE9B578318D1EAF7E0
{
public:
	// System.Int32 Rewired.HID.HIDHat::rawValue
	int32_t ___rawValue_2;
	// System.Single Rewired.HID.HIDHat::timestamp
	float ___timestamp_3;
	// System.Int32 Rewired.HID.HIDHat::byteLength
	int32_t ___byteLength_4;
	// System.Int32 Rewired.HID.HIDHat::startIndex
	int32_t ___startIndex_5;
	// Rewired.HID.HIDHat_Type Rewired.HID.HIDHat::type
	int32_t ___type_6;
	// System.Func`2<System.Int32,System.Int32> Rewired.HID.HIDHat::agqHqDPvTEqPJElFDveRIhPWjClF
	Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E * ___agqHqDPvTEqPJElFDveRIhPWjClF_7;

public:
	inline static int32_t get_offset_of_rawValue_2() { return static_cast<int32_t>(offsetof(HIDHat_t71BDF11807DD41A08195396005D5D890157791FF, ___rawValue_2)); }
	inline int32_t get_rawValue_2() const { return ___rawValue_2; }
	inline int32_t* get_address_of_rawValue_2() { return &___rawValue_2; }
	inline void set_rawValue_2(int32_t value)
	{
		___rawValue_2 = value;
	}

	inline static int32_t get_offset_of_timestamp_3() { return static_cast<int32_t>(offsetof(HIDHat_t71BDF11807DD41A08195396005D5D890157791FF, ___timestamp_3)); }
	inline float get_timestamp_3() const { return ___timestamp_3; }
	inline float* get_address_of_timestamp_3() { return &___timestamp_3; }
	inline void set_timestamp_3(float value)
	{
		___timestamp_3 = value;
	}

	inline static int32_t get_offset_of_byteLength_4() { return static_cast<int32_t>(offsetof(HIDHat_t71BDF11807DD41A08195396005D5D890157791FF, ___byteLength_4)); }
	inline int32_t get_byteLength_4() const { return ___byteLength_4; }
	inline int32_t* get_address_of_byteLength_4() { return &___byteLength_4; }
	inline void set_byteLength_4(int32_t value)
	{
		___byteLength_4 = value;
	}

	inline static int32_t get_offset_of_startIndex_5() { return static_cast<int32_t>(offsetof(HIDHat_t71BDF11807DD41A08195396005D5D890157791FF, ___startIndex_5)); }
	inline int32_t get_startIndex_5() const { return ___startIndex_5; }
	inline int32_t* get_address_of_startIndex_5() { return &___startIndex_5; }
	inline void set_startIndex_5(int32_t value)
	{
		___startIndex_5 = value;
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(HIDHat_t71BDF11807DD41A08195396005D5D890157791FF, ___type_6)); }
	inline int32_t get_type_6() const { return ___type_6; }
	inline int32_t* get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(int32_t value)
	{
		___type_6 = value;
	}

	inline static int32_t get_offset_of_agqHqDPvTEqPJElFDveRIhPWjClF_7() { return static_cast<int32_t>(offsetof(HIDHat_t71BDF11807DD41A08195396005D5D890157791FF, ___agqHqDPvTEqPJElFDveRIhPWjClF_7)); }
	inline Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E * get_agqHqDPvTEqPJElFDveRIhPWjClF_7() const { return ___agqHqDPvTEqPJElFDveRIhPWjClF_7; }
	inline Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E ** get_address_of_agqHqDPvTEqPJElFDveRIhPWjClF_7() { return &___agqHqDPvTEqPJElFDveRIhPWjClF_7; }
	inline void set_agqHqDPvTEqPJElFDveRIhPWjClF_7(Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E * value)
	{
		___agqHqDPvTEqPJElFDveRIhPWjClF_7 = value;
		Il2CppCodeGenWriteBarrier((&___agqHqDPvTEqPJElFDveRIhPWjClF_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDHAT_T71BDF11807DD41A08195396005D5D890157791FF_H
#ifndef OUTPUTREPORT_TE986EE6C5A1C4E91ECB37D1333990550C69812F5_H
#define OUTPUTREPORT_TE986EE6C5A1C4E91ECB37D1333990550C69812F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.OutputReport
struct  OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5 
{
public:
	// System.Int32 Rewired.HID.OutputReport::reportId
	int32_t ___reportId_0;
	// System.IntPtr Rewired.HID.OutputReport::buffer
	intptr_t ___buffer_1;
	// System.Int32 Rewired.HID.OutputReport::bufferLength
	int32_t ___bufferLength_2;
	// System.Int32 Rewired.HID.OutputReport::reportLength
	int32_t ___reportLength_3;
	// Rewired.HID.OutputReportOptions Rewired.HID.OutputReport::options
	int32_t ___options_4;

public:
	inline static int32_t get_offset_of_reportId_0() { return static_cast<int32_t>(offsetof(OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5, ___reportId_0)); }
	inline int32_t get_reportId_0() const { return ___reportId_0; }
	inline int32_t* get_address_of_reportId_0() { return &___reportId_0; }
	inline void set_reportId_0(int32_t value)
	{
		___reportId_0 = value;
	}

	inline static int32_t get_offset_of_buffer_1() { return static_cast<int32_t>(offsetof(OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5, ___buffer_1)); }
	inline intptr_t get_buffer_1() const { return ___buffer_1; }
	inline intptr_t* get_address_of_buffer_1() { return &___buffer_1; }
	inline void set_buffer_1(intptr_t value)
	{
		___buffer_1 = value;
	}

	inline static int32_t get_offset_of_bufferLength_2() { return static_cast<int32_t>(offsetof(OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5, ___bufferLength_2)); }
	inline int32_t get_bufferLength_2() const { return ___bufferLength_2; }
	inline int32_t* get_address_of_bufferLength_2() { return &___bufferLength_2; }
	inline void set_bufferLength_2(int32_t value)
	{
		___bufferLength_2 = value;
	}

	inline static int32_t get_offset_of_reportLength_3() { return static_cast<int32_t>(offsetof(OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5, ___reportLength_3)); }
	inline int32_t get_reportLength_3() const { return ___reportLength_3; }
	inline int32_t* get_address_of_reportLength_3() { return &___reportLength_3; }
	inline void set_reportLength_3(int32_t value)
	{
		___reportLength_3 = value;
	}

	inline static int32_t get_offset_of_options_4() { return static_cast<int32_t>(offsetof(OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5, ___options_4)); }
	inline int32_t get_options_4() const { return ___options_4; }
	inline int32_t* get_address_of_options_4() { return &___options_4; }
	inline void set_options_4(int32_t value)
	{
		___options_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTREPORT_TE986EE6C5A1C4E91ECB37D1333990550C69812F5_H
#ifndef CUSTOMINPUTMANAGER_TD00B53C595959EFFFF471392F87F7F4E794718DC_H
#define CUSTOMINPUTMANAGER_TD00B53C595959EFFFF471392F87F7F4E794718DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputManagers.CustomInputManager
struct  CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC  : public PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A
{
public:
	// System.Collections.Generic.List`1<Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb> Rewired.InputManagers.CustomInputManager::lZlHpVlqScCxOayHMQQNLIJUPtMA
	List_1_t68D5DD05DB0C6C03A43191A3A25FD11A787C15BD * ___lZlHpVlqScCxOayHMQQNLIJUPtMA_5;
	// System.Int32 Rewired.InputManagers.CustomInputManager::GZolyQbnhDcIglfHmqdnClfPLwP
	int32_t ___GZolyQbnhDcIglfHmqdnClfPLwP_6;
	// Rewired.InputManagers.CustomInputManager_igstrunnCuMHZFMjGQSDGcyfZnU Rewired.InputManagers.CustomInputManager::iswBkwCyDabLmfxdAYpLbXBWfvHN
	igstrunnCuMHZFMjGQSDGcyfZnU_t8BA0D7526D539420345A84552152540A82348119 * ___iswBkwCyDabLmfxdAYpLbXBWfvHN_7;
	// Rewired.UpdateLoopType Rewired.InputManagers.CustomInputManager::YIgjjtvsNpmLAzEahZSayMqwKAO
	int32_t ___YIgjjtvsNpmLAzEahZSayMqwKAO_8;
	// System.Action`2<System.Int32,Rewired.ControllerDataUpdater> Rewired.InputManagers.CustomInputManager::nEiQhTKZDBYqGTkqzeEgrkzxQan
	Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A * ___nEiQhTKZDBYqGTkqzeEgrkzxQan_9;
	// Rewired.PlatformInputManager Rewired.InputManagers.CustomInputManager::fVwZoOiZNlMKSCdQWxpuPNZKDrf
	PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A * ___fVwZoOiZNlMKSCdQWxpuPNZKDrf_10;
	// Rewired.Platforms.Custom.CustomInputSource Rewired.InputManagers.CustomInputManager::siaFEtqGznPqyDTNAcRWMeuzqae
	CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24 * ___siaFEtqGznPqyDTNAcRWMeuzqae_11;
	// System.Boolean Rewired.InputManagers.CustomInputManager::cteekJgqyXKoMIQGdQUhjuJXMhSq
	bool ___cteekJgqyXKoMIQGdQUhjuJXMhSq_12;
	// System.Func`2<Rewired.BridgedControllerHWInfo,Rewired.HardwareJoystickMap_InputManager> Rewired.InputManagers.CustomInputManager::QEAEcDsIVKSxlRksWqQtzkbPdqrh
	Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_13;
	// System.Func`1<System.Int32> Rewired.InputManagers.CustomInputManager::ZGtOAcUCAuhJuSLEHTELVSEYNBN
	Func_1_t30631A63BE46FE93700939B764202D360449FE30 * ___ZGtOAcUCAuhJuSLEHTELVSEYNBN_14;

public:
	inline static int32_t get_offset_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_5() { return static_cast<int32_t>(offsetof(CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC, ___lZlHpVlqScCxOayHMQQNLIJUPtMA_5)); }
	inline List_1_t68D5DD05DB0C6C03A43191A3A25FD11A787C15BD * get_lZlHpVlqScCxOayHMQQNLIJUPtMA_5() const { return ___lZlHpVlqScCxOayHMQQNLIJUPtMA_5; }
	inline List_1_t68D5DD05DB0C6C03A43191A3A25FD11A787C15BD ** get_address_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_5() { return &___lZlHpVlqScCxOayHMQQNLIJUPtMA_5; }
	inline void set_lZlHpVlqScCxOayHMQQNLIJUPtMA_5(List_1_t68D5DD05DB0C6C03A43191A3A25FD11A787C15BD * value)
	{
		___lZlHpVlqScCxOayHMQQNLIJUPtMA_5 = value;
		Il2CppCodeGenWriteBarrier((&___lZlHpVlqScCxOayHMQQNLIJUPtMA_5), value);
	}

	inline static int32_t get_offset_of_GZolyQbnhDcIglfHmqdnClfPLwP_6() { return static_cast<int32_t>(offsetof(CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC, ___GZolyQbnhDcIglfHmqdnClfPLwP_6)); }
	inline int32_t get_GZolyQbnhDcIglfHmqdnClfPLwP_6() const { return ___GZolyQbnhDcIglfHmqdnClfPLwP_6; }
	inline int32_t* get_address_of_GZolyQbnhDcIglfHmqdnClfPLwP_6() { return &___GZolyQbnhDcIglfHmqdnClfPLwP_6; }
	inline void set_GZolyQbnhDcIglfHmqdnClfPLwP_6(int32_t value)
	{
		___GZolyQbnhDcIglfHmqdnClfPLwP_6 = value;
	}

	inline static int32_t get_offset_of_iswBkwCyDabLmfxdAYpLbXBWfvHN_7() { return static_cast<int32_t>(offsetof(CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC, ___iswBkwCyDabLmfxdAYpLbXBWfvHN_7)); }
	inline igstrunnCuMHZFMjGQSDGcyfZnU_t8BA0D7526D539420345A84552152540A82348119 * get_iswBkwCyDabLmfxdAYpLbXBWfvHN_7() const { return ___iswBkwCyDabLmfxdAYpLbXBWfvHN_7; }
	inline igstrunnCuMHZFMjGQSDGcyfZnU_t8BA0D7526D539420345A84552152540A82348119 ** get_address_of_iswBkwCyDabLmfxdAYpLbXBWfvHN_7() { return &___iswBkwCyDabLmfxdAYpLbXBWfvHN_7; }
	inline void set_iswBkwCyDabLmfxdAYpLbXBWfvHN_7(igstrunnCuMHZFMjGQSDGcyfZnU_t8BA0D7526D539420345A84552152540A82348119 * value)
	{
		___iswBkwCyDabLmfxdAYpLbXBWfvHN_7 = value;
		Il2CppCodeGenWriteBarrier((&___iswBkwCyDabLmfxdAYpLbXBWfvHN_7), value);
	}

	inline static int32_t get_offset_of_YIgjjtvsNpmLAzEahZSayMqwKAO_8() { return static_cast<int32_t>(offsetof(CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC, ___YIgjjtvsNpmLAzEahZSayMqwKAO_8)); }
	inline int32_t get_YIgjjtvsNpmLAzEahZSayMqwKAO_8() const { return ___YIgjjtvsNpmLAzEahZSayMqwKAO_8; }
	inline int32_t* get_address_of_YIgjjtvsNpmLAzEahZSayMqwKAO_8() { return &___YIgjjtvsNpmLAzEahZSayMqwKAO_8; }
	inline void set_YIgjjtvsNpmLAzEahZSayMqwKAO_8(int32_t value)
	{
		___YIgjjtvsNpmLAzEahZSayMqwKAO_8 = value;
	}

	inline static int32_t get_offset_of_nEiQhTKZDBYqGTkqzeEgrkzxQan_9() { return static_cast<int32_t>(offsetof(CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC, ___nEiQhTKZDBYqGTkqzeEgrkzxQan_9)); }
	inline Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A * get_nEiQhTKZDBYqGTkqzeEgrkzxQan_9() const { return ___nEiQhTKZDBYqGTkqzeEgrkzxQan_9; }
	inline Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A ** get_address_of_nEiQhTKZDBYqGTkqzeEgrkzxQan_9() { return &___nEiQhTKZDBYqGTkqzeEgrkzxQan_9; }
	inline void set_nEiQhTKZDBYqGTkqzeEgrkzxQan_9(Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A * value)
	{
		___nEiQhTKZDBYqGTkqzeEgrkzxQan_9 = value;
		Il2CppCodeGenWriteBarrier((&___nEiQhTKZDBYqGTkqzeEgrkzxQan_9), value);
	}

	inline static int32_t get_offset_of_fVwZoOiZNlMKSCdQWxpuPNZKDrf_10() { return static_cast<int32_t>(offsetof(CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC, ___fVwZoOiZNlMKSCdQWxpuPNZKDrf_10)); }
	inline PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A * get_fVwZoOiZNlMKSCdQWxpuPNZKDrf_10() const { return ___fVwZoOiZNlMKSCdQWxpuPNZKDrf_10; }
	inline PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A ** get_address_of_fVwZoOiZNlMKSCdQWxpuPNZKDrf_10() { return &___fVwZoOiZNlMKSCdQWxpuPNZKDrf_10; }
	inline void set_fVwZoOiZNlMKSCdQWxpuPNZKDrf_10(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A * value)
	{
		___fVwZoOiZNlMKSCdQWxpuPNZKDrf_10 = value;
		Il2CppCodeGenWriteBarrier((&___fVwZoOiZNlMKSCdQWxpuPNZKDrf_10), value);
	}

	inline static int32_t get_offset_of_siaFEtqGznPqyDTNAcRWMeuzqae_11() { return static_cast<int32_t>(offsetof(CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC, ___siaFEtqGznPqyDTNAcRWMeuzqae_11)); }
	inline CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24 * get_siaFEtqGznPqyDTNAcRWMeuzqae_11() const { return ___siaFEtqGznPqyDTNAcRWMeuzqae_11; }
	inline CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24 ** get_address_of_siaFEtqGznPqyDTNAcRWMeuzqae_11() { return &___siaFEtqGznPqyDTNAcRWMeuzqae_11; }
	inline void set_siaFEtqGznPqyDTNAcRWMeuzqae_11(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24 * value)
	{
		___siaFEtqGznPqyDTNAcRWMeuzqae_11 = value;
		Il2CppCodeGenWriteBarrier((&___siaFEtqGznPqyDTNAcRWMeuzqae_11), value);
	}

	inline static int32_t get_offset_of_cteekJgqyXKoMIQGdQUhjuJXMhSq_12() { return static_cast<int32_t>(offsetof(CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC, ___cteekJgqyXKoMIQGdQUhjuJXMhSq_12)); }
	inline bool get_cteekJgqyXKoMIQGdQUhjuJXMhSq_12() const { return ___cteekJgqyXKoMIQGdQUhjuJXMhSq_12; }
	inline bool* get_address_of_cteekJgqyXKoMIQGdQUhjuJXMhSq_12() { return &___cteekJgqyXKoMIQGdQUhjuJXMhSq_12; }
	inline void set_cteekJgqyXKoMIQGdQUhjuJXMhSq_12(bool value)
	{
		___cteekJgqyXKoMIQGdQUhjuJXMhSq_12 = value;
	}

	inline static int32_t get_offset_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_13() { return static_cast<int32_t>(offsetof(CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC, ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_13)); }
	inline Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * get_QEAEcDsIVKSxlRksWqQtzkbPdqrh_13() const { return ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_13; }
	inline Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF ** get_address_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_13() { return &___QEAEcDsIVKSxlRksWqQtzkbPdqrh_13; }
	inline void set_QEAEcDsIVKSxlRksWqQtzkbPdqrh_13(Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * value)
	{
		___QEAEcDsIVKSxlRksWqQtzkbPdqrh_13 = value;
		Il2CppCodeGenWriteBarrier((&___QEAEcDsIVKSxlRksWqQtzkbPdqrh_13), value);
	}

	inline static int32_t get_offset_of_ZGtOAcUCAuhJuSLEHTELVSEYNBN_14() { return static_cast<int32_t>(offsetof(CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC, ___ZGtOAcUCAuhJuSLEHTELVSEYNBN_14)); }
	inline Func_1_t30631A63BE46FE93700939B764202D360449FE30 * get_ZGtOAcUCAuhJuSLEHTELVSEYNBN_14() const { return ___ZGtOAcUCAuhJuSLEHTELVSEYNBN_14; }
	inline Func_1_t30631A63BE46FE93700939B764202D360449FE30 ** get_address_of_ZGtOAcUCAuhJuSLEHTELVSEYNBN_14() { return &___ZGtOAcUCAuhJuSLEHTELVSEYNBN_14; }
	inline void set_ZGtOAcUCAuhJuSLEHTELVSEYNBN_14(Func_1_t30631A63BE46FE93700939B764202D360449FE30 * value)
	{
		___ZGtOAcUCAuhJuSLEHTELVSEYNBN_14 = value;
		Il2CppCodeGenWriteBarrier((&___ZGtOAcUCAuhJuSLEHTELVSEYNBN_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMINPUTMANAGER_TD00B53C595959EFFFF471392F87F7F4E794718DC_H
#ifndef OSBOCXHMGYBBOUDBLKGMAEXKFJB_TE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49_H
#define OSBOCXHMGYBBOUDBLKGMAEXKFJB_TE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb
struct  OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49  : public RuntimeObject
{
public:
	// Rewired.InputSource Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::lHdmDauerBwdDwLUcaCxBythtdWu
	int32_t ___lHdmDauerBwdDwLUcaCxBythtdWu_0;
	// Rewired.Platforms.Custom.CustomInputSource Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::siaFEtqGznPqyDTNAcRWMeuzqae
	CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24 * ___siaFEtqGznPqyDTNAcRWMeuzqae_1;
	// Rewired.Controller_Extension Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::keBYOCobzoABWhYylPhjApVSLXnN
	Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * ___keBYOCobzoABWhYylPhjApVSLXnN_2;
	// System.Int32 Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::avoCosfGauXluftrTLZWMUMjMSvo
	int32_t ___avoCosfGauXluftrTLZWMUMjMSvo_3;
	// System.Int32 Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::vHhTUeEjpjXweHfcvokTPubqCBh
	int32_t ___vHhTUeEjpjXweHfcvokTPubqCBh_4;
	// System.Nullable`1<System.Int64> Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::kNCtRZYNhfpgNEjWCvYWWaJdsPw
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___kNCtRZYNhfpgNEjWCvYWWaJdsPw_5;
	// System.Int32 Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::pVMgxVlhOWBbMBjIelvXUbEMRHTh
	int32_t ___pVMgxVlhOWBbMBjIelvXUbEMRHTh_6;
	// System.Guid Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::mMvnhxixERRYbVLPXEwyRrtXYwt
	Guid_t  ___mMvnhxixERRYbVLPXEwyRrtXYwt_7;
	// System.String Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::qTNEkibhoLvLepXYZmXNSItgadN
	String_t* ___qTNEkibhoLvLepXYZmXNSItgadN_8;
	// System.String Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::WSWhrfsXjaoUEREiHmegKVMtbnt
	String_t* ___WSWhrfsXjaoUEREiHmegKVMtbnt_9;
	// System.Int32 Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::itTafNyeQoucrbhPkPsSqRewAEI
	int32_t ___itTafNyeQoucrbhPkPsSqRewAEI_10;
	// System.Int32 Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::tOaTpOwzcPDCHdCDIyYaeuASgAM
	int32_t ___tOaTpOwzcPDCHdCDIyYaeuASgAM_11;
	// System.Single[] Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::iKsRvufntavyFjWZbUibgspoftw
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___iKsRvufntavyFjWZbUibgspoftw_12;
	// System.Boolean[] Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::YuSoQESZTtPuhZKVJFBavCSGQsh
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___YuSoQESZTtPuhZKVJFBavCSGQsh_13;
	// Rewired.HardwareJoystickMap_InputManager Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::motlnrXwRclwbbdquGWkOEYsYFy
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 * ___motlnrXwRclwbbdquGWkOEYsYFy_14;
	// Rewired.Platforms.Custom.CustomInputSource_Joystick Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::DLgnRYsKBieSednqoFxsgeQMfJE
	Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09 * ___DLgnRYsKBieSednqoFxsgeQMfJE_15;
	// System.Boolean Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::uTrFqghzwmUUNLMxoQSRVgfDfsj
	bool ___uTrFqghzwmUUNLMxoQSRVgfDfsj_16;
	// System.Func`2<Rewired.BridgedControllerHWInfo,Rewired.HardwareJoystickMap_InputManager> Rewired.InputManagers.CustomInputManager_OSbOCXhMGybBoudblkgMAExKfJb::QEAEcDsIVKSxlRksWqQtzkbPdqrh
	Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_17;

public:
	inline static int32_t get_offset_of_lHdmDauerBwdDwLUcaCxBythtdWu_0() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___lHdmDauerBwdDwLUcaCxBythtdWu_0)); }
	inline int32_t get_lHdmDauerBwdDwLUcaCxBythtdWu_0() const { return ___lHdmDauerBwdDwLUcaCxBythtdWu_0; }
	inline int32_t* get_address_of_lHdmDauerBwdDwLUcaCxBythtdWu_0() { return &___lHdmDauerBwdDwLUcaCxBythtdWu_0; }
	inline void set_lHdmDauerBwdDwLUcaCxBythtdWu_0(int32_t value)
	{
		___lHdmDauerBwdDwLUcaCxBythtdWu_0 = value;
	}

	inline static int32_t get_offset_of_siaFEtqGznPqyDTNAcRWMeuzqae_1() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___siaFEtqGznPqyDTNAcRWMeuzqae_1)); }
	inline CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24 * get_siaFEtqGznPqyDTNAcRWMeuzqae_1() const { return ___siaFEtqGznPqyDTNAcRWMeuzqae_1; }
	inline CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24 ** get_address_of_siaFEtqGznPqyDTNAcRWMeuzqae_1() { return &___siaFEtqGznPqyDTNAcRWMeuzqae_1; }
	inline void set_siaFEtqGznPqyDTNAcRWMeuzqae_1(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24 * value)
	{
		___siaFEtqGznPqyDTNAcRWMeuzqae_1 = value;
		Il2CppCodeGenWriteBarrier((&___siaFEtqGznPqyDTNAcRWMeuzqae_1), value);
	}

	inline static int32_t get_offset_of_keBYOCobzoABWhYylPhjApVSLXnN_2() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___keBYOCobzoABWhYylPhjApVSLXnN_2)); }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * get_keBYOCobzoABWhYylPhjApVSLXnN_2() const { return ___keBYOCobzoABWhYylPhjApVSLXnN_2; }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD ** get_address_of_keBYOCobzoABWhYylPhjApVSLXnN_2() { return &___keBYOCobzoABWhYylPhjApVSLXnN_2; }
	inline void set_keBYOCobzoABWhYylPhjApVSLXnN_2(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * value)
	{
		___keBYOCobzoABWhYylPhjApVSLXnN_2 = value;
		Il2CppCodeGenWriteBarrier((&___keBYOCobzoABWhYylPhjApVSLXnN_2), value);
	}

	inline static int32_t get_offset_of_avoCosfGauXluftrTLZWMUMjMSvo_3() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___avoCosfGauXluftrTLZWMUMjMSvo_3)); }
	inline int32_t get_avoCosfGauXluftrTLZWMUMjMSvo_3() const { return ___avoCosfGauXluftrTLZWMUMjMSvo_3; }
	inline int32_t* get_address_of_avoCosfGauXluftrTLZWMUMjMSvo_3() { return &___avoCosfGauXluftrTLZWMUMjMSvo_3; }
	inline void set_avoCosfGauXluftrTLZWMUMjMSvo_3(int32_t value)
	{
		___avoCosfGauXluftrTLZWMUMjMSvo_3 = value;
	}

	inline static int32_t get_offset_of_vHhTUeEjpjXweHfcvokTPubqCBh_4() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___vHhTUeEjpjXweHfcvokTPubqCBh_4)); }
	inline int32_t get_vHhTUeEjpjXweHfcvokTPubqCBh_4() const { return ___vHhTUeEjpjXweHfcvokTPubqCBh_4; }
	inline int32_t* get_address_of_vHhTUeEjpjXweHfcvokTPubqCBh_4() { return &___vHhTUeEjpjXweHfcvokTPubqCBh_4; }
	inline void set_vHhTUeEjpjXweHfcvokTPubqCBh_4(int32_t value)
	{
		___vHhTUeEjpjXweHfcvokTPubqCBh_4 = value;
	}

	inline static int32_t get_offset_of_kNCtRZYNhfpgNEjWCvYWWaJdsPw_5() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___kNCtRZYNhfpgNEjWCvYWWaJdsPw_5)); }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  get_kNCtRZYNhfpgNEjWCvYWWaJdsPw_5() const { return ___kNCtRZYNhfpgNEjWCvYWWaJdsPw_5; }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 * get_address_of_kNCtRZYNhfpgNEjWCvYWWaJdsPw_5() { return &___kNCtRZYNhfpgNEjWCvYWWaJdsPw_5; }
	inline void set_kNCtRZYNhfpgNEjWCvYWWaJdsPw_5(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  value)
	{
		___kNCtRZYNhfpgNEjWCvYWWaJdsPw_5 = value;
	}

	inline static int32_t get_offset_of_pVMgxVlhOWBbMBjIelvXUbEMRHTh_6() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___pVMgxVlhOWBbMBjIelvXUbEMRHTh_6)); }
	inline int32_t get_pVMgxVlhOWBbMBjIelvXUbEMRHTh_6() const { return ___pVMgxVlhOWBbMBjIelvXUbEMRHTh_6; }
	inline int32_t* get_address_of_pVMgxVlhOWBbMBjIelvXUbEMRHTh_6() { return &___pVMgxVlhOWBbMBjIelvXUbEMRHTh_6; }
	inline void set_pVMgxVlhOWBbMBjIelvXUbEMRHTh_6(int32_t value)
	{
		___pVMgxVlhOWBbMBjIelvXUbEMRHTh_6 = value;
	}

	inline static int32_t get_offset_of_mMvnhxixERRYbVLPXEwyRrtXYwt_7() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___mMvnhxixERRYbVLPXEwyRrtXYwt_7)); }
	inline Guid_t  get_mMvnhxixERRYbVLPXEwyRrtXYwt_7() const { return ___mMvnhxixERRYbVLPXEwyRrtXYwt_7; }
	inline Guid_t * get_address_of_mMvnhxixERRYbVLPXEwyRrtXYwt_7() { return &___mMvnhxixERRYbVLPXEwyRrtXYwt_7; }
	inline void set_mMvnhxixERRYbVLPXEwyRrtXYwt_7(Guid_t  value)
	{
		___mMvnhxixERRYbVLPXEwyRrtXYwt_7 = value;
	}

	inline static int32_t get_offset_of_qTNEkibhoLvLepXYZmXNSItgadN_8() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___qTNEkibhoLvLepXYZmXNSItgadN_8)); }
	inline String_t* get_qTNEkibhoLvLepXYZmXNSItgadN_8() const { return ___qTNEkibhoLvLepXYZmXNSItgadN_8; }
	inline String_t** get_address_of_qTNEkibhoLvLepXYZmXNSItgadN_8() { return &___qTNEkibhoLvLepXYZmXNSItgadN_8; }
	inline void set_qTNEkibhoLvLepXYZmXNSItgadN_8(String_t* value)
	{
		___qTNEkibhoLvLepXYZmXNSItgadN_8 = value;
		Il2CppCodeGenWriteBarrier((&___qTNEkibhoLvLepXYZmXNSItgadN_8), value);
	}

	inline static int32_t get_offset_of_WSWhrfsXjaoUEREiHmegKVMtbnt_9() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___WSWhrfsXjaoUEREiHmegKVMtbnt_9)); }
	inline String_t* get_WSWhrfsXjaoUEREiHmegKVMtbnt_9() const { return ___WSWhrfsXjaoUEREiHmegKVMtbnt_9; }
	inline String_t** get_address_of_WSWhrfsXjaoUEREiHmegKVMtbnt_9() { return &___WSWhrfsXjaoUEREiHmegKVMtbnt_9; }
	inline void set_WSWhrfsXjaoUEREiHmegKVMtbnt_9(String_t* value)
	{
		___WSWhrfsXjaoUEREiHmegKVMtbnt_9 = value;
		Il2CppCodeGenWriteBarrier((&___WSWhrfsXjaoUEREiHmegKVMtbnt_9), value);
	}

	inline static int32_t get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_10() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___itTafNyeQoucrbhPkPsSqRewAEI_10)); }
	inline int32_t get_itTafNyeQoucrbhPkPsSqRewAEI_10() const { return ___itTafNyeQoucrbhPkPsSqRewAEI_10; }
	inline int32_t* get_address_of_itTafNyeQoucrbhPkPsSqRewAEI_10() { return &___itTafNyeQoucrbhPkPsSqRewAEI_10; }
	inline void set_itTafNyeQoucrbhPkPsSqRewAEI_10(int32_t value)
	{
		___itTafNyeQoucrbhPkPsSqRewAEI_10 = value;
	}

	inline static int32_t get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_11() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___tOaTpOwzcPDCHdCDIyYaeuASgAM_11)); }
	inline int32_t get_tOaTpOwzcPDCHdCDIyYaeuASgAM_11() const { return ___tOaTpOwzcPDCHdCDIyYaeuASgAM_11; }
	inline int32_t* get_address_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_11() { return &___tOaTpOwzcPDCHdCDIyYaeuASgAM_11; }
	inline void set_tOaTpOwzcPDCHdCDIyYaeuASgAM_11(int32_t value)
	{
		___tOaTpOwzcPDCHdCDIyYaeuASgAM_11 = value;
	}

	inline static int32_t get_offset_of_iKsRvufntavyFjWZbUibgspoftw_12() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___iKsRvufntavyFjWZbUibgspoftw_12)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_iKsRvufntavyFjWZbUibgspoftw_12() const { return ___iKsRvufntavyFjWZbUibgspoftw_12; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_iKsRvufntavyFjWZbUibgspoftw_12() { return &___iKsRvufntavyFjWZbUibgspoftw_12; }
	inline void set_iKsRvufntavyFjWZbUibgspoftw_12(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___iKsRvufntavyFjWZbUibgspoftw_12 = value;
		Il2CppCodeGenWriteBarrier((&___iKsRvufntavyFjWZbUibgspoftw_12), value);
	}

	inline static int32_t get_offset_of_YuSoQESZTtPuhZKVJFBavCSGQsh_13() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___YuSoQESZTtPuhZKVJFBavCSGQsh_13)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_YuSoQESZTtPuhZKVJFBavCSGQsh_13() const { return ___YuSoQESZTtPuhZKVJFBavCSGQsh_13; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_YuSoQESZTtPuhZKVJFBavCSGQsh_13() { return &___YuSoQESZTtPuhZKVJFBavCSGQsh_13; }
	inline void set_YuSoQESZTtPuhZKVJFBavCSGQsh_13(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___YuSoQESZTtPuhZKVJFBavCSGQsh_13 = value;
		Il2CppCodeGenWriteBarrier((&___YuSoQESZTtPuhZKVJFBavCSGQsh_13), value);
	}

	inline static int32_t get_offset_of_motlnrXwRclwbbdquGWkOEYsYFy_14() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___motlnrXwRclwbbdquGWkOEYsYFy_14)); }
	inline HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 * get_motlnrXwRclwbbdquGWkOEYsYFy_14() const { return ___motlnrXwRclwbbdquGWkOEYsYFy_14; }
	inline HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 ** get_address_of_motlnrXwRclwbbdquGWkOEYsYFy_14() { return &___motlnrXwRclwbbdquGWkOEYsYFy_14; }
	inline void set_motlnrXwRclwbbdquGWkOEYsYFy_14(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 * value)
	{
		___motlnrXwRclwbbdquGWkOEYsYFy_14 = value;
		Il2CppCodeGenWriteBarrier((&___motlnrXwRclwbbdquGWkOEYsYFy_14), value);
	}

	inline static int32_t get_offset_of_DLgnRYsKBieSednqoFxsgeQMfJE_15() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___DLgnRYsKBieSednqoFxsgeQMfJE_15)); }
	inline Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09 * get_DLgnRYsKBieSednqoFxsgeQMfJE_15() const { return ___DLgnRYsKBieSednqoFxsgeQMfJE_15; }
	inline Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09 ** get_address_of_DLgnRYsKBieSednqoFxsgeQMfJE_15() { return &___DLgnRYsKBieSednqoFxsgeQMfJE_15; }
	inline void set_DLgnRYsKBieSednqoFxsgeQMfJE_15(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09 * value)
	{
		___DLgnRYsKBieSednqoFxsgeQMfJE_15 = value;
		Il2CppCodeGenWriteBarrier((&___DLgnRYsKBieSednqoFxsgeQMfJE_15), value);
	}

	inline static int32_t get_offset_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_16() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___uTrFqghzwmUUNLMxoQSRVgfDfsj_16)); }
	inline bool get_uTrFqghzwmUUNLMxoQSRVgfDfsj_16() const { return ___uTrFqghzwmUUNLMxoQSRVgfDfsj_16; }
	inline bool* get_address_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_16() { return &___uTrFqghzwmUUNLMxoQSRVgfDfsj_16; }
	inline void set_uTrFqghzwmUUNLMxoQSRVgfDfsj_16(bool value)
	{
		___uTrFqghzwmUUNLMxoQSRVgfDfsj_16 = value;
	}

	inline static int32_t get_offset_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_17() { return static_cast<int32_t>(offsetof(OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49, ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_17)); }
	inline Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * get_QEAEcDsIVKSxlRksWqQtzkbPdqrh_17() const { return ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_17; }
	inline Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF ** get_address_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_17() { return &___QEAEcDsIVKSxlRksWqQtzkbPdqrh_17; }
	inline void set_QEAEcDsIVKSxlRksWqQtzkbPdqrh_17(Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * value)
	{
		___QEAEcDsIVKSxlRksWqQtzkbPdqrh_17 = value;
		Il2CppCodeGenWriteBarrier((&___QEAEcDsIVKSxlRksWqQtzkbPdqrh_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSBOCXHMGYBBOUDBLKGMAEXKFJB_TE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49_H
#ifndef SOMWFHWDFFMANNPQUSGWLSUWHDI_TA293FB49E75154F1D97915E7039597E1022636F6_H
#define SOMWFHWDFFMANNPQUSGWLSUWHDI_TA293FB49E75154F1D97915E7039597E1022636F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoMwFHWdfFmaNnPQuSGWlSuWHDi
struct  SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6  : public RuntimeObject
{
public:
	// SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl[] SoMwFHWdfFmaNnPQuSGWlSuWHDi::HWmOewXpuebOsZDStBejWuBUGqK
	mtCbAGdYHzRqiPnOmbyDgpWTYHdlU5BU5D_tB58DCAF360AEA76C17C02C816B58FDE76058A165* ___HWmOewXpuebOsZDStBejWuBUGqK_0;
	// Rewired.UpdateLoopType SoMwFHWdfFmaNnPQuSGWlSuWHDi::YIgjjtvsNpmLAzEahZSayMqwKAO
	int32_t ___YIgjjtvsNpmLAzEahZSayMqwKAO_1;
	// Rewired.Keyboard SoMwFHWdfFmaNnPQuSGWlSuWHDi::OSuLPPJEiNhXcAilTdfZMoZXfznt
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * ___OSuLPPJEiNhXcAilTdfZMoZXfznt_2;
	// SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl SoMwFHWdfFmaNnPQuSGWlSuWHDi::YaDAnfimULzphKnSejtwEcfSlkEJ
	mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F * ___YaDAnfimULzphKnSejtwEcfSlkEJ_3;

public:
	inline static int32_t get_offset_of_HWmOewXpuebOsZDStBejWuBUGqK_0() { return static_cast<int32_t>(offsetof(SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6, ___HWmOewXpuebOsZDStBejWuBUGqK_0)); }
	inline mtCbAGdYHzRqiPnOmbyDgpWTYHdlU5BU5D_tB58DCAF360AEA76C17C02C816B58FDE76058A165* get_HWmOewXpuebOsZDStBejWuBUGqK_0() const { return ___HWmOewXpuebOsZDStBejWuBUGqK_0; }
	inline mtCbAGdYHzRqiPnOmbyDgpWTYHdlU5BU5D_tB58DCAF360AEA76C17C02C816B58FDE76058A165** get_address_of_HWmOewXpuebOsZDStBejWuBUGqK_0() { return &___HWmOewXpuebOsZDStBejWuBUGqK_0; }
	inline void set_HWmOewXpuebOsZDStBejWuBUGqK_0(mtCbAGdYHzRqiPnOmbyDgpWTYHdlU5BU5D_tB58DCAF360AEA76C17C02C816B58FDE76058A165* value)
	{
		___HWmOewXpuebOsZDStBejWuBUGqK_0 = value;
		Il2CppCodeGenWriteBarrier((&___HWmOewXpuebOsZDStBejWuBUGqK_0), value);
	}

	inline static int32_t get_offset_of_YIgjjtvsNpmLAzEahZSayMqwKAO_1() { return static_cast<int32_t>(offsetof(SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6, ___YIgjjtvsNpmLAzEahZSayMqwKAO_1)); }
	inline int32_t get_YIgjjtvsNpmLAzEahZSayMqwKAO_1() const { return ___YIgjjtvsNpmLAzEahZSayMqwKAO_1; }
	inline int32_t* get_address_of_YIgjjtvsNpmLAzEahZSayMqwKAO_1() { return &___YIgjjtvsNpmLAzEahZSayMqwKAO_1; }
	inline void set_YIgjjtvsNpmLAzEahZSayMqwKAO_1(int32_t value)
	{
		___YIgjjtvsNpmLAzEahZSayMqwKAO_1 = value;
	}

	inline static int32_t get_offset_of_OSuLPPJEiNhXcAilTdfZMoZXfznt_2() { return static_cast<int32_t>(offsetof(SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6, ___OSuLPPJEiNhXcAilTdfZMoZXfznt_2)); }
	inline Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * get_OSuLPPJEiNhXcAilTdfZMoZXfznt_2() const { return ___OSuLPPJEiNhXcAilTdfZMoZXfznt_2; }
	inline Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 ** get_address_of_OSuLPPJEiNhXcAilTdfZMoZXfznt_2() { return &___OSuLPPJEiNhXcAilTdfZMoZXfznt_2; }
	inline void set_OSuLPPJEiNhXcAilTdfZMoZXfznt_2(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * value)
	{
		___OSuLPPJEiNhXcAilTdfZMoZXfznt_2 = value;
		Il2CppCodeGenWriteBarrier((&___OSuLPPJEiNhXcAilTdfZMoZXfznt_2), value);
	}

	inline static int32_t get_offset_of_YaDAnfimULzphKnSejtwEcfSlkEJ_3() { return static_cast<int32_t>(offsetof(SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6, ___YaDAnfimULzphKnSejtwEcfSlkEJ_3)); }
	inline mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F * get_YaDAnfimULzphKnSejtwEcfSlkEJ_3() const { return ___YaDAnfimULzphKnSejtwEcfSlkEJ_3; }
	inline mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F ** get_address_of_YaDAnfimULzphKnSejtwEcfSlkEJ_3() { return &___YaDAnfimULzphKnSejtwEcfSlkEJ_3; }
	inline void set_YaDAnfimULzphKnSejtwEcfSlkEJ_3(mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F * value)
	{
		___YaDAnfimULzphKnSejtwEcfSlkEJ_3 = value;
		Il2CppCodeGenWriteBarrier((&___YaDAnfimULzphKnSejtwEcfSlkEJ_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOMWFHWDFFMANNPQUSGWLSUWHDI_TA293FB49E75154F1D97915E7039597E1022636F6_H
#ifndef MTCBAGDYHZRQIPNOMBYDGPWTYHDL_T974A60D44D98143ACAA72A5ADBE24822BE21B10F_H
#define MTCBAGDYHZRQIPNOMBYDGPWTYHDL_T974A60D44D98143ACAA72A5ADBE24822BE21B10F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl
struct  mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F  : public RuntimeObject
{
public:
	// Rewired.ModifierKeyFlags SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl::OZOUoJMKKtpUIlClzaTXojhyQdm
	int32_t ___OZOUoJMKKtpUIlClzaTXojhyQdm_0;
	// Rewired.Utils.Classes.Data.ExpandableArray_DataContainer`1<SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl_MVPpNOKkKsBHVbOLknceWHXOeUB> SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl::FqkHlxUUCNWHSlCnAhrViyWnBHqt
	ExpandableArray_DataContainer_1_t07F3C761135F4A055596287551ABF7675C077873 * ___FqkHlxUUCNWHSlCnAhrViyWnBHqt_1;
	// Rewired.Utils.Classes.Data.ExpandableArray_DataContainer`1<SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl_MVPpNOKkKsBHVbOLknceWHXOeUB> SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl::WvgzPICLZvrDlmflhcFctvIRNFi
	ExpandableArray_DataContainer_1_t07F3C761135F4A055596287551ABF7675C077873 * ___WvgzPICLZvrDlmflhcFctvIRNFi_2;
	// Rewired.Keyboard SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl::OSuLPPJEiNhXcAilTdfZMoZXfznt
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * ___OSuLPPJEiNhXcAilTdfZMoZXfznt_3;

public:
	inline static int32_t get_offset_of_OZOUoJMKKtpUIlClzaTXojhyQdm_0() { return static_cast<int32_t>(offsetof(mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F, ___OZOUoJMKKtpUIlClzaTXojhyQdm_0)); }
	inline int32_t get_OZOUoJMKKtpUIlClzaTXojhyQdm_0() const { return ___OZOUoJMKKtpUIlClzaTXojhyQdm_0; }
	inline int32_t* get_address_of_OZOUoJMKKtpUIlClzaTXojhyQdm_0() { return &___OZOUoJMKKtpUIlClzaTXojhyQdm_0; }
	inline void set_OZOUoJMKKtpUIlClzaTXojhyQdm_0(int32_t value)
	{
		___OZOUoJMKKtpUIlClzaTXojhyQdm_0 = value;
	}

	inline static int32_t get_offset_of_FqkHlxUUCNWHSlCnAhrViyWnBHqt_1() { return static_cast<int32_t>(offsetof(mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F, ___FqkHlxUUCNWHSlCnAhrViyWnBHqt_1)); }
	inline ExpandableArray_DataContainer_1_t07F3C761135F4A055596287551ABF7675C077873 * get_FqkHlxUUCNWHSlCnAhrViyWnBHqt_1() const { return ___FqkHlxUUCNWHSlCnAhrViyWnBHqt_1; }
	inline ExpandableArray_DataContainer_1_t07F3C761135F4A055596287551ABF7675C077873 ** get_address_of_FqkHlxUUCNWHSlCnAhrViyWnBHqt_1() { return &___FqkHlxUUCNWHSlCnAhrViyWnBHqt_1; }
	inline void set_FqkHlxUUCNWHSlCnAhrViyWnBHqt_1(ExpandableArray_DataContainer_1_t07F3C761135F4A055596287551ABF7675C077873 * value)
	{
		___FqkHlxUUCNWHSlCnAhrViyWnBHqt_1 = value;
		Il2CppCodeGenWriteBarrier((&___FqkHlxUUCNWHSlCnAhrViyWnBHqt_1), value);
	}

	inline static int32_t get_offset_of_WvgzPICLZvrDlmflhcFctvIRNFi_2() { return static_cast<int32_t>(offsetof(mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F, ___WvgzPICLZvrDlmflhcFctvIRNFi_2)); }
	inline ExpandableArray_DataContainer_1_t07F3C761135F4A055596287551ABF7675C077873 * get_WvgzPICLZvrDlmflhcFctvIRNFi_2() const { return ___WvgzPICLZvrDlmflhcFctvIRNFi_2; }
	inline ExpandableArray_DataContainer_1_t07F3C761135F4A055596287551ABF7675C077873 ** get_address_of_WvgzPICLZvrDlmflhcFctvIRNFi_2() { return &___WvgzPICLZvrDlmflhcFctvIRNFi_2; }
	inline void set_WvgzPICLZvrDlmflhcFctvIRNFi_2(ExpandableArray_DataContainer_1_t07F3C761135F4A055596287551ABF7675C077873 * value)
	{
		___WvgzPICLZvrDlmflhcFctvIRNFi_2 = value;
		Il2CppCodeGenWriteBarrier((&___WvgzPICLZvrDlmflhcFctvIRNFi_2), value);
	}

	inline static int32_t get_offset_of_OSuLPPJEiNhXcAilTdfZMoZXfznt_3() { return static_cast<int32_t>(offsetof(mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F, ___OSuLPPJEiNhXcAilTdfZMoZXfznt_3)); }
	inline Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * get_OSuLPPJEiNhXcAilTdfZMoZXfznt_3() const { return ___OSuLPPJEiNhXcAilTdfZMoZXfznt_3; }
	inline Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 ** get_address_of_OSuLPPJEiNhXcAilTdfZMoZXfznt_3() { return &___OSuLPPJEiNhXcAilTdfZMoZXfznt_3; }
	inline void set_OSuLPPJEiNhXcAilTdfZMoZXfznt_3(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * value)
	{
		___OSuLPPJEiNhXcAilTdfZMoZXfznt_3 = value;
		Il2CppCodeGenWriteBarrier((&___OSuLPPJEiNhXcAilTdfZMoZXfznt_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MTCBAGDYHZRQIPNOMBYDGPWTYHDL_T974A60D44D98143ACAA72A5ADBE24822BE21B10F_H
#ifndef MVPPNOKKKSBHVBOLKNCEWHXOEUB_TE0AD501DCDBE57A64499C912F0DA0DF46FA22C37_H
#define MVPPNOKKKSBHVBOLKNCEWHXOEUB_TE0AD501DCDBE57A64499C912F0DA0DF46FA22C37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl_MVPpNOKkKsBHVbOLknceWHXOeUB
struct  MVPpNOKkKsBHVbOLknceWHXOeUB_tE0AD501DCDBE57A64499C912F0DA0DF46FA22C37  : public RuntimeObject
{
public:
	// Rewired.KeyboardKeyCode SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl_MVPpNOKkKsBHVbOLknceWHXOeUB::yIoGRcgIykirtuDoiviLGImmgkq
	int32_t ___yIoGRcgIykirtuDoiviLGImmgkq_0;
	// Rewired.ModifierKeyFlags SoMwFHWdfFmaNnPQuSGWlSuWHDi_mtCbAGdYHzRqiPnOmbyDgpWTYHdl_MVPpNOKkKsBHVbOLknceWHXOeUB::jMKLVesCgaLYbTmUqCleosFlcphh
	int32_t ___jMKLVesCgaLYbTmUqCleosFlcphh_1;

public:
	inline static int32_t get_offset_of_yIoGRcgIykirtuDoiviLGImmgkq_0() { return static_cast<int32_t>(offsetof(MVPpNOKkKsBHVbOLknceWHXOeUB_tE0AD501DCDBE57A64499C912F0DA0DF46FA22C37, ___yIoGRcgIykirtuDoiviLGImmgkq_0)); }
	inline int32_t get_yIoGRcgIykirtuDoiviLGImmgkq_0() const { return ___yIoGRcgIykirtuDoiviLGImmgkq_0; }
	inline int32_t* get_address_of_yIoGRcgIykirtuDoiviLGImmgkq_0() { return &___yIoGRcgIykirtuDoiviLGImmgkq_0; }
	inline void set_yIoGRcgIykirtuDoiviLGImmgkq_0(int32_t value)
	{
		___yIoGRcgIykirtuDoiviLGImmgkq_0 = value;
	}

	inline static int32_t get_offset_of_jMKLVesCgaLYbTmUqCleosFlcphh_1() { return static_cast<int32_t>(offsetof(MVPpNOKkKsBHVbOLknceWHXOeUB_tE0AD501DCDBE57A64499C912F0DA0DF46FA22C37, ___jMKLVesCgaLYbTmUqCleosFlcphh_1)); }
	inline int32_t get_jMKLVesCgaLYbTmUqCleosFlcphh_1() const { return ___jMKLVesCgaLYbTmUqCleosFlcphh_1; }
	inline int32_t* get_address_of_jMKLVesCgaLYbTmUqCleosFlcphh_1() { return &___jMKLVesCgaLYbTmUqCleosFlcphh_1; }
	inline void set_jMKLVesCgaLYbTmUqCleosFlcphh_1(int32_t value)
	{
		___jMKLVesCgaLYbTmUqCleosFlcphh_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MVPPNOKKKSBHVBOLKNCEWHXOEUB_TE0AD501DCDBE57A64499C912F0DA0DF46FA22C37_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef DEXVKUCQURZFRSBNARGAFCEPSBP_T9FFAFDF5D8D521ECE2C69F4BE1B908875D25112E_H
#define DEXVKUCQURZFRSBNARGAFCEPSBP_T9FFAFDF5D8D521ECE2C69F4BE1B908875D25112E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iDabdpqyvlEAXmjzfkEiOCBPdMuc_deXVkUCQurzFRSBNargAFcePsbp
struct  deXVkUCQurzFRSBNargAFcePsbp_t9FFAFDF5D8D521ECE2C69F4BE1B908875D25112E  : public RuntimeObject
{
public:
	// iDabdpqyvlEAXmjzfkEiOCBPdMuc_deXVkUCQurzFRSBNargAFcePsbp_yAEDWkzBznxclWFLPxuGoyUBKeT iDabdpqyvlEAXmjzfkEiOCBPdMuc_deXVkUCQurzFRSBNargAFcePsbp::mvKHSWNtYxtrXAPVpClHLUktCzV
	uint8_t ___mvKHSWNtYxtrXAPVpClHLUktCzV_0;
	// System.UInt32 iDabdpqyvlEAXmjzfkEiOCBPdMuc_deXVkUCQurzFRSBNargAFcePsbp::GwxvhxNuDvVBGaKjmuSESUcbudE
	uint32_t ___GwxvhxNuDvVBGaKjmuSESUcbudE_1;
	// System.Boolean iDabdpqyvlEAXmjzfkEiOCBPdMuc_deXVkUCQurzFRSBNargAFcePsbp::JjCmPMTQfjvEUCHkNBeRcjNBieD
	bool ___JjCmPMTQfjvEUCHkNBeRcjNBieD_2;

public:
	inline static int32_t get_offset_of_mvKHSWNtYxtrXAPVpClHLUktCzV_0() { return static_cast<int32_t>(offsetof(deXVkUCQurzFRSBNargAFcePsbp_t9FFAFDF5D8D521ECE2C69F4BE1B908875D25112E, ___mvKHSWNtYxtrXAPVpClHLUktCzV_0)); }
	inline uint8_t get_mvKHSWNtYxtrXAPVpClHLUktCzV_0() const { return ___mvKHSWNtYxtrXAPVpClHLUktCzV_0; }
	inline uint8_t* get_address_of_mvKHSWNtYxtrXAPVpClHLUktCzV_0() { return &___mvKHSWNtYxtrXAPVpClHLUktCzV_0; }
	inline void set_mvKHSWNtYxtrXAPVpClHLUktCzV_0(uint8_t value)
	{
		___mvKHSWNtYxtrXAPVpClHLUktCzV_0 = value;
	}

	inline static int32_t get_offset_of_GwxvhxNuDvVBGaKjmuSESUcbudE_1() { return static_cast<int32_t>(offsetof(deXVkUCQurzFRSBNargAFcePsbp_t9FFAFDF5D8D521ECE2C69F4BE1B908875D25112E, ___GwxvhxNuDvVBGaKjmuSESUcbudE_1)); }
	inline uint32_t get_GwxvhxNuDvVBGaKjmuSESUcbudE_1() const { return ___GwxvhxNuDvVBGaKjmuSESUcbudE_1; }
	inline uint32_t* get_address_of_GwxvhxNuDvVBGaKjmuSESUcbudE_1() { return &___GwxvhxNuDvVBGaKjmuSESUcbudE_1; }
	inline void set_GwxvhxNuDvVBGaKjmuSESUcbudE_1(uint32_t value)
	{
		___GwxvhxNuDvVBGaKjmuSESUcbudE_1 = value;
	}

	inline static int32_t get_offset_of_JjCmPMTQfjvEUCHkNBeRcjNBieD_2() { return static_cast<int32_t>(offsetof(deXVkUCQurzFRSBNargAFcePsbp_t9FFAFDF5D8D521ECE2C69F4BE1B908875D25112E, ___JjCmPMTQfjvEUCHkNBeRcjNBieD_2)); }
	inline bool get_JjCmPMTQfjvEUCHkNBeRcjNBieD_2() const { return ___JjCmPMTQfjvEUCHkNBeRcjNBieD_2; }
	inline bool* get_address_of_JjCmPMTQfjvEUCHkNBeRcjNBieD_2() { return &___JjCmPMTQfjvEUCHkNBeRcjNBieD_2; }
	inline void set_JjCmPMTQfjvEUCHkNBeRcjNBieD_2(bool value)
	{
		___JjCmPMTQfjvEUCHkNBeRcjNBieD_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEXVKUCQURZFRSBNARGAFCEPSBP_T9FFAFDF5D8D521ECE2C69F4BE1B908875D25112E_H
#ifndef CONTROLLERTEMPLATEELEMENTIDENTIFIER_EDITOR_T11EE1851F65BDA3CD3765D6129DBB482EE7ABC27_H
#define CONTROLLERTEMPLATEELEMENTIDENTIFIER_EDITOR_T11EE1851F65BDA3CD3765D6129DBB482EE7ABC27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.ControllerTemplateElementIdentifier_Editor
struct  ControllerTemplateElementIdentifier_Editor_t11EE1851F65BDA3CD3765D6129DBB482EE7ABC27  : public ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40
{
public:
	// System.String Rewired.Data.ControllerTemplateElementIdentifier_Editor::_scriptingName
	String_t* ____scriptingName_6;
	// System.String Rewired.Data.ControllerTemplateElementIdentifier_Editor::_alternateScriptingName
	String_t* ____alternateScriptingName_7;
	// System.Boolean Rewired.Data.ControllerTemplateElementIdentifier_Editor::_excludeFromExport
	bool ____excludeFromExport_8;
	// System.Boolean Rewired.Data.ControllerTemplateElementIdentifier_Editor::_useEditorElementTypeOverride
	bool ____useEditorElementTypeOverride_9;
	// Rewired.ControllerElementType Rewired.Data.ControllerTemplateElementIdentifier_Editor::_editorElementTypeOverride
	int32_t ____editorElementTypeOverride_10;

public:
	inline static int32_t get_offset_of__scriptingName_6() { return static_cast<int32_t>(offsetof(ControllerTemplateElementIdentifier_Editor_t11EE1851F65BDA3CD3765D6129DBB482EE7ABC27, ____scriptingName_6)); }
	inline String_t* get__scriptingName_6() const { return ____scriptingName_6; }
	inline String_t** get_address_of__scriptingName_6() { return &____scriptingName_6; }
	inline void set__scriptingName_6(String_t* value)
	{
		____scriptingName_6 = value;
		Il2CppCodeGenWriteBarrier((&____scriptingName_6), value);
	}

	inline static int32_t get_offset_of__alternateScriptingName_7() { return static_cast<int32_t>(offsetof(ControllerTemplateElementIdentifier_Editor_t11EE1851F65BDA3CD3765D6129DBB482EE7ABC27, ____alternateScriptingName_7)); }
	inline String_t* get__alternateScriptingName_7() const { return ____alternateScriptingName_7; }
	inline String_t** get_address_of__alternateScriptingName_7() { return &____alternateScriptingName_7; }
	inline void set__alternateScriptingName_7(String_t* value)
	{
		____alternateScriptingName_7 = value;
		Il2CppCodeGenWriteBarrier((&____alternateScriptingName_7), value);
	}

	inline static int32_t get_offset_of__excludeFromExport_8() { return static_cast<int32_t>(offsetof(ControllerTemplateElementIdentifier_Editor_t11EE1851F65BDA3CD3765D6129DBB482EE7ABC27, ____excludeFromExport_8)); }
	inline bool get__excludeFromExport_8() const { return ____excludeFromExport_8; }
	inline bool* get_address_of__excludeFromExport_8() { return &____excludeFromExport_8; }
	inline void set__excludeFromExport_8(bool value)
	{
		____excludeFromExport_8 = value;
	}

	inline static int32_t get_offset_of__useEditorElementTypeOverride_9() { return static_cast<int32_t>(offsetof(ControllerTemplateElementIdentifier_Editor_t11EE1851F65BDA3CD3765D6129DBB482EE7ABC27, ____useEditorElementTypeOverride_9)); }
	inline bool get__useEditorElementTypeOverride_9() const { return ____useEditorElementTypeOverride_9; }
	inline bool* get_address_of__useEditorElementTypeOverride_9() { return &____useEditorElementTypeOverride_9; }
	inline void set__useEditorElementTypeOverride_9(bool value)
	{
		____useEditorElementTypeOverride_9 = value;
	}

	inline static int32_t get_offset_of__editorElementTypeOverride_10() { return static_cast<int32_t>(offsetof(ControllerTemplateElementIdentifier_Editor_t11EE1851F65BDA3CD3765D6129DBB482EE7ABC27, ____editorElementTypeOverride_10)); }
	inline int32_t get__editorElementTypeOverride_10() const { return ____editorElementTypeOverride_10; }
	inline int32_t* get_address_of__editorElementTypeOverride_10() { return &____editorElementTypeOverride_10; }
	inline void set__editorElementTypeOverride_10(int32_t value)
	{
		____editorElementTypeOverride_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEELEMENTIDENTIFIER_EDITOR_T11EE1851F65BDA3CD3765D6129DBB482EE7ABC27_H
#ifndef DUALSHOCK4DRIVER_T398D14889F3FD6AA980F4FA18DB5CE39BE628D49_H
#define DUALSHOCK4DRIVER_T398D14889F3FD6AA980F4FA18DB5CE39BE628D49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.Drivers.DualShock4Driver
struct  DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49  : public HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC
{
public:
	// System.Boolean Rewired.HID.Drivers.DualShock4Driver::vbAdkxyBQGZnWgwStHAftFUhnMl
	bool ___vbAdkxyBQGZnWgwStHAftFUhnMl_69;
	// System.Int32 Rewired.HID.Drivers.DualShock4Driver::cvbkCLSefIoPdaRkJzWOVwVdDOF
	int32_t ___cvbkCLSefIoPdaRkJzWOVwVdDOF_70;
	// System.Int32 Rewired.HID.Drivers.DualShock4Driver::mjLbjWGxNTiuJiCJZwmkHKFcaCLf
	int32_t ___mjLbjWGxNTiuJiCJZwmkHKFcaCLf_71;
	// System.Boolean Rewired.HID.Drivers.DualShock4Driver::cRTHNMIZPcYTXDzMxbEdznBfJju
	bool ___cRTHNMIZPcYTXDzMxbEdznBfJju_72;
	// System.Byte Rewired.HID.Drivers.DualShock4Driver::vpDaVaPWWJvlTXUvCLmTamgdTZu
	uint8_t ___vpDaVaPWWJvlTXUvCLmTamgdTZu_73;
	// System.Int32 Rewired.HID.Drivers.DualShock4Driver::pIzBOGVpMGmZYfvyQCflfLljPsu
	int32_t ___pIzBOGVpMGmZYfvyQCflfLljPsu_74;
	// System.Int32 Rewired.HID.Drivers.DualShock4Driver::ZkXtNTyqIqlwndwCWoNxaqbTNZO
	int32_t ___ZkXtNTyqIqlwndwCWoNxaqbTNZO_75;
	// System.Int32 Rewired.HID.Drivers.DualShock4Driver::xKcEWzaZZMnwclywIKtCnglaySh
	int32_t ___xKcEWzaZZMnwclywIKtCnglaySh_76;
	// System.Int32 Rewired.HID.Drivers.DualShock4Driver::xzrQffrYIPOPiwKYJcUAWeFIxnL
	int32_t ___xzrQffrYIPOPiwKYJcUAWeFIxnL_77;
	// System.Int32 Rewired.HID.Drivers.DualShock4Driver::cjHetCnPAKaCZYGymgXYKYCcheu
	int32_t ___cjHetCnPAKaCZYGymgXYKYCcheu_78;
	// System.Int32 Rewired.HID.Drivers.DualShock4Driver::GFSUWBglLSCeWBmOcfOpbJiRMBMd
	int32_t ___GFSUWBglLSCeWBmOcfOpbJiRMBMd_79;
	// Rewired.Utils.Classes.Data.NativeBuffer Rewired.HID.Drivers.DualShock4Driver::JVsgwlgmwsLVfxuQuuLMmdvVseuU
	NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * ___JVsgwlgmwsLVfxuQuuLMmdvVseuU_80;
	// Rewired.Utils.Classes.Data.NativeBuffer Rewired.HID.Drivers.DualShock4Driver::WgXlHLobIXEGVFDvcmIWrNRWIsj
	NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * ___WgXlHLobIXEGVFDvcmIWrNRWIsj_81;
	// Rewired.HID.OutputReport Rewired.HID.Drivers.DualShock4Driver::ZKOHuQntYJaUOOJHSLzgGWFgqQF
	OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5  ___ZKOHuQntYJaUOOJHSLzgGWFgqQF_82;
	// System.Func`2<Rewired.HID.OutputReport,System.Boolean> Rewired.HID.Drivers.DualShock4Driver::jjxLdqHvqzvguEjvbDELBdomYEaU
	Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8 * ___jjxLdqHvqzvguEjvbDELBdomYEaU_83;
	// System.Action`1<Rewired.HID.OutputReport> Rewired.HID.Drivers.DualShock4Driver::KrnGaZawVhHQVCvIixlFJZuYWiA
	Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832 * ___KrnGaZawVhHQVCvIixlFJZuYWiA_84;
	// System.Boolean Rewired.HID.Drivers.DualShock4Driver::EjvDLAplqfwQVpPeGHHWCocbDKpT
	bool ___EjvDLAplqfwQVpPeGHHWCocbDKpT_85;
	// System.Boolean Rewired.HID.Drivers.DualShock4Driver::WUDRxWarrxHANXPgdPRtgWlPwYH
	bool ___WUDRxWarrxHANXPgdPRtgWlPwYH_86;
	// System.Single Rewired.HID.Drivers.DualShock4Driver::jpRbbilkJKfhmFMAmPONKvFcRvaO
	float ___jpRbbilkJKfhmFMAmPONKvFcRvaO_87;
	// System.Byte Rewired.HID.Drivers.DualShock4Driver::HqmcZGILGJJHIugaGAfpYXwIZel
	uint8_t ___HqmcZGILGJJHIugaGAfpYXwIZel_88;
	// UnityEngine.Quaternion Rewired.HID.Drivers.DualShock4Driver::cbXHqdaCQrWlIqinLBwNKaSAoVdd
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___cbXHqdaCQrWlIqinLBwNKaSAoVdd_89;
	// System.UInt16 Rewired.HID.Drivers.DualShock4Driver::HJPwxWlXiwCYDinWNJehoORKeaAG
	uint16_t ___HJPwxWlXiwCYDinWNJehoORKeaAG_90;
	// System.Single Rewired.HID.Drivers.DualShock4Driver::osiGJAFTSlCIOMcRcChRIwdmebHj
	float ___osiGJAFTSlCIOMcRcChRIwdmebHj_91;
	// System.Single Rewired.HID.Drivers.DualShock4Driver::crwevXVJojEkpHUxVTSVSqjIbrKg
	float ___crwevXVJojEkpHUxVTSVSqjIbrKg_92;
	// System.Single Rewired.HID.Drivers.DualShock4Driver::TPAStIxrbeqxjYXsGDsZlhWCYmG
	float ___TPAStIxrbeqxjYXsGDsZlhWCYmG_93;
	// System.Byte Rewired.HID.Drivers.DualShock4Driver::vrqVuHCBCDfnbTkILCuGeYDmBXRZ
	uint8_t ___vrqVuHCBCDfnbTkILCuGeYDmBXRZ_94;
	// System.Byte Rewired.HID.Drivers.DualShock4Driver::GqXNxQsKQjHVuDfkVIXTcQvsrXTS
	uint8_t ___GqXNxQsKQjHVuDfkVIXTcQvsrXTS_95;
	// UnityEngine.Quaternion Rewired.HID.Drivers.DualShock4Driver::guWMOiNMigqDftPgOHjtkmbvIMah
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___guWMOiNMigqDftPgOHjtkmbvIMah_96;
	// System.Boolean Rewired.HID.Drivers.DualShock4Driver::lWdOljnpmaKTjiGpJhpWZDwuBbgA
	bool ___lWdOljnpmaKTjiGpJhpWZDwuBbgA_97;
	// System.Int32 Rewired.HID.Drivers.DualShock4Driver::sVlrrYXpkKVzsJCRWwGTyeXhRkm
	int32_t ___sVlrrYXpkKVzsJCRWwGTyeXhRkm_98;
	// System.Int32[] Rewired.HID.Drivers.DualShock4Driver::mQFzRirpsuMkLakfKFaPDILeusGG
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mQFzRirpsuMkLakfKFaPDILeusGG_99;
	// System.Int32[] Rewired.HID.Drivers.DualShock4Driver::RgRMMUDigxVtMdfhjtgBOIBeaHq
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___RgRMMUDigxVtMdfhjtgBOIBeaHq_100;

public:
	inline static int32_t get_offset_of_vbAdkxyBQGZnWgwStHAftFUhnMl_69() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___vbAdkxyBQGZnWgwStHAftFUhnMl_69)); }
	inline bool get_vbAdkxyBQGZnWgwStHAftFUhnMl_69() const { return ___vbAdkxyBQGZnWgwStHAftFUhnMl_69; }
	inline bool* get_address_of_vbAdkxyBQGZnWgwStHAftFUhnMl_69() { return &___vbAdkxyBQGZnWgwStHAftFUhnMl_69; }
	inline void set_vbAdkxyBQGZnWgwStHAftFUhnMl_69(bool value)
	{
		___vbAdkxyBQGZnWgwStHAftFUhnMl_69 = value;
	}

	inline static int32_t get_offset_of_cvbkCLSefIoPdaRkJzWOVwVdDOF_70() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___cvbkCLSefIoPdaRkJzWOVwVdDOF_70)); }
	inline int32_t get_cvbkCLSefIoPdaRkJzWOVwVdDOF_70() const { return ___cvbkCLSefIoPdaRkJzWOVwVdDOF_70; }
	inline int32_t* get_address_of_cvbkCLSefIoPdaRkJzWOVwVdDOF_70() { return &___cvbkCLSefIoPdaRkJzWOVwVdDOF_70; }
	inline void set_cvbkCLSefIoPdaRkJzWOVwVdDOF_70(int32_t value)
	{
		___cvbkCLSefIoPdaRkJzWOVwVdDOF_70 = value;
	}

	inline static int32_t get_offset_of_mjLbjWGxNTiuJiCJZwmkHKFcaCLf_71() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___mjLbjWGxNTiuJiCJZwmkHKFcaCLf_71)); }
	inline int32_t get_mjLbjWGxNTiuJiCJZwmkHKFcaCLf_71() const { return ___mjLbjWGxNTiuJiCJZwmkHKFcaCLf_71; }
	inline int32_t* get_address_of_mjLbjWGxNTiuJiCJZwmkHKFcaCLf_71() { return &___mjLbjWGxNTiuJiCJZwmkHKFcaCLf_71; }
	inline void set_mjLbjWGxNTiuJiCJZwmkHKFcaCLf_71(int32_t value)
	{
		___mjLbjWGxNTiuJiCJZwmkHKFcaCLf_71 = value;
	}

	inline static int32_t get_offset_of_cRTHNMIZPcYTXDzMxbEdznBfJju_72() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___cRTHNMIZPcYTXDzMxbEdznBfJju_72)); }
	inline bool get_cRTHNMIZPcYTXDzMxbEdznBfJju_72() const { return ___cRTHNMIZPcYTXDzMxbEdznBfJju_72; }
	inline bool* get_address_of_cRTHNMIZPcYTXDzMxbEdznBfJju_72() { return &___cRTHNMIZPcYTXDzMxbEdznBfJju_72; }
	inline void set_cRTHNMIZPcYTXDzMxbEdznBfJju_72(bool value)
	{
		___cRTHNMIZPcYTXDzMxbEdznBfJju_72 = value;
	}

	inline static int32_t get_offset_of_vpDaVaPWWJvlTXUvCLmTamgdTZu_73() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___vpDaVaPWWJvlTXUvCLmTamgdTZu_73)); }
	inline uint8_t get_vpDaVaPWWJvlTXUvCLmTamgdTZu_73() const { return ___vpDaVaPWWJvlTXUvCLmTamgdTZu_73; }
	inline uint8_t* get_address_of_vpDaVaPWWJvlTXUvCLmTamgdTZu_73() { return &___vpDaVaPWWJvlTXUvCLmTamgdTZu_73; }
	inline void set_vpDaVaPWWJvlTXUvCLmTamgdTZu_73(uint8_t value)
	{
		___vpDaVaPWWJvlTXUvCLmTamgdTZu_73 = value;
	}

	inline static int32_t get_offset_of_pIzBOGVpMGmZYfvyQCflfLljPsu_74() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___pIzBOGVpMGmZYfvyQCflfLljPsu_74)); }
	inline int32_t get_pIzBOGVpMGmZYfvyQCflfLljPsu_74() const { return ___pIzBOGVpMGmZYfvyQCflfLljPsu_74; }
	inline int32_t* get_address_of_pIzBOGVpMGmZYfvyQCflfLljPsu_74() { return &___pIzBOGVpMGmZYfvyQCflfLljPsu_74; }
	inline void set_pIzBOGVpMGmZYfvyQCflfLljPsu_74(int32_t value)
	{
		___pIzBOGVpMGmZYfvyQCflfLljPsu_74 = value;
	}

	inline static int32_t get_offset_of_ZkXtNTyqIqlwndwCWoNxaqbTNZO_75() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___ZkXtNTyqIqlwndwCWoNxaqbTNZO_75)); }
	inline int32_t get_ZkXtNTyqIqlwndwCWoNxaqbTNZO_75() const { return ___ZkXtNTyqIqlwndwCWoNxaqbTNZO_75; }
	inline int32_t* get_address_of_ZkXtNTyqIqlwndwCWoNxaqbTNZO_75() { return &___ZkXtNTyqIqlwndwCWoNxaqbTNZO_75; }
	inline void set_ZkXtNTyqIqlwndwCWoNxaqbTNZO_75(int32_t value)
	{
		___ZkXtNTyqIqlwndwCWoNxaqbTNZO_75 = value;
	}

	inline static int32_t get_offset_of_xKcEWzaZZMnwclywIKtCnglaySh_76() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___xKcEWzaZZMnwclywIKtCnglaySh_76)); }
	inline int32_t get_xKcEWzaZZMnwclywIKtCnglaySh_76() const { return ___xKcEWzaZZMnwclywIKtCnglaySh_76; }
	inline int32_t* get_address_of_xKcEWzaZZMnwclywIKtCnglaySh_76() { return &___xKcEWzaZZMnwclywIKtCnglaySh_76; }
	inline void set_xKcEWzaZZMnwclywIKtCnglaySh_76(int32_t value)
	{
		___xKcEWzaZZMnwclywIKtCnglaySh_76 = value;
	}

	inline static int32_t get_offset_of_xzrQffrYIPOPiwKYJcUAWeFIxnL_77() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___xzrQffrYIPOPiwKYJcUAWeFIxnL_77)); }
	inline int32_t get_xzrQffrYIPOPiwKYJcUAWeFIxnL_77() const { return ___xzrQffrYIPOPiwKYJcUAWeFIxnL_77; }
	inline int32_t* get_address_of_xzrQffrYIPOPiwKYJcUAWeFIxnL_77() { return &___xzrQffrYIPOPiwKYJcUAWeFIxnL_77; }
	inline void set_xzrQffrYIPOPiwKYJcUAWeFIxnL_77(int32_t value)
	{
		___xzrQffrYIPOPiwKYJcUAWeFIxnL_77 = value;
	}

	inline static int32_t get_offset_of_cjHetCnPAKaCZYGymgXYKYCcheu_78() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___cjHetCnPAKaCZYGymgXYKYCcheu_78)); }
	inline int32_t get_cjHetCnPAKaCZYGymgXYKYCcheu_78() const { return ___cjHetCnPAKaCZYGymgXYKYCcheu_78; }
	inline int32_t* get_address_of_cjHetCnPAKaCZYGymgXYKYCcheu_78() { return &___cjHetCnPAKaCZYGymgXYKYCcheu_78; }
	inline void set_cjHetCnPAKaCZYGymgXYKYCcheu_78(int32_t value)
	{
		___cjHetCnPAKaCZYGymgXYKYCcheu_78 = value;
	}

	inline static int32_t get_offset_of_GFSUWBglLSCeWBmOcfOpbJiRMBMd_79() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___GFSUWBglLSCeWBmOcfOpbJiRMBMd_79)); }
	inline int32_t get_GFSUWBglLSCeWBmOcfOpbJiRMBMd_79() const { return ___GFSUWBglLSCeWBmOcfOpbJiRMBMd_79; }
	inline int32_t* get_address_of_GFSUWBglLSCeWBmOcfOpbJiRMBMd_79() { return &___GFSUWBglLSCeWBmOcfOpbJiRMBMd_79; }
	inline void set_GFSUWBglLSCeWBmOcfOpbJiRMBMd_79(int32_t value)
	{
		___GFSUWBglLSCeWBmOcfOpbJiRMBMd_79 = value;
	}

	inline static int32_t get_offset_of_JVsgwlgmwsLVfxuQuuLMmdvVseuU_80() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___JVsgwlgmwsLVfxuQuuLMmdvVseuU_80)); }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * get_JVsgwlgmwsLVfxuQuuLMmdvVseuU_80() const { return ___JVsgwlgmwsLVfxuQuuLMmdvVseuU_80; }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 ** get_address_of_JVsgwlgmwsLVfxuQuuLMmdvVseuU_80() { return &___JVsgwlgmwsLVfxuQuuLMmdvVseuU_80; }
	inline void set_JVsgwlgmwsLVfxuQuuLMmdvVseuU_80(NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * value)
	{
		___JVsgwlgmwsLVfxuQuuLMmdvVseuU_80 = value;
		Il2CppCodeGenWriteBarrier((&___JVsgwlgmwsLVfxuQuuLMmdvVseuU_80), value);
	}

	inline static int32_t get_offset_of_WgXlHLobIXEGVFDvcmIWrNRWIsj_81() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___WgXlHLobIXEGVFDvcmIWrNRWIsj_81)); }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * get_WgXlHLobIXEGVFDvcmIWrNRWIsj_81() const { return ___WgXlHLobIXEGVFDvcmIWrNRWIsj_81; }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 ** get_address_of_WgXlHLobIXEGVFDvcmIWrNRWIsj_81() { return &___WgXlHLobIXEGVFDvcmIWrNRWIsj_81; }
	inline void set_WgXlHLobIXEGVFDvcmIWrNRWIsj_81(NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * value)
	{
		___WgXlHLobIXEGVFDvcmIWrNRWIsj_81 = value;
		Il2CppCodeGenWriteBarrier((&___WgXlHLobIXEGVFDvcmIWrNRWIsj_81), value);
	}

	inline static int32_t get_offset_of_ZKOHuQntYJaUOOJHSLzgGWFgqQF_82() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___ZKOHuQntYJaUOOJHSLzgGWFgqQF_82)); }
	inline OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5  get_ZKOHuQntYJaUOOJHSLzgGWFgqQF_82() const { return ___ZKOHuQntYJaUOOJHSLzgGWFgqQF_82; }
	inline OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5 * get_address_of_ZKOHuQntYJaUOOJHSLzgGWFgqQF_82() { return &___ZKOHuQntYJaUOOJHSLzgGWFgqQF_82; }
	inline void set_ZKOHuQntYJaUOOJHSLzgGWFgqQF_82(OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5  value)
	{
		___ZKOHuQntYJaUOOJHSLzgGWFgqQF_82 = value;
	}

	inline static int32_t get_offset_of_jjxLdqHvqzvguEjvbDELBdomYEaU_83() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___jjxLdqHvqzvguEjvbDELBdomYEaU_83)); }
	inline Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8 * get_jjxLdqHvqzvguEjvbDELBdomYEaU_83() const { return ___jjxLdqHvqzvguEjvbDELBdomYEaU_83; }
	inline Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8 ** get_address_of_jjxLdqHvqzvguEjvbDELBdomYEaU_83() { return &___jjxLdqHvqzvguEjvbDELBdomYEaU_83; }
	inline void set_jjxLdqHvqzvguEjvbDELBdomYEaU_83(Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8 * value)
	{
		___jjxLdqHvqzvguEjvbDELBdomYEaU_83 = value;
		Il2CppCodeGenWriteBarrier((&___jjxLdqHvqzvguEjvbDELBdomYEaU_83), value);
	}

	inline static int32_t get_offset_of_KrnGaZawVhHQVCvIixlFJZuYWiA_84() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___KrnGaZawVhHQVCvIixlFJZuYWiA_84)); }
	inline Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832 * get_KrnGaZawVhHQVCvIixlFJZuYWiA_84() const { return ___KrnGaZawVhHQVCvIixlFJZuYWiA_84; }
	inline Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832 ** get_address_of_KrnGaZawVhHQVCvIixlFJZuYWiA_84() { return &___KrnGaZawVhHQVCvIixlFJZuYWiA_84; }
	inline void set_KrnGaZawVhHQVCvIixlFJZuYWiA_84(Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832 * value)
	{
		___KrnGaZawVhHQVCvIixlFJZuYWiA_84 = value;
		Il2CppCodeGenWriteBarrier((&___KrnGaZawVhHQVCvIixlFJZuYWiA_84), value);
	}

	inline static int32_t get_offset_of_EjvDLAplqfwQVpPeGHHWCocbDKpT_85() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___EjvDLAplqfwQVpPeGHHWCocbDKpT_85)); }
	inline bool get_EjvDLAplqfwQVpPeGHHWCocbDKpT_85() const { return ___EjvDLAplqfwQVpPeGHHWCocbDKpT_85; }
	inline bool* get_address_of_EjvDLAplqfwQVpPeGHHWCocbDKpT_85() { return &___EjvDLAplqfwQVpPeGHHWCocbDKpT_85; }
	inline void set_EjvDLAplqfwQVpPeGHHWCocbDKpT_85(bool value)
	{
		___EjvDLAplqfwQVpPeGHHWCocbDKpT_85 = value;
	}

	inline static int32_t get_offset_of_WUDRxWarrxHANXPgdPRtgWlPwYH_86() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___WUDRxWarrxHANXPgdPRtgWlPwYH_86)); }
	inline bool get_WUDRxWarrxHANXPgdPRtgWlPwYH_86() const { return ___WUDRxWarrxHANXPgdPRtgWlPwYH_86; }
	inline bool* get_address_of_WUDRxWarrxHANXPgdPRtgWlPwYH_86() { return &___WUDRxWarrxHANXPgdPRtgWlPwYH_86; }
	inline void set_WUDRxWarrxHANXPgdPRtgWlPwYH_86(bool value)
	{
		___WUDRxWarrxHANXPgdPRtgWlPwYH_86 = value;
	}

	inline static int32_t get_offset_of_jpRbbilkJKfhmFMAmPONKvFcRvaO_87() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___jpRbbilkJKfhmFMAmPONKvFcRvaO_87)); }
	inline float get_jpRbbilkJKfhmFMAmPONKvFcRvaO_87() const { return ___jpRbbilkJKfhmFMAmPONKvFcRvaO_87; }
	inline float* get_address_of_jpRbbilkJKfhmFMAmPONKvFcRvaO_87() { return &___jpRbbilkJKfhmFMAmPONKvFcRvaO_87; }
	inline void set_jpRbbilkJKfhmFMAmPONKvFcRvaO_87(float value)
	{
		___jpRbbilkJKfhmFMAmPONKvFcRvaO_87 = value;
	}

	inline static int32_t get_offset_of_HqmcZGILGJJHIugaGAfpYXwIZel_88() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___HqmcZGILGJJHIugaGAfpYXwIZel_88)); }
	inline uint8_t get_HqmcZGILGJJHIugaGAfpYXwIZel_88() const { return ___HqmcZGILGJJHIugaGAfpYXwIZel_88; }
	inline uint8_t* get_address_of_HqmcZGILGJJHIugaGAfpYXwIZel_88() { return &___HqmcZGILGJJHIugaGAfpYXwIZel_88; }
	inline void set_HqmcZGILGJJHIugaGAfpYXwIZel_88(uint8_t value)
	{
		___HqmcZGILGJJHIugaGAfpYXwIZel_88 = value;
	}

	inline static int32_t get_offset_of_cbXHqdaCQrWlIqinLBwNKaSAoVdd_89() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___cbXHqdaCQrWlIqinLBwNKaSAoVdd_89)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_cbXHqdaCQrWlIqinLBwNKaSAoVdd_89() const { return ___cbXHqdaCQrWlIqinLBwNKaSAoVdd_89; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_cbXHqdaCQrWlIqinLBwNKaSAoVdd_89() { return &___cbXHqdaCQrWlIqinLBwNKaSAoVdd_89; }
	inline void set_cbXHqdaCQrWlIqinLBwNKaSAoVdd_89(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___cbXHqdaCQrWlIqinLBwNKaSAoVdd_89 = value;
	}

	inline static int32_t get_offset_of_HJPwxWlXiwCYDinWNJehoORKeaAG_90() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___HJPwxWlXiwCYDinWNJehoORKeaAG_90)); }
	inline uint16_t get_HJPwxWlXiwCYDinWNJehoORKeaAG_90() const { return ___HJPwxWlXiwCYDinWNJehoORKeaAG_90; }
	inline uint16_t* get_address_of_HJPwxWlXiwCYDinWNJehoORKeaAG_90() { return &___HJPwxWlXiwCYDinWNJehoORKeaAG_90; }
	inline void set_HJPwxWlXiwCYDinWNJehoORKeaAG_90(uint16_t value)
	{
		___HJPwxWlXiwCYDinWNJehoORKeaAG_90 = value;
	}

	inline static int32_t get_offset_of_osiGJAFTSlCIOMcRcChRIwdmebHj_91() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___osiGJAFTSlCIOMcRcChRIwdmebHj_91)); }
	inline float get_osiGJAFTSlCIOMcRcChRIwdmebHj_91() const { return ___osiGJAFTSlCIOMcRcChRIwdmebHj_91; }
	inline float* get_address_of_osiGJAFTSlCIOMcRcChRIwdmebHj_91() { return &___osiGJAFTSlCIOMcRcChRIwdmebHj_91; }
	inline void set_osiGJAFTSlCIOMcRcChRIwdmebHj_91(float value)
	{
		___osiGJAFTSlCIOMcRcChRIwdmebHj_91 = value;
	}

	inline static int32_t get_offset_of_crwevXVJojEkpHUxVTSVSqjIbrKg_92() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___crwevXVJojEkpHUxVTSVSqjIbrKg_92)); }
	inline float get_crwevXVJojEkpHUxVTSVSqjIbrKg_92() const { return ___crwevXVJojEkpHUxVTSVSqjIbrKg_92; }
	inline float* get_address_of_crwevXVJojEkpHUxVTSVSqjIbrKg_92() { return &___crwevXVJojEkpHUxVTSVSqjIbrKg_92; }
	inline void set_crwevXVJojEkpHUxVTSVSqjIbrKg_92(float value)
	{
		___crwevXVJojEkpHUxVTSVSqjIbrKg_92 = value;
	}

	inline static int32_t get_offset_of_TPAStIxrbeqxjYXsGDsZlhWCYmG_93() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___TPAStIxrbeqxjYXsGDsZlhWCYmG_93)); }
	inline float get_TPAStIxrbeqxjYXsGDsZlhWCYmG_93() const { return ___TPAStIxrbeqxjYXsGDsZlhWCYmG_93; }
	inline float* get_address_of_TPAStIxrbeqxjYXsGDsZlhWCYmG_93() { return &___TPAStIxrbeqxjYXsGDsZlhWCYmG_93; }
	inline void set_TPAStIxrbeqxjYXsGDsZlhWCYmG_93(float value)
	{
		___TPAStIxrbeqxjYXsGDsZlhWCYmG_93 = value;
	}

	inline static int32_t get_offset_of_vrqVuHCBCDfnbTkILCuGeYDmBXRZ_94() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___vrqVuHCBCDfnbTkILCuGeYDmBXRZ_94)); }
	inline uint8_t get_vrqVuHCBCDfnbTkILCuGeYDmBXRZ_94() const { return ___vrqVuHCBCDfnbTkILCuGeYDmBXRZ_94; }
	inline uint8_t* get_address_of_vrqVuHCBCDfnbTkILCuGeYDmBXRZ_94() { return &___vrqVuHCBCDfnbTkILCuGeYDmBXRZ_94; }
	inline void set_vrqVuHCBCDfnbTkILCuGeYDmBXRZ_94(uint8_t value)
	{
		___vrqVuHCBCDfnbTkILCuGeYDmBXRZ_94 = value;
	}

	inline static int32_t get_offset_of_GqXNxQsKQjHVuDfkVIXTcQvsrXTS_95() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___GqXNxQsKQjHVuDfkVIXTcQvsrXTS_95)); }
	inline uint8_t get_GqXNxQsKQjHVuDfkVIXTcQvsrXTS_95() const { return ___GqXNxQsKQjHVuDfkVIXTcQvsrXTS_95; }
	inline uint8_t* get_address_of_GqXNxQsKQjHVuDfkVIXTcQvsrXTS_95() { return &___GqXNxQsKQjHVuDfkVIXTcQvsrXTS_95; }
	inline void set_GqXNxQsKQjHVuDfkVIXTcQvsrXTS_95(uint8_t value)
	{
		___GqXNxQsKQjHVuDfkVIXTcQvsrXTS_95 = value;
	}

	inline static int32_t get_offset_of_guWMOiNMigqDftPgOHjtkmbvIMah_96() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___guWMOiNMigqDftPgOHjtkmbvIMah_96)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_guWMOiNMigqDftPgOHjtkmbvIMah_96() const { return ___guWMOiNMigqDftPgOHjtkmbvIMah_96; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_guWMOiNMigqDftPgOHjtkmbvIMah_96() { return &___guWMOiNMigqDftPgOHjtkmbvIMah_96; }
	inline void set_guWMOiNMigqDftPgOHjtkmbvIMah_96(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___guWMOiNMigqDftPgOHjtkmbvIMah_96 = value;
	}

	inline static int32_t get_offset_of_lWdOljnpmaKTjiGpJhpWZDwuBbgA_97() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___lWdOljnpmaKTjiGpJhpWZDwuBbgA_97)); }
	inline bool get_lWdOljnpmaKTjiGpJhpWZDwuBbgA_97() const { return ___lWdOljnpmaKTjiGpJhpWZDwuBbgA_97; }
	inline bool* get_address_of_lWdOljnpmaKTjiGpJhpWZDwuBbgA_97() { return &___lWdOljnpmaKTjiGpJhpWZDwuBbgA_97; }
	inline void set_lWdOljnpmaKTjiGpJhpWZDwuBbgA_97(bool value)
	{
		___lWdOljnpmaKTjiGpJhpWZDwuBbgA_97 = value;
	}

	inline static int32_t get_offset_of_sVlrrYXpkKVzsJCRWwGTyeXhRkm_98() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___sVlrrYXpkKVzsJCRWwGTyeXhRkm_98)); }
	inline int32_t get_sVlrrYXpkKVzsJCRWwGTyeXhRkm_98() const { return ___sVlrrYXpkKVzsJCRWwGTyeXhRkm_98; }
	inline int32_t* get_address_of_sVlrrYXpkKVzsJCRWwGTyeXhRkm_98() { return &___sVlrrYXpkKVzsJCRWwGTyeXhRkm_98; }
	inline void set_sVlrrYXpkKVzsJCRWwGTyeXhRkm_98(int32_t value)
	{
		___sVlrrYXpkKVzsJCRWwGTyeXhRkm_98 = value;
	}

	inline static int32_t get_offset_of_mQFzRirpsuMkLakfKFaPDILeusGG_99() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___mQFzRirpsuMkLakfKFaPDILeusGG_99)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mQFzRirpsuMkLakfKFaPDILeusGG_99() const { return ___mQFzRirpsuMkLakfKFaPDILeusGG_99; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mQFzRirpsuMkLakfKFaPDILeusGG_99() { return &___mQFzRirpsuMkLakfKFaPDILeusGG_99; }
	inline void set_mQFzRirpsuMkLakfKFaPDILeusGG_99(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mQFzRirpsuMkLakfKFaPDILeusGG_99 = value;
		Il2CppCodeGenWriteBarrier((&___mQFzRirpsuMkLakfKFaPDILeusGG_99), value);
	}

	inline static int32_t get_offset_of_RgRMMUDigxVtMdfhjtgBOIBeaHq_100() { return static_cast<int32_t>(offsetof(DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49, ___RgRMMUDigxVtMdfhjtgBOIBeaHq_100)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_RgRMMUDigxVtMdfhjtgBOIBeaHq_100() const { return ___RgRMMUDigxVtMdfhjtgBOIBeaHq_100; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_RgRMMUDigxVtMdfhjtgBOIBeaHq_100() { return &___RgRMMUDigxVtMdfhjtgBOIBeaHq_100; }
	inline void set_RgRMMUDigxVtMdfhjtgBOIBeaHq_100(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___RgRMMUDigxVtMdfhjtgBOIBeaHq_100 = value;
		Il2CppCodeGenWriteBarrier((&___RgRMMUDigxVtMdfhjtgBOIBeaHq_100), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUALSHOCK4DRIVER_T398D14889F3FD6AA980F4FA18DB5CE39BE628D49_H
#ifndef RAILDRIVERDRIVER_TDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688_H
#define RAILDRIVERDRIVER_TDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.Drivers.RailDriverDriver
struct  RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688  : public HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC
{
public:
	// Rewired.Utils.Classes.Data.NativeBuffer Rewired.HID.Drivers.RailDriverDriver::JVsgwlgmwsLVfxuQuuLMmdvVseuU
	NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * ___JVsgwlgmwsLVfxuQuuLMmdvVseuU_34;
	// Rewired.Utils.Classes.Data.NativeBuffer Rewired.HID.Drivers.RailDriverDriver::WgXlHLobIXEGVFDvcmIWrNRWIsj
	NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * ___WgXlHLobIXEGVFDvcmIWrNRWIsj_35;
	// System.Boolean Rewired.HID.Drivers.RailDriverDriver::eqENXoqEArTCUWjquEQYFDTxSOvW
	bool ___eqENXoqEArTCUWjquEQYFDTxSOvW_36;
	// System.Byte[] Rewired.HID.Drivers.RailDriverDriver::FwNKSIMHOALacyXryDksxPdrVVl
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___FwNKSIMHOALacyXryDksxPdrVVl_37;
	// Rewired.HID.OutputReport Rewired.HID.Drivers.RailDriverDriver::ZKOHuQntYJaUOOJHSLzgGWFgqQF
	OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5  ___ZKOHuQntYJaUOOJHSLzgGWFgqQF_38;
	// System.Func`2<Rewired.HID.OutputReport,System.Boolean> Rewired.HID.Drivers.RailDriverDriver::jjxLdqHvqzvguEjvbDELBdomYEaU
	Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8 * ___jjxLdqHvqzvguEjvbDELBdomYEaU_39;
	// System.Action`1<Rewired.HID.OutputReport> Rewired.HID.Drivers.RailDriverDriver::KrnGaZawVhHQVCvIixlFJZuYWiA
	Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832 * ___KrnGaZawVhHQVCvIixlFJZuYWiA_40;

public:
	inline static int32_t get_offset_of_JVsgwlgmwsLVfxuQuuLMmdvVseuU_34() { return static_cast<int32_t>(offsetof(RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688, ___JVsgwlgmwsLVfxuQuuLMmdvVseuU_34)); }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * get_JVsgwlgmwsLVfxuQuuLMmdvVseuU_34() const { return ___JVsgwlgmwsLVfxuQuuLMmdvVseuU_34; }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 ** get_address_of_JVsgwlgmwsLVfxuQuuLMmdvVseuU_34() { return &___JVsgwlgmwsLVfxuQuuLMmdvVseuU_34; }
	inline void set_JVsgwlgmwsLVfxuQuuLMmdvVseuU_34(NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * value)
	{
		___JVsgwlgmwsLVfxuQuuLMmdvVseuU_34 = value;
		Il2CppCodeGenWriteBarrier((&___JVsgwlgmwsLVfxuQuuLMmdvVseuU_34), value);
	}

	inline static int32_t get_offset_of_WgXlHLobIXEGVFDvcmIWrNRWIsj_35() { return static_cast<int32_t>(offsetof(RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688, ___WgXlHLobIXEGVFDvcmIWrNRWIsj_35)); }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * get_WgXlHLobIXEGVFDvcmIWrNRWIsj_35() const { return ___WgXlHLobIXEGVFDvcmIWrNRWIsj_35; }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 ** get_address_of_WgXlHLobIXEGVFDvcmIWrNRWIsj_35() { return &___WgXlHLobIXEGVFDvcmIWrNRWIsj_35; }
	inline void set_WgXlHLobIXEGVFDvcmIWrNRWIsj_35(NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * value)
	{
		___WgXlHLobIXEGVFDvcmIWrNRWIsj_35 = value;
		Il2CppCodeGenWriteBarrier((&___WgXlHLobIXEGVFDvcmIWrNRWIsj_35), value);
	}

	inline static int32_t get_offset_of_eqENXoqEArTCUWjquEQYFDTxSOvW_36() { return static_cast<int32_t>(offsetof(RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688, ___eqENXoqEArTCUWjquEQYFDTxSOvW_36)); }
	inline bool get_eqENXoqEArTCUWjquEQYFDTxSOvW_36() const { return ___eqENXoqEArTCUWjquEQYFDTxSOvW_36; }
	inline bool* get_address_of_eqENXoqEArTCUWjquEQYFDTxSOvW_36() { return &___eqENXoqEArTCUWjquEQYFDTxSOvW_36; }
	inline void set_eqENXoqEArTCUWjquEQYFDTxSOvW_36(bool value)
	{
		___eqENXoqEArTCUWjquEQYFDTxSOvW_36 = value;
	}

	inline static int32_t get_offset_of_FwNKSIMHOALacyXryDksxPdrVVl_37() { return static_cast<int32_t>(offsetof(RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688, ___FwNKSIMHOALacyXryDksxPdrVVl_37)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_FwNKSIMHOALacyXryDksxPdrVVl_37() const { return ___FwNKSIMHOALacyXryDksxPdrVVl_37; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_FwNKSIMHOALacyXryDksxPdrVVl_37() { return &___FwNKSIMHOALacyXryDksxPdrVVl_37; }
	inline void set_FwNKSIMHOALacyXryDksxPdrVVl_37(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___FwNKSIMHOALacyXryDksxPdrVVl_37 = value;
		Il2CppCodeGenWriteBarrier((&___FwNKSIMHOALacyXryDksxPdrVVl_37), value);
	}

	inline static int32_t get_offset_of_ZKOHuQntYJaUOOJHSLzgGWFgqQF_38() { return static_cast<int32_t>(offsetof(RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688, ___ZKOHuQntYJaUOOJHSLzgGWFgqQF_38)); }
	inline OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5  get_ZKOHuQntYJaUOOJHSLzgGWFgqQF_38() const { return ___ZKOHuQntYJaUOOJHSLzgGWFgqQF_38; }
	inline OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5 * get_address_of_ZKOHuQntYJaUOOJHSLzgGWFgqQF_38() { return &___ZKOHuQntYJaUOOJHSLzgGWFgqQF_38; }
	inline void set_ZKOHuQntYJaUOOJHSLzgGWFgqQF_38(OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5  value)
	{
		___ZKOHuQntYJaUOOJHSLzgGWFgqQF_38 = value;
	}

	inline static int32_t get_offset_of_jjxLdqHvqzvguEjvbDELBdomYEaU_39() { return static_cast<int32_t>(offsetof(RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688, ___jjxLdqHvqzvguEjvbDELBdomYEaU_39)); }
	inline Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8 * get_jjxLdqHvqzvguEjvbDELBdomYEaU_39() const { return ___jjxLdqHvqzvguEjvbDELBdomYEaU_39; }
	inline Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8 ** get_address_of_jjxLdqHvqzvguEjvbDELBdomYEaU_39() { return &___jjxLdqHvqzvguEjvbDELBdomYEaU_39; }
	inline void set_jjxLdqHvqzvguEjvbDELBdomYEaU_39(Func_2_tFEE8FAC186D3DEBE6E21AD2F086AD8498981AFA8 * value)
	{
		___jjxLdqHvqzvguEjvbDELBdomYEaU_39 = value;
		Il2CppCodeGenWriteBarrier((&___jjxLdqHvqzvguEjvbDELBdomYEaU_39), value);
	}

	inline static int32_t get_offset_of_KrnGaZawVhHQVCvIixlFJZuYWiA_40() { return static_cast<int32_t>(offsetof(RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688, ___KrnGaZawVhHQVCvIixlFJZuYWiA_40)); }
	inline Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832 * get_KrnGaZawVhHQVCvIixlFJZuYWiA_40() const { return ___KrnGaZawVhHQVCvIixlFJZuYWiA_40; }
	inline Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832 ** get_address_of_KrnGaZawVhHQVCvIixlFJZuYWiA_40() { return &___KrnGaZawVhHQVCvIixlFJZuYWiA_40; }
	inline void set_KrnGaZawVhHQVCvIixlFJZuYWiA_40(Action_1_t9B3B6567C2DBEE56DBFDE9299EA322B6F9488832 * value)
	{
		___KrnGaZawVhHQVCvIixlFJZuYWiA_40 = value;
		Il2CppCodeGenWriteBarrier((&___KrnGaZawVhHQVCvIixlFJZuYWiA_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAILDRIVERDRIVER_TDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688_H
#ifndef XNFFBHKEKMHRVYHGDZBOQGTGQXS_T1548DFA2A812084546F6CD0007C7C746DC655D26_H
#define XNFFBHKEKMHRVYHGDZBOQGTGQXS_T1548DFA2A812084546F6CD0007C7C746DC655D26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HIDGyroscope_xNFFbHKeKmHrVYhgdzBoQgtgqxs
struct  xNFFbHKeKmHrVYhgdzBoQgtgqxs_t1548DFA2A812084546F6CD0007C7C746DC655D26  : public CVaCXXbjFHbTGydqcOoiGPdeykAL_tD7DD1276581C9E6FDF35379998DD5CEE6A2567D5
{
public:
	// System.Single[] Rewired.HID.HIDGyroscope_xNFFbHKeKmHrVYhgdzBoQgtgqxs::idzBDqZvmdhAhJjckuXPucOyrRds
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___idzBDqZvmdhAhJjckuXPucOyrRds_1;
	// System.Single[] Rewired.HID.HIDGyroscope_xNFFbHKeKmHrVYhgdzBoQgtgqxs::HrLhhDFKAQCiJIauDwTQdshRCqV
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___HrLhhDFKAQCiJIauDwTQdshRCqV_2;
	// Rewired.Utils.Classes.Data.ExpandableArray_DataContainer`1<Rewired.HID.HIDGyroscope_hoiXDorKAcXnjheVDxCUSVqObdh> Rewired.HID.HIDGyroscope_xNFFbHKeKmHrVYhgdzBoQgtgqxs::psFuKOhcRHTESTSvwhokIWDxocu
	ExpandableArray_DataContainer_1_tE100BB3581A1C6D56FD572CFE6FEBB136AC77E7C * ___psFuKOhcRHTESTSvwhokIWDxocu_3;
	// Rewired.Utils.Classes.Data.ExpandableArray_DataContainer`1<Rewired.HID.HIDGyroscope_hoiXDorKAcXnjheVDxCUSVqObdh> Rewired.HID.HIDGyroscope_xNFFbHKeKmHrVYhgdzBoQgtgqxs::PCNLBpxMQWqHvwdbidXvESQeOqNr
	ExpandableArray_DataContainer_1_tE100BB3581A1C6D56FD572CFE6FEBB136AC77E7C * ___PCNLBpxMQWqHvwdbidXvESQeOqNr_4;

public:
	inline static int32_t get_offset_of_idzBDqZvmdhAhJjckuXPucOyrRds_1() { return static_cast<int32_t>(offsetof(xNFFbHKeKmHrVYhgdzBoQgtgqxs_t1548DFA2A812084546F6CD0007C7C746DC655D26, ___idzBDqZvmdhAhJjckuXPucOyrRds_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_idzBDqZvmdhAhJjckuXPucOyrRds_1() const { return ___idzBDqZvmdhAhJjckuXPucOyrRds_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_idzBDqZvmdhAhJjckuXPucOyrRds_1() { return &___idzBDqZvmdhAhJjckuXPucOyrRds_1; }
	inline void set_idzBDqZvmdhAhJjckuXPucOyrRds_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___idzBDqZvmdhAhJjckuXPucOyrRds_1 = value;
		Il2CppCodeGenWriteBarrier((&___idzBDqZvmdhAhJjckuXPucOyrRds_1), value);
	}

	inline static int32_t get_offset_of_HrLhhDFKAQCiJIauDwTQdshRCqV_2() { return static_cast<int32_t>(offsetof(xNFFbHKeKmHrVYhgdzBoQgtgqxs_t1548DFA2A812084546F6CD0007C7C746DC655D26, ___HrLhhDFKAQCiJIauDwTQdshRCqV_2)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_HrLhhDFKAQCiJIauDwTQdshRCqV_2() const { return ___HrLhhDFKAQCiJIauDwTQdshRCqV_2; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_HrLhhDFKAQCiJIauDwTQdshRCqV_2() { return &___HrLhhDFKAQCiJIauDwTQdshRCqV_2; }
	inline void set_HrLhhDFKAQCiJIauDwTQdshRCqV_2(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___HrLhhDFKAQCiJIauDwTQdshRCqV_2 = value;
		Il2CppCodeGenWriteBarrier((&___HrLhhDFKAQCiJIauDwTQdshRCqV_2), value);
	}

	inline static int32_t get_offset_of_psFuKOhcRHTESTSvwhokIWDxocu_3() { return static_cast<int32_t>(offsetof(xNFFbHKeKmHrVYhgdzBoQgtgqxs_t1548DFA2A812084546F6CD0007C7C746DC655D26, ___psFuKOhcRHTESTSvwhokIWDxocu_3)); }
	inline ExpandableArray_DataContainer_1_tE100BB3581A1C6D56FD572CFE6FEBB136AC77E7C * get_psFuKOhcRHTESTSvwhokIWDxocu_3() const { return ___psFuKOhcRHTESTSvwhokIWDxocu_3; }
	inline ExpandableArray_DataContainer_1_tE100BB3581A1C6D56FD572CFE6FEBB136AC77E7C ** get_address_of_psFuKOhcRHTESTSvwhokIWDxocu_3() { return &___psFuKOhcRHTESTSvwhokIWDxocu_3; }
	inline void set_psFuKOhcRHTESTSvwhokIWDxocu_3(ExpandableArray_DataContainer_1_tE100BB3581A1C6D56FD572CFE6FEBB136AC77E7C * value)
	{
		___psFuKOhcRHTESTSvwhokIWDxocu_3 = value;
		Il2CppCodeGenWriteBarrier((&___psFuKOhcRHTESTSvwhokIWDxocu_3), value);
	}

	inline static int32_t get_offset_of_PCNLBpxMQWqHvwdbidXvESQeOqNr_4() { return static_cast<int32_t>(offsetof(xNFFbHKeKmHrVYhgdzBoQgtgqxs_t1548DFA2A812084546F6CD0007C7C746DC655D26, ___PCNLBpxMQWqHvwdbidXvESQeOqNr_4)); }
	inline ExpandableArray_DataContainer_1_tE100BB3581A1C6D56FD572CFE6FEBB136AC77E7C * get_PCNLBpxMQWqHvwdbidXvESQeOqNr_4() const { return ___PCNLBpxMQWqHvwdbidXvESQeOqNr_4; }
	inline ExpandableArray_DataContainer_1_tE100BB3581A1C6D56FD572CFE6FEBB136AC77E7C ** get_address_of_PCNLBpxMQWqHvwdbidXvESQeOqNr_4() { return &___PCNLBpxMQWqHvwdbidXvESQeOqNr_4; }
	inline void set_PCNLBpxMQWqHvwdbidXvESQeOqNr_4(ExpandableArray_DataContainer_1_tE100BB3581A1C6D56FD572CFE6FEBB136AC77E7C * value)
	{
		___PCNLBpxMQWqHvwdbidXvESQeOqNr_4 = value;
		Il2CppCodeGenWriteBarrier((&___PCNLBpxMQWqHvwdbidXvESQeOqNr_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XNFFBHKEKMHRVYHGDZBOQGTGQXS_T1548DFA2A812084546F6CD0007C7C746DC655D26_H
#ifndef WRITEREPORTDELEGATE_TE0510A5AB2468221BF3ECDD4A176FE7F3A71706F_H
#define WRITEREPORTDELEGATE_TE0510A5AB2468221BF3ECDD4A176FE7F3A71706F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HidOutputReportHandler_WriteReportDelegate
struct  WriteReportDelegate_tE0510A5AB2468221BF3ECDD4A176FE7F3A71706F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEREPORTDELEGATE_TE0510A5AB2468221BF3ECDD4A176FE7F3A71706F_H
#ifndef QQWWJHHBPJGWYXOUXBIERDAKYMC_T6DA2E79E0B0780669217400A6D117C18088D5EE2_H
#define QQWWJHHBPJGWYXOUXBIERDAKYMC_T6DA2E79E0B0780669217400A6D117C18088D5EE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HID.HidOutputReportHandler_qQwWjhhBpjGwyXouxBiErDakyMc
struct  qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2  : public RuntimeObject
{
public:
	// System.Boolean Rewired.HID.HidOutputReportHandler_qQwWjhhBpjGwyXouxBiErDakyMc::hBdFlAtFYUGadBUKnUCaSvYWLJA
	bool ___hBdFlAtFYUGadBUKnUCaSvYWLJA_0;
	// Rewired.HID.OutputReport Rewired.HID.HidOutputReportHandler_qQwWjhhBpjGwyXouxBiErDakyMc::BgwYOgUBehzXecfdAQjeyMPbNZG
	OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5  ___BgwYOgUBehzXecfdAQjeyMPbNZG_1;
	// Rewired.Utils.Classes.Data.NativeBuffer Rewired.HID.HidOutputReportHandler_qQwWjhhBpjGwyXouxBiErDakyMc::gWYcSreEUttuvqTHqKoRwtWQvHb
	NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * ___gWYcSreEUttuvqTHqKoRwtWQvHb_2;
	// System.Boolean Rewired.HID.HidOutputReportHandler_qQwWjhhBpjGwyXouxBiErDakyMc::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_3;

public:
	inline static int32_t get_offset_of_hBdFlAtFYUGadBUKnUCaSvYWLJA_0() { return static_cast<int32_t>(offsetof(qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2, ___hBdFlAtFYUGadBUKnUCaSvYWLJA_0)); }
	inline bool get_hBdFlAtFYUGadBUKnUCaSvYWLJA_0() const { return ___hBdFlAtFYUGadBUKnUCaSvYWLJA_0; }
	inline bool* get_address_of_hBdFlAtFYUGadBUKnUCaSvYWLJA_0() { return &___hBdFlAtFYUGadBUKnUCaSvYWLJA_0; }
	inline void set_hBdFlAtFYUGadBUKnUCaSvYWLJA_0(bool value)
	{
		___hBdFlAtFYUGadBUKnUCaSvYWLJA_0 = value;
	}

	inline static int32_t get_offset_of_BgwYOgUBehzXecfdAQjeyMPbNZG_1() { return static_cast<int32_t>(offsetof(qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2, ___BgwYOgUBehzXecfdAQjeyMPbNZG_1)); }
	inline OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5  get_BgwYOgUBehzXecfdAQjeyMPbNZG_1() const { return ___BgwYOgUBehzXecfdAQjeyMPbNZG_1; }
	inline OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5 * get_address_of_BgwYOgUBehzXecfdAQjeyMPbNZG_1() { return &___BgwYOgUBehzXecfdAQjeyMPbNZG_1; }
	inline void set_BgwYOgUBehzXecfdAQjeyMPbNZG_1(OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5  value)
	{
		___BgwYOgUBehzXecfdAQjeyMPbNZG_1 = value;
	}

	inline static int32_t get_offset_of_gWYcSreEUttuvqTHqKoRwtWQvHb_2() { return static_cast<int32_t>(offsetof(qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2, ___gWYcSreEUttuvqTHqKoRwtWQvHb_2)); }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * get_gWYcSreEUttuvqTHqKoRwtWQvHb_2() const { return ___gWYcSreEUttuvqTHqKoRwtWQvHb_2; }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 ** get_address_of_gWYcSreEUttuvqTHqKoRwtWQvHb_2() { return &___gWYcSreEUttuvqTHqKoRwtWQvHb_2; }
	inline void set_gWYcSreEUttuvqTHqKoRwtWQvHb_2(NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * value)
	{
		___gWYcSreEUttuvqTHqKoRwtWQvHb_2 = value;
		Il2CppCodeGenWriteBarrier((&___gWYcSreEUttuvqTHqKoRwtWQvHb_2), value);
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_3() { return static_cast<int32_t>(offsetof(qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2, ___SeCUoinDywZmqZDHRKupOdOaTke_3)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_3() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_3; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_3() { return &___SeCUoinDywZmqZDHRKupOdOaTke_3; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_3(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QQWWJHHBPJGWYXOUXBIERDAKYMC_T6DA2E79E0B0780669217400A6D117C18088D5EE2_H
#ifndef AXISVALUECHANGEDEVENTHANDLER_T14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237_H
#define AXISVALUECHANGEDEVENTHANDLER_T14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Internal.StandaloneAxis_AxisValueChangedEventHandler
struct  AxisValueChangedEventHandler_t14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISVALUECHANGEDEVENTHANDLER_T14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237_H
#ifndef BUTTONDOWNEVENTHANDLER_TDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE_H
#define BUTTONDOWNEVENTHANDLER_TDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Internal.StandaloneAxis_ButtonDownEventHandler
struct  ButtonDownEventHandler_tDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONDOWNEVENTHANDLER_TDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE_H
#ifndef BUTTONUPEVENTHANDLER_T7D7868DC7B0024CD5CC804C9D9BA28C302222FB9_H
#define BUTTONUPEVENTHANDLER_T7D7868DC7B0024CD5CC804C9D9BA28C302222FB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Internal.StandaloneAxis_ButtonUpEventHandler
struct  ButtonUpEventHandler_t7D7868DC7B0024CD5CC804C9D9BA28C302222FB9  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONUPEVENTHANDLER_T7D7868DC7B0024CD5CC804C9D9BA28C302222FB9_H
#ifndef BUTTONVALUECHANGEDEVENTHANDLER_TD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5_H
#define BUTTONVALUECHANGEDEVENTHANDLER_TD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Internal.StandaloneAxis_ButtonValueChangedEventHandler
struct  ButtonValueChangedEventHandler_tD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONVALUECHANGEDEVENTHANDLER_TD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5_H
#ifndef VALUECHANGEDEVENTHANDLER_T4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7_H
#define VALUECHANGEDEVENTHANDLER_T4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Internal.StandaloneAxis2D_ValueChangedEventHandler
struct  ValueChangedEventHandler_t4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECHANGEDEVENTHANDLER_T4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7_H
#ifndef JOYSTICKCALIBRATIONMAPSAVEDATA_TEA7AF93E292FA30ADE52E7573A4A900AE05F095D_H
#define JOYSTICKCALIBRATIONMAPSAVEDATA_TEA7AF93E292FA30ADE52E7573A4A900AE05F095D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.JoystickCalibrationMapSaveData
struct  JoystickCalibrationMapSaveData_tEA7AF93E292FA30ADE52E7573A4A900AE05F095D  : public CalibrationMapSaveData_tE4E68C169ADAF1B228EFF24A8A0301FBB3937C71
{
public:
	// System.Guid Rewired.JoystickCalibrationMapSaveData::DqbGjcJsIYMblhndFwIlPVbyBLpR
	Guid_t  ___DqbGjcJsIYMblhndFwIlPVbyBLpR_3;

public:
	inline static int32_t get_offset_of_DqbGjcJsIYMblhndFwIlPVbyBLpR_3() { return static_cast<int32_t>(offsetof(JoystickCalibrationMapSaveData_tEA7AF93E292FA30ADE52E7573A4A900AE05F095D, ___DqbGjcJsIYMblhndFwIlPVbyBLpR_3)); }
	inline Guid_t  get_DqbGjcJsIYMblhndFwIlPVbyBLpR_3() const { return ___DqbGjcJsIYMblhndFwIlPVbyBLpR_3; }
	inline Guid_t * get_address_of_DqbGjcJsIYMblhndFwIlPVbyBLpR_3() { return &___DqbGjcJsIYMblhndFwIlPVbyBLpR_3; }
	inline void set_DqbGjcJsIYMblhndFwIlPVbyBLpR_3(Guid_t  value)
	{
		___DqbGjcJsIYMblhndFwIlPVbyBLpR_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKCALIBRATIONMAPSAVEDATA_TEA7AF93E292FA30ADE52E7573A4A900AE05F095D_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef INPUTMANAGER_BASE_T59B15491EE0FD742B61D3285150943B4A6D35701_H
#define INPUTMANAGER_BASE_T59B15491EE0FD742B61D3285150943B4A6D35701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputManager_Base
struct  InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Rewired.InputManager_Base::_dontDestroyOnLoad
	bool ____dontDestroyOnLoad_4;
	// Rewired.Data.UserData Rewired.InputManager_Base::_userData
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ____userData_5;
	// Rewired.Data.ControllerDataFiles Rewired.InputManager_Base::_controllerDataFiles
	ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5 * ____controllerDataFiles_6;
	// System.Boolean Rewired.InputManager_Base::isCompiling
	bool ___isCompiling_7;
	// System.Boolean Rewired.InputManager_Base::initialized
	bool ___initialized_8;
	// System.Boolean Rewired.InputManager_Base::criticalError
	bool ___criticalError_9;
	// Rewired.Platforms.EditorPlatform Rewired.InputManager_Base::editorPlatform
	int32_t ___editorPlatform_10;
	// Rewired.Platforms.Platform Rewired.InputManager_Base::platform
	int32_t ___platform_11;
	// Rewired.Platforms.WebplayerPlatform Rewired.InputManager_Base::webplayerPlatform
	int32_t ___webplayerPlatform_12;
	// System.Boolean Rewired.InputManager_Base::isEditor
	bool ___isEditor_13;
	// System.Boolean Rewired.InputManager_Base::_detectedPlatformInEditor
	bool ____detectedPlatformInEditor_14;
	// Rewired.Platforms.ScriptingBackend Rewired.InputManager_Base::scriptingBackend
	int32_t ___scriptingBackend_15;
	// Rewired.Platforms.ScriptingAPILevel Rewired.InputManager_Base::scriptingAPILevel
	int32_t ___scriptingAPILevel_16;
	// System.Boolean Rewired.InputManager_Base::_duplicateRIMError
	bool ____duplicateRIMError_17;
	// System.Boolean Rewired.InputManager_Base::_isAwake
	bool ____isAwake_18;

public:
	inline static int32_t get_offset_of__dontDestroyOnLoad_4() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ____dontDestroyOnLoad_4)); }
	inline bool get__dontDestroyOnLoad_4() const { return ____dontDestroyOnLoad_4; }
	inline bool* get_address_of__dontDestroyOnLoad_4() { return &____dontDestroyOnLoad_4; }
	inline void set__dontDestroyOnLoad_4(bool value)
	{
		____dontDestroyOnLoad_4 = value;
	}

	inline static int32_t get_offset_of__userData_5() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ____userData_5)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get__userData_5() const { return ____userData_5; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of__userData_5() { return &____userData_5; }
	inline void set__userData_5(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		____userData_5 = value;
		Il2CppCodeGenWriteBarrier((&____userData_5), value);
	}

	inline static int32_t get_offset_of__controllerDataFiles_6() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ____controllerDataFiles_6)); }
	inline ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5 * get__controllerDataFiles_6() const { return ____controllerDataFiles_6; }
	inline ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5 ** get_address_of__controllerDataFiles_6() { return &____controllerDataFiles_6; }
	inline void set__controllerDataFiles_6(ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5 * value)
	{
		____controllerDataFiles_6 = value;
		Il2CppCodeGenWriteBarrier((&____controllerDataFiles_6), value);
	}

	inline static int32_t get_offset_of_isCompiling_7() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___isCompiling_7)); }
	inline bool get_isCompiling_7() const { return ___isCompiling_7; }
	inline bool* get_address_of_isCompiling_7() { return &___isCompiling_7; }
	inline void set_isCompiling_7(bool value)
	{
		___isCompiling_7 = value;
	}

	inline static int32_t get_offset_of_initialized_8() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___initialized_8)); }
	inline bool get_initialized_8() const { return ___initialized_8; }
	inline bool* get_address_of_initialized_8() { return &___initialized_8; }
	inline void set_initialized_8(bool value)
	{
		___initialized_8 = value;
	}

	inline static int32_t get_offset_of_criticalError_9() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___criticalError_9)); }
	inline bool get_criticalError_9() const { return ___criticalError_9; }
	inline bool* get_address_of_criticalError_9() { return &___criticalError_9; }
	inline void set_criticalError_9(bool value)
	{
		___criticalError_9 = value;
	}

	inline static int32_t get_offset_of_editorPlatform_10() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___editorPlatform_10)); }
	inline int32_t get_editorPlatform_10() const { return ___editorPlatform_10; }
	inline int32_t* get_address_of_editorPlatform_10() { return &___editorPlatform_10; }
	inline void set_editorPlatform_10(int32_t value)
	{
		___editorPlatform_10 = value;
	}

	inline static int32_t get_offset_of_platform_11() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___platform_11)); }
	inline int32_t get_platform_11() const { return ___platform_11; }
	inline int32_t* get_address_of_platform_11() { return &___platform_11; }
	inline void set_platform_11(int32_t value)
	{
		___platform_11 = value;
	}

	inline static int32_t get_offset_of_webplayerPlatform_12() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___webplayerPlatform_12)); }
	inline int32_t get_webplayerPlatform_12() const { return ___webplayerPlatform_12; }
	inline int32_t* get_address_of_webplayerPlatform_12() { return &___webplayerPlatform_12; }
	inline void set_webplayerPlatform_12(int32_t value)
	{
		___webplayerPlatform_12 = value;
	}

	inline static int32_t get_offset_of_isEditor_13() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___isEditor_13)); }
	inline bool get_isEditor_13() const { return ___isEditor_13; }
	inline bool* get_address_of_isEditor_13() { return &___isEditor_13; }
	inline void set_isEditor_13(bool value)
	{
		___isEditor_13 = value;
	}

	inline static int32_t get_offset_of__detectedPlatformInEditor_14() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ____detectedPlatformInEditor_14)); }
	inline bool get__detectedPlatformInEditor_14() const { return ____detectedPlatformInEditor_14; }
	inline bool* get_address_of__detectedPlatformInEditor_14() { return &____detectedPlatformInEditor_14; }
	inline void set__detectedPlatformInEditor_14(bool value)
	{
		____detectedPlatformInEditor_14 = value;
	}

	inline static int32_t get_offset_of_scriptingBackend_15() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___scriptingBackend_15)); }
	inline int32_t get_scriptingBackend_15() const { return ___scriptingBackend_15; }
	inline int32_t* get_address_of_scriptingBackend_15() { return &___scriptingBackend_15; }
	inline void set_scriptingBackend_15(int32_t value)
	{
		___scriptingBackend_15 = value;
	}

	inline static int32_t get_offset_of_scriptingAPILevel_16() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___scriptingAPILevel_16)); }
	inline int32_t get_scriptingAPILevel_16() const { return ___scriptingAPILevel_16; }
	inline int32_t* get_address_of_scriptingAPILevel_16() { return &___scriptingAPILevel_16; }
	inline void set_scriptingAPILevel_16(int32_t value)
	{
		___scriptingAPILevel_16 = value;
	}

	inline static int32_t get_offset_of__duplicateRIMError_17() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ____duplicateRIMError_17)); }
	inline bool get__duplicateRIMError_17() const { return ____duplicateRIMError_17; }
	inline bool* get_address_of__duplicateRIMError_17() { return &____duplicateRIMError_17; }
	inline void set__duplicateRIMError_17(bool value)
	{
		____duplicateRIMError_17 = value;
	}

	inline static int32_t get_offset_of__isAwake_18() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ____isAwake_18)); }
	inline bool get__isAwake_18() const { return ____isAwake_18; }
	inline bool* get_address_of__isAwake_18() { return &____isAwake_18; }
	inline void set__isAwake_18(bool value)
	{
		____isAwake_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMANAGER_BASE_T59B15491EE0FD742B61D3285150943B4A6D35701_H
#ifndef ONGUIHELPER_T4041D6BAB19CDA8CFB4BDF4268CAC1A2E4774121_H
#define ONGUIHELPER_T4041D6BAB19CDA8CFB4BDF4268CAC1A2E4774121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Internal.OnGUIHelper
struct  OnGUIHelper_t4041D6BAB19CDA8CFB4BDF4268CAC1A2E4774121  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.InputManager_Base Rewired.Internal.OnGUIHelper::YsciKTkdsyeKqevEdLRABIojdVHx
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * ___YsciKTkdsyeKqevEdLRABIojdVHx_4;

public:
	inline static int32_t get_offset_of_YsciKTkdsyeKqevEdLRABIojdVHx_4() { return static_cast<int32_t>(offsetof(OnGUIHelper_t4041D6BAB19CDA8CFB4BDF4268CAC1A2E4774121, ___YsciKTkdsyeKqevEdLRABIojdVHx_4)); }
	inline InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * get_YsciKTkdsyeKqevEdLRABIojdVHx_4() const { return ___YsciKTkdsyeKqevEdLRABIojdVHx_4; }
	inline InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 ** get_address_of_YsciKTkdsyeKqevEdLRABIojdVHx_4() { return &___YsciKTkdsyeKqevEdLRABIojdVHx_4; }
	inline void set_YsciKTkdsyeKqevEdLRABIojdVHx_4(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * value)
	{
		___YsciKTkdsyeKqevEdLRABIojdVHx_4 = value;
		Il2CppCodeGenWriteBarrier((&___YsciKTkdsyeKqevEdLRABIojdVHx_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGUIHELPER_T4041D6BAB19CDA8CFB4BDF4268CAC1A2E4774121_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4500 = { sizeof (ControllerDisconnectedEventArgs_tE3F08FB1D1CCD23522D1FE41576E01ECCC2275D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4500[1] = 
{
	ControllerDisconnectedEventArgs_tE3F08FB1D1CCD23522D1FE41576E01ECCC2275D5::get_offset_of_rewiredId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4501 = { sizeof (HIDControllerElement_tC98B3FAE2975F27917258ACE9B578318D1EAF7E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4501[2] = 
{
	HIDControllerElement_tC98B3FAE2975F27917258ACE9B578318D1EAF7E0::get_offset_of_reportId_0(),
	HIDControllerElement_tC98B3FAE2975F27917258ACE9B578318D1EAF7E0::get_offset_of_hidInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4502 = { sizeof (HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4502[10] = 
{
	HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366::get_offset_of_usagePage_0(),
	HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366::get_offset_of_usage_1(),
	HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366::get_offset_of_dataIndex_2(),
	HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366::get_offset_of_bitSize_3(),
	HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366::get_offset_of_logicalMin_4(),
	HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366::get_offset_of_logicalMax_5(),
	HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366::get_offset_of_physicalMin_6(),
	HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366::get_offset_of_physicalMax_7(),
	HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366::get_offset_of_units_8(),
	HIDInfo_tC6AFF24A777D4DFEA56D99BCF63C31CE90E21366::get_offset_of_unitsExp_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4503 = { sizeof (HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4503[7] = 
{
	HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76::get_offset_of_rawValue_2(),
	HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76::get_offset_of_timestamp_3(),
	HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76::get_offset_of_valueLength_4(),
	HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76::get_offset_of_NRwCWKPZMBPqSTcLtLnOurBUoWN_5(),
	HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76::get_offset_of_MXKCkdMwCxEJlCsqhAxVfcBDfLde_6(),
	HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76::get_offset_of_AfklhFSjQRdMZXJChQYWNazMHbq_7(),
	HIDAccelerometer_tDDC5E85F6E67F7DE79EBC8CFE7C84C385467FB76::get_offset_of_agqHqDPvTEqPJElFDveRIhPWjClF_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4504 = { sizeof (HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4504[8] = 
{
	HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D::get_offset_of_rawValue_2(),
	HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D::get_offset_of_timestamp_3(),
	HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D::get_offset_of_byteLength_4(),
	HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D::get_offset_of_startIndex_5(),
	HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D::get_offset_of_isSigned_6(),
	HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D::get_offset_of_minValue_7(),
	HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D::get_offset_of_maxValue_8(),
	HIDAxis_tCD328EF91E1C7EA0A3C34DA98AFEE5CA727BF09D::get_offset_of_zeroValue_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4505 = { sizeof (HIDButton_t6FDD5F213F676E5818EA2F010CF356AB8CC61F33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4505[2] = 
{
	HIDButton_t6FDD5F213F676E5818EA2F010CF356AB8CC61F33::get_offset_of_rawValue_2(),
	HIDButton_t6FDD5F213F676E5818EA2F010CF356AB8CC61F33::get_offset_of_timestamp_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4506 = { sizeof (HidAsyncState_tC164BD9FBE8C9F0429BDF795DCADBBABD1D3DF3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4506[2] = 
{
	HidAsyncState_tC164BD9FBE8C9F0429BDF795DCADBBABD1D3DF3F::get_offset_of_sPTpDqQdEBUDvQzULhTTSiouVDp_0(),
	HidAsyncState_tC164BD9FBE8C9F0429BDF795DCADBBABD1D3DF3F::get_offset_of_EzfDOGOPpWwwmlcesauoDQIXFNqR_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4507 = { sizeof (OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5)+ sizeof (RuntimeObject), sizeof(OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4507[5] = 
{
	OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5::get_offset_of_reportId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5::get_offset_of_buffer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5::get_offset_of_bufferLength_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5::get_offset_of_reportLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OutputReport_tE986EE6C5A1C4E91ECB37D1333990550C69812F5::get_offset_of_options_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4508 = { sizeof (HIDControllerElementWithDataSet_t991E71547369AF5788D0C1935AC633A2A0128775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4508[1] = 
{
	HIDControllerElementWithDataSet_t991E71547369AF5788D0C1935AC633A2A0128775::get_offset_of_dataSet_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4509 = { sizeof (wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4509[7] = 
{
	wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5::get_offset_of_dufrXbHCNDnCDAkzaZIcSkKuBqZ_0(),
	wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5::get_offset_of_UtyHrsDgBPEcHKmyNNRLLGUMtoub_1(),
	wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5::get_offset_of_XzwvNxyYkXcaiidxwJGvQNwBigT_2(),
	wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5::get_offset_of_YaDAnfimULzphKnSejtwEcfSlkEJ_3(),
	wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5::get_offset_of_uiElHIMsJhOUGwxstGSvtQgdJHm_4(),
	wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5::get_offset_of_mitExlcuToNzViGEORWHsMktptx_5(),
	wgqSTfYHDsMNygvPFQwICLAnDKO_t8BAD9E0D116BF67A723C851334BA553EA028C7E5::get_offset_of_ffNWJUuxPxuFMIZszOmCdhFAkUp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4510 = { sizeof (CVaCXXbjFHbTGydqcOoiGPdeykAL_tD7DD1276581C9E6FDF35379998DD5CEE6A2567D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4510[1] = 
{
	CVaCXXbjFHbTGydqcOoiGPdeykAL_tD7DD1276581C9E6FDF35379998DD5CEE6A2567D5::get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4511 = { sizeof (HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4511[9] = 
{
	HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC::get_offset_of_axes_0(),
	HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC::get_offset_of_buttons_1(),
	HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC::get_offset_of_hats_2(),
	HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC::get_offset_of_accelerometers_3(),
	HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC::get_offset_of_gyroscopes_4(),
	HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC::get_offset_of_touchpads_5(),
	HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC::get_offset_of_vibrationMotors_6(),
	HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC::get_offset_of_lights_7(),
	HIDDeviceDriver_t58509E517EE4CB9F0364323BB01CD6A39705AFFC::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4512 = { sizeof (InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4512[10] = 
{
	InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8::get_offset_of_updateLoopSetting_0(),
	InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8::get_offset_of_connectionType_1(),
	InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8::get_offset_of_minAxisValue_2(),
	InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8::get_offset_of_maxAxisValue_3(),
	InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8::get_offset_of_hatZeroValue_4(),
	InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8::get_offset_of_hatSpan_5(),
	InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8::get_offset_of_inputReportLength_6(),
	InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8::get_offset_of_outputReportLength_7(),
	InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8::get_offset_of_synchronousWriteOutputReportDelegate_8(),
	InitArgs_t4772E24C0FD1AF75A609A9B4D7C40E69E19B4FB8::get_offset_of_asynchronousWriteOutputReportDelegate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4513 = { sizeof (DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4513[92] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_vbAdkxyBQGZnWgwStHAftFUhnMl_69(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_cvbkCLSefIoPdaRkJzWOVwVdDOF_70(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_mjLbjWGxNTiuJiCJZwmkHKFcaCLf_71(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_cRTHNMIZPcYTXDzMxbEdznBfJju_72(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_vpDaVaPWWJvlTXUvCLmTamgdTZu_73(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_pIzBOGVpMGmZYfvyQCflfLljPsu_74(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_ZkXtNTyqIqlwndwCWoNxaqbTNZO_75(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_xKcEWzaZZMnwclywIKtCnglaySh_76(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_xzrQffrYIPOPiwKYJcUAWeFIxnL_77(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_cjHetCnPAKaCZYGymgXYKYCcheu_78(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_GFSUWBglLSCeWBmOcfOpbJiRMBMd_79(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_JVsgwlgmwsLVfxuQuuLMmdvVseuU_80(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_WgXlHLobIXEGVFDvcmIWrNRWIsj_81(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_ZKOHuQntYJaUOOJHSLzgGWFgqQF_82(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_jjxLdqHvqzvguEjvbDELBdomYEaU_83(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_KrnGaZawVhHQVCvIixlFJZuYWiA_84(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_EjvDLAplqfwQVpPeGHHWCocbDKpT_85(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_WUDRxWarrxHANXPgdPRtgWlPwYH_86(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_jpRbbilkJKfhmFMAmPONKvFcRvaO_87(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_HqmcZGILGJJHIugaGAfpYXwIZel_88(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_cbXHqdaCQrWlIqinLBwNKaSAoVdd_89(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_HJPwxWlXiwCYDinWNJehoORKeaAG_90(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_osiGJAFTSlCIOMcRcChRIwdmebHj_91(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_crwevXVJojEkpHUxVTSVSqjIbrKg_92(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_TPAStIxrbeqxjYXsGDsZlhWCYmG_93(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_vrqVuHCBCDfnbTkILCuGeYDmBXRZ_94(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_GqXNxQsKQjHVuDfkVIXTcQvsrXTS_95(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_guWMOiNMigqDftPgOHjtkmbvIMah_96(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_lWdOljnpmaKTjiGpJhpWZDwuBbgA_97(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_sVlrrYXpkKVzsJCRWwGTyeXhRkm_98(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_mQFzRirpsuMkLakfKFaPDILeusGG_99(),
	DualShock4Driver_t398D14889F3FD6AA980F4FA18DB5CE39BE628D49::get_offset_of_RgRMMUDigxVtMdfhjtgBOIBeaHq_100(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4514 = { sizeof (dfGZrwvjtfUsZphIPrbTLQZNLZk_t5948285F74620B4781E1B062543C1AEB806E7329)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4514[4] = 
{
	dfGZrwvjtfUsZphIPrbTLQZNLZk_t5948285F74620B4781E1B062543C1AEB806E7329::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4515 = { sizeof (dnjPjQRXIqTAGDQohaBrWcsnlgz_tEAAA8E42BD0166A0B52A03C9394D9A67C9143C0E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4515[4] = 
{
	dnjPjQRXIqTAGDQohaBrWcsnlgz_tEAAA8E42BD0166A0B52A03C9394D9A67C9143C0E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4516 = { sizeof (RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4516[32] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688::get_offset_of_JVsgwlgmwsLVfxuQuuLMmdvVseuU_34(),
	RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688::get_offset_of_WgXlHLobIXEGVFDvcmIWrNRWIsj_35(),
	RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688::get_offset_of_eqENXoqEArTCUWjquEQYFDTxSOvW_36(),
	RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688::get_offset_of_FwNKSIMHOALacyXryDksxPdrVVl_37(),
	RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688::get_offset_of_ZKOHuQntYJaUOOJHSLzgGWFgqQF_38(),
	RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688::get_offset_of_jjxLdqHvqzvguEjvbDELBdomYEaU_39(),
	RailDriverDriver_tDEFCACA16E7E1EC1DC1AE413BE986B199EB3C688::get_offset_of_KrnGaZawVhHQVCvIixlFJZuYWiA_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4517 = { sizeof (hzMKpWAHmjTXmhpJeEaDSvuQvfn_tD0A3554765214F4827E150DD368FD87254B3B60C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4517[3] = 
{
	hzMKpWAHmjTXmhpJeEaDSvuQvfn_tD0A3554765214F4827E150DD368FD87254B3B60C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4518 = { sizeof (OutputReportOptions_t8F0DDE8BA800FA9FBE0634A7A5D64439C2BEE3CF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4518[4] = 
{
	OutputReportOptions_t8F0DDE8BA800FA9FBE0634A7A5D64439C2BEE3CF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4519 = { sizeof (DeviceConnectionType_t4A8B5BD929AC202D84804E92063E6F53C7ABDB25)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4519[4] = 
{
	DeviceConnectionType_t4A8B5BD929AC202D84804E92063E6F53C7ABDB25::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4520 = { sizeof (HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4520[9] = 
{
	HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220::get_offset_of_timestamp_3(),
	HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220::get_offset_of_lastRawValue_4(),
	HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220::get_offset_of_valueLength_5(),
	HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220::get_offset_of_NRwCWKPZMBPqSTcLtLnOurBUoWN_6(),
	HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220::get_offset_of_nfXJRuyZzhrVGDTDLHeNPPEfijfA_7(),
	HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220::get_offset_of_MXKCkdMwCxEJlCsqhAxVfcBDfLde_8(),
	HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220::get_offset_of_AfklhFSjQRdMZXJChQYWNazMHbq_9(),
	HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220::get_offset_of_agqHqDPvTEqPJElFDveRIhPWjClF_10(),
	HIDGyroscope_t1042E70E0081BB80264CF6FDA5E73C4F79250220::get_offset_of_jAVLkMiBPrMxvfdCjoUzCmsmHBY_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4521 = { sizeof (AOalgZclMUAOEImjGRYdmCrtRtkR_tD17EB85A0DF821B33D140C6E7AA46F2D60BD9AC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4521[2] = 
{
	AOalgZclMUAOEImjGRYdmCrtRtkR_tD17EB85A0DF821B33D140C6E7AA46F2D60BD9AC1::get_offset_of_LyLzjLOmwSaNWEHoBSRAyumGQSE_7(),
	AOalgZclMUAOEImjGRYdmCrtRtkR_tD17EB85A0DF821B33D140C6E7AA46F2D60BD9AC1::get_offset_of_NjyjytGcOPRyPyTXdTHZglaaOxF_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4522 = { sizeof (xNFFbHKeKmHrVYhgdzBoQgtgqxs_t1548DFA2A812084546F6CD0007C7C746DC655D26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4522[4] = 
{
	xNFFbHKeKmHrVYhgdzBoQgtgqxs_t1548DFA2A812084546F6CD0007C7C746DC655D26::get_offset_of_idzBDqZvmdhAhJjckuXPucOyrRds_1(),
	xNFFbHKeKmHrVYhgdzBoQgtgqxs_t1548DFA2A812084546F6CD0007C7C746DC655D26::get_offset_of_HrLhhDFKAQCiJIauDwTQdshRCqV_2(),
	xNFFbHKeKmHrVYhgdzBoQgtgqxs_t1548DFA2A812084546F6CD0007C7C746DC655D26::get_offset_of_psFuKOhcRHTESTSvwhokIWDxocu_3(),
	xNFFbHKeKmHrVYhgdzBoQgtgqxs_t1548DFA2A812084546F6CD0007C7C746DC655D26::get_offset_of_PCNLBpxMQWqHvwdbidXvESQeOqNr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4523 = { sizeof (hoiXDorKAcXnjheVDxCUSVqObdh_t2E4948874796211EAC2BEAF1E0D53EC71073A135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4523[2] = 
{
	hoiXDorKAcXnjheVDxCUSVqObdh_t2E4948874796211EAC2BEAF1E0D53EC71073A135::get_offset_of_HrLhhDFKAQCiJIauDwTQdshRCqV_0(),
	hoiXDorKAcXnjheVDxCUSVqObdh_t2E4948874796211EAC2BEAF1E0D53EC71073A135::get_offset_of_qKAHqFHSelUbirCSxInOiDPbgyyc_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4524 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4524[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4525 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4526 = { sizeof (HIDHat_t71BDF11807DD41A08195396005D5D890157791FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4526[6] = 
{
	HIDHat_t71BDF11807DD41A08195396005D5D890157791FF::get_offset_of_rawValue_2(),
	HIDHat_t71BDF11807DD41A08195396005D5D890157791FF::get_offset_of_timestamp_3(),
	HIDHat_t71BDF11807DD41A08195396005D5D890157791FF::get_offset_of_byteLength_4(),
	HIDHat_t71BDF11807DD41A08195396005D5D890157791FF::get_offset_of_startIndex_5(),
	HIDHat_t71BDF11807DD41A08195396005D5D890157791FF::get_offset_of_type_6(),
	HIDHat_t71BDF11807DD41A08195396005D5D890157791FF::get_offset_of_agqHqDPvTEqPJElFDveRIhPWjClF_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4527 = { sizeof (Type_t872FDC137DC2F491C6B457F36C652F61641ADFCF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4527[3] = 
{
	Type_t872FDC137DC2F491C6B457F36C652F61641ADFCF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4528 = { sizeof (HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4528[11] = 
{
	0,
	0,
	0,
	HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1::get_offset_of_YCIAjsdTgJBhSKEobujeDTVToefg_3(),
	HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1::get_offset_of_gWYcSreEUttuvqTHqKoRwtWQvHb_4(),
	HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1::get_offset_of_AcueBFpbGaWvgcHGsyajhWTFHb_5(),
	HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1::get_offset_of_yWQMhXTZSDJlTYAWMbZbErNHEywF_6(),
	HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1::get_offset_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_7(),
	HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1::get_offset_of_sAfeizOAktnwQKxVxmsSWsUMLvc_8(),
	HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1::get_offset_of_miTAQrjTIXETROKSlTwwkjkJlreA_9(),
	HidOutputReportHandler_tDA4D176FDDB65CC0B114E2EBF83635F58D1DE5C1::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4529 = { sizeof (WriteReportDelegate_tE0510A5AB2468221BF3ECDD4A176FE7F3A71706F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4530 = { sizeof (qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4530[4] = 
{
	qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2::get_offset_of_hBdFlAtFYUGadBUKnUCaSvYWLJA_0(),
	qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2::get_offset_of_BgwYOgUBehzXecfdAQjeyMPbNZG_1(),
	qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2::get_offset_of_gWYcSreEUttuvqTHqKoRwtWQvHb_2(),
	qQwWjhhBpjGwyXouxBiErDakyMc_t6DA2E79E0B0780669217400A6D117C18088D5EE2::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4531 = { sizeof (HIDLight_t660171518264B44E52EC67A0A99260D58A3EF792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4531[4] = 
{
	HIDLight_t660171518264B44E52EC67A0A99260D58A3EF792::get_offset_of_QCkHJtJyNQQdBeNwHiKkWzyBkcy_0(),
	HIDLight_t660171518264B44E52EC67A0A99260D58A3EF792::get_offset_of_cZtjlqxSiTJhueALMVKEKENfssc_1(),
	HIDLight_t660171518264B44E52EC67A0A99260D58A3EF792::get_offset_of_NIfzXOfUmcMwYJBnCegHlLvZrWm_2(),
	HIDLight_t660171518264B44E52EC67A0A99260D58A3EF792::get_offset_of_ylgqmkRBsacEGDjEnylhZJSqJLM_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4532 = { sizeof (SpecialDevices_t7FD926F80A8BFC55C5B0D654087F1E1969608160), -1, sizeof(SpecialDevices_t7FD926F80A8BFC55C5B0D654087F1E1969608160_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4532[2] = 
{
	0,
	SpecialDevices_t7FD926F80A8BFC55C5B0D654087F1E1969608160_StaticFields::get_offset_of_OiwDpoVrGMJDTSsgSenCIKLVmQA_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4533 = { sizeof (xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4533[8] = 
{
	xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69::get_offset_of_qNNdzuExLuNfrdyOzaNcsNVrpIQc_0(),
	xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69::get_offset_of_KclFaRHPVyrRHSfCxDIVgvgADJgN_1(),
	xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69::get_offset_of_hkAHAbBESJASJeGsvHpkANiUnas_2(),
	xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69::get_offset_of_vVbUoUJKTYYgJmwHgAyrHHnVNrB_3(),
	xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69::get_offset_of_GwkaAVhZdVBugFUdUPGxbbAwtRY_4(),
	xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69::get_offset_of_NrTqLQBJEpjtlDuUGPfGaBOGvpp_5(),
	xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69::get_offset_of_NmDDIiBdffXLHnURuiGdFYLeXJWi_6(),
	xgTeshlOrQNHJRhpOldNrbEuIDD_tF0D0E27B41450747E58631E8D6AEF6CC74733B69::get_offset_of_XRELSQboAmhkEqjogcpwBWhsogP_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4534 = { sizeof (HIDTouchpad_tB50BF65E87F43683093CD5B26D9ED251664532C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4534[5] = 
{
	HIDTouchpad_tB50BF65E87F43683093CD5B26D9ED251664532C0::get_offset_of_dyaUNLHBIeUikBzFJeaTDfszbXPG_2(),
	HIDTouchpad_tB50BF65E87F43683093CD5B26D9ED251664532C0::get_offset_of_nXTEALcSUETkLFyjMScGMCqzyDsh_3(),
	HIDTouchpad_tB50BF65E87F43683093CD5B26D9ED251664532C0::get_offset_of_QsxvkxJlxUAhMGXKYwoKQkbOURZ_4(),
	HIDTouchpad_tB50BF65E87F43683093CD5B26D9ED251664532C0::get_offset_of_agqHqDPvTEqPJElFDveRIhPWjClF_5(),
	HIDTouchpad_tB50BF65E87F43683093CD5B26D9ED251664532C0::get_offset_of_values_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4535 = { sizeof (TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4535[7] = 
{
	TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F::get_offset_of_maxTouches_0(),
	TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F::get_offset_of_minX_1(),
	TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F::get_offset_of_maxX_2(),
	TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F::get_offset_of_minY_3(),
	TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F::get_offset_of_maxY_4(),
	TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F::get_offset_of_invertY_5(),
	TouchpadInfo_tA3B53F0E0274E8C4CF836AB60D089EAEE6BBC75F::get_offset_of_reverseY_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4536 = { sizeof (TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7)+ sizeof (RuntimeObject), sizeof(TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4536[9] = 
{
	TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7::get_offset_of_touchId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7::get_offset_of_isTouching_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7::get_offset_of_positionRawX_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7::get_offset_of_positionRawY_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7::get_offset_of_positionX_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7::get_offset_of_positionY_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7::get_offset_of_positionAbsX_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchData_t385EDA338DB337DD4443411767E7AE83BDE51AA7::get_offset_of_positionAbsY_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4537 = { sizeof (SNWfMeAUUAAydzPHHxGGFlmYkig_t6CD465E16931B16F59BEC3F4D9308A7155AA8DA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4537[19] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4538 = { sizeof (HIDVibrationMotor_tD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4538[4] = 
{
	HIDVibrationMotor_tD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD::get_offset_of_RjVwQUrdIaflqLncuxqWYkoeksF_0(),
	HIDVibrationMotor_tD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD::get_offset_of_IkjGTcZHBbXyWdZhHrngxaAVUov_1(),
	HIDVibrationMotor_tD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD::get_offset_of_WPgfnXCmbruqQhVPUePGTNIbeIs_2(),
	HIDVibrationMotor_tD6DC9E10D59BEB04531EFDBB1AAC9822362B14DD::get_offset_of_ylgqmkRBsacEGDjEnylhZJSqJLM_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4539 = { sizeof (InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4539[15] = 
{
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of__dontDestroyOnLoad_4(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of__userData_5(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of__controllerDataFiles_6(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of_isCompiling_7(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of_initialized_8(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of_criticalError_9(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of_editorPlatform_10(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of_platform_11(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of_webplayerPlatform_12(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of_isEditor_13(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of__detectedPlatformInEditor_14(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of_scriptingBackend_15(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of_scriptingAPILevel_16(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of__duplicateRIMError_17(),
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701::get_offset_of__isAwake_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4540 = { sizeof (ExceptionPoint_t3DC2DA588E23852E8B0D31F5FF7073AE412F8CA5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4540[4] = 
{
	ExceptionPoint_t3DC2DA588E23852E8B0D31F5FF7073AE412F8CA5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4541 = { sizeof (SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4541[4] = 
{
	SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6::get_offset_of_HWmOewXpuebOsZDStBejWuBUGqK_0(),
	SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6::get_offset_of_YIgjjtvsNpmLAzEahZSayMqwKAO_1(),
	SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6::get_offset_of_OSuLPPJEiNhXcAilTdfZMoZXfznt_2(),
	SoMwFHWdfFmaNnPQuSGWlSuWHDi_tA293FB49E75154F1D97915E7039597E1022636F6::get_offset_of_YaDAnfimULzphKnSejtwEcfSlkEJ_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4542 = { sizeof (mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4542[4] = 
{
	mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F::get_offset_of_OZOUoJMKKtpUIlClzaTXojhyQdm_0(),
	mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F::get_offset_of_FqkHlxUUCNWHSlCnAhrViyWnBHqt_1(),
	mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F::get_offset_of_WvgzPICLZvrDlmflhcFctvIRNFi_2(),
	mtCbAGdYHzRqiPnOmbyDgpWTYHdl_t974A60D44D98143ACAA72A5ADBE24822BE21B10F::get_offset_of_OSuLPPJEiNhXcAilTdfZMoZXfznt_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4543 = { sizeof (MVPpNOKkKsBHVbOLknceWHXOeUB_tE0AD501DCDBE57A64499C912F0DA0DF46FA22C37), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4543[2] = 
{
	MVPpNOKkKsBHVbOLknceWHXOeUB_tE0AD501DCDBE57A64499C912F0DA0DF46FA22C37::get_offset_of_yIoGRcgIykirtuDoiviLGImmgkq_0(),
	MVPpNOKkKsBHVbOLknceWHXOeUB_tE0AD501DCDBE57A64499C912F0DA0DF46FA22C37::get_offset_of_jMKLVesCgaLYbTmUqCleosFlcphh_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4544 = { sizeof (yUPiWDfKBoDlPMYXQfAysIcWjmR_tB416753D72DE2ECA22B99511F5EC4BEDC77EA0C8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4544[3] = 
{
	yUPiWDfKBoDlPMYXQfAysIcWjmR_tB416753D72DE2ECA22B99511F5EC4BEDC77EA0C8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4545 = { sizeof (OnGUIHelper_t4041D6BAB19CDA8CFB4BDF4268CAC1A2E4774121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4545[1] = 
{
	OnGUIHelper_t4041D6BAB19CDA8CFB4BDF4268CAC1A2E4774121::get_offset_of_YsciKTkdsyeKqevEdLRABIojdVHx_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4546 = { sizeof (StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4546[12] = 
{
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E::get_offset_of__buttonActivationThreshold_0(),
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E::get_offset_of__calibration_1(),
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E::get_offset_of__valueRaw_2(),
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E::get_offset_of__valueRawPrev_3(),
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E::get_offset_of_KUlSPPdLfEKOBhsvlUicZMIWLRE_4(),
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E::get_offset_of_mGhyHlStIdvIiyOnXQeuafDvcjng_5(),
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E::get_offset_of_xKHiNcdbLaKdvcxFiGwrKuIRfUU_6(),
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E::get_offset_of_DEgkhrYBjiHUxeODZhwJYEMpWwu_7(),
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E::get_offset_of_PIDWGAUFGQIIdjKDIBekmKewfmzb_8(),
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E::get_offset_of_LiXxUKIllyDqWfhinmsMtlvlqxZ_9(),
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E::get_offset_of_WuAAKQyCOFjlKqFbRJifGpYdQfP_10(),
	StandaloneAxis_tA87F652134EED1F82C20E8D5492BAA7A03A84B0E::get_offset_of_SBrWOewdLfDOaaQtOSpeIoHHxxX_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4547 = { sizeof (AxisValueChangedEventHandler_t14D2E5A89F07B6992B6BA5F3EB66DDE1AC806237), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4548 = { sizeof (ButtonValueChangedEventHandler_tD16D16969B9F4D7D53C93DA6EBE2A5F4C8CDBBB5), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4549 = { sizeof (ButtonDownEventHandler_tDF0430A2FA1C7BD6D7815D2F3DD54D9CCE6118EE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4550 = { sizeof (ButtonUpEventHandler_t7D7868DC7B0024CD5CC804C9D9BA28C302222FB9), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4551 = { sizeof (StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4551[6] = 
{
	StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A::get_offset_of__calibration_0(),
	StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A::get_offset_of__xAxis_1(),
	StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A::get_offset_of__yAxis_2(),
	StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A::get_offset_of__allowEvents_3(),
	StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A::get_offset_of__ValueChangedEvent_4(),
	StandaloneAxis2D_t6B514C46E4E8BD60077ACF59F158467F2B1C942A::get_offset_of__RawValueChangedEvent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4552 = { sizeof (ValueChangedEventHandler_t4BAA2C9B4574E43316256DED3A2887B0BB6EC8D7), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4553 = { sizeof (SHhhwmrrEBdklqDsPHHCPGltOQH_t526B73E627059D9BB097766207D6D3B243833459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4554 = { sizeof (UnityInputHelper_t3D91912A1788E7F4F2971FA4E168665E00E04636), -1, sizeof(UnityInputHelper_t3D91912A1788E7F4F2971FA4E168665E00E04636_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4554[1] = 
{
	UnityInputHelper_t3D91912A1788E7F4F2971FA4E168665E00E04636_StaticFields::get_offset_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4555 = { sizeof (lYuJuZsYJJgZnLsElVIsjMUXGeV_t5FFDD188949D9675A8ECC4F400C5FA1A0F0CCEA9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4555[2] = 
{
	lYuJuZsYJJgZnLsElVIsjMUXGeV_t5FFDD188949D9675A8ECC4F400C5FA1A0F0CCEA9::get_offset_of_ojmYnkCHKzVQFExRGawmjxOAwip_0(),
	lYuJuZsYJJgZnLsElVIsjMUXGeV_t5FFDD188949D9675A8ECC4F400C5FA1A0F0CCEA9::get_offset_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4556 = { sizeof (UnityUnifiedKeyboardSource_t360607795D1E16597E86DCF369EA3EBB2C2C78DD), -1, sizeof(UnityUnifiedKeyboardSource_t360607795D1E16597E86DCF369EA3EBB2C2C78DD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4556[3] = 
{
	0,
	UnityUnifiedKeyboardSource_t360607795D1E16597E86DCF369EA3EBB2C2C78DD_StaticFields::get_offset_of_ZDxZczTtwYyKCbAxUkBLSReseUH_1(),
	UnityUnifiedKeyboardSource_t360607795D1E16597E86DCF369EA3EBB2C2C78DD::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4557 = { sizeof (UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5), -1, sizeof(UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4557[6] = 
{
	UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5_StaticFields::get_offset_of_ZDxZczTtwYyKCbAxUkBLSReseUH_0(),
	UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5::get_offset_of_PwPjYSFGGMniIMfHkZVcHaYxjMi_1(),
	UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5::get_offset_of_ITEosTYotauoYUkEMfXpdEGBpoIi_2(),
	UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5::get_offset_of_iVJAyfgBNrLbCgocuKyKOjvhJjT_3(),
	UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_4(),
	UnityUnifiedMouseSource_t190B342D704A3698587DE5CC8FDB3B8B4D4839C5_StaticFields::get_offset_of_wKZsRNpOhQctCfJnvfAdTetobSIx_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4558 = { sizeof (hJvPEYlJFiWOITXXSJHwonxNnUv_tCE253F5A620DDCC3B6E224B3CD0BD31047F9BB3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4558[2] = 
{
	hJvPEYlJFiWOITXXSJHwonxNnUv_tCE253F5A620DDCC3B6E224B3CD0BD31047F9BB3A::get_offset_of_ITEosTYotauoYUkEMfXpdEGBpoIi_0(),
	hJvPEYlJFiWOITXXSJHwonxNnUv_tCE253F5A620DDCC3B6E224B3CD0BD31047F9BB3A::get_offset_of_iVJAyfgBNrLbCgocuKyKOjvhJjT_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559 = { sizeof (iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B), -1, sizeof(iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4559[10] = 
{
	0,
	0,
	iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields::get_offset_of_NZUXTnMHDkwJgNxVMtXOIRsHDGN_2(),
	iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields::get_offset_of_PtVcZnVLLfsPRoQGwAAZntklgED_3(),
	iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields::get_offset_of_KXtDRnIjAQGAjYaGjSzcpROKhQZ_4(),
	iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B::get_offset_of_QVmtnTUxrGwnkEUluvTIVeAJedsI_5(),
	iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B::get_offset_of_HWmOewXpuebOsZDStBejWuBUGqK_6(),
	iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields::get_offset_of_xuKcWqHPyZZLojTegosyfuffNaVL_7(),
	iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields::get_offset_of_xgFcMzbcKwfNhKMwwFcokhPKEXIb_8(),
	iDabdpqyvlEAXmjzfkEiOCBPdMuc_t7E16D5BBE8507AE86374AD4C25BE551F8033B28B_StaticFields::get_offset_of_wzuTgIXkBigvyhwmUgzxbJRwaLDH_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560 = { sizeof (deXVkUCQurzFRSBNargAFcePsbp_t9FFAFDF5D8D521ECE2C69F4BE1B908875D25112E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4560[3] = 
{
	deXVkUCQurzFRSBNargAFcePsbp_t9FFAFDF5D8D521ECE2C69F4BE1B908875D25112E::get_offset_of_mvKHSWNtYxtrXAPVpClHLUktCzV_0(),
	deXVkUCQurzFRSBNargAFcePsbp_t9FFAFDF5D8D521ECE2C69F4BE1B908875D25112E::get_offset_of_GwxvhxNuDvVBGaKjmuSESUcbudE_1(),
	deXVkUCQurzFRSBNargAFcePsbp_t9FFAFDF5D8D521ECE2C69F4BE1B908875D25112E::get_offset_of_JjCmPMTQfjvEUCHkNBeRcjNBieD_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561 = { sizeof (yAEDWkzBznxclWFLPxuGoyUBKeT_t030E2C54B6EC34CD15754F1E9F912A67AB6E3EAB)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4561[6] = 
{
	yAEDWkzBznxclWFLPxuGoyUBKeT_t030E2C54B6EC34CD15754F1E9F912A67AB6E3EAB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562 = { sizeof (OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708), -1, sizeof(OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4562[36] = 
{
	0,
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_2(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_3(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_4(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_kNThXpJRuNjadiuBHlBgpclZYks_5(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_NSVrbXrLyVPttWtnsqehtpGnXCG_6(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields::get_offset_of_AEpGUwNwMAsWjLqSLSkygEagFsW_7(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields::get_offset_of_QUvyLwiKJQNzsCxNpgqsoOLwSsX_8(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields::get_offset_of_mRQSosFNYcuzgGZwdtMLlTBUFBV_9(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields::get_offset_of_DdAQuNXlVlFtzKHQSEFqDCRtRgt_10(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields::get_offset_of_oeJpjybqWWdBoUSIPTRbUtisjll_11(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields::get_offset_of_WuuCRjELljHtiCCsZMoeiRWBMSt_12(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_kQtPGxSDMqIDmNwBSXMjhzIdBbA_13(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_KwUMroYKNRAKXmVJWzcwAZRnqTx_14(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_aZqkxOCUfsonDTHNWausiXJBOvOu_15(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_vzFkOfiyuXiBznPLJddntTPCLHp_16(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_LbAAJeTOMjPeFMaCYBaygBzGqKCe_17(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_eupBpCkNZkzzzDHeCmeGOtfktiK_18(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_GxbsvPonBsCebHUvFmTUGSJKPNy_19(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_TkCeQwbtSOAqKaEpBqiGfOMtlHyh_20(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_phClTRYtbLMsxPaWqfWLCoiEcmlf_21(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_dSvBRDQTaBdKBeoGIRyCvWRfmAW_22(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_NZLeLHoYohvUVkNahXYNolJkMaF_23(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_KCcLZLFnCVvWttVAUFgzikonYgI_24(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_eJLELoqXznZJForjqQNdZWufUik_25(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_wettkZCPIyShlpqdnjaNbxAuAvkR_26(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_VvDPuKmCDZnivIDnrejTBCxXiIYZ_27(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_nEWeDBkhKnQxcpyoDgpAVMPOmHD_28(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_MWdTUcwbndAVXjzmXDzdiioSjos_29(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_lkENoKGchYvqwItosrqdPkQJuXx_30(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_YMEVpOuYpVjEtBldKIauEuQBazMc_31(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_OdSbXBLJoYyBYTxCowWEcLeROjZ_32(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_JjCmPMTQfjvEUCHkNBeRcjNBieD_33(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708::get_offset_of_oqVsjNRLeoYJHQGxOeiecQoOuNz_34(),
	OmePuLXqingOShAfdEjGZVdOnDpG_t2556EC33AB84FBD2176F62176E5F8A97FE7F8708_StaticFields::get_offset_of_PTHUuKTeYDyTLouJwwjwtdtVAGFd_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563 = { sizeof (cZaxobkPCTtFHFGFxSkpddMuaQs_t1317D0DE87A339014626829F75A1491BC984394C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4563[4] = 
{
	cZaxobkPCTtFHFGFxSkpddMuaQs_t1317D0DE87A339014626829F75A1491BC984394C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564 = { sizeof (WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4564[4] = 
{
	WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99::get_offset_of_XzwvNxyYkXcaiidxwJGvQNwBigT_0(),
	WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99::get_offset_of_bJGqRCGyLwMdbyXFtRqWyHanjhF_1(),
	WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99::get_offset_of_uiElHIMsJhOUGwxstGSvtQgdJHm_2(),
	WjDjjZjmTrLgvroIUzlAqRnRCmzJ_t8846D339F090A739301F627234D2FE3F00045B99::get_offset_of_YaDAnfimULzphKnSejtwEcfSlkEJ_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565 = { sizeof (LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4565[25] = 
{
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_xBYHLUKVdSEjOEPIkUktZiSGzVBE_0(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_kNThXpJRuNjadiuBHlBgpclZYks_1(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_zjYBUXbgxFkfVKHODHNCeWOHMfQc_2(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_CgFpjNWUaxudvYYjWafrhISXfsX_3(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_AzcaYoGJNjQgjFntSQDdTBEQrJJI_4(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_XJzgQnByCGDxnamkpUkReEJkbORp_5(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_UWLOvhrjuLGUzrhYiVuOYFsiVgW_6(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_jZuVxjpUtIHgWikNwxZtQcMRgGB_7(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_iMEYBpBPvCQGaPNoMfhjVvkHhrSb_8(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_pWXDxpuOcPfcTJvNeqHftHwISveU_9(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_nnqAvqEbNhGPWaKJhFKBxsKIdBiO_10(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_rGTlPXCBdVBtLOWjFDRxKopXiAE_11(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_ZIimzTsAoQjJCPHJabxdFjKEAjWE_12(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_EDtJGekJemucNcdKBHzoKxKvbHp_13(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_AOjNPhmpFfSsfgsHzQEuIbBWMmf_14(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_pNvLaxscwlwCkBrJwGJrJqUsuPi_15(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_dMgxPdNCqXlVlcUpUsDbXeutRij_16(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_xMJoTTkcCHywdQKtIpIJTZAzKDp_17(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_JPerCemNbwwhxjieMcolXzApwdg_18(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_UhxJMgpGhYKbuyqOMbfuYlAwhVMB_19(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_pyKYQGkOujslKcEyGgJjLcZosGz_20(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_zNndbbdmxoHBurCJgVAnUpeNSbuu_21(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_RJVzwOkzMvoymRuaJSGzPuRCILq_22(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_kdsPsXHldQcEYrcKmrJXiCQcZZU_23(),
	LBaYaJDRjCvogHekEOTUEmcDnEB_tE08793447A23877E83B61A730E12F94A0B11B25A::get_offset_of_CzufUZdOxzNVUAmhknXMtEeNCg_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566 = { sizeof (EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4566[3] = 
{
	EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94::get_offset_of_HWmOewXpuebOsZDStBejWuBUGqK_0(),
	EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94::get_offset_of_XTGIIIRFvRSEWTCNbDuWdIDniEr_1(),
	EAJRBzBYjjtwooLSURpauuDghJn_t985525D4ADBA589270E310CA763D8D21A2304C94::get_offset_of_WzMPJskYTcPesnNpifYmyBmyoWg_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567 = { sizeof (zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4567[3] = 
{
	zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5::get_offset_of_BcxgghPdhVrGYkkGtFRBBCNhtYPq_0(),
	zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5::get_offset_of_TlggNiFPzcPcGmHWHZntSPnyQaj_1(),
	zMiZeuUVPippzZoUlAuLXMyxVeJ_t1838478BC9B4D5D798C517D8DA250BF017B9FEB5::get_offset_of_uLaCafIsJgBVaUtOsrrFOcozUPA_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568 = { sizeof (nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4568[8] = 
{
	nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22::get_offset_of_sHcpZMJInjIQFVhudaFOLDXUgfvb_0(),
	nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22::get_offset_of_BNOJwGEydMJVXSuTJakmKeLIhTQ_1(),
	nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22::get_offset_of_ZCPKVMwlCoxJXBikDmhLmwDFMdF_2(),
	nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22::get_offset_of_BQwulTWiPxilhhBeLZrXjhMwunr_3(),
	nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22::get_offset_of_viKiGUPKnvKiBpsmKMiFyEiTesBf_4(),
	nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22::get_offset_of_PdOJVOhXFvBNcgTTFTRrdsGQtwc_5(),
	nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22::get_offset_of_nPxkyYJsDBYIqAAGFFPsCatPbPa_6(),
	nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22::get_offset_of_mBQzQCMXDIDEMnkhmuwuMBXYnWa_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569 = { sizeof (tFGjMRkSnzMdvjlUOfGbmVBOoWy_t5DC09B766606920055715F138215F62E731B9D61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4569[3] = 
{
	tFGjMRkSnzMdvjlUOfGbmVBOoWy_t5DC09B766606920055715F138215F62E731B9D61::get_offset_of_iexOpLWpDlgqvuDaErqqJlUOkHy_0(),
	tFGjMRkSnzMdvjlUOfGbmVBOoWy_t5DC09B766606920055715F138215F62E731B9D61::get_offset_of_MlHuhkxTPKDycqbMwTBkBIGihuU_1(),
	tFGjMRkSnzMdvjlUOfGbmVBOoWy_t5DC09B766606920055715F138215F62E731B9D61::get_offset_of_mmqqnntCpwCJvoDhRgRifrXtNvUc_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570 = { sizeof (lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4570[37] = 
{
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_QgoIWtHsaMGVeBFhqUvbmSmsiRh_0(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_rHKCUSVpCkqbhfZvJGgsrHqdAPs_1(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_dKRJCZQhWFihhgHVLBWkTcBEUEZ_2(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_tvaElgXLUKAHFDoRhflnYxhnyPI_3(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_bemErvveeaCaSZFVNunRQfuZPez_4(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_OSuLPPJEiNhXcAilTdfZMoZXfznt_5(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_vcFHuVHhWhJcpPiVLFZqonrrcV_6(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_bsdXVIEsRZlaTBlTrOAhsApnYKn_7(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_mCOQtFensPUoYxPTcAQQdFgYYSo_8(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_zaBpOeSvCieYmNRzYvTnztQTkri_9(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_OjQnvtvXVmEgxqUHpwsCCndFmCp_10(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_xbSkPOtNiebOSCgAhOovPqRqXwvm_11(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_IyEdcVHkuDkafEWHmxaqzzZeFqNW_12(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_HaAKNEMDBYyMhrIoUCHGzFFyJiI_13(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_xkHAaTRrzATFXVORsdQcGVcAyKu_14(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_pVgKJMsbUYqDnPgywSbnsKwnJSU_15(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_baGZLwAXmBfCREgsimWQSbLkIpIj_16(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_vwyzzodFvzaJuDLLaAMUycIZhjD_17(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_stygjDDRGdOuLuSiDJenZijkpLMI_18(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_YiuZEsEepQwFmUNRStSPyEzuABi_19(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_CbqXRwUReIvUcKUTjbTmSBpjpzO_20(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_plOMmYwRyscVwBsKeVnZUOnVRLL_21(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_uFBUjqesoWEDylgvLnqGzSXhYov_22(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_xUzeyjlHpwGUxEwdPtENmOvsKbtN_23(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_jrhqyYlRWCcRxAwNnKcXJyaAtcA_24(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_tpzVLnfsmVSJSCcIXipLHRtetoAv_25(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_OdOCHXeXDFufODzdKwToFSkQCsc_26(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_KisfUtAbFzkOvdyrnDCIAPLyGpMK_27(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_IEqlUZsiLAwwHwjaJiWawGshrHQ_28(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_fsKUnkudBKaclGoZoLGqLplnpcq_29(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_IREyufqHKXHZPEQHPEAqenDEAUFv_30(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_kSRoFZFcwKfkpCQvLgASZpiKDjWp_31(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_bbAisqwPdLehArffZbUYobsOVJk_32(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_dKOMDeTFhcAGvhGXFvxwMQTzTpa_33(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_kxPEmGNYGoPtVtPrSMMzAAUWENaG_34(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_pvjIVuFuziOtBNaRGTkkFsOlRDp_35(),
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571 = { sizeof (NAMaVbGyjTbiugbZjFqTWyaRoKSe_t0C0CA1B0F694D27E7C4C70B578E574A4BA792125)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4571[3] = 
{
	NAMaVbGyjTbiugbZjFqTWyaRoKSe_t0C0CA1B0F694D27E7C4C70B578E574A4BA792125::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572 = { sizeof (JLPbgmCslxcSncPKJybubLvESakA_tD2EB3276EC7B244F65D32F176710DDA90AEA3CC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4572[3] = 
{
	JLPbgmCslxcSncPKJybubLvESakA_tD2EB3276EC7B244F65D32F176710DDA90AEA3CC7::get_offset_of_mpQsQGNdoKAwJwXPzsVPtDdrMBq_0(),
	JLPbgmCslxcSncPKJybubLvESakA_tD2EB3276EC7B244F65D32F176710DDA90AEA3CC7::get_offset_of_UUQMotrrrdlXgpwOmqCtjHqhvxY_1(),
	JLPbgmCslxcSncPKJybubLvESakA_tD2EB3276EC7B244F65D32F176710DDA90AEA3CC7::get_offset_of_DDVHsbCpPQKwnQeEtRpjMMeFxLoN_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573 = { sizeof (kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4573[8] = 
{
	kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF::get_offset_of_iqBiBbmpNmwUcFGQZprtTGfIrcb_4(),
	kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF::get_offset_of_fsXsmfwBNAZfagSBJBZfiAnrlqch_5(),
	kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF::get_offset_of_HuWurlclvrHdMSaYmLaocIShGBdd_6(),
	kXCJrtbPieUBVYcpUDwOFJSwIwHC_tBF7DFF727132588BEBBFE526F8A87281EA1003CF::get_offset_of_vyUAWHyQzNBXmJvIdvpaOQOqBFQk_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574 = { sizeof (sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4574[8] = 
{
	sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C::get_offset_of_ygjTxdCscBQrKEzNUaPpemDmmZg_4(),
	sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C::get_offset_of_PcKcMsBLTsjDxArqCnLFFSqoJbI_5(),
	sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C::get_offset_of_nFbQIufzVdAoNuZfoBqCXfUUOKN_6(),
	sAnnMdWBsGBkNagSOnMteQVYEQU_tEB6E9B0777FE09C90D929B448A791778B8AB121C::get_offset_of_CXbAsCcpaRocwlxePSqDsZRxJTuA_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575 = { sizeof (CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4575[10] = 
{
	CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC::get_offset_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_5(),
	CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC::get_offset_of_GZolyQbnhDcIglfHmqdnClfPLwP_6(),
	CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC::get_offset_of_iswBkwCyDabLmfxdAYpLbXBWfvHN_7(),
	CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC::get_offset_of_YIgjjtvsNpmLAzEahZSayMqwKAO_8(),
	CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC::get_offset_of_nEiQhTKZDBYqGTkqzeEgrkzxQan_9(),
	CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC::get_offset_of_fVwZoOiZNlMKSCdQWxpuPNZKDrf_10(),
	CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC::get_offset_of_siaFEtqGznPqyDTNAcRWMeuzqae_11(),
	CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC::get_offset_of_cteekJgqyXKoMIQGdQUhjuJXMhSq_12(),
	CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC::get_offset_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_13(),
	CustomInputManager_tD00B53C595959EFFFF471392F87F7F4E794718DC::get_offset_of_ZGtOAcUCAuhJuSLEHTELVSEYNBN_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576 = { sizeof (OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4576[18] = 
{
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_lHdmDauerBwdDwLUcaCxBythtdWu_0(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_siaFEtqGznPqyDTNAcRWMeuzqae_1(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_keBYOCobzoABWhYylPhjApVSLXnN_2(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_avoCosfGauXluftrTLZWMUMjMSvo_3(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_vHhTUeEjpjXweHfcvokTPubqCBh_4(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_kNCtRZYNhfpgNEjWCvYWWaJdsPw_5(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_pVMgxVlhOWBbMBjIelvXUbEMRHTh_6(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_mMvnhxixERRYbVLPXEwyRrtXYwt_7(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_qTNEkibhoLvLepXYZmXNSItgadN_8(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_WSWhrfsXjaoUEREiHmegKVMtbnt_9(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_10(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_11(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_iKsRvufntavyFjWZbUibgspoftw_12(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_YuSoQESZTtPuhZKVJFBavCSGQsh_13(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_motlnrXwRclwbbdquGWkOEYsYFy_14(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_DLgnRYsKBieSednqoFxsgeQMfJE_15(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_16(),
	OSbOCXhMGybBoudblkgMAExKfJb_tE1A7AAAE2FE929AC0074EA9234DE5E5F8EF06E49::get_offset_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577 = { sizeof (igstrunnCuMHZFMjGQSDGcyfZnU_t8BA0D7526D539420345A84552152540A82348119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4577[1] = 
{
	igstrunnCuMHZFMjGQSDGcyfZnU_t8BA0D7526D539420345A84552152540A82348119::get_offset_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578 = { sizeof (awgCdigOnXUFUEptnriTXuUzGCk_t9E212A84A1064C41E8047F80A895BAE9D40EB46E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4578[3] = 
{
	awgCdigOnXUFUEptnriTXuUzGCk_t9E212A84A1064C41E8047F80A895BAE9D40EB46E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579 = { sizeof (SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4579[6] = 
{
	SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4::get_offset_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0(),
	SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4::get_offset_of_TbyfgvoLcvCnLzBZpOVXBdPAtwA_1(),
	SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4::get_offset_of_lXiIYlzqhDMJpyJyatjvSnwcuzH_2(),
	SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4::get_offset_of_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3(),
	SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4::get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_4(),
	SAPqXzqjGZlmsJaBMWgsmLJhEOu_t39A21FD1CED838703B16F0AF92AE1EC8C6CCDFA4::get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580 = { sizeof (DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4580[12] = 
{
	DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8::get_offset_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_5(),
	DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8::get_offset_of_GZolyQbnhDcIglfHmqdnClfPLwP_6(),
	DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8::get_offset_of_iswBkwCyDabLmfxdAYpLbXBWfvHN_7(),
	DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8::get_offset_of_cteekJgqyXKoMIQGdQUhjuJXMhSq_8(),
	DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8::get_offset_of_YIgjjtvsNpmLAzEahZSayMqwKAO_9(),
	DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8::get_offset_of_caGFUHzciIUISHXbwfgAhiHUvar_10(),
	DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8::get_offset_of_YJmfkQYaPrFhhpPpeaupYcLjhKSG_11(),
	DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8::get_offset_of_nEiQhTKZDBYqGTkqzeEgrkzxQan_12(),
	DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8::get_offset_of_fVwZoOiZNlMKSCdQWxpuPNZKDrf_13(),
	DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8::get_offset_of_dErtTfMMIryBKEuDkHRJfAiDoNC_14(),
	DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8::get_offset_of_VwrWeijqNdKIlgwMWGqaMIqSSZC_15(),
	DvbYkiORxIPlVcnxRmcWLbPwAdhB_t25691620CA1CA97FADA23D30620195A339F082A8::get_offset_of_ZhnvtkDwYNjiCyjHcDyrReRJeeR_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581 = { sizeof (jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4581[16] = 
{
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_avoCosfGauXluftrTLZWMUMjMSvo_0(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_vHhTUeEjpjXweHfcvokTPubqCBh_1(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_pVMgxVlhOWBbMBjIelvXUbEMRHTh_2(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_mMvnhxixERRYbVLPXEwyRrtXYwt_3(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_FNogqVfZDudFppBZSIvJRbuRKgJv_4(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_ynRWyQCwQkkFzvADNXNoLIVFuMQ_5(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_mOTUgFdbiXaaBraSubWzIyhlnpm_6(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_7(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_8(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_iKsRvufntavyFjWZbUibgspoftw_9(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_YuSoQESZTtPuhZKVJFBavCSGQsh_10(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_aXlEqhFZoFpFDaLNqEPWcHalryK_11(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_nhzyCKqFLpLsxhCUUDwhedVVBXIa_12(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_MVAyPoAtTKpsQbvLfEuJaoeeEfFH_13(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_motlnrXwRclwbbdquGWkOEYsYFy_14(),
	jzMnORPzOilIJZDTJTcDzOnMbEdi_t861B1435A56D8ADF47C2E099EBABBBD104B0E2B8::get_offset_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582 = { sizeof (sSSwLdgRIGhAHubEfzNAExVFpqE_tEC4C43E6DFB8DA01667D46FA8A9F981B8871D642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4582[1] = 
{
	sSSwLdgRIGhAHubEfzNAExVFpqE_tEC4C43E6DFB8DA01667D46FA8A9F981B8871D642::get_offset_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583 = { sizeof (rWYfLXYWrUBXvHKTQRhUWTjlfhC_tA9296F8C28B1445F23223BB190C79458BC389738)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4583[3] = 
{
	rWYfLXYWrUBXvHKTQRhUWTjlfhC_tA9296F8C28B1445F23223BB190C79458BC389738::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584 = { sizeof (bUptQGxKYmtFEzDAvGXbiJIbkczf_t6A0E7CFEFE93C50BF31EE4989A128A205B1015C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4584[4] = 
{
	bUptQGxKYmtFEzDAvGXbiJIbkczf_t6A0E7CFEFE93C50BF31EE4989A128A205B1015C1::get_offset_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0(),
	bUptQGxKYmtFEzDAvGXbiJIbkczf_t6A0E7CFEFE93C50BF31EE4989A128A205B1015C1::get_offset_of_ynRWyQCwQkkFzvADNXNoLIVFuMQ_1(),
	bUptQGxKYmtFEzDAvGXbiJIbkczf_t6A0E7CFEFE93C50BF31EE4989A128A205B1015C1::get_offset_of_lXiIYlzqhDMJpyJyatjvSnwcuzH_2(),
	bUptQGxKYmtFEzDAvGXbiJIbkczf_t6A0E7CFEFE93C50BF31EE4989A128A205B1015C1::get_offset_of_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585 = { sizeof (LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4585[9] = 
{
	LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA::get_offset_of_egbgDzhjndOTKCklEgspYpnNGmim_0(),
	LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA::get_offset_of_anPcJjMsMDSJwvtMEoZwvAUataJ_1(),
	LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA::get_offset_of_loWjoBuDbZXrdQdTsQFIrOmbiYA_2(),
	LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA::get_offset_of_PhOrdQVRimdzfuIpLkOsoIGzHIeD_3(),
	LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA::get_offset_of_IjcqxMjNVxBsZYkDWgLiYmCBJsi_4(),
	LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA::get_offset_of_EcJHmTtpwPjcbSwyMcKCsDWZObj_5(),
	LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA::get_offset_of_ZvHntwlAZZfXbFvKCBDFrhulswnd_6(),
	LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA::get_offset_of_bsdXVIEsRZlaTBlTrOAhsApnYKn_7(),
	LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA::get_offset_of_ffNWJUuxPxuFMIZszOmCdhFAkUp_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586 = { sizeof (ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7), -1, sizeof(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4586[19] = 
{
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of__actionCategoryId_0(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of__actionId_1(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of__elementType_2(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of__elementIdentifierId_3(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of__axisRange_4(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of__invert_5(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of__axisContribution_6(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of__keyboardKeyCode_7(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of__modifierKey1_8(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of__modifierKey2_9(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of__modifierKey3_10(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of_uNIXhFwhKcbRMaCTeDSCheRwjLdb_11(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_12(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of_BpnqUtABZEySqFzPQqIktIgZaUw_13(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of_EcYoSgaQzXASycflhjuFGZOdPsvy_14(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of_JcIOHtlyyDcisHJclHvGjyIzQHK_15(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7::get_offset_of_WHCIpuTfwdHNjfJipsoGkINikLt_16(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7_StaticFields::get_offset_of_uidCounter_17(),
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7_StaticFields::get_offset_of_s_toStringSB_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587 = { sizeof (CalibrationMapSaveData_tE4E68C169ADAF1B228EFF24A8A0301FBB3937C71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4587[3] = 
{
	CalibrationMapSaveData_tE4E68C169ADAF1B228EFF24A8A0301FBB3937C71::get_offset_of_kCIxZYCRZqQcvNwpCAQgMRtbDyH_0(),
	CalibrationMapSaveData_tE4E68C169ADAF1B228EFF24A8A0301FBB3937C71::get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1(),
	CalibrationMapSaveData_tE4E68C169ADAF1B228EFF24A8A0301FBB3937C71::get_offset_of_fGtUJMoDYGQBkIHcDCTnfmUopvdN_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588 = { sizeof (JoystickCalibrationMapSaveData_tEA7AF93E292FA30ADE52E7573A4A900AE05F095D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4588[1] = 
{
	JoystickCalibrationMapSaveData_tEA7AF93E292FA30ADE52E7573A4A900AE05F095D::get_offset_of_DqbGjcJsIYMblhndFwIlPVbyBLpR_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589 = { sizeof (CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4589[3] = 
{
	CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3::get_offset_of_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_0(),
	CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3::get_offset_of_AHeRqytYRGxoDmzkycBJFsLjKkW_1(),
	CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590 = { sizeof (AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4590[12] = 
{
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F::get_offset_of__calibrationMode_0(),
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F::get_offset_of__hardwareCalibrations_1(),
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F::get_offset_of__enabled_2(),
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F::get_offset_of__deadZone_3(),
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F::get_offset_of__calibratedZero_4(),
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F::get_offset_of__calibratedMin_5(),
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F::get_offset_of__calibratedMax_6(),
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F::get_offset_of__invert_7(),
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F::get_offset_of__sensitivityType_8(),
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F::get_offset_of__sensitivity_9(),
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F::get_offset_of__sensitivityCurve_10(),
	AxisCalibration_t46E34D338183B23046A06AD9D8A558F56EAD9E2F::get_offset_of__applyRangeCalibration_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591 = { sizeof (AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4591[11] = 
{
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171::get_offset_of_enabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171::get_offset_of_deadZone_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171::get_offset_of_zero_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171::get_offset_of_min_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171::get_offset_of_max_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171::get_offset_of_invert_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171::get_offset_of_sensitivityType_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171::get_offset_of_sensitivity_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171::get_offset_of_sensitivityCurve_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171::get_offset_of_applyRangeCalibration_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171::get_offset_of_calibrations_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592 = { sizeof (Axis2DCalibration_t4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4592[2] = 
{
	Axis2DCalibration_t4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1::get_offset_of__deadZoneType_0(),
	Axis2DCalibration_t4D1E47BBC054402D6B3A6FD728F4573A8B4FE0E1::get_offset_of__sensitivityType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593 = { sizeof (ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66), -1, sizeof(ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4593[9] = 
{
	ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66::get_offset_of__id_0(),
	ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66::get_offset_of__name_1(),
	ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66::get_offset_of__positiveName_2(),
	ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66::get_offset_of__negativeName_3(),
	ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66::get_offset_of__elementType_4(),
	ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66::get_offset_of__compoundElementType_5(),
	ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66::get_offset_of_isMappableOnPlatform_6(),
	ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66::get_offset_of_FiGYLzvVYjaKFXwFzEVgnkASbbH_7(),
	ControllerElementIdentifier_tFC073776F9B708D8AC266037F2C7BA1D3C747C66_StaticFields::get_offset_of_TfeYSmUqhkiytsLVYgfweLtJVdB_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594 = { sizeof (ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4594[6] = 
{
	ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40::get_offset_of__id_0(),
	ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40::get_offset_of__name_1(),
	ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40::get_offset_of__positiveName_2(),
	ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40::get_offset_of__negativeName_3(),
	ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40::get_offset_of__elementType_4(),
	ControllerTemplateElementIdentifier_tDC34C52C37F54EA7BFEFF9F304F9A47030DB2F40::get_offset_of_isMappableOnPlatform_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595 = { sizeof (ControllerTemplateElementIdentifier_Editor_t11EE1851F65BDA3CD3765D6129DBB482EE7ABC27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4595[5] = 
{
	ControllerTemplateElementIdentifier_Editor_t11EE1851F65BDA3CD3765D6129DBB482EE7ABC27::get_offset_of__scriptingName_6(),
	ControllerTemplateElementIdentifier_Editor_t11EE1851F65BDA3CD3765D6129DBB482EE7ABC27::get_offset_of__alternateScriptingName_7(),
	ControllerTemplateElementIdentifier_Editor_t11EE1851F65BDA3CD3765D6129DBB482EE7ABC27::get_offset_of__excludeFromExport_8(),
	ControllerTemplateElementIdentifier_Editor_t11EE1851F65BDA3CD3765D6129DBB482EE7ABC27::get_offset_of__useEditorElementTypeOverride_9(),
	ControllerTemplateElementIdentifier_Editor_t11EE1851F65BDA3CD3765D6129DBB482EE7ABC27::get_offset_of__editorElementTypeOverride_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596 = { sizeof (ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4596[5] = 
{
	ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D::get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_0(),
	ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D::get_offset_of_VZYtnnAqBdPNaSltSWGWGTTbFPN_1(),
	ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D::get_offset_of_HEDjmuguOtJTvuChkRgbhJaBjAU_2(),
	ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_3(),
	ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D::get_offset_of_MOUTEkpzvrLxSPrledaFXJEDTx_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597 = { sizeof (Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4597[7] = 
{
	Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB::get_offset_of__tag_0(),
	Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB::get_offset_of__enable_1(),
	Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB::get_offset_of__categoryIds_2(),
	Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB::get_offset_of__layoutIds_3(),
	Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB::get_offset_of__controllerSetSelector_4(),
	Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB::get_offset_of__preInitCategoryNames_5(),
	Rule_t80000D71FA6C3B46A6508B1EA019CFE2B70A8FCB::get_offset_of__preInitLayoutNames_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598 = { sizeof (RuleSet_tC0E87F2AC099155968FF378786CE9483F6B47E6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4598[4] = 
{
	0,
	RuleSet_tC0E87F2AC099155968FF378786CE9483F6B47E6C::get_offset_of__enabled_1(),
	RuleSet_tC0E87F2AC099155968FF378786CE9483F6B47E6C::get_offset_of__tag_2(),
	RuleSet_tC0E87F2AC099155968FF378786CE9483F6B47E6C::get_offset_of__rules_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599 = { sizeof (ixNifobzbtiwSHPyWPtxRGELpf_t6742005DEB9045D54B12821D97E544A8E0DAF484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4599[2] = 
{
	ixNifobzbtiwSHPyWPtxRGELpf_t6742005DEB9045D54B12821D97E544A8E0DAF484::get_offset_of_mgHlmSwiXWBcWiZgWIYuggfBYfpv_0(),
	ixNifobzbtiwSHPyWPtxRGELpf_t6742005DEB9045D54B12821D97E544A8E0DAF484::get_offset_of_kXPzYCvJwftDdckxbGhbBRgTrNKq_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
