﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// MS.Internal.Xml.Cache.XPathNode[]
struct XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B;
// MS.Internal.Xml.XPath.AstNode
struct AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C;
// MS.Internal.Xml.XPath.Axis
struct Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70;
// Microsoft.CSharp.CodeDomProvider
struct CodeDomProvider_t20C00DE7C8797892A59C1059898FB80842DB65F7;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>
struct Dictionary_2_t0F6B4307B0F9876BDCB2062F814BAC1D8ABA9646;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.Queue
struct Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3;
// System.Comparison`1<System.Xml.Serialization.XmlReflectionMember>
struct Comparison_1_t60DBA8E81BD9729FE925CE66DAF18BDE1233E76D;
// System.Decimal[]
struct DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61;
// System.Net.ICredentials
struct ICredentials_t1A41F1096B037CAB53AE01434DF0747881455344;
// System.Net.IWebProxy
struct IWebProxy_tA24C0862A1ACA35D20FD079E2672CA5786C1A67E;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t63EC46F14F048DC9EF6BF1362E8AEBEA1A05A5EA;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tEFDFBE18E061A6065AB2FF735F1425FB59F919BC;
// System.Runtime.Serialization.ObjectIDGenerator
struct ObjectIDGenerator_tD65209D0A1CDA08C9C709A4D1997D8091A93C1A1;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.Tasks.Task`1<System.IO.Stream>
struct Task_1_tAA7557E5A6A8B2C89E44907CB5B0502A618E9D01;
// System.Threading.Tasks.Task`1<System.Object>
struct Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09;
// System.Type
struct Type_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.IValidationEventHandling
struct IValidationEventHandling_tABD39B6B973C0A0DC259D55D8C4179A43ACAB41B;
// System.Xml.PositionInfo
struct PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684;
// System.Xml.Schema.ActiveAxis
struct ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E;
// System.Xml.Schema.Asttree
struct Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA;
// System.Xml.Schema.Asttree[]
struct AsttreeU5BU5D_tE98D3D56BDC1A5B380B40C34285F212D51BF37D3;
// System.Xml.Schema.CompiledIdentityConstraint
struct CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E;
// System.Xml.Schema.ConstraintStruct
struct ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54;
// System.Xml.Schema.DoubleLinkAxis
struct DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955;
// System.Xml.Schema.ForwardAxis
struct ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253;
// System.Xml.Schema.KeySequence
struct KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE;
// System.Xml.Schema.LocatedActiveAxis[]
struct LocatedActiveAxisU5BU5D_t4148EC17C851E170B819098896BDD191CDDC41D1;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89;
// System.Xml.Schema.SelectorActiveAxis
struct SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64;
// System.Xml.Schema.TypedObject/DecimalStruct
struct DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11;
// System.Xml.Schema.TypedObject[]
struct TypedObjectU5BU5D_t5F58D2C06E283EF28F0674C78DEB47DA64DF9CE1;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF;
// System.Xml.Schema.ValidationState
struct ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B;
// System.Xml.Schema.XmlSchema
struct XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5;
// System.Xml.Schema.XmlSchemaCompilationSettings
struct XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC;
// System.Xml.Schema.XmlSchemaComplexType
struct XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550;
// System.Xml.Schema.XmlSchemaPatternFacet
struct XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322;
// System.Xml.Serialization.EnumMap/EnumMapMember[]
struct EnumMapMemberU5BU5D_t65FB66969212223ED79971B0473634E03D6C1B2E;
// System.Xml.Serialization.ListMap
struct ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742;
// System.Xml.Serialization.ObjectMap
struct ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677;
// System.Xml.Serialization.ReflectionHelper
struct ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6;
// System.Xml.Serialization.TypeData
struct TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94;
// System.Xml.Serialization.XmlAnyAttributeAttribute
struct XmlAnyAttributeAttribute_tBB53254892DFE4AB748B01108539540EC451C680;
// System.Xml.Serialization.XmlAnyElementAttributes
struct XmlAnyElementAttributes_tED6F51ECCCB48BBB4C5E4D826A7E5CF341F33160;
// System.Xml.Serialization.XmlArrayAttribute
struct XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19;
// System.Xml.Serialization.XmlArrayItemAttributes
struct XmlArrayItemAttributes_t7BFEEA6B880F1F3FC7DF8EB92C33E65C56FA0A49;
// System.Xml.Serialization.XmlAttributeAttribute
struct XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E;
// System.Xml.Serialization.XmlAttributeOverrides
struct XmlAttributeOverrides_tE2AE30774D1F8B3860BFE1A8B6233970E88C7F7E;
// System.Xml.Serialization.XmlAttributes
struct XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A;
// System.Xml.Serialization.XmlChoiceIdentifierAttribute
struct XmlChoiceIdentifierAttribute_t67E5AE7310D779F7533770C3D3F7CAA65A5E6B8B;
// System.Xml.Serialization.XmlElementAttributes
struct XmlElementAttributes_t3F224B71C1BC11E8EF6B4E8E45A940B394FA6D34;
// System.Xml.Serialization.XmlEnumAttribute
struct XmlEnumAttribute_tBD75888B09FE971438F4FA55FC302BC5FA3936F0;
// System.Xml.Serialization.XmlMapping
struct XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726;
// System.Xml.Serialization.XmlRootAttribute
struct XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C;
// System.Xml.Serialization.XmlSerializationWriteCallback
struct XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA;
// System.Xml.Serialization.XmlSerializationWriterInterpreter
struct XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221;
// System.Xml.Serialization.XmlSerializer/SerializerData
struct SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8;
// System.Xml.Serialization.XmlSerializerImplementation
struct XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D;
// System.Xml.Serialization.XmlTextAttribute
struct XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7;
// System.Xml.Serialization.XmlTypeAttribute
struct XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08;
// System.Xml.Serialization.XmlTypeMapElementInfoList
struct XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0;
// System.Xml.Serialization.XmlTypeMapMember
struct XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D;
// System.Xml.Serialization.XmlTypeMapMemberAnyAttribute
struct XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE;
// System.Xml.Serialization.XmlTypeMapMemberAnyElement
struct XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472;
// System.Xml.Serialization.XmlTypeMapMemberAttribute[]
struct XmlTypeMapMemberAttributeU5BU5D_t1A3EF531DFF7CF757326012C417167F79DDE0D10;
// System.Xml.Serialization.XmlTypeMapMemberNamespaces
struct XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43;
// System.Xml.Serialization.XmlTypeMapping
struct XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3;
// System.Xml.XPath.XPathNavigatorKeyComparer
struct XPathNavigatorKeyComparer_t6A0E82BEC0BE42351DDB26EAA86333C11E0A9378;
// System.Xml.XmlNameTable
struct XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD;
// System.Xml.XmlResolver
struct XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280;
// System.Xml.XmlUrlResolver
struct XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9;
// System.Xml.XmlValidatingReaderImpl
struct XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E;
// System.Xml.XmlWriter
struct XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASTNODE_TEB8196548F948D593E8705AE90AECD4B02D2D93C_H
#define ASTNODE_TEB8196548F948D593E8705AE90AECD4B02D2D93C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.AstNode
struct  AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTNODE_TEB8196548F948D593E8705AE90AECD4B02D2D93C_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef ARRAYLIST_T4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4_H
#define ARRAYLIST_T4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ArrayList
struct  ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4  : public RuntimeObject
{
public:
	// System.Object[] System.Collections.ArrayList::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_0;
	// System.Int32 System.Collections.ArrayList::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.ArrayList::_version
	int32_t ____version_2;
	// System.Object System.Collections.ArrayList::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__items_0() { return static_cast<int32_t>(offsetof(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4, ____items_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_0() const { return ____items_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_0() { return &____items_0; }
	inline void set__items_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_0 = value;
		Il2CppCodeGenWriteBarrier((&____items_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_3), value);
	}
};

struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4_StaticFields
{
public:
	// System.Object[] System.Collections.ArrayList::emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___emptyArray_4;

public:
	inline static int32_t get_offset_of_emptyArray_4() { return static_cast<int32_t>(offsetof(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4_StaticFields, ___emptyArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_emptyArray_4() const { return ___emptyArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_emptyArray_4() { return &___emptyArray_4; }
	inline void set_emptyArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___emptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___emptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYLIST_T4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4_H
#ifndef COLLECTIONBASE_TF5D4583FF325726066A9803839A04E9C0084ED01_H
#define COLLECTIONBASE_TF5D4583FF325726066A9803839A04E9C0084ED01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_tF5D4583FF325726066A9803839A04E9C0084ED01  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_tF5D4583FF325726066A9803839A04E9C0084ED01, ___list_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_list_0() const { return ___list_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_TF5D4583FF325726066A9803839A04E9C0084ED01_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef RES_TAEAB44EC8659FC8A5C53E8AB74EA846A640AD03A_H
#define RES_TAEAB44EC8659FC8A5C53E8AB74EA846A640AD03A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Res
struct  Res_tAEAB44EC8659FC8A5C53E8AB74EA846A640AD03A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RES_TAEAB44EC8659FC8A5C53E8AB74EA846A640AD03A_H
#ifndef ACTIVEAXIS_TD7958CF29FECEF35D850FA29624E57EF040E315E_H
#define ACTIVEAXIS_TD7958CF29FECEF35D850FA29624E57EF040E315E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ActiveAxis
struct  ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.ActiveAxis::currentDepth
	int32_t ___currentDepth_0;
	// System.Boolean System.Xml.Schema.ActiveAxis::isActive
	bool ___isActive_1;
	// System.Xml.Schema.Asttree System.Xml.Schema.ActiveAxis::axisTree
	Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA * ___axisTree_2;
	// System.Collections.ArrayList System.Xml.Schema.ActiveAxis::axisStack
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___axisStack_3;

public:
	inline static int32_t get_offset_of_currentDepth_0() { return static_cast<int32_t>(offsetof(ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E, ___currentDepth_0)); }
	inline int32_t get_currentDepth_0() const { return ___currentDepth_0; }
	inline int32_t* get_address_of_currentDepth_0() { return &___currentDepth_0; }
	inline void set_currentDepth_0(int32_t value)
	{
		___currentDepth_0 = value;
	}

	inline static int32_t get_offset_of_isActive_1() { return static_cast<int32_t>(offsetof(ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E, ___isActive_1)); }
	inline bool get_isActive_1() const { return ___isActive_1; }
	inline bool* get_address_of_isActive_1() { return &___isActive_1; }
	inline void set_isActive_1(bool value)
	{
		___isActive_1 = value;
	}

	inline static int32_t get_offset_of_axisTree_2() { return static_cast<int32_t>(offsetof(ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E, ___axisTree_2)); }
	inline Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA * get_axisTree_2() const { return ___axisTree_2; }
	inline Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA ** get_address_of_axisTree_2() { return &___axisTree_2; }
	inline void set_axisTree_2(Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA * value)
	{
		___axisTree_2 = value;
		Il2CppCodeGenWriteBarrier((&___axisTree_2), value);
	}

	inline static int32_t get_offset_of_axisStack_3() { return static_cast<int32_t>(offsetof(ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E, ___axisStack_3)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_axisStack_3() const { return ___axisStack_3; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_axisStack_3() { return &___axisStack_3; }
	inline void set_axisStack_3(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___axisStack_3 = value;
		Il2CppCodeGenWriteBarrier((&___axisStack_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEAXIS_TD7958CF29FECEF35D850FA29624E57EF040E315E_H
#ifndef ASTTREE_T9D8B0988F0376119C895FC560A1A81FB9C452FCA_H
#define ASTTREE_T9D8B0988F0376119C895FC560A1A81FB9C452FCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Asttree
struct  Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Xml.Schema.Asttree::fAxisArray
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___fAxisArray_0;
	// System.String System.Xml.Schema.Asttree::xpathexpr
	String_t* ___xpathexpr_1;
	// System.Boolean System.Xml.Schema.Asttree::isField
	bool ___isField_2;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.Asttree::nsmgr
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ___nsmgr_3;

public:
	inline static int32_t get_offset_of_fAxisArray_0() { return static_cast<int32_t>(offsetof(Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA, ___fAxisArray_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_fAxisArray_0() const { return ___fAxisArray_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_fAxisArray_0() { return &___fAxisArray_0; }
	inline void set_fAxisArray_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___fAxisArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___fAxisArray_0), value);
	}

	inline static int32_t get_offset_of_xpathexpr_1() { return static_cast<int32_t>(offsetof(Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA, ___xpathexpr_1)); }
	inline String_t* get_xpathexpr_1() const { return ___xpathexpr_1; }
	inline String_t** get_address_of_xpathexpr_1() { return &___xpathexpr_1; }
	inline void set_xpathexpr_1(String_t* value)
	{
		___xpathexpr_1 = value;
		Il2CppCodeGenWriteBarrier((&___xpathexpr_1), value);
	}

	inline static int32_t get_offset_of_isField_2() { return static_cast<int32_t>(offsetof(Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA, ___isField_2)); }
	inline bool get_isField_2() const { return ___isField_2; }
	inline bool* get_address_of_isField_2() { return &___isField_2; }
	inline void set_isField_2(bool value)
	{
		___isField_2 = value;
	}

	inline static int32_t get_offset_of_nsmgr_3() { return static_cast<int32_t>(offsetof(Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA, ___nsmgr_3)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get_nsmgr_3() const { return ___nsmgr_3; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of_nsmgr_3() { return &___nsmgr_3; }
	inline void set_nsmgr_3(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		___nsmgr_3 = value;
		Il2CppCodeGenWriteBarrier((&___nsmgr_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTTREE_T9D8B0988F0376119C895FC560A1A81FB9C452FCA_H
#ifndef AXISELEMENT_T8DC31C9834DF2F50E9E4D245D07C1AE68F226D80_H
#define AXISELEMENT_T8DC31C9834DF2F50E9E4D245D07C1AE68F226D80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AxisElement
struct  AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80  : public RuntimeObject
{
public:
	// System.Xml.Schema.DoubleLinkAxis System.Xml.Schema.AxisElement::curNode
	DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * ___curNode_0;
	// System.Int32 System.Xml.Schema.AxisElement::rootDepth
	int32_t ___rootDepth_1;
	// System.Int32 System.Xml.Schema.AxisElement::curDepth
	int32_t ___curDepth_2;
	// System.Boolean System.Xml.Schema.AxisElement::isMatch
	bool ___isMatch_3;

public:
	inline static int32_t get_offset_of_curNode_0() { return static_cast<int32_t>(offsetof(AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80, ___curNode_0)); }
	inline DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * get_curNode_0() const { return ___curNode_0; }
	inline DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 ** get_address_of_curNode_0() { return &___curNode_0; }
	inline void set_curNode_0(DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * value)
	{
		___curNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___curNode_0), value);
	}

	inline static int32_t get_offset_of_rootDepth_1() { return static_cast<int32_t>(offsetof(AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80, ___rootDepth_1)); }
	inline int32_t get_rootDepth_1() const { return ___rootDepth_1; }
	inline int32_t* get_address_of_rootDepth_1() { return &___rootDepth_1; }
	inline void set_rootDepth_1(int32_t value)
	{
		___rootDepth_1 = value;
	}

	inline static int32_t get_offset_of_curDepth_2() { return static_cast<int32_t>(offsetof(AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80, ___curDepth_2)); }
	inline int32_t get_curDepth_2() const { return ___curDepth_2; }
	inline int32_t* get_address_of_curDepth_2() { return &___curDepth_2; }
	inline void set_curDepth_2(int32_t value)
	{
		___curDepth_2 = value;
	}

	inline static int32_t get_offset_of_isMatch_3() { return static_cast<int32_t>(offsetof(AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80, ___isMatch_3)); }
	inline bool get_isMatch_3() const { return ___isMatch_3; }
	inline bool* get_address_of_isMatch_3() { return &___isMatch_3; }
	inline void set_isMatch_3(bool value)
	{
		___isMatch_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISELEMENT_T8DC31C9834DF2F50E9E4D245D07C1AE68F226D80_H
#ifndef AXISSTACK_T3AA7511C349203BE08B1ECF76EA6C0031534C4A2_H
#define AXISSTACK_T3AA7511C349203BE08B1ECF76EA6C0031534C4A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AxisStack
struct  AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Xml.Schema.AxisStack::stack
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___stack_0;
	// System.Xml.Schema.ForwardAxis System.Xml.Schema.AxisStack::subtree
	ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253 * ___subtree_1;
	// System.Xml.Schema.ActiveAxis System.Xml.Schema.AxisStack::parent
	ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E * ___parent_2;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2, ___stack_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_stack_0() const { return ___stack_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___stack_0), value);
	}

	inline static int32_t get_offset_of_subtree_1() { return static_cast<int32_t>(offsetof(AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2, ___subtree_1)); }
	inline ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253 * get_subtree_1() const { return ___subtree_1; }
	inline ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253 ** get_address_of_subtree_1() { return &___subtree_1; }
	inline void set_subtree_1(ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253 * value)
	{
		___subtree_1 = value;
		Il2CppCodeGenWriteBarrier((&___subtree_1), value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2, ___parent_2)); }
	inline ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E * get_parent_2() const { return ___parent_2; }
	inline ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___parent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSTACK_T3AA7511C349203BE08B1ECF76EA6C0031534C4A2_H
#ifndef BASEPROCESSOR_T68F1D18B3D8A79746118A87DF029D5FA8E90D427_H
#define BASEPROCESSOR_T68F1D18B3D8A79746118A87DF029D5FA8E90D427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseProcessor
struct  BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.BaseProcessor::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_0;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseProcessor::schemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ___schemaNames_1;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.BaseProcessor::eventHandler
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___eventHandler_2;
	// System.Xml.Schema.XmlSchemaCompilationSettings System.Xml.Schema.BaseProcessor::compilationSettings
	XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC * ___compilationSettings_3;
	// System.Int32 System.Xml.Schema.BaseProcessor::errorCount
	int32_t ___errorCount_4;
	// System.String System.Xml.Schema.BaseProcessor::NsXml
	String_t* ___NsXml_5;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___nameTable_0)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_0), value);
	}

	inline static int32_t get_offset_of_schemaNames_1() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___schemaNames_1)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get_schemaNames_1() const { return ___schemaNames_1; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of_schemaNames_1() { return &___schemaNames_1; }
	inline void set_schemaNames_1(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		___schemaNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_1), value);
	}

	inline static int32_t get_offset_of_eventHandler_2() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___eventHandler_2)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_eventHandler_2() const { return ___eventHandler_2; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_eventHandler_2() { return &___eventHandler_2; }
	inline void set_eventHandler_2(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___eventHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandler_2), value);
	}

	inline static int32_t get_offset_of_compilationSettings_3() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___compilationSettings_3)); }
	inline XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC * get_compilationSettings_3() const { return ___compilationSettings_3; }
	inline XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC ** get_address_of_compilationSettings_3() { return &___compilationSettings_3; }
	inline void set_compilationSettings_3(XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC * value)
	{
		___compilationSettings_3 = value;
		Il2CppCodeGenWriteBarrier((&___compilationSettings_3), value);
	}

	inline static int32_t get_offset_of_errorCount_4() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___errorCount_4)); }
	inline int32_t get_errorCount_4() const { return ___errorCount_4; }
	inline int32_t* get_address_of_errorCount_4() { return &___errorCount_4; }
	inline void set_errorCount_4(int32_t value)
	{
		___errorCount_4 = value;
	}

	inline static int32_t get_offset_of_NsXml_5() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___NsXml_5)); }
	inline String_t* get_NsXml_5() const { return ___NsXml_5; }
	inline String_t** get_address_of_NsXml_5() { return &___NsXml_5; }
	inline void set_NsXml_5(String_t* value)
	{
		___NsXml_5 = value;
		Il2CppCodeGenWriteBarrier((&___NsXml_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEPROCESSOR_T68F1D18B3D8A79746118A87DF029D5FA8E90D427_H
#ifndef BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#define BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseValidator
struct  BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaCollection System.Xml.Schema.BaseValidator::schemaCollection
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * ___schemaCollection_0;
	// System.Xml.IValidationEventHandling System.Xml.Schema.BaseValidator::eventHandling
	RuntimeObject* ___eventHandling_1;
	// System.Xml.XmlNameTable System.Xml.Schema.BaseValidator::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_2;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseValidator::schemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ___schemaNames_3;
	// System.Xml.PositionInfo System.Xml.Schema.BaseValidator::positionInfo
	PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * ___positionInfo_4;
	// System.Xml.XmlResolver System.Xml.Schema.BaseValidator::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_5;
	// System.Uri System.Xml.Schema.BaseValidator::baseUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_6;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.BaseValidator::schemaInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___schemaInfo_7;
	// System.Xml.XmlValidatingReaderImpl System.Xml.Schema.BaseValidator::reader
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * ___reader_8;
	// System.Xml.XmlQualifiedName System.Xml.Schema.BaseValidator::elementName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___elementName_9;
	// System.Xml.Schema.ValidationState System.Xml.Schema.BaseValidator::context
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * ___context_10;
	// System.Text.StringBuilder System.Xml.Schema.BaseValidator::textValue
	StringBuilder_t * ___textValue_11;
	// System.String System.Xml.Schema.BaseValidator::textString
	String_t* ___textString_12;
	// System.Boolean System.Xml.Schema.BaseValidator::hasSibling
	bool ___hasSibling_13;
	// System.Boolean System.Xml.Schema.BaseValidator::checkDatatype
	bool ___checkDatatype_14;

public:
	inline static int32_t get_offset_of_schemaCollection_0() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaCollection_0)); }
	inline XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * get_schemaCollection_0() const { return ___schemaCollection_0; }
	inline XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 ** get_address_of_schemaCollection_0() { return &___schemaCollection_0; }
	inline void set_schemaCollection_0(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * value)
	{
		___schemaCollection_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaCollection_0), value);
	}

	inline static int32_t get_offset_of_eventHandling_1() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___eventHandling_1)); }
	inline RuntimeObject* get_eventHandling_1() const { return ___eventHandling_1; }
	inline RuntimeObject** get_address_of_eventHandling_1() { return &___eventHandling_1; }
	inline void set_eventHandling_1(RuntimeObject* value)
	{
		___eventHandling_1 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandling_1), value);
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___nameTable_2)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_schemaNames_3() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaNames_3)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get_schemaNames_3() const { return ___schemaNames_3; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of_schemaNames_3() { return &___schemaNames_3; }
	inline void set_schemaNames_3(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		___schemaNames_3 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_3), value);
	}

	inline static int32_t get_offset_of_positionInfo_4() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___positionInfo_4)); }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * get_positionInfo_4() const { return ___positionInfo_4; }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 ** get_address_of_positionInfo_4() { return &___positionInfo_4; }
	inline void set_positionInfo_4(PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * value)
	{
		___positionInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_4), value);
	}

	inline static int32_t get_offset_of_xmlResolver_5() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___xmlResolver_5)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_5() const { return ___xmlResolver_5; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_5() { return &___xmlResolver_5; }
	inline void set_xmlResolver_5(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_5), value);
	}

	inline static int32_t get_offset_of_baseUri_6() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___baseUri_6)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_baseUri_6() const { return ___baseUri_6; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_baseUri_6() { return &___baseUri_6; }
	inline void set_baseUri_6(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___baseUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_6), value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaInfo_7)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_7), value);
	}

	inline static int32_t get_offset_of_reader_8() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___reader_8)); }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * get_reader_8() const { return ___reader_8; }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E ** get_address_of_reader_8() { return &___reader_8; }
	inline void set_reader_8(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * value)
	{
		___reader_8 = value;
		Il2CppCodeGenWriteBarrier((&___reader_8), value);
	}

	inline static int32_t get_offset_of_elementName_9() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___elementName_9)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_elementName_9() const { return ___elementName_9; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_elementName_9() { return &___elementName_9; }
	inline void set_elementName_9(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___elementName_9 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_9), value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___context_10)); }
	inline ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * get_context_10() const { return ___context_10; }
	inline ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_textValue_11() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___textValue_11)); }
	inline StringBuilder_t * get_textValue_11() const { return ___textValue_11; }
	inline StringBuilder_t ** get_address_of_textValue_11() { return &___textValue_11; }
	inline void set_textValue_11(StringBuilder_t * value)
	{
		___textValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___textValue_11), value);
	}

	inline static int32_t get_offset_of_textString_12() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___textString_12)); }
	inline String_t* get_textString_12() const { return ___textString_12; }
	inline String_t** get_address_of_textString_12() { return &___textString_12; }
	inline void set_textString_12(String_t* value)
	{
		___textString_12 = value;
		Il2CppCodeGenWriteBarrier((&___textString_12), value);
	}

	inline static int32_t get_offset_of_hasSibling_13() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___hasSibling_13)); }
	inline bool get_hasSibling_13() const { return ___hasSibling_13; }
	inline bool* get_address_of_hasSibling_13() { return &___hasSibling_13; }
	inline void set_hasSibling_13(bool value)
	{
		___hasSibling_13 = value;
	}

	inline static int32_t get_offset_of_checkDatatype_14() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___checkDatatype_14)); }
	inline bool get_checkDatatype_14() const { return ___checkDatatype_14; }
	inline bool* get_address_of_checkDatatype_14() { return &___checkDatatype_14; }
	inline void set_checkDatatype_14(bool value)
	{
		___checkDatatype_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#ifndef BITSET_T0E4C53EC600670A4B74C5671553596978880138C_H
#define BITSET_T0E4C53EC600670A4B74C5671553596978880138C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BitSet
struct  BitSet_t0E4C53EC600670A4B74C5671553596978880138C  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.BitSet::count
	int32_t ___count_0;
	// System.UInt32[] System.Xml.Schema.BitSet::bits
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___bits_1;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(BitSet_t0E4C53EC600670A4B74C5671553596978880138C, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_bits_1() { return static_cast<int32_t>(offsetof(BitSet_t0E4C53EC600670A4B74C5671553596978880138C, ___bits_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_bits_1() const { return ___bits_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_bits_1() { return &___bits_1; }
	inline void set_bits_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___bits_1 = value;
		Il2CppCodeGenWriteBarrier((&___bits_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITSET_T0E4C53EC600670A4B74C5671553596978880138C_H
#ifndef CHAMELEONKEY_TB2DC0F8044027C691C7325267CB62C0BBDB1CB2D_H
#define CHAMELEONKEY_TB2DC0F8044027C691C7325267CB62C0BBDB1CB2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ChameleonKey
struct  ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D  : public RuntimeObject
{
public:
	// System.String System.Xml.Schema.ChameleonKey::targetNS
	String_t* ___targetNS_0;
	// System.Uri System.Xml.Schema.ChameleonKey::chameleonLocation
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___chameleonLocation_1;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.ChameleonKey::originalSchema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___originalSchema_2;
	// System.Int32 System.Xml.Schema.ChameleonKey::hashCode
	int32_t ___hashCode_3;

public:
	inline static int32_t get_offset_of_targetNS_0() { return static_cast<int32_t>(offsetof(ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D, ___targetNS_0)); }
	inline String_t* get_targetNS_0() const { return ___targetNS_0; }
	inline String_t** get_address_of_targetNS_0() { return &___targetNS_0; }
	inline void set_targetNS_0(String_t* value)
	{
		___targetNS_0 = value;
		Il2CppCodeGenWriteBarrier((&___targetNS_0), value);
	}

	inline static int32_t get_offset_of_chameleonLocation_1() { return static_cast<int32_t>(offsetof(ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D, ___chameleonLocation_1)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_chameleonLocation_1() const { return ___chameleonLocation_1; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_chameleonLocation_1() { return &___chameleonLocation_1; }
	inline void set_chameleonLocation_1(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___chameleonLocation_1 = value;
		Il2CppCodeGenWriteBarrier((&___chameleonLocation_1), value);
	}

	inline static int32_t get_offset_of_originalSchema_2() { return static_cast<int32_t>(offsetof(ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D, ___originalSchema_2)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_originalSchema_2() const { return ___originalSchema_2; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_originalSchema_2() { return &___originalSchema_2; }
	inline void set_originalSchema_2(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___originalSchema_2 = value;
		Il2CppCodeGenWriteBarrier((&___originalSchema_2), value);
	}

	inline static int32_t get_offset_of_hashCode_3() { return static_cast<int32_t>(offsetof(ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D, ___hashCode_3)); }
	inline int32_t get_hashCode_3() const { return ___hashCode_3; }
	inline int32_t* get_address_of_hashCode_3() { return &___hashCode_3; }
	inline void set_hashCode_3(int32_t value)
	{
		___hashCode_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAMELEONKEY_TB2DC0F8044027C691C7325267CB62C0BBDB1CB2D_H
#ifndef CONSTRAINTSTRUCT_T3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54_H
#define CONSTRAINTSTRUCT_T3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ConstraintStruct
struct  ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54  : public RuntimeObject
{
public:
	// System.Xml.Schema.CompiledIdentityConstraint System.Xml.Schema.ConstraintStruct::constraint
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * ___constraint_0;
	// System.Xml.Schema.SelectorActiveAxis System.Xml.Schema.ConstraintStruct::axisSelector
	SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64 * ___axisSelector_1;
	// System.Collections.ArrayList System.Xml.Schema.ConstraintStruct::axisFields
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___axisFields_2;
	// System.Collections.Hashtable System.Xml.Schema.ConstraintStruct::qualifiedTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___qualifiedTable_3;
	// System.Collections.Hashtable System.Xml.Schema.ConstraintStruct::keyrefTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___keyrefTable_4;
	// System.Int32 System.Xml.Schema.ConstraintStruct::tableDim
	int32_t ___tableDim_5;

public:
	inline static int32_t get_offset_of_constraint_0() { return static_cast<int32_t>(offsetof(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54, ___constraint_0)); }
	inline CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * get_constraint_0() const { return ___constraint_0; }
	inline CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E ** get_address_of_constraint_0() { return &___constraint_0; }
	inline void set_constraint_0(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * value)
	{
		___constraint_0 = value;
		Il2CppCodeGenWriteBarrier((&___constraint_0), value);
	}

	inline static int32_t get_offset_of_axisSelector_1() { return static_cast<int32_t>(offsetof(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54, ___axisSelector_1)); }
	inline SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64 * get_axisSelector_1() const { return ___axisSelector_1; }
	inline SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64 ** get_address_of_axisSelector_1() { return &___axisSelector_1; }
	inline void set_axisSelector_1(SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64 * value)
	{
		___axisSelector_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisSelector_1), value);
	}

	inline static int32_t get_offset_of_axisFields_2() { return static_cast<int32_t>(offsetof(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54, ___axisFields_2)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_axisFields_2() const { return ___axisFields_2; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_axisFields_2() { return &___axisFields_2; }
	inline void set_axisFields_2(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___axisFields_2 = value;
		Il2CppCodeGenWriteBarrier((&___axisFields_2), value);
	}

	inline static int32_t get_offset_of_qualifiedTable_3() { return static_cast<int32_t>(offsetof(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54, ___qualifiedTable_3)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_qualifiedTable_3() const { return ___qualifiedTable_3; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_qualifiedTable_3() { return &___qualifiedTable_3; }
	inline void set_qualifiedTable_3(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___qualifiedTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedTable_3), value);
	}

	inline static int32_t get_offset_of_keyrefTable_4() { return static_cast<int32_t>(offsetof(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54, ___keyrefTable_4)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_keyrefTable_4() const { return ___keyrefTable_4; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_keyrefTable_4() { return &___keyrefTable_4; }
	inline void set_keyrefTable_4(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___keyrefTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___keyrefTable_4), value);
	}

	inline static int32_t get_offset_of_tableDim_5() { return static_cast<int32_t>(offsetof(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54, ___tableDim_5)); }
	inline int32_t get_tableDim_5() const { return ___tableDim_5; }
	inline int32_t* get_address_of_tableDim_5() { return &___tableDim_5; }
	inline void set_tableDim_5(int32_t value)
	{
		___tableDim_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTSTRUCT_T3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54_H
#ifndef FORWARDAXIS_TD7F42B3CDF95D130675D57307CC20296D6829253_H
#define FORWARDAXIS_TD7F42B3CDF95D130675D57307CC20296D6829253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ForwardAxis
struct  ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253  : public RuntimeObject
{
public:
	// System.Xml.Schema.DoubleLinkAxis System.Xml.Schema.ForwardAxis::topNode
	DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * ___topNode_0;
	// System.Xml.Schema.DoubleLinkAxis System.Xml.Schema.ForwardAxis::rootNode
	DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * ___rootNode_1;
	// System.Boolean System.Xml.Schema.ForwardAxis::isAttribute
	bool ___isAttribute_2;
	// System.Boolean System.Xml.Schema.ForwardAxis::isDss
	bool ___isDss_3;
	// System.Boolean System.Xml.Schema.ForwardAxis::isSelfAxis
	bool ___isSelfAxis_4;

public:
	inline static int32_t get_offset_of_topNode_0() { return static_cast<int32_t>(offsetof(ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253, ___topNode_0)); }
	inline DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * get_topNode_0() const { return ___topNode_0; }
	inline DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 ** get_address_of_topNode_0() { return &___topNode_0; }
	inline void set_topNode_0(DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * value)
	{
		___topNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___topNode_0), value);
	}

	inline static int32_t get_offset_of_rootNode_1() { return static_cast<int32_t>(offsetof(ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253, ___rootNode_1)); }
	inline DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * get_rootNode_1() const { return ___rootNode_1; }
	inline DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 ** get_address_of_rootNode_1() { return &___rootNode_1; }
	inline void set_rootNode_1(DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955 * value)
	{
		___rootNode_1 = value;
		Il2CppCodeGenWriteBarrier((&___rootNode_1), value);
	}

	inline static int32_t get_offset_of_isAttribute_2() { return static_cast<int32_t>(offsetof(ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253, ___isAttribute_2)); }
	inline bool get_isAttribute_2() const { return ___isAttribute_2; }
	inline bool* get_address_of_isAttribute_2() { return &___isAttribute_2; }
	inline void set_isAttribute_2(bool value)
	{
		___isAttribute_2 = value;
	}

	inline static int32_t get_offset_of_isDss_3() { return static_cast<int32_t>(offsetof(ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253, ___isDss_3)); }
	inline bool get_isDss_3() const { return ___isDss_3; }
	inline bool* get_address_of_isDss_3() { return &___isDss_3; }
	inline void set_isDss_3(bool value)
	{
		___isDss_3 = value;
	}

	inline static int32_t get_offset_of_isSelfAxis_4() { return static_cast<int32_t>(offsetof(ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253, ___isSelfAxis_4)); }
	inline bool get_isSelfAxis_4() const { return ___isSelfAxis_4; }
	inline bool* get_address_of_isSelfAxis_4() { return &___isSelfAxis_4; }
	inline void set_isSelfAxis_4(bool value)
	{
		___isSelfAxis_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORWARDAXIS_TD7F42B3CDF95D130675D57307CC20296D6829253_H
#ifndef KSSTRUCT_TB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E_H
#define KSSTRUCT_TB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.KSStruct
struct  KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.KSStruct::depth
	int32_t ___depth_0;
	// System.Xml.Schema.KeySequence System.Xml.Schema.KSStruct::ks
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE * ___ks_1;
	// System.Xml.Schema.LocatedActiveAxis[] System.Xml.Schema.KSStruct::fields
	LocatedActiveAxisU5BU5D_t4148EC17C851E170B819098896BDD191CDDC41D1* ___fields_2;

public:
	inline static int32_t get_offset_of_depth_0() { return static_cast<int32_t>(offsetof(KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E, ___depth_0)); }
	inline int32_t get_depth_0() const { return ___depth_0; }
	inline int32_t* get_address_of_depth_0() { return &___depth_0; }
	inline void set_depth_0(int32_t value)
	{
		___depth_0 = value;
	}

	inline static int32_t get_offset_of_ks_1() { return static_cast<int32_t>(offsetof(KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E, ___ks_1)); }
	inline KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE * get_ks_1() const { return ___ks_1; }
	inline KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE ** get_address_of_ks_1() { return &___ks_1; }
	inline void set_ks_1(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE * value)
	{
		___ks_1 = value;
		Il2CppCodeGenWriteBarrier((&___ks_1), value);
	}

	inline static int32_t get_offset_of_fields_2() { return static_cast<int32_t>(offsetof(KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E, ___fields_2)); }
	inline LocatedActiveAxisU5BU5D_t4148EC17C851E170B819098896BDD191CDDC41D1* get_fields_2() const { return ___fields_2; }
	inline LocatedActiveAxisU5BU5D_t4148EC17C851E170B819098896BDD191CDDC41D1** get_address_of_fields_2() { return &___fields_2; }
	inline void set_fields_2(LocatedActiveAxisU5BU5D_t4148EC17C851E170B819098896BDD191CDDC41D1* value)
	{
		___fields_2 = value;
		Il2CppCodeGenWriteBarrier((&___fields_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KSSTRUCT_TB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E_H
#ifndef KEYSEQUENCE_TEDAF2417185973D2C57AB5B1A35D379652C484BE_H
#define KEYSEQUENCE_TEDAF2417185973D2C57AB5B1A35D379652C484BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.KeySequence
struct  KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE  : public RuntimeObject
{
public:
	// System.Xml.Schema.TypedObject[] System.Xml.Schema.KeySequence::ks
	TypedObjectU5BU5D_t5F58D2C06E283EF28F0674C78DEB47DA64DF9CE1* ___ks_0;
	// System.Int32 System.Xml.Schema.KeySequence::dim
	int32_t ___dim_1;
	// System.Int32 System.Xml.Schema.KeySequence::hashcode
	int32_t ___hashcode_2;
	// System.Int32 System.Xml.Schema.KeySequence::posline
	int32_t ___posline_3;
	// System.Int32 System.Xml.Schema.KeySequence::poscol
	int32_t ___poscol_4;

public:
	inline static int32_t get_offset_of_ks_0() { return static_cast<int32_t>(offsetof(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE, ___ks_0)); }
	inline TypedObjectU5BU5D_t5F58D2C06E283EF28F0674C78DEB47DA64DF9CE1* get_ks_0() const { return ___ks_0; }
	inline TypedObjectU5BU5D_t5F58D2C06E283EF28F0674C78DEB47DA64DF9CE1** get_address_of_ks_0() { return &___ks_0; }
	inline void set_ks_0(TypedObjectU5BU5D_t5F58D2C06E283EF28F0674C78DEB47DA64DF9CE1* value)
	{
		___ks_0 = value;
		Il2CppCodeGenWriteBarrier((&___ks_0), value);
	}

	inline static int32_t get_offset_of_dim_1() { return static_cast<int32_t>(offsetof(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE, ___dim_1)); }
	inline int32_t get_dim_1() const { return ___dim_1; }
	inline int32_t* get_address_of_dim_1() { return &___dim_1; }
	inline void set_dim_1(int32_t value)
	{
		___dim_1 = value;
	}

	inline static int32_t get_offset_of_hashcode_2() { return static_cast<int32_t>(offsetof(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE, ___hashcode_2)); }
	inline int32_t get_hashcode_2() const { return ___hashcode_2; }
	inline int32_t* get_address_of_hashcode_2() { return &___hashcode_2; }
	inline void set_hashcode_2(int32_t value)
	{
		___hashcode_2 = value;
	}

	inline static int32_t get_offset_of_posline_3() { return static_cast<int32_t>(offsetof(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE, ___posline_3)); }
	inline int32_t get_posline_3() const { return ___posline_3; }
	inline int32_t* get_address_of_posline_3() { return &___posline_3; }
	inline void set_posline_3(int32_t value)
	{
		___posline_3 = value;
	}

	inline static int32_t get_offset_of_poscol_4() { return static_cast<int32_t>(offsetof(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE, ___poscol_4)); }
	inline int32_t get_poscol_4() const { return ___poscol_4; }
	inline int32_t* get_address_of_poscol_4() { return &___poscol_4; }
	inline void set_poscol_4(int32_t value)
	{
		___poscol_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYSEQUENCE_TEDAF2417185973D2C57AB5B1A35D379652C484BE_H
#ifndef SYMBOLSDICTIONARY_TD5FE8542DCD1430A2E53390D10EE48FCA52227A6_H
#define SYMBOLSDICTIONARY_TD5FE8542DCD1430A2E53390D10EE48FCA52227A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SymbolsDictionary
struct  SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.SymbolsDictionary::last
	int32_t ___last_0;
	// System.Collections.Hashtable System.Xml.Schema.SymbolsDictionary::names
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___names_1;
	// System.Collections.Hashtable System.Xml.Schema.SymbolsDictionary::wildcards
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___wildcards_2;
	// System.Collections.ArrayList System.Xml.Schema.SymbolsDictionary::particles
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___particles_3;
	// System.Object System.Xml.Schema.SymbolsDictionary::particleLast
	RuntimeObject * ___particleLast_4;
	// System.Boolean System.Xml.Schema.SymbolsDictionary::isUpaEnforced
	bool ___isUpaEnforced_5;

public:
	inline static int32_t get_offset_of_last_0() { return static_cast<int32_t>(offsetof(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6, ___last_0)); }
	inline int32_t get_last_0() const { return ___last_0; }
	inline int32_t* get_address_of_last_0() { return &___last_0; }
	inline void set_last_0(int32_t value)
	{
		___last_0 = value;
	}

	inline static int32_t get_offset_of_names_1() { return static_cast<int32_t>(offsetof(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6, ___names_1)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_names_1() const { return ___names_1; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_names_1() { return &___names_1; }
	inline void set_names_1(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___names_1 = value;
		Il2CppCodeGenWriteBarrier((&___names_1), value);
	}

	inline static int32_t get_offset_of_wildcards_2() { return static_cast<int32_t>(offsetof(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6, ___wildcards_2)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_wildcards_2() const { return ___wildcards_2; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_wildcards_2() { return &___wildcards_2; }
	inline void set_wildcards_2(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___wildcards_2 = value;
		Il2CppCodeGenWriteBarrier((&___wildcards_2), value);
	}

	inline static int32_t get_offset_of_particles_3() { return static_cast<int32_t>(offsetof(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6, ___particles_3)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_particles_3() const { return ___particles_3; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_particles_3() { return &___particles_3; }
	inline void set_particles_3(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___particles_3 = value;
		Il2CppCodeGenWriteBarrier((&___particles_3), value);
	}

	inline static int32_t get_offset_of_particleLast_4() { return static_cast<int32_t>(offsetof(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6, ___particleLast_4)); }
	inline RuntimeObject * get_particleLast_4() const { return ___particleLast_4; }
	inline RuntimeObject ** get_address_of_particleLast_4() { return &___particleLast_4; }
	inline void set_particleLast_4(RuntimeObject * value)
	{
		___particleLast_4 = value;
		Il2CppCodeGenWriteBarrier((&___particleLast_4), value);
	}

	inline static int32_t get_offset_of_isUpaEnforced_5() { return static_cast<int32_t>(offsetof(SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6, ___isUpaEnforced_5)); }
	inline bool get_isUpaEnforced_5() const { return ___isUpaEnforced_5; }
	inline bool* get_address_of_isUpaEnforced_5() { return &___isUpaEnforced_5; }
	inline void set_isUpaEnforced_5(bool value)
	{
		___isUpaEnforced_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLSDICTIONARY_TD5FE8542DCD1430A2E53390D10EE48FCA52227A6_H
#ifndef TYPEDOBJECT_T4DD26D496368DF76E600AAF06AADEE3E5E49FF9D_H
#define TYPEDOBJECT_T4DD26D496368DF76E600AAF06AADEE3E5E49FF9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.TypedObject
struct  TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D  : public RuntimeObject
{
public:
	// System.Xml.Schema.TypedObject_DecimalStruct System.Xml.Schema.TypedObject::dstruct
	DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11 * ___dstruct_0;
	// System.Object System.Xml.Schema.TypedObject::ovalue
	RuntimeObject * ___ovalue_1;
	// System.String System.Xml.Schema.TypedObject::svalue
	String_t* ___svalue_2;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.TypedObject::xsdtype
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___xsdtype_3;
	// System.Int32 System.Xml.Schema.TypedObject::dim
	int32_t ___dim_4;
	// System.Boolean System.Xml.Schema.TypedObject::isList
	bool ___isList_5;

public:
	inline static int32_t get_offset_of_dstruct_0() { return static_cast<int32_t>(offsetof(TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D, ___dstruct_0)); }
	inline DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11 * get_dstruct_0() const { return ___dstruct_0; }
	inline DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11 ** get_address_of_dstruct_0() { return &___dstruct_0; }
	inline void set_dstruct_0(DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11 * value)
	{
		___dstruct_0 = value;
		Il2CppCodeGenWriteBarrier((&___dstruct_0), value);
	}

	inline static int32_t get_offset_of_ovalue_1() { return static_cast<int32_t>(offsetof(TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D, ___ovalue_1)); }
	inline RuntimeObject * get_ovalue_1() const { return ___ovalue_1; }
	inline RuntimeObject ** get_address_of_ovalue_1() { return &___ovalue_1; }
	inline void set_ovalue_1(RuntimeObject * value)
	{
		___ovalue_1 = value;
		Il2CppCodeGenWriteBarrier((&___ovalue_1), value);
	}

	inline static int32_t get_offset_of_svalue_2() { return static_cast<int32_t>(offsetof(TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D, ___svalue_2)); }
	inline String_t* get_svalue_2() const { return ___svalue_2; }
	inline String_t** get_address_of_svalue_2() { return &___svalue_2; }
	inline void set_svalue_2(String_t* value)
	{
		___svalue_2 = value;
		Il2CppCodeGenWriteBarrier((&___svalue_2), value);
	}

	inline static int32_t get_offset_of_xsdtype_3() { return static_cast<int32_t>(offsetof(TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D, ___xsdtype_3)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get_xsdtype_3() const { return ___xsdtype_3; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of_xsdtype_3() { return &___xsdtype_3; }
	inline void set_xsdtype_3(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		___xsdtype_3 = value;
		Il2CppCodeGenWriteBarrier((&___xsdtype_3), value);
	}

	inline static int32_t get_offset_of_dim_4() { return static_cast<int32_t>(offsetof(TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D, ___dim_4)); }
	inline int32_t get_dim_4() const { return ___dim_4; }
	inline int32_t* get_address_of_dim_4() { return &___dim_4; }
	inline void set_dim_4(int32_t value)
	{
		___dim_4 = value;
	}

	inline static int32_t get_offset_of_isList_5() { return static_cast<int32_t>(offsetof(TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D, ___isList_5)); }
	inline bool get_isList_5() const { return ___isList_5; }
	inline bool* get_address_of_isList_5() { return &___isList_5; }
	inline void set_isList_5(bool value)
	{
		___isList_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDOBJECT_T4DD26D496368DF76E600AAF06AADEE3E5E49FF9D_H
#ifndef DECIMALSTRUCT_TD351714FCA963CEC4A7E529F0066EE965EA7DD11_H
#define DECIMALSTRUCT_TD351714FCA963CEC4A7E529F0066EE965EA7DD11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.TypedObject_DecimalStruct
struct  DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Schema.TypedObject_DecimalStruct::isDecimal
	bool ___isDecimal_0;
	// System.Decimal[] System.Xml.Schema.TypedObject_DecimalStruct::dvalue
	DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F* ___dvalue_1;

public:
	inline static int32_t get_offset_of_isDecimal_0() { return static_cast<int32_t>(offsetof(DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11, ___isDecimal_0)); }
	inline bool get_isDecimal_0() const { return ___isDecimal_0; }
	inline bool* get_address_of_isDecimal_0() { return &___isDecimal_0; }
	inline void set_isDecimal_0(bool value)
	{
		___isDecimal_0 = value;
	}

	inline static int32_t get_offset_of_dvalue_1() { return static_cast<int32_t>(offsetof(DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11, ___dvalue_1)); }
	inline DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F* get_dvalue_1() const { return ___dvalue_1; }
	inline DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F** get_address_of_dvalue_1() { return &___dvalue_1; }
	inline void set_dvalue_1(DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F* value)
	{
		___dvalue_1 = value;
		Il2CppCodeGenWriteBarrier((&___dvalue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMALSTRUCT_TD351714FCA963CEC4A7E529F0066EE965EA7DD11_H
#ifndef CODEIDENTIFIER_TDFB64E36DC62ADF524279E6F46F4C002F81A0BE0_H
#define CODEIDENTIFIER_TDFB64E36DC62ADF524279E6F46F4C002F81A0BE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.CodeIdentifier
struct  CodeIdentifier_tDFB64E36DC62ADF524279E6F46F4C002F81A0BE0  : public RuntimeObject
{
public:

public:
};

struct CodeIdentifier_tDFB64E36DC62ADF524279E6F46F4C002F81A0BE0_StaticFields
{
public:
	// Microsoft.CSharp.CodeDomProvider System.Xml.Serialization.CodeIdentifier::csharp
	CodeDomProvider_t20C00DE7C8797892A59C1059898FB80842DB65F7 * ___csharp_0;

public:
	inline static int32_t get_offset_of_csharp_0() { return static_cast<int32_t>(offsetof(CodeIdentifier_tDFB64E36DC62ADF524279E6F46F4C002F81A0BE0_StaticFields, ___csharp_0)); }
	inline CodeDomProvider_t20C00DE7C8797892A59C1059898FB80842DB65F7 * get_csharp_0() const { return ___csharp_0; }
	inline CodeDomProvider_t20C00DE7C8797892A59C1059898FB80842DB65F7 ** get_address_of_csharp_0() { return &___csharp_0; }
	inline void set_csharp_0(CodeDomProvider_t20C00DE7C8797892A59C1059898FB80842DB65F7 * value)
	{
		___csharp_0 = value;
		Il2CppCodeGenWriteBarrier((&___csharp_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEIDENTIFIER_TDFB64E36DC62ADF524279E6F46F4C002F81A0BE0_H
#ifndef ENUMMAPMEMBER_T508AEADF6DA5EFA4F7B0522FA689C3A8638118E3_H
#define ENUMMAPMEMBER_T508AEADF6DA5EFA4F7B0522FA689C3A8638118E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.EnumMap_EnumMapMember
struct  EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3  : public RuntimeObject
{
public:
	// System.String System.Xml.Serialization.EnumMap_EnumMapMember::_xmlName
	String_t* ____xmlName_0;
	// System.String System.Xml.Serialization.EnumMap_EnumMapMember::_enumName
	String_t* ____enumName_1;
	// System.Int64 System.Xml.Serialization.EnumMap_EnumMapMember::_value
	int64_t ____value_2;

public:
	inline static int32_t get_offset_of__xmlName_0() { return static_cast<int32_t>(offsetof(EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3, ____xmlName_0)); }
	inline String_t* get__xmlName_0() const { return ____xmlName_0; }
	inline String_t** get_address_of__xmlName_0() { return &____xmlName_0; }
	inline void set__xmlName_0(String_t* value)
	{
		____xmlName_0 = value;
		Il2CppCodeGenWriteBarrier((&____xmlName_0), value);
	}

	inline static int32_t get_offset_of__enumName_1() { return static_cast<int32_t>(offsetof(EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3, ____enumName_1)); }
	inline String_t* get__enumName_1() const { return ____enumName_1; }
	inline String_t** get_address_of__enumName_1() { return &____enumName_1; }
	inline void set__enumName_1(String_t* value)
	{
		____enumName_1 = value;
		Il2CppCodeGenWriteBarrier((&____enumName_1), value);
	}

	inline static int32_t get_offset_of__value_2() { return static_cast<int32_t>(offsetof(EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3, ____value_2)); }
	inline int64_t get__value_2() const { return ____value_2; }
	inline int64_t* get_address_of__value_2() { return &____value_2; }
	inline void set__value_2(int64_t value)
	{
		____value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMMAPMEMBER_T508AEADF6DA5EFA4F7B0522FA689C3A8638118E3_H
#ifndef OBJECTMAP_T53CD0E614E60A73AB445A2A757CCD3C356F31677_H
#define OBJECTMAP_T53CD0E614E60A73AB445A2A757CCD3C356F31677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.ObjectMap
struct  ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTMAP_T53CD0E614E60A73AB445A2A757CCD3C356F31677_H
#ifndef REFLECTIONHELPER_TE199EEE7D70F52495BFC2BA2C57EB73A91F512E6_H
#define REFLECTIONHELPER_TE199EEE7D70F52495BFC2BA2C57EB73A91F512E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.ReflectionHelper
struct  ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.ReflectionHelper::_clrTypes
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____clrTypes_0;
	// System.Collections.Hashtable System.Xml.Serialization.ReflectionHelper::_schemaTypes
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____schemaTypes_1;

public:
	inline static int32_t get_offset_of__clrTypes_0() { return static_cast<int32_t>(offsetof(ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6, ____clrTypes_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__clrTypes_0() const { return ____clrTypes_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__clrTypes_0() { return &____clrTypes_0; }
	inline void set__clrTypes_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____clrTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&____clrTypes_0), value);
	}

	inline static int32_t get_offset_of__schemaTypes_1() { return static_cast<int32_t>(offsetof(ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6, ____schemaTypes_1)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__schemaTypes_1() const { return ____schemaTypes_1; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__schemaTypes_1() { return &____schemaTypes_1; }
	inline void set__schemaTypes_1(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____schemaTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&____schemaTypes_1), value);
	}
};

struct ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6_StaticFields
{
public:
	// System.Reflection.ParameterModifier[] System.Xml.Serialization.ReflectionHelper::empty_modifiers
	ParameterModifierU5BU5D_t63EC46F14F048DC9EF6BF1362E8AEBEA1A05A5EA* ___empty_modifiers_2;

public:
	inline static int32_t get_offset_of_empty_modifiers_2() { return static_cast<int32_t>(offsetof(ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6_StaticFields, ___empty_modifiers_2)); }
	inline ParameterModifierU5BU5D_t63EC46F14F048DC9EF6BF1362E8AEBEA1A05A5EA* get_empty_modifiers_2() const { return ___empty_modifiers_2; }
	inline ParameterModifierU5BU5D_t63EC46F14F048DC9EF6BF1362E8AEBEA1A05A5EA** get_address_of_empty_modifiers_2() { return &___empty_modifiers_2; }
	inline void set_empty_modifiers_2(ParameterModifierU5BU5D_t63EC46F14F048DC9EF6BF1362E8AEBEA1A05A5EA* value)
	{
		___empty_modifiers_2 = value;
		Il2CppCodeGenWriteBarrier((&___empty_modifiers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONHELPER_TE199EEE7D70F52495BFC2BA2C57EB73A91F512E6_H
#ifndef TYPEMEMBER_T9BB952F057312683E0A48C45A067C78A0B9F74AB_H
#define TYPEMEMBER_T9BB952F057312683E0A48C45A067C78A0B9F74AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.TypeMember
struct  TypeMember_t9BB952F057312683E0A48C45A067C78A0B9F74AB  : public RuntimeObject
{
public:
	// System.Type System.Xml.Serialization.TypeMember::type
	Type_t * ___type_0;
	// System.String System.Xml.Serialization.TypeMember::member
	String_t* ___member_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TypeMember_t9BB952F057312683E0A48C45A067C78A0B9F74AB, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_member_1() { return static_cast<int32_t>(offsetof(TypeMember_t9BB952F057312683E0A48C45A067C78A0B9F74AB, ___member_1)); }
	inline String_t* get_member_1() const { return ___member_1; }
	inline String_t** get_address_of_member_1() { return &___member_1; }
	inline void set_member_1(String_t* value)
	{
		___member_1 = value;
		Il2CppCodeGenWriteBarrier((&___member_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEMEMBER_T9BB952F057312683E0A48C45A067C78A0B9F74AB_H
#ifndef TYPETRANSLATOR_T2B73F6806E7647A48688794BCD1E9C3793BE47EA_H
#define TYPETRANSLATOR_T2B73F6806E7647A48688794BCD1E9C3793BE47EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.TypeTranslator
struct  TypeTranslator_t2B73F6806E7647A48688794BCD1E9C3793BE47EA  : public RuntimeObject
{
public:

public:
};

struct TypeTranslator_t2B73F6806E7647A48688794BCD1E9C3793BE47EA_StaticFields
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::nameCache
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___nameCache_0;
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::primitiveTypes
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___primitiveTypes_1;
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::primitiveArrayTypes
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___primitiveArrayTypes_2;
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::nullableTypes
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___nullableTypes_3;

public:
	inline static int32_t get_offset_of_nameCache_0() { return static_cast<int32_t>(offsetof(TypeTranslator_t2B73F6806E7647A48688794BCD1E9C3793BE47EA_StaticFields, ___nameCache_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_nameCache_0() const { return ___nameCache_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_nameCache_0() { return &___nameCache_0; }
	inline void set_nameCache_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___nameCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameCache_0), value);
	}

	inline static int32_t get_offset_of_primitiveTypes_1() { return static_cast<int32_t>(offsetof(TypeTranslator_t2B73F6806E7647A48688794BCD1E9C3793BE47EA_StaticFields, ___primitiveTypes_1)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_primitiveTypes_1() const { return ___primitiveTypes_1; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_primitiveTypes_1() { return &___primitiveTypes_1; }
	inline void set_primitiveTypes_1(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___primitiveTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveTypes_1), value);
	}

	inline static int32_t get_offset_of_primitiveArrayTypes_2() { return static_cast<int32_t>(offsetof(TypeTranslator_t2B73F6806E7647A48688794BCD1E9C3793BE47EA_StaticFields, ___primitiveArrayTypes_2)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_primitiveArrayTypes_2() const { return ___primitiveArrayTypes_2; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_primitiveArrayTypes_2() { return &___primitiveArrayTypes_2; }
	inline void set_primitiveArrayTypes_2(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___primitiveArrayTypes_2 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveArrayTypes_2), value);
	}

	inline static int32_t get_offset_of_nullableTypes_3() { return static_cast<int32_t>(offsetof(TypeTranslator_t2B73F6806E7647A48688794BCD1E9C3793BE47EA_StaticFields, ___nullableTypes_3)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_nullableTypes_3() const { return ___nullableTypes_3; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_nullableTypes_3() { return &___nullableTypes_3; }
	inline void set_nullableTypes_3(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___nullableTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___nullableTypes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPETRANSLATOR_T2B73F6806E7647A48688794BCD1E9C3793BE47EA_H
#ifndef XMLATTRIBUTEOVERRIDES_TE2AE30774D1F8B3860BFE1A8B6233970E88C7F7E_H
#define XMLATTRIBUTEOVERRIDES_TE2AE30774D1F8B3860BFE1A8B6233970E88C7F7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAttributeOverrides
struct  XmlAttributeOverrides_tE2AE30774D1F8B3860BFE1A8B6233970E88C7F7E  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.XmlAttributeOverrides::overrides
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___overrides_0;

public:
	inline static int32_t get_offset_of_overrides_0() { return static_cast<int32_t>(offsetof(XmlAttributeOverrides_tE2AE30774D1F8B3860BFE1A8B6233970E88C7F7E, ___overrides_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_overrides_0() const { return ___overrides_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_overrides_0() { return &___overrides_0; }
	inline void set_overrides_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___overrides_0 = value;
		Il2CppCodeGenWriteBarrier((&___overrides_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTEOVERRIDES_TE2AE30774D1F8B3860BFE1A8B6233970E88C7F7E_H
#ifndef XMLATTRIBUTES_T2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A_H
#define XMLATTRIBUTES_T2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAttributes
struct  XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A  : public RuntimeObject
{
public:
	// System.Xml.Serialization.XmlAnyAttributeAttribute System.Xml.Serialization.XmlAttributes::xmlAnyAttribute
	XmlAnyAttributeAttribute_tBB53254892DFE4AB748B01108539540EC451C680 * ___xmlAnyAttribute_0;
	// System.Xml.Serialization.XmlAnyElementAttributes System.Xml.Serialization.XmlAttributes::xmlAnyElements
	XmlAnyElementAttributes_tED6F51ECCCB48BBB4C5E4D826A7E5CF341F33160 * ___xmlAnyElements_1;
	// System.Xml.Serialization.XmlArrayAttribute System.Xml.Serialization.XmlAttributes::xmlArray
	XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19 * ___xmlArray_2;
	// System.Xml.Serialization.XmlArrayItemAttributes System.Xml.Serialization.XmlAttributes::xmlArrayItems
	XmlArrayItemAttributes_t7BFEEA6B880F1F3FC7DF8EB92C33E65C56FA0A49 * ___xmlArrayItems_3;
	// System.Xml.Serialization.XmlAttributeAttribute System.Xml.Serialization.XmlAttributes::xmlAttribute
	XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E * ___xmlAttribute_4;
	// System.Xml.Serialization.XmlChoiceIdentifierAttribute System.Xml.Serialization.XmlAttributes::xmlChoiceIdentifier
	XmlChoiceIdentifierAttribute_t67E5AE7310D779F7533770C3D3F7CAA65A5E6B8B * ___xmlChoiceIdentifier_5;
	// System.Object System.Xml.Serialization.XmlAttributes::xmlDefaultValue
	RuntimeObject * ___xmlDefaultValue_6;
	// System.Xml.Serialization.XmlElementAttributes System.Xml.Serialization.XmlAttributes::xmlElements
	XmlElementAttributes_t3F224B71C1BC11E8EF6B4E8E45A940B394FA6D34 * ___xmlElements_7;
	// System.Xml.Serialization.XmlEnumAttribute System.Xml.Serialization.XmlAttributes::xmlEnum
	XmlEnumAttribute_tBD75888B09FE971438F4FA55FC302BC5FA3936F0 * ___xmlEnum_8;
	// System.Boolean System.Xml.Serialization.XmlAttributes::xmlIgnore
	bool ___xmlIgnore_9;
	// System.Boolean System.Xml.Serialization.XmlAttributes::xmlns
	bool ___xmlns_10;
	// System.Xml.Serialization.XmlRootAttribute System.Xml.Serialization.XmlAttributes::xmlRoot
	XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C * ___xmlRoot_11;
	// System.Xml.Serialization.XmlTextAttribute System.Xml.Serialization.XmlAttributes::xmlText
	XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7 * ___xmlText_12;
	// System.Xml.Serialization.XmlTypeAttribute System.Xml.Serialization.XmlAttributes::xmlType
	XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08 * ___xmlType_13;

public:
	inline static int32_t get_offset_of_xmlAnyAttribute_0() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlAnyAttribute_0)); }
	inline XmlAnyAttributeAttribute_tBB53254892DFE4AB748B01108539540EC451C680 * get_xmlAnyAttribute_0() const { return ___xmlAnyAttribute_0; }
	inline XmlAnyAttributeAttribute_tBB53254892DFE4AB748B01108539540EC451C680 ** get_address_of_xmlAnyAttribute_0() { return &___xmlAnyAttribute_0; }
	inline void set_xmlAnyAttribute_0(XmlAnyAttributeAttribute_tBB53254892DFE4AB748B01108539540EC451C680 * value)
	{
		___xmlAnyAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___xmlAnyAttribute_0), value);
	}

	inline static int32_t get_offset_of_xmlAnyElements_1() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlAnyElements_1)); }
	inline XmlAnyElementAttributes_tED6F51ECCCB48BBB4C5E4D826A7E5CF341F33160 * get_xmlAnyElements_1() const { return ___xmlAnyElements_1; }
	inline XmlAnyElementAttributes_tED6F51ECCCB48BBB4C5E4D826A7E5CF341F33160 ** get_address_of_xmlAnyElements_1() { return &___xmlAnyElements_1; }
	inline void set_xmlAnyElements_1(XmlAnyElementAttributes_tED6F51ECCCB48BBB4C5E4D826A7E5CF341F33160 * value)
	{
		___xmlAnyElements_1 = value;
		Il2CppCodeGenWriteBarrier((&___xmlAnyElements_1), value);
	}

	inline static int32_t get_offset_of_xmlArray_2() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlArray_2)); }
	inline XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19 * get_xmlArray_2() const { return ___xmlArray_2; }
	inline XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19 ** get_address_of_xmlArray_2() { return &___xmlArray_2; }
	inline void set_xmlArray_2(XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19 * value)
	{
		___xmlArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___xmlArray_2), value);
	}

	inline static int32_t get_offset_of_xmlArrayItems_3() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlArrayItems_3)); }
	inline XmlArrayItemAttributes_t7BFEEA6B880F1F3FC7DF8EB92C33E65C56FA0A49 * get_xmlArrayItems_3() const { return ___xmlArrayItems_3; }
	inline XmlArrayItemAttributes_t7BFEEA6B880F1F3FC7DF8EB92C33E65C56FA0A49 ** get_address_of_xmlArrayItems_3() { return &___xmlArrayItems_3; }
	inline void set_xmlArrayItems_3(XmlArrayItemAttributes_t7BFEEA6B880F1F3FC7DF8EB92C33E65C56FA0A49 * value)
	{
		___xmlArrayItems_3 = value;
		Il2CppCodeGenWriteBarrier((&___xmlArrayItems_3), value);
	}

	inline static int32_t get_offset_of_xmlAttribute_4() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlAttribute_4)); }
	inline XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E * get_xmlAttribute_4() const { return ___xmlAttribute_4; }
	inline XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E ** get_address_of_xmlAttribute_4() { return &___xmlAttribute_4; }
	inline void set_xmlAttribute_4(XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E * value)
	{
		___xmlAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___xmlAttribute_4), value);
	}

	inline static int32_t get_offset_of_xmlChoiceIdentifier_5() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlChoiceIdentifier_5)); }
	inline XmlChoiceIdentifierAttribute_t67E5AE7310D779F7533770C3D3F7CAA65A5E6B8B * get_xmlChoiceIdentifier_5() const { return ___xmlChoiceIdentifier_5; }
	inline XmlChoiceIdentifierAttribute_t67E5AE7310D779F7533770C3D3F7CAA65A5E6B8B ** get_address_of_xmlChoiceIdentifier_5() { return &___xmlChoiceIdentifier_5; }
	inline void set_xmlChoiceIdentifier_5(XmlChoiceIdentifierAttribute_t67E5AE7310D779F7533770C3D3F7CAA65A5E6B8B * value)
	{
		___xmlChoiceIdentifier_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlChoiceIdentifier_5), value);
	}

	inline static int32_t get_offset_of_xmlDefaultValue_6() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlDefaultValue_6)); }
	inline RuntimeObject * get_xmlDefaultValue_6() const { return ___xmlDefaultValue_6; }
	inline RuntimeObject ** get_address_of_xmlDefaultValue_6() { return &___xmlDefaultValue_6; }
	inline void set_xmlDefaultValue_6(RuntimeObject * value)
	{
		___xmlDefaultValue_6 = value;
		Il2CppCodeGenWriteBarrier((&___xmlDefaultValue_6), value);
	}

	inline static int32_t get_offset_of_xmlElements_7() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlElements_7)); }
	inline XmlElementAttributes_t3F224B71C1BC11E8EF6B4E8E45A940B394FA6D34 * get_xmlElements_7() const { return ___xmlElements_7; }
	inline XmlElementAttributes_t3F224B71C1BC11E8EF6B4E8E45A940B394FA6D34 ** get_address_of_xmlElements_7() { return &___xmlElements_7; }
	inline void set_xmlElements_7(XmlElementAttributes_t3F224B71C1BC11E8EF6B4E8E45A940B394FA6D34 * value)
	{
		___xmlElements_7 = value;
		Il2CppCodeGenWriteBarrier((&___xmlElements_7), value);
	}

	inline static int32_t get_offset_of_xmlEnum_8() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlEnum_8)); }
	inline XmlEnumAttribute_tBD75888B09FE971438F4FA55FC302BC5FA3936F0 * get_xmlEnum_8() const { return ___xmlEnum_8; }
	inline XmlEnumAttribute_tBD75888B09FE971438F4FA55FC302BC5FA3936F0 ** get_address_of_xmlEnum_8() { return &___xmlEnum_8; }
	inline void set_xmlEnum_8(XmlEnumAttribute_tBD75888B09FE971438F4FA55FC302BC5FA3936F0 * value)
	{
		___xmlEnum_8 = value;
		Il2CppCodeGenWriteBarrier((&___xmlEnum_8), value);
	}

	inline static int32_t get_offset_of_xmlIgnore_9() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlIgnore_9)); }
	inline bool get_xmlIgnore_9() const { return ___xmlIgnore_9; }
	inline bool* get_address_of_xmlIgnore_9() { return &___xmlIgnore_9; }
	inline void set_xmlIgnore_9(bool value)
	{
		___xmlIgnore_9 = value;
	}

	inline static int32_t get_offset_of_xmlns_10() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlns_10)); }
	inline bool get_xmlns_10() const { return ___xmlns_10; }
	inline bool* get_address_of_xmlns_10() { return &___xmlns_10; }
	inline void set_xmlns_10(bool value)
	{
		___xmlns_10 = value;
	}

	inline static int32_t get_offset_of_xmlRoot_11() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlRoot_11)); }
	inline XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C * get_xmlRoot_11() const { return ___xmlRoot_11; }
	inline XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C ** get_address_of_xmlRoot_11() { return &___xmlRoot_11; }
	inline void set_xmlRoot_11(XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C * value)
	{
		___xmlRoot_11 = value;
		Il2CppCodeGenWriteBarrier((&___xmlRoot_11), value);
	}

	inline static int32_t get_offset_of_xmlText_12() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlText_12)); }
	inline XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7 * get_xmlText_12() const { return ___xmlText_12; }
	inline XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7 ** get_address_of_xmlText_12() { return &___xmlText_12; }
	inline void set_xmlText_12(XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7 * value)
	{
		___xmlText_12 = value;
		Il2CppCodeGenWriteBarrier((&___xmlText_12), value);
	}

	inline static int32_t get_offset_of_xmlType_13() { return static_cast<int32_t>(offsetof(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A, ___xmlType_13)); }
	inline XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08 * get_xmlType_13() const { return ___xmlType_13; }
	inline XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08 ** get_address_of_xmlType_13() { return &___xmlType_13; }
	inline void set_xmlType_13(XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08 * value)
	{
		___xmlType_13 = value;
		Il2CppCodeGenWriteBarrier((&___xmlType_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTES_T2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A_H
#ifndef XMLCUSTOMFORMATTER_T20B9A7F147CB04B1552D1B5B1A62F60975441707_H
#define XMLCUSTOMFORMATTER_T20B9A7F147CB04B1552D1B5B1A62F60975441707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlCustomFormatter
struct  XmlCustomFormatter_t20B9A7F147CB04B1552D1B5B1A62F60975441707  : public RuntimeObject
{
public:

public:
};

struct XmlCustomFormatter_t20B9A7F147CB04B1552D1B5B1A62F60975441707_StaticFields
{
public:
	// System.String[] System.Xml.Serialization.XmlCustomFormatter::allTimeFormats
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___allTimeFormats_0;

public:
	inline static int32_t get_offset_of_allTimeFormats_0() { return static_cast<int32_t>(offsetof(XmlCustomFormatter_t20B9A7F147CB04B1552D1B5B1A62F60975441707_StaticFields, ___allTimeFormats_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_allTimeFormats_0() const { return ___allTimeFormats_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_allTimeFormats_0() { return &___allTimeFormats_0; }
	inline void set_allTimeFormats_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___allTimeFormats_0 = value;
		Il2CppCodeGenWriteBarrier((&___allTimeFormats_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCUSTOMFORMATTER_T20B9A7F147CB04B1552D1B5B1A62F60975441707_H
#ifndef XMLREFLECTIONIMPORTER_T01142C73A76F11C3781558BEE362720BFDDDED3A_H
#define XMLREFLECTIONIMPORTER_T01142C73A76F11C3781558BEE362720BFDDDED3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlReflectionImporter
struct  XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A  : public RuntimeObject
{
public:
	// System.String System.Xml.Serialization.XmlReflectionImporter::initialDefaultNamespace
	String_t* ___initialDefaultNamespace_0;
	// System.Xml.Serialization.XmlAttributeOverrides System.Xml.Serialization.XmlReflectionImporter::attributeOverrides
	XmlAttributeOverrides_tE2AE30774D1F8B3860BFE1A8B6233970E88C7F7E * ___attributeOverrides_1;
	// System.Collections.ArrayList System.Xml.Serialization.XmlReflectionImporter::includedTypes
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___includedTypes_2;
	// System.Xml.Serialization.ReflectionHelper System.Xml.Serialization.XmlReflectionImporter::helper
	ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6 * ___helper_3;
	// System.Int32 System.Xml.Serialization.XmlReflectionImporter::arrayChoiceCount
	int32_t ___arrayChoiceCount_4;
	// System.Collections.ArrayList System.Xml.Serialization.XmlReflectionImporter::relatedMaps
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___relatedMaps_5;
	// System.Boolean System.Xml.Serialization.XmlReflectionImporter::allowPrivateTypes
	bool ___allowPrivateTypes_6;

public:
	inline static int32_t get_offset_of_initialDefaultNamespace_0() { return static_cast<int32_t>(offsetof(XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A, ___initialDefaultNamespace_0)); }
	inline String_t* get_initialDefaultNamespace_0() const { return ___initialDefaultNamespace_0; }
	inline String_t** get_address_of_initialDefaultNamespace_0() { return &___initialDefaultNamespace_0; }
	inline void set_initialDefaultNamespace_0(String_t* value)
	{
		___initialDefaultNamespace_0 = value;
		Il2CppCodeGenWriteBarrier((&___initialDefaultNamespace_0), value);
	}

	inline static int32_t get_offset_of_attributeOverrides_1() { return static_cast<int32_t>(offsetof(XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A, ___attributeOverrides_1)); }
	inline XmlAttributeOverrides_tE2AE30774D1F8B3860BFE1A8B6233970E88C7F7E * get_attributeOverrides_1() const { return ___attributeOverrides_1; }
	inline XmlAttributeOverrides_tE2AE30774D1F8B3860BFE1A8B6233970E88C7F7E ** get_address_of_attributeOverrides_1() { return &___attributeOverrides_1; }
	inline void set_attributeOverrides_1(XmlAttributeOverrides_tE2AE30774D1F8B3860BFE1A8B6233970E88C7F7E * value)
	{
		___attributeOverrides_1 = value;
		Il2CppCodeGenWriteBarrier((&___attributeOverrides_1), value);
	}

	inline static int32_t get_offset_of_includedTypes_2() { return static_cast<int32_t>(offsetof(XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A, ___includedTypes_2)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_includedTypes_2() const { return ___includedTypes_2; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_includedTypes_2() { return &___includedTypes_2; }
	inline void set_includedTypes_2(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___includedTypes_2 = value;
		Il2CppCodeGenWriteBarrier((&___includedTypes_2), value);
	}

	inline static int32_t get_offset_of_helper_3() { return static_cast<int32_t>(offsetof(XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A, ___helper_3)); }
	inline ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6 * get_helper_3() const { return ___helper_3; }
	inline ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6 ** get_address_of_helper_3() { return &___helper_3; }
	inline void set_helper_3(ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6 * value)
	{
		___helper_3 = value;
		Il2CppCodeGenWriteBarrier((&___helper_3), value);
	}

	inline static int32_t get_offset_of_arrayChoiceCount_4() { return static_cast<int32_t>(offsetof(XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A, ___arrayChoiceCount_4)); }
	inline int32_t get_arrayChoiceCount_4() const { return ___arrayChoiceCount_4; }
	inline int32_t* get_address_of_arrayChoiceCount_4() { return &___arrayChoiceCount_4; }
	inline void set_arrayChoiceCount_4(int32_t value)
	{
		___arrayChoiceCount_4 = value;
	}

	inline static int32_t get_offset_of_relatedMaps_5() { return static_cast<int32_t>(offsetof(XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A, ___relatedMaps_5)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_relatedMaps_5() const { return ___relatedMaps_5; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_relatedMaps_5() { return &___relatedMaps_5; }
	inline void set_relatedMaps_5(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___relatedMaps_5 = value;
		Il2CppCodeGenWriteBarrier((&___relatedMaps_5), value);
	}

	inline static int32_t get_offset_of_allowPrivateTypes_6() { return static_cast<int32_t>(offsetof(XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A, ___allowPrivateTypes_6)); }
	inline bool get_allowPrivateTypes_6() const { return ___allowPrivateTypes_6; }
	inline bool* get_address_of_allowPrivateTypes_6() { return &___allowPrivateTypes_6; }
	inline void set_allowPrivateTypes_6(bool value)
	{
		___allowPrivateTypes_6 = value;
	}
};

struct XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A_StaticFields
{
public:
	// System.String System.Xml.Serialization.XmlReflectionImporter::errSimple
	String_t* ___errSimple_7;
	// System.String System.Xml.Serialization.XmlReflectionImporter::errSimple2
	String_t* ___errSimple2_8;

public:
	inline static int32_t get_offset_of_errSimple_7() { return static_cast<int32_t>(offsetof(XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A_StaticFields, ___errSimple_7)); }
	inline String_t* get_errSimple_7() const { return ___errSimple_7; }
	inline String_t** get_address_of_errSimple_7() { return &___errSimple_7; }
	inline void set_errSimple_7(String_t* value)
	{
		___errSimple_7 = value;
		Il2CppCodeGenWriteBarrier((&___errSimple_7), value);
	}

	inline static int32_t get_offset_of_errSimple2_8() { return static_cast<int32_t>(offsetof(XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A_StaticFields, ___errSimple2_8)); }
	inline String_t* get_errSimple2_8() const { return ___errSimple2_8; }
	inline String_t** get_address_of_errSimple2_8() { return &___errSimple2_8; }
	inline void set_errSimple2_8(String_t* value)
	{
		___errSimple2_8 = value;
		Il2CppCodeGenWriteBarrier((&___errSimple2_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREFLECTIONIMPORTER_T01142C73A76F11C3781558BEE362720BFDDDED3A_H
#ifndef U3CU3EC_T5DDAAEFA8D571BE991743E9ADABA18D78665921F_H
#define U3CU3EC_T5DDAAEFA8D571BE991743E9ADABA18D78665921F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlReflectionImporter_<>c
struct  U3CU3Ec_t5DDAAEFA8D571BE991743E9ADABA18D78665921F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t5DDAAEFA8D571BE991743E9ADABA18D78665921F_StaticFields
{
public:
	// System.Xml.Serialization.XmlReflectionImporter_<>c System.Xml.Serialization.XmlReflectionImporter_<>c::<>9
	U3CU3Ec_t5DDAAEFA8D571BE991743E9ADABA18D78665921F * ___U3CU3E9_0;
	// System.Comparison`1<System.Xml.Serialization.XmlReflectionMember> System.Xml.Serialization.XmlReflectionImporter_<>c::<>9__28_0
	Comparison_1_t60DBA8E81BD9729FE925CE66DAF18BDE1233E76D * ___U3CU3E9__28_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5DDAAEFA8D571BE991743E9ADABA18D78665921F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t5DDAAEFA8D571BE991743E9ADABA18D78665921F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t5DDAAEFA8D571BE991743E9ADABA18D78665921F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t5DDAAEFA8D571BE991743E9ADABA18D78665921F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__28_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5DDAAEFA8D571BE991743E9ADABA18D78665921F_StaticFields, ___U3CU3E9__28_0_1)); }
	inline Comparison_1_t60DBA8E81BD9729FE925CE66DAF18BDE1233E76D * get_U3CU3E9__28_0_1() const { return ___U3CU3E9__28_0_1; }
	inline Comparison_1_t60DBA8E81BD9729FE925CE66DAF18BDE1233E76D ** get_address_of_U3CU3E9__28_0_1() { return &___U3CU3E9__28_0_1; }
	inline void set_U3CU3E9__28_0_1(Comparison_1_t60DBA8E81BD9729FE925CE66DAF18BDE1233E76D * value)
	{
		___U3CU3E9__28_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__28_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T5DDAAEFA8D571BE991743E9ADABA18D78665921F_H
#ifndef XMLREFLECTIONMEMBER_T126EB4F0E44820A989FFB6F076234CDDB43D8B2D_H
#define XMLREFLECTIONMEMBER_T126EB4F0E44820A989FFB6F076234CDDB43D8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlReflectionMember
struct  XmlReflectionMember_t126EB4F0E44820A989FFB6F076234CDDB43D8B2D  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Serialization.XmlReflectionMember::isReturnValue
	bool ___isReturnValue_0;
	// System.String System.Xml.Serialization.XmlReflectionMember::memberName
	String_t* ___memberName_1;
	// System.Type System.Xml.Serialization.XmlReflectionMember::memberType
	Type_t * ___memberType_2;
	// System.Xml.Serialization.XmlAttributes System.Xml.Serialization.XmlReflectionMember::xmlAttributes
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A * ___xmlAttributes_3;
	// System.Type System.Xml.Serialization.XmlReflectionMember::declaringType
	Type_t * ___declaringType_4;

public:
	inline static int32_t get_offset_of_isReturnValue_0() { return static_cast<int32_t>(offsetof(XmlReflectionMember_t126EB4F0E44820A989FFB6F076234CDDB43D8B2D, ___isReturnValue_0)); }
	inline bool get_isReturnValue_0() const { return ___isReturnValue_0; }
	inline bool* get_address_of_isReturnValue_0() { return &___isReturnValue_0; }
	inline void set_isReturnValue_0(bool value)
	{
		___isReturnValue_0 = value;
	}

	inline static int32_t get_offset_of_memberName_1() { return static_cast<int32_t>(offsetof(XmlReflectionMember_t126EB4F0E44820A989FFB6F076234CDDB43D8B2D, ___memberName_1)); }
	inline String_t* get_memberName_1() const { return ___memberName_1; }
	inline String_t** get_address_of_memberName_1() { return &___memberName_1; }
	inline void set_memberName_1(String_t* value)
	{
		___memberName_1 = value;
		Il2CppCodeGenWriteBarrier((&___memberName_1), value);
	}

	inline static int32_t get_offset_of_memberType_2() { return static_cast<int32_t>(offsetof(XmlReflectionMember_t126EB4F0E44820A989FFB6F076234CDDB43D8B2D, ___memberType_2)); }
	inline Type_t * get_memberType_2() const { return ___memberType_2; }
	inline Type_t ** get_address_of_memberType_2() { return &___memberType_2; }
	inline void set_memberType_2(Type_t * value)
	{
		___memberType_2 = value;
		Il2CppCodeGenWriteBarrier((&___memberType_2), value);
	}

	inline static int32_t get_offset_of_xmlAttributes_3() { return static_cast<int32_t>(offsetof(XmlReflectionMember_t126EB4F0E44820A989FFB6F076234CDDB43D8B2D, ___xmlAttributes_3)); }
	inline XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A * get_xmlAttributes_3() const { return ___xmlAttributes_3; }
	inline XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A ** get_address_of_xmlAttributes_3() { return &___xmlAttributes_3; }
	inline void set_xmlAttributes_3(XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A * value)
	{
		___xmlAttributes_3 = value;
		Il2CppCodeGenWriteBarrier((&___xmlAttributes_3), value);
	}

	inline static int32_t get_offset_of_declaringType_4() { return static_cast<int32_t>(offsetof(XmlReflectionMember_t126EB4F0E44820A989FFB6F076234CDDB43D8B2D, ___declaringType_4)); }
	inline Type_t * get_declaringType_4() const { return ___declaringType_4; }
	inline Type_t ** get_address_of_declaringType_4() { return &___declaringType_4; }
	inline void set_declaringType_4(Type_t * value)
	{
		___declaringType_4 = value;
		Il2CppCodeGenWriteBarrier((&___declaringType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREFLECTIONMEMBER_T126EB4F0E44820A989FFB6F076234CDDB43D8B2D_H
#ifndef XMLSERIALIZATIONGENERATEDCODE_TD121073E4460FCF7F6B549E4621B0CB971D2E6CE_H
#define XMLSERIALIZATIONGENERATEDCODE_TD121073E4460FCF7F6B549E4621B0CB971D2E6CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationGeneratedCode
struct  XmlSerializationGeneratedCode_tD121073E4460FCF7F6B549E4621B0CB971D2E6CE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZATIONGENERATEDCODE_TD121073E4460FCF7F6B549E4621B0CB971D2E6CE_H
#ifndef WRITECALLBACKINFO_T20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC_H
#define WRITECALLBACKINFO_T20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationWriter_WriteCallbackInfo
struct  WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC  : public RuntimeObject
{
public:
	// System.Type System.Xml.Serialization.XmlSerializationWriter_WriteCallbackInfo::Type
	Type_t * ___Type_0;
	// System.String System.Xml.Serialization.XmlSerializationWriter_WriteCallbackInfo::TypeName
	String_t* ___TypeName_1;
	// System.String System.Xml.Serialization.XmlSerializationWriter_WriteCallbackInfo::TypeNs
	String_t* ___TypeNs_2;
	// System.Xml.Serialization.XmlSerializationWriteCallback System.Xml.Serialization.XmlSerializationWriter_WriteCallbackInfo::Callback
	XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA * ___Callback_3;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}

	inline static int32_t get_offset_of_TypeNs_2() { return static_cast<int32_t>(offsetof(WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC, ___TypeNs_2)); }
	inline String_t* get_TypeNs_2() const { return ___TypeNs_2; }
	inline String_t** get_address_of_TypeNs_2() { return &___TypeNs_2; }
	inline void set_TypeNs_2(String_t* value)
	{
		___TypeNs_2 = value;
		Il2CppCodeGenWriteBarrier((&___TypeNs_2), value);
	}

	inline static int32_t get_offset_of_Callback_3() { return static_cast<int32_t>(offsetof(WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC, ___Callback_3)); }
	inline XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA * get_Callback_3() const { return ___Callback_3; }
	inline XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA ** get_address_of_Callback_3() { return &___Callback_3; }
	inline void set_Callback_3(XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA * value)
	{
		___Callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITECALLBACKINFO_T20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC_H
#ifndef CALLBACKINFO_T6FC419FB131647181747C58C4AA7642E18071292_H
#define CALLBACKINFO_T6FC419FB131647181747C58C4AA7642E18071292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationWriterInterpreter_CallbackInfo
struct  CallbackInfo_t6FC419FB131647181747C58C4AA7642E18071292  : public RuntimeObject
{
public:
	// System.Xml.Serialization.XmlSerializationWriterInterpreter System.Xml.Serialization.XmlSerializationWriterInterpreter_CallbackInfo::_swi
	XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221 * ____swi_0;
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.XmlSerializationWriterInterpreter_CallbackInfo::_typeMap
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * ____typeMap_1;

public:
	inline static int32_t get_offset_of__swi_0() { return static_cast<int32_t>(offsetof(CallbackInfo_t6FC419FB131647181747C58C4AA7642E18071292, ____swi_0)); }
	inline XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221 * get__swi_0() const { return ____swi_0; }
	inline XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221 ** get_address_of__swi_0() { return &____swi_0; }
	inline void set__swi_0(XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221 * value)
	{
		____swi_0 = value;
		Il2CppCodeGenWriteBarrier((&____swi_0), value);
	}

	inline static int32_t get_offset_of__typeMap_1() { return static_cast<int32_t>(offsetof(CallbackInfo_t6FC419FB131647181747C58C4AA7642E18071292, ____typeMap_1)); }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * get__typeMap_1() const { return ____typeMap_1; }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA ** get_address_of__typeMap_1() { return &____typeMap_1; }
	inline void set__typeMap_1(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * value)
	{
		____typeMap_1 = value;
		Il2CppCodeGenWriteBarrier((&____typeMap_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKINFO_T6FC419FB131647181747C58C4AA7642E18071292_H
#ifndef XMLSERIALIZER_T01B5C784057A14663B4041A53328764327555D5C_H
#define XMLSERIALIZER_T01B5C784057A14663B4041A53328764327555D5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializer
struct  XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Serialization.XmlSerializer::customSerializer
	bool ___customSerializer_4;
	// System.Xml.Serialization.XmlMapping System.Xml.Serialization.XmlSerializer::typeMapping
	XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * ___typeMapping_5;
	// System.Xml.Serialization.XmlSerializer_SerializerData System.Xml.Serialization.XmlSerializer::serializerData
	SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8 * ___serializerData_6;

public:
	inline static int32_t get_offset_of_customSerializer_4() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C, ___customSerializer_4)); }
	inline bool get_customSerializer_4() const { return ___customSerializer_4; }
	inline bool* get_address_of_customSerializer_4() { return &___customSerializer_4; }
	inline void set_customSerializer_4(bool value)
	{
		___customSerializer_4 = value;
	}

	inline static int32_t get_offset_of_typeMapping_5() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C, ___typeMapping_5)); }
	inline XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * get_typeMapping_5() const { return ___typeMapping_5; }
	inline XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 ** get_address_of_typeMapping_5() { return &___typeMapping_5; }
	inline void set_typeMapping_5(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * value)
	{
		___typeMapping_5 = value;
		Il2CppCodeGenWriteBarrier((&___typeMapping_5), value);
	}

	inline static int32_t get_offset_of_serializerData_6() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C, ___serializerData_6)); }
	inline SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8 * get_serializerData_6() const { return ___serializerData_6; }
	inline SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8 ** get_address_of_serializerData_6() { return &___serializerData_6; }
	inline void set_serializerData_6(SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8 * value)
	{
		___serializerData_6 = value;
		Il2CppCodeGenWriteBarrier((&___serializerData_6), value);
	}
};

struct XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields
{
public:
	// System.Int32 System.Xml.Serialization.XmlSerializer::generationThreshold
	int32_t ___generationThreshold_0;
	// System.Boolean System.Xml.Serialization.XmlSerializer::backgroundGeneration
	bool ___backgroundGeneration_1;
	// System.Boolean System.Xml.Serialization.XmlSerializer::deleteTempFiles
	bool ___deleteTempFiles_2;
	// System.Boolean System.Xml.Serialization.XmlSerializer::generatorFallback
	bool ___generatorFallback_3;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializer::serializerTypes
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___serializerTypes_7;
	// System.Text.Encoding System.Xml.Serialization.XmlSerializer::DefaultEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___DefaultEncoding_8;

public:
	inline static int32_t get_offset_of_generationThreshold_0() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields, ___generationThreshold_0)); }
	inline int32_t get_generationThreshold_0() const { return ___generationThreshold_0; }
	inline int32_t* get_address_of_generationThreshold_0() { return &___generationThreshold_0; }
	inline void set_generationThreshold_0(int32_t value)
	{
		___generationThreshold_0 = value;
	}

	inline static int32_t get_offset_of_backgroundGeneration_1() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields, ___backgroundGeneration_1)); }
	inline bool get_backgroundGeneration_1() const { return ___backgroundGeneration_1; }
	inline bool* get_address_of_backgroundGeneration_1() { return &___backgroundGeneration_1; }
	inline void set_backgroundGeneration_1(bool value)
	{
		___backgroundGeneration_1 = value;
	}

	inline static int32_t get_offset_of_deleteTempFiles_2() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields, ___deleteTempFiles_2)); }
	inline bool get_deleteTempFiles_2() const { return ___deleteTempFiles_2; }
	inline bool* get_address_of_deleteTempFiles_2() { return &___deleteTempFiles_2; }
	inline void set_deleteTempFiles_2(bool value)
	{
		___deleteTempFiles_2 = value;
	}

	inline static int32_t get_offset_of_generatorFallback_3() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields, ___generatorFallback_3)); }
	inline bool get_generatorFallback_3() const { return ___generatorFallback_3; }
	inline bool* get_address_of_generatorFallback_3() { return &___generatorFallback_3; }
	inline void set_generatorFallback_3(bool value)
	{
		___generatorFallback_3 = value;
	}

	inline static int32_t get_offset_of_serializerTypes_7() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields, ___serializerTypes_7)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_serializerTypes_7() const { return ___serializerTypes_7; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_serializerTypes_7() { return &___serializerTypes_7; }
	inline void set_serializerTypes_7(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___serializerTypes_7 = value;
		Il2CppCodeGenWriteBarrier((&___serializerTypes_7), value);
	}

	inline static int32_t get_offset_of_DefaultEncoding_8() { return static_cast<int32_t>(offsetof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields, ___DefaultEncoding_8)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_DefaultEncoding_8() const { return ___DefaultEncoding_8; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_DefaultEncoding_8() { return &___DefaultEncoding_8; }
	inline void set_DefaultEncoding_8(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___DefaultEncoding_8 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultEncoding_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZER_T01B5C784057A14663B4041A53328764327555D5C_H
#ifndef SERIALIZERDATA_TFD8E75C5BC50CD6C3A3F382789E88E422381A7A8_H
#define SERIALIZERDATA_TFD8E75C5BC50CD6C3A3F382789E88E422381A7A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializer_SerializerData
struct  SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8  : public RuntimeObject
{
public:
	// System.Type System.Xml.Serialization.XmlSerializer_SerializerData::WriterType
	Type_t * ___WriterType_0;
	// System.Reflection.MethodInfo System.Xml.Serialization.XmlSerializer_SerializerData::WriterMethod
	MethodInfo_t * ___WriterMethod_1;
	// System.Xml.Serialization.XmlSerializerImplementation System.Xml.Serialization.XmlSerializer_SerializerData::Implementation
	XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D * ___Implementation_2;

public:
	inline static int32_t get_offset_of_WriterType_0() { return static_cast<int32_t>(offsetof(SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8, ___WriterType_0)); }
	inline Type_t * get_WriterType_0() const { return ___WriterType_0; }
	inline Type_t ** get_address_of_WriterType_0() { return &___WriterType_0; }
	inline void set_WriterType_0(Type_t * value)
	{
		___WriterType_0 = value;
		Il2CppCodeGenWriteBarrier((&___WriterType_0), value);
	}

	inline static int32_t get_offset_of_WriterMethod_1() { return static_cast<int32_t>(offsetof(SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8, ___WriterMethod_1)); }
	inline MethodInfo_t * get_WriterMethod_1() const { return ___WriterMethod_1; }
	inline MethodInfo_t ** get_address_of_WriterMethod_1() { return &___WriterMethod_1; }
	inline void set_WriterMethod_1(MethodInfo_t * value)
	{
		___WriterMethod_1 = value;
		Il2CppCodeGenWriteBarrier((&___WriterMethod_1), value);
	}

	inline static int32_t get_offset_of_Implementation_2() { return static_cast<int32_t>(offsetof(SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8, ___Implementation_2)); }
	inline XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D * get_Implementation_2() const { return ___Implementation_2; }
	inline XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D ** get_address_of_Implementation_2() { return &___Implementation_2; }
	inline void set_Implementation_2(XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D * value)
	{
		___Implementation_2 = value;
		Il2CppCodeGenWriteBarrier((&___Implementation_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZERDATA_TFD8E75C5BC50CD6C3A3F382789E88E422381A7A8_H
#ifndef XMLSERIALIZERIMPLEMENTATION_TF78BC93462D4003D83EAA821331747A0624F837D_H
#define XMLSERIALIZERIMPLEMENTATION_TF78BC93462D4003D83EAA821331747A0624F837D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializerImplementation
struct  XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZERIMPLEMENTATION_TF78BC93462D4003D83EAA821331747A0624F837D_H
#ifndef XMLSERIALIZERNAMESPACES_T31E17AE7CF53591901A576C48D1E446BB985676F_H
#define XMLSERIALIZERNAMESPACES_T31E17AE7CF53591901A576C48D1E446BB985676F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializerNamespaces
struct  XmlSerializerNamespaces_t31E17AE7CF53591901A576C48D1E446BB985676F  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializerNamespaces::namespaces
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___namespaces_0;

public:
	inline static int32_t get_offset_of_namespaces_0() { return static_cast<int32_t>(offsetof(XmlSerializerNamespaces_t31E17AE7CF53591901A576C48D1E446BB985676F, ___namespaces_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_namespaces_0() const { return ___namespaces_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_namespaces_0() { return &___namespaces_0; }
	inline void set_namespaces_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___namespaces_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZERNAMESPACES_T31E17AE7CF53591901A576C48D1E446BB985676F_H
#ifndef XMLTYPEMAPMEMBER_T8272D1E62F842F97D749F6FFC6B4AE57754C914D_H
#define XMLTYPEMAPMEMBER_T8272D1E62F842F97D749F6FFC6B4AE57754C914D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMember
struct  XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D  : public RuntimeObject
{
public:
	// System.String System.Xml.Serialization.XmlTypeMapMember::_name
	String_t* ____name_0;
	// System.Int32 System.Xml.Serialization.XmlTypeMapMember::_index
	int32_t ____index_1;
	// System.Int32 System.Xml.Serialization.XmlTypeMapMember::_globalIndex
	int32_t ____globalIndex_2;
	// System.Int32 System.Xml.Serialization.XmlTypeMapMember::_specifiedGlobalIndex
	int32_t ____specifiedGlobalIndex_3;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.XmlTypeMapMember::_typeData
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * ____typeData_4;
	// System.Reflection.MemberInfo System.Xml.Serialization.XmlTypeMapMember::_member
	MemberInfo_t * ____member_5;
	// System.Reflection.MemberInfo System.Xml.Serialization.XmlTypeMapMember::_specifiedMember
	MemberInfo_t * ____specifiedMember_6;
	// System.Reflection.MethodInfo System.Xml.Serialization.XmlTypeMapMember::_shouldSerialize
	MethodInfo_t * ____shouldSerialize_7;
	// System.Object System.Xml.Serialization.XmlTypeMapMember::_defaultValue
	RuntimeObject * ____defaultValue_8;
	// System.Int32 System.Xml.Serialization.XmlTypeMapMember::_flags
	int32_t ____flags_9;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__globalIndex_2() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____globalIndex_2)); }
	inline int32_t get__globalIndex_2() const { return ____globalIndex_2; }
	inline int32_t* get_address_of__globalIndex_2() { return &____globalIndex_2; }
	inline void set__globalIndex_2(int32_t value)
	{
		____globalIndex_2 = value;
	}

	inline static int32_t get_offset_of__specifiedGlobalIndex_3() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____specifiedGlobalIndex_3)); }
	inline int32_t get__specifiedGlobalIndex_3() const { return ____specifiedGlobalIndex_3; }
	inline int32_t* get_address_of__specifiedGlobalIndex_3() { return &____specifiedGlobalIndex_3; }
	inline void set__specifiedGlobalIndex_3(int32_t value)
	{
		____specifiedGlobalIndex_3 = value;
	}

	inline static int32_t get_offset_of__typeData_4() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____typeData_4)); }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * get__typeData_4() const { return ____typeData_4; }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 ** get_address_of__typeData_4() { return &____typeData_4; }
	inline void set__typeData_4(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * value)
	{
		____typeData_4 = value;
		Il2CppCodeGenWriteBarrier((&____typeData_4), value);
	}

	inline static int32_t get_offset_of__member_5() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____member_5)); }
	inline MemberInfo_t * get__member_5() const { return ____member_5; }
	inline MemberInfo_t ** get_address_of__member_5() { return &____member_5; }
	inline void set__member_5(MemberInfo_t * value)
	{
		____member_5 = value;
		Il2CppCodeGenWriteBarrier((&____member_5), value);
	}

	inline static int32_t get_offset_of__specifiedMember_6() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____specifiedMember_6)); }
	inline MemberInfo_t * get__specifiedMember_6() const { return ____specifiedMember_6; }
	inline MemberInfo_t ** get_address_of__specifiedMember_6() { return &____specifiedMember_6; }
	inline void set__specifiedMember_6(MemberInfo_t * value)
	{
		____specifiedMember_6 = value;
		Il2CppCodeGenWriteBarrier((&____specifiedMember_6), value);
	}

	inline static int32_t get_offset_of__shouldSerialize_7() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____shouldSerialize_7)); }
	inline MethodInfo_t * get__shouldSerialize_7() const { return ____shouldSerialize_7; }
	inline MethodInfo_t ** get_address_of__shouldSerialize_7() { return &____shouldSerialize_7; }
	inline void set__shouldSerialize_7(MethodInfo_t * value)
	{
		____shouldSerialize_7 = value;
		Il2CppCodeGenWriteBarrier((&____shouldSerialize_7), value);
	}

	inline static int32_t get_offset_of__defaultValue_8() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____defaultValue_8)); }
	inline RuntimeObject * get__defaultValue_8() const { return ____defaultValue_8; }
	inline RuntimeObject ** get_address_of__defaultValue_8() { return &____defaultValue_8; }
	inline void set__defaultValue_8(RuntimeObject * value)
	{
		____defaultValue_8 = value;
		Il2CppCodeGenWriteBarrier((&____defaultValue_8), value);
	}

	inline static int32_t get_offset_of__flags_9() { return static_cast<int32_t>(offsetof(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D, ____flags_9)); }
	inline int32_t get__flags_9() const { return ____flags_9; }
	inline int32_t* get_address_of__flags_9() { return &____flags_9; }
	inline void set__flags_9(int32_t value)
	{
		____flags_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBER_T8272D1E62F842F97D749F6FFC6B4AE57754C914D_H
#ifndef XPATHDOCUMENT_TD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321_H
#define XPATHDOCUMENT_TD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathDocument
struct  XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321  : public RuntimeObject
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] System.Xml.XPath.XPathDocument::pageXmlNmsp
	XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* ___pageXmlNmsp_0;
	// System.Int32 System.Xml.XPath.XPathDocument::idxXmlNmsp
	int32_t ___idxXmlNmsp_1;
	// System.Xml.XmlNameTable System.Xml.XPath.XPathDocument::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_2;
	// System.Collections.Generic.Dictionary`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef> System.Xml.XPath.XPathDocument::mapNmsp
	Dictionary_2_t0F6B4307B0F9876BDCB2062F814BAC1D8ABA9646 * ___mapNmsp_3;

public:
	inline static int32_t get_offset_of_pageXmlNmsp_0() { return static_cast<int32_t>(offsetof(XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321, ___pageXmlNmsp_0)); }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* get_pageXmlNmsp_0() const { return ___pageXmlNmsp_0; }
	inline XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B** get_address_of_pageXmlNmsp_0() { return &___pageXmlNmsp_0; }
	inline void set_pageXmlNmsp_0(XPathNodeU5BU5D_tBC351C5172049794ED3550500C082D4E1F1D5A8B* value)
	{
		___pageXmlNmsp_0 = value;
		Il2CppCodeGenWriteBarrier((&___pageXmlNmsp_0), value);
	}

	inline static int32_t get_offset_of_idxXmlNmsp_1() { return static_cast<int32_t>(offsetof(XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321, ___idxXmlNmsp_1)); }
	inline int32_t get_idxXmlNmsp_1() const { return ___idxXmlNmsp_1; }
	inline int32_t* get_address_of_idxXmlNmsp_1() { return &___idxXmlNmsp_1; }
	inline void set_idxXmlNmsp_1(int32_t value)
	{
		___idxXmlNmsp_1 = value;
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321, ___nameTable_2)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_mapNmsp_3() { return static_cast<int32_t>(offsetof(XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321, ___mapNmsp_3)); }
	inline Dictionary_2_t0F6B4307B0F9876BDCB2062F814BAC1D8ABA9646 * get_mapNmsp_3() const { return ___mapNmsp_3; }
	inline Dictionary_2_t0F6B4307B0F9876BDCB2062F814BAC1D8ABA9646 ** get_address_of_mapNmsp_3() { return &___mapNmsp_3; }
	inline void set_mapNmsp_3(Dictionary_2_t0F6B4307B0F9876BDCB2062F814BAC1D8ABA9646 * value)
	{
		___mapNmsp_3 = value;
		Il2CppCodeGenWriteBarrier((&___mapNmsp_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHDOCUMENT_TD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321_H
#ifndef XPATHITEM_TCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98_H
#define XPATHITEM_TCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_tCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_TCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98_H
#ifndef XPATHNAVIGATORKEYCOMPARER_T6A0E82BEC0BE42351DDB26EAA86333C11E0A9378_H
#define XPATHNAVIGATORKEYCOMPARER_T6A0E82BEC0BE42351DDB26EAA86333C11E0A9378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigatorKeyComparer
struct  XPathNavigatorKeyComparer_t6A0E82BEC0BE42351DDB26EAA86333C11E0A9378  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATORKEYCOMPARER_T6A0E82BEC0BE42351DDB26EAA86333C11E0A9378_H
#ifndef XMLRESOLVER_T93269F14B2F8750D040AED2FB7A303CE85016280_H
#define XMLRESOLVER_T93269F14B2F8750D040AED2FB7A303CE85016280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlResolver
struct  XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRESOLVER_T93269F14B2F8750D040AED2FB7A303CE85016280_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#define INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#define ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_defaultContextAction_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifndef CONFIGUREDTASKAWAITER_T27ECAA4B74502BD3AC7E68F47088D46DAA13D133_H
#define CONFIGUREDTASKAWAITER_T27ECAA4B74502BD3AC7E68F47088D46DAA13D133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter<System.IO.Stream>
struct  ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter::m_task
	Task_1_tAA7557E5A6A8B2C89E44907CB5B0502A618E9D01 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133, ___m_task_0)); }
	inline Task_1_tAA7557E5A6A8B2C89E44907CB5B0502A618E9D01 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_tAA7557E5A6A8B2C89E44907CB5B0502A618E9D01 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_tAA7557E5A6A8B2C89E44907CB5B0502A618E9D01 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGUREDTASKAWAITER_T27ECAA4B74502BD3AC7E68F47088D46DAA13D133_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef AUTOVALIDATOR_T348DF836BF1829DC3B8DBA19A6E0EFC1A6EE85EB_H
#define AUTOVALIDATOR_T348DF836BF1829DC3B8DBA19A6E0EFC1A6EE85EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AutoValidator
struct  AutoValidator_t348DF836BF1829DC3B8DBA19A6E0EFC1A6EE85EB  : public BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOVALIDATOR_T348DF836BF1829DC3B8DBA19A6E0EFC1A6EE85EB_H
#ifndef LOCATEDACTIVEAXIS_TCCF3C0E41A32AC342B9B40531789937908E510D5_H
#define LOCATEDACTIVEAXIS_TCCF3C0E41A32AC342B9B40531789937908E510D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.LocatedActiveAxis
struct  LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5  : public ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E
{
public:
	// System.Int32 System.Xml.Schema.LocatedActiveAxis::column
	int32_t ___column_4;
	// System.Boolean System.Xml.Schema.LocatedActiveAxis::isMatched
	bool ___isMatched_5;
	// System.Xml.Schema.KeySequence System.Xml.Schema.LocatedActiveAxis::Ks
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE * ___Ks_6;

public:
	inline static int32_t get_offset_of_column_4() { return static_cast<int32_t>(offsetof(LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5, ___column_4)); }
	inline int32_t get_column_4() const { return ___column_4; }
	inline int32_t* get_address_of_column_4() { return &___column_4; }
	inline void set_column_4(int32_t value)
	{
		___column_4 = value;
	}

	inline static int32_t get_offset_of_isMatched_5() { return static_cast<int32_t>(offsetof(LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5, ___isMatched_5)); }
	inline bool get_isMatched_5() const { return ___isMatched_5; }
	inline bool* get_address_of_isMatched_5() { return &___isMatched_5; }
	inline void set_isMatched_5(bool value)
	{
		___isMatched_5 = value;
	}

	inline static int32_t get_offset_of_Ks_6() { return static_cast<int32_t>(offsetof(LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5, ___Ks_6)); }
	inline KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE * get_Ks_6() const { return ___Ks_6; }
	inline KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE ** get_address_of_Ks_6() { return &___Ks_6; }
	inline void set_Ks_6(KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE * value)
	{
		___Ks_6 = value;
		Il2CppCodeGenWriteBarrier((&___Ks_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATEDACTIVEAXIS_TCCF3C0E41A32AC342B9B40531789937908E510D5_H
#ifndef SELECTORACTIVEAXIS_T7EA444AA4AD068A5F4A8C12A756792925AF59A64_H
#define SELECTORACTIVEAXIS_T7EA444AA4AD068A5F4A8C12A756792925AF59A64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SelectorActiveAxis
struct  SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64  : public ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E
{
public:
	// System.Xml.Schema.ConstraintStruct System.Xml.Schema.SelectorActiveAxis::cs
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54 * ___cs_4;
	// System.Collections.ArrayList System.Xml.Schema.SelectorActiveAxis::KSs
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___KSs_5;
	// System.Int32 System.Xml.Schema.SelectorActiveAxis::KSpointer
	int32_t ___KSpointer_6;

public:
	inline static int32_t get_offset_of_cs_4() { return static_cast<int32_t>(offsetof(SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64, ___cs_4)); }
	inline ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54 * get_cs_4() const { return ___cs_4; }
	inline ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54 ** get_address_of_cs_4() { return &___cs_4; }
	inline void set_cs_4(ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54 * value)
	{
		___cs_4 = value;
		Il2CppCodeGenWriteBarrier((&___cs_4), value);
	}

	inline static int32_t get_offset_of_KSs_5() { return static_cast<int32_t>(offsetof(SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64, ___KSs_5)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_KSs_5() const { return ___KSs_5; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_KSs_5() { return &___KSs_5; }
	inline void set_KSs_5(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___KSs_5 = value;
		Il2CppCodeGenWriteBarrier((&___KSs_5), value);
	}

	inline static int32_t get_offset_of_KSpointer_6() { return static_cast<int32_t>(offsetof(SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64, ___KSpointer_6)); }
	inline int32_t get_KSpointer_6() const { return ___KSpointer_6; }
	inline int32_t* get_address_of_KSpointer_6() { return &___KSpointer_6; }
	inline void set_KSpointer_6(int32_t value)
	{
		___KSpointer_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTORACTIVEAXIS_T7EA444AA4AD068A5F4A8C12A756792925AF59A64_H
#ifndef UPAEXCEPTION_TED69C4AC662CA80677ABA101231042E747107151_H
#define UPAEXCEPTION_TED69C4AC662CA80677ABA101231042E747107151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.UpaException
struct  UpaException_tED69C4AC662CA80677ABA101231042E747107151  : public Exception_t
{
public:
	// System.Object System.Xml.Schema.UpaException::particle1
	RuntimeObject * ___particle1_17;
	// System.Object System.Xml.Schema.UpaException::particle2
	RuntimeObject * ___particle2_18;

public:
	inline static int32_t get_offset_of_particle1_17() { return static_cast<int32_t>(offsetof(UpaException_tED69C4AC662CA80677ABA101231042E747107151, ___particle1_17)); }
	inline RuntimeObject * get_particle1_17() const { return ___particle1_17; }
	inline RuntimeObject ** get_address_of_particle1_17() { return &___particle1_17; }
	inline void set_particle1_17(RuntimeObject * value)
	{
		___particle1_17 = value;
		Il2CppCodeGenWriteBarrier((&___particle1_17), value);
	}

	inline static int32_t get_offset_of_particle2_18() { return static_cast<int32_t>(offsetof(UpaException_tED69C4AC662CA80677ABA101231042E747107151, ___particle2_18)); }
	inline RuntimeObject * get_particle2_18() const { return ___particle2_18; }
	inline RuntimeObject ** get_address_of_particle2_18() { return &___particle2_18; }
	inline void set_particle2_18(RuntimeObject * value)
	{
		___particle2_18 = value;
		Il2CppCodeGenWriteBarrier((&___particle2_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPAEXCEPTION_TED69C4AC662CA80677ABA101231042E747107151_H
#ifndef CLASSMAP_T7673ADE61E787987E671A668712C12D77FAAC6DA_H
#define CLASSMAP_T7673ADE61E787987E671A668712C12D77FAAC6DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.ClassMap
struct  ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA  : public ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.ClassMap::_elements
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____elements_0;
	// System.Collections.ArrayList System.Xml.Serialization.ClassMap::_elementMembers
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____elementMembers_1;
	// System.Collections.Hashtable System.Xml.Serialization.ClassMap::_attributeMembers
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____attributeMembers_2;
	// System.Xml.Serialization.XmlTypeMapMemberAttribute[] System.Xml.Serialization.ClassMap::_attributeMembersArray
	XmlTypeMapMemberAttributeU5BU5D_t1A3EF531DFF7CF757326012C417167F79DDE0D10* ____attributeMembersArray_3;
	// System.Collections.ArrayList System.Xml.Serialization.ClassMap::_flatLists
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____flatLists_4;
	// System.Collections.ArrayList System.Xml.Serialization.ClassMap::_allMembers
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____allMembers_5;
	// System.Collections.ArrayList System.Xml.Serialization.ClassMap::_membersWithDefault
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____membersWithDefault_6;
	// System.Collections.ArrayList System.Xml.Serialization.ClassMap::_listMembers
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____listMembers_7;
	// System.Xml.Serialization.XmlTypeMapMemberAnyElement System.Xml.Serialization.ClassMap::_defaultAnyElement
	XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472 * ____defaultAnyElement_8;
	// System.Xml.Serialization.XmlTypeMapMemberAnyAttribute System.Xml.Serialization.ClassMap::_defaultAnyAttribute
	XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE * ____defaultAnyAttribute_9;
	// System.Xml.Serialization.XmlTypeMapMemberNamespaces System.Xml.Serialization.ClassMap::_namespaceDeclarations
	XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43 * ____namespaceDeclarations_10;
	// System.Xml.Serialization.XmlTypeMapMember System.Xml.Serialization.ClassMap::_xmlTextCollector
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * ____xmlTextCollector_11;
	// System.Xml.Serialization.XmlTypeMapMember System.Xml.Serialization.ClassMap::_returnMember
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * ____returnMember_12;
	// System.Boolean System.Xml.Serialization.ClassMap::_ignoreMemberNamespace
	bool ____ignoreMemberNamespace_13;
	// System.Boolean System.Xml.Serialization.ClassMap::_canBeSimpleType
	bool ____canBeSimpleType_14;

public:
	inline static int32_t get_offset_of__elements_0() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____elements_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__elements_0() const { return ____elements_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__elements_0() { return &____elements_0; }
	inline void set__elements_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____elements_0 = value;
		Il2CppCodeGenWriteBarrier((&____elements_0), value);
	}

	inline static int32_t get_offset_of__elementMembers_1() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____elementMembers_1)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__elementMembers_1() const { return ____elementMembers_1; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__elementMembers_1() { return &____elementMembers_1; }
	inline void set__elementMembers_1(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____elementMembers_1 = value;
		Il2CppCodeGenWriteBarrier((&____elementMembers_1), value);
	}

	inline static int32_t get_offset_of__attributeMembers_2() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____attributeMembers_2)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__attributeMembers_2() const { return ____attributeMembers_2; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__attributeMembers_2() { return &____attributeMembers_2; }
	inline void set__attributeMembers_2(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____attributeMembers_2 = value;
		Il2CppCodeGenWriteBarrier((&____attributeMembers_2), value);
	}

	inline static int32_t get_offset_of__attributeMembersArray_3() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____attributeMembersArray_3)); }
	inline XmlTypeMapMemberAttributeU5BU5D_t1A3EF531DFF7CF757326012C417167F79DDE0D10* get__attributeMembersArray_3() const { return ____attributeMembersArray_3; }
	inline XmlTypeMapMemberAttributeU5BU5D_t1A3EF531DFF7CF757326012C417167F79DDE0D10** get_address_of__attributeMembersArray_3() { return &____attributeMembersArray_3; }
	inline void set__attributeMembersArray_3(XmlTypeMapMemberAttributeU5BU5D_t1A3EF531DFF7CF757326012C417167F79DDE0D10* value)
	{
		____attributeMembersArray_3 = value;
		Il2CppCodeGenWriteBarrier((&____attributeMembersArray_3), value);
	}

	inline static int32_t get_offset_of__flatLists_4() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____flatLists_4)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__flatLists_4() const { return ____flatLists_4; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__flatLists_4() { return &____flatLists_4; }
	inline void set__flatLists_4(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____flatLists_4 = value;
		Il2CppCodeGenWriteBarrier((&____flatLists_4), value);
	}

	inline static int32_t get_offset_of__allMembers_5() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____allMembers_5)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__allMembers_5() const { return ____allMembers_5; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__allMembers_5() { return &____allMembers_5; }
	inline void set__allMembers_5(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____allMembers_5 = value;
		Il2CppCodeGenWriteBarrier((&____allMembers_5), value);
	}

	inline static int32_t get_offset_of__membersWithDefault_6() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____membersWithDefault_6)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__membersWithDefault_6() const { return ____membersWithDefault_6; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__membersWithDefault_6() { return &____membersWithDefault_6; }
	inline void set__membersWithDefault_6(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____membersWithDefault_6 = value;
		Il2CppCodeGenWriteBarrier((&____membersWithDefault_6), value);
	}

	inline static int32_t get_offset_of__listMembers_7() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____listMembers_7)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__listMembers_7() const { return ____listMembers_7; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__listMembers_7() { return &____listMembers_7; }
	inline void set__listMembers_7(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____listMembers_7 = value;
		Il2CppCodeGenWriteBarrier((&____listMembers_7), value);
	}

	inline static int32_t get_offset_of__defaultAnyElement_8() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____defaultAnyElement_8)); }
	inline XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472 * get__defaultAnyElement_8() const { return ____defaultAnyElement_8; }
	inline XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472 ** get_address_of__defaultAnyElement_8() { return &____defaultAnyElement_8; }
	inline void set__defaultAnyElement_8(XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472 * value)
	{
		____defaultAnyElement_8 = value;
		Il2CppCodeGenWriteBarrier((&____defaultAnyElement_8), value);
	}

	inline static int32_t get_offset_of__defaultAnyAttribute_9() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____defaultAnyAttribute_9)); }
	inline XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE * get__defaultAnyAttribute_9() const { return ____defaultAnyAttribute_9; }
	inline XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE ** get_address_of__defaultAnyAttribute_9() { return &____defaultAnyAttribute_9; }
	inline void set__defaultAnyAttribute_9(XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE * value)
	{
		____defaultAnyAttribute_9 = value;
		Il2CppCodeGenWriteBarrier((&____defaultAnyAttribute_9), value);
	}

	inline static int32_t get_offset_of__namespaceDeclarations_10() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____namespaceDeclarations_10)); }
	inline XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43 * get__namespaceDeclarations_10() const { return ____namespaceDeclarations_10; }
	inline XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43 ** get_address_of__namespaceDeclarations_10() { return &____namespaceDeclarations_10; }
	inline void set__namespaceDeclarations_10(XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43 * value)
	{
		____namespaceDeclarations_10 = value;
		Il2CppCodeGenWriteBarrier((&____namespaceDeclarations_10), value);
	}

	inline static int32_t get_offset_of__xmlTextCollector_11() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____xmlTextCollector_11)); }
	inline XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * get__xmlTextCollector_11() const { return ____xmlTextCollector_11; }
	inline XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D ** get_address_of__xmlTextCollector_11() { return &____xmlTextCollector_11; }
	inline void set__xmlTextCollector_11(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * value)
	{
		____xmlTextCollector_11 = value;
		Il2CppCodeGenWriteBarrier((&____xmlTextCollector_11), value);
	}

	inline static int32_t get_offset_of__returnMember_12() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____returnMember_12)); }
	inline XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * get__returnMember_12() const { return ____returnMember_12; }
	inline XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D ** get_address_of__returnMember_12() { return &____returnMember_12; }
	inline void set__returnMember_12(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * value)
	{
		____returnMember_12 = value;
		Il2CppCodeGenWriteBarrier((&____returnMember_12), value);
	}

	inline static int32_t get_offset_of__ignoreMemberNamespace_13() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____ignoreMemberNamespace_13)); }
	inline bool get__ignoreMemberNamespace_13() const { return ____ignoreMemberNamespace_13; }
	inline bool* get_address_of__ignoreMemberNamespace_13() { return &____ignoreMemberNamespace_13; }
	inline void set__ignoreMemberNamespace_13(bool value)
	{
		____ignoreMemberNamespace_13 = value;
	}

	inline static int32_t get_offset_of__canBeSimpleType_14() { return static_cast<int32_t>(offsetof(ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA, ____canBeSimpleType_14)); }
	inline bool get__canBeSimpleType_14() const { return ____canBeSimpleType_14; }
	inline bool* get_address_of__canBeSimpleType_14() { return &____canBeSimpleType_14; }
	inline void set__canBeSimpleType_14(bool value)
	{
		____canBeSimpleType_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSMAP_T7673ADE61E787987E671A668712C12D77FAAC6DA_H
#ifndef ENUMMAP_TD7B24FAC46ED105487BE3269534CBB5072D8652A_H
#define ENUMMAP_TD7B24FAC46ED105487BE3269534CBB5072D8652A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.EnumMap
struct  EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A  : public ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677
{
public:
	// System.Xml.Serialization.EnumMap_EnumMapMember[] System.Xml.Serialization.EnumMap::_members
	EnumMapMemberU5BU5D_t65FB66969212223ED79971B0473634E03D6C1B2E* ____members_0;
	// System.Boolean System.Xml.Serialization.EnumMap::_isFlags
	bool ____isFlags_1;
	// System.String[] System.Xml.Serialization.EnumMap::_enumNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____enumNames_2;
	// System.String[] System.Xml.Serialization.EnumMap::_xmlNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____xmlNames_3;
	// System.Int64[] System.Xml.Serialization.EnumMap::_values
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____values_4;

public:
	inline static int32_t get_offset_of__members_0() { return static_cast<int32_t>(offsetof(EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A, ____members_0)); }
	inline EnumMapMemberU5BU5D_t65FB66969212223ED79971B0473634E03D6C1B2E* get__members_0() const { return ____members_0; }
	inline EnumMapMemberU5BU5D_t65FB66969212223ED79971B0473634E03D6C1B2E** get_address_of__members_0() { return &____members_0; }
	inline void set__members_0(EnumMapMemberU5BU5D_t65FB66969212223ED79971B0473634E03D6C1B2E* value)
	{
		____members_0 = value;
		Il2CppCodeGenWriteBarrier((&____members_0), value);
	}

	inline static int32_t get_offset_of__isFlags_1() { return static_cast<int32_t>(offsetof(EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A, ____isFlags_1)); }
	inline bool get__isFlags_1() const { return ____isFlags_1; }
	inline bool* get_address_of__isFlags_1() { return &____isFlags_1; }
	inline void set__isFlags_1(bool value)
	{
		____isFlags_1 = value;
	}

	inline static int32_t get_offset_of__enumNames_2() { return static_cast<int32_t>(offsetof(EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A, ____enumNames_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__enumNames_2() const { return ____enumNames_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__enumNames_2() { return &____enumNames_2; }
	inline void set__enumNames_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____enumNames_2 = value;
		Il2CppCodeGenWriteBarrier((&____enumNames_2), value);
	}

	inline static int32_t get_offset_of__xmlNames_3() { return static_cast<int32_t>(offsetof(EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A, ____xmlNames_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__xmlNames_3() const { return ____xmlNames_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__xmlNames_3() { return &____xmlNames_3; }
	inline void set__xmlNames_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____xmlNames_3 = value;
		Il2CppCodeGenWriteBarrier((&____xmlNames_3), value);
	}

	inline static int32_t get_offset_of__values_4() { return static_cast<int32_t>(offsetof(EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A, ____values_4)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__values_4() const { return ____values_4; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__values_4() { return &____values_4; }
	inline void set__values_4(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____values_4 = value;
		Il2CppCodeGenWriteBarrier((&____values_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMMAP_TD7B24FAC46ED105487BE3269534CBB5072D8652A_H
#ifndef LISTMAP_TAE3FA433501B26F844EF9942DB4C054DA7CE2742_H
#define LISTMAP_TAE3FA433501B26F844EF9942DB4C054DA7CE2742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.ListMap
struct  ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742  : public ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677
{
public:
	// System.Xml.Serialization.XmlTypeMapElementInfoList System.Xml.Serialization.ListMap::_itemInfo
	XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 * ____itemInfo_0;
	// System.String System.Xml.Serialization.ListMap::_choiceMember
	String_t* ____choiceMember_1;

public:
	inline static int32_t get_offset_of__itemInfo_0() { return static_cast<int32_t>(offsetof(ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742, ____itemInfo_0)); }
	inline XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 * get__itemInfo_0() const { return ____itemInfo_0; }
	inline XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 ** get_address_of__itemInfo_0() { return &____itemInfo_0; }
	inline void set__itemInfo_0(XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 * value)
	{
		____itemInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____itemInfo_0), value);
	}

	inline static int32_t get_offset_of__choiceMember_1() { return static_cast<int32_t>(offsetof(ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742, ____choiceMember_1)); }
	inline String_t* get__choiceMember_1() const { return ____choiceMember_1; }
	inline String_t** get_address_of__choiceMember_1() { return &____choiceMember_1; }
	inline void set__choiceMember_1(String_t* value)
	{
		____choiceMember_1 = value;
		Il2CppCodeGenWriteBarrier((&____choiceMember_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTMAP_TAE3FA433501B26F844EF9942DB4C054DA7CE2742_H
#ifndef XMLANYATTRIBUTEATTRIBUTE_TBB53254892DFE4AB748B01108539540EC451C680_H
#define XMLANYATTRIBUTEATTRIBUTE_TBB53254892DFE4AB748B01108539540EC451C680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAnyAttributeAttribute
struct  XmlAnyAttributeAttribute_tBB53254892DFE4AB748B01108539540EC451C680  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYATTRIBUTEATTRIBUTE_TBB53254892DFE4AB748B01108539540EC451C680_H
#ifndef XMLANYELEMENTATTRIBUTE_TFAA9F7AEF617E275A422AEC9EC38EB6F7376960E_H
#define XMLANYELEMENTATTRIBUTE_TFAA9F7AEF617E275A422AEC9EC38EB6F7376960E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAnyElementAttribute
struct  XmlAnyElementAttribute_tFAA9F7AEF617E275A422AEC9EC38EB6F7376960E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Xml.Serialization.XmlAnyElementAttribute::elementName
	String_t* ___elementName_0;
	// System.String System.Xml.Serialization.XmlAnyElementAttribute::ns
	String_t* ___ns_1;
	// System.Int32 System.Xml.Serialization.XmlAnyElementAttribute::order
	int32_t ___order_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlAnyElementAttribute_tFAA9F7AEF617E275A422AEC9EC38EB6F7376960E, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(XmlAnyElementAttribute_tFAA9F7AEF617E275A422AEC9EC38EB6F7376960E, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier((&___ns_1), value);
	}

	inline static int32_t get_offset_of_order_2() { return static_cast<int32_t>(offsetof(XmlAnyElementAttribute_tFAA9F7AEF617E275A422AEC9EC38EB6F7376960E, ___order_2)); }
	inline int32_t get_order_2() const { return ___order_2; }
	inline int32_t* get_address_of_order_2() { return &___order_2; }
	inline void set_order_2(int32_t value)
	{
		___order_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYELEMENTATTRIBUTE_TFAA9F7AEF617E275A422AEC9EC38EB6F7376960E_H
#ifndef XMLANYELEMENTATTRIBUTES_TED6F51ECCCB48BBB4C5E4D826A7E5CF341F33160_H
#define XMLANYELEMENTATTRIBUTES_TED6F51ECCCB48BBB4C5E4D826A7E5CF341F33160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAnyElementAttributes
struct  XmlAnyElementAttributes_tED6F51ECCCB48BBB4C5E4D826A7E5CF341F33160  : public CollectionBase_tF5D4583FF325726066A9803839A04E9C0084ED01
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYELEMENTATTRIBUTES_TED6F51ECCCB48BBB4C5E4D826A7E5CF341F33160_H
#ifndef XMLARRAYITEMATTRIBUTES_T7BFEEA6B880F1F3FC7DF8EB92C33E65C56FA0A49_H
#define XMLARRAYITEMATTRIBUTES_T7BFEEA6B880F1F3FC7DF8EB92C33E65C56FA0A49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlArrayItemAttributes
struct  XmlArrayItemAttributes_t7BFEEA6B880F1F3FC7DF8EB92C33E65C56FA0A49  : public CollectionBase_tF5D4583FF325726066A9803839A04E9C0084ED01
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLARRAYITEMATTRIBUTES_T7BFEEA6B880F1F3FC7DF8EB92C33E65C56FA0A49_H
#ifndef XMLCHOICEIDENTIFIERATTRIBUTE_T67E5AE7310D779F7533770C3D3F7CAA65A5E6B8B_H
#define XMLCHOICEIDENTIFIERATTRIBUTE_T67E5AE7310D779F7533770C3D3F7CAA65A5E6B8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlChoiceIdentifierAttribute
struct  XmlChoiceIdentifierAttribute_t67E5AE7310D779F7533770C3D3F7CAA65A5E6B8B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Xml.Serialization.XmlChoiceIdentifierAttribute::memberName
	String_t* ___memberName_0;

public:
	inline static int32_t get_offset_of_memberName_0() { return static_cast<int32_t>(offsetof(XmlChoiceIdentifierAttribute_t67E5AE7310D779F7533770C3D3F7CAA65A5E6B8B, ___memberName_0)); }
	inline String_t* get_memberName_0() const { return ___memberName_0; }
	inline String_t** get_address_of_memberName_0() { return &___memberName_0; }
	inline void set_memberName_0(String_t* value)
	{
		___memberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___memberName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHOICEIDENTIFIERATTRIBUTE_T67E5AE7310D779F7533770C3D3F7CAA65A5E6B8B_H
#ifndef XMLELEMENTATTRIBUTES_T3F224B71C1BC11E8EF6B4E8E45A940B394FA6D34_H
#define XMLELEMENTATTRIBUTES_T3F224B71C1BC11E8EF6B4E8E45A940B394FA6D34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlElementAttributes
struct  XmlElementAttributes_t3F224B71C1BC11E8EF6B4E8E45A940B394FA6D34  : public CollectionBase_tF5D4583FF325726066A9803839A04E9C0084ED01
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTATTRIBUTES_T3F224B71C1BC11E8EF6B4E8E45A940B394FA6D34_H
#ifndef XMLENUMATTRIBUTE_TBD75888B09FE971438F4FA55FC302BC5FA3936F0_H
#define XMLENUMATTRIBUTE_TBD75888B09FE971438F4FA55FC302BC5FA3936F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlEnumAttribute
struct  XmlEnumAttribute_tBD75888B09FE971438F4FA55FC302BC5FA3936F0  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Xml.Serialization.XmlEnumAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(XmlEnumAttribute_tBD75888B09FE971438F4FA55FC302BC5FA3936F0, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENUMATTRIBUTE_TBD75888B09FE971438F4FA55FC302BC5FA3936F0_H
#ifndef XMLIGNOREATTRIBUTE_T42AF5E2677A8626D8F5A1685C84B4E1C35B54AE6_H
#define XMLIGNOREATTRIBUTE_T42AF5E2677A8626D8F5A1685C84B4E1C35B54AE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlIgnoreAttribute
struct  XmlIgnoreAttribute_t42AF5E2677A8626D8F5A1685C84B4E1C35B54AE6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIGNOREATTRIBUTE_T42AF5E2677A8626D8F5A1685C84B4E1C35B54AE6_H
#ifndef XMLINCLUDEATTRIBUTE_T2E1C9EC424546DAAAFC56DC6AB70F874A12A9E7E_H
#define XMLINCLUDEATTRIBUTE_T2E1C9EC424546DAAAFC56DC6AB70F874A12A9E7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlIncludeAttribute
struct  XmlIncludeAttribute_t2E1C9EC424546DAAAFC56DC6AB70F874A12A9E7E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type System.Xml.Serialization.XmlIncludeAttribute::type
	Type_t * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(XmlIncludeAttribute_t2E1C9EC424546DAAAFC56DC6AB70F874A12A9E7E, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLINCLUDEATTRIBUTE_T2E1C9EC424546DAAAFC56DC6AB70F874A12A9E7E_H
#ifndef XMLNAMESPACEDECLARATIONSATTRIBUTE_TB6BE2A5A9D1956B5C33803E844385480EEE97957_H
#define XMLNAMESPACEDECLARATIONSATTRIBUTE_TB6BE2A5A9D1956B5C33803E844385480EEE97957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlNamespaceDeclarationsAttribute
struct  XmlNamespaceDeclarationsAttribute_tB6BE2A5A9D1956B5C33803E844385480EEE97957  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEDECLARATIONSATTRIBUTE_TB6BE2A5A9D1956B5C33803E844385480EEE97957_H
#ifndef XMLROOTATTRIBUTE_T656E9D8E274574F4FF0EEF8920170A435A73F06C_H
#define XMLROOTATTRIBUTE_T656E9D8E274574F4FF0EEF8920170A435A73F06C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlRootAttribute
struct  XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Xml.Serialization.XmlRootAttribute::elementName
	String_t* ___elementName_0;
	// System.Boolean System.Xml.Serialization.XmlRootAttribute::isNullable
	bool ___isNullable_1;
	// System.String System.Xml.Serialization.XmlRootAttribute::ns
	String_t* ___ns_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_isNullable_1() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C, ___isNullable_1)); }
	inline bool get_isNullable_1() const { return ___isNullable_1; }
	inline bool* get_address_of_isNullable_1() { return &___isNullable_1; }
	inline void set_isNullable_1(bool value)
	{
		___isNullable_1 = value;
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((&___ns_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLROOTATTRIBUTE_T656E9D8E274574F4FF0EEF8920170A435A73F06C_H
#ifndef XMLSCHEMAPROVIDERATTRIBUTE_T1E12BA0206734CB8E31915BE0B2DD0B65A6B9F66_H
#define XMLSCHEMAPROVIDERATTRIBUTE_T1E12BA0206734CB8E31915BE0B2DD0B65A6B9F66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSchemaProviderAttribute
struct  XmlSchemaProviderAttribute_t1E12BA0206734CB8E31915BE0B2DD0B65A6B9F66  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Xml.Serialization.XmlSchemaProviderAttribute::_methodName
	String_t* ____methodName_0;
	// System.Boolean System.Xml.Serialization.XmlSchemaProviderAttribute::_isAny
	bool ____isAny_1;

public:
	inline static int32_t get_offset_of__methodName_0() { return static_cast<int32_t>(offsetof(XmlSchemaProviderAttribute_t1E12BA0206734CB8E31915BE0B2DD0B65A6B9F66, ____methodName_0)); }
	inline String_t* get__methodName_0() const { return ____methodName_0; }
	inline String_t** get_address_of__methodName_0() { return &____methodName_0; }
	inline void set__methodName_0(String_t* value)
	{
		____methodName_0 = value;
		Il2CppCodeGenWriteBarrier((&____methodName_0), value);
	}

	inline static int32_t get_offset_of__isAny_1() { return static_cast<int32_t>(offsetof(XmlSchemaProviderAttribute_t1E12BA0206734CB8E31915BE0B2DD0B65A6B9F66, ____isAny_1)); }
	inline bool get__isAny_1() const { return ____isAny_1; }
	inline bool* get_address_of__isAny_1() { return &____isAny_1; }
	inline void set__isAny_1(bool value)
	{
		____isAny_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAPROVIDERATTRIBUTE_T1E12BA0206734CB8E31915BE0B2DD0B65A6B9F66_H
#ifndef XMLSERIALIZATIONWRITER_TD7F886F1AE76D54C208A41797D76E908AA2086F6_H
#define XMLSERIALIZATIONWRITER_TD7F886F1AE76D54C208A41797D76E908AA2086F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationWriter
struct  XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6  : public XmlSerializationGeneratedCode_tD121073E4460FCF7F6B549E4621B0CB971D2E6CE
{
public:
	// System.Runtime.Serialization.ObjectIDGenerator System.Xml.Serialization.XmlSerializationWriter::idGenerator
	ObjectIDGenerator_tD65209D0A1CDA08C9C709A4D1997D8091A93C1A1 * ___idGenerator_0;
	// System.Int32 System.Xml.Serialization.XmlSerializationWriter::qnameCount
	int32_t ___qnameCount_1;
	// System.Boolean System.Xml.Serialization.XmlSerializationWriter::topLevelElement
	bool ___topLevelElement_2;
	// System.Collections.ArrayList System.Xml.Serialization.XmlSerializationWriter::namespaces
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___namespaces_3;
	// System.Xml.XmlWriter System.Xml.Serialization.XmlSerializationWriter::writer
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * ___writer_4;
	// System.Collections.Queue System.Xml.Serialization.XmlSerializationWriter::referencedElements
	Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * ___referencedElements_5;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializationWriter::callbacks
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___callbacks_6;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializationWriter::serializedObjects
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___serializedObjects_7;

public:
	inline static int32_t get_offset_of_idGenerator_0() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___idGenerator_0)); }
	inline ObjectIDGenerator_tD65209D0A1CDA08C9C709A4D1997D8091A93C1A1 * get_idGenerator_0() const { return ___idGenerator_0; }
	inline ObjectIDGenerator_tD65209D0A1CDA08C9C709A4D1997D8091A93C1A1 ** get_address_of_idGenerator_0() { return &___idGenerator_0; }
	inline void set_idGenerator_0(ObjectIDGenerator_tD65209D0A1CDA08C9C709A4D1997D8091A93C1A1 * value)
	{
		___idGenerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___idGenerator_0), value);
	}

	inline static int32_t get_offset_of_qnameCount_1() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___qnameCount_1)); }
	inline int32_t get_qnameCount_1() const { return ___qnameCount_1; }
	inline int32_t* get_address_of_qnameCount_1() { return &___qnameCount_1; }
	inline void set_qnameCount_1(int32_t value)
	{
		___qnameCount_1 = value;
	}

	inline static int32_t get_offset_of_topLevelElement_2() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___topLevelElement_2)); }
	inline bool get_topLevelElement_2() const { return ___topLevelElement_2; }
	inline bool* get_address_of_topLevelElement_2() { return &___topLevelElement_2; }
	inline void set_topLevelElement_2(bool value)
	{
		___topLevelElement_2 = value;
	}

	inline static int32_t get_offset_of_namespaces_3() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___namespaces_3)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_namespaces_3() const { return ___namespaces_3; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_namespaces_3() { return &___namespaces_3; }
	inline void set_namespaces_3(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___namespaces_3 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_3), value);
	}

	inline static int32_t get_offset_of_writer_4() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___writer_4)); }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * get_writer_4() const { return ___writer_4; }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 ** get_address_of_writer_4() { return &___writer_4; }
	inline void set_writer_4(XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * value)
	{
		___writer_4 = value;
		Il2CppCodeGenWriteBarrier((&___writer_4), value);
	}

	inline static int32_t get_offset_of_referencedElements_5() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___referencedElements_5)); }
	inline Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * get_referencedElements_5() const { return ___referencedElements_5; }
	inline Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 ** get_address_of_referencedElements_5() { return &___referencedElements_5; }
	inline void set_referencedElements_5(Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * value)
	{
		___referencedElements_5 = value;
		Il2CppCodeGenWriteBarrier((&___referencedElements_5), value);
	}

	inline static int32_t get_offset_of_callbacks_6() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___callbacks_6)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_callbacks_6() const { return ___callbacks_6; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_callbacks_6() { return &___callbacks_6; }
	inline void set_callbacks_6(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___callbacks_6 = value;
		Il2CppCodeGenWriteBarrier((&___callbacks_6), value);
	}

	inline static int32_t get_offset_of_serializedObjects_7() { return static_cast<int32_t>(offsetof(XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6, ___serializedObjects_7)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_serializedObjects_7() const { return ___serializedObjects_7; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_serializedObjects_7() { return &___serializedObjects_7; }
	inline void set_serializedObjects_7(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___serializedObjects_7 = value;
		Il2CppCodeGenWriteBarrier((&___serializedObjects_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZATIONWRITER_TD7F886F1AE76D54C208A41797D76E908AA2086F6_H
#ifndef XMLTEXTATTRIBUTE_T260DF760832CEB66E748BDEDE998C3480FB902A7_H
#define XMLTEXTATTRIBUTE_T260DF760832CEB66E748BDEDE998C3480FB902A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTextAttribute
struct  XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Xml.Serialization.XmlTextAttribute::dataType
	String_t* ___dataType_0;
	// System.Type System.Xml.Serialization.XmlTextAttribute::type
	Type_t * ___type_1;

public:
	inline static int32_t get_offset_of_dataType_0() { return static_cast<int32_t>(offsetof(XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7, ___dataType_0)); }
	inline String_t* get_dataType_0() const { return ___dataType_0; }
	inline String_t** get_address_of_dataType_0() { return &___dataType_0; }
	inline void set_dataType_0(String_t* value)
	{
		___dataType_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataType_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTATTRIBUTE_T260DF760832CEB66E748BDEDE998C3480FB902A7_H
#ifndef XMLTYPEATTRIBUTE_T8386F99942A9FABF3B9AF64324C28F674EE31E08_H
#define XMLTYPEATTRIBUTE_T8386F99942A9FABF3B9AF64324C28F674EE31E08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeAttribute
struct  XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.Xml.Serialization.XmlTypeAttribute::includeInSchema
	bool ___includeInSchema_0;
	// System.String System.Xml.Serialization.XmlTypeAttribute::ns
	String_t* ___ns_1;
	// System.String System.Xml.Serialization.XmlTypeAttribute::typeName
	String_t* ___typeName_2;

public:
	inline static int32_t get_offset_of_includeInSchema_0() { return static_cast<int32_t>(offsetof(XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08, ___includeInSchema_0)); }
	inline bool get_includeInSchema_0() const { return ___includeInSchema_0; }
	inline bool* get_address_of_includeInSchema_0() { return &___includeInSchema_0; }
	inline void set_includeInSchema_0(bool value)
	{
		___includeInSchema_0 = value;
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier((&___ns_1), value);
	}

	inline static int32_t get_offset_of_typeName_2() { return static_cast<int32_t>(offsetof(XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08, ___typeName_2)); }
	inline String_t* get_typeName_2() const { return ___typeName_2; }
	inline String_t** get_address_of_typeName_2() { return &___typeName_2; }
	inline void set_typeName_2(String_t* value)
	{
		___typeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEATTRIBUTE_T8386F99942A9FABF3B9AF64324C28F674EE31E08_H
#ifndef XMLTYPECONVERTORATTRIBUTE_TD8D068FF3EE07B1E93D3F5E657558E0E53D3CF4B_H
#define XMLTYPECONVERTORATTRIBUTE_TD8D068FF3EE07B1E93D3F5E657558E0E53D3CF4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeConvertorAttribute
struct  XmlTypeConvertorAttribute_tD8D068FF3EE07B1E93D3F5E657558E0E53D3CF4B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Xml.Serialization.XmlTypeConvertorAttribute::<Method>k__BackingField
	String_t* ___U3CMethodU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CMethodU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(XmlTypeConvertorAttribute_tD8D068FF3EE07B1E93D3F5E657558E0E53D3CF4B, ___U3CMethodU3Ek__BackingField_0)); }
	inline String_t* get_U3CMethodU3Ek__BackingField_0() const { return ___U3CMethodU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMethodU3Ek__BackingField_0() { return &___U3CMethodU3Ek__BackingField_0; }
	inline void set_U3CMethodU3Ek__BackingField_0(String_t* value)
	{
		___U3CMethodU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMethodU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPECONVERTORATTRIBUTE_TD8D068FF3EE07B1E93D3F5E657558E0E53D3CF4B_H
#ifndef XMLTYPEMAPELEMENTINFOLIST_TCD7D387F78138B4E137D285319EC10532B14FBB0_H
#define XMLTYPEMAPELEMENTINFOLIST_TCD7D387F78138B4E137D285319EC10532B14FBB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapElementInfoList
struct  XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0  : public ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPELEMENTINFOLIST_TCD7D387F78138B4E137D285319EC10532B14FBB0_H
#ifndef XMLTYPEMAPMEMBERANYATTRIBUTE_T0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE_H
#define XMLTYPEMAPMEMBERANYATTRIBUTE_T0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberAnyAttribute
struct  XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE  : public XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERANYATTRIBUTE_T0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE_H
#ifndef XMLTYPEMAPMEMBERELEMENT_T51AF456B8ECA454E76169688454E82735FB1AE00_H
#define XMLTYPEMAPMEMBERELEMENT_T51AF456B8ECA454E76169688454E82735FB1AE00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberElement
struct  XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00  : public XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D
{
public:
	// System.Xml.Serialization.XmlTypeMapElementInfoList System.Xml.Serialization.XmlTypeMapMemberElement::_elementInfo
	XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 * ____elementInfo_10;
	// System.String System.Xml.Serialization.XmlTypeMapMemberElement::_choiceMember
	String_t* ____choiceMember_11;
	// System.Boolean System.Xml.Serialization.XmlTypeMapMemberElement::_isTextCollector
	bool ____isTextCollector_12;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.XmlTypeMapMemberElement::_choiceTypeData
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * ____choiceTypeData_13;

public:
	inline static int32_t get_offset_of__elementInfo_10() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00, ____elementInfo_10)); }
	inline XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 * get__elementInfo_10() const { return ____elementInfo_10; }
	inline XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 ** get_address_of__elementInfo_10() { return &____elementInfo_10; }
	inline void set__elementInfo_10(XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0 * value)
	{
		____elementInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&____elementInfo_10), value);
	}

	inline static int32_t get_offset_of__choiceMember_11() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00, ____choiceMember_11)); }
	inline String_t* get__choiceMember_11() const { return ____choiceMember_11; }
	inline String_t** get_address_of__choiceMember_11() { return &____choiceMember_11; }
	inline void set__choiceMember_11(String_t* value)
	{
		____choiceMember_11 = value;
		Il2CppCodeGenWriteBarrier((&____choiceMember_11), value);
	}

	inline static int32_t get_offset_of__isTextCollector_12() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00, ____isTextCollector_12)); }
	inline bool get__isTextCollector_12() const { return ____isTextCollector_12; }
	inline bool* get_address_of__isTextCollector_12() { return &____isTextCollector_12; }
	inline void set__isTextCollector_12(bool value)
	{
		____isTextCollector_12 = value;
	}

	inline static int32_t get_offset_of__choiceTypeData_13() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00, ____choiceTypeData_13)); }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * get__choiceTypeData_13() const { return ____choiceTypeData_13; }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 ** get_address_of__choiceTypeData_13() { return &____choiceTypeData_13; }
	inline void set__choiceTypeData_13(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * value)
	{
		____choiceTypeData_13 = value;
		Il2CppCodeGenWriteBarrier((&____choiceTypeData_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERELEMENT_T51AF456B8ECA454E76169688454E82735FB1AE00_H
#ifndef XMLTYPEMAPMEMBERNAMESPACES_T08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43_H
#define XMLTYPEMAPMEMBERNAMESPACES_T08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberNamespaces
struct  XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43  : public XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERNAMESPACES_T08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43_H
#ifndef XPATHNAVIGATOR_T4E711E4382A896C152FA99D0D39A8C6CEC238EC3_H
#define XPATHNAVIGATOR_T4E711E4382A896C152FA99D0D39A8C6CEC238EC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator
struct  XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3  : public XPathItem_tCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98
{
public:

public:
};

struct XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields
{
public:
	// System.Xml.XPath.XPathNavigatorKeyComparer System.Xml.XPath.XPathNavigator::comparer
	XPathNavigatorKeyComparer_t6A0E82BEC0BE42351DDB26EAA86333C11E0A9378 * ___comparer_0;
	// System.Char[] System.Xml.XPath.XPathNavigator::NodeTypeLetter
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___NodeTypeLetter_1;
	// System.Char[] System.Xml.XPath.XPathNavigator::UniqueIdTbl
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___UniqueIdTbl_2;
	// System.Int32[] System.Xml.XPath.XPathNavigator::ContentKindMasks
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ContentKindMasks_3;

public:
	inline static int32_t get_offset_of_comparer_0() { return static_cast<int32_t>(offsetof(XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields, ___comparer_0)); }
	inline XPathNavigatorKeyComparer_t6A0E82BEC0BE42351DDB26EAA86333C11E0A9378 * get_comparer_0() const { return ___comparer_0; }
	inline XPathNavigatorKeyComparer_t6A0E82BEC0BE42351DDB26EAA86333C11E0A9378 ** get_address_of_comparer_0() { return &___comparer_0; }
	inline void set_comparer_0(XPathNavigatorKeyComparer_t6A0E82BEC0BE42351DDB26EAA86333C11E0A9378 * value)
	{
		___comparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_0), value);
	}

	inline static int32_t get_offset_of_NodeTypeLetter_1() { return static_cast<int32_t>(offsetof(XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields, ___NodeTypeLetter_1)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_NodeTypeLetter_1() const { return ___NodeTypeLetter_1; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_NodeTypeLetter_1() { return &___NodeTypeLetter_1; }
	inline void set_NodeTypeLetter_1(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___NodeTypeLetter_1 = value;
		Il2CppCodeGenWriteBarrier((&___NodeTypeLetter_1), value);
	}

	inline static int32_t get_offset_of_UniqueIdTbl_2() { return static_cast<int32_t>(offsetof(XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields, ___UniqueIdTbl_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_UniqueIdTbl_2() const { return ___UniqueIdTbl_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_UniqueIdTbl_2() { return &___UniqueIdTbl_2; }
	inline void set_UniqueIdTbl_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___UniqueIdTbl_2 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueIdTbl_2), value);
	}

	inline static int32_t get_offset_of_ContentKindMasks_3() { return static_cast<int32_t>(offsetof(XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields, ___ContentKindMasks_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ContentKindMasks_3() const { return ___ContentKindMasks_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ContentKindMasks_3() { return &___ContentKindMasks_3; }
	inline void set_ContentKindMasks_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ContentKindMasks_3 = value;
		Il2CppCodeGenWriteBarrier((&___ContentKindMasks_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATOR_T4E711E4382A896C152FA99D0D39A8C6CEC238EC3_H
#ifndef DEBUGGERDISPLAYPROXY_T7058342D7CBA1293CD786F1EFA632C42E370A97A_H
#define DEBUGGERDISPLAYPROXY_T7058342D7CBA1293CD786F1EFA632C42E370A97A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator_DebuggerDisplayProxy
struct  DebuggerDisplayProxy_t7058342D7CBA1293CD786F1EFA632C42E370A97A 
{
public:
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator_DebuggerDisplayProxy::nav
	XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3 * ___nav_0;

public:
	inline static int32_t get_offset_of_nav_0() { return static_cast<int32_t>(offsetof(DebuggerDisplayProxy_t7058342D7CBA1293CD786F1EFA632C42E370A97A, ___nav_0)); }
	inline XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3 * get_nav_0() const { return ___nav_0; }
	inline XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3 ** get_address_of_nav_0() { return &___nav_0; }
	inline void set_nav_0(XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3 * value)
	{
		___nav_0 = value;
		Il2CppCodeGenWriteBarrier((&___nav_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XPath.XPathNavigator/DebuggerDisplayProxy
struct DebuggerDisplayProxy_t7058342D7CBA1293CD786F1EFA632C42E370A97A_marshaled_pinvoke
{
	XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3 * ___nav_0;
};
// Native definition for COM marshalling of System.Xml.XPath.XPathNavigator/DebuggerDisplayProxy
struct DebuggerDisplayProxy_t7058342D7CBA1293CD786F1EFA632C42E370A97A_marshaled_com
{
	XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3 * ___nav_0;
};
#endif // DEBUGGERDISPLAYPROXY_T7058342D7CBA1293CD786F1EFA632C42E370A97A_H
#ifndef XMLURLRESOLVER_T43FE24F4601B4197354EFB5783E5CB9F57997EC9_H
#define XMLURLRESOLVER_T43FE24F4601B4197354EFB5783E5CB9F57997EC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUrlResolver
struct  XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9  : public XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280
{
public:
	// System.Net.ICredentials System.Xml.XmlUrlResolver::_credentials
	RuntimeObject* ____credentials_1;
	// System.Net.IWebProxy System.Xml.XmlUrlResolver::_proxy
	RuntimeObject* ____proxy_2;
	// System.Net.Cache.RequestCachePolicy System.Xml.XmlUrlResolver::_cachePolicy
	RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * ____cachePolicy_3;

public:
	inline static int32_t get_offset_of__credentials_1() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9, ____credentials_1)); }
	inline RuntimeObject* get__credentials_1() const { return ____credentials_1; }
	inline RuntimeObject** get_address_of__credentials_1() { return &____credentials_1; }
	inline void set__credentials_1(RuntimeObject* value)
	{
		____credentials_1 = value;
		Il2CppCodeGenWriteBarrier((&____credentials_1), value);
	}

	inline static int32_t get_offset_of__proxy_2() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9, ____proxy_2)); }
	inline RuntimeObject* get__proxy_2() const { return ____proxy_2; }
	inline RuntimeObject** get_address_of__proxy_2() { return &____proxy_2; }
	inline void set__proxy_2(RuntimeObject* value)
	{
		____proxy_2 = value;
		Il2CppCodeGenWriteBarrier((&____proxy_2), value);
	}

	inline static int32_t get_offset_of__cachePolicy_3() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9, ____cachePolicy_3)); }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * get__cachePolicy_3() const { return ____cachePolicy_3; }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 ** get_address_of__cachePolicy_3() { return &____cachePolicy_3; }
	inline void set__cachePolicy_3(RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * value)
	{
		____cachePolicy_3 = value;
		Il2CppCodeGenWriteBarrier((&____cachePolicy_3), value);
	}
};

struct XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9_StaticFields
{
public:
	// System.Object System.Xml.XmlUrlResolver::s_DownloadManager
	RuntimeObject * ___s_DownloadManager_0;

public:
	inline static int32_t get_offset_of_s_DownloadManager_0() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9_StaticFields, ___s_DownloadManager_0)); }
	inline RuntimeObject * get_s_DownloadManager_0() const { return ___s_DownloadManager_0; }
	inline RuntimeObject ** get_address_of_s_DownloadManager_0() { return &___s_DownloadManager_0; }
	inline void set_s_DownloadManager_0(RuntimeObject * value)
	{
		___s_DownloadManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_DownloadManager_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLURLRESOLVER_T43FE24F4601B4197354EFB5783E5CB9F57997EC9_H
#ifndef AXISTYPE_T4CA4EB4650FD84E62398568AD4D97C8CE272F979_H
#define AXISTYPE_T4CA4EB4650FD84E62398568AD4D97C8CE272F979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Axis_AxisType
struct  AxisType_t4CA4EB4650FD84E62398568AD4D97C8CE272F979 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.Axis_AxisType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisType_t4CA4EB4650FD84E62398568AD4D97C8CE272F979, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTYPE_T4CA4EB4650FD84E62398568AD4D97C8CE272F979_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef ASYNCTASKMETHODBUILDER_1_T2A9513A084F4B19851B91EF1F22BB57776D35663_H
#define ASYNCTASKMETHODBUILDER_1_T2A9513A084F4B19851B91EF1F22BB57776D35663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>
struct  AsyncTaskMethodBuilder_1_t2A9513A084F4B19851B91EF1F22BB57776D35663 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2A9513A084F4B19851B91EF1F22BB57776D35663, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2A9513A084F4B19851B91EF1F22BB57776D35663, ___m_task_2)); }
	inline Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t2A9513A084F4B19851B91EF1F22BB57776D35663_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2A9513A084F4B19851B91EF1F22BB57776D35663_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T2A9513A084F4B19851B91EF1F22BB57776D35663_H
#ifndef CONSTRAINTROLE_TDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397_H
#define CONSTRAINTROLE_TDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.CompiledIdentityConstraint_ConstraintRole
struct  ConstraintRole_tDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397 
{
public:
	// System.Int32 System.Xml.Schema.CompiledIdentityConstraint_ConstraintRole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConstraintRole_tDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTROLE_TDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397_H
#ifndef XMLSCHEMAFORM_T86E3CD541907A35AC5B505EF118C3E20CC760276_H
#define XMLSCHEMAFORM_T86E3CD541907A35AC5B505EF118C3E20CC760276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaForm
struct  XmlSchemaForm_t86E3CD541907A35AC5B505EF118C3E20CC760276 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaForm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaForm_t86E3CD541907A35AC5B505EF118C3E20CC760276, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFORM_T86E3CD541907A35AC5B505EF118C3E20CC760276_H
#ifndef SCHEMATYPES_TC46A6854DAA25ADC2A819D4600D261B211E9D7C7_H
#define SCHEMATYPES_TC46A6854DAA25ADC2A819D4600D261B211E9D7C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SchemaTypes
struct  SchemaTypes_tC46A6854DAA25ADC2A819D4600D261B211E9D7C7 
{
public:
	// System.Int32 System.Xml.Serialization.SchemaTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SchemaTypes_tC46A6854DAA25ADC2A819D4600D261B211E9D7C7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMATYPES_TC46A6854DAA25ADC2A819D4600D261B211E9D7C7_H
#ifndef SERIALIZATIONFORMAT_T0A1D537B68CA8F48ACBF510EEA30EFD70F1602A3_H
#define SERIALIZATIONFORMAT_T0A1D537B68CA8F48ACBF510EEA30EFD70F1602A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SerializationFormat
struct  SerializationFormat_t0A1D537B68CA8F48ACBF510EEA30EFD70F1602A3 
{
public:
	// System.Int32 System.Xml.Serialization.SerializationFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SerializationFormat_t0A1D537B68CA8F48ACBF510EEA30EFD70F1602A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONFORMAT_T0A1D537B68CA8F48ACBF510EEA30EFD70F1602A3_H
#ifndef XMLTYPEMAPMEMBEREXPANDABLE_TEFC74196D6503AB38EEA1AD371BA55EABC7C30ED_H
#define XMLTYPEMAPMEMBEREXPANDABLE_TEFC74196D6503AB38EEA1AD371BA55EABC7C30ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberExpandable
struct  XmlTypeMapMemberExpandable_tEFC74196D6503AB38EEA1AD371BA55EABC7C30ED  : public XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00
{
public:
	// System.Int32 System.Xml.Serialization.XmlTypeMapMemberExpandable::_flatArrayIndex
	int32_t ____flatArrayIndex_14;

public:
	inline static int32_t get_offset_of__flatArrayIndex_14() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberExpandable_tEFC74196D6503AB38EEA1AD371BA55EABC7C30ED, ____flatArrayIndex_14)); }
	inline int32_t get__flatArrayIndex_14() const { return ____flatArrayIndex_14; }
	inline int32_t* get_address_of__flatArrayIndex_14() { return &____flatArrayIndex_14; }
	inline void set__flatArrayIndex_14(int32_t value)
	{
		____flatArrayIndex_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBEREXPANDABLE_TEFC74196D6503AB38EEA1AD371BA55EABC7C30ED_H
#ifndef XMLTYPEMAPMEMBERLIST_T408EB21F4809E9C2E2059BDE5F77F28F503EA48F_H
#define XMLTYPEMAPMEMBERLIST_T408EB21F4809E9C2E2059BDE5F77F28F503EA48F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberList
struct  XmlTypeMapMemberList_t408EB21F4809E9C2E2059BDE5F77F28F503EA48F  : public XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERLIST_T408EB21F4809E9C2E2059BDE5F77F28F503EA48F_H
#ifndef XPATHEXCEPTION_T6FE79C310C86CF7BA39A7098B00AACE76BC28E1E_H
#define XPATHEXCEPTION_T6FE79C310C86CF7BA39A7098B00AACE76BC28E1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathException
struct  XPathException_t6FE79C310C86CF7BA39A7098B00AACE76BC28E1E  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.Xml.XPath.XPathException::res
	String_t* ___res_17;
	// System.String[] System.Xml.XPath.XPathException::args
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___args_18;
	// System.String System.Xml.XPath.XPathException::message
	String_t* ___message_19;

public:
	inline static int32_t get_offset_of_res_17() { return static_cast<int32_t>(offsetof(XPathException_t6FE79C310C86CF7BA39A7098B00AACE76BC28E1E, ___res_17)); }
	inline String_t* get_res_17() const { return ___res_17; }
	inline String_t** get_address_of_res_17() { return &___res_17; }
	inline void set_res_17(String_t* value)
	{
		___res_17 = value;
		Il2CppCodeGenWriteBarrier((&___res_17), value);
	}

	inline static int32_t get_offset_of_args_18() { return static_cast<int32_t>(offsetof(XPathException_t6FE79C310C86CF7BA39A7098B00AACE76BC28E1E, ___args_18)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_args_18() const { return ___args_18; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_args_18() { return &___args_18; }
	inline void set_args_18(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___args_18 = value;
		Il2CppCodeGenWriteBarrier((&___args_18), value);
	}

	inline static int32_t get_offset_of_message_19() { return static_cast<int32_t>(offsetof(XPathException_t6FE79C310C86CF7BA39A7098B00AACE76BC28E1E, ___message_19)); }
	inline String_t* get_message_19() const { return ___message_19; }
	inline String_t** get_address_of_message_19() { return &___message_19; }
	inline void set_message_19(String_t* value)
	{
		___message_19 = value;
		Il2CppCodeGenWriteBarrier((&___message_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHEXCEPTION_T6FE79C310C86CF7BA39A7098B00AACE76BC28E1E_H
#ifndef XPATHNAMESPACESCOPE_T2B3F07359A5FD32041B7DD6FB6A452DBA19D5EA2_H
#define XPATHNAMESPACESCOPE_T2B3F07359A5FD32041B7DD6FB6A452DBA19D5EA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNamespaceScope
struct  XPathNamespaceScope_t2B3F07359A5FD32041B7DD6FB6A452DBA19D5EA2 
{
public:
	// System.Int32 System.Xml.XPath.XPathNamespaceScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XPathNamespaceScope_t2B3F07359A5FD32041B7DD6FB6A452DBA19D5EA2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAMESPACESCOPE_T2B3F07359A5FD32041B7DD6FB6A452DBA19D5EA2_H
#ifndef XPATHNODETYPE_TB1DB3F39502492756781F03A9645DD587B81BEF2_H
#define XPATHNODETYPE_TB1DB3F39502492756781F03A9645DD587B81BEF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeType
struct  XPathNodeType_tB1DB3F39502492756781F03A9645DD587B81BEF2 
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XPathNodeType_tB1DB3F39502492756781F03A9645DD587B81BEF2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODETYPE_TB1DB3F39502492756781F03A9645DD587B81BEF2_H
#ifndef XPATHRESULTTYPE_T1043308EA53B9A2365747EBE5C4A8C2E68E0FDBC_H
#define XPATHRESULTTYPE_T1043308EA53B9A2365747EBE5C4A8C2E68E0FDBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathResultType
struct  XPathResultType_t1043308EA53B9A2365747EBE5C4A8C2E68E0FDBC 
{
public:
	// System.Int32 System.Xml.XPath.XPathResultType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XPathResultType_t1043308EA53B9A2365747EBE5C4A8C2E68E0FDBC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHRESULTTYPE_T1043308EA53B9A2365747EBE5C4A8C2E68E0FDBC_H
#ifndef AXIS_T27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70_H
#define AXIS_T27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Axis
struct  Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70  : public AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C
{
public:
	// MS.Internal.Xml.XPath.Axis_AxisType MS.Internal.Xml.XPath.Axis::axisType
	int32_t ___axisType_0;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Axis::input
	AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C * ___input_1;
	// System.String MS.Internal.Xml.XPath.Axis::prefix
	String_t* ___prefix_2;
	// System.String MS.Internal.Xml.XPath.Axis::name
	String_t* ___name_3;
	// System.Xml.XPath.XPathNodeType MS.Internal.Xml.XPath.Axis::nodeType
	int32_t ___nodeType_4;
	// System.Boolean MS.Internal.Xml.XPath.Axis::abbrAxis
	bool ___abbrAxis_5;
	// System.String MS.Internal.Xml.XPath.Axis::urn
	String_t* ___urn_6;

public:
	inline static int32_t get_offset_of_axisType_0() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___axisType_0)); }
	inline int32_t get_axisType_0() const { return ___axisType_0; }
	inline int32_t* get_address_of_axisType_0() { return &___axisType_0; }
	inline void set_axisType_0(int32_t value)
	{
		___axisType_0 = value;
	}

	inline static int32_t get_offset_of_input_1() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___input_1)); }
	inline AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C * get_input_1() const { return ___input_1; }
	inline AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C ** get_address_of_input_1() { return &___input_1; }
	inline void set_input_1(AstNode_tEB8196548F948D593E8705AE90AECD4B02D2D93C * value)
	{
		___input_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_1), value);
	}

	inline static int32_t get_offset_of_prefix_2() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___prefix_2)); }
	inline String_t* get_prefix_2() const { return ___prefix_2; }
	inline String_t** get_address_of_prefix_2() { return &___prefix_2; }
	inline void set_prefix_2(String_t* value)
	{
		___prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_2), value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_nodeType_4() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___nodeType_4)); }
	inline int32_t get_nodeType_4() const { return ___nodeType_4; }
	inline int32_t* get_address_of_nodeType_4() { return &___nodeType_4; }
	inline void set_nodeType_4(int32_t value)
	{
		___nodeType_4 = value;
	}

	inline static int32_t get_offset_of_abbrAxis_5() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___abbrAxis_5)); }
	inline bool get_abbrAxis_5() const { return ___abbrAxis_5; }
	inline bool* get_address_of_abbrAxis_5() { return &___abbrAxis_5; }
	inline void set_abbrAxis_5(bool value)
	{
		___abbrAxis_5 = value;
	}

	inline static int32_t get_offset_of_urn_6() { return static_cast<int32_t>(offsetof(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70, ___urn_6)); }
	inline String_t* get_urn_6() const { return ___urn_6; }
	inline String_t** get_address_of_urn_6() { return &___urn_6; }
	inline void set_urn_6(String_t* value)
	{
		___urn_6 = value;
		Il2CppCodeGenWriteBarrier((&___urn_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPILEDIDENTITYCONSTRAINT_T6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_H
#define COMPILEDIDENTITYCONSTRAINT_T6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.CompiledIdentityConstraint
struct  CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E  : public RuntimeObject
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.CompiledIdentityConstraint::name
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___name_0;
	// System.Xml.Schema.CompiledIdentityConstraint_ConstraintRole System.Xml.Schema.CompiledIdentityConstraint::role
	int32_t ___role_1;
	// System.Xml.Schema.Asttree System.Xml.Schema.CompiledIdentityConstraint::selector
	Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA * ___selector_2;
	// System.Xml.Schema.Asttree[] System.Xml.Schema.CompiledIdentityConstraint::fields
	AsttreeU5BU5D_tE98D3D56BDC1A5B380B40C34285F212D51BF37D3* ___fields_3;
	// System.Xml.XmlQualifiedName System.Xml.Schema.CompiledIdentityConstraint::refer
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___refer_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E, ___name_0)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_name_0() const { return ___name_0; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_role_1() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E, ___role_1)); }
	inline int32_t get_role_1() const { return ___role_1; }
	inline int32_t* get_address_of_role_1() { return &___role_1; }
	inline void set_role_1(int32_t value)
	{
		___role_1 = value;
	}

	inline static int32_t get_offset_of_selector_2() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E, ___selector_2)); }
	inline Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA * get_selector_2() const { return ___selector_2; }
	inline Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA ** get_address_of_selector_2() { return &___selector_2; }
	inline void set_selector_2(Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA * value)
	{
		___selector_2 = value;
		Il2CppCodeGenWriteBarrier((&___selector_2), value);
	}

	inline static int32_t get_offset_of_fields_3() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E, ___fields_3)); }
	inline AsttreeU5BU5D_tE98D3D56BDC1A5B380B40C34285F212D51BF37D3* get_fields_3() const { return ___fields_3; }
	inline AsttreeU5BU5D_tE98D3D56BDC1A5B380B40C34285F212D51BF37D3** get_address_of_fields_3() { return &___fields_3; }
	inline void set_fields_3(AsttreeU5BU5D_tE98D3D56BDC1A5B380B40C34285F212D51BF37D3* value)
	{
		___fields_3 = value;
		Il2CppCodeGenWriteBarrier((&___fields_3), value);
	}

	inline static int32_t get_offset_of_refer_4() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E, ___refer_4)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_refer_4() const { return ___refer_4; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_refer_4() { return &___refer_4; }
	inline void set_refer_4(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___refer_4 = value;
		Il2CppCodeGenWriteBarrier((&___refer_4), value);
	}
};

struct CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_StaticFields
{
public:
	// System.Xml.Schema.CompiledIdentityConstraint System.Xml.Schema.CompiledIdentityConstraint::Empty
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_StaticFields, ___Empty_5)); }
	inline CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * get_Empty_5() const { return ___Empty_5; }
	inline CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E ** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E * value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILEDIDENTITYCONSTRAINT_T6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_H
#ifndef TYPEDATA_T8621F7A6F76A14C9DB043389026C8CDE7FDB5F94_H
#define TYPEDATA_T8621F7A6F76A14C9DB043389026C8CDE7FDB5F94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.TypeData
struct  TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94  : public RuntimeObject
{
public:
	// System.Type System.Xml.Serialization.TypeData::type
	Type_t * ___type_0;
	// System.String System.Xml.Serialization.TypeData::elementName
	String_t* ___elementName_1;
	// System.Xml.Serialization.SchemaTypes System.Xml.Serialization.TypeData::sType
	int32_t ___sType_2;
	// System.Type System.Xml.Serialization.TypeData::listItemType
	Type_t * ___listItemType_3;
	// System.String System.Xml.Serialization.TypeData::typeName
	String_t* ___typeName_4;
	// System.String System.Xml.Serialization.TypeData::fullTypeName
	String_t* ___fullTypeName_5;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.TypeData::listItemTypeData
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * ___listItemTypeData_6;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.TypeData::mappedType
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * ___mappedType_7;
	// System.Xml.Schema.XmlSchemaPatternFacet System.Xml.Serialization.TypeData::facet
	XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322 * ___facet_8;
	// System.Reflection.MethodInfo System.Xml.Serialization.TypeData::typeConvertor
	MethodInfo_t * ___typeConvertor_9;
	// System.Boolean System.Xml.Serialization.TypeData::hasPublicConstructor
	bool ___hasPublicConstructor_10;
	// System.Boolean System.Xml.Serialization.TypeData::nullableOverride
	bool ___nullableOverride_11;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_elementName_1() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94, ___elementName_1)); }
	inline String_t* get_elementName_1() const { return ___elementName_1; }
	inline String_t** get_address_of_elementName_1() { return &___elementName_1; }
	inline void set_elementName_1(String_t* value)
	{
		___elementName_1 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_1), value);
	}

	inline static int32_t get_offset_of_sType_2() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94, ___sType_2)); }
	inline int32_t get_sType_2() const { return ___sType_2; }
	inline int32_t* get_address_of_sType_2() { return &___sType_2; }
	inline void set_sType_2(int32_t value)
	{
		___sType_2 = value;
	}

	inline static int32_t get_offset_of_listItemType_3() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94, ___listItemType_3)); }
	inline Type_t * get_listItemType_3() const { return ___listItemType_3; }
	inline Type_t ** get_address_of_listItemType_3() { return &___listItemType_3; }
	inline void set_listItemType_3(Type_t * value)
	{
		___listItemType_3 = value;
		Il2CppCodeGenWriteBarrier((&___listItemType_3), value);
	}

	inline static int32_t get_offset_of_typeName_4() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94, ___typeName_4)); }
	inline String_t* get_typeName_4() const { return ___typeName_4; }
	inline String_t** get_address_of_typeName_4() { return &___typeName_4; }
	inline void set_typeName_4(String_t* value)
	{
		___typeName_4 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_4), value);
	}

	inline static int32_t get_offset_of_fullTypeName_5() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94, ___fullTypeName_5)); }
	inline String_t* get_fullTypeName_5() const { return ___fullTypeName_5; }
	inline String_t** get_address_of_fullTypeName_5() { return &___fullTypeName_5; }
	inline void set_fullTypeName_5(String_t* value)
	{
		___fullTypeName_5 = value;
		Il2CppCodeGenWriteBarrier((&___fullTypeName_5), value);
	}

	inline static int32_t get_offset_of_listItemTypeData_6() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94, ___listItemTypeData_6)); }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * get_listItemTypeData_6() const { return ___listItemTypeData_6; }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 ** get_address_of_listItemTypeData_6() { return &___listItemTypeData_6; }
	inline void set_listItemTypeData_6(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * value)
	{
		___listItemTypeData_6 = value;
		Il2CppCodeGenWriteBarrier((&___listItemTypeData_6), value);
	}

	inline static int32_t get_offset_of_mappedType_7() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94, ___mappedType_7)); }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * get_mappedType_7() const { return ___mappedType_7; }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 ** get_address_of_mappedType_7() { return &___mappedType_7; }
	inline void set_mappedType_7(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * value)
	{
		___mappedType_7 = value;
		Il2CppCodeGenWriteBarrier((&___mappedType_7), value);
	}

	inline static int32_t get_offset_of_facet_8() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94, ___facet_8)); }
	inline XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322 * get_facet_8() const { return ___facet_8; }
	inline XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322 ** get_address_of_facet_8() { return &___facet_8; }
	inline void set_facet_8(XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322 * value)
	{
		___facet_8 = value;
		Il2CppCodeGenWriteBarrier((&___facet_8), value);
	}

	inline static int32_t get_offset_of_typeConvertor_9() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94, ___typeConvertor_9)); }
	inline MethodInfo_t * get_typeConvertor_9() const { return ___typeConvertor_9; }
	inline MethodInfo_t ** get_address_of_typeConvertor_9() { return &___typeConvertor_9; }
	inline void set_typeConvertor_9(MethodInfo_t * value)
	{
		___typeConvertor_9 = value;
		Il2CppCodeGenWriteBarrier((&___typeConvertor_9), value);
	}

	inline static int32_t get_offset_of_hasPublicConstructor_10() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94, ___hasPublicConstructor_10)); }
	inline bool get_hasPublicConstructor_10() const { return ___hasPublicConstructor_10; }
	inline bool* get_address_of_hasPublicConstructor_10() { return &___hasPublicConstructor_10; }
	inline void set_hasPublicConstructor_10(bool value)
	{
		___hasPublicConstructor_10 = value;
	}

	inline static int32_t get_offset_of_nullableOverride_11() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94, ___nullableOverride_11)); }
	inline bool get_nullableOverride_11() const { return ___nullableOverride_11; }
	inline bool* get_address_of_nullableOverride_11() { return &___nullableOverride_11; }
	inline void set_nullableOverride_11(bool value)
	{
		___nullableOverride_11 = value;
	}
};

struct TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94_StaticFields
{
public:
	// System.String[] System.Xml.Serialization.TypeData::keywords
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___keywords_12;

public:
	inline static int32_t get_offset_of_keywords_12() { return static_cast<int32_t>(offsetof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94_StaticFields, ___keywords_12)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_keywords_12() const { return ___keywords_12; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_keywords_12() { return &___keywords_12; }
	inline void set_keywords_12(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___keywords_12 = value;
		Il2CppCodeGenWriteBarrier((&___keywords_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDATA_T8621F7A6F76A14C9DB043389026C8CDE7FDB5F94_H
#ifndef XMLARRAYATTRIBUTE_T45D35647871B0E473D2E427F41F16C58E91D6B19_H
#define XMLARRAYATTRIBUTE_T45D35647871B0E473D2E427F41F16C58E91D6B19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlArrayAttribute
struct  XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Xml.Serialization.XmlArrayAttribute::elementName
	String_t* ___elementName_0;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlArrayAttribute::form
	int32_t ___form_1;
	// System.Boolean System.Xml.Serialization.XmlArrayAttribute::isNullable
	bool ___isNullable_2;
	// System.String System.Xml.Serialization.XmlArrayAttribute::ns
	String_t* ___ns_3;
	// System.Int32 System.Xml.Serialization.XmlArrayAttribute::order
	int32_t ___order_4;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_form_1() { return static_cast<int32_t>(offsetof(XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19, ___form_1)); }
	inline int32_t get_form_1() const { return ___form_1; }
	inline int32_t* get_address_of_form_1() { return &___form_1; }
	inline void set_form_1(int32_t value)
	{
		___form_1 = value;
	}

	inline static int32_t get_offset_of_isNullable_2() { return static_cast<int32_t>(offsetof(XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19, ___isNullable_2)); }
	inline bool get_isNullable_2() const { return ___isNullable_2; }
	inline bool* get_address_of_isNullable_2() { return &___isNullable_2; }
	inline void set_isNullable_2(bool value)
	{
		___isNullable_2 = value;
	}

	inline static int32_t get_offset_of_ns_3() { return static_cast<int32_t>(offsetof(XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19, ___ns_3)); }
	inline String_t* get_ns_3() const { return ___ns_3; }
	inline String_t** get_address_of_ns_3() { return &___ns_3; }
	inline void set_ns_3(String_t* value)
	{
		___ns_3 = value;
		Il2CppCodeGenWriteBarrier((&___ns_3), value);
	}

	inline static int32_t get_offset_of_order_4() { return static_cast<int32_t>(offsetof(XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19, ___order_4)); }
	inline int32_t get_order_4() const { return ___order_4; }
	inline int32_t* get_address_of_order_4() { return &___order_4; }
	inline void set_order_4(int32_t value)
	{
		___order_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLARRAYATTRIBUTE_T45D35647871B0E473D2E427F41F16C58E91D6B19_H
#ifndef XMLARRAYITEMATTRIBUTE_T820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE_H
#define XMLARRAYITEMATTRIBUTE_T820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlArrayItemAttribute
struct  XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Xml.Serialization.XmlArrayItemAttribute::dataType
	String_t* ___dataType_0;
	// System.String System.Xml.Serialization.XmlArrayItemAttribute::elementName
	String_t* ___elementName_1;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlArrayItemAttribute::form
	int32_t ___form_2;
	// System.String System.Xml.Serialization.XmlArrayItemAttribute::ns
	String_t* ___ns_3;
	// System.Boolean System.Xml.Serialization.XmlArrayItemAttribute::isNullable
	bool ___isNullable_4;
	// System.Boolean System.Xml.Serialization.XmlArrayItemAttribute::isNullableSpecified
	bool ___isNullableSpecified_5;
	// System.Int32 System.Xml.Serialization.XmlArrayItemAttribute::nestingLevel
	int32_t ___nestingLevel_6;
	// System.Type System.Xml.Serialization.XmlArrayItemAttribute::type
	Type_t * ___type_7;

public:
	inline static int32_t get_offset_of_dataType_0() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE, ___dataType_0)); }
	inline String_t* get_dataType_0() const { return ___dataType_0; }
	inline String_t** get_address_of_dataType_0() { return &___dataType_0; }
	inline void set_dataType_0(String_t* value)
	{
		___dataType_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataType_0), value);
	}

	inline static int32_t get_offset_of_elementName_1() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE, ___elementName_1)); }
	inline String_t* get_elementName_1() const { return ___elementName_1; }
	inline String_t** get_address_of_elementName_1() { return &___elementName_1; }
	inline void set_elementName_1(String_t* value)
	{
		___elementName_1 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_1), value);
	}

	inline static int32_t get_offset_of_form_2() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE, ___form_2)); }
	inline int32_t get_form_2() const { return ___form_2; }
	inline int32_t* get_address_of_form_2() { return &___form_2; }
	inline void set_form_2(int32_t value)
	{
		___form_2 = value;
	}

	inline static int32_t get_offset_of_ns_3() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE, ___ns_3)); }
	inline String_t* get_ns_3() const { return ___ns_3; }
	inline String_t** get_address_of_ns_3() { return &___ns_3; }
	inline void set_ns_3(String_t* value)
	{
		___ns_3 = value;
		Il2CppCodeGenWriteBarrier((&___ns_3), value);
	}

	inline static int32_t get_offset_of_isNullable_4() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE, ___isNullable_4)); }
	inline bool get_isNullable_4() const { return ___isNullable_4; }
	inline bool* get_address_of_isNullable_4() { return &___isNullable_4; }
	inline void set_isNullable_4(bool value)
	{
		___isNullable_4 = value;
	}

	inline static int32_t get_offset_of_isNullableSpecified_5() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE, ___isNullableSpecified_5)); }
	inline bool get_isNullableSpecified_5() const { return ___isNullableSpecified_5; }
	inline bool* get_address_of_isNullableSpecified_5() { return &___isNullableSpecified_5; }
	inline void set_isNullableSpecified_5(bool value)
	{
		___isNullableSpecified_5 = value;
	}

	inline static int32_t get_offset_of_nestingLevel_6() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE, ___nestingLevel_6)); }
	inline int32_t get_nestingLevel_6() const { return ___nestingLevel_6; }
	inline int32_t* get_address_of_nestingLevel_6() { return &___nestingLevel_6; }
	inline void set_nestingLevel_6(int32_t value)
	{
		___nestingLevel_6 = value;
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE, ___type_7)); }
	inline Type_t * get_type_7() const { return ___type_7; }
	inline Type_t ** get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(Type_t * value)
	{
		___type_7 = value;
		Il2CppCodeGenWriteBarrier((&___type_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLARRAYITEMATTRIBUTE_T820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE_H
#ifndef XMLATTRIBUTEATTRIBUTE_TD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E_H
#define XMLATTRIBUTEATTRIBUTE_TD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAttributeAttribute
struct  XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Xml.Serialization.XmlAttributeAttribute::attributeName
	String_t* ___attributeName_0;
	// System.String System.Xml.Serialization.XmlAttributeAttribute::dataType
	String_t* ___dataType_1;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlAttributeAttribute::form
	int32_t ___form_2;
	// System.String System.Xml.Serialization.XmlAttributeAttribute::ns
	String_t* ___ns_3;

public:
	inline static int32_t get_offset_of_attributeName_0() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E, ___attributeName_0)); }
	inline String_t* get_attributeName_0() const { return ___attributeName_0; }
	inline String_t** get_address_of_attributeName_0() { return &___attributeName_0; }
	inline void set_attributeName_0(String_t* value)
	{
		___attributeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeName_0), value);
	}

	inline static int32_t get_offset_of_dataType_1() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E, ___dataType_1)); }
	inline String_t* get_dataType_1() const { return ___dataType_1; }
	inline String_t** get_address_of_dataType_1() { return &___dataType_1; }
	inline void set_dataType_1(String_t* value)
	{
		___dataType_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataType_1), value);
	}

	inline static int32_t get_offset_of_form_2() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E, ___form_2)); }
	inline int32_t get_form_2() const { return ___form_2; }
	inline int32_t* get_address_of_form_2() { return &___form_2; }
	inline void set_form_2(int32_t value)
	{
		___form_2 = value;
	}

	inline static int32_t get_offset_of_ns_3() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E, ___ns_3)); }
	inline String_t* get_ns_3() const { return ___ns_3; }
	inline String_t** get_address_of_ns_3() { return &___ns_3; }
	inline void set_ns_3(String_t* value)
	{
		___ns_3 = value;
		Il2CppCodeGenWriteBarrier((&___ns_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTEATTRIBUTE_TD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E_H
#ifndef XMLELEMENTATTRIBUTE_TDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B_H
#define XMLELEMENTATTRIBUTE_TDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlElementAttribute
struct  XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Xml.Serialization.XmlElementAttribute::dataType
	String_t* ___dataType_0;
	// System.String System.Xml.Serialization.XmlElementAttribute::elementName
	String_t* ___elementName_1;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlElementAttribute::form
	int32_t ___form_2;
	// System.String System.Xml.Serialization.XmlElementAttribute::ns
	String_t* ___ns_3;
	// System.Boolean System.Xml.Serialization.XmlElementAttribute::isNullable
	bool ___isNullable_4;
	// System.Type System.Xml.Serialization.XmlElementAttribute::type
	Type_t * ___type_5;
	// System.Int32 System.Xml.Serialization.XmlElementAttribute::order
	int32_t ___order_6;

public:
	inline static int32_t get_offset_of_dataType_0() { return static_cast<int32_t>(offsetof(XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B, ___dataType_0)); }
	inline String_t* get_dataType_0() const { return ___dataType_0; }
	inline String_t** get_address_of_dataType_0() { return &___dataType_0; }
	inline void set_dataType_0(String_t* value)
	{
		___dataType_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataType_0), value);
	}

	inline static int32_t get_offset_of_elementName_1() { return static_cast<int32_t>(offsetof(XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B, ___elementName_1)); }
	inline String_t* get_elementName_1() const { return ___elementName_1; }
	inline String_t** get_address_of_elementName_1() { return &___elementName_1; }
	inline void set_elementName_1(String_t* value)
	{
		___elementName_1 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_1), value);
	}

	inline static int32_t get_offset_of_form_2() { return static_cast<int32_t>(offsetof(XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B, ___form_2)); }
	inline int32_t get_form_2() const { return ___form_2; }
	inline int32_t* get_address_of_form_2() { return &___form_2; }
	inline void set_form_2(int32_t value)
	{
		___form_2 = value;
	}

	inline static int32_t get_offset_of_ns_3() { return static_cast<int32_t>(offsetof(XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B, ___ns_3)); }
	inline String_t* get_ns_3() const { return ___ns_3; }
	inline String_t** get_address_of_ns_3() { return &___ns_3; }
	inline void set_ns_3(String_t* value)
	{
		___ns_3 = value;
		Il2CppCodeGenWriteBarrier((&___ns_3), value);
	}

	inline static int32_t get_offset_of_isNullable_4() { return static_cast<int32_t>(offsetof(XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B, ___isNullable_4)); }
	inline bool get_isNullable_4() const { return ___isNullable_4; }
	inline bool* get_address_of_isNullable_4() { return &___isNullable_4; }
	inline void set_isNullable_4(bool value)
	{
		___isNullable_4 = value;
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B, ___type_5)); }
	inline Type_t * get_type_5() const { return ___type_5; }
	inline Type_t ** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(Type_t * value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier((&___type_5), value);
	}

	inline static int32_t get_offset_of_order_6() { return static_cast<int32_t>(offsetof(XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B, ___order_6)); }
	inline int32_t get_order_6() const { return ___order_6; }
	inline int32_t* get_address_of_order_6() { return &___order_6; }
	inline void set_order_6(int32_t value)
	{
		___order_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTATTRIBUTE_TDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B_H
#ifndef XMLMAPPING_TB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726_H
#define XMLMAPPING_TB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlMapping
struct  XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726  : public RuntimeObject
{
public:
	// System.Xml.Serialization.ObjectMap System.Xml.Serialization.XmlMapping::map
	ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677 * ___map_0;
	// System.Collections.ArrayList System.Xml.Serialization.XmlMapping::relatedMaps
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___relatedMaps_1;
	// System.Xml.Serialization.SerializationFormat System.Xml.Serialization.XmlMapping::format
	int32_t ___format_2;
	// System.String System.Xml.Serialization.XmlMapping::_elementName
	String_t* ____elementName_3;
	// System.String System.Xml.Serialization.XmlMapping::_namespace
	String_t* ____namespace_4;
	// System.String System.Xml.Serialization.XmlMapping::key
	String_t* ___key_5;

public:
	inline static int32_t get_offset_of_map_0() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ___map_0)); }
	inline ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677 * get_map_0() const { return ___map_0; }
	inline ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677 ** get_address_of_map_0() { return &___map_0; }
	inline void set_map_0(ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677 * value)
	{
		___map_0 = value;
		Il2CppCodeGenWriteBarrier((&___map_0), value);
	}

	inline static int32_t get_offset_of_relatedMaps_1() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ___relatedMaps_1)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_relatedMaps_1() const { return ___relatedMaps_1; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_relatedMaps_1() { return &___relatedMaps_1; }
	inline void set_relatedMaps_1(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___relatedMaps_1 = value;
		Il2CppCodeGenWriteBarrier((&___relatedMaps_1), value);
	}

	inline static int32_t get_offset_of_format_2() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ___format_2)); }
	inline int32_t get_format_2() const { return ___format_2; }
	inline int32_t* get_address_of_format_2() { return &___format_2; }
	inline void set_format_2(int32_t value)
	{
		___format_2 = value;
	}

	inline static int32_t get_offset_of__elementName_3() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ____elementName_3)); }
	inline String_t* get__elementName_3() const { return ____elementName_3; }
	inline String_t** get_address_of__elementName_3() { return &____elementName_3; }
	inline void set__elementName_3(String_t* value)
	{
		____elementName_3 = value;
		Il2CppCodeGenWriteBarrier((&____elementName_3), value);
	}

	inline static int32_t get_offset_of__namespace_4() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ____namespace_4)); }
	inline String_t* get__namespace_4() const { return ____namespace_4; }
	inline String_t** get_address_of__namespace_4() { return &____namespace_4; }
	inline void set__namespace_4(String_t* value)
	{
		____namespace_4 = value;
		Il2CppCodeGenWriteBarrier((&____namespace_4), value);
	}

	inline static int32_t get_offset_of_key_5() { return static_cast<int32_t>(offsetof(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726, ___key_5)); }
	inline String_t* get_key_5() const { return ___key_5; }
	inline String_t** get_address_of_key_5() { return &___key_5; }
	inline void set_key_5(String_t* value)
	{
		___key_5 = value;
		Il2CppCodeGenWriteBarrier((&___key_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLMAPPING_TB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726_H
#ifndef XMLSERIALIZATIONWRITERINTERPRETER_T0820AC26DA279FAB2E359E7A655A6582506D3221_H
#define XMLSERIALIZATIONWRITERINTERPRETER_T0820AC26DA279FAB2E359E7A655A6582506D3221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationWriterInterpreter
struct  XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221  : public XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6
{
public:
	// System.Xml.Serialization.XmlMapping System.Xml.Serialization.XmlSerializationWriterInterpreter::_typeMap
	XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * ____typeMap_8;
	// System.Xml.Serialization.SerializationFormat System.Xml.Serialization.XmlSerializationWriterInterpreter::_format
	int32_t ____format_9;

public:
	inline static int32_t get_offset_of__typeMap_8() { return static_cast<int32_t>(offsetof(XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221, ____typeMap_8)); }
	inline XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * get__typeMap_8() const { return ____typeMap_8; }
	inline XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 ** get_address_of__typeMap_8() { return &____typeMap_8; }
	inline void set__typeMap_8(XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726 * value)
	{
		____typeMap_8 = value;
		Il2CppCodeGenWriteBarrier((&____typeMap_8), value);
	}

	inline static int32_t get_offset_of__format_9() { return static_cast<int32_t>(offsetof(XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221, ____format_9)); }
	inline int32_t get__format_9() const { return ____format_9; }
	inline int32_t* get_address_of__format_9() { return &____format_9; }
	inline void set__format_9(int32_t value)
	{
		____format_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZATIONWRITERINTERPRETER_T0820AC26DA279FAB2E359E7A655A6582506D3221_H
#ifndef XMLTYPEMAPELEMENTINFO_T78BD14FFCF51D7812853407BC715516F09FA083F_H
#define XMLTYPEMAPELEMENTINFO_T78BD14FFCF51D7812853407BC715516F09FA083F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapElementInfo
struct  XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F  : public RuntimeObject
{
public:
	// System.String System.Xml.Serialization.XmlTypeMapElementInfo::_elementName
	String_t* ____elementName_0;
	// System.String System.Xml.Serialization.XmlTypeMapElementInfo::_namespace
	String_t* ____namespace_1;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlTypeMapElementInfo::_form
	int32_t ____form_2;
	// System.Xml.Serialization.XmlTypeMapMember System.Xml.Serialization.XmlTypeMapElementInfo::_member
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * ____member_3;
	// System.Object System.Xml.Serialization.XmlTypeMapElementInfo::_choiceValue
	RuntimeObject * ____choiceValue_4;
	// System.Boolean System.Xml.Serialization.XmlTypeMapElementInfo::_isNullable
	bool ____isNullable_5;
	// System.Int32 System.Xml.Serialization.XmlTypeMapElementInfo::_nestingLevel
	int32_t ____nestingLevel_6;
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.XmlTypeMapElementInfo::_mappedType
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * ____mappedType_7;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.XmlTypeMapElementInfo::_type
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * ____type_8;
	// System.Boolean System.Xml.Serialization.XmlTypeMapElementInfo::_wrappedElement
	bool ____wrappedElement_9;
	// System.Int32 System.Xml.Serialization.XmlTypeMapElementInfo::_explicitOrder
	int32_t ____explicitOrder_10;

public:
	inline static int32_t get_offset_of__elementName_0() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____elementName_0)); }
	inline String_t* get__elementName_0() const { return ____elementName_0; }
	inline String_t** get_address_of__elementName_0() { return &____elementName_0; }
	inline void set__elementName_0(String_t* value)
	{
		____elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&____elementName_0), value);
	}

	inline static int32_t get_offset_of__namespace_1() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____namespace_1)); }
	inline String_t* get__namespace_1() const { return ____namespace_1; }
	inline String_t** get_address_of__namespace_1() { return &____namespace_1; }
	inline void set__namespace_1(String_t* value)
	{
		____namespace_1 = value;
		Il2CppCodeGenWriteBarrier((&____namespace_1), value);
	}

	inline static int32_t get_offset_of__form_2() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____form_2)); }
	inline int32_t get__form_2() const { return ____form_2; }
	inline int32_t* get_address_of__form_2() { return &____form_2; }
	inline void set__form_2(int32_t value)
	{
		____form_2 = value;
	}

	inline static int32_t get_offset_of__member_3() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____member_3)); }
	inline XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * get__member_3() const { return ____member_3; }
	inline XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D ** get_address_of__member_3() { return &____member_3; }
	inline void set__member_3(XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D * value)
	{
		____member_3 = value;
		Il2CppCodeGenWriteBarrier((&____member_3), value);
	}

	inline static int32_t get_offset_of__choiceValue_4() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____choiceValue_4)); }
	inline RuntimeObject * get__choiceValue_4() const { return ____choiceValue_4; }
	inline RuntimeObject ** get_address_of__choiceValue_4() { return &____choiceValue_4; }
	inline void set__choiceValue_4(RuntimeObject * value)
	{
		____choiceValue_4 = value;
		Il2CppCodeGenWriteBarrier((&____choiceValue_4), value);
	}

	inline static int32_t get_offset_of__isNullable_5() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____isNullable_5)); }
	inline bool get__isNullable_5() const { return ____isNullable_5; }
	inline bool* get_address_of__isNullable_5() { return &____isNullable_5; }
	inline void set__isNullable_5(bool value)
	{
		____isNullable_5 = value;
	}

	inline static int32_t get_offset_of__nestingLevel_6() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____nestingLevel_6)); }
	inline int32_t get__nestingLevel_6() const { return ____nestingLevel_6; }
	inline int32_t* get_address_of__nestingLevel_6() { return &____nestingLevel_6; }
	inline void set__nestingLevel_6(int32_t value)
	{
		____nestingLevel_6 = value;
	}

	inline static int32_t get_offset_of__mappedType_7() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____mappedType_7)); }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * get__mappedType_7() const { return ____mappedType_7; }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA ** get_address_of__mappedType_7() { return &____mappedType_7; }
	inline void set__mappedType_7(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * value)
	{
		____mappedType_7 = value;
		Il2CppCodeGenWriteBarrier((&____mappedType_7), value);
	}

	inline static int32_t get_offset_of__type_8() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____type_8)); }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * get__type_8() const { return ____type_8; }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 ** get_address_of__type_8() { return &____type_8; }
	inline void set__type_8(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * value)
	{
		____type_8 = value;
		Il2CppCodeGenWriteBarrier((&____type_8), value);
	}

	inline static int32_t get_offset_of__wrappedElement_9() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____wrappedElement_9)); }
	inline bool get__wrappedElement_9() const { return ____wrappedElement_9; }
	inline bool* get_address_of__wrappedElement_9() { return &____wrappedElement_9; }
	inline void set__wrappedElement_9(bool value)
	{
		____wrappedElement_9 = value;
	}

	inline static int32_t get_offset_of__explicitOrder_10() { return static_cast<int32_t>(offsetof(XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F, ____explicitOrder_10)); }
	inline int32_t get__explicitOrder_10() const { return ____explicitOrder_10; }
	inline int32_t* get_address_of__explicitOrder_10() { return &____explicitOrder_10; }
	inline void set__explicitOrder_10(int32_t value)
	{
		____explicitOrder_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPELEMENTINFO_T78BD14FFCF51D7812853407BC715516F09FA083F_H
#ifndef XMLTYPEMAPMEMBERANYELEMENT_TEC6C4FFA530F292BC4C09145DA5937285A664472_H
#define XMLTYPEMAPMEMBERANYELEMENT_TEC6C4FFA530F292BC4C09145DA5937285A664472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberAnyElement
struct  XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472  : public XmlTypeMapMemberExpandable_tEFC74196D6503AB38EEA1AD371BA55EABC7C30ED
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERANYELEMENT_TEC6C4FFA530F292BC4C09145DA5937285A664472_H
#ifndef XMLTYPEMAPMEMBERATTRIBUTE_TD9692D2B3223A558EE5B6FADCED2F92183F16C6C_H
#define XMLTYPEMAPMEMBERATTRIBUTE_TD9692D2B3223A558EE5B6FADCED2F92183F16C6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberAttribute
struct  XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C  : public XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D
{
public:
	// System.String System.Xml.Serialization.XmlTypeMapMemberAttribute::_attributeName
	String_t* ____attributeName_10;
	// System.String System.Xml.Serialization.XmlTypeMapMemberAttribute::_namespace
	String_t* ____namespace_11;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlTypeMapMemberAttribute::_form
	int32_t ____form_12;
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.XmlTypeMapMemberAttribute::_mappedType
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * ____mappedType_13;

public:
	inline static int32_t get_offset_of__attributeName_10() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C, ____attributeName_10)); }
	inline String_t* get__attributeName_10() const { return ____attributeName_10; }
	inline String_t** get_address_of__attributeName_10() { return &____attributeName_10; }
	inline void set__attributeName_10(String_t* value)
	{
		____attributeName_10 = value;
		Il2CppCodeGenWriteBarrier((&____attributeName_10), value);
	}

	inline static int32_t get_offset_of__namespace_11() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C, ____namespace_11)); }
	inline String_t* get__namespace_11() const { return ____namespace_11; }
	inline String_t** get_address_of__namespace_11() { return &____namespace_11; }
	inline void set__namespace_11(String_t* value)
	{
		____namespace_11 = value;
		Il2CppCodeGenWriteBarrier((&____namespace_11), value);
	}

	inline static int32_t get_offset_of__form_12() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C, ____form_12)); }
	inline int32_t get__form_12() const { return ____form_12; }
	inline int32_t* get_address_of__form_12() { return &____form_12; }
	inline void set__form_12(int32_t value)
	{
		____form_12 = value;
	}

	inline static int32_t get_offset_of__mappedType_13() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C, ____mappedType_13)); }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * get__mappedType_13() const { return ____mappedType_13; }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA ** get_address_of__mappedType_13() { return &____mappedType_13; }
	inline void set__mappedType_13(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * value)
	{
		____mappedType_13 = value;
		Il2CppCodeGenWriteBarrier((&____mappedType_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERATTRIBUTE_TD9692D2B3223A558EE5B6FADCED2F92183F16C6C_H
#ifndef XMLTYPEMAPMEMBERFLATLIST_TEA9F25C615A27D3B9761879A55C2BF74E394D31F_H
#define XMLTYPEMAPMEMBERFLATLIST_TEA9F25C615A27D3B9761879A55C2BF74E394D31F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberFlatList
struct  XmlTypeMapMemberFlatList_tEA9F25C615A27D3B9761879A55C2BF74E394D31F  : public XmlTypeMapMemberExpandable_tEFC74196D6503AB38EEA1AD371BA55EABC7C30ED
{
public:
	// System.Xml.Serialization.ListMap System.Xml.Serialization.XmlTypeMapMemberFlatList::_listMap
	ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742 * ____listMap_15;

public:
	inline static int32_t get_offset_of__listMap_15() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberFlatList_tEA9F25C615A27D3B9761879A55C2BF74E394D31F, ____listMap_15)); }
	inline ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742 * get__listMap_15() const { return ____listMap_15; }
	inline ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742 ** get_address_of__listMap_15() { return &____listMap_15; }
	inline void set__listMap_15(ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742 * value)
	{
		____listMap_15 = value;
		Il2CppCodeGenWriteBarrier((&____listMap_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPMEMBERFLATLIST_TEA9F25C615A27D3B9761879A55C2BF74E394D31F_H
#ifndef U3CGETENTITYASYNCU3ED__15_T383C4608618A66FB04F41F38C0534EE9E14CC934_H
#define U3CGETENTITYASYNCU3ED__15_T383C4608618A66FB04F41F38C0534EE9E14CC934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUrlResolver_<GetEntityAsync>d__15
struct  U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934 
{
public:
	// System.Int32 System.Xml.XmlUrlResolver_<GetEntityAsync>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object> System.Xml.XmlUrlResolver_<GetEntityAsync>d__15::<>t__builder
	AsyncTaskMethodBuilder_1_t2A9513A084F4B19851B91EF1F22BB57776D35663  ___U3CU3Et__builder_1;
	// System.Type System.Xml.XmlUrlResolver_<GetEntityAsync>d__15::ofObjectToReturn
	Type_t * ___ofObjectToReturn_2;
	// System.Uri System.Xml.XmlUrlResolver_<GetEntityAsync>d__15::absoluteUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___absoluteUri_3;
	// System.Xml.XmlUrlResolver System.Xml.XmlUrlResolver_<GetEntityAsync>d__15::<>4__this
	XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9 * ___U3CU3E4__this_4;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1_ConfiguredTaskAwaiter<System.IO.Stream> System.Xml.XmlUrlResolver_<GetEntityAsync>d__15::<>u__1
	ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t2A9513A084F4B19851B91EF1F22BB57776D35663  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t2A9513A084F4B19851B91EF1F22BB57776D35663 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t2A9513A084F4B19851B91EF1F22BB57776D35663  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_ofObjectToReturn_2() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934, ___ofObjectToReturn_2)); }
	inline Type_t * get_ofObjectToReturn_2() const { return ___ofObjectToReturn_2; }
	inline Type_t ** get_address_of_ofObjectToReturn_2() { return &___ofObjectToReturn_2; }
	inline void set_ofObjectToReturn_2(Type_t * value)
	{
		___ofObjectToReturn_2 = value;
		Il2CppCodeGenWriteBarrier((&___ofObjectToReturn_2), value);
	}

	inline static int32_t get_offset_of_absoluteUri_3() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934, ___absoluteUri_3)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_absoluteUri_3() const { return ___absoluteUri_3; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_absoluteUri_3() { return &___absoluteUri_3; }
	inline void set_absoluteUri_3(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___absoluteUri_3 = value;
		Il2CppCodeGenWriteBarrier((&___absoluteUri_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934, ___U3CU3E4__this_4)); }
	inline XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934, ___U3CU3Eu__1_5)); }
	inline ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133 * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENTITYASYNCU3ED__15_T383C4608618A66FB04F41F38C0534EE9E14CC934_H
#ifndef DOUBLELINKAXIS_T46C722C5523ACDE575BB767D7794D6CE1C1C5955_H
#define DOUBLELINKAXIS_T46C722C5523ACDE575BB767D7794D6CE1C1C5955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DoubleLinkAxis
struct  DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955  : public Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70
{
public:
	// MS.Internal.Xml.XPath.Axis System.Xml.Schema.DoubleLinkAxis::next
	Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70 * ___next_7;

public:
	inline static int32_t get_offset_of_next_7() { return static_cast<int32_t>(offsetof(DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955, ___next_7)); }
	inline Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70 * get_next_7() const { return ___next_7; }
	inline Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70 ** get_address_of_next_7() { return &___next_7; }
	inline void set_next_7(Axis_t27746009CB5ACF6F272A1FB55CE1F6F0F75B1F70 * value)
	{
		___next_7 = value;
		Il2CppCodeGenWriteBarrier((&___next_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLELINKAXIS_T46C722C5523ACDE575BB767D7794D6CE1C1C5955_H
#ifndef XMLMEMBERSMAPPING_T9ED4FD5026D98AFF2FF78E902B0BD4FE0579C991_H
#define XMLMEMBERSMAPPING_T9ED4FD5026D98AFF2FF78E902B0BD4FE0579C991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlMembersMapping
struct  XmlMembersMapping_t9ED4FD5026D98AFF2FF78E902B0BD4FE0579C991  : public XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726
{
public:
	// System.Boolean System.Xml.Serialization.XmlMembersMapping::_hasWrapperElement
	bool ____hasWrapperElement_6;

public:
	inline static int32_t get_offset_of__hasWrapperElement_6() { return static_cast<int32_t>(offsetof(XmlMembersMapping_t9ED4FD5026D98AFF2FF78E902B0BD4FE0579C991, ____hasWrapperElement_6)); }
	inline bool get__hasWrapperElement_6() const { return ____hasWrapperElement_6; }
	inline bool* get_address_of__hasWrapperElement_6() { return &____hasWrapperElement_6; }
	inline void set__hasWrapperElement_6(bool value)
	{
		____hasWrapperElement_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLMEMBERSMAPPING_T9ED4FD5026D98AFF2FF78E902B0BD4FE0579C991_H
#ifndef XMLSERIALIZATIONWRITECALLBACK_TC98B104DBDBC776D566F853B9DED7668089431FA_H
#define XMLSERIALIZATIONWRITECALLBACK_TC98B104DBDBC776D566F853B9DED7668089431FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializationWriteCallback
struct  XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZATIONWRITECALLBACK_TC98B104DBDBC776D566F853B9DED7668089431FA_H
#ifndef XMLTYPEMAPPING_T2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA_H
#define XMLTYPEMAPPING_T2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapping
struct  XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA  : public XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726
{
public:
	// System.String System.Xml.Serialization.XmlTypeMapping::xmlType
	String_t* ___xmlType_6;
	// System.String System.Xml.Serialization.XmlTypeMapping::xmlTypeNamespace
	String_t* ___xmlTypeNamespace_7;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.XmlTypeMapping::type
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * ___type_8;
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.XmlTypeMapping::baseMap
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * ___baseMap_9;
	// System.Boolean System.Xml.Serialization.XmlTypeMapping::multiReferenceType
	bool ___multiReferenceType_10;
	// System.Boolean System.Xml.Serialization.XmlTypeMapping::includeInSchema
	bool ___includeInSchema_11;
	// System.Boolean System.Xml.Serialization.XmlTypeMapping::isNullable
	bool ___isNullable_12;
	// System.Boolean System.Xml.Serialization.XmlTypeMapping::isAny
	bool ___isAny_13;
	// System.Collections.ArrayList System.Xml.Serialization.XmlTypeMapping::_derivedTypes
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____derivedTypes_14;

public:
	inline static int32_t get_offset_of_xmlType_6() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___xmlType_6)); }
	inline String_t* get_xmlType_6() const { return ___xmlType_6; }
	inline String_t** get_address_of_xmlType_6() { return &___xmlType_6; }
	inline void set_xmlType_6(String_t* value)
	{
		___xmlType_6 = value;
		Il2CppCodeGenWriteBarrier((&___xmlType_6), value);
	}

	inline static int32_t get_offset_of_xmlTypeNamespace_7() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___xmlTypeNamespace_7)); }
	inline String_t* get_xmlTypeNamespace_7() const { return ___xmlTypeNamespace_7; }
	inline String_t** get_address_of_xmlTypeNamespace_7() { return &___xmlTypeNamespace_7; }
	inline void set_xmlTypeNamespace_7(String_t* value)
	{
		___xmlTypeNamespace_7 = value;
		Il2CppCodeGenWriteBarrier((&___xmlTypeNamespace_7), value);
	}

	inline static int32_t get_offset_of_type_8() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___type_8)); }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * get_type_8() const { return ___type_8; }
	inline TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 ** get_address_of_type_8() { return &___type_8; }
	inline void set_type_8(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94 * value)
	{
		___type_8 = value;
		Il2CppCodeGenWriteBarrier((&___type_8), value);
	}

	inline static int32_t get_offset_of_baseMap_9() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___baseMap_9)); }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * get_baseMap_9() const { return ___baseMap_9; }
	inline XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA ** get_address_of_baseMap_9() { return &___baseMap_9; }
	inline void set_baseMap_9(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA * value)
	{
		___baseMap_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseMap_9), value);
	}

	inline static int32_t get_offset_of_multiReferenceType_10() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___multiReferenceType_10)); }
	inline bool get_multiReferenceType_10() const { return ___multiReferenceType_10; }
	inline bool* get_address_of_multiReferenceType_10() { return &___multiReferenceType_10; }
	inline void set_multiReferenceType_10(bool value)
	{
		___multiReferenceType_10 = value;
	}

	inline static int32_t get_offset_of_includeInSchema_11() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___includeInSchema_11)); }
	inline bool get_includeInSchema_11() const { return ___includeInSchema_11; }
	inline bool* get_address_of_includeInSchema_11() { return &___includeInSchema_11; }
	inline void set_includeInSchema_11(bool value)
	{
		___includeInSchema_11 = value;
	}

	inline static int32_t get_offset_of_isNullable_12() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___isNullable_12)); }
	inline bool get_isNullable_12() const { return ___isNullable_12; }
	inline bool* get_address_of_isNullable_12() { return &___isNullable_12; }
	inline void set_isNullable_12(bool value)
	{
		___isNullable_12 = value;
	}

	inline static int32_t get_offset_of_isAny_13() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ___isAny_13)); }
	inline bool get_isAny_13() const { return ___isAny_13; }
	inline bool* get_address_of_isAny_13() { return &___isAny_13; }
	inline void set_isAny_13(bool value)
	{
		___isAny_13 = value;
	}

	inline static int32_t get_offset_of__derivedTypes_14() { return static_cast<int32_t>(offsetof(XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA, ____derivedTypes_14)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__derivedTypes_14() const { return ____derivedTypes_14; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__derivedTypes_14() { return &____derivedTypes_14; }
	inline void set__derivedTypes_14(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____derivedTypes_14 = value;
		Il2CppCodeGenWriteBarrier((&____derivedTypes_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPEMAPPING_T2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA_H
#ifndef HASHCODEOFSTRINGDELEGATE_TCAF2245F039C500045953429EF1FB0BA86326AE8_H
#define HASHCODEOFSTRINGDELEGATE_TCAF2245F039C500045953429EF1FB0BA86326AE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlQualifiedName_HashCodeOfStringDelegate
struct  HashCodeOfStringDelegate_tCAF2245F039C500045953429EF1FB0BA86326AE8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHCODEOFSTRINGDELEGATE_TCAF2245F039C500045953429EF1FB0BA86326AE8_H
#ifndef XMLSERIALIZABLEMAPPING_T7C5AB206754A0134E47A02D23DDBE336BE9DEB0D_H
#define XMLSERIALIZABLEMAPPING_T7C5AB206754A0134E47A02D23DDBE336BE9DEB0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializableMapping
struct  XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D  : public XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Serialization.XmlSerializableMapping::_schema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ____schema_15;
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Serialization.XmlSerializableMapping::_schemaType
	XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * ____schemaType_16;
	// System.Xml.XmlQualifiedName System.Xml.Serialization.XmlSerializableMapping::_schemaTypeName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ____schemaTypeName_17;

public:
	inline static int32_t get_offset_of__schema_15() { return static_cast<int32_t>(offsetof(XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D, ____schema_15)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get__schema_15() const { return ____schema_15; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of__schema_15() { return &____schema_15; }
	inline void set__schema_15(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		____schema_15 = value;
		Il2CppCodeGenWriteBarrier((&____schema_15), value);
	}

	inline static int32_t get_offset_of__schemaType_16() { return static_cast<int32_t>(offsetof(XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D, ____schemaType_16)); }
	inline XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * get__schemaType_16() const { return ____schemaType_16; }
	inline XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 ** get_address_of__schemaType_16() { return &____schemaType_16; }
	inline void set__schemaType_16(XmlSchemaComplexType_tFC7E88DAF03B273154AF9721FACDFF3E7F6C94A9 * value)
	{
		____schemaType_16 = value;
		Il2CppCodeGenWriteBarrier((&____schemaType_16), value);
	}

	inline static int32_t get_offset_of__schemaTypeName_17() { return static_cast<int32_t>(offsetof(XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D, ____schemaTypeName_17)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get__schemaTypeName_17() const { return ____schemaTypeName_17; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of__schemaTypeName_17() { return &____schemaTypeName_17; }
	inline void set__schemaTypeName_17(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		____schemaTypeName_17 = value;
		Il2CppCodeGenWriteBarrier((&____schemaTypeName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZABLEMAPPING_T7C5AB206754A0134E47A02D23DDBE336BE9DEB0D_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (HashCodeOfStringDelegate_tCAF2245F039C500045953429EF1FB0BA86326AE8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9), -1, sizeof(XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2302[4] = 
{
	XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9_StaticFields::get_offset_of_s_DownloadManager_0(),
	XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9::get_offset_of__credentials_1(),
	XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9::get_offset_of__proxy_2(),
	XmlUrlResolver_t43FE24F4601B4197354EFB5783E5CB9F57997EC9::get_offset_of__cachePolicy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[6] = 
{
	U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934::get_offset_of_ofObjectToReturn_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934::get_offset_of_absoluteUri_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934::get_offset_of_U3CU3E4__this_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetEntityAsyncU3Ed__15_t383C4608618A66FB04F41F38C0534EE9E14CC934::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (Res_tAEAB44EC8659FC8A5C53E8AB74EA846A640AD03A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[4] = 
{
	XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321::get_offset_of_pageXmlNmsp_0(),
	XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321::get_offset_of_idxXmlNmsp_1(),
	XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321::get_offset_of_nameTable_2(),
	XPathDocument_tD4D9B64566DEA1CC8DE8219C2CB4515BF0FA5321::get_offset_of_mapNmsp_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (XPathException_t6FE79C310C86CF7BA39A7098B00AACE76BC28E1E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[3] = 
{
	XPathException_t6FE79C310C86CF7BA39A7098B00AACE76BC28E1E::get_offset_of_res_17(),
	XPathException_t6FE79C310C86CF7BA39A7098B00AACE76BC28E1E::get_offset_of_args_18(),
	XPathException_t6FE79C310C86CF7BA39A7098B00AACE76BC28E1E::get_offset_of_message_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (XPathResultType_t1043308EA53B9A2365747EBE5C4A8C2E68E0FDBC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2308[8] = 
{
	XPathResultType_t1043308EA53B9A2365747EBE5C4A8C2E68E0FDBC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (XPathItem_tCFAE3473CA8FED583EE90F08AF79BB5E1D7B3F98), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (XPathNamespaceScope_t2B3F07359A5FD32041B7DD6FB6A452DBA19D5EA2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2310[4] = 
{
	XPathNamespaceScope_t2B3F07359A5FD32041B7DD6FB6A452DBA19D5EA2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3), -1, sizeof(XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2311[4] = 
{
	XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields::get_offset_of_comparer_0(),
	XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields::get_offset_of_NodeTypeLetter_1(),
	XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields::get_offset_of_UniqueIdTbl_2(),
	XPathNavigator_t4E711E4382A896C152FA99D0D39A8C6CEC238EC3_StaticFields::get_offset_of_ContentKindMasks_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (DebuggerDisplayProxy_t7058342D7CBA1293CD786F1EFA632C42E370A97A)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2312[1] = 
{
	DebuggerDisplayProxy_t7058342D7CBA1293CD786F1EFA632C42E370A97A::get_offset_of_nav_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (XPathNavigatorKeyComparer_t6A0E82BEC0BE42351DDB26EAA86333C11E0A9378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (XPathNodeType_tB1DB3F39502492756781F03A9645DD587B81BEF2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2314[11] = 
{
	XPathNodeType_tB1DB3F39502492756781F03A9645DD587B81BEF2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (CodeIdentifier_tDFB64E36DC62ADF524279E6F46F4C002F81A0BE0), -1, sizeof(CodeIdentifier_tDFB64E36DC62ADF524279E6F46F4C002F81A0BE0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2315[1] = 
{
	CodeIdentifier_tDFB64E36DC62ADF524279E6F46F4C002F81A0BE0_StaticFields::get_offset_of_csharp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (XmlSerializerNamespaces_t31E17AE7CF53591901A576C48D1E446BB985676F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[1] = 
{
	XmlSerializerNamespaces_t31E17AE7CF53591901A576C48D1E446BB985676F::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6), -1, sizeof(ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2318[3] = 
{
	ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6::get_offset_of__clrTypes_0(),
	ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6::get_offset_of__schemaTypes_1(),
	ReflectionHelper_tE199EEE7D70F52495BFC2BA2C57EB73A91F512E6_StaticFields::get_offset_of_empty_modifiers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (SchemaTypes_tC46A6854DAA25ADC2A819D4600D261B211E9D7C7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2319[9] = 
{
	SchemaTypes_tC46A6854DAA25ADC2A819D4600D261B211E9D7C7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (XmlTypeConvertorAttribute_tD8D068FF3EE07B1E93D3F5E657558E0E53D3CF4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[1] = 
{
	XmlTypeConvertorAttribute_tD8D068FF3EE07B1E93D3F5E657558E0E53D3CF4B::get_offset_of_U3CMethodU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94), -1, sizeof(TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2321[13] = 
{
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94::get_offset_of_type_0(),
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94::get_offset_of_elementName_1(),
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94::get_offset_of_sType_2(),
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94::get_offset_of_listItemType_3(),
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94::get_offset_of_typeName_4(),
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94::get_offset_of_fullTypeName_5(),
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94::get_offset_of_listItemTypeData_6(),
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94::get_offset_of_mappedType_7(),
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94::get_offset_of_facet_8(),
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94::get_offset_of_typeConvertor_9(),
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94::get_offset_of_hasPublicConstructor_10(),
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94::get_offset_of_nullableOverride_11(),
	TypeData_t8621F7A6F76A14C9DB043389026C8CDE7FDB5F94_StaticFields::get_offset_of_keywords_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (TypeMember_t9BB952F057312683E0A48C45A067C78A0B9F74AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[2] = 
{
	TypeMember_t9BB952F057312683E0A48C45A067C78A0B9F74AB::get_offset_of_type_0(),
	TypeMember_t9BB952F057312683E0A48C45A067C78A0B9F74AB::get_offset_of_member_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (TypeTranslator_t2B73F6806E7647A48688794BCD1E9C3793BE47EA), -1, sizeof(TypeTranslator_t2B73F6806E7647A48688794BCD1E9C3793BE47EA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2323[4] = 
{
	TypeTranslator_t2B73F6806E7647A48688794BCD1E9C3793BE47EA_StaticFields::get_offset_of_nameCache_0(),
	TypeTranslator_t2B73F6806E7647A48688794BCD1E9C3793BE47EA_StaticFields::get_offset_of_primitiveTypes_1(),
	TypeTranslator_t2B73F6806E7647A48688794BCD1E9C3793BE47EA_StaticFields::get_offset_of_primitiveArrayTypes_2(),
	TypeTranslator_t2B73F6806E7647A48688794BCD1E9C3793BE47EA_StaticFields::get_offset_of_nullableTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (XmlAnyAttributeAttribute_tBB53254892DFE4AB748B01108539540EC451C680), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (XmlAnyElementAttribute_tFAA9F7AEF617E275A422AEC9EC38EB6F7376960E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[3] = 
{
	XmlAnyElementAttribute_tFAA9F7AEF617E275A422AEC9EC38EB6F7376960E::get_offset_of_elementName_0(),
	XmlAnyElementAttribute_tFAA9F7AEF617E275A422AEC9EC38EB6F7376960E::get_offset_of_ns_1(),
	XmlAnyElementAttribute_tFAA9F7AEF617E275A422AEC9EC38EB6F7376960E::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (XmlAnyElementAttributes_tED6F51ECCCB48BBB4C5E4D826A7E5CF341F33160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[5] = 
{
	XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19::get_offset_of_elementName_0(),
	XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19::get_offset_of_form_1(),
	XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19::get_offset_of_isNullable_2(),
	XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19::get_offset_of_ns_3(),
	XmlArrayAttribute_t45D35647871B0E473D2E427F41F16C58E91D6B19::get_offset_of_order_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[8] = 
{
	XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE::get_offset_of_dataType_0(),
	XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE::get_offset_of_elementName_1(),
	XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE::get_offset_of_form_2(),
	XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE::get_offset_of_ns_3(),
	XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE::get_offset_of_isNullable_4(),
	XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE::get_offset_of_isNullableSpecified_5(),
	XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE::get_offset_of_nestingLevel_6(),
	XmlArrayItemAttribute_t820765BEF0B3531EE2E06E8BDD5FEF9D61BC41BE::get_offset_of_type_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (XmlArrayItemAttributes_t7BFEEA6B880F1F3FC7DF8EB92C33E65C56FA0A49), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[4] = 
{
	XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E::get_offset_of_attributeName_0(),
	XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E::get_offset_of_dataType_1(),
	XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E::get_offset_of_form_2(),
	XmlAttributeAttribute_tD7DC6A2C6116DA78F1F851A8C5EEB81F8407250E::get_offset_of_ns_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (XmlAttributeOverrides_tE2AE30774D1F8B3860BFE1A8B6233970E88C7F7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[1] = 
{
	XmlAttributeOverrides_tE2AE30774D1F8B3860BFE1A8B6233970E88C7F7E::get_offset_of_overrides_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[14] = 
{
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlAnyAttribute_0(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlAnyElements_1(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlArray_2(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlArrayItems_3(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlAttribute_4(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlChoiceIdentifier_5(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlDefaultValue_6(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlElements_7(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlEnum_8(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlIgnore_9(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlns_10(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlRoot_11(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlText_12(),
	XmlAttributes_t2A5CB1F25B2AB96531D6B3A188E48F9C2BE1077A::get_offset_of_xmlType_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (XmlChoiceIdentifierAttribute_t67E5AE7310D779F7533770C3D3F7CAA65A5E6B8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[1] = 
{
	XmlChoiceIdentifierAttribute_t67E5AE7310D779F7533770C3D3F7CAA65A5E6B8B::get_offset_of_memberName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (XmlCustomFormatter_t20B9A7F147CB04B1552D1B5B1A62F60975441707), -1, sizeof(XmlCustomFormatter_t20B9A7F147CB04B1552D1B5B1A62F60975441707_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2334[1] = 
{
	XmlCustomFormatter_t20B9A7F147CB04B1552D1B5B1A62F60975441707_StaticFields::get_offset_of_allTimeFormats_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[7] = 
{
	XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B::get_offset_of_dataType_0(),
	XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B::get_offset_of_elementName_1(),
	XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B::get_offset_of_form_2(),
	XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B::get_offset_of_ns_3(),
	XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B::get_offset_of_isNullable_4(),
	XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B::get_offset_of_type_5(),
	XmlElementAttribute_tDC5015BE6B1EBF2474C3D44845FF6D5107C1BC2B::get_offset_of_order_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (XmlElementAttributes_t3F224B71C1BC11E8EF6B4E8E45A940B394FA6D34), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (XmlEnumAttribute_tBD75888B09FE971438F4FA55FC302BC5FA3936F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[1] = 
{
	XmlEnumAttribute_tBD75888B09FE971438F4FA55FC302BC5FA3936F0::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (XmlIgnoreAttribute_t42AF5E2677A8626D8F5A1685C84B4E1C35B54AE6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (XmlIncludeAttribute_t2E1C9EC424546DAAAFC56DC6AB70F874A12A9E7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[1] = 
{
	XmlIncludeAttribute_t2E1C9EC424546DAAAFC56DC6AB70F874A12A9E7E::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[6] = 
{
	XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726::get_offset_of_map_0(),
	XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726::get_offset_of_relatedMaps_1(),
	XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726::get_offset_of_format_2(),
	XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726::get_offset_of__elementName_3(),
	XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726::get_offset_of__namespace_4(),
	XmlMapping_tB7CF6BB6E7D3D1B53FEF3813B8ECF7368F6F0726::get_offset_of_key_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (ObjectMap_t53CD0E614E60A73AB445A2A757CCD3C356F31677), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (SerializationFormat_t0A1D537B68CA8F48ACBF510EEA30EFD70F1602A3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2342[3] = 
{
	SerializationFormat_t0A1D537B68CA8F48ACBF510EEA30EFD70F1602A3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (XmlMembersMapping_t9ED4FD5026D98AFF2FF78E902B0BD4FE0579C991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[1] = 
{
	XmlMembersMapping_t9ED4FD5026D98AFF2FF78E902B0BD4FE0579C991::get_offset_of__hasWrapperElement_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (XmlNamespaceDeclarationsAttribute_tB6BE2A5A9D1956B5C33803E844385480EEE97957), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A), -1, sizeof(XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2345[9] = 
{
	XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A::get_offset_of_initialDefaultNamespace_0(),
	XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A::get_offset_of_attributeOverrides_1(),
	XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A::get_offset_of_includedTypes_2(),
	XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A::get_offset_of_helper_3(),
	XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A::get_offset_of_arrayChoiceCount_4(),
	XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A::get_offset_of_relatedMaps_5(),
	XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A::get_offset_of_allowPrivateTypes_6(),
	XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A_StaticFields::get_offset_of_errSimple_7(),
	XmlReflectionImporter_t01142C73A76F11C3781558BEE362720BFDDDED3A_StaticFields::get_offset_of_errSimple2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (U3CU3Ec_t5DDAAEFA8D571BE991743E9ADABA18D78665921F), -1, sizeof(U3CU3Ec_t5DDAAEFA8D571BE991743E9ADABA18D78665921F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2346[2] = 
{
	U3CU3Ec_t5DDAAEFA8D571BE991743E9ADABA18D78665921F_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t5DDAAEFA8D571BE991743E9ADABA18D78665921F_StaticFields::get_offset_of_U3CU3E9__28_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (XmlReflectionMember_t126EB4F0E44820A989FFB6F076234CDDB43D8B2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[5] = 
{
	XmlReflectionMember_t126EB4F0E44820A989FFB6F076234CDDB43D8B2D::get_offset_of_isReturnValue_0(),
	XmlReflectionMember_t126EB4F0E44820A989FFB6F076234CDDB43D8B2D::get_offset_of_memberName_1(),
	XmlReflectionMember_t126EB4F0E44820A989FFB6F076234CDDB43D8B2D::get_offset_of_memberType_2(),
	XmlReflectionMember_t126EB4F0E44820A989FFB6F076234CDDB43D8B2D::get_offset_of_xmlAttributes_3(),
	XmlReflectionMember_t126EB4F0E44820A989FFB6F076234CDDB43D8B2D::get_offset_of_declaringType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[3] = 
{
	XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C::get_offset_of_elementName_0(),
	XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C::get_offset_of_isNullable_1(),
	XmlRootAttribute_t656E9D8E274574F4FF0EEF8920170A435A73F06C::get_offset_of_ns_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (XmlSchemaProviderAttribute_t1E12BA0206734CB8E31915BE0B2DD0B65A6B9F66), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[2] = 
{
	XmlSchemaProviderAttribute_t1E12BA0206734CB8E31915BE0B2DD0B65A6B9F66::get_offset_of__methodName_0(),
	XmlSchemaProviderAttribute_t1E12BA0206734CB8E31915BE0B2DD0B65A6B9F66::get_offset_of__isAny_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (XmlSerializationGeneratedCode_tD121073E4460FCF7F6B549E4621B0CB971D2E6CE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (XmlSerializationWriteCallback_tC98B104DBDBC776D566F853B9DED7668089431FA), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[8] = 
{
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_idGenerator_0(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_qnameCount_1(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_topLevelElement_2(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_namespaces_3(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_writer_4(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_referencedElements_5(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_callbacks_6(),
	XmlSerializationWriter_tD7F886F1AE76D54C208A41797D76E908AA2086F6::get_offset_of_serializedObjects_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[4] = 
{
	WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC::get_offset_of_Type_0(),
	WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC::get_offset_of_TypeName_1(),
	WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC::get_offset_of_TypeNs_2(),
	WriteCallbackInfo_t20F1DFAFFBA7B903CBCCFA106E37D0CFFCEBA7CC::get_offset_of_Callback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[2] = 
{
	XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221::get_offset_of__typeMap_8(),
	XmlSerializationWriterInterpreter_t0820AC26DA279FAB2E359E7A655A6582506D3221::get_offset_of__format_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (CallbackInfo_t6FC419FB131647181747C58C4AA7642E18071292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[2] = 
{
	CallbackInfo_t6FC419FB131647181747C58C4AA7642E18071292::get_offset_of__swi_0(),
	CallbackInfo_t6FC419FB131647181747C58C4AA7642E18071292::get_offset_of__typeMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C), -1, sizeof(XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2356[9] = 
{
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields::get_offset_of_generationThreshold_0(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields::get_offset_of_backgroundGeneration_1(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields::get_offset_of_deleteTempFiles_2(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields::get_offset_of_generatorFallback_3(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C::get_offset_of_customSerializer_4(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C::get_offset_of_typeMapping_5(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C::get_offset_of_serializerData_6(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields::get_offset_of_serializerTypes_7(),
	XmlSerializer_t01B5C784057A14663B4041A53328764327555D5C_StaticFields::get_offset_of_DefaultEncoding_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[3] = 
{
	SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8::get_offset_of_WriterType_0(),
	SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8::get_offset_of_WriterMethod_1(),
	SerializerData_tFD8E75C5BC50CD6C3A3F382789E88E422381A7A8::get_offset_of_Implementation_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (XmlSerializerImplementation_tF78BC93462D4003D83EAA821331747A0624F837D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[2] = 
{
	XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7::get_offset_of_dataType_0(),
	XmlTextAttribute_t260DF760832CEB66E748BDEDE998C3480FB902A7::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[3] = 
{
	XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08::get_offset_of_includeInSchema_0(),
	XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08::get_offset_of_ns_1(),
	XmlTypeAttribute_t8386F99942A9FABF3B9AF64324C28F674EE31E08::get_offset_of_typeName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[11] = 
{
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__elementName_0(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__namespace_1(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__form_2(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__member_3(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__choiceValue_4(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__isNullable_5(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__nestingLevel_6(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__mappedType_7(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__type_8(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__wrappedElement_9(),
	XmlTypeMapElementInfo_t78BD14FFCF51D7812853407BC715516F09FA083F::get_offset_of__explicitOrder_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (XmlTypeMapElementInfoList_tCD7D387F78138B4E137D285319EC10532B14FBB0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[10] = 
{
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__name_0(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__index_1(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__globalIndex_2(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__specifiedGlobalIndex_3(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__typeData_4(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__member_5(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__specifiedMember_6(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__shouldSerialize_7(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__defaultValue_8(),
	XmlTypeMapMember_t8272D1E62F842F97D749F6FFC6B4AE57754C914D::get_offset_of__flags_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[4] = 
{
	XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C::get_offset_of__attributeName_10(),
	XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C::get_offset_of__namespace_11(),
	XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C::get_offset_of__form_12(),
	XmlTypeMapMemberAttribute_tD9692D2B3223A558EE5B6FADCED2F92183F16C6C::get_offset_of__mappedType_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2365[4] = 
{
	XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00::get_offset_of__elementInfo_10(),
	XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00::get_offset_of__choiceMember_11(),
	XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00::get_offset_of__isTextCollector_12(),
	XmlTypeMapMemberElement_t51AF456B8ECA454E76169688454E82735FB1AE00::get_offset_of__choiceTypeData_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (XmlTypeMapMemberList_t408EB21F4809E9C2E2059BDE5F77F28F503EA48F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (XmlTypeMapMemberExpandable_tEFC74196D6503AB38EEA1AD371BA55EABC7C30ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2367[1] = 
{
	XmlTypeMapMemberExpandable_tEFC74196D6503AB38EEA1AD371BA55EABC7C30ED::get_offset_of__flatArrayIndex_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (XmlTypeMapMemberFlatList_tEA9F25C615A27D3B9761879A55C2BF74E394D31F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[1] = 
{
	XmlTypeMapMemberFlatList_tEA9F25C615A27D3B9761879A55C2BF74E394D31F::get_offset_of__listMap_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (XmlTypeMapMemberAnyElement_tEC6C4FFA530F292BC4C09145DA5937285A664472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (XmlTypeMapMemberAnyAttribute_t0D4E196AF4FEEE5092B2FF4111A717B5C3E459BE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (XmlTypeMapMemberNamespaces_t08BCB01966D11BB8968E6C39E1ACF60C9EEE8C43), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[9] = 
{
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_xmlType_6(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_xmlTypeNamespace_7(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_type_8(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_baseMap_9(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_multiReferenceType_10(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_includeInSchema_11(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_isNullable_12(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of_isAny_13(),
	XmlTypeMapping_t2EEAB82D2FD8C11B68D4C98CF19A78DADDB1E4CA::get_offset_of__derivedTypes_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2373[3] = 
{
	XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D::get_offset_of__schema_15(),
	XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D::get_offset_of__schemaType_16(),
	XmlSerializableMapping_t7C5AB206754A0134E47A02D23DDBE336BE9DEB0D::get_offset_of__schemaTypeName_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[15] = 
{
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__elements_0(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__elementMembers_1(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__attributeMembers_2(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__attributeMembersArray_3(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__flatLists_4(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__allMembers_5(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__membersWithDefault_6(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__listMembers_7(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__defaultAnyElement_8(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__defaultAnyAttribute_9(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__namespaceDeclarations_10(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__xmlTextCollector_11(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__returnMember_12(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__ignoreMemberNamespace_13(),
	ClassMap_t7673ADE61E787987E671A668712C12D77FAAC6DA::get_offset_of__canBeSimpleType_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[2] = 
{
	ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742::get_offset_of__itemInfo_0(),
	ListMap_tAE3FA433501B26F844EF9942DB4C054DA7CE2742::get_offset_of__choiceMember_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[5] = 
{
	EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A::get_offset_of__members_0(),
	EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A::get_offset_of__isFlags_1(),
	EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A::get_offset_of__enumNames_2(),
	EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A::get_offset_of__xmlNames_3(),
	EnumMap_tD7B24FAC46ED105487BE3269534CBB5072D8652A::get_offset_of__values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[3] = 
{
	EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3::get_offset_of__xmlName_0(),
	EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3::get_offset_of__enumName_1(),
	EnumMapMember_t508AEADF6DA5EFA4F7B0522FA689C3A8638118E3::get_offset_of__value_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[4] = 
{
	AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80::get_offset_of_curNode_0(),
	AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80::get_offset_of_rootDepth_1(),
	AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80::get_offset_of_curDepth_2(),
	AxisElement_t8DC31C9834DF2F50E9E4D245D07C1AE68F226D80::get_offset_of_isMatch_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[3] = 
{
	AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2::get_offset_of_stack_0(),
	AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2::get_offset_of_subtree_1(),
	AxisStack_t3AA7511C349203BE08B1ECF76EA6C0031534C4A2::get_offset_of_parent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[4] = 
{
	ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E::get_offset_of_currentDepth_0(),
	ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E::get_offset_of_isActive_1(),
	ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E::get_offset_of_axisTree_2(),
	ActiveAxis_tD7958CF29FECEF35D850FA29624E57EF040E315E::get_offset_of_axisStack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[1] = 
{
	DoubleLinkAxis_t46C722C5523ACDE575BB767D7794D6CE1C1C5955::get_offset_of_next_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[5] = 
{
	ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253::get_offset_of_topNode_0(),
	ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253::get_offset_of_rootNode_1(),
	ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253::get_offset_of_isAttribute_2(),
	ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253::get_offset_of_isDss_3(),
	ForwardAxis_tD7F42B3CDF95D130675D57307CC20296D6829253::get_offset_of_isSelfAxis_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[4] = 
{
	Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA::get_offset_of_fAxisArray_0(),
	Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA::get_offset_of_xpathexpr_1(),
	Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA::get_offset_of_isField_2(),
	Asttree_t9D8B0988F0376119C895FC560A1A81FB9C452FCA::get_offset_of_nsmgr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (AutoValidator_t348DF836BF1829DC3B8DBA19A6E0EFC1A6EE85EB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[6] = 
{
	BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427::get_offset_of_nameTable_0(),
	BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427::get_offset_of_schemaNames_1(),
	BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427::get_offset_of_eventHandler_2(),
	BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427::get_offset_of_compilationSettings_3(),
	BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427::get_offset_of_errorCount_4(),
	BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427::get_offset_of_NsXml_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[15] = 
{
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_schemaCollection_0(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_eventHandling_1(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_nameTable_2(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_schemaNames_3(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_positionInfo_4(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_xmlResolver_5(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_baseUri_6(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_schemaInfo_7(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_reader_8(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_elementName_9(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_context_10(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_textValue_11(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_textString_12(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_hasSibling_13(),
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43::get_offset_of_checkDatatype_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (BitSet_t0E4C53EC600670A4B74C5671553596978880138C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[2] = 
{
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C::get_offset_of_count_0(),
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C::get_offset_of_bits_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[4] = 
{
	ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D::get_offset_of_targetNS_0(),
	ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D::get_offset_of_chameleonLocation_1(),
	ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D::get_offset_of_originalSchema_2(),
	ChameleonKey_tB2DC0F8044027C691C7325267CB62C0BBDB1CB2D::get_offset_of_hashCode_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E), -1, sizeof(CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2389[6] = 
{
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E::get_offset_of_name_0(),
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E::get_offset_of_role_1(),
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E::get_offset_of_selector_2(),
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E::get_offset_of_fields_3(),
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E::get_offset_of_refer_4(),
	CompiledIdentityConstraint_t6BF02C65AF10432D69198C6F7B1D6874BDB75A9E_StaticFields::get_offset_of_Empty_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (ConstraintRole_tDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2390[4] = 
{
	ConstraintRole_tDF2C7F62C4BF46B0BDDB0F70B8FDE3DBF2BB4397::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[6] = 
{
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54::get_offset_of_constraint_0(),
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54::get_offset_of_axisSelector_1(),
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54::get_offset_of_axisFields_2(),
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54::get_offset_of_qualifiedTable_3(),
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54::get_offset_of_keyrefTable_4(),
	ConstraintStruct_t3BDD584A0E978A3DC9EF55B9780CDAA8557F4A54::get_offset_of_tableDim_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[3] = 
{
	LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5::get_offset_of_column_4(),
	LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5::get_offset_of_isMatched_5(),
	LocatedActiveAxis_tCCF3C0E41A32AC342B9B40531789937908E510D5::get_offset_of_Ks_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[3] = 
{
	SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64::get_offset_of_cs_4(),
	SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64::get_offset_of_KSs_5(),
	SelectorActiveAxis_t7EA444AA4AD068A5F4A8C12A756792925AF59A64::get_offset_of_KSpointer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[3] = 
{
	KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E::get_offset_of_depth_0(),
	KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E::get_offset_of_ks_1(),
	KSStruct_tB1C37F7C8A40E2FE6DA9177ACF331189D3EB022E::get_offset_of_fields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[6] = 
{
	TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D::get_offset_of_dstruct_0(),
	TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D::get_offset_of_ovalue_1(),
	TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D::get_offset_of_svalue_2(),
	TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D::get_offset_of_xsdtype_3(),
	TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D::get_offset_of_dim_4(),
	TypedObject_t4DD26D496368DF76E600AAF06AADEE3E5E49FF9D::get_offset_of_isList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[2] = 
{
	DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11::get_offset_of_isDecimal_0(),
	DecimalStruct_tD351714FCA963CEC4A7E529F0066EE965EA7DD11::get_offset_of_dvalue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[5] = 
{
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE::get_offset_of_ks_0(),
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE::get_offset_of_dim_1(),
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE::get_offset_of_hashcode_2(),
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE::get_offset_of_posline_3(),
	KeySequence_tEDAF2417185973D2C57AB5B1A35D379652C484BE::get_offset_of_poscol_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (UpaException_tED69C4AC662CA80677ABA101231042E747107151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[2] = 
{
	UpaException_tED69C4AC662CA80677ABA101231042E747107151::get_offset_of_particle1_17(),
	UpaException_tED69C4AC662CA80677ABA101231042E747107151::get_offset_of_particle2_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[6] = 
{
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6::get_offset_of_last_0(),
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6::get_offset_of_names_1(),
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6::get_offset_of_wildcards_2(),
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6::get_offset_of_particles_3(),
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6::get_offset_of_particleLast_4(),
	SymbolsDictionary_tD5FE8542DCD1430A2E53390D10EE48FCA52227A6::get_offset_of_isUpaEnforced_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
