﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// EhbFMnIgrveIkvxMjDinIUjSQTaf
struct EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5;
// LJAWzBuJIsAgLPPZeEUKfpasUWYi
struct LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384;
// Rewired.ButtonStateRecorder
struct ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148;
// Rewired.CalibrationMap
struct CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3;
// Rewired.Controller
struct Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671;
// Rewired.Controller/Axis2D[]
struct Axis2DU5BU5D_t3EA76FC4806A467C9FF20A2DB0C67407FEFFAB01;
// Rewired.Controller/Axis[]
struct AxisU5BU5D_t4A9D9CB010B4F05D4472FB918F08E8B1434B7BEB;
// Rewired.Controller/Button[]
struct ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C;
// Rewired.Controller/CompoundElement/OqivKqJqmnbJCfstNgONqfwyygM[]
struct OqivKqJqmnbJCfstNgONqfwyygMU5BU5D_t6A7F37C622A08B7738262EF07A8E50A0D7C112CC;
// Rewired.Controller/Element
struct Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5;
// Rewired.Controller/Element/ViIrqleiiIiQeymndbeHmwQgupz
struct ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416;
// Rewired.Controller/Element/ViIrqleiiIiQeymndbeHmwQgupz/LgyDESrvlXkrldhlJfbWEsuKfeVY
struct LgyDESrvlXkrldhlJfbWEsuKfeVY_t861AA9B44D7A8C3C4D68C812E433DD1D8F09803D;
// Rewired.Controller/Element/ViIrqleiiIiQeymndbeHmwQgupz/LgyDESrvlXkrldhlJfbWEsuKfeVY[]
struct LgyDESrvlXkrldhlJfbWEsuKfeVYU5BU5D_tD84BF7E1E9CB321A501CCD29EA0C82CAD309D17F;
// Rewired.Controller/Extension
struct Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD;
// Rewired.Controller/Hat[]
struct HatU5BU5D_tA98392C24E41A77FBD67D0260A24A3AD18B22B5E;
// Rewired.ControllerDataUpdater
struct ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F;
// Rewired.ControllerExtensions.DualShock4Extension/tZdHWjEZWrnwXHpafbBUORQRReH
struct tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D;
// Rewired.ControllerExtensions.RailDriverExtension/ffvcqZUeADdPtkxnXkBtOZLlBgQx
struct ffvcqZUeADdPtkxnXkBtOZLlBgQx_tD4D732B3A84E0F9D7F1391C9276759C09B2B03E6;
// Rewired.ControllerExtensions.SteamControllerExtension/tBEetyAkXzmxTaZXitHgYebQLVui
struct tBEetyAkXzmxTaZXitHgYebQLVui_tCBAE66D0EF5D308109B13047A13AE8C86F80185E;
// Rewired.ControllerTemplate/GfqjFzggDiRITiiqcxubCCJucSZL[]
struct GfqjFzggDiRITiiqcxubCCJucSZLU5BU5D_t67419B8798C6BB4669E248C852383AEB826E7C66;
// Rewired.ControllerTemplate/hkuffRDZsiyCBEXTWgHFcjFwHWW[]
struct hkuffRDZsiyCBEXTWgHFcjFwHWWU5BU5D_t4BCE28438EFD7AEBAF6ACD08D75CEE279515BF39;
// Rewired.ControllerWithAxes
struct ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA;
// Rewired.Data.Mapping.HardwareAxisInfo
struct HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4;
// Rewired.Data.Mapping.HardwareButtonInfo
struct HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273;
// Rewired.Data.Mapping.IHardwareControllerTemplateMap_Internal
struct IHardwareControllerTemplateMap_Internal_t119A68B7FCC1CF7E413A3456D099FA04F6170896;
// Rewired.Drivers.Interfaces.IDriver_DualShock4
struct IDriver_DualShock4_tFB3972C7A75D97CD423CA2BACAD73030ED190209;
// Rewired.Drivers.Interfaces.IDriver_RailDriver
struct IDriver_RailDriver_tF9E224A758AA90709F89F40B51040D7024B1D1E1;
// Rewired.HardwareControllerMap_Game
struct HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3;
// Rewired.IControllerElementTarget
struct IControllerElementTarget_tBECEEA8A0AF0C16A3D41416E35B98CFD09422658;
// Rewired.IControllerTemplate
struct IControllerTemplate_tB89722FA815A90C11CB2E71E1861B539F69285F3;
// Rewired.IControllerTemplateElement
struct IControllerTemplateElement_t459175B17801437F275095D98285CB22E2F08C66;
// Rewired.IControllerTemplateElement[]
struct IControllerTemplateElementU5BU5D_t1660B9BD48816BC80A9C52B74BFCF80922F1C060;
// Rewired.IControllerTemplate[]
struct IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6;
// Rewired.Interfaces.IControllerExtensionSource
struct IControllerExtensionSource_tB7B0912C0560F4E99E7927ED74A63303585AD9F3;
// Rewired.Interfaces.IInputManagerJoystickPublic
struct IInputManagerJoystickPublic_t4D44B08E924E115C6EE1E41987B26DB151F7C30B;
// Rewired.Interfaces.ISteamControllerInternal
struct ISteamControllerInternal_tBC8A0A41CDDAACD1C698F3C783BED32478C5DE28;
// Rewired.Interfaces.IUnifiedKeyboardSource
struct IUnifiedKeyboardSource_t94183BB51C9B2A7CA8011D4C731C2CB013B5E591;
// Rewired.Interfaces.IUnifiedMouseSource
struct IUnifiedMouseSource_tF1958F0193D41ADC2F469AE84F413647B71B31FB;
// Rewired.JoystickType[]
struct JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA;
// Rewired.Keyboard
struct Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004;
// Rewired.KeyboardKeyCode[]
struct KeyboardKeyCodeU5BU5D_t88F2320800F8F14E7935678C5AA693EEF40514B2;
// Rewired.PidVid[]
struct PidVidU5BU5D_tF8661A2C9138A10268BF87CD9D6C36EE964F2B51;
// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,Rewired.IControllerTemplateElement>
struct ADictionary_2_t68A8B0123876EA082FBDA6132BFFCD5DB743965D;
// Rewired.Utils.Classes.Data.ADictionary`2<System.String,Rewired.IControllerTemplateElement>
struct ADictionary_2_tD6BAFD11BFA92734BD04AC9CA5B5B7B493FAB1E7;
// Rewired.Utils.Classes.Utility.IObjectPool
struct IObjectPool_t5EAF82120F17121BF7F7E4835AAB71A92BFB5ACB;
// Rewired.Utils.Classes.Utility.ObjectPool`1<agYfxXkuAOdBmliRwltxLMjyFHG>
struct ObjectPool_1_tB6807F662FF447933F0831D0B781A0C387428B91;
// Rewired.Utils.Classes.Utility.TimerAbs
struct TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429;
// Rewired.Utils.Classes.Utility.TimerAbs[]
struct TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo>
struct IEnumerator_1_t5448337C1643DF9D47E013DBFF871416D4D3F5DC;
// System.Collections.Generic.IList`1<Rewired.Controller/Element>
struct IList_1_t743C6DA41FA4F41F7DDEC5C049DA8C51F315819C;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t946CA250FDCDAF3493418F5354C7D62897FA0DE8;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_tFFB5515FC97391D04D5034ED7F334357FED1FAE6;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller/Axis2D>
struct ReadOnlyCollection_1_t0DE413092804EC8FE479436075E87B4EC39398DD;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller/Axis>
struct ReadOnlyCollection_1_t2FC4D82F7AABEAB28A14216C3A57818B355A98EB;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller/Button>
struct ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller/Element/ViIrqleiiIiQeymndbeHmwQgupz/LgyDESrvlXkrldhlJfbWEsuKfeVY>
struct ReadOnlyCollection_1_t3253802893B57BA7CB1891841097377E60D0CC29;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller/Element>
struct ReadOnlyCollection_1_tBF1621D3F7F9871C04D918E0DF726F42C36F91F4;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller/Hat>
struct ReadOnlyCollection_1_tCB8970A81C163E87CB4AFB390129098C4DA72E82;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.IControllerTemplate>
struct ReadOnlyCollection_1_t29628C2D08376268FE32FB6C6D7854BE606C1802;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.IControllerTemplateElement>
struct ReadOnlyCollection_1_tBD7D8D2E9C5F8B4B0C4FF78D2202E23AC1F7F65B;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.JoystickType>
struct ReadOnlyCollection_1_tF2D56725AA006BC417608E3AA831C086DC3FB2F3;
// System.Func`1<agYfxXkuAOdBmliRwltxLMjyFHG>
struct Func_1_t11F5484DB54AD62A37F5BAC05DCF931BAE388545;
// System.Func`2<Rewired.KeyboardKeyCode,System.Int32>
struct Func_2_t20DE2EBC38186DD09844EAE5633BA6EF803D8900;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E;
// System.Func`2<System.Int32,System.Single>
struct Func_2_t3E6DA33748FAB0ECE27E377896D0680D974FE1CF;
// System.Func`3<Rewired.Controller,System.Guid,System.Boolean>
struct Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE;
// System.Func`3<Rewired.Controller,System.Type,System.Boolean>
struct Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef OQIVKQJQMNBJCFSTNGONQFWYYGM_TEAF2F982AA4612530E700293E930ABD6AEBF3B1B_H
#define OQIVKQJQMNBJCFSTNGONQFWYYGM_TEAF2F982AA4612530E700293E930ABD6AEBF3B1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_CompoundElement_OqivKqJqmnbJCfstNgONqfwyygM
struct  OqivKqJqmnbJCfstNgONqfwyygM_tEAF2F982AA4612530E700293E930ABD6AEBF3B1B  : public RuntimeObject
{
public:
	// Rewired.Controller_Element Rewired.Controller_CompoundElement_OqivKqJqmnbJCfstNgONqfwyygM::YCBcgPOLoWsaeULLzpTLswYuYbQ
	Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 * ___YCBcgPOLoWsaeULLzpTLswYuYbQ_0;
	// System.Int32 Rewired.Controller_CompoundElement_OqivKqJqmnbJCfstNgONqfwyygM::nRRgINdksoiMPGmVMZmtHuHLdkgI
	int32_t ___nRRgINdksoiMPGmVMZmtHuHLdkgI_1;

public:
	inline static int32_t get_offset_of_YCBcgPOLoWsaeULLzpTLswYuYbQ_0() { return static_cast<int32_t>(offsetof(OqivKqJqmnbJCfstNgONqfwyygM_tEAF2F982AA4612530E700293E930ABD6AEBF3B1B, ___YCBcgPOLoWsaeULLzpTLswYuYbQ_0)); }
	inline Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 * get_YCBcgPOLoWsaeULLzpTLswYuYbQ_0() const { return ___YCBcgPOLoWsaeULLzpTLswYuYbQ_0; }
	inline Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 ** get_address_of_YCBcgPOLoWsaeULLzpTLswYuYbQ_0() { return &___YCBcgPOLoWsaeULLzpTLswYuYbQ_0; }
	inline void set_YCBcgPOLoWsaeULLzpTLswYuYbQ_0(Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 * value)
	{
		___YCBcgPOLoWsaeULLzpTLswYuYbQ_0 = value;
		Il2CppCodeGenWriteBarrier((&___YCBcgPOLoWsaeULLzpTLswYuYbQ_0), value);
	}

	inline static int32_t get_offset_of_nRRgINdksoiMPGmVMZmtHuHLdkgI_1() { return static_cast<int32_t>(offsetof(OqivKqJqmnbJCfstNgONqfwyygM_tEAF2F982AA4612530E700293E930ABD6AEBF3B1B, ___nRRgINdksoiMPGmVMZmtHuHLdkgI_1)); }
	inline int32_t get_nRRgINdksoiMPGmVMZmtHuHLdkgI_1() const { return ___nRRgINdksoiMPGmVMZmtHuHLdkgI_1; }
	inline int32_t* get_address_of_nRRgINdksoiMPGmVMZmtHuHLdkgI_1() { return &___nRRgINdksoiMPGmVMZmtHuHLdkgI_1; }
	inline void set_nRRgINdksoiMPGmVMZmtHuHLdkgI_1(int32_t value)
	{
		___nRRgINdksoiMPGmVMZmtHuHLdkgI_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OQIVKQJQMNBJCFSTNGONQFWYYGM_TEAF2F982AA4612530E700293E930ABD6AEBF3B1B_H
#ifndef VIIRQLEIIIIQEYMNDBEHMWQGUPZ_TB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416_H
#define VIIRQLEIIIIQEYMNDBEHMWQGUPZ_TB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz
struct  ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz::hBucQzYvWtOAWgdnwwVORAQRwyd
	int32_t ___hBucQzYvWtOAWgdnwwVORAQRwyd_0;
	// System.Int32[] Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz::bJGqRCGyLwMdbyXFtRqWyHanjhF
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___bJGqRCGyLwMdbyXFtRqWyHanjhF_1;
	// Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz_LgyDESrvlXkrldhlJfbWEsuKfeVY[] Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz::XzwvNxyYkXcaiidxwJGvQNwBigT
	LgyDESrvlXkrldhlJfbWEsuKfeVYU5BU5D_tD84BF7E1E9CB321A501CCD29EA0C82CAD309D17F* ___XzwvNxyYkXcaiidxwJGvQNwBigT_2;
	// Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz_LgyDESrvlXkrldhlJfbWEsuKfeVY Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz::YaDAnfimULzphKnSejtwEcfSlkEJ
	LgyDESrvlXkrldhlJfbWEsuKfeVY_t861AA9B44D7A8C3C4D68C812E433DD1D8F09803D * ___YaDAnfimULzphKnSejtwEcfSlkEJ_3;
	// System.Int32 Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz::uiElHIMsJhOUGwxstGSvtQgdJHm
	int32_t ___uiElHIMsJhOUGwxstGSvtQgdJHm_4;
	// System.Int32 Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz::mitExlcuToNzViGEORWHsMktptx
	int32_t ___mitExlcuToNzViGEORWHsMktptx_5;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz_LgyDESrvlXkrldhlJfbWEsuKfeVY> Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz::xnWxJKPxjErcevqVtlosHZHrsO
	ReadOnlyCollection_1_t3253802893B57BA7CB1891841097377E60D0CC29 * ___xnWxJKPxjErcevqVtlosHZHrsO_6;

public:
	inline static int32_t get_offset_of_hBucQzYvWtOAWgdnwwVORAQRwyd_0() { return static_cast<int32_t>(offsetof(ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416, ___hBucQzYvWtOAWgdnwwVORAQRwyd_0)); }
	inline int32_t get_hBucQzYvWtOAWgdnwwVORAQRwyd_0() const { return ___hBucQzYvWtOAWgdnwwVORAQRwyd_0; }
	inline int32_t* get_address_of_hBucQzYvWtOAWgdnwwVORAQRwyd_0() { return &___hBucQzYvWtOAWgdnwwVORAQRwyd_0; }
	inline void set_hBucQzYvWtOAWgdnwwVORAQRwyd_0(int32_t value)
	{
		___hBucQzYvWtOAWgdnwwVORAQRwyd_0 = value;
	}

	inline static int32_t get_offset_of_bJGqRCGyLwMdbyXFtRqWyHanjhF_1() { return static_cast<int32_t>(offsetof(ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416, ___bJGqRCGyLwMdbyXFtRqWyHanjhF_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_bJGqRCGyLwMdbyXFtRqWyHanjhF_1() const { return ___bJGqRCGyLwMdbyXFtRqWyHanjhF_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_bJGqRCGyLwMdbyXFtRqWyHanjhF_1() { return &___bJGqRCGyLwMdbyXFtRqWyHanjhF_1; }
	inline void set_bJGqRCGyLwMdbyXFtRqWyHanjhF_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___bJGqRCGyLwMdbyXFtRqWyHanjhF_1 = value;
		Il2CppCodeGenWriteBarrier((&___bJGqRCGyLwMdbyXFtRqWyHanjhF_1), value);
	}

	inline static int32_t get_offset_of_XzwvNxyYkXcaiidxwJGvQNwBigT_2() { return static_cast<int32_t>(offsetof(ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416, ___XzwvNxyYkXcaiidxwJGvQNwBigT_2)); }
	inline LgyDESrvlXkrldhlJfbWEsuKfeVYU5BU5D_tD84BF7E1E9CB321A501CCD29EA0C82CAD309D17F* get_XzwvNxyYkXcaiidxwJGvQNwBigT_2() const { return ___XzwvNxyYkXcaiidxwJGvQNwBigT_2; }
	inline LgyDESrvlXkrldhlJfbWEsuKfeVYU5BU5D_tD84BF7E1E9CB321A501CCD29EA0C82CAD309D17F** get_address_of_XzwvNxyYkXcaiidxwJGvQNwBigT_2() { return &___XzwvNxyYkXcaiidxwJGvQNwBigT_2; }
	inline void set_XzwvNxyYkXcaiidxwJGvQNwBigT_2(LgyDESrvlXkrldhlJfbWEsuKfeVYU5BU5D_tD84BF7E1E9CB321A501CCD29EA0C82CAD309D17F* value)
	{
		___XzwvNxyYkXcaiidxwJGvQNwBigT_2 = value;
		Il2CppCodeGenWriteBarrier((&___XzwvNxyYkXcaiidxwJGvQNwBigT_2), value);
	}

	inline static int32_t get_offset_of_YaDAnfimULzphKnSejtwEcfSlkEJ_3() { return static_cast<int32_t>(offsetof(ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416, ___YaDAnfimULzphKnSejtwEcfSlkEJ_3)); }
	inline LgyDESrvlXkrldhlJfbWEsuKfeVY_t861AA9B44D7A8C3C4D68C812E433DD1D8F09803D * get_YaDAnfimULzphKnSejtwEcfSlkEJ_3() const { return ___YaDAnfimULzphKnSejtwEcfSlkEJ_3; }
	inline LgyDESrvlXkrldhlJfbWEsuKfeVY_t861AA9B44D7A8C3C4D68C812E433DD1D8F09803D ** get_address_of_YaDAnfimULzphKnSejtwEcfSlkEJ_3() { return &___YaDAnfimULzphKnSejtwEcfSlkEJ_3; }
	inline void set_YaDAnfimULzphKnSejtwEcfSlkEJ_3(LgyDESrvlXkrldhlJfbWEsuKfeVY_t861AA9B44D7A8C3C4D68C812E433DD1D8F09803D * value)
	{
		___YaDAnfimULzphKnSejtwEcfSlkEJ_3 = value;
		Il2CppCodeGenWriteBarrier((&___YaDAnfimULzphKnSejtwEcfSlkEJ_3), value);
	}

	inline static int32_t get_offset_of_uiElHIMsJhOUGwxstGSvtQgdJHm_4() { return static_cast<int32_t>(offsetof(ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416, ___uiElHIMsJhOUGwxstGSvtQgdJHm_4)); }
	inline int32_t get_uiElHIMsJhOUGwxstGSvtQgdJHm_4() const { return ___uiElHIMsJhOUGwxstGSvtQgdJHm_4; }
	inline int32_t* get_address_of_uiElHIMsJhOUGwxstGSvtQgdJHm_4() { return &___uiElHIMsJhOUGwxstGSvtQgdJHm_4; }
	inline void set_uiElHIMsJhOUGwxstGSvtQgdJHm_4(int32_t value)
	{
		___uiElHIMsJhOUGwxstGSvtQgdJHm_4 = value;
	}

	inline static int32_t get_offset_of_mitExlcuToNzViGEORWHsMktptx_5() { return static_cast<int32_t>(offsetof(ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416, ___mitExlcuToNzViGEORWHsMktptx_5)); }
	inline int32_t get_mitExlcuToNzViGEORWHsMktptx_5() const { return ___mitExlcuToNzViGEORWHsMktptx_5; }
	inline int32_t* get_address_of_mitExlcuToNzViGEORWHsMktptx_5() { return &___mitExlcuToNzViGEORWHsMktptx_5; }
	inline void set_mitExlcuToNzViGEORWHsMktptx_5(int32_t value)
	{
		___mitExlcuToNzViGEORWHsMktptx_5 = value;
	}

	inline static int32_t get_offset_of_xnWxJKPxjErcevqVtlosHZHrsO_6() { return static_cast<int32_t>(offsetof(ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416, ___xnWxJKPxjErcevqVtlosHZHrsO_6)); }
	inline ReadOnlyCollection_1_t3253802893B57BA7CB1891841097377E60D0CC29 * get_xnWxJKPxjErcevqVtlosHZHrsO_6() const { return ___xnWxJKPxjErcevqVtlosHZHrsO_6; }
	inline ReadOnlyCollection_1_t3253802893B57BA7CB1891841097377E60D0CC29 ** get_address_of_xnWxJKPxjErcevqVtlosHZHrsO_6() { return &___xnWxJKPxjErcevqVtlosHZHrsO_6; }
	inline void set_xnWxJKPxjErcevqVtlosHZHrsO_6(ReadOnlyCollection_1_t3253802893B57BA7CB1891841097377E60D0CC29 * value)
	{
		___xnWxJKPxjErcevqVtlosHZHrsO_6 = value;
		Il2CppCodeGenWriteBarrier((&___xnWxJKPxjErcevqVtlosHZHrsO_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIIRQLEIIIIQEYMNDBEHMWQGUPZ_TB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416_H
#ifndef LGYDESRVLXKRLDHLJFBWESUKFEVY_T861AA9B44D7A8C3C4D68C812E433DD1D8F09803D_H
#define LGYDESRVLXKRLDHLJFBWESUKFEVY_T861AA9B44D7A8C3C4D68C812E433DD1D8F09803D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz_LgyDESrvlXkrldhlJfbWEsuKfeVY
struct  LgyDESrvlXkrldhlJfbWEsuKfeVY_t861AA9B44D7A8C3C4D68C812E433DD1D8F09803D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LGYDESRVLXKRLDHLJFBWESUKFEVY_T861AA9B44D7A8C3C4D68C812E433DD1D8F09803D_H
#ifndef EXTENSION_TB52F54389416488161F1C79DFA089F142E5AD3FD_H
#define EXTENSION_TB52F54389416488161F1C79DFA089F142E5AD3FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Extension
struct  Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD  : public RuntimeObject
{
public:
	// Rewired.Controller Rewired.Controller_Extension::gcFDOBoqpaBweHMwNlbneFOEEAm
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___gcFDOBoqpaBweHMwNlbneFOEEAm_0;
	// Rewired.Interfaces.IControllerExtensionSource Rewired.Controller_Extension::gkqUnrSMeLSOlffXjmHcoYDAlQT
	RuntimeObject* ___gkqUnrSMeLSOlffXjmHcoYDAlQT_1;
	// System.Int32 Rewired.Controller_Extension::_reInputId
	int32_t ____reInputId_2;

public:
	inline static int32_t get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_0() { return static_cast<int32_t>(offsetof(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD, ___gcFDOBoqpaBweHMwNlbneFOEEAm_0)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_gcFDOBoqpaBweHMwNlbneFOEEAm_0() const { return ___gcFDOBoqpaBweHMwNlbneFOEEAm_0; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_gcFDOBoqpaBweHMwNlbneFOEEAm_0() { return &___gcFDOBoqpaBweHMwNlbneFOEEAm_0; }
	inline void set_gcFDOBoqpaBweHMwNlbneFOEEAm_0(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___gcFDOBoqpaBweHMwNlbneFOEEAm_0 = value;
		Il2CppCodeGenWriteBarrier((&___gcFDOBoqpaBweHMwNlbneFOEEAm_0), value);
	}

	inline static int32_t get_offset_of_gkqUnrSMeLSOlffXjmHcoYDAlQT_1() { return static_cast<int32_t>(offsetof(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD, ___gkqUnrSMeLSOlffXjmHcoYDAlQT_1)); }
	inline RuntimeObject* get_gkqUnrSMeLSOlffXjmHcoYDAlQT_1() const { return ___gkqUnrSMeLSOlffXjmHcoYDAlQT_1; }
	inline RuntimeObject** get_address_of_gkqUnrSMeLSOlffXjmHcoYDAlQT_1() { return &___gkqUnrSMeLSOlffXjmHcoYDAlQT_1; }
	inline void set_gkqUnrSMeLSOlffXjmHcoYDAlQT_1(RuntimeObject* value)
	{
		___gkqUnrSMeLSOlffXjmHcoYDAlQT_1 = value;
		Il2CppCodeGenWriteBarrier((&___gkqUnrSMeLSOlffXjmHcoYDAlQT_1), value);
	}

	inline static int32_t get_offset_of__reInputId_2() { return static_cast<int32_t>(offsetof(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD, ____reInputId_2)); }
	inline int32_t get__reInputId_2() const { return ____reInputId_2; }
	inline int32_t* get_address_of__reInputId_2() { return &____reInputId_2; }
	inline void set__reInputId_2(int32_t value)
	{
		____reInputId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSION_TB52F54389416488161F1C79DFA089F142E5AD3FD_H
#ifndef TZDHWJEZWRNWXHPAFBBUORQRREH_TEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D_H
#define TZDHWJEZWRNWXHPAFBBUORQRREH_TEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerExtensions.DualShock4Extension_tZdHWjEZWrnwXHpafbBUORQRReH
struct  tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D  : public RuntimeObject
{
public:
	// Rewired.Drivers.Interfaces.IDriver_DualShock4 Rewired.ControllerExtensions.DualShock4Extension_tZdHWjEZWrnwXHpafbBUORQRReH::WKrdOrHChcdoEToFNLUKCQfYSQj
	RuntimeObject* ___WKrdOrHChcdoEToFNLUKCQfYSQj_0;
	// System.Boolean Rewired.ControllerExtensions.DualShock4Extension_tZdHWjEZWrnwXHpafbBUORQRReH::ZsEfMISqALipCaWIrZdQwCvMLkx
	bool ___ZsEfMISqALipCaWIrZdQwCvMLkx_1;
	// System.Int32 Rewired.ControllerExtensions.DualShock4Extension_tZdHWjEZWrnwXHpafbBUORQRReH::YJKKCodZAAdHLAznZhhKAnRrzmS
	int32_t ___YJKKCodZAAdHLAznZhhKAnRrzmS_2;

public:
	inline static int32_t get_offset_of_WKrdOrHChcdoEToFNLUKCQfYSQj_0() { return static_cast<int32_t>(offsetof(tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D, ___WKrdOrHChcdoEToFNLUKCQfYSQj_0)); }
	inline RuntimeObject* get_WKrdOrHChcdoEToFNLUKCQfYSQj_0() const { return ___WKrdOrHChcdoEToFNLUKCQfYSQj_0; }
	inline RuntimeObject** get_address_of_WKrdOrHChcdoEToFNLUKCQfYSQj_0() { return &___WKrdOrHChcdoEToFNLUKCQfYSQj_0; }
	inline void set_WKrdOrHChcdoEToFNLUKCQfYSQj_0(RuntimeObject* value)
	{
		___WKrdOrHChcdoEToFNLUKCQfYSQj_0 = value;
		Il2CppCodeGenWriteBarrier((&___WKrdOrHChcdoEToFNLUKCQfYSQj_0), value);
	}

	inline static int32_t get_offset_of_ZsEfMISqALipCaWIrZdQwCvMLkx_1() { return static_cast<int32_t>(offsetof(tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D, ___ZsEfMISqALipCaWIrZdQwCvMLkx_1)); }
	inline bool get_ZsEfMISqALipCaWIrZdQwCvMLkx_1() const { return ___ZsEfMISqALipCaWIrZdQwCvMLkx_1; }
	inline bool* get_address_of_ZsEfMISqALipCaWIrZdQwCvMLkx_1() { return &___ZsEfMISqALipCaWIrZdQwCvMLkx_1; }
	inline void set_ZsEfMISqALipCaWIrZdQwCvMLkx_1(bool value)
	{
		___ZsEfMISqALipCaWIrZdQwCvMLkx_1 = value;
	}

	inline static int32_t get_offset_of_YJKKCodZAAdHLAznZhhKAnRrzmS_2() { return static_cast<int32_t>(offsetof(tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D, ___YJKKCodZAAdHLAznZhhKAnRrzmS_2)); }
	inline int32_t get_YJKKCodZAAdHLAznZhhKAnRrzmS_2() const { return ___YJKKCodZAAdHLAznZhhKAnRrzmS_2; }
	inline int32_t* get_address_of_YJKKCodZAAdHLAznZhhKAnRrzmS_2() { return &___YJKKCodZAAdHLAznZhhKAnRrzmS_2; }
	inline void set_YJKKCodZAAdHLAznZhhKAnRrzmS_2(int32_t value)
	{
		___YJKKCodZAAdHLAznZhhKAnRrzmS_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TZDHWJEZWRNWXHPAFBBUORQRREH_TEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D_H
#ifndef FFVCQZUEADDPTKXNXKBTOZLLBGQX_TD4D732B3A84E0F9D7F1391C9276759C09B2B03E6_H
#define FFVCQZUEADDPTKXNXKBTOZLLBGQX_TD4D732B3A84E0F9D7F1391C9276759C09B2B03E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerExtensions.RailDriverExtension_ffvcqZUeADdPtkxnXkBtOZLlBgQx
struct  ffvcqZUeADdPtkxnXkBtOZLlBgQx_tD4D732B3A84E0F9D7F1391C9276759C09B2B03E6  : public RuntimeObject
{
public:
	// Rewired.Drivers.Interfaces.IDriver_RailDriver Rewired.ControllerExtensions.RailDriverExtension_ffvcqZUeADdPtkxnXkBtOZLlBgQx::WKrdOrHChcdoEToFNLUKCQfYSQj
	RuntimeObject* ___WKrdOrHChcdoEToFNLUKCQfYSQj_0;

public:
	inline static int32_t get_offset_of_WKrdOrHChcdoEToFNLUKCQfYSQj_0() { return static_cast<int32_t>(offsetof(ffvcqZUeADdPtkxnXkBtOZLlBgQx_tD4D732B3A84E0F9D7F1391C9276759C09B2B03E6, ___WKrdOrHChcdoEToFNLUKCQfYSQj_0)); }
	inline RuntimeObject* get_WKrdOrHChcdoEToFNLUKCQfYSQj_0() const { return ___WKrdOrHChcdoEToFNLUKCQfYSQj_0; }
	inline RuntimeObject** get_address_of_WKrdOrHChcdoEToFNLUKCQfYSQj_0() { return &___WKrdOrHChcdoEToFNLUKCQfYSQj_0; }
	inline void set_WKrdOrHChcdoEToFNLUKCQfYSQj_0(RuntimeObject* value)
	{
		___WKrdOrHChcdoEToFNLUKCQfYSQj_0 = value;
		Il2CppCodeGenWriteBarrier((&___WKrdOrHChcdoEToFNLUKCQfYSQj_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FFVCQZUEADDPTKXNXKBTOZLLBGQX_TD4D732B3A84E0F9D7F1391C9276759C09B2B03E6_H
#ifndef TBEETYAKXZMXTAZXITHGYEBQLVUI_TCBAE66D0EF5D308109B13047A13AE8C86F80185E_H
#define TBEETYAKXZMXTAZXITHGYEBQLVUI_TCBAE66D0EF5D308109B13047A13AE8C86F80185E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerExtensions.SteamControllerExtension_tBEetyAkXzmxTaZXitHgYebQLVui
struct  tBEetyAkXzmxTaZXitHgYebQLVui_tCBAE66D0EF5D308109B13047A13AE8C86F80185E  : public RuntimeObject
{
public:
	// Rewired.Interfaces.ISteamControllerInternal Rewired.ControllerExtensions.SteamControllerExtension_tBEetyAkXzmxTaZXitHgYebQLVui::saVMPymulxTFDfqZAdAfUKUVFQp
	RuntimeObject* ___saVMPymulxTFDfqZAdAfUKUVFQp_0;

public:
	inline static int32_t get_offset_of_saVMPymulxTFDfqZAdAfUKUVFQp_0() { return static_cast<int32_t>(offsetof(tBEetyAkXzmxTaZXitHgYebQLVui_tCBAE66D0EF5D308109B13047A13AE8C86F80185E, ___saVMPymulxTFDfqZAdAfUKUVFQp_0)); }
	inline RuntimeObject* get_saVMPymulxTFDfqZAdAfUKUVFQp_0() const { return ___saVMPymulxTFDfqZAdAfUKUVFQp_0; }
	inline RuntimeObject** get_address_of_saVMPymulxTFDfqZAdAfUKUVFQp_0() { return &___saVMPymulxTFDfqZAdAfUKUVFQp_0; }
	inline void set_saVMPymulxTFDfqZAdAfUKUVFQp_0(RuntimeObject* value)
	{
		___saVMPymulxTFDfqZAdAfUKUVFQp_0 = value;
		Il2CppCodeGenWriteBarrier((&___saVMPymulxTFDfqZAdAfUKUVFQp_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TBEETYAKXZMXTAZXITHGYEBQLVUI_TCBAE66D0EF5D308109B13047A13AE8C86F80185E_H
#ifndef GFQJFZGGDIRITIIQCXUBCCJUCSZL_T528C22A61812E797D0E6B022915EBE59FF18742D_H
#define GFQJFZGGDIRITIIQCXUBCCJUCSZL_T528C22A61812E797D0E6B022915EBE59FF18742D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_GfqjFzggDiRITiiqcxubCCJucSZL
struct  GfqjFzggDiRITiiqcxubCCJucSZL_t528C22A61812E797D0E6B022915EBE59FF18742D  : public RuntimeObject
{
public:
	// Rewired.Controller_Element Rewired.ControllerTemplate_GfqjFzggDiRITiiqcxubCCJucSZL::YCBcgPOLoWsaeULLzpTLswYuYbQ
	Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 * ___YCBcgPOLoWsaeULLzpTLswYuYbQ_0;
	// Rewired.IControllerElementTarget Rewired.ControllerTemplate_GfqjFzggDiRITiiqcxubCCJucSZL::YsciKTkdsyeKqevEdLRABIojdVHx
	RuntimeObject* ___YsciKTkdsyeKqevEdLRABIojdVHx_1;

public:
	inline static int32_t get_offset_of_YCBcgPOLoWsaeULLzpTLswYuYbQ_0() { return static_cast<int32_t>(offsetof(GfqjFzggDiRITiiqcxubCCJucSZL_t528C22A61812E797D0E6B022915EBE59FF18742D, ___YCBcgPOLoWsaeULLzpTLswYuYbQ_0)); }
	inline Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 * get_YCBcgPOLoWsaeULLzpTLswYuYbQ_0() const { return ___YCBcgPOLoWsaeULLzpTLswYuYbQ_0; }
	inline Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 ** get_address_of_YCBcgPOLoWsaeULLzpTLswYuYbQ_0() { return &___YCBcgPOLoWsaeULLzpTLswYuYbQ_0; }
	inline void set_YCBcgPOLoWsaeULLzpTLswYuYbQ_0(Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 * value)
	{
		___YCBcgPOLoWsaeULLzpTLswYuYbQ_0 = value;
		Il2CppCodeGenWriteBarrier((&___YCBcgPOLoWsaeULLzpTLswYuYbQ_0), value);
	}

	inline static int32_t get_offset_of_YsciKTkdsyeKqevEdLRABIojdVHx_1() { return static_cast<int32_t>(offsetof(GfqjFzggDiRITiiqcxubCCJucSZL_t528C22A61812E797D0E6B022915EBE59FF18742D, ___YsciKTkdsyeKqevEdLRABIojdVHx_1)); }
	inline RuntimeObject* get_YsciKTkdsyeKqevEdLRABIojdVHx_1() const { return ___YsciKTkdsyeKqevEdLRABIojdVHx_1; }
	inline RuntimeObject** get_address_of_YsciKTkdsyeKqevEdLRABIojdVHx_1() { return &___YsciKTkdsyeKqevEdLRABIojdVHx_1; }
	inline void set_YsciKTkdsyeKqevEdLRABIojdVHx_1(RuntimeObject* value)
	{
		___YsciKTkdsyeKqevEdLRABIojdVHx_1 = value;
		Il2CppCodeGenWriteBarrier((&___YsciKTkdsyeKqevEdLRABIojdVHx_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GFQJFZGGDIRITIIQCXUBCCJUCSZL_T528C22A61812E797D0E6B022915EBE59FF18742D_H
#ifndef OVCTKTPAVRYBBQCFMUSNIGZUZYN_TFBE7311BB2977AE8D51BF394298877340699D6EA_H
#define OVCTKTPAVRYBBQCFMUSNIGZUZYN_TFBE7311BB2977AE8D51BF394298877340699D6EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_oVctktPaVrYbbqcFmUSnIGzUzYN
struct  oVctktPaVrYbbqcFmUSnIGzUzYN_tFBE7311BB2977AE8D51BF394298877340699D6EA  : public RuntimeObject
{
public:
	// Rewired.Controller Rewired.ControllerTemplate_oVctktPaVrYbbqcFmUSnIGzUzYN::QaeAlqSBKVmDRaDTcYzPlckcanC
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___QaeAlqSBKVmDRaDTcYzPlckcanC_0;
	// Rewired.Data.Mapping.IHardwareControllerTemplateMap_Internal Rewired.ControllerTemplate_oVctktPaVrYbbqcFmUSnIGzUzYN::RsUadSyOfwghqaAMaOLCTYUuHka
	RuntimeObject* ___RsUadSyOfwghqaAMaOLCTYUuHka_1;

public:
	inline static int32_t get_offset_of_QaeAlqSBKVmDRaDTcYzPlckcanC_0() { return static_cast<int32_t>(offsetof(oVctktPaVrYbbqcFmUSnIGzUzYN_tFBE7311BB2977AE8D51BF394298877340699D6EA, ___QaeAlqSBKVmDRaDTcYzPlckcanC_0)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_QaeAlqSBKVmDRaDTcYzPlckcanC_0() const { return ___QaeAlqSBKVmDRaDTcYzPlckcanC_0; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_QaeAlqSBKVmDRaDTcYzPlckcanC_0() { return &___QaeAlqSBKVmDRaDTcYzPlckcanC_0; }
	inline void set_QaeAlqSBKVmDRaDTcYzPlckcanC_0(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___QaeAlqSBKVmDRaDTcYzPlckcanC_0 = value;
		Il2CppCodeGenWriteBarrier((&___QaeAlqSBKVmDRaDTcYzPlckcanC_0), value);
	}

	inline static int32_t get_offset_of_RsUadSyOfwghqaAMaOLCTYUuHka_1() { return static_cast<int32_t>(offsetof(oVctktPaVrYbbqcFmUSnIGzUzYN_tFBE7311BB2977AE8D51BF394298877340699D6EA, ___RsUadSyOfwghqaAMaOLCTYUuHka_1)); }
	inline RuntimeObject* get_RsUadSyOfwghqaAMaOLCTYUuHka_1() const { return ___RsUadSyOfwghqaAMaOLCTYUuHka_1; }
	inline RuntimeObject** get_address_of_RsUadSyOfwghqaAMaOLCTYUuHka_1() { return &___RsUadSyOfwghqaAMaOLCTYUuHka_1; }
	inline void set_RsUadSyOfwghqaAMaOLCTYUuHka_1(RuntimeObject* value)
	{
		___RsUadSyOfwghqaAMaOLCTYUuHka_1 = value;
		Il2CppCodeGenWriteBarrier((&___RsUadSyOfwghqaAMaOLCTYUuHka_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVCTKTPAVRYBBQCFMUSNIGZUZYN_TFBE7311BB2977AE8D51BF394298877340699D6EA_H
#ifndef CONTROLLERTEMPLATESPECIALELEMENTMAPPING_T35C3BB93612616CCEAEF857E70C5824933B85390_H
#define CONTROLLERTEMPLATESPECIALELEMENTMAPPING_T35C3BB93612616CCEAEF857E70C5824933B85390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ControllerTemplateSpecialElementMapping
struct  ControllerTemplateSpecialElementMapping_t35C3BB93612616CCEAEF857E70C5824933B85390  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATESPECIALELEMENTMAPPING_T35C3BB93612616CCEAEF857E70C5824933B85390_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef KOCBMWIKFCZEWIYUFHSOZJIEHTIJ_T323E34E602CC0412547FC6147CDCF72A6613EC5F_H
#define KOCBMWIKFCZEWIYUFHSOZJIEHTIJ_T323E34E602CC0412547FC6147CDCF72A6613EC5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj
struct  KOCBmwikfCzewiyUFHSozJiEHTIj_t323E34E602CC0412547FC6147CDCF72A6613EC5F  : public ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KOCBMWIKFCZEWIYUFHSOZJIEHTIJ_T323E34E602CC0412547FC6147CDCF72A6613EC5F_H
#ifndef ZDPUSUZVQBSEOTFCVPSNSYHXQLB_T271C64225BFFAD98AA24BCB4C8CE5AB36EA26664_H
#define ZDPUSUZVQBSEOTFCVPSNSYHXQLB_T271C64225BFFAD98AA24BCB4C8CE5AB36EA26664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb
struct  zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664  : public LgyDESrvlXkrldhlJfbWEsuKfeVY_t861AA9B44D7A8C3C4D68C812E433DD1D8F09803D
{
public:
	// System.Single Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb::ujWJxEJcSVHSdnQYrtHnXDWQpnZ
	float ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_1;
	// System.Single Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb::onYGDjMoebGBXHApYQtbIBUNPzwH
	float ___onYGDjMoebGBXHApYQtbIBUNPzwH_2;
	// System.Single Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb::aYeFKQaDJsBdYbdxdccnalEBhTdw
	float ___aYeFKQaDJsBdYbdxdccnalEBhTdw_3;
	// System.Single Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb::PudYZCFEKJBeUWywNbBYFyWYBJt
	float ___PudYZCFEKJBeUWywNbBYFyWYBJt_4;
	// System.Single Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb::ZhJUsMLFILMLypTfQsPsELJqNPn
	float ___ZhJUsMLFILMLypTfQsPsELJqNPn_5;
	// System.Single Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb::qcqAgGtAMdifMYRQFFkQlthQdow
	float ___qcqAgGtAMdifMYRQFFkQlthQdow_6;
	// System.Single Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb::VzEHWjEtvrwkkGwcOYbRCXOdcQD
	float ___VzEHWjEtvrwkkGwcOYbRCXOdcQD_7;
	// System.Single Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb::dIqFoxehjyutbFetSezzpojUnOd
	float ___dIqFoxehjyutbFetSezzpojUnOd_8;
	// System.Single Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb::yFYxFqUsPfdYYvTkIijWBOiMXsY
	float ___yFYxFqUsPfdYYvTkIijWBOiMXsY_9;
	// System.Single Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb::qXFGuRbHaDptgrXRrKFonlRQxQa
	float ___qXFGuRbHaDptgrXRrKFonlRQxQa_10;
	// System.Single Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb::RbFcQJoIgvfPWSkMrFJeGwvJPMCF
	float ___RbFcQJoIgvfPWSkMrFJeGwvJPMCF_11;
	// System.Single Rewired.Controller_Axis_KOCBmwikfCzewiyUFHSozJiEHTIj_zdpuSuzVqBSeotfCVpsnSYHxqlb::HVDpkXFtDumDetiFRnhwHQlruSe
	float ___HVDpkXFtDumDetiFRnhwHQlruSe_12;

public:
	inline static int32_t get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_1() { return static_cast<int32_t>(offsetof(zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664, ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_1)); }
	inline float get_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_1() const { return ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_1; }
	inline float* get_address_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_1() { return &___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_1; }
	inline void set_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_1(float value)
	{
		___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_1 = value;
	}

	inline static int32_t get_offset_of_onYGDjMoebGBXHApYQtbIBUNPzwH_2() { return static_cast<int32_t>(offsetof(zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664, ___onYGDjMoebGBXHApYQtbIBUNPzwH_2)); }
	inline float get_onYGDjMoebGBXHApYQtbIBUNPzwH_2() const { return ___onYGDjMoebGBXHApYQtbIBUNPzwH_2; }
	inline float* get_address_of_onYGDjMoebGBXHApYQtbIBUNPzwH_2() { return &___onYGDjMoebGBXHApYQtbIBUNPzwH_2; }
	inline void set_onYGDjMoebGBXHApYQtbIBUNPzwH_2(float value)
	{
		___onYGDjMoebGBXHApYQtbIBUNPzwH_2 = value;
	}

	inline static int32_t get_offset_of_aYeFKQaDJsBdYbdxdccnalEBhTdw_3() { return static_cast<int32_t>(offsetof(zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664, ___aYeFKQaDJsBdYbdxdccnalEBhTdw_3)); }
	inline float get_aYeFKQaDJsBdYbdxdccnalEBhTdw_3() const { return ___aYeFKQaDJsBdYbdxdccnalEBhTdw_3; }
	inline float* get_address_of_aYeFKQaDJsBdYbdxdccnalEBhTdw_3() { return &___aYeFKQaDJsBdYbdxdccnalEBhTdw_3; }
	inline void set_aYeFKQaDJsBdYbdxdccnalEBhTdw_3(float value)
	{
		___aYeFKQaDJsBdYbdxdccnalEBhTdw_3 = value;
	}

	inline static int32_t get_offset_of_PudYZCFEKJBeUWywNbBYFyWYBJt_4() { return static_cast<int32_t>(offsetof(zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664, ___PudYZCFEKJBeUWywNbBYFyWYBJt_4)); }
	inline float get_PudYZCFEKJBeUWywNbBYFyWYBJt_4() const { return ___PudYZCFEKJBeUWywNbBYFyWYBJt_4; }
	inline float* get_address_of_PudYZCFEKJBeUWywNbBYFyWYBJt_4() { return &___PudYZCFEKJBeUWywNbBYFyWYBJt_4; }
	inline void set_PudYZCFEKJBeUWywNbBYFyWYBJt_4(float value)
	{
		___PudYZCFEKJBeUWywNbBYFyWYBJt_4 = value;
	}

	inline static int32_t get_offset_of_ZhJUsMLFILMLypTfQsPsELJqNPn_5() { return static_cast<int32_t>(offsetof(zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664, ___ZhJUsMLFILMLypTfQsPsELJqNPn_5)); }
	inline float get_ZhJUsMLFILMLypTfQsPsELJqNPn_5() const { return ___ZhJUsMLFILMLypTfQsPsELJqNPn_5; }
	inline float* get_address_of_ZhJUsMLFILMLypTfQsPsELJqNPn_5() { return &___ZhJUsMLFILMLypTfQsPsELJqNPn_5; }
	inline void set_ZhJUsMLFILMLypTfQsPsELJqNPn_5(float value)
	{
		___ZhJUsMLFILMLypTfQsPsELJqNPn_5 = value;
	}

	inline static int32_t get_offset_of_qcqAgGtAMdifMYRQFFkQlthQdow_6() { return static_cast<int32_t>(offsetof(zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664, ___qcqAgGtAMdifMYRQFFkQlthQdow_6)); }
	inline float get_qcqAgGtAMdifMYRQFFkQlthQdow_6() const { return ___qcqAgGtAMdifMYRQFFkQlthQdow_6; }
	inline float* get_address_of_qcqAgGtAMdifMYRQFFkQlthQdow_6() { return &___qcqAgGtAMdifMYRQFFkQlthQdow_6; }
	inline void set_qcqAgGtAMdifMYRQFFkQlthQdow_6(float value)
	{
		___qcqAgGtAMdifMYRQFFkQlthQdow_6 = value;
	}

	inline static int32_t get_offset_of_VzEHWjEtvrwkkGwcOYbRCXOdcQD_7() { return static_cast<int32_t>(offsetof(zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664, ___VzEHWjEtvrwkkGwcOYbRCXOdcQD_7)); }
	inline float get_VzEHWjEtvrwkkGwcOYbRCXOdcQD_7() const { return ___VzEHWjEtvrwkkGwcOYbRCXOdcQD_7; }
	inline float* get_address_of_VzEHWjEtvrwkkGwcOYbRCXOdcQD_7() { return &___VzEHWjEtvrwkkGwcOYbRCXOdcQD_7; }
	inline void set_VzEHWjEtvrwkkGwcOYbRCXOdcQD_7(float value)
	{
		___VzEHWjEtvrwkkGwcOYbRCXOdcQD_7 = value;
	}

	inline static int32_t get_offset_of_dIqFoxehjyutbFetSezzpojUnOd_8() { return static_cast<int32_t>(offsetof(zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664, ___dIqFoxehjyutbFetSezzpojUnOd_8)); }
	inline float get_dIqFoxehjyutbFetSezzpojUnOd_8() const { return ___dIqFoxehjyutbFetSezzpojUnOd_8; }
	inline float* get_address_of_dIqFoxehjyutbFetSezzpojUnOd_8() { return &___dIqFoxehjyutbFetSezzpojUnOd_8; }
	inline void set_dIqFoxehjyutbFetSezzpojUnOd_8(float value)
	{
		___dIqFoxehjyutbFetSezzpojUnOd_8 = value;
	}

	inline static int32_t get_offset_of_yFYxFqUsPfdYYvTkIijWBOiMXsY_9() { return static_cast<int32_t>(offsetof(zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664, ___yFYxFqUsPfdYYvTkIijWBOiMXsY_9)); }
	inline float get_yFYxFqUsPfdYYvTkIijWBOiMXsY_9() const { return ___yFYxFqUsPfdYYvTkIijWBOiMXsY_9; }
	inline float* get_address_of_yFYxFqUsPfdYYvTkIijWBOiMXsY_9() { return &___yFYxFqUsPfdYYvTkIijWBOiMXsY_9; }
	inline void set_yFYxFqUsPfdYYvTkIijWBOiMXsY_9(float value)
	{
		___yFYxFqUsPfdYYvTkIijWBOiMXsY_9 = value;
	}

	inline static int32_t get_offset_of_qXFGuRbHaDptgrXRrKFonlRQxQa_10() { return static_cast<int32_t>(offsetof(zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664, ___qXFGuRbHaDptgrXRrKFonlRQxQa_10)); }
	inline float get_qXFGuRbHaDptgrXRrKFonlRQxQa_10() const { return ___qXFGuRbHaDptgrXRrKFonlRQxQa_10; }
	inline float* get_address_of_qXFGuRbHaDptgrXRrKFonlRQxQa_10() { return &___qXFGuRbHaDptgrXRrKFonlRQxQa_10; }
	inline void set_qXFGuRbHaDptgrXRrKFonlRQxQa_10(float value)
	{
		___qXFGuRbHaDptgrXRrKFonlRQxQa_10 = value;
	}

	inline static int32_t get_offset_of_RbFcQJoIgvfPWSkMrFJeGwvJPMCF_11() { return static_cast<int32_t>(offsetof(zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664, ___RbFcQJoIgvfPWSkMrFJeGwvJPMCF_11)); }
	inline float get_RbFcQJoIgvfPWSkMrFJeGwvJPMCF_11() const { return ___RbFcQJoIgvfPWSkMrFJeGwvJPMCF_11; }
	inline float* get_address_of_RbFcQJoIgvfPWSkMrFJeGwvJPMCF_11() { return &___RbFcQJoIgvfPWSkMrFJeGwvJPMCF_11; }
	inline void set_RbFcQJoIgvfPWSkMrFJeGwvJPMCF_11(float value)
	{
		___RbFcQJoIgvfPWSkMrFJeGwvJPMCF_11 = value;
	}

	inline static int32_t get_offset_of_HVDpkXFtDumDetiFRnhwHQlruSe_12() { return static_cast<int32_t>(offsetof(zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664, ___HVDpkXFtDumDetiFRnhwHQlruSe_12)); }
	inline float get_HVDpkXFtDumDetiFRnhwHQlruSe_12() const { return ___HVDpkXFtDumDetiFRnhwHQlruSe_12; }
	inline float* get_address_of_HVDpkXFtDumDetiFRnhwHQlruSe_12() { return &___HVDpkXFtDumDetiFRnhwHQlruSe_12; }
	inline void set_HVDpkXFtDumDetiFRnhwHQlruSe_12(float value)
	{
		___HVDpkXFtDumDetiFRnhwHQlruSe_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZDPUSUZVQBSEOTFCVPSNSYHXQLB_T271C64225BFFAD98AA24BCB4C8CE5AB36EA26664_H
#ifndef TSRYXASYNKKXLGZOFANSLEFXENS_T86EC68CC7A26C51CF8A25E0868E2A3A4E31A78D3_H
#define TSRYXASYNKKXLGZOFANSLEFXENS_T86EC68CC7A26C51CF8A25E0868E2A3A4E31A78D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Button_TSRYxaSynKkxLGzOfANSlEfXenS
struct  TSRYxaSynKkxLGzOfANSlEfXenS_t86EC68CC7A26C51CF8A25E0868E2A3A4E31A78D3  : public ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TSRYXASYNKKXLGZOFANSLEFXENS_T86EC68CC7A26C51CF8A25E0868E2A3A4E31A78D3_H
#ifndef PPLXXTNSVASNDVJXSIAZKIKOJTT_T6CD8DE45D90987D75EE26FEFB110FC910A94C2A7_H
#define PPLXXTNSVASNDVJXSIAZKIKOJTT_T6CD8DE45D90987D75EE26FEFB110FC910A94C2A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Button_TSRYxaSynKkxLGzOfANSlEfXenS_pplxxtNsVaSNdvjXsiaZkikOjTt
struct  pplxxtNsVaSNdvjXsiaZkikOjTt_t6CD8DE45D90987D75EE26FEFB110FC910A94C2A7  : public LgyDESrvlXkrldhlJfbWEsuKfeVY_t861AA9B44D7A8C3C4D68C812E433DD1D8F09803D
{
public:
	// System.Boolean Rewired.Controller_Button_TSRYxaSynKkxLGzOfANSlEfXenS_pplxxtNsVaSNdvjXsiaZkikOjTt::ujWJxEJcSVHSdnQYrtHnXDWQpnZ
	bool ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0;
	// System.Boolean Rewired.Controller_Button_TSRYxaSynKkxLGzOfANSlEfXenS_pplxxtNsVaSNdvjXsiaZkikOjTt::onYGDjMoebGBXHApYQtbIBUNPzwH
	bool ___onYGDjMoebGBXHApYQtbIBUNPzwH_1;
	// Rewired.ButtonStateRecorder Rewired.Controller_Button_TSRYxaSynKkxLGzOfANSlEfXenS_pplxxtNsVaSNdvjXsiaZkikOjTt::cvUezygDxrEziLDuCyJRvkQuBlTD
	ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148 * ___cvUezygDxrEziLDuCyJRvkQuBlTD_2;
	// EhbFMnIgrveIkvxMjDinIUjSQTaf Rewired.Controller_Button_TSRYxaSynKkxLGzOfANSlEfXenS_pplxxtNsVaSNdvjXsiaZkikOjTt::bQWchkeBhmcXPRmhgvECfWjozcXw
	EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5 * ___bQWchkeBhmcXPRmhgvECfWjozcXw_3;

public:
	inline static int32_t get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0() { return static_cast<int32_t>(offsetof(pplxxtNsVaSNdvjXsiaZkikOjTt_t6CD8DE45D90987D75EE26FEFB110FC910A94C2A7, ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0)); }
	inline bool get_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0() const { return ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0; }
	inline bool* get_address_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0() { return &___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0; }
	inline void set_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0(bool value)
	{
		___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0 = value;
	}

	inline static int32_t get_offset_of_onYGDjMoebGBXHApYQtbIBUNPzwH_1() { return static_cast<int32_t>(offsetof(pplxxtNsVaSNdvjXsiaZkikOjTt_t6CD8DE45D90987D75EE26FEFB110FC910A94C2A7, ___onYGDjMoebGBXHApYQtbIBUNPzwH_1)); }
	inline bool get_onYGDjMoebGBXHApYQtbIBUNPzwH_1() const { return ___onYGDjMoebGBXHApYQtbIBUNPzwH_1; }
	inline bool* get_address_of_onYGDjMoebGBXHApYQtbIBUNPzwH_1() { return &___onYGDjMoebGBXHApYQtbIBUNPzwH_1; }
	inline void set_onYGDjMoebGBXHApYQtbIBUNPzwH_1(bool value)
	{
		___onYGDjMoebGBXHApYQtbIBUNPzwH_1 = value;
	}

	inline static int32_t get_offset_of_cvUezygDxrEziLDuCyJRvkQuBlTD_2() { return static_cast<int32_t>(offsetof(pplxxtNsVaSNdvjXsiaZkikOjTt_t6CD8DE45D90987D75EE26FEFB110FC910A94C2A7, ___cvUezygDxrEziLDuCyJRvkQuBlTD_2)); }
	inline ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148 * get_cvUezygDxrEziLDuCyJRvkQuBlTD_2() const { return ___cvUezygDxrEziLDuCyJRvkQuBlTD_2; }
	inline ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148 ** get_address_of_cvUezygDxrEziLDuCyJRvkQuBlTD_2() { return &___cvUezygDxrEziLDuCyJRvkQuBlTD_2; }
	inline void set_cvUezygDxrEziLDuCyJRvkQuBlTD_2(ButtonStateRecorder_t0FCB8D46E64106B5362E2AE5074A70BEBEC3B148 * value)
	{
		___cvUezygDxrEziLDuCyJRvkQuBlTD_2 = value;
		Il2CppCodeGenWriteBarrier((&___cvUezygDxrEziLDuCyJRvkQuBlTD_2), value);
	}

	inline static int32_t get_offset_of_bQWchkeBhmcXPRmhgvECfWjozcXw_3() { return static_cast<int32_t>(offsetof(pplxxtNsVaSNdvjXsiaZkikOjTt_t6CD8DE45D90987D75EE26FEFB110FC910A94C2A7, ___bQWchkeBhmcXPRmhgvECfWjozcXw_3)); }
	inline EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5 * get_bQWchkeBhmcXPRmhgvECfWjozcXw_3() const { return ___bQWchkeBhmcXPRmhgvECfWjozcXw_3; }
	inline EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5 ** get_address_of_bQWchkeBhmcXPRmhgvECfWjozcXw_3() { return &___bQWchkeBhmcXPRmhgvECfWjozcXw_3; }
	inline void set_bQWchkeBhmcXPRmhgvECfWjozcXw_3(EhbFMnIgrveIkvxMjDinIUjSQTaf_tF09D190303034EC83B44DAFBC62D0DCFC736A8B5 * value)
	{
		___bQWchkeBhmcXPRmhgvECfWjozcXw_3 = value;
		Il2CppCodeGenWriteBarrier((&___bQWchkeBhmcXPRmhgvECfWjozcXw_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PPLXXTNSVASNDVJXSIAZKIKOJTT_T6CD8DE45D90987D75EE26FEFB110FC910A94C2A7_H
#ifndef DUALSHOCK4EXTENSION_TDF6532F36F998F4D62A88536ED8244AB6C3BCD7E_H
#define DUALSHOCK4EXTENSION_TDF6532F36F998F4D62A88536ED8244AB6C3BCD7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerExtensions.DualShock4Extension
struct  DualShock4Extension_tDF6532F36F998F4D62A88536ED8244AB6C3BCD7E  : public Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD
{
public:
	// Rewired.ControllerExtensions.DualShock4Extension_tZdHWjEZWrnwXHpafbBUORQRReH Rewired.ControllerExtensions.DualShock4Extension::gkqUnrSMeLSOlffXjmHcoYDAlQT
	tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D * ___gkqUnrSMeLSOlffXjmHcoYDAlQT_3;
	// System.Boolean Rewired.ControllerExtensions.DualShock4Extension::EdmoIszvSQiLETihGiwjrkdiQJU
	bool ___EdmoIszvSQiLETihGiwjrkdiQJU_4;
	// Rewired.Utils.Classes.Utility.TimerAbs[] Rewired.ControllerExtensions.DualShock4Extension::CdVWyCtwdYqRItEtEoYUUDYOAqxD
	TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC* ___CdVWyCtwdYqRItEtEoYUUDYOAqxD_5;

public:
	inline static int32_t get_offset_of_gkqUnrSMeLSOlffXjmHcoYDAlQT_3() { return static_cast<int32_t>(offsetof(DualShock4Extension_tDF6532F36F998F4D62A88536ED8244AB6C3BCD7E, ___gkqUnrSMeLSOlffXjmHcoYDAlQT_3)); }
	inline tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D * get_gkqUnrSMeLSOlffXjmHcoYDAlQT_3() const { return ___gkqUnrSMeLSOlffXjmHcoYDAlQT_3; }
	inline tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D ** get_address_of_gkqUnrSMeLSOlffXjmHcoYDAlQT_3() { return &___gkqUnrSMeLSOlffXjmHcoYDAlQT_3; }
	inline void set_gkqUnrSMeLSOlffXjmHcoYDAlQT_3(tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D * value)
	{
		___gkqUnrSMeLSOlffXjmHcoYDAlQT_3 = value;
		Il2CppCodeGenWriteBarrier((&___gkqUnrSMeLSOlffXjmHcoYDAlQT_3), value);
	}

	inline static int32_t get_offset_of_EdmoIszvSQiLETihGiwjrkdiQJU_4() { return static_cast<int32_t>(offsetof(DualShock4Extension_tDF6532F36F998F4D62A88536ED8244AB6C3BCD7E, ___EdmoIszvSQiLETihGiwjrkdiQJU_4)); }
	inline bool get_EdmoIszvSQiLETihGiwjrkdiQJU_4() const { return ___EdmoIszvSQiLETihGiwjrkdiQJU_4; }
	inline bool* get_address_of_EdmoIszvSQiLETihGiwjrkdiQJU_4() { return &___EdmoIszvSQiLETihGiwjrkdiQJU_4; }
	inline void set_EdmoIszvSQiLETihGiwjrkdiQJU_4(bool value)
	{
		___EdmoIszvSQiLETihGiwjrkdiQJU_4 = value;
	}

	inline static int32_t get_offset_of_CdVWyCtwdYqRItEtEoYUUDYOAqxD_5() { return static_cast<int32_t>(offsetof(DualShock4Extension_tDF6532F36F998F4D62A88536ED8244AB6C3BCD7E, ___CdVWyCtwdYqRItEtEoYUUDYOAqxD_5)); }
	inline TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC* get_CdVWyCtwdYqRItEtEoYUUDYOAqxD_5() const { return ___CdVWyCtwdYqRItEtEoYUUDYOAqxD_5; }
	inline TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC** get_address_of_CdVWyCtwdYqRItEtEoYUUDYOAqxD_5() { return &___CdVWyCtwdYqRItEtEoYUUDYOAqxD_5; }
	inline void set_CdVWyCtwdYqRItEtEoYUUDYOAqxD_5(TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC* value)
	{
		___CdVWyCtwdYqRItEtEoYUUDYOAqxD_5 = value;
		Il2CppCodeGenWriteBarrier((&___CdVWyCtwdYqRItEtEoYUUDYOAqxD_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUALSHOCK4EXTENSION_TDF6532F36F998F4D62A88536ED8244AB6C3BCD7E_H
#ifndef RAILDRIVEREXTENSION_TDAC9FCBEF64FC452942C77A592436F630B90D3EE_H
#define RAILDRIVEREXTENSION_TDAC9FCBEF64FC452942C77A592436F630B90D3EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerExtensions.RailDriverExtension
struct  RailDriverExtension_tDAC9FCBEF64FC452942C77A592436F630B90D3EE  : public Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD
{
public:
	// Rewired.ControllerExtensions.RailDriverExtension_ffvcqZUeADdPtkxnXkBtOZLlBgQx Rewired.ControllerExtensions.RailDriverExtension::zSCjKsgSKyXaLUKczdjHusCWAAC
	ffvcqZUeADdPtkxnXkBtOZLlBgQx_tD4D732B3A84E0F9D7F1391C9276759C09B2B03E6 * ___zSCjKsgSKyXaLUKczdjHusCWAAC_3;

public:
	inline static int32_t get_offset_of_zSCjKsgSKyXaLUKczdjHusCWAAC_3() { return static_cast<int32_t>(offsetof(RailDriverExtension_tDAC9FCBEF64FC452942C77A592436F630B90D3EE, ___zSCjKsgSKyXaLUKczdjHusCWAAC_3)); }
	inline ffvcqZUeADdPtkxnXkBtOZLlBgQx_tD4D732B3A84E0F9D7F1391C9276759C09B2B03E6 * get_zSCjKsgSKyXaLUKczdjHusCWAAC_3() const { return ___zSCjKsgSKyXaLUKczdjHusCWAAC_3; }
	inline ffvcqZUeADdPtkxnXkBtOZLlBgQx_tD4D732B3A84E0F9D7F1391C9276759C09B2B03E6 ** get_address_of_zSCjKsgSKyXaLUKczdjHusCWAAC_3() { return &___zSCjKsgSKyXaLUKczdjHusCWAAC_3; }
	inline void set_zSCjKsgSKyXaLUKczdjHusCWAAC_3(ffvcqZUeADdPtkxnXkBtOZLlBgQx_tD4D732B3A84E0F9D7F1391C9276759C09B2B03E6 * value)
	{
		___zSCjKsgSKyXaLUKczdjHusCWAAC_3 = value;
		Il2CppCodeGenWriteBarrier((&___zSCjKsgSKyXaLUKczdjHusCWAAC_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAILDRIVEREXTENSION_TDAC9FCBEF64FC452942C77A592436F630B90D3EE_H
#ifndef STEAMCONTROLLEREXTENSION_T0C1E15C33BE26F8C7AD76DCC8130466B3A108207_H
#define STEAMCONTROLLEREXTENSION_T0C1E15C33BE26F8C7AD76DCC8130466B3A108207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerExtensions.SteamControllerExtension
struct  SteamControllerExtension_t0C1E15C33BE26F8C7AD76DCC8130466B3A108207  : public Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD
{
public:
	// Rewired.ControllerExtensions.SteamControllerExtension_tBEetyAkXzmxTaZXitHgYebQLVui Rewired.ControllerExtensions.SteamControllerExtension::zSCjKsgSKyXaLUKczdjHusCWAAC
	tBEetyAkXzmxTaZXitHgYebQLVui_tCBAE66D0EF5D308109B13047A13AE8C86F80185E * ___zSCjKsgSKyXaLUKczdjHusCWAAC_3;

public:
	inline static int32_t get_offset_of_zSCjKsgSKyXaLUKczdjHusCWAAC_3() { return static_cast<int32_t>(offsetof(SteamControllerExtension_t0C1E15C33BE26F8C7AD76DCC8130466B3A108207, ___zSCjKsgSKyXaLUKczdjHusCWAAC_3)); }
	inline tBEetyAkXzmxTaZXitHgYebQLVui_tCBAE66D0EF5D308109B13047A13AE8C86F80185E * get_zSCjKsgSKyXaLUKczdjHusCWAAC_3() const { return ___zSCjKsgSKyXaLUKczdjHusCWAAC_3; }
	inline tBEetyAkXzmxTaZXitHgYebQLVui_tCBAE66D0EF5D308109B13047A13AE8C86F80185E ** get_address_of_zSCjKsgSKyXaLUKczdjHusCWAAC_3() { return &___zSCjKsgSKyXaLUKczdjHusCWAAC_3; }
	inline void set_zSCjKsgSKyXaLUKczdjHusCWAAC_3(tBEetyAkXzmxTaZXitHgYebQLVui_tCBAE66D0EF5D308109B13047A13AE8C86F80185E * value)
	{
		___zSCjKsgSKyXaLUKczdjHusCWAAC_3 = value;
		Il2CppCodeGenWriteBarrier((&___zSCjKsgSKyXaLUKczdjHusCWAAC_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEAMCONTROLLEREXTENSION_T0C1E15C33BE26F8C7AD76DCC8130466B3A108207_H
#ifndef CONTROLLERTEMPLATEDPADMAPPING_T03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C_H
#define CONTROLLERTEMPLATEDPADMAPPING_T03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ControllerTemplateDPadMapping
struct  ControllerTemplateDPadMapping_t03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C  : public ControllerTemplateSpecialElementMapping_t35C3BB93612616CCEAEF857E70C5824933B85390
{
public:
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateDPadMapping::eid_up
	int32_t ___eid_up_0;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateDPadMapping::eid_right
	int32_t ___eid_right_1;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateDPadMapping::eid_down
	int32_t ___eid_down_2;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateDPadMapping::eid_left
	int32_t ___eid_left_3;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateDPadMapping::eid_press
	int32_t ___eid_press_4;

public:
	inline static int32_t get_offset_of_eid_up_0() { return static_cast<int32_t>(offsetof(ControllerTemplateDPadMapping_t03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C, ___eid_up_0)); }
	inline int32_t get_eid_up_0() const { return ___eid_up_0; }
	inline int32_t* get_address_of_eid_up_0() { return &___eid_up_0; }
	inline void set_eid_up_0(int32_t value)
	{
		___eid_up_0 = value;
	}

	inline static int32_t get_offset_of_eid_right_1() { return static_cast<int32_t>(offsetof(ControllerTemplateDPadMapping_t03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C, ___eid_right_1)); }
	inline int32_t get_eid_right_1() const { return ___eid_right_1; }
	inline int32_t* get_address_of_eid_right_1() { return &___eid_right_1; }
	inline void set_eid_right_1(int32_t value)
	{
		___eid_right_1 = value;
	}

	inline static int32_t get_offset_of_eid_down_2() { return static_cast<int32_t>(offsetof(ControllerTemplateDPadMapping_t03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C, ___eid_down_2)); }
	inline int32_t get_eid_down_2() const { return ___eid_down_2; }
	inline int32_t* get_address_of_eid_down_2() { return &___eid_down_2; }
	inline void set_eid_down_2(int32_t value)
	{
		___eid_down_2 = value;
	}

	inline static int32_t get_offset_of_eid_left_3() { return static_cast<int32_t>(offsetof(ControllerTemplateDPadMapping_t03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C, ___eid_left_3)); }
	inline int32_t get_eid_left_3() const { return ___eid_left_3; }
	inline int32_t* get_address_of_eid_left_3() { return &___eid_left_3; }
	inline void set_eid_left_3(int32_t value)
	{
		___eid_left_3 = value;
	}

	inline static int32_t get_offset_of_eid_press_4() { return static_cast<int32_t>(offsetof(ControllerTemplateDPadMapping_t03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C, ___eid_press_4)); }
	inline int32_t get_eid_press_4() const { return ___eid_press_4; }
	inline int32_t* get_address_of_eid_press_4() { return &___eid_press_4; }
	inline void set_eid_press_4(int32_t value)
	{
		___eid_press_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEDPADMAPPING_T03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C_H
#ifndef CONTROLLERTEMPLATEHATMAPPING_T5B2290EA4E925630DE2E6D525E777E0C719CBB08_H
#define CONTROLLERTEMPLATEHATMAPPING_T5B2290EA4E925630DE2E6D525E777E0C719CBB08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ControllerTemplateHatMapping
struct  ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08  : public ControllerTemplateSpecialElementMapping_t35C3BB93612616CCEAEF857E70C5824933B85390
{
public:
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateHatMapping::eid_up
	int32_t ___eid_up_0;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateHatMapping::eid_upRight
	int32_t ___eid_upRight_1;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateHatMapping::eid_right
	int32_t ___eid_right_2;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateHatMapping::eid_downRight
	int32_t ___eid_downRight_3;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateHatMapping::eid_down
	int32_t ___eid_down_4;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateHatMapping::eid_downLeft
	int32_t ___eid_downLeft_5;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateHatMapping::eid_left
	int32_t ___eid_left_6;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateHatMapping::eid_upLeft
	int32_t ___eid_upLeft_7;

public:
	inline static int32_t get_offset_of_eid_up_0() { return static_cast<int32_t>(offsetof(ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08, ___eid_up_0)); }
	inline int32_t get_eid_up_0() const { return ___eid_up_0; }
	inline int32_t* get_address_of_eid_up_0() { return &___eid_up_0; }
	inline void set_eid_up_0(int32_t value)
	{
		___eid_up_0 = value;
	}

	inline static int32_t get_offset_of_eid_upRight_1() { return static_cast<int32_t>(offsetof(ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08, ___eid_upRight_1)); }
	inline int32_t get_eid_upRight_1() const { return ___eid_upRight_1; }
	inline int32_t* get_address_of_eid_upRight_1() { return &___eid_upRight_1; }
	inline void set_eid_upRight_1(int32_t value)
	{
		___eid_upRight_1 = value;
	}

	inline static int32_t get_offset_of_eid_right_2() { return static_cast<int32_t>(offsetof(ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08, ___eid_right_2)); }
	inline int32_t get_eid_right_2() const { return ___eid_right_2; }
	inline int32_t* get_address_of_eid_right_2() { return &___eid_right_2; }
	inline void set_eid_right_2(int32_t value)
	{
		___eid_right_2 = value;
	}

	inline static int32_t get_offset_of_eid_downRight_3() { return static_cast<int32_t>(offsetof(ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08, ___eid_downRight_3)); }
	inline int32_t get_eid_downRight_3() const { return ___eid_downRight_3; }
	inline int32_t* get_address_of_eid_downRight_3() { return &___eid_downRight_3; }
	inline void set_eid_downRight_3(int32_t value)
	{
		___eid_downRight_3 = value;
	}

	inline static int32_t get_offset_of_eid_down_4() { return static_cast<int32_t>(offsetof(ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08, ___eid_down_4)); }
	inline int32_t get_eid_down_4() const { return ___eid_down_4; }
	inline int32_t* get_address_of_eid_down_4() { return &___eid_down_4; }
	inline void set_eid_down_4(int32_t value)
	{
		___eid_down_4 = value;
	}

	inline static int32_t get_offset_of_eid_downLeft_5() { return static_cast<int32_t>(offsetof(ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08, ___eid_downLeft_5)); }
	inline int32_t get_eid_downLeft_5() const { return ___eid_downLeft_5; }
	inline int32_t* get_address_of_eid_downLeft_5() { return &___eid_downLeft_5; }
	inline void set_eid_downLeft_5(int32_t value)
	{
		___eid_downLeft_5 = value;
	}

	inline static int32_t get_offset_of_eid_left_6() { return static_cast<int32_t>(offsetof(ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08, ___eid_left_6)); }
	inline int32_t get_eid_left_6() const { return ___eid_left_6; }
	inline int32_t* get_address_of_eid_left_6() { return &___eid_left_6; }
	inline void set_eid_left_6(int32_t value)
	{
		___eid_left_6 = value;
	}

	inline static int32_t get_offset_of_eid_upLeft_7() { return static_cast<int32_t>(offsetof(ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08, ___eid_upLeft_7)); }
	inline int32_t get_eid_upLeft_7() const { return ___eid_upLeft_7; }
	inline int32_t* get_address_of_eid_upLeft_7() { return &___eid_upLeft_7; }
	inline void set_eid_upLeft_7(int32_t value)
	{
		___eid_upLeft_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEHATMAPPING_T5B2290EA4E925630DE2E6D525E777E0C719CBB08_H
#ifndef CONTROLLERTEMPLATESTICK6DMAPPING_T608ECF2EF4917548DB725B125B69663F41060A15_H
#define CONTROLLERTEMPLATESTICK6DMAPPING_T608ECF2EF4917548DB725B125B69663F41060A15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ControllerTemplateStick6DMapping
struct  ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15  : public ControllerTemplateSpecialElementMapping_t35C3BB93612616CCEAEF857E70C5824933B85390
{
public:
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateStick6DMapping::eid_positionX
	int32_t ___eid_positionX_0;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateStick6DMapping::eid_positionY
	int32_t ___eid_positionY_1;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateStick6DMapping::eid_positionZ
	int32_t ___eid_positionZ_2;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateStick6DMapping::eid_rotationX
	int32_t ___eid_rotationX_3;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateStick6DMapping::eid_rotationY
	int32_t ___eid_rotationY_4;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateStick6DMapping::eid_rotationZ
	int32_t ___eid_rotationZ_5;

public:
	inline static int32_t get_offset_of_eid_positionX_0() { return static_cast<int32_t>(offsetof(ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15, ___eid_positionX_0)); }
	inline int32_t get_eid_positionX_0() const { return ___eid_positionX_0; }
	inline int32_t* get_address_of_eid_positionX_0() { return &___eid_positionX_0; }
	inline void set_eid_positionX_0(int32_t value)
	{
		___eid_positionX_0 = value;
	}

	inline static int32_t get_offset_of_eid_positionY_1() { return static_cast<int32_t>(offsetof(ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15, ___eid_positionY_1)); }
	inline int32_t get_eid_positionY_1() const { return ___eid_positionY_1; }
	inline int32_t* get_address_of_eid_positionY_1() { return &___eid_positionY_1; }
	inline void set_eid_positionY_1(int32_t value)
	{
		___eid_positionY_1 = value;
	}

	inline static int32_t get_offset_of_eid_positionZ_2() { return static_cast<int32_t>(offsetof(ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15, ___eid_positionZ_2)); }
	inline int32_t get_eid_positionZ_2() const { return ___eid_positionZ_2; }
	inline int32_t* get_address_of_eid_positionZ_2() { return &___eid_positionZ_2; }
	inline void set_eid_positionZ_2(int32_t value)
	{
		___eid_positionZ_2 = value;
	}

	inline static int32_t get_offset_of_eid_rotationX_3() { return static_cast<int32_t>(offsetof(ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15, ___eid_rotationX_3)); }
	inline int32_t get_eid_rotationX_3() const { return ___eid_rotationX_3; }
	inline int32_t* get_address_of_eid_rotationX_3() { return &___eid_rotationX_3; }
	inline void set_eid_rotationX_3(int32_t value)
	{
		___eid_rotationX_3 = value;
	}

	inline static int32_t get_offset_of_eid_rotationY_4() { return static_cast<int32_t>(offsetof(ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15, ___eid_rotationY_4)); }
	inline int32_t get_eid_rotationY_4() const { return ___eid_rotationY_4; }
	inline int32_t* get_address_of_eid_rotationY_4() { return &___eid_rotationY_4; }
	inline void set_eid_rotationY_4(int32_t value)
	{
		___eid_rotationY_4 = value;
	}

	inline static int32_t get_offset_of_eid_rotationZ_5() { return static_cast<int32_t>(offsetof(ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15, ___eid_rotationZ_5)); }
	inline int32_t get_eid_rotationZ_5() const { return ___eid_rotationZ_5; }
	inline int32_t* get_address_of_eid_rotationZ_5() { return &___eid_rotationZ_5; }
	inline void set_eid_rotationZ_5(int32_t value)
	{
		___eid_rotationZ_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATESTICK6DMAPPING_T608ECF2EF4917548DB725B125B69663F41060A15_H
#ifndef CONTROLLERTEMPLATESTICKMAPPING_T1847BAF56312244E2E51C54DFF9028F671528CEF_H
#define CONTROLLERTEMPLATESTICKMAPPING_T1847BAF56312244E2E51C54DFF9028F671528CEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ControllerTemplateStickMapping
struct  ControllerTemplateStickMapping_t1847BAF56312244E2E51C54DFF9028F671528CEF  : public ControllerTemplateSpecialElementMapping_t35C3BB93612616CCEAEF857E70C5824933B85390
{
public:
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateStickMapping::eid_axisX
	int32_t ___eid_axisX_0;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateStickMapping::eid_axisY
	int32_t ___eid_axisY_1;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateStickMapping::eid_axisZ
	int32_t ___eid_axisZ_2;

public:
	inline static int32_t get_offset_of_eid_axisX_0() { return static_cast<int32_t>(offsetof(ControllerTemplateStickMapping_t1847BAF56312244E2E51C54DFF9028F671528CEF, ___eid_axisX_0)); }
	inline int32_t get_eid_axisX_0() const { return ___eid_axisX_0; }
	inline int32_t* get_address_of_eid_axisX_0() { return &___eid_axisX_0; }
	inline void set_eid_axisX_0(int32_t value)
	{
		___eid_axisX_0 = value;
	}

	inline static int32_t get_offset_of_eid_axisY_1() { return static_cast<int32_t>(offsetof(ControllerTemplateStickMapping_t1847BAF56312244E2E51C54DFF9028F671528CEF, ___eid_axisY_1)); }
	inline int32_t get_eid_axisY_1() const { return ___eid_axisY_1; }
	inline int32_t* get_address_of_eid_axisY_1() { return &___eid_axisY_1; }
	inline void set_eid_axisY_1(int32_t value)
	{
		___eid_axisY_1 = value;
	}

	inline static int32_t get_offset_of_eid_axisZ_2() { return static_cast<int32_t>(offsetof(ControllerTemplateStickMapping_t1847BAF56312244E2E51C54DFF9028F671528CEF, ___eid_axisZ_2)); }
	inline int32_t get_eid_axisZ_2() const { return ___eid_axisZ_2; }
	inline int32_t* get_address_of_eid_axisZ_2() { return &___eid_axisZ_2; }
	inline void set_eid_axisZ_2(int32_t value)
	{
		___eid_axisZ_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATESTICKMAPPING_T1847BAF56312244E2E51C54DFF9028F671528CEF_H
#ifndef CONTROLLERTEMPLATETHROTTLEMAPPING_T37A060630236F3393B3A986301C804A848F82866_H
#define CONTROLLERTEMPLATETHROTTLEMAPPING_T37A060630236F3393B3A986301C804A848F82866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ControllerTemplateThrottleMapping
struct  ControllerTemplateThrottleMapping_t37A060630236F3393B3A986301C804A848F82866  : public ControllerTemplateSpecialElementMapping_t35C3BB93612616CCEAEF857E70C5824933B85390
{
public:
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateThrottleMapping::eid_axis
	int32_t ___eid_axis_0;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateThrottleMapping::eid_minDetent
	int32_t ___eid_minDetent_1;

public:
	inline static int32_t get_offset_of_eid_axis_0() { return static_cast<int32_t>(offsetof(ControllerTemplateThrottleMapping_t37A060630236F3393B3A986301C804A848F82866, ___eid_axis_0)); }
	inline int32_t get_eid_axis_0() const { return ___eid_axis_0; }
	inline int32_t* get_address_of_eid_axis_0() { return &___eid_axis_0; }
	inline void set_eid_axis_0(int32_t value)
	{
		___eid_axis_0 = value;
	}

	inline static int32_t get_offset_of_eid_minDetent_1() { return static_cast<int32_t>(offsetof(ControllerTemplateThrottleMapping_t37A060630236F3393B3A986301C804A848F82866, ___eid_minDetent_1)); }
	inline int32_t get_eid_minDetent_1() const { return ___eid_minDetent_1; }
	inline int32_t* get_address_of_eid_minDetent_1() { return &___eid_minDetent_1; }
	inline void set_eid_minDetent_1(int32_t value)
	{
		___eid_minDetent_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATETHROTTLEMAPPING_T37A060630236F3393B3A986301C804A848F82866_H
#ifndef CONTROLLERTEMPLATETHUMBSTICKMAPPING_T278A01E802FCFDD0E3F3DA0071E06AE934D2D63A_H
#define CONTROLLERTEMPLATETHUMBSTICKMAPPING_T278A01E802FCFDD0E3F3DA0071E06AE934D2D63A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ControllerTemplateThumbStickMapping
struct  ControllerTemplateThumbStickMapping_t278A01E802FCFDD0E3F3DA0071E06AE934D2D63A  : public ControllerTemplateSpecialElementMapping_t35C3BB93612616CCEAEF857E70C5824933B85390
{
public:
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateThumbStickMapping::eid_axisX
	int32_t ___eid_axisX_0;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateThumbStickMapping::eid_axisY
	int32_t ___eid_axisY_1;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateThumbStickMapping::eid_button
	int32_t ___eid_button_2;

public:
	inline static int32_t get_offset_of_eid_axisX_0() { return static_cast<int32_t>(offsetof(ControllerTemplateThumbStickMapping_t278A01E802FCFDD0E3F3DA0071E06AE934D2D63A, ___eid_axisX_0)); }
	inline int32_t get_eid_axisX_0() const { return ___eid_axisX_0; }
	inline int32_t* get_address_of_eid_axisX_0() { return &___eid_axisX_0; }
	inline void set_eid_axisX_0(int32_t value)
	{
		___eid_axisX_0 = value;
	}

	inline static int32_t get_offset_of_eid_axisY_1() { return static_cast<int32_t>(offsetof(ControllerTemplateThumbStickMapping_t278A01E802FCFDD0E3F3DA0071E06AE934D2D63A, ___eid_axisY_1)); }
	inline int32_t get_eid_axisY_1() const { return ___eid_axisY_1; }
	inline int32_t* get_address_of_eid_axisY_1() { return &___eid_axisY_1; }
	inline void set_eid_axisY_1(int32_t value)
	{
		___eid_axisY_1 = value;
	}

	inline static int32_t get_offset_of_eid_button_2() { return static_cast<int32_t>(offsetof(ControllerTemplateThumbStickMapping_t278A01E802FCFDD0E3F3DA0071E06AE934D2D63A, ___eid_button_2)); }
	inline int32_t get_eid_button_2() const { return ___eid_button_2; }
	inline int32_t* get_address_of_eid_button_2() { return &___eid_button_2; }
	inline void set_eid_button_2(int32_t value)
	{
		___eid_button_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATETHUMBSTICKMAPPING_T278A01E802FCFDD0E3F3DA0071E06AE934D2D63A_H
#ifndef CONTROLLERTEMPLATEYOKEMAPPING_TECBF73125DA9F22DDCB5B6FB11764D2ED0D558FF_H
#define CONTROLLERTEMPLATEYOKEMAPPING_TECBF73125DA9F22DDCB5B6FB11764D2ED0D558FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.ControllerTemplateYokeMapping
struct  ControllerTemplateYokeMapping_tECBF73125DA9F22DDCB5B6FB11764D2ED0D558FF  : public ControllerTemplateSpecialElementMapping_t35C3BB93612616CCEAEF857E70C5824933B85390
{
public:
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateYokeMapping::eid_axisX
	int32_t ___eid_axisX_0;
	// System.Int32 Rewired.Data.Mapping.ControllerTemplateYokeMapping::eid_axisZ
	int32_t ___eid_axisZ_1;

public:
	inline static int32_t get_offset_of_eid_axisX_0() { return static_cast<int32_t>(offsetof(ControllerTemplateYokeMapping_tECBF73125DA9F22DDCB5B6FB11764D2ED0D558FF, ___eid_axisX_0)); }
	inline int32_t get_eid_axisX_0() const { return ___eid_axisX_0; }
	inline int32_t* get_address_of_eid_axisX_0() { return &___eid_axisX_0; }
	inline void set_eid_axisX_0(int32_t value)
	{
		___eid_axisX_0 = value;
	}

	inline static int32_t get_offset_of_eid_axisZ_1() { return static_cast<int32_t>(offsetof(ControllerTemplateYokeMapping_tECBF73125DA9F22DDCB5B6FB11764D2ED0D558FF, ___eid_axisZ_1)); }
	inline int32_t get_eid_axisZ_1() const { return ___eid_axisZ_1; }
	inline int32_t* get_address_of_eid_axisZ_1() { return &___eid_axisZ_1; }
	inline void set_eid_axisZ_1(int32_t value)
	{
		___eid_axisZ_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEYOKEMAPPING_TECBF73125DA9F22DDCB5B6FB11764D2ED0D558FF_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#define AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisRange
struct  AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652 
{
public:
	// System.Int32 Rewired.AxisRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifndef AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#define AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisSensitivityType
struct  AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A 
{
public:
	// System.Int32 Rewired.AxisSensitivityType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#ifndef COMPOUNDCONTROLLERELEMENTTYPE_T38C73D05390CC2625D0760EB81BACFDEC36EB6B2_H
#define COMPOUNDCONTROLLERELEMENTTYPE_T38C73D05390CC2625D0760EB81BACFDEC36EB6B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.CompoundControllerElementType
struct  CompoundControllerElementType_t38C73D05390CC2625D0760EB81BACFDEC36EB6B2 
{
public:
	// System.Int32 Rewired.CompoundControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompoundControllerElementType_t38C73D05390CC2625D0760EB81BACFDEC36EB6B2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDCONTROLLERELEMENTTYPE_T38C73D05390CC2625D0760EB81BACFDEC36EB6B2_H
#ifndef LOGLEVEL_TE523370914B61878CEB702CDD7AC1D9D56B8749D_H
#define LOGLEVEL_TE523370914B61878CEB702CDD7AC1D9D56B8749D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Config.LogLevel
struct  LogLevel_tE523370914B61878CEB702CDD7AC1D9D56B8749D 
{
public:
	// System.Int32 Rewired.Config.LogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevel_tE523370914B61878CEB702CDD7AC1D9D56B8749D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_TE523370914B61878CEB702CDD7AC1D9D56B8749D_H
#ifndef LOGLEVELFLAGS_TD2231B5E28D4ADA489FAE89122340FE9D6755981_H
#define LOGLEVELFLAGS_TD2231B5E28D4ADA489FAE89122340FE9D6755981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Config.LogLevelFlags
struct  LogLevelFlags_tD2231B5E28D4ADA489FAE89122340FE9D6755981 
{
public:
	// System.Int32 Rewired.Config.LogLevelFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevelFlags_tD2231B5E28D4ADA489FAE89122340FE9D6755981, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVELFLAGS_TD2231B5E28D4ADA489FAE89122340FE9D6755981_H
#ifndef THROTTLECALIBRATIONMODE_T9454AA7B39A1EE99DD4B92B4E3A15AE78CD52EF9_H
#define THROTTLECALIBRATIONMODE_T9454AA7B39A1EE99DD4B92B4E3A15AE78CD52EF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Config.ThrottleCalibrationMode
struct  ThrottleCalibrationMode_t9454AA7B39A1EE99DD4B92B4E3A15AE78CD52EF9 
{
public:
	// System.Int32 Rewired.Config.ThrottleCalibrationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ThrottleCalibrationMode_t9454AA7B39A1EE99DD4B92B4E3A15AE78CD52EF9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THROTTLECALIBRATIONMODE_T9454AA7B39A1EE99DD4B92B4E3A15AE78CD52EF9_H
#ifndef LFFDFXIVWCOEAGRPRDJGALFFNJCH_T48AEF236799222E249090C8A32B297FD292F8174_H
#define LFFDFXIVWCOEAGRPRDJGALFFNJCH_T48AEF236799222E249090C8A32B297FD292F8174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Button_TSRYxaSynKkxLGzOfANSlEfXenS_lffDFXIvwcOeAGrPrdjgaLfFnjCH
struct  lffDFXIvwcOeAGrPrdjgaLfFnjCH_t48AEF236799222E249090C8A32B297FD292F8174  : public pplxxtNsVaSNdvjXsiaZkikOjTt_t6CD8DE45D90987D75EE26FEFB110FC910A94C2A7
{
public:
	// System.Single Rewired.Controller_Button_TSRYxaSynKkxLGzOfANSlEfXenS_lffDFXIvwcOeAGrPrdjgaLfFnjCH::HughelYTlpIBvxzJFnUqfWyvfca
	float ___HughelYTlpIBvxzJFnUqfWyvfca_4;
	// System.Single Rewired.Controller_Button_TSRYxaSynKkxLGzOfANSlEfXenS_lffDFXIvwcOeAGrPrdjgaLfFnjCH::axNcSentbHTBzAxIYzGTkwfaHSE
	float ___axNcSentbHTBzAxIYzGTkwfaHSE_5;

public:
	inline static int32_t get_offset_of_HughelYTlpIBvxzJFnUqfWyvfca_4() { return static_cast<int32_t>(offsetof(lffDFXIvwcOeAGrPrdjgaLfFnjCH_t48AEF236799222E249090C8A32B297FD292F8174, ___HughelYTlpIBvxzJFnUqfWyvfca_4)); }
	inline float get_HughelYTlpIBvxzJFnUqfWyvfca_4() const { return ___HughelYTlpIBvxzJFnUqfWyvfca_4; }
	inline float* get_address_of_HughelYTlpIBvxzJFnUqfWyvfca_4() { return &___HughelYTlpIBvxzJFnUqfWyvfca_4; }
	inline void set_HughelYTlpIBvxzJFnUqfWyvfca_4(float value)
	{
		___HughelYTlpIBvxzJFnUqfWyvfca_4 = value;
	}

	inline static int32_t get_offset_of_axNcSentbHTBzAxIYzGTkwfaHSE_5() { return static_cast<int32_t>(offsetof(lffDFXIvwcOeAGrPrdjgaLfFnjCH_t48AEF236799222E249090C8A32B297FD292F8174, ___axNcSentbHTBzAxIYzGTkwfaHSE_5)); }
	inline float get_axNcSentbHTBzAxIYzGTkwfaHSE_5() const { return ___axNcSentbHTBzAxIYzGTkwfaHSE_5; }
	inline float* get_address_of_axNcSentbHTBzAxIYzGTkwfaHSE_5() { return &___axNcSentbHTBzAxIYzGTkwfaHSE_5; }
	inline void set_axNcSentbHTBzAxIYzGTkwfaHSE_5(float value)
	{
		___axNcSentbHTBzAxIYzGTkwfaHSE_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LFFDFXIVWCOEAGRPRDJGALFFNJCH_T48AEF236799222E249090C8A32B297FD292F8174_H
#ifndef CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#define CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerElementType
struct  ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA 
{
public:
	// System.Int32 Rewired.ControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifndef DUALSHOCK4MOTORTYPE_TC3A25346F0A0B4B33211B4705E345D34D4D39D6D_H
#define DUALSHOCK4MOTORTYPE_TC3A25346F0A0B4B33211B4705E345D34D4D39D6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerExtensions.DualShock4MotorType
struct  DualShock4MotorType_tC3A25346F0A0B4B33211B4705E345D34D4D39D6D 
{
public:
	// System.Int32 Rewired.ControllerExtensions.DualShock4MotorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DualShock4MotorType_tC3A25346F0A0B4B33211B4705E345D34D4D39D6D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUALSHOCK4MOTORTYPE_TC3A25346F0A0B4B33211B4705E345D34D4D39D6D_H
#ifndef STEAMCONTROLLERACTIONORIGIN_TEB8F62A1D2A8DA6EF474542244C27C157E9126D1_H
#define STEAMCONTROLLERACTIONORIGIN_TEB8F62A1D2A8DA6EF474542244C27C157E9126D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerExtensions.SteamControllerActionOrigin
struct  SteamControllerActionOrigin_tEB8F62A1D2A8DA6EF474542244C27C157E9126D1 
{
public:
	// System.Int32 Rewired.ControllerExtensions.SteamControllerActionOrigin::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SteamControllerActionOrigin_tEB8F62A1D2A8DA6EF474542244C27C157E9126D1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEAMCONTROLLERACTIONORIGIN_TEB8F62A1D2A8DA6EF474542244C27C157E9126D1_H
#ifndef STEAMCONTROLLERPADTYPE_T1251D69AA1C544283A77D2E12966B387B3F535B9_H
#define STEAMCONTROLLERPADTYPE_T1251D69AA1C544283A77D2E12966B387B3F535B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerExtensions.SteamControllerPadType
struct  SteamControllerPadType_t1251D69AA1C544283A77D2E12966B387B3F535B9 
{
public:
	// System.UInt32 Rewired.ControllerExtensions.SteamControllerPadType::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SteamControllerPadType_t1251D69AA1C544283A77D2E12966B387B3F535B9, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEAMCONTROLLERPADTYPE_T1251D69AA1C544283A77D2E12966B387B3F535B9_H
#ifndef CONTROLLERTEMPLATE_TB3D9364CC27D096A6AAE8B9960440B43ECAC8B98_H
#define CONTROLLERTEMPLATE_TB3D9364CC27D096A6AAE8B9960440B43ECAC8B98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate
struct  ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98  : public RuntimeObject
{
public:
	// System.String Rewired.ControllerTemplate::xzFIhZhMHNeQYGDUhfMfvvqdZQds
	String_t* ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_0;
	// System.Guid Rewired.ControllerTemplate::IVAjJNOdTXXjgnowouzEZvvrKDh
	Guid_t  ___IVAjJNOdTXXjgnowouzEZvvrKDh_1;
	// Rewired.Controller Rewired.ControllerTemplate::gcFDOBoqpaBweHMwNlbneFOEEAm
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___gcFDOBoqpaBweHMwNlbneFOEEAm_2;
	// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,Rewired.IControllerTemplateElement> Rewired.ControllerTemplate::NOXxtGrpSqMcIFZTsrJcnLlMjrR
	ADictionary_2_t68A8B0123876EA082FBDA6132BFFCD5DB743965D * ___NOXxtGrpSqMcIFZTsrJcnLlMjrR_3;
	// Rewired.Utils.Classes.Data.ADictionary`2<System.String,Rewired.IControllerTemplateElement> Rewired.ControllerTemplate::jQsjEfMifTDFgHVpZohgCksDsnei
	ADictionary_2_tD6BAFD11BFA92734BD04AC9CA5B5B7B493FAB1E7 * ___jQsjEfMifTDFgHVpZohgCksDsnei_4;
	// Rewired.IControllerTemplateElement[] Rewired.ControllerTemplate::tqrCjCDiVROjgZQMTxdrZcCDZcA
	IControllerTemplateElementU5BU5D_t1660B9BD48816BC80A9C52B74BFCF80922F1C060* ___tqrCjCDiVROjgZQMTxdrZcCDZcA_5;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.IControllerTemplateElement> Rewired.ControllerTemplate::HeiaDLCywuXsOrgGQhDKAJyFIubw
	ReadOnlyCollection_1_tBD7D8D2E9C5F8B4B0C4FF78D2202E23AC1F7F65B * ___HeiaDLCywuXsOrgGQhDKAJyFIubw_6;
	// System.Int32 Rewired.ControllerTemplate::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_7;

public:
	inline static int32_t get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_0() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_0)); }
	inline String_t* get_xzFIhZhMHNeQYGDUhfMfvvqdZQds_0() const { return ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_0; }
	inline String_t** get_address_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_0() { return &___xzFIhZhMHNeQYGDUhfMfvvqdZQds_0; }
	inline void set_xzFIhZhMHNeQYGDUhfMfvvqdZQds_0(String_t* value)
	{
		___xzFIhZhMHNeQYGDUhfMfvvqdZQds_0 = value;
		Il2CppCodeGenWriteBarrier((&___xzFIhZhMHNeQYGDUhfMfvvqdZQds_0), value);
	}

	inline static int32_t get_offset_of_IVAjJNOdTXXjgnowouzEZvvrKDh_1() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___IVAjJNOdTXXjgnowouzEZvvrKDh_1)); }
	inline Guid_t  get_IVAjJNOdTXXjgnowouzEZvvrKDh_1() const { return ___IVAjJNOdTXXjgnowouzEZvvrKDh_1; }
	inline Guid_t * get_address_of_IVAjJNOdTXXjgnowouzEZvvrKDh_1() { return &___IVAjJNOdTXXjgnowouzEZvvrKDh_1; }
	inline void set_IVAjJNOdTXXjgnowouzEZvvrKDh_1(Guid_t  value)
	{
		___IVAjJNOdTXXjgnowouzEZvvrKDh_1 = value;
	}

	inline static int32_t get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_2() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___gcFDOBoqpaBweHMwNlbneFOEEAm_2)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_gcFDOBoqpaBweHMwNlbneFOEEAm_2() const { return ___gcFDOBoqpaBweHMwNlbneFOEEAm_2; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_gcFDOBoqpaBweHMwNlbneFOEEAm_2() { return &___gcFDOBoqpaBweHMwNlbneFOEEAm_2; }
	inline void set_gcFDOBoqpaBweHMwNlbneFOEEAm_2(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___gcFDOBoqpaBweHMwNlbneFOEEAm_2 = value;
		Il2CppCodeGenWriteBarrier((&___gcFDOBoqpaBweHMwNlbneFOEEAm_2), value);
	}

	inline static int32_t get_offset_of_NOXxtGrpSqMcIFZTsrJcnLlMjrR_3() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___NOXxtGrpSqMcIFZTsrJcnLlMjrR_3)); }
	inline ADictionary_2_t68A8B0123876EA082FBDA6132BFFCD5DB743965D * get_NOXxtGrpSqMcIFZTsrJcnLlMjrR_3() const { return ___NOXxtGrpSqMcIFZTsrJcnLlMjrR_3; }
	inline ADictionary_2_t68A8B0123876EA082FBDA6132BFFCD5DB743965D ** get_address_of_NOXxtGrpSqMcIFZTsrJcnLlMjrR_3() { return &___NOXxtGrpSqMcIFZTsrJcnLlMjrR_3; }
	inline void set_NOXxtGrpSqMcIFZTsrJcnLlMjrR_3(ADictionary_2_t68A8B0123876EA082FBDA6132BFFCD5DB743965D * value)
	{
		___NOXxtGrpSqMcIFZTsrJcnLlMjrR_3 = value;
		Il2CppCodeGenWriteBarrier((&___NOXxtGrpSqMcIFZTsrJcnLlMjrR_3), value);
	}

	inline static int32_t get_offset_of_jQsjEfMifTDFgHVpZohgCksDsnei_4() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___jQsjEfMifTDFgHVpZohgCksDsnei_4)); }
	inline ADictionary_2_tD6BAFD11BFA92734BD04AC9CA5B5B7B493FAB1E7 * get_jQsjEfMifTDFgHVpZohgCksDsnei_4() const { return ___jQsjEfMifTDFgHVpZohgCksDsnei_4; }
	inline ADictionary_2_tD6BAFD11BFA92734BD04AC9CA5B5B7B493FAB1E7 ** get_address_of_jQsjEfMifTDFgHVpZohgCksDsnei_4() { return &___jQsjEfMifTDFgHVpZohgCksDsnei_4; }
	inline void set_jQsjEfMifTDFgHVpZohgCksDsnei_4(ADictionary_2_tD6BAFD11BFA92734BD04AC9CA5B5B7B493FAB1E7 * value)
	{
		___jQsjEfMifTDFgHVpZohgCksDsnei_4 = value;
		Il2CppCodeGenWriteBarrier((&___jQsjEfMifTDFgHVpZohgCksDsnei_4), value);
	}

	inline static int32_t get_offset_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_5() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___tqrCjCDiVROjgZQMTxdrZcCDZcA_5)); }
	inline IControllerTemplateElementU5BU5D_t1660B9BD48816BC80A9C52B74BFCF80922F1C060* get_tqrCjCDiVROjgZQMTxdrZcCDZcA_5() const { return ___tqrCjCDiVROjgZQMTxdrZcCDZcA_5; }
	inline IControllerTemplateElementU5BU5D_t1660B9BD48816BC80A9C52B74BFCF80922F1C060** get_address_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_5() { return &___tqrCjCDiVROjgZQMTxdrZcCDZcA_5; }
	inline void set_tqrCjCDiVROjgZQMTxdrZcCDZcA_5(IControllerTemplateElementU5BU5D_t1660B9BD48816BC80A9C52B74BFCF80922F1C060* value)
	{
		___tqrCjCDiVROjgZQMTxdrZcCDZcA_5 = value;
		Il2CppCodeGenWriteBarrier((&___tqrCjCDiVROjgZQMTxdrZcCDZcA_5), value);
	}

	inline static int32_t get_offset_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_6() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___HeiaDLCywuXsOrgGQhDKAJyFIubw_6)); }
	inline ReadOnlyCollection_1_tBD7D8D2E9C5F8B4B0C4FF78D2202E23AC1F7F65B * get_HeiaDLCywuXsOrgGQhDKAJyFIubw_6() const { return ___HeiaDLCywuXsOrgGQhDKAJyFIubw_6; }
	inline ReadOnlyCollection_1_tBD7D8D2E9C5F8B4B0C4FF78D2202E23AC1F7F65B ** get_address_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_6() { return &___HeiaDLCywuXsOrgGQhDKAJyFIubw_6; }
	inline void set_HeiaDLCywuXsOrgGQhDKAJyFIubw_6(ReadOnlyCollection_1_tBD7D8D2E9C5F8B4B0C4FF78D2202E23AC1F7F65B * value)
	{
		___HeiaDLCywuXsOrgGQhDKAJyFIubw_6 = value;
		Il2CppCodeGenWriteBarrier((&___HeiaDLCywuXsOrgGQhDKAJyFIubw_6), value);
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_7() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_7)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_7() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_7; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_7() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_7; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_7(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATE_TB3D9364CC27D096A6AAE8B9960440B43ECAC8B98_H
#ifndef CONTROLLERTEMPLATEELEMENTTYPE_T64649CE1F95852B8211119A5D65EE4ACDA0C49D2_H
#define CONTROLLERTEMPLATEELEMENTTYPE_T64649CE1F95852B8211119A5D65EE4ACDA0C49D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplateElementType
struct  ControllerTemplateElementType_t64649CE1F95852B8211119A5D65EE4ACDA0C49D2 
{
public:
	// System.Int32 Rewired.ControllerTemplateElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerTemplateElementType_t64649CE1F95852B8211119A5D65EE4ACDA0C49D2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEELEMENTTYPE_T64649CE1F95852B8211119A5D65EE4ACDA0C49D2_H
#ifndef CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#define CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerType
struct  ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4 
{
public:
	// System.Int32 Rewired.ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifndef INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#define INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSource
struct  InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B 
{
public:
	// System.Int32 Rewired.InputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifndef MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#define MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKeyFlags
struct  ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651 
{
public:
	// System.Int32 Rewired.ModifierKeyFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifndef POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#define POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Pole
struct  Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C 
{
public:
	// System.Int32 Rewired.Pole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef LJAWZBUJISAGLPPZEEUKFPASUWYI_TC17A4E1E7A8A558859C293A84360B1DB84D83384_H
#define LJAWZBUJISAGLPPZEEUKFPASUWYI_TC17A4E1E7A8A558859C293A84360B1DB84D83384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LJAWzBuJIsAgLPPZeEUKfpasUWYi
struct  LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384  : public RuntimeObject
{
public:
	// Rewired.ControllerTemplateElementType LJAWzBuJIsAgLPPZeEUKfpasUWYi::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_0;
	// System.Boolean LJAWzBuJIsAgLPPZeEUKfpasUWYi::eznOANlIXKDtOGirosRhYGEDDrn
	bool ___eznOANlIXKDtOGirosRhYGEDDrn_1;
	// Rewired.IControllerElementTarget LJAWzBuJIsAgLPPZeEUKfpasUWYi::YnprvkQpvYdMKdzVQVDfUcJcXFy
	RuntimeObject* ___YnprvkQpvYdMKdzVQVDfUcJcXFy_2;
	// Rewired.IControllerElementTarget LJAWzBuJIsAgLPPZeEUKfpasUWYi::VTSBUugZkJasAwWRIbsEjuePrNWv
	RuntimeObject* ___VTSBUugZkJasAwWRIbsEjuePrNWv_3;
	// Rewired.IControllerElementTarget LJAWzBuJIsAgLPPZeEUKfpasUWYi::QIobvlUKxZwRHGhPgtUxpMGNvvb
	RuntimeObject* ___QIobvlUKxZwRHGhPgtUxpMGNvvb_4;

public:
	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_0() { return static_cast<int32_t>(offsetof(LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384, ___RmmmEkVblcqxoqGYhifgdSESkSn_0)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_0() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_0; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_0() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_0; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_0(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_0 = value;
	}

	inline static int32_t get_offset_of_eznOANlIXKDtOGirosRhYGEDDrn_1() { return static_cast<int32_t>(offsetof(LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384, ___eznOANlIXKDtOGirosRhYGEDDrn_1)); }
	inline bool get_eznOANlIXKDtOGirosRhYGEDDrn_1() const { return ___eznOANlIXKDtOGirosRhYGEDDrn_1; }
	inline bool* get_address_of_eznOANlIXKDtOGirosRhYGEDDrn_1() { return &___eznOANlIXKDtOGirosRhYGEDDrn_1; }
	inline void set_eznOANlIXKDtOGirosRhYGEDDrn_1(bool value)
	{
		___eznOANlIXKDtOGirosRhYGEDDrn_1 = value;
	}

	inline static int32_t get_offset_of_YnprvkQpvYdMKdzVQVDfUcJcXFy_2() { return static_cast<int32_t>(offsetof(LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384, ___YnprvkQpvYdMKdzVQVDfUcJcXFy_2)); }
	inline RuntimeObject* get_YnprvkQpvYdMKdzVQVDfUcJcXFy_2() const { return ___YnprvkQpvYdMKdzVQVDfUcJcXFy_2; }
	inline RuntimeObject** get_address_of_YnprvkQpvYdMKdzVQVDfUcJcXFy_2() { return &___YnprvkQpvYdMKdzVQVDfUcJcXFy_2; }
	inline void set_YnprvkQpvYdMKdzVQVDfUcJcXFy_2(RuntimeObject* value)
	{
		___YnprvkQpvYdMKdzVQVDfUcJcXFy_2 = value;
		Il2CppCodeGenWriteBarrier((&___YnprvkQpvYdMKdzVQVDfUcJcXFy_2), value);
	}

	inline static int32_t get_offset_of_VTSBUugZkJasAwWRIbsEjuePrNWv_3() { return static_cast<int32_t>(offsetof(LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384, ___VTSBUugZkJasAwWRIbsEjuePrNWv_3)); }
	inline RuntimeObject* get_VTSBUugZkJasAwWRIbsEjuePrNWv_3() const { return ___VTSBUugZkJasAwWRIbsEjuePrNWv_3; }
	inline RuntimeObject** get_address_of_VTSBUugZkJasAwWRIbsEjuePrNWv_3() { return &___VTSBUugZkJasAwWRIbsEjuePrNWv_3; }
	inline void set_VTSBUugZkJasAwWRIbsEjuePrNWv_3(RuntimeObject* value)
	{
		___VTSBUugZkJasAwWRIbsEjuePrNWv_3 = value;
		Il2CppCodeGenWriteBarrier((&___VTSBUugZkJasAwWRIbsEjuePrNWv_3), value);
	}

	inline static int32_t get_offset_of_QIobvlUKxZwRHGhPgtUxpMGNvvb_4() { return static_cast<int32_t>(offsetof(LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384, ___QIobvlUKxZwRHGhPgtUxpMGNvvb_4)); }
	inline RuntimeObject* get_QIobvlUKxZwRHGhPgtUxpMGNvvb_4() const { return ___QIobvlUKxZwRHGhPgtUxpMGNvvb_4; }
	inline RuntimeObject** get_address_of_QIobvlUKxZwRHGhPgtUxpMGNvvb_4() { return &___QIobvlUKxZwRHGhPgtUxpMGNvvb_4; }
	inline void set_QIobvlUKxZwRHGhPgtUxpMGNvvb_4(RuntimeObject* value)
	{
		___QIobvlUKxZwRHGhPgtUxpMGNvvb_4 = value;
		Il2CppCodeGenWriteBarrier((&___QIobvlUKxZwRHGhPgtUxpMGNvvb_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LJAWZBUJISAGLPPZEEUKFPASUWYI_TC17A4E1E7A8A558859C293A84360B1DB84D83384_H
#ifndef CONSTS_T13F3F01F099D81D71412103E16570479844CF780_H
#define CONSTS_T13F3F01F099D81D71412103E16570479844CF780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Consts
struct  Consts_t13F3F01F099D81D71412103E16570479844CF780  : public RuntimeObject
{
public:

public:
};

struct Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields
{
public:
	// Rewired.PidVid[] Rewired.Consts::questionablePidVids
	PidVidU5BU5D_tF8661A2C9138A10268BF87CD9D6C36EE964F2B51* ___questionablePidVids_253;
	// System.Int32[] Rewired.Consts::questionableVIDs
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___questionableVIDs_254;
	// System.Guid Rewired.Consts::joystickGuid_appleMFiController
	Guid_t  ___joystickGuid_appleMFiController_255;
	// System.Guid Rewired.Consts::joystickGuid_standardizedGamepad
	Guid_t  ___joystickGuid_standardizedGamepad_256;
	// System.Guid Rewired.Consts::joystickGuid_SonyDualShock4
	Guid_t  ___joystickGuid_SonyDualShock4_257;
	// System.Guid Rewired.Consts::hardwareTypeGuid_universalKeyboard
	Guid_t  ___hardwareTypeGuid_universalKeyboard_258;
	// System.Guid Rewired.Consts::hardwareTypeGuid_universalMouse
	Guid_t  ___hardwareTypeGuid_universalMouse_259;
	// System.Collections.Generic.IList`1<System.String> Rewired.Consts::unityMouseElementNames
	RuntimeObject* ___unityMouseElementNames_260;
	// System.String[] Rewired.Consts::oilgvhIIXpJffRBfPRfShHYUoNi
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___oilgvhIIXpJffRBfPRfShHYUoNi_261;
	// System.Collections.Generic.IList`1<System.String> Rewired.Consts::unityMouseAxisPositiveNames
	RuntimeObject* ___unityMouseAxisPositiveNames_262;
	// System.String[] Rewired.Consts::iZFGbNBwLBNYUtWVIHzCDUIbCmOQ
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___iZFGbNBwLBNYUtWVIHzCDUIbCmOQ_263;
	// System.Collections.Generic.IList`1<System.String> Rewired.Consts::unityMouseAxisNegativeNames
	RuntimeObject* ___unityMouseAxisNegativeNames_264;
	// System.String[] Rewired.Consts::RHirAoZBvKDVhHaheZciDYnHUEN
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___RHirAoZBvKDVhHaheZciDYnHUEN_265;
	// System.Collections.Generic.IList`1<System.String> Rewired.Consts::rawInputUnifiedMouseElementNames
	RuntimeObject* ___rawInputUnifiedMouseElementNames_266;
	// System.String[] Rewired.Consts::VcnXCLOKxcEFcLrWCgIMroyNYkt
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___VcnXCLOKxcEFcLrWCgIMroyNYkt_267;
	// System.Collections.Generic.IList`1<System.String> Rewired.Consts::rawInputUnifiedMouseAxisPositiveNames
	RuntimeObject* ___rawInputUnifiedMouseAxisPositiveNames_268;
	// System.String[] Rewired.Consts::MCEbGtKwlQAUWcpqsvbpNEDKOr
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___MCEbGtKwlQAUWcpqsvbpNEDKOr_269;
	// System.Collections.Generic.IList`1<System.String> Rewired.Consts::rawInputUnifiedMouseAxisNegativeNames
	RuntimeObject* ___rawInputUnifiedMouseAxisNegativeNames_270;
	// System.String[] Rewired.Consts::CkoPXBSQrCLFXCCmqQinTbBsLLb
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___CkoPXBSQrCLFXCCmqQinTbBsLLb_271;
	// System.Collections.Generic.IList`1<System.Int32> Rewired.Consts::unityMouseElementIdentifierIds
	RuntimeObject* ___unityMouseElementIdentifierIds_272;
	// System.Int32[] Rewired.Consts::fdUCNxJTlLdMkOKZhkqemMmsmyRC
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___fdUCNxJTlLdMkOKZhkqemMmsmyRC_273;
	// System.Collections.Generic.IList`1<System.Int32> Rewired.Consts::rawInputUnifiedMouseElementIdentifierIds
	RuntimeObject* ___rawInputUnifiedMouseElementIdentifierIds_274;
	// System.Int32[] Rewired.Consts::fHduvNyJgsIQEbQYpRLDgrkAqgS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___fHduvNyJgsIQEbQYpRLDgrkAqgS_275;
	// System.Collections.Generic.IList`1<System.String> Rewired.Consts::mouseAxisUnityNames
	RuntimeObject* ___mouseAxisUnityNames_276;
	// System.String[] Rewired.Consts::AFolGfcvGmlfjHKfLUpiqMPPgMC
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___AFolGfcvGmlfjHKfLUpiqMPPgMC_277;
	// System.Collections.Generic.IList`1<System.String> Rewired.Consts::mouseButtonUnityNames
	RuntimeObject* ___mouseButtonUnityNames_278;
	// System.String[] Rewired.Consts::jBLfNftWBRMiEtPOZmryCzIlXOn
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___jBLfNftWBRMiEtPOZmryCzIlXOn_279;
	// System.Collections.Generic.IList`1<System.String> Rewired.Consts::keyboardKeyNames
	RuntimeObject* ___keyboardKeyNames_280;
	// System.String[] Rewired.Consts::iCvRWnIpVHWjGBXoLDPSyGqWCQh
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___iCvRWnIpVHWjGBXoLDPSyGqWCQh_281;
	// System.Collections.Generic.IList`1<System.Int32> Rewired.Consts::keyboardKeyValues
	RuntimeObject* ___keyboardKeyValues_282;
	// System.Int32[] Rewired.Consts::_keyboardKeyValues
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____keyboardKeyValues_283;
	// System.Collections.Generic.IList`1<System.String> Rewired.Consts::modifierKeyShortNames
	RuntimeObject* ___modifierKeyShortNames_284;
	// System.String[] Rewired.Consts::NMLcViElmPXmZReaSpfhyXMuatO
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___NMLcViElmPXmZReaSpfhyXMuatO_285;

public:
	inline static int32_t get_offset_of_questionablePidVids_253() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___questionablePidVids_253)); }
	inline PidVidU5BU5D_tF8661A2C9138A10268BF87CD9D6C36EE964F2B51* get_questionablePidVids_253() const { return ___questionablePidVids_253; }
	inline PidVidU5BU5D_tF8661A2C9138A10268BF87CD9D6C36EE964F2B51** get_address_of_questionablePidVids_253() { return &___questionablePidVids_253; }
	inline void set_questionablePidVids_253(PidVidU5BU5D_tF8661A2C9138A10268BF87CD9D6C36EE964F2B51* value)
	{
		___questionablePidVids_253 = value;
		Il2CppCodeGenWriteBarrier((&___questionablePidVids_253), value);
	}

	inline static int32_t get_offset_of_questionableVIDs_254() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___questionableVIDs_254)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_questionableVIDs_254() const { return ___questionableVIDs_254; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_questionableVIDs_254() { return &___questionableVIDs_254; }
	inline void set_questionableVIDs_254(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___questionableVIDs_254 = value;
		Il2CppCodeGenWriteBarrier((&___questionableVIDs_254), value);
	}

	inline static int32_t get_offset_of_joystickGuid_appleMFiController_255() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___joystickGuid_appleMFiController_255)); }
	inline Guid_t  get_joystickGuid_appleMFiController_255() const { return ___joystickGuid_appleMFiController_255; }
	inline Guid_t * get_address_of_joystickGuid_appleMFiController_255() { return &___joystickGuid_appleMFiController_255; }
	inline void set_joystickGuid_appleMFiController_255(Guid_t  value)
	{
		___joystickGuid_appleMFiController_255 = value;
	}

	inline static int32_t get_offset_of_joystickGuid_standardizedGamepad_256() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___joystickGuid_standardizedGamepad_256)); }
	inline Guid_t  get_joystickGuid_standardizedGamepad_256() const { return ___joystickGuid_standardizedGamepad_256; }
	inline Guid_t * get_address_of_joystickGuid_standardizedGamepad_256() { return &___joystickGuid_standardizedGamepad_256; }
	inline void set_joystickGuid_standardizedGamepad_256(Guid_t  value)
	{
		___joystickGuid_standardizedGamepad_256 = value;
	}

	inline static int32_t get_offset_of_joystickGuid_SonyDualShock4_257() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___joystickGuid_SonyDualShock4_257)); }
	inline Guid_t  get_joystickGuid_SonyDualShock4_257() const { return ___joystickGuid_SonyDualShock4_257; }
	inline Guid_t * get_address_of_joystickGuid_SonyDualShock4_257() { return &___joystickGuid_SonyDualShock4_257; }
	inline void set_joystickGuid_SonyDualShock4_257(Guid_t  value)
	{
		___joystickGuid_SonyDualShock4_257 = value;
	}

	inline static int32_t get_offset_of_hardwareTypeGuid_universalKeyboard_258() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___hardwareTypeGuid_universalKeyboard_258)); }
	inline Guid_t  get_hardwareTypeGuid_universalKeyboard_258() const { return ___hardwareTypeGuid_universalKeyboard_258; }
	inline Guid_t * get_address_of_hardwareTypeGuid_universalKeyboard_258() { return &___hardwareTypeGuid_universalKeyboard_258; }
	inline void set_hardwareTypeGuid_universalKeyboard_258(Guid_t  value)
	{
		___hardwareTypeGuid_universalKeyboard_258 = value;
	}

	inline static int32_t get_offset_of_hardwareTypeGuid_universalMouse_259() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___hardwareTypeGuid_universalMouse_259)); }
	inline Guid_t  get_hardwareTypeGuid_universalMouse_259() const { return ___hardwareTypeGuid_universalMouse_259; }
	inline Guid_t * get_address_of_hardwareTypeGuid_universalMouse_259() { return &___hardwareTypeGuid_universalMouse_259; }
	inline void set_hardwareTypeGuid_universalMouse_259(Guid_t  value)
	{
		___hardwareTypeGuid_universalMouse_259 = value;
	}

	inline static int32_t get_offset_of_unityMouseElementNames_260() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___unityMouseElementNames_260)); }
	inline RuntimeObject* get_unityMouseElementNames_260() const { return ___unityMouseElementNames_260; }
	inline RuntimeObject** get_address_of_unityMouseElementNames_260() { return &___unityMouseElementNames_260; }
	inline void set_unityMouseElementNames_260(RuntimeObject* value)
	{
		___unityMouseElementNames_260 = value;
		Il2CppCodeGenWriteBarrier((&___unityMouseElementNames_260), value);
	}

	inline static int32_t get_offset_of_oilgvhIIXpJffRBfPRfShHYUoNi_261() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___oilgvhIIXpJffRBfPRfShHYUoNi_261)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_oilgvhIIXpJffRBfPRfShHYUoNi_261() const { return ___oilgvhIIXpJffRBfPRfShHYUoNi_261; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_oilgvhIIXpJffRBfPRfShHYUoNi_261() { return &___oilgvhIIXpJffRBfPRfShHYUoNi_261; }
	inline void set_oilgvhIIXpJffRBfPRfShHYUoNi_261(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___oilgvhIIXpJffRBfPRfShHYUoNi_261 = value;
		Il2CppCodeGenWriteBarrier((&___oilgvhIIXpJffRBfPRfShHYUoNi_261), value);
	}

	inline static int32_t get_offset_of_unityMouseAxisPositiveNames_262() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___unityMouseAxisPositiveNames_262)); }
	inline RuntimeObject* get_unityMouseAxisPositiveNames_262() const { return ___unityMouseAxisPositiveNames_262; }
	inline RuntimeObject** get_address_of_unityMouseAxisPositiveNames_262() { return &___unityMouseAxisPositiveNames_262; }
	inline void set_unityMouseAxisPositiveNames_262(RuntimeObject* value)
	{
		___unityMouseAxisPositiveNames_262 = value;
		Il2CppCodeGenWriteBarrier((&___unityMouseAxisPositiveNames_262), value);
	}

	inline static int32_t get_offset_of_iZFGbNBwLBNYUtWVIHzCDUIbCmOQ_263() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___iZFGbNBwLBNYUtWVIHzCDUIbCmOQ_263)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_iZFGbNBwLBNYUtWVIHzCDUIbCmOQ_263() const { return ___iZFGbNBwLBNYUtWVIHzCDUIbCmOQ_263; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_iZFGbNBwLBNYUtWVIHzCDUIbCmOQ_263() { return &___iZFGbNBwLBNYUtWVIHzCDUIbCmOQ_263; }
	inline void set_iZFGbNBwLBNYUtWVIHzCDUIbCmOQ_263(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___iZFGbNBwLBNYUtWVIHzCDUIbCmOQ_263 = value;
		Il2CppCodeGenWriteBarrier((&___iZFGbNBwLBNYUtWVIHzCDUIbCmOQ_263), value);
	}

	inline static int32_t get_offset_of_unityMouseAxisNegativeNames_264() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___unityMouseAxisNegativeNames_264)); }
	inline RuntimeObject* get_unityMouseAxisNegativeNames_264() const { return ___unityMouseAxisNegativeNames_264; }
	inline RuntimeObject** get_address_of_unityMouseAxisNegativeNames_264() { return &___unityMouseAxisNegativeNames_264; }
	inline void set_unityMouseAxisNegativeNames_264(RuntimeObject* value)
	{
		___unityMouseAxisNegativeNames_264 = value;
		Il2CppCodeGenWriteBarrier((&___unityMouseAxisNegativeNames_264), value);
	}

	inline static int32_t get_offset_of_RHirAoZBvKDVhHaheZciDYnHUEN_265() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___RHirAoZBvKDVhHaheZciDYnHUEN_265)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_RHirAoZBvKDVhHaheZciDYnHUEN_265() const { return ___RHirAoZBvKDVhHaheZciDYnHUEN_265; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_RHirAoZBvKDVhHaheZciDYnHUEN_265() { return &___RHirAoZBvKDVhHaheZciDYnHUEN_265; }
	inline void set_RHirAoZBvKDVhHaheZciDYnHUEN_265(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___RHirAoZBvKDVhHaheZciDYnHUEN_265 = value;
		Il2CppCodeGenWriteBarrier((&___RHirAoZBvKDVhHaheZciDYnHUEN_265), value);
	}

	inline static int32_t get_offset_of_rawInputUnifiedMouseElementNames_266() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___rawInputUnifiedMouseElementNames_266)); }
	inline RuntimeObject* get_rawInputUnifiedMouseElementNames_266() const { return ___rawInputUnifiedMouseElementNames_266; }
	inline RuntimeObject** get_address_of_rawInputUnifiedMouseElementNames_266() { return &___rawInputUnifiedMouseElementNames_266; }
	inline void set_rawInputUnifiedMouseElementNames_266(RuntimeObject* value)
	{
		___rawInputUnifiedMouseElementNames_266 = value;
		Il2CppCodeGenWriteBarrier((&___rawInputUnifiedMouseElementNames_266), value);
	}

	inline static int32_t get_offset_of_VcnXCLOKxcEFcLrWCgIMroyNYkt_267() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___VcnXCLOKxcEFcLrWCgIMroyNYkt_267)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_VcnXCLOKxcEFcLrWCgIMroyNYkt_267() const { return ___VcnXCLOKxcEFcLrWCgIMroyNYkt_267; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_VcnXCLOKxcEFcLrWCgIMroyNYkt_267() { return &___VcnXCLOKxcEFcLrWCgIMroyNYkt_267; }
	inline void set_VcnXCLOKxcEFcLrWCgIMroyNYkt_267(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___VcnXCLOKxcEFcLrWCgIMroyNYkt_267 = value;
		Il2CppCodeGenWriteBarrier((&___VcnXCLOKxcEFcLrWCgIMroyNYkt_267), value);
	}

	inline static int32_t get_offset_of_rawInputUnifiedMouseAxisPositiveNames_268() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___rawInputUnifiedMouseAxisPositiveNames_268)); }
	inline RuntimeObject* get_rawInputUnifiedMouseAxisPositiveNames_268() const { return ___rawInputUnifiedMouseAxisPositiveNames_268; }
	inline RuntimeObject** get_address_of_rawInputUnifiedMouseAxisPositiveNames_268() { return &___rawInputUnifiedMouseAxisPositiveNames_268; }
	inline void set_rawInputUnifiedMouseAxisPositiveNames_268(RuntimeObject* value)
	{
		___rawInputUnifiedMouseAxisPositiveNames_268 = value;
		Il2CppCodeGenWriteBarrier((&___rawInputUnifiedMouseAxisPositiveNames_268), value);
	}

	inline static int32_t get_offset_of_MCEbGtKwlQAUWcpqsvbpNEDKOr_269() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___MCEbGtKwlQAUWcpqsvbpNEDKOr_269)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_MCEbGtKwlQAUWcpqsvbpNEDKOr_269() const { return ___MCEbGtKwlQAUWcpqsvbpNEDKOr_269; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_MCEbGtKwlQAUWcpqsvbpNEDKOr_269() { return &___MCEbGtKwlQAUWcpqsvbpNEDKOr_269; }
	inline void set_MCEbGtKwlQAUWcpqsvbpNEDKOr_269(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___MCEbGtKwlQAUWcpqsvbpNEDKOr_269 = value;
		Il2CppCodeGenWriteBarrier((&___MCEbGtKwlQAUWcpqsvbpNEDKOr_269), value);
	}

	inline static int32_t get_offset_of_rawInputUnifiedMouseAxisNegativeNames_270() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___rawInputUnifiedMouseAxisNegativeNames_270)); }
	inline RuntimeObject* get_rawInputUnifiedMouseAxisNegativeNames_270() const { return ___rawInputUnifiedMouseAxisNegativeNames_270; }
	inline RuntimeObject** get_address_of_rawInputUnifiedMouseAxisNegativeNames_270() { return &___rawInputUnifiedMouseAxisNegativeNames_270; }
	inline void set_rawInputUnifiedMouseAxisNegativeNames_270(RuntimeObject* value)
	{
		___rawInputUnifiedMouseAxisNegativeNames_270 = value;
		Il2CppCodeGenWriteBarrier((&___rawInputUnifiedMouseAxisNegativeNames_270), value);
	}

	inline static int32_t get_offset_of_CkoPXBSQrCLFXCCmqQinTbBsLLb_271() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___CkoPXBSQrCLFXCCmqQinTbBsLLb_271)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_CkoPXBSQrCLFXCCmqQinTbBsLLb_271() const { return ___CkoPXBSQrCLFXCCmqQinTbBsLLb_271; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_CkoPXBSQrCLFXCCmqQinTbBsLLb_271() { return &___CkoPXBSQrCLFXCCmqQinTbBsLLb_271; }
	inline void set_CkoPXBSQrCLFXCCmqQinTbBsLLb_271(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___CkoPXBSQrCLFXCCmqQinTbBsLLb_271 = value;
		Il2CppCodeGenWriteBarrier((&___CkoPXBSQrCLFXCCmqQinTbBsLLb_271), value);
	}

	inline static int32_t get_offset_of_unityMouseElementIdentifierIds_272() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___unityMouseElementIdentifierIds_272)); }
	inline RuntimeObject* get_unityMouseElementIdentifierIds_272() const { return ___unityMouseElementIdentifierIds_272; }
	inline RuntimeObject** get_address_of_unityMouseElementIdentifierIds_272() { return &___unityMouseElementIdentifierIds_272; }
	inline void set_unityMouseElementIdentifierIds_272(RuntimeObject* value)
	{
		___unityMouseElementIdentifierIds_272 = value;
		Il2CppCodeGenWriteBarrier((&___unityMouseElementIdentifierIds_272), value);
	}

	inline static int32_t get_offset_of_fdUCNxJTlLdMkOKZhkqemMmsmyRC_273() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___fdUCNxJTlLdMkOKZhkqemMmsmyRC_273)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_fdUCNxJTlLdMkOKZhkqemMmsmyRC_273() const { return ___fdUCNxJTlLdMkOKZhkqemMmsmyRC_273; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_fdUCNxJTlLdMkOKZhkqemMmsmyRC_273() { return &___fdUCNxJTlLdMkOKZhkqemMmsmyRC_273; }
	inline void set_fdUCNxJTlLdMkOKZhkqemMmsmyRC_273(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___fdUCNxJTlLdMkOKZhkqemMmsmyRC_273 = value;
		Il2CppCodeGenWriteBarrier((&___fdUCNxJTlLdMkOKZhkqemMmsmyRC_273), value);
	}

	inline static int32_t get_offset_of_rawInputUnifiedMouseElementIdentifierIds_274() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___rawInputUnifiedMouseElementIdentifierIds_274)); }
	inline RuntimeObject* get_rawInputUnifiedMouseElementIdentifierIds_274() const { return ___rawInputUnifiedMouseElementIdentifierIds_274; }
	inline RuntimeObject** get_address_of_rawInputUnifiedMouseElementIdentifierIds_274() { return &___rawInputUnifiedMouseElementIdentifierIds_274; }
	inline void set_rawInputUnifiedMouseElementIdentifierIds_274(RuntimeObject* value)
	{
		___rawInputUnifiedMouseElementIdentifierIds_274 = value;
		Il2CppCodeGenWriteBarrier((&___rawInputUnifiedMouseElementIdentifierIds_274), value);
	}

	inline static int32_t get_offset_of_fHduvNyJgsIQEbQYpRLDgrkAqgS_275() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___fHduvNyJgsIQEbQYpRLDgrkAqgS_275)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_fHduvNyJgsIQEbQYpRLDgrkAqgS_275() const { return ___fHduvNyJgsIQEbQYpRLDgrkAqgS_275; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_fHduvNyJgsIQEbQYpRLDgrkAqgS_275() { return &___fHduvNyJgsIQEbQYpRLDgrkAqgS_275; }
	inline void set_fHduvNyJgsIQEbQYpRLDgrkAqgS_275(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___fHduvNyJgsIQEbQYpRLDgrkAqgS_275 = value;
		Il2CppCodeGenWriteBarrier((&___fHduvNyJgsIQEbQYpRLDgrkAqgS_275), value);
	}

	inline static int32_t get_offset_of_mouseAxisUnityNames_276() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___mouseAxisUnityNames_276)); }
	inline RuntimeObject* get_mouseAxisUnityNames_276() const { return ___mouseAxisUnityNames_276; }
	inline RuntimeObject** get_address_of_mouseAxisUnityNames_276() { return &___mouseAxisUnityNames_276; }
	inline void set_mouseAxisUnityNames_276(RuntimeObject* value)
	{
		___mouseAxisUnityNames_276 = value;
		Il2CppCodeGenWriteBarrier((&___mouseAxisUnityNames_276), value);
	}

	inline static int32_t get_offset_of_AFolGfcvGmlfjHKfLUpiqMPPgMC_277() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___AFolGfcvGmlfjHKfLUpiqMPPgMC_277)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_AFolGfcvGmlfjHKfLUpiqMPPgMC_277() const { return ___AFolGfcvGmlfjHKfLUpiqMPPgMC_277; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_AFolGfcvGmlfjHKfLUpiqMPPgMC_277() { return &___AFolGfcvGmlfjHKfLUpiqMPPgMC_277; }
	inline void set_AFolGfcvGmlfjHKfLUpiqMPPgMC_277(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___AFolGfcvGmlfjHKfLUpiqMPPgMC_277 = value;
		Il2CppCodeGenWriteBarrier((&___AFolGfcvGmlfjHKfLUpiqMPPgMC_277), value);
	}

	inline static int32_t get_offset_of_mouseButtonUnityNames_278() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___mouseButtonUnityNames_278)); }
	inline RuntimeObject* get_mouseButtonUnityNames_278() const { return ___mouseButtonUnityNames_278; }
	inline RuntimeObject** get_address_of_mouseButtonUnityNames_278() { return &___mouseButtonUnityNames_278; }
	inline void set_mouseButtonUnityNames_278(RuntimeObject* value)
	{
		___mouseButtonUnityNames_278 = value;
		Il2CppCodeGenWriteBarrier((&___mouseButtonUnityNames_278), value);
	}

	inline static int32_t get_offset_of_jBLfNftWBRMiEtPOZmryCzIlXOn_279() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___jBLfNftWBRMiEtPOZmryCzIlXOn_279)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_jBLfNftWBRMiEtPOZmryCzIlXOn_279() const { return ___jBLfNftWBRMiEtPOZmryCzIlXOn_279; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_jBLfNftWBRMiEtPOZmryCzIlXOn_279() { return &___jBLfNftWBRMiEtPOZmryCzIlXOn_279; }
	inline void set_jBLfNftWBRMiEtPOZmryCzIlXOn_279(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___jBLfNftWBRMiEtPOZmryCzIlXOn_279 = value;
		Il2CppCodeGenWriteBarrier((&___jBLfNftWBRMiEtPOZmryCzIlXOn_279), value);
	}

	inline static int32_t get_offset_of_keyboardKeyNames_280() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___keyboardKeyNames_280)); }
	inline RuntimeObject* get_keyboardKeyNames_280() const { return ___keyboardKeyNames_280; }
	inline RuntimeObject** get_address_of_keyboardKeyNames_280() { return &___keyboardKeyNames_280; }
	inline void set_keyboardKeyNames_280(RuntimeObject* value)
	{
		___keyboardKeyNames_280 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardKeyNames_280), value);
	}

	inline static int32_t get_offset_of_iCvRWnIpVHWjGBXoLDPSyGqWCQh_281() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___iCvRWnIpVHWjGBXoLDPSyGqWCQh_281)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_iCvRWnIpVHWjGBXoLDPSyGqWCQh_281() const { return ___iCvRWnIpVHWjGBXoLDPSyGqWCQh_281; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_iCvRWnIpVHWjGBXoLDPSyGqWCQh_281() { return &___iCvRWnIpVHWjGBXoLDPSyGqWCQh_281; }
	inline void set_iCvRWnIpVHWjGBXoLDPSyGqWCQh_281(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___iCvRWnIpVHWjGBXoLDPSyGqWCQh_281 = value;
		Il2CppCodeGenWriteBarrier((&___iCvRWnIpVHWjGBXoLDPSyGqWCQh_281), value);
	}

	inline static int32_t get_offset_of_keyboardKeyValues_282() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___keyboardKeyValues_282)); }
	inline RuntimeObject* get_keyboardKeyValues_282() const { return ___keyboardKeyValues_282; }
	inline RuntimeObject** get_address_of_keyboardKeyValues_282() { return &___keyboardKeyValues_282; }
	inline void set_keyboardKeyValues_282(RuntimeObject* value)
	{
		___keyboardKeyValues_282 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardKeyValues_282), value);
	}

	inline static int32_t get_offset_of__keyboardKeyValues_283() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ____keyboardKeyValues_283)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__keyboardKeyValues_283() const { return ____keyboardKeyValues_283; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__keyboardKeyValues_283() { return &____keyboardKeyValues_283; }
	inline void set__keyboardKeyValues_283(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____keyboardKeyValues_283 = value;
		Il2CppCodeGenWriteBarrier((&____keyboardKeyValues_283), value);
	}

	inline static int32_t get_offset_of_modifierKeyShortNames_284() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___modifierKeyShortNames_284)); }
	inline RuntimeObject* get_modifierKeyShortNames_284() const { return ___modifierKeyShortNames_284; }
	inline RuntimeObject** get_address_of_modifierKeyShortNames_284() { return &___modifierKeyShortNames_284; }
	inline void set_modifierKeyShortNames_284(RuntimeObject* value)
	{
		___modifierKeyShortNames_284 = value;
		Il2CppCodeGenWriteBarrier((&___modifierKeyShortNames_284), value);
	}

	inline static int32_t get_offset_of_NMLcViElmPXmZReaSpfhyXMuatO_285() { return static_cast<int32_t>(offsetof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields, ___NMLcViElmPXmZReaSpfhyXMuatO_285)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_NMLcViElmPXmZReaSpfhyXMuatO_285() const { return ___NMLcViElmPXmZReaSpfhyXMuatO_285; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_NMLcViElmPXmZReaSpfhyXMuatO_285() { return &___NMLcViElmPXmZReaSpfhyXMuatO_285; }
	inline void set_NMLcViElmPXmZReaSpfhyXMuatO_285(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___NMLcViElmPXmZReaSpfhyXMuatO_285 = value;
		Il2CppCodeGenWriteBarrier((&___NMLcViElmPXmZReaSpfhyXMuatO_285), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTS_T13F3F01F099D81D71412103E16570479844CF780_H
#ifndef COMPOUNDELEMENT_T210CC0E132785075DA64D166B02AFE1D6ECBC3BA_H
#define COMPOUNDELEMENT_T210CC0E132785075DA64D166B02AFE1D6ECBC3BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_CompoundElement
struct  CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Controller_CompoundElement::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_0;
	// System.String Rewired.Controller_CompoundElement::xzFIhZhMHNeQYGDUhfMfvvqdZQds
	String_t* ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1;
	// Rewired.CompoundControllerElementType Rewired.Controller_CompoundElement::DzAgXbAsFcjwinjPLOBYLcFrhIr
	int32_t ___DzAgXbAsFcjwinjPLOBYLcFrhIr_2;
	// System.Int32 Rewired.Controller_CompoundElement::prxRdKvUePoVtgFVPLYBrpHztMr
	int32_t ___prxRdKvUePoVtgFVPLYBrpHztMr_3;
	// Rewired.Controller_CompoundElement_OqivKqJqmnbJCfstNgONqfwyygM[] Rewired.Controller_CompoundElement::muHfgZrAoceOXsMhCljOQSVcJGk
	OqivKqJqmnbJCfstNgONqfwyygMU5BU5D_t6A7F37C622A08B7738262EF07A8E50A0D7C112CC* ___muHfgZrAoceOXsMhCljOQSVcJGk_4;
	// Rewired.Controller Rewired.Controller_CompoundElement::gcFDOBoqpaBweHMwNlbneFOEEAm
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___gcFDOBoqpaBweHMwNlbneFOEEAm_5;
	// System.Int32 Rewired.Controller_CompoundElement::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_6;

public:
	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_0() { return static_cast<int32_t>(offsetof(CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA, ___FrsWYgeIWMnjOcXDysagwZGcCMF_0)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_0() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_0; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_0() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_0; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_0(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_0 = value;
	}

	inline static int32_t get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1() { return static_cast<int32_t>(offsetof(CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA, ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1)); }
	inline String_t* get_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1() const { return ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1; }
	inline String_t** get_address_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1() { return &___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1; }
	inline void set_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1(String_t* value)
	{
		___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1 = value;
		Il2CppCodeGenWriteBarrier((&___xzFIhZhMHNeQYGDUhfMfvvqdZQds_1), value);
	}

	inline static int32_t get_offset_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_2() { return static_cast<int32_t>(offsetof(CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA, ___DzAgXbAsFcjwinjPLOBYLcFrhIr_2)); }
	inline int32_t get_DzAgXbAsFcjwinjPLOBYLcFrhIr_2() const { return ___DzAgXbAsFcjwinjPLOBYLcFrhIr_2; }
	inline int32_t* get_address_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_2() { return &___DzAgXbAsFcjwinjPLOBYLcFrhIr_2; }
	inline void set_DzAgXbAsFcjwinjPLOBYLcFrhIr_2(int32_t value)
	{
		___DzAgXbAsFcjwinjPLOBYLcFrhIr_2 = value;
	}

	inline static int32_t get_offset_of_prxRdKvUePoVtgFVPLYBrpHztMr_3() { return static_cast<int32_t>(offsetof(CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA, ___prxRdKvUePoVtgFVPLYBrpHztMr_3)); }
	inline int32_t get_prxRdKvUePoVtgFVPLYBrpHztMr_3() const { return ___prxRdKvUePoVtgFVPLYBrpHztMr_3; }
	inline int32_t* get_address_of_prxRdKvUePoVtgFVPLYBrpHztMr_3() { return &___prxRdKvUePoVtgFVPLYBrpHztMr_3; }
	inline void set_prxRdKvUePoVtgFVPLYBrpHztMr_3(int32_t value)
	{
		___prxRdKvUePoVtgFVPLYBrpHztMr_3 = value;
	}

	inline static int32_t get_offset_of_muHfgZrAoceOXsMhCljOQSVcJGk_4() { return static_cast<int32_t>(offsetof(CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA, ___muHfgZrAoceOXsMhCljOQSVcJGk_4)); }
	inline OqivKqJqmnbJCfstNgONqfwyygMU5BU5D_t6A7F37C622A08B7738262EF07A8E50A0D7C112CC* get_muHfgZrAoceOXsMhCljOQSVcJGk_4() const { return ___muHfgZrAoceOXsMhCljOQSVcJGk_4; }
	inline OqivKqJqmnbJCfstNgONqfwyygMU5BU5D_t6A7F37C622A08B7738262EF07A8E50A0D7C112CC** get_address_of_muHfgZrAoceOXsMhCljOQSVcJGk_4() { return &___muHfgZrAoceOXsMhCljOQSVcJGk_4; }
	inline void set_muHfgZrAoceOXsMhCljOQSVcJGk_4(OqivKqJqmnbJCfstNgONqfwyygMU5BU5D_t6A7F37C622A08B7738262EF07A8E50A0D7C112CC* value)
	{
		___muHfgZrAoceOXsMhCljOQSVcJGk_4 = value;
		Il2CppCodeGenWriteBarrier((&___muHfgZrAoceOXsMhCljOQSVcJGk_4), value);
	}

	inline static int32_t get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_5() { return static_cast<int32_t>(offsetof(CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA, ___gcFDOBoqpaBweHMwNlbneFOEEAm_5)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_gcFDOBoqpaBweHMwNlbneFOEEAm_5() const { return ___gcFDOBoqpaBweHMwNlbneFOEEAm_5; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_gcFDOBoqpaBweHMwNlbneFOEEAm_5() { return &___gcFDOBoqpaBweHMwNlbneFOEEAm_5; }
	inline void set_gcFDOBoqpaBweHMwNlbneFOEEAm_5(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___gcFDOBoqpaBweHMwNlbneFOEEAm_5 = value;
		Il2CppCodeGenWriteBarrier((&___gcFDOBoqpaBweHMwNlbneFOEEAm_5), value);
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_6() { return static_cast<int32_t>(offsetof(CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_6)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_6() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_6; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_6() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_6; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_6(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDELEMENT_T210CC0E132785075DA64D166B02AFE1D6ECBC3BA_H
#ifndef ELEMENT_T55D35599A6B182030EA05B6CA5AB494E24ACA2E5_H
#define ELEMENT_T55D35599A6B182030EA05B6CA5AB494E24ACA2E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Element
struct  Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Controller_Element::id
	int32_t ___id_0;
	// System.String Rewired.Controller_Element::name
	String_t* ___name_1;
	// Rewired.ControllerElementType Rewired.Controller_Element::type
	int32_t ___type_2;
	// Rewired.Controller_Element_ViIrqleiiIiQeymndbeHmwQgupz Rewired.Controller_Element::agfbgFjcERRTUxQhZFpdHagNwfl
	ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416 * ___agfbgFjcERRTUxQhZFpdHagNwfl_3;
	// System.Int32 Rewired.Controller_Element::XttWneCpteIqXNlWieWWCeqJLle
	int32_t ___XttWneCpteIqXNlWieWWCeqJLle_4;
	// Rewired.Controller Rewired.Controller_Element::gcFDOBoqpaBweHMwNlbneFOEEAm
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___gcFDOBoqpaBweHMwNlbneFOEEAm_5;
	// System.Int32 Rewired.Controller_Element::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_6;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_agfbgFjcERRTUxQhZFpdHagNwfl_3() { return static_cast<int32_t>(offsetof(Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5, ___agfbgFjcERRTUxQhZFpdHagNwfl_3)); }
	inline ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416 * get_agfbgFjcERRTUxQhZFpdHagNwfl_3() const { return ___agfbgFjcERRTUxQhZFpdHagNwfl_3; }
	inline ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416 ** get_address_of_agfbgFjcERRTUxQhZFpdHagNwfl_3() { return &___agfbgFjcERRTUxQhZFpdHagNwfl_3; }
	inline void set_agfbgFjcERRTUxQhZFpdHagNwfl_3(ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416 * value)
	{
		___agfbgFjcERRTUxQhZFpdHagNwfl_3 = value;
		Il2CppCodeGenWriteBarrier((&___agfbgFjcERRTUxQhZFpdHagNwfl_3), value);
	}

	inline static int32_t get_offset_of_XttWneCpteIqXNlWieWWCeqJLle_4() { return static_cast<int32_t>(offsetof(Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5, ___XttWneCpteIqXNlWieWWCeqJLle_4)); }
	inline int32_t get_XttWneCpteIqXNlWieWWCeqJLle_4() const { return ___XttWneCpteIqXNlWieWWCeqJLle_4; }
	inline int32_t* get_address_of_XttWneCpteIqXNlWieWWCeqJLle_4() { return &___XttWneCpteIqXNlWieWWCeqJLle_4; }
	inline void set_XttWneCpteIqXNlWieWWCeqJLle_4(int32_t value)
	{
		___XttWneCpteIqXNlWieWWCeqJLle_4 = value;
	}

	inline static int32_t get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_5() { return static_cast<int32_t>(offsetof(Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5, ___gcFDOBoqpaBweHMwNlbneFOEEAm_5)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_gcFDOBoqpaBweHMwNlbneFOEEAm_5() const { return ___gcFDOBoqpaBweHMwNlbneFOEEAm_5; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_gcFDOBoqpaBweHMwNlbneFOEEAm_5() { return &___gcFDOBoqpaBweHMwNlbneFOEEAm_5; }
	inline void set_gcFDOBoqpaBweHMwNlbneFOEEAm_5(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___gcFDOBoqpaBweHMwNlbneFOEEAm_5 = value;
		Il2CppCodeGenWriteBarrier((&___gcFDOBoqpaBweHMwNlbneFOEEAm_5), value);
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_6() { return static_cast<int32_t>(offsetof(Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_6)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_6() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_6; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_6() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_6; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_6(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T55D35599A6B182030EA05B6CA5AB494E24ACA2E5_H
#ifndef CONTROLLERELEMENTTARGET_T270C9829BE907AF87D4109ADF4BABEC4C35DA02B_H
#define CONTROLLERELEMENTTARGET_T270C9829BE907AF87D4109ADF4BABEC4C35DA02B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerElementTarget
struct  ControllerElementTarget_t270C9829BE907AF87D4109ADF4BABEC4C35DA02B 
{
public:
	// Rewired.Controller_Element Rewired.ControllerElementTarget::ZljBzOgnBdZbhDAoXUvCVQsaWkR
	Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 * ___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0;
	// Rewired.AxisRange Rewired.ControllerElementTarget::xTkhpClvSKvoiLfYNzcXIDURAPx
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_1;

public:
	inline static int32_t get_offset_of_ZljBzOgnBdZbhDAoXUvCVQsaWkR_0() { return static_cast<int32_t>(offsetof(ControllerElementTarget_t270C9829BE907AF87D4109ADF4BABEC4C35DA02B, ___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0)); }
	inline Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 * get_ZljBzOgnBdZbhDAoXUvCVQsaWkR_0() const { return ___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0; }
	inline Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 ** get_address_of_ZljBzOgnBdZbhDAoXUvCVQsaWkR_0() { return &___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0; }
	inline void set_ZljBzOgnBdZbhDAoXUvCVQsaWkR_0(Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 * value)
	{
		___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0 = value;
		Il2CppCodeGenWriteBarrier((&___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0), value);
	}

	inline static int32_t get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_1() { return static_cast<int32_t>(offsetof(ControllerElementTarget_t270C9829BE907AF87D4109ADF4BABEC4C35DA02B, ___xTkhpClvSKvoiLfYNzcXIDURAPx_1)); }
	inline int32_t get_xTkhpClvSKvoiLfYNzcXIDURAPx_1() const { return ___xTkhpClvSKvoiLfYNzcXIDURAPx_1; }
	inline int32_t* get_address_of_xTkhpClvSKvoiLfYNzcXIDURAPx_1() { return &___xTkhpClvSKvoiLfYNzcXIDURAPx_1; }
	inline void set_xTkhpClvSKvoiLfYNzcXIDURAPx_1(int32_t value)
	{
		___xTkhpClvSKvoiLfYNzcXIDURAPx_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ControllerElementTarget
struct ControllerElementTarget_t270C9829BE907AF87D4109ADF4BABEC4C35DA02B_marshaled_pinvoke
{
	Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 * ___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_1;
};
// Native definition for COM marshalling of Rewired.ControllerElementTarget
struct ControllerElementTarget_t270C9829BE907AF87D4109ADF4BABEC4C35DA02B_marshaled_com
{
	Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5 * ___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_1;
};
#endif // CONTROLLERELEMENTTARGET_T270C9829BE907AF87D4109ADF4BABEC4C35DA02B_H
#ifndef CONTROLLERIDENTIFIER_T725D2A7A9387931F93131874C32B08A3BCFF9DD6_H
#define CONTROLLERIDENTIFIER_T725D2A7A9387931F93131874C32B08A3BCFF9DD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerIdentifier
struct  ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6 
{
public:
	// System.Int32 Rewired.ControllerIdentifier::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_0;
	// Rewired.ControllerType Rewired.ControllerIdentifier::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1;
	// System.Guid Rewired.ControllerIdentifier::hVfvAlYZzngYWNUKHfQeqsdsSsN
	Guid_t  ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2;
	// System.String Rewired.ControllerIdentifier::fGtUJMoDYGQBkIHcDCTnfmUopvdN
	String_t* ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3;
	// System.Guid Rewired.ControllerIdentifier::uoQfWOPIJljmoZOdOiUDjMDpAEI
	Guid_t  ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4;

public:
	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_0() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___CdiTZueJOweHxLVLesdWcZkZxNV_0)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_0() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_0; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_0() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_0; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_0(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_0 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_1(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1 = value;
	}

	inline static int32_t get_offset_of_hVfvAlYZzngYWNUKHfQeqsdsSsN_2() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2)); }
	inline Guid_t  get_hVfvAlYZzngYWNUKHfQeqsdsSsN_2() const { return ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2; }
	inline Guid_t * get_address_of_hVfvAlYZzngYWNUKHfQeqsdsSsN_2() { return &___hVfvAlYZzngYWNUKHfQeqsdsSsN_2; }
	inline void set_hVfvAlYZzngYWNUKHfQeqsdsSsN_2(Guid_t  value)
	{
		___hVfvAlYZzngYWNUKHfQeqsdsSsN_2 = value;
	}

	inline static int32_t get_offset_of_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3)); }
	inline String_t* get_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3() const { return ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3; }
	inline String_t** get_address_of_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3() { return &___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3; }
	inline void set_fGtUJMoDYGQBkIHcDCTnfmUopvdN_3(String_t* value)
	{
		___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3 = value;
		Il2CppCodeGenWriteBarrier((&___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3), value);
	}

	inline static int32_t get_offset_of_uoQfWOPIJljmoZOdOiUDjMDpAEI_4() { return static_cast<int32_t>(offsetof(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6, ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4)); }
	inline Guid_t  get_uoQfWOPIJljmoZOdOiUDjMDpAEI_4() const { return ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4; }
	inline Guid_t * get_address_of_uoQfWOPIJljmoZOdOiUDjMDpAEI_4() { return &___uoQfWOPIJljmoZOdOiUDjMDpAEI_4; }
	inline void set_uoQfWOPIJljmoZOdOiUDjMDpAEI_4(Guid_t  value)
	{
		___uoQfWOPIJljmoZOdOiUDjMDpAEI_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ControllerIdentifier
struct ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6_marshaled_pinvoke
{
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_0;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1;
	Guid_t  ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2;
	char* ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3;
	Guid_t  ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4;
};
// Native definition for COM marshalling of Rewired.ControllerIdentifier
struct ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6_marshaled_com
{
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_0;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_1;
	Guid_t  ___hVfvAlYZzngYWNUKHfQeqsdsSsN_2;
	Il2CppChar* ___fGtUJMoDYGQBkIHcDCTnfmUopvdN_3;
	Guid_t  ___uoQfWOPIJljmoZOdOiUDjMDpAEI_4;
};
#endif // CONTROLLERIDENTIFIER_T725D2A7A9387931F93131874C32B08A3BCFF9DD6_H
#ifndef CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#define CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerPollingInfo
struct  ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 
{
public:
	// System.Boolean Rewired.ControllerPollingInfo::XxREeDCHebPEeYaAATiXyFbnbIU
	bool ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	// System.Int32 Rewired.ControllerPollingInfo::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	// System.Int32 Rewired.ControllerPollingInfo::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	// System.String Rewired.ControllerPollingInfo::tUeBKTuOCBEsklhkCXrBJHMCCni
	String_t* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	// Rewired.ControllerType Rewired.ControllerPollingInfo::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	// Rewired.ControllerElementType Rewired.ControllerPollingInfo::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	// System.Int32 Rewired.ControllerPollingInfo::JcIOHtlyyDcisHJclHvGjyIzQHK
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	// Rewired.Pole Rewired.ControllerPollingInfo::gOvKiYbtKZOeaPYXdsADyDYKhyR
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	// System.String Rewired.ControllerPollingInfo::BpnqUtABZEySqFzPQqIktIgZaUw
	String_t* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	// System.Int32 Rewired.ControllerPollingInfo::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	// UnityEngine.KeyCode Rewired.ControllerPollingInfo::UBcIxoTNwLQLduWHPwWDYgtuvif
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;

public:
	inline static int32_t get_offset_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___XxREeDCHebPEeYaAATiXyFbnbIU_0)); }
	inline bool get_XxREeDCHebPEeYaAATiXyFbnbIU_0() const { return ___XxREeDCHebPEeYaAATiXyFbnbIU_0; }
	inline bool* get_address_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() { return &___XxREeDCHebPEeYaAATiXyFbnbIU_0; }
	inline void set_XxREeDCHebPEeYaAATiXyFbnbIU_0(bool value)
	{
		___XxREeDCHebPEeYaAATiXyFbnbIU_0 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___FCIziTxDWAnyHATWwZeMEmiXvdc_1)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_1() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_1(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_1 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___CdiTZueJOweHxLVLesdWcZkZxNV_2)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_2() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_2(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_2 = value;
	}

	inline static int32_t get_offset_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___tUeBKTuOCBEsklhkCXrBJHMCCni_3)); }
	inline String_t* get_tUeBKTuOCBEsklhkCXrBJHMCCni_3() const { return ___tUeBKTuOCBEsklhkCXrBJHMCCni_3; }
	inline String_t** get_address_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() { return &___tUeBKTuOCBEsklhkCXrBJHMCCni_3; }
	inline void set_tUeBKTuOCBEsklhkCXrBJHMCCni_3(String_t* value)
	{
		___tUeBKTuOCBEsklhkCXrBJHMCCni_3 = value;
		Il2CppCodeGenWriteBarrier((&___tUeBKTuOCBEsklhkCXrBJHMCCni_3), value);
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4 = value;
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___RmmmEkVblcqxoqGYhifgdSESkSn_5)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_5() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_5; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_5; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_5(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_5 = value;
	}

	inline static int32_t get_offset_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___JcIOHtlyyDcisHJclHvGjyIzQHK_6)); }
	inline int32_t get_JcIOHtlyyDcisHJclHvGjyIzQHK_6() const { return ___JcIOHtlyyDcisHJclHvGjyIzQHK_6; }
	inline int32_t* get_address_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() { return &___JcIOHtlyyDcisHJclHvGjyIzQHK_6; }
	inline void set_JcIOHtlyyDcisHJclHvGjyIzQHK_6(int32_t value)
	{
		___JcIOHtlyyDcisHJclHvGjyIzQHK_6 = value;
	}

	inline static int32_t get_offset_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7)); }
	inline int32_t get_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() const { return ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7; }
	inline int32_t* get_address_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() { return &___gOvKiYbtKZOeaPYXdsADyDYKhyR_7; }
	inline void set_gOvKiYbtKZOeaPYXdsADyDYKhyR_7(int32_t value)
	{
		___gOvKiYbtKZOeaPYXdsADyDYKhyR_7 = value;
	}

	inline static int32_t get_offset_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___BpnqUtABZEySqFzPQqIktIgZaUw_8)); }
	inline String_t* get_BpnqUtABZEySqFzPQqIktIgZaUw_8() const { return ___BpnqUtABZEySqFzPQqIktIgZaUw_8; }
	inline String_t** get_address_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() { return &___BpnqUtABZEySqFzPQqIktIgZaUw_8; }
	inline void set_BpnqUtABZEySqFzPQqIktIgZaUw_8(String_t* value)
	{
		___BpnqUtABZEySqFzPQqIktIgZaUw_8 = value;
		Il2CppCodeGenWriteBarrier((&___BpnqUtABZEySqFzPQqIktIgZaUw_8), value);
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___FrsWYgeIWMnjOcXDysagwZGcCMF_9)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_9() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_9; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_9; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_9(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_9 = value;
	}

	inline static int32_t get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___UBcIxoTNwLQLduWHPwWDYgtuvif_10)); }
	inline int32_t get_UBcIxoTNwLQLduWHPwWDYgtuvif_10() const { return ___UBcIxoTNwLQLduWHPwWDYgtuvif_10; }
	inline int32_t* get_address_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() { return &___UBcIxoTNwLQLduWHPwWDYgtuvif_10; }
	inline void set_UBcIxoTNwLQLduWHPwWDYgtuvif_10(int32_t value)
	{
		___UBcIxoTNwLQLduWHPwWDYgtuvif_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ControllerPollingInfo
struct ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_pinvoke
{
	int32_t ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	char* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	char* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;
};
// Native definition for COM marshalling of Rewired.ControllerPollingInfo
struct ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_com
{
	int32_t ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	Il2CppChar* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	Il2CppChar* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;
};
#endif // CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#ifndef HKUFFRDZSIYCBEXTWGHFCJFWHWW_TE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361_H
#define HKUFFRDZSIYCBEXTWGHFCJFWHWW_TE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_hkuffRDZsiyCBEXTWgHFcjFwHWW
struct  hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361  : public RuntimeObject
{
public:
	// Rewired.IControllerTemplate Rewired.ControllerTemplate_hkuffRDZsiyCBEXTWgHFcjFwHWW::waEhkaNJermXjlpTevBkHdlvdCD
	RuntimeObject* ___waEhkaNJermXjlpTevBkHdlvdCD_0;
	// System.Int32 Rewired.ControllerTemplate_hkuffRDZsiyCBEXTWgHFcjFwHWW::WHCIpuTfwdHNjfJipsoGkINikLt
	int32_t ___WHCIpuTfwdHNjfJipsoGkINikLt_1;
	// System.String Rewired.ControllerTemplate_hkuffRDZsiyCBEXTWgHFcjFwHWW::xzFIhZhMHNeQYGDUhfMfvvqdZQds
	String_t* ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_2;
	// Rewired.ControllerTemplateElementType Rewired.ControllerTemplate_hkuffRDZsiyCBEXTWgHFcjFwHWW::DzAgXbAsFcjwinjPLOBYLcFrhIr
	int32_t ___DzAgXbAsFcjwinjPLOBYLcFrhIr_3;
	// System.Int32 Rewired.ControllerTemplate_hkuffRDZsiyCBEXTWgHFcjFwHWW::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_4;

public:
	inline static int32_t get_offset_of_waEhkaNJermXjlpTevBkHdlvdCD_0() { return static_cast<int32_t>(offsetof(hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361, ___waEhkaNJermXjlpTevBkHdlvdCD_0)); }
	inline RuntimeObject* get_waEhkaNJermXjlpTevBkHdlvdCD_0() const { return ___waEhkaNJermXjlpTevBkHdlvdCD_0; }
	inline RuntimeObject** get_address_of_waEhkaNJermXjlpTevBkHdlvdCD_0() { return &___waEhkaNJermXjlpTevBkHdlvdCD_0; }
	inline void set_waEhkaNJermXjlpTevBkHdlvdCD_0(RuntimeObject* value)
	{
		___waEhkaNJermXjlpTevBkHdlvdCD_0 = value;
		Il2CppCodeGenWriteBarrier((&___waEhkaNJermXjlpTevBkHdlvdCD_0), value);
	}

	inline static int32_t get_offset_of_WHCIpuTfwdHNjfJipsoGkINikLt_1() { return static_cast<int32_t>(offsetof(hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361, ___WHCIpuTfwdHNjfJipsoGkINikLt_1)); }
	inline int32_t get_WHCIpuTfwdHNjfJipsoGkINikLt_1() const { return ___WHCIpuTfwdHNjfJipsoGkINikLt_1; }
	inline int32_t* get_address_of_WHCIpuTfwdHNjfJipsoGkINikLt_1() { return &___WHCIpuTfwdHNjfJipsoGkINikLt_1; }
	inline void set_WHCIpuTfwdHNjfJipsoGkINikLt_1(int32_t value)
	{
		___WHCIpuTfwdHNjfJipsoGkINikLt_1 = value;
	}

	inline static int32_t get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_2() { return static_cast<int32_t>(offsetof(hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361, ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_2)); }
	inline String_t* get_xzFIhZhMHNeQYGDUhfMfvvqdZQds_2() const { return ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_2; }
	inline String_t** get_address_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_2() { return &___xzFIhZhMHNeQYGDUhfMfvvqdZQds_2; }
	inline void set_xzFIhZhMHNeQYGDUhfMfvvqdZQds_2(String_t* value)
	{
		___xzFIhZhMHNeQYGDUhfMfvvqdZQds_2 = value;
		Il2CppCodeGenWriteBarrier((&___xzFIhZhMHNeQYGDUhfMfvvqdZQds_2), value);
	}

	inline static int32_t get_offset_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_3() { return static_cast<int32_t>(offsetof(hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361, ___DzAgXbAsFcjwinjPLOBYLcFrhIr_3)); }
	inline int32_t get_DzAgXbAsFcjwinjPLOBYLcFrhIr_3() const { return ___DzAgXbAsFcjwinjPLOBYLcFrhIr_3; }
	inline int32_t* get_address_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_3() { return &___DzAgXbAsFcjwinjPLOBYLcFrhIr_3; }
	inline void set_DzAgXbAsFcjwinjPLOBYLcFrhIr_3(int32_t value)
	{
		___DzAgXbAsFcjwinjPLOBYLcFrhIr_3 = value;
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_4() { return static_cast<int32_t>(offsetof(hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_4)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_4() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_4; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_4() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_4; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_4(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HKUFFRDZSIYCBEXTWGHFCJFWHWW_TE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361_H
#ifndef CONTROLLERTEMPLATEACTIONELEMENTMAP_T1439E6E2414F53444D2DE3350DD2B9CAFFF26F49_H
#define CONTROLLERTEMPLATEACTIONELEMENTMAP_T1439E6E2414F53444D2DE3350DD2B9CAFFF26F49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplateActionElementMap
struct  ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49  : public RuntimeObject
{
public:
	// System.Int32 Rewired.ControllerTemplateActionElementMap::WHCIpuTfwdHNjfJipsoGkINikLt
	int32_t ___WHCIpuTfwdHNjfJipsoGkINikLt_0;
	// Rewired.ControllerTemplateElementType Rewired.ControllerTemplateActionElementMap::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_1;
	// System.Boolean Rewired.ControllerTemplateActionElementMap::eWTjKkaJVjCZqrsQHQXMtJYuNhD
	bool ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_2;
	// System.Int32 Rewired.ControllerTemplateActionElementMap::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_3;
	// System.Int32 Rewired.ControllerTemplateActionElementMap::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_4;

public:
	inline static int32_t get_offset_of_WHCIpuTfwdHNjfJipsoGkINikLt_0() { return static_cast<int32_t>(offsetof(ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49, ___WHCIpuTfwdHNjfJipsoGkINikLt_0)); }
	inline int32_t get_WHCIpuTfwdHNjfJipsoGkINikLt_0() const { return ___WHCIpuTfwdHNjfJipsoGkINikLt_0; }
	inline int32_t* get_address_of_WHCIpuTfwdHNjfJipsoGkINikLt_0() { return &___WHCIpuTfwdHNjfJipsoGkINikLt_0; }
	inline void set_WHCIpuTfwdHNjfJipsoGkINikLt_0(int32_t value)
	{
		___WHCIpuTfwdHNjfJipsoGkINikLt_0 = value;
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_1() { return static_cast<int32_t>(offsetof(ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49, ___RmmmEkVblcqxoqGYhifgdSESkSn_1)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_1() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_1; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_1() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_1; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_1(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_1 = value;
	}

	inline static int32_t get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_2() { return static_cast<int32_t>(offsetof(ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49, ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_2)); }
	inline bool get_eWTjKkaJVjCZqrsQHQXMtJYuNhD_2() const { return ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_2; }
	inline bool* get_address_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_2() { return &___eWTjKkaJVjCZqrsQHQXMtJYuNhD_2; }
	inline void set_eWTjKkaJVjCZqrsQHQXMtJYuNhD_2(bool value)
	{
		___eWTjKkaJVjCZqrsQHQXMtJYuNhD_2 = value;
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_3() { return static_cast<int32_t>(offsetof(ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49, ___LmADKPFcmVAiabETiHqQTGSgvmcg_3)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_3() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_3; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_3() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_3; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_3(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_3 = value;
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_4() { return static_cast<int32_t>(offsetof(ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49, ___FrsWYgeIWMnjOcXDysagwZGcCMF_4)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_4() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_4; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_4() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_4; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_4(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_4 = value;
	}
};

struct ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49_StaticFields
{
public:
	// System.Int32 Rewired.ControllerTemplateActionElementMap::vzbvBVwssABPkQFVkXwztQaEVuB
	int32_t ___vzbvBVwssABPkQFVkXwztQaEVuB_5;

public:
	inline static int32_t get_offset_of_vzbvBVwssABPkQFVkXwztQaEVuB_5() { return static_cast<int32_t>(offsetof(ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49_StaticFields, ___vzbvBVwssABPkQFVkXwztQaEVuB_5)); }
	inline int32_t get_vzbvBVwssABPkQFVkXwztQaEVuB_5() const { return ___vzbvBVwssABPkQFVkXwztQaEVuB_5; }
	inline int32_t* get_address_of_vzbvBVwssABPkQFVkXwztQaEVuB_5() { return &___vzbvBVwssABPkQFVkXwztQaEVuB_5; }
	inline void set_vzbvBVwssABPkQFVkXwztQaEVuB_5(int32_t value)
	{
		___vzbvBVwssABPkQFVkXwztQaEVuB_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEACTIONELEMENTMAP_T1439E6E2414F53444D2DE3350DD2B9CAFFF26F49_H
#ifndef CONTROLLERTEMPLATEELEMENTTARGET_T4E640D6AEA752EBCA12DCEDB931EE315E8E8B676_H
#define CONTROLLERTEMPLATEELEMENTTARGET_T4E640D6AEA752EBCA12DCEDB931EE315E8E8B676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplateElementTarget
struct  ControllerTemplateElementTarget_t4E640D6AEA752EBCA12DCEDB931EE315E8E8B676 
{
public:
	// Rewired.IControllerTemplateElement Rewired.ControllerTemplateElementTarget::ZljBzOgnBdZbhDAoXUvCVQsaWkR
	RuntimeObject* ___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0;
	// Rewired.AxisRange Rewired.ControllerTemplateElementTarget::xTkhpClvSKvoiLfYNzcXIDURAPx
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_1;

public:
	inline static int32_t get_offset_of_ZljBzOgnBdZbhDAoXUvCVQsaWkR_0() { return static_cast<int32_t>(offsetof(ControllerTemplateElementTarget_t4E640D6AEA752EBCA12DCEDB931EE315E8E8B676, ___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0)); }
	inline RuntimeObject* get_ZljBzOgnBdZbhDAoXUvCVQsaWkR_0() const { return ___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0; }
	inline RuntimeObject** get_address_of_ZljBzOgnBdZbhDAoXUvCVQsaWkR_0() { return &___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0; }
	inline void set_ZljBzOgnBdZbhDAoXUvCVQsaWkR_0(RuntimeObject* value)
	{
		___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0 = value;
		Il2CppCodeGenWriteBarrier((&___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0), value);
	}

	inline static int32_t get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_1() { return static_cast<int32_t>(offsetof(ControllerTemplateElementTarget_t4E640D6AEA752EBCA12DCEDB931EE315E8E8B676, ___xTkhpClvSKvoiLfYNzcXIDURAPx_1)); }
	inline int32_t get_xTkhpClvSKvoiLfYNzcXIDURAPx_1() const { return ___xTkhpClvSKvoiLfYNzcXIDURAPx_1; }
	inline int32_t* get_address_of_xTkhpClvSKvoiLfYNzcXIDURAPx_1() { return &___xTkhpClvSKvoiLfYNzcXIDURAPx_1; }
	inline void set_xTkhpClvSKvoiLfYNzcXIDURAPx_1(int32_t value)
	{
		___xTkhpClvSKvoiLfYNzcXIDURAPx_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ControllerTemplateElementTarget
struct ControllerTemplateElementTarget_t4E640D6AEA752EBCA12DCEDB931EE315E8E8B676_marshaled_pinvoke
{
	RuntimeObject* ___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_1;
};
// Native definition for COM marshalling of Rewired.ControllerTemplateElementTarget
struct ControllerTemplateElementTarget_t4E640D6AEA752EBCA12DCEDB931EE315E8E8B676_marshaled_com
{
	RuntimeObject* ___ZljBzOgnBdZbhDAoXUvCVQsaWkR_0;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_1;
};
#endif // CONTROLLERTEMPLATEELEMENTTARGET_T4E640D6AEA752EBCA12DCEDB931EE315E8E8B676_H
#ifndef AGYFXXKUAODBMLIRWLTXLMJYFHG_T0B004A1E1C048628567A7FE3526B5BC0557CC197_H
#define AGYFXXKUAODBMLIRWLTXLMJYFHG_T0B004A1E1C048628567A7FE3526B5BC0557CC197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// agYfxXkuAOdBmliRwltxLMjyFHG
struct  agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197  : public RuntimeObject
{
public:
	// Rewired.Controller agYfxXkuAOdBmliRwltxLMjyFHG::gcFDOBoqpaBweHMwNlbneFOEEAm
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___gcFDOBoqpaBweHMwNlbneFOEEAm_1;
	// System.Int32 agYfxXkuAOdBmliRwltxLMjyFHG::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_2;
	// Rewired.AxisRange agYfxXkuAOdBmliRwltxLMjyFHG::xTkhpClvSKvoiLfYNzcXIDURAPx
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_3;
	// Rewired.Utils.Classes.Utility.IObjectPool agYfxXkuAOdBmliRwltxLMjyFHG::PavQAlUTQJCXgnaaIZJqMrVxxkr
	RuntimeObject* ___PavQAlUTQJCXgnaaIZJqMrVxxkr_4;
	// System.Boolean agYfxXkuAOdBmliRwltxLMjyFHG::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_5;

public:
	inline static int32_t get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_1() { return static_cast<int32_t>(offsetof(agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197, ___gcFDOBoqpaBweHMwNlbneFOEEAm_1)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_gcFDOBoqpaBweHMwNlbneFOEEAm_1() const { return ___gcFDOBoqpaBweHMwNlbneFOEEAm_1; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_gcFDOBoqpaBweHMwNlbneFOEEAm_1() { return &___gcFDOBoqpaBweHMwNlbneFOEEAm_1; }
	inline void set_gcFDOBoqpaBweHMwNlbneFOEEAm_1(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___gcFDOBoqpaBweHMwNlbneFOEEAm_1 = value;
		Il2CppCodeGenWriteBarrier((&___gcFDOBoqpaBweHMwNlbneFOEEAm_1), value);
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_2() { return static_cast<int32_t>(offsetof(agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197, ___FrsWYgeIWMnjOcXDysagwZGcCMF_2)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_2() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_2; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_2() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_2; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_2(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_2 = value;
	}

	inline static int32_t get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_3() { return static_cast<int32_t>(offsetof(agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197, ___xTkhpClvSKvoiLfYNzcXIDURAPx_3)); }
	inline int32_t get_xTkhpClvSKvoiLfYNzcXIDURAPx_3() const { return ___xTkhpClvSKvoiLfYNzcXIDURAPx_3; }
	inline int32_t* get_address_of_xTkhpClvSKvoiLfYNzcXIDURAPx_3() { return &___xTkhpClvSKvoiLfYNzcXIDURAPx_3; }
	inline void set_xTkhpClvSKvoiLfYNzcXIDURAPx_3(int32_t value)
	{
		___xTkhpClvSKvoiLfYNzcXIDURAPx_3 = value;
	}

	inline static int32_t get_offset_of_PavQAlUTQJCXgnaaIZJqMrVxxkr_4() { return static_cast<int32_t>(offsetof(agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197, ___PavQAlUTQJCXgnaaIZJqMrVxxkr_4)); }
	inline RuntimeObject* get_PavQAlUTQJCXgnaaIZJqMrVxxkr_4() const { return ___PavQAlUTQJCXgnaaIZJqMrVxxkr_4; }
	inline RuntimeObject** get_address_of_PavQAlUTQJCXgnaaIZJqMrVxxkr_4() { return &___PavQAlUTQJCXgnaaIZJqMrVxxkr_4; }
	inline void set_PavQAlUTQJCXgnaaIZJqMrVxxkr_4(RuntimeObject* value)
	{
		___PavQAlUTQJCXgnaaIZJqMrVxxkr_4 = value;
		Il2CppCodeGenWriteBarrier((&___PavQAlUTQJCXgnaaIZJqMrVxxkr_4), value);
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_5() { return static_cast<int32_t>(offsetof(agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197, ___SeCUoinDywZmqZDHRKupOdOaTke_5)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_5() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_5; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_5() { return &___SeCUoinDywZmqZDHRKupOdOaTke_5; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_5(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_5 = value;
	}
};

struct agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197_StaticFields
{
public:
	// Rewired.Utils.Classes.Utility.ObjectPool`1<agYfxXkuAOdBmliRwltxLMjyFHG> agYfxXkuAOdBmliRwltxLMjyFHG::JEJCOBMKlXtzWWwDefwoYuLjvRi
	ObjectPool_1_tB6807F662FF447933F0831D0B781A0C387428B91 * ___JEJCOBMKlXtzWWwDefwoYuLjvRi_0;
	// System.Func`1<agYfxXkuAOdBmliRwltxLMjyFHG> agYfxXkuAOdBmliRwltxLMjyFHG::wKZsRNpOhQctCfJnvfAdTetobSIx
	Func_1_t11F5484DB54AD62A37F5BAC05DCF931BAE388545 * ___wKZsRNpOhQctCfJnvfAdTetobSIx_6;

public:
	inline static int32_t get_offset_of_JEJCOBMKlXtzWWwDefwoYuLjvRi_0() { return static_cast<int32_t>(offsetof(agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197_StaticFields, ___JEJCOBMKlXtzWWwDefwoYuLjvRi_0)); }
	inline ObjectPool_1_tB6807F662FF447933F0831D0B781A0C387428B91 * get_JEJCOBMKlXtzWWwDefwoYuLjvRi_0() const { return ___JEJCOBMKlXtzWWwDefwoYuLjvRi_0; }
	inline ObjectPool_1_tB6807F662FF447933F0831D0B781A0C387428B91 ** get_address_of_JEJCOBMKlXtzWWwDefwoYuLjvRi_0() { return &___JEJCOBMKlXtzWWwDefwoYuLjvRi_0; }
	inline void set_JEJCOBMKlXtzWWwDefwoYuLjvRi_0(ObjectPool_1_tB6807F662FF447933F0831D0B781A0C387428B91 * value)
	{
		___JEJCOBMKlXtzWWwDefwoYuLjvRi_0 = value;
		Il2CppCodeGenWriteBarrier((&___JEJCOBMKlXtzWWwDefwoYuLjvRi_0), value);
	}

	inline static int32_t get_offset_of_wKZsRNpOhQctCfJnvfAdTetobSIx_6() { return static_cast<int32_t>(offsetof(agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197_StaticFields, ___wKZsRNpOhQctCfJnvfAdTetobSIx_6)); }
	inline Func_1_t11F5484DB54AD62A37F5BAC05DCF931BAE388545 * get_wKZsRNpOhQctCfJnvfAdTetobSIx_6() const { return ___wKZsRNpOhQctCfJnvfAdTetobSIx_6; }
	inline Func_1_t11F5484DB54AD62A37F5BAC05DCF931BAE388545 ** get_address_of_wKZsRNpOhQctCfJnvfAdTetobSIx_6() { return &___wKZsRNpOhQctCfJnvfAdTetobSIx_6; }
	inline void set_wKZsRNpOhQctCfJnvfAdTetobSIx_6(Func_1_t11F5484DB54AD62A37F5BAC05DCF931BAE388545 * value)
	{
		___wKZsRNpOhQctCfJnvfAdTetobSIx_6 = value;
		Il2CppCodeGenWriteBarrier((&___wKZsRNpOhQctCfJnvfAdTetobSIx_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGYFXXKUAODBMLIRWLTXLMJYFHG_T0B004A1E1C048628567A7FE3526B5BC0557CC197_H
#ifndef CONTROLLER_T5780C278CCFB7A81A0272F3B056B1EF463D9B671_H
#define CONTROLLER_T5780C278CCFB7A81A0272F3B056B1EF463D9B671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller
struct  Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Controller::id
	int32_t ___id_0;
	// System.String Rewired.Controller::_tag
	String_t* ____tag_1;
	// System.String Rewired.Controller::_name
	String_t* ____name_2;
	// System.String Rewired.Controller::_hardwareName
	String_t* ____hardwareName_3;
	// Rewired.ControllerType Rewired.Controller::_type
	int32_t ____type_4;
	// System.Guid Rewired.Controller::hVfvAlYZzngYWNUKHfQeqsdsSsN
	Guid_t  ___hVfvAlYZzngYWNUKHfQeqsdsSsN_5;
	// System.String Rewired.Controller::_hardwareIdentifier
	String_t* ____hardwareIdentifier_6;
	// System.Boolean Rewired.Controller::_isConnected
	bool ____isConnected_7;
	// Rewired.Controller_Extension Rewired.Controller::keBYOCobzoABWhYylPhjApVSLXnN
	Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * ___keBYOCobzoABWhYylPhjApVSLXnN_8;
	// System.Boolean Rewired.Controller::eWTjKkaJVjCZqrsQHQXMtJYuNhD
	bool ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_9;
	// Rewired.ControllerIdentifier Rewired.Controller::VlCVoudhBiuwhdYzYzMjmHCukZv
	ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6  ___VlCVoudhBiuwhdYzYzMjmHCukZv_10;
	// System.Int32 Rewired.Controller::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_11;
	// System.Int32 Rewired.Controller::_buttonCount
	int32_t ____buttonCount_12;
	// Rewired.Controller_Button[] Rewired.Controller::buttons
	ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C* ___buttons_13;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller_Button> Rewired.Controller::buttons_readOnly
	ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383 * ___buttons_readOnly_14;
	// System.Collections.Generic.IList`1<Rewired.Controller_Element> Rewired.Controller::tqrCjCDiVROjgZQMTxdrZcCDZcA
	RuntimeObject* ___tqrCjCDiVROjgZQMTxdrZcCDZcA_15;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller_Element> Rewired.Controller::HeiaDLCywuXsOrgGQhDKAJyFIubw
	ReadOnlyCollection_1_tBF1621D3F7F9871C04D918E0DF726F42C36F91F4 * ___HeiaDLCywuXsOrgGQhDKAJyFIubw_16;
	// Rewired.InputSource Rewired.Controller::lHdmDauerBwdDwLUcaCxBythtdWu
	int32_t ___lHdmDauerBwdDwLUcaCxBythtdWu_17;
	// Rewired.ControllerDataUpdater Rewired.Controller::NYtqDwOoerMRNEOsCUlBWwihMNG
	ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F * ___NYtqDwOoerMRNEOsCUlBWwihMNG_18;
	// Rewired.HardwareControllerMap_Game Rewired.Controller::motlnrXwRclwbbdquGWkOEYsYFy
	HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * ___motlnrXwRclwbbdquGWkOEYsYFy_19;
	// System.UInt32 Rewired.Controller::chxPGnvGYohtyammAxrWAboQuVg
	uint32_t ___chxPGnvGYohtyammAxrWAboQuVg_20;
	// System.UInt32 Rewired.Controller::iaxnzSzdijUTJnJzreKDdKvgbmk
	uint32_t ___iaxnzSzdijUTJnJzreKDdKvgbmk_21;
	// System.UInt32 Rewired.Controller::lMdEFUXdOFLrujmaUFFpIWQxdWyh
	uint32_t ___lMdEFUXdOFLrujmaUFFpIWQxdWyh_22;
	// System.Action`1<System.Boolean> Rewired.Controller::yQeGjgudbMdGxktFDlTRKRPYNpwO
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___yQeGjgudbMdGxktFDlTRKRPYNpwO_23;
	// Rewired.IControllerTemplate[] Rewired.Controller::RrVywEyDCOVEzMNbWtqScfAjGSfA
	IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6* ___RrVywEyDCOVEzMNbWtqScfAjGSfA_24;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.IControllerTemplate> Rewired.Controller::PWJCQVymGuHZlwtENLzIkOlXzFO
	ReadOnlyCollection_1_t29628C2D08376268FE32FB6C6D7854BE606C1802 * ___PWJCQVymGuHZlwtENLzIkOlXzFO_25;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of__tag_1() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____tag_1)); }
	inline String_t* get__tag_1() const { return ____tag_1; }
	inline String_t** get_address_of__tag_1() { return &____tag_1; }
	inline void set__tag_1(String_t* value)
	{
		____tag_1 = value;
		Il2CppCodeGenWriteBarrier((&____tag_1), value);
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}

	inline static int32_t get_offset_of__hardwareName_3() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____hardwareName_3)); }
	inline String_t* get__hardwareName_3() const { return ____hardwareName_3; }
	inline String_t** get_address_of__hardwareName_3() { return &____hardwareName_3; }
	inline void set__hardwareName_3(String_t* value)
	{
		____hardwareName_3 = value;
		Il2CppCodeGenWriteBarrier((&____hardwareName_3), value);
	}

	inline static int32_t get_offset_of__type_4() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____type_4)); }
	inline int32_t get__type_4() const { return ____type_4; }
	inline int32_t* get_address_of__type_4() { return &____type_4; }
	inline void set__type_4(int32_t value)
	{
		____type_4 = value;
	}

	inline static int32_t get_offset_of_hVfvAlYZzngYWNUKHfQeqsdsSsN_5() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___hVfvAlYZzngYWNUKHfQeqsdsSsN_5)); }
	inline Guid_t  get_hVfvAlYZzngYWNUKHfQeqsdsSsN_5() const { return ___hVfvAlYZzngYWNUKHfQeqsdsSsN_5; }
	inline Guid_t * get_address_of_hVfvAlYZzngYWNUKHfQeqsdsSsN_5() { return &___hVfvAlYZzngYWNUKHfQeqsdsSsN_5; }
	inline void set_hVfvAlYZzngYWNUKHfQeqsdsSsN_5(Guid_t  value)
	{
		___hVfvAlYZzngYWNUKHfQeqsdsSsN_5 = value;
	}

	inline static int32_t get_offset_of__hardwareIdentifier_6() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____hardwareIdentifier_6)); }
	inline String_t* get__hardwareIdentifier_6() const { return ____hardwareIdentifier_6; }
	inline String_t** get_address_of__hardwareIdentifier_6() { return &____hardwareIdentifier_6; }
	inline void set__hardwareIdentifier_6(String_t* value)
	{
		____hardwareIdentifier_6 = value;
		Il2CppCodeGenWriteBarrier((&____hardwareIdentifier_6), value);
	}

	inline static int32_t get_offset_of__isConnected_7() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____isConnected_7)); }
	inline bool get__isConnected_7() const { return ____isConnected_7; }
	inline bool* get_address_of__isConnected_7() { return &____isConnected_7; }
	inline void set__isConnected_7(bool value)
	{
		____isConnected_7 = value;
	}

	inline static int32_t get_offset_of_keBYOCobzoABWhYylPhjApVSLXnN_8() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___keBYOCobzoABWhYylPhjApVSLXnN_8)); }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * get_keBYOCobzoABWhYylPhjApVSLXnN_8() const { return ___keBYOCobzoABWhYylPhjApVSLXnN_8; }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD ** get_address_of_keBYOCobzoABWhYylPhjApVSLXnN_8() { return &___keBYOCobzoABWhYylPhjApVSLXnN_8; }
	inline void set_keBYOCobzoABWhYylPhjApVSLXnN_8(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * value)
	{
		___keBYOCobzoABWhYylPhjApVSLXnN_8 = value;
		Il2CppCodeGenWriteBarrier((&___keBYOCobzoABWhYylPhjApVSLXnN_8), value);
	}

	inline static int32_t get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_9() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_9)); }
	inline bool get_eWTjKkaJVjCZqrsQHQXMtJYuNhD_9() const { return ___eWTjKkaJVjCZqrsQHQXMtJYuNhD_9; }
	inline bool* get_address_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_9() { return &___eWTjKkaJVjCZqrsQHQXMtJYuNhD_9; }
	inline void set_eWTjKkaJVjCZqrsQHQXMtJYuNhD_9(bool value)
	{
		___eWTjKkaJVjCZqrsQHQXMtJYuNhD_9 = value;
	}

	inline static int32_t get_offset_of_VlCVoudhBiuwhdYzYzMjmHCukZv_10() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___VlCVoudhBiuwhdYzYzMjmHCukZv_10)); }
	inline ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6  get_VlCVoudhBiuwhdYzYzMjmHCukZv_10() const { return ___VlCVoudhBiuwhdYzYzMjmHCukZv_10; }
	inline ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6 * get_address_of_VlCVoudhBiuwhdYzYzMjmHCukZv_10() { return &___VlCVoudhBiuwhdYzYzMjmHCukZv_10; }
	inline void set_VlCVoudhBiuwhdYzYzMjmHCukZv_10(ControllerIdentifier_t725D2A7A9387931F93131874C32B08A3BCFF9DD6  value)
	{
		___VlCVoudhBiuwhdYzYzMjmHCukZv_10 = value;
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_11() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_11)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_11() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_11; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_11() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_11; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_11(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_11 = value;
	}

	inline static int32_t get_offset_of__buttonCount_12() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ____buttonCount_12)); }
	inline int32_t get__buttonCount_12() const { return ____buttonCount_12; }
	inline int32_t* get_address_of__buttonCount_12() { return &____buttonCount_12; }
	inline void set__buttonCount_12(int32_t value)
	{
		____buttonCount_12 = value;
	}

	inline static int32_t get_offset_of_buttons_13() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___buttons_13)); }
	inline ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C* get_buttons_13() const { return ___buttons_13; }
	inline ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C** get_address_of_buttons_13() { return &___buttons_13; }
	inline void set_buttons_13(ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C* value)
	{
		___buttons_13 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_13), value);
	}

	inline static int32_t get_offset_of_buttons_readOnly_14() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___buttons_readOnly_14)); }
	inline ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383 * get_buttons_readOnly_14() const { return ___buttons_readOnly_14; }
	inline ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383 ** get_address_of_buttons_readOnly_14() { return &___buttons_readOnly_14; }
	inline void set_buttons_readOnly_14(ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383 * value)
	{
		___buttons_readOnly_14 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_readOnly_14), value);
	}

	inline static int32_t get_offset_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_15() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___tqrCjCDiVROjgZQMTxdrZcCDZcA_15)); }
	inline RuntimeObject* get_tqrCjCDiVROjgZQMTxdrZcCDZcA_15() const { return ___tqrCjCDiVROjgZQMTxdrZcCDZcA_15; }
	inline RuntimeObject** get_address_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_15() { return &___tqrCjCDiVROjgZQMTxdrZcCDZcA_15; }
	inline void set_tqrCjCDiVROjgZQMTxdrZcCDZcA_15(RuntimeObject* value)
	{
		___tqrCjCDiVROjgZQMTxdrZcCDZcA_15 = value;
		Il2CppCodeGenWriteBarrier((&___tqrCjCDiVROjgZQMTxdrZcCDZcA_15), value);
	}

	inline static int32_t get_offset_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_16() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___HeiaDLCywuXsOrgGQhDKAJyFIubw_16)); }
	inline ReadOnlyCollection_1_tBF1621D3F7F9871C04D918E0DF726F42C36F91F4 * get_HeiaDLCywuXsOrgGQhDKAJyFIubw_16() const { return ___HeiaDLCywuXsOrgGQhDKAJyFIubw_16; }
	inline ReadOnlyCollection_1_tBF1621D3F7F9871C04D918E0DF726F42C36F91F4 ** get_address_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_16() { return &___HeiaDLCywuXsOrgGQhDKAJyFIubw_16; }
	inline void set_HeiaDLCywuXsOrgGQhDKAJyFIubw_16(ReadOnlyCollection_1_tBF1621D3F7F9871C04D918E0DF726F42C36F91F4 * value)
	{
		___HeiaDLCywuXsOrgGQhDKAJyFIubw_16 = value;
		Il2CppCodeGenWriteBarrier((&___HeiaDLCywuXsOrgGQhDKAJyFIubw_16), value);
	}

	inline static int32_t get_offset_of_lHdmDauerBwdDwLUcaCxBythtdWu_17() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___lHdmDauerBwdDwLUcaCxBythtdWu_17)); }
	inline int32_t get_lHdmDauerBwdDwLUcaCxBythtdWu_17() const { return ___lHdmDauerBwdDwLUcaCxBythtdWu_17; }
	inline int32_t* get_address_of_lHdmDauerBwdDwLUcaCxBythtdWu_17() { return &___lHdmDauerBwdDwLUcaCxBythtdWu_17; }
	inline void set_lHdmDauerBwdDwLUcaCxBythtdWu_17(int32_t value)
	{
		___lHdmDauerBwdDwLUcaCxBythtdWu_17 = value;
	}

	inline static int32_t get_offset_of_NYtqDwOoerMRNEOsCUlBWwihMNG_18() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___NYtqDwOoerMRNEOsCUlBWwihMNG_18)); }
	inline ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F * get_NYtqDwOoerMRNEOsCUlBWwihMNG_18() const { return ___NYtqDwOoerMRNEOsCUlBWwihMNG_18; }
	inline ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F ** get_address_of_NYtqDwOoerMRNEOsCUlBWwihMNG_18() { return &___NYtqDwOoerMRNEOsCUlBWwihMNG_18; }
	inline void set_NYtqDwOoerMRNEOsCUlBWwihMNG_18(ControllerDataUpdater_tBB4E0047FF8D6FAC9C5641EE94260D70C6DE1B9F * value)
	{
		___NYtqDwOoerMRNEOsCUlBWwihMNG_18 = value;
		Il2CppCodeGenWriteBarrier((&___NYtqDwOoerMRNEOsCUlBWwihMNG_18), value);
	}

	inline static int32_t get_offset_of_motlnrXwRclwbbdquGWkOEYsYFy_19() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___motlnrXwRclwbbdquGWkOEYsYFy_19)); }
	inline HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * get_motlnrXwRclwbbdquGWkOEYsYFy_19() const { return ___motlnrXwRclwbbdquGWkOEYsYFy_19; }
	inline HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 ** get_address_of_motlnrXwRclwbbdquGWkOEYsYFy_19() { return &___motlnrXwRclwbbdquGWkOEYsYFy_19; }
	inline void set_motlnrXwRclwbbdquGWkOEYsYFy_19(HardwareControllerMap_Game_t941EA366B3E6D843EBE08EEBA0930D8A338B49D3 * value)
	{
		___motlnrXwRclwbbdquGWkOEYsYFy_19 = value;
		Il2CppCodeGenWriteBarrier((&___motlnrXwRclwbbdquGWkOEYsYFy_19), value);
	}

	inline static int32_t get_offset_of_chxPGnvGYohtyammAxrWAboQuVg_20() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___chxPGnvGYohtyammAxrWAboQuVg_20)); }
	inline uint32_t get_chxPGnvGYohtyammAxrWAboQuVg_20() const { return ___chxPGnvGYohtyammAxrWAboQuVg_20; }
	inline uint32_t* get_address_of_chxPGnvGYohtyammAxrWAboQuVg_20() { return &___chxPGnvGYohtyammAxrWAboQuVg_20; }
	inline void set_chxPGnvGYohtyammAxrWAboQuVg_20(uint32_t value)
	{
		___chxPGnvGYohtyammAxrWAboQuVg_20 = value;
	}

	inline static int32_t get_offset_of_iaxnzSzdijUTJnJzreKDdKvgbmk_21() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___iaxnzSzdijUTJnJzreKDdKvgbmk_21)); }
	inline uint32_t get_iaxnzSzdijUTJnJzreKDdKvgbmk_21() const { return ___iaxnzSzdijUTJnJzreKDdKvgbmk_21; }
	inline uint32_t* get_address_of_iaxnzSzdijUTJnJzreKDdKvgbmk_21() { return &___iaxnzSzdijUTJnJzreKDdKvgbmk_21; }
	inline void set_iaxnzSzdijUTJnJzreKDdKvgbmk_21(uint32_t value)
	{
		___iaxnzSzdijUTJnJzreKDdKvgbmk_21 = value;
	}

	inline static int32_t get_offset_of_lMdEFUXdOFLrujmaUFFpIWQxdWyh_22() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___lMdEFUXdOFLrujmaUFFpIWQxdWyh_22)); }
	inline uint32_t get_lMdEFUXdOFLrujmaUFFpIWQxdWyh_22() const { return ___lMdEFUXdOFLrujmaUFFpIWQxdWyh_22; }
	inline uint32_t* get_address_of_lMdEFUXdOFLrujmaUFFpIWQxdWyh_22() { return &___lMdEFUXdOFLrujmaUFFpIWQxdWyh_22; }
	inline void set_lMdEFUXdOFLrujmaUFFpIWQxdWyh_22(uint32_t value)
	{
		___lMdEFUXdOFLrujmaUFFpIWQxdWyh_22 = value;
	}

	inline static int32_t get_offset_of_yQeGjgudbMdGxktFDlTRKRPYNpwO_23() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___yQeGjgudbMdGxktFDlTRKRPYNpwO_23)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_yQeGjgudbMdGxktFDlTRKRPYNpwO_23() const { return ___yQeGjgudbMdGxktFDlTRKRPYNpwO_23; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_yQeGjgudbMdGxktFDlTRKRPYNpwO_23() { return &___yQeGjgudbMdGxktFDlTRKRPYNpwO_23; }
	inline void set_yQeGjgudbMdGxktFDlTRKRPYNpwO_23(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___yQeGjgudbMdGxktFDlTRKRPYNpwO_23 = value;
		Il2CppCodeGenWriteBarrier((&___yQeGjgudbMdGxktFDlTRKRPYNpwO_23), value);
	}

	inline static int32_t get_offset_of_RrVywEyDCOVEzMNbWtqScfAjGSfA_24() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___RrVywEyDCOVEzMNbWtqScfAjGSfA_24)); }
	inline IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6* get_RrVywEyDCOVEzMNbWtqScfAjGSfA_24() const { return ___RrVywEyDCOVEzMNbWtqScfAjGSfA_24; }
	inline IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6** get_address_of_RrVywEyDCOVEzMNbWtqScfAjGSfA_24() { return &___RrVywEyDCOVEzMNbWtqScfAjGSfA_24; }
	inline void set_RrVywEyDCOVEzMNbWtqScfAjGSfA_24(IControllerTemplateU5BU5D_t6B3C97490FB9ECABF62EFD5CE8335DABC6E527B6* value)
	{
		___RrVywEyDCOVEzMNbWtqScfAjGSfA_24 = value;
		Il2CppCodeGenWriteBarrier((&___RrVywEyDCOVEzMNbWtqScfAjGSfA_24), value);
	}

	inline static int32_t get_offset_of_PWJCQVymGuHZlwtENLzIkOlXzFO_25() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671, ___PWJCQVymGuHZlwtENLzIkOlXzFO_25)); }
	inline ReadOnlyCollection_1_t29628C2D08376268FE32FB6C6D7854BE606C1802 * get_PWJCQVymGuHZlwtENLzIkOlXzFO_25() const { return ___PWJCQVymGuHZlwtENLzIkOlXzFO_25; }
	inline ReadOnlyCollection_1_t29628C2D08376268FE32FB6C6D7854BE606C1802 ** get_address_of_PWJCQVymGuHZlwtENLzIkOlXzFO_25() { return &___PWJCQVymGuHZlwtENLzIkOlXzFO_25; }
	inline void set_PWJCQVymGuHZlwtENLzIkOlXzFO_25(ReadOnlyCollection_1_t29628C2D08376268FE32FB6C6D7854BE606C1802 * value)
	{
		___PWJCQVymGuHZlwtENLzIkOlXzFO_25 = value;
		Il2CppCodeGenWriteBarrier((&___PWJCQVymGuHZlwtENLzIkOlXzFO_25), value);
	}
};

struct Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields
{
public:
	// System.Func`3<Rewired.Controller,System.Guid,System.Boolean> Rewired.Controller::kpwpfTuXUblRlYTjfSeSLEksNTd
	Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE * ___kpwpfTuXUblRlYTjfSeSLEksNTd_26;
	// System.Func`3<Rewired.Controller,System.Type,System.Boolean> Rewired.Controller::fpQFUcijUIunFTiZNbdIrMQqJvMf
	Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 * ___fpQFUcijUIunFTiZNbdIrMQqJvMf_27;
	// System.Func`3<Rewired.Controller,System.Guid,System.Boolean> Rewired.Controller::jqUTJMOeCBaPwenidONjsgqqmhF
	Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE * ___jqUTJMOeCBaPwenidONjsgqqmhF_28;
	// System.Func`3<Rewired.Controller,System.Type,System.Boolean> Rewired.Controller::yGfaFeDTgjQCiyfEnFKwcIrTEKK
	Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 * ___yGfaFeDTgjQCiyfEnFKwcIrTEKK_29;

public:
	inline static int32_t get_offset_of_kpwpfTuXUblRlYTjfSeSLEksNTd_26() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields, ___kpwpfTuXUblRlYTjfSeSLEksNTd_26)); }
	inline Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE * get_kpwpfTuXUblRlYTjfSeSLEksNTd_26() const { return ___kpwpfTuXUblRlYTjfSeSLEksNTd_26; }
	inline Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE ** get_address_of_kpwpfTuXUblRlYTjfSeSLEksNTd_26() { return &___kpwpfTuXUblRlYTjfSeSLEksNTd_26; }
	inline void set_kpwpfTuXUblRlYTjfSeSLEksNTd_26(Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE * value)
	{
		___kpwpfTuXUblRlYTjfSeSLEksNTd_26 = value;
		Il2CppCodeGenWriteBarrier((&___kpwpfTuXUblRlYTjfSeSLEksNTd_26), value);
	}

	inline static int32_t get_offset_of_fpQFUcijUIunFTiZNbdIrMQqJvMf_27() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields, ___fpQFUcijUIunFTiZNbdIrMQqJvMf_27)); }
	inline Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 * get_fpQFUcijUIunFTiZNbdIrMQqJvMf_27() const { return ___fpQFUcijUIunFTiZNbdIrMQqJvMf_27; }
	inline Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 ** get_address_of_fpQFUcijUIunFTiZNbdIrMQqJvMf_27() { return &___fpQFUcijUIunFTiZNbdIrMQqJvMf_27; }
	inline void set_fpQFUcijUIunFTiZNbdIrMQqJvMf_27(Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 * value)
	{
		___fpQFUcijUIunFTiZNbdIrMQqJvMf_27 = value;
		Il2CppCodeGenWriteBarrier((&___fpQFUcijUIunFTiZNbdIrMQqJvMf_27), value);
	}

	inline static int32_t get_offset_of_jqUTJMOeCBaPwenidONjsgqqmhF_28() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields, ___jqUTJMOeCBaPwenidONjsgqqmhF_28)); }
	inline Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE * get_jqUTJMOeCBaPwenidONjsgqqmhF_28() const { return ___jqUTJMOeCBaPwenidONjsgqqmhF_28; }
	inline Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE ** get_address_of_jqUTJMOeCBaPwenidONjsgqqmhF_28() { return &___jqUTJMOeCBaPwenidONjsgqqmhF_28; }
	inline void set_jqUTJMOeCBaPwenidONjsgqqmhF_28(Func_3_t01432DF24DAEAC832D9079B28AE7CD1B99E3AACE * value)
	{
		___jqUTJMOeCBaPwenidONjsgqqmhF_28 = value;
		Il2CppCodeGenWriteBarrier((&___jqUTJMOeCBaPwenidONjsgqqmhF_28), value);
	}

	inline static int32_t get_offset_of_yGfaFeDTgjQCiyfEnFKwcIrTEKK_29() { return static_cast<int32_t>(offsetof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields, ___yGfaFeDTgjQCiyfEnFKwcIrTEKK_29)); }
	inline Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 * get_yGfaFeDTgjQCiyfEnFKwcIrTEKK_29() const { return ___yGfaFeDTgjQCiyfEnFKwcIrTEKK_29; }
	inline Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 ** get_address_of_yGfaFeDTgjQCiyfEnFKwcIrTEKK_29() { return &___yGfaFeDTgjQCiyfEnFKwcIrTEKK_29; }
	inline void set_yGfaFeDTgjQCiyfEnFKwcIrTEKK_29(Func_3_t722B6E01079F18BA4476D2B9A59F2DD94555B0F1 * value)
	{
		___yGfaFeDTgjQCiyfEnFKwcIrTEKK_29 = value;
		Il2CppCodeGenWriteBarrier((&___yGfaFeDTgjQCiyfEnFKwcIrTEKK_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLER_T5780C278CCFB7A81A0272F3B056B1EF463D9B671_H
#ifndef AXIS_T1E0EEBF040B05CB6932311D5F7233C785BB6BEC2_H
#define AXIS_T1E0EEBF040B05CB6932311D5F7233C785BB6BEC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Axis
struct  Axis_t1E0EEBF040B05CB6932311D5F7233C785BB6BEC2  : public Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5
{
public:
	// Rewired.AxisRange Rewired.Controller_Axis::vxFkLJbJCCLNaDtBkbHBIBaBIFQx
	int32_t ___vxFkLJbJCCLNaDtBkbHBIBaBIFQx_7;
	// Rewired.Data.Mapping.HardwareAxisInfo Rewired.Controller_Axis::WXHyRtmmNLOjAyGOdSOPfTCVJpE
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * ___WXHyRtmmNLOjAyGOdSOPfTCVJpE_8;

public:
	inline static int32_t get_offset_of_vxFkLJbJCCLNaDtBkbHBIBaBIFQx_7() { return static_cast<int32_t>(offsetof(Axis_t1E0EEBF040B05CB6932311D5F7233C785BB6BEC2, ___vxFkLJbJCCLNaDtBkbHBIBaBIFQx_7)); }
	inline int32_t get_vxFkLJbJCCLNaDtBkbHBIBaBIFQx_7() const { return ___vxFkLJbJCCLNaDtBkbHBIBaBIFQx_7; }
	inline int32_t* get_address_of_vxFkLJbJCCLNaDtBkbHBIBaBIFQx_7() { return &___vxFkLJbJCCLNaDtBkbHBIBaBIFQx_7; }
	inline void set_vxFkLJbJCCLNaDtBkbHBIBaBIFQx_7(int32_t value)
	{
		___vxFkLJbJCCLNaDtBkbHBIBaBIFQx_7 = value;
	}

	inline static int32_t get_offset_of_WXHyRtmmNLOjAyGOdSOPfTCVJpE_8() { return static_cast<int32_t>(offsetof(Axis_t1E0EEBF040B05CB6932311D5F7233C785BB6BEC2, ___WXHyRtmmNLOjAyGOdSOPfTCVJpE_8)); }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * get_WXHyRtmmNLOjAyGOdSOPfTCVJpE_8() const { return ___WXHyRtmmNLOjAyGOdSOPfTCVJpE_8; }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 ** get_address_of_WXHyRtmmNLOjAyGOdSOPfTCVJpE_8() { return &___WXHyRtmmNLOjAyGOdSOPfTCVJpE_8; }
	inline void set_WXHyRtmmNLOjAyGOdSOPfTCVJpE_8(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * value)
	{
		___WXHyRtmmNLOjAyGOdSOPfTCVJpE_8 = value;
		Il2CppCodeGenWriteBarrier((&___WXHyRtmmNLOjAyGOdSOPfTCVJpE_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T1E0EEBF040B05CB6932311D5F7233C785BB6BEC2_H
#ifndef AXIS2D_T6A78C56910F95EB75AA0F947EA910C4D6EBA7756_H
#define AXIS2D_T6A78C56910F95EB75AA0F947EA910C4D6EBA7756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Axis2D
struct  Axis2D_t6A78C56910F95EB75AA0F947EA910C4D6EBA7756  : public CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA
{
public:
	// Rewired.CalibrationMap Rewired.Controller_Axis2D::GloxFTNLDyfkAYIBdLQDdtRmDqh
	CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 * ___GloxFTNLDyfkAYIBdLQDdtRmDqh_8;

public:
	inline static int32_t get_offset_of_GloxFTNLDyfkAYIBdLQDdtRmDqh_8() { return static_cast<int32_t>(offsetof(Axis2D_t6A78C56910F95EB75AA0F947EA910C4D6EBA7756, ___GloxFTNLDyfkAYIBdLQDdtRmDqh_8)); }
	inline CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 * get_GloxFTNLDyfkAYIBdLQDdtRmDqh_8() const { return ___GloxFTNLDyfkAYIBdLQDdtRmDqh_8; }
	inline CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 ** get_address_of_GloxFTNLDyfkAYIBdLQDdtRmDqh_8() { return &___GloxFTNLDyfkAYIBdLQDdtRmDqh_8; }
	inline void set_GloxFTNLDyfkAYIBdLQDdtRmDqh_8(CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 * value)
	{
		___GloxFTNLDyfkAYIBdLQDdtRmDqh_8 = value;
		Il2CppCodeGenWriteBarrier((&___GloxFTNLDyfkAYIBdLQDdtRmDqh_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS2D_T6A78C56910F95EB75AA0F947EA910C4D6EBA7756_H
#ifndef BUTTON_T78CBA8C35AB8697397D63BF8CF63EB251C4FE457_H
#define BUTTON_T78CBA8C35AB8697397D63BF8CF63EB251C4FE457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Button
struct  Button_t78CBA8C35AB8697397D63BF8CF63EB251C4FE457  : public Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5
{
public:
	// System.Boolean Rewired.Controller_Button::nhcfgKoolkfQhlQolAJyYBOalZj
	bool ___nhcfgKoolkfQhlQolAJyYBOalZj_7;
	// Rewired.Data.Mapping.HardwareButtonInfo Rewired.Controller_Button::wMidbXbtClOiqHSBrJEybRddXCQs
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * ___wMidbXbtClOiqHSBrJEybRddXCQs_8;

public:
	inline static int32_t get_offset_of_nhcfgKoolkfQhlQolAJyYBOalZj_7() { return static_cast<int32_t>(offsetof(Button_t78CBA8C35AB8697397D63BF8CF63EB251C4FE457, ___nhcfgKoolkfQhlQolAJyYBOalZj_7)); }
	inline bool get_nhcfgKoolkfQhlQolAJyYBOalZj_7() const { return ___nhcfgKoolkfQhlQolAJyYBOalZj_7; }
	inline bool* get_address_of_nhcfgKoolkfQhlQolAJyYBOalZj_7() { return &___nhcfgKoolkfQhlQolAJyYBOalZj_7; }
	inline void set_nhcfgKoolkfQhlQolAJyYBOalZj_7(bool value)
	{
		___nhcfgKoolkfQhlQolAJyYBOalZj_7 = value;
	}

	inline static int32_t get_offset_of_wMidbXbtClOiqHSBrJEybRddXCQs_8() { return static_cast<int32_t>(offsetof(Button_t78CBA8C35AB8697397D63BF8CF63EB251C4FE457, ___wMidbXbtClOiqHSBrJEybRddXCQs_8)); }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * get_wMidbXbtClOiqHSBrJEybRddXCQs_8() const { return ___wMidbXbtClOiqHSBrJEybRddXCQs_8; }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 ** get_address_of_wMidbXbtClOiqHSBrJEybRddXCQs_8() { return &___wMidbXbtClOiqHSBrJEybRddXCQs_8; }
	inline void set_wMidbXbtClOiqHSBrJEybRddXCQs_8(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * value)
	{
		___wMidbXbtClOiqHSBrJEybRddXCQs_8 = value;
		Il2CppCodeGenWriteBarrier((&___wMidbXbtClOiqHSBrJEybRddXCQs_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T78CBA8C35AB8697397D63BF8CF63EB251C4FE457_H
#ifndef DMNGJFFSQZLLHVDWDESQORBZHQYM_T77780D7B4EDECB26ACA0D75D3D00859112FA2B04_H
#define DMNGJFFSQZLLHVDWDESQORBZHQYM_T77780D7B4EDECB26ACA0D75D3D00859112FA2B04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_DmnGJFFSqZLlhvDwdEsqOrBzhQYm
struct  DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Controller_DmnGJFFSqZLlhvDwdEsqOrBzhQYm::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Controller_DmnGJFFSqZLlhvDwdEsqOrBzhQYm::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Controller_DmnGJFFSqZLlhvDwdEsqOrBzhQYm::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Controller Rewired.Controller_DmnGJFFSqZLlhvDwdEsqOrBzhQYm::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Controller_DmnGJFFSqZLlhvDwdEsqOrBzhQYm::okMFupfyoBNZDyKMyvAtwGIUiAd
	int32_t ___okMFupfyoBNZDyKMyvAtwGIUiAd_4;
	// System.Int32 Rewired.Controller_DmnGJFFSqZLlhvDwdEsqOrBzhQYm::xHCilCRdZxQtYjPcCdudbctXZFs
	int32_t ___xHCilCRdZxQtYjPcCdudbctXZFs_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4() { return static_cast<int32_t>(offsetof(DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04, ___okMFupfyoBNZDyKMyvAtwGIUiAd_4)); }
	inline int32_t get_okMFupfyoBNZDyKMyvAtwGIUiAd_4() const { return ___okMFupfyoBNZDyKMyvAtwGIUiAd_4; }
	inline int32_t* get_address_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4() { return &___okMFupfyoBNZDyKMyvAtwGIUiAd_4; }
	inline void set_okMFupfyoBNZDyKMyvAtwGIUiAd_4(int32_t value)
	{
		___okMFupfyoBNZDyKMyvAtwGIUiAd_4 = value;
	}

	inline static int32_t get_offset_of_xHCilCRdZxQtYjPcCdudbctXZFs_5() { return static_cast<int32_t>(offsetof(DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04, ___xHCilCRdZxQtYjPcCdudbctXZFs_5)); }
	inline int32_t get_xHCilCRdZxQtYjPcCdudbctXZFs_5() const { return ___xHCilCRdZxQtYjPcCdudbctXZFs_5; }
	inline int32_t* get_address_of_xHCilCRdZxQtYjPcCdudbctXZFs_5() { return &___xHCilCRdZxQtYjPcCdudbctXZFs_5; }
	inline void set_xHCilCRdZxQtYjPcCdudbctXZFs_5(int32_t value)
	{
		___xHCilCRdZxQtYjPcCdudbctXZFs_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DMNGJFFSQZLLHVDWDESQORBZHQYM_T77780D7B4EDECB26ACA0D75D3D00859112FA2B04_H
#ifndef HAT_TBFF0311A328F874AF529CC3E7E433ABBA9618BFD_H
#define HAT_TBFF0311A328F874AF529CC3E7E433ABBA9618BFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Hat
struct  Hat_tBFF0311A328F874AF529CC3E7E433ABBA9618BFD  : public CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA
{
public:
	// System.Int32 Rewired.Controller_Hat::UuxMEBaYYjrHTBlndFvGcptaFref
	int32_t ___UuxMEBaYYjrHTBlndFvGcptaFref_16;
	// Rewired.Controller_Button[] Rewired.Controller_Hat::QjgkqxyMBXGPcdjZSGsOcXzPxUl
	ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C* ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_17;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller_Button> Rewired.Controller_Hat::zyezhlpxlGSWpqZrzsjFXrQGANV
	ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383 * ___zyezhlpxlGSWpqZrzsjFXrQGANV_18;
	// System.Int32[] Rewired.Controller_Hat::slFYKRaWbLbVpueSIalPBHdcNrnA
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___slFYKRaWbLbVpueSIalPBHdcNrnA_19;
	// System.Boolean Rewired.Controller_Hat::IIUdmUgsuNIiSukBSWRAfmwzjEo
	bool ___IIUdmUgsuNIiSukBSWRAfmwzjEo_20;

public:
	inline static int32_t get_offset_of_UuxMEBaYYjrHTBlndFvGcptaFref_16() { return static_cast<int32_t>(offsetof(Hat_tBFF0311A328F874AF529CC3E7E433ABBA9618BFD, ___UuxMEBaYYjrHTBlndFvGcptaFref_16)); }
	inline int32_t get_UuxMEBaYYjrHTBlndFvGcptaFref_16() const { return ___UuxMEBaYYjrHTBlndFvGcptaFref_16; }
	inline int32_t* get_address_of_UuxMEBaYYjrHTBlndFvGcptaFref_16() { return &___UuxMEBaYYjrHTBlndFvGcptaFref_16; }
	inline void set_UuxMEBaYYjrHTBlndFvGcptaFref_16(int32_t value)
	{
		___UuxMEBaYYjrHTBlndFvGcptaFref_16 = value;
	}

	inline static int32_t get_offset_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_17() { return static_cast<int32_t>(offsetof(Hat_tBFF0311A328F874AF529CC3E7E433ABBA9618BFD, ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_17)); }
	inline ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C* get_QjgkqxyMBXGPcdjZSGsOcXzPxUl_17() const { return ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_17; }
	inline ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C** get_address_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_17() { return &___QjgkqxyMBXGPcdjZSGsOcXzPxUl_17; }
	inline void set_QjgkqxyMBXGPcdjZSGsOcXzPxUl_17(ButtonU5BU5D_tEB078799E0930AD6615B644AD177B6C1AF07577C* value)
	{
		___QjgkqxyMBXGPcdjZSGsOcXzPxUl_17 = value;
		Il2CppCodeGenWriteBarrier((&___QjgkqxyMBXGPcdjZSGsOcXzPxUl_17), value);
	}

	inline static int32_t get_offset_of_zyezhlpxlGSWpqZrzsjFXrQGANV_18() { return static_cast<int32_t>(offsetof(Hat_tBFF0311A328F874AF529CC3E7E433ABBA9618BFD, ___zyezhlpxlGSWpqZrzsjFXrQGANV_18)); }
	inline ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383 * get_zyezhlpxlGSWpqZrzsjFXrQGANV_18() const { return ___zyezhlpxlGSWpqZrzsjFXrQGANV_18; }
	inline ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383 ** get_address_of_zyezhlpxlGSWpqZrzsjFXrQGANV_18() { return &___zyezhlpxlGSWpqZrzsjFXrQGANV_18; }
	inline void set_zyezhlpxlGSWpqZrzsjFXrQGANV_18(ReadOnlyCollection_1_tCA51394B27F18929AD1D619E80250F91DC20B383 * value)
	{
		___zyezhlpxlGSWpqZrzsjFXrQGANV_18 = value;
		Il2CppCodeGenWriteBarrier((&___zyezhlpxlGSWpqZrzsjFXrQGANV_18), value);
	}

	inline static int32_t get_offset_of_slFYKRaWbLbVpueSIalPBHdcNrnA_19() { return static_cast<int32_t>(offsetof(Hat_tBFF0311A328F874AF529CC3E7E433ABBA9618BFD, ___slFYKRaWbLbVpueSIalPBHdcNrnA_19)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_slFYKRaWbLbVpueSIalPBHdcNrnA_19() const { return ___slFYKRaWbLbVpueSIalPBHdcNrnA_19; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_slFYKRaWbLbVpueSIalPBHdcNrnA_19() { return &___slFYKRaWbLbVpueSIalPBHdcNrnA_19; }
	inline void set_slFYKRaWbLbVpueSIalPBHdcNrnA_19(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___slFYKRaWbLbVpueSIalPBHdcNrnA_19 = value;
		Il2CppCodeGenWriteBarrier((&___slFYKRaWbLbVpueSIalPBHdcNrnA_19), value);
	}

	inline static int32_t get_offset_of_IIUdmUgsuNIiSukBSWRAfmwzjEo_20() { return static_cast<int32_t>(offsetof(Hat_tBFF0311A328F874AF529CC3E7E433ABBA9618BFD, ___IIUdmUgsuNIiSukBSWRAfmwzjEo_20)); }
	inline bool get_IIUdmUgsuNIiSukBSWRAfmwzjEo_20() const { return ___IIUdmUgsuNIiSukBSWRAfmwzjEo_20; }
	inline bool* get_address_of_IIUdmUgsuNIiSukBSWRAfmwzjEo_20() { return &___IIUdmUgsuNIiSukBSWRAfmwzjEo_20; }
	inline void set_IIUdmUgsuNIiSukBSWRAfmwzjEo_20(bool value)
	{
		___IIUdmUgsuNIiSukBSWRAfmwzjEo_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HAT_TBFF0311A328F874AF529CC3E7E433ABBA9618BFD_H
#ifndef QBXIIDKSQPVGSSBRMNESDNPCBGOT_T91C3E09AA6A7E8156010A7397E5584D255CE47B8_H
#define QBXIIDKSQPVGSSBRMNESDNPCBGOT_T91C3E09AA6A7E8156010A7397E5584D255CE47B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_QBxIidkSqpvGsSBrMNESDNpCbgOt
struct  QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Controller_QBxIidkSqpvGsSBrMNESDNpCbgOt::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Controller_QBxIidkSqpvGsSBrMNESDNpCbgOt::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Controller_QBxIidkSqpvGsSBrMNESDNpCbgOt::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Controller Rewired.Controller_QBxIidkSqpvGsSBrMNESDNpCbgOt::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Controller_QBxIidkSqpvGsSBrMNESDNpCbgOt::kTJJZJZwOLwTeTZLVgkhUriUmfS
	int32_t ___kTJJZJZwOLwTeTZLVgkhUriUmfS_4;
	// System.Int32 Rewired.Controller_QBxIidkSqpvGsSBrMNESDNpCbgOt::OVugcnIKhqBnpgCbITPtTrvSLaT
	int32_t ___OVugcnIKhqBnpgCbITPtTrvSLaT_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_kTJJZJZwOLwTeTZLVgkhUriUmfS_4() { return static_cast<int32_t>(offsetof(QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8, ___kTJJZJZwOLwTeTZLVgkhUriUmfS_4)); }
	inline int32_t get_kTJJZJZwOLwTeTZLVgkhUriUmfS_4() const { return ___kTJJZJZwOLwTeTZLVgkhUriUmfS_4; }
	inline int32_t* get_address_of_kTJJZJZwOLwTeTZLVgkhUriUmfS_4() { return &___kTJJZJZwOLwTeTZLVgkhUriUmfS_4; }
	inline void set_kTJJZJZwOLwTeTZLVgkhUriUmfS_4(int32_t value)
	{
		___kTJJZJZwOLwTeTZLVgkhUriUmfS_4 = value;
	}

	inline static int32_t get_offset_of_OVugcnIKhqBnpgCbITPtTrvSLaT_5() { return static_cast<int32_t>(offsetof(QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8, ___OVugcnIKhqBnpgCbITPtTrvSLaT_5)); }
	inline int32_t get_OVugcnIKhqBnpgCbITPtTrvSLaT_5() const { return ___OVugcnIKhqBnpgCbITPtTrvSLaT_5; }
	inline int32_t* get_address_of_OVugcnIKhqBnpgCbITPtTrvSLaT_5() { return &___OVugcnIKhqBnpgCbITPtTrvSLaT_5; }
	inline void set_OVugcnIKhqBnpgCbITPtTrvSLaT_5(int32_t value)
	{
		___OVugcnIKhqBnpgCbITPtTrvSLaT_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QBXIIDKSQPVGSSBRMNESDNPCBGOT_T91C3E09AA6A7E8156010A7397E5584D255CE47B8_H
#ifndef NFVUTDUMEYERXJDOUSPQSAHENMT_TE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC_H
#define NFVUTDUMEYERXJDOUSPQSAHENMT_TE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_NfVUTdUMeYeRXJDOUspqsAHENMt
struct  NfVUTdUMeYeRXJDOUspqsAHENMt_tE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC  : public hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361
{
public:
	// System.Int32 Rewired.ControllerTemplate_NfVUTdUMeYeRXJDOUspqsAHENMt::prxRdKvUePoVtgFVPLYBrpHztMr
	int32_t ___prxRdKvUePoVtgFVPLYBrpHztMr_5;
	// Rewired.ControllerTemplate_hkuffRDZsiyCBEXTWgHFcjFwHWW[] Rewired.ControllerTemplate_NfVUTdUMeYeRXJDOUspqsAHENMt::tqrCjCDiVROjgZQMTxdrZcCDZcA
	hkuffRDZsiyCBEXTWgHFcjFwHWWU5BU5D_t4BCE28438EFD7AEBAF6ACD08D75CEE279515BF39* ___tqrCjCDiVROjgZQMTxdrZcCDZcA_6;

public:
	inline static int32_t get_offset_of_prxRdKvUePoVtgFVPLYBrpHztMr_5() { return static_cast<int32_t>(offsetof(NfVUTdUMeYeRXJDOUspqsAHENMt_tE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC, ___prxRdKvUePoVtgFVPLYBrpHztMr_5)); }
	inline int32_t get_prxRdKvUePoVtgFVPLYBrpHztMr_5() const { return ___prxRdKvUePoVtgFVPLYBrpHztMr_5; }
	inline int32_t* get_address_of_prxRdKvUePoVtgFVPLYBrpHztMr_5() { return &___prxRdKvUePoVtgFVPLYBrpHztMr_5; }
	inline void set_prxRdKvUePoVtgFVPLYBrpHztMr_5(int32_t value)
	{
		___prxRdKvUePoVtgFVPLYBrpHztMr_5 = value;
	}

	inline static int32_t get_offset_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_6() { return static_cast<int32_t>(offsetof(NfVUTdUMeYeRXJDOUspqsAHENMt_tE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC, ___tqrCjCDiVROjgZQMTxdrZcCDZcA_6)); }
	inline hkuffRDZsiyCBEXTWgHFcjFwHWWU5BU5D_t4BCE28438EFD7AEBAF6ACD08D75CEE279515BF39* get_tqrCjCDiVROjgZQMTxdrZcCDZcA_6() const { return ___tqrCjCDiVROjgZQMTxdrZcCDZcA_6; }
	inline hkuffRDZsiyCBEXTWgHFcjFwHWWU5BU5D_t4BCE28438EFD7AEBAF6ACD08D75CEE279515BF39** get_address_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_6() { return &___tqrCjCDiVROjgZQMTxdrZcCDZcA_6; }
	inline void set_tqrCjCDiVROjgZQMTxdrZcCDZcA_6(hkuffRDZsiyCBEXTWgHFcjFwHWWU5BU5D_t4BCE28438EFD7AEBAF6ACD08D75CEE279515BF39* value)
	{
		___tqrCjCDiVROjgZQMTxdrZcCDZcA_6 = value;
		Il2CppCodeGenWriteBarrier((&___tqrCjCDiVROjgZQMTxdrZcCDZcA_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NFVUTDUMEYERXJDOUSPQSAHENMT_TE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC_H
#ifndef XBTQKGHJDTDWJBTJNFXTFMWVTYXB_TAFD13155A620AB3F978D566FDCBB681CE95A6948_H
#define XBTQKGHJDTDWJBTJNFXTFMWVTYXB_TAFD13155A620AB3F978D566FDCBB681CE95A6948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_xBtqKGHjDTDwJbtJNFxTfMwVtyXb
struct  xBtqKGHjDTDwJbtJNFxTfMwVtyXb_tAFD13155A620AB3F978D566FDCBB681CE95A6948  : public hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361
{
public:
	// System.Int32 Rewired.ControllerTemplate_xBtqKGHjDTDwJbtJNFxTfMwVtyXb::mBlHhaIUcxyZnUQmkSHJiOjfLOB
	int32_t ___mBlHhaIUcxyZnUQmkSHJiOjfLOB_5;
	// Rewired.ControllerTemplate_GfqjFzggDiRITiiqcxubCCJucSZL[] Rewired.ControllerTemplate_xBtqKGHjDTDwJbtJNFxTfMwVtyXb::evyqkhhlRJKGPpArmfuDFVDbvFQ
	GfqjFzggDiRITiiqcxubCCJucSZLU5BU5D_t67419B8798C6BB4669E248C852383AEB826E7C66* ___evyqkhhlRJKGPpArmfuDFVDbvFQ_6;

public:
	inline static int32_t get_offset_of_mBlHhaIUcxyZnUQmkSHJiOjfLOB_5() { return static_cast<int32_t>(offsetof(xBtqKGHjDTDwJbtJNFxTfMwVtyXb_tAFD13155A620AB3F978D566FDCBB681CE95A6948, ___mBlHhaIUcxyZnUQmkSHJiOjfLOB_5)); }
	inline int32_t get_mBlHhaIUcxyZnUQmkSHJiOjfLOB_5() const { return ___mBlHhaIUcxyZnUQmkSHJiOjfLOB_5; }
	inline int32_t* get_address_of_mBlHhaIUcxyZnUQmkSHJiOjfLOB_5() { return &___mBlHhaIUcxyZnUQmkSHJiOjfLOB_5; }
	inline void set_mBlHhaIUcxyZnUQmkSHJiOjfLOB_5(int32_t value)
	{
		___mBlHhaIUcxyZnUQmkSHJiOjfLOB_5 = value;
	}

	inline static int32_t get_offset_of_evyqkhhlRJKGPpArmfuDFVDbvFQ_6() { return static_cast<int32_t>(offsetof(xBtqKGHjDTDwJbtJNFxTfMwVtyXb_tAFD13155A620AB3F978D566FDCBB681CE95A6948, ___evyqkhhlRJKGPpArmfuDFVDbvFQ_6)); }
	inline GfqjFzggDiRITiiqcxubCCJucSZLU5BU5D_t67419B8798C6BB4669E248C852383AEB826E7C66* get_evyqkhhlRJKGPpArmfuDFVDbvFQ_6() const { return ___evyqkhhlRJKGPpArmfuDFVDbvFQ_6; }
	inline GfqjFzggDiRITiiqcxubCCJucSZLU5BU5D_t67419B8798C6BB4669E248C852383AEB826E7C66** get_address_of_evyqkhhlRJKGPpArmfuDFVDbvFQ_6() { return &___evyqkhhlRJKGPpArmfuDFVDbvFQ_6; }
	inline void set_evyqkhhlRJKGPpArmfuDFVDbvFQ_6(GfqjFzggDiRITiiqcxubCCJucSZLU5BU5D_t67419B8798C6BB4669E248C852383AEB826E7C66* value)
	{
		___evyqkhhlRJKGPpArmfuDFVDbvFQ_6 = value;
		Il2CppCodeGenWriteBarrier((&___evyqkhhlRJKGPpArmfuDFVDbvFQ_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XBTQKGHJDTDWJBTJNFXTFMWVTYXB_TAFD13155A620AB3F978D566FDCBB681CE95A6948_H
#ifndef CONTROLLERTEMPLATEACTIONAXISMAP_T311F982CCAEDB0D25D6CAD1ABD4CE49E6BF2992C_H
#define CONTROLLERTEMPLATEACTIONAXISMAP_T311F982CCAEDB0D25D6CAD1ABD4CE49E6BF2992C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplateActionAxisMap
struct  ControllerTemplateActionAxisMap_t311F982CCAEDB0D25D6CAD1ABD4CE49E6BF2992C  : public ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49
{
public:
	// Rewired.AxisRange Rewired.ControllerTemplateActionAxisMap::xTkhpClvSKvoiLfYNzcXIDURAPx
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_6;
	// Rewired.Pole Rewired.ControllerTemplateActionAxisMap::magSqBEVFaJkoQfcAvXLVDzmcnZB
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_7;
	// System.Boolean Rewired.ControllerTemplateActionAxisMap::GfvPtFqTnCIcrWebHAQzqOoKceI
	bool ___GfvPtFqTnCIcrWebHAQzqOoKceI_8;

public:
	inline static int32_t get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_6() { return static_cast<int32_t>(offsetof(ControllerTemplateActionAxisMap_t311F982CCAEDB0D25D6CAD1ABD4CE49E6BF2992C, ___xTkhpClvSKvoiLfYNzcXIDURAPx_6)); }
	inline int32_t get_xTkhpClvSKvoiLfYNzcXIDURAPx_6() const { return ___xTkhpClvSKvoiLfYNzcXIDURAPx_6; }
	inline int32_t* get_address_of_xTkhpClvSKvoiLfYNzcXIDURAPx_6() { return &___xTkhpClvSKvoiLfYNzcXIDURAPx_6; }
	inline void set_xTkhpClvSKvoiLfYNzcXIDURAPx_6(int32_t value)
	{
		___xTkhpClvSKvoiLfYNzcXIDURAPx_6 = value;
	}

	inline static int32_t get_offset_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_7() { return static_cast<int32_t>(offsetof(ControllerTemplateActionAxisMap_t311F982CCAEDB0D25D6CAD1ABD4CE49E6BF2992C, ___magSqBEVFaJkoQfcAvXLVDzmcnZB_7)); }
	inline int32_t get_magSqBEVFaJkoQfcAvXLVDzmcnZB_7() const { return ___magSqBEVFaJkoQfcAvXLVDzmcnZB_7; }
	inline int32_t* get_address_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_7() { return &___magSqBEVFaJkoQfcAvXLVDzmcnZB_7; }
	inline void set_magSqBEVFaJkoQfcAvXLVDzmcnZB_7(int32_t value)
	{
		___magSqBEVFaJkoQfcAvXLVDzmcnZB_7 = value;
	}

	inline static int32_t get_offset_of_GfvPtFqTnCIcrWebHAQzqOoKceI_8() { return static_cast<int32_t>(offsetof(ControllerTemplateActionAxisMap_t311F982CCAEDB0D25D6CAD1ABD4CE49E6BF2992C, ___GfvPtFqTnCIcrWebHAQzqOoKceI_8)); }
	inline bool get_GfvPtFqTnCIcrWebHAQzqOoKceI_8() const { return ___GfvPtFqTnCIcrWebHAQzqOoKceI_8; }
	inline bool* get_address_of_GfvPtFqTnCIcrWebHAQzqOoKceI_8() { return &___GfvPtFqTnCIcrWebHAQzqOoKceI_8; }
	inline void set_GfvPtFqTnCIcrWebHAQzqOoKceI_8(bool value)
	{
		___GfvPtFqTnCIcrWebHAQzqOoKceI_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEACTIONAXISMAP_T311F982CCAEDB0D25D6CAD1ABD4CE49E6BF2992C_H
#ifndef CONTROLLERTEMPLATEACTIONBUTTONMAP_T16DD6328C5BE272917E348DD67578B7486270D77_H
#define CONTROLLERTEMPLATEACTIONBUTTONMAP_T16DD6328C5BE272917E348DD67578B7486270D77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplateActionButtonMap
struct  ControllerTemplateActionButtonMap_t16DD6328C5BE272917E348DD67578B7486270D77  : public ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49
{
public:
	// Rewired.Pole Rewired.ControllerTemplateActionButtonMap::magSqBEVFaJkoQfcAvXLVDzmcnZB
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_6;

public:
	inline static int32_t get_offset_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_6() { return static_cast<int32_t>(offsetof(ControllerTemplateActionButtonMap_t16DD6328C5BE272917E348DD67578B7486270D77, ___magSqBEVFaJkoQfcAvXLVDzmcnZB_6)); }
	inline int32_t get_magSqBEVFaJkoQfcAvXLVDzmcnZB_6() const { return ___magSqBEVFaJkoQfcAvXLVDzmcnZB_6; }
	inline int32_t* get_address_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_6() { return &___magSqBEVFaJkoQfcAvXLVDzmcnZB_6; }
	inline void set_magSqBEVFaJkoQfcAvXLVDzmcnZB_6(int32_t value)
	{
		___magSqBEVFaJkoQfcAvXLVDzmcnZB_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEACTIONBUTTONMAP_T16DD6328C5BE272917E348DD67578B7486270D77_H
#ifndef FFLKIMYLYEKWCIXTGCJVZWRSAVC_T6216AFA82C00EA53FA32D0BDA30F9B1FF868289E_H
#define FFLKIMYLYEKWCIXTGCJVZWRSAVC_T6216AFA82C00EA53FA32D0BDA30F9B1FF868289E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerWithAxes_FflkIMylYekwciXtGCJvZWRSaVc
struct  FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ControllerWithAxes_FflkIMylYekwciXtGCJvZWRSaVc::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerWithAxes_FflkIMylYekwciXtGCJvZWRSaVc::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerWithAxes_FflkIMylYekwciXtGCJvZWRSaVc::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerWithAxes Rewired.ControllerWithAxes_FflkIMylYekwciXtGCJvZWRSaVc::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerPollingInfo Rewired.ControllerWithAxes_FflkIMylYekwciXtGCJvZWRSaVc::BVlhkuUrRIekOUuHScNQyxbyOfu
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___BVlhkuUrRIekOUuHScNQyxbyOfu_4;
	// Rewired.ControllerPollingInfo Rewired.ControllerWithAxes_FflkIMylYekwciXtGCJvZWRSaVc::DzLLFEghsbvQsAIVtiTJfpVcUcv
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___DzLLFEghsbvQsAIVtiTJfpVcUcv_5;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ControllerWithAxes_FflkIMylYekwciXtGCJvZWRSaVc::zCSdpqWqddovuMgwXAspBmdHeqjU
	RuntimeObject* ___zCSdpqWqddovuMgwXAspBmdHeqjU_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ControllerWithAxes_FflkIMylYekwciXtGCJvZWRSaVc::GFlKUhIfsrNtSvqPGiPCGTHELDOP
	RuntimeObject* ___GFlKUhIfsrNtSvqPGiPCGTHELDOP_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_BVlhkuUrRIekOUuHScNQyxbyOfu_4() { return static_cast<int32_t>(offsetof(FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E, ___BVlhkuUrRIekOUuHScNQyxbyOfu_4)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_BVlhkuUrRIekOUuHScNQyxbyOfu_4() const { return ___BVlhkuUrRIekOUuHScNQyxbyOfu_4; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_BVlhkuUrRIekOUuHScNQyxbyOfu_4() { return &___BVlhkuUrRIekOUuHScNQyxbyOfu_4; }
	inline void set_BVlhkuUrRIekOUuHScNQyxbyOfu_4(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___BVlhkuUrRIekOUuHScNQyxbyOfu_4 = value;
	}

	inline static int32_t get_offset_of_DzLLFEghsbvQsAIVtiTJfpVcUcv_5() { return static_cast<int32_t>(offsetof(FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E, ___DzLLFEghsbvQsAIVtiTJfpVcUcv_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_DzLLFEghsbvQsAIVtiTJfpVcUcv_5() const { return ___DzLLFEghsbvQsAIVtiTJfpVcUcv_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_DzLLFEghsbvQsAIVtiTJfpVcUcv_5() { return &___DzLLFEghsbvQsAIVtiTJfpVcUcv_5; }
	inline void set_DzLLFEghsbvQsAIVtiTJfpVcUcv_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___DzLLFEghsbvQsAIVtiTJfpVcUcv_5 = value;
	}

	inline static int32_t get_offset_of_zCSdpqWqddovuMgwXAspBmdHeqjU_6() { return static_cast<int32_t>(offsetof(FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E, ___zCSdpqWqddovuMgwXAspBmdHeqjU_6)); }
	inline RuntimeObject* get_zCSdpqWqddovuMgwXAspBmdHeqjU_6() const { return ___zCSdpqWqddovuMgwXAspBmdHeqjU_6; }
	inline RuntimeObject** get_address_of_zCSdpqWqddovuMgwXAspBmdHeqjU_6() { return &___zCSdpqWqddovuMgwXAspBmdHeqjU_6; }
	inline void set_zCSdpqWqddovuMgwXAspBmdHeqjU_6(RuntimeObject* value)
	{
		___zCSdpqWqddovuMgwXAspBmdHeqjU_6 = value;
		Il2CppCodeGenWriteBarrier((&___zCSdpqWqddovuMgwXAspBmdHeqjU_6), value);
	}

	inline static int32_t get_offset_of_GFlKUhIfsrNtSvqPGiPCGTHELDOP_7() { return static_cast<int32_t>(offsetof(FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E, ___GFlKUhIfsrNtSvqPGiPCGTHELDOP_7)); }
	inline RuntimeObject* get_GFlKUhIfsrNtSvqPGiPCGTHELDOP_7() const { return ___GFlKUhIfsrNtSvqPGiPCGTHELDOP_7; }
	inline RuntimeObject** get_address_of_GFlKUhIfsrNtSvqPGiPCGTHELDOP_7() { return &___GFlKUhIfsrNtSvqPGiPCGTHELDOP_7; }
	inline void set_GFlKUhIfsrNtSvqPGiPCGTHELDOP_7(RuntimeObject* value)
	{
		___GFlKUhIfsrNtSvqPGiPCGTHELDOP_7 = value;
		Il2CppCodeGenWriteBarrier((&___GFlKUhIfsrNtSvqPGiPCGTHELDOP_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FFLKIMYLYEKWCIXTGCJVZWRSAVC_T6216AFA82C00EA53FA32D0BDA30F9B1FF868289E_H
#ifndef CFQIKYOICOMVPYQUCNDHHHLKWLV_TAB98DA2DBDB052DCA815CC7E9976DDEEED19889B_H
#define CFQIKYOICOMVPYQUCNDHHHLKWLV_TAB98DA2DBDB052DCA815CC7E9976DDEEED19889B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerWithAxes_cfqIKYOicOMvpYQUcNdHhHlKwlv
struct  cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ControllerWithAxes_cfqIKYOicOMvpYQUcNdHhHlKwlv::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerWithAxes_cfqIKYOicOMvpYQUcNdHhHlKwlv::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerWithAxes_cfqIKYOicOMvpYQUcNdHhHlKwlv::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerWithAxes Rewired.ControllerWithAxes_cfqIKYOicOMvpYQUcNdHhHlKwlv::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerPollingInfo Rewired.ControllerWithAxes_cfqIKYOicOMvpYQUcNdHhHlKwlv::vCzGEQLpDyCakhWGGSKDIApvFIGB
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___vCzGEQLpDyCakhWGGSKDIApvFIGB_4;
	// Rewired.ControllerPollingInfo Rewired.ControllerWithAxes_cfqIKYOicOMvpYQUcNdHhHlKwlv::MYbsRdJqOsruJPqDMcqcpriuBNZ
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___MYbsRdJqOsruJPqDMcqcpriuBNZ_5;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ControllerWithAxes_cfqIKYOicOMvpYQUcNdHhHlKwlv::ErSDOSEebFXZngDEPXYtFPIADhic
	RuntimeObject* ___ErSDOSEebFXZngDEPXYtFPIADhic_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ControllerWithAxes_cfqIKYOicOMvpYQUcNdHhHlKwlv::NoTAyTxmULscBYhxegWobtJUNJhc
	RuntimeObject* ___NoTAyTxmULscBYhxegWobtJUNJhc_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_vCzGEQLpDyCakhWGGSKDIApvFIGB_4() { return static_cast<int32_t>(offsetof(cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B, ___vCzGEQLpDyCakhWGGSKDIApvFIGB_4)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_vCzGEQLpDyCakhWGGSKDIApvFIGB_4() const { return ___vCzGEQLpDyCakhWGGSKDIApvFIGB_4; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_vCzGEQLpDyCakhWGGSKDIApvFIGB_4() { return &___vCzGEQLpDyCakhWGGSKDIApvFIGB_4; }
	inline void set_vCzGEQLpDyCakhWGGSKDIApvFIGB_4(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___vCzGEQLpDyCakhWGGSKDIApvFIGB_4 = value;
	}

	inline static int32_t get_offset_of_MYbsRdJqOsruJPqDMcqcpriuBNZ_5() { return static_cast<int32_t>(offsetof(cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B, ___MYbsRdJqOsruJPqDMcqcpriuBNZ_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_MYbsRdJqOsruJPqDMcqcpriuBNZ_5() const { return ___MYbsRdJqOsruJPqDMcqcpriuBNZ_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_MYbsRdJqOsruJPqDMcqcpriuBNZ_5() { return &___MYbsRdJqOsruJPqDMcqcpriuBNZ_5; }
	inline void set_MYbsRdJqOsruJPqDMcqcpriuBNZ_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___MYbsRdJqOsruJPqDMcqcpriuBNZ_5 = value;
	}

	inline static int32_t get_offset_of_ErSDOSEebFXZngDEPXYtFPIADhic_6() { return static_cast<int32_t>(offsetof(cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B, ___ErSDOSEebFXZngDEPXYtFPIADhic_6)); }
	inline RuntimeObject* get_ErSDOSEebFXZngDEPXYtFPIADhic_6() const { return ___ErSDOSEebFXZngDEPXYtFPIADhic_6; }
	inline RuntimeObject** get_address_of_ErSDOSEebFXZngDEPXYtFPIADhic_6() { return &___ErSDOSEebFXZngDEPXYtFPIADhic_6; }
	inline void set_ErSDOSEebFXZngDEPXYtFPIADhic_6(RuntimeObject* value)
	{
		___ErSDOSEebFXZngDEPXYtFPIADhic_6 = value;
		Il2CppCodeGenWriteBarrier((&___ErSDOSEebFXZngDEPXYtFPIADhic_6), value);
	}

	inline static int32_t get_offset_of_NoTAyTxmULscBYhxegWobtJUNJhc_7() { return static_cast<int32_t>(offsetof(cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B, ___NoTAyTxmULscBYhxegWobtJUNJhc_7)); }
	inline RuntimeObject* get_NoTAyTxmULscBYhxegWobtJUNJhc_7() const { return ___NoTAyTxmULscBYhxegWobtJUNJhc_7; }
	inline RuntimeObject** get_address_of_NoTAyTxmULscBYhxegWobtJUNJhc_7() { return &___NoTAyTxmULscBYhxegWobtJUNJhc_7; }
	inline void set_NoTAyTxmULscBYhxegWobtJUNJhc_7(RuntimeObject* value)
	{
		___NoTAyTxmULscBYhxegWobtJUNJhc_7 = value;
		Il2CppCodeGenWriteBarrier((&___NoTAyTxmULscBYhxegWobtJUNJhc_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFQIKYOICOMVPYQUCNDHHHLKWLV_TAB98DA2DBDB052DCA815CC7E9976DDEEED19889B_H
#ifndef DBMPLGQPPFQJQIDMCLVCNWPZALW_T4BD6F305F630CA731CB9263A7D123956003A2238_H
#define DBMPLGQPPFQJQIDMCLVCNWPZALW_T4BD6F305F630CA731CB9263A7D123956003A2238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerWithAxes_dbMPlGqPPfqJQIDMCLVcnwPzALW
struct  dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ControllerWithAxes_dbMPlGqPPfqJQIDMCLVcnwPzALW::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ControllerWithAxes_dbMPlGqPPfqJQIDMCLVcnwPzALW::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ControllerWithAxes_dbMPlGqPPfqJQIDMCLVcnwPzALW::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ControllerWithAxes Rewired.ControllerWithAxes_dbMPlGqPPfqJQIDMCLVcnwPzALW::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.ControllerWithAxes_dbMPlGqPPfqJQIDMCLVcnwPzALW::vLHeOKbAsNDmvwORrSjMmEJWlNtc
	int32_t ___vLHeOKbAsNDmvwORrSjMmEJWlNtc_4;
	// Rewired.Pole Rewired.ControllerWithAxes_dbMPlGqPPfqJQIDMCLVcnwPzALW::kvNWPugkqNwMBtMEUHQgoecgPEH
	int32_t ___kvNWPugkqNwMBtMEUHQgoecgPEH_5;
	// System.Int32 Rewired.ControllerWithAxes_dbMPlGqPPfqJQIDMCLVcnwPzALW::qbrmVnPLFWJXzrauyOLtmgkTYiE
	int32_t ___qbrmVnPLFWJXzrauyOLtmgkTYiE_6;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_vLHeOKbAsNDmvwORrSjMmEJWlNtc_4() { return static_cast<int32_t>(offsetof(dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238, ___vLHeOKbAsNDmvwORrSjMmEJWlNtc_4)); }
	inline int32_t get_vLHeOKbAsNDmvwORrSjMmEJWlNtc_4() const { return ___vLHeOKbAsNDmvwORrSjMmEJWlNtc_4; }
	inline int32_t* get_address_of_vLHeOKbAsNDmvwORrSjMmEJWlNtc_4() { return &___vLHeOKbAsNDmvwORrSjMmEJWlNtc_4; }
	inline void set_vLHeOKbAsNDmvwORrSjMmEJWlNtc_4(int32_t value)
	{
		___vLHeOKbAsNDmvwORrSjMmEJWlNtc_4 = value;
	}

	inline static int32_t get_offset_of_kvNWPugkqNwMBtMEUHQgoecgPEH_5() { return static_cast<int32_t>(offsetof(dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238, ___kvNWPugkqNwMBtMEUHQgoecgPEH_5)); }
	inline int32_t get_kvNWPugkqNwMBtMEUHQgoecgPEH_5() const { return ___kvNWPugkqNwMBtMEUHQgoecgPEH_5; }
	inline int32_t* get_address_of_kvNWPugkqNwMBtMEUHQgoecgPEH_5() { return &___kvNWPugkqNwMBtMEUHQgoecgPEH_5; }
	inline void set_kvNWPugkqNwMBtMEUHQgoecgPEH_5(int32_t value)
	{
		___kvNWPugkqNwMBtMEUHQgoecgPEH_5 = value;
	}

	inline static int32_t get_offset_of_qbrmVnPLFWJXzrauyOLtmgkTYiE_6() { return static_cast<int32_t>(offsetof(dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238, ___qbrmVnPLFWJXzrauyOLtmgkTYiE_6)); }
	inline int32_t get_qbrmVnPLFWJXzrauyOLtmgkTYiE_6() const { return ___qbrmVnPLFWJXzrauyOLtmgkTYiE_6; }
	inline int32_t* get_address_of_qbrmVnPLFWJXzrauyOLtmgkTYiE_6() { return &___qbrmVnPLFWJXzrauyOLtmgkTYiE_6; }
	inline void set_qbrmVnPLFWJXzrauyOLtmgkTYiE_6(int32_t value)
	{
		___qbrmVnPLFWJXzrauyOLtmgkTYiE_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DBMPLGQPPFQJQIDMCLVCNWPZALW_T4BD6F305F630CA731CB9263A7D123956003A2238_H
#ifndef U3CPOLLFORALLKEYSU3ED__0_T779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B_H
#define U3CPOLLFORALLKEYSU3ED__0_T779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Keyboard_<PollForAllKeys>d__0
struct  U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Keyboard_<PollForAllKeys>d__0::<>2__current
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___U3CU3E2__current_0;
	// System.Int32 Rewired.Keyboard_<PollForAllKeys>d__0::<>1__state
	int32_t ___U3CU3E1__state_1;
	// System.Int32 Rewired.Keyboard_<PollForAllKeys>d__0::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Rewired.Keyboard Rewired.Keyboard_<PollForAllKeys>d__0::<>4__this
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * ___U3CU3E4__this_3;
	// System.Int32 Rewired.Keyboard_<PollForAllKeys>d__0::<count>5__1
	int32_t ___U3CcountU3E5__1_4;
	// System.Int32 Rewired.Keyboard_<PollForAllKeys>d__0::<i>5__2
	int32_t ___U3CiU3E5__2_5;
	// UnityEngine.KeyCode Rewired.Keyboard_<PollForAllKeys>d__0::<keyCode>5__3
	int32_t ___U3CkeyCodeU3E5__3_6;
	// System.Boolean Rewired.Keyboard_<PollForAllKeys>d__0::<value>5__4
	bool ___U3CvalueU3E5__4_7;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B, ___U3CU3E2__current_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___U3CU3E2__current_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B, ___U3CU3E4__this_3)); }
	inline Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CcountU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B, ___U3CcountU3E5__1_4)); }
	inline int32_t get_U3CcountU3E5__1_4() const { return ___U3CcountU3E5__1_4; }
	inline int32_t* get_address_of_U3CcountU3E5__1_4() { return &___U3CcountU3E5__1_4; }
	inline void set_U3CcountU3E5__1_4(int32_t value)
	{
		___U3CcountU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B, ___U3CiU3E5__2_5)); }
	inline int32_t get_U3CiU3E5__2_5() const { return ___U3CiU3E5__2_5; }
	inline int32_t* get_address_of_U3CiU3E5__2_5() { return &___U3CiU3E5__2_5; }
	inline void set_U3CiU3E5__2_5(int32_t value)
	{
		___U3CiU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CkeyCodeU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B, ___U3CkeyCodeU3E5__3_6)); }
	inline int32_t get_U3CkeyCodeU3E5__3_6() const { return ___U3CkeyCodeU3E5__3_6; }
	inline int32_t* get_address_of_U3CkeyCodeU3E5__3_6() { return &___U3CkeyCodeU3E5__3_6; }
	inline void set_U3CkeyCodeU3E5__3_6(int32_t value)
	{
		___U3CkeyCodeU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CvalueU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B, ___U3CvalueU3E5__4_7)); }
	inline bool get_U3CvalueU3E5__4_7() const { return ___U3CvalueU3E5__4_7; }
	inline bool* get_address_of_U3CvalueU3E5__4_7() { return &___U3CvalueU3E5__4_7; }
	inline void set_U3CvalueU3E5__4_7(bool value)
	{
		___U3CvalueU3E5__4_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOLLFORALLKEYSU3ED__0_T779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B_H
#ifndef U3CPOLLFORALLKEYSDOWNU3ED__7_TC694B70AC0E28BABB75E38FB1344F4503490B006_H
#define U3CPOLLFORALLKEYSDOWNU3ED__7_TC694B70AC0E28BABB75E38FB1344F4503490B006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Keyboard_<PollForAllKeysDown>d__7
struct  U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Keyboard_<PollForAllKeysDown>d__7::<>2__current
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___U3CU3E2__current_0;
	// System.Int32 Rewired.Keyboard_<PollForAllKeysDown>d__7::<>1__state
	int32_t ___U3CU3E1__state_1;
	// System.Int32 Rewired.Keyboard_<PollForAllKeysDown>d__7::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Rewired.Keyboard Rewired.Keyboard_<PollForAllKeysDown>d__7::<>4__this
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * ___U3CU3E4__this_3;
	// System.Int32 Rewired.Keyboard_<PollForAllKeysDown>d__7::<count>5__8
	int32_t ___U3CcountU3E5__8_4;
	// System.Int32 Rewired.Keyboard_<PollForAllKeysDown>d__7::<i>5__9
	int32_t ___U3CiU3E5__9_5;
	// UnityEngine.KeyCode Rewired.Keyboard_<PollForAllKeysDown>d__7::<keyCode>5__a
	int32_t ___U3CkeyCodeU3E5__a_6;
	// System.Boolean Rewired.Keyboard_<PollForAllKeysDown>d__7::<value>5__b
	bool ___U3CvalueU3E5__b_7;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006, ___U3CU3E2__current_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___U3CU3E2__current_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006, ___U3CU3E4__this_3)); }
	inline Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CcountU3E5__8_4() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006, ___U3CcountU3E5__8_4)); }
	inline int32_t get_U3CcountU3E5__8_4() const { return ___U3CcountU3E5__8_4; }
	inline int32_t* get_address_of_U3CcountU3E5__8_4() { return &___U3CcountU3E5__8_4; }
	inline void set_U3CcountU3E5__8_4(int32_t value)
	{
		___U3CcountU3E5__8_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__9_5() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006, ___U3CiU3E5__9_5)); }
	inline int32_t get_U3CiU3E5__9_5() const { return ___U3CiU3E5__9_5; }
	inline int32_t* get_address_of_U3CiU3E5__9_5() { return &___U3CiU3E5__9_5; }
	inline void set_U3CiU3E5__9_5(int32_t value)
	{
		___U3CiU3E5__9_5 = value;
	}

	inline static int32_t get_offset_of_U3CkeyCodeU3E5__a_6() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006, ___U3CkeyCodeU3E5__a_6)); }
	inline int32_t get_U3CkeyCodeU3E5__a_6() const { return ___U3CkeyCodeU3E5__a_6; }
	inline int32_t* get_address_of_U3CkeyCodeU3E5__a_6() { return &___U3CkeyCodeU3E5__a_6; }
	inline void set_U3CkeyCodeU3E5__a_6(int32_t value)
	{
		___U3CkeyCodeU3E5__a_6 = value;
	}

	inline static int32_t get_offset_of_U3CvalueU3E5__b_7() { return static_cast<int32_t>(offsetof(U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006, ___U3CvalueU3E5__b_7)); }
	inline bool get_U3CvalueU3E5__b_7() const { return ___U3CvalueU3E5__b_7; }
	inline bool* get_address_of_U3CvalueU3E5__b_7() { return &___U3CvalueU3E5__b_7; }
	inline void set_U3CvalueU3E5__b_7(bool value)
	{
		___U3CvalueU3E5__b_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOLLFORALLKEYSDOWNU3ED__7_TC694B70AC0E28BABB75E38FB1344F4503490B006_H
#ifndef GCUUALLDTDNQOAUGVTUMUJDQUKM_T330424FF28C1320876DAE843D96B438FB2A79A35_H
#define GCUUALLDTDNQOAUGVTUMUJDQUKM_T330424FF28C1320876DAE843D96B438FB2A79A35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_GCUuallDtDNqoAUGVtUMUJdqukM
struct  GCUuallDtDNqoAUGVtUMUJdqukM_t330424FF28C1320876DAE843D96B438FB2A79A35  : public NfVUTdUMeYeRXJDOUspqsAHENMt_tE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCUUALLDTDNQOAUGVTUMUJDQUKM_T330424FF28C1320876DAE843D96B438FB2A79A35_H
#ifndef ZZSCXOBIUBFDJABDCTFEGCGTWPEM_T36997E96128051C1F7FB8955C195166A8BA33E33_H
#define ZZSCXOBIUBFDJABDCTFEGCGTWPEM_T36997E96128051C1F7FB8955C195166A8BA33E33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_ZZScxobIubfDJABdCTFegCgTWpeM
struct  ZZScxobIubfDJABdCTFegCgTWpeM_t36997E96128051C1F7FB8955C195166A8BA33E33  : public NfVUTdUMeYeRXJDOUspqsAHENMt_tE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZZSCXOBIUBFDJABDCTFEGCGTWPEM_T36997E96128051C1F7FB8955C195166A8BA33E33_H
#ifndef BZWGCKHAPJEZTCWBGOAZUYVMBAC_T6904AD0C9C60F59789EBFC1AAACC539BEB16D923_H
#define BZWGCKHAPJEZTCWBGOAZUYVMBAC_T6904AD0C9C60F59789EBFC1AAACC539BEB16D923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_bZwgckhapJeZTCwbgoaZUyvmBac
struct  bZwgckhapJeZTCwbgoaZUyvmBac_t6904AD0C9C60F59789EBFC1AAACC539BEB16D923  : public NfVUTdUMeYeRXJDOUspqsAHENMt_tE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BZWGCKHAPJEZTCWBGOAZUYVMBAC_T6904AD0C9C60F59789EBFC1AAACC539BEB16D923_H
#ifndef DIWZBFNRSFHUMXEWKVFOUNVHKTB_T4BE7E36D57BC02EAB8AF094AA1BCD2A268107B8A_H
#define DIWZBFNRSFHUMXEWKVFOUNVHKTB_T4BE7E36D57BC02EAB8AF094AA1BCD2A268107B8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_dIwzbFNrSfHUmxEwkvfoUNVhkTB
struct  dIwzbFNrSfHUmxEwkvfoUNVhkTB_t4BE7E36D57BC02EAB8AF094AA1BCD2A268107B8A  : public NfVUTdUMeYeRXJDOUspqsAHENMt_tE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIWZBFNRSFHUMXEWKVFOUNVHKTB_T4BE7E36D57BC02EAB8AF094AA1BCD2A268107B8A_H
#ifndef LYEKGVCDJBBGXPNYQFAVBSJFJMAW_T7C80FB69C9335C24D04413C4FDB247F19846E46B_H
#define LYEKGVCDJBBGXPNYQFAVBSJFJMAW_T7C80FB69C9335C24D04413C4FDB247F19846E46B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_lyekGvCdjbBGxpnyQFAvBsjfJMaw
struct  lyekGvCdjbBGxpnyQFAvBsjfJMaw_t7C80FB69C9335C24D04413C4FDB247F19846E46B  : public NfVUTdUMeYeRXJDOUspqsAHENMt_tE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LYEKGVCDJBBGXPNYQFAVBSJFJMAW_T7C80FB69C9335C24D04413C4FDB247F19846E46B_H
#ifndef UHYVDRLCNXOTEHFQIRCWLAIBMVQ_T111272E40546E3E745B37B768C719637CD36BB35_H
#define UHYVDRLCNXOTEHFQIRCWLAIBMVQ_T111272E40546E3E745B37B768C719637CD36BB35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_uhYVdRLcNxOtEHFQiRcwlAibmvQ
struct  uhYVdRLcNxOtEHFQiRcwlAibmvQ_t111272E40546E3E745B37B768C719637CD36BB35  : public NfVUTdUMeYeRXJDOUspqsAHENMt_tE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UHYVDRLCNXOTEHFQIRCWLAIBMVQ_T111272E40546E3E745B37B768C719637CD36BB35_H
#ifndef XSFYAIWOTPPLHKEMMAZMDWHACLVG_T5B3392C09884B24F2D82C6DCE566408DC2057427_H
#define XSFYAIWOTPPLHKEMMAZMDWHACLVG_T5B3392C09884B24F2D82C6DCE566408DC2057427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_xSfYaIWOtPpLHkeMmaZMDWhAcLvg
struct  xSfYaIWOtPpLHkeMmaZMDWhAcLvg_t5B3392C09884B24F2D82C6DCE566408DC2057427  : public xBtqKGHjDTDwJbtJNFxTfMwVtyXb_tAFD13155A620AB3F978D566FDCBB681CE95A6948
{
public:
	// LJAWzBuJIsAgLPPZeEUKfpasUWYi Rewired.ControllerTemplate_xSfYaIWOtPpLHkeMmaZMDWhAcLvg::YnprvkQpvYdMKdzVQVDfUcJcXFy
	LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384 * ___YnprvkQpvYdMKdzVQVDfUcJcXFy_7;
	// System.String Rewired.ControllerTemplate_xSfYaIWOtPpLHkeMmaZMDWhAcLvg::uFXeDuanEZKOoogpAMktZMXKVzf
	String_t* ___uFXeDuanEZKOoogpAMktZMXKVzf_8;
	// System.String Rewired.ControllerTemplate_xSfYaIWOtPpLHkeMmaZMDWhAcLvg::mzLgTYPgqCwurKDKFRDEUSASIMe
	String_t* ___mzLgTYPgqCwurKDKFRDEUSASIMe_9;

public:
	inline static int32_t get_offset_of_YnprvkQpvYdMKdzVQVDfUcJcXFy_7() { return static_cast<int32_t>(offsetof(xSfYaIWOtPpLHkeMmaZMDWhAcLvg_t5B3392C09884B24F2D82C6DCE566408DC2057427, ___YnprvkQpvYdMKdzVQVDfUcJcXFy_7)); }
	inline LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384 * get_YnprvkQpvYdMKdzVQVDfUcJcXFy_7() const { return ___YnprvkQpvYdMKdzVQVDfUcJcXFy_7; }
	inline LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384 ** get_address_of_YnprvkQpvYdMKdzVQVDfUcJcXFy_7() { return &___YnprvkQpvYdMKdzVQVDfUcJcXFy_7; }
	inline void set_YnprvkQpvYdMKdzVQVDfUcJcXFy_7(LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384 * value)
	{
		___YnprvkQpvYdMKdzVQVDfUcJcXFy_7 = value;
		Il2CppCodeGenWriteBarrier((&___YnprvkQpvYdMKdzVQVDfUcJcXFy_7), value);
	}

	inline static int32_t get_offset_of_uFXeDuanEZKOoogpAMktZMXKVzf_8() { return static_cast<int32_t>(offsetof(xSfYaIWOtPpLHkeMmaZMDWhAcLvg_t5B3392C09884B24F2D82C6DCE566408DC2057427, ___uFXeDuanEZKOoogpAMktZMXKVzf_8)); }
	inline String_t* get_uFXeDuanEZKOoogpAMktZMXKVzf_8() const { return ___uFXeDuanEZKOoogpAMktZMXKVzf_8; }
	inline String_t** get_address_of_uFXeDuanEZKOoogpAMktZMXKVzf_8() { return &___uFXeDuanEZKOoogpAMktZMXKVzf_8; }
	inline void set_uFXeDuanEZKOoogpAMktZMXKVzf_8(String_t* value)
	{
		___uFXeDuanEZKOoogpAMktZMXKVzf_8 = value;
		Il2CppCodeGenWriteBarrier((&___uFXeDuanEZKOoogpAMktZMXKVzf_8), value);
	}

	inline static int32_t get_offset_of_mzLgTYPgqCwurKDKFRDEUSASIMe_9() { return static_cast<int32_t>(offsetof(xSfYaIWOtPpLHkeMmaZMDWhAcLvg_t5B3392C09884B24F2D82C6DCE566408DC2057427, ___mzLgTYPgqCwurKDKFRDEUSASIMe_9)); }
	inline String_t* get_mzLgTYPgqCwurKDKFRDEUSASIMe_9() const { return ___mzLgTYPgqCwurKDKFRDEUSASIMe_9; }
	inline String_t** get_address_of_mzLgTYPgqCwurKDKFRDEUSASIMe_9() { return &___mzLgTYPgqCwurKDKFRDEUSASIMe_9; }
	inline void set_mzLgTYPgqCwurKDKFRDEUSASIMe_9(String_t* value)
	{
		___mzLgTYPgqCwurKDKFRDEUSASIMe_9 = value;
		Il2CppCodeGenWriteBarrier((&___mzLgTYPgqCwurKDKFRDEUSASIMe_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSFYAIWOTPPLHKEMMAZMDWHACLVG_T5B3392C09884B24F2D82C6DCE566408DC2057427_H
#ifndef CONTROLLERWITHMAP_TC7B548CC97EDE21A4083A5A89F7089BC98BEB05A_H
#define CONTROLLERWITHMAP_TC7B548CC97EDE21A4083A5A89F7089BC98BEB05A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerWithMap
struct  ControllerWithMap_tC7B548CC97EDE21A4083A5A89F7089BC98BEB05A  : public Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERWITHMAP_TC7B548CC97EDE21A4083A5A89F7089BC98BEB05A_H
#ifndef INNQWHBBCRTIXAGSHOHNVAVJZPS_T0C9ED2B60365981769A8BB04A1600D47BA6E5A27_H
#define INNQWHBBCRTIXAGSHOHNVAVJZPS_T0C9ED2B60365981769A8BB04A1600D47BA6E5A27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_INnqWhbBCrTiXAGSHOHNvaVJzPs
struct  INnqWhbBCrTiXAGSHOHNvaVJzPs_t0C9ED2B60365981769A8BB04A1600D47BA6E5A27  : public xSfYaIWOtPpLHkeMmaZMDWhAcLvg_t5B3392C09884B24F2D82C6DCE566408DC2057427
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INNQWHBBCRTIXAGSHOHNVAVJZPS_T0C9ED2B60365981769A8BB04A1600D47BA6E5A27_H
#ifndef RGOUSCEFDVFEDKYKBIUYDLNBCQNJ_T93CA4BD2E1428AE948989B2815C92705D1E7701A_H
#define RGOUSCEFDVFEDKYKBIUYDLNBCQNJ_T93CA4BD2E1428AE948989B2815C92705D1E7701A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_RgOUscEFDVFEDkYKbIuydlNBCQnj
struct  RgOUscEFDVFEDkYKbIuydlNBCQnj_t93CA4BD2E1428AE948989B2815C92705D1E7701A  : public bZwgckhapJeZTCwbgoaZUyvmBac_t6904AD0C9C60F59789EBFC1AAACC539BEB16D923
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RGOUSCEFDVFEDKYKBIUYDLNBCQNJ_T93CA4BD2E1428AE948989B2815C92705D1E7701A_H
#ifndef SXVAZCFKQGKAFFPZCOBZNAMRUDUR_T4A9F955BC1B89AC842D0F11D038F273753A77BCB_H
#define SXVAZCFKQGKAFFPZCOBZNAMRUDUR_T4A9F955BC1B89AC842D0F11D038F273753A77BCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_SxvAzCFkqgkafFPzcOBznamRUDUr
struct  SxvAzCFkqgkafFPzcOBznamRUDUr_t4A9F955BC1B89AC842D0F11D038F273753A77BCB  : public dIwzbFNrSfHUmxEwkvfoUNVhkTB_t4BE7E36D57BC02EAB8AF094AA1BCD2A268107B8A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SXVAZCFKQGKAFFPZCOBZNAMRUDUR_T4A9F955BC1B89AC842D0F11D038F273753A77BCB_H
#ifndef WTHOZEHQKMJSRQAYMBMGGOYWAQDW_T4B275BFA133AF988B67A4E85CF96F18967A3AB91_H
#define WTHOZEHQKMJSRQAYMBMGGOYWAQDW_T4B275BFA133AF988B67A4E85CF96F18967A3AB91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_WtHOzEhqKMjSrqaYmbMGGoywaqdw
struct  WtHOzEhqKMjSrqaYmbMGGoywaqdw_t4B275BFA133AF988B67A4E85CF96F18967A3AB91  : public dIwzbFNrSfHUmxEwkvfoUNVhkTB_t4BE7E36D57BC02EAB8AF094AA1BCD2A268107B8A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WTHOZEHQKMJSRQAYMBMGGOYWAQDW_T4B275BFA133AF988B67A4E85CF96F18967A3AB91_H
#ifndef QWIJNHLTAXPTUNNKQBNVQSUEJKQ_TB65E2739D369C3A6F971179555110D5AA0077B61_H
#define QWIJNHLTAXPTUNNKQBNVQSUEJKQ_TB65E2739D369C3A6F971179555110D5AA0077B61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_qWiJNHLTaxptUnnKqBNVqSuEJkQ
struct  qWiJNHLTaxptUnnKqBNVqSuEJkQ_tB65E2739D369C3A6F971179555110D5AA0077B61  : public ZZScxobIubfDJABdCTFegCgTWpeM_t36997E96128051C1F7FB8955C195166A8BA33E33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QWIJNHLTAXPTUNNKQBNVQSUEJKQ_TB65E2739D369C3A6F971179555110D5AA0077B61_H
#ifndef XHXJJWGFLFYVIZOPGKJRAPKIGVH_TEE45EFE5E00595896D02DF5A020D338993A04B17_H
#define XHXJJWGFLFYVIZOPGKJRAPKIGVH_TEE45EFE5E00595896D02DF5A020D338993A04B17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate_xHXjJwgfLfyVIZOpGKJrAPKIGvh
struct  xHXjJwgfLfyVIZOpGKJrAPKIGvh_tEE45EFE5E00595896D02DF5A020D338993A04B17  : public xSfYaIWOtPpLHkeMmaZMDWhAcLvg_t5B3392C09884B24F2D82C6DCE566408DC2057427
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XHXJJWGFLFYVIZOPGKJRAPKIGVH_TEE45EFE5E00595896D02DF5A020D338993A04B17_H
#ifndef CONTROLLERWITHAXES_TCDA27FD69855F6BE3B3D7082423DF80DFB6737EA_H
#define CONTROLLERWITHAXES_TCDA27FD69855F6BE3B3D7082423DF80DFB6737EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerWithAxes
struct  ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA  : public ControllerWithMap_tC7B548CC97EDE21A4083A5A89F7089BC98BEB05A
{
public:
	// System.Int32 Rewired.ControllerWithAxes::_axisCount
	int32_t ____axisCount_30;
	// System.Int32 Rewired.ControllerWithAxes::_axis2DCount
	int32_t ____axis2DCount_31;
	// Rewired.Controller_Axis[] Rewired.ControllerWithAxes::axes
	AxisU5BU5D_t4A9D9CB010B4F05D4472FB918F08E8B1434B7BEB* ___axes_32;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller_Axis> Rewired.ControllerWithAxes::axes_readOnly
	ReadOnlyCollection_1_t2FC4D82F7AABEAB28A14216C3A57818B355A98EB * ___axes_readOnly_33;
	// Rewired.Controller_Axis2D[] Rewired.ControllerWithAxes::axes2D
	Axis2DU5BU5D_t3EA76FC4806A467C9FF20A2DB0C67407FEFFAB01* ___axes2D_34;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller_Axis2D> Rewired.ControllerWithAxes::axes2D_readOnly
	ReadOnlyCollection_1_t0DE413092804EC8FE479436075E87B4EC39398DD * ___axes2D_readOnly_35;
	// Rewired.CalibrationMap Rewired.ControllerWithAxes::_calibrationMap
	CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 * ____calibrationMap_36;
	// System.Single[] Rewired.ControllerWithAxes::RYvBRKhOPyKLQmBIWkATAGQZFsx
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___RYvBRKhOPyKLQmBIWkATAGQZFsx_37;
	// System.UInt32 Rewired.ControllerWithAxes::yONceHIgnOVErPDkLcNXIyVNordO
	uint32_t ___yONceHIgnOVErPDkLcNXIyVNordO_38;
	// System.Func`2<System.Int32,System.Int32> Rewired.ControllerWithAxes::joOWJwHpONIMeHsABaYdHxXiaRK
	Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E * ___joOWJwHpONIMeHsABaYdHxXiaRK_39;

public:
	inline static int32_t get_offset_of__axisCount_30() { return static_cast<int32_t>(offsetof(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA, ____axisCount_30)); }
	inline int32_t get__axisCount_30() const { return ____axisCount_30; }
	inline int32_t* get_address_of__axisCount_30() { return &____axisCount_30; }
	inline void set__axisCount_30(int32_t value)
	{
		____axisCount_30 = value;
	}

	inline static int32_t get_offset_of__axis2DCount_31() { return static_cast<int32_t>(offsetof(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA, ____axis2DCount_31)); }
	inline int32_t get__axis2DCount_31() const { return ____axis2DCount_31; }
	inline int32_t* get_address_of__axis2DCount_31() { return &____axis2DCount_31; }
	inline void set__axis2DCount_31(int32_t value)
	{
		____axis2DCount_31 = value;
	}

	inline static int32_t get_offset_of_axes_32() { return static_cast<int32_t>(offsetof(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA, ___axes_32)); }
	inline AxisU5BU5D_t4A9D9CB010B4F05D4472FB918F08E8B1434B7BEB* get_axes_32() const { return ___axes_32; }
	inline AxisU5BU5D_t4A9D9CB010B4F05D4472FB918F08E8B1434B7BEB** get_address_of_axes_32() { return &___axes_32; }
	inline void set_axes_32(AxisU5BU5D_t4A9D9CB010B4F05D4472FB918F08E8B1434B7BEB* value)
	{
		___axes_32 = value;
		Il2CppCodeGenWriteBarrier((&___axes_32), value);
	}

	inline static int32_t get_offset_of_axes_readOnly_33() { return static_cast<int32_t>(offsetof(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA, ___axes_readOnly_33)); }
	inline ReadOnlyCollection_1_t2FC4D82F7AABEAB28A14216C3A57818B355A98EB * get_axes_readOnly_33() const { return ___axes_readOnly_33; }
	inline ReadOnlyCollection_1_t2FC4D82F7AABEAB28A14216C3A57818B355A98EB ** get_address_of_axes_readOnly_33() { return &___axes_readOnly_33; }
	inline void set_axes_readOnly_33(ReadOnlyCollection_1_t2FC4D82F7AABEAB28A14216C3A57818B355A98EB * value)
	{
		___axes_readOnly_33 = value;
		Il2CppCodeGenWriteBarrier((&___axes_readOnly_33), value);
	}

	inline static int32_t get_offset_of_axes2D_34() { return static_cast<int32_t>(offsetof(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA, ___axes2D_34)); }
	inline Axis2DU5BU5D_t3EA76FC4806A467C9FF20A2DB0C67407FEFFAB01* get_axes2D_34() const { return ___axes2D_34; }
	inline Axis2DU5BU5D_t3EA76FC4806A467C9FF20A2DB0C67407FEFFAB01** get_address_of_axes2D_34() { return &___axes2D_34; }
	inline void set_axes2D_34(Axis2DU5BU5D_t3EA76FC4806A467C9FF20A2DB0C67407FEFFAB01* value)
	{
		___axes2D_34 = value;
		Il2CppCodeGenWriteBarrier((&___axes2D_34), value);
	}

	inline static int32_t get_offset_of_axes2D_readOnly_35() { return static_cast<int32_t>(offsetof(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA, ___axes2D_readOnly_35)); }
	inline ReadOnlyCollection_1_t0DE413092804EC8FE479436075E87B4EC39398DD * get_axes2D_readOnly_35() const { return ___axes2D_readOnly_35; }
	inline ReadOnlyCollection_1_t0DE413092804EC8FE479436075E87B4EC39398DD ** get_address_of_axes2D_readOnly_35() { return &___axes2D_readOnly_35; }
	inline void set_axes2D_readOnly_35(ReadOnlyCollection_1_t0DE413092804EC8FE479436075E87B4EC39398DD * value)
	{
		___axes2D_readOnly_35 = value;
		Il2CppCodeGenWriteBarrier((&___axes2D_readOnly_35), value);
	}

	inline static int32_t get_offset_of__calibrationMap_36() { return static_cast<int32_t>(offsetof(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA, ____calibrationMap_36)); }
	inline CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 * get__calibrationMap_36() const { return ____calibrationMap_36; }
	inline CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 ** get_address_of__calibrationMap_36() { return &____calibrationMap_36; }
	inline void set__calibrationMap_36(CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 * value)
	{
		____calibrationMap_36 = value;
		Il2CppCodeGenWriteBarrier((&____calibrationMap_36), value);
	}

	inline static int32_t get_offset_of_RYvBRKhOPyKLQmBIWkATAGQZFsx_37() { return static_cast<int32_t>(offsetof(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA, ___RYvBRKhOPyKLQmBIWkATAGQZFsx_37)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_RYvBRKhOPyKLQmBIWkATAGQZFsx_37() const { return ___RYvBRKhOPyKLQmBIWkATAGQZFsx_37; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_RYvBRKhOPyKLQmBIWkATAGQZFsx_37() { return &___RYvBRKhOPyKLQmBIWkATAGQZFsx_37; }
	inline void set_RYvBRKhOPyKLQmBIWkATAGQZFsx_37(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___RYvBRKhOPyKLQmBIWkATAGQZFsx_37 = value;
		Il2CppCodeGenWriteBarrier((&___RYvBRKhOPyKLQmBIWkATAGQZFsx_37), value);
	}

	inline static int32_t get_offset_of_yONceHIgnOVErPDkLcNXIyVNordO_38() { return static_cast<int32_t>(offsetof(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA, ___yONceHIgnOVErPDkLcNXIyVNordO_38)); }
	inline uint32_t get_yONceHIgnOVErPDkLcNXIyVNordO_38() const { return ___yONceHIgnOVErPDkLcNXIyVNordO_38; }
	inline uint32_t* get_address_of_yONceHIgnOVErPDkLcNXIyVNordO_38() { return &___yONceHIgnOVErPDkLcNXIyVNordO_38; }
	inline void set_yONceHIgnOVErPDkLcNXIyVNordO_38(uint32_t value)
	{
		___yONceHIgnOVErPDkLcNXIyVNordO_38 = value;
	}

	inline static int32_t get_offset_of_joOWJwHpONIMeHsABaYdHxXiaRK_39() { return static_cast<int32_t>(offsetof(ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA, ___joOWJwHpONIMeHsABaYdHxXiaRK_39)); }
	inline Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E * get_joOWJwHpONIMeHsABaYdHxXiaRK_39() const { return ___joOWJwHpONIMeHsABaYdHxXiaRK_39; }
	inline Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E ** get_address_of_joOWJwHpONIMeHsABaYdHxXiaRK_39() { return &___joOWJwHpONIMeHsABaYdHxXiaRK_39; }
	inline void set_joOWJwHpONIMeHsABaYdHxXiaRK_39(Func_2_t7E959DEAF1C1E69DDAA4CC9284FA7F005E80606E * value)
	{
		___joOWJwHpONIMeHsABaYdHxXiaRK_39 = value;
		Il2CppCodeGenWriteBarrier((&___joOWJwHpONIMeHsABaYdHxXiaRK_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERWITHAXES_TCDA27FD69855F6BE3B3D7082423DF80DFB6737EA_H
#ifndef KEYBOARD_T977AD488FDAD35BD15DC1B397E40BCD64495F004_H
#define KEYBOARD_T977AD488FDAD35BD15DC1B397E40BCD64495F004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Keyboard
struct  Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004  : public ControllerWithMap_tC7B548CC97EDE21A4083A5A89F7089BC98BEB05A
{
public:
	// Rewired.Interfaces.IUnifiedKeyboardSource Rewired.Keyboard::_source
	RuntimeObject* ____source_31;
	// Rewired.ModifierKeyFlags Rewired.Keyboard::currentModfierKeyFlags
	int32_t ___currentModfierKeyFlags_32;
	// Rewired.ModifierKeyFlags Rewired.Keyboard::currentModfierKeyFlagsDouble
	int32_t ___currentModfierKeyFlagsDouble_33;
	// System.Func`2<Rewired.KeyboardKeyCode,System.Int32> Rewired.Keyboard::_getKeyIndexDelegate
	Func_2_t20DE2EBC38186DD09844EAE5633BA6EF803D8900 * ____getKeyIndexDelegate_34;
	// System.Int32[] Rewired.Keyboard::keyCodeToKeyIndex
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___keyCodeToKeyIndex_35;
	// System.Int32 Rewired.Keyboard::maxKeyValue
	int32_t ___maxKeyValue_37;

public:
	inline static int32_t get_offset_of__source_31() { return static_cast<int32_t>(offsetof(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004, ____source_31)); }
	inline RuntimeObject* get__source_31() const { return ____source_31; }
	inline RuntimeObject** get_address_of__source_31() { return &____source_31; }
	inline void set__source_31(RuntimeObject* value)
	{
		____source_31 = value;
		Il2CppCodeGenWriteBarrier((&____source_31), value);
	}

	inline static int32_t get_offset_of_currentModfierKeyFlags_32() { return static_cast<int32_t>(offsetof(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004, ___currentModfierKeyFlags_32)); }
	inline int32_t get_currentModfierKeyFlags_32() const { return ___currentModfierKeyFlags_32; }
	inline int32_t* get_address_of_currentModfierKeyFlags_32() { return &___currentModfierKeyFlags_32; }
	inline void set_currentModfierKeyFlags_32(int32_t value)
	{
		___currentModfierKeyFlags_32 = value;
	}

	inline static int32_t get_offset_of_currentModfierKeyFlagsDouble_33() { return static_cast<int32_t>(offsetof(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004, ___currentModfierKeyFlagsDouble_33)); }
	inline int32_t get_currentModfierKeyFlagsDouble_33() const { return ___currentModfierKeyFlagsDouble_33; }
	inline int32_t* get_address_of_currentModfierKeyFlagsDouble_33() { return &___currentModfierKeyFlagsDouble_33; }
	inline void set_currentModfierKeyFlagsDouble_33(int32_t value)
	{
		___currentModfierKeyFlagsDouble_33 = value;
	}

	inline static int32_t get_offset_of__getKeyIndexDelegate_34() { return static_cast<int32_t>(offsetof(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004, ____getKeyIndexDelegate_34)); }
	inline Func_2_t20DE2EBC38186DD09844EAE5633BA6EF803D8900 * get__getKeyIndexDelegate_34() const { return ____getKeyIndexDelegate_34; }
	inline Func_2_t20DE2EBC38186DD09844EAE5633BA6EF803D8900 ** get_address_of__getKeyIndexDelegate_34() { return &____getKeyIndexDelegate_34; }
	inline void set__getKeyIndexDelegate_34(Func_2_t20DE2EBC38186DD09844EAE5633BA6EF803D8900 * value)
	{
		____getKeyIndexDelegate_34 = value;
		Il2CppCodeGenWriteBarrier((&____getKeyIndexDelegate_34), value);
	}

	inline static int32_t get_offset_of_keyCodeToKeyIndex_35() { return static_cast<int32_t>(offsetof(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004, ___keyCodeToKeyIndex_35)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_keyCodeToKeyIndex_35() const { return ___keyCodeToKeyIndex_35; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_keyCodeToKeyIndex_35() { return &___keyCodeToKeyIndex_35; }
	inline void set_keyCodeToKeyIndex_35(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___keyCodeToKeyIndex_35 = value;
		Il2CppCodeGenWriteBarrier((&___keyCodeToKeyIndex_35), value);
	}

	inline static int32_t get_offset_of_maxKeyValue_37() { return static_cast<int32_t>(offsetof(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004, ___maxKeyValue_37)); }
	inline int32_t get_maxKeyValue_37() const { return ___maxKeyValue_37; }
	inline int32_t* get_address_of_maxKeyValue_37() { return &___maxKeyValue_37; }
	inline void set_maxKeyValue_37(int32_t value)
	{
		___maxKeyValue_37 = value;
	}
};

struct Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004_StaticFields
{
public:
	// Rewired.Keyboard Rewired.Keyboard::singleton
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * ___singleton_30;
	// Rewired.KeyboardKeyCode[] Rewired.Keyboard::__keyIndexToKeyboardKeyCode
	KeyboardKeyCodeU5BU5D_t88F2320800F8F14E7935678C5AA693EEF40514B2* _____keyIndexToKeyboardKeyCode_36;
	// System.Guid Rewired.Keyboard::s_deviceInstanceGuid
	Guid_t  ___s_deviceInstanceGuid_38;

public:
	inline static int32_t get_offset_of_singleton_30() { return static_cast<int32_t>(offsetof(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004_StaticFields, ___singleton_30)); }
	inline Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * get_singleton_30() const { return ___singleton_30; }
	inline Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 ** get_address_of_singleton_30() { return &___singleton_30; }
	inline void set_singleton_30(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004 * value)
	{
		___singleton_30 = value;
		Il2CppCodeGenWriteBarrier((&___singleton_30), value);
	}

	inline static int32_t get_offset_of___keyIndexToKeyboardKeyCode_36() { return static_cast<int32_t>(offsetof(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004_StaticFields, _____keyIndexToKeyboardKeyCode_36)); }
	inline KeyboardKeyCodeU5BU5D_t88F2320800F8F14E7935678C5AA693EEF40514B2* get___keyIndexToKeyboardKeyCode_36() const { return _____keyIndexToKeyboardKeyCode_36; }
	inline KeyboardKeyCodeU5BU5D_t88F2320800F8F14E7935678C5AA693EEF40514B2** get_address_of___keyIndexToKeyboardKeyCode_36() { return &_____keyIndexToKeyboardKeyCode_36; }
	inline void set___keyIndexToKeyboardKeyCode_36(KeyboardKeyCodeU5BU5D_t88F2320800F8F14E7935678C5AA693EEF40514B2* value)
	{
		_____keyIndexToKeyboardKeyCode_36 = value;
		Il2CppCodeGenWriteBarrier((&_____keyIndexToKeyboardKeyCode_36), value);
	}

	inline static int32_t get_offset_of_s_deviceInstanceGuid_38() { return static_cast<int32_t>(offsetof(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004_StaticFields, ___s_deviceInstanceGuid_38)); }
	inline Guid_t  get_s_deviceInstanceGuid_38() const { return ___s_deviceInstanceGuid_38; }
	inline Guid_t * get_address_of_s_deviceInstanceGuid_38() { return &___s_deviceInstanceGuid_38; }
	inline void set_s_deviceInstanceGuid_38(Guid_t  value)
	{
		___s_deviceInstanceGuid_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARD_T977AD488FDAD35BD15DC1B397E40BCD64495F004_H
#ifndef CUSTOMCONTROLLER_TDAEEDDC6632D300249A22FA978C1360DE1EB0C1A_H
#define CUSTOMCONTROLLER_TDAEEDDC6632D300249A22FA978C1360DE1EB0C1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.CustomController
struct  CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A  : public ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA
{
public:
	// System.Int32 Rewired.CustomController::_sourceControllerId
	int32_t ____sourceControllerId_40;
	// System.Func`2<System.Int32,System.Single> Rewired.CustomController::axisUpdateCallback
	Func_2_t3E6DA33748FAB0ECE27E377896D0680D974FE1CF * ___axisUpdateCallback_41;
	// System.Func`2<System.Int32,System.Boolean> Rewired.CustomController::buttonUpdateCallback
	Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * ___buttonUpdateCallback_42;
	// System.Boolean Rewired.CustomController::useUpdateCallbacks
	bool ___useUpdateCallbacks_43;
	// System.Guid Rewired.CustomController::_deviceInstanceGuid
	Guid_t  ____deviceInstanceGuid_44;

public:
	inline static int32_t get_offset_of__sourceControllerId_40() { return static_cast<int32_t>(offsetof(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A, ____sourceControllerId_40)); }
	inline int32_t get__sourceControllerId_40() const { return ____sourceControllerId_40; }
	inline int32_t* get_address_of__sourceControllerId_40() { return &____sourceControllerId_40; }
	inline void set__sourceControllerId_40(int32_t value)
	{
		____sourceControllerId_40 = value;
	}

	inline static int32_t get_offset_of_axisUpdateCallback_41() { return static_cast<int32_t>(offsetof(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A, ___axisUpdateCallback_41)); }
	inline Func_2_t3E6DA33748FAB0ECE27E377896D0680D974FE1CF * get_axisUpdateCallback_41() const { return ___axisUpdateCallback_41; }
	inline Func_2_t3E6DA33748FAB0ECE27E377896D0680D974FE1CF ** get_address_of_axisUpdateCallback_41() { return &___axisUpdateCallback_41; }
	inline void set_axisUpdateCallback_41(Func_2_t3E6DA33748FAB0ECE27E377896D0680D974FE1CF * value)
	{
		___axisUpdateCallback_41 = value;
		Il2CppCodeGenWriteBarrier((&___axisUpdateCallback_41), value);
	}

	inline static int32_t get_offset_of_buttonUpdateCallback_42() { return static_cast<int32_t>(offsetof(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A, ___buttonUpdateCallback_42)); }
	inline Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * get_buttonUpdateCallback_42() const { return ___buttonUpdateCallback_42; }
	inline Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 ** get_address_of_buttonUpdateCallback_42() { return &___buttonUpdateCallback_42; }
	inline void set_buttonUpdateCallback_42(Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * value)
	{
		___buttonUpdateCallback_42 = value;
		Il2CppCodeGenWriteBarrier((&___buttonUpdateCallback_42), value);
	}

	inline static int32_t get_offset_of_useUpdateCallbacks_43() { return static_cast<int32_t>(offsetof(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A, ___useUpdateCallbacks_43)); }
	inline bool get_useUpdateCallbacks_43() const { return ___useUpdateCallbacks_43; }
	inline bool* get_address_of_useUpdateCallbacks_43() { return &___useUpdateCallbacks_43; }
	inline void set_useUpdateCallbacks_43(bool value)
	{
		___useUpdateCallbacks_43 = value;
	}

	inline static int32_t get_offset_of__deviceInstanceGuid_44() { return static_cast<int32_t>(offsetof(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A, ____deviceInstanceGuid_44)); }
	inline Guid_t  get__deviceInstanceGuid_44() const { return ____deviceInstanceGuid_44; }
	inline Guid_t * get_address_of__deviceInstanceGuid_44() { return &____deviceInstanceGuid_44; }
	inline void set__deviceInstanceGuid_44(Guid_t  value)
	{
		____deviceInstanceGuid_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLER_TDAEEDDC6632D300249A22FA978C1360DE1EB0C1A_H
#ifndef JOYSTICK_T88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39_H
#define JOYSTICK_T88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Joystick
struct  Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39  : public ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA
{
public:
	// Rewired.Interfaces.IInputManagerJoystickPublic Rewired.Joystick::_sourceJoystick
	RuntimeObject* ____sourceJoystick_42;
	// Rewired.JoystickType[] Rewired.Joystick::_joystickTypes
	JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA* ____joystickTypes_43;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.JoystickType> Rewired.Joystick::_joystickTypes_readOnly
	ReadOnlyCollection_1_tF2D56725AA006BC417608E3AA831C086DC3FB2F3 * ____joystickTypes_readOnly_44;
	// System.Boolean Rewired.Joystick::_supportsVibration
	bool ____supportsVibration_45;
	// System.Boolean Rewired.Joystick::_supportsLocalVibration
	bool ____supportsLocalVibration_46;
	// System.Boolean Rewired.Joystick::_supportsVoice
	bool ____supportsVoice_47;
	// System.Int32 Rewired.Joystick::_localVibrationMotorCount
	int32_t ____localVibrationMotorCount_48;
	// System.Single[] Rewired.Joystick::_localVibrationMotorValues
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ____localVibrationMotorValues_49;
	// Rewired.Utils.Classes.Utility.TimerAbs[] Rewired.Joystick::_localVibrationStopTimers
	TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC* ____localVibrationStopTimers_50;
	// System.Int32 Rewired.Joystick::_hatCount
	int32_t ____hatCount_51;
	// Rewired.Controller_Hat[] Rewired.Joystick::_hats
	HatU5BU5D_tA98392C24E41A77FBD67D0260A24A3AD18B22B5E* ____hats_52;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Controller_Hat> Rewired.Joystick::hats_readOnly
	ReadOnlyCollection_1_tCB8970A81C163E87CB4AFB390129098C4DA72E82 * ___hats_readOnly_53;

public:
	inline static int32_t get_offset_of__sourceJoystick_42() { return static_cast<int32_t>(offsetof(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39, ____sourceJoystick_42)); }
	inline RuntimeObject* get__sourceJoystick_42() const { return ____sourceJoystick_42; }
	inline RuntimeObject** get_address_of__sourceJoystick_42() { return &____sourceJoystick_42; }
	inline void set__sourceJoystick_42(RuntimeObject* value)
	{
		____sourceJoystick_42 = value;
		Il2CppCodeGenWriteBarrier((&____sourceJoystick_42), value);
	}

	inline static int32_t get_offset_of__joystickTypes_43() { return static_cast<int32_t>(offsetof(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39, ____joystickTypes_43)); }
	inline JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA* get__joystickTypes_43() const { return ____joystickTypes_43; }
	inline JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA** get_address_of__joystickTypes_43() { return &____joystickTypes_43; }
	inline void set__joystickTypes_43(JoystickTypeU5BU5D_t441284FE2D4324E2C6DA63D5E966FB756FA283FA* value)
	{
		____joystickTypes_43 = value;
		Il2CppCodeGenWriteBarrier((&____joystickTypes_43), value);
	}

	inline static int32_t get_offset_of__joystickTypes_readOnly_44() { return static_cast<int32_t>(offsetof(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39, ____joystickTypes_readOnly_44)); }
	inline ReadOnlyCollection_1_tF2D56725AA006BC417608E3AA831C086DC3FB2F3 * get__joystickTypes_readOnly_44() const { return ____joystickTypes_readOnly_44; }
	inline ReadOnlyCollection_1_tF2D56725AA006BC417608E3AA831C086DC3FB2F3 ** get_address_of__joystickTypes_readOnly_44() { return &____joystickTypes_readOnly_44; }
	inline void set__joystickTypes_readOnly_44(ReadOnlyCollection_1_tF2D56725AA006BC417608E3AA831C086DC3FB2F3 * value)
	{
		____joystickTypes_readOnly_44 = value;
		Il2CppCodeGenWriteBarrier((&____joystickTypes_readOnly_44), value);
	}

	inline static int32_t get_offset_of__supportsVibration_45() { return static_cast<int32_t>(offsetof(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39, ____supportsVibration_45)); }
	inline bool get__supportsVibration_45() const { return ____supportsVibration_45; }
	inline bool* get_address_of__supportsVibration_45() { return &____supportsVibration_45; }
	inline void set__supportsVibration_45(bool value)
	{
		____supportsVibration_45 = value;
	}

	inline static int32_t get_offset_of__supportsLocalVibration_46() { return static_cast<int32_t>(offsetof(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39, ____supportsLocalVibration_46)); }
	inline bool get__supportsLocalVibration_46() const { return ____supportsLocalVibration_46; }
	inline bool* get_address_of__supportsLocalVibration_46() { return &____supportsLocalVibration_46; }
	inline void set__supportsLocalVibration_46(bool value)
	{
		____supportsLocalVibration_46 = value;
	}

	inline static int32_t get_offset_of__supportsVoice_47() { return static_cast<int32_t>(offsetof(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39, ____supportsVoice_47)); }
	inline bool get__supportsVoice_47() const { return ____supportsVoice_47; }
	inline bool* get_address_of__supportsVoice_47() { return &____supportsVoice_47; }
	inline void set__supportsVoice_47(bool value)
	{
		____supportsVoice_47 = value;
	}

	inline static int32_t get_offset_of__localVibrationMotorCount_48() { return static_cast<int32_t>(offsetof(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39, ____localVibrationMotorCount_48)); }
	inline int32_t get__localVibrationMotorCount_48() const { return ____localVibrationMotorCount_48; }
	inline int32_t* get_address_of__localVibrationMotorCount_48() { return &____localVibrationMotorCount_48; }
	inline void set__localVibrationMotorCount_48(int32_t value)
	{
		____localVibrationMotorCount_48 = value;
	}

	inline static int32_t get_offset_of__localVibrationMotorValues_49() { return static_cast<int32_t>(offsetof(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39, ____localVibrationMotorValues_49)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get__localVibrationMotorValues_49() const { return ____localVibrationMotorValues_49; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of__localVibrationMotorValues_49() { return &____localVibrationMotorValues_49; }
	inline void set__localVibrationMotorValues_49(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		____localVibrationMotorValues_49 = value;
		Il2CppCodeGenWriteBarrier((&____localVibrationMotorValues_49), value);
	}

	inline static int32_t get_offset_of__localVibrationStopTimers_50() { return static_cast<int32_t>(offsetof(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39, ____localVibrationStopTimers_50)); }
	inline TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC* get__localVibrationStopTimers_50() const { return ____localVibrationStopTimers_50; }
	inline TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC** get_address_of__localVibrationStopTimers_50() { return &____localVibrationStopTimers_50; }
	inline void set__localVibrationStopTimers_50(TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC* value)
	{
		____localVibrationStopTimers_50 = value;
		Il2CppCodeGenWriteBarrier((&____localVibrationStopTimers_50), value);
	}

	inline static int32_t get_offset_of__hatCount_51() { return static_cast<int32_t>(offsetof(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39, ____hatCount_51)); }
	inline int32_t get__hatCount_51() const { return ____hatCount_51; }
	inline int32_t* get_address_of__hatCount_51() { return &____hatCount_51; }
	inline void set__hatCount_51(int32_t value)
	{
		____hatCount_51 = value;
	}

	inline static int32_t get_offset_of__hats_52() { return static_cast<int32_t>(offsetof(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39, ____hats_52)); }
	inline HatU5BU5D_tA98392C24E41A77FBD67D0260A24A3AD18B22B5E* get__hats_52() const { return ____hats_52; }
	inline HatU5BU5D_tA98392C24E41A77FBD67D0260A24A3AD18B22B5E** get_address_of__hats_52() { return &____hats_52; }
	inline void set__hats_52(HatU5BU5D_tA98392C24E41A77FBD67D0260A24A3AD18B22B5E* value)
	{
		____hats_52 = value;
		Il2CppCodeGenWriteBarrier((&____hats_52), value);
	}

	inline static int32_t get_offset_of_hats_readOnly_53() { return static_cast<int32_t>(offsetof(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39, ___hats_readOnly_53)); }
	inline ReadOnlyCollection_1_tCB8970A81C163E87CB4AFB390129098C4DA72E82 * get_hats_readOnly_53() const { return ___hats_readOnly_53; }
	inline ReadOnlyCollection_1_tCB8970A81C163E87CB4AFB390129098C4DA72E82 ** get_address_of_hats_readOnly_53() { return &___hats_readOnly_53; }
	inline void set_hats_readOnly_53(ReadOnlyCollection_1_tCB8970A81C163E87CB4AFB390129098C4DA72E82 * value)
	{
		___hats_readOnly_53 = value;
		Il2CppCodeGenWriteBarrier((&___hats_readOnly_53), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39_H
#ifndef MOUSE_T3B255E519CED8AF070A577BFEEFB5169ECBECBF3_H
#define MOUSE_T3B255E519CED8AF070A577BFEEFB5169ECBECBF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Mouse
struct  Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3  : public ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA
{
public:
	// Rewired.Utils.Classes.Utility.TimerAbs Rewired.Mouse::mouseAxisPollingTimer
	TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * ___mouseAxisPollingTimer_40;
	// System.Single[] Rewired.Mouse::cumulativeMousePollingAxes
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___cumulativeMousePollingAxes_41;
	// UnityEngine.Vector2 Rewired.Mouse::_screenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____screenPosition_42;
	// UnityEngine.Vector2 Rewired.Mouse::_screenPositionPrev
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____screenPositionPrev_43;
	// System.Int32 Rewired.Mouse::_lastScreenPositionUpdateFrame
	int32_t ____lastScreenPositionUpdateFrame_44;
	// Rewired.Interfaces.IUnifiedMouseSource Rewired.Mouse::_source
	RuntimeObject* ____source_45;

public:
	inline static int32_t get_offset_of_mouseAxisPollingTimer_40() { return static_cast<int32_t>(offsetof(Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3, ___mouseAxisPollingTimer_40)); }
	inline TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * get_mouseAxisPollingTimer_40() const { return ___mouseAxisPollingTimer_40; }
	inline TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 ** get_address_of_mouseAxisPollingTimer_40() { return &___mouseAxisPollingTimer_40; }
	inline void set_mouseAxisPollingTimer_40(TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * value)
	{
		___mouseAxisPollingTimer_40 = value;
		Il2CppCodeGenWriteBarrier((&___mouseAxisPollingTimer_40), value);
	}

	inline static int32_t get_offset_of_cumulativeMousePollingAxes_41() { return static_cast<int32_t>(offsetof(Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3, ___cumulativeMousePollingAxes_41)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_cumulativeMousePollingAxes_41() const { return ___cumulativeMousePollingAxes_41; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_cumulativeMousePollingAxes_41() { return &___cumulativeMousePollingAxes_41; }
	inline void set_cumulativeMousePollingAxes_41(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___cumulativeMousePollingAxes_41 = value;
		Il2CppCodeGenWriteBarrier((&___cumulativeMousePollingAxes_41), value);
	}

	inline static int32_t get_offset_of__screenPosition_42() { return static_cast<int32_t>(offsetof(Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3, ____screenPosition_42)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__screenPosition_42() const { return ____screenPosition_42; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__screenPosition_42() { return &____screenPosition_42; }
	inline void set__screenPosition_42(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____screenPosition_42 = value;
	}

	inline static int32_t get_offset_of__screenPositionPrev_43() { return static_cast<int32_t>(offsetof(Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3, ____screenPositionPrev_43)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__screenPositionPrev_43() const { return ____screenPositionPrev_43; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__screenPositionPrev_43() { return &____screenPositionPrev_43; }
	inline void set__screenPositionPrev_43(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____screenPositionPrev_43 = value;
	}

	inline static int32_t get_offset_of__lastScreenPositionUpdateFrame_44() { return static_cast<int32_t>(offsetof(Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3, ____lastScreenPositionUpdateFrame_44)); }
	inline int32_t get__lastScreenPositionUpdateFrame_44() const { return ____lastScreenPositionUpdateFrame_44; }
	inline int32_t* get_address_of__lastScreenPositionUpdateFrame_44() { return &____lastScreenPositionUpdateFrame_44; }
	inline void set__lastScreenPositionUpdateFrame_44(int32_t value)
	{
		____lastScreenPositionUpdateFrame_44 = value;
	}

	inline static int32_t get_offset_of__source_45() { return static_cast<int32_t>(offsetof(Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3, ____source_45)); }
	inline RuntimeObject* get__source_45() const { return ____source_45; }
	inline RuntimeObject** get_address_of__source_45() { return &____source_45; }
	inline void set__source_45(RuntimeObject* value)
	{
		____source_45 = value;
		Il2CppCodeGenWriteBarrier((&____source_45), value);
	}
};

struct Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3_StaticFields
{
public:
	// System.Guid Rewired.Mouse::s_deviceInstanceGuid
	Guid_t  ___s_deviceInstanceGuid_46;

public:
	inline static int32_t get_offset_of_s_deviceInstanceGuid_46() { return static_cast<int32_t>(offsetof(Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3_StaticFields, ___s_deviceInstanceGuid_46)); }
	inline Guid_t  get_s_deviceInstanceGuid_46() const { return ___s_deviceInstanceGuid_46; }
	inline Guid_t * get_address_of_s_deviceInstanceGuid_46() { return &___s_deviceInstanceGuid_46; }
	inline void set_s_deviceInstanceGuid_46(Guid_t  value)
	{
		___s_deviceInstanceGuid_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSE_T3B255E519CED8AF070A577BFEEFB5169ECBECBF3_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4000 = { sizeof (ThrottleCalibrationMode_t9454AA7B39A1EE99DD4B92B4E3A15AE78CD52EF9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4000[3] = 
{
	ThrottleCalibrationMode_t9454AA7B39A1EE99DD4B92B4E3A15AE78CD52EF9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4001 = { sizeof (LogLevelFlags_tD2231B5E28D4ADA489FAE89122340FE9D6755981)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4001[6] = 
{
	LogLevelFlags_tD2231B5E28D4ADA489FAE89122340FE9D6755981::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4002 = { sizeof (LogLevel_tE523370914B61878CEB702CDD7AC1D9D56B8749D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4002[5] = 
{
	LogLevel_tE523370914B61878CEB702CDD7AC1D9D56B8749D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4003 = { sizeof (Consts_t13F3F01F099D81D71412103E16570479844CF780), -1, sizeof(Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4003[286] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_questionablePidVids_253(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_questionableVIDs_254(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_joystickGuid_appleMFiController_255(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_joystickGuid_standardizedGamepad_256(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_joystickGuid_SonyDualShock4_257(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_hardwareTypeGuid_universalKeyboard_258(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_hardwareTypeGuid_universalMouse_259(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_unityMouseElementNames_260(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_oilgvhIIXpJffRBfPRfShHYUoNi_261(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_unityMouseAxisPositiveNames_262(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_iZFGbNBwLBNYUtWVIHzCDUIbCmOQ_263(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_unityMouseAxisNegativeNames_264(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_RHirAoZBvKDVhHaheZciDYnHUEN_265(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_rawInputUnifiedMouseElementNames_266(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_VcnXCLOKxcEFcLrWCgIMroyNYkt_267(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_rawInputUnifiedMouseAxisPositiveNames_268(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_MCEbGtKwlQAUWcpqsvbpNEDKOr_269(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_rawInputUnifiedMouseAxisNegativeNames_270(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_CkoPXBSQrCLFXCCmqQinTbBsLLb_271(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_unityMouseElementIdentifierIds_272(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_fdUCNxJTlLdMkOKZhkqemMmsmyRC_273(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_rawInputUnifiedMouseElementIdentifierIds_274(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_fHduvNyJgsIQEbQYpRLDgrkAqgS_275(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_mouseAxisUnityNames_276(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_AFolGfcvGmlfjHKfLUpiqMPPgMC_277(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_mouseButtonUnityNames_278(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_jBLfNftWBRMiEtPOZmryCzIlXOn_279(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_keyboardKeyNames_280(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_iCvRWnIpVHWjGBXoLDPSyGqWCQh_281(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_keyboardKeyValues_282(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of__keyboardKeyValues_283(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_modifierKeyShortNames_284(),
	Consts_t13F3F01F099D81D71412103E16570479844CF780_StaticFields::get_offset_of_NMLcViElmPXmZReaSpfhyXMuatO_285(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4004 = { sizeof (Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671), -1, sizeof(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4004[30] = 
{
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_id_0(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of__tag_1(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of__name_2(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of__hardwareName_3(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of__type_4(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_hVfvAlYZzngYWNUKHfQeqsdsSsN_5(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of__hardwareIdentifier_6(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of__isConnected_7(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_keBYOCobzoABWhYylPhjApVSLXnN_8(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_9(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_VlCVoudhBiuwhdYzYzMjmHCukZv_10(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_11(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of__buttonCount_12(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_buttons_13(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_buttons_readOnly_14(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_15(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_16(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_lHdmDauerBwdDwLUcaCxBythtdWu_17(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_NYtqDwOoerMRNEOsCUlBWwihMNG_18(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_motlnrXwRclwbbdquGWkOEYsYFy_19(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_chxPGnvGYohtyammAxrWAboQuVg_20(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_iaxnzSzdijUTJnJzreKDdKvgbmk_21(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_lMdEFUXdOFLrujmaUFFpIWQxdWyh_22(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_yQeGjgudbMdGxktFDlTRKRPYNpwO_23(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_RrVywEyDCOVEzMNbWtqScfAjGSfA_24(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671::get_offset_of_PWJCQVymGuHZlwtENLzIkOlXzFO_25(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields::get_offset_of_kpwpfTuXUblRlYTjfSeSLEksNTd_26(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields::get_offset_of_fpQFUcijUIunFTiZNbdIrMQqJvMf_27(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields::get_offset_of_jqUTJMOeCBaPwenidONjsgqqmhF_28(),
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671_StaticFields::get_offset_of_yGfaFeDTgjQCiyfEnFKwcIrTEKK_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4005 = { sizeof (Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4005[7] = 
{
	Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5::get_offset_of_id_0(),
	Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5::get_offset_of_name_1(),
	Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5::get_offset_of_type_2(),
	Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5::get_offset_of_agfbgFjcERRTUxQhZFpdHagNwfl_3(),
	Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5::get_offset_of_XttWneCpteIqXNlWieWWCeqJLle_4(),
	Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5::get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_5(),
	Element_t55D35599A6B182030EA05B6CA5AB494E24ACA2E5::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4006 = { sizeof (ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4006[7] = 
{
	ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416::get_offset_of_hBucQzYvWtOAWgdnwwVORAQRwyd_0(),
	ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416::get_offset_of_bJGqRCGyLwMdbyXFtRqWyHanjhF_1(),
	ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416::get_offset_of_XzwvNxyYkXcaiidxwJGvQNwBigT_2(),
	ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416::get_offset_of_YaDAnfimULzphKnSejtwEcfSlkEJ_3(),
	ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416::get_offset_of_uiElHIMsJhOUGwxstGSvtQgdJHm_4(),
	ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416::get_offset_of_mitExlcuToNzViGEORWHsMktptx_5(),
	ViIrqleiiIiQeymndbeHmwQgupz_tB86214FDB5CEBF09EAAB8A6D8AB1AD69D1175416::get_offset_of_xnWxJKPxjErcevqVtlosHZHrsO_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4007 = { sizeof (LgyDESrvlXkrldhlJfbWEsuKfeVY_t861AA9B44D7A8C3C4D68C812E433DD1D8F09803D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4008 = { sizeof (Axis_t1E0EEBF040B05CB6932311D5F7233C785BB6BEC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4008[2] = 
{
	Axis_t1E0EEBF040B05CB6932311D5F7233C785BB6BEC2::get_offset_of_vxFkLJbJCCLNaDtBkbHBIBaBIFQx_7(),
	Axis_t1E0EEBF040B05CB6932311D5F7233C785BB6BEC2::get_offset_of_WXHyRtmmNLOjAyGOdSOPfTCVJpE_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4009 = { sizeof (KOCBmwikfCzewiyUFHSozJiEHTIj_t323E34E602CC0412547FC6147CDCF72A6613EC5F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4010 = { sizeof (zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4010[13] = 
{
	0,
	zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664::get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_1(),
	zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664::get_offset_of_onYGDjMoebGBXHApYQtbIBUNPzwH_2(),
	zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664::get_offset_of_aYeFKQaDJsBdYbdxdccnalEBhTdw_3(),
	zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664::get_offset_of_PudYZCFEKJBeUWywNbBYFyWYBJt_4(),
	zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664::get_offset_of_ZhJUsMLFILMLypTfQsPsELJqNPn_5(),
	zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664::get_offset_of_qcqAgGtAMdifMYRQFFkQlthQdow_6(),
	zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664::get_offset_of_VzEHWjEtvrwkkGwcOYbRCXOdcQD_7(),
	zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664::get_offset_of_dIqFoxehjyutbFetSezzpojUnOd_8(),
	zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664::get_offset_of_yFYxFqUsPfdYYvTkIijWBOiMXsY_9(),
	zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664::get_offset_of_qXFGuRbHaDptgrXRrKFonlRQxQa_10(),
	zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664::get_offset_of_RbFcQJoIgvfPWSkMrFJeGwvJPMCF_11(),
	zdpuSuzVqBSeotfCVpsnSYHxqlb_t271C64225BFFAD98AA24BCB4C8CE5AB36EA26664::get_offset_of_HVDpkXFtDumDetiFRnhwHQlruSe_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4011 = { sizeof (Button_t78CBA8C35AB8697397D63BF8CF63EB251C4FE457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4011[2] = 
{
	Button_t78CBA8C35AB8697397D63BF8CF63EB251C4FE457::get_offset_of_nhcfgKoolkfQhlQolAJyYBOalZj_7(),
	Button_t78CBA8C35AB8697397D63BF8CF63EB251C4FE457::get_offset_of_wMidbXbtClOiqHSBrJEybRddXCQs_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4012 = { sizeof (TSRYxaSynKkxLGzOfANSlEfXenS_t86EC68CC7A26C51CF8A25E0868E2A3A4E31A78D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4013 = { sizeof (pplxxtNsVaSNdvjXsiaZkikOjTt_t6CD8DE45D90987D75EE26FEFB110FC910A94C2A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4013[4] = 
{
	pplxxtNsVaSNdvjXsiaZkikOjTt_t6CD8DE45D90987D75EE26FEFB110FC910A94C2A7::get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_0(),
	pplxxtNsVaSNdvjXsiaZkikOjTt_t6CD8DE45D90987D75EE26FEFB110FC910A94C2A7::get_offset_of_onYGDjMoebGBXHApYQtbIBUNPzwH_1(),
	pplxxtNsVaSNdvjXsiaZkikOjTt_t6CD8DE45D90987D75EE26FEFB110FC910A94C2A7::get_offset_of_cvUezygDxrEziLDuCyJRvkQuBlTD_2(),
	pplxxtNsVaSNdvjXsiaZkikOjTt_t6CD8DE45D90987D75EE26FEFB110FC910A94C2A7::get_offset_of_bQWchkeBhmcXPRmhgvECfWjozcXw_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4014 = { sizeof (lffDFXIvwcOeAGrPrdjgaLfFnjCH_t48AEF236799222E249090C8A32B297FD292F8174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4014[2] = 
{
	lffDFXIvwcOeAGrPrdjgaLfFnjCH_t48AEF236799222E249090C8A32B297FD292F8174::get_offset_of_HughelYTlpIBvxzJFnUqfWyvfca_4(),
	lffDFXIvwcOeAGrPrdjgaLfFnjCH_t48AEF236799222E249090C8A32B297FD292F8174::get_offset_of_axNcSentbHTBzAxIYzGTkwfaHSE_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4015 = { sizeof (CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4015[7] = 
{
	CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA::get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_0(),
	CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA::get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_1(),
	CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA::get_offset_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_2(),
	CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA::get_offset_of_prxRdKvUePoVtgFVPLYBrpHztMr_3(),
	CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA::get_offset_of_muHfgZrAoceOXsMhCljOQSVcJGk_4(),
	CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA::get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_5(),
	CompoundElement_t210CC0E132785075DA64D166B02AFE1D6ECBC3BA::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4016 = { sizeof (OqivKqJqmnbJCfstNgONqfwyygM_tEAF2F982AA4612530E700293E930ABD6AEBF3B1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4016[2] = 
{
	OqivKqJqmnbJCfstNgONqfwyygM_tEAF2F982AA4612530E700293E930ABD6AEBF3B1B::get_offset_of_YCBcgPOLoWsaeULLzpTLswYuYbQ_0(),
	OqivKqJqmnbJCfstNgONqfwyygM_tEAF2F982AA4612530E700293E930ABD6AEBF3B1B::get_offset_of_nRRgINdksoiMPGmVMZmtHuHLdkgI_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4017 = { sizeof (Axis2D_t6A78C56910F95EB75AA0F947EA910C4D6EBA7756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4017[2] = 
{
	0,
	Axis2D_t6A78C56910F95EB75AA0F947EA910C4D6EBA7756::get_offset_of_GloxFTNLDyfkAYIBdLQDdtRmDqh_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4018 = { sizeof (Hat_tBFF0311A328F874AF529CC3E7E433ABBA9618BFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4018[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Hat_tBFF0311A328F874AF529CC3E7E433ABBA9618BFD::get_offset_of_UuxMEBaYYjrHTBlndFvGcptaFref_16(),
	Hat_tBFF0311A328F874AF529CC3E7E433ABBA9618BFD::get_offset_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_17(),
	Hat_tBFF0311A328F874AF529CC3E7E433ABBA9618BFD::get_offset_of_zyezhlpxlGSWpqZrzsjFXrQGANV_18(),
	Hat_tBFF0311A328F874AF529CC3E7E433ABBA9618BFD::get_offset_of_slFYKRaWbLbVpueSIalPBHdcNrnA_19(),
	Hat_tBFF0311A328F874AF529CC3E7E433ABBA9618BFD::get_offset_of_IIUdmUgsuNIiSukBSWRAfmwzjEo_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4019 = { sizeof (Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4019[3] = 
{
	Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD::get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_0(),
	Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD::get_offset_of_gkqUnrSMeLSOlffXjmHcoYDAlQT_1(),
	Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD::get_offset_of__reInputId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4020 = { sizeof (DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4020[6] = 
{
	DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04::get_offset_of_okMFupfyoBNZDyKMyvAtwGIUiAd_4(),
	DmnGJFFSqZLlhvDwdEsqOrBzhQYm_t77780D7B4EDECB26ACA0D75D3D00859112FA2B04::get_offset_of_xHCilCRdZxQtYjPcCdudbctXZFs_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4021 = { sizeof (QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4021[6] = 
{
	QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8::get_offset_of_kTJJZJZwOLwTeTZLVgkhUriUmfS_4(),
	QBxIidkSqpvGsSBrMNESDNpCbgOt_t91C3E09AA6A7E8156010A7397E5584D255CE47B8::get_offset_of_OVugcnIKhqBnpgCbITPtTrvSLaT_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4022 = { sizeof (ControllerWithMap_tC7B548CC97EDE21A4083A5A89F7089BC98BEB05A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023 = { sizeof (ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4023[10] = 
{
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA::get_offset_of__axisCount_30(),
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA::get_offset_of__axis2DCount_31(),
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA::get_offset_of_axes_32(),
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA::get_offset_of_axes_readOnly_33(),
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA::get_offset_of_axes2D_34(),
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA::get_offset_of_axes2D_readOnly_35(),
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA::get_offset_of__calibrationMap_36(),
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA::get_offset_of_RYvBRKhOPyKLQmBIWkATAGQZFsx_37(),
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA::get_offset_of_yONceHIgnOVErPDkLcNXIyVNordO_38(),
	ControllerWithAxes_tCDA27FD69855F6BE3B3D7082423DF80DFB6737EA::get_offset_of_joOWJwHpONIMeHsABaYdHxXiaRK_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024 = { sizeof (FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4024[8] = 
{
	FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E::get_offset_of_BVlhkuUrRIekOUuHScNQyxbyOfu_4(),
	FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E::get_offset_of_DzLLFEghsbvQsAIVtiTJfpVcUcv_5(),
	FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E::get_offset_of_zCSdpqWqddovuMgwXAspBmdHeqjU_6(),
	FflkIMylYekwciXtGCJvZWRSaVc_t6216AFA82C00EA53FA32D0BDA30F9B1FF868289E::get_offset_of_GFlKUhIfsrNtSvqPGiPCGTHELDOP_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025 = { sizeof (cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4025[8] = 
{
	cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B::get_offset_of_vCzGEQLpDyCakhWGGSKDIApvFIGB_4(),
	cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B::get_offset_of_MYbsRdJqOsruJPqDMcqcpriuBNZ_5(),
	cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B::get_offset_of_ErSDOSEebFXZngDEPXYtFPIADhic_6(),
	cfqIKYOicOMvpYQUcNdHhHlKwlv_tAB98DA2DBDB052DCA815CC7E9976DDEEED19889B::get_offset_of_NoTAyTxmULscBYhxegWobtJUNJhc_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026 = { sizeof (dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4026[7] = 
{
	dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238::get_offset_of_vLHeOKbAsNDmvwORrSjMmEJWlNtc_4(),
	dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238::get_offset_of_kvNWPugkqNwMBtMEUHQgoecgPEH_5(),
	dbMPlGqPPfqJQIDMCLVcnwPzALW_t4BD6F305F630CA731CB9263A7D123956003A2238::get_offset_of_qbrmVnPLFWJXzrauyOLtmgkTYiE_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027 = { sizeof (CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4027[5] = 
{
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A::get_offset_of__sourceControllerId_40(),
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A::get_offset_of_axisUpdateCallback_41(),
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A::get_offset_of_buttonUpdateCallback_42(),
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A::get_offset_of_useUpdateCallbacks_43(),
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A::get_offset_of__deviceInstanceGuid_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030 = { sizeof (DualShock4Extension_tDF6532F36F998F4D62A88536ED8244AB6C3BCD7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4030[3] = 
{
	DualShock4Extension_tDF6532F36F998F4D62A88536ED8244AB6C3BCD7E::get_offset_of_gkqUnrSMeLSOlffXjmHcoYDAlQT_3(),
	DualShock4Extension_tDF6532F36F998F4D62A88536ED8244AB6C3BCD7E::get_offset_of_EdmoIszvSQiLETihGiwjrkdiQJU_4(),
	DualShock4Extension_tDF6532F36F998F4D62A88536ED8244AB6C3BCD7E::get_offset_of_CdVWyCtwdYqRItEtEoYUUDYOAqxD_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031 = { sizeof (tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4031[3] = 
{
	tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D::get_offset_of_WKrdOrHChcdoEToFNLUKCQfYSQj_0(),
	tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D::get_offset_of_ZsEfMISqALipCaWIrZdQwCvMLkx_1(),
	tZdHWjEZWrnwXHpafbBUORQRReH_tEA1396796E329B2BCB1FA7C4F2F2B9A8B50BA30D::get_offset_of_YJKKCodZAAdHLAznZhhKAnRrzmS_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033 = { sizeof (DualShock4MotorType_tC3A25346F0A0B4B33211B4705E345D34D4D39D6D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4033[5] = 
{
	DualShock4MotorType_tC3A25346F0A0B4B33211B4705E345D34D4D39D6D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034 = { sizeof (RailDriverExtension_tDAC9FCBEF64FC452942C77A592436F630B90D3EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4034[1] = 
{
	RailDriverExtension_tDAC9FCBEF64FC452942C77A592436F630B90D3EE::get_offset_of_zSCjKsgSKyXaLUKczdjHusCWAAC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035 = { sizeof (ffvcqZUeADdPtkxnXkBtOZLlBgQx_tD4D732B3A84E0F9D7F1391C9276759C09B2B03E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4035[1] = 
{
	ffvcqZUeADdPtkxnXkBtOZLlBgQx_tD4D732B3A84E0F9D7F1391C9276759C09B2B03E6::get_offset_of_WKrdOrHChcdoEToFNLUKCQfYSQj_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036 = { sizeof (SteamControllerPadType_t1251D69AA1C544283A77D2E12966B387B3F535B9)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4036[3] = 
{
	SteamControllerPadType_t1251D69AA1C544283A77D2E12966B387B3F535B9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037 = { sizeof (SteamControllerActionOrigin_tEB8F62A1D2A8DA6EF474542244C27C157E9126D1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4037[198] = 
{
	SteamControllerActionOrigin_tEB8F62A1D2A8DA6EF474542244C27C157E9126D1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038 = { sizeof (SteamControllerExtension_t0C1E15C33BE26F8C7AD76DCC8130466B3A108207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4038[1] = 
{
	SteamControllerExtension_t0C1E15C33BE26F8C7AD76DCC8130466B3A108207::get_offset_of_zSCjKsgSKyXaLUKczdjHusCWAAC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039 = { sizeof (tBEetyAkXzmxTaZXitHgYebQLVui_tCBAE66D0EF5D308109B13047A13AE8C86F80185E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4039[1] = 
{
	tBEetyAkXzmxTaZXitHgYebQLVui_tCBAE66D0EF5D308109B13047A13AE8C86F80185E::get_offset_of_saVMPymulxTFDfqZAdAfUKUVFQp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040 = { sizeof (Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4040[14] = 
{
	0,
	0,
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39::get_offset_of__sourceJoystick_42(),
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39::get_offset_of__joystickTypes_43(),
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39::get_offset_of__joystickTypes_readOnly_44(),
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39::get_offset_of__supportsVibration_45(),
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39::get_offset_of__supportsLocalVibration_46(),
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39::get_offset_of__supportsVoice_47(),
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39::get_offset_of__localVibrationMotorCount_48(),
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39::get_offset_of__localVibrationMotorValues_49(),
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39::get_offset_of__localVibrationStopTimers_50(),
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39::get_offset_of__hatCount_51(),
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39::get_offset_of__hats_52(),
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39::get_offset_of_hats_readOnly_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041 = { sizeof (Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004), -1, sizeof(Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4041[9] = 
{
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004_StaticFields::get_offset_of_singleton_30(),
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004::get_offset_of__source_31(),
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004::get_offset_of_currentModfierKeyFlags_32(),
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004::get_offset_of_currentModfierKeyFlagsDouble_33(),
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004::get_offset_of__getKeyIndexDelegate_34(),
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004::get_offset_of_keyCodeToKeyIndex_35(),
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004_StaticFields::get_offset_of___keyIndexToKeyboardKeyCode_36(),
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004::get_offset_of_maxKeyValue_37(),
	Keyboard_t977AD488FDAD35BD15DC1B397E40BCD64495F004_StaticFields::get_offset_of_s_deviceInstanceGuid_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042 = { sizeof (U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4042[8] = 
{
	U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B::get_offset_of_U3CU3E2__current_0(),
	U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B::get_offset_of_U3CU3E1__state_1(),
	U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B::get_offset_of_U3CU3E4__this_3(),
	U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B::get_offset_of_U3CcountU3E5__1_4(),
	U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B::get_offset_of_U3CiU3E5__2_5(),
	U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B::get_offset_of_U3CkeyCodeU3E5__3_6(),
	U3CPollForAllKeysU3Ed__0_t779D79463929DAB2B0CEEBFBDC0DE7DA8BC65A7B::get_offset_of_U3CvalueU3E5__4_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043 = { sizeof (U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4043[8] = 
{
	U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006::get_offset_of_U3CU3E2__current_0(),
	U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006::get_offset_of_U3CU3E1__state_1(),
	U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006::get_offset_of_U3CU3E4__this_3(),
	U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006::get_offset_of_U3CcountU3E5__8_4(),
	U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006::get_offset_of_U3CiU3E5__9_5(),
	U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006::get_offset_of_U3CkeyCodeU3E5__a_6(),
	U3CPollForAllKeysDownU3Ed__7_tC694B70AC0E28BABB75E38FB1344F4503490B006::get_offset_of_U3CvalueU3E5__b_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044 = { sizeof (Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3), -1, sizeof(Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4044[7] = 
{
	Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3::get_offset_of_mouseAxisPollingTimer_40(),
	Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3::get_offset_of_cumulativeMousePollingAxes_41(),
	Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3::get_offset_of__screenPosition_42(),
	Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3::get_offset_of__screenPositionPrev_43(),
	Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3::get_offset_of__lastScreenPositionUpdateFrame_44(),
	Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3::get_offset_of__source_45(),
	Mouse_t3B255E519CED8AF070A577BFEEFB5169ECBECBF3_StaticFields::get_offset_of_s_deviceInstanceGuid_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046 = { sizeof (ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4046[8] = 
{
	ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98::get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_0(),
	ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98::get_offset_of_IVAjJNOdTXXjgnowouzEZvvrKDh_1(),
	ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98::get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_2(),
	ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98::get_offset_of_NOXxtGrpSqMcIFZTsrJcnLlMjrR_3(),
	ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98::get_offset_of_jQsjEfMifTDFgHVpZohgCksDsnei_4(),
	ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98::get_offset_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_5(),
	ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98::get_offset_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_6(),
	ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047 = { sizeof (hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4047[5] = 
{
	hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361::get_offset_of_waEhkaNJermXjlpTevBkHdlvdCD_0(),
	hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361::get_offset_of_WHCIpuTfwdHNjfJipsoGkINikLt_1(),
	hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361::get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_2(),
	hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361::get_offset_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_3(),
	hkuffRDZsiyCBEXTWgHFcjFwHWW_tE89E11B59A913B5CBB2A4D7DFC43CF79E9B78361::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048 = { sizeof (xBtqKGHjDTDwJbtJNFxTfMwVtyXb_tAFD13155A620AB3F978D566FDCBB681CE95A6948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4048[2] = 
{
	xBtqKGHjDTDwJbtJNFxTfMwVtyXb_tAFD13155A620AB3F978D566FDCBB681CE95A6948::get_offset_of_mBlHhaIUcxyZnUQmkSHJiOjfLOB_5(),
	xBtqKGHjDTDwJbtJNFxTfMwVtyXb_tAFD13155A620AB3F978D566FDCBB681CE95A6948::get_offset_of_evyqkhhlRJKGPpArmfuDFVDbvFQ_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049 = { sizeof (xSfYaIWOtPpLHkeMmaZMDWhAcLvg_t5B3392C09884B24F2D82C6DCE566408DC2057427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4049[3] = 
{
	xSfYaIWOtPpLHkeMmaZMDWhAcLvg_t5B3392C09884B24F2D82C6DCE566408DC2057427::get_offset_of_YnprvkQpvYdMKdzVQVDfUcJcXFy_7(),
	xSfYaIWOtPpLHkeMmaZMDWhAcLvg_t5B3392C09884B24F2D82C6DCE566408DC2057427::get_offset_of_uFXeDuanEZKOoogpAMktZMXKVzf_8(),
	xSfYaIWOtPpLHkeMmaZMDWhAcLvg_t5B3392C09884B24F2D82C6DCE566408DC2057427::get_offset_of_mzLgTYPgqCwurKDKFRDEUSASIMe_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050 = { sizeof (INnqWhbBCrTiXAGSHOHNvaVJzPs_t0C9ED2B60365981769A8BB04A1600D47BA6E5A27), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051 = { sizeof (xHXjJwgfLfyVIZOpGKJrAPKIGvh_tEE45EFE5E00595896D02DF5A020D338993A04B17), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052 = { sizeof (NfVUTdUMeYeRXJDOUspqsAHENMt_tE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4052[2] = 
{
	NfVUTdUMeYeRXJDOUspqsAHENMt_tE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC::get_offset_of_prxRdKvUePoVtgFVPLYBrpHztMr_5(),
	NfVUTdUMeYeRXJDOUspqsAHENMt_tE92ECD8249DBD6C544BAF9F3C51574ADDE80D1CC::get_offset_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053 = { sizeof (dIwzbFNrSfHUmxEwkvfoUNVhkTB_t4BE7E36D57BC02EAB8AF094AA1BCD2A268107B8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4053[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054 = { sizeof (bZwgckhapJeZTCwbgoaZUyvmBac_t6904AD0C9C60F59789EBFC1AAACC539BEB16D923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4054[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055 = { sizeof (ZZScxobIubfDJABdCTFegCgTWpeM_t36997E96128051C1F7FB8955C195166A8BA33E33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4055[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056 = { sizeof (RgOUscEFDVFEDkYKbIuydlNBCQnj_t93CA4BD2E1428AE948989B2815C92705D1E7701A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4056[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057 = { sizeof (SxvAzCFkqgkafFPzcOBznamRUDUr_t4A9F955BC1B89AC842D0F11D038F273753A77BCB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4057[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058 = { sizeof (uhYVdRLcNxOtEHFQiRcwlAibmvQ_t111272E40546E3E745B37B768C719637CD36BB35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4058[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059 = { sizeof (lyekGvCdjbBGxpnyQFAvBsjfJMaw_t7C80FB69C9335C24D04413C4FDB247F19846E46B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4059[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060 = { sizeof (GCUuallDtDNqoAUGVtUMUJdqukM_t330424FF28C1320876DAE843D96B438FB2A79A35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4060[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061 = { sizeof (WtHOzEhqKMjSrqaYmbMGGoywaqdw_t4B275BFA133AF988B67A4E85CF96F18967A3AB91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4061[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062 = { sizeof (qWiJNHLTaxptUnnKqBNVqSuEJkQ_tB65E2739D369C3A6F971179555110D5AA0077B61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4062[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063 = { sizeof (GfqjFzggDiRITiiqcxubCCJucSZL_t528C22A61812E797D0E6B022915EBE59FF18742D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4063[2] = 
{
	GfqjFzggDiRITiiqcxubCCJucSZL_t528C22A61812E797D0E6B022915EBE59FF18742D::get_offset_of_YCBcgPOLoWsaeULLzpTLswYuYbQ_0(),
	GfqjFzggDiRITiiqcxubCCJucSZL_t528C22A61812E797D0E6B022915EBE59FF18742D::get_offset_of_YsciKTkdsyeKqevEdLRABIojdVHx_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064 = { sizeof (oVctktPaVrYbbqcFmUSnIGzUzYN_tFBE7311BB2977AE8D51BF394298877340699D6EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4064[2] = 
{
	oVctktPaVrYbbqcFmUSnIGzUzYN_tFBE7311BB2977AE8D51BF394298877340699D6EA::get_offset_of_QaeAlqSBKVmDRaDTcYzPlckcanC_0(),
	oVctktPaVrYbbqcFmUSnIGzUzYN_tFBE7311BB2977AE8D51BF394298877340699D6EA::get_offset_of_RsUadSyOfwghqaAMaOLCTYUuHka_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4079 = { sizeof (ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49), -1, sizeof(ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4079[6] = 
{
	ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49::get_offset_of_WHCIpuTfwdHNjfJipsoGkINikLt_0(),
	ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49::get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_1(),
	ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49::get_offset_of_eWTjKkaJVjCZqrsQHQXMtJYuNhD_2(),
	ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49::get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_3(),
	ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49::get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_4(),
	ControllerTemplateActionElementMap_t1439E6E2414F53444D2DE3350DD2B9CAFFF26F49_StaticFields::get_offset_of_vzbvBVwssABPkQFVkXwztQaEVuB_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4080 = { sizeof (ControllerTemplateActionButtonMap_t16DD6328C5BE272917E348DD67578B7486270D77), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4080[1] = 
{
	ControllerTemplateActionButtonMap_t16DD6328C5BE272917E348DD67578B7486270D77::get_offset_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4081 = { sizeof (ControllerTemplateActionAxisMap_t311F982CCAEDB0D25D6CAD1ABD4CE49E6BF2992C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4081[3] = 
{
	ControllerTemplateActionAxisMap_t311F982CCAEDB0D25D6CAD1ABD4CE49E6BF2992C::get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_6(),
	ControllerTemplateActionAxisMap_t311F982CCAEDB0D25D6CAD1ABD4CE49E6BF2992C::get_offset_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_7(),
	ControllerTemplateActionAxisMap_t311F982CCAEDB0D25D6CAD1ABD4CE49E6BF2992C::get_offset_of_GfvPtFqTnCIcrWebHAQzqOoKceI_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4082 = { sizeof (ControllerTemplateSpecialElementMapping_t35C3BB93612616CCEAEF857E70C5824933B85390), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4083 = { sizeof (ControllerTemplateThumbStickMapping_t278A01E802FCFDD0E3F3DA0071E06AE934D2D63A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4083[3] = 
{
	ControllerTemplateThumbStickMapping_t278A01E802FCFDD0E3F3DA0071E06AE934D2D63A::get_offset_of_eid_axisX_0(),
	ControllerTemplateThumbStickMapping_t278A01E802FCFDD0E3F3DA0071E06AE934D2D63A::get_offset_of_eid_axisY_1(),
	ControllerTemplateThumbStickMapping_t278A01E802FCFDD0E3F3DA0071E06AE934D2D63A::get_offset_of_eid_button_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4084 = { sizeof (ControllerTemplateDPadMapping_t03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4084[5] = 
{
	ControllerTemplateDPadMapping_t03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C::get_offset_of_eid_up_0(),
	ControllerTemplateDPadMapping_t03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C::get_offset_of_eid_right_1(),
	ControllerTemplateDPadMapping_t03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C::get_offset_of_eid_down_2(),
	ControllerTemplateDPadMapping_t03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C::get_offset_of_eid_left_3(),
	ControllerTemplateDPadMapping_t03D4F439390A611B5C0F6E69C81F5CB81A0C8E2C::get_offset_of_eid_press_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4085 = { sizeof (ControllerTemplateStickMapping_t1847BAF56312244E2E51C54DFF9028F671528CEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4085[3] = 
{
	ControllerTemplateStickMapping_t1847BAF56312244E2E51C54DFF9028F671528CEF::get_offset_of_eid_axisX_0(),
	ControllerTemplateStickMapping_t1847BAF56312244E2E51C54DFF9028F671528CEF::get_offset_of_eid_axisY_1(),
	ControllerTemplateStickMapping_t1847BAF56312244E2E51C54DFF9028F671528CEF::get_offset_of_eid_axisZ_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4086 = { sizeof (ControllerTemplateThrottleMapping_t37A060630236F3393B3A986301C804A848F82866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4086[2] = 
{
	ControllerTemplateThrottleMapping_t37A060630236F3393B3A986301C804A848F82866::get_offset_of_eid_axis_0(),
	ControllerTemplateThrottleMapping_t37A060630236F3393B3A986301C804A848F82866::get_offset_of_eid_minDetent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4087 = { sizeof (ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4087[8] = 
{
	ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08::get_offset_of_eid_up_0(),
	ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08::get_offset_of_eid_upRight_1(),
	ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08::get_offset_of_eid_right_2(),
	ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08::get_offset_of_eid_downRight_3(),
	ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08::get_offset_of_eid_down_4(),
	ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08::get_offset_of_eid_downLeft_5(),
	ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08::get_offset_of_eid_left_6(),
	ControllerTemplateHatMapping_t5B2290EA4E925630DE2E6D525E777E0C719CBB08::get_offset_of_eid_upLeft_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4088 = { sizeof (ControllerTemplateYokeMapping_tECBF73125DA9F22DDCB5B6FB11764D2ED0D558FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4088[2] = 
{
	ControllerTemplateYokeMapping_tECBF73125DA9F22DDCB5B6FB11764D2ED0D558FF::get_offset_of_eid_axisX_0(),
	ControllerTemplateYokeMapping_tECBF73125DA9F22DDCB5B6FB11764D2ED0D558FF::get_offset_of_eid_axisZ_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4089 = { sizeof (ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4089[6] = 
{
	ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15::get_offset_of_eid_positionX_0(),
	ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15::get_offset_of_eid_positionY_1(),
	ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15::get_offset_of_eid_positionZ_2(),
	ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15::get_offset_of_eid_rotationX_3(),
	ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15::get_offset_of_eid_rotationY_4(),
	ControllerTemplateStick6DMapping_t608ECF2EF4917548DB725B125B69663F41060A15::get_offset_of_eid_rotationZ_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4090 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4091 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4092 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4093 = { sizeof (LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4093[5] = 
{
	LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384::get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_0(),
	LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384::get_offset_of_eznOANlIXKDtOGirosRhYGEDDrn_1(),
	LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384::get_offset_of_YnprvkQpvYdMKdzVQVDfUcJcXFy_2(),
	LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384::get_offset_of_VTSBUugZkJasAwWRIbsEjuePrNWv_3(),
	LJAWzBuJIsAgLPPZeEUKfpasUWYi_tC17A4E1E7A8A558859C293A84360B1DB84D83384::get_offset_of_QIobvlUKxZwRHGhPgtUxpMGNvvb_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4094 = { sizeof (ControllerTemplateElementTarget_t4E640D6AEA752EBCA12DCEDB931EE315E8E8B676)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4094[2] = 
{
	ControllerTemplateElementTarget_t4E640D6AEA752EBCA12DCEDB931EE315E8E8B676::get_offset_of_ZljBzOgnBdZbhDAoXUvCVQsaWkR_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerTemplateElementTarget_t4E640D6AEA752EBCA12DCEDB931EE315E8E8B676::get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4095 = { sizeof (ControllerElementTarget_t270C9829BE907AF87D4109ADF4BABEC4C35DA02B)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4095[2] = 
{
	ControllerElementTarget_t270C9829BE907AF87D4109ADF4BABEC4C35DA02B::get_offset_of_ZljBzOgnBdZbhDAoXUvCVQsaWkR_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerElementTarget_t270C9829BE907AF87D4109ADF4BABEC4C35DA02B::get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4096 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4097 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4098 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4099 = { sizeof (agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197), -1, sizeof(agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4099[7] = 
{
	agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197_StaticFields::get_offset_of_JEJCOBMKlXtzWWwDefwoYuLjvRi_0(),
	agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197::get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_1(),
	agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197::get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_2(),
	agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197::get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_3(),
	agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197::get_offset_of_PavQAlUTQJCXgnaaIZJqMrVxxkr_4(),
	agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_5(),
	agYfxXkuAOdBmliRwltxLMjyFHG_t0B004A1E1C048628567A7FE3526B5BC0557CC197_StaticFields::get_offset_of_wKZsRNpOhQctCfJnvfAdTetobSIx_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
