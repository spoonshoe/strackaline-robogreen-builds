﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Rewired.CalibrationMap
struct CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3;
// Rewired.ControllerMap
struct ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB;
// Rewired.CustomController
struct CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A;
// Rewired.Demos.ControlMapperDemoMessage
struct ControlMapperDemoMessage_tCDC84E65EDB19FA393B98CB685326785436108AB;
// Rewired.Demos.ControlRemappingDemo1/ControllerSelection
struct ControllerSelection_t54D35C54472361116F871407E095920CDB855060;
// Rewired.Demos.ControlRemappingDemo1/DialogHelper
struct DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769;
// Rewired.Demos.GamepadTemplateUI.ControllerUIEffect
struct ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646;
// Rewired.Demos.GamepadTemplateUI.ControllerUIElement
struct ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF;
// Rewired.Demos.GamepadTemplateUI.ControllerUIElement[]
struct ControllerUIElementU5BU5D_t376F8D72795D1B61D41CD2CEC0474C8C986AFEC1;
// Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI/Stick[]
struct StickU5BU5D_t53B3B6F4BB1EC7672F71353B94E6AC4E227536C6;
// Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI/UIElement[]
struct UIElementU5BU5D_tDE25211E4110FAB7BC5D5E015E8847D8C03E4217;
// Rewired.Demos.SimpleCombinedKeyboardMouseRemapping
struct SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0;
// Rewired.Demos.SimpleControlRemapping
struct SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24;
// Rewired.Demos.TouchButtonExample[]
struct TouchButtonExampleU5BU5D_t95076935AE312ABDCCA9F593C220A33C5BED51C2;
// Rewired.Demos.TouchJoystickExample[]
struct TouchJoystickExampleU5BU5D_tAECDB332525B37004D94DEB18DC34FD2CDB5561E;
// Rewired.InputAction
struct InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA;
// Rewired.InputMapper
struct InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB;
// Rewired.InputMapper/ConflictFoundEventData
struct ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966;
// Rewired.InputMapper/Context
struct Context_tDDC4D5CD175FF95675F41961F66B1383287803A1;
// Rewired.Joystick
struct Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39;
// Rewired.Player
struct Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F;
// Rewired.PlayerMouse
struct PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D;
// Rewired.UI.ControlMapper.ControlMapper
struct ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7;
// Rewired.UI.ControlMapper.ThemeSettings/CustomAnimationTriggers
struct CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775;
// Rewired.UI.ControlMapper.ThemeSettings/ImageSettings
struct ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334;
// Rewired.UI.ControlMapper.ThemeSettings/ScrollbarSettings
struct ScrollbarSettings_tD2F8212CF389C6CC9C87CBB7985294663B3C791E;
// Rewired.UI.ControlMapper.ThemeSettings/SelectableSettings
struct SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29;
// Rewired.UI.ControlMapper.ThemeSettings/SliderSettings
struct SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA;
// Rewired.UI.ControlMapper.ThemeSettings/TextSettings
struct TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881;
// Rewired.UI.ControlMapper.ThemedElement/ElementInfo[]
struct ElementInfoU5BU5D_t5E095D1C45E7F0B39A10D4FB52C13B53EC9CD00B;
// Rewired.UI.ControlMapper.UIImageHelper/State
struct State_t58DA7EDFC97612C3E0E86571BA582D5F00193027;
// Rewired.UI.ControlMapper.UISliderControl
struct UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92;
// Rewired.UI.ControlMapper.Window
struct Window_t05E229BCE867863C1A33354186F3E7641707AAA5;
// Rewired.UI.ControlMapper.Window/Timer
struct Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Int32>
struct Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735;
// System.Action`2<System.Int32,Rewired.Demos.ControlRemappingDemo1/UserResponse>
struct Action_2_tE781714D4A31960931D519E5977B63D5A785D392;
// System.Action`2<System.Int32,System.Single>
struct Action_2_t634E33D1E916177D11D3008C7BCF5A07ADECEA10;
// System.Action`2<System.String,System.String>
struct Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Demos.GamepadTemplateUI.ControllerUIElement>
struct Dictionary_2_tF131C1F2A1D51A5DD801312B18E23DAB27F1C838;
// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.UI.ControlMapper.UIControl>
struct Dictionary_2_t4541A154CF51CC7F413C4CD599DCC87BFD4189C5;
// System.Collections.Generic.IList`1<Rewired.ControllerTemplateElementTarget>
struct IList_1_t65D814EF78C38BACA10BBDA87F7BB30859B29805;
// System.Collections.Generic.List`1<Rewired.Demos.DualShock4SpecialFeaturesExample/Touch>
struct List_1_t0D8D6E6E704B54D6FA758AC7D04275A1AABFF578;
// System.Collections.Generic.List`1<Rewired.Demos.PressStartToJoinExample_Assigner/PlayerMap>
struct List_1_t0E2A8C3EA3D601F7E6D6C0F50353922447DF0205;
// System.Collections.Generic.List`1<Rewired.Demos.SimpleCombinedKeyboardMouseRemapping/Row>
struct List_1_tBDA5BA13CAE0CC282027AA9C5F64A121D05BE771;
// System.Collections.Generic.List`1<Rewired.Demos.SimpleControlRemapping/Row>
struct List_1_t98B481DEA763C89090B7F0152A1C4350750410D0;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t48FDD3CF761FA5A5451E50F64C5529D419339F18;
// System.Collections.Generic.Queue`1<Rewired.Demos.ControlRemappingDemo1/QueueEntry>
struct Queue_1_t3E8D76F296626DD8BC156049D08357B155EF6502;
// System.Collections.Generic.Queue`1<Rewired.Demos.DualShock4SpecialFeaturesExample/Touch>
struct Queue_1_t0C455C90B9FAF2877555879F1BD5C0F3F2E4C9C0;
// System.Collections.Generic.Queue`1<Rewired.Joystick>
struct Queue_1_t4843A3235E672F8624CEBA0AD4E460BD0183A6C5;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.Examples.Benchmark01
struct Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761;
// TMPro.Examples.Benchmark01_UGUI
struct Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55;
// TMPro.Examples.ShaderPropAnimator
struct ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_tD52C5B081FC480F24349C2C0F5099F0693EEC913;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C;
// TMPro.TMP_Text
struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7;
// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90;
// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F;
// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8;
// TMPro.TMP_TextEventHandler/SpriteSelectionEvent
struct SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32;
// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554;
// TMPro.TextContainer
struct TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE;
// TMPro.TextMeshPro
struct TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.CharacterController
struct CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Font
struct Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSELECTDEFAULTDEFERREDU3ED__7_T67E156353E135BDAEA30F758F9B6A60D80B81982_H
#define U3CSELECTDEFAULTDEFERREDU3ED__7_T67E156353E135BDAEA30F758F9B6A60D80B81982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlMapperDemoMessage_<SelectDefaultDeferred>d__7
struct  U3CSelectDefaultDeferredU3Ed__7_t67E156353E135BDAEA30F758F9B6A60D80B81982  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Demos.ControlMapperDemoMessage_<SelectDefaultDeferred>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Rewired.Demos.ControlMapperDemoMessage_<SelectDefaultDeferred>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Rewired.Demos.ControlMapperDemoMessage Rewired.Demos.ControlMapperDemoMessage_<SelectDefaultDeferred>d__7::<>4__this
	ControlMapperDemoMessage_tCDC84E65EDB19FA393B98CB685326785436108AB * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSelectDefaultDeferredU3Ed__7_t67E156353E135BDAEA30F758F9B6A60D80B81982, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSelectDefaultDeferredU3Ed__7_t67E156353E135BDAEA30F758F9B6A60D80B81982, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSelectDefaultDeferredU3Ed__7_t67E156353E135BDAEA30F758F9B6A60D80B81982, ___U3CU3E4__this_2)); }
	inline ControlMapperDemoMessage_tCDC84E65EDB19FA393B98CB685326785436108AB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ControlMapperDemoMessage_tCDC84E65EDB19FA393B98CB685326785436108AB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ControlMapperDemoMessage_tCDC84E65EDB19FA393B98CB685326785436108AB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSELECTDEFAULTDEFERREDU3ED__7_T67E156353E135BDAEA30F758F9B6A60D80B81982_H
#ifndef U3CU3EC__DISPLAYCLASS20_0_TF92CE9050A37C93C70BCBB33B8AF0525780435B9_H
#define U3CU3EC__DISPLAYCLASS20_0_TF92CE9050A37C93C70BCBB33B8AF0525780435B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.DualShock4SpecialFeaturesExample_<>c__DisplayClass20_0
struct  U3CU3Ec__DisplayClass20_0_tF92CE9050A37C93C70BCBB33B8AF0525780435B9  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Demos.DualShock4SpecialFeaturesExample_<>c__DisplayClass20_0::touchId
	int32_t ___touchId_0;

public:
	inline static int32_t get_offset_of_touchId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tF92CE9050A37C93C70BCBB33B8AF0525780435B9, ___touchId_0)); }
	inline int32_t get_touchId_0() const { return ___touchId_0; }
	inline int32_t* get_address_of_touchId_0() { return &___touchId_0; }
	inline void set_touchId_0(int32_t value)
	{
		___touchId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS20_0_TF92CE9050A37C93C70BCBB33B8AF0525780435B9_H
#ifndef TOUCH_TD19B9C083AA87301A8FB528C6577E26EC60A10A5_H
#define TOUCH_TD19B9C083AA87301A8FB528C6577E26EC60A10A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.DualShock4SpecialFeaturesExample_Touch
struct  Touch_tD19B9C083AA87301A8FB528C6577E26EC60A10A5  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Rewired.Demos.DualShock4SpecialFeaturesExample_Touch::go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go_0;
	// System.Int32 Rewired.Demos.DualShock4SpecialFeaturesExample_Touch::touchId
	int32_t ___touchId_1;

public:
	inline static int32_t get_offset_of_go_0() { return static_cast<int32_t>(offsetof(Touch_tD19B9C083AA87301A8FB528C6577E26EC60A10A5, ___go_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_go_0() const { return ___go_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_go_0() { return &___go_0; }
	inline void set_go_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___go_0 = value;
		Il2CppCodeGenWriteBarrier((&___go_0), value);
	}

	inline static int32_t get_offset_of_touchId_1() { return static_cast<int32_t>(offsetof(Touch_tD19B9C083AA87301A8FB528C6577E26EC60A10A5, ___touchId_1)); }
	inline int32_t get_touchId_1() const { return ___touchId_1; }
	inline int32_t* get_address_of_touchId_1() { return &___touchId_1; }
	inline void set_touchId_1(int32_t value)
	{
		___touchId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_TD19B9C083AA87301A8FB528C6577E26EC60A10A5_H
#ifndef UIELEMENT_T4381888A44D4BDFB1BE2256F0CEE10CEB405569D_H
#define UIELEMENT_T4381888A44D4BDFB1BE2256F0CEE10CEB405569D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI_UIElement
struct  UIElement_t4381888A44D4BDFB1BE2256F0CEE10CEB405569D  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI_UIElement::id
	int32_t ___id_0;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI_UIElement::element
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___element_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(UIElement_t4381888A44D4BDFB1BE2256F0CEE10CEB405569D, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_element_1() { return static_cast<int32_t>(offsetof(UIElement_t4381888A44D4BDFB1BE2256F0CEE10CEB405569D, ___element_1)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_element_1() const { return ___element_1; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_element_1() { return &___element_1; }
	inline void set_element_1(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___element_1 = value;
		Il2CppCodeGenWriteBarrier((&___element_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIELEMENT_T4381888A44D4BDFB1BE2256F0CEE10CEB405569D_H
#ifndef PLAYERMAP_T202B5F7585A6844370F11924F2CF1794A7E66908_H
#define PLAYERMAP_T202B5F7585A6844370F11924F2CF1794A7E66908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.PressStartToJoinExample_Assigner_PlayerMap
struct  PlayerMap_t202B5F7585A6844370F11924F2CF1794A7E66908  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Demos.PressStartToJoinExample_Assigner_PlayerMap::rewiredPlayerId
	int32_t ___rewiredPlayerId_0;
	// System.Int32 Rewired.Demos.PressStartToJoinExample_Assigner_PlayerMap::gamePlayerId
	int32_t ___gamePlayerId_1;

public:
	inline static int32_t get_offset_of_rewiredPlayerId_0() { return static_cast<int32_t>(offsetof(PlayerMap_t202B5F7585A6844370F11924F2CF1794A7E66908, ___rewiredPlayerId_0)); }
	inline int32_t get_rewiredPlayerId_0() const { return ___rewiredPlayerId_0; }
	inline int32_t* get_address_of_rewiredPlayerId_0() { return &___rewiredPlayerId_0; }
	inline void set_rewiredPlayerId_0(int32_t value)
	{
		___rewiredPlayerId_0 = value;
	}

	inline static int32_t get_offset_of_gamePlayerId_1() { return static_cast<int32_t>(offsetof(PlayerMap_t202B5F7585A6844370F11924F2CF1794A7E66908, ___gamePlayerId_1)); }
	inline int32_t get_gamePlayerId_1() const { return ___gamePlayerId_1; }
	inline int32_t* get_address_of_gamePlayerId_1() { return &___gamePlayerId_1; }
	inline void set_gamePlayerId_1(int32_t value)
	{
		___gamePlayerId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMAP_T202B5F7585A6844370F11924F2CF1794A7E66908_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_TD07560944F4325B6A2336CEFAFB46B956C653D6D_H
#define U3CU3EC__DISPLAYCLASS17_0_TD07560944F4325B6A2336CEFAFB46B956C653D6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_tD07560944F4325B6A2336CEFAFB46B956C653D6D  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_<>c__DisplayClass17_0::index
	int32_t ___index_0;
	// System.Int32 Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_<>c__DisplayClass17_0::actionElementMapId
	int32_t ___actionElementMapId_1;
	// Rewired.Demos.SimpleCombinedKeyboardMouseRemapping Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_<>c__DisplayClass17_0::<>4__this
	SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tD07560944F4325B6A2336CEFAFB46B956C653D6D, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_actionElementMapId_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tD07560944F4325B6A2336CEFAFB46B956C653D6D, ___actionElementMapId_1)); }
	inline int32_t get_actionElementMapId_1() const { return ___actionElementMapId_1; }
	inline int32_t* get_address_of_actionElementMapId_1() { return &___actionElementMapId_1; }
	inline void set_actionElementMapId_1(int32_t value)
	{
		___actionElementMapId_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tD07560944F4325B6A2336CEFAFB46B956C653D6D, ___U3CU3E4__this_2)); }
	inline SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_TD07560944F4325B6A2336CEFAFB46B956C653D6D_H
#ifndef U3CSTARTLISTENINGDELAYEDU3ED__22_T13BD143FDA42DC18B223473B10BE69D70389AE42_H
#define U3CSTARTLISTENINGDELAYEDU3ED__22_T13BD143FDA42DC18B223473B10BE69D70389AE42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_<StartListeningDelayed>d__22
struct  U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_<StartListeningDelayed>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_<StartListeningDelayed>d__22::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_<StartListeningDelayed>d__22::index
	int32_t ___index_2;
	// Rewired.ControllerMap Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_<StartListeningDelayed>d__22::keyboardMap
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___keyboardMap_3;
	// Rewired.ControllerMap Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_<StartListeningDelayed>d__22::mouseMap
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___mouseMap_4;
	// System.Int32 Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_<StartListeningDelayed>d__22::actionElementMapToReplaceId
	int32_t ___actionElementMapToReplaceId_5;
	// Rewired.Demos.SimpleCombinedKeyboardMouseRemapping Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_<StartListeningDelayed>d__22::<>4__this
	SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0 * ___U3CU3E4__this_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_keyboardMap_3() { return static_cast<int32_t>(offsetof(U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42, ___keyboardMap_3)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_keyboardMap_3() const { return ___keyboardMap_3; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_keyboardMap_3() { return &___keyboardMap_3; }
	inline void set_keyboardMap_3(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___keyboardMap_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardMap_3), value);
	}

	inline static int32_t get_offset_of_mouseMap_4() { return static_cast<int32_t>(offsetof(U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42, ___mouseMap_4)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_mouseMap_4() const { return ___mouseMap_4; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_mouseMap_4() { return &___mouseMap_4; }
	inline void set_mouseMap_4(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___mouseMap_4 = value;
		Il2CppCodeGenWriteBarrier((&___mouseMap_4), value);
	}

	inline static int32_t get_offset_of_actionElementMapToReplaceId_5() { return static_cast<int32_t>(offsetof(U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42, ___actionElementMapToReplaceId_5)); }
	inline int32_t get_actionElementMapToReplaceId_5() const { return ___actionElementMapToReplaceId_5; }
	inline int32_t* get_address_of_actionElementMapToReplaceId_5() { return &___actionElementMapToReplaceId_5; }
	inline void set_actionElementMapToReplaceId_5(int32_t value)
	{
		___actionElementMapToReplaceId_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_6() { return static_cast<int32_t>(offsetof(U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42, ___U3CU3E4__this_6)); }
	inline SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0 * get_U3CU3E4__this_6() const { return ___U3CU3E4__this_6; }
	inline SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0 ** get_address_of_U3CU3E4__this_6() { return &___U3CU3E4__this_6; }
	inline void set_U3CU3E4__this_6(SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0 * value)
	{
		___U3CU3E4__this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTLISTENINGDELAYEDU3ED__22_T13BD143FDA42DC18B223473B10BE69D70389AE42_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_T979318D186C1B8D1138D1642B4A4F4B3EDA8D479_H
#define U3CU3EC__DISPLAYCLASS21_0_T979318D186C1B8D1138D1642B4A4F4B3EDA8D479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.SimpleControlRemapping_<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_t979318D186C1B8D1138D1642B4A4F4B3EDA8D479  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Demos.SimpleControlRemapping_<>c__DisplayClass21_0::index
	int32_t ___index_0;
	// System.Int32 Rewired.Demos.SimpleControlRemapping_<>c__DisplayClass21_0::actionElementMapId
	int32_t ___actionElementMapId_1;
	// Rewired.Demos.SimpleControlRemapping Rewired.Demos.SimpleControlRemapping_<>c__DisplayClass21_0::<>4__this
	SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t979318D186C1B8D1138D1642B4A4F4B3EDA8D479, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_actionElementMapId_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t979318D186C1B8D1138D1642B4A4F4B3EDA8D479, ___actionElementMapId_1)); }
	inline int32_t get_actionElementMapId_1() const { return ___actionElementMapId_1; }
	inline int32_t* get_address_of_actionElementMapId_1() { return &___actionElementMapId_1; }
	inline void set_actionElementMapId_1(int32_t value)
	{
		___actionElementMapId_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t979318D186C1B8D1138D1642B4A4F4B3EDA8D479, ___U3CU3E4__this_2)); }
	inline SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_T979318D186C1B8D1138D1642B4A4F4B3EDA8D479_H
#ifndef U3CSTARTLISTENINGDELAYEDU3ED__28_T983DAA6DEEF9337A5CF63899D4693879D6510A28_H
#define U3CSTARTLISTENINGDELAYEDU3ED__28_T983DAA6DEEF9337A5CF63899D4693879D6510A28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.SimpleControlRemapping_<StartListeningDelayed>d__28
struct  U3CStartListeningDelayedU3Ed__28_t983DAA6DEEF9337A5CF63899D4693879D6510A28  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Demos.SimpleControlRemapping_<StartListeningDelayed>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Rewired.Demos.SimpleControlRemapping_<StartListeningDelayed>d__28::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 Rewired.Demos.SimpleControlRemapping_<StartListeningDelayed>d__28::index
	int32_t ___index_2;
	// System.Int32 Rewired.Demos.SimpleControlRemapping_<StartListeningDelayed>d__28::actionElementMapToReplaceId
	int32_t ___actionElementMapToReplaceId_3;
	// Rewired.Demos.SimpleControlRemapping Rewired.Demos.SimpleControlRemapping_<StartListeningDelayed>d__28::<>4__this
	SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartListeningDelayedU3Ed__28_t983DAA6DEEF9337A5CF63899D4693879D6510A28, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartListeningDelayedU3Ed__28_t983DAA6DEEF9337A5CF63899D4693879D6510A28, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(U3CStartListeningDelayedU3Ed__28_t983DAA6DEEF9337A5CF63899D4693879D6510A28, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_actionElementMapToReplaceId_3() { return static_cast<int32_t>(offsetof(U3CStartListeningDelayedU3Ed__28_t983DAA6DEEF9337A5CF63899D4693879D6510A28, ___actionElementMapToReplaceId_3)); }
	inline int32_t get_actionElementMapToReplaceId_3() const { return ___actionElementMapToReplaceId_3; }
	inline int32_t* get_address_of_actionElementMapToReplaceId_3() { return &___actionElementMapToReplaceId_3; }
	inline void set_actionElementMapToReplaceId_3(int32_t value)
	{
		___actionElementMapToReplaceId_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CStartListeningDelayedU3Ed__28_t983DAA6DEEF9337A5CF63899D4693879D6510A28, ___U3CU3E4__this_4)); }
	inline SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTLISTENINGDELAYEDU3ED__28_T983DAA6DEEF9337A5CF63899D4693879D6510A28_H
#ifndef CUSTOMANIMATIONTRIGGERS_TD6D8534858DC614F75D7CDAFD5D050B6A989D775_H
#define CUSTOMANIMATIONTRIGGERS_TD6D8534858DC614F75D7CDAFD5D050B6A989D775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemeSettings_CustomAnimationTriggers
struct  CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775  : public RuntimeObject
{
public:
	// System.String Rewired.UI.ControlMapper.ThemeSettings_CustomAnimationTriggers::m_DisabledTrigger
	String_t* ___m_DisabledTrigger_0;
	// System.String Rewired.UI.ControlMapper.ThemeSettings_CustomAnimationTriggers::m_HighlightedTrigger
	String_t* ___m_HighlightedTrigger_1;
	// System.String Rewired.UI.ControlMapper.ThemeSettings_CustomAnimationTriggers::m_NormalTrigger
	String_t* ___m_NormalTrigger_2;
	// System.String Rewired.UI.ControlMapper.ThemeSettings_CustomAnimationTriggers::m_PressedTrigger
	String_t* ___m_PressedTrigger_3;
	// System.String Rewired.UI.ControlMapper.ThemeSettings_CustomAnimationTriggers::m_SelectedTrigger
	String_t* ___m_SelectedTrigger_4;
	// System.String Rewired.UI.ControlMapper.ThemeSettings_CustomAnimationTriggers::m_DisabledHighlightedTrigger
	String_t* ___m_DisabledHighlightedTrigger_5;

public:
	inline static int32_t get_offset_of_m_DisabledTrigger_0() { return static_cast<int32_t>(offsetof(CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775, ___m_DisabledTrigger_0)); }
	inline String_t* get_m_DisabledTrigger_0() const { return ___m_DisabledTrigger_0; }
	inline String_t** get_address_of_m_DisabledTrigger_0() { return &___m_DisabledTrigger_0; }
	inline void set_m_DisabledTrigger_0(String_t* value)
	{
		___m_DisabledTrigger_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledTrigger_0), value);
	}

	inline static int32_t get_offset_of_m_HighlightedTrigger_1() { return static_cast<int32_t>(offsetof(CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775, ___m_HighlightedTrigger_1)); }
	inline String_t* get_m_HighlightedTrigger_1() const { return ___m_HighlightedTrigger_1; }
	inline String_t** get_address_of_m_HighlightedTrigger_1() { return &___m_HighlightedTrigger_1; }
	inline void set_m_HighlightedTrigger_1(String_t* value)
	{
		___m_HighlightedTrigger_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedTrigger_1), value);
	}

	inline static int32_t get_offset_of_m_NormalTrigger_2() { return static_cast<int32_t>(offsetof(CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775, ___m_NormalTrigger_2)); }
	inline String_t* get_m_NormalTrigger_2() const { return ___m_NormalTrigger_2; }
	inline String_t** get_address_of_m_NormalTrigger_2() { return &___m_NormalTrigger_2; }
	inline void set_m_NormalTrigger_2(String_t* value)
	{
		___m_NormalTrigger_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_NormalTrigger_2), value);
	}

	inline static int32_t get_offset_of_m_PressedTrigger_3() { return static_cast<int32_t>(offsetof(CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775, ___m_PressedTrigger_3)); }
	inline String_t* get_m_PressedTrigger_3() const { return ___m_PressedTrigger_3; }
	inline String_t** get_address_of_m_PressedTrigger_3() { return &___m_PressedTrigger_3; }
	inline void set_m_PressedTrigger_3(String_t* value)
	{
		___m_PressedTrigger_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedTrigger_3), value);
	}

	inline static int32_t get_offset_of_m_SelectedTrigger_4() { return static_cast<int32_t>(offsetof(CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775, ___m_SelectedTrigger_4)); }
	inline String_t* get_m_SelectedTrigger_4() const { return ___m_SelectedTrigger_4; }
	inline String_t** get_address_of_m_SelectedTrigger_4() { return &___m_SelectedTrigger_4; }
	inline void set_m_SelectedTrigger_4(String_t* value)
	{
		___m_SelectedTrigger_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectedTrigger_4), value);
	}

	inline static int32_t get_offset_of_m_DisabledHighlightedTrigger_5() { return static_cast<int32_t>(offsetof(CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775, ___m_DisabledHighlightedTrigger_5)); }
	inline String_t* get_m_DisabledHighlightedTrigger_5() const { return ___m_DisabledHighlightedTrigger_5; }
	inline String_t** get_address_of_m_DisabledHighlightedTrigger_5() { return &___m_DisabledHighlightedTrigger_5; }
	inline void set_m_DisabledHighlightedTrigger_5(String_t* value)
	{
		___m_DisabledHighlightedTrigger_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledHighlightedTrigger_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMANIMATIONTRIGGERS_TD6D8534858DC614F75D7CDAFD5D050B6A989D775_H
#ifndef ELEMENTINFO_TB01A7EDE8DB4744B88FE22ECF59583DDBBA431F7_H
#define ELEMENTINFO_TB01A7EDE8DB4744B88FE22ECF59583DDBBA431F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemedElement_ElementInfo
struct  ElementInfo_tB01A7EDE8DB4744B88FE22ECF59583DDBBA431F7  : public RuntimeObject
{
public:
	// System.String Rewired.UI.ControlMapper.ThemedElement_ElementInfo::_themeClass
	String_t* ____themeClass_0;
	// UnityEngine.Component Rewired.UI.ControlMapper.ThemedElement_ElementInfo::_component
	Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ____component_1;

public:
	inline static int32_t get_offset_of__themeClass_0() { return static_cast<int32_t>(offsetof(ElementInfo_tB01A7EDE8DB4744B88FE22ECF59583DDBBA431F7, ____themeClass_0)); }
	inline String_t* get__themeClass_0() const { return ____themeClass_0; }
	inline String_t** get_address_of__themeClass_0() { return &____themeClass_0; }
	inline void set__themeClass_0(String_t* value)
	{
		____themeClass_0 = value;
		Il2CppCodeGenWriteBarrier((&____themeClass_0), value);
	}

	inline static int32_t get_offset_of__component_1() { return static_cast<int32_t>(offsetof(ElementInfo_tB01A7EDE8DB4744B88FE22ECF59583DDBBA431F7, ____component_1)); }
	inline Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * get__component_1() const { return ____component_1; }
	inline Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 ** get_address_of__component_1() { return &____component_1; }
	inline void set__component_1(Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * value)
	{
		____component_1 = value;
		Il2CppCodeGenWriteBarrier((&____component_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTINFO_TB01A7EDE8DB4744B88FE22ECF59583DDBBA431F7_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T008ACE768E4A001876943FA94BA8FAD949138441_H
#define U3CU3EC__DISPLAYCLASS6_0_T008ACE768E4A001876943FA94BA8FAD949138441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.UIControlSet_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t008ACE768E4A001876943FA94BA8FAD949138441  : public RuntimeObject
{
public:
	// System.Action`2<System.Int32,System.Single> Rewired.UI.ControlMapper.UIControlSet_<>c__DisplayClass6_0::valueChangedCallback
	Action_2_t634E33D1E916177D11D3008C7BCF5A07ADECEA10 * ___valueChangedCallback_0;
	// Rewired.UI.ControlMapper.UISliderControl Rewired.UI.ControlMapper.UIControlSet_<>c__DisplayClass6_0::control
	UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92 * ___control_1;
	// System.Action`1<System.Int32> Rewired.UI.ControlMapper.UIControlSet_<>c__DisplayClass6_0::cancelCallback
	Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * ___cancelCallback_2;

public:
	inline static int32_t get_offset_of_valueChangedCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t008ACE768E4A001876943FA94BA8FAD949138441, ___valueChangedCallback_0)); }
	inline Action_2_t634E33D1E916177D11D3008C7BCF5A07ADECEA10 * get_valueChangedCallback_0() const { return ___valueChangedCallback_0; }
	inline Action_2_t634E33D1E916177D11D3008C7BCF5A07ADECEA10 ** get_address_of_valueChangedCallback_0() { return &___valueChangedCallback_0; }
	inline void set_valueChangedCallback_0(Action_2_t634E33D1E916177D11D3008C7BCF5A07ADECEA10 * value)
	{
		___valueChangedCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedCallback_0), value);
	}

	inline static int32_t get_offset_of_control_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t008ACE768E4A001876943FA94BA8FAD949138441, ___control_1)); }
	inline UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92 * get_control_1() const { return ___control_1; }
	inline UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92 ** get_address_of_control_1() { return &___control_1; }
	inline void set_control_1(UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92 * value)
	{
		___control_1 = value;
		Il2CppCodeGenWriteBarrier((&___control_1), value);
	}

	inline static int32_t get_offset_of_cancelCallback_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t008ACE768E4A001876943FA94BA8FAD949138441, ___cancelCallback_2)); }
	inline Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * get_cancelCallback_2() const { return ___cancelCallback_2; }
	inline Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B ** get_address_of_cancelCallback_2() { return &___cancelCallback_2; }
	inline void set_cancelCallback_2(Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * value)
	{
		___cancelCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___cancelCallback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T008ACE768E4A001876943FA94BA8FAD949138441_H
#ifndef UISELECTIONUTILITY_TA9D5D557D510F89529406B6A3FAA73FEC05D998E_H
#define UISELECTIONUTILITY_TA9D5D557D510F89529406B6A3FAA73FEC05D998E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.UISelectionUtility
struct  UISelectionUtility_tA9D5D557D510F89529406B6A3FAA73FEC05D998E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISELECTIONUTILITY_TA9D5D557D510F89529406B6A3FAA73FEC05D998E_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T3EBDB86D7688B23255C6D053440E7DA9DB627692_H
#define U3CU3EC__DISPLAYCLASS10_0_T3EBDB86D7688B23255C6D053440E7DA9DB627692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.UISliderControl_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t3EBDB86D7688B23255C6D053440E7DA9DB627692  : public RuntimeObject
{
public:
	// System.Action Rewired.UI.ControlMapper.UISliderControl_<>c__DisplayClass10_0::cancelCallback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___cancelCallback_0;

public:
	inline static int32_t get_offset_of_cancelCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t3EBDB86D7688B23255C6D053440E7DA9DB627692, ___cancelCallback_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_cancelCallback_0() const { return ___cancelCallback_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_cancelCallback_0() { return &___cancelCallback_0; }
	inline void set_cancelCallback_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___cancelCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___cancelCallback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T3EBDB86D7688B23255C6D053440E7DA9DB627692_H
#ifndef UITOOLS_T6184EE485E213E26F09C49D40DC3A81E739E4B21_H
#define UITOOLS_T6184EE485E213E26F09C49D40DC3A81E739E4B21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.UITools
struct  UITools_t6184EE485E213E26F09C49D40DC3A81E739E4B21  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOOLS_T6184EE485E213E26F09C49D40DC3A81E739E4B21_H
#ifndef U3CONENABLEASYNCU3ED__64_T31291985E6CB87CFA16B38FCA85522337BDCF4FB_H
#define U3CONENABLEASYNCU3ED__64_T31291985E6CB87CFA16B38FCA85522337BDCF4FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.Window_<OnEnableAsync>d__64
struct  U3COnEnableAsyncU3Ed__64_t31291985E6CB87CFA16B38FCA85522337BDCF4FB  : public RuntimeObject
{
public:
	// System.Int32 Rewired.UI.ControlMapper.Window_<OnEnableAsync>d__64::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Rewired.UI.ControlMapper.Window_<OnEnableAsync>d__64::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Rewired.UI.ControlMapper.Window Rewired.UI.ControlMapper.Window_<OnEnableAsync>d__64::<>4__this
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnEnableAsyncU3Ed__64_t31291985E6CB87CFA16B38FCA85522337BDCF4FB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3COnEnableAsyncU3Ed__64_t31291985E6CB87CFA16B38FCA85522337BDCF4FB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnEnableAsyncU3Ed__64_t31291985E6CB87CFA16B38FCA85522337BDCF4FB, ___U3CU3E4__this_2)); }
	inline Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Window_t05E229BCE867863C1A33354186F3E7641707AAA5 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONENABLEASYNCU3ED__64_T31291985E6CB87CFA16B38FCA85522337BDCF4FB_H
#ifndef TIMER_TDCE4DB70509B3359154AF619B30AE8BA8C82D67D_H
#define TIMER_TDCE4DB70509B3359154AF619B30AE8BA8C82D67D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.Window_Timer
struct  Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D  : public RuntimeObject
{
public:
	// System.Boolean Rewired.UI.ControlMapper.Window_Timer::_started
	bool ____started_0;
	// System.Single Rewired.UI.ControlMapper.Window_Timer::end
	float ___end_1;

public:
	inline static int32_t get_offset_of__started_0() { return static_cast<int32_t>(offsetof(Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D, ____started_0)); }
	inline bool get__started_0() const { return ____started_0; }
	inline bool* get_address_of__started_0() { return &____started_0; }
	inline void set__started_0(bool value)
	{
		____started_0 = value;
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D, ___end_1)); }
	inline float get_end_1() const { return ___end_1; }
	inline float* get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(float value)
	{
		___end_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMER_TDCE4DB70509B3359154AF619B30AE8BA8C82D67D_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CSTARTU3ED__10_T9DEB397698E0A4D729CAE51E4D27693C60088E4F_H
#define U3CSTARTU3ED__10_T9DEB397698E0A4D729CAE51E4D27693C60088E4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_<Start>d__10
struct  U3CStartU3Ed__10_t9DEB397698E0A4D729CAE51E4D27693C60088E4F  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_<Start>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.Benchmark01_<Start>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.Benchmark01 TMPro.Examples.Benchmark01_<Start>d__10::<>4__this
	Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761 * ___U3CU3E4__this_2;
	// System.Int32 TMPro.Examples.Benchmark01_<Start>d__10::<i>5__1
	int32_t ___U3CiU3E5__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t9DEB397698E0A4D729CAE51E4D27693C60088E4F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t9DEB397698E0A4D729CAE51E4D27693C60088E4F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t9DEB397698E0A4D729CAE51E4D27693C60088E4F, ___U3CU3E4__this_2)); }
	inline Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t9DEB397698E0A4D729CAE51E4D27693C60088E4F, ___U3CiU3E5__1_3)); }
	inline int32_t get_U3CiU3E5__1_3() const { return ___U3CiU3E5__1_3; }
	inline int32_t* get_address_of_U3CiU3E5__1_3() { return &___U3CiU3E5__1_3; }
	inline void set_U3CiU3E5__1_3(int32_t value)
	{
		___U3CiU3E5__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__10_T9DEB397698E0A4D729CAE51E4D27693C60088E4F_H
#ifndef U3CSTARTU3ED__10_T85A55BE1C5DC2607319D687991E4E5F7AC25386E_H
#define U3CSTARTU3ED__10_T85A55BE1C5DC2607319D687991E4E5F7AC25386E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI_<Start>d__10
struct  U3CStartU3Ed__10_t85A55BE1C5DC2607319D687991E4E5F7AC25386E  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI_<Start>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.Benchmark01_UGUI TMPro.Examples.Benchmark01_UGUI_<Start>d__10::<>4__this
	Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55 * ___U3CU3E4__this_2;
	// System.Int32 TMPro.Examples.Benchmark01_UGUI_<Start>d__10::<i>5__1
	int32_t ___U3CiU3E5__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t85A55BE1C5DC2607319D687991E4E5F7AC25386E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t85A55BE1C5DC2607319D687991E4E5F7AC25386E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t85A55BE1C5DC2607319D687991E4E5F7AC25386E, ___U3CU3E4__this_2)); }
	inline Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t85A55BE1C5DC2607319D687991E4E5F7AC25386E, ___U3CiU3E5__1_3)); }
	inline int32_t get_U3CiU3E5__1_3() const { return ___U3CiU3E5__1_3; }
	inline int32_t* get_address_of_U3CiU3E5__1_3() { return &___U3CiU3E5__1_3; }
	inline void set_U3CiU3E5__1_3(int32_t value)
	{
		___U3CiU3E5__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__10_T85A55BE1C5DC2607319D687991E4E5F7AC25386E_H
#ifndef U3CANIMATEPROPERTIESU3ED__6_T90BF7D7B9FC009B24809156AE0F91678207A6231_H
#define U3CANIMATEPROPERTIESU3ED__6_T90BF7D7B9FC009B24809156AE0F91678207A6231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6
struct  U3CAnimatePropertiesU3Ed__6_t90BF7D7B9FC009B24809156AE0F91678207A6231  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.ShaderPropAnimator TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::<>4__this
	ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72 * ___U3CU3E4__this_2;
	// System.Single TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::<glowPower>5__1
	float ___U3CglowPowerU3E5__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ed__6_t90BF7D7B9FC009B24809156AE0F91678207A6231, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ed__6_t90BF7D7B9FC009B24809156AE0F91678207A6231, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ed__6_t90BF7D7B9FC009B24809156AE0F91678207A6231, ___U3CU3E4__this_2)); }
	inline ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CglowPowerU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ed__6_t90BF7D7B9FC009B24809156AE0F91678207A6231, ___U3CglowPowerU3E5__1_3)); }
	inline float get_U3CglowPowerU3E5__1_3() const { return ___U3CglowPowerU3E5__1_3; }
	inline float* get_address_of_U3CglowPowerU3E5__1_3() { return &___U3CglowPowerU3E5__1_3; }
	inline void set_U3CglowPowerU3E5__1_3(float value)
	{
		___U3CglowPowerU3E5__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEPROPERTIESU3ED__6_T90BF7D7B9FC009B24809156AE0F91678207A6231_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef TARGETMAPPING_TCF6708DB0700100C669867F37A1066537F71BFD7_H
#define TARGETMAPPING_TCF6708DB0700100C669867F37A1066537F71BFD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_TargetMapping
struct  TargetMapping_tCF6708DB0700100C669867F37A1066537F71BFD7 
{
public:
	// Rewired.ControllerMap Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_TargetMapping::controllerMap
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___controllerMap_0;
	// System.Int32 Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_TargetMapping::actionElementMapId
	int32_t ___actionElementMapId_1;

public:
	inline static int32_t get_offset_of_controllerMap_0() { return static_cast<int32_t>(offsetof(TargetMapping_tCF6708DB0700100C669867F37A1066537F71BFD7, ___controllerMap_0)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_controllerMap_0() const { return ___controllerMap_0; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_controllerMap_0() { return &___controllerMap_0; }
	inline void set_controllerMap_0(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___controllerMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___controllerMap_0), value);
	}

	inline static int32_t get_offset_of_actionElementMapId_1() { return static_cast<int32_t>(offsetof(TargetMapping_tCF6708DB0700100C669867F37A1066537F71BFD7, ___actionElementMapId_1)); }
	inline int32_t get_actionElementMapId_1() const { return ___actionElementMapId_1; }
	inline int32_t* get_address_of_actionElementMapId_1() { return &___actionElementMapId_1; }
	inline void set_actionElementMapId_1(int32_t value)
	{
		___actionElementMapId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.Demos.SimpleCombinedKeyboardMouseRemapping/TargetMapping
struct TargetMapping_tCF6708DB0700100C669867F37A1066537F71BFD7_marshaled_pinvoke
{
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___controllerMap_0;
	int32_t ___actionElementMapId_1;
};
// Native definition for COM marshalling of Rewired.Demos.SimpleCombinedKeyboardMouseRemapping/TargetMapping
struct TargetMapping_tCF6708DB0700100C669867F37A1066537F71BFD7_marshaled_com
{
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___controllerMap_0;
	int32_t ___actionElementMapId_1;
};
#endif // TARGETMAPPING_TCF6708DB0700100C669867F37A1066537F71BFD7_H
#ifndef CUSTOMSPRITESTATE_T6D5244DC97A342D95C8B3D1B9564379C210DB56A_H
#define CUSTOMSPRITESTATE_T6D5244DC97A342D95C8B3D1B9564379C210DB56A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemeSettings_CustomSpriteState
struct  CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A 
{
public:
	// UnityEngine.Sprite Rewired.UI.ControlMapper.ThemeSettings_CustomSpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_0;
	// UnityEngine.Sprite Rewired.UI.ControlMapper.ThemeSettings_CustomSpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_1;
	// UnityEngine.Sprite Rewired.UI.ControlMapper.ThemeSettings_CustomSpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_2;
	// UnityEngine.Sprite Rewired.UI.ControlMapper.ThemeSettings_CustomSpriteState::m_SelectedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_3;
	// UnityEngine.Sprite Rewired.UI.ControlMapper.ThemeSettings_CustomSpriteState::m_DisabledHighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledHighlightedSprite_4;

public:
	inline static int32_t get_offset_of_m_DisabledSprite_0() { return static_cast<int32_t>(offsetof(CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A, ___m_DisabledSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_0() const { return ___m_DisabledSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_0() { return &___m_DisabledSprite_0; }
	inline void set_m_DisabledSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_0), value);
	}

	inline static int32_t get_offset_of_m_HighlightedSprite_1() { return static_cast<int32_t>(offsetof(CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A, ___m_HighlightedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_1() const { return ___m_HighlightedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_1() { return &___m_HighlightedSprite_1; }
	inline void set_m_HighlightedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_2() { return static_cast<int32_t>(offsetof(CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A, ___m_PressedSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_2() const { return ___m_PressedSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_2() { return &___m_PressedSprite_2; }
	inline void set_m_PressedSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_2), value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_3() { return static_cast<int32_t>(offsetof(CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A, ___m_SelectedSprite_3)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_SelectedSprite_3() const { return ___m_SelectedSprite_3; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_SelectedSprite_3() { return &___m_SelectedSprite_3; }
	inline void set_m_SelectedSprite_3(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_SelectedSprite_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectedSprite_3), value);
	}

	inline static int32_t get_offset_of_m_DisabledHighlightedSprite_4() { return static_cast<int32_t>(offsetof(CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A, ___m_DisabledHighlightedSprite_4)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledHighlightedSprite_4() const { return ___m_DisabledHighlightedSprite_4; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledHighlightedSprite_4() { return &___m_DisabledHighlightedSprite_4; }
	inline void set_m_DisabledHighlightedSprite_4(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledHighlightedSprite_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledHighlightedSprite_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.UI.ControlMapper.ThemeSettings/CustomSpriteState
struct CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_2;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_3;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledHighlightedSprite_4;
};
// Native definition for COM marshalling of Rewired.UI.ControlMapper.ThemeSettings/CustomSpriteState
struct CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_2;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_3;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledHighlightedSprite_4;
};
#endif // CUSTOMSPRITESTATE_T6D5244DC97A342D95C8B3D1B9564379C210DB56A_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef UNITYEVENT_2_TFA823C95B6CF586A1E67D0BD121D05EABE421A72_H
#define UNITYEVENT_2_TFA823C95B6CF586A1E67D0BD121D05EABE421A72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Char,System.Int32>
struct  UnityEvent_2_tFA823C95B6CF586A1E67D0BD121D05EABE421A72  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_tFA823C95B6CF586A1E67D0BD121D05EABE421A72, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_TFA823C95B6CF586A1E67D0BD121D05EABE421A72_H
#ifndef UNITYEVENT_3_T832C126B9440500DC7A72F1FAC225BF1007C27A9_H
#define UNITYEVENT_3_T832C126B9440500DC7A72F1FAC225BF1007C27A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct  UnityEvent_3_t832C126B9440500DC7A72F1FAC225BF1007C27A9  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t832C126B9440500DC7A72F1FAC225BF1007C27A9, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T832C126B9440500DC7A72F1FAC225BF1007C27A9_H
#ifndef UNITYEVENT_3_T3BD0B621DC6963B11E735B769684BC16A5D05CB1_H
#define UNITYEVENT_3_T3BD0B621DC6963B11E735B769684BC16A5D05CB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.String,System.Int32>
struct  UnityEvent_3_t3BD0B621DC6963B11E735B769684BC16A5D05CB1  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t3BD0B621DC6963B11E735B769684BC16A5D05CB1, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T3BD0B621DC6963B11E735B769684BC16A5D05CB1_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#define AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisRange
struct  AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652 
{
public:
	// System.Int32 Rewired.AxisRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifndef CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#define CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerType
struct  ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4 
{
public:
	// System.Int32 Rewired.ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifndef DIALOGTYPE_T53EE61F8728BCDABFA5158705E217E6AC47831BE_H
#define DIALOGTYPE_T53EE61F8728BCDABFA5158705E217E6AC47831BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_DialogHelper_DialogType
struct  DialogType_t53EE61F8728BCDABFA5158705E217E6AC47831BE 
{
public:
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_DialogHelper_DialogType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DialogType_t53EE61F8728BCDABFA5158705E217E6AC47831BE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGTYPE_T53EE61F8728BCDABFA5158705E217E6AC47831BE_H
#ifndef ELEMENTASSIGNMENTCHANGETYPE_T7572EB1ADF6266C81710E0DF149EC64DE4CCCB70_H
#define ELEMENTASSIGNMENTCHANGETYPE_T7572EB1ADF6266C81710E0DF149EC64DE4CCCB70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_ElementAssignmentChangeType
struct  ElementAssignmentChangeType_t7572EB1ADF6266C81710E0DF149EC64DE4CCCB70 
{
public:
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_ElementAssignmentChangeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementAssignmentChangeType_t7572EB1ADF6266C81710E0DF149EC64DE4CCCB70, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTASSIGNMENTCHANGETYPE_T7572EB1ADF6266C81710E0DF149EC64DE4CCCB70_H
#ifndef QUEUEACTIONTYPE_TE2FB972C36B9841FFDAA9842823FACF93D2ABC04_H
#define QUEUEACTIONTYPE_TE2FB972C36B9841FFDAA9842823FACF93D2ABC04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_QueueActionType
struct  QueueActionType_tE2FB972C36B9841FFDAA9842823FACF93D2ABC04 
{
public:
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_QueueActionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(QueueActionType_tE2FB972C36B9841FFDAA9842823FACF93D2ABC04, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUEACTIONTYPE_TE2FB972C36B9841FFDAA9842823FACF93D2ABC04_H
#ifndef STATE_T5004BB852A21B919F0D832BADCFF68CD47473C13_H
#define STATE_T5004BB852A21B919F0D832BADCFF68CD47473C13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_QueueEntry_State
struct  State_t5004BB852A21B919F0D832BADCFF68CD47473C13 
{
public:
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_QueueEntry_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t5004BB852A21B919F0D832BADCFF68CD47473C13, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T5004BB852A21B919F0D832BADCFF68CD47473C13_H
#ifndef USERRESPONSE_T661D779E3B9E6188F653D07C4C5449E9E28D39B7_H
#define USERRESPONSE_T661D779E3B9E6188F653D07C4C5449E9E28D39B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_UserResponse
struct  UserResponse_t661D779E3B9E6188F653D07C4C5449E9E28D39B7 
{
public:
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_UserResponse::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UserResponse_t661D779E3B9E6188F653D07C4C5449E9E28D39B7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERRESPONSE_T661D779E3B9E6188F653D07C4C5449E9E28D39B7_H
#ifndef WINDOWPROPERTIES_TDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48_H
#define WINDOWPROPERTIES_TDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_WindowProperties
struct  WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48 
{
public:
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_WindowProperties::windowId
	int32_t ___windowId_0;
	// UnityEngine.Rect Rewired.Demos.ControlRemappingDemo1_WindowProperties::rect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect_1;
	// System.Action`2<System.String,System.String> Rewired.Demos.ControlRemappingDemo1_WindowProperties::windowDrawDelegate
	Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * ___windowDrawDelegate_2;
	// System.String Rewired.Demos.ControlRemappingDemo1_WindowProperties::title
	String_t* ___title_3;
	// System.String Rewired.Demos.ControlRemappingDemo1_WindowProperties::message
	String_t* ___message_4;

public:
	inline static int32_t get_offset_of_windowId_0() { return static_cast<int32_t>(offsetof(WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48, ___windowId_0)); }
	inline int32_t get_windowId_0() const { return ___windowId_0; }
	inline int32_t* get_address_of_windowId_0() { return &___windowId_0; }
	inline void set_windowId_0(int32_t value)
	{
		___windowId_0 = value;
	}

	inline static int32_t get_offset_of_rect_1() { return static_cast<int32_t>(offsetof(WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48, ___rect_1)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_rect_1() const { return ___rect_1; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_rect_1() { return &___rect_1; }
	inline void set_rect_1(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___rect_1 = value;
	}

	inline static int32_t get_offset_of_windowDrawDelegate_2() { return static_cast<int32_t>(offsetof(WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48, ___windowDrawDelegate_2)); }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * get_windowDrawDelegate_2() const { return ___windowDrawDelegate_2; }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 ** get_address_of_windowDrawDelegate_2() { return &___windowDrawDelegate_2; }
	inline void set_windowDrawDelegate_2(Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * value)
	{
		___windowDrawDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&___windowDrawDelegate_2), value);
	}

	inline static int32_t get_offset_of_title_3() { return static_cast<int32_t>(offsetof(WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48, ___title_3)); }
	inline String_t* get_title_3() const { return ___title_3; }
	inline String_t** get_address_of_title_3() { return &___title_3; }
	inline void set_title_3(String_t* value)
	{
		___title_3 = value;
		Il2CppCodeGenWriteBarrier((&___title_3), value);
	}

	inline static int32_t get_offset_of_message_4() { return static_cast<int32_t>(offsetof(WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48, ___message_4)); }
	inline String_t* get_message_4() const { return ___message_4; }
	inline String_t** get_address_of_message_4() { return &___message_4; }
	inline void set_message_4(String_t* value)
	{
		___message_4 = value;
		Il2CppCodeGenWriteBarrier((&___message_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.Demos.ControlRemappingDemo1/WindowProperties
struct WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48_marshaled_pinvoke
{
	int32_t ___windowId_0;
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect_1;
	Il2CppMethodPointer ___windowDrawDelegate_2;
	char* ___title_3;
	char* ___message_4;
};
// Native definition for COM marshalling of Rewired.Demos.ControlRemappingDemo1/WindowProperties
struct WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48_marshaled_com
{
	int32_t ___windowId_0;
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect_1;
	Il2CppMethodPointer ___windowDrawDelegate_2;
	Il2CppChar* ___title_3;
	Il2CppChar* ___message_4;
};
#endif // WINDOWPROPERTIES_TDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48_H
#ifndef STICK_T8DF1E9F62C0E4349392388A6E1CF7279391435E8_H
#define STICK_T8DF1E9F62C0E4349392388A6E1CF7279391435E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI_Stick
struct  Stick_t8DF1E9F62C0E4349392388A6E1CF7279391435E8  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI_Stick::_transform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____transform_0;
	// UnityEngine.Vector2 Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI_Stick::_origPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____origPosition_1;
	// System.Int32 Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI_Stick::_xAxisElementId
	int32_t ____xAxisElementId_2;
	// System.Int32 Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI_Stick::_yAxisElementId
	int32_t ____yAxisElementId_3;

public:
	inline static int32_t get_offset_of__transform_0() { return static_cast<int32_t>(offsetof(Stick_t8DF1E9F62C0E4349392388A6E1CF7279391435E8, ____transform_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__transform_0() const { return ____transform_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__transform_0() { return &____transform_0; }
	inline void set__transform_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____transform_0 = value;
		Il2CppCodeGenWriteBarrier((&____transform_0), value);
	}

	inline static int32_t get_offset_of__origPosition_1() { return static_cast<int32_t>(offsetof(Stick_t8DF1E9F62C0E4349392388A6E1CF7279391435E8, ____origPosition_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__origPosition_1() const { return ____origPosition_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__origPosition_1() { return &____origPosition_1; }
	inline void set__origPosition_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____origPosition_1 = value;
	}

	inline static int32_t get_offset_of__xAxisElementId_2() { return static_cast<int32_t>(offsetof(Stick_t8DF1E9F62C0E4349392388A6E1CF7279391435E8, ____xAxisElementId_2)); }
	inline int32_t get__xAxisElementId_2() const { return ____xAxisElementId_2; }
	inline int32_t* get_address_of__xAxisElementId_2() { return &____xAxisElementId_2; }
	inline void set__xAxisElementId_2(int32_t value)
	{
		____xAxisElementId_2 = value;
	}

	inline static int32_t get_offset_of__yAxisElementId_3() { return static_cast<int32_t>(offsetof(Stick_t8DF1E9F62C0E4349392388A6E1CF7279391435E8, ____yAxisElementId_3)); }
	inline int32_t get__yAxisElementId_3() const { return ____yAxisElementId_3; }
	inline int32_t* get_address_of__yAxisElementId_3() { return &____yAxisElementId_3; }
	inline void set__yAxisElementId_3(int32_t value)
	{
		____yAxisElementId_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STICK_T8DF1E9F62C0E4349392388A6E1CF7279391435E8_H
#ifndef CUSTOMCOLORBLOCK_T0BB2DE1051C2719944E3C6D0F2663F16DC6850F2_H
#define CUSTOMCOLORBLOCK_T0BB2DE1051C2719944E3C6D0F2663F16DC6850F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemeSettings_CustomColorBlock
struct  CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2 
{
public:
	// System.Single Rewired.UI.ControlMapper.ThemeSettings_CustomColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_0;
	// UnityEngine.Color Rewired.UI.ControlMapper.ThemeSettings_CustomColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_1;
	// System.Single Rewired.UI.ControlMapper.ThemeSettings_CustomColorBlock::m_FadeDuration
	float ___m_FadeDuration_2;
	// UnityEngine.Color Rewired.UI.ControlMapper.ThemeSettings_CustomColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_3;
	// UnityEngine.Color Rewired.UI.ControlMapper.ThemeSettings_CustomColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_4;
	// UnityEngine.Color Rewired.UI.ControlMapper.ThemeSettings_CustomColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_5;
	// UnityEngine.Color Rewired.UI.ControlMapper.ThemeSettings_CustomColorBlock::m_SelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_SelectedColor_6;
	// UnityEngine.Color Rewired.UI.ControlMapper.ThemeSettings_CustomColorBlock::m_DisabledHighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledHighlightedColor_7;

public:
	inline static int32_t get_offset_of_m_ColorMultiplier_0() { return static_cast<int32_t>(offsetof(CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2, ___m_ColorMultiplier_0)); }
	inline float get_m_ColorMultiplier_0() const { return ___m_ColorMultiplier_0; }
	inline float* get_address_of_m_ColorMultiplier_0() { return &___m_ColorMultiplier_0; }
	inline void set_m_ColorMultiplier_0(float value)
	{
		___m_ColorMultiplier_0 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_1() { return static_cast<int32_t>(offsetof(CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2, ___m_DisabledColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_1() const { return ___m_DisabledColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_1() { return &___m_DisabledColor_1; }
	inline void set_m_DisabledColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_1 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_2() { return static_cast<int32_t>(offsetof(CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2, ___m_FadeDuration_2)); }
	inline float get_m_FadeDuration_2() const { return ___m_FadeDuration_2; }
	inline float* get_address_of_m_FadeDuration_2() { return &___m_FadeDuration_2; }
	inline void set_m_FadeDuration_2(float value)
	{
		___m_FadeDuration_2 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_3() { return static_cast<int32_t>(offsetof(CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2, ___m_HighlightedColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_3() const { return ___m_HighlightedColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_3() { return &___m_HighlightedColor_3; }
	inline void set_m_HighlightedColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_NormalColor_4() { return static_cast<int32_t>(offsetof(CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2, ___m_NormalColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_4() const { return ___m_NormalColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_4() { return &___m_NormalColor_4; }
	inline void set_m_NormalColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_4 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_5() { return static_cast<int32_t>(offsetof(CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2, ___m_PressedColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_5() const { return ___m_PressedColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_5() { return &___m_PressedColor_5; }
	inline void set_m_PressedColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_5 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_6() { return static_cast<int32_t>(offsetof(CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2, ___m_SelectedColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_SelectedColor_6() const { return ___m_SelectedColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_SelectedColor_6() { return &___m_SelectedColor_6; }
	inline void set_m_SelectedColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_SelectedColor_6 = value;
	}

	inline static int32_t get_offset_of_m_DisabledHighlightedColor_7() { return static_cast<int32_t>(offsetof(CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2, ___m_DisabledHighlightedColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledHighlightedColor_7() const { return ___m_DisabledHighlightedColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledHighlightedColor_7() { return &___m_DisabledHighlightedColor_7; }
	inline void set_m_DisabledHighlightedColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledHighlightedColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCOLORBLOCK_T0BB2DE1051C2719944E3C6D0F2663F16DC6850F2_H
#ifndef FONTSTYLEOVERRIDE_TA66591F7413303450D9AB376077A42A8717AAC4D_H
#define FONTSTYLEOVERRIDE_TA66591F7413303450D9AB376077A42A8717AAC4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemeSettings_FontStyleOverride
struct  FontStyleOverride_tA66591F7413303450D9AB376077A42A8717AAC4D 
{
public:
	// System.Int32 Rewired.UI.ControlMapper.ThemeSettings_FontStyleOverride::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyleOverride_tA66591F7413303450D9AB376077A42A8717AAC4D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLEOVERRIDE_TA66591F7413303450D9AB376077A42A8717AAC4D_H
#ifndef STATE_T58DA7EDFC97612C3E0E86571BA582D5F00193027_H
#define STATE_T58DA7EDFC97612C3E0E86571BA582D5F00193027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.UIImageHelper_State
struct  State_t58DA7EDFC97612C3E0E86571BA582D5F00193027  : public RuntimeObject
{
public:
	// UnityEngine.Color Rewired.UI.ControlMapper.UIImageHelper_State::color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___color_0;

public:
	inline static int32_t get_offset_of_color_0() { return static_cast<int32_t>(offsetof(State_t58DA7EDFC97612C3E0E86571BA582D5F00193027, ___color_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_color_0() const { return ___color_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_color_0() { return &___color_0; }
	inline void set_color_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___color_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T58DA7EDFC97612C3E0E86571BA582D5F00193027_H
#ifndef CAMERAMODES_T4B24D50582F214FB9AB0B82E700305CB97C9CF9D_H
#define CAMERAMODES_T4B24D50582F214FB9AB0B82E700305CB97C9CF9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController_CameraModes
struct  CameraModes_t4B24D50582F214FB9AB0B82E700305CB97C9CF9D 
{
public:
	// System.Int32 TMPro.Examples.CameraController_CameraModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraModes_t4B24D50582F214FB9AB0B82E700305CB97C9CF9D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMODES_T4B24D50582F214FB9AB0B82E700305CB97C9CF9D_H
#ifndef MOTIONTYPE_T0B038BCA79B1865C903414BFE3B65AF46B2A6833_H
#define MOTIONTYPE_T0B038BCA79B1865C903414BFE3B65AF46B2A6833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin_MotionType
struct  MotionType_t0B038BCA79B1865C903414BFE3B65AF46B2A6833 
{
public:
	// System.Int32 TMPro.Examples.ObjectSpin_MotionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MotionType_t0B038BCA79B1865C903414BFE3B65AF46B2A6833, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONTYPE_T0B038BCA79B1865C903414BFE3B65AF46B2A6833_H
#ifndef CHARACTERSELECTIONEVENT_T346DE6835B0DF86A32928016EE2C413E919DBF90_H
#define CHARACTERSELECTIONEVENT_T346DE6835B0DF86A32928016EE2C413E919DBF90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler_CharacterSelectionEvent
struct  CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90  : public UnityEvent_2_tFA823C95B6CF586A1E67D0BD121D05EABE421A72
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSELECTIONEVENT_T346DE6835B0DF86A32928016EE2C413E919DBF90_H
#ifndef LINESELECTIONEVENT_T9F223C373E9EF88E12570D86DE2651F8EDEEAB3F_H
#define LINESELECTIONEVENT_T9F223C373E9EF88E12570D86DE2651F8EDEEAB3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler_LineSelectionEvent
struct  LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F  : public UnityEvent_3_t832C126B9440500DC7A72F1FAC225BF1007C27A9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESELECTIONEVENT_T9F223C373E9EF88E12570D86DE2651F8EDEEAB3F_H
#ifndef LINKSELECTIONEVENT_T61E2583194386CC362B13606ECE2F840876A24E8_H
#define LINKSELECTIONEVENT_T61E2583194386CC362B13606ECE2F840876A24E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler_LinkSelectionEvent
struct  LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8  : public UnityEvent_3_t3BD0B621DC6963B11E735B769684BC16A5D05CB1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKSELECTIONEVENT_T61E2583194386CC362B13606ECE2F840876A24E8_H
#ifndef SPRITESELECTIONEVENT_TCE8FEB1D487ED84CBA38BC47F8949C5537672B32_H
#define SPRITESELECTIONEVENT_TCE8FEB1D487ED84CBA38BC47F8949C5537672B32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler_SpriteSelectionEvent
struct  SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32  : public UnityEvent_2_tFA823C95B6CF586A1E67D0BD121D05EABE421A72
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESELECTIONEVENT_TCE8FEB1D487ED84CBA38BC47F8949C5537672B32_H
#ifndef WORDSELECTIONEVENT_TF37108E717AF8FCEB63C4B4CB11231A5F030A554_H
#define WORDSELECTIONEVENT_TF37108E717AF8FCEB63C4B4CB11231A5F030A554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler_WordSelectionEvent
struct  WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554  : public UnityEvent_3_t832C126B9440500DC7A72F1FAC225BF1007C27A9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSELECTIONEVENT_TF37108E717AF8FCEB63C4B4CB11231A5F030A554_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef SCREENORIENTATION_T4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51_H
#define SCREENORIENTATION_T4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenOrientation_t4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51_H
#ifndef FILLMETHOD_T0DB7332683118B7C7D2748BE74CFBF19CD19F8C5_H
#define FILLMETHOD_T0DB7332683118B7C7D2748BE74CFBF19CD19F8C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image_FillMethod
struct  FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5 
{
public:
	// System.Int32 UnityEngine.UI.Image_FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T0DB7332683118B7C7D2748BE74CFBF19CD19F8C5_H
#ifndef TYPE_T96B8A259B84ADA5E7D3B1F13AEAE22175937F38A_H
#define TYPE_T96B8A259B84ADA5E7D3B1F13AEAE22175937F38A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image_Type
struct  Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A 
{
public:
	// System.Int32 UnityEngine.UI.Image_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T96B8A259B84ADA5E7D3B1F13AEAE22175937F38A_H
#ifndef TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#define TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable_Transition
struct  Transition_tA9261C608B54C52324084A0B080E7A3E0548A181 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tA9261C608B54C52324084A0B080E7A3E0548A181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifndef CONTROLLERSELECTION_T54D35C54472361116F871407E095920CDB855060_H
#define CONTROLLERSELECTION_T54D35C54472361116F871407E095920CDB855060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_ControllerSelection
struct  ControllerSelection_t54D35C54472361116F871407E095920CDB855060  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_ControllerSelection::_id
	int32_t ____id_0;
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_ControllerSelection::_idPrev
	int32_t ____idPrev_1;
	// Rewired.ControllerType Rewired.Demos.ControlRemappingDemo1_ControllerSelection::_type
	int32_t ____type_2;
	// Rewired.ControllerType Rewired.Demos.ControlRemappingDemo1_ControllerSelection::_typePrev
	int32_t ____typePrev_3;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(ControllerSelection_t54D35C54472361116F871407E095920CDB855060, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__idPrev_1() { return static_cast<int32_t>(offsetof(ControllerSelection_t54D35C54472361116F871407E095920CDB855060, ____idPrev_1)); }
	inline int32_t get__idPrev_1() const { return ____idPrev_1; }
	inline int32_t* get_address_of__idPrev_1() { return &____idPrev_1; }
	inline void set__idPrev_1(int32_t value)
	{
		____idPrev_1 = value;
	}

	inline static int32_t get_offset_of__type_2() { return static_cast<int32_t>(offsetof(ControllerSelection_t54D35C54472361116F871407E095920CDB855060, ____type_2)); }
	inline int32_t get__type_2() const { return ____type_2; }
	inline int32_t* get_address_of__type_2() { return &____type_2; }
	inline void set__type_2(int32_t value)
	{
		____type_2 = value;
	}

	inline static int32_t get_offset_of__typePrev_3() { return static_cast<int32_t>(offsetof(ControllerSelection_t54D35C54472361116F871407E095920CDB855060, ____typePrev_3)); }
	inline int32_t get__typePrev_3() const { return ____typePrev_3; }
	inline int32_t* get_address_of__typePrev_3() { return &____typePrev_3; }
	inline void set__typePrev_3(int32_t value)
	{
		____typePrev_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERSELECTION_T54D35C54472361116F871407E095920CDB855060_H
#ifndef DIALOGHELPER_T37B815A90FB939F50D3FD5106E83FA48C41C5769_H
#define DIALOGHELPER_T37B815A90FB939F50D3FD5106E83FA48C41C5769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_DialogHelper
struct  DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769  : public RuntimeObject
{
public:
	// Rewired.Demos.ControlRemappingDemo1_DialogHelper_DialogType Rewired.Demos.ControlRemappingDemo1_DialogHelper::_type
	int32_t ____type_2;
	// System.Boolean Rewired.Demos.ControlRemappingDemo1_DialogHelper::_enabled
	bool ____enabled_3;
	// System.Single Rewired.Demos.ControlRemappingDemo1_DialogHelper::_busyTime
	float ____busyTime_4;
	// System.Boolean Rewired.Demos.ControlRemappingDemo1_DialogHelper::_busyTimerRunning
	bool ____busyTimerRunning_5;
	// System.Action`1<System.Int32> Rewired.Demos.ControlRemappingDemo1_DialogHelper::drawWindowDelegate
	Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * ___drawWindowDelegate_6;
	// UnityEngine.GUI_WindowFunction Rewired.Demos.ControlRemappingDemo1_DialogHelper::drawWindowFunction
	WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100 * ___drawWindowFunction_7;
	// Rewired.Demos.ControlRemappingDemo1_WindowProperties Rewired.Demos.ControlRemappingDemo1_DialogHelper::windowProperties
	WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48  ___windowProperties_8;
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_DialogHelper::currentActionId
	int32_t ___currentActionId_9;
	// System.Action`2<System.Int32,Rewired.Demos.ControlRemappingDemo1_UserResponse> Rewired.Demos.ControlRemappingDemo1_DialogHelper::resultCallback
	Action_2_tE781714D4A31960931D519E5977B63D5A785D392 * ___resultCallback_10;

public:
	inline static int32_t get_offset_of__type_2() { return static_cast<int32_t>(offsetof(DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769, ____type_2)); }
	inline int32_t get__type_2() const { return ____type_2; }
	inline int32_t* get_address_of__type_2() { return &____type_2; }
	inline void set__type_2(int32_t value)
	{
		____type_2 = value;
	}

	inline static int32_t get_offset_of__enabled_3() { return static_cast<int32_t>(offsetof(DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769, ____enabled_3)); }
	inline bool get__enabled_3() const { return ____enabled_3; }
	inline bool* get_address_of__enabled_3() { return &____enabled_3; }
	inline void set__enabled_3(bool value)
	{
		____enabled_3 = value;
	}

	inline static int32_t get_offset_of__busyTime_4() { return static_cast<int32_t>(offsetof(DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769, ____busyTime_4)); }
	inline float get__busyTime_4() const { return ____busyTime_4; }
	inline float* get_address_of__busyTime_4() { return &____busyTime_4; }
	inline void set__busyTime_4(float value)
	{
		____busyTime_4 = value;
	}

	inline static int32_t get_offset_of__busyTimerRunning_5() { return static_cast<int32_t>(offsetof(DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769, ____busyTimerRunning_5)); }
	inline bool get__busyTimerRunning_5() const { return ____busyTimerRunning_5; }
	inline bool* get_address_of__busyTimerRunning_5() { return &____busyTimerRunning_5; }
	inline void set__busyTimerRunning_5(bool value)
	{
		____busyTimerRunning_5 = value;
	}

	inline static int32_t get_offset_of_drawWindowDelegate_6() { return static_cast<int32_t>(offsetof(DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769, ___drawWindowDelegate_6)); }
	inline Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * get_drawWindowDelegate_6() const { return ___drawWindowDelegate_6; }
	inline Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B ** get_address_of_drawWindowDelegate_6() { return &___drawWindowDelegate_6; }
	inline void set_drawWindowDelegate_6(Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * value)
	{
		___drawWindowDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((&___drawWindowDelegate_6), value);
	}

	inline static int32_t get_offset_of_drawWindowFunction_7() { return static_cast<int32_t>(offsetof(DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769, ___drawWindowFunction_7)); }
	inline WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100 * get_drawWindowFunction_7() const { return ___drawWindowFunction_7; }
	inline WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100 ** get_address_of_drawWindowFunction_7() { return &___drawWindowFunction_7; }
	inline void set_drawWindowFunction_7(WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100 * value)
	{
		___drawWindowFunction_7 = value;
		Il2CppCodeGenWriteBarrier((&___drawWindowFunction_7), value);
	}

	inline static int32_t get_offset_of_windowProperties_8() { return static_cast<int32_t>(offsetof(DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769, ___windowProperties_8)); }
	inline WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48  get_windowProperties_8() const { return ___windowProperties_8; }
	inline WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48 * get_address_of_windowProperties_8() { return &___windowProperties_8; }
	inline void set_windowProperties_8(WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48  value)
	{
		___windowProperties_8 = value;
	}

	inline static int32_t get_offset_of_currentActionId_9() { return static_cast<int32_t>(offsetof(DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769, ___currentActionId_9)); }
	inline int32_t get_currentActionId_9() const { return ___currentActionId_9; }
	inline int32_t* get_address_of_currentActionId_9() { return &___currentActionId_9; }
	inline void set_currentActionId_9(int32_t value)
	{
		___currentActionId_9 = value;
	}

	inline static int32_t get_offset_of_resultCallback_10() { return static_cast<int32_t>(offsetof(DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769, ___resultCallback_10)); }
	inline Action_2_tE781714D4A31960931D519E5977B63D5A785D392 * get_resultCallback_10() const { return ___resultCallback_10; }
	inline Action_2_tE781714D4A31960931D519E5977B63D5A785D392 ** get_address_of_resultCallback_10() { return &___resultCallback_10; }
	inline void set_resultCallback_10(Action_2_tE781714D4A31960931D519E5977B63D5A785D392 * value)
	{
		___resultCallback_10 = value;
		Il2CppCodeGenWriteBarrier((&___resultCallback_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGHELPER_T37B815A90FB939F50D3FD5106E83FA48C41C5769_H
#ifndef QUEUEENTRY_T983822D3991505A18F5AADD8D195EF9BF05E9075_H
#define QUEUEENTRY_T983822D3991505A18F5AADD8D195EF9BF05E9075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_QueueEntry
struct  QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_QueueEntry::<id>k__BackingField
	int32_t ___U3CidU3Ek__BackingField_0;
	// Rewired.Demos.ControlRemappingDemo1_QueueActionType Rewired.Demos.ControlRemappingDemo1_QueueEntry::<queueActionType>k__BackingField
	int32_t ___U3CqueueActionTypeU3Ek__BackingField_1;
	// Rewired.Demos.ControlRemappingDemo1_QueueEntry_State Rewired.Demos.ControlRemappingDemo1_QueueEntry::<state>k__BackingField
	int32_t ___U3CstateU3Ek__BackingField_2;
	// Rewired.Demos.ControlRemappingDemo1_UserResponse Rewired.Demos.ControlRemappingDemo1_QueueEntry::<response>k__BackingField
	int32_t ___U3CresponseU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075, ___U3CidU3Ek__BackingField_0)); }
	inline int32_t get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(int32_t value)
	{
		___U3CidU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CqueueActionTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075, ___U3CqueueActionTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CqueueActionTypeU3Ek__BackingField_1() const { return ___U3CqueueActionTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CqueueActionTypeU3Ek__BackingField_1() { return &___U3CqueueActionTypeU3Ek__BackingField_1; }
	inline void set_U3CqueueActionTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CqueueActionTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CstateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075, ___U3CstateU3Ek__BackingField_2)); }
	inline int32_t get_U3CstateU3Ek__BackingField_2() const { return ___U3CstateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CstateU3Ek__BackingField_2() { return &___U3CstateU3Ek__BackingField_2; }
	inline void set_U3CstateU3Ek__BackingField_2(int32_t value)
	{
		___U3CstateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CresponseU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075, ___U3CresponseU3Ek__BackingField_3)); }
	inline int32_t get_U3CresponseU3Ek__BackingField_3() const { return ___U3CresponseU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CresponseU3Ek__BackingField_3() { return &___U3CresponseU3Ek__BackingField_3; }
	inline void set_U3CresponseU3Ek__BackingField_3(int32_t value)
	{
		___U3CresponseU3Ek__BackingField_3 = value;
	}
};

struct QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075_StaticFields
{
public:
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_QueueEntry::uidCounter
	int32_t ___uidCounter_4;

public:
	inline static int32_t get_offset_of_uidCounter_4() { return static_cast<int32_t>(offsetof(QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075_StaticFields, ___uidCounter_4)); }
	inline int32_t get_uidCounter_4() const { return ___uidCounter_4; }
	inline int32_t* get_address_of_uidCounter_4() { return &___uidCounter_4; }
	inline void set_uidCounter_4(int32_t value)
	{
		___uidCounter_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUEENTRY_T983822D3991505A18F5AADD8D195EF9BF05E9075_H
#ifndef ROW_T527913C609FCA2EFA018E5FDC5013A69A553DF99_H
#define ROW_T527913C609FCA2EFA018E5FDC5013A69A553DF99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_Row
struct  Row_t527913C609FCA2EFA018E5FDC5013A69A553DF99  : public RuntimeObject
{
public:
	// Rewired.InputAction Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_Row::action
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___action_0;
	// Rewired.AxisRange Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_Row::actionRange
	int32_t ___actionRange_1;
	// UnityEngine.UI.Button Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_Row::button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___button_2;
	// UnityEngine.UI.Text Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_Row::text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___text_3;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(Row_t527913C609FCA2EFA018E5FDC5013A69A553DF99, ___action_0)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_action_0() const { return ___action_0; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_actionRange_1() { return static_cast<int32_t>(offsetof(Row_t527913C609FCA2EFA018E5FDC5013A69A553DF99, ___actionRange_1)); }
	inline int32_t get_actionRange_1() const { return ___actionRange_1; }
	inline int32_t* get_address_of_actionRange_1() { return &___actionRange_1; }
	inline void set_actionRange_1(int32_t value)
	{
		___actionRange_1 = value;
	}

	inline static int32_t get_offset_of_button_2() { return static_cast<int32_t>(offsetof(Row_t527913C609FCA2EFA018E5FDC5013A69A553DF99, ___button_2)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_button_2() const { return ___button_2; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_button_2() { return &___button_2; }
	inline void set_button_2(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___button_2 = value;
		Il2CppCodeGenWriteBarrier((&___button_2), value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(Row_t527913C609FCA2EFA018E5FDC5013A69A553DF99, ___text_3)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_text_3() const { return ___text_3; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROW_T527913C609FCA2EFA018E5FDC5013A69A553DF99_H
#ifndef ROW_T6311A257F15636BEFEDD024070E739E1E899A7A5_H
#define ROW_T6311A257F15636BEFEDD024070E739E1E899A7A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.SimpleControlRemapping_Row
struct  Row_t6311A257F15636BEFEDD024070E739E1E899A7A5  : public RuntimeObject
{
public:
	// Rewired.InputAction Rewired.Demos.SimpleControlRemapping_Row::action
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___action_0;
	// Rewired.AxisRange Rewired.Demos.SimpleControlRemapping_Row::actionRange
	int32_t ___actionRange_1;
	// UnityEngine.UI.Button Rewired.Demos.SimpleControlRemapping_Row::button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___button_2;
	// UnityEngine.UI.Text Rewired.Demos.SimpleControlRemapping_Row::text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___text_3;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(Row_t6311A257F15636BEFEDD024070E739E1E899A7A5, ___action_0)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_action_0() const { return ___action_0; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_actionRange_1() { return static_cast<int32_t>(offsetof(Row_t6311A257F15636BEFEDD024070E739E1E899A7A5, ___actionRange_1)); }
	inline int32_t get_actionRange_1() const { return ___actionRange_1; }
	inline int32_t* get_address_of_actionRange_1() { return &___actionRange_1; }
	inline void set_actionRange_1(int32_t value)
	{
		___actionRange_1 = value;
	}

	inline static int32_t get_offset_of_button_2() { return static_cast<int32_t>(offsetof(Row_t6311A257F15636BEFEDD024070E739E1E899A7A5, ___button_2)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_button_2() const { return ___button_2; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_button_2() { return &___button_2; }
	inline void set_button_2(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___button_2 = value;
		Il2CppCodeGenWriteBarrier((&___button_2), value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(Row_t6311A257F15636BEFEDD024070E739E1E899A7A5, ___text_3)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_text_3() const { return ___text_3; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROW_T6311A257F15636BEFEDD024070E739E1E899A7A5_H
#ifndef IMAGESETTINGS_T2B2F106AAEF92D7897E28D313933626836660334_H
#define IMAGESETTINGS_T2B2F106AAEF92D7897E28D313933626836660334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings
struct  ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334  : public RuntimeObject
{
public:
	// UnityEngine.Color Rewired.UI.ControlMapper.ThemeSettings_ImageSettings::_color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____color_0;
	// UnityEngine.Sprite Rewired.UI.ControlMapper.ThemeSettings_ImageSettings::_sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ____sprite_1;
	// UnityEngine.Material Rewired.UI.ControlMapper.ThemeSettings_ImageSettings::_materal
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____materal_2;
	// UnityEngine.UI.Image_Type Rewired.UI.ControlMapper.ThemeSettings_ImageSettings::_type
	int32_t ____type_3;
	// System.Boolean Rewired.UI.ControlMapper.ThemeSettings_ImageSettings::_preserveAspect
	bool ____preserveAspect_4;
	// System.Boolean Rewired.UI.ControlMapper.ThemeSettings_ImageSettings::_fillCenter
	bool ____fillCenter_5;
	// UnityEngine.UI.Image_FillMethod Rewired.UI.ControlMapper.ThemeSettings_ImageSettings::_fillMethod
	int32_t ____fillMethod_6;
	// System.Single Rewired.UI.ControlMapper.ThemeSettings_ImageSettings::_fillAmout
	float ____fillAmout_7;
	// System.Boolean Rewired.UI.ControlMapper.ThemeSettings_ImageSettings::_fillClockwise
	bool ____fillClockwise_8;
	// System.Int32 Rewired.UI.ControlMapper.ThemeSettings_ImageSettings::_fillOrigin
	int32_t ____fillOrigin_9;

public:
	inline static int32_t get_offset_of__color_0() { return static_cast<int32_t>(offsetof(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334, ____color_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__color_0() const { return ____color_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__color_0() { return &____color_0; }
	inline void set__color_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____color_0 = value;
	}

	inline static int32_t get_offset_of__sprite_1() { return static_cast<int32_t>(offsetof(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334, ____sprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get__sprite_1() const { return ____sprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of__sprite_1() { return &____sprite_1; }
	inline void set__sprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		____sprite_1 = value;
		Il2CppCodeGenWriteBarrier((&____sprite_1), value);
	}

	inline static int32_t get_offset_of__materal_2() { return static_cast<int32_t>(offsetof(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334, ____materal_2)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__materal_2() const { return ____materal_2; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__materal_2() { return &____materal_2; }
	inline void set__materal_2(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____materal_2 = value;
		Il2CppCodeGenWriteBarrier((&____materal_2), value);
	}

	inline static int32_t get_offset_of__type_3() { return static_cast<int32_t>(offsetof(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334, ____type_3)); }
	inline int32_t get__type_3() const { return ____type_3; }
	inline int32_t* get_address_of__type_3() { return &____type_3; }
	inline void set__type_3(int32_t value)
	{
		____type_3 = value;
	}

	inline static int32_t get_offset_of__preserveAspect_4() { return static_cast<int32_t>(offsetof(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334, ____preserveAspect_4)); }
	inline bool get__preserveAspect_4() const { return ____preserveAspect_4; }
	inline bool* get_address_of__preserveAspect_4() { return &____preserveAspect_4; }
	inline void set__preserveAspect_4(bool value)
	{
		____preserveAspect_4 = value;
	}

	inline static int32_t get_offset_of__fillCenter_5() { return static_cast<int32_t>(offsetof(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334, ____fillCenter_5)); }
	inline bool get__fillCenter_5() const { return ____fillCenter_5; }
	inline bool* get_address_of__fillCenter_5() { return &____fillCenter_5; }
	inline void set__fillCenter_5(bool value)
	{
		____fillCenter_5 = value;
	}

	inline static int32_t get_offset_of__fillMethod_6() { return static_cast<int32_t>(offsetof(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334, ____fillMethod_6)); }
	inline int32_t get__fillMethod_6() const { return ____fillMethod_6; }
	inline int32_t* get_address_of__fillMethod_6() { return &____fillMethod_6; }
	inline void set__fillMethod_6(int32_t value)
	{
		____fillMethod_6 = value;
	}

	inline static int32_t get_offset_of__fillAmout_7() { return static_cast<int32_t>(offsetof(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334, ____fillAmout_7)); }
	inline float get__fillAmout_7() const { return ____fillAmout_7; }
	inline float* get_address_of__fillAmout_7() { return &____fillAmout_7; }
	inline void set__fillAmout_7(float value)
	{
		____fillAmout_7 = value;
	}

	inline static int32_t get_offset_of__fillClockwise_8() { return static_cast<int32_t>(offsetof(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334, ____fillClockwise_8)); }
	inline bool get__fillClockwise_8() const { return ____fillClockwise_8; }
	inline bool* get_address_of__fillClockwise_8() { return &____fillClockwise_8; }
	inline void set__fillClockwise_8(bool value)
	{
		____fillClockwise_8 = value;
	}

	inline static int32_t get_offset_of__fillOrigin_9() { return static_cast<int32_t>(offsetof(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334, ____fillOrigin_9)); }
	inline int32_t get__fillOrigin_9() const { return ____fillOrigin_9; }
	inline int32_t* get_address_of__fillOrigin_9() { return &____fillOrigin_9; }
	inline void set__fillOrigin_9(int32_t value)
	{
		____fillOrigin_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGESETTINGS_T2B2F106AAEF92D7897E28D313933626836660334_H
#ifndef SELECTABLESETTINGS_BASE_T76E264AC58FB9CF85893DD251ABD047352C316D9_H
#define SELECTABLESETTINGS_BASE_T76E264AC58FB9CF85893DD251ABD047352C316D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemeSettings_SelectableSettings_Base
struct  SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9  : public RuntimeObject
{
public:
	// UnityEngine.UI.Selectable_Transition Rewired.UI.ControlMapper.ThemeSettings_SelectableSettings_Base::_transition
	int32_t ____transition_0;
	// Rewired.UI.ControlMapper.ThemeSettings_CustomColorBlock Rewired.UI.ControlMapper.ThemeSettings_SelectableSettings_Base::_colors
	CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2  ____colors_1;
	// Rewired.UI.ControlMapper.ThemeSettings_CustomSpriteState Rewired.UI.ControlMapper.ThemeSettings_SelectableSettings_Base::_spriteState
	CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A  ____spriteState_2;
	// Rewired.UI.ControlMapper.ThemeSettings_CustomAnimationTriggers Rewired.UI.ControlMapper.ThemeSettings_SelectableSettings_Base::_animationTriggers
	CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775 * ____animationTriggers_3;

public:
	inline static int32_t get_offset_of__transition_0() { return static_cast<int32_t>(offsetof(SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9, ____transition_0)); }
	inline int32_t get__transition_0() const { return ____transition_0; }
	inline int32_t* get_address_of__transition_0() { return &____transition_0; }
	inline void set__transition_0(int32_t value)
	{
		____transition_0 = value;
	}

	inline static int32_t get_offset_of__colors_1() { return static_cast<int32_t>(offsetof(SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9, ____colors_1)); }
	inline CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2  get__colors_1() const { return ____colors_1; }
	inline CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2 * get_address_of__colors_1() { return &____colors_1; }
	inline void set__colors_1(CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2  value)
	{
		____colors_1 = value;
	}

	inline static int32_t get_offset_of__spriteState_2() { return static_cast<int32_t>(offsetof(SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9, ____spriteState_2)); }
	inline CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A  get__spriteState_2() const { return ____spriteState_2; }
	inline CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A * get_address_of__spriteState_2() { return &____spriteState_2; }
	inline void set__spriteState_2(CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A  value)
	{
		____spriteState_2 = value;
	}

	inline static int32_t get_offset_of__animationTriggers_3() { return static_cast<int32_t>(offsetof(SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9, ____animationTriggers_3)); }
	inline CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775 * get__animationTriggers_3() const { return ____animationTriggers_3; }
	inline CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775 ** get_address_of__animationTriggers_3() { return &____animationTriggers_3; }
	inline void set__animationTriggers_3(CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775 * value)
	{
		____animationTriggers_3 = value;
		Il2CppCodeGenWriteBarrier((&____animationTriggers_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLESETTINGS_BASE_T76E264AC58FB9CF85893DD251ABD047352C316D9_H
#ifndef TEXTSETTINGS_T6C6E84301648111BFD3DE698C0DCFADDE6855881_H
#define TEXTSETTINGS_T6C6E84301648111BFD3DE698C0DCFADDE6855881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemeSettings_TextSettings
struct  TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881  : public RuntimeObject
{
public:
	// UnityEngine.Color Rewired.UI.ControlMapper.ThemeSettings_TextSettings::_color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____color_0;
	// UnityEngine.Font Rewired.UI.ControlMapper.ThemeSettings_TextSettings::_font
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ____font_1;
	// Rewired.UI.ControlMapper.ThemeSettings_FontStyleOverride Rewired.UI.ControlMapper.ThemeSettings_TextSettings::_style
	int32_t ____style_2;
	// System.Single Rewired.UI.ControlMapper.ThemeSettings_TextSettings::_lineSpacing
	float ____lineSpacing_3;
	// System.Single Rewired.UI.ControlMapper.ThemeSettings_TextSettings::_sizeMultiplier
	float ____sizeMultiplier_4;

public:
	inline static int32_t get_offset_of__color_0() { return static_cast<int32_t>(offsetof(TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881, ____color_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__color_0() const { return ____color_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__color_0() { return &____color_0; }
	inline void set__color_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____color_0 = value;
	}

	inline static int32_t get_offset_of__font_1() { return static_cast<int32_t>(offsetof(TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881, ____font_1)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get__font_1() const { return ____font_1; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of__font_1() { return &____font_1; }
	inline void set__font_1(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		____font_1 = value;
		Il2CppCodeGenWriteBarrier((&____font_1), value);
	}

	inline static int32_t get_offset_of__style_2() { return static_cast<int32_t>(offsetof(TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881, ____style_2)); }
	inline int32_t get__style_2() const { return ____style_2; }
	inline int32_t* get_address_of__style_2() { return &____style_2; }
	inline void set__style_2(int32_t value)
	{
		____style_2 = value;
	}

	inline static int32_t get_offset_of__lineSpacing_3() { return static_cast<int32_t>(offsetof(TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881, ____lineSpacing_3)); }
	inline float get__lineSpacing_3() const { return ____lineSpacing_3; }
	inline float* get_address_of__lineSpacing_3() { return &____lineSpacing_3; }
	inline void set__lineSpacing_3(float value)
	{
		____lineSpacing_3 = value;
	}

	inline static int32_t get_offset_of__sizeMultiplier_4() { return static_cast<int32_t>(offsetof(TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881, ____sizeMultiplier_4)); }
	inline float get__sizeMultiplier_4() const { return ____sizeMultiplier_4; }
	inline float* get_address_of__sizeMultiplier_4() { return &____sizeMultiplier_4; }
	inline void set__sizeMultiplier_4(float value)
	{
		____sizeMultiplier_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTSETTINGS_T6C6E84301648111BFD3DE698C0DCFADDE6855881_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef CALIBRATION_TF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C_H
#define CALIBRATION_TF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_Calibration
struct  Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C  : public QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075
{
public:
	// Rewired.Player Rewired.Demos.ControlRemappingDemo1_Calibration::<player>k__BackingField
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___U3CplayerU3Ek__BackingField_5;
	// Rewired.ControllerType Rewired.Demos.ControlRemappingDemo1_Calibration::<controllerType>k__BackingField
	int32_t ___U3CcontrollerTypeU3Ek__BackingField_6;
	// Rewired.Joystick Rewired.Demos.ControlRemappingDemo1_Calibration::<joystick>k__BackingField
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___U3CjoystickU3Ek__BackingField_7;
	// Rewired.CalibrationMap Rewired.Demos.ControlRemappingDemo1_Calibration::<calibrationMap>k__BackingField
	CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 * ___U3CcalibrationMapU3Ek__BackingField_8;
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_Calibration::selectedElementIdentifierId
	int32_t ___selectedElementIdentifierId_9;
	// System.Boolean Rewired.Demos.ControlRemappingDemo1_Calibration::recording
	bool ___recording_10;

public:
	inline static int32_t get_offset_of_U3CplayerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C, ___U3CplayerU3Ek__BackingField_5)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_U3CplayerU3Ek__BackingField_5() const { return ___U3CplayerU3Ek__BackingField_5; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_U3CplayerU3Ek__BackingField_5() { return &___U3CplayerU3Ek__BackingField_5; }
	inline void set_U3CplayerU3Ek__BackingField_5(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___U3CplayerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplayerU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CcontrollerTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C, ___U3CcontrollerTypeU3Ek__BackingField_6)); }
	inline int32_t get_U3CcontrollerTypeU3Ek__BackingField_6() const { return ___U3CcontrollerTypeU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CcontrollerTypeU3Ek__BackingField_6() { return &___U3CcontrollerTypeU3Ek__BackingField_6; }
	inline void set_U3CcontrollerTypeU3Ek__BackingField_6(int32_t value)
	{
		___U3CcontrollerTypeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CjoystickU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C, ___U3CjoystickU3Ek__BackingField_7)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_U3CjoystickU3Ek__BackingField_7() const { return ___U3CjoystickU3Ek__BackingField_7; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_U3CjoystickU3Ek__BackingField_7() { return &___U3CjoystickU3Ek__BackingField_7; }
	inline void set_U3CjoystickU3Ek__BackingField_7(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___U3CjoystickU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjoystickU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CcalibrationMapU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C, ___U3CcalibrationMapU3Ek__BackingField_8)); }
	inline CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 * get_U3CcalibrationMapU3Ek__BackingField_8() const { return ___U3CcalibrationMapU3Ek__BackingField_8; }
	inline CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 ** get_address_of_U3CcalibrationMapU3Ek__BackingField_8() { return &___U3CcalibrationMapU3Ek__BackingField_8; }
	inline void set_U3CcalibrationMapU3Ek__BackingField_8(CalibrationMap_t52D0C36A8E4195533E3CDF25F67BA8DA3EFB51A3 * value)
	{
		___U3CcalibrationMapU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcalibrationMapU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_selectedElementIdentifierId_9() { return static_cast<int32_t>(offsetof(Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C, ___selectedElementIdentifierId_9)); }
	inline int32_t get_selectedElementIdentifierId_9() const { return ___selectedElementIdentifierId_9; }
	inline int32_t* get_address_of_selectedElementIdentifierId_9() { return &___selectedElementIdentifierId_9; }
	inline void set_selectedElementIdentifierId_9(int32_t value)
	{
		___selectedElementIdentifierId_9 = value;
	}

	inline static int32_t get_offset_of_recording_10() { return static_cast<int32_t>(offsetof(Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C, ___recording_10)); }
	inline bool get_recording_10() const { return ___recording_10; }
	inline bool* get_address_of_recording_10() { return &___recording_10; }
	inline void set_recording_10(bool value)
	{
		___recording_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALIBRATION_TF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C_H
#ifndef ELEMENTASSIGNMENTCHANGE_T49582C14BDA64F069F91C19388473C56968576AB_H
#define ELEMENTASSIGNMENTCHANGE_T49582C14BDA64F069F91C19388473C56968576AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_ElementAssignmentChange
struct  ElementAssignmentChange_t49582C14BDA64F069F91C19388473C56968576AB  : public QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075
{
public:
	// Rewired.Demos.ControlRemappingDemo1_ElementAssignmentChangeType Rewired.Demos.ControlRemappingDemo1_ElementAssignmentChange::<changeType>k__BackingField
	int32_t ___U3CchangeTypeU3Ek__BackingField_5;
	// Rewired.InputMapper_Context Rewired.Demos.ControlRemappingDemo1_ElementAssignmentChange::<context>k__BackingField
	Context_tDDC4D5CD175FF95675F41961F66B1383287803A1 * ___U3CcontextU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CchangeTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ElementAssignmentChange_t49582C14BDA64F069F91C19388473C56968576AB, ___U3CchangeTypeU3Ek__BackingField_5)); }
	inline int32_t get_U3CchangeTypeU3Ek__BackingField_5() const { return ___U3CchangeTypeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CchangeTypeU3Ek__BackingField_5() { return &___U3CchangeTypeU3Ek__BackingField_5; }
	inline void set_U3CchangeTypeU3Ek__BackingField_5(int32_t value)
	{
		___U3CchangeTypeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CcontextU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ElementAssignmentChange_t49582C14BDA64F069F91C19388473C56968576AB, ___U3CcontextU3Ek__BackingField_6)); }
	inline Context_tDDC4D5CD175FF95675F41961F66B1383287803A1 * get_U3CcontextU3Ek__BackingField_6() const { return ___U3CcontextU3Ek__BackingField_6; }
	inline Context_tDDC4D5CD175FF95675F41961F66B1383287803A1 ** get_address_of_U3CcontextU3Ek__BackingField_6() { return &___U3CcontextU3Ek__BackingField_6; }
	inline void set_U3CcontextU3Ek__BackingField_6(Context_tDDC4D5CD175FF95675F41961F66B1383287803A1 * value)
	{
		___U3CcontextU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontextU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTASSIGNMENTCHANGE_T49582C14BDA64F069F91C19388473C56968576AB_H
#ifndef FALLBACKJOYSTICKIDENTIFICATION_T9C9B29957528D4E41D258D222915271A533279AB_H
#define FALLBACKJOYSTICKIDENTIFICATION_T9C9B29957528D4E41D258D222915271A533279AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_FallbackJoystickIdentification
struct  FallbackJoystickIdentification_t9C9B29957528D4E41D258D222915271A533279AB  : public QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075
{
public:
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_FallbackJoystickIdentification::<joystickId>k__BackingField
	int32_t ___U3CjoystickIdU3Ek__BackingField_5;
	// System.String Rewired.Demos.ControlRemappingDemo1_FallbackJoystickIdentification::<joystickName>k__BackingField
	String_t* ___U3CjoystickNameU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CjoystickIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FallbackJoystickIdentification_t9C9B29957528D4E41D258D222915271A533279AB, ___U3CjoystickIdU3Ek__BackingField_5)); }
	inline int32_t get_U3CjoystickIdU3Ek__BackingField_5() const { return ___U3CjoystickIdU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CjoystickIdU3Ek__BackingField_5() { return &___U3CjoystickIdU3Ek__BackingField_5; }
	inline void set_U3CjoystickIdU3Ek__BackingField_5(int32_t value)
	{
		___U3CjoystickIdU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CjoystickNameU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FallbackJoystickIdentification_t9C9B29957528D4E41D258D222915271A533279AB, ___U3CjoystickNameU3Ek__BackingField_6)); }
	inline String_t* get_U3CjoystickNameU3Ek__BackingField_6() const { return ___U3CjoystickNameU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CjoystickNameU3Ek__BackingField_6() { return &___U3CjoystickNameU3Ek__BackingField_6; }
	inline void set_U3CjoystickNameU3Ek__BackingField_6(String_t* value)
	{
		___U3CjoystickNameU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjoystickNameU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FALLBACKJOYSTICKIDENTIFICATION_T9C9B29957528D4E41D258D222915271A533279AB_H
#ifndef JOYSTICKASSIGNMENTCHANGE_T30E92AC93BA606A5BAC07757C8727DA55FD28EA3_H
#define JOYSTICKASSIGNMENTCHANGE_T30E92AC93BA606A5BAC07757C8727DA55FD28EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1_JoystickAssignmentChange
struct  JoystickAssignmentChange_t30E92AC93BA606A5BAC07757C8727DA55FD28EA3  : public QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075
{
public:
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_JoystickAssignmentChange::<playerId>k__BackingField
	int32_t ___U3CplayerIdU3Ek__BackingField_5;
	// System.Int32 Rewired.Demos.ControlRemappingDemo1_JoystickAssignmentChange::<joystickId>k__BackingField
	int32_t ___U3CjoystickIdU3Ek__BackingField_6;
	// System.Boolean Rewired.Demos.ControlRemappingDemo1_JoystickAssignmentChange::<assign>k__BackingField
	bool ___U3CassignU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CplayerIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(JoystickAssignmentChange_t30E92AC93BA606A5BAC07757C8727DA55FD28EA3, ___U3CplayerIdU3Ek__BackingField_5)); }
	inline int32_t get_U3CplayerIdU3Ek__BackingField_5() const { return ___U3CplayerIdU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CplayerIdU3Ek__BackingField_5() { return &___U3CplayerIdU3Ek__BackingField_5; }
	inline void set_U3CplayerIdU3Ek__BackingField_5(int32_t value)
	{
		___U3CplayerIdU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CjoystickIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JoystickAssignmentChange_t30E92AC93BA606A5BAC07757C8727DA55FD28EA3, ___U3CjoystickIdU3Ek__BackingField_6)); }
	inline int32_t get_U3CjoystickIdU3Ek__BackingField_6() const { return ___U3CjoystickIdU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CjoystickIdU3Ek__BackingField_6() { return &___U3CjoystickIdU3Ek__BackingField_6; }
	inline void set_U3CjoystickIdU3Ek__BackingField_6(int32_t value)
	{
		___U3CjoystickIdU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CassignU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JoystickAssignmentChange_t30E92AC93BA606A5BAC07757C8727DA55FD28EA3, ___U3CassignU3Ek__BackingField_7)); }
	inline bool get_U3CassignU3Ek__BackingField_7() const { return ___U3CassignU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CassignU3Ek__BackingField_7() { return &___U3CassignU3Ek__BackingField_7; }
	inline void set_U3CassignU3Ek__BackingField_7(bool value)
	{
		___U3CassignU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKASSIGNMENTCHANGE_T30E92AC93BA606A5BAC07757C8727DA55FD28EA3_H
#ifndef THEMESETTINGS_TA2883FBE792D84E1948703D519AFDDA052B97BEF_H
#define THEMESETTINGS_TA2883FBE792D84E1948703D519AFDDA052B97BEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemeSettings
struct  ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings::_mainWindowBackground
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____mainWindowBackground_4;
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings::_popupWindowBackground
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____popupWindowBackground_5;
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings::_areaBackground
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____areaBackground_6;
	// Rewired.UI.ControlMapper.ThemeSettings_SelectableSettings Rewired.UI.ControlMapper.ThemeSettings::_selectableSettings
	SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29 * ____selectableSettings_7;
	// Rewired.UI.ControlMapper.ThemeSettings_SelectableSettings Rewired.UI.ControlMapper.ThemeSettings::_buttonSettings
	SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29 * ____buttonSettings_8;
	// Rewired.UI.ControlMapper.ThemeSettings_SelectableSettings Rewired.UI.ControlMapper.ThemeSettings::_inputGridFieldSettings
	SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29 * ____inputGridFieldSettings_9;
	// Rewired.UI.ControlMapper.ThemeSettings_ScrollbarSettings Rewired.UI.ControlMapper.ThemeSettings::_scrollbarSettings
	ScrollbarSettings_tD2F8212CF389C6CC9C87CBB7985294663B3C791E * ____scrollbarSettings_10;
	// Rewired.UI.ControlMapper.ThemeSettings_SliderSettings Rewired.UI.ControlMapper.ThemeSettings::_sliderSettings
	SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA * ____sliderSettings_11;
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings::_invertToggle
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____invertToggle_12;
	// UnityEngine.Color Rewired.UI.ControlMapper.ThemeSettings::_invertToggleDisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____invertToggleDisabledColor_13;
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings::_calibrationBackground
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____calibrationBackground_14;
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings::_calibrationValueMarker
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____calibrationValueMarker_15;
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings::_calibrationRawValueMarker
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____calibrationRawValueMarker_16;
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings::_calibrationZeroMarker
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____calibrationZeroMarker_17;
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings::_calibrationCalibratedZeroMarker
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____calibrationCalibratedZeroMarker_18;
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings::_calibrationDeadzone
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____calibrationDeadzone_19;
	// Rewired.UI.ControlMapper.ThemeSettings_TextSettings Rewired.UI.ControlMapper.ThemeSettings::_textSettings
	TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881 * ____textSettings_20;
	// Rewired.UI.ControlMapper.ThemeSettings_TextSettings Rewired.UI.ControlMapper.ThemeSettings::_buttonTextSettings
	TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881 * ____buttonTextSettings_21;
	// Rewired.UI.ControlMapper.ThemeSettings_TextSettings Rewired.UI.ControlMapper.ThemeSettings::_inputGridFieldTextSettings
	TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881 * ____inputGridFieldTextSettings_22;

public:
	inline static int32_t get_offset_of__mainWindowBackground_4() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____mainWindowBackground_4)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__mainWindowBackground_4() const { return ____mainWindowBackground_4; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__mainWindowBackground_4() { return &____mainWindowBackground_4; }
	inline void set__mainWindowBackground_4(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____mainWindowBackground_4 = value;
		Il2CppCodeGenWriteBarrier((&____mainWindowBackground_4), value);
	}

	inline static int32_t get_offset_of__popupWindowBackground_5() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____popupWindowBackground_5)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__popupWindowBackground_5() const { return ____popupWindowBackground_5; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__popupWindowBackground_5() { return &____popupWindowBackground_5; }
	inline void set__popupWindowBackground_5(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____popupWindowBackground_5 = value;
		Il2CppCodeGenWriteBarrier((&____popupWindowBackground_5), value);
	}

	inline static int32_t get_offset_of__areaBackground_6() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____areaBackground_6)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__areaBackground_6() const { return ____areaBackground_6; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__areaBackground_6() { return &____areaBackground_6; }
	inline void set__areaBackground_6(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____areaBackground_6 = value;
		Il2CppCodeGenWriteBarrier((&____areaBackground_6), value);
	}

	inline static int32_t get_offset_of__selectableSettings_7() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____selectableSettings_7)); }
	inline SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29 * get__selectableSettings_7() const { return ____selectableSettings_7; }
	inline SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29 ** get_address_of__selectableSettings_7() { return &____selectableSettings_7; }
	inline void set__selectableSettings_7(SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29 * value)
	{
		____selectableSettings_7 = value;
		Il2CppCodeGenWriteBarrier((&____selectableSettings_7), value);
	}

	inline static int32_t get_offset_of__buttonSettings_8() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____buttonSettings_8)); }
	inline SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29 * get__buttonSettings_8() const { return ____buttonSettings_8; }
	inline SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29 ** get_address_of__buttonSettings_8() { return &____buttonSettings_8; }
	inline void set__buttonSettings_8(SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29 * value)
	{
		____buttonSettings_8 = value;
		Il2CppCodeGenWriteBarrier((&____buttonSettings_8), value);
	}

	inline static int32_t get_offset_of__inputGridFieldSettings_9() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____inputGridFieldSettings_9)); }
	inline SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29 * get__inputGridFieldSettings_9() const { return ____inputGridFieldSettings_9; }
	inline SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29 ** get_address_of__inputGridFieldSettings_9() { return &____inputGridFieldSettings_9; }
	inline void set__inputGridFieldSettings_9(SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29 * value)
	{
		____inputGridFieldSettings_9 = value;
		Il2CppCodeGenWriteBarrier((&____inputGridFieldSettings_9), value);
	}

	inline static int32_t get_offset_of__scrollbarSettings_10() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____scrollbarSettings_10)); }
	inline ScrollbarSettings_tD2F8212CF389C6CC9C87CBB7985294663B3C791E * get__scrollbarSettings_10() const { return ____scrollbarSettings_10; }
	inline ScrollbarSettings_tD2F8212CF389C6CC9C87CBB7985294663B3C791E ** get_address_of__scrollbarSettings_10() { return &____scrollbarSettings_10; }
	inline void set__scrollbarSettings_10(ScrollbarSettings_tD2F8212CF389C6CC9C87CBB7985294663B3C791E * value)
	{
		____scrollbarSettings_10 = value;
		Il2CppCodeGenWriteBarrier((&____scrollbarSettings_10), value);
	}

	inline static int32_t get_offset_of__sliderSettings_11() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____sliderSettings_11)); }
	inline SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA * get__sliderSettings_11() const { return ____sliderSettings_11; }
	inline SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA ** get_address_of__sliderSettings_11() { return &____sliderSettings_11; }
	inline void set__sliderSettings_11(SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA * value)
	{
		____sliderSettings_11 = value;
		Il2CppCodeGenWriteBarrier((&____sliderSettings_11), value);
	}

	inline static int32_t get_offset_of__invertToggle_12() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____invertToggle_12)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__invertToggle_12() const { return ____invertToggle_12; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__invertToggle_12() { return &____invertToggle_12; }
	inline void set__invertToggle_12(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____invertToggle_12 = value;
		Il2CppCodeGenWriteBarrier((&____invertToggle_12), value);
	}

	inline static int32_t get_offset_of__invertToggleDisabledColor_13() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____invertToggleDisabledColor_13)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__invertToggleDisabledColor_13() const { return ____invertToggleDisabledColor_13; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__invertToggleDisabledColor_13() { return &____invertToggleDisabledColor_13; }
	inline void set__invertToggleDisabledColor_13(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____invertToggleDisabledColor_13 = value;
	}

	inline static int32_t get_offset_of__calibrationBackground_14() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____calibrationBackground_14)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__calibrationBackground_14() const { return ____calibrationBackground_14; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__calibrationBackground_14() { return &____calibrationBackground_14; }
	inline void set__calibrationBackground_14(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____calibrationBackground_14 = value;
		Il2CppCodeGenWriteBarrier((&____calibrationBackground_14), value);
	}

	inline static int32_t get_offset_of__calibrationValueMarker_15() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____calibrationValueMarker_15)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__calibrationValueMarker_15() const { return ____calibrationValueMarker_15; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__calibrationValueMarker_15() { return &____calibrationValueMarker_15; }
	inline void set__calibrationValueMarker_15(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____calibrationValueMarker_15 = value;
		Il2CppCodeGenWriteBarrier((&____calibrationValueMarker_15), value);
	}

	inline static int32_t get_offset_of__calibrationRawValueMarker_16() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____calibrationRawValueMarker_16)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__calibrationRawValueMarker_16() const { return ____calibrationRawValueMarker_16; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__calibrationRawValueMarker_16() { return &____calibrationRawValueMarker_16; }
	inline void set__calibrationRawValueMarker_16(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____calibrationRawValueMarker_16 = value;
		Il2CppCodeGenWriteBarrier((&____calibrationRawValueMarker_16), value);
	}

	inline static int32_t get_offset_of__calibrationZeroMarker_17() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____calibrationZeroMarker_17)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__calibrationZeroMarker_17() const { return ____calibrationZeroMarker_17; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__calibrationZeroMarker_17() { return &____calibrationZeroMarker_17; }
	inline void set__calibrationZeroMarker_17(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____calibrationZeroMarker_17 = value;
		Il2CppCodeGenWriteBarrier((&____calibrationZeroMarker_17), value);
	}

	inline static int32_t get_offset_of__calibrationCalibratedZeroMarker_18() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____calibrationCalibratedZeroMarker_18)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__calibrationCalibratedZeroMarker_18() const { return ____calibrationCalibratedZeroMarker_18; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__calibrationCalibratedZeroMarker_18() { return &____calibrationCalibratedZeroMarker_18; }
	inline void set__calibrationCalibratedZeroMarker_18(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____calibrationCalibratedZeroMarker_18 = value;
		Il2CppCodeGenWriteBarrier((&____calibrationCalibratedZeroMarker_18), value);
	}

	inline static int32_t get_offset_of__calibrationDeadzone_19() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____calibrationDeadzone_19)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__calibrationDeadzone_19() const { return ____calibrationDeadzone_19; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__calibrationDeadzone_19() { return &____calibrationDeadzone_19; }
	inline void set__calibrationDeadzone_19(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____calibrationDeadzone_19 = value;
		Il2CppCodeGenWriteBarrier((&____calibrationDeadzone_19), value);
	}

	inline static int32_t get_offset_of__textSettings_20() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____textSettings_20)); }
	inline TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881 * get__textSettings_20() const { return ____textSettings_20; }
	inline TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881 ** get_address_of__textSettings_20() { return &____textSettings_20; }
	inline void set__textSettings_20(TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881 * value)
	{
		____textSettings_20 = value;
		Il2CppCodeGenWriteBarrier((&____textSettings_20), value);
	}

	inline static int32_t get_offset_of__buttonTextSettings_21() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____buttonTextSettings_21)); }
	inline TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881 * get__buttonTextSettings_21() const { return ____buttonTextSettings_21; }
	inline TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881 ** get_address_of__buttonTextSettings_21() { return &____buttonTextSettings_21; }
	inline void set__buttonTextSettings_21(TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881 * value)
	{
		____buttonTextSettings_21 = value;
		Il2CppCodeGenWriteBarrier((&____buttonTextSettings_21), value);
	}

	inline static int32_t get_offset_of__inputGridFieldTextSettings_22() { return static_cast<int32_t>(offsetof(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF, ____inputGridFieldTextSettings_22)); }
	inline TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881 * get__inputGridFieldTextSettings_22() const { return ____inputGridFieldTextSettings_22; }
	inline TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881 ** get_address_of__inputGridFieldTextSettings_22() { return &____inputGridFieldTextSettings_22; }
	inline void set__inputGridFieldTextSettings_22(TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881 * value)
	{
		____inputGridFieldTextSettings_22 = value;
		Il2CppCodeGenWriteBarrier((&____inputGridFieldTextSettings_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THEMESETTINGS_TA2883FBE792D84E1948703D519AFDDA052B97BEF_H
#ifndef SCROLLBARSETTINGS_TD2F8212CF389C6CC9C87CBB7985294663B3C791E_H
#define SCROLLBARSETTINGS_TD2F8212CF389C6CC9C87CBB7985294663B3C791E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemeSettings_ScrollbarSettings
struct  ScrollbarSettings_tD2F8212CF389C6CC9C87CBB7985294663B3C791E  : public SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9
{
public:
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings_ScrollbarSettings::_handleImageSettings
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____handleImageSettings_4;
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings_ScrollbarSettings::_backgroundImageSettings
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____backgroundImageSettings_5;

public:
	inline static int32_t get_offset_of__handleImageSettings_4() { return static_cast<int32_t>(offsetof(ScrollbarSettings_tD2F8212CF389C6CC9C87CBB7985294663B3C791E, ____handleImageSettings_4)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__handleImageSettings_4() const { return ____handleImageSettings_4; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__handleImageSettings_4() { return &____handleImageSettings_4; }
	inline void set__handleImageSettings_4(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____handleImageSettings_4 = value;
		Il2CppCodeGenWriteBarrier((&____handleImageSettings_4), value);
	}

	inline static int32_t get_offset_of__backgroundImageSettings_5() { return static_cast<int32_t>(offsetof(ScrollbarSettings_tD2F8212CF389C6CC9C87CBB7985294663B3C791E, ____backgroundImageSettings_5)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__backgroundImageSettings_5() const { return ____backgroundImageSettings_5; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__backgroundImageSettings_5() { return &____backgroundImageSettings_5; }
	inline void set__backgroundImageSettings_5(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____backgroundImageSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&____backgroundImageSettings_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBARSETTINGS_TD2F8212CF389C6CC9C87CBB7985294663B3C791E_H
#ifndef SELECTABLESETTINGS_T405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29_H
#define SELECTABLESETTINGS_T405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemeSettings_SelectableSettings
struct  SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29  : public SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9
{
public:
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings_SelectableSettings::_imageSettings
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____imageSettings_4;

public:
	inline static int32_t get_offset_of__imageSettings_4() { return static_cast<int32_t>(offsetof(SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29, ____imageSettings_4)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__imageSettings_4() const { return ____imageSettings_4; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__imageSettings_4() { return &____imageSettings_4; }
	inline void set__imageSettings_4(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____imageSettings_4 = value;
		Il2CppCodeGenWriteBarrier((&____imageSettings_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLESETTINGS_T405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29_H
#ifndef SLIDERSETTINGS_T1720381297B7AD1955027301474A71FB41F571DA_H
#define SLIDERSETTINGS_T1720381297B7AD1955027301474A71FB41F571DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemeSettings_SliderSettings
struct  SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA  : public SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9
{
public:
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings_SliderSettings::_handleImageSettings
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____handleImageSettings_4;
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings_SliderSettings::_fillImageSettings
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____fillImageSettings_5;
	// Rewired.UI.ControlMapper.ThemeSettings_ImageSettings Rewired.UI.ControlMapper.ThemeSettings_SliderSettings::_backgroundImageSettings
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * ____backgroundImageSettings_6;

public:
	inline static int32_t get_offset_of__handleImageSettings_4() { return static_cast<int32_t>(offsetof(SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA, ____handleImageSettings_4)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__handleImageSettings_4() const { return ____handleImageSettings_4; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__handleImageSettings_4() { return &____handleImageSettings_4; }
	inline void set__handleImageSettings_4(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____handleImageSettings_4 = value;
		Il2CppCodeGenWriteBarrier((&____handleImageSettings_4), value);
	}

	inline static int32_t get_offset_of__fillImageSettings_5() { return static_cast<int32_t>(offsetof(SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA, ____fillImageSettings_5)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__fillImageSettings_5() const { return ____fillImageSettings_5; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__fillImageSettings_5() { return &____fillImageSettings_5; }
	inline void set__fillImageSettings_5(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____fillImageSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&____fillImageSettings_5), value);
	}

	inline static int32_t get_offset_of__backgroundImageSettings_6() { return static_cast<int32_t>(offsetof(SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA, ____backgroundImageSettings_6)); }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * get__backgroundImageSettings_6() const { return ____backgroundImageSettings_6; }
	inline ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 ** get_address_of__backgroundImageSettings_6() { return &____backgroundImageSettings_6; }
	inline void set__backgroundImageSettings_6(ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334 * value)
	{
		____backgroundImageSettings_6 = value;
		Il2CppCodeGenWriteBarrier((&____backgroundImageSettings_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDERSETTINGS_T1720381297B7AD1955027301474A71FB41F571DA_H
#ifndef TMP_INPUTVALIDATOR_T4C673E12211AFB82AAF94D9DEA556FDC306E69CD_H
#define TMP_INPUTVALIDATOR_T4C673E12211AFB82AAF94D9DEA556FDC306E69CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputValidator
struct  TMP_InputValidator_t4C673E12211AFB82AAF94D9DEA556FDC306E69CD  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTVALIDATOR_T4C673E12211AFB82AAF94D9DEA556FDC306E69CD_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef TMP_DIGITVALIDATOR_TD53B3EF123D04F923055895ED56555317D239AB5_H
#define TMP_DIGITVALIDATOR_TD53B3EF123D04F923055895ED56555317D239AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_DigitValidator
struct  TMP_DigitValidator_tD53B3EF123D04F923055895ED56555317D239AB5  : public TMP_InputValidator_t4C673E12211AFB82AAF94D9DEA556FDC306E69CD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_DIGITVALIDATOR_TD53B3EF123D04F923055895ED56555317D239AB5_H
#ifndef TMP_PHONENUMBERVALIDATOR_T7EB41CFDB7C6AA586BF5AF04151FC2228F565BD2_H
#define TMP_PHONENUMBERVALIDATOR_T7EB41CFDB7C6AA586BF5AF04151FC2228F565BD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PhoneNumberValidator
struct  TMP_PhoneNumberValidator_t7EB41CFDB7C6AA586BF5AF04151FC2228F565BD2  : public TMP_InputValidator_t4C673E12211AFB82AAF94D9DEA556FDC306E69CD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PHONENUMBERVALIDATOR_T7EB41CFDB7C6AA586BF5AF04151FC2228F565BD2_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef BULLET_T9D5DCCAF7BECEA01BC40DFB414AB0C3D8EBFB418_H
#define BULLET_T9D5DCCAF7BECEA01BC40DFB414AB0C3D8EBFB418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.Bullet
struct  Bullet_t9D5DCCAF7BECEA01BC40DFB414AB0C3D8EBFB418  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Rewired.Demos.Bullet::lifeTime
	float ___lifeTime_4;
	// System.Boolean Rewired.Demos.Bullet::die
	bool ___die_5;
	// System.Single Rewired.Demos.Bullet::deathTime
	float ___deathTime_6;

public:
	inline static int32_t get_offset_of_lifeTime_4() { return static_cast<int32_t>(offsetof(Bullet_t9D5DCCAF7BECEA01BC40DFB414AB0C3D8EBFB418, ___lifeTime_4)); }
	inline float get_lifeTime_4() const { return ___lifeTime_4; }
	inline float* get_address_of_lifeTime_4() { return &___lifeTime_4; }
	inline void set_lifeTime_4(float value)
	{
		___lifeTime_4 = value;
	}

	inline static int32_t get_offset_of_die_5() { return static_cast<int32_t>(offsetof(Bullet_t9D5DCCAF7BECEA01BC40DFB414AB0C3D8EBFB418, ___die_5)); }
	inline bool get_die_5() const { return ___die_5; }
	inline bool* get_address_of_die_5() { return &___die_5; }
	inline void set_die_5(bool value)
	{
		___die_5 = value;
	}

	inline static int32_t get_offset_of_deathTime_6() { return static_cast<int32_t>(offsetof(Bullet_t9D5DCCAF7BECEA01BC40DFB414AB0C3D8EBFB418, ___deathTime_6)); }
	inline float get_deathTime_6() const { return ___deathTime_6; }
	inline float* get_address_of_deathTime_6() { return &___deathTime_6; }
	inline void set_deathTime_6(float value)
	{
		___deathTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULLET_T9D5DCCAF7BECEA01BC40DFB414AB0C3D8EBFB418_H
#ifndef CONTROLMAPPERDEMOMESSAGE_TCDC84E65EDB19FA393B98CB685326785436108AB_H
#define CONTROLMAPPERDEMOMESSAGE_TCDC84E65EDB19FA393B98CB685326785436108AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlMapperDemoMessage
struct  ControlMapperDemoMessage_tCDC84E65EDB19FA393B98CB685326785436108AB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.UI.ControlMapper.ControlMapper Rewired.Demos.ControlMapperDemoMessage::controlMapper
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * ___controlMapper_4;
	// UnityEngine.UI.Selectable Rewired.Demos.ControlMapperDemoMessage::defaultSelectable
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___defaultSelectable_5;

public:
	inline static int32_t get_offset_of_controlMapper_4() { return static_cast<int32_t>(offsetof(ControlMapperDemoMessage_tCDC84E65EDB19FA393B98CB685326785436108AB, ___controlMapper_4)); }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * get_controlMapper_4() const { return ___controlMapper_4; }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 ** get_address_of_controlMapper_4() { return &___controlMapper_4; }
	inline void set_controlMapper_4(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * value)
	{
		___controlMapper_4 = value;
		Il2CppCodeGenWriteBarrier((&___controlMapper_4), value);
	}

	inline static int32_t get_offset_of_defaultSelectable_5() { return static_cast<int32_t>(offsetof(ControlMapperDemoMessage_tCDC84E65EDB19FA393B98CB685326785436108AB, ___defaultSelectable_5)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_defaultSelectable_5() const { return ___defaultSelectable_5; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_defaultSelectable_5() { return &___defaultSelectable_5; }
	inline void set_defaultSelectable_5(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___defaultSelectable_5 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSelectable_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLMAPPERDEMOMESSAGE_TCDC84E65EDB19FA393B98CB685326785436108AB_H
#ifndef CONTROLREMAPPINGDEMO1_T9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0_H
#define CONTROLREMAPPINGDEMO1_T9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.ControlRemappingDemo1
struct  ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.Demos.ControlRemappingDemo1_DialogHelper Rewired.Demos.ControlRemappingDemo1::dialog
	DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769 * ___dialog_7;
	// Rewired.InputMapper Rewired.Demos.ControlRemappingDemo1::inputMapper
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * ___inputMapper_8;
	// Rewired.InputMapper_ConflictFoundEventData Rewired.Demos.ControlRemappingDemo1::conflictFoundEventData
	ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966 * ___conflictFoundEventData_9;
	// System.Boolean Rewired.Demos.ControlRemappingDemo1::guiState
	bool ___guiState_10;
	// System.Boolean Rewired.Demos.ControlRemappingDemo1::busy
	bool ___busy_11;
	// System.Boolean Rewired.Demos.ControlRemappingDemo1::pageGUIState
	bool ___pageGUIState_12;
	// Rewired.Player Rewired.Demos.ControlRemappingDemo1::selectedPlayer
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___selectedPlayer_13;
	// System.Int32 Rewired.Demos.ControlRemappingDemo1::selectedMapCategoryId
	int32_t ___selectedMapCategoryId_14;
	// Rewired.Demos.ControlRemappingDemo1_ControllerSelection Rewired.Demos.ControlRemappingDemo1::selectedController
	ControllerSelection_t54D35C54472361116F871407E095920CDB855060 * ___selectedController_15;
	// Rewired.ControllerMap Rewired.Demos.ControlRemappingDemo1::selectedMap
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___selectedMap_16;
	// System.Boolean Rewired.Demos.ControlRemappingDemo1::showMenu
	bool ___showMenu_17;
	// System.Boolean Rewired.Demos.ControlRemappingDemo1::startListening
	bool ___startListening_18;
	// UnityEngine.Vector2 Rewired.Demos.ControlRemappingDemo1::actionScrollPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___actionScrollPos_19;
	// UnityEngine.Vector2 Rewired.Demos.ControlRemappingDemo1::calibrateScrollPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___calibrateScrollPos_20;
	// System.Collections.Generic.Queue`1<Rewired.Demos.ControlRemappingDemo1_QueueEntry> Rewired.Demos.ControlRemappingDemo1::actionQueue
	Queue_1_t3E8D76F296626DD8BC156049D08357B155EF6502 * ___actionQueue_21;
	// System.Boolean Rewired.Demos.ControlRemappingDemo1::setupFinished
	bool ___setupFinished_22;
	// System.Boolean Rewired.Demos.ControlRemappingDemo1::initialized
	bool ___initialized_23;
	// System.Boolean Rewired.Demos.ControlRemappingDemo1::isCompiling
	bool ___isCompiling_24;
	// UnityEngine.GUIStyle Rewired.Demos.ControlRemappingDemo1::style_wordWrap
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___style_wordWrap_25;
	// UnityEngine.GUIStyle Rewired.Demos.ControlRemappingDemo1::style_centeredBox
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___style_centeredBox_26;

public:
	inline static int32_t get_offset_of_dialog_7() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___dialog_7)); }
	inline DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769 * get_dialog_7() const { return ___dialog_7; }
	inline DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769 ** get_address_of_dialog_7() { return &___dialog_7; }
	inline void set_dialog_7(DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769 * value)
	{
		___dialog_7 = value;
		Il2CppCodeGenWriteBarrier((&___dialog_7), value);
	}

	inline static int32_t get_offset_of_inputMapper_8() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___inputMapper_8)); }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * get_inputMapper_8() const { return ___inputMapper_8; }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB ** get_address_of_inputMapper_8() { return &___inputMapper_8; }
	inline void set_inputMapper_8(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * value)
	{
		___inputMapper_8 = value;
		Il2CppCodeGenWriteBarrier((&___inputMapper_8), value);
	}

	inline static int32_t get_offset_of_conflictFoundEventData_9() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___conflictFoundEventData_9)); }
	inline ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966 * get_conflictFoundEventData_9() const { return ___conflictFoundEventData_9; }
	inline ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966 ** get_address_of_conflictFoundEventData_9() { return &___conflictFoundEventData_9; }
	inline void set_conflictFoundEventData_9(ConflictFoundEventData_tF7B847B50A4A524CBF545F22446C6809656D1966 * value)
	{
		___conflictFoundEventData_9 = value;
		Il2CppCodeGenWriteBarrier((&___conflictFoundEventData_9), value);
	}

	inline static int32_t get_offset_of_guiState_10() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___guiState_10)); }
	inline bool get_guiState_10() const { return ___guiState_10; }
	inline bool* get_address_of_guiState_10() { return &___guiState_10; }
	inline void set_guiState_10(bool value)
	{
		___guiState_10 = value;
	}

	inline static int32_t get_offset_of_busy_11() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___busy_11)); }
	inline bool get_busy_11() const { return ___busy_11; }
	inline bool* get_address_of_busy_11() { return &___busy_11; }
	inline void set_busy_11(bool value)
	{
		___busy_11 = value;
	}

	inline static int32_t get_offset_of_pageGUIState_12() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___pageGUIState_12)); }
	inline bool get_pageGUIState_12() const { return ___pageGUIState_12; }
	inline bool* get_address_of_pageGUIState_12() { return &___pageGUIState_12; }
	inline void set_pageGUIState_12(bool value)
	{
		___pageGUIState_12 = value;
	}

	inline static int32_t get_offset_of_selectedPlayer_13() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___selectedPlayer_13)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_selectedPlayer_13() const { return ___selectedPlayer_13; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_selectedPlayer_13() { return &___selectedPlayer_13; }
	inline void set_selectedPlayer_13(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___selectedPlayer_13 = value;
		Il2CppCodeGenWriteBarrier((&___selectedPlayer_13), value);
	}

	inline static int32_t get_offset_of_selectedMapCategoryId_14() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___selectedMapCategoryId_14)); }
	inline int32_t get_selectedMapCategoryId_14() const { return ___selectedMapCategoryId_14; }
	inline int32_t* get_address_of_selectedMapCategoryId_14() { return &___selectedMapCategoryId_14; }
	inline void set_selectedMapCategoryId_14(int32_t value)
	{
		___selectedMapCategoryId_14 = value;
	}

	inline static int32_t get_offset_of_selectedController_15() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___selectedController_15)); }
	inline ControllerSelection_t54D35C54472361116F871407E095920CDB855060 * get_selectedController_15() const { return ___selectedController_15; }
	inline ControllerSelection_t54D35C54472361116F871407E095920CDB855060 ** get_address_of_selectedController_15() { return &___selectedController_15; }
	inline void set_selectedController_15(ControllerSelection_t54D35C54472361116F871407E095920CDB855060 * value)
	{
		___selectedController_15 = value;
		Il2CppCodeGenWriteBarrier((&___selectedController_15), value);
	}

	inline static int32_t get_offset_of_selectedMap_16() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___selectedMap_16)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_selectedMap_16() const { return ___selectedMap_16; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_selectedMap_16() { return &___selectedMap_16; }
	inline void set_selectedMap_16(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___selectedMap_16 = value;
		Il2CppCodeGenWriteBarrier((&___selectedMap_16), value);
	}

	inline static int32_t get_offset_of_showMenu_17() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___showMenu_17)); }
	inline bool get_showMenu_17() const { return ___showMenu_17; }
	inline bool* get_address_of_showMenu_17() { return &___showMenu_17; }
	inline void set_showMenu_17(bool value)
	{
		___showMenu_17 = value;
	}

	inline static int32_t get_offset_of_startListening_18() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___startListening_18)); }
	inline bool get_startListening_18() const { return ___startListening_18; }
	inline bool* get_address_of_startListening_18() { return &___startListening_18; }
	inline void set_startListening_18(bool value)
	{
		___startListening_18 = value;
	}

	inline static int32_t get_offset_of_actionScrollPos_19() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___actionScrollPos_19)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_actionScrollPos_19() const { return ___actionScrollPos_19; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_actionScrollPos_19() { return &___actionScrollPos_19; }
	inline void set_actionScrollPos_19(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___actionScrollPos_19 = value;
	}

	inline static int32_t get_offset_of_calibrateScrollPos_20() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___calibrateScrollPos_20)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_calibrateScrollPos_20() const { return ___calibrateScrollPos_20; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_calibrateScrollPos_20() { return &___calibrateScrollPos_20; }
	inline void set_calibrateScrollPos_20(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___calibrateScrollPos_20 = value;
	}

	inline static int32_t get_offset_of_actionQueue_21() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___actionQueue_21)); }
	inline Queue_1_t3E8D76F296626DD8BC156049D08357B155EF6502 * get_actionQueue_21() const { return ___actionQueue_21; }
	inline Queue_1_t3E8D76F296626DD8BC156049D08357B155EF6502 ** get_address_of_actionQueue_21() { return &___actionQueue_21; }
	inline void set_actionQueue_21(Queue_1_t3E8D76F296626DD8BC156049D08357B155EF6502 * value)
	{
		___actionQueue_21 = value;
		Il2CppCodeGenWriteBarrier((&___actionQueue_21), value);
	}

	inline static int32_t get_offset_of_setupFinished_22() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___setupFinished_22)); }
	inline bool get_setupFinished_22() const { return ___setupFinished_22; }
	inline bool* get_address_of_setupFinished_22() { return &___setupFinished_22; }
	inline void set_setupFinished_22(bool value)
	{
		___setupFinished_22 = value;
	}

	inline static int32_t get_offset_of_initialized_23() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___initialized_23)); }
	inline bool get_initialized_23() const { return ___initialized_23; }
	inline bool* get_address_of_initialized_23() { return &___initialized_23; }
	inline void set_initialized_23(bool value)
	{
		___initialized_23 = value;
	}

	inline static int32_t get_offset_of_isCompiling_24() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___isCompiling_24)); }
	inline bool get_isCompiling_24() const { return ___isCompiling_24; }
	inline bool* get_address_of_isCompiling_24() { return &___isCompiling_24; }
	inline void set_isCompiling_24(bool value)
	{
		___isCompiling_24 = value;
	}

	inline static int32_t get_offset_of_style_wordWrap_25() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___style_wordWrap_25)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_style_wordWrap_25() const { return ___style_wordWrap_25; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_style_wordWrap_25() { return &___style_wordWrap_25; }
	inline void set_style_wordWrap_25(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___style_wordWrap_25 = value;
		Il2CppCodeGenWriteBarrier((&___style_wordWrap_25), value);
	}

	inline static int32_t get_offset_of_style_centeredBox_26() { return static_cast<int32_t>(offsetof(ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0, ___style_centeredBox_26)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_style_centeredBox_26() const { return ___style_centeredBox_26; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_style_centeredBox_26() { return &___style_centeredBox_26; }
	inline void set_style_centeredBox_26(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___style_centeredBox_26 = value;
		Il2CppCodeGenWriteBarrier((&___style_centeredBox_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLREMAPPINGDEMO1_T9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0_H
#ifndef CUSTOMCONTROLLERDEMO_T960BD05C542B8580E69F73C92EB18970E15444F5_H
#define CUSTOMCONTROLLERDEMO_T960BD05C542B8580E69F73C92EB18970E15444F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.CustomControllerDemo
struct  CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Rewired.Demos.CustomControllerDemo::playerId
	int32_t ___playerId_4;
	// System.String Rewired.Demos.CustomControllerDemo::controllerTag
	String_t* ___controllerTag_5;
	// System.Boolean Rewired.Demos.CustomControllerDemo::useUpdateCallbacks
	bool ___useUpdateCallbacks_6;
	// System.Int32 Rewired.Demos.CustomControllerDemo::buttonCount
	int32_t ___buttonCount_7;
	// System.Int32 Rewired.Demos.CustomControllerDemo::axisCount
	int32_t ___axisCount_8;
	// System.Single[] Rewired.Demos.CustomControllerDemo::axisValues
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___axisValues_9;
	// System.Boolean[] Rewired.Demos.CustomControllerDemo::buttonValues
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___buttonValues_10;
	// Rewired.Demos.TouchJoystickExample[] Rewired.Demos.CustomControllerDemo::joysticks
	TouchJoystickExampleU5BU5D_tAECDB332525B37004D94DEB18DC34FD2CDB5561E* ___joysticks_11;
	// Rewired.Demos.TouchButtonExample[] Rewired.Demos.CustomControllerDemo::buttons
	TouchButtonExampleU5BU5D_t95076935AE312ABDCCA9F593C220A33C5BED51C2* ___buttons_12;
	// Rewired.CustomController Rewired.Demos.CustomControllerDemo::controller
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * ___controller_13;
	// System.Boolean Rewired.Demos.CustomControllerDemo::initialized
	bool ___initialized_14;

public:
	inline static int32_t get_offset_of_playerId_4() { return static_cast<int32_t>(offsetof(CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5, ___playerId_4)); }
	inline int32_t get_playerId_4() const { return ___playerId_4; }
	inline int32_t* get_address_of_playerId_4() { return &___playerId_4; }
	inline void set_playerId_4(int32_t value)
	{
		___playerId_4 = value;
	}

	inline static int32_t get_offset_of_controllerTag_5() { return static_cast<int32_t>(offsetof(CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5, ___controllerTag_5)); }
	inline String_t* get_controllerTag_5() const { return ___controllerTag_5; }
	inline String_t** get_address_of_controllerTag_5() { return &___controllerTag_5; }
	inline void set_controllerTag_5(String_t* value)
	{
		___controllerTag_5 = value;
		Il2CppCodeGenWriteBarrier((&___controllerTag_5), value);
	}

	inline static int32_t get_offset_of_useUpdateCallbacks_6() { return static_cast<int32_t>(offsetof(CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5, ___useUpdateCallbacks_6)); }
	inline bool get_useUpdateCallbacks_6() const { return ___useUpdateCallbacks_6; }
	inline bool* get_address_of_useUpdateCallbacks_6() { return &___useUpdateCallbacks_6; }
	inline void set_useUpdateCallbacks_6(bool value)
	{
		___useUpdateCallbacks_6 = value;
	}

	inline static int32_t get_offset_of_buttonCount_7() { return static_cast<int32_t>(offsetof(CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5, ___buttonCount_7)); }
	inline int32_t get_buttonCount_7() const { return ___buttonCount_7; }
	inline int32_t* get_address_of_buttonCount_7() { return &___buttonCount_7; }
	inline void set_buttonCount_7(int32_t value)
	{
		___buttonCount_7 = value;
	}

	inline static int32_t get_offset_of_axisCount_8() { return static_cast<int32_t>(offsetof(CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5, ___axisCount_8)); }
	inline int32_t get_axisCount_8() const { return ___axisCount_8; }
	inline int32_t* get_address_of_axisCount_8() { return &___axisCount_8; }
	inline void set_axisCount_8(int32_t value)
	{
		___axisCount_8 = value;
	}

	inline static int32_t get_offset_of_axisValues_9() { return static_cast<int32_t>(offsetof(CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5, ___axisValues_9)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_axisValues_9() const { return ___axisValues_9; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_axisValues_9() { return &___axisValues_9; }
	inline void set_axisValues_9(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___axisValues_9 = value;
		Il2CppCodeGenWriteBarrier((&___axisValues_9), value);
	}

	inline static int32_t get_offset_of_buttonValues_10() { return static_cast<int32_t>(offsetof(CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5, ___buttonValues_10)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_buttonValues_10() const { return ___buttonValues_10; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_buttonValues_10() { return &___buttonValues_10; }
	inline void set_buttonValues_10(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___buttonValues_10 = value;
		Il2CppCodeGenWriteBarrier((&___buttonValues_10), value);
	}

	inline static int32_t get_offset_of_joysticks_11() { return static_cast<int32_t>(offsetof(CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5, ___joysticks_11)); }
	inline TouchJoystickExampleU5BU5D_tAECDB332525B37004D94DEB18DC34FD2CDB5561E* get_joysticks_11() const { return ___joysticks_11; }
	inline TouchJoystickExampleU5BU5D_tAECDB332525B37004D94DEB18DC34FD2CDB5561E** get_address_of_joysticks_11() { return &___joysticks_11; }
	inline void set_joysticks_11(TouchJoystickExampleU5BU5D_tAECDB332525B37004D94DEB18DC34FD2CDB5561E* value)
	{
		___joysticks_11 = value;
		Il2CppCodeGenWriteBarrier((&___joysticks_11), value);
	}

	inline static int32_t get_offset_of_buttons_12() { return static_cast<int32_t>(offsetof(CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5, ___buttons_12)); }
	inline TouchButtonExampleU5BU5D_t95076935AE312ABDCCA9F593C220A33C5BED51C2* get_buttons_12() const { return ___buttons_12; }
	inline TouchButtonExampleU5BU5D_t95076935AE312ABDCCA9F593C220A33C5BED51C2** get_address_of_buttons_12() { return &___buttons_12; }
	inline void set_buttons_12(TouchButtonExampleU5BU5D_t95076935AE312ABDCCA9F593C220A33C5BED51C2* value)
	{
		___buttons_12 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_12), value);
	}

	inline static int32_t get_offset_of_controller_13() { return static_cast<int32_t>(offsetof(CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5, ___controller_13)); }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * get_controller_13() const { return ___controller_13; }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A ** get_address_of_controller_13() { return &___controller_13; }
	inline void set_controller_13(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * value)
	{
		___controller_13 = value;
		Il2CppCodeGenWriteBarrier((&___controller_13), value);
	}

	inline static int32_t get_offset_of_initialized_14() { return static_cast<int32_t>(offsetof(CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5, ___initialized_14)); }
	inline bool get_initialized_14() const { return ___initialized_14; }
	inline bool* get_address_of_initialized_14() { return &___initialized_14; }
	inline void set_initialized_14(bool value)
	{
		___initialized_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLERDEMO_T960BD05C542B8580E69F73C92EB18970E15444F5_H
#ifndef CUSTOMCONTROLLERDEMO_PLAYER_T775C638BACB048093AF2D13D03252556497C7499_H
#define CUSTOMCONTROLLERDEMO_PLAYER_T775C638BACB048093AF2D13D03252556497C7499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.CustomControllerDemo_Player
struct  CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Rewired.Demos.CustomControllerDemo_Player::playerId
	int32_t ___playerId_4;
	// System.Single Rewired.Demos.CustomControllerDemo_Player::speed
	float ___speed_5;
	// System.Single Rewired.Demos.CustomControllerDemo_Player::bulletSpeed
	float ___bulletSpeed_6;
	// UnityEngine.GameObject Rewired.Demos.CustomControllerDemo_Player::bulletPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bulletPrefab_7;
	// Rewired.Player Rewired.Demos.CustomControllerDemo_Player::_player
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ____player_8;
	// UnityEngine.CharacterController Rewired.Demos.CustomControllerDemo_Player::cc
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___cc_9;

public:
	inline static int32_t get_offset_of_playerId_4() { return static_cast<int32_t>(offsetof(CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499, ___playerId_4)); }
	inline int32_t get_playerId_4() const { return ___playerId_4; }
	inline int32_t* get_address_of_playerId_4() { return &___playerId_4; }
	inline void set_playerId_4(int32_t value)
	{
		___playerId_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_bulletSpeed_6() { return static_cast<int32_t>(offsetof(CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499, ___bulletSpeed_6)); }
	inline float get_bulletSpeed_6() const { return ___bulletSpeed_6; }
	inline float* get_address_of_bulletSpeed_6() { return &___bulletSpeed_6; }
	inline void set_bulletSpeed_6(float value)
	{
		___bulletSpeed_6 = value;
	}

	inline static int32_t get_offset_of_bulletPrefab_7() { return static_cast<int32_t>(offsetof(CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499, ___bulletPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bulletPrefab_7() const { return ___bulletPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bulletPrefab_7() { return &___bulletPrefab_7; }
	inline void set_bulletPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bulletPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___bulletPrefab_7), value);
	}

	inline static int32_t get_offset_of__player_8() { return static_cast<int32_t>(offsetof(CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499, ____player_8)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get__player_8() const { return ____player_8; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of__player_8() { return &____player_8; }
	inline void set__player_8(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		____player_8 = value;
		Il2CppCodeGenWriteBarrier((&____player_8), value);
	}

	inline static int32_t get_offset_of_cc_9() { return static_cast<int32_t>(offsetof(CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499, ___cc_9)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_cc_9() const { return ___cc_9; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_cc_9() { return &___cc_9; }
	inline void set_cc_9(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___cc_9 = value;
		Il2CppCodeGenWriteBarrier((&___cc_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLERDEMO_PLAYER_T775C638BACB048093AF2D13D03252556497C7499_H
#ifndef CUSTOMCONTROLLERSTILTDEMO_T47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2_H
#define CUSTOMCONTROLLERSTILTDEMO_T47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.CustomControllersTiltDemo
struct  CustomControllersTiltDemo_t47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform Rewired.Demos.CustomControllersTiltDemo::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_4;
	// System.Single Rewired.Demos.CustomControllersTiltDemo::speed
	float ___speed_5;
	// Rewired.CustomController Rewired.Demos.CustomControllersTiltDemo::controller
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * ___controller_6;
	// Rewired.Player Rewired.Demos.CustomControllersTiltDemo::player
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___player_7;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(CustomControllersTiltDemo_t47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2, ___target_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_4() const { return ___target_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(CustomControllersTiltDemo_t47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_controller_6() { return static_cast<int32_t>(offsetof(CustomControllersTiltDemo_t47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2, ___controller_6)); }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * get_controller_6() const { return ___controller_6; }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A ** get_address_of_controller_6() { return &___controller_6; }
	inline void set_controller_6(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * value)
	{
		___controller_6 = value;
		Il2CppCodeGenWriteBarrier((&___controller_6), value);
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(CustomControllersTiltDemo_t47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2, ___player_7)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_player_7() const { return ___player_7; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier((&___player_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTROLLERSTILTDEMO_T47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2_H
#ifndef DUALSHOCK4SPECIALFEATURESEXAMPLE_T52837908EE3CFBF853334EC27E43E5486DA253BA_H
#define DUALSHOCK4SPECIALFEATURESEXAMPLE_T52837908EE3CFBF853334EC27E43E5486DA253BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.DualShock4SpecialFeaturesExample
struct  DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Rewired.Demos.DualShock4SpecialFeaturesExample::playerId
	int32_t ___playerId_5;
	// UnityEngine.Transform Rewired.Demos.DualShock4SpecialFeaturesExample::touchpadTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___touchpadTransform_6;
	// UnityEngine.GameObject Rewired.Demos.DualShock4SpecialFeaturesExample::lightObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lightObject_7;
	// UnityEngine.Transform Rewired.Demos.DualShock4SpecialFeaturesExample::accelerometerTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___accelerometerTransform_8;
	// System.Collections.Generic.List`1<Rewired.Demos.DualShock4SpecialFeaturesExample_Touch> Rewired.Demos.DualShock4SpecialFeaturesExample::touches
	List_1_t0D8D6E6E704B54D6FA758AC7D04275A1AABFF578 * ___touches_9;
	// System.Collections.Generic.Queue`1<Rewired.Demos.DualShock4SpecialFeaturesExample_Touch> Rewired.Demos.DualShock4SpecialFeaturesExample::unusedTouches
	Queue_1_t0C455C90B9FAF2877555879F1BD5C0F3F2E4C9C0 * ___unusedTouches_10;
	// System.Boolean Rewired.Demos.DualShock4SpecialFeaturesExample::isFlashing
	bool ___isFlashing_11;
	// UnityEngine.GUIStyle Rewired.Demos.DualShock4SpecialFeaturesExample::textStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___textStyle_12;

public:
	inline static int32_t get_offset_of_playerId_5() { return static_cast<int32_t>(offsetof(DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA, ___playerId_5)); }
	inline int32_t get_playerId_5() const { return ___playerId_5; }
	inline int32_t* get_address_of_playerId_5() { return &___playerId_5; }
	inline void set_playerId_5(int32_t value)
	{
		___playerId_5 = value;
	}

	inline static int32_t get_offset_of_touchpadTransform_6() { return static_cast<int32_t>(offsetof(DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA, ___touchpadTransform_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_touchpadTransform_6() const { return ___touchpadTransform_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_touchpadTransform_6() { return &___touchpadTransform_6; }
	inline void set_touchpadTransform_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___touchpadTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___touchpadTransform_6), value);
	}

	inline static int32_t get_offset_of_lightObject_7() { return static_cast<int32_t>(offsetof(DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA, ___lightObject_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_lightObject_7() const { return ___lightObject_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_lightObject_7() { return &___lightObject_7; }
	inline void set_lightObject_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___lightObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___lightObject_7), value);
	}

	inline static int32_t get_offset_of_accelerometerTransform_8() { return static_cast<int32_t>(offsetof(DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA, ___accelerometerTransform_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_accelerometerTransform_8() const { return ___accelerometerTransform_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_accelerometerTransform_8() { return &___accelerometerTransform_8; }
	inline void set_accelerometerTransform_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___accelerometerTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___accelerometerTransform_8), value);
	}

	inline static int32_t get_offset_of_touches_9() { return static_cast<int32_t>(offsetof(DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA, ___touches_9)); }
	inline List_1_t0D8D6E6E704B54D6FA758AC7D04275A1AABFF578 * get_touches_9() const { return ___touches_9; }
	inline List_1_t0D8D6E6E704B54D6FA758AC7D04275A1AABFF578 ** get_address_of_touches_9() { return &___touches_9; }
	inline void set_touches_9(List_1_t0D8D6E6E704B54D6FA758AC7D04275A1AABFF578 * value)
	{
		___touches_9 = value;
		Il2CppCodeGenWriteBarrier((&___touches_9), value);
	}

	inline static int32_t get_offset_of_unusedTouches_10() { return static_cast<int32_t>(offsetof(DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA, ___unusedTouches_10)); }
	inline Queue_1_t0C455C90B9FAF2877555879F1BD5C0F3F2E4C9C0 * get_unusedTouches_10() const { return ___unusedTouches_10; }
	inline Queue_1_t0C455C90B9FAF2877555879F1BD5C0F3F2E4C9C0 ** get_address_of_unusedTouches_10() { return &___unusedTouches_10; }
	inline void set_unusedTouches_10(Queue_1_t0C455C90B9FAF2877555879F1BD5C0F3F2E4C9C0 * value)
	{
		___unusedTouches_10 = value;
		Il2CppCodeGenWriteBarrier((&___unusedTouches_10), value);
	}

	inline static int32_t get_offset_of_isFlashing_11() { return static_cast<int32_t>(offsetof(DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA, ___isFlashing_11)); }
	inline bool get_isFlashing_11() const { return ___isFlashing_11; }
	inline bool* get_address_of_isFlashing_11() { return &___isFlashing_11; }
	inline void set_isFlashing_11(bool value)
	{
		___isFlashing_11 = value;
	}

	inline static int32_t get_offset_of_textStyle_12() { return static_cast<int32_t>(offsetof(DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA, ___textStyle_12)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_textStyle_12() const { return ___textStyle_12; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_textStyle_12() { return &___textStyle_12; }
	inline void set_textStyle_12(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___textStyle_12 = value;
		Il2CppCodeGenWriteBarrier((&___textStyle_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUALSHOCK4SPECIALFEATURESEXAMPLE_T52837908EE3CFBF853334EC27E43E5486DA253BA_H
#ifndef EIGHTPLAYERSEXAMPLE_PLAYER_TC56813DDB9E28E6B22A606781620A16544ADF598_H
#define EIGHTPLAYERSEXAMPLE_PLAYER_TC56813DDB9E28E6B22A606781620A16544ADF598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.EightPlayersExample_Player
struct  EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Rewired.Demos.EightPlayersExample_Player::playerId
	int32_t ___playerId_4;
	// System.Single Rewired.Demos.EightPlayersExample_Player::moveSpeed
	float ___moveSpeed_5;
	// System.Single Rewired.Demos.EightPlayersExample_Player::bulletSpeed
	float ___bulletSpeed_6;
	// UnityEngine.GameObject Rewired.Demos.EightPlayersExample_Player::bulletPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bulletPrefab_7;
	// Rewired.Player Rewired.Demos.EightPlayersExample_Player::player
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___player_8;
	// UnityEngine.CharacterController Rewired.Demos.EightPlayersExample_Player::cc
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___cc_9;
	// UnityEngine.Vector3 Rewired.Demos.EightPlayersExample_Player::moveVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___moveVector_10;
	// System.Boolean Rewired.Demos.EightPlayersExample_Player::fire
	bool ___fire_11;
	// System.Boolean Rewired.Demos.EightPlayersExample_Player::initialized
	bool ___initialized_12;

public:
	inline static int32_t get_offset_of_playerId_4() { return static_cast<int32_t>(offsetof(EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598, ___playerId_4)); }
	inline int32_t get_playerId_4() const { return ___playerId_4; }
	inline int32_t* get_address_of_playerId_4() { return &___playerId_4; }
	inline void set_playerId_4(int32_t value)
	{
		___playerId_4 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_5() { return static_cast<int32_t>(offsetof(EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598, ___moveSpeed_5)); }
	inline float get_moveSpeed_5() const { return ___moveSpeed_5; }
	inline float* get_address_of_moveSpeed_5() { return &___moveSpeed_5; }
	inline void set_moveSpeed_5(float value)
	{
		___moveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_bulletSpeed_6() { return static_cast<int32_t>(offsetof(EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598, ___bulletSpeed_6)); }
	inline float get_bulletSpeed_6() const { return ___bulletSpeed_6; }
	inline float* get_address_of_bulletSpeed_6() { return &___bulletSpeed_6; }
	inline void set_bulletSpeed_6(float value)
	{
		___bulletSpeed_6 = value;
	}

	inline static int32_t get_offset_of_bulletPrefab_7() { return static_cast<int32_t>(offsetof(EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598, ___bulletPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bulletPrefab_7() const { return ___bulletPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bulletPrefab_7() { return &___bulletPrefab_7; }
	inline void set_bulletPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bulletPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___bulletPrefab_7), value);
	}

	inline static int32_t get_offset_of_player_8() { return static_cast<int32_t>(offsetof(EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598, ___player_8)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_player_8() const { return ___player_8; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_player_8() { return &___player_8; }
	inline void set_player_8(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___player_8 = value;
		Il2CppCodeGenWriteBarrier((&___player_8), value);
	}

	inline static int32_t get_offset_of_cc_9() { return static_cast<int32_t>(offsetof(EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598, ___cc_9)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_cc_9() const { return ___cc_9; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_cc_9() { return &___cc_9; }
	inline void set_cc_9(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___cc_9 = value;
		Il2CppCodeGenWriteBarrier((&___cc_9), value);
	}

	inline static int32_t get_offset_of_moveVector_10() { return static_cast<int32_t>(offsetof(EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598, ___moveVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_moveVector_10() const { return ___moveVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_moveVector_10() { return &___moveVector_10; }
	inline void set_moveVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___moveVector_10 = value;
	}

	inline static int32_t get_offset_of_fire_11() { return static_cast<int32_t>(offsetof(EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598, ___fire_11)); }
	inline bool get_fire_11() const { return ___fire_11; }
	inline bool* get_address_of_fire_11() { return &___fire_11; }
	inline void set_fire_11(bool value)
	{
		___fire_11 = value;
	}

	inline static int32_t get_offset_of_initialized_12() { return static_cast<int32_t>(offsetof(EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598, ___initialized_12)); }
	inline bool get_initialized_12() const { return ___initialized_12; }
	inline bool* get_address_of_initialized_12() { return &___initialized_12; }
	inline void set_initialized_12(bool value)
	{
		___initialized_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EIGHTPLAYERSEXAMPLE_PLAYER_TC56813DDB9E28E6B22A606781620A16544ADF598_H
#ifndef FALLBACKJOYSTICKIDENTIFICATIONDEMO_TFED66528A72925296ED5FEDAB90A9B04D45E66B9_H
#define FALLBACKJOYSTICKIDENTIFICATIONDEMO_TFED66528A72925296ED5FEDAB90A9B04D45E66B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.FallbackJoystickIdentificationDemo
struct  FallbackJoystickIdentificationDemo_tFED66528A72925296ED5FEDAB90A9B04D45E66B9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Rewired.Demos.FallbackJoystickIdentificationDemo::identifyRequired
	bool ___identifyRequired_7;
	// System.Collections.Generic.Queue`1<Rewired.Joystick> Rewired.Demos.FallbackJoystickIdentificationDemo::joysticksToIdentify
	Queue_1_t4843A3235E672F8624CEBA0AD4E460BD0183A6C5 * ___joysticksToIdentify_8;
	// System.Single Rewired.Demos.FallbackJoystickIdentificationDemo::nextInputAllowedTime
	float ___nextInputAllowedTime_9;
	// UnityEngine.GUIStyle Rewired.Demos.FallbackJoystickIdentificationDemo::style
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___style_10;

public:
	inline static int32_t get_offset_of_identifyRequired_7() { return static_cast<int32_t>(offsetof(FallbackJoystickIdentificationDemo_tFED66528A72925296ED5FEDAB90A9B04D45E66B9, ___identifyRequired_7)); }
	inline bool get_identifyRequired_7() const { return ___identifyRequired_7; }
	inline bool* get_address_of_identifyRequired_7() { return &___identifyRequired_7; }
	inline void set_identifyRequired_7(bool value)
	{
		___identifyRequired_7 = value;
	}

	inline static int32_t get_offset_of_joysticksToIdentify_8() { return static_cast<int32_t>(offsetof(FallbackJoystickIdentificationDemo_tFED66528A72925296ED5FEDAB90A9B04D45E66B9, ___joysticksToIdentify_8)); }
	inline Queue_1_t4843A3235E672F8624CEBA0AD4E460BD0183A6C5 * get_joysticksToIdentify_8() const { return ___joysticksToIdentify_8; }
	inline Queue_1_t4843A3235E672F8624CEBA0AD4E460BD0183A6C5 ** get_address_of_joysticksToIdentify_8() { return &___joysticksToIdentify_8; }
	inline void set_joysticksToIdentify_8(Queue_1_t4843A3235E672F8624CEBA0AD4E460BD0183A6C5 * value)
	{
		___joysticksToIdentify_8 = value;
		Il2CppCodeGenWriteBarrier((&___joysticksToIdentify_8), value);
	}

	inline static int32_t get_offset_of_nextInputAllowedTime_9() { return static_cast<int32_t>(offsetof(FallbackJoystickIdentificationDemo_tFED66528A72925296ED5FEDAB90A9B04D45E66B9, ___nextInputAllowedTime_9)); }
	inline float get_nextInputAllowedTime_9() const { return ___nextInputAllowedTime_9; }
	inline float* get_address_of_nextInputAllowedTime_9() { return &___nextInputAllowedTime_9; }
	inline void set_nextInputAllowedTime_9(float value)
	{
		___nextInputAllowedTime_9 = value;
	}

	inline static int32_t get_offset_of_style_10() { return static_cast<int32_t>(offsetof(FallbackJoystickIdentificationDemo_tFED66528A72925296ED5FEDAB90A9B04D45E66B9, ___style_10)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_style_10() const { return ___style_10; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_style_10() { return &___style_10; }
	inline void set_style_10(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___style_10 = value;
		Il2CppCodeGenWriteBarrier((&___style_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FALLBACKJOYSTICKIDENTIFICATIONDEMO_TFED66528A72925296ED5FEDAB90A9B04D45E66B9_H
#ifndef CONTROLLERUIEFFECT_T6D031A2496C863217415265EC5E20CA169702646_H
#define CONTROLLERUIEFFECT_T6D031A2496C863217415265EC5E20CA169702646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.GamepadTemplateUI.ControllerUIEffect
struct  ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Color Rewired.Demos.GamepadTemplateUI.ControllerUIEffect::_highlightColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____highlightColor_4;
	// UnityEngine.UI.Image Rewired.Demos.GamepadTemplateUI.ControllerUIEffect::_image
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____image_5;
	// UnityEngine.Color Rewired.Demos.GamepadTemplateUI.ControllerUIEffect::_color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____color_6;
	// UnityEngine.Color Rewired.Demos.GamepadTemplateUI.ControllerUIEffect::_origColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____origColor_7;
	// System.Boolean Rewired.Demos.GamepadTemplateUI.ControllerUIEffect::_isActive
	bool ____isActive_8;
	// System.Single Rewired.Demos.GamepadTemplateUI.ControllerUIEffect::_highlightAmount
	float ____highlightAmount_9;

public:
	inline static int32_t get_offset_of__highlightColor_4() { return static_cast<int32_t>(offsetof(ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646, ____highlightColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__highlightColor_4() const { return ____highlightColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__highlightColor_4() { return &____highlightColor_4; }
	inline void set__highlightColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____highlightColor_4 = value;
	}

	inline static int32_t get_offset_of__image_5() { return static_cast<int32_t>(offsetof(ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646, ____image_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__image_5() const { return ____image_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__image_5() { return &____image_5; }
	inline void set__image_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____image_5 = value;
		Il2CppCodeGenWriteBarrier((&____image_5), value);
	}

	inline static int32_t get_offset_of__color_6() { return static_cast<int32_t>(offsetof(ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646, ____color_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__color_6() const { return ____color_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__color_6() { return &____color_6; }
	inline void set__color_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____color_6 = value;
	}

	inline static int32_t get_offset_of__origColor_7() { return static_cast<int32_t>(offsetof(ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646, ____origColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__origColor_7() const { return ____origColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__origColor_7() { return &____origColor_7; }
	inline void set__origColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____origColor_7 = value;
	}

	inline static int32_t get_offset_of__isActive_8() { return static_cast<int32_t>(offsetof(ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646, ____isActive_8)); }
	inline bool get__isActive_8() const { return ____isActive_8; }
	inline bool* get_address_of__isActive_8() { return &____isActive_8; }
	inline void set__isActive_8(bool value)
	{
		____isActive_8 = value;
	}

	inline static int32_t get_offset_of__highlightAmount_9() { return static_cast<int32_t>(offsetof(ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646, ____highlightAmount_9)); }
	inline float get__highlightAmount_9() const { return ____highlightAmount_9; }
	inline float* get_address_of__highlightAmount_9() { return &____highlightAmount_9; }
	inline void set__highlightAmount_9(float value)
	{
		____highlightAmount_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERUIEFFECT_T6D031A2496C863217415265EC5E20CA169702646_H
#ifndef CONTROLLERUIELEMENT_TDBD0FBE9590A07647991556D8F5415587954CDAF_H
#define CONTROLLERUIELEMENT_TDBD0FBE9590A07647991556D8F5415587954CDAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.GamepadTemplateUI.ControllerUIElement
struct  ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Color Rewired.Demos.GamepadTemplateUI.ControllerUIElement::_highlightColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____highlightColor_4;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIEffect Rewired.Demos.GamepadTemplateUI.ControllerUIElement::_positiveUIEffect
	ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646 * ____positiveUIEffect_5;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIEffect Rewired.Demos.GamepadTemplateUI.ControllerUIElement::_negativeUIEffect
	ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646 * ____negativeUIEffect_6;
	// UnityEngine.UI.Text Rewired.Demos.GamepadTemplateUI.ControllerUIElement::_label
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____label_7;
	// UnityEngine.UI.Text Rewired.Demos.GamepadTemplateUI.ControllerUIElement::_positiveLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____positiveLabel_8;
	// UnityEngine.UI.Text Rewired.Demos.GamepadTemplateUI.ControllerUIElement::_negativeLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____negativeLabel_9;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement[] Rewired.Demos.GamepadTemplateUI.ControllerUIElement::_childElements
	ControllerUIElementU5BU5D_t376F8D72795D1B61D41CD2CEC0474C8C986AFEC1* ____childElements_10;
	// UnityEngine.UI.Image Rewired.Demos.GamepadTemplateUI.ControllerUIElement::_image
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____image_11;
	// UnityEngine.Color Rewired.Demos.GamepadTemplateUI.ControllerUIElement::_color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____color_12;
	// UnityEngine.Color Rewired.Demos.GamepadTemplateUI.ControllerUIElement::_origColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____origColor_13;
	// System.Boolean Rewired.Demos.GamepadTemplateUI.ControllerUIElement::_isActive
	bool ____isActive_14;
	// System.Single Rewired.Demos.GamepadTemplateUI.ControllerUIElement::_highlightAmount
	float ____highlightAmount_15;

public:
	inline static int32_t get_offset_of__highlightColor_4() { return static_cast<int32_t>(offsetof(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF, ____highlightColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__highlightColor_4() const { return ____highlightColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__highlightColor_4() { return &____highlightColor_4; }
	inline void set__highlightColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____highlightColor_4 = value;
	}

	inline static int32_t get_offset_of__positiveUIEffect_5() { return static_cast<int32_t>(offsetof(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF, ____positiveUIEffect_5)); }
	inline ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646 * get__positiveUIEffect_5() const { return ____positiveUIEffect_5; }
	inline ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646 ** get_address_of__positiveUIEffect_5() { return &____positiveUIEffect_5; }
	inline void set__positiveUIEffect_5(ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646 * value)
	{
		____positiveUIEffect_5 = value;
		Il2CppCodeGenWriteBarrier((&____positiveUIEffect_5), value);
	}

	inline static int32_t get_offset_of__negativeUIEffect_6() { return static_cast<int32_t>(offsetof(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF, ____negativeUIEffect_6)); }
	inline ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646 * get__negativeUIEffect_6() const { return ____negativeUIEffect_6; }
	inline ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646 ** get_address_of__negativeUIEffect_6() { return &____negativeUIEffect_6; }
	inline void set__negativeUIEffect_6(ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646 * value)
	{
		____negativeUIEffect_6 = value;
		Il2CppCodeGenWriteBarrier((&____negativeUIEffect_6), value);
	}

	inline static int32_t get_offset_of__label_7() { return static_cast<int32_t>(offsetof(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF, ____label_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__label_7() const { return ____label_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__label_7() { return &____label_7; }
	inline void set__label_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____label_7 = value;
		Il2CppCodeGenWriteBarrier((&____label_7), value);
	}

	inline static int32_t get_offset_of__positiveLabel_8() { return static_cast<int32_t>(offsetof(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF, ____positiveLabel_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__positiveLabel_8() const { return ____positiveLabel_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__positiveLabel_8() { return &____positiveLabel_8; }
	inline void set__positiveLabel_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____positiveLabel_8 = value;
		Il2CppCodeGenWriteBarrier((&____positiveLabel_8), value);
	}

	inline static int32_t get_offset_of__negativeLabel_9() { return static_cast<int32_t>(offsetof(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF, ____negativeLabel_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__negativeLabel_9() const { return ____negativeLabel_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__negativeLabel_9() { return &____negativeLabel_9; }
	inline void set__negativeLabel_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____negativeLabel_9 = value;
		Il2CppCodeGenWriteBarrier((&____negativeLabel_9), value);
	}

	inline static int32_t get_offset_of__childElements_10() { return static_cast<int32_t>(offsetof(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF, ____childElements_10)); }
	inline ControllerUIElementU5BU5D_t376F8D72795D1B61D41CD2CEC0474C8C986AFEC1* get__childElements_10() const { return ____childElements_10; }
	inline ControllerUIElementU5BU5D_t376F8D72795D1B61D41CD2CEC0474C8C986AFEC1** get_address_of__childElements_10() { return &____childElements_10; }
	inline void set__childElements_10(ControllerUIElementU5BU5D_t376F8D72795D1B61D41CD2CEC0474C8C986AFEC1* value)
	{
		____childElements_10 = value;
		Il2CppCodeGenWriteBarrier((&____childElements_10), value);
	}

	inline static int32_t get_offset_of__image_11() { return static_cast<int32_t>(offsetof(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF, ____image_11)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__image_11() const { return ____image_11; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__image_11() { return &____image_11; }
	inline void set__image_11(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____image_11 = value;
		Il2CppCodeGenWriteBarrier((&____image_11), value);
	}

	inline static int32_t get_offset_of__color_12() { return static_cast<int32_t>(offsetof(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF, ____color_12)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__color_12() const { return ____color_12; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__color_12() { return &____color_12; }
	inline void set__color_12(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____color_12 = value;
	}

	inline static int32_t get_offset_of__origColor_13() { return static_cast<int32_t>(offsetof(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF, ____origColor_13)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__origColor_13() const { return ____origColor_13; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__origColor_13() { return &____origColor_13; }
	inline void set__origColor_13(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____origColor_13 = value;
	}

	inline static int32_t get_offset_of__isActive_14() { return static_cast<int32_t>(offsetof(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF, ____isActive_14)); }
	inline bool get__isActive_14() const { return ____isActive_14; }
	inline bool* get_address_of__isActive_14() { return &____isActive_14; }
	inline void set__isActive_14(bool value)
	{
		____isActive_14 = value;
	}

	inline static int32_t get_offset_of__highlightAmount_15() { return static_cast<int32_t>(offsetof(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF, ____highlightAmount_15)); }
	inline float get__highlightAmount_15() const { return ____highlightAmount_15; }
	inline float* get_address_of__highlightAmount_15() { return &____highlightAmount_15; }
	inline void set__highlightAmount_15(float value)
	{
		____highlightAmount_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERUIELEMENT_TDBD0FBE9590A07647991556D8F5415587954CDAF_H
#ifndef GAMEPADTEMPLATEUI_T12D192A7CBAFEA2A9B157D4BD652A6A104047E9A_H
#define GAMEPADTEMPLATEUI_T12D192A7CBAFEA2A9B157D4BD652A6A104047E9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI
struct  GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::playerId
	int32_t ___playerId_5;
	// UnityEngine.RectTransform Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::leftStick
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___leftStick_6;
	// UnityEngine.RectTransform Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::rightStick
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rightStick_7;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::leftStickX
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___leftStickX_8;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::leftStickY
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___leftStickY_9;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::leftStickButton
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___leftStickButton_10;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::rightStickX
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___rightStickX_11;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::rightStickY
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___rightStickY_12;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::rightStickButton
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___rightStickButton_13;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::actionBottomRow1
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___actionBottomRow1_14;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::actionBottomRow2
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___actionBottomRow2_15;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::actionBottomRow3
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___actionBottomRow3_16;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::actionTopRow1
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___actionTopRow1_17;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::actionTopRow2
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___actionTopRow2_18;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::actionTopRow3
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___actionTopRow3_19;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::leftShoulder
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___leftShoulder_20;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::leftTrigger
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___leftTrigger_21;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::rightShoulder
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___rightShoulder_22;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::rightTrigger
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___rightTrigger_23;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::center1
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___center1_24;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::center2
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___center2_25;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::center3
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___center3_26;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::dPadUp
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___dPadUp_27;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::dPadRight
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___dPadRight_28;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::dPadDown
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___dPadDown_29;
	// Rewired.Demos.GamepadTemplateUI.ControllerUIElement Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::dPadLeft
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * ___dPadLeft_30;
	// Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI_UIElement[] Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::_uiElementsArray
	UIElementU5BU5D_tDE25211E4110FAB7BC5D5E015E8847D8C03E4217* ____uiElementsArray_31;
	// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Demos.GamepadTemplateUI.ControllerUIElement> Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::_uiElements
	Dictionary_2_tF131C1F2A1D51A5DD801312B18E23DAB27F1C838 * ____uiElements_32;
	// System.Collections.Generic.IList`1<Rewired.ControllerTemplateElementTarget> Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::_tempTargetList
	RuntimeObject* ____tempTargetList_33;
	// Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI_Stick[] Rewired.Demos.GamepadTemplateUI.GamepadTemplateUI::_sticks
	StickU5BU5D_t53B3B6F4BB1EC7672F71353B94E6AC4E227536C6* ____sticks_34;

public:
	inline static int32_t get_offset_of_playerId_5() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___playerId_5)); }
	inline int32_t get_playerId_5() const { return ___playerId_5; }
	inline int32_t* get_address_of_playerId_5() { return &___playerId_5; }
	inline void set_playerId_5(int32_t value)
	{
		___playerId_5 = value;
	}

	inline static int32_t get_offset_of_leftStick_6() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___leftStick_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_leftStick_6() const { return ___leftStick_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_leftStick_6() { return &___leftStick_6; }
	inline void set_leftStick_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___leftStick_6 = value;
		Il2CppCodeGenWriteBarrier((&___leftStick_6), value);
	}

	inline static int32_t get_offset_of_rightStick_7() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___rightStick_7)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rightStick_7() const { return ___rightStick_7; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rightStick_7() { return &___rightStick_7; }
	inline void set_rightStick_7(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rightStick_7 = value;
		Il2CppCodeGenWriteBarrier((&___rightStick_7), value);
	}

	inline static int32_t get_offset_of_leftStickX_8() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___leftStickX_8)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_leftStickX_8() const { return ___leftStickX_8; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_leftStickX_8() { return &___leftStickX_8; }
	inline void set_leftStickX_8(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___leftStickX_8 = value;
		Il2CppCodeGenWriteBarrier((&___leftStickX_8), value);
	}

	inline static int32_t get_offset_of_leftStickY_9() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___leftStickY_9)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_leftStickY_9() const { return ___leftStickY_9; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_leftStickY_9() { return &___leftStickY_9; }
	inline void set_leftStickY_9(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___leftStickY_9 = value;
		Il2CppCodeGenWriteBarrier((&___leftStickY_9), value);
	}

	inline static int32_t get_offset_of_leftStickButton_10() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___leftStickButton_10)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_leftStickButton_10() const { return ___leftStickButton_10; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_leftStickButton_10() { return &___leftStickButton_10; }
	inline void set_leftStickButton_10(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___leftStickButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___leftStickButton_10), value);
	}

	inline static int32_t get_offset_of_rightStickX_11() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___rightStickX_11)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_rightStickX_11() const { return ___rightStickX_11; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_rightStickX_11() { return &___rightStickX_11; }
	inline void set_rightStickX_11(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___rightStickX_11 = value;
		Il2CppCodeGenWriteBarrier((&___rightStickX_11), value);
	}

	inline static int32_t get_offset_of_rightStickY_12() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___rightStickY_12)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_rightStickY_12() const { return ___rightStickY_12; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_rightStickY_12() { return &___rightStickY_12; }
	inline void set_rightStickY_12(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___rightStickY_12 = value;
		Il2CppCodeGenWriteBarrier((&___rightStickY_12), value);
	}

	inline static int32_t get_offset_of_rightStickButton_13() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___rightStickButton_13)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_rightStickButton_13() const { return ___rightStickButton_13; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_rightStickButton_13() { return &___rightStickButton_13; }
	inline void set_rightStickButton_13(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___rightStickButton_13 = value;
		Il2CppCodeGenWriteBarrier((&___rightStickButton_13), value);
	}

	inline static int32_t get_offset_of_actionBottomRow1_14() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___actionBottomRow1_14)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_actionBottomRow1_14() const { return ___actionBottomRow1_14; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_actionBottomRow1_14() { return &___actionBottomRow1_14; }
	inline void set_actionBottomRow1_14(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___actionBottomRow1_14 = value;
		Il2CppCodeGenWriteBarrier((&___actionBottomRow1_14), value);
	}

	inline static int32_t get_offset_of_actionBottomRow2_15() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___actionBottomRow2_15)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_actionBottomRow2_15() const { return ___actionBottomRow2_15; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_actionBottomRow2_15() { return &___actionBottomRow2_15; }
	inline void set_actionBottomRow2_15(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___actionBottomRow2_15 = value;
		Il2CppCodeGenWriteBarrier((&___actionBottomRow2_15), value);
	}

	inline static int32_t get_offset_of_actionBottomRow3_16() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___actionBottomRow3_16)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_actionBottomRow3_16() const { return ___actionBottomRow3_16; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_actionBottomRow3_16() { return &___actionBottomRow3_16; }
	inline void set_actionBottomRow3_16(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___actionBottomRow3_16 = value;
		Il2CppCodeGenWriteBarrier((&___actionBottomRow3_16), value);
	}

	inline static int32_t get_offset_of_actionTopRow1_17() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___actionTopRow1_17)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_actionTopRow1_17() const { return ___actionTopRow1_17; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_actionTopRow1_17() { return &___actionTopRow1_17; }
	inline void set_actionTopRow1_17(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___actionTopRow1_17 = value;
		Il2CppCodeGenWriteBarrier((&___actionTopRow1_17), value);
	}

	inline static int32_t get_offset_of_actionTopRow2_18() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___actionTopRow2_18)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_actionTopRow2_18() const { return ___actionTopRow2_18; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_actionTopRow2_18() { return &___actionTopRow2_18; }
	inline void set_actionTopRow2_18(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___actionTopRow2_18 = value;
		Il2CppCodeGenWriteBarrier((&___actionTopRow2_18), value);
	}

	inline static int32_t get_offset_of_actionTopRow3_19() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___actionTopRow3_19)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_actionTopRow3_19() const { return ___actionTopRow3_19; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_actionTopRow3_19() { return &___actionTopRow3_19; }
	inline void set_actionTopRow3_19(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___actionTopRow3_19 = value;
		Il2CppCodeGenWriteBarrier((&___actionTopRow3_19), value);
	}

	inline static int32_t get_offset_of_leftShoulder_20() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___leftShoulder_20)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_leftShoulder_20() const { return ___leftShoulder_20; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_leftShoulder_20() { return &___leftShoulder_20; }
	inline void set_leftShoulder_20(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___leftShoulder_20 = value;
		Il2CppCodeGenWriteBarrier((&___leftShoulder_20), value);
	}

	inline static int32_t get_offset_of_leftTrigger_21() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___leftTrigger_21)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_leftTrigger_21() const { return ___leftTrigger_21; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_leftTrigger_21() { return &___leftTrigger_21; }
	inline void set_leftTrigger_21(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___leftTrigger_21 = value;
		Il2CppCodeGenWriteBarrier((&___leftTrigger_21), value);
	}

	inline static int32_t get_offset_of_rightShoulder_22() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___rightShoulder_22)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_rightShoulder_22() const { return ___rightShoulder_22; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_rightShoulder_22() { return &___rightShoulder_22; }
	inline void set_rightShoulder_22(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___rightShoulder_22 = value;
		Il2CppCodeGenWriteBarrier((&___rightShoulder_22), value);
	}

	inline static int32_t get_offset_of_rightTrigger_23() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___rightTrigger_23)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_rightTrigger_23() const { return ___rightTrigger_23; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_rightTrigger_23() { return &___rightTrigger_23; }
	inline void set_rightTrigger_23(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___rightTrigger_23 = value;
		Il2CppCodeGenWriteBarrier((&___rightTrigger_23), value);
	}

	inline static int32_t get_offset_of_center1_24() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___center1_24)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_center1_24() const { return ___center1_24; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_center1_24() { return &___center1_24; }
	inline void set_center1_24(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___center1_24 = value;
		Il2CppCodeGenWriteBarrier((&___center1_24), value);
	}

	inline static int32_t get_offset_of_center2_25() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___center2_25)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_center2_25() const { return ___center2_25; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_center2_25() { return &___center2_25; }
	inline void set_center2_25(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___center2_25 = value;
		Il2CppCodeGenWriteBarrier((&___center2_25), value);
	}

	inline static int32_t get_offset_of_center3_26() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___center3_26)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_center3_26() const { return ___center3_26; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_center3_26() { return &___center3_26; }
	inline void set_center3_26(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___center3_26 = value;
		Il2CppCodeGenWriteBarrier((&___center3_26), value);
	}

	inline static int32_t get_offset_of_dPadUp_27() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___dPadUp_27)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_dPadUp_27() const { return ___dPadUp_27; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_dPadUp_27() { return &___dPadUp_27; }
	inline void set_dPadUp_27(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___dPadUp_27 = value;
		Il2CppCodeGenWriteBarrier((&___dPadUp_27), value);
	}

	inline static int32_t get_offset_of_dPadRight_28() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___dPadRight_28)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_dPadRight_28() const { return ___dPadRight_28; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_dPadRight_28() { return &___dPadRight_28; }
	inline void set_dPadRight_28(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___dPadRight_28 = value;
		Il2CppCodeGenWriteBarrier((&___dPadRight_28), value);
	}

	inline static int32_t get_offset_of_dPadDown_29() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___dPadDown_29)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_dPadDown_29() const { return ___dPadDown_29; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_dPadDown_29() { return &___dPadDown_29; }
	inline void set_dPadDown_29(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___dPadDown_29 = value;
		Il2CppCodeGenWriteBarrier((&___dPadDown_29), value);
	}

	inline static int32_t get_offset_of_dPadLeft_30() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ___dPadLeft_30)); }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * get_dPadLeft_30() const { return ___dPadLeft_30; }
	inline ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF ** get_address_of_dPadLeft_30() { return &___dPadLeft_30; }
	inline void set_dPadLeft_30(ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF * value)
	{
		___dPadLeft_30 = value;
		Il2CppCodeGenWriteBarrier((&___dPadLeft_30), value);
	}

	inline static int32_t get_offset_of__uiElementsArray_31() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ____uiElementsArray_31)); }
	inline UIElementU5BU5D_tDE25211E4110FAB7BC5D5E015E8847D8C03E4217* get__uiElementsArray_31() const { return ____uiElementsArray_31; }
	inline UIElementU5BU5D_tDE25211E4110FAB7BC5D5E015E8847D8C03E4217** get_address_of__uiElementsArray_31() { return &____uiElementsArray_31; }
	inline void set__uiElementsArray_31(UIElementU5BU5D_tDE25211E4110FAB7BC5D5E015E8847D8C03E4217* value)
	{
		____uiElementsArray_31 = value;
		Il2CppCodeGenWriteBarrier((&____uiElementsArray_31), value);
	}

	inline static int32_t get_offset_of__uiElements_32() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ____uiElements_32)); }
	inline Dictionary_2_tF131C1F2A1D51A5DD801312B18E23DAB27F1C838 * get__uiElements_32() const { return ____uiElements_32; }
	inline Dictionary_2_tF131C1F2A1D51A5DD801312B18E23DAB27F1C838 ** get_address_of__uiElements_32() { return &____uiElements_32; }
	inline void set__uiElements_32(Dictionary_2_tF131C1F2A1D51A5DD801312B18E23DAB27F1C838 * value)
	{
		____uiElements_32 = value;
		Il2CppCodeGenWriteBarrier((&____uiElements_32), value);
	}

	inline static int32_t get_offset_of__tempTargetList_33() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ____tempTargetList_33)); }
	inline RuntimeObject* get__tempTargetList_33() const { return ____tempTargetList_33; }
	inline RuntimeObject** get_address_of__tempTargetList_33() { return &____tempTargetList_33; }
	inline void set__tempTargetList_33(RuntimeObject* value)
	{
		____tempTargetList_33 = value;
		Il2CppCodeGenWriteBarrier((&____tempTargetList_33), value);
	}

	inline static int32_t get_offset_of__sticks_34() { return static_cast<int32_t>(offsetof(GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A, ____sticks_34)); }
	inline StickU5BU5D_t53B3B6F4BB1EC7672F71353B94E6AC4E227536C6* get__sticks_34() const { return ____sticks_34; }
	inline StickU5BU5D_t53B3B6F4BB1EC7672F71353B94E6AC4E227536C6** get_address_of__sticks_34() { return &____sticks_34; }
	inline void set__sticks_34(StickU5BU5D_t53B3B6F4BB1EC7672F71353B94E6AC4E227536C6* value)
	{
		____sticks_34 = value;
		Il2CppCodeGenWriteBarrier((&____sticks_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEPADTEMPLATEUI_T12D192A7CBAFEA2A9B157D4BD652A6A104047E9A_H
#ifndef PLAYERMOUSESPRITEEXAMPLE_TD784EBE45656902A6A61570F5694D6BF516DA718_H
#define PLAYERMOUSESPRITEEXAMPLE_TD784EBE45656902A6A61570F5694D6BF516DA718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.PlayerMouseSpriteExample
struct  PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Rewired.Demos.PlayerMouseSpriteExample::playerId
	int32_t ___playerId_4;
	// System.String Rewired.Demos.PlayerMouseSpriteExample::horizontalAction
	String_t* ___horizontalAction_5;
	// System.String Rewired.Demos.PlayerMouseSpriteExample::verticalAction
	String_t* ___verticalAction_6;
	// System.String Rewired.Demos.PlayerMouseSpriteExample::wheelAction
	String_t* ___wheelAction_7;
	// System.String Rewired.Demos.PlayerMouseSpriteExample::leftButtonAction
	String_t* ___leftButtonAction_8;
	// System.String Rewired.Demos.PlayerMouseSpriteExample::rightButtonAction
	String_t* ___rightButtonAction_9;
	// System.String Rewired.Demos.PlayerMouseSpriteExample::middleButtonAction
	String_t* ___middleButtonAction_10;
	// System.Single Rewired.Demos.PlayerMouseSpriteExample::distanceFromCamera
	float ___distanceFromCamera_11;
	// System.Single Rewired.Demos.PlayerMouseSpriteExample::spriteScale
	float ___spriteScale_12;
	// UnityEngine.GameObject Rewired.Demos.PlayerMouseSpriteExample::pointerPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pointerPrefab_13;
	// UnityEngine.GameObject Rewired.Demos.PlayerMouseSpriteExample::clickEffectPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___clickEffectPrefab_14;
	// System.Boolean Rewired.Demos.PlayerMouseSpriteExample::hideHardwarePointer
	bool ___hideHardwarePointer_15;
	// UnityEngine.GameObject Rewired.Demos.PlayerMouseSpriteExample::pointer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pointer_16;
	// Rewired.PlayerMouse Rewired.Demos.PlayerMouseSpriteExample::mouse
	PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D * ___mouse_17;

public:
	inline static int32_t get_offset_of_playerId_4() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___playerId_4)); }
	inline int32_t get_playerId_4() const { return ___playerId_4; }
	inline int32_t* get_address_of_playerId_4() { return &___playerId_4; }
	inline void set_playerId_4(int32_t value)
	{
		___playerId_4 = value;
	}

	inline static int32_t get_offset_of_horizontalAction_5() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___horizontalAction_5)); }
	inline String_t* get_horizontalAction_5() const { return ___horizontalAction_5; }
	inline String_t** get_address_of_horizontalAction_5() { return &___horizontalAction_5; }
	inline void set_horizontalAction_5(String_t* value)
	{
		___horizontalAction_5 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAction_5), value);
	}

	inline static int32_t get_offset_of_verticalAction_6() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___verticalAction_6)); }
	inline String_t* get_verticalAction_6() const { return ___verticalAction_6; }
	inline String_t** get_address_of_verticalAction_6() { return &___verticalAction_6; }
	inline void set_verticalAction_6(String_t* value)
	{
		___verticalAction_6 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAction_6), value);
	}

	inline static int32_t get_offset_of_wheelAction_7() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___wheelAction_7)); }
	inline String_t* get_wheelAction_7() const { return ___wheelAction_7; }
	inline String_t** get_address_of_wheelAction_7() { return &___wheelAction_7; }
	inline void set_wheelAction_7(String_t* value)
	{
		___wheelAction_7 = value;
		Il2CppCodeGenWriteBarrier((&___wheelAction_7), value);
	}

	inline static int32_t get_offset_of_leftButtonAction_8() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___leftButtonAction_8)); }
	inline String_t* get_leftButtonAction_8() const { return ___leftButtonAction_8; }
	inline String_t** get_address_of_leftButtonAction_8() { return &___leftButtonAction_8; }
	inline void set_leftButtonAction_8(String_t* value)
	{
		___leftButtonAction_8 = value;
		Il2CppCodeGenWriteBarrier((&___leftButtonAction_8), value);
	}

	inline static int32_t get_offset_of_rightButtonAction_9() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___rightButtonAction_9)); }
	inline String_t* get_rightButtonAction_9() const { return ___rightButtonAction_9; }
	inline String_t** get_address_of_rightButtonAction_9() { return &___rightButtonAction_9; }
	inline void set_rightButtonAction_9(String_t* value)
	{
		___rightButtonAction_9 = value;
		Il2CppCodeGenWriteBarrier((&___rightButtonAction_9), value);
	}

	inline static int32_t get_offset_of_middleButtonAction_10() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___middleButtonAction_10)); }
	inline String_t* get_middleButtonAction_10() const { return ___middleButtonAction_10; }
	inline String_t** get_address_of_middleButtonAction_10() { return &___middleButtonAction_10; }
	inline void set_middleButtonAction_10(String_t* value)
	{
		___middleButtonAction_10 = value;
		Il2CppCodeGenWriteBarrier((&___middleButtonAction_10), value);
	}

	inline static int32_t get_offset_of_distanceFromCamera_11() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___distanceFromCamera_11)); }
	inline float get_distanceFromCamera_11() const { return ___distanceFromCamera_11; }
	inline float* get_address_of_distanceFromCamera_11() { return &___distanceFromCamera_11; }
	inline void set_distanceFromCamera_11(float value)
	{
		___distanceFromCamera_11 = value;
	}

	inline static int32_t get_offset_of_spriteScale_12() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___spriteScale_12)); }
	inline float get_spriteScale_12() const { return ___spriteScale_12; }
	inline float* get_address_of_spriteScale_12() { return &___spriteScale_12; }
	inline void set_spriteScale_12(float value)
	{
		___spriteScale_12 = value;
	}

	inline static int32_t get_offset_of_pointerPrefab_13() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___pointerPrefab_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pointerPrefab_13() const { return ___pointerPrefab_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pointerPrefab_13() { return &___pointerPrefab_13; }
	inline void set_pointerPrefab_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pointerPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((&___pointerPrefab_13), value);
	}

	inline static int32_t get_offset_of_clickEffectPrefab_14() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___clickEffectPrefab_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_clickEffectPrefab_14() const { return ___clickEffectPrefab_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_clickEffectPrefab_14() { return &___clickEffectPrefab_14; }
	inline void set_clickEffectPrefab_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___clickEffectPrefab_14 = value;
		Il2CppCodeGenWriteBarrier((&___clickEffectPrefab_14), value);
	}

	inline static int32_t get_offset_of_hideHardwarePointer_15() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___hideHardwarePointer_15)); }
	inline bool get_hideHardwarePointer_15() const { return ___hideHardwarePointer_15; }
	inline bool* get_address_of_hideHardwarePointer_15() { return &___hideHardwarePointer_15; }
	inline void set_hideHardwarePointer_15(bool value)
	{
		___hideHardwarePointer_15 = value;
	}

	inline static int32_t get_offset_of_pointer_16() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___pointer_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pointer_16() const { return ___pointer_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pointer_16() { return &___pointer_16; }
	inline void set_pointer_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pointer_16 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_16), value);
	}

	inline static int32_t get_offset_of_mouse_17() { return static_cast<int32_t>(offsetof(PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718, ___mouse_17)); }
	inline PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D * get_mouse_17() const { return ___mouse_17; }
	inline PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D ** get_address_of_mouse_17() { return &___mouse_17; }
	inline void set_mouse_17(PlayerMouse_tFE3F2427C1D7E8AE42A497332E5D40F903252B3D * value)
	{
		___mouse_17 = value;
		Il2CppCodeGenWriteBarrier((&___mouse_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMOUSESPRITEEXAMPLE_TD784EBE45656902A6A61570F5694D6BF516DA718_H
#ifndef PLAYERPOINTEREVENTHANDLEREXAMPLE_TB30A902E62F9AA0746875C484FA7F29652E66F18_H
#define PLAYERPOINTEREVENTHANDLEREXAMPLE_TB30A902E62F9AA0746875C484FA7F29652E66F18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.PlayerPointerEventHandlerExample
struct  PlayerPointerEventHandlerExample_tB30A902E62F9AA0746875C484FA7F29652E66F18  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Rewired.Demos.PlayerPointerEventHandlerExample::text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___text_4;
	// System.Collections.Generic.List`1<System.String> Rewired.Demos.PlayerPointerEventHandlerExample::log
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___log_6;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(PlayerPointerEventHandlerExample_tB30A902E62F9AA0746875C484FA7F29652E66F18, ___text_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_text_4() const { return ___text_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_log_6() { return static_cast<int32_t>(offsetof(PlayerPointerEventHandlerExample_tB30A902E62F9AA0746875C484FA7F29652E66F18, ___log_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_log_6() const { return ___log_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_log_6() { return &___log_6; }
	inline void set_log_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___log_6 = value;
		Il2CppCodeGenWriteBarrier((&___log_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPOINTEREVENTHANDLEREXAMPLE_TB30A902E62F9AA0746875C484FA7F29652E66F18_H
#ifndef PRESSANYBUTTONTOJOINEXAMPLE_ASSIGNER_T22AF45AE8FEC38D2EA590D0041865CB39160ED1E_H
#define PRESSANYBUTTONTOJOINEXAMPLE_ASSIGNER_T22AF45AE8FEC38D2EA590D0041865CB39160ED1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.PressAnyButtonToJoinExample_Assigner
struct  PressAnyButtonToJoinExample_Assigner_t22AF45AE8FEC38D2EA590D0041865CB39160ED1E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESSANYBUTTONTOJOINEXAMPLE_ASSIGNER_T22AF45AE8FEC38D2EA590D0041865CB39160ED1E_H
#ifndef PRESSANYBUTTONTOJOINEXAMPLE_GAMEPLAYER_TCF49A9C64DA59FB40343CF81607D25BF6BC25AF9_H
#define PRESSANYBUTTONTOJOINEXAMPLE_GAMEPLAYER_TCF49A9C64DA59FB40343CF81607D25BF6BC25AF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.PressAnyButtonToJoinExample_GamePlayer
struct  PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Rewired.Demos.PressAnyButtonToJoinExample_GamePlayer::playerId
	int32_t ___playerId_4;
	// System.Single Rewired.Demos.PressAnyButtonToJoinExample_GamePlayer::moveSpeed
	float ___moveSpeed_5;
	// System.Single Rewired.Demos.PressAnyButtonToJoinExample_GamePlayer::bulletSpeed
	float ___bulletSpeed_6;
	// UnityEngine.GameObject Rewired.Demos.PressAnyButtonToJoinExample_GamePlayer::bulletPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bulletPrefab_7;
	// UnityEngine.CharacterController Rewired.Demos.PressAnyButtonToJoinExample_GamePlayer::cc
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___cc_8;
	// UnityEngine.Vector3 Rewired.Demos.PressAnyButtonToJoinExample_GamePlayer::moveVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___moveVector_9;
	// System.Boolean Rewired.Demos.PressAnyButtonToJoinExample_GamePlayer::fire
	bool ___fire_10;

public:
	inline static int32_t get_offset_of_playerId_4() { return static_cast<int32_t>(offsetof(PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9, ___playerId_4)); }
	inline int32_t get_playerId_4() const { return ___playerId_4; }
	inline int32_t* get_address_of_playerId_4() { return &___playerId_4; }
	inline void set_playerId_4(int32_t value)
	{
		___playerId_4 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_5() { return static_cast<int32_t>(offsetof(PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9, ___moveSpeed_5)); }
	inline float get_moveSpeed_5() const { return ___moveSpeed_5; }
	inline float* get_address_of_moveSpeed_5() { return &___moveSpeed_5; }
	inline void set_moveSpeed_5(float value)
	{
		___moveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_bulletSpeed_6() { return static_cast<int32_t>(offsetof(PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9, ___bulletSpeed_6)); }
	inline float get_bulletSpeed_6() const { return ___bulletSpeed_6; }
	inline float* get_address_of_bulletSpeed_6() { return &___bulletSpeed_6; }
	inline void set_bulletSpeed_6(float value)
	{
		___bulletSpeed_6 = value;
	}

	inline static int32_t get_offset_of_bulletPrefab_7() { return static_cast<int32_t>(offsetof(PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9, ___bulletPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bulletPrefab_7() const { return ___bulletPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bulletPrefab_7() { return &___bulletPrefab_7; }
	inline void set_bulletPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bulletPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___bulletPrefab_7), value);
	}

	inline static int32_t get_offset_of_cc_8() { return static_cast<int32_t>(offsetof(PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9, ___cc_8)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_cc_8() const { return ___cc_8; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_cc_8() { return &___cc_8; }
	inline void set_cc_8(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___cc_8 = value;
		Il2CppCodeGenWriteBarrier((&___cc_8), value);
	}

	inline static int32_t get_offset_of_moveVector_9() { return static_cast<int32_t>(offsetof(PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9, ___moveVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_moveVector_9() const { return ___moveVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_moveVector_9() { return &___moveVector_9; }
	inline void set_moveVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___moveVector_9 = value;
	}

	inline static int32_t get_offset_of_fire_10() { return static_cast<int32_t>(offsetof(PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9, ___fire_10)); }
	inline bool get_fire_10() const { return ___fire_10; }
	inline bool* get_address_of_fire_10() { return &___fire_10; }
	inline void set_fire_10(bool value)
	{
		___fire_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESSANYBUTTONTOJOINEXAMPLE_GAMEPLAYER_TCF49A9C64DA59FB40343CF81607D25BF6BC25AF9_H
#ifndef PRESSSTARTTOJOINEXAMPLE_ASSIGNER_T5778175307AC1A3C22FF4CAC097823D447D78296_H
#define PRESSSTARTTOJOINEXAMPLE_ASSIGNER_T5778175307AC1A3C22FF4CAC097823D447D78296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.PressStartToJoinExample_Assigner
struct  PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Rewired.Demos.PressStartToJoinExample_Assigner::maxPlayers
	int32_t ___maxPlayers_5;
	// System.Collections.Generic.List`1<Rewired.Demos.PressStartToJoinExample_Assigner_PlayerMap> Rewired.Demos.PressStartToJoinExample_Assigner::playerMap
	List_1_t0E2A8C3EA3D601F7E6D6C0F50353922447DF0205 * ___playerMap_6;
	// System.Int32 Rewired.Demos.PressStartToJoinExample_Assigner::gamePlayerIdCounter
	int32_t ___gamePlayerIdCounter_7;

public:
	inline static int32_t get_offset_of_maxPlayers_5() { return static_cast<int32_t>(offsetof(PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296, ___maxPlayers_5)); }
	inline int32_t get_maxPlayers_5() const { return ___maxPlayers_5; }
	inline int32_t* get_address_of_maxPlayers_5() { return &___maxPlayers_5; }
	inline void set_maxPlayers_5(int32_t value)
	{
		___maxPlayers_5 = value;
	}

	inline static int32_t get_offset_of_playerMap_6() { return static_cast<int32_t>(offsetof(PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296, ___playerMap_6)); }
	inline List_1_t0E2A8C3EA3D601F7E6D6C0F50353922447DF0205 * get_playerMap_6() const { return ___playerMap_6; }
	inline List_1_t0E2A8C3EA3D601F7E6D6C0F50353922447DF0205 ** get_address_of_playerMap_6() { return &___playerMap_6; }
	inline void set_playerMap_6(List_1_t0E2A8C3EA3D601F7E6D6C0F50353922447DF0205 * value)
	{
		___playerMap_6 = value;
		Il2CppCodeGenWriteBarrier((&___playerMap_6), value);
	}

	inline static int32_t get_offset_of_gamePlayerIdCounter_7() { return static_cast<int32_t>(offsetof(PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296, ___gamePlayerIdCounter_7)); }
	inline int32_t get_gamePlayerIdCounter_7() const { return ___gamePlayerIdCounter_7; }
	inline int32_t* get_address_of_gamePlayerIdCounter_7() { return &___gamePlayerIdCounter_7; }
	inline void set_gamePlayerIdCounter_7(int32_t value)
	{
		___gamePlayerIdCounter_7 = value;
	}
};

struct PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296_StaticFields
{
public:
	// Rewired.Demos.PressStartToJoinExample_Assigner Rewired.Demos.PressStartToJoinExample_Assigner::instance
	PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296_StaticFields, ___instance_4)); }
	inline PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296 * get_instance_4() const { return ___instance_4; }
	inline PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESSSTARTTOJOINEXAMPLE_ASSIGNER_T5778175307AC1A3C22FF4CAC097823D447D78296_H
#ifndef PRESSSTARTTOJOINEXAMPLE_GAMEPLAYER_T5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E_H
#define PRESSSTARTTOJOINEXAMPLE_GAMEPLAYER_T5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.PressStartToJoinExample_GamePlayer
struct  PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Rewired.Demos.PressStartToJoinExample_GamePlayer::gamePlayerId
	int32_t ___gamePlayerId_4;
	// System.Single Rewired.Demos.PressStartToJoinExample_GamePlayer::moveSpeed
	float ___moveSpeed_5;
	// System.Single Rewired.Demos.PressStartToJoinExample_GamePlayer::bulletSpeed
	float ___bulletSpeed_6;
	// UnityEngine.GameObject Rewired.Demos.PressStartToJoinExample_GamePlayer::bulletPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bulletPrefab_7;
	// UnityEngine.CharacterController Rewired.Demos.PressStartToJoinExample_GamePlayer::cc
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___cc_8;
	// UnityEngine.Vector3 Rewired.Demos.PressStartToJoinExample_GamePlayer::moveVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___moveVector_9;
	// System.Boolean Rewired.Demos.PressStartToJoinExample_GamePlayer::fire
	bool ___fire_10;

public:
	inline static int32_t get_offset_of_gamePlayerId_4() { return static_cast<int32_t>(offsetof(PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E, ___gamePlayerId_4)); }
	inline int32_t get_gamePlayerId_4() const { return ___gamePlayerId_4; }
	inline int32_t* get_address_of_gamePlayerId_4() { return &___gamePlayerId_4; }
	inline void set_gamePlayerId_4(int32_t value)
	{
		___gamePlayerId_4 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_5() { return static_cast<int32_t>(offsetof(PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E, ___moveSpeed_5)); }
	inline float get_moveSpeed_5() const { return ___moveSpeed_5; }
	inline float* get_address_of_moveSpeed_5() { return &___moveSpeed_5; }
	inline void set_moveSpeed_5(float value)
	{
		___moveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_bulletSpeed_6() { return static_cast<int32_t>(offsetof(PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E, ___bulletSpeed_6)); }
	inline float get_bulletSpeed_6() const { return ___bulletSpeed_6; }
	inline float* get_address_of_bulletSpeed_6() { return &___bulletSpeed_6; }
	inline void set_bulletSpeed_6(float value)
	{
		___bulletSpeed_6 = value;
	}

	inline static int32_t get_offset_of_bulletPrefab_7() { return static_cast<int32_t>(offsetof(PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E, ___bulletPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bulletPrefab_7() const { return ___bulletPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bulletPrefab_7() { return &___bulletPrefab_7; }
	inline void set_bulletPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bulletPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___bulletPrefab_7), value);
	}

	inline static int32_t get_offset_of_cc_8() { return static_cast<int32_t>(offsetof(PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E, ___cc_8)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_cc_8() const { return ___cc_8; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_cc_8() { return &___cc_8; }
	inline void set_cc_8(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___cc_8 = value;
		Il2CppCodeGenWriteBarrier((&___cc_8), value);
	}

	inline static int32_t get_offset_of_moveVector_9() { return static_cast<int32_t>(offsetof(PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E, ___moveVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_moveVector_9() const { return ___moveVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_moveVector_9() { return &___moveVector_9; }
	inline void set_moveVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___moveVector_9 = value;
	}

	inline static int32_t get_offset_of_fire_10() { return static_cast<int32_t>(offsetof(PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E, ___fire_10)); }
	inline bool get_fire_10() const { return ___fire_10; }
	inline bool* get_address_of_fire_10() { return &___fire_10; }
	inline void set_fire_10(bool value)
	{
		___fire_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESSSTARTTOJOINEXAMPLE_GAMEPLAYER_T5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E_H
#ifndef SIMPLECOMBINEDKEYBOARDMOUSEREMAPPING_T99831416E94DF644CA9BB116B1447C334F8499F0_H
#define SIMPLECOMBINEDKEYBOARDMOUSEREMAPPING_T99831416E94DF644CA9BB116B1447C334F8499F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.SimpleCombinedKeyboardMouseRemapping
struct  SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.InputMapper Rewired.Demos.SimpleCombinedKeyboardMouseRemapping::inputMapper_keyboard
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * ___inputMapper_keyboard_7;
	// Rewired.InputMapper Rewired.Demos.SimpleCombinedKeyboardMouseRemapping::inputMapper_mouse
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * ___inputMapper_mouse_8;
	// UnityEngine.GameObject Rewired.Demos.SimpleCombinedKeyboardMouseRemapping::buttonPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___buttonPrefab_9;
	// UnityEngine.GameObject Rewired.Demos.SimpleCombinedKeyboardMouseRemapping::textPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textPrefab_10;
	// UnityEngine.RectTransform Rewired.Demos.SimpleCombinedKeyboardMouseRemapping::fieldGroupTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___fieldGroupTransform_11;
	// UnityEngine.RectTransform Rewired.Demos.SimpleCombinedKeyboardMouseRemapping::actionGroupTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___actionGroupTransform_12;
	// UnityEngine.UI.Text Rewired.Demos.SimpleCombinedKeyboardMouseRemapping::controllerNameUIText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___controllerNameUIText_13;
	// UnityEngine.UI.Text Rewired.Demos.SimpleCombinedKeyboardMouseRemapping::statusUIText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___statusUIText_14;
	// System.Collections.Generic.List`1<Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_Row> Rewired.Demos.SimpleCombinedKeyboardMouseRemapping::rows
	List_1_tBDA5BA13CAE0CC282027AA9C5F64A121D05BE771 * ___rows_15;
	// Rewired.Demos.SimpleCombinedKeyboardMouseRemapping_TargetMapping Rewired.Demos.SimpleCombinedKeyboardMouseRemapping::_replaceTargetMapping
	TargetMapping_tCF6708DB0700100C669867F37A1066537F71BFD7  ____replaceTargetMapping_16;

public:
	inline static int32_t get_offset_of_inputMapper_keyboard_7() { return static_cast<int32_t>(offsetof(SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0, ___inputMapper_keyboard_7)); }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * get_inputMapper_keyboard_7() const { return ___inputMapper_keyboard_7; }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB ** get_address_of_inputMapper_keyboard_7() { return &___inputMapper_keyboard_7; }
	inline void set_inputMapper_keyboard_7(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * value)
	{
		___inputMapper_keyboard_7 = value;
		Il2CppCodeGenWriteBarrier((&___inputMapper_keyboard_7), value);
	}

	inline static int32_t get_offset_of_inputMapper_mouse_8() { return static_cast<int32_t>(offsetof(SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0, ___inputMapper_mouse_8)); }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * get_inputMapper_mouse_8() const { return ___inputMapper_mouse_8; }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB ** get_address_of_inputMapper_mouse_8() { return &___inputMapper_mouse_8; }
	inline void set_inputMapper_mouse_8(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * value)
	{
		___inputMapper_mouse_8 = value;
		Il2CppCodeGenWriteBarrier((&___inputMapper_mouse_8), value);
	}

	inline static int32_t get_offset_of_buttonPrefab_9() { return static_cast<int32_t>(offsetof(SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0, ___buttonPrefab_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_buttonPrefab_9() const { return ___buttonPrefab_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_buttonPrefab_9() { return &___buttonPrefab_9; }
	inline void set_buttonPrefab_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___buttonPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___buttonPrefab_9), value);
	}

	inline static int32_t get_offset_of_textPrefab_10() { return static_cast<int32_t>(offsetof(SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0, ___textPrefab_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textPrefab_10() const { return ___textPrefab_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textPrefab_10() { return &___textPrefab_10; }
	inline void set_textPrefab_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___textPrefab_10), value);
	}

	inline static int32_t get_offset_of_fieldGroupTransform_11() { return static_cast<int32_t>(offsetof(SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0, ___fieldGroupTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_fieldGroupTransform_11() const { return ___fieldGroupTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_fieldGroupTransform_11() { return &___fieldGroupTransform_11; }
	inline void set_fieldGroupTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___fieldGroupTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___fieldGroupTransform_11), value);
	}

	inline static int32_t get_offset_of_actionGroupTransform_12() { return static_cast<int32_t>(offsetof(SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0, ___actionGroupTransform_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_actionGroupTransform_12() const { return ___actionGroupTransform_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_actionGroupTransform_12() { return &___actionGroupTransform_12; }
	inline void set_actionGroupTransform_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___actionGroupTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___actionGroupTransform_12), value);
	}

	inline static int32_t get_offset_of_controllerNameUIText_13() { return static_cast<int32_t>(offsetof(SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0, ___controllerNameUIText_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_controllerNameUIText_13() const { return ___controllerNameUIText_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_controllerNameUIText_13() { return &___controllerNameUIText_13; }
	inline void set_controllerNameUIText_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___controllerNameUIText_13 = value;
		Il2CppCodeGenWriteBarrier((&___controllerNameUIText_13), value);
	}

	inline static int32_t get_offset_of_statusUIText_14() { return static_cast<int32_t>(offsetof(SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0, ___statusUIText_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_statusUIText_14() const { return ___statusUIText_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_statusUIText_14() { return &___statusUIText_14; }
	inline void set_statusUIText_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___statusUIText_14 = value;
		Il2CppCodeGenWriteBarrier((&___statusUIText_14), value);
	}

	inline static int32_t get_offset_of_rows_15() { return static_cast<int32_t>(offsetof(SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0, ___rows_15)); }
	inline List_1_tBDA5BA13CAE0CC282027AA9C5F64A121D05BE771 * get_rows_15() const { return ___rows_15; }
	inline List_1_tBDA5BA13CAE0CC282027AA9C5F64A121D05BE771 ** get_address_of_rows_15() { return &___rows_15; }
	inline void set_rows_15(List_1_tBDA5BA13CAE0CC282027AA9C5F64A121D05BE771 * value)
	{
		___rows_15 = value;
		Il2CppCodeGenWriteBarrier((&___rows_15), value);
	}

	inline static int32_t get_offset_of__replaceTargetMapping_16() { return static_cast<int32_t>(offsetof(SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0, ____replaceTargetMapping_16)); }
	inline TargetMapping_tCF6708DB0700100C669867F37A1066537F71BFD7  get__replaceTargetMapping_16() const { return ____replaceTargetMapping_16; }
	inline TargetMapping_tCF6708DB0700100C669867F37A1066537F71BFD7 * get_address_of__replaceTargetMapping_16() { return &____replaceTargetMapping_16; }
	inline void set__replaceTargetMapping_16(TargetMapping_tCF6708DB0700100C669867F37A1066537F71BFD7  value)
	{
		____replaceTargetMapping_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLECOMBINEDKEYBOARDMOUSEREMAPPING_T99831416E94DF644CA9BB116B1447C334F8499F0_H
#ifndef SIMPLECONTROLREMAPPING_T52866691ACBC69EB8C3D48ABBA21BE2AFF791A24_H
#define SIMPLECONTROLREMAPPING_T52866691ACBC69EB8C3D48ABBA21BE2AFF791A24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.SimpleControlRemapping
struct  SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.InputMapper Rewired.Demos.SimpleControlRemapping::inputMapper
	InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * ___inputMapper_7;
	// UnityEngine.GameObject Rewired.Demos.SimpleControlRemapping::buttonPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___buttonPrefab_8;
	// UnityEngine.GameObject Rewired.Demos.SimpleControlRemapping::textPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textPrefab_9;
	// UnityEngine.RectTransform Rewired.Demos.SimpleControlRemapping::fieldGroupTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___fieldGroupTransform_10;
	// UnityEngine.RectTransform Rewired.Demos.SimpleControlRemapping::actionGroupTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___actionGroupTransform_11;
	// UnityEngine.UI.Text Rewired.Demos.SimpleControlRemapping::controllerNameUIText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___controllerNameUIText_12;
	// UnityEngine.UI.Text Rewired.Demos.SimpleControlRemapping::statusUIText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___statusUIText_13;
	// Rewired.ControllerType Rewired.Demos.SimpleControlRemapping::selectedControllerType
	int32_t ___selectedControllerType_14;
	// System.Int32 Rewired.Demos.SimpleControlRemapping::selectedControllerId
	int32_t ___selectedControllerId_15;
	// System.Collections.Generic.List`1<Rewired.Demos.SimpleControlRemapping_Row> Rewired.Demos.SimpleControlRemapping::rows
	List_1_t98B481DEA763C89090B7F0152A1C4350750410D0 * ___rows_16;

public:
	inline static int32_t get_offset_of_inputMapper_7() { return static_cast<int32_t>(offsetof(SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24, ___inputMapper_7)); }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * get_inputMapper_7() const { return ___inputMapper_7; }
	inline InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB ** get_address_of_inputMapper_7() { return &___inputMapper_7; }
	inline void set_inputMapper_7(InputMapper_t88964D3FF000CFD6AA43DBCAFA71A96264C7F2EB * value)
	{
		___inputMapper_7 = value;
		Il2CppCodeGenWriteBarrier((&___inputMapper_7), value);
	}

	inline static int32_t get_offset_of_buttonPrefab_8() { return static_cast<int32_t>(offsetof(SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24, ___buttonPrefab_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_buttonPrefab_8() const { return ___buttonPrefab_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_buttonPrefab_8() { return &___buttonPrefab_8; }
	inline void set_buttonPrefab_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___buttonPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___buttonPrefab_8), value);
	}

	inline static int32_t get_offset_of_textPrefab_9() { return static_cast<int32_t>(offsetof(SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24, ___textPrefab_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textPrefab_9() const { return ___textPrefab_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textPrefab_9() { return &___textPrefab_9; }
	inline void set_textPrefab_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___textPrefab_9), value);
	}

	inline static int32_t get_offset_of_fieldGroupTransform_10() { return static_cast<int32_t>(offsetof(SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24, ___fieldGroupTransform_10)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_fieldGroupTransform_10() const { return ___fieldGroupTransform_10; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_fieldGroupTransform_10() { return &___fieldGroupTransform_10; }
	inline void set_fieldGroupTransform_10(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___fieldGroupTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___fieldGroupTransform_10), value);
	}

	inline static int32_t get_offset_of_actionGroupTransform_11() { return static_cast<int32_t>(offsetof(SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24, ___actionGroupTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_actionGroupTransform_11() const { return ___actionGroupTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_actionGroupTransform_11() { return &___actionGroupTransform_11; }
	inline void set_actionGroupTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___actionGroupTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___actionGroupTransform_11), value);
	}

	inline static int32_t get_offset_of_controllerNameUIText_12() { return static_cast<int32_t>(offsetof(SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24, ___controllerNameUIText_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_controllerNameUIText_12() const { return ___controllerNameUIText_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_controllerNameUIText_12() { return &___controllerNameUIText_12; }
	inline void set_controllerNameUIText_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___controllerNameUIText_12 = value;
		Il2CppCodeGenWriteBarrier((&___controllerNameUIText_12), value);
	}

	inline static int32_t get_offset_of_statusUIText_13() { return static_cast<int32_t>(offsetof(SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24, ___statusUIText_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_statusUIText_13() const { return ___statusUIText_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_statusUIText_13() { return &___statusUIText_13; }
	inline void set_statusUIText_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___statusUIText_13 = value;
		Il2CppCodeGenWriteBarrier((&___statusUIText_13), value);
	}

	inline static int32_t get_offset_of_selectedControllerType_14() { return static_cast<int32_t>(offsetof(SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24, ___selectedControllerType_14)); }
	inline int32_t get_selectedControllerType_14() const { return ___selectedControllerType_14; }
	inline int32_t* get_address_of_selectedControllerType_14() { return &___selectedControllerType_14; }
	inline void set_selectedControllerType_14(int32_t value)
	{
		___selectedControllerType_14 = value;
	}

	inline static int32_t get_offset_of_selectedControllerId_15() { return static_cast<int32_t>(offsetof(SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24, ___selectedControllerId_15)); }
	inline int32_t get_selectedControllerId_15() const { return ___selectedControllerId_15; }
	inline int32_t* get_address_of_selectedControllerId_15() { return &___selectedControllerId_15; }
	inline void set_selectedControllerId_15(int32_t value)
	{
		___selectedControllerId_15 = value;
	}

	inline static int32_t get_offset_of_rows_16() { return static_cast<int32_t>(offsetof(SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24, ___rows_16)); }
	inline List_1_t98B481DEA763C89090B7F0152A1C4350750410D0 * get_rows_16() const { return ___rows_16; }
	inline List_1_t98B481DEA763C89090B7F0152A1C4350750410D0 ** get_address_of_rows_16() { return &___rows_16; }
	inline void set_rows_16(List_1_t98B481DEA763C89090B7F0152A1C4350750410D0 * value)
	{
		___rows_16 = value;
		Il2CppCodeGenWriteBarrier((&___rows_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLECONTROLREMAPPING_T52866691ACBC69EB8C3D48ABBA21BE2AFF791A24_H
#ifndef TOUCHBUTTONEXAMPLE_T373D910E3161BD489B1A9358D1728E4EC7905E66_H
#define TOUCHBUTTONEXAMPLE_T373D910E3161BD489B1A9358D1728E4EC7905E66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.TouchButtonExample
struct  TouchButtonExample_t373D910E3161BD489B1A9358D1728E4EC7905E66  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Rewired.Demos.TouchButtonExample::allowMouseControl
	bool ___allowMouseControl_4;
	// System.Boolean Rewired.Demos.TouchButtonExample::<isPressed>k__BackingField
	bool ___U3CisPressedU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_allowMouseControl_4() { return static_cast<int32_t>(offsetof(TouchButtonExample_t373D910E3161BD489B1A9358D1728E4EC7905E66, ___allowMouseControl_4)); }
	inline bool get_allowMouseControl_4() const { return ___allowMouseControl_4; }
	inline bool* get_address_of_allowMouseControl_4() { return &___allowMouseControl_4; }
	inline void set_allowMouseControl_4(bool value)
	{
		___allowMouseControl_4 = value;
	}

	inline static int32_t get_offset_of_U3CisPressedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TouchButtonExample_t373D910E3161BD489B1A9358D1728E4EC7905E66, ___U3CisPressedU3Ek__BackingField_5)); }
	inline bool get_U3CisPressedU3Ek__BackingField_5() const { return ___U3CisPressedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CisPressedU3Ek__BackingField_5() { return &___U3CisPressedU3Ek__BackingField_5; }
	inline void set_U3CisPressedU3Ek__BackingField_5(bool value)
	{
		___U3CisPressedU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHBUTTONEXAMPLE_T373D910E3161BD489B1A9358D1728E4EC7905E66_H
#ifndef TOUCHCONTROLS1_MANIPULATECUBE_T777052D91E1E2E18A7CAA676E5F819C41A2D695F_H
#define TOUCHCONTROLS1_MANIPULATECUBE_T777052D91E1E2E18A7CAA676E5F819C41A2D695F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.TouchControls1_ManipulateCube
struct  TouchControls1_ManipulateCube_t777052D91E1E2E18A7CAA676E5F819C41A2D695F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Rewired.Demos.TouchControls1_ManipulateCube::rotateSpeed
	float ___rotateSpeed_4;
	// System.Single Rewired.Demos.TouchControls1_ManipulateCube::moveSpeed
	float ___moveSpeed_5;
	// System.Int32 Rewired.Demos.TouchControls1_ManipulateCube::currentColorIndex
	int32_t ___currentColorIndex_6;
	// UnityEngine.Color[] Rewired.Demos.TouchControls1_ManipulateCube::colors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___colors_7;

public:
	inline static int32_t get_offset_of_rotateSpeed_4() { return static_cast<int32_t>(offsetof(TouchControls1_ManipulateCube_t777052D91E1E2E18A7CAA676E5F819C41A2D695F, ___rotateSpeed_4)); }
	inline float get_rotateSpeed_4() const { return ___rotateSpeed_4; }
	inline float* get_address_of_rotateSpeed_4() { return &___rotateSpeed_4; }
	inline void set_rotateSpeed_4(float value)
	{
		___rotateSpeed_4 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_5() { return static_cast<int32_t>(offsetof(TouchControls1_ManipulateCube_t777052D91E1E2E18A7CAA676E5F819C41A2D695F, ___moveSpeed_5)); }
	inline float get_moveSpeed_5() const { return ___moveSpeed_5; }
	inline float* get_address_of_moveSpeed_5() { return &___moveSpeed_5; }
	inline void set_moveSpeed_5(float value)
	{
		___moveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_currentColorIndex_6() { return static_cast<int32_t>(offsetof(TouchControls1_ManipulateCube_t777052D91E1E2E18A7CAA676E5F819C41A2D695F, ___currentColorIndex_6)); }
	inline int32_t get_currentColorIndex_6() const { return ___currentColorIndex_6; }
	inline int32_t* get_address_of_currentColorIndex_6() { return &___currentColorIndex_6; }
	inline void set_currentColorIndex_6(int32_t value)
	{
		___currentColorIndex_6 = value;
	}

	inline static int32_t get_offset_of_colors_7() { return static_cast<int32_t>(offsetof(TouchControls1_ManipulateCube_t777052D91E1E2E18A7CAA676E5F819C41A2D695F, ___colors_7)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_colors_7() const { return ___colors_7; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_colors_7() { return &___colors_7; }
	inline void set_colors_7(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___colors_7 = value;
		Il2CppCodeGenWriteBarrier((&___colors_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCONTROLS1_MANIPULATECUBE_T777052D91E1E2E18A7CAA676E5F819C41A2D695F_H
#ifndef TOUCHJOYSTICKEXAMPLE_T25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE_H
#define TOUCHJOYSTICKEXAMPLE_T25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.TouchJoystickExample
struct  TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Rewired.Demos.TouchJoystickExample::allowMouseControl
	bool ___allowMouseControl_4;
	// System.Int32 Rewired.Demos.TouchJoystickExample::radius
	int32_t ___radius_5;
	// UnityEngine.Vector2 Rewired.Demos.TouchJoystickExample::origAnchoredPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___origAnchoredPosition_6;
	// UnityEngine.Vector3 Rewired.Demos.TouchJoystickExample::origWorldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origWorldPosition_7;
	// UnityEngine.Vector2 Rewired.Demos.TouchJoystickExample::origScreenResolution
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___origScreenResolution_8;
	// UnityEngine.ScreenOrientation Rewired.Demos.TouchJoystickExample::origScreenOrientation
	int32_t ___origScreenOrientation_9;
	// System.Boolean Rewired.Demos.TouchJoystickExample::hasFinger
	bool ___hasFinger_10;
	// System.Int32 Rewired.Demos.TouchJoystickExample::lastFingerId
	int32_t ___lastFingerId_11;
	// UnityEngine.Vector2 Rewired.Demos.TouchJoystickExample::<position>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CpositionU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_allowMouseControl_4() { return static_cast<int32_t>(offsetof(TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE, ___allowMouseControl_4)); }
	inline bool get_allowMouseControl_4() const { return ___allowMouseControl_4; }
	inline bool* get_address_of_allowMouseControl_4() { return &___allowMouseControl_4; }
	inline void set_allowMouseControl_4(bool value)
	{
		___allowMouseControl_4 = value;
	}

	inline static int32_t get_offset_of_radius_5() { return static_cast<int32_t>(offsetof(TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE, ___radius_5)); }
	inline int32_t get_radius_5() const { return ___radius_5; }
	inline int32_t* get_address_of_radius_5() { return &___radius_5; }
	inline void set_radius_5(int32_t value)
	{
		___radius_5 = value;
	}

	inline static int32_t get_offset_of_origAnchoredPosition_6() { return static_cast<int32_t>(offsetof(TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE, ___origAnchoredPosition_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_origAnchoredPosition_6() const { return ___origAnchoredPosition_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_origAnchoredPosition_6() { return &___origAnchoredPosition_6; }
	inline void set_origAnchoredPosition_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___origAnchoredPosition_6 = value;
	}

	inline static int32_t get_offset_of_origWorldPosition_7() { return static_cast<int32_t>(offsetof(TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE, ___origWorldPosition_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_origWorldPosition_7() const { return ___origWorldPosition_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_origWorldPosition_7() { return &___origWorldPosition_7; }
	inline void set_origWorldPosition_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___origWorldPosition_7 = value;
	}

	inline static int32_t get_offset_of_origScreenResolution_8() { return static_cast<int32_t>(offsetof(TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE, ___origScreenResolution_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_origScreenResolution_8() const { return ___origScreenResolution_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_origScreenResolution_8() { return &___origScreenResolution_8; }
	inline void set_origScreenResolution_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___origScreenResolution_8 = value;
	}

	inline static int32_t get_offset_of_origScreenOrientation_9() { return static_cast<int32_t>(offsetof(TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE, ___origScreenOrientation_9)); }
	inline int32_t get_origScreenOrientation_9() const { return ___origScreenOrientation_9; }
	inline int32_t* get_address_of_origScreenOrientation_9() { return &___origScreenOrientation_9; }
	inline void set_origScreenOrientation_9(int32_t value)
	{
		___origScreenOrientation_9 = value;
	}

	inline static int32_t get_offset_of_hasFinger_10() { return static_cast<int32_t>(offsetof(TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE, ___hasFinger_10)); }
	inline bool get_hasFinger_10() const { return ___hasFinger_10; }
	inline bool* get_address_of_hasFinger_10() { return &___hasFinger_10; }
	inline void set_hasFinger_10(bool value)
	{
		___hasFinger_10 = value;
	}

	inline static int32_t get_offset_of_lastFingerId_11() { return static_cast<int32_t>(offsetof(TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE, ___lastFingerId_11)); }
	inline int32_t get_lastFingerId_11() const { return ___lastFingerId_11; }
	inline int32_t* get_address_of_lastFingerId_11() { return &___lastFingerId_11; }
	inline void set_lastFingerId_11(int32_t value)
	{
		___lastFingerId_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHJOYSTICKEXAMPLE_T25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE_H
#ifndef THEMEDELEMENT_T58E027D40D50175509155F319FF8B73FCCAC582D_H
#define THEMEDELEMENT_T58E027D40D50175509155F319FF8B73FCCAC582D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ThemedElement
struct  ThemedElement_t58E027D40D50175509155F319FF8B73FCCAC582D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.UI.ControlMapper.ThemedElement_ElementInfo[] Rewired.UI.ControlMapper.ThemedElement::_elements
	ElementInfoU5BU5D_t5E095D1C45E7F0B39A10D4FB52C13B53EC9CD00B* ____elements_4;

public:
	inline static int32_t get_offset_of__elements_4() { return static_cast<int32_t>(offsetof(ThemedElement_t58E027D40D50175509155F319FF8B73FCCAC582D, ____elements_4)); }
	inline ElementInfoU5BU5D_t5E095D1C45E7F0B39A10D4FB52C13B53EC9CD00B* get__elements_4() const { return ____elements_4; }
	inline ElementInfoU5BU5D_t5E095D1C45E7F0B39A10D4FB52C13B53EC9CD00B** get_address_of__elements_4() { return &____elements_4; }
	inline void set__elements_4(ElementInfoU5BU5D_t5E095D1C45E7F0B39A10D4FB52C13B53EC9CD00B* value)
	{
		____elements_4 = value;
		Il2CppCodeGenWriteBarrier((&____elements_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THEMEDELEMENT_T58E027D40D50175509155F319FF8B73FCCAC582D_H
#ifndef UICONTROL_T4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3_H
#define UICONTROL_T4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.UIControl
struct  UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.UIControl::title
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___title_4;
	// System.Int32 Rewired.UI.ControlMapper.UIControl::_id
	int32_t ____id_5;
	// System.Boolean Rewired.UI.ControlMapper.UIControl::_showTitle
	bool ____showTitle_6;

public:
	inline static int32_t get_offset_of_title_4() { return static_cast<int32_t>(offsetof(UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3, ___title_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_title_4() const { return ___title_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_title_4() { return &___title_4; }
	inline void set_title_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___title_4 = value;
		Il2CppCodeGenWriteBarrier((&___title_4), value);
	}

	inline static int32_t get_offset_of__id_5() { return static_cast<int32_t>(offsetof(UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3, ____id_5)); }
	inline int32_t get__id_5() const { return ____id_5; }
	inline int32_t* get_address_of__id_5() { return &____id_5; }
	inline void set__id_5(int32_t value)
	{
		____id_5 = value;
	}

	inline static int32_t get_offset_of__showTitle_6() { return static_cast<int32_t>(offsetof(UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3, ____showTitle_6)); }
	inline bool get__showTitle_6() const { return ____showTitle_6; }
	inline bool* get_address_of__showTitle_6() { return &____showTitle_6; }
	inline void set__showTitle_6(bool value)
	{
		____showTitle_6 = value;
	}
};

struct UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3_StaticFields
{
public:
	// System.Int32 Rewired.UI.ControlMapper.UIControl::_uidCounter
	int32_t ____uidCounter_7;

public:
	inline static int32_t get_offset_of__uidCounter_7() { return static_cast<int32_t>(offsetof(UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3_StaticFields, ____uidCounter_7)); }
	inline int32_t get__uidCounter_7() const { return ____uidCounter_7; }
	inline int32_t* get_address_of__uidCounter_7() { return &____uidCounter_7; }
	inline void set__uidCounter_7(int32_t value)
	{
		____uidCounter_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICONTROL_T4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3_H
#ifndef UICONTROLSET_T0BE4C6F06B453187D70603E003EB07A252B5C083_H
#define UICONTROLSET_T0BE4C6F06B453187D70603E003EB07A252B5C083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.UIControlSet
struct  UIControlSet_t0BE4C6F06B453187D70603E003EB07A252B5C083  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.UIControlSet::title
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___title_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.UI.ControlMapper.UIControl> Rewired.UI.ControlMapper.UIControlSet::_controls
	Dictionary_2_t4541A154CF51CC7F413C4CD599DCC87BFD4189C5 * ____controls_5;

public:
	inline static int32_t get_offset_of_title_4() { return static_cast<int32_t>(offsetof(UIControlSet_t0BE4C6F06B453187D70603E003EB07A252B5C083, ___title_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_title_4() const { return ___title_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_title_4() { return &___title_4; }
	inline void set_title_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___title_4 = value;
		Il2CppCodeGenWriteBarrier((&___title_4), value);
	}

	inline static int32_t get_offset_of__controls_5() { return static_cast<int32_t>(offsetof(UIControlSet_t0BE4C6F06B453187D70603E003EB07A252B5C083, ____controls_5)); }
	inline Dictionary_2_t4541A154CF51CC7F413C4CD599DCC87BFD4189C5 * get__controls_5() const { return ____controls_5; }
	inline Dictionary_2_t4541A154CF51CC7F413C4CD599DCC87BFD4189C5 ** get_address_of__controls_5() { return &____controls_5; }
	inline void set__controls_5(Dictionary_2_t4541A154CF51CC7F413C4CD599DCC87BFD4189C5 * value)
	{
		____controls_5 = value;
		Il2CppCodeGenWriteBarrier((&____controls_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICONTROLSET_T0BE4C6F06B453187D70603E003EB07A252B5C083_H
#ifndef UIELEMENTINFO_T2DBC68E382F8428D528D5AF6D14EB38F998B3ECA_H
#define UIELEMENTINFO_T2DBC68E382F8428D528D5AF6D14EB38F998B3ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.UIElementInfo
struct  UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Rewired.UI.ControlMapper.UIElementInfo::identifier
	String_t* ___identifier_4;
	// System.Int32 Rewired.UI.ControlMapper.UIElementInfo::intData
	int32_t ___intData_5;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.UIElementInfo::text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___text_6;
	// System.Action`1<UnityEngine.GameObject> Rewired.UI.ControlMapper.UIElementInfo::OnSelectedEvent
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___OnSelectedEvent_7;

public:
	inline static int32_t get_offset_of_identifier_4() { return static_cast<int32_t>(offsetof(UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA, ___identifier_4)); }
	inline String_t* get_identifier_4() const { return ___identifier_4; }
	inline String_t** get_address_of_identifier_4() { return &___identifier_4; }
	inline void set_identifier_4(String_t* value)
	{
		___identifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_4), value);
	}

	inline static int32_t get_offset_of_intData_5() { return static_cast<int32_t>(offsetof(UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA, ___intData_5)); }
	inline int32_t get_intData_5() const { return ___intData_5; }
	inline int32_t* get_address_of_intData_5() { return &___intData_5; }
	inline void set_intData_5(int32_t value)
	{
		___intData_5 = value;
	}

	inline static int32_t get_offset_of_text_6() { return static_cast<int32_t>(offsetof(UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA, ___text_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_text_6() const { return ___text_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_text_6() { return &___text_6; }
	inline void set_text_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___text_6 = value;
		Il2CppCodeGenWriteBarrier((&___text_6), value);
	}

	inline static int32_t get_offset_of_OnSelectedEvent_7() { return static_cast<int32_t>(offsetof(UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA, ___OnSelectedEvent_7)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_OnSelectedEvent_7() const { return ___OnSelectedEvent_7; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_OnSelectedEvent_7() { return &___OnSelectedEvent_7; }
	inline void set_OnSelectedEvent_7(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___OnSelectedEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectedEvent_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIELEMENTINFO_T2DBC68E382F8428D528D5AF6D14EB38F998B3ECA_H
#ifndef UIGROUP_T3E1723AB180AC374C3A733F4337345E9F9AECD46_H
#define UIGROUP_T3E1723AB180AC374C3A733F4337345E9F9AECD46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.UIGroup
struct  UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.UIGroup::_label
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____label_4;
	// UnityEngine.Transform Rewired.UI.ControlMapper.UIGroup::_content
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____content_5;

public:
	inline static int32_t get_offset_of__label_4() { return static_cast<int32_t>(offsetof(UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46, ____label_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__label_4() const { return ____label_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__label_4() { return &____label_4; }
	inline void set__label_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____label_4 = value;
		Il2CppCodeGenWriteBarrier((&____label_4), value);
	}

	inline static int32_t get_offset_of__content_5() { return static_cast<int32_t>(offsetof(UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46, ____content_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__content_5() const { return ____content_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__content_5() { return &____content_5; }
	inline void set__content_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____content_5 = value;
		Il2CppCodeGenWriteBarrier((&____content_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGROUP_T3E1723AB180AC374C3A733F4337345E9F9AECD46_H
#ifndef UIIMAGEHELPER_TAAAEB396C88B3CB670E40DB3F5C3EB9C5F7509CA_H
#define UIIMAGEHELPER_TAAAEB396C88B3CB670E40DB3F5C3EB9C5F7509CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.UIImageHelper
struct  UIImageHelper_tAAAEB396C88B3CB670E40DB3F5C3EB9C5F7509CA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.UI.ControlMapper.UIImageHelper_State Rewired.UI.ControlMapper.UIImageHelper::enabledState
	State_t58DA7EDFC97612C3E0E86571BA582D5F00193027 * ___enabledState_4;
	// Rewired.UI.ControlMapper.UIImageHelper_State Rewired.UI.ControlMapper.UIImageHelper::disabledState
	State_t58DA7EDFC97612C3E0E86571BA582D5F00193027 * ___disabledState_5;
	// System.Boolean Rewired.UI.ControlMapper.UIImageHelper::currentState
	bool ___currentState_6;

public:
	inline static int32_t get_offset_of_enabledState_4() { return static_cast<int32_t>(offsetof(UIImageHelper_tAAAEB396C88B3CB670E40DB3F5C3EB9C5F7509CA, ___enabledState_4)); }
	inline State_t58DA7EDFC97612C3E0E86571BA582D5F00193027 * get_enabledState_4() const { return ___enabledState_4; }
	inline State_t58DA7EDFC97612C3E0E86571BA582D5F00193027 ** get_address_of_enabledState_4() { return &___enabledState_4; }
	inline void set_enabledState_4(State_t58DA7EDFC97612C3E0E86571BA582D5F00193027 * value)
	{
		___enabledState_4 = value;
		Il2CppCodeGenWriteBarrier((&___enabledState_4), value);
	}

	inline static int32_t get_offset_of_disabledState_5() { return static_cast<int32_t>(offsetof(UIImageHelper_tAAAEB396C88B3CB670E40DB3F5C3EB9C5F7509CA, ___disabledState_5)); }
	inline State_t58DA7EDFC97612C3E0E86571BA582D5F00193027 * get_disabledState_5() const { return ___disabledState_5; }
	inline State_t58DA7EDFC97612C3E0E86571BA582D5F00193027 ** get_address_of_disabledState_5() { return &___disabledState_5; }
	inline void set_disabledState_5(State_t58DA7EDFC97612C3E0E86571BA582D5F00193027 * value)
	{
		___disabledState_5 = value;
		Il2CppCodeGenWriteBarrier((&___disabledState_5), value);
	}

	inline static int32_t get_offset_of_currentState_6() { return static_cast<int32_t>(offsetof(UIImageHelper_tAAAEB396C88B3CB670E40DB3F5C3EB9C5F7509CA, ___currentState_6)); }
	inline bool get_currentState_6() const { return ___currentState_6; }
	inline bool* get_address_of_currentState_6() { return &___currentState_6; }
	inline void set_currentState_6(bool value)
	{
		___currentState_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIIMAGEHELPER_TAAAEB396C88B3CB670E40DB3F5C3EB9C5F7509CA_H
#ifndef WINDOW_T05E229BCE867863C1A33354186F3E7641707AAA5_H
#define WINDOW_T05E229BCE867863C1A33354186F3E7641707AAA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.Window
struct  Window_t05E229BCE867863C1A33354186F3E7641707AAA5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image Rewired.UI.ControlMapper.Window::backgroundImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___backgroundImage_4;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.Window::content
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___content_5;
	// System.Boolean Rewired.UI.ControlMapper.Window::_initialized
	bool ____initialized_6;
	// System.Int32 Rewired.UI.ControlMapper.Window::_id
	int32_t ____id_7;
	// UnityEngine.RectTransform Rewired.UI.ControlMapper.Window::_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____rectTransform_8;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.Window::_titleText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____titleText_9;
	// System.Collections.Generic.List`1<UnityEngine.UI.Text> Rewired.UI.ControlMapper.Window::_contentText
	List_1_t48FDD3CF761FA5A5451E50F64C5529D419339F18 * ____contentText_10;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.Window::_defaultUIElement
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____defaultUIElement_11;
	// System.Action`1<System.Int32> Rewired.UI.ControlMapper.Window::_updateCallback
	Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * ____updateCallback_12;
	// System.Func`2<System.Int32,System.Boolean> Rewired.UI.ControlMapper.Window::_isFocusedCallback
	Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * ____isFocusedCallback_13;
	// Rewired.UI.ControlMapper.Window_Timer Rewired.UI.ControlMapper.Window::_timer
	Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D * ____timer_14;
	// UnityEngine.CanvasGroup Rewired.UI.ControlMapper.Window::_canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ____canvasGroup_15;
	// UnityEngine.Events.UnityAction Rewired.UI.ControlMapper.Window::cancelCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___cancelCallback_16;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.Window::lastUISelection
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lastUISelection_17;

public:
	inline static int32_t get_offset_of_backgroundImage_4() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ___backgroundImage_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_backgroundImage_4() const { return ___backgroundImage_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_backgroundImage_4() { return &___backgroundImage_4; }
	inline void set_backgroundImage_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___backgroundImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundImage_4), value);
	}

	inline static int32_t get_offset_of_content_5() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ___content_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_content_5() const { return ___content_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_content_5() { return &___content_5; }
	inline void set_content_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___content_5 = value;
		Il2CppCodeGenWriteBarrier((&___content_5), value);
	}

	inline static int32_t get_offset_of__initialized_6() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____initialized_6)); }
	inline bool get__initialized_6() const { return ____initialized_6; }
	inline bool* get_address_of__initialized_6() { return &____initialized_6; }
	inline void set__initialized_6(bool value)
	{
		____initialized_6 = value;
	}

	inline static int32_t get_offset_of__id_7() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____id_7)); }
	inline int32_t get__id_7() const { return ____id_7; }
	inline int32_t* get_address_of__id_7() { return &____id_7; }
	inline void set__id_7(int32_t value)
	{
		____id_7 = value;
	}

	inline static int32_t get_offset_of__rectTransform_8() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____rectTransform_8)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__rectTransform_8() const { return ____rectTransform_8; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__rectTransform_8() { return &____rectTransform_8; }
	inline void set__rectTransform_8(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____rectTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_8), value);
	}

	inline static int32_t get_offset_of__titleText_9() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____titleText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__titleText_9() const { return ____titleText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__titleText_9() { return &____titleText_9; }
	inline void set__titleText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____titleText_9 = value;
		Il2CppCodeGenWriteBarrier((&____titleText_9), value);
	}

	inline static int32_t get_offset_of__contentText_10() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____contentText_10)); }
	inline List_1_t48FDD3CF761FA5A5451E50F64C5529D419339F18 * get__contentText_10() const { return ____contentText_10; }
	inline List_1_t48FDD3CF761FA5A5451E50F64C5529D419339F18 ** get_address_of__contentText_10() { return &____contentText_10; }
	inline void set__contentText_10(List_1_t48FDD3CF761FA5A5451E50F64C5529D419339F18 * value)
	{
		____contentText_10 = value;
		Il2CppCodeGenWriteBarrier((&____contentText_10), value);
	}

	inline static int32_t get_offset_of__defaultUIElement_11() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____defaultUIElement_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__defaultUIElement_11() const { return ____defaultUIElement_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__defaultUIElement_11() { return &____defaultUIElement_11; }
	inline void set__defaultUIElement_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____defaultUIElement_11 = value;
		Il2CppCodeGenWriteBarrier((&____defaultUIElement_11), value);
	}

	inline static int32_t get_offset_of__updateCallback_12() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____updateCallback_12)); }
	inline Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * get__updateCallback_12() const { return ____updateCallback_12; }
	inline Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B ** get_address_of__updateCallback_12() { return &____updateCallback_12; }
	inline void set__updateCallback_12(Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * value)
	{
		____updateCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&____updateCallback_12), value);
	}

	inline static int32_t get_offset_of__isFocusedCallback_13() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____isFocusedCallback_13)); }
	inline Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * get__isFocusedCallback_13() const { return ____isFocusedCallback_13; }
	inline Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 ** get_address_of__isFocusedCallback_13() { return &____isFocusedCallback_13; }
	inline void set__isFocusedCallback_13(Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * value)
	{
		____isFocusedCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&____isFocusedCallback_13), value);
	}

	inline static int32_t get_offset_of__timer_14() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____timer_14)); }
	inline Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D * get__timer_14() const { return ____timer_14; }
	inline Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D ** get_address_of__timer_14() { return &____timer_14; }
	inline void set__timer_14(Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D * value)
	{
		____timer_14 = value;
		Il2CppCodeGenWriteBarrier((&____timer_14), value);
	}

	inline static int32_t get_offset_of__canvasGroup_15() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____canvasGroup_15)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get__canvasGroup_15() const { return ____canvasGroup_15; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of__canvasGroup_15() { return &____canvasGroup_15; }
	inline void set__canvasGroup_15(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		____canvasGroup_15 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_15), value);
	}

	inline static int32_t get_offset_of_cancelCallback_16() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ___cancelCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_cancelCallback_16() const { return ___cancelCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_cancelCallback_16() { return &___cancelCallback_16; }
	inline void set_cancelCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___cancelCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___cancelCallback_16), value);
	}

	inline static int32_t get_offset_of_lastUISelection_17() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ___lastUISelection_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_lastUISelection_17() const { return ___lastUISelection_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_lastUISelection_17() { return &___lastUISelection_17; }
	inline void set_lastUISelection_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___lastUISelection_17 = value;
		Il2CppCodeGenWriteBarrier((&___lastUISelection_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOW_T05E229BCE867863C1A33354186F3E7641707AAA5_H
#ifndef BENCHMARK01_T375270EB369DC01A4C37A6381970857C4BD87761_H
#define BENCHMARK01_T375270EB369DC01A4C37A6381970857C4BD87761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01
struct  Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 TMPro.Examples.Benchmark01::BenchmarkType
	int32_t ___BenchmarkType_4;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01::TMProFont
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___TMProFont_5;
	// UnityEngine.Font TMPro.Examples.Benchmark01::TextMeshFont
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ___TextMeshFont_6;
	// TMPro.TextMeshPro TMPro.Examples.Benchmark01::m_textMeshPro
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * ___m_textMeshPro_7;
	// TMPro.TextContainer TMPro.Examples.Benchmark01::m_textContainer
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE * ___m_textContainer_8;
	// UnityEngine.TextMesh TMPro.Examples.Benchmark01::m_textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___m_textMesh_9;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material01
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_material01_12;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material02
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_material02_13;

public:
	inline static int32_t get_offset_of_BenchmarkType_4() { return static_cast<int32_t>(offsetof(Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761, ___BenchmarkType_4)); }
	inline int32_t get_BenchmarkType_4() const { return ___BenchmarkType_4; }
	inline int32_t* get_address_of_BenchmarkType_4() { return &___BenchmarkType_4; }
	inline void set_BenchmarkType_4(int32_t value)
	{
		___BenchmarkType_4 = value;
	}

	inline static int32_t get_offset_of_TMProFont_5() { return static_cast<int32_t>(offsetof(Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761, ___TMProFont_5)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_TMProFont_5() const { return ___TMProFont_5; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_TMProFont_5() { return &___TMProFont_5; }
	inline void set_TMProFont_5(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___TMProFont_5 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_5), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_6() { return static_cast<int32_t>(offsetof(Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761, ___TextMeshFont_6)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get_TextMeshFont_6() const { return ___TextMeshFont_6; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of_TextMeshFont_6() { return &___TextMeshFont_6; }
	inline void set_TextMeshFont_6(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		___TextMeshFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_6), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_7() { return static_cast<int32_t>(offsetof(Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761, ___m_textMeshPro_7)); }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * get_m_textMeshPro_7() const { return ___m_textMeshPro_7; }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 ** get_address_of_m_textMeshPro_7() { return &___m_textMeshPro_7; }
	inline void set_m_textMeshPro_7(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * value)
	{
		___m_textMeshPro_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_7), value);
	}

	inline static int32_t get_offset_of_m_textContainer_8() { return static_cast<int32_t>(offsetof(Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761, ___m_textContainer_8)); }
	inline TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE * get_m_textContainer_8() const { return ___m_textContainer_8; }
	inline TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE ** get_address_of_m_textContainer_8() { return &___m_textContainer_8; }
	inline void set_m_textContainer_8(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE * value)
	{
		___m_textContainer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_8), value);
	}

	inline static int32_t get_offset_of_m_textMesh_9() { return static_cast<int32_t>(offsetof(Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761, ___m_textMesh_9)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_m_textMesh_9() const { return ___m_textMesh_9; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_m_textMesh_9() { return &___m_textMesh_9; }
	inline void set_m_textMesh_9(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___m_textMesh_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_9), value);
	}

	inline static int32_t get_offset_of_m_material01_12() { return static_cast<int32_t>(offsetof(Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761, ___m_material01_12)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_material01_12() const { return ___m_material01_12; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_material01_12() { return &___m_material01_12; }
	inline void set_m_material01_12(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_material01_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_12), value);
	}

	inline static int32_t get_offset_of_m_material02_13() { return static_cast<int32_t>(offsetof(Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761, ___m_material02_13)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_material02_13() const { return ___m_material02_13; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_material02_13() { return &___m_material02_13; }
	inline void set_m_material02_13(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_material02_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_T375270EB369DC01A4C37A6381970857C4BD87761_H
#ifndef BENCHMARK01_UGUI_TDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55_H
#define BENCHMARK01_UGUI_TDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI
struct  Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI::BenchmarkType
	int32_t ___BenchmarkType_4;
	// UnityEngine.Canvas TMPro.Examples.Benchmark01_UGUI::canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___canvas_5;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01_UGUI::TMProFont
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___TMProFont_6;
	// UnityEngine.Font TMPro.Examples.Benchmark01_UGUI::TextMeshFont
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ___TextMeshFont_7;
	// TMPro.TextMeshProUGUI TMPro.Examples.Benchmark01_UGUI::m_textMeshPro
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___m_textMeshPro_8;
	// UnityEngine.UI.Text TMPro.Examples.Benchmark01_UGUI::m_textMesh
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_textMesh_9;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material01
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_material01_12;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material02
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_material02_13;

public:
	inline static int32_t get_offset_of_BenchmarkType_4() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55, ___BenchmarkType_4)); }
	inline int32_t get_BenchmarkType_4() const { return ___BenchmarkType_4; }
	inline int32_t* get_address_of_BenchmarkType_4() { return &___BenchmarkType_4; }
	inline void set_BenchmarkType_4(int32_t value)
	{
		___BenchmarkType_4 = value;
	}

	inline static int32_t get_offset_of_canvas_5() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55, ___canvas_5)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_canvas_5() const { return ___canvas_5; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_canvas_5() { return &___canvas_5; }
	inline void set_canvas_5(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___canvas_5 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_5), value);
	}

	inline static int32_t get_offset_of_TMProFont_6() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55, ___TMProFont_6)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_TMProFont_6() const { return ___TMProFont_6; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_TMProFont_6() { return &___TMProFont_6; }
	inline void set_TMProFont_6(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___TMProFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_6), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_7() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55, ___TextMeshFont_7)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get_TextMeshFont_7() const { return ___TextMeshFont_7; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of_TextMeshFont_7() { return &___TextMeshFont_7; }
	inline void set_TextMeshFont_7(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		___TextMeshFont_7 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_7), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_8() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55, ___m_textMeshPro_8)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_m_textMeshPro_8() const { return ___m_textMeshPro_8; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_m_textMeshPro_8() { return &___m_textMeshPro_8; }
	inline void set_m_textMeshPro_8(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___m_textMeshPro_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_8), value);
	}

	inline static int32_t get_offset_of_m_textMesh_9() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55, ___m_textMesh_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_textMesh_9() const { return ___m_textMesh_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_textMesh_9() { return &___m_textMesh_9; }
	inline void set_m_textMesh_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_textMesh_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_9), value);
	}

	inline static int32_t get_offset_of_m_material01_12() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55, ___m_material01_12)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_material01_12() const { return ___m_material01_12; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_material01_12() { return &___m_material01_12; }
	inline void set_m_material01_12(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_material01_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_12), value);
	}

	inline static int32_t get_offset_of_m_material02_13() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55, ___m_material02_13)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_material02_13() const { return ___m_material02_13; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_material02_13() { return &___m_material02_13; }
	inline void set_m_material02_13(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_material02_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_UGUI_TDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55_H
#ifndef BENCHMARK02_T5AD0E34D131A57AAA8B39CED7AC8E60946686BB5_H
#define BENCHMARK02_T5AD0E34D131A57AAA8B39CED7AC8E60946686BB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark02
struct  Benchmark02_t5AD0E34D131A57AAA8B39CED7AC8E60946686BB5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 TMPro.Examples.Benchmark02::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark02::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.Benchmark02::floatingText_Script
	TextMeshProFloatingText_tD52C5B081FC480F24349C2C0F5099F0693EEC913 * ___floatingText_Script_6;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark02_t5AD0E34D131A57AAA8B39CED7AC8E60946686BB5, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(Benchmark02_t5AD0E34D131A57AAA8B39CED7AC8E60946686BB5, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_floatingText_Script_6() { return static_cast<int32_t>(offsetof(Benchmark02_t5AD0E34D131A57AAA8B39CED7AC8E60946686BB5, ___floatingText_Script_6)); }
	inline TextMeshProFloatingText_tD52C5B081FC480F24349C2C0F5099F0693EEC913 * get_floatingText_Script_6() const { return ___floatingText_Script_6; }
	inline TextMeshProFloatingText_tD52C5B081FC480F24349C2C0F5099F0693EEC913 ** get_address_of_floatingText_Script_6() { return &___floatingText_Script_6; }
	inline void set_floatingText_Script_6(TextMeshProFloatingText_tD52C5B081FC480F24349C2C0F5099F0693EEC913 * value)
	{
		___floatingText_Script_6 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK02_T5AD0E34D131A57AAA8B39CED7AC8E60946686BB5_H
#ifndef BENCHMARK03_TC3AB21ABCF4386EC933960A70AE20001D6D3CD87_H
#define BENCHMARK03_TC3AB21ABCF4386EC933960A70AE20001D6D3CD87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark03
struct  Benchmark03_tC3AB21ABCF4386EC933960A70AE20001D6D3CD87  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 TMPro.Examples.Benchmark03::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark03::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// UnityEngine.Font TMPro.Examples.Benchmark03::TheFont
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ___TheFont_6;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark03_tC3AB21ABCF4386EC933960A70AE20001D6D3CD87, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(Benchmark03_tC3AB21ABCF4386EC933960A70AE20001D6D3CD87, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_TheFont_6() { return static_cast<int32_t>(offsetof(Benchmark03_tC3AB21ABCF4386EC933960A70AE20001D6D3CD87, ___TheFont_6)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get_TheFont_6() const { return ___TheFont_6; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of_TheFont_6() { return &___TheFont_6; }
	inline void set_TheFont_6(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		___TheFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK03_TC3AB21ABCF4386EC933960A70AE20001D6D3CD87_H
#ifndef BENCHMARK04_T428B8F183784D591AAEAD632C4C56D6C227BD6D9_H
#define BENCHMARK04_T428B8F183784D591AAEAD632C4C56D6C227BD6D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark04
struct  Benchmark04_t428B8F183784D591AAEAD632C4C56D6C227BD6D9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 TMPro.Examples.Benchmark04::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark04::MinPointSize
	int32_t ___MinPointSize_5;
	// System.Int32 TMPro.Examples.Benchmark04::MaxPointSize
	int32_t ___MaxPointSize_6;
	// System.Int32 TMPro.Examples.Benchmark04::Steps
	int32_t ___Steps_7;
	// UnityEngine.Transform TMPro.Examples.Benchmark04::m_Transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_Transform_8;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark04_t428B8F183784D591AAEAD632C4C56D6C227BD6D9, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_MinPointSize_5() { return static_cast<int32_t>(offsetof(Benchmark04_t428B8F183784D591AAEAD632C4C56D6C227BD6D9, ___MinPointSize_5)); }
	inline int32_t get_MinPointSize_5() const { return ___MinPointSize_5; }
	inline int32_t* get_address_of_MinPointSize_5() { return &___MinPointSize_5; }
	inline void set_MinPointSize_5(int32_t value)
	{
		___MinPointSize_5 = value;
	}

	inline static int32_t get_offset_of_MaxPointSize_6() { return static_cast<int32_t>(offsetof(Benchmark04_t428B8F183784D591AAEAD632C4C56D6C227BD6D9, ___MaxPointSize_6)); }
	inline int32_t get_MaxPointSize_6() const { return ___MaxPointSize_6; }
	inline int32_t* get_address_of_MaxPointSize_6() { return &___MaxPointSize_6; }
	inline void set_MaxPointSize_6(int32_t value)
	{
		___MaxPointSize_6 = value;
	}

	inline static int32_t get_offset_of_Steps_7() { return static_cast<int32_t>(offsetof(Benchmark04_t428B8F183784D591AAEAD632C4C56D6C227BD6D9, ___Steps_7)); }
	inline int32_t get_Steps_7() const { return ___Steps_7; }
	inline int32_t* get_address_of_Steps_7() { return &___Steps_7; }
	inline void set_Steps_7(int32_t value)
	{
		___Steps_7 = value;
	}

	inline static int32_t get_offset_of_m_Transform_8() { return static_cast<int32_t>(offsetof(Benchmark04_t428B8F183784D591AAEAD632C4C56D6C227BD6D9, ___m_Transform_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_Transform_8() const { return ___m_Transform_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_Transform_8() { return &___m_Transform_8; }
	inline void set_m_Transform_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_Transform_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK04_T428B8F183784D591AAEAD632C4C56D6C227BD6D9_H
#ifndef CAMERACONTROLLER_T1B35987C4C1AC6395807F1CCC998C5ED956D7911_H
#define CAMERACONTROLLER_T1B35987C4C1AC6395807F1CCC998C5ED956D7911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController
struct  CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform TMPro.Examples.CameraController::cameraTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cameraTransform_4;
	// UnityEngine.Transform TMPro.Examples.CameraController::dummyTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___dummyTarget_5;
	// UnityEngine.Transform TMPro.Examples.CameraController::CameraTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___CameraTarget_6;
	// System.Single TMPro.Examples.CameraController::FollowDistance
	float ___FollowDistance_7;
	// System.Single TMPro.Examples.CameraController::MaxFollowDistance
	float ___MaxFollowDistance_8;
	// System.Single TMPro.Examples.CameraController::MinFollowDistance
	float ___MinFollowDistance_9;
	// System.Single TMPro.Examples.CameraController::ElevationAngle
	float ___ElevationAngle_10;
	// System.Single TMPro.Examples.CameraController::MaxElevationAngle
	float ___MaxElevationAngle_11;
	// System.Single TMPro.Examples.CameraController::MinElevationAngle
	float ___MinElevationAngle_12;
	// System.Single TMPro.Examples.CameraController::OrbitalAngle
	float ___OrbitalAngle_13;
	// TMPro.Examples.CameraController_CameraModes TMPro.Examples.CameraController::CameraMode
	int32_t ___CameraMode_14;
	// System.Boolean TMPro.Examples.CameraController::MovementSmoothing
	bool ___MovementSmoothing_15;
	// System.Boolean TMPro.Examples.CameraController::RotationSmoothing
	bool ___RotationSmoothing_16;
	// System.Boolean TMPro.Examples.CameraController::previousSmoothing
	bool ___previousSmoothing_17;
	// System.Single TMPro.Examples.CameraController::MovementSmoothingValue
	float ___MovementSmoothingValue_18;
	// System.Single TMPro.Examples.CameraController::RotationSmoothingValue
	float ___RotationSmoothingValue_19;
	// System.Single TMPro.Examples.CameraController::MoveSensitivity
	float ___MoveSensitivity_20;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::currentVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentVelocity_21;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::desiredPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___desiredPosition_22;
	// System.Single TMPro.Examples.CameraController::mouseX
	float ___mouseX_23;
	// System.Single TMPro.Examples.CameraController::mouseY
	float ___mouseY_24;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::moveVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___moveVector_25;
	// System.Single TMPro.Examples.CameraController::mouseWheel
	float ___mouseWheel_26;

public:
	inline static int32_t get_offset_of_cameraTransform_4() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___cameraTransform_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_cameraTransform_4() const { return ___cameraTransform_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_cameraTransform_4() { return &___cameraTransform_4; }
	inline void set_cameraTransform_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___cameraTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_4), value);
	}

	inline static int32_t get_offset_of_dummyTarget_5() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___dummyTarget_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_dummyTarget_5() const { return ___dummyTarget_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_dummyTarget_5() { return &___dummyTarget_5; }
	inline void set_dummyTarget_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___dummyTarget_5 = value;
		Il2CppCodeGenWriteBarrier((&___dummyTarget_5), value);
	}

	inline static int32_t get_offset_of_CameraTarget_6() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___CameraTarget_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_CameraTarget_6() const { return ___CameraTarget_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_CameraTarget_6() { return &___CameraTarget_6; }
	inline void set_CameraTarget_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___CameraTarget_6 = value;
		Il2CppCodeGenWriteBarrier((&___CameraTarget_6), value);
	}

	inline static int32_t get_offset_of_FollowDistance_7() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___FollowDistance_7)); }
	inline float get_FollowDistance_7() const { return ___FollowDistance_7; }
	inline float* get_address_of_FollowDistance_7() { return &___FollowDistance_7; }
	inline void set_FollowDistance_7(float value)
	{
		___FollowDistance_7 = value;
	}

	inline static int32_t get_offset_of_MaxFollowDistance_8() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___MaxFollowDistance_8)); }
	inline float get_MaxFollowDistance_8() const { return ___MaxFollowDistance_8; }
	inline float* get_address_of_MaxFollowDistance_8() { return &___MaxFollowDistance_8; }
	inline void set_MaxFollowDistance_8(float value)
	{
		___MaxFollowDistance_8 = value;
	}

	inline static int32_t get_offset_of_MinFollowDistance_9() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___MinFollowDistance_9)); }
	inline float get_MinFollowDistance_9() const { return ___MinFollowDistance_9; }
	inline float* get_address_of_MinFollowDistance_9() { return &___MinFollowDistance_9; }
	inline void set_MinFollowDistance_9(float value)
	{
		___MinFollowDistance_9 = value;
	}

	inline static int32_t get_offset_of_ElevationAngle_10() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___ElevationAngle_10)); }
	inline float get_ElevationAngle_10() const { return ___ElevationAngle_10; }
	inline float* get_address_of_ElevationAngle_10() { return &___ElevationAngle_10; }
	inline void set_ElevationAngle_10(float value)
	{
		___ElevationAngle_10 = value;
	}

	inline static int32_t get_offset_of_MaxElevationAngle_11() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___MaxElevationAngle_11)); }
	inline float get_MaxElevationAngle_11() const { return ___MaxElevationAngle_11; }
	inline float* get_address_of_MaxElevationAngle_11() { return &___MaxElevationAngle_11; }
	inline void set_MaxElevationAngle_11(float value)
	{
		___MaxElevationAngle_11 = value;
	}

	inline static int32_t get_offset_of_MinElevationAngle_12() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___MinElevationAngle_12)); }
	inline float get_MinElevationAngle_12() const { return ___MinElevationAngle_12; }
	inline float* get_address_of_MinElevationAngle_12() { return &___MinElevationAngle_12; }
	inline void set_MinElevationAngle_12(float value)
	{
		___MinElevationAngle_12 = value;
	}

	inline static int32_t get_offset_of_OrbitalAngle_13() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___OrbitalAngle_13)); }
	inline float get_OrbitalAngle_13() const { return ___OrbitalAngle_13; }
	inline float* get_address_of_OrbitalAngle_13() { return &___OrbitalAngle_13; }
	inline void set_OrbitalAngle_13(float value)
	{
		___OrbitalAngle_13 = value;
	}

	inline static int32_t get_offset_of_CameraMode_14() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___CameraMode_14)); }
	inline int32_t get_CameraMode_14() const { return ___CameraMode_14; }
	inline int32_t* get_address_of_CameraMode_14() { return &___CameraMode_14; }
	inline void set_CameraMode_14(int32_t value)
	{
		___CameraMode_14 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothing_15() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___MovementSmoothing_15)); }
	inline bool get_MovementSmoothing_15() const { return ___MovementSmoothing_15; }
	inline bool* get_address_of_MovementSmoothing_15() { return &___MovementSmoothing_15; }
	inline void set_MovementSmoothing_15(bool value)
	{
		___MovementSmoothing_15 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothing_16() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___RotationSmoothing_16)); }
	inline bool get_RotationSmoothing_16() const { return ___RotationSmoothing_16; }
	inline bool* get_address_of_RotationSmoothing_16() { return &___RotationSmoothing_16; }
	inline void set_RotationSmoothing_16(bool value)
	{
		___RotationSmoothing_16 = value;
	}

	inline static int32_t get_offset_of_previousSmoothing_17() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___previousSmoothing_17)); }
	inline bool get_previousSmoothing_17() const { return ___previousSmoothing_17; }
	inline bool* get_address_of_previousSmoothing_17() { return &___previousSmoothing_17; }
	inline void set_previousSmoothing_17(bool value)
	{
		___previousSmoothing_17 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothingValue_18() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___MovementSmoothingValue_18)); }
	inline float get_MovementSmoothingValue_18() const { return ___MovementSmoothingValue_18; }
	inline float* get_address_of_MovementSmoothingValue_18() { return &___MovementSmoothingValue_18; }
	inline void set_MovementSmoothingValue_18(float value)
	{
		___MovementSmoothingValue_18 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothingValue_19() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___RotationSmoothingValue_19)); }
	inline float get_RotationSmoothingValue_19() const { return ___RotationSmoothingValue_19; }
	inline float* get_address_of_RotationSmoothingValue_19() { return &___RotationSmoothingValue_19; }
	inline void set_RotationSmoothingValue_19(float value)
	{
		___RotationSmoothingValue_19 = value;
	}

	inline static int32_t get_offset_of_MoveSensitivity_20() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___MoveSensitivity_20)); }
	inline float get_MoveSensitivity_20() const { return ___MoveSensitivity_20; }
	inline float* get_address_of_MoveSensitivity_20() { return &___MoveSensitivity_20; }
	inline void set_MoveSensitivity_20(float value)
	{
		___MoveSensitivity_20 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_21() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___currentVelocity_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_currentVelocity_21() const { return ___currentVelocity_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_currentVelocity_21() { return &___currentVelocity_21; }
	inline void set_currentVelocity_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___currentVelocity_21 = value;
	}

	inline static int32_t get_offset_of_desiredPosition_22() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___desiredPosition_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_desiredPosition_22() const { return ___desiredPosition_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_desiredPosition_22() { return &___desiredPosition_22; }
	inline void set_desiredPosition_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___desiredPosition_22 = value;
	}

	inline static int32_t get_offset_of_mouseX_23() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___mouseX_23)); }
	inline float get_mouseX_23() const { return ___mouseX_23; }
	inline float* get_address_of_mouseX_23() { return &___mouseX_23; }
	inline void set_mouseX_23(float value)
	{
		___mouseX_23 = value;
	}

	inline static int32_t get_offset_of_mouseY_24() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___mouseY_24)); }
	inline float get_mouseY_24() const { return ___mouseY_24; }
	inline float* get_address_of_mouseY_24() { return &___mouseY_24; }
	inline void set_mouseY_24(float value)
	{
		___mouseY_24 = value;
	}

	inline static int32_t get_offset_of_moveVector_25() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___moveVector_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_moveVector_25() const { return ___moveVector_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_moveVector_25() { return &___moveVector_25; }
	inline void set_moveVector_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___moveVector_25 = value;
	}

	inline static int32_t get_offset_of_mouseWheel_26() { return static_cast<int32_t>(offsetof(CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911, ___mouseWheel_26)); }
	inline float get_mouseWheel_26() const { return ___mouseWheel_26; }
	inline float* get_address_of_mouseWheel_26() { return &___mouseWheel_26; }
	inline void set_mouseWheel_26(float value)
	{
		___mouseWheel_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T1B35987C4C1AC6395807F1CCC998C5ED956D7911_H
#ifndef OBJECTSPIN_T5EEBF4BFCCD478B89C227B8F04670C937B7E10B5_H
#define OBJECTSPIN_T5EEBF4BFCCD478B89C227B8F04670C937B7E10B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin
struct  ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single TMPro.Examples.ObjectSpin::SpinSpeed
	float ___SpinSpeed_4;
	// System.Int32 TMPro.Examples.ObjectSpin::RotationRange
	int32_t ___RotationRange_5;
	// UnityEngine.Transform TMPro.Examples.ObjectSpin::m_transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_transform_6;
	// System.Single TMPro.Examples.ObjectSpin::m_time
	float ___m_time_7;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_prevPOS
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_prevPOS_8;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Rotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_initial_Rotation_9;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_initial_Position_10;
	// UnityEngine.Color32 TMPro.Examples.ObjectSpin::m_lightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_lightColor_11;
	// System.Int32 TMPro.Examples.ObjectSpin::frames
	int32_t ___frames_12;
	// TMPro.Examples.ObjectSpin_MotionType TMPro.Examples.ObjectSpin::Motion
	int32_t ___Motion_13;

public:
	inline static int32_t get_offset_of_SpinSpeed_4() { return static_cast<int32_t>(offsetof(ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5, ___SpinSpeed_4)); }
	inline float get_SpinSpeed_4() const { return ___SpinSpeed_4; }
	inline float* get_address_of_SpinSpeed_4() { return &___SpinSpeed_4; }
	inline void set_SpinSpeed_4(float value)
	{
		___SpinSpeed_4 = value;
	}

	inline static int32_t get_offset_of_RotationRange_5() { return static_cast<int32_t>(offsetof(ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5, ___RotationRange_5)); }
	inline int32_t get_RotationRange_5() const { return ___RotationRange_5; }
	inline int32_t* get_address_of_RotationRange_5() { return &___RotationRange_5; }
	inline void set_RotationRange_5(int32_t value)
	{
		___RotationRange_5 = value;
	}

	inline static int32_t get_offset_of_m_transform_6() { return static_cast<int32_t>(offsetof(ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5, ___m_transform_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_transform_6() const { return ___m_transform_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_transform_6() { return &___m_transform_6; }
	inline void set_m_transform_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_6), value);
	}

	inline static int32_t get_offset_of_m_time_7() { return static_cast<int32_t>(offsetof(ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5, ___m_time_7)); }
	inline float get_m_time_7() const { return ___m_time_7; }
	inline float* get_address_of_m_time_7() { return &___m_time_7; }
	inline void set_m_time_7(float value)
	{
		___m_time_7 = value;
	}

	inline static int32_t get_offset_of_m_prevPOS_8() { return static_cast<int32_t>(offsetof(ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5, ___m_prevPOS_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_prevPOS_8() const { return ___m_prevPOS_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_prevPOS_8() { return &___m_prevPOS_8; }
	inline void set_m_prevPOS_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_prevPOS_8 = value;
	}

	inline static int32_t get_offset_of_m_initial_Rotation_9() { return static_cast<int32_t>(offsetof(ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5, ___m_initial_Rotation_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_initial_Rotation_9() const { return ___m_initial_Rotation_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_initial_Rotation_9() { return &___m_initial_Rotation_9; }
	inline void set_m_initial_Rotation_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_initial_Rotation_9 = value;
	}

	inline static int32_t get_offset_of_m_initial_Position_10() { return static_cast<int32_t>(offsetof(ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5, ___m_initial_Position_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_initial_Position_10() const { return ___m_initial_Position_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_initial_Position_10() { return &___m_initial_Position_10; }
	inline void set_m_initial_Position_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_initial_Position_10 = value;
	}

	inline static int32_t get_offset_of_m_lightColor_11() { return static_cast<int32_t>(offsetof(ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5, ___m_lightColor_11)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_lightColor_11() const { return ___m_lightColor_11; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_lightColor_11() { return &___m_lightColor_11; }
	inline void set_m_lightColor_11(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_lightColor_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5, ___frames_12)); }
	inline int32_t get_frames_12() const { return ___frames_12; }
	inline int32_t* get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(int32_t value)
	{
		___frames_12 = value;
	}

	inline static int32_t get_offset_of_Motion_13() { return static_cast<int32_t>(offsetof(ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5, ___Motion_13)); }
	inline int32_t get_Motion_13() const { return ___Motion_13; }
	inline int32_t* get_address_of_Motion_13() { return &___Motion_13; }
	inline void set_Motion_13(int32_t value)
	{
		___Motion_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPIN_T5EEBF4BFCCD478B89C227B8F04670C937B7E10B5_H
#ifndef SHADERPROPANIMATOR_TBD6FBF3230284F283E6BC35E93E5A260853D5A72_H
#define SHADERPROPANIMATOR_TBD6FBF3230284F283E6BC35E93E5A260853D5A72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator
struct  ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Renderer TMPro.Examples.ShaderPropAnimator::m_Renderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___m_Renderer_4;
	// UnityEngine.Material TMPro.Examples.ShaderPropAnimator::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_5;
	// UnityEngine.AnimationCurve TMPro.Examples.ShaderPropAnimator::GlowCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___GlowCurve_6;
	// System.Single TMPro.Examples.ShaderPropAnimator::m_frame
	float ___m_frame_7;

public:
	inline static int32_t get_offset_of_m_Renderer_4() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72, ___m_Renderer_4)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_m_Renderer_4() const { return ___m_Renderer_4; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_m_Renderer_4() { return &___m_Renderer_4; }
	inline void set_m_Renderer_4(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___m_Renderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_4), value);
	}

	inline static int32_t get_offset_of_m_Material_5() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72, ___m_Material_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_5() const { return ___m_Material_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_5() { return &___m_Material_5; }
	inline void set_m_Material_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_5), value);
	}

	inline static int32_t get_offset_of_GlowCurve_6() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72, ___GlowCurve_6)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_GlowCurve_6() const { return ___GlowCurve_6; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_GlowCurve_6() { return &___GlowCurve_6; }
	inline void set_GlowCurve_6(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___GlowCurve_6 = value;
		Il2CppCodeGenWriteBarrier((&___GlowCurve_6), value);
	}

	inline static int32_t get_offset_of_m_frame_7() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72, ___m_frame_7)); }
	inline float get_m_frame_7() const { return ___m_frame_7; }
	inline float* get_address_of_m_frame_7() { return &___m_frame_7; }
	inline void set_m_frame_7(float value)
	{
		___m_frame_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERPROPANIMATOR_TBD6FBF3230284F283E6BC35E93E5A260853D5A72_H
#ifndef SIMPLESCRIPT_TBCE5C1CADFA47423C376344DE84BC0C57CEEC844_H
#define SIMPLESCRIPT_TBCE5C1CADFA47423C376344DE84BC0C57CEEC844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SimpleScript
struct  SimpleScript_tBCE5C1CADFA47423C376344DE84BC0C57CEEC844  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TextMeshPro TMPro.Examples.SimpleScript::m_textMeshPro
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * ___m_textMeshPro_4;
	// System.Single TMPro.Examples.SimpleScript::m_frame
	float ___m_frame_6;

public:
	inline static int32_t get_offset_of_m_textMeshPro_4() { return static_cast<int32_t>(offsetof(SimpleScript_tBCE5C1CADFA47423C376344DE84BC0C57CEEC844, ___m_textMeshPro_4)); }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * get_m_textMeshPro_4() const { return ___m_textMeshPro_4; }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 ** get_address_of_m_textMeshPro_4() { return &___m_textMeshPro_4; }
	inline void set_m_textMeshPro_4(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * value)
	{
		___m_textMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_4), value);
	}

	inline static int32_t get_offset_of_m_frame_6() { return static_cast<int32_t>(offsetof(SimpleScript_tBCE5C1CADFA47423C376344DE84BC0C57CEEC844, ___m_frame_6)); }
	inline float get_m_frame_6() const { return ___m_frame_6; }
	inline float* get_address_of_m_frame_6() { return &___m_frame_6; }
	inline void set_m_frame_6(float value)
	{
		___m_frame_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESCRIPT_TBCE5C1CADFA47423C376344DE84BC0C57CEEC844_H
#ifndef TMP_TEXTEVENTHANDLER_T6046295E0F06C5138916541DBE6B476E6FE66B54_H
#define TMP_TEXTEVENTHANDLER_T6046295E0F06C5138916541DBE6B476E6FE66B54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler
struct  TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TMP_TextEventHandler_CharacterSelectionEvent TMPro.TMP_TextEventHandler::m_OnCharacterSelection
	CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90 * ___m_OnCharacterSelection_4;
	// TMPro.TMP_TextEventHandler_SpriteSelectionEvent TMPro.TMP_TextEventHandler::m_OnSpriteSelection
	SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32 * ___m_OnSpriteSelection_5;
	// TMPro.TMP_TextEventHandler_WordSelectionEvent TMPro.TMP_TextEventHandler::m_OnWordSelection
	WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554 * ___m_OnWordSelection_6;
	// TMPro.TMP_TextEventHandler_LineSelectionEvent TMPro.TMP_TextEventHandler::m_OnLineSelection
	LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F * ___m_OnLineSelection_7;
	// TMPro.TMP_TextEventHandler_LinkSelectionEvent TMPro.TMP_TextEventHandler::m_OnLinkSelection
	LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8 * ___m_OnLinkSelection_8;
	// TMPro.TMP_Text TMPro.TMP_TextEventHandler::m_TextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_TextComponent_9;
	// UnityEngine.Camera TMPro.TMP_TextEventHandler::m_Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_Camera_10;
	// UnityEngine.Canvas TMPro.TMP_TextEventHandler::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Int32 TMPro.TMP_TextEventHandler::m_selectedLink
	int32_t ___m_selectedLink_12;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastCharIndex
	int32_t ___m_lastCharIndex_13;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastWordIndex
	int32_t ___m_lastWordIndex_14;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastLineIndex
	int32_t ___m_lastLineIndex_15;

public:
	inline static int32_t get_offset_of_m_OnCharacterSelection_4() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_OnCharacterSelection_4)); }
	inline CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90 * get_m_OnCharacterSelection_4() const { return ___m_OnCharacterSelection_4; }
	inline CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90 ** get_address_of_m_OnCharacterSelection_4() { return &___m_OnCharacterSelection_4; }
	inline void set_m_OnCharacterSelection_4(CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90 * value)
	{
		___m_OnCharacterSelection_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCharacterSelection_4), value);
	}

	inline static int32_t get_offset_of_m_OnSpriteSelection_5() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_OnSpriteSelection_5)); }
	inline SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32 * get_m_OnSpriteSelection_5() const { return ___m_OnSpriteSelection_5; }
	inline SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32 ** get_address_of_m_OnSpriteSelection_5() { return &___m_OnSpriteSelection_5; }
	inline void set_m_OnSpriteSelection_5(SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32 * value)
	{
		___m_OnSpriteSelection_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSpriteSelection_5), value);
	}

	inline static int32_t get_offset_of_m_OnWordSelection_6() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_OnWordSelection_6)); }
	inline WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554 * get_m_OnWordSelection_6() const { return ___m_OnWordSelection_6; }
	inline WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554 ** get_address_of_m_OnWordSelection_6() { return &___m_OnWordSelection_6; }
	inline void set_m_OnWordSelection_6(WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554 * value)
	{
		___m_OnWordSelection_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnWordSelection_6), value);
	}

	inline static int32_t get_offset_of_m_OnLineSelection_7() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_OnLineSelection_7)); }
	inline LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F * get_m_OnLineSelection_7() const { return ___m_OnLineSelection_7; }
	inline LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F ** get_address_of_m_OnLineSelection_7() { return &___m_OnLineSelection_7; }
	inline void set_m_OnLineSelection_7(LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F * value)
	{
		___m_OnLineSelection_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLineSelection_7), value);
	}

	inline static int32_t get_offset_of_m_OnLinkSelection_8() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_OnLinkSelection_8)); }
	inline LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8 * get_m_OnLinkSelection_8() const { return ___m_OnLinkSelection_8; }
	inline LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8 ** get_address_of_m_OnLinkSelection_8() { return &___m_OnLinkSelection_8; }
	inline void set_m_OnLinkSelection_8(LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8 * value)
	{
		___m_OnLinkSelection_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLinkSelection_8), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_9() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_TextComponent_9)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_TextComponent_9() const { return ___m_TextComponent_9; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_TextComponent_9() { return &___m_TextComponent_9; }
	inline void set_m_TextComponent_9(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_TextComponent_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_9), value);
	}

	inline static int32_t get_offset_of_m_Camera_10() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_Camera_10)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_Camera_10() const { return ___m_Camera_10; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_Camera_10() { return &___m_Camera_10; }
	inline void set_m_Camera_10(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_Camera_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_selectedLink_12() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_selectedLink_12)); }
	inline int32_t get_m_selectedLink_12() const { return ___m_selectedLink_12; }
	inline int32_t* get_address_of_m_selectedLink_12() { return &___m_selectedLink_12; }
	inline void set_m_selectedLink_12(int32_t value)
	{
		___m_selectedLink_12 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_13() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_lastCharIndex_13)); }
	inline int32_t get_m_lastCharIndex_13() const { return ___m_lastCharIndex_13; }
	inline int32_t* get_address_of_m_lastCharIndex_13() { return &___m_lastCharIndex_13; }
	inline void set_m_lastCharIndex_13(int32_t value)
	{
		___m_lastCharIndex_13 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_14() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_lastWordIndex_14)); }
	inline int32_t get_m_lastWordIndex_14() const { return ___m_lastWordIndex_14; }
	inline int32_t* get_address_of_m_lastWordIndex_14() { return &___m_lastWordIndex_14; }
	inline void set_m_lastWordIndex_14(int32_t value)
	{
		___m_lastWordIndex_14 = value;
	}

	inline static int32_t get_offset_of_m_lastLineIndex_15() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_lastLineIndex_15)); }
	inline int32_t get_m_lastLineIndex_15() const { return ___m_lastLineIndex_15; }
	inline int32_t* get_address_of_m_lastLineIndex_15() { return &___m_lastLineIndex_15; }
	inline void set_m_lastLineIndex_15(int32_t value)
	{
		___m_lastLineIndex_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTHANDLER_T6046295E0F06C5138916541DBE6B476E6FE66B54_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef UIPOINTER_T20009A3C951AB72215997ECD66AD19EBEEC4D4AC_H
#define UIPOINTER_T20009A3C951AB72215997ECD66AD19EBEEC4D4AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Demos.UIPointer
struct  UIPointer_t20009A3C951AB72215997ECD66AD19EBEEC4D4AC  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Boolean Rewired.Demos.UIPointer::_hideHardwarePointer
	bool ____hideHardwarePointer_4;
	// System.Boolean Rewired.Demos.UIPointer::_autoSort
	bool ____autoSort_5;
	// UnityEngine.RectTransform Rewired.Demos.UIPointer::_canvasRectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____canvasRectTransform_6;

public:
	inline static int32_t get_offset_of__hideHardwarePointer_4() { return static_cast<int32_t>(offsetof(UIPointer_t20009A3C951AB72215997ECD66AD19EBEEC4D4AC, ____hideHardwarePointer_4)); }
	inline bool get__hideHardwarePointer_4() const { return ____hideHardwarePointer_4; }
	inline bool* get_address_of__hideHardwarePointer_4() { return &____hideHardwarePointer_4; }
	inline void set__hideHardwarePointer_4(bool value)
	{
		____hideHardwarePointer_4 = value;
	}

	inline static int32_t get_offset_of__autoSort_5() { return static_cast<int32_t>(offsetof(UIPointer_t20009A3C951AB72215997ECD66AD19EBEEC4D4AC, ____autoSort_5)); }
	inline bool get__autoSort_5() const { return ____autoSort_5; }
	inline bool* get_address_of__autoSort_5() { return &____autoSort_5; }
	inline void set__autoSort_5(bool value)
	{
		____autoSort_5 = value;
	}

	inline static int32_t get_offset_of__canvasRectTransform_6() { return static_cast<int32_t>(offsetof(UIPointer_t20009A3C951AB72215997ECD66AD19EBEEC4D4AC, ____canvasRectTransform_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__canvasRectTransform_6() const { return ____canvasRectTransform_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__canvasRectTransform_6() { return &____canvasRectTransform_6; }
	inline void set__canvasRectTransform_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____canvasRectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&____canvasRectTransform_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOINTER_T20009A3C951AB72215997ECD66AD19EBEEC4D4AC_H
#ifndef INPUTFIELDINFO_T0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5_H
#define INPUTFIELDINFO_T0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.InputFieldInfo
struct  InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5  : public UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA
{
public:
	// System.Int32 Rewired.UI.ControlMapper.InputFieldInfo::<actionId>k__BackingField
	int32_t ___U3CactionIdU3Ek__BackingField_8;
	// Rewired.AxisRange Rewired.UI.ControlMapper.InputFieldInfo::<axisRange>k__BackingField
	int32_t ___U3CaxisRangeU3Ek__BackingField_9;
	// System.Int32 Rewired.UI.ControlMapper.InputFieldInfo::<actionElementMapId>k__BackingField
	int32_t ___U3CactionElementMapIdU3Ek__BackingField_10;
	// Rewired.ControllerType Rewired.UI.ControlMapper.InputFieldInfo::<controllerType>k__BackingField
	int32_t ___U3CcontrollerTypeU3Ek__BackingField_11;
	// System.Int32 Rewired.UI.ControlMapper.InputFieldInfo::<controllerId>k__BackingField
	int32_t ___U3CcontrollerIdU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CactionIdU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5, ___U3CactionIdU3Ek__BackingField_8)); }
	inline int32_t get_U3CactionIdU3Ek__BackingField_8() const { return ___U3CactionIdU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CactionIdU3Ek__BackingField_8() { return &___U3CactionIdU3Ek__BackingField_8; }
	inline void set_U3CactionIdU3Ek__BackingField_8(int32_t value)
	{
		___U3CactionIdU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CaxisRangeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5, ___U3CaxisRangeU3Ek__BackingField_9)); }
	inline int32_t get_U3CaxisRangeU3Ek__BackingField_9() const { return ___U3CaxisRangeU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CaxisRangeU3Ek__BackingField_9() { return &___U3CaxisRangeU3Ek__BackingField_9; }
	inline void set_U3CaxisRangeU3Ek__BackingField_9(int32_t value)
	{
		___U3CaxisRangeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CactionElementMapIdU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5, ___U3CactionElementMapIdU3Ek__BackingField_10)); }
	inline int32_t get_U3CactionElementMapIdU3Ek__BackingField_10() const { return ___U3CactionElementMapIdU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CactionElementMapIdU3Ek__BackingField_10() { return &___U3CactionElementMapIdU3Ek__BackingField_10; }
	inline void set_U3CactionElementMapIdU3Ek__BackingField_10(int32_t value)
	{
		___U3CactionElementMapIdU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CcontrollerTypeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5, ___U3CcontrollerTypeU3Ek__BackingField_11)); }
	inline int32_t get_U3CcontrollerTypeU3Ek__BackingField_11() const { return ___U3CcontrollerTypeU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CcontrollerTypeU3Ek__BackingField_11() { return &___U3CcontrollerTypeU3Ek__BackingField_11; }
	inline void set_U3CcontrollerTypeU3Ek__BackingField_11(int32_t value)
	{
		___U3CcontrollerTypeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CcontrollerIdU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5, ___U3CcontrollerIdU3Ek__BackingField_12)); }
	inline int32_t get_U3CcontrollerIdU3Ek__BackingField_12() const { return ___U3CcontrollerIdU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CcontrollerIdU3Ek__BackingField_12() { return &___U3CcontrollerIdU3Ek__BackingField_12; }
	inline void set_U3CcontrollerIdU3Ek__BackingField_12(int32_t value)
	{
		___U3CcontrollerIdU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFIELDINFO_T0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5_H
#ifndef UISLIDERCONTROL_T08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92_H
#define UISLIDERCONTROL_T08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.UISliderControl
struct  UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92  : public UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3
{
public:
	// UnityEngine.UI.Image Rewired.UI.ControlMapper.UISliderControl::iconImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___iconImage_8;
	// UnityEngine.UI.Slider Rewired.UI.ControlMapper.UISliderControl::slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___slider_9;
	// System.Boolean Rewired.UI.ControlMapper.UISliderControl::_showIcon
	bool ____showIcon_10;
	// System.Boolean Rewired.UI.ControlMapper.UISliderControl::_showSlider
	bool ____showSlider_11;

public:
	inline static int32_t get_offset_of_iconImage_8() { return static_cast<int32_t>(offsetof(UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92, ___iconImage_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_iconImage_8() const { return ___iconImage_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_iconImage_8() { return &___iconImage_8; }
	inline void set_iconImage_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___iconImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___iconImage_8), value);
	}

	inline static int32_t get_offset_of_slider_9() { return static_cast<int32_t>(offsetof(UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92, ___slider_9)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_slider_9() const { return ___slider_9; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_slider_9() { return &___slider_9; }
	inline void set_slider_9(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___slider_9 = value;
		Il2CppCodeGenWriteBarrier((&___slider_9), value);
	}

	inline static int32_t get_offset_of__showIcon_10() { return static_cast<int32_t>(offsetof(UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92, ____showIcon_10)); }
	inline bool get__showIcon_10() const { return ____showIcon_10; }
	inline bool* get_address_of__showIcon_10() { return &____showIcon_10; }
	inline void set__showIcon_10(bool value)
	{
		____showIcon_10 = value;
	}

	inline static int32_t get_offset_of__showSlider_11() { return static_cast<int32_t>(offsetof(UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92, ____showSlider_11)); }
	inline bool get__showSlider_11() const { return ____showSlider_11; }
	inline bool* get_address_of__showSlider_11() { return &____showSlider_11; }
	inline void set__showSlider_11(bool value)
	{
		____showSlider_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERCONTROL_T08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92_H
#ifndef TOGGLEINFO_T8DA365719E98EA07FC1AC81BFE78EC1846FE8B45_H
#define TOGGLEINFO_T8DA365719E98EA07FC1AC81BFE78EC1846FE8B45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ToggleInfo
struct  ToggleInfo_t8DA365719E98EA07FC1AC81BFE78EC1846FE8B45  : public InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEINFO_T8DA365719E98EA07FC1AC81BFE78EC1846FE8B45_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400 = { sizeof (ThemedElement_t58E027D40D50175509155F319FF8B73FCCAC582D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5400[1] = 
{
	ThemedElement_t58E027D40D50175509155F319FF8B73FCCAC582D::get_offset_of__elements_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401 = { sizeof (ElementInfo_tB01A7EDE8DB4744B88FE22ECF59583DDBBA431F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5401[2] = 
{
	ElementInfo_tB01A7EDE8DB4744B88FE22ECF59583DDBBA431F7::get_offset_of__themeClass_0(),
	ElementInfo_tB01A7EDE8DB4744B88FE22ECF59583DDBBA431F7::get_offset_of__component_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402 = { sizeof (ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5402[19] = 
{
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__mainWindowBackground_4(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__popupWindowBackground_5(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__areaBackground_6(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__selectableSettings_7(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__buttonSettings_8(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__inputGridFieldSettings_9(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__scrollbarSettings_10(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__sliderSettings_11(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__invertToggle_12(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__invertToggleDisabledColor_13(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__calibrationBackground_14(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__calibrationValueMarker_15(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__calibrationRawValueMarker_16(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__calibrationZeroMarker_17(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__calibrationCalibratedZeroMarker_18(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__calibrationDeadzone_19(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__textSettings_20(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__buttonTextSettings_21(),
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF::get_offset_of__inputGridFieldTextSettings_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403 = { sizeof (SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5403[4] = 
{
	SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9::get_offset_of__transition_0(),
	SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9::get_offset_of__colors_1(),
	SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9::get_offset_of__spriteState_2(),
	SelectableSettings_Base_t76E264AC58FB9CF85893DD251ABD047352C316D9::get_offset_of__animationTriggers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404 = { sizeof (SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5404[1] = 
{
	SelectableSettings_t405B3E3D3E6F36AE5B87C0E1CCDA688FF2AC8A29::get_offset_of__imageSettings_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405 = { sizeof (SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5405[3] = 
{
	SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA::get_offset_of__handleImageSettings_4(),
	SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA::get_offset_of__fillImageSettings_5(),
	SliderSettings_t1720381297B7AD1955027301474A71FB41F571DA::get_offset_of__backgroundImageSettings_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406 = { sizeof (ScrollbarSettings_tD2F8212CF389C6CC9C87CBB7985294663B3C791E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5406[2] = 
{
	ScrollbarSettings_tD2F8212CF389C6CC9C87CBB7985294663B3C791E::get_offset_of__handleImageSettings_4(),
	ScrollbarSettings_tD2F8212CF389C6CC9C87CBB7985294663B3C791E::get_offset_of__backgroundImageSettings_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407 = { sizeof (ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5407[10] = 
{
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334::get_offset_of__color_0(),
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334::get_offset_of__sprite_1(),
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334::get_offset_of__materal_2(),
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334::get_offset_of__type_3(),
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334::get_offset_of__preserveAspect_4(),
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334::get_offset_of__fillCenter_5(),
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334::get_offset_of__fillMethod_6(),
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334::get_offset_of__fillAmout_7(),
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334::get_offset_of__fillClockwise_8(),
	ImageSettings_t2B2F106AAEF92D7897E28D313933626836660334::get_offset_of__fillOrigin_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408 = { sizeof (CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2)+ sizeof (RuntimeObject), sizeof(CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5408[8] = 
{
	CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2::get_offset_of_m_ColorMultiplier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2::get_offset_of_m_DisabledColor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2::get_offset_of_m_FadeDuration_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2::get_offset_of_m_HighlightedColor_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2::get_offset_of_m_NormalColor_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2::get_offset_of_m_PressedColor_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2::get_offset_of_m_SelectedColor_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomColorBlock_t0BB2DE1051C2719944E3C6D0F2663F16DC6850F2::get_offset_of_m_DisabledHighlightedColor_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409 = { sizeof (CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5409[5] = 
{
	CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A::get_offset_of_m_DisabledSprite_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A::get_offset_of_m_HighlightedSprite_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A::get_offset_of_m_PressedSprite_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A::get_offset_of_m_SelectedSprite_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomSpriteState_t6D5244DC97A342D95C8B3D1B9564379C210DB56A::get_offset_of_m_DisabledHighlightedSprite_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410 = { sizeof (CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5410[6] = 
{
	CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775::get_offset_of_m_DisabledTrigger_0(),
	CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775::get_offset_of_m_HighlightedTrigger_1(),
	CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775::get_offset_of_m_NormalTrigger_2(),
	CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775::get_offset_of_m_PressedTrigger_3(),
	CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775::get_offset_of_m_SelectedTrigger_4(),
	CustomAnimationTriggers_tD6D8534858DC614F75D7CDAFD5D050B6A989D775::get_offset_of_m_DisabledHighlightedTrigger_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411 = { sizeof (TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5411[5] = 
{
	TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881::get_offset_of__color_0(),
	TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881::get_offset_of__font_1(),
	TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881::get_offset_of__style_2(),
	TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881::get_offset_of__lineSpacing_3(),
	TextSettings_t6C6E84301648111BFD3DE698C0DCFADDE6855881::get_offset_of__sizeMultiplier_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412 = { sizeof (FontStyleOverride_tA66591F7413303450D9AB376077A42A8717AAC4D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5412[6] = 
{
	FontStyleOverride_tA66591F7413303450D9AB376077A42A8717AAC4D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413 = { sizeof (ToggleInfo_t8DA365719E98EA07FC1AC81BFE78EC1846FE8B45), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414 = { sizeof (UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3), -1, sizeof(UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5414[4] = 
{
	UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3::get_offset_of_title_4(),
	UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3::get_offset_of__id_5(),
	UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3::get_offset_of__showTitle_6(),
	UIControl_t4219BAE1DA8CE761B0F29A498EEE68B06D20ACA3_StaticFields::get_offset_of__uidCounter_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415 = { sizeof (UIControlSet_t0BE4C6F06B453187D70603E003EB07A252B5C083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5415[2] = 
{
	UIControlSet_t0BE4C6F06B453187D70603E003EB07A252B5C083::get_offset_of_title_4(),
	UIControlSet_t0BE4C6F06B453187D70603E003EB07A252B5C083::get_offset_of__controls_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416 = { sizeof (U3CU3Ec__DisplayClass6_0_t008ACE768E4A001876943FA94BA8FAD949138441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5416[3] = 
{
	U3CU3Ec__DisplayClass6_0_t008ACE768E4A001876943FA94BA8FAD949138441::get_offset_of_valueChangedCallback_0(),
	U3CU3Ec__DisplayClass6_0_t008ACE768E4A001876943FA94BA8FAD949138441::get_offset_of_control_1(),
	U3CU3Ec__DisplayClass6_0_t008ACE768E4A001876943FA94BA8FAD949138441::get_offset_of_cancelCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417 = { sizeof (UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5417[4] = 
{
	UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA::get_offset_of_identifier_4(),
	UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA::get_offset_of_intData_5(),
	UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA::get_offset_of_text_6(),
	UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA::get_offset_of_OnSelectedEvent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418 = { sizeof (UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5418[2] = 
{
	UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46::get_offset_of__label_4(),
	UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46::get_offset_of__content_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419 = { sizeof (UIImageHelper_tAAAEB396C88B3CB670E40DB3F5C3EB9C5F7509CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5419[3] = 
{
	UIImageHelper_tAAAEB396C88B3CB670E40DB3F5C3EB9C5F7509CA::get_offset_of_enabledState_4(),
	UIImageHelper_tAAAEB396C88B3CB670E40DB3F5C3EB9C5F7509CA::get_offset_of_disabledState_5(),
	UIImageHelper_tAAAEB396C88B3CB670E40DB3F5C3EB9C5F7509CA::get_offset_of_currentState_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420 = { sizeof (State_t58DA7EDFC97612C3E0E86571BA582D5F00193027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5420[1] = 
{
	State_t58DA7EDFC97612C3E0E86571BA582D5F00193027::get_offset_of_color_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421 = { sizeof (UISelectionUtility_tA9D5D557D510F89529406B6A3FAA73FEC05D998E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422 = { sizeof (UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5422[4] = 
{
	UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92::get_offset_of_iconImage_8(),
	UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92::get_offset_of_slider_9(),
	UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92::get_offset_of__showIcon_10(),
	UISliderControl_t08CFC6CB9BAA1D05BAAC15D47F19BCF411F68C92::get_offset_of__showSlider_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423 = { sizeof (U3CU3Ec__DisplayClass10_0_t3EBDB86D7688B23255C6D053440E7DA9DB627692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5423[1] = 
{
	U3CU3Ec__DisplayClass10_0_t3EBDB86D7688B23255C6D053440E7DA9DB627692::get_offset_of_cancelCallback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424 = { sizeof (UITools_t6184EE485E213E26F09C49D40DC3A81E739E4B21), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425 = { sizeof (Window_t05E229BCE867863C1A33354186F3E7641707AAA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5425[14] = 
{
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of_backgroundImage_4(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of_content_5(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of__initialized_6(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of__id_7(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of__rectTransform_8(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of__titleText_9(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of__contentText_10(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of__defaultUIElement_11(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of__updateCallback_12(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of__isFocusedCallback_13(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of__timer_14(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of__canvasGroup_15(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of_cancelCallback_16(),
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5::get_offset_of_lastUISelection_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426 = { sizeof (Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5426[2] = 
{
	Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D::get_offset_of__started_0(),
	Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D::get_offset_of_end_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427 = { sizeof (U3COnEnableAsyncU3Ed__64_t31291985E6CB87CFA16B38FCA85522337BDCF4FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5427[3] = 
{
	U3COnEnableAsyncU3Ed__64_t31291985E6CB87CFA16B38FCA85522337BDCF4FB::get_offset_of_U3CU3E1__state_0(),
	U3COnEnableAsyncU3Ed__64_t31291985E6CB87CFA16B38FCA85522337BDCF4FB::get_offset_of_U3CU3E2__current_1(),
	U3COnEnableAsyncU3Ed__64_t31291985E6CB87CFA16B38FCA85522337BDCF4FB::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428 = { sizeof (ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5428[23] = 
{
	0,
	0,
	0,
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_dialog_7(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_inputMapper_8(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_conflictFoundEventData_9(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_guiState_10(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_busy_11(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_pageGUIState_12(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_selectedPlayer_13(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_selectedMapCategoryId_14(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_selectedController_15(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_selectedMap_16(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_showMenu_17(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_startListening_18(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_actionScrollPos_19(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_calibrateScrollPos_20(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_actionQueue_21(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_setupFinished_22(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_initialized_23(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_isCompiling_24(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_style_wordWrap_25(),
	ControlRemappingDemo1_t9429CC0CE70734A8C94C49A1C1E0E2659B0A30C0::get_offset_of_style_centeredBox_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429 = { sizeof (ControllerSelection_t54D35C54472361116F871407E095920CDB855060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5429[4] = 
{
	ControllerSelection_t54D35C54472361116F871407E095920CDB855060::get_offset_of__id_0(),
	ControllerSelection_t54D35C54472361116F871407E095920CDB855060::get_offset_of__idPrev_1(),
	ControllerSelection_t54D35C54472361116F871407E095920CDB855060::get_offset_of__type_2(),
	ControllerSelection_t54D35C54472361116F871407E095920CDB855060::get_offset_of__typePrev_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430 = { sizeof (DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5430[11] = 
{
	0,
	0,
	DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769::get_offset_of__type_2(),
	DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769::get_offset_of__enabled_3(),
	DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769::get_offset_of__busyTime_4(),
	DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769::get_offset_of__busyTimerRunning_5(),
	DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769::get_offset_of_drawWindowDelegate_6(),
	DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769::get_offset_of_drawWindowFunction_7(),
	DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769::get_offset_of_windowProperties_8(),
	DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769::get_offset_of_currentActionId_9(),
	DialogHelper_t37B815A90FB939F50D3FD5106E83FA48C41C5769::get_offset_of_resultCallback_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431 = { sizeof (DialogType_t53EE61F8728BCDABFA5158705E217E6AC47831BE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5431[7] = 
{
	DialogType_t53EE61F8728BCDABFA5158705E217E6AC47831BE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432 = { sizeof (QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075), -1, sizeof(QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5432[5] = 
{
	QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075::get_offset_of_U3CidU3Ek__BackingField_0(),
	QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075::get_offset_of_U3CqueueActionTypeU3Ek__BackingField_1(),
	QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075::get_offset_of_U3CstateU3Ek__BackingField_2(),
	QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075::get_offset_of_U3CresponseU3Ek__BackingField_3(),
	QueueEntry_t983822D3991505A18F5AADD8D195EF9BF05E9075_StaticFields::get_offset_of_uidCounter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433 = { sizeof (State_t5004BB852A21B919F0D832BADCFF68CD47473C13)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5433[4] = 
{
	State_t5004BB852A21B919F0D832BADCFF68CD47473C13::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434 = { sizeof (JoystickAssignmentChange_t30E92AC93BA606A5BAC07757C8727DA55FD28EA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5434[3] = 
{
	JoystickAssignmentChange_t30E92AC93BA606A5BAC07757C8727DA55FD28EA3::get_offset_of_U3CplayerIdU3Ek__BackingField_5(),
	JoystickAssignmentChange_t30E92AC93BA606A5BAC07757C8727DA55FD28EA3::get_offset_of_U3CjoystickIdU3Ek__BackingField_6(),
	JoystickAssignmentChange_t30E92AC93BA606A5BAC07757C8727DA55FD28EA3::get_offset_of_U3CassignU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435 = { sizeof (ElementAssignmentChange_t49582C14BDA64F069F91C19388473C56968576AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5435[2] = 
{
	ElementAssignmentChange_t49582C14BDA64F069F91C19388473C56968576AB::get_offset_of_U3CchangeTypeU3Ek__BackingField_5(),
	ElementAssignmentChange_t49582C14BDA64F069F91C19388473C56968576AB::get_offset_of_U3CcontextU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436 = { sizeof (FallbackJoystickIdentification_t9C9B29957528D4E41D258D222915271A533279AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5436[2] = 
{
	FallbackJoystickIdentification_t9C9B29957528D4E41D258D222915271A533279AB::get_offset_of_U3CjoystickIdU3Ek__BackingField_5(),
	FallbackJoystickIdentification_t9C9B29957528D4E41D258D222915271A533279AB::get_offset_of_U3CjoystickNameU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437 = { sizeof (Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5437[6] = 
{
	Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C::get_offset_of_U3CplayerU3Ek__BackingField_5(),
	Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C::get_offset_of_U3CcontrollerTypeU3Ek__BackingField_6(),
	Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C::get_offset_of_U3CjoystickU3Ek__BackingField_7(),
	Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C::get_offset_of_U3CcalibrationMapU3Ek__BackingField_8(),
	Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C::get_offset_of_selectedElementIdentifierId_9(),
	Calibration_tF8B690C0D37CCAAD1EC7ED4557876B4D4D48491C::get_offset_of_recording_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438 = { sizeof (WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48)+ sizeof (RuntimeObject), sizeof(WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5438[5] = 
{
	WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48::get_offset_of_windowId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48::get_offset_of_rect_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48::get_offset_of_windowDrawDelegate_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48::get_offset_of_title_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WindowProperties_tDA12E8DF0A71D01B7151F9C787CAE2023B2BAC48::get_offset_of_message_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439 = { sizeof (QueueActionType_tE2FB972C36B9841FFDAA9842823FACF93D2ABC04)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5439[6] = 
{
	QueueActionType_tE2FB972C36B9841FFDAA9842823FACF93D2ABC04::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440 = { sizeof (ElementAssignmentChangeType_t7572EB1ADF6266C81710E0DF149EC64DE4CCCB70)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5440[6] = 
{
	ElementAssignmentChangeType_t7572EB1ADF6266C81710E0DF149EC64DE4CCCB70::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441 = { sizeof (UserResponse_t661D779E3B9E6188F653D07C4C5449E9E28D39B7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5441[5] = 
{
	UserResponse_t661D779E3B9E6188F653D07C4C5449E9E28D39B7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442 = { sizeof (CustomControllersTiltDemo_t47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5442[4] = 
{
	CustomControllersTiltDemo_t47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2::get_offset_of_target_4(),
	CustomControllersTiltDemo_t47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2::get_offset_of_speed_5(),
	CustomControllersTiltDemo_t47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2::get_offset_of_controller_6(),
	CustomControllersTiltDemo_t47EFBBAF52663B7BABDF012C8425B7DF5D77AFF2::get_offset_of_player_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443 = { sizeof (CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5443[11] = 
{
	CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5::get_offset_of_playerId_4(),
	CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5::get_offset_of_controllerTag_5(),
	CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5::get_offset_of_useUpdateCallbacks_6(),
	CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5::get_offset_of_buttonCount_7(),
	CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5::get_offset_of_axisCount_8(),
	CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5::get_offset_of_axisValues_9(),
	CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5::get_offset_of_buttonValues_10(),
	CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5::get_offset_of_joysticks_11(),
	CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5::get_offset_of_buttons_12(),
	CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5::get_offset_of_controller_13(),
	CustomControllerDemo_t960BD05C542B8580E69F73C92EB18970E15444F5::get_offset_of_initialized_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444 = { sizeof (CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5444[6] = 
{
	CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499::get_offset_of_playerId_4(),
	CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499::get_offset_of_speed_5(),
	CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499::get_offset_of_bulletSpeed_6(),
	CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499::get_offset_of_bulletPrefab_7(),
	CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499::get_offset_of__player_8(),
	CustomControllerDemo_Player_t775C638BACB048093AF2D13D03252556497C7499::get_offset_of_cc_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445 = { sizeof (TouchButtonExample_t373D910E3161BD489B1A9358D1728E4EC7905E66), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5445[2] = 
{
	TouchButtonExample_t373D910E3161BD489B1A9358D1728E4EC7905E66::get_offset_of_allowMouseControl_4(),
	TouchButtonExample_t373D910E3161BD489B1A9358D1728E4EC7905E66::get_offset_of_U3CisPressedU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446 = { sizeof (TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5446[9] = 
{
	TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE::get_offset_of_allowMouseControl_4(),
	TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE::get_offset_of_radius_5(),
	TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE::get_offset_of_origAnchoredPosition_6(),
	TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE::get_offset_of_origWorldPosition_7(),
	TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE::get_offset_of_origScreenResolution_8(),
	TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE::get_offset_of_origScreenOrientation_9(),
	TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE::get_offset_of_hasFinger_10(),
	TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE::get_offset_of_lastFingerId_11(),
	TouchJoystickExample_t25AA4E5E5D4BFAEC1255F896C06D279621A4D9DE::get_offset_of_U3CpositionU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447 = { sizeof (DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5447[9] = 
{
	0,
	DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA::get_offset_of_playerId_5(),
	DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA::get_offset_of_touchpadTransform_6(),
	DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA::get_offset_of_lightObject_7(),
	DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA::get_offset_of_accelerometerTransform_8(),
	DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA::get_offset_of_touches_9(),
	DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA::get_offset_of_unusedTouches_10(),
	DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA::get_offset_of_isFlashing_11(),
	DualShock4SpecialFeaturesExample_t52837908EE3CFBF853334EC27E43E5486DA253BA::get_offset_of_textStyle_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448 = { sizeof (Touch_tD19B9C083AA87301A8FB528C6577E26EC60A10A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5448[2] = 
{
	Touch_tD19B9C083AA87301A8FB528C6577E26EC60A10A5::get_offset_of_go_0(),
	Touch_tD19B9C083AA87301A8FB528C6577E26EC60A10A5::get_offset_of_touchId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449 = { sizeof (U3CU3Ec__DisplayClass20_0_tF92CE9050A37C93C70BCBB33B8AF0525780435B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5449[1] = 
{
	U3CU3Ec__DisplayClass20_0_tF92CE9050A37C93C70BCBB33B8AF0525780435B9::get_offset_of_touchId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450 = { sizeof (EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5450[9] = 
{
	EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598::get_offset_of_playerId_4(),
	EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598::get_offset_of_moveSpeed_5(),
	EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598::get_offset_of_bulletSpeed_6(),
	EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598::get_offset_of_bulletPrefab_7(),
	EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598::get_offset_of_player_8(),
	EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598::get_offset_of_cc_9(),
	EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598::get_offset_of_moveVector_10(),
	EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598::get_offset_of_fire_11(),
	EightPlayersExample_Player_tC56813DDB9E28E6B22A606781620A16544ADF598::get_offset_of_initialized_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451 = { sizeof (FallbackJoystickIdentificationDemo_tFED66528A72925296ED5FEDAB90A9B04D45E66B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5451[7] = 
{
	0,
	0,
	0,
	FallbackJoystickIdentificationDemo_tFED66528A72925296ED5FEDAB90A9B04D45E66B9::get_offset_of_identifyRequired_7(),
	FallbackJoystickIdentificationDemo_tFED66528A72925296ED5FEDAB90A9B04D45E66B9::get_offset_of_joysticksToIdentify_8(),
	FallbackJoystickIdentificationDemo_tFED66528A72925296ED5FEDAB90A9B04D45E66B9::get_offset_of_nextInputAllowedTime_9(),
	FallbackJoystickIdentificationDemo_tFED66528A72925296ED5FEDAB90A9B04D45E66B9::get_offset_of_style_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452 = { sizeof (PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5452[14] = 
{
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_playerId_4(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_horizontalAction_5(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_verticalAction_6(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_wheelAction_7(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_leftButtonAction_8(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_rightButtonAction_9(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_middleButtonAction_10(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_distanceFromCamera_11(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_spriteScale_12(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_pointerPrefab_13(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_clickEffectPrefab_14(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_hideHardwarePointer_15(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_pointer_16(),
	PlayerMouseSpriteExample_tD784EBE45656902A6A61570F5694D6BF516DA718::get_offset_of_mouse_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453 = { sizeof (PlayerPointerEventHandlerExample_tB30A902E62F9AA0746875C484FA7F29652E66F18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5453[3] = 
{
	PlayerPointerEventHandlerExample_tB30A902E62F9AA0746875C484FA7F29652E66F18::get_offset_of_text_4(),
	0,
	PlayerPointerEventHandlerExample_tB30A902E62F9AA0746875C484FA7F29652E66F18::get_offset_of_log_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454 = { sizeof (UIPointer_t20009A3C951AB72215997ECD66AD19EBEEC4D4AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5454[3] = 
{
	UIPointer_t20009A3C951AB72215997ECD66AD19EBEEC4D4AC::get_offset_of__hideHardwarePointer_4(),
	UIPointer_t20009A3C951AB72215997ECD66AD19EBEEC4D4AC::get_offset_of__autoSort_5(),
	UIPointer_t20009A3C951AB72215997ECD66AD19EBEEC4D4AC::get_offset_of__canvasRectTransform_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455 = { sizeof (PressAnyButtonToJoinExample_Assigner_t22AF45AE8FEC38D2EA590D0041865CB39160ED1E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5456 = { sizeof (PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5456[7] = 
{
	PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9::get_offset_of_playerId_4(),
	PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9::get_offset_of_moveSpeed_5(),
	PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9::get_offset_of_bulletSpeed_6(),
	PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9::get_offset_of_bulletPrefab_7(),
	PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9::get_offset_of_cc_8(),
	PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9::get_offset_of_moveVector_9(),
	PressAnyButtonToJoinExample_GamePlayer_tCF49A9C64DA59FB40343CF81607D25BF6BC25AF9::get_offset_of_fire_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5457 = { sizeof (PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296), -1, sizeof(PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5457[4] = 
{
	PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296_StaticFields::get_offset_of_instance_4(),
	PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296::get_offset_of_maxPlayers_5(),
	PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296::get_offset_of_playerMap_6(),
	PressStartToJoinExample_Assigner_t5778175307AC1A3C22FF4CAC097823D447D78296::get_offset_of_gamePlayerIdCounter_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5458 = { sizeof (PlayerMap_t202B5F7585A6844370F11924F2CF1794A7E66908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5458[2] = 
{
	PlayerMap_t202B5F7585A6844370F11924F2CF1794A7E66908::get_offset_of_rewiredPlayerId_0(),
	PlayerMap_t202B5F7585A6844370F11924F2CF1794A7E66908::get_offset_of_gamePlayerId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5459 = { sizeof (PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5459[7] = 
{
	PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E::get_offset_of_gamePlayerId_4(),
	PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E::get_offset_of_moveSpeed_5(),
	PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E::get_offset_of_bulletSpeed_6(),
	PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E::get_offset_of_bulletPrefab_7(),
	PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E::get_offset_of_cc_8(),
	PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E::get_offset_of_moveVector_9(),
	PressStartToJoinExample_GamePlayer_t5EF63AD8C11924B17E2D2E46CCD0FBA1DE17B33E::get_offset_of_fire_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5460 = { sizeof (Bullet_t9D5DCCAF7BECEA01BC40DFB414AB0C3D8EBFB418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5460[3] = 
{
	Bullet_t9D5DCCAF7BECEA01BC40DFB414AB0C3D8EBFB418::get_offset_of_lifeTime_4(),
	Bullet_t9D5DCCAF7BECEA01BC40DFB414AB0C3D8EBFB418::get_offset_of_die_5(),
	Bullet_t9D5DCCAF7BECEA01BC40DFB414AB0C3D8EBFB418::get_offset_of_deathTime_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5461 = { sizeof (SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5461[13] = 
{
	0,
	0,
	0,
	SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0::get_offset_of_inputMapper_keyboard_7(),
	SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0::get_offset_of_inputMapper_mouse_8(),
	SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0::get_offset_of_buttonPrefab_9(),
	SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0::get_offset_of_textPrefab_10(),
	SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0::get_offset_of_fieldGroupTransform_11(),
	SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0::get_offset_of_actionGroupTransform_12(),
	SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0::get_offset_of_controllerNameUIText_13(),
	SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0::get_offset_of_statusUIText_14(),
	SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0::get_offset_of_rows_15(),
	SimpleCombinedKeyboardMouseRemapping_t99831416E94DF644CA9BB116B1447C334F8499F0::get_offset_of__replaceTargetMapping_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5462 = { sizeof (Row_t527913C609FCA2EFA018E5FDC5013A69A553DF99), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5462[4] = 
{
	Row_t527913C609FCA2EFA018E5FDC5013A69A553DF99::get_offset_of_action_0(),
	Row_t527913C609FCA2EFA018E5FDC5013A69A553DF99::get_offset_of_actionRange_1(),
	Row_t527913C609FCA2EFA018E5FDC5013A69A553DF99::get_offset_of_button_2(),
	Row_t527913C609FCA2EFA018E5FDC5013A69A553DF99::get_offset_of_text_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5463 = { sizeof (TargetMapping_tCF6708DB0700100C669867F37A1066537F71BFD7)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5463[2] = 
{
	TargetMapping_tCF6708DB0700100C669867F37A1066537F71BFD7::get_offset_of_controllerMap_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetMapping_tCF6708DB0700100C669867F37A1066537F71BFD7::get_offset_of_actionElementMapId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5464 = { sizeof (U3CU3Ec__DisplayClass17_0_tD07560944F4325B6A2336CEFAFB46B956C653D6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5464[3] = 
{
	U3CU3Ec__DisplayClass17_0_tD07560944F4325B6A2336CEFAFB46B956C653D6D::get_offset_of_index_0(),
	U3CU3Ec__DisplayClass17_0_tD07560944F4325B6A2336CEFAFB46B956C653D6D::get_offset_of_actionElementMapId_1(),
	U3CU3Ec__DisplayClass17_0_tD07560944F4325B6A2336CEFAFB46B956C653D6D::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5465 = { sizeof (U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5465[7] = 
{
	U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42::get_offset_of_U3CU3E1__state_0(),
	U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42::get_offset_of_U3CU3E2__current_1(),
	U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42::get_offset_of_index_2(),
	U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42::get_offset_of_keyboardMap_3(),
	U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42::get_offset_of_mouseMap_4(),
	U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42::get_offset_of_actionElementMapToReplaceId_5(),
	U3CStartListeningDelayedU3Ed__22_t13BD143FDA42DC18B223473B10BE69D70389AE42::get_offset_of_U3CU3E4__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5466 = { sizeof (SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5466[13] = 
{
	0,
	0,
	0,
	SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24::get_offset_of_inputMapper_7(),
	SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24::get_offset_of_buttonPrefab_8(),
	SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24::get_offset_of_textPrefab_9(),
	SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24::get_offset_of_fieldGroupTransform_10(),
	SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24::get_offset_of_actionGroupTransform_11(),
	SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24::get_offset_of_controllerNameUIText_12(),
	SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24::get_offset_of_statusUIText_13(),
	SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24::get_offset_of_selectedControllerType_14(),
	SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24::get_offset_of_selectedControllerId_15(),
	SimpleControlRemapping_t52866691ACBC69EB8C3D48ABBA21BE2AFF791A24::get_offset_of_rows_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5467 = { sizeof (Row_t6311A257F15636BEFEDD024070E739E1E899A7A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5467[4] = 
{
	Row_t6311A257F15636BEFEDD024070E739E1E899A7A5::get_offset_of_action_0(),
	Row_t6311A257F15636BEFEDD024070E739E1E899A7A5::get_offset_of_actionRange_1(),
	Row_t6311A257F15636BEFEDD024070E739E1E899A7A5::get_offset_of_button_2(),
	Row_t6311A257F15636BEFEDD024070E739E1E899A7A5::get_offset_of_text_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5468 = { sizeof (U3CU3Ec__DisplayClass21_0_t979318D186C1B8D1138D1642B4A4F4B3EDA8D479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5468[3] = 
{
	U3CU3Ec__DisplayClass21_0_t979318D186C1B8D1138D1642B4A4F4B3EDA8D479::get_offset_of_index_0(),
	U3CU3Ec__DisplayClass21_0_t979318D186C1B8D1138D1642B4A4F4B3EDA8D479::get_offset_of_actionElementMapId_1(),
	U3CU3Ec__DisplayClass21_0_t979318D186C1B8D1138D1642B4A4F4B3EDA8D479::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5469 = { sizeof (U3CStartListeningDelayedU3Ed__28_t983DAA6DEEF9337A5CF63899D4693879D6510A28), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5469[5] = 
{
	U3CStartListeningDelayedU3Ed__28_t983DAA6DEEF9337A5CF63899D4693879D6510A28::get_offset_of_U3CU3E1__state_0(),
	U3CStartListeningDelayedU3Ed__28_t983DAA6DEEF9337A5CF63899D4693879D6510A28::get_offset_of_U3CU3E2__current_1(),
	U3CStartListeningDelayedU3Ed__28_t983DAA6DEEF9337A5CF63899D4693879D6510A28::get_offset_of_index_2(),
	U3CStartListeningDelayedU3Ed__28_t983DAA6DEEF9337A5CF63899D4693879D6510A28::get_offset_of_actionElementMapToReplaceId_3(),
	U3CStartListeningDelayedU3Ed__28_t983DAA6DEEF9337A5CF63899D4693879D6510A28::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5470 = { sizeof (ControlMapperDemoMessage_tCDC84E65EDB19FA393B98CB685326785436108AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5470[2] = 
{
	ControlMapperDemoMessage_tCDC84E65EDB19FA393B98CB685326785436108AB::get_offset_of_controlMapper_4(),
	ControlMapperDemoMessage_tCDC84E65EDB19FA393B98CB685326785436108AB::get_offset_of_defaultSelectable_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5471 = { sizeof (U3CSelectDefaultDeferredU3Ed__7_t67E156353E135BDAEA30F758F9B6A60D80B81982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5471[3] = 
{
	U3CSelectDefaultDeferredU3Ed__7_t67E156353E135BDAEA30F758F9B6A60D80B81982::get_offset_of_U3CU3E1__state_0(),
	U3CSelectDefaultDeferredU3Ed__7_t67E156353E135BDAEA30F758F9B6A60D80B81982::get_offset_of_U3CU3E2__current_1(),
	U3CSelectDefaultDeferredU3Ed__7_t67E156353E135BDAEA30F758F9B6A60D80B81982::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5472 = { sizeof (TouchControls1_ManipulateCube_t777052D91E1E2E18A7CAA676E5F819C41A2D695F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5472[4] = 
{
	TouchControls1_ManipulateCube_t777052D91E1E2E18A7CAA676E5F819C41A2D695F::get_offset_of_rotateSpeed_4(),
	TouchControls1_ManipulateCube_t777052D91E1E2E18A7CAA676E5F819C41A2D695F::get_offset_of_moveSpeed_5(),
	TouchControls1_ManipulateCube_t777052D91E1E2E18A7CAA676E5F819C41A2D695F::get_offset_of_currentColorIndex_6(),
	TouchControls1_ManipulateCube_t777052D91E1E2E18A7CAA676E5F819C41A2D695F::get_offset_of_colors_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5473 = { sizeof (ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5473[6] = 
{
	ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646::get_offset_of__highlightColor_4(),
	ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646::get_offset_of__image_5(),
	ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646::get_offset_of__color_6(),
	ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646::get_offset_of__origColor_7(),
	ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646::get_offset_of__isActive_8(),
	ControllerUIEffect_t6D031A2496C863217415265EC5E20CA169702646::get_offset_of__highlightAmount_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5474 = { sizeof (ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5474[12] = 
{
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF::get_offset_of__highlightColor_4(),
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF::get_offset_of__positiveUIEffect_5(),
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF::get_offset_of__negativeUIEffect_6(),
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF::get_offset_of__label_7(),
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF::get_offset_of__positiveLabel_8(),
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF::get_offset_of__negativeLabel_9(),
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF::get_offset_of__childElements_10(),
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF::get_offset_of__image_11(),
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF::get_offset_of__color_12(),
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF::get_offset_of__origColor_13(),
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF::get_offset_of__isActive_14(),
	ControllerUIElement_tDBD0FBE9590A07647991556D8F5415587954CDAF::get_offset_of__highlightAmount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5475 = { sizeof (GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5475[31] = 
{
	0,
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_playerId_5(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_leftStick_6(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_rightStick_7(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_leftStickX_8(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_leftStickY_9(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_leftStickButton_10(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_rightStickX_11(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_rightStickY_12(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_rightStickButton_13(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_actionBottomRow1_14(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_actionBottomRow2_15(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_actionBottomRow3_16(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_actionTopRow1_17(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_actionTopRow2_18(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_actionTopRow3_19(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_leftShoulder_20(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_leftTrigger_21(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_rightShoulder_22(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_rightTrigger_23(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_center1_24(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_center2_25(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_center3_26(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_dPadUp_27(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_dPadRight_28(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_dPadDown_29(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of_dPadLeft_30(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of__uiElementsArray_31(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of__uiElements_32(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of__tempTargetList_33(),
	GamepadTemplateUI_t12D192A7CBAFEA2A9B157D4BD652A6A104047E9A::get_offset_of__sticks_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5476 = { sizeof (Stick_t8DF1E9F62C0E4349392388A6E1CF7279391435E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5476[4] = 
{
	Stick_t8DF1E9F62C0E4349392388A6E1CF7279391435E8::get_offset_of__transform_0(),
	Stick_t8DF1E9F62C0E4349392388A6E1CF7279391435E8::get_offset_of__origPosition_1(),
	Stick_t8DF1E9F62C0E4349392388A6E1CF7279391435E8::get_offset_of__xAxisElementId_2(),
	Stick_t8DF1E9F62C0E4349392388A6E1CF7279391435E8::get_offset_of__yAxisElementId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5477 = { sizeof (UIElement_t4381888A44D4BDFB1BE2256F0CEE10CEB405569D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5477[2] = 
{
	UIElement_t4381888A44D4BDFB1BE2256F0CEE10CEB405569D::get_offset_of_id_0(),
	UIElement_t4381888A44D4BDFB1BE2256F0CEE10CEB405569D::get_offset_of_element_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5478 = { sizeof (TMP_DigitValidator_tD53B3EF123D04F923055895ED56555317D239AB5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5479 = { sizeof (TMP_PhoneNumberValidator_t7EB41CFDB7C6AA586BF5AF04151FC2228F565BD2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5480 = { sizeof (TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5480[12] = 
{
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_OnCharacterSelection_4(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_OnSpriteSelection_5(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_OnWordSelection_6(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_OnLineSelection_7(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_OnLinkSelection_8(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_TextComponent_9(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_Camera_10(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_Canvas_11(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_selectedLink_12(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_lastCharIndex_13(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_lastWordIndex_14(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_lastLineIndex_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5481 = { sizeof (CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5482 = { sizeof (SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5483 = { sizeof (WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5484 = { sizeof (LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5485 = { sizeof (LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5486 = { sizeof (Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5486[10] = 
{
	Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761::get_offset_of_BenchmarkType_4(),
	Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761::get_offset_of_TMProFont_5(),
	Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761::get_offset_of_TextMeshFont_6(),
	Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761::get_offset_of_m_textMeshPro_7(),
	Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761::get_offset_of_m_textContainer_8(),
	Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761::get_offset_of_m_textMesh_9(),
	0,
	0,
	Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761::get_offset_of_m_material01_12(),
	Benchmark01_t375270EB369DC01A4C37A6381970857C4BD87761::get_offset_of_m_material02_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5487 = { sizeof (U3CStartU3Ed__10_t9DEB397698E0A4D729CAE51E4D27693C60088E4F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5487[4] = 
{
	U3CStartU3Ed__10_t9DEB397698E0A4D729CAE51E4D27693C60088E4F::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__10_t9DEB397698E0A4D729CAE51E4D27693C60088E4F::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__10_t9DEB397698E0A4D729CAE51E4D27693C60088E4F::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__10_t9DEB397698E0A4D729CAE51E4D27693C60088E4F::get_offset_of_U3CiU3E5__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5488 = { sizeof (Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5488[10] = 
{
	Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55::get_offset_of_BenchmarkType_4(),
	Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55::get_offset_of_canvas_5(),
	Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55::get_offset_of_TMProFont_6(),
	Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55::get_offset_of_TextMeshFont_7(),
	Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55::get_offset_of_m_textMeshPro_8(),
	Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55::get_offset_of_m_textMesh_9(),
	0,
	0,
	Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55::get_offset_of_m_material01_12(),
	Benchmark01_UGUI_tDD8C13B9B8E4ED9FD1680BAD52CF615726F14B55::get_offset_of_m_material02_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5489 = { sizeof (U3CStartU3Ed__10_t85A55BE1C5DC2607319D687991E4E5F7AC25386E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5489[4] = 
{
	U3CStartU3Ed__10_t85A55BE1C5DC2607319D687991E4E5F7AC25386E::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__10_t85A55BE1C5DC2607319D687991E4E5F7AC25386E::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__10_t85A55BE1C5DC2607319D687991E4E5F7AC25386E::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__10_t85A55BE1C5DC2607319D687991E4E5F7AC25386E::get_offset_of_U3CiU3E5__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5490 = { sizeof (Benchmark02_t5AD0E34D131A57AAA8B39CED7AC8E60946686BB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5490[3] = 
{
	Benchmark02_t5AD0E34D131A57AAA8B39CED7AC8E60946686BB5::get_offset_of_SpawnType_4(),
	Benchmark02_t5AD0E34D131A57AAA8B39CED7AC8E60946686BB5::get_offset_of_NumberOfNPC_5(),
	Benchmark02_t5AD0E34D131A57AAA8B39CED7AC8E60946686BB5::get_offset_of_floatingText_Script_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5491 = { sizeof (Benchmark03_tC3AB21ABCF4386EC933960A70AE20001D6D3CD87), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5491[3] = 
{
	Benchmark03_tC3AB21ABCF4386EC933960A70AE20001D6D3CD87::get_offset_of_SpawnType_4(),
	Benchmark03_tC3AB21ABCF4386EC933960A70AE20001D6D3CD87::get_offset_of_NumberOfNPC_5(),
	Benchmark03_tC3AB21ABCF4386EC933960A70AE20001D6D3CD87::get_offset_of_TheFont_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5492 = { sizeof (Benchmark04_t428B8F183784D591AAEAD632C4C56D6C227BD6D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5492[5] = 
{
	Benchmark04_t428B8F183784D591AAEAD632C4C56D6C227BD6D9::get_offset_of_SpawnType_4(),
	Benchmark04_t428B8F183784D591AAEAD632C4C56D6C227BD6D9::get_offset_of_MinPointSize_5(),
	Benchmark04_t428B8F183784D591AAEAD632C4C56D6C227BD6D9::get_offset_of_MaxPointSize_6(),
	Benchmark04_t428B8F183784D591AAEAD632C4C56D6C227BD6D9::get_offset_of_Steps_7(),
	Benchmark04_t428B8F183784D591AAEAD632C4C56D6C227BD6D9::get_offset_of_m_Transform_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5493 = { sizeof (CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5493[25] = 
{
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_cameraTransform_4(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_dummyTarget_5(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_CameraTarget_6(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_FollowDistance_7(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_MaxFollowDistance_8(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_MinFollowDistance_9(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_ElevationAngle_10(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_MaxElevationAngle_11(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_MinElevationAngle_12(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_OrbitalAngle_13(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_CameraMode_14(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_MovementSmoothing_15(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_RotationSmoothing_16(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_previousSmoothing_17(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_MovementSmoothingValue_18(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_RotationSmoothingValue_19(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_MoveSensitivity_20(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_currentVelocity_21(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_desiredPosition_22(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_mouseX_23(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_mouseY_24(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_moveVector_25(),
	CameraController_t1B35987C4C1AC6395807F1CCC998C5ED956D7911::get_offset_of_mouseWheel_26(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5494 = { sizeof (CameraModes_t4B24D50582F214FB9AB0B82E700305CB97C9CF9D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5494[4] = 
{
	CameraModes_t4B24D50582F214FB9AB0B82E700305CB97C9CF9D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5495 = { sizeof (ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5495[10] = 
{
	ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5::get_offset_of_SpinSpeed_4(),
	ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5::get_offset_of_RotationRange_5(),
	ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5::get_offset_of_m_transform_6(),
	ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5::get_offset_of_m_time_7(),
	ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5::get_offset_of_m_prevPOS_8(),
	ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5::get_offset_of_m_initial_Rotation_9(),
	ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5::get_offset_of_m_initial_Position_10(),
	ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5::get_offset_of_m_lightColor_11(),
	ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5::get_offset_of_frames_12(),
	ObjectSpin_t5EEBF4BFCCD478B89C227B8F04670C937B7E10B5::get_offset_of_Motion_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5496 = { sizeof (MotionType_t0B038BCA79B1865C903414BFE3B65AF46B2A6833)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5496[4] = 
{
	MotionType_t0B038BCA79B1865C903414BFE3B65AF46B2A6833::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5497 = { sizeof (ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5497[4] = 
{
	ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72::get_offset_of_m_Renderer_4(),
	ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72::get_offset_of_m_Material_5(),
	ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72::get_offset_of_GlowCurve_6(),
	ShaderPropAnimator_tBD6FBF3230284F283E6BC35E93E5A260853D5A72::get_offset_of_m_frame_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5498 = { sizeof (U3CAnimatePropertiesU3Ed__6_t90BF7D7B9FC009B24809156AE0F91678207A6231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5498[4] = 
{
	U3CAnimatePropertiesU3Ed__6_t90BF7D7B9FC009B24809156AE0F91678207A6231::get_offset_of_U3CU3E1__state_0(),
	U3CAnimatePropertiesU3Ed__6_t90BF7D7B9FC009B24809156AE0F91678207A6231::get_offset_of_U3CU3E2__current_1(),
	U3CAnimatePropertiesU3Ed__6_t90BF7D7B9FC009B24809156AE0F91678207A6231::get_offset_of_U3CU3E4__this_2(),
	U3CAnimatePropertiesU3Ed__6_t90BF7D7B9FC009B24809156AE0F91678207A6231::get_offset_of_U3CglowPowerU3E5__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5499 = { sizeof (SimpleScript_tBCE5C1CADFA47423C376344DE84BC0C57CEEC844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5499[3] = 
{
	SimpleScript_tBCE5C1CADFA47423C376344DE84BC0C57CEEC844::get_offset_of_m_textMeshPro_4(),
	0,
	SimpleScript_tBCE5C1CADFA47423C376344DE84BC0C57CEEC844::get_offset_of_m_frame_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
