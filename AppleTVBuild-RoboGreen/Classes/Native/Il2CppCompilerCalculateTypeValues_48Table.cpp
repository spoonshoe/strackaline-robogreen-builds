﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// EaCqncmrbxTySfotaFWfqDQVLIS/FOBdOrKrxGfXvkOuUmdtNzedDCs
struct FOBdOrKrxGfXvkOuUmdtNzedDCs_t3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309;
// EaCqncmrbxTySfotaFWfqDQVLIS/FOBdOrKrxGfXvkOuUmdtNzedDCs/GMJeTtZnicgHDDzqLfgDMVufCNPJ[]
struct GMJeTtZnicgHDDzqLfgDMVufCNPJU5BU5D_tA1A488C9527B108BE7BD4D53225C78F3389A5FB7;
// EaCqncmrbxTySfotaFWfqDQVLIS/FOBdOrKrxGfXvkOuUmdtNzedDCs/KCtXOMZUwpdfWoEclFRMgBlAMdPd[]
struct KCtXOMZUwpdfWoEclFRMgBlAMdPdU5BU5D_t0EE4FD2BD2E228277AB1C807D86C6CB45FF1018B;
// EaCqncmrbxTySfotaFWfqDQVLIS/FOBdOrKrxGfXvkOuUmdtNzedDCs/TsEOKKcYyURGUdCblaSthzyWFsU
struct TsEOKKcYyURGUdCblaSthzyWFsU_t835CAF85D62B6B5E478ECC574C9864E2115F0575;
// EaCqncmrbxTySfotaFWfqDQVLIS/FOBdOrKrxGfXvkOuUmdtNzedDCs/VsbAISQkdJoqOdmXONtnUYGDNdA[]
struct VsbAISQkdJoqOdmXONtnUYGDNdAU5BU5D_t659F3F12A92C612FBA1B07A8AB7D0F8058CB65F5;
// KDIcNAKNsuNvLiMepNxDAEccLSR
struct KDIcNAKNsuNvLiMepNxDAEccLSR_tA3DD4DB1AB688F1291A2B02EC7E3CFD5A0829709;
// MUJAEshscmeXKPKBeiQXLdyHSgZ
struct MUJAEshscmeXKPKBeiQXLdyHSgZ_t94E1E4243BF2917620850C52F104B1ED4A91DEF2;
// Rewired.ActionElementMap
struct ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7;
// Rewired.ButtonLoopSet
struct ButtonLoopSet_t043F70DD0163AEE263E80CB7B1AA6C550C46F0AC;
// Rewired.Controller/Extension
struct Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD;
// Rewired.CustomControllerMap
struct CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A;
// Rewired.HardwareJoystickMap_InputManager
struct HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0;
// Rewired.InputCategory
struct InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918;
// Rewired.Interfaces.IElementIdentifierTool
struct IElementIdentifierTool_t6628138FD205036B5A52B487F6ECE778328F764F;
// Rewired.Interfaces.IInputSource
struct IInputSource_t8DF3A6FCB5F4078F54A113D8D99FDC8B190313E3;
// Rewired.Internal.GUIText
struct GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920;
// Rewired.JoystickMap
struct JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24;
// Rewired.KeyboardMap
struct KeyboardMap_t03C5D3775649F3FEBBE43728D8FB16C32E61EBBA;
// Rewired.MouseMap
struct MouseMap_t58C438E7108115F44D16041A902ECCBD89BD2008;
// Rewired.PlatformInputManager
struct PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A;
// Rewired.ReInput/ControllerHelper/ConflictCheckingHelper
struct ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531;
// Rewired.ReInput/ControllerHelper/PollingHelper
struct PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A;
// Rewired.ReInput/pqJryspmPjvziIiviMzAyYQWLVF/HqnQFyrBLcPwCVhzxoyPgeBoOXL
struct HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8;
// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,Rewired.ReInput/pqJryspmPjvziIiviMzAyYQWLVF/HqnQFyrBLcPwCVhzxoyPgeBoOXL>
struct ADictionary_2_tA57AA08C0A4762A370A000FDE98B64C2CBB8EE69;
// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,VIhaqNNVJFgtSIZzvDNBXPXkcJWN>
struct ADictionary_2_t57BF542F20A63D9C2A5896F5B39567CA3038D7D5;
// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,jqtkirjooyDJUdKqWkqxpLraQfy>
struct ADictionary_2_tC50755225741EF63FB74BAB10788403F768DCD75;
// Rewired.Utils.Classes.Data.IndexedDictionary`2<System.Int32,EaCqncmrbxTySfotaFWfqDQVLIS/FOBdOrKrxGfXvkOuUmdtNzedDCs>
struct IndexedDictionary_2_t4D8816C0C74DC28005A0D828B26C87A58FA95C04;
// Rewired.Utils.Classes.Data.NativeBuffer
struct NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11;
// Rewired.Utils.Classes.Utility.StopwatchBase
struct StopwatchBase_t6D1ECB46BA1A327900AA160BD2BAADA752D17CB7;
// Rewired.Utils.Classes.Utility.ValueWatcher[]
struct ValueWatcherU5BU5D_t8687D2EBEAB4477F1B039B002F3511C4CD66AE78;
// Rewired.Utils.Classes.Utility.ValueWatcher`1<System.Boolean>
struct ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82;
// Rewired.Utils.Classes.Utility.ValueWatcher`1<System.Int32>
struct ValueWatcher_1_t51B841BFDCBA0C11D90E97527B04A020743EF58D;
// Rewired.Utils.Classes.Utility.ValueWatcher`1<System.Single>
struct ValueWatcher_1_tE17E1335AC012E8A17DEC7D1B384AA0132E48844;
// Rewired.Utils.Classes.Utility.ValueWatcher`1<System.String>
struct ValueWatcher_1_t9CE6AA7C51537EE99C1884F3943EFE63E679CCA8;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Rewired.BridgedController>
struct Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5;
// System.Action`1<Rewired.ControllerDisconnectedEventArgs>
struct Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0;
// System.Action`1<Rewired.UpdateControllerInfoEventArgs>
struct Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3;
// System.Action`2<System.Int32,Rewired.ControllerDataUpdater>
struct Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IDictionary`2<System.String,System.Boolean>
struct IDictionary_2_t7AB8D4C6DF7D454AF704DB31F1F1C7DA3BE3419D;
// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo>
struct IEnumerator_1_t5448337C1643DF9D47E013DBFF871416D4D3F5DC;
// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo>
struct IEnumerator_1_tB5F8E3B1AA2E4BF2597F179C159DF6EFBC7DAE69;
// System.Collections.Generic.IList`1<Rewired.CustomController>
struct IList_1_t72F8D0A343B835033A661FC40376E4E35F64327B;
// System.Collections.Generic.IList`1<Rewired.Joystick>
struct IList_1_tF47B0A47132A7F677E03201FFE653320541BBA20;
// System.Collections.Generic.IList`1<Rewired.Player>
struct IList_1_t6E46C98ED98DF6EC33AA741543131318B0F5A06A;
// System.Collections.Generic.List`1<qQSKQqvJRxFNPvSjTomQuZOntrH/lNgIgGeozTFHoKvpOBlVjbAtvLyd>
struct List_1_tC39A715C490DCEA570E936A2EE4E24DDB56C808F;
// System.Collections.Generic.List`1<qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa/QnHiHbnYGCBSFowNhmjvNvYtpXM>
struct List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF;
// System.Comparison`1<Rewired.InputAction>
struct Comparison_1_t2F2BB3FA13CDC653407D0BEDA5FD4696B0B3BA4D;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`1<System.Boolean>
struct Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1;
// System.Func`1<System.Int32>
struct Func_1_t30631A63BE46FE93700939B764202D360449FE30;
// System.Func`1<System.Single>
struct Func_1_tA655F82C43D9C03F75E783D0DF2629CF9676E735;
// System.Func`1<System.String>
struct Func_1_tFFD07C3F37BA096E036FCF22D6E90F7FF88C5220;
// System.Func`2<Rewired.BridgedControllerHWInfo,Rewired.HardwareJoystickMap_InputManager>
struct Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// XinyVXTRfxekDvrLaNfmyUwaYzV
struct XinyVXTRfxekDvrLaNfmyUwaYzV_tFC161390B4FF558F7F27D77244EC0FA850BF5C40;
// owCsobmYStIRvNUHffmjMFLdZEz
struct owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628;
// qQSKQqvJRxFNPvSjTomQuZOntrH/tLqZqETDWvmFaUyFhoSscPGCHa
struct tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef FOBDORKRXGFXVKOUUMDTNZEDDCS_T3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309_H
#define FOBDORKRXGFXVKOUUMDTNZEDDCS_T3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs
struct  FOBdOrKrxGfXvkOuUmdtNzedDCs_t3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309  : public RuntimeObject
{
public:
	// EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_KCtXOMZUwpdfWoEclFRMgBlAMdPd[] EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs::lZlHpVlqScCxOayHMQQNLIJUPtMA
	KCtXOMZUwpdfWoEclFRMgBlAMdPdU5BU5D_t0EE4FD2BD2E228277AB1C807D86C6CB45FF1018B* ___lZlHpVlqScCxOayHMQQNLIJUPtMA_0;
	// EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_TsEOKKcYyURGUdCblaSthzyWFsU EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs::vcFHuVHhWhJcpPiVLFZqonrrcV
	TsEOKKcYyURGUdCblaSthzyWFsU_t835CAF85D62B6B5E478ECC574C9864E2115F0575 * ___vcFHuVHhWhJcpPiVLFZqonrrcV_1;

public:
	inline static int32_t get_offset_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_0() { return static_cast<int32_t>(offsetof(FOBdOrKrxGfXvkOuUmdtNzedDCs_t3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309, ___lZlHpVlqScCxOayHMQQNLIJUPtMA_0)); }
	inline KCtXOMZUwpdfWoEclFRMgBlAMdPdU5BU5D_t0EE4FD2BD2E228277AB1C807D86C6CB45FF1018B* get_lZlHpVlqScCxOayHMQQNLIJUPtMA_0() const { return ___lZlHpVlqScCxOayHMQQNLIJUPtMA_0; }
	inline KCtXOMZUwpdfWoEclFRMgBlAMdPdU5BU5D_t0EE4FD2BD2E228277AB1C807D86C6CB45FF1018B** get_address_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_0() { return &___lZlHpVlqScCxOayHMQQNLIJUPtMA_0; }
	inline void set_lZlHpVlqScCxOayHMQQNLIJUPtMA_0(KCtXOMZUwpdfWoEclFRMgBlAMdPdU5BU5D_t0EE4FD2BD2E228277AB1C807D86C6CB45FF1018B* value)
	{
		___lZlHpVlqScCxOayHMQQNLIJUPtMA_0 = value;
		Il2CppCodeGenWriteBarrier((&___lZlHpVlqScCxOayHMQQNLIJUPtMA_0), value);
	}

	inline static int32_t get_offset_of_vcFHuVHhWhJcpPiVLFZqonrrcV_1() { return static_cast<int32_t>(offsetof(FOBdOrKrxGfXvkOuUmdtNzedDCs_t3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309, ___vcFHuVHhWhJcpPiVLFZqonrrcV_1)); }
	inline TsEOKKcYyURGUdCblaSthzyWFsU_t835CAF85D62B6B5E478ECC574C9864E2115F0575 * get_vcFHuVHhWhJcpPiVLFZqonrrcV_1() const { return ___vcFHuVHhWhJcpPiVLFZqonrrcV_1; }
	inline TsEOKKcYyURGUdCblaSthzyWFsU_t835CAF85D62B6B5E478ECC574C9864E2115F0575 ** get_address_of_vcFHuVHhWhJcpPiVLFZqonrrcV_1() { return &___vcFHuVHhWhJcpPiVLFZqonrrcV_1; }
	inline void set_vcFHuVHhWhJcpPiVLFZqonrrcV_1(TsEOKKcYyURGUdCblaSthzyWFsU_t835CAF85D62B6B5E478ECC574C9864E2115F0575 * value)
	{
		___vcFHuVHhWhJcpPiVLFZqonrrcV_1 = value;
		Il2CppCodeGenWriteBarrier((&___vcFHuVHhWhJcpPiVLFZqonrrcV_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOBDORKRXGFXVKOUUMDTNZEDDCS_T3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309_H
#ifndef KCTXOMZUWPDFWOECLFRMGBLAMDPD_T30709A985B3D262DC097B4F3845C4185ED11F107_H
#define KCTXOMZUWPDFWOECLFRMGBLAMDPD_T30709A985B3D262DC097B4F3845C4185ED11F107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_KCtXOMZUwpdfWoEclFRMgBlAMdPd
struct  KCtXOMZUwpdfWoEclFRMgBlAMdPd_t30709A985B3D262DC097B4F3845C4185ED11F107  : public RuntimeObject
{
public:
	// System.Int32 EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_KCtXOMZUwpdfWoEclFRMgBlAMdPd::JTlYHbNuRvgezaodmDszddIQvhCD
	int32_t ___JTlYHbNuRvgezaodmDszddIQvhCD_0;
	// EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_VsbAISQkdJoqOdmXONtnUYGDNdA[] EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_KCtXOMZUwpdfWoEclFRMgBlAMdPd::jygrDMKtEfPwNxIkfLKMOCmUyrS
	VsbAISQkdJoqOdmXONtnUYGDNdAU5BU5D_t659F3F12A92C612FBA1B07A8AB7D0F8058CB65F5* ___jygrDMKtEfPwNxIkfLKMOCmUyrS_1;
	// EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_GMJeTtZnicgHDDzqLfgDMVufCNPJ[] EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs_KCtXOMZUwpdfWoEclFRMgBlAMdPd::ojmYnkCHKzVQFExRGawmjxOAwip
	GMJeTtZnicgHDDzqLfgDMVufCNPJU5BU5D_tA1A488C9527B108BE7BD4D53225C78F3389A5FB7* ___ojmYnkCHKzVQFExRGawmjxOAwip_2;

public:
	inline static int32_t get_offset_of_JTlYHbNuRvgezaodmDszddIQvhCD_0() { return static_cast<int32_t>(offsetof(KCtXOMZUwpdfWoEclFRMgBlAMdPd_t30709A985B3D262DC097B4F3845C4185ED11F107, ___JTlYHbNuRvgezaodmDszddIQvhCD_0)); }
	inline int32_t get_JTlYHbNuRvgezaodmDszddIQvhCD_0() const { return ___JTlYHbNuRvgezaodmDszddIQvhCD_0; }
	inline int32_t* get_address_of_JTlYHbNuRvgezaodmDszddIQvhCD_0() { return &___JTlYHbNuRvgezaodmDszddIQvhCD_0; }
	inline void set_JTlYHbNuRvgezaodmDszddIQvhCD_0(int32_t value)
	{
		___JTlYHbNuRvgezaodmDszddIQvhCD_0 = value;
	}

	inline static int32_t get_offset_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_1() { return static_cast<int32_t>(offsetof(KCtXOMZUwpdfWoEclFRMgBlAMdPd_t30709A985B3D262DC097B4F3845C4185ED11F107, ___jygrDMKtEfPwNxIkfLKMOCmUyrS_1)); }
	inline VsbAISQkdJoqOdmXONtnUYGDNdAU5BU5D_t659F3F12A92C612FBA1B07A8AB7D0F8058CB65F5* get_jygrDMKtEfPwNxIkfLKMOCmUyrS_1() const { return ___jygrDMKtEfPwNxIkfLKMOCmUyrS_1; }
	inline VsbAISQkdJoqOdmXONtnUYGDNdAU5BU5D_t659F3F12A92C612FBA1B07A8AB7D0F8058CB65F5** get_address_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_1() { return &___jygrDMKtEfPwNxIkfLKMOCmUyrS_1; }
	inline void set_jygrDMKtEfPwNxIkfLKMOCmUyrS_1(VsbAISQkdJoqOdmXONtnUYGDNdAU5BU5D_t659F3F12A92C612FBA1B07A8AB7D0F8058CB65F5* value)
	{
		___jygrDMKtEfPwNxIkfLKMOCmUyrS_1 = value;
		Il2CppCodeGenWriteBarrier((&___jygrDMKtEfPwNxIkfLKMOCmUyrS_1), value);
	}

	inline static int32_t get_offset_of_ojmYnkCHKzVQFExRGawmjxOAwip_2() { return static_cast<int32_t>(offsetof(KCtXOMZUwpdfWoEclFRMgBlAMdPd_t30709A985B3D262DC097B4F3845C4185ED11F107, ___ojmYnkCHKzVQFExRGawmjxOAwip_2)); }
	inline GMJeTtZnicgHDDzqLfgDMVufCNPJU5BU5D_tA1A488C9527B108BE7BD4D53225C78F3389A5FB7* get_ojmYnkCHKzVQFExRGawmjxOAwip_2() const { return ___ojmYnkCHKzVQFExRGawmjxOAwip_2; }
	inline GMJeTtZnicgHDDzqLfgDMVufCNPJU5BU5D_tA1A488C9527B108BE7BD4D53225C78F3389A5FB7** get_address_of_ojmYnkCHKzVQFExRGawmjxOAwip_2() { return &___ojmYnkCHKzVQFExRGawmjxOAwip_2; }
	inline void set_ojmYnkCHKzVQFExRGawmjxOAwip_2(GMJeTtZnicgHDDzqLfgDMVufCNPJU5BU5D_tA1A488C9527B108BE7BD4D53225C78F3389A5FB7* value)
	{
		___ojmYnkCHKzVQFExRGawmjxOAwip_2 = value;
		Il2CppCodeGenWriteBarrier((&___ojmYnkCHKzVQFExRGawmjxOAwip_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KCTXOMZUWPDFWOECLFRMGBLAMDPD_T30709A985B3D262DC097B4F3845C4185ED11F107_H
#ifndef RAPFTFLMDCODAGLXUFWGWRYFGIR_T4D7AFB2D4817B66B8260A2ABFC09E59BA9638DD5_H
#define RAPFTFLMDCODAGLXUFWGWRYFGIR_T4D7AFB2D4817B66B8260A2ABFC09E59BA9638DD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RapfTflmdcOdagLxUfWgWRYFgIR
struct  RapfTflmdcOdagLxUfWgWRYFgIR_t4D7AFB2D4817B66B8260A2ABFC09E59BA9638DD5  : public RuntimeObject
{
public:
	// Rewired.Internal.GUIText RapfTflmdcOdagLxUfWgWRYFgIR::GbhogeXOuqAWXhmgfRAmudFOsEia
	GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920 * ___GbhogeXOuqAWXhmgfRAmudFOsEia_0;
	// System.String RapfTflmdcOdagLxUfWgWRYFgIR::ifzUzDIkGKOLpvauQZqGqyDgvJM
	String_t* ___ifzUzDIkGKOLpvauQZqGqyDgvJM_1;
	// System.Int32 RapfTflmdcOdagLxUfWgWRYFgIR::gTshMfebAuuRWoIiARFpiGoZoPk
	int32_t ___gTshMfebAuuRWoIiARFpiGoZoPk_2;

public:
	inline static int32_t get_offset_of_GbhogeXOuqAWXhmgfRAmudFOsEia_0() { return static_cast<int32_t>(offsetof(RapfTflmdcOdagLxUfWgWRYFgIR_t4D7AFB2D4817B66B8260A2ABFC09E59BA9638DD5, ___GbhogeXOuqAWXhmgfRAmudFOsEia_0)); }
	inline GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920 * get_GbhogeXOuqAWXhmgfRAmudFOsEia_0() const { return ___GbhogeXOuqAWXhmgfRAmudFOsEia_0; }
	inline GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920 ** get_address_of_GbhogeXOuqAWXhmgfRAmudFOsEia_0() { return &___GbhogeXOuqAWXhmgfRAmudFOsEia_0; }
	inline void set_GbhogeXOuqAWXhmgfRAmudFOsEia_0(GUIText_t84576ACF2926E50EB99E8E3EAB1737B0E96F1920 * value)
	{
		___GbhogeXOuqAWXhmgfRAmudFOsEia_0 = value;
		Il2CppCodeGenWriteBarrier((&___GbhogeXOuqAWXhmgfRAmudFOsEia_0), value);
	}

	inline static int32_t get_offset_of_ifzUzDIkGKOLpvauQZqGqyDgvJM_1() { return static_cast<int32_t>(offsetof(RapfTflmdcOdagLxUfWgWRYFgIR_t4D7AFB2D4817B66B8260A2ABFC09E59BA9638DD5, ___ifzUzDIkGKOLpvauQZqGqyDgvJM_1)); }
	inline String_t* get_ifzUzDIkGKOLpvauQZqGqyDgvJM_1() const { return ___ifzUzDIkGKOLpvauQZqGqyDgvJM_1; }
	inline String_t** get_address_of_ifzUzDIkGKOLpvauQZqGqyDgvJM_1() { return &___ifzUzDIkGKOLpvauQZqGqyDgvJM_1; }
	inline void set_ifzUzDIkGKOLpvauQZqGqyDgvJM_1(String_t* value)
	{
		___ifzUzDIkGKOLpvauQZqGqyDgvJM_1 = value;
		Il2CppCodeGenWriteBarrier((&___ifzUzDIkGKOLpvauQZqGqyDgvJM_1), value);
	}

	inline static int32_t get_offset_of_gTshMfebAuuRWoIiARFpiGoZoPk_2() { return static_cast<int32_t>(offsetof(RapfTflmdcOdagLxUfWgWRYFgIR_t4D7AFB2D4817B66B8260A2ABFC09E59BA9638DD5, ___gTshMfebAuuRWoIiARFpiGoZoPk_2)); }
	inline int32_t get_gTshMfebAuuRWoIiARFpiGoZoPk_2() const { return ___gTshMfebAuuRWoIiARFpiGoZoPk_2; }
	inline int32_t* get_address_of_gTshMfebAuuRWoIiARFpiGoZoPk_2() { return &___gTshMfebAuuRWoIiARFpiGoZoPk_2; }
	inline void set_gTshMfebAuuRWoIiARFpiGoZoPk_2(int32_t value)
	{
		___gTshMfebAuuRWoIiARFpiGoZoPk_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAPFTFLMDCODAGLXUFWGWRYFGIR_T4D7AFB2D4817B66B8260A2ABFC09E59BA9638DD5_H
#ifndef FLTZELIMAREBQQWALOTPDTOQUOK_T7B8E84D261131F81F9CB39918B4CCA94CD9517C6_H
#define FLTZELIMAREBQQWALOTPDTOQUOK_T7B8E84D261131F81F9CB39918B4CCA94CD9517C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.Tools.DebugInformation_FLTzelIMaReBqqwAlotpdtoqUOK
struct  FLTzelIMaReBqqwAlotpdtoqUOK_t7B8E84D261131F81F9CB39918B4CCA94CD9517C6  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Dev.Tools.DebugInformation_FLTzelIMaReBqqwAlotpdtoqUOK::IZxaLXvQArYNNUktxzvawNmZpkj
	bool ___IZxaLXvQArYNNUktxzvawNmZpkj_0;

public:
	inline static int32_t get_offset_of_IZxaLXvQArYNNUktxzvawNmZpkj_0() { return static_cast<int32_t>(offsetof(FLTzelIMaReBqqwAlotpdtoqUOK_t7B8E84D261131F81F9CB39918B4CCA94CD9517C6, ___IZxaLXvQArYNNUktxzvawNmZpkj_0)); }
	inline bool get_IZxaLXvQArYNNUktxzvawNmZpkj_0() const { return ___IZxaLXvQArYNNUktxzvawNmZpkj_0; }
	inline bool* get_address_of_IZxaLXvQArYNNUktxzvawNmZpkj_0() { return &___IZxaLXvQArYNNUktxzvawNmZpkj_0; }
	inline void set_IZxaLXvQArYNNUktxzvawNmZpkj_0(bool value)
	{
		___IZxaLXvQArYNNUktxzvawNmZpkj_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLTZELIMAREBQQWALOTPDTOQUOK_T7B8E84D261131F81F9CB39918B4CCA94CD9517C6_H
#ifndef KGCELMPSHPTAYNVJFWSOTYHDRQK_T9A56EFA94D7770D79F2AFA3C2D84A42F97E51F6E_H
#define KGCELMPSHPTAYNVJFWSOTYHDRQK_T9A56EFA94D7770D79F2AFA3C2D84A42F97E51F6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.Tools.DebugInformation_KGCeLmPShpTaYNvjfwSOtyhDrqk
struct  KGCeLmPShpTaYNvjfwSOtyhDrqk_t9A56EFA94D7770D79F2AFA3C2D84A42F97E51F6E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KGCELMPSHPTAYNVJFWSOTYHDRQK_T9A56EFA94D7770D79F2AFA3C2D84A42F97E51F6E_H
#ifndef PNOULHJPFHAVIIYXKBBDJMOXCJQ_T8C79150C2E842D92DD73EC11D2E1A54C1583F1CB_H
#define PNOULHJPFHAVIIYXKBBDJMOXCJQ_T8C79150C2E842D92DD73EC11D2E1A54C1583F1CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.Tools.DebugInformation_PNOULhjPFHAvIIYXkBbDJmOxcjQ
struct  PNOULhjPFHAvIIYXkBbDJmOxcjQ_t8C79150C2E842D92DD73EC11D2E1A54C1583F1CB  : public RuntimeObject
{
public:

public:
};

struct PNOULhjPFHAvIIYXkBbDJmOxcjQ_t8C79150C2E842D92DD73EC11D2E1A54C1583F1CB_StaticFields
{
public:
	// System.Int32 Rewired.Dev.Tools.DebugInformation_PNOULhjPFHAvIIYXkBbDJmOxcjQ::wpuUcfVnfsggekXSGAdRLcxuPvec
	int32_t ___wpuUcfVnfsggekXSGAdRLcxuPvec_0;

public:
	inline static int32_t get_offset_of_wpuUcfVnfsggekXSGAdRLcxuPvec_0() { return static_cast<int32_t>(offsetof(PNOULhjPFHAvIIYXkBbDJmOxcjQ_t8C79150C2E842D92DD73EC11D2E1A54C1583F1CB_StaticFields, ___wpuUcfVnfsggekXSGAdRLcxuPvec_0)); }
	inline int32_t get_wpuUcfVnfsggekXSGAdRLcxuPvec_0() const { return ___wpuUcfVnfsggekXSGAdRLcxuPvec_0; }
	inline int32_t* get_address_of_wpuUcfVnfsggekXSGAdRLcxuPvec_0() { return &___wpuUcfVnfsggekXSGAdRLcxuPvec_0; }
	inline void set_wpuUcfVnfsggekXSGAdRLcxuPvec_0(int32_t value)
	{
		___wpuUcfVnfsggekXSGAdRLcxuPvec_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PNOULHJPFHAVIIYXKBBDJMOXCJQ_T8C79150C2E842D92DD73EC11D2E1A54C1583F1CB_H
#ifndef EIOUCXQXPIDLEIRRXPQZEBZFHCX_T037D03A46FB198374E670DD87D8ADBE71D8E808D_H
#define EIOUCXQXPIDLEIRRXPQZEBZFHCX_T037D03A46FB198374E670DD87D8ADBE71D8E808D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.Tools.DebugInformation_eIOucXQXPidleIRrXPQzebZFhcX
struct  eIOucXQXPidleIRrXPQzebZFhcX_t037D03A46FB198374E670DD87D8ADBE71D8E808D  : public RuntimeObject
{
public:
	// Rewired.InputCategory Rewired.Dev.Tools.DebugInformation_eIOucXQXPidleIRrXPQzebZFhcX::NutGqeIClygQlMZLGvMceGuLZh
	InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * ___NutGqeIClygQlMZLGvMceGuLZh_0;

public:
	inline static int32_t get_offset_of_NutGqeIClygQlMZLGvMceGuLZh_0() { return static_cast<int32_t>(offsetof(eIOucXQXPidleIRrXPQzebZFhcX_t037D03A46FB198374E670DD87D8ADBE71D8E808D, ___NutGqeIClygQlMZLGvMceGuLZh_0)); }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * get_NutGqeIClygQlMZLGvMceGuLZh_0() const { return ___NutGqeIClygQlMZLGvMceGuLZh_0; }
	inline InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 ** get_address_of_NutGqeIClygQlMZLGvMceGuLZh_0() { return &___NutGqeIClygQlMZLGvMceGuLZh_0; }
	inline void set_NutGqeIClygQlMZLGvMceGuLZh_0(InputCategory_t775B2399E2070EF82BBA5D9676BD00C30159D918 * value)
	{
		___NutGqeIClygQlMZLGvMceGuLZh_0 = value;
		Il2CppCodeGenWriteBarrier((&___NutGqeIClygQlMZLGvMceGuLZh_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EIOUCXQXPIDLEIRRXPQZEBZFHCX_T037D03A46FB198374E670DD87D8ADBE71D8E808D_H
#ifndef MIMFYJRNIGQWWASOPIKBCOGDLOB_TE836FB405B4F6F3AACBE2D8BA9F6E7C26F4BDF99_H
#define MIMFYJRNIGQWWASOPIKBCOGDLOB_TE836FB405B4F6F3AACBE2D8BA9F6E7C26F4BDF99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.Tools.DebugInformation_mImfYjRnIGQWWaSoPiKbCoGdLOb
struct  mImfYjRnIGQWWaSoPiKbCoGdLOb_tE836FB405B4F6F3AACBE2D8BA9F6E7C26F4BDF99  : public RuntimeObject
{
public:

public:
};

struct mImfYjRnIGQWWaSoPiKbCoGdLOb_tE836FB405B4F6F3AACBE2D8BA9F6E7C26F4BDF99_StaticFields
{
public:
	// System.Single Rewired.Dev.Tools.DebugInformation_mImfYjRnIGQWWaSoPiKbCoGdLOb::zMcnnBFtPTJiMXPQbYfzZbFGcSfi
	float ___zMcnnBFtPTJiMXPQbYfzZbFGcSfi_0;
	// System.Single Rewired.Dev.Tools.DebugInformation_mImfYjRnIGQWWaSoPiKbCoGdLOb::jZGZszBGmrMHTMrsAGOwSgYfhFd
	float ___jZGZszBGmrMHTMrsAGOwSgYfhFd_1;

public:
	inline static int32_t get_offset_of_zMcnnBFtPTJiMXPQbYfzZbFGcSfi_0() { return static_cast<int32_t>(offsetof(mImfYjRnIGQWWaSoPiKbCoGdLOb_tE836FB405B4F6F3AACBE2D8BA9F6E7C26F4BDF99_StaticFields, ___zMcnnBFtPTJiMXPQbYfzZbFGcSfi_0)); }
	inline float get_zMcnnBFtPTJiMXPQbYfzZbFGcSfi_0() const { return ___zMcnnBFtPTJiMXPQbYfzZbFGcSfi_0; }
	inline float* get_address_of_zMcnnBFtPTJiMXPQbYfzZbFGcSfi_0() { return &___zMcnnBFtPTJiMXPQbYfzZbFGcSfi_0; }
	inline void set_zMcnnBFtPTJiMXPQbYfzZbFGcSfi_0(float value)
	{
		___zMcnnBFtPTJiMXPQbYfzZbFGcSfi_0 = value;
	}

	inline static int32_t get_offset_of_jZGZszBGmrMHTMrsAGOwSgYfhFd_1() { return static_cast<int32_t>(offsetof(mImfYjRnIGQWWaSoPiKbCoGdLOb_tE836FB405B4F6F3AACBE2D8BA9F6E7C26F4BDF99_StaticFields, ___jZGZszBGmrMHTMrsAGOwSgYfhFd_1)); }
	inline float get_jZGZszBGmrMHTMrsAGOwSgYfhFd_1() const { return ___jZGZszBGmrMHTMrsAGOwSgYfhFd_1; }
	inline float* get_address_of_jZGZszBGmrMHTMrsAGOwSgYfhFd_1() { return &___jZGZszBGmrMHTMrsAGOwSgYfhFd_1; }
	inline void set_jZGZszBGmrMHTMrsAGOwSgYfhFd_1(float value)
	{
		___jZGZszBGmrMHTMrsAGOwSgYfhFd_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIMFYJRNIGQWWASOPIKBCOGDLOB_TE836FB405B4F6F3AACBE2D8BA9F6E7C26F4BDF99_H
#ifndef PLATFORMINPUTMANAGER_TF891982F1ED83426874318B2057900C730AAA62A_H
#define PLATFORMINPUTMANAGER_TF891982F1ED83426874318B2057900C730AAA62A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PlatformInputManager
struct  PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.BridgedController> Rewired.PlatformInputManager::_DeviceConnectedEvent
	Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5 * ____DeviceConnectedEvent_0;
	// System.Action`1<Rewired.ControllerDisconnectedEventArgs> Rewired.PlatformInputManager::_DeviceDisconnectedEvent
	Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0 * ____DeviceDisconnectedEvent_1;
	// System.Action`1<Rewired.UpdateControllerInfoEventArgs> Rewired.PlatformInputManager::_UpdateControllerInfoEvent
	Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3 * ____UpdateControllerInfoEvent_2;
	// System.Action Rewired.PlatformInputManager::_SystemDeviceConnectedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____SystemDeviceConnectedEvent_3;
	// System.Action Rewired.PlatformInputManager::_SystemDeviceDisconnectedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____SystemDeviceDisconnectedEvent_4;

public:
	inline static int32_t get_offset_of__DeviceConnectedEvent_0() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____DeviceConnectedEvent_0)); }
	inline Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5 * get__DeviceConnectedEvent_0() const { return ____DeviceConnectedEvent_0; }
	inline Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5 ** get_address_of__DeviceConnectedEvent_0() { return &____DeviceConnectedEvent_0; }
	inline void set__DeviceConnectedEvent_0(Action_1_tE90DAC97066A0ACCF1B2F27839802EFA321C28B5 * value)
	{
		____DeviceConnectedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____DeviceConnectedEvent_0), value);
	}

	inline static int32_t get_offset_of__DeviceDisconnectedEvent_1() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____DeviceDisconnectedEvent_1)); }
	inline Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0 * get__DeviceDisconnectedEvent_1() const { return ____DeviceDisconnectedEvent_1; }
	inline Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0 ** get_address_of__DeviceDisconnectedEvent_1() { return &____DeviceDisconnectedEvent_1; }
	inline void set__DeviceDisconnectedEvent_1(Action_1_t719D4F4ED82D9C5AE4E183CF2B312A0D5B24C6E0 * value)
	{
		____DeviceDisconnectedEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&____DeviceDisconnectedEvent_1), value);
	}

	inline static int32_t get_offset_of__UpdateControllerInfoEvent_2() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____UpdateControllerInfoEvent_2)); }
	inline Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3 * get__UpdateControllerInfoEvent_2() const { return ____UpdateControllerInfoEvent_2; }
	inline Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3 ** get_address_of__UpdateControllerInfoEvent_2() { return &____UpdateControllerInfoEvent_2; }
	inline void set__UpdateControllerInfoEvent_2(Action_1_t2138117522AB336981D5026CCF967CFE4EC0C7A3 * value)
	{
		____UpdateControllerInfoEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateControllerInfoEvent_2), value);
	}

	inline static int32_t get_offset_of__SystemDeviceConnectedEvent_3() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____SystemDeviceConnectedEvent_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__SystemDeviceConnectedEvent_3() const { return ____SystemDeviceConnectedEvent_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__SystemDeviceConnectedEvent_3() { return &____SystemDeviceConnectedEvent_3; }
	inline void set__SystemDeviceConnectedEvent_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____SystemDeviceConnectedEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&____SystemDeviceConnectedEvent_3), value);
	}

	inline static int32_t get_offset_of__SystemDeviceDisconnectedEvent_4() { return static_cast<int32_t>(offsetof(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A, ____SystemDeviceDisconnectedEvent_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__SystemDeviceDisconnectedEvent_4() const { return ____SystemDeviceDisconnectedEvent_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__SystemDeviceDisconnectedEvent_4() { return &____SystemDeviceDisconnectedEvent_4; }
	inline void set__SystemDeviceDisconnectedEvent_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____SystemDeviceDisconnectedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&____SystemDeviceDisconnectedEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMINPUTMANAGER_TF891982F1ED83426874318B2057900C730AAA62A_H
#ifndef MQKNBNQPNVBXAFJURWIMREWJEPX_TDA208E365DB809C8477A2F047E8CAD06B771192E_H
#define MQKNBNQPNVBXAFJURWIMREWJEPX_TDA208E365DB809C8477A2F047E8CAD06B771192E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx
struct  MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E  : public RuntimeObject
{
public:
	// Rewired.Utils.Classes.Utility.ValueWatcher`1<System.Boolean> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::WGCdlWCIlNjPpctEurlSzZFfPFD
	ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 * ___WGCdlWCIlNjPpctEurlSzZFfPFD_0;
	// Rewired.Utils.Classes.Utility.ValueWatcher`1<System.Boolean> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::KfeqNphxbTPTdykssKVqYrxbxrc
	ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 * ___KfeqNphxbTPTdykssKVqYrxbxrc_1;
	// Rewired.Utils.Classes.Utility.ValueWatcher`1<System.Boolean> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::nFOWbEPonqKitfalvGFCfOPbQQG
	ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 * ___nFOWbEPonqKitfalvGFCfOPbQQG_2;
	// Rewired.Utils.Classes.Utility.ValueWatcher`1<System.Int32> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::nyArQoHIYEElDwAvhKPFdbxYiY
	ValueWatcher_1_t51B841BFDCBA0C11D90E97527B04A020743EF58D * ___nyArQoHIYEElDwAvhKPFdbxYiY_3;
	// Rewired.Utils.Classes.Utility.ValueWatcher`1<System.Single> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::kEjtQBWtabLkjEDzeMaeglYiBmU
	ValueWatcher_1_tE17E1335AC012E8A17DEC7D1B384AA0132E48844 * ___kEjtQBWtabLkjEDzeMaeglYiBmU_4;
	// Rewired.Utils.Classes.Utility.ValueWatcher`1<System.String> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::AwkUOamRZcvdzKinYnrvOZuIqNl
	ValueWatcher_1_t9CE6AA7C51537EE99C1884F3943EFE63E679CCA8 * ___AwkUOamRZcvdzKinYnrvOZuIqNl_5;
	// Rewired.Utils.Classes.Utility.ValueWatcher`1<System.Boolean> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::aYUPMDUNGHqiWFWonBtZAAqDYJi
	ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 * ___aYUPMDUNGHqiWFWonBtZAAqDYJi_6;
	// System.Int32 Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::HQyzNoPvdEapYtzbEmvKxIBiXNW
	int32_t ___HQyzNoPvdEapYtzbEmvKxIBiXNW_7;
	// Rewired.Utils.Classes.Utility.ValueWatcher[] Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::QKVefOfCLeFRMPSTgLRChgteCu
	ValueWatcherU5BU5D_t8687D2EBEAB4477F1B039B002F3511C4CD66AE78* ___QKVefOfCLeFRMPSTgLRChgteCu_8;

public:
	inline static int32_t get_offset_of_WGCdlWCIlNjPpctEurlSzZFfPFD_0() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E, ___WGCdlWCIlNjPpctEurlSzZFfPFD_0)); }
	inline ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 * get_WGCdlWCIlNjPpctEurlSzZFfPFD_0() const { return ___WGCdlWCIlNjPpctEurlSzZFfPFD_0; }
	inline ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 ** get_address_of_WGCdlWCIlNjPpctEurlSzZFfPFD_0() { return &___WGCdlWCIlNjPpctEurlSzZFfPFD_0; }
	inline void set_WGCdlWCIlNjPpctEurlSzZFfPFD_0(ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 * value)
	{
		___WGCdlWCIlNjPpctEurlSzZFfPFD_0 = value;
		Il2CppCodeGenWriteBarrier((&___WGCdlWCIlNjPpctEurlSzZFfPFD_0), value);
	}

	inline static int32_t get_offset_of_KfeqNphxbTPTdykssKVqYrxbxrc_1() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E, ___KfeqNphxbTPTdykssKVqYrxbxrc_1)); }
	inline ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 * get_KfeqNphxbTPTdykssKVqYrxbxrc_1() const { return ___KfeqNphxbTPTdykssKVqYrxbxrc_1; }
	inline ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 ** get_address_of_KfeqNphxbTPTdykssKVqYrxbxrc_1() { return &___KfeqNphxbTPTdykssKVqYrxbxrc_1; }
	inline void set_KfeqNphxbTPTdykssKVqYrxbxrc_1(ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 * value)
	{
		___KfeqNphxbTPTdykssKVqYrxbxrc_1 = value;
		Il2CppCodeGenWriteBarrier((&___KfeqNphxbTPTdykssKVqYrxbxrc_1), value);
	}

	inline static int32_t get_offset_of_nFOWbEPonqKitfalvGFCfOPbQQG_2() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E, ___nFOWbEPonqKitfalvGFCfOPbQQG_2)); }
	inline ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 * get_nFOWbEPonqKitfalvGFCfOPbQQG_2() const { return ___nFOWbEPonqKitfalvGFCfOPbQQG_2; }
	inline ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 ** get_address_of_nFOWbEPonqKitfalvGFCfOPbQQG_2() { return &___nFOWbEPonqKitfalvGFCfOPbQQG_2; }
	inline void set_nFOWbEPonqKitfalvGFCfOPbQQG_2(ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 * value)
	{
		___nFOWbEPonqKitfalvGFCfOPbQQG_2 = value;
		Il2CppCodeGenWriteBarrier((&___nFOWbEPonqKitfalvGFCfOPbQQG_2), value);
	}

	inline static int32_t get_offset_of_nyArQoHIYEElDwAvhKPFdbxYiY_3() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E, ___nyArQoHIYEElDwAvhKPFdbxYiY_3)); }
	inline ValueWatcher_1_t51B841BFDCBA0C11D90E97527B04A020743EF58D * get_nyArQoHIYEElDwAvhKPFdbxYiY_3() const { return ___nyArQoHIYEElDwAvhKPFdbxYiY_3; }
	inline ValueWatcher_1_t51B841BFDCBA0C11D90E97527B04A020743EF58D ** get_address_of_nyArQoHIYEElDwAvhKPFdbxYiY_3() { return &___nyArQoHIYEElDwAvhKPFdbxYiY_3; }
	inline void set_nyArQoHIYEElDwAvhKPFdbxYiY_3(ValueWatcher_1_t51B841BFDCBA0C11D90E97527B04A020743EF58D * value)
	{
		___nyArQoHIYEElDwAvhKPFdbxYiY_3 = value;
		Il2CppCodeGenWriteBarrier((&___nyArQoHIYEElDwAvhKPFdbxYiY_3), value);
	}

	inline static int32_t get_offset_of_kEjtQBWtabLkjEDzeMaeglYiBmU_4() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E, ___kEjtQBWtabLkjEDzeMaeglYiBmU_4)); }
	inline ValueWatcher_1_tE17E1335AC012E8A17DEC7D1B384AA0132E48844 * get_kEjtQBWtabLkjEDzeMaeglYiBmU_4() const { return ___kEjtQBWtabLkjEDzeMaeglYiBmU_4; }
	inline ValueWatcher_1_tE17E1335AC012E8A17DEC7D1B384AA0132E48844 ** get_address_of_kEjtQBWtabLkjEDzeMaeglYiBmU_4() { return &___kEjtQBWtabLkjEDzeMaeglYiBmU_4; }
	inline void set_kEjtQBWtabLkjEDzeMaeglYiBmU_4(ValueWatcher_1_tE17E1335AC012E8A17DEC7D1B384AA0132E48844 * value)
	{
		___kEjtQBWtabLkjEDzeMaeglYiBmU_4 = value;
		Il2CppCodeGenWriteBarrier((&___kEjtQBWtabLkjEDzeMaeglYiBmU_4), value);
	}

	inline static int32_t get_offset_of_AwkUOamRZcvdzKinYnrvOZuIqNl_5() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E, ___AwkUOamRZcvdzKinYnrvOZuIqNl_5)); }
	inline ValueWatcher_1_t9CE6AA7C51537EE99C1884F3943EFE63E679CCA8 * get_AwkUOamRZcvdzKinYnrvOZuIqNl_5() const { return ___AwkUOamRZcvdzKinYnrvOZuIqNl_5; }
	inline ValueWatcher_1_t9CE6AA7C51537EE99C1884F3943EFE63E679CCA8 ** get_address_of_AwkUOamRZcvdzKinYnrvOZuIqNl_5() { return &___AwkUOamRZcvdzKinYnrvOZuIqNl_5; }
	inline void set_AwkUOamRZcvdzKinYnrvOZuIqNl_5(ValueWatcher_1_t9CE6AA7C51537EE99C1884F3943EFE63E679CCA8 * value)
	{
		___AwkUOamRZcvdzKinYnrvOZuIqNl_5 = value;
		Il2CppCodeGenWriteBarrier((&___AwkUOamRZcvdzKinYnrvOZuIqNl_5), value);
	}

	inline static int32_t get_offset_of_aYUPMDUNGHqiWFWonBtZAAqDYJi_6() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E, ___aYUPMDUNGHqiWFWonBtZAAqDYJi_6)); }
	inline ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 * get_aYUPMDUNGHqiWFWonBtZAAqDYJi_6() const { return ___aYUPMDUNGHqiWFWonBtZAAqDYJi_6; }
	inline ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 ** get_address_of_aYUPMDUNGHqiWFWonBtZAAqDYJi_6() { return &___aYUPMDUNGHqiWFWonBtZAAqDYJi_6; }
	inline void set_aYUPMDUNGHqiWFWonBtZAAqDYJi_6(ValueWatcher_1_t449BABD3D84C22D7855CE6FB0D54B90BD33EEC82 * value)
	{
		___aYUPMDUNGHqiWFWonBtZAAqDYJi_6 = value;
		Il2CppCodeGenWriteBarrier((&___aYUPMDUNGHqiWFWonBtZAAqDYJi_6), value);
	}

	inline static int32_t get_offset_of_HQyzNoPvdEapYtzbEmvKxIBiXNW_7() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E, ___HQyzNoPvdEapYtzbEmvKxIBiXNW_7)); }
	inline int32_t get_HQyzNoPvdEapYtzbEmvKxIBiXNW_7() const { return ___HQyzNoPvdEapYtzbEmvKxIBiXNW_7; }
	inline int32_t* get_address_of_HQyzNoPvdEapYtzbEmvKxIBiXNW_7() { return &___HQyzNoPvdEapYtzbEmvKxIBiXNW_7; }
	inline void set_HQyzNoPvdEapYtzbEmvKxIBiXNW_7(int32_t value)
	{
		___HQyzNoPvdEapYtzbEmvKxIBiXNW_7 = value;
	}

	inline static int32_t get_offset_of_QKVefOfCLeFRMPSTgLRChgteCu_8() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E, ___QKVefOfCLeFRMPSTgLRChgteCu_8)); }
	inline ValueWatcherU5BU5D_t8687D2EBEAB4477F1B039B002F3511C4CD66AE78* get_QKVefOfCLeFRMPSTgLRChgteCu_8() const { return ___QKVefOfCLeFRMPSTgLRChgteCu_8; }
	inline ValueWatcherU5BU5D_t8687D2EBEAB4477F1B039B002F3511C4CD66AE78** get_address_of_QKVefOfCLeFRMPSTgLRChgteCu_8() { return &___QKVefOfCLeFRMPSTgLRChgteCu_8; }
	inline void set_QKVefOfCLeFRMPSTgLRChgteCu_8(ValueWatcherU5BU5D_t8687D2EBEAB4477F1B039B002F3511C4CD66AE78* value)
	{
		___QKVefOfCLeFRMPSTgLRChgteCu_8 = value;
		Il2CppCodeGenWriteBarrier((&___QKVefOfCLeFRMPSTgLRChgteCu_8), value);
	}
};

struct MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields
{
public:
	// System.Func`1<System.Boolean> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::FehaJfEEjpeYLfpmUMzkFAbCRwsA
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___FehaJfEEjpeYLfpmUMzkFAbCRwsA_9;
	// System.Func`1<System.Boolean> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::GMPHKVIYVSfxfzouOasvunIymPL
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___GMPHKVIYVSfxfzouOasvunIymPL_10;
	// System.Func`1<System.Int32> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::mMURPWMpQTiDTaELQZQNlGDUORm
	Func_1_t30631A63BE46FE93700939B764202D360449FE30 * ___mMURPWMpQTiDTaELQZQNlGDUORm_11;
	// System.Func`1<System.Single> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::rRiHYDzLJkdCLwJhYbxMYZlGox
	Func_1_tA655F82C43D9C03F75E783D0DF2629CF9676E735 * ___rRiHYDzLJkdCLwJhYbxMYZlGox_12;
	// System.Func`1<System.Boolean> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::EkGbeTUTBdDIoDPcAfixArdMdHyG
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___EkGbeTUTBdDIoDPcAfixArdMdHyG_13;
	// System.Func`1<System.String> Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx::OJQStWFoTlmLdwbYTlSqDmvhecEF
	Func_1_tFFD07C3F37BA096E036FCF22D6E90F7FF88C5220 * ___OJQStWFoTlmLdwbYTlSqDmvhecEF_14;

public:
	inline static int32_t get_offset_of_FehaJfEEjpeYLfpmUMzkFAbCRwsA_9() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields, ___FehaJfEEjpeYLfpmUMzkFAbCRwsA_9)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_FehaJfEEjpeYLfpmUMzkFAbCRwsA_9() const { return ___FehaJfEEjpeYLfpmUMzkFAbCRwsA_9; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_FehaJfEEjpeYLfpmUMzkFAbCRwsA_9() { return &___FehaJfEEjpeYLfpmUMzkFAbCRwsA_9; }
	inline void set_FehaJfEEjpeYLfpmUMzkFAbCRwsA_9(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___FehaJfEEjpeYLfpmUMzkFAbCRwsA_9 = value;
		Il2CppCodeGenWriteBarrier((&___FehaJfEEjpeYLfpmUMzkFAbCRwsA_9), value);
	}

	inline static int32_t get_offset_of_GMPHKVIYVSfxfzouOasvunIymPL_10() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields, ___GMPHKVIYVSfxfzouOasvunIymPL_10)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_GMPHKVIYVSfxfzouOasvunIymPL_10() const { return ___GMPHKVIYVSfxfzouOasvunIymPL_10; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_GMPHKVIYVSfxfzouOasvunIymPL_10() { return &___GMPHKVIYVSfxfzouOasvunIymPL_10; }
	inline void set_GMPHKVIYVSfxfzouOasvunIymPL_10(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___GMPHKVIYVSfxfzouOasvunIymPL_10 = value;
		Il2CppCodeGenWriteBarrier((&___GMPHKVIYVSfxfzouOasvunIymPL_10), value);
	}

	inline static int32_t get_offset_of_mMURPWMpQTiDTaELQZQNlGDUORm_11() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields, ___mMURPWMpQTiDTaELQZQNlGDUORm_11)); }
	inline Func_1_t30631A63BE46FE93700939B764202D360449FE30 * get_mMURPWMpQTiDTaELQZQNlGDUORm_11() const { return ___mMURPWMpQTiDTaELQZQNlGDUORm_11; }
	inline Func_1_t30631A63BE46FE93700939B764202D360449FE30 ** get_address_of_mMURPWMpQTiDTaELQZQNlGDUORm_11() { return &___mMURPWMpQTiDTaELQZQNlGDUORm_11; }
	inline void set_mMURPWMpQTiDTaELQZQNlGDUORm_11(Func_1_t30631A63BE46FE93700939B764202D360449FE30 * value)
	{
		___mMURPWMpQTiDTaELQZQNlGDUORm_11 = value;
		Il2CppCodeGenWriteBarrier((&___mMURPWMpQTiDTaELQZQNlGDUORm_11), value);
	}

	inline static int32_t get_offset_of_rRiHYDzLJkdCLwJhYbxMYZlGox_12() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields, ___rRiHYDzLJkdCLwJhYbxMYZlGox_12)); }
	inline Func_1_tA655F82C43D9C03F75E783D0DF2629CF9676E735 * get_rRiHYDzLJkdCLwJhYbxMYZlGox_12() const { return ___rRiHYDzLJkdCLwJhYbxMYZlGox_12; }
	inline Func_1_tA655F82C43D9C03F75E783D0DF2629CF9676E735 ** get_address_of_rRiHYDzLJkdCLwJhYbxMYZlGox_12() { return &___rRiHYDzLJkdCLwJhYbxMYZlGox_12; }
	inline void set_rRiHYDzLJkdCLwJhYbxMYZlGox_12(Func_1_tA655F82C43D9C03F75E783D0DF2629CF9676E735 * value)
	{
		___rRiHYDzLJkdCLwJhYbxMYZlGox_12 = value;
		Il2CppCodeGenWriteBarrier((&___rRiHYDzLJkdCLwJhYbxMYZlGox_12), value);
	}

	inline static int32_t get_offset_of_EkGbeTUTBdDIoDPcAfixArdMdHyG_13() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields, ___EkGbeTUTBdDIoDPcAfixArdMdHyG_13)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_EkGbeTUTBdDIoDPcAfixArdMdHyG_13() const { return ___EkGbeTUTBdDIoDPcAfixArdMdHyG_13; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_EkGbeTUTBdDIoDPcAfixArdMdHyG_13() { return &___EkGbeTUTBdDIoDPcAfixArdMdHyG_13; }
	inline void set_EkGbeTUTBdDIoDPcAfixArdMdHyG_13(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___EkGbeTUTBdDIoDPcAfixArdMdHyG_13 = value;
		Il2CppCodeGenWriteBarrier((&___EkGbeTUTBdDIoDPcAfixArdMdHyG_13), value);
	}

	inline static int32_t get_offset_of_OJQStWFoTlmLdwbYTlSqDmvhecEF_14() { return static_cast<int32_t>(offsetof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields, ___OJQStWFoTlmLdwbYTlSqDmvhecEF_14)); }
	inline Func_1_tFFD07C3F37BA096E036FCF22D6E90F7FF88C5220 * get_OJQStWFoTlmLdwbYTlSqDmvhecEF_14() const { return ___OJQStWFoTlmLdwbYTlSqDmvhecEF_14; }
	inline Func_1_tFFD07C3F37BA096E036FCF22D6E90F7FF88C5220 ** get_address_of_OJQStWFoTlmLdwbYTlSqDmvhecEF_14() { return &___OJQStWFoTlmLdwbYTlSqDmvhecEF_14; }
	inline void set_OJQStWFoTlmLdwbYTlSqDmvhecEF_14(Func_1_tFFD07C3F37BA096E036FCF22D6E90F7FF88C5220 * value)
	{
		___OJQStWFoTlmLdwbYTlSqDmvhecEF_14 = value;
		Il2CppCodeGenWriteBarrier((&___OJQStWFoTlmLdwbYTlSqDmvhecEF_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MQKNBNQPNVBXAFJURWIMREWJEPX_TDA208E365DB809C8477A2F047E8CAD06B771192E_H
#ifndef PQJRYSPMPJVZIIIVIMZAYYQWLVF_T23A934806527E77ED77ACDD6B97574DC00008DA6_H
#define PQJRYSPMPJVZIIIVIMZAYYQWLVF_T23A934806527E77ED77ACDD6B97574DC00008DA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF
struct  pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6  : public RuntimeObject
{
public:
	// Rewired.Utils.Classes.Utility.StopwatchBase Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF::nHlVzWkGpFfUWPJtIRsEsNgLDELG
	StopwatchBase_t6D1ECB46BA1A327900AA160BD2BAADA752D17CB7 * ___nHlVzWkGpFfUWPJtIRsEsNgLDELG_0;
	// System.Single Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF::sDEzAjQAWfwrpOqYgXBvfAdrDfhg
	float ___sDEzAjQAWfwrpOqYgXBvfAdrDfhg_1;
	// Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_HqnQFyrBLcPwCVhzxoyPgeBoOXL Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF::WzMPJskYTcPesnNpifYmyBmyoWg
	HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8 * ___WzMPJskYTcPesnNpifYmyBmyoWg_2;
	// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_HqnQFyrBLcPwCVhzxoyPgeBoOXL> Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF::hmLGQccjeUxxLDHBmtORSRecFvq
	ADictionary_2_tA57AA08C0A4762A370A000FDE98B64C2CBB8EE69 * ___hmLGQccjeUxxLDHBmtORSRecFvq_3;
	// System.UInt32 Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF::QkiftyHpjHSxNjtYSUvKFpWPMbWG
	uint32_t ___QkiftyHpjHSxNjtYSUvKFpWPMbWG_4;

public:
	inline static int32_t get_offset_of_nHlVzWkGpFfUWPJtIRsEsNgLDELG_0() { return static_cast<int32_t>(offsetof(pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6, ___nHlVzWkGpFfUWPJtIRsEsNgLDELG_0)); }
	inline StopwatchBase_t6D1ECB46BA1A327900AA160BD2BAADA752D17CB7 * get_nHlVzWkGpFfUWPJtIRsEsNgLDELG_0() const { return ___nHlVzWkGpFfUWPJtIRsEsNgLDELG_0; }
	inline StopwatchBase_t6D1ECB46BA1A327900AA160BD2BAADA752D17CB7 ** get_address_of_nHlVzWkGpFfUWPJtIRsEsNgLDELG_0() { return &___nHlVzWkGpFfUWPJtIRsEsNgLDELG_0; }
	inline void set_nHlVzWkGpFfUWPJtIRsEsNgLDELG_0(StopwatchBase_t6D1ECB46BA1A327900AA160BD2BAADA752D17CB7 * value)
	{
		___nHlVzWkGpFfUWPJtIRsEsNgLDELG_0 = value;
		Il2CppCodeGenWriteBarrier((&___nHlVzWkGpFfUWPJtIRsEsNgLDELG_0), value);
	}

	inline static int32_t get_offset_of_sDEzAjQAWfwrpOqYgXBvfAdrDfhg_1() { return static_cast<int32_t>(offsetof(pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6, ___sDEzAjQAWfwrpOqYgXBvfAdrDfhg_1)); }
	inline float get_sDEzAjQAWfwrpOqYgXBvfAdrDfhg_1() const { return ___sDEzAjQAWfwrpOqYgXBvfAdrDfhg_1; }
	inline float* get_address_of_sDEzAjQAWfwrpOqYgXBvfAdrDfhg_1() { return &___sDEzAjQAWfwrpOqYgXBvfAdrDfhg_1; }
	inline void set_sDEzAjQAWfwrpOqYgXBvfAdrDfhg_1(float value)
	{
		___sDEzAjQAWfwrpOqYgXBvfAdrDfhg_1 = value;
	}

	inline static int32_t get_offset_of_WzMPJskYTcPesnNpifYmyBmyoWg_2() { return static_cast<int32_t>(offsetof(pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6, ___WzMPJskYTcPesnNpifYmyBmyoWg_2)); }
	inline HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8 * get_WzMPJskYTcPesnNpifYmyBmyoWg_2() const { return ___WzMPJskYTcPesnNpifYmyBmyoWg_2; }
	inline HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8 ** get_address_of_WzMPJskYTcPesnNpifYmyBmyoWg_2() { return &___WzMPJskYTcPesnNpifYmyBmyoWg_2; }
	inline void set_WzMPJskYTcPesnNpifYmyBmyoWg_2(HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8 * value)
	{
		___WzMPJskYTcPesnNpifYmyBmyoWg_2 = value;
		Il2CppCodeGenWriteBarrier((&___WzMPJskYTcPesnNpifYmyBmyoWg_2), value);
	}

	inline static int32_t get_offset_of_hmLGQccjeUxxLDHBmtORSRecFvq_3() { return static_cast<int32_t>(offsetof(pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6, ___hmLGQccjeUxxLDHBmtORSRecFvq_3)); }
	inline ADictionary_2_tA57AA08C0A4762A370A000FDE98B64C2CBB8EE69 * get_hmLGQccjeUxxLDHBmtORSRecFvq_3() const { return ___hmLGQccjeUxxLDHBmtORSRecFvq_3; }
	inline ADictionary_2_tA57AA08C0A4762A370A000FDE98B64C2CBB8EE69 ** get_address_of_hmLGQccjeUxxLDHBmtORSRecFvq_3() { return &___hmLGQccjeUxxLDHBmtORSRecFvq_3; }
	inline void set_hmLGQccjeUxxLDHBmtORSRecFvq_3(ADictionary_2_tA57AA08C0A4762A370A000FDE98B64C2CBB8EE69 * value)
	{
		___hmLGQccjeUxxLDHBmtORSRecFvq_3 = value;
		Il2CppCodeGenWriteBarrier((&___hmLGQccjeUxxLDHBmtORSRecFvq_3), value);
	}

	inline static int32_t get_offset_of_QkiftyHpjHSxNjtYSUvKFpWPMbWG_4() { return static_cast<int32_t>(offsetof(pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6, ___QkiftyHpjHSxNjtYSUvKFpWPMbWG_4)); }
	inline uint32_t get_QkiftyHpjHSxNjtYSUvKFpWPMbWG_4() const { return ___QkiftyHpjHSxNjtYSUvKFpWPMbWG_4; }
	inline uint32_t* get_address_of_QkiftyHpjHSxNjtYSUvKFpWPMbWG_4() { return &___QkiftyHpjHSxNjtYSUvKFpWPMbWG_4; }
	inline void set_QkiftyHpjHSxNjtYSUvKFpWPMbWG_4(uint32_t value)
	{
		___QkiftyHpjHSxNjtYSUvKFpWPMbWG_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PQJRYSPMPJVZIIIVIMZAYYQWLVF_T23A934806527E77ED77ACDD6B97574DC00008DA6_H
#ifndef PRGSMIROCDXXYKABICIZKVJUFDY_T71FA8DFC2A0814DE269004402D1B9E4E9C6DAF72_H
#define PRGSMIROCDXXYKABICIZKVJUFDY_T71FA8DFC2A0814DE269004402D1B9E4E9C6DAF72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_prGSMirOcDXXYKABICizKVJUfDY
struct  prGSMirOcDXXYKABICizKVJUfDY_t71FA8DFC2A0814DE269004402D1B9E4E9C6DAF72  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRGSMIROCDXXYKABICIZKVJUFDY_T71FA8DFC2A0814DE269004402D1B9E4E9C6DAF72_H
#ifndef CODEHELPER_T8618ECFBF040A78FEDA26D398742608E1AA036AB_H
#define CODEHELPER_T8618ECFBF040A78FEDA26D398742608E1AA036AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.CodeHelper
struct  CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEHELPER_T8618ECFBF040A78FEDA26D398742608E1AA036AB_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef MPQCGVJVQCCLNQDBBCYXRPOSZQV_TC2A3F8E7F287CF9FB37D2D1A1ECE971704C02252_H
#define MPQCGVJVQCCLNQDBBCYXRPOSZQV_TC2A3F8E7F287CF9FB37D2D1A1ECE971704C02252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// mPQCgvJVqccLNQdbbCYXrpOszqv
struct  mPQCgvJVqccLNQdbbCYXrpOszqv_tC2A3F8E7F287CF9FB37D2D1A1ECE971704C02252  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MPQCGVJVQCCLNQDBBCYXRPOSZQV_TC2A3F8E7F287CF9FB37D2D1A1ECE971704C02252_H
#ifndef TLQZQETDWVMFAUYFHOSSCPGCHA_TA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773_H
#define TLQZQETDWVMFAUYFHOSSCPGCHA_TA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa
struct  tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM> qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa::AHOFIsQodiSzMcHMSUpdlsGdKfH
	List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0;

public:
	inline static int32_t get_offset_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() { return static_cast<int32_t>(offsetof(tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773, ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0)); }
	inline List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() const { return ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0; }
	inline List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF ** get_address_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() { return &___AHOFIsQodiSzMcHMSUpdlsGdKfH_0; }
	inline void set_AHOFIsQodiSzMcHMSUpdlsGdKfH_0(List_1_tB5CAF9C6ABFFAA3AD1E3CADE86E65D8C3853B6FF * value)
	{
		___AHOFIsQodiSzMcHMSUpdlsGdKfH_0 = value;
		Il2CppCodeGenWriteBarrier((&___AHOFIsQodiSzMcHMSUpdlsGdKfH_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLQZQETDWVMFAUYFHOSSCPGCHA_TA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773_H
#ifndef SSMYCZXLYXGIXUSUHXFHCEQECTR_T23709CD7AD3923255200D20AD2F6D7232F5C3B26_H
#define SSMYCZXLYXGIXUSUHXFHCEQECTR_T23709CD7AD3923255200D20AD2F6D7232F5C3B26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// sSmyCZxlYXGixusuhxfHceqeCTr
struct  sSmyCZxlYXGixusuhxfHceqeCTr_t23709CD7AD3923255200D20AD2F6D7232F5C3B26  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSMYCZXLYXGIXUSUHXFHCEQECTR_T23709CD7AD3923255200D20AD2F6D7232F5C3B26_H
#ifndef PIDVID_TE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D_H
#define PIDVID_TE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.PidVid
struct  PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D 
{
public:
	// System.UInt16 Rewired.PidVid::productId
	uint16_t ___productId_1;
	// System.UInt16 Rewired.PidVid::vendorId
	uint16_t ___vendorId_2;

public:
	inline static int32_t get_offset_of_productId_1() { return static_cast<int32_t>(offsetof(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D, ___productId_1)); }
	inline uint16_t get_productId_1() const { return ___productId_1; }
	inline uint16_t* get_address_of_productId_1() { return &___productId_1; }
	inline void set_productId_1(uint16_t value)
	{
		___productId_1 = value;
	}

	inline static int32_t get_offset_of_vendorId_2() { return static_cast<int32_t>(offsetof(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D, ___vendorId_2)); }
	inline uint16_t get_vendorId_2() const { return ___vendorId_2; }
	inline uint16_t* get_address_of_vendorId_2() { return &___vendorId_2; }
	inline void set_vendorId_2(uint16_t value)
	{
		___vendorId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIDVID_TE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D_H
#ifndef CONFLICTCHECKINGHELPER_T86BCFF8098407F0F2AC252C2EDB520222E11E531_H
#define CONFLICTCHECKINGHELPER_T86BCFF8098407F0F2AC252C2EDB520222E11E531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper
struct  ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531  : public CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB
{
public:

public:
};

struct ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531_StaticFields
{
public:
	// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper Rewired.ReInput_ControllerHelper_ConflictCheckingHelper::FBhGsngGbrZoBDJQYQqNPKqLfYtD
	ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0;

public:
	inline static int32_t get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return static_cast<int32_t>(offsetof(ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531_StaticFields, ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0)); }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * get_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() const { return ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 ** get_address_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return &___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline void set_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * value)
	{
		___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0 = value;
		Il2CppCodeGenWriteBarrier((&___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFLICTCHECKINGHELPER_T86BCFF8098407F0F2AC252C2EDB520222E11E531_H
#ifndef MAPPINGHELPER_T2F1FD9406CF911414A90FCB4852B174257FA2BF6_H
#define MAPPINGHELPER_T2F1FD9406CF911414A90FCB4852B174257FA2BF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_MappingHelper
struct  MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6  : public CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB
{
public:

public:
};

struct MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6_StaticFields
{
public:
	// Rewired.ReInput_MappingHelper Rewired.ReInput_MappingHelper::FBhGsngGbrZoBDJQYQqNPKqLfYtD
	MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0;

public:
	inline static int32_t get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return static_cast<int32_t>(offsetof(MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6_StaticFields, ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0)); }
	inline MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * get_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() const { return ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 ** get_address_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return &___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline void set_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * value)
	{
		___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0 = value;
		Il2CppCodeGenWriteBarrier((&___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGHELPER_T2F1FD9406CF911414A90FCB4852B174257FA2BF6_H
#ifndef PLAYERHELPER_T2C3BED5411C72051E42F567C5BA51C28FF0F6412_H
#define PLAYERHELPER_T2C3BED5411C72051E42F567C5BA51C28FF0F6412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_PlayerHelper
struct  PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412  : public CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB
{
public:

public:
};

struct PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412_StaticFields
{
public:
	// Rewired.ReInput_PlayerHelper Rewired.ReInput_PlayerHelper::FBhGsngGbrZoBDJQYQqNPKqLfYtD
	PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412 * ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0;

public:
	inline static int32_t get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return static_cast<int32_t>(offsetof(PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412_StaticFields, ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0)); }
	inline PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412 * get_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() const { return ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412 ** get_address_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return &___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline void set_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412 * value)
	{
		___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0 = value;
		Il2CppCodeGenWriteBarrier((&___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERHELPER_T2C3BED5411C72051E42F567C5BA51C28FF0F6412_H
#ifndef TIMEHELPER_TE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B_H
#define TIMEHELPER_TE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_TimeHelper
struct  TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B  : public CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB
{
public:

public:
};

struct TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B_StaticFields
{
public:
	// Rewired.ReInput_TimeHelper Rewired.ReInput_TimeHelper::FBhGsngGbrZoBDJQYQqNPKqLfYtD
	TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B * ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0;

public:
	inline static int32_t get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return static_cast<int32_t>(offsetof(TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B_StaticFields, ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0)); }
	inline TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B * get_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() const { return ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B ** get_address_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return &___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline void set_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B * value)
	{
		___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0 = value;
		Il2CppCodeGenWriteBarrier((&___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEHELPER_TE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B_H
#ifndef UNITYTOUCH_T854507D66472271756069BB218E8CEEE3AD9B1F9_H
#define UNITYTOUCH_T854507D66472271756069BB218E8CEEE3AD9B1F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_UnityTouch
struct  UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9  : public CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB
{
public:

public:
};

struct UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9_StaticFields
{
public:
	// Rewired.ReInput_UnityTouch Rewired.ReInput_UnityTouch::FBhGsngGbrZoBDJQYQqNPKqLfYtD
	UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9 * ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0;

public:
	inline static int32_t get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return static_cast<int32_t>(offsetof(UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9_StaticFields, ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0)); }
	inline UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9 * get_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() const { return ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9 ** get_address_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return &___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline void set_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9 * value)
	{
		___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0 = value;
		Il2CppCodeGenWriteBarrier((&___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTOUCH_T854507D66472271756069BB218E8CEEE3AD9B1F9_H
#ifndef BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#define BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#ifndef CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#define CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INT16_T823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D_H
#define INT16_T823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int16
struct  Int16_t823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_t823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16_T823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef QQSKQQVJRXFNPVSJTOMQUZONTRH_T90D9ECA2A0A9866470299ACBD3CED42FB841B98B_H
#define QQSKQQVJRXFNPVSJTOMQUZONTRH_T90D9ECA2A0A9866470299ACBD3CED42FB841B98B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// qQSKQqvJRxFNPvSjTomQuZOntrH
struct  qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B  : public PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A
{
public:
	// Rewired.Interfaces.IInputSource qQSKQqvJRxFNPvSjTomQuZOntrH::ytEpmEgckhdtBXUMvGmTeXChJTg
	RuntimeObject* ___ytEpmEgckhdtBXUMvGmTeXChJTg_6;
	// System.Collections.Generic.List`1<qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd> qQSKQqvJRxFNPvSjTomQuZOntrH::lZlHpVlqScCxOayHMQQNLIJUPtMA
	List_1_tC39A715C490DCEA570E936A2EE4E24DDB56C808F * ___lZlHpVlqScCxOayHMQQNLIJUPtMA_7;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH::GZolyQbnhDcIglfHmqdnClfPLwP
	int32_t ___GZolyQbnhDcIglfHmqdnClfPLwP_8;
	// qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa qQSKQqvJRxFNPvSjTomQuZOntrH::iswBkwCyDabLmfxdAYpLbXBWfvHN
	tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773 * ___iswBkwCyDabLmfxdAYpLbXBWfvHN_9;
	// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH::cteekJgqyXKoMIQGdQUhjuJXMhSq
	bool ___cteekJgqyXKoMIQGdQUhjuJXMhSq_10;
	// System.Action`2<System.Int32,Rewired.ControllerDataUpdater> qQSKQqvJRxFNPvSjTomQuZOntrH::nEiQhTKZDBYqGTkqzeEgrkzxQan
	Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A * ___nEiQhTKZDBYqGTkqzeEgrkzxQan_11;
	// Rewired.PlatformInputManager qQSKQqvJRxFNPvSjTomQuZOntrH::fVwZoOiZNlMKSCdQWxpuPNZKDrf
	PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A * ___fVwZoOiZNlMKSCdQWxpuPNZKDrf_12;
	// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH::GPamRvCbOttEEABtEfVCeWQVUod
	bool ___GPamRvCbOttEEABtEfVCeWQVUod_13;
	// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH::cAcffkahmtipZtjyREtLadTZZhr
	bool ___cAcffkahmtipZtjyREtLadTZZhr_14;
	// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH::JfNLsrbeYBZuaaMZBfeVwFCxYsI
	bool ___JfNLsrbeYBZuaaMZBfeVwFCxYsI_15;
	// System.Func`2<Rewired.BridgedControllerHWInfo,Rewired.HardwareJoystickMap_InputManager> qQSKQqvJRxFNPvSjTomQuZOntrH::QEAEcDsIVKSxlRksWqQtzkbPdqrh
	Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_16;
	// System.Func`1<System.Int32> qQSKQqvJRxFNPvSjTomQuZOntrH::ZGtOAcUCAuhJuSLEHTELVSEYNBN
	Func_1_t30631A63BE46FE93700939B764202D360449FE30 * ___ZGtOAcUCAuhJuSLEHTELVSEYNBN_17;

public:
	inline static int32_t get_offset_of_ytEpmEgckhdtBXUMvGmTeXChJTg_6() { return static_cast<int32_t>(offsetof(qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B, ___ytEpmEgckhdtBXUMvGmTeXChJTg_6)); }
	inline RuntimeObject* get_ytEpmEgckhdtBXUMvGmTeXChJTg_6() const { return ___ytEpmEgckhdtBXUMvGmTeXChJTg_6; }
	inline RuntimeObject** get_address_of_ytEpmEgckhdtBXUMvGmTeXChJTg_6() { return &___ytEpmEgckhdtBXUMvGmTeXChJTg_6; }
	inline void set_ytEpmEgckhdtBXUMvGmTeXChJTg_6(RuntimeObject* value)
	{
		___ytEpmEgckhdtBXUMvGmTeXChJTg_6 = value;
		Il2CppCodeGenWriteBarrier((&___ytEpmEgckhdtBXUMvGmTeXChJTg_6), value);
	}

	inline static int32_t get_offset_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_7() { return static_cast<int32_t>(offsetof(qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B, ___lZlHpVlqScCxOayHMQQNLIJUPtMA_7)); }
	inline List_1_tC39A715C490DCEA570E936A2EE4E24DDB56C808F * get_lZlHpVlqScCxOayHMQQNLIJUPtMA_7() const { return ___lZlHpVlqScCxOayHMQQNLIJUPtMA_7; }
	inline List_1_tC39A715C490DCEA570E936A2EE4E24DDB56C808F ** get_address_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_7() { return &___lZlHpVlqScCxOayHMQQNLIJUPtMA_7; }
	inline void set_lZlHpVlqScCxOayHMQQNLIJUPtMA_7(List_1_tC39A715C490DCEA570E936A2EE4E24DDB56C808F * value)
	{
		___lZlHpVlqScCxOayHMQQNLIJUPtMA_7 = value;
		Il2CppCodeGenWriteBarrier((&___lZlHpVlqScCxOayHMQQNLIJUPtMA_7), value);
	}

	inline static int32_t get_offset_of_GZolyQbnhDcIglfHmqdnClfPLwP_8() { return static_cast<int32_t>(offsetof(qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B, ___GZolyQbnhDcIglfHmqdnClfPLwP_8)); }
	inline int32_t get_GZolyQbnhDcIglfHmqdnClfPLwP_8() const { return ___GZolyQbnhDcIglfHmqdnClfPLwP_8; }
	inline int32_t* get_address_of_GZolyQbnhDcIglfHmqdnClfPLwP_8() { return &___GZolyQbnhDcIglfHmqdnClfPLwP_8; }
	inline void set_GZolyQbnhDcIglfHmqdnClfPLwP_8(int32_t value)
	{
		___GZolyQbnhDcIglfHmqdnClfPLwP_8 = value;
	}

	inline static int32_t get_offset_of_iswBkwCyDabLmfxdAYpLbXBWfvHN_9() { return static_cast<int32_t>(offsetof(qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B, ___iswBkwCyDabLmfxdAYpLbXBWfvHN_9)); }
	inline tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773 * get_iswBkwCyDabLmfxdAYpLbXBWfvHN_9() const { return ___iswBkwCyDabLmfxdAYpLbXBWfvHN_9; }
	inline tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773 ** get_address_of_iswBkwCyDabLmfxdAYpLbXBWfvHN_9() { return &___iswBkwCyDabLmfxdAYpLbXBWfvHN_9; }
	inline void set_iswBkwCyDabLmfxdAYpLbXBWfvHN_9(tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773 * value)
	{
		___iswBkwCyDabLmfxdAYpLbXBWfvHN_9 = value;
		Il2CppCodeGenWriteBarrier((&___iswBkwCyDabLmfxdAYpLbXBWfvHN_9), value);
	}

	inline static int32_t get_offset_of_cteekJgqyXKoMIQGdQUhjuJXMhSq_10() { return static_cast<int32_t>(offsetof(qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B, ___cteekJgqyXKoMIQGdQUhjuJXMhSq_10)); }
	inline bool get_cteekJgqyXKoMIQGdQUhjuJXMhSq_10() const { return ___cteekJgqyXKoMIQGdQUhjuJXMhSq_10; }
	inline bool* get_address_of_cteekJgqyXKoMIQGdQUhjuJXMhSq_10() { return &___cteekJgqyXKoMIQGdQUhjuJXMhSq_10; }
	inline void set_cteekJgqyXKoMIQGdQUhjuJXMhSq_10(bool value)
	{
		___cteekJgqyXKoMIQGdQUhjuJXMhSq_10 = value;
	}

	inline static int32_t get_offset_of_nEiQhTKZDBYqGTkqzeEgrkzxQan_11() { return static_cast<int32_t>(offsetof(qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B, ___nEiQhTKZDBYqGTkqzeEgrkzxQan_11)); }
	inline Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A * get_nEiQhTKZDBYqGTkqzeEgrkzxQan_11() const { return ___nEiQhTKZDBYqGTkqzeEgrkzxQan_11; }
	inline Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A ** get_address_of_nEiQhTKZDBYqGTkqzeEgrkzxQan_11() { return &___nEiQhTKZDBYqGTkqzeEgrkzxQan_11; }
	inline void set_nEiQhTKZDBYqGTkqzeEgrkzxQan_11(Action_2_t7BEEAFCFB52F10ABA8C24BDBDCAA872D13CEFC2A * value)
	{
		___nEiQhTKZDBYqGTkqzeEgrkzxQan_11 = value;
		Il2CppCodeGenWriteBarrier((&___nEiQhTKZDBYqGTkqzeEgrkzxQan_11), value);
	}

	inline static int32_t get_offset_of_fVwZoOiZNlMKSCdQWxpuPNZKDrf_12() { return static_cast<int32_t>(offsetof(qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B, ___fVwZoOiZNlMKSCdQWxpuPNZKDrf_12)); }
	inline PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A * get_fVwZoOiZNlMKSCdQWxpuPNZKDrf_12() const { return ___fVwZoOiZNlMKSCdQWxpuPNZKDrf_12; }
	inline PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A ** get_address_of_fVwZoOiZNlMKSCdQWxpuPNZKDrf_12() { return &___fVwZoOiZNlMKSCdQWxpuPNZKDrf_12; }
	inline void set_fVwZoOiZNlMKSCdQWxpuPNZKDrf_12(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A * value)
	{
		___fVwZoOiZNlMKSCdQWxpuPNZKDrf_12 = value;
		Il2CppCodeGenWriteBarrier((&___fVwZoOiZNlMKSCdQWxpuPNZKDrf_12), value);
	}

	inline static int32_t get_offset_of_GPamRvCbOttEEABtEfVCeWQVUod_13() { return static_cast<int32_t>(offsetof(qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B, ___GPamRvCbOttEEABtEfVCeWQVUod_13)); }
	inline bool get_GPamRvCbOttEEABtEfVCeWQVUod_13() const { return ___GPamRvCbOttEEABtEfVCeWQVUod_13; }
	inline bool* get_address_of_GPamRvCbOttEEABtEfVCeWQVUod_13() { return &___GPamRvCbOttEEABtEfVCeWQVUod_13; }
	inline void set_GPamRvCbOttEEABtEfVCeWQVUod_13(bool value)
	{
		___GPamRvCbOttEEABtEfVCeWQVUod_13 = value;
	}

	inline static int32_t get_offset_of_cAcffkahmtipZtjyREtLadTZZhr_14() { return static_cast<int32_t>(offsetof(qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B, ___cAcffkahmtipZtjyREtLadTZZhr_14)); }
	inline bool get_cAcffkahmtipZtjyREtLadTZZhr_14() const { return ___cAcffkahmtipZtjyREtLadTZZhr_14; }
	inline bool* get_address_of_cAcffkahmtipZtjyREtLadTZZhr_14() { return &___cAcffkahmtipZtjyREtLadTZZhr_14; }
	inline void set_cAcffkahmtipZtjyREtLadTZZhr_14(bool value)
	{
		___cAcffkahmtipZtjyREtLadTZZhr_14 = value;
	}

	inline static int32_t get_offset_of_JfNLsrbeYBZuaaMZBfeVwFCxYsI_15() { return static_cast<int32_t>(offsetof(qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B, ___JfNLsrbeYBZuaaMZBfeVwFCxYsI_15)); }
	inline bool get_JfNLsrbeYBZuaaMZBfeVwFCxYsI_15() const { return ___JfNLsrbeYBZuaaMZBfeVwFCxYsI_15; }
	inline bool* get_address_of_JfNLsrbeYBZuaaMZBfeVwFCxYsI_15() { return &___JfNLsrbeYBZuaaMZBfeVwFCxYsI_15; }
	inline void set_JfNLsrbeYBZuaaMZBfeVwFCxYsI_15(bool value)
	{
		___JfNLsrbeYBZuaaMZBfeVwFCxYsI_15 = value;
	}

	inline static int32_t get_offset_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_16() { return static_cast<int32_t>(offsetof(qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B, ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_16)); }
	inline Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * get_QEAEcDsIVKSxlRksWqQtzkbPdqrh_16() const { return ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_16; }
	inline Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF ** get_address_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_16() { return &___QEAEcDsIVKSxlRksWqQtzkbPdqrh_16; }
	inline void set_QEAEcDsIVKSxlRksWqQtzkbPdqrh_16(Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * value)
	{
		___QEAEcDsIVKSxlRksWqQtzkbPdqrh_16 = value;
		Il2CppCodeGenWriteBarrier((&___QEAEcDsIVKSxlRksWqQtzkbPdqrh_16), value);
	}

	inline static int32_t get_offset_of_ZGtOAcUCAuhJuSLEHTELVSEYNBN_17() { return static_cast<int32_t>(offsetof(qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B, ___ZGtOAcUCAuhJuSLEHTELVSEYNBN_17)); }
	inline Func_1_t30631A63BE46FE93700939B764202D360449FE30 * get_ZGtOAcUCAuhJuSLEHTELVSEYNBN_17() const { return ___ZGtOAcUCAuhJuSLEHTELVSEYNBN_17; }
	inline Func_1_t30631A63BE46FE93700939B764202D360449FE30 ** get_address_of_ZGtOAcUCAuhJuSLEHTELVSEYNBN_17() { return &___ZGtOAcUCAuhJuSLEHTELVSEYNBN_17; }
	inline void set_ZGtOAcUCAuhJuSLEHTELVSEYNBN_17(Func_1_t30631A63BE46FE93700939B764202D360449FE30 * value)
	{
		___ZGtOAcUCAuhJuSLEHTELVSEYNBN_17 = value;
		Il2CppCodeGenWriteBarrier((&___ZGtOAcUCAuhJuSLEHTELVSEYNBN_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QQSKQQVJRXFNPVSJTOMQUZONTRH_T90D9ECA2A0A9866470299ACBD3CED42FB841B98B_H
#ifndef FQPHKOCAEPHJWMNNLBQZDTCJYOSS_T2BF2BA133B1B8F66920F86CAA3DC54E8DAB83F0D_H
#define FQPHKOCAEPHJWMNNLBQZDTCJYOSS_T2BF2BA133B1B8F66920F86CAA3DC54E8DAB83F0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FqPHkOCAePhjWmNNlBQZdTcjYOsS
struct  FqPHkOCAePhjWmNNlBQZdTcjYOsS_t2BF2BA133B1B8F66920F86CAA3DC54E8DAB83F0D 
{
public:
	// System.Int32 FqPHkOCAePhjWmNNlBQZdTcjYOsS::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FqPHkOCAePhjWmNNlBQZdTcjYOsS_t2BF2BA133B1B8F66920F86CAA3DC54E8DAB83F0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FQPHKOCAEPHJWMNNLBQZDTCJYOSS_T2BF2BA133B1B8F66920F86CAA3DC54E8DAB83F0D_H
#ifndef HMKCBNHBWILPJGRZSGXRJAWYBZWJ_TB883F030C68CFF0B7BC4ED35A6CDFEF96AB51CCE_H
#define HMKCBNHBWILPJGRZSGXRJAWYBZWJ_TB883F030C68CFF0B7BC4ED35A6CDFEF96AB51CCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HMkCBnHBWiLpjGRZSgxrJAWYBzwJ
struct  HMkCBnHBWiLpjGRZSgxrJAWYBzwJ_tB883F030C68CFF0B7BC4ED35A6CDFEF96AB51CCE 
{
public:
	// System.Int32 HMkCBnHBWiLpjGRZSgxrJAWYBzwJ::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HMkCBnHBWiLpjGRZSgxrJAWYBzwJ_tB883F030C68CFF0B7BC4ED35A6CDFEF96AB51CCE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMKCBNHBWILPJGRZSGXRJAWYBZWJ_TB883F030C68CFF0B7BC4ED35A6CDFEF96AB51CCE_H
#ifndef ICHXVDPVWHSSBDZLSYETZTMYRDS_TBD87306C8F04931182AF282F823D03CCE7174316_H
#define ICHXVDPVWHSSBDZLSYETZTMYRDS_TBD87306C8F04931182AF282F823D03CCE7174316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IChXVdPvWhSSbdzlSYeTZTMYrdS
struct  IChXVdPvWhSSbdzlSYeTZTMYrdS_tBD87306C8F04931182AF282F823D03CCE7174316 
{
public:
	// System.Int32 IChXVdPvWhSSbdzlSYeTZTMYrdS::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IChXVdPvWhSSbdzlSYeTZTMYrdS_tBD87306C8F04931182AF282F823D03CCE7174316, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICHXVDPVWHSSBDZLSYETZTMYRDS_TBD87306C8F04931182AF282F823D03CCE7174316_H
#ifndef MBVFZMIEGEGUKQUJBEKKBFRRYOF_TAAAE57B3F8C1999A05711B9BAD359271AC95FBAC_H
#define MBVFZMIEGEGUKQUJBEKKBFRRYOF_TAAAE57B3F8C1999A05711B9BAD359271AC95FBAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MBvfZmieGEGukQujbekkbfrRyOF
struct  MBvfZmieGEGukQujbekkbfrRyOF_tAAAE57B3F8C1999A05711B9BAD359271AC95FBAC 
{
public:
	// System.Byte MBvfZmieGEGukQujbekkbfrRyOF::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MBvfZmieGEGukQujbekkbfrRyOF_tAAAE57B3F8C1999A05711B9BAD359271AC95FBAC, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MBVFZMIEGEGUKQUJBEKKBFRRYOF_TAAAE57B3F8C1999A05711B9BAD359271AC95FBAC_H
#ifndef AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#define AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisRange
struct  AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652 
{
public:
	// System.Int32 Rewired.AxisRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifndef CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#define CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerElementType
struct  ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA 
{
public:
	// System.Int32 Rewired.ControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifndef CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#define CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerType
struct  ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4 
{
public:
	// System.Int32 Rewired.ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifndef MNWYGNQCNSFAUAALHKVZJHPOQICX_TBBC8F63C8A5D931070FEC280ABB619D506033D2C_H
#define MNWYGNQCNSFAUAALHKVZJHPOQICX_TBBC8F63C8A5D931070FEC280ABB619D506033D2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.Tools.DebugInformation_MnwygNQCnSFAUaAlhkvZjhpOqiCX
struct  MnwygNQCnSFAUaAlhkvZjhpOqiCX_tBBC8F63C8A5D931070FEC280ABB619D506033D2C 
{
public:
	// System.Int32 Rewired.Dev.Tools.DebugInformation_MnwygNQCnSFAUaAlhkvZjhpOqiCX::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MnwygNQCnSFAUaAlhkvZjhpOqiCX_tBBC8F63C8A5D931070FEC280ABB619D506033D2C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MNWYGNQCNSFAUAALHKVZJHPOQICX_TBBC8F63C8A5D931070FEC280ABB619D506033D2C_H
#ifndef ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#define ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentType
struct  ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394 
{
public:
	// System.Int32 Rewired.ElementAssignmentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#ifndef MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#define MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKeyFlags
struct  ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651 
{
public:
	// System.Int32 Rewired.ModifierKeyFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifndef PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#define PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Platform
struct  Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD 
{
public:
	// System.Int32 Rewired.Platforms.Platform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#ifndef POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#define POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Pole
struct  Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C 
{
public:
	// System.Int32 Rewired.Pole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifndef UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#define UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UpdateLoopType
struct  UpdateLoopType_t407C8154C4B63048CA3DC1A59E6458956BC31F0A 
{
public:
	// System.Int32 Rewired.UpdateLoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateLoopType_t407C8154C4B63048CA3DC1A59E6458956BC31F0A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#ifndef INTPTRWRAPPER_TC0106F75B7B197945867CAE3172E1F8D6419CDF7_H
#define INTPTRWRAPPER_TC0106F75B7B197945867CAE3172E1F8D6419CDF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.Data.IntPtrWrapper
struct  IntPtrWrapper_tC0106F75B7B197945867CAE3172E1F8D6419CDF7  : public RuntimeObject
{
public:
	// System.IntPtr Rewired.Utils.Classes.Data.IntPtrWrapper::oGMSBvcDAaFiWyWelymrQnRYvlG
	intptr_t ___oGMSBvcDAaFiWyWelymrQnRYvlG_0;

public:
	inline static int32_t get_offset_of_oGMSBvcDAaFiWyWelymrQnRYvlG_0() { return static_cast<int32_t>(offsetof(IntPtrWrapper_tC0106F75B7B197945867CAE3172E1F8D6419CDF7, ___oGMSBvcDAaFiWyWelymrQnRYvlG_0)); }
	inline intptr_t get_oGMSBvcDAaFiWyWelymrQnRYvlG_0() const { return ___oGMSBvcDAaFiWyWelymrQnRYvlG_0; }
	inline intptr_t* get_address_of_oGMSBvcDAaFiWyWelymrQnRYvlG_0() { return &___oGMSBvcDAaFiWyWelymrQnRYvlG_0; }
	inline void set_oGMSBvcDAaFiWyWelymrQnRYvlG_0(intptr_t value)
	{
		___oGMSBvcDAaFiWyWelymrQnRYvlG_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTRWRAPPER_TC0106F75B7B197945867CAE3172E1F8D6419CDF7_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef CALLINGCONVENTION_T1CA20C42BA91F62017CDE4192C0FF930E81A1193_H
#define CALLINGCONVENTION_T1CA20C42BA91F62017CDE4192C0FF930E81A1193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.CallingConvention
struct  CallingConvention_t1CA20C42BA91F62017CDE4192C0FF930E81A1193 
{
public:
	// System.Int32 System.Runtime.InteropServices.CallingConvention::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CallingConvention_t1CA20C42BA91F62017CDE4192C0FF930E81A1193, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLINGCONVENTION_T1CA20C42BA91F62017CDE4192C0FF930E81A1193_H
#ifndef CHARSET_T2DEB2DA03477AAFC8FD2E1EC33F49904F85632A5_H
#define CHARSET_T2DEB2DA03477AAFC8FD2E1EC33F49904F85632A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.CharSet
struct  CharSet_t2DEB2DA03477AAFC8FD2E1EC33F49904F85632A5 
{
public:
	// System.Int32 System.Runtime.InteropServices.CharSet::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharSet_t2DEB2DA03477AAFC8FD2E1EC33F49904F85632A5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARSET_T2DEB2DA03477AAFC8FD2E1EC33F49904F85632A5_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef HVOPYEEANXJLKUZFNVDURXVDBHT_T95A50A2BC62012F0E55DB118812809287F428C01_H
#define HVOPYEEANXJLKUZFNVDURXVDBHT_T95A50A2BC62012F0E55DB118812809287F428C01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_HVopYEEANXJLkUzfnvduRxvDbht
struct  HVopYEEANXJLkUzfnvduRxvDbht_t95A50A2BC62012F0E55DB118812809287F428C01 
{
public:
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_HVopYEEANXJLkUzfnvduRxvDbht::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HVopYEEANXJLkUzfnvduRxvDbht_t95A50A2BC62012F0E55DB118812809287F428C01, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HVOPYEEANXJLKUZFNVDURXVDBHT_T95A50A2BC62012F0E55DB118812809287F428C01_H
#ifndef LHFCKESNXYGGEJIJUHTBHPGXXFM_TC5E7CAB207379A9CC35EC5CE2E58CC202232E31A_H
#define LHFCKESNXYGGEJIJUHTBHPGXXFM_TC5E7CAB207379A9CC35EC5CE2E58CC202232E31A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_LHFcKEsNXygGejIjuHtBhpgXxFm
struct  LHFcKEsNXygGejIjuHtBhpgXxFm_tC5E7CAB207379A9CC35EC5CE2E58CC202232E31A 
{
public:
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_LHFcKEsNXygGejIjuHtBhpgXxFm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LHFcKEsNXygGejIjuHtBhpgXxFm_tC5E7CAB207379A9CC35EC5CE2E58CC202232E31A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LHFCKESNXYGGEJIJUHTBHPGXXFM_TC5E7CAB207379A9CC35EC5CE2E58CC202232E31A_H
#ifndef NOOBESRFGOQBCHXHXKMTXHHJGAS_TF15A216F8E24D6E9AE3A08910336666E8F1E9777_H
#define NOOBESRFGOQBCHXHXKMTXHHJGAS_TF15A216F8E24D6E9AE3A08910336666E8F1E9777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas
struct  nOobeSrFgOqBcHXHXKMtXHhJGas_tF15A216F8E24D6E9AE3A08910336666E8F1E9777 
{
public:
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(nOobeSrFgOqBcHXHXKMtXHhJGas_tF15A216F8E24D6E9AE3A08910336666E8F1E9777, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOOBESRFGOQBCHXHXKMTXHHJGAS_TF15A216F8E24D6E9AE3A08910336666E8F1E9777_H
#ifndef JPODCKCKEIWEHEVGKKRPQYJNEZI_T762B967C0BA200484BEAB356DDE26EBC18B7D25E_H
#define JPODCKCKEIWEHEVGKKRPQYJNEZI_T762B967C0BA200484BEAB356DDE26EBC18B7D25E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// jpOdckCKEIweHeVgKkrPQyjNezi
struct  jpOdckCKEIweHeVgKkrPQyjNezi_t762B967C0BA200484BEAB356DDE26EBC18B7D25E 
{
public:
	// System.Int32 jpOdckCKEIweHeVgKkrPQyjNezi::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(jpOdckCKEIweHeVgKkrPQyjNezi_t762B967C0BA200484BEAB356DDE26EBC18B7D25E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPODCKCKEIWEHEVGKKRPQYJNEZI_T762B967C0BA200484BEAB356DDE26EBC18B7D25E_H
#ifndef OWCSOBMYSTIRVNUHFFMJMFLDZEZ_TA61D69AED297CF98D852CC503479D02181B74628_H
#define OWCSOBMYSTIRVNUHFFMJMFLDZEZ_TA61D69AED297CF98D852CC503479D02181B74628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// owCsobmYStIRvNUHffmjMFLdZEz
struct  owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628  : public RuntimeObject
{
public:
	// System.Int32 owCsobmYStIRvNUHffmjMFLdZEz::JTlYHbNuRvgezaodmDszddIQvhCD
	int32_t ___JTlYHbNuRvgezaodmDszddIQvhCD_0;
	// System.Int32 owCsobmYStIRvNUHffmjMFLdZEz::hMBbQHSYpxrZxtjFPBFvFGsQNmg
	int32_t ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_1;
	// System.Boolean owCsobmYStIRvNUHffmjMFLdZEz::MSRWTMYePuQljcKSSgqgZVJnxaO
	bool ___MSRWTMYePuQljcKSSgqgZVJnxaO_2;
	// System.String owCsobmYStIRvNUHffmjMFLdZEz::PILNSwvruoIylrlWZVyEHrlXRBt
	String_t* ___PILNSwvruoIylrlWZVyEHrlXRBt_3;
	// System.String owCsobmYStIRvNUHffmjMFLdZEz::zTBxQtRhsiyWjlXakgtukSbwLlp
	String_t* ___zTBxQtRhsiyWjlXakgtukSbwLlp_4;
	// System.Guid owCsobmYStIRvNUHffmjMFLdZEz::RZDzxZHLcHGhqfWjRESXQZBgUPw
	Guid_t  ___RZDzxZHLcHGhqfWjRESXQZBgUPw_5;
	// System.Guid owCsobmYStIRvNUHffmjMFLdZEz::DyFaiKFDPJYHiKaiDwSFsqplZQr
	Guid_t  ___DyFaiKFDPJYHiKaiDwSFsqplZQr_6;
	// System.Int32 owCsobmYStIRvNUHffmjMFLdZEz::tOaTpOwzcPDCHdCDIyYaeuASgAM
	int32_t ___tOaTpOwzcPDCHdCDIyYaeuASgAM_7;
	// System.Int32 owCsobmYStIRvNUHffmjMFLdZEz::itTafNyeQoucrbhPkPsSqRewAEI
	int32_t ___itTafNyeQoucrbhPkPsSqRewAEI_8;
	// System.Int32 owCsobmYStIRvNUHffmjMFLdZEz::ZWYAwyuulqYogftJTkDBnLhijEo
	int32_t ___ZWYAwyuulqYogftJTkDBnLhijEo_9;
	// System.Int32 owCsobmYStIRvNUHffmjMFLdZEz::BQhIlyGtJuwhVjMyFqaFVZMDOIY
	int32_t ___BQhIlyGtJuwhVjMyFqaFVZMDOIY_10;
	// Rewired.PidVid owCsobmYStIRvNUHffmjMFLdZEz::yauaFjfmZQzNovSFRGthYyFJrrj
	PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D  ___yauaFjfmZQzNovSFRGthYyFJrrj_11;
	// System.Guid owCsobmYStIRvNUHffmjMFLdZEz::nlEaopEverUmqALsSlwZYfPupTlP
	Guid_t  ___nlEaopEverUmqALsSlwZYfPupTlP_12;
	// System.Int32 owCsobmYStIRvNUHffmjMFLdZEz::qNNdzuExLuNfrdyOzaNcsNVrpIQc
	int32_t ___qNNdzuExLuNfrdyOzaNcsNVrpIQc_13;
	// System.Int32 owCsobmYStIRvNUHffmjMFLdZEz::KclFaRHPVyrRHSfCxDIVgvgADJgN
	int32_t ___KclFaRHPVyrRHSfCxDIVgvgADJgN_14;

public:
	inline static int32_t get_offset_of_JTlYHbNuRvgezaodmDszddIQvhCD_0() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___JTlYHbNuRvgezaodmDszddIQvhCD_0)); }
	inline int32_t get_JTlYHbNuRvgezaodmDszddIQvhCD_0() const { return ___JTlYHbNuRvgezaodmDszddIQvhCD_0; }
	inline int32_t* get_address_of_JTlYHbNuRvgezaodmDszddIQvhCD_0() { return &___JTlYHbNuRvgezaodmDszddIQvhCD_0; }
	inline void set_JTlYHbNuRvgezaodmDszddIQvhCD_0(int32_t value)
	{
		___JTlYHbNuRvgezaodmDszddIQvhCD_0 = value;
	}

	inline static int32_t get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_1() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_1)); }
	inline int32_t get_hMBbQHSYpxrZxtjFPBFvFGsQNmg_1() const { return ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_1; }
	inline int32_t* get_address_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_1() { return &___hMBbQHSYpxrZxtjFPBFvFGsQNmg_1; }
	inline void set_hMBbQHSYpxrZxtjFPBFvFGsQNmg_1(int32_t value)
	{
		___hMBbQHSYpxrZxtjFPBFvFGsQNmg_1 = value;
	}

	inline static int32_t get_offset_of_MSRWTMYePuQljcKSSgqgZVJnxaO_2() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___MSRWTMYePuQljcKSSgqgZVJnxaO_2)); }
	inline bool get_MSRWTMYePuQljcKSSgqgZVJnxaO_2() const { return ___MSRWTMYePuQljcKSSgqgZVJnxaO_2; }
	inline bool* get_address_of_MSRWTMYePuQljcKSSgqgZVJnxaO_2() { return &___MSRWTMYePuQljcKSSgqgZVJnxaO_2; }
	inline void set_MSRWTMYePuQljcKSSgqgZVJnxaO_2(bool value)
	{
		___MSRWTMYePuQljcKSSgqgZVJnxaO_2 = value;
	}

	inline static int32_t get_offset_of_PILNSwvruoIylrlWZVyEHrlXRBt_3() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___PILNSwvruoIylrlWZVyEHrlXRBt_3)); }
	inline String_t* get_PILNSwvruoIylrlWZVyEHrlXRBt_3() const { return ___PILNSwvruoIylrlWZVyEHrlXRBt_3; }
	inline String_t** get_address_of_PILNSwvruoIylrlWZVyEHrlXRBt_3() { return &___PILNSwvruoIylrlWZVyEHrlXRBt_3; }
	inline void set_PILNSwvruoIylrlWZVyEHrlXRBt_3(String_t* value)
	{
		___PILNSwvruoIylrlWZVyEHrlXRBt_3 = value;
		Il2CppCodeGenWriteBarrier((&___PILNSwvruoIylrlWZVyEHrlXRBt_3), value);
	}

	inline static int32_t get_offset_of_zTBxQtRhsiyWjlXakgtukSbwLlp_4() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___zTBxQtRhsiyWjlXakgtukSbwLlp_4)); }
	inline String_t* get_zTBxQtRhsiyWjlXakgtukSbwLlp_4() const { return ___zTBxQtRhsiyWjlXakgtukSbwLlp_4; }
	inline String_t** get_address_of_zTBxQtRhsiyWjlXakgtukSbwLlp_4() { return &___zTBxQtRhsiyWjlXakgtukSbwLlp_4; }
	inline void set_zTBxQtRhsiyWjlXakgtukSbwLlp_4(String_t* value)
	{
		___zTBxQtRhsiyWjlXakgtukSbwLlp_4 = value;
		Il2CppCodeGenWriteBarrier((&___zTBxQtRhsiyWjlXakgtukSbwLlp_4), value);
	}

	inline static int32_t get_offset_of_RZDzxZHLcHGhqfWjRESXQZBgUPw_5() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___RZDzxZHLcHGhqfWjRESXQZBgUPw_5)); }
	inline Guid_t  get_RZDzxZHLcHGhqfWjRESXQZBgUPw_5() const { return ___RZDzxZHLcHGhqfWjRESXQZBgUPw_5; }
	inline Guid_t * get_address_of_RZDzxZHLcHGhqfWjRESXQZBgUPw_5() { return &___RZDzxZHLcHGhqfWjRESXQZBgUPw_5; }
	inline void set_RZDzxZHLcHGhqfWjRESXQZBgUPw_5(Guid_t  value)
	{
		___RZDzxZHLcHGhqfWjRESXQZBgUPw_5 = value;
	}

	inline static int32_t get_offset_of_DyFaiKFDPJYHiKaiDwSFsqplZQr_6() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___DyFaiKFDPJYHiKaiDwSFsqplZQr_6)); }
	inline Guid_t  get_DyFaiKFDPJYHiKaiDwSFsqplZQr_6() const { return ___DyFaiKFDPJYHiKaiDwSFsqplZQr_6; }
	inline Guid_t * get_address_of_DyFaiKFDPJYHiKaiDwSFsqplZQr_6() { return &___DyFaiKFDPJYHiKaiDwSFsqplZQr_6; }
	inline void set_DyFaiKFDPJYHiKaiDwSFsqplZQr_6(Guid_t  value)
	{
		___DyFaiKFDPJYHiKaiDwSFsqplZQr_6 = value;
	}

	inline static int32_t get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_7() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___tOaTpOwzcPDCHdCDIyYaeuASgAM_7)); }
	inline int32_t get_tOaTpOwzcPDCHdCDIyYaeuASgAM_7() const { return ___tOaTpOwzcPDCHdCDIyYaeuASgAM_7; }
	inline int32_t* get_address_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_7() { return &___tOaTpOwzcPDCHdCDIyYaeuASgAM_7; }
	inline void set_tOaTpOwzcPDCHdCDIyYaeuASgAM_7(int32_t value)
	{
		___tOaTpOwzcPDCHdCDIyYaeuASgAM_7 = value;
	}

	inline static int32_t get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_8() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___itTafNyeQoucrbhPkPsSqRewAEI_8)); }
	inline int32_t get_itTafNyeQoucrbhPkPsSqRewAEI_8() const { return ___itTafNyeQoucrbhPkPsSqRewAEI_8; }
	inline int32_t* get_address_of_itTafNyeQoucrbhPkPsSqRewAEI_8() { return &___itTafNyeQoucrbhPkPsSqRewAEI_8; }
	inline void set_itTafNyeQoucrbhPkPsSqRewAEI_8(int32_t value)
	{
		___itTafNyeQoucrbhPkPsSqRewAEI_8 = value;
	}

	inline static int32_t get_offset_of_ZWYAwyuulqYogftJTkDBnLhijEo_9() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___ZWYAwyuulqYogftJTkDBnLhijEo_9)); }
	inline int32_t get_ZWYAwyuulqYogftJTkDBnLhijEo_9() const { return ___ZWYAwyuulqYogftJTkDBnLhijEo_9; }
	inline int32_t* get_address_of_ZWYAwyuulqYogftJTkDBnLhijEo_9() { return &___ZWYAwyuulqYogftJTkDBnLhijEo_9; }
	inline void set_ZWYAwyuulqYogftJTkDBnLhijEo_9(int32_t value)
	{
		___ZWYAwyuulqYogftJTkDBnLhijEo_9 = value;
	}

	inline static int32_t get_offset_of_BQhIlyGtJuwhVjMyFqaFVZMDOIY_10() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___BQhIlyGtJuwhVjMyFqaFVZMDOIY_10)); }
	inline int32_t get_BQhIlyGtJuwhVjMyFqaFVZMDOIY_10() const { return ___BQhIlyGtJuwhVjMyFqaFVZMDOIY_10; }
	inline int32_t* get_address_of_BQhIlyGtJuwhVjMyFqaFVZMDOIY_10() { return &___BQhIlyGtJuwhVjMyFqaFVZMDOIY_10; }
	inline void set_BQhIlyGtJuwhVjMyFqaFVZMDOIY_10(int32_t value)
	{
		___BQhIlyGtJuwhVjMyFqaFVZMDOIY_10 = value;
	}

	inline static int32_t get_offset_of_yauaFjfmZQzNovSFRGthYyFJrrj_11() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___yauaFjfmZQzNovSFRGthYyFJrrj_11)); }
	inline PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D  get_yauaFjfmZQzNovSFRGthYyFJrrj_11() const { return ___yauaFjfmZQzNovSFRGthYyFJrrj_11; }
	inline PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D * get_address_of_yauaFjfmZQzNovSFRGthYyFJrrj_11() { return &___yauaFjfmZQzNovSFRGthYyFJrrj_11; }
	inline void set_yauaFjfmZQzNovSFRGthYyFJrrj_11(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D  value)
	{
		___yauaFjfmZQzNovSFRGthYyFJrrj_11 = value;
	}

	inline static int32_t get_offset_of_nlEaopEverUmqALsSlwZYfPupTlP_12() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___nlEaopEverUmqALsSlwZYfPupTlP_12)); }
	inline Guid_t  get_nlEaopEverUmqALsSlwZYfPupTlP_12() const { return ___nlEaopEverUmqALsSlwZYfPupTlP_12; }
	inline Guid_t * get_address_of_nlEaopEverUmqALsSlwZYfPupTlP_12() { return &___nlEaopEverUmqALsSlwZYfPupTlP_12; }
	inline void set_nlEaopEverUmqALsSlwZYfPupTlP_12(Guid_t  value)
	{
		___nlEaopEverUmqALsSlwZYfPupTlP_12 = value;
	}

	inline static int32_t get_offset_of_qNNdzuExLuNfrdyOzaNcsNVrpIQc_13() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___qNNdzuExLuNfrdyOzaNcsNVrpIQc_13)); }
	inline int32_t get_qNNdzuExLuNfrdyOzaNcsNVrpIQc_13() const { return ___qNNdzuExLuNfrdyOzaNcsNVrpIQc_13; }
	inline int32_t* get_address_of_qNNdzuExLuNfrdyOzaNcsNVrpIQc_13() { return &___qNNdzuExLuNfrdyOzaNcsNVrpIQc_13; }
	inline void set_qNNdzuExLuNfrdyOzaNcsNVrpIQc_13(int32_t value)
	{
		___qNNdzuExLuNfrdyOzaNcsNVrpIQc_13 = value;
	}

	inline static int32_t get_offset_of_KclFaRHPVyrRHSfCxDIVgvgADJgN_14() { return static_cast<int32_t>(offsetof(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628, ___KclFaRHPVyrRHSfCxDIVgvgADJgN_14)); }
	inline int32_t get_KclFaRHPVyrRHSfCxDIVgvgADJgN_14() const { return ___KclFaRHPVyrRHSfCxDIVgvgADJgN_14; }
	inline int32_t* get_address_of_KclFaRHPVyrRHSfCxDIVgvgADJgN_14() { return &___KclFaRHPVyrRHSfCxDIVgvgADJgN_14; }
	inline void set_KclFaRHPVyrRHSfCxDIVgvgADJgN_14(int32_t value)
	{
		___KclFaRHPVyrRHSfCxDIVgvgADJgN_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OWCSOBMYSTIRVNUHFFMJMFLDZEZ_TA61D69AED297CF98D852CC503479D02181B74628_H
#ifndef QNHIHBNYGCBSFOWNHMJVNVYTPXM_T3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4_H
#define QNHIHBNYGCBSFOWNHMJVNVYTPXM_T3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM
struct  QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4  : public RuntimeObject
{
public:
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::nvorGNbxnIKRTEQakQRxoDaDbkF
	int32_t ___nvorGNbxnIKRTEQakQRxoDaDbkF_0;
	// System.Guid qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::nlEaopEverUmqALsSlwZYfPupTlP
	Guid_t  ___nlEaopEverUmqALsSlwZYfPupTlP_1;
	// System.Guid qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::bLJDeZdmqhplurpuKUnpKCpNysd
	Guid_t  ___bLJDeZdmqhplurpuKUnpKCpNysd_2;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::fYYSPmKTkyNnvcLPwwaCPYkcWqv
	int32_t ___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::UdWYvkBYkFbtQVrPZwZuKINGjC
	int32_t ___UdWYvkBYkFbtQVrPZwZuKINGjC_4;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::VYSASJjqEIsmJcmnIHADUYEaRjQC
	int32_t ___VYSASJjqEIsmJcmnIHADUYEaRjQC_5;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_QnHiHbnYGCBSFowNhmjvNvYtpXM::aWhbUpAtiftmjIGzRSoPwmEnzb
	int32_t ___aWhbUpAtiftmjIGzRSoPwmEnzb_6;

public:
	inline static int32_t get_offset_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___nvorGNbxnIKRTEQakQRxoDaDbkF_0)); }
	inline int32_t get_nvorGNbxnIKRTEQakQRxoDaDbkF_0() const { return ___nvorGNbxnIKRTEQakQRxoDaDbkF_0; }
	inline int32_t* get_address_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0() { return &___nvorGNbxnIKRTEQakQRxoDaDbkF_0; }
	inline void set_nvorGNbxnIKRTEQakQRxoDaDbkF_0(int32_t value)
	{
		___nvorGNbxnIKRTEQakQRxoDaDbkF_0 = value;
	}

	inline static int32_t get_offset_of_nlEaopEverUmqALsSlwZYfPupTlP_1() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___nlEaopEverUmqALsSlwZYfPupTlP_1)); }
	inline Guid_t  get_nlEaopEverUmqALsSlwZYfPupTlP_1() const { return ___nlEaopEverUmqALsSlwZYfPupTlP_1; }
	inline Guid_t * get_address_of_nlEaopEverUmqALsSlwZYfPupTlP_1() { return &___nlEaopEverUmqALsSlwZYfPupTlP_1; }
	inline void set_nlEaopEverUmqALsSlwZYfPupTlP_1(Guid_t  value)
	{
		___nlEaopEverUmqALsSlwZYfPupTlP_1 = value;
	}

	inline static int32_t get_offset_of_bLJDeZdmqhplurpuKUnpKCpNysd_2() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___bLJDeZdmqhplurpuKUnpKCpNysd_2)); }
	inline Guid_t  get_bLJDeZdmqhplurpuKUnpKCpNysd_2() const { return ___bLJDeZdmqhplurpuKUnpKCpNysd_2; }
	inline Guid_t * get_address_of_bLJDeZdmqhplurpuKUnpKCpNysd_2() { return &___bLJDeZdmqhplurpuKUnpKCpNysd_2; }
	inline void set_bLJDeZdmqhplurpuKUnpKCpNysd_2(Guid_t  value)
	{
		___bLJDeZdmqhplurpuKUnpKCpNysd_2 = value;
	}

	inline static int32_t get_offset_of_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3)); }
	inline int32_t get_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3() const { return ___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3; }
	inline int32_t* get_address_of_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3() { return &___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3; }
	inline void set_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3(int32_t value)
	{
		___fYYSPmKTkyNnvcLPwwaCPYkcWqv_3 = value;
	}

	inline static int32_t get_offset_of_UdWYvkBYkFbtQVrPZwZuKINGjC_4() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___UdWYvkBYkFbtQVrPZwZuKINGjC_4)); }
	inline int32_t get_UdWYvkBYkFbtQVrPZwZuKINGjC_4() const { return ___UdWYvkBYkFbtQVrPZwZuKINGjC_4; }
	inline int32_t* get_address_of_UdWYvkBYkFbtQVrPZwZuKINGjC_4() { return &___UdWYvkBYkFbtQVrPZwZuKINGjC_4; }
	inline void set_UdWYvkBYkFbtQVrPZwZuKINGjC_4(int32_t value)
	{
		___UdWYvkBYkFbtQVrPZwZuKINGjC_4 = value;
	}

	inline static int32_t get_offset_of_VYSASJjqEIsmJcmnIHADUYEaRjQC_5() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___VYSASJjqEIsmJcmnIHADUYEaRjQC_5)); }
	inline int32_t get_VYSASJjqEIsmJcmnIHADUYEaRjQC_5() const { return ___VYSASJjqEIsmJcmnIHADUYEaRjQC_5; }
	inline int32_t* get_address_of_VYSASJjqEIsmJcmnIHADUYEaRjQC_5() { return &___VYSASJjqEIsmJcmnIHADUYEaRjQC_5; }
	inline void set_VYSASJjqEIsmJcmnIHADUYEaRjQC_5(int32_t value)
	{
		___VYSASJjqEIsmJcmnIHADUYEaRjQC_5 = value;
	}

	inline static int32_t get_offset_of_aWhbUpAtiftmjIGzRSoPwmEnzb_6() { return static_cast<int32_t>(offsetof(QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4, ___aWhbUpAtiftmjIGzRSoPwmEnzb_6)); }
	inline int32_t get_aWhbUpAtiftmjIGzRSoPwmEnzb_6() const { return ___aWhbUpAtiftmjIGzRSoPwmEnzb_6; }
	inline int32_t* get_address_of_aWhbUpAtiftmjIGzRSoPwmEnzb_6() { return &___aWhbUpAtiftmjIGzRSoPwmEnzb_6; }
	inline void set_aWhbUpAtiftmjIGzRSoPwmEnzb_6(int32_t value)
	{
		___aWhbUpAtiftmjIGzRSoPwmEnzb_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QNHIHBNYGCBSFOWNHMJVNVYTPXM_T3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4_H
#ifndef ZSSAVPFNUYBPGVXQQMQBVEPVQJCO_T2FC14182EE0D01A0915FBE74BC6FE5FE071F7B68_H
#define ZSSAVPFNUYBPGVXQQMQBVEPVQJCO_T2FC14182EE0D01A0915FBE74BC6FE5FE071F7B68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_zSsAVpfNUYBPgVxqQMqbVEPVqJCO
struct  zSsAVpfNUYBPgVxqQMqbVEPVqJCO_t2FC14182EE0D01A0915FBE74BC6FE5FE071F7B68 
{
public:
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_tLqZqETDWvmFaUyFhoSscPGCHa_zSsAVpfNUYBPgVxqQMqbVEPVqJCO::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(zSsAVpfNUYBPgVxqQMqbVEPVqJCO_t2FC14182EE0D01A0915FBE74BC6FE5FE071F7B68, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZSSAVPFNUYBPGVXQQMQBVEPVQJCO_T2FC14182EE0D01A0915FBE74BC6FE5FE071F7B68_H
#ifndef QGJNESSSNZSLMPHWBTXQPCMNOWQ_T763F7594985FF852608F0BE4356621CC5A42DE8D_H
#define QGJNESSSNZSLMPHWBTXQPCMNOWQ_T763F7594985FF852608F0BE4356621CC5A42DE8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// qgjNESsSnzSlMpHwBtxQPCMNOwq
struct  qgjNESsSnzSlMpHwBtxQPCMNOwq_t763F7594985FF852608F0BE4356621CC5A42DE8D 
{
public:
	// System.Int32 qgjNESsSnzSlMpHwBtxQPCMNOwq::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(qgjNESsSnzSlMpHwBtxQPCMNOwq_t763F7594985FF852608F0BE4356621CC5A42DE8D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QGJNESSSNZSLMPHWBTXQPCMNOWQ_T763F7594985FF852608F0BE4356621CC5A42DE8D_H
#ifndef YDIMZUFFVMUNTOIIJEYTBOMBALJ_TD984A3F451D6EC1352F9810F5C89709BB0F53387_H
#define YDIMZUFFVMUNTOIIJEYTBOMBALJ_TD984A3F451D6EC1352F9810F5C89709BB0F53387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ydiMzuFfVMUntoIIJEytBOmbAlJ
struct  ydiMzuFfVMUntoIIJEytBOmbAlJ_tD984A3F451D6EC1352F9810F5C89709BB0F53387 
{
public:
	// System.Int32 ydiMzuFfVMUntoIIJEytBOmbAlJ::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ydiMzuFfVMUntoIIJEytBOmbAlJ_tD984A3F451D6EC1352F9810F5C89709BB0F53387, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YDIMZUFFVMUNTOIIJEYTBOMBALJ_TD984A3F451D6EC1352F9810F5C89709BB0F53387_H
#ifndef EACQNCMRBXTYSFOTAFWFQDQVLIS_T6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0_H
#define EACQNCMRBXTYSFOTAFWFQDQVLIS_T6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EaCqncmrbxTySfotaFWfqDQVLIS
struct  EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0  : public RuntimeObject
{
public:
	// Rewired.UpdateLoopType EaCqncmrbxTySfotaFWfqDQVLIS::YIgjjtvsNpmLAzEahZSayMqwKAO
	int32_t ___YIgjjtvsNpmLAzEahZSayMqwKAO_0;
	// EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs EaCqncmrbxTySfotaFWfqDQVLIS::FXaTAHAQGPmajGVCvwXILbcNwbj
	FOBdOrKrxGfXvkOuUmdtNzedDCs_t3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309 * ___FXaTAHAQGPmajGVCvwXILbcNwbj_1;
	// Rewired.Utils.Classes.Data.IndexedDictionary`2<System.Int32,EaCqncmrbxTySfotaFWfqDQVLIS_FOBdOrKrxGfXvkOuUmdtNzedDCs> EaCqncmrbxTySfotaFWfqDQVLIS::HWmOewXpuebOsZDStBejWuBUGqK
	IndexedDictionary_2_t4D8816C0C74DC28005A0D828B26C87A58FA95C04 * ___HWmOewXpuebOsZDStBejWuBUGqK_2;

public:
	inline static int32_t get_offset_of_YIgjjtvsNpmLAzEahZSayMqwKAO_0() { return static_cast<int32_t>(offsetof(EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0, ___YIgjjtvsNpmLAzEahZSayMqwKAO_0)); }
	inline int32_t get_YIgjjtvsNpmLAzEahZSayMqwKAO_0() const { return ___YIgjjtvsNpmLAzEahZSayMqwKAO_0; }
	inline int32_t* get_address_of_YIgjjtvsNpmLAzEahZSayMqwKAO_0() { return &___YIgjjtvsNpmLAzEahZSayMqwKAO_0; }
	inline void set_YIgjjtvsNpmLAzEahZSayMqwKAO_0(int32_t value)
	{
		___YIgjjtvsNpmLAzEahZSayMqwKAO_0 = value;
	}

	inline static int32_t get_offset_of_FXaTAHAQGPmajGVCvwXILbcNwbj_1() { return static_cast<int32_t>(offsetof(EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0, ___FXaTAHAQGPmajGVCvwXILbcNwbj_1)); }
	inline FOBdOrKrxGfXvkOuUmdtNzedDCs_t3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309 * get_FXaTAHAQGPmajGVCvwXILbcNwbj_1() const { return ___FXaTAHAQGPmajGVCvwXILbcNwbj_1; }
	inline FOBdOrKrxGfXvkOuUmdtNzedDCs_t3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309 ** get_address_of_FXaTAHAQGPmajGVCvwXILbcNwbj_1() { return &___FXaTAHAQGPmajGVCvwXILbcNwbj_1; }
	inline void set_FXaTAHAQGPmajGVCvwXILbcNwbj_1(FOBdOrKrxGfXvkOuUmdtNzedDCs_t3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309 * value)
	{
		___FXaTAHAQGPmajGVCvwXILbcNwbj_1 = value;
		Il2CppCodeGenWriteBarrier((&___FXaTAHAQGPmajGVCvwXILbcNwbj_1), value);
	}

	inline static int32_t get_offset_of_HWmOewXpuebOsZDStBejWuBUGqK_2() { return static_cast<int32_t>(offsetof(EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0, ___HWmOewXpuebOsZDStBejWuBUGqK_2)); }
	inline IndexedDictionary_2_t4D8816C0C74DC28005A0D828B26C87A58FA95C04 * get_HWmOewXpuebOsZDStBejWuBUGqK_2() const { return ___HWmOewXpuebOsZDStBejWuBUGqK_2; }
	inline IndexedDictionary_2_t4D8816C0C74DC28005A0D828B26C87A58FA95C04 ** get_address_of_HWmOewXpuebOsZDStBejWuBUGqK_2() { return &___HWmOewXpuebOsZDStBejWuBUGqK_2; }
	inline void set_HWmOewXpuebOsZDStBejWuBUGqK_2(IndexedDictionary_2_t4D8816C0C74DC28005A0D828B26C87A58FA95C04 * value)
	{
		___HWmOewXpuebOsZDStBejWuBUGqK_2 = value;
		Il2CppCodeGenWriteBarrier((&___HWmOewXpuebOsZDStBejWuBUGqK_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EACQNCMRBXTYSFOTAFWFQDQVLIS_T6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0_H
#ifndef KDICNAKNSUNVLIMEPNXDAECCLSR_TA3DD4DB1AB688F1291A2B02EC7E3CFD5A0829709_H
#define KDICNAKNSUNVLIMEPNXDAECCLSR_TA3DD4DB1AB688F1291A2B02EC7E3CFD5A0829709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KDIcNAKNsuNvLiMepNxDAEccLSR
struct  KDIcNAKNsuNvLiMepNxDAEccLSR_tA3DD4DB1AB688F1291A2B02EC7E3CFD5A0829709  : public IntPtrWrapper_tC0106F75B7B197945867CAE3172E1F8D6419CDF7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KDICNAKNSUNVLIMEPNXDAECCLSR_TA3DD4DB1AB688F1291A2B02EC7E3CFD5A0829709_H
#ifndef OBSEVQMYNUNRMIEJCOVEEISBRKY_T344CF2A67444B48D7C343EFAE6FA77658C73D533_H
#define OBSEVQMYNUNRMIEJCOVEEISBRKY_T344CF2A67444B48D7C343EFAE6FA77658C73D533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OBseVQmynUNRMiejCOveEisbrKY
struct  OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533  : public RuntimeObject
{
public:
	// KDIcNAKNsuNvLiMepNxDAEccLSR OBseVQmynUNRMiejCOveEisbrKY::sUZJrREVbDgnurDWKIIUlQKqWwq
	KDIcNAKNsuNvLiMepNxDAEccLSR_tA3DD4DB1AB688F1291A2B02EC7E3CFD5A0829709 * ___sUZJrREVbDgnurDWKIIUlQKqWwq_0;
	// ydiMzuFfVMUntoIIJEytBOmbAlJ OBseVQmynUNRMiejCOveEisbrKY::umQghCmsfCFpiiISpgIfIkmOcxB
	int32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_1;
	// System.Int32 OBseVQmynUNRMiejCOveEisbrKY::ohwKaeWdBHSsqiXAUCNnodIzwsY
	int32_t ___ohwKaeWdBHSsqiXAUCNnodIzwsY_2;
	// owCsobmYStIRvNUHffmjMFLdZEz OBseVQmynUNRMiejCOveEisbrKY::dyaUNLHBIeUikBzFJeaTDfszbXPG
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628 * ___dyaUNLHBIeUikBzFJeaTDfszbXPG_3;
	// System.Boolean OBseVQmynUNRMiejCOveEisbrKY::ffNWJUuxPxuFMIZszOmCdhFAkUp
	bool ___ffNWJUuxPxuFMIZszOmCdhFAkUp_4;
	// System.Boolean OBseVQmynUNRMiejCOveEisbrKY::RnljUVWuXLIugvtjrPQPUGngyWX
	bool ___RnljUVWuXLIugvtjrPQPUGngyWX_5;
	// XinyVXTRfxekDvrLaNfmyUwaYzV OBseVQmynUNRMiejCOveEisbrKY::bgdXMWDMghwHHNuDKlbXfHeIkOO
	XinyVXTRfxekDvrLaNfmyUwaYzV_tFC161390B4FF558F7F27D77244EC0FA850BF5C40 * ___bgdXMWDMghwHHNuDKlbXfHeIkOO_6;
	// System.Boolean OBseVQmynUNRMiejCOveEisbrKY::QNbgrVHxgRFXwNcjxsxdFGgPDyIT
	bool ___QNbgrVHxgRFXwNcjxsxdFGgPDyIT_7;
	// System.Int32 OBseVQmynUNRMiejCOveEisbrKY::YJKKCodZAAdHLAznZhhKAnRrzmS
	int32_t ___YJKKCodZAAdHLAznZhhKAnRrzmS_8;
	// System.Single[] OBseVQmynUNRMiejCOveEisbrKY::PlgzRPAQDKiUowbwIUFftEMvgpZ
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___PlgzRPAQDKiUowbwIUFftEMvgpZ_9;
	// System.Boolean OBseVQmynUNRMiejCOveEisbrKY::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_10;

public:
	inline static int32_t get_offset_of_sUZJrREVbDgnurDWKIIUlQKqWwq_0() { return static_cast<int32_t>(offsetof(OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533, ___sUZJrREVbDgnurDWKIIUlQKqWwq_0)); }
	inline KDIcNAKNsuNvLiMepNxDAEccLSR_tA3DD4DB1AB688F1291A2B02EC7E3CFD5A0829709 * get_sUZJrREVbDgnurDWKIIUlQKqWwq_0() const { return ___sUZJrREVbDgnurDWKIIUlQKqWwq_0; }
	inline KDIcNAKNsuNvLiMepNxDAEccLSR_tA3DD4DB1AB688F1291A2B02EC7E3CFD5A0829709 ** get_address_of_sUZJrREVbDgnurDWKIIUlQKqWwq_0() { return &___sUZJrREVbDgnurDWKIIUlQKqWwq_0; }
	inline void set_sUZJrREVbDgnurDWKIIUlQKqWwq_0(KDIcNAKNsuNvLiMepNxDAEccLSR_tA3DD4DB1AB688F1291A2B02EC7E3CFD5A0829709 * value)
	{
		___sUZJrREVbDgnurDWKIIUlQKqWwq_0 = value;
		Il2CppCodeGenWriteBarrier((&___sUZJrREVbDgnurDWKIIUlQKqWwq_0), value);
	}

	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_1() { return static_cast<int32_t>(offsetof(OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533, ___umQghCmsfCFpiiISpgIfIkmOcxB_1)); }
	inline int32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_1() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_1; }
	inline int32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_1() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_1; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_1(int32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_1 = value;
	}

	inline static int32_t get_offset_of_ohwKaeWdBHSsqiXAUCNnodIzwsY_2() { return static_cast<int32_t>(offsetof(OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533, ___ohwKaeWdBHSsqiXAUCNnodIzwsY_2)); }
	inline int32_t get_ohwKaeWdBHSsqiXAUCNnodIzwsY_2() const { return ___ohwKaeWdBHSsqiXAUCNnodIzwsY_2; }
	inline int32_t* get_address_of_ohwKaeWdBHSsqiXAUCNnodIzwsY_2() { return &___ohwKaeWdBHSsqiXAUCNnodIzwsY_2; }
	inline void set_ohwKaeWdBHSsqiXAUCNnodIzwsY_2(int32_t value)
	{
		___ohwKaeWdBHSsqiXAUCNnodIzwsY_2 = value;
	}

	inline static int32_t get_offset_of_dyaUNLHBIeUikBzFJeaTDfszbXPG_3() { return static_cast<int32_t>(offsetof(OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533, ___dyaUNLHBIeUikBzFJeaTDfszbXPG_3)); }
	inline owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628 * get_dyaUNLHBIeUikBzFJeaTDfszbXPG_3() const { return ___dyaUNLHBIeUikBzFJeaTDfszbXPG_3; }
	inline owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628 ** get_address_of_dyaUNLHBIeUikBzFJeaTDfszbXPG_3() { return &___dyaUNLHBIeUikBzFJeaTDfszbXPG_3; }
	inline void set_dyaUNLHBIeUikBzFJeaTDfszbXPG_3(owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628 * value)
	{
		___dyaUNLHBIeUikBzFJeaTDfszbXPG_3 = value;
		Il2CppCodeGenWriteBarrier((&___dyaUNLHBIeUikBzFJeaTDfszbXPG_3), value);
	}

	inline static int32_t get_offset_of_ffNWJUuxPxuFMIZszOmCdhFAkUp_4() { return static_cast<int32_t>(offsetof(OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533, ___ffNWJUuxPxuFMIZszOmCdhFAkUp_4)); }
	inline bool get_ffNWJUuxPxuFMIZszOmCdhFAkUp_4() const { return ___ffNWJUuxPxuFMIZszOmCdhFAkUp_4; }
	inline bool* get_address_of_ffNWJUuxPxuFMIZszOmCdhFAkUp_4() { return &___ffNWJUuxPxuFMIZszOmCdhFAkUp_4; }
	inline void set_ffNWJUuxPxuFMIZszOmCdhFAkUp_4(bool value)
	{
		___ffNWJUuxPxuFMIZszOmCdhFAkUp_4 = value;
	}

	inline static int32_t get_offset_of_RnljUVWuXLIugvtjrPQPUGngyWX_5() { return static_cast<int32_t>(offsetof(OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533, ___RnljUVWuXLIugvtjrPQPUGngyWX_5)); }
	inline bool get_RnljUVWuXLIugvtjrPQPUGngyWX_5() const { return ___RnljUVWuXLIugvtjrPQPUGngyWX_5; }
	inline bool* get_address_of_RnljUVWuXLIugvtjrPQPUGngyWX_5() { return &___RnljUVWuXLIugvtjrPQPUGngyWX_5; }
	inline void set_RnljUVWuXLIugvtjrPQPUGngyWX_5(bool value)
	{
		___RnljUVWuXLIugvtjrPQPUGngyWX_5 = value;
	}

	inline static int32_t get_offset_of_bgdXMWDMghwHHNuDKlbXfHeIkOO_6() { return static_cast<int32_t>(offsetof(OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533, ___bgdXMWDMghwHHNuDKlbXfHeIkOO_6)); }
	inline XinyVXTRfxekDvrLaNfmyUwaYzV_tFC161390B4FF558F7F27D77244EC0FA850BF5C40 * get_bgdXMWDMghwHHNuDKlbXfHeIkOO_6() const { return ___bgdXMWDMghwHHNuDKlbXfHeIkOO_6; }
	inline XinyVXTRfxekDvrLaNfmyUwaYzV_tFC161390B4FF558F7F27D77244EC0FA850BF5C40 ** get_address_of_bgdXMWDMghwHHNuDKlbXfHeIkOO_6() { return &___bgdXMWDMghwHHNuDKlbXfHeIkOO_6; }
	inline void set_bgdXMWDMghwHHNuDKlbXfHeIkOO_6(XinyVXTRfxekDvrLaNfmyUwaYzV_tFC161390B4FF558F7F27D77244EC0FA850BF5C40 * value)
	{
		___bgdXMWDMghwHHNuDKlbXfHeIkOO_6 = value;
		Il2CppCodeGenWriteBarrier((&___bgdXMWDMghwHHNuDKlbXfHeIkOO_6), value);
	}

	inline static int32_t get_offset_of_QNbgrVHxgRFXwNcjxsxdFGgPDyIT_7() { return static_cast<int32_t>(offsetof(OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533, ___QNbgrVHxgRFXwNcjxsxdFGgPDyIT_7)); }
	inline bool get_QNbgrVHxgRFXwNcjxsxdFGgPDyIT_7() const { return ___QNbgrVHxgRFXwNcjxsxdFGgPDyIT_7; }
	inline bool* get_address_of_QNbgrVHxgRFXwNcjxsxdFGgPDyIT_7() { return &___QNbgrVHxgRFXwNcjxsxdFGgPDyIT_7; }
	inline void set_QNbgrVHxgRFXwNcjxsxdFGgPDyIT_7(bool value)
	{
		___QNbgrVHxgRFXwNcjxsxdFGgPDyIT_7 = value;
	}

	inline static int32_t get_offset_of_YJKKCodZAAdHLAznZhhKAnRrzmS_8() { return static_cast<int32_t>(offsetof(OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533, ___YJKKCodZAAdHLAznZhhKAnRrzmS_8)); }
	inline int32_t get_YJKKCodZAAdHLAznZhhKAnRrzmS_8() const { return ___YJKKCodZAAdHLAznZhhKAnRrzmS_8; }
	inline int32_t* get_address_of_YJKKCodZAAdHLAznZhhKAnRrzmS_8() { return &___YJKKCodZAAdHLAznZhhKAnRrzmS_8; }
	inline void set_YJKKCodZAAdHLAznZhhKAnRrzmS_8(int32_t value)
	{
		___YJKKCodZAAdHLAznZhhKAnRrzmS_8 = value;
	}

	inline static int32_t get_offset_of_PlgzRPAQDKiUowbwIUFftEMvgpZ_9() { return static_cast<int32_t>(offsetof(OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533, ___PlgzRPAQDKiUowbwIUFftEMvgpZ_9)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_PlgzRPAQDKiUowbwIUFftEMvgpZ_9() const { return ___PlgzRPAQDKiUowbwIUFftEMvgpZ_9; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_PlgzRPAQDKiUowbwIUFftEMvgpZ_9() { return &___PlgzRPAQDKiUowbwIUFftEMvgpZ_9; }
	inline void set_PlgzRPAQDKiUowbwIUFftEMvgpZ_9(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___PlgzRPAQDKiUowbwIUFftEMvgpZ_9 = value;
		Il2CppCodeGenWriteBarrier((&___PlgzRPAQDKiUowbwIUFftEMvgpZ_9), value);
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_10() { return static_cast<int32_t>(offsetof(OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533, ___SeCUoinDywZmqZDHRKupOdOaTke_10)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_10() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_10; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_10() { return &___SeCUoinDywZmqZDHRKupOdOaTke_10; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_10(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSEVQMYNUNRMIEJCOVEEISBRKY_T344CF2A67444B48D7C343EFAE6FA77658C73D533_H
#ifndef CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#define CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerPollingInfo
struct  ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 
{
public:
	// System.Boolean Rewired.ControllerPollingInfo::XxREeDCHebPEeYaAATiXyFbnbIU
	bool ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	// System.Int32 Rewired.ControllerPollingInfo::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	// System.Int32 Rewired.ControllerPollingInfo::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	// System.String Rewired.ControllerPollingInfo::tUeBKTuOCBEsklhkCXrBJHMCCni
	String_t* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	// Rewired.ControllerType Rewired.ControllerPollingInfo::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	// Rewired.ControllerElementType Rewired.ControllerPollingInfo::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	// System.Int32 Rewired.ControllerPollingInfo::JcIOHtlyyDcisHJclHvGjyIzQHK
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	// Rewired.Pole Rewired.ControllerPollingInfo::gOvKiYbtKZOeaPYXdsADyDYKhyR
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	// System.String Rewired.ControllerPollingInfo::BpnqUtABZEySqFzPQqIktIgZaUw
	String_t* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	// System.Int32 Rewired.ControllerPollingInfo::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	// UnityEngine.KeyCode Rewired.ControllerPollingInfo::UBcIxoTNwLQLduWHPwWDYgtuvif
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;

public:
	inline static int32_t get_offset_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___XxREeDCHebPEeYaAATiXyFbnbIU_0)); }
	inline bool get_XxREeDCHebPEeYaAATiXyFbnbIU_0() const { return ___XxREeDCHebPEeYaAATiXyFbnbIU_0; }
	inline bool* get_address_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() { return &___XxREeDCHebPEeYaAATiXyFbnbIU_0; }
	inline void set_XxREeDCHebPEeYaAATiXyFbnbIU_0(bool value)
	{
		___XxREeDCHebPEeYaAATiXyFbnbIU_0 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___FCIziTxDWAnyHATWwZeMEmiXvdc_1)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_1() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_1(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_1 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___CdiTZueJOweHxLVLesdWcZkZxNV_2)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_2() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_2(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_2 = value;
	}

	inline static int32_t get_offset_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___tUeBKTuOCBEsklhkCXrBJHMCCni_3)); }
	inline String_t* get_tUeBKTuOCBEsklhkCXrBJHMCCni_3() const { return ___tUeBKTuOCBEsklhkCXrBJHMCCni_3; }
	inline String_t** get_address_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() { return &___tUeBKTuOCBEsklhkCXrBJHMCCni_3; }
	inline void set_tUeBKTuOCBEsklhkCXrBJHMCCni_3(String_t* value)
	{
		___tUeBKTuOCBEsklhkCXrBJHMCCni_3 = value;
		Il2CppCodeGenWriteBarrier((&___tUeBKTuOCBEsklhkCXrBJHMCCni_3), value);
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4 = value;
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___RmmmEkVblcqxoqGYhifgdSESkSn_5)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_5() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_5; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_5; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_5(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_5 = value;
	}

	inline static int32_t get_offset_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___JcIOHtlyyDcisHJclHvGjyIzQHK_6)); }
	inline int32_t get_JcIOHtlyyDcisHJclHvGjyIzQHK_6() const { return ___JcIOHtlyyDcisHJclHvGjyIzQHK_6; }
	inline int32_t* get_address_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() { return &___JcIOHtlyyDcisHJclHvGjyIzQHK_6; }
	inline void set_JcIOHtlyyDcisHJclHvGjyIzQHK_6(int32_t value)
	{
		___JcIOHtlyyDcisHJclHvGjyIzQHK_6 = value;
	}

	inline static int32_t get_offset_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7)); }
	inline int32_t get_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() const { return ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7; }
	inline int32_t* get_address_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() { return &___gOvKiYbtKZOeaPYXdsADyDYKhyR_7; }
	inline void set_gOvKiYbtKZOeaPYXdsADyDYKhyR_7(int32_t value)
	{
		___gOvKiYbtKZOeaPYXdsADyDYKhyR_7 = value;
	}

	inline static int32_t get_offset_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___BpnqUtABZEySqFzPQqIktIgZaUw_8)); }
	inline String_t* get_BpnqUtABZEySqFzPQqIktIgZaUw_8() const { return ___BpnqUtABZEySqFzPQqIktIgZaUw_8; }
	inline String_t** get_address_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() { return &___BpnqUtABZEySqFzPQqIktIgZaUw_8; }
	inline void set_BpnqUtABZEySqFzPQqIktIgZaUw_8(String_t* value)
	{
		___BpnqUtABZEySqFzPQqIktIgZaUw_8 = value;
		Il2CppCodeGenWriteBarrier((&___BpnqUtABZEySqFzPQqIktIgZaUw_8), value);
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___FrsWYgeIWMnjOcXDysagwZGcCMF_9)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_9() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_9; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_9; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_9(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_9 = value;
	}

	inline static int32_t get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___UBcIxoTNwLQLduWHPwWDYgtuvif_10)); }
	inline int32_t get_UBcIxoTNwLQLduWHPwWDYgtuvif_10() const { return ___UBcIxoTNwLQLduWHPwWDYgtuvif_10; }
	inline int32_t* get_address_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() { return &___UBcIxoTNwLQLduWHPwWDYgtuvif_10; }
	inline void set_UBcIxoTNwLQLduWHPwWDYgtuvif_10(int32_t value)
	{
		___UBcIxoTNwLQLduWHPwWDYgtuvif_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ControllerPollingInfo
struct ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_pinvoke
{
	int32_t ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	char* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	char* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;
};
// Native definition for COM marshalling of Rewired.ControllerPollingInfo
struct ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_com
{
	int32_t ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	Il2CppChar* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	Il2CppChar* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;
};
#endif // CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#ifndef ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#define ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentConflictCheck
struct  ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 
{
public:
	// Rewired.ElementAssignmentType Rewired.ElementAssignmentConflictCheck::PReGOawJrxHuYbkYlXhFoNBbppd
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	// Rewired.ControllerType Rewired.ElementAssignmentConflictCheck::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::kNupzNtNRIxDZONVUSwnnBWcpIT
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::fAMwdVQKMkdvbVWXbkkCQoSadHid
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::TXYPyQJDwYiLBxGVhCRAhruZxByr
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	// Rewired.AxisRange Rewired.ElementAssignmentConflictCheck::xTkhpClvSKvoiLfYNzcXIDURAPx
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	// UnityEngine.KeyCode Rewired.ElementAssignmentConflictCheck::UBcIxoTNwLQLduWHPwWDYgtuvif
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignmentConflictCheck::rUpHewPnhPrBuIDeIasWkyvRDkW
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	// Rewired.Pole Rewired.ElementAssignmentConflictCheck::magSqBEVFaJkoQfcAvXLVDzmcnZB
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	// System.Boolean Rewired.ElementAssignmentConflictCheck::GfvPtFqTnCIcrWebHAQzqOoKceI
	bool ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;

public:
	inline static int32_t get_offset_of_PReGOawJrxHuYbkYlXhFoNBbppd_0() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___PReGOawJrxHuYbkYlXhFoNBbppd_0)); }
	inline int32_t get_PReGOawJrxHuYbkYlXhFoNBbppd_0() const { return ___PReGOawJrxHuYbkYlXhFoNBbppd_0; }
	inline int32_t* get_address_of_PReGOawJrxHuYbkYlXhFoNBbppd_0() { return &___PReGOawJrxHuYbkYlXhFoNBbppd_0; }
	inline void set_PReGOawJrxHuYbkYlXhFoNBbppd_0(int32_t value)
	{
		___PReGOawJrxHuYbkYlXhFoNBbppd_0 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___FCIziTxDWAnyHATWwZeMEmiXvdc_1)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_1() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_1(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_1 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___CdiTZueJOweHxLVLesdWcZkZxNV_3)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_3() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_3; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_3; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_3(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_3 = value;
	}

	inline static int32_t get_offset_of_kNupzNtNRIxDZONVUSwnnBWcpIT_4() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___kNupzNtNRIxDZONVUSwnnBWcpIT_4)); }
	inline int32_t get_kNupzNtNRIxDZONVUSwnnBWcpIT_4() const { return ___kNupzNtNRIxDZONVUSwnnBWcpIT_4; }
	inline int32_t* get_address_of_kNupzNtNRIxDZONVUSwnnBWcpIT_4() { return &___kNupzNtNRIxDZONVUSwnnBWcpIT_4; }
	inline void set_kNupzNtNRIxDZONVUSwnnBWcpIT_4(int32_t value)
	{
		___kNupzNtNRIxDZONVUSwnnBWcpIT_4 = value;
	}

	inline static int32_t get_offset_of_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5)); }
	inline int32_t get_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() const { return ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5; }
	inline int32_t* get_address_of_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() { return &___fAMwdVQKMkdvbVWXbkkCQoSadHid_5; }
	inline void set_fAMwdVQKMkdvbVWXbkkCQoSadHid_5(int32_t value)
	{
		___fAMwdVQKMkdvbVWXbkkCQoSadHid_5 = value;
	}

	inline static int32_t get_offset_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6)); }
	inline int32_t get_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() const { return ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline int32_t* get_address_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return &___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline void set_TXYPyQJDwYiLBxGVhCRAhruZxByr_6(int32_t value)
	{
		___TXYPyQJDwYiLBxGVhCRAhruZxByr_6 = value;
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_7() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___FrsWYgeIWMnjOcXDysagwZGcCMF_7)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_7() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_7; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_7() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_7; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_7(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_7 = value;
	}

	inline static int32_t get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_8() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___xTkhpClvSKvoiLfYNzcXIDURAPx_8)); }
	inline int32_t get_xTkhpClvSKvoiLfYNzcXIDURAPx_8() const { return ___xTkhpClvSKvoiLfYNzcXIDURAPx_8; }
	inline int32_t* get_address_of_xTkhpClvSKvoiLfYNzcXIDURAPx_8() { return &___xTkhpClvSKvoiLfYNzcXIDURAPx_8; }
	inline void set_xTkhpClvSKvoiLfYNzcXIDURAPx_8(int32_t value)
	{
		___xTkhpClvSKvoiLfYNzcXIDURAPx_8 = value;
	}

	inline static int32_t get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_9() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___UBcIxoTNwLQLduWHPwWDYgtuvif_9)); }
	inline int32_t get_UBcIxoTNwLQLduWHPwWDYgtuvif_9() const { return ___UBcIxoTNwLQLduWHPwWDYgtuvif_9; }
	inline int32_t* get_address_of_UBcIxoTNwLQLduWHPwWDYgtuvif_9() { return &___UBcIxoTNwLQLduWHPwWDYgtuvif_9; }
	inline void set_UBcIxoTNwLQLduWHPwWDYgtuvif_9(int32_t value)
	{
		___UBcIxoTNwLQLduWHPwWDYgtuvif_9 = value;
	}

	inline static int32_t get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___rUpHewPnhPrBuIDeIasWkyvRDkW_10)); }
	inline int32_t get_rUpHewPnhPrBuIDeIasWkyvRDkW_10() const { return ___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline int32_t* get_address_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return &___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline void set_rUpHewPnhPrBuIDeIasWkyvRDkW_10(int32_t value)
	{
		___rUpHewPnhPrBuIDeIasWkyvRDkW_10 = value;
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___LmADKPFcmVAiabETiHqQTGSgvmcg_11)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_11() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_11(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_11 = value;
	}

	inline static int32_t get_offset_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12)); }
	inline int32_t get_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() const { return ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12; }
	inline int32_t* get_address_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() { return &___magSqBEVFaJkoQfcAvXLVDzmcnZB_12; }
	inline void set_magSqBEVFaJkoQfcAvXLVDzmcnZB_12(int32_t value)
	{
		___magSqBEVFaJkoQfcAvXLVDzmcnZB_12 = value;
	}

	inline static int32_t get_offset_of_GfvPtFqTnCIcrWebHAQzqOoKceI_13() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___GfvPtFqTnCIcrWebHAQzqOoKceI_13)); }
	inline bool get_GfvPtFqTnCIcrWebHAQzqOoKceI_13() const { return ___GfvPtFqTnCIcrWebHAQzqOoKceI_13; }
	inline bool* get_address_of_GfvPtFqTnCIcrWebHAQzqOoKceI_13() { return &___GfvPtFqTnCIcrWebHAQzqOoKceI_13; }
	inline void set_GfvPtFqTnCIcrWebHAQzqOoKceI_13(bool value)
	{
		___GfvPtFqTnCIcrWebHAQzqOoKceI_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignmentConflictCheck
struct ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_marshaled_pinvoke
{
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	int32_t ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;
};
// Native definition for COM marshalling of Rewired.ElementAssignmentConflictCheck
struct ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_marshaled_com
{
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	int32_t ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;
};
#endif // ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#ifndef ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#define ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentConflictInfo
struct  ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D 
{
public:
	// System.Boolean Rewired.ElementAssignmentConflictInfo::rLCeavZDeONdAmfQvjLvpIKqsIQ
	bool ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	// System.Boolean Rewired.ElementAssignmentConflictInfo::uUoPCsKlKtgHCnSDBCrTHvhFbFXy
	bool ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	// Rewired.ControllerType Rewired.ElementAssignmentConflictInfo::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::kNupzNtNRIxDZONVUSwnnBWcpIT
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::TXYPyQJDwYiLBxGVhCRAhruZxByr
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	// Rewired.ControllerElementType Rewired.ElementAssignmentConflictInfo::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	// UnityEngine.KeyCode Rewired.ElementAssignmentConflictInfo::SkzHHHthNTcRwnaarBvUnkLqLeI
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignmentConflictInfo::rUpHewPnhPrBuIDeIasWkyvRDkW
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;

public:
	inline static int32_t get_offset_of_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0)); }
	inline bool get_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() const { return ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0; }
	inline bool* get_address_of_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() { return &___rLCeavZDeONdAmfQvjLvpIKqsIQ_0; }
	inline void set_rLCeavZDeONdAmfQvjLvpIKqsIQ_0(bool value)
	{
		___rLCeavZDeONdAmfQvjLvpIKqsIQ_0 = value;
	}

	inline static int32_t get_offset_of_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1)); }
	inline bool get_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() const { return ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1; }
	inline bool* get_address_of_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() { return &___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1; }
	inline void set_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1(bool value)
	{
		___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___FCIziTxDWAnyHATWwZeMEmiXvdc_2)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_2() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_2(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_2 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___CdiTZueJOweHxLVLesdWcZkZxNV_4)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_4() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_4; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_4; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_4(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_4 = value;
	}

	inline static int32_t get_offset_of_kNupzNtNRIxDZONVUSwnnBWcpIT_5() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___kNupzNtNRIxDZONVUSwnnBWcpIT_5)); }
	inline int32_t get_kNupzNtNRIxDZONVUSwnnBWcpIT_5() const { return ___kNupzNtNRIxDZONVUSwnnBWcpIT_5; }
	inline int32_t* get_address_of_kNupzNtNRIxDZONVUSwnnBWcpIT_5() { return &___kNupzNtNRIxDZONVUSwnnBWcpIT_5; }
	inline void set_kNupzNtNRIxDZONVUSwnnBWcpIT_5(int32_t value)
	{
		___kNupzNtNRIxDZONVUSwnnBWcpIT_5 = value;
	}

	inline static int32_t get_offset_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6)); }
	inline int32_t get_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() const { return ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline int32_t* get_address_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return &___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline void set_TXYPyQJDwYiLBxGVhCRAhruZxByr_6(int32_t value)
	{
		___TXYPyQJDwYiLBxGVhCRAhruZxByr_6 = value;
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_7() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___RmmmEkVblcqxoqGYhifgdSESkSn_7)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_7() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_7; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_7() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_7; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_7(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_7 = value;
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_8() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___FrsWYgeIWMnjOcXDysagwZGcCMF_8)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_8() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_8; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_8() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_8; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_8(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_8 = value;
	}

	inline static int32_t get_offset_of_SkzHHHthNTcRwnaarBvUnkLqLeI_9() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___SkzHHHthNTcRwnaarBvUnkLqLeI_9)); }
	inline int32_t get_SkzHHHthNTcRwnaarBvUnkLqLeI_9() const { return ___SkzHHHthNTcRwnaarBvUnkLqLeI_9; }
	inline int32_t* get_address_of_SkzHHHthNTcRwnaarBvUnkLqLeI_9() { return &___SkzHHHthNTcRwnaarBvUnkLqLeI_9; }
	inline void set_SkzHHHthNTcRwnaarBvUnkLqLeI_9(int32_t value)
	{
		___SkzHHHthNTcRwnaarBvUnkLqLeI_9 = value;
	}

	inline static int32_t get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___rUpHewPnhPrBuIDeIasWkyvRDkW_10)); }
	inline int32_t get_rUpHewPnhPrBuIDeIasWkyvRDkW_10() const { return ___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline int32_t* get_address_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return &___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline void set_rUpHewPnhPrBuIDeIasWkyvRDkW_10(int32_t value)
	{
		___rUpHewPnhPrBuIDeIasWkyvRDkW_10 = value;
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___LmADKPFcmVAiabETiHqQTGSgvmcg_11)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_11() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_11(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignmentConflictInfo
struct ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_marshaled_pinvoke
{
	int32_t ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	int32_t ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
};
// Native definition for COM marshalling of Rewired.ElementAssignmentConflictInfo
struct ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_marshaled_com
{
	int32_t ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	int32_t ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
};
#endif // ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#ifndef HQNQFYRBLCPWCVHZXOYPGEBOOXL_TFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8_H
#define HQNQFYRBLCPWCVHZXOYPGEBOOXL_TFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_HqnQFyrBLcPwCVhzxoyPgeBoOXL
struct  HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8  : public RuntimeObject
{
public:
	// Rewired.UpdateLoopType Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_HqnQFyrBLcPwCVhzxoyPgeBoOXL::TWKNVXwJTlddHEpGDbHCUyznGXe
	int32_t ___TWKNVXwJTlddHEpGDbHCUyznGXe_0;
	// System.Single Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_HqnQFyrBLcPwCVhzxoyPgeBoOXL::DdAQuNXlVlFtzKHQSEFqDCRtRgt
	float ___DdAQuNXlVlFtzKHQSEFqDCRtRgt_1;
	// System.Single Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_HqnQFyrBLcPwCVhzxoyPgeBoOXL::FIYhvJgqZRRePhbHOATKhehYewa
	float ___FIYhvJgqZRRePhbHOATKhehYewa_2;
	// System.Single Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_HqnQFyrBLcPwCVhzxoyPgeBoOXL::oeJpjybqWWdBoUSIPTRbUtisjll
	float ___oeJpjybqWWdBoUSIPTRbUtisjll_3;
	// System.Single Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_HqnQFyrBLcPwCVhzxoyPgeBoOXL::NsVTlLVHdMBMZiGpdgoZcLGKPHci
	float ___NsVTlLVHdMBMZiGpdgoZcLGKPHci_4;
	// System.UInt32 Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_HqnQFyrBLcPwCVhzxoyPgeBoOXL::NtGqRWkVRBBUHEeZvzqBCkzqIAL
	uint32_t ___NtGqRWkVRBBUHEeZvzqBCkzqIAL_5;
	// System.UInt32 Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_HqnQFyrBLcPwCVhzxoyPgeBoOXL::QVhKynZjqsHvJXTGAiWaOYyGjHk
	uint32_t ___QVhKynZjqsHvJXTGAiWaOYyGjHk_6;
	// System.Single Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_HqnQFyrBLcPwCVhzxoyPgeBoOXL::nVwnLcGgetYmaZOKTSgvAOhbQHP
	float ___nVwnLcGgetYmaZOKTSgvAOhbQHP_7;
	// System.Single Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF_HqnQFyrBLcPwCVhzxoyPgeBoOXL::UTKDHGiHcPBEaJGmcNIhonLlQYfK
	float ___UTKDHGiHcPBEaJGmcNIhonLlQYfK_8;

public:
	inline static int32_t get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_0() { return static_cast<int32_t>(offsetof(HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8, ___TWKNVXwJTlddHEpGDbHCUyznGXe_0)); }
	inline int32_t get_TWKNVXwJTlddHEpGDbHCUyznGXe_0() const { return ___TWKNVXwJTlddHEpGDbHCUyznGXe_0; }
	inline int32_t* get_address_of_TWKNVXwJTlddHEpGDbHCUyznGXe_0() { return &___TWKNVXwJTlddHEpGDbHCUyznGXe_0; }
	inline void set_TWKNVXwJTlddHEpGDbHCUyznGXe_0(int32_t value)
	{
		___TWKNVXwJTlddHEpGDbHCUyznGXe_0 = value;
	}

	inline static int32_t get_offset_of_DdAQuNXlVlFtzKHQSEFqDCRtRgt_1() { return static_cast<int32_t>(offsetof(HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8, ___DdAQuNXlVlFtzKHQSEFqDCRtRgt_1)); }
	inline float get_DdAQuNXlVlFtzKHQSEFqDCRtRgt_1() const { return ___DdAQuNXlVlFtzKHQSEFqDCRtRgt_1; }
	inline float* get_address_of_DdAQuNXlVlFtzKHQSEFqDCRtRgt_1() { return &___DdAQuNXlVlFtzKHQSEFqDCRtRgt_1; }
	inline void set_DdAQuNXlVlFtzKHQSEFqDCRtRgt_1(float value)
	{
		___DdAQuNXlVlFtzKHQSEFqDCRtRgt_1 = value;
	}

	inline static int32_t get_offset_of_FIYhvJgqZRRePhbHOATKhehYewa_2() { return static_cast<int32_t>(offsetof(HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8, ___FIYhvJgqZRRePhbHOATKhehYewa_2)); }
	inline float get_FIYhvJgqZRRePhbHOATKhehYewa_2() const { return ___FIYhvJgqZRRePhbHOATKhehYewa_2; }
	inline float* get_address_of_FIYhvJgqZRRePhbHOATKhehYewa_2() { return &___FIYhvJgqZRRePhbHOATKhehYewa_2; }
	inline void set_FIYhvJgqZRRePhbHOATKhehYewa_2(float value)
	{
		___FIYhvJgqZRRePhbHOATKhehYewa_2 = value;
	}

	inline static int32_t get_offset_of_oeJpjybqWWdBoUSIPTRbUtisjll_3() { return static_cast<int32_t>(offsetof(HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8, ___oeJpjybqWWdBoUSIPTRbUtisjll_3)); }
	inline float get_oeJpjybqWWdBoUSIPTRbUtisjll_3() const { return ___oeJpjybqWWdBoUSIPTRbUtisjll_3; }
	inline float* get_address_of_oeJpjybqWWdBoUSIPTRbUtisjll_3() { return &___oeJpjybqWWdBoUSIPTRbUtisjll_3; }
	inline void set_oeJpjybqWWdBoUSIPTRbUtisjll_3(float value)
	{
		___oeJpjybqWWdBoUSIPTRbUtisjll_3 = value;
	}

	inline static int32_t get_offset_of_NsVTlLVHdMBMZiGpdgoZcLGKPHci_4() { return static_cast<int32_t>(offsetof(HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8, ___NsVTlLVHdMBMZiGpdgoZcLGKPHci_4)); }
	inline float get_NsVTlLVHdMBMZiGpdgoZcLGKPHci_4() const { return ___NsVTlLVHdMBMZiGpdgoZcLGKPHci_4; }
	inline float* get_address_of_NsVTlLVHdMBMZiGpdgoZcLGKPHci_4() { return &___NsVTlLVHdMBMZiGpdgoZcLGKPHci_4; }
	inline void set_NsVTlLVHdMBMZiGpdgoZcLGKPHci_4(float value)
	{
		___NsVTlLVHdMBMZiGpdgoZcLGKPHci_4 = value;
	}

	inline static int32_t get_offset_of_NtGqRWkVRBBUHEeZvzqBCkzqIAL_5() { return static_cast<int32_t>(offsetof(HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8, ___NtGqRWkVRBBUHEeZvzqBCkzqIAL_5)); }
	inline uint32_t get_NtGqRWkVRBBUHEeZvzqBCkzqIAL_5() const { return ___NtGqRWkVRBBUHEeZvzqBCkzqIAL_5; }
	inline uint32_t* get_address_of_NtGqRWkVRBBUHEeZvzqBCkzqIAL_5() { return &___NtGqRWkVRBBUHEeZvzqBCkzqIAL_5; }
	inline void set_NtGqRWkVRBBUHEeZvzqBCkzqIAL_5(uint32_t value)
	{
		___NtGqRWkVRBBUHEeZvzqBCkzqIAL_5 = value;
	}

	inline static int32_t get_offset_of_QVhKynZjqsHvJXTGAiWaOYyGjHk_6() { return static_cast<int32_t>(offsetof(HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8, ___QVhKynZjqsHvJXTGAiWaOYyGjHk_6)); }
	inline uint32_t get_QVhKynZjqsHvJXTGAiWaOYyGjHk_6() const { return ___QVhKynZjqsHvJXTGAiWaOYyGjHk_6; }
	inline uint32_t* get_address_of_QVhKynZjqsHvJXTGAiWaOYyGjHk_6() { return &___QVhKynZjqsHvJXTGAiWaOYyGjHk_6; }
	inline void set_QVhKynZjqsHvJXTGAiWaOYyGjHk_6(uint32_t value)
	{
		___QVhKynZjqsHvJXTGAiWaOYyGjHk_6 = value;
	}

	inline static int32_t get_offset_of_nVwnLcGgetYmaZOKTSgvAOhbQHP_7() { return static_cast<int32_t>(offsetof(HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8, ___nVwnLcGgetYmaZOKTSgvAOhbQHP_7)); }
	inline float get_nVwnLcGgetYmaZOKTSgvAOhbQHP_7() const { return ___nVwnLcGgetYmaZOKTSgvAOhbQHP_7; }
	inline float* get_address_of_nVwnLcGgetYmaZOKTSgvAOhbQHP_7() { return &___nVwnLcGgetYmaZOKTSgvAOhbQHP_7; }
	inline void set_nVwnLcGgetYmaZOKTSgvAOhbQHP_7(float value)
	{
		___nVwnLcGgetYmaZOKTSgvAOhbQHP_7 = value;
	}

	inline static int32_t get_offset_of_UTKDHGiHcPBEaJGmcNIhonLlQYfK_8() { return static_cast<int32_t>(offsetof(HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8, ___UTKDHGiHcPBEaJGmcNIhonLlQYfK_8)); }
	inline float get_UTKDHGiHcPBEaJGmcNIhonLlQYfK_8() const { return ___UTKDHGiHcPBEaJGmcNIhonLlQYfK_8; }
	inline float* get_address_of_UTKDHGiHcPBEaJGmcNIhonLlQYfK_8() { return &___UTKDHGiHcPBEaJGmcNIhonLlQYfK_8; }
	inline void set_UTKDHGiHcPBEaJGmcNIhonLlQYfK_8(float value)
	{
		___UTKDHGiHcPBEaJGmcNIhonLlQYfK_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HQNQFYRBLCPWCVHZXOYPGEBOOXL_TFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GVRKMEBKQRXIZPPNLDGUFPXGYIXV_T0C525DFA1DF74B163B9F31FA2D3AB8D36CBD703F_H
#define GVRKMEBKQRXIZPPNLDGUFPXGYIXV_T0C525DFA1DF74B163B9F31FA2D3AB8D36CBD703F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV
struct  gvrkMEBkQRXiZPPNldGUfPXGyiXV_t0C525DFA1DF74B163B9F31FA2D3AB8D36CBD703F  : public RuntimeObject
{
public:

public:
};

struct gvrkMEBkQRXiZPPNldGUfPXGyiXV_t0C525DFA1DF74B163B9F31FA2D3AB8D36CBD703F_StaticFields
{
public:
	// Rewired.Platforms.Platform gvrkMEBkQRXiZPPNldGUfPXGyiXV::apqaknfudBKPnUptdxzZmITDZBs
	int32_t ___apqaknfudBKPnUptdxzZmITDZBs_2;

public:
	inline static int32_t get_offset_of_apqaknfudBKPnUptdxzZmITDZBs_2() { return static_cast<int32_t>(offsetof(gvrkMEBkQRXiZPPNldGUfPXGyiXV_t0C525DFA1DF74B163B9F31FA2D3AB8D36CBD703F_StaticFields, ___apqaknfudBKPnUptdxzZmITDZBs_2)); }
	inline int32_t get_apqaknfudBKPnUptdxzZmITDZBs_2() const { return ___apqaknfudBKPnUptdxzZmITDZBs_2; }
	inline int32_t* get_address_of_apqaknfudBKPnUptdxzZmITDZBs_2() { return &___apqaknfudBKPnUptdxzZmITDZBs_2; }
	inline void set_apqaknfudBKPnUptdxzZmITDZBs_2(int32_t value)
	{
		___apqaknfudBKPnUptdxzZmITDZBs_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKMEBKQRXIZPPNLDGUFPXGYIXV_T0C525DFA1DF74B163B9F31FA2D3AB8D36CBD703F_H
#ifndef CJXQDHRLAJJTVIBURZZUVXUWUBO_TC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A_H
#define CJXQDHRLAJJTVIBURZZUVXUWUBO_TC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_CJxQDhrlajjtvIBURZzUvXUWUbo
struct  CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_LHFcKEsNXygGejIjuHtBhpgXxFm gvrkMEBkQRXiZPPNldGUfPXGyiXV_CJxQDhrlajjtvIBURZzUvXUWUbo::MjAkTUfgJOwhLMrDUgbOBSkgoJh
	int32_t ___MjAkTUfgJOwhLMrDUgbOBSkgoJh_0;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_CJxQDhrlajjtvIBURZzUvXUWUbo::anEZvqTOCnVJRFCZbeXXfTPchXyD
	int32_t ___anEZvqTOCnVJRFCZbeXXfTPchXyD_1;
	// System.UInt16 gvrkMEBkQRXiZPPNldGUfPXGyiXV_CJxQDhrlajjtvIBURZzUvXUWUbo::eoSBvxdKQXbBQxZCRzHwAmstPuKH
	uint16_t ___eoSBvxdKQXbBQxZCRzHwAmstPuKH_2;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_CJxQDhrlajjtvIBURZzUvXUWUbo::tpBctiuLUtOkwlAyUwwLNoEbBRW
	uint32_t ___tpBctiuLUtOkwlAyUwwLNoEbBRW_3;

public:
	inline static int32_t get_offset_of_MjAkTUfgJOwhLMrDUgbOBSkgoJh_0() { return static_cast<int32_t>(offsetof(CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A, ___MjAkTUfgJOwhLMrDUgbOBSkgoJh_0)); }
	inline int32_t get_MjAkTUfgJOwhLMrDUgbOBSkgoJh_0() const { return ___MjAkTUfgJOwhLMrDUgbOBSkgoJh_0; }
	inline int32_t* get_address_of_MjAkTUfgJOwhLMrDUgbOBSkgoJh_0() { return &___MjAkTUfgJOwhLMrDUgbOBSkgoJh_0; }
	inline void set_MjAkTUfgJOwhLMrDUgbOBSkgoJh_0(int32_t value)
	{
		___MjAkTUfgJOwhLMrDUgbOBSkgoJh_0 = value;
	}

	inline static int32_t get_offset_of_anEZvqTOCnVJRFCZbeXXfTPchXyD_1() { return static_cast<int32_t>(offsetof(CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A, ___anEZvqTOCnVJRFCZbeXXfTPchXyD_1)); }
	inline int32_t get_anEZvqTOCnVJRFCZbeXXfTPchXyD_1() const { return ___anEZvqTOCnVJRFCZbeXXfTPchXyD_1; }
	inline int32_t* get_address_of_anEZvqTOCnVJRFCZbeXXfTPchXyD_1() { return &___anEZvqTOCnVJRFCZbeXXfTPchXyD_1; }
	inline void set_anEZvqTOCnVJRFCZbeXXfTPchXyD_1(int32_t value)
	{
		___anEZvqTOCnVJRFCZbeXXfTPchXyD_1 = value;
	}

	inline static int32_t get_offset_of_eoSBvxdKQXbBQxZCRzHwAmstPuKH_2() { return static_cast<int32_t>(offsetof(CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A, ___eoSBvxdKQXbBQxZCRzHwAmstPuKH_2)); }
	inline uint16_t get_eoSBvxdKQXbBQxZCRzHwAmstPuKH_2() const { return ___eoSBvxdKQXbBQxZCRzHwAmstPuKH_2; }
	inline uint16_t* get_address_of_eoSBvxdKQXbBQxZCRzHwAmstPuKH_2() { return &___eoSBvxdKQXbBQxZCRzHwAmstPuKH_2; }
	inline void set_eoSBvxdKQXbBQxZCRzHwAmstPuKH_2(uint16_t value)
	{
		___eoSBvxdKQXbBQxZCRzHwAmstPuKH_2 = value;
	}

	inline static int32_t get_offset_of_tpBctiuLUtOkwlAyUwwLNoEbBRW_3() { return static_cast<int32_t>(offsetof(CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A, ___tpBctiuLUtOkwlAyUwwLNoEbBRW_3)); }
	inline uint32_t get_tpBctiuLUtOkwlAyUwwLNoEbBRW_3() const { return ___tpBctiuLUtOkwlAyUwwLNoEbBRW_3; }
	inline uint32_t* get_address_of_tpBctiuLUtOkwlAyUwwLNoEbBRW_3() { return &___tpBctiuLUtOkwlAyUwwLNoEbBRW_3; }
	inline void set_tpBctiuLUtOkwlAyUwwLNoEbBRW_3(uint32_t value)
	{
		___tpBctiuLUtOkwlAyUwwLNoEbBRW_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CJXQDHRLAJJTVIBURZZUVXUWUBO_TC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A_H
#ifndef HDOHFYADSWIBUZQWNDUNFIPFVAZE_TCFC0698F326902D5EDDF9992104023027410A554_H
#define HDOHFYADSWIBUZQWNDUNFIPFVAZE_TCFC0698F326902D5EDDF9992104023027410A554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_HdoHFYadSWiBUzqWnduNfIPFVazE
struct  HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_HdoHFYadSWiBUzqWnduNfIPFVazE::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_HdoHFYadSWiBUzqWnduNfIPFVazE::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_HdoHFYadSWiBUzqWnduNfIPFVazE::ZXOWQoAgWuCntwkclqxZSCDQZSd
	int32_t ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return static_cast<int32_t>(offsetof(HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554, ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2)); }
	inline int32_t get_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() const { return ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline int32_t* get_address_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return &___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline void set_ZXOWQoAgWuCntwkclqxZSCDQZSd_2(int32_t value)
	{
		___ZXOWQoAgWuCntwkclqxZSCDQZSd_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HDOHFYADSWIBUZQWNDUNFIPFVAZE_TCFC0698F326902D5EDDF9992104023027410A554_H
#ifndef JSSRNMIUWVPAOUYNOKKJPLOPPIS_T5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7_H
#define JSSRNMIUWVPAOUYNOKKJPLOPPIS_T5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_JsSrNMiUWvPaoUYnokkJplOPpIs
struct  JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_JsSrNMiUWvPaoUYnokkJplOPpIs::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_JsSrNMiUWvPaoUYnokkJplOPpIs::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_JsSrNMiUWvPaoUYnokkJplOPpIs::ZXOWQoAgWuCntwkclqxZSCDQZSd
	int32_t ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_JsSrNMiUWvPaoUYnokkJplOPpIs::XbjzjtekkFzjyzBDCNprtMTvRMx
	uint8_t ___XbjzjtekkFzjyzBDCNprtMTvRMx_3;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_JsSrNMiUWvPaoUYnokkJplOPpIs::BfGikzYXODBhJWbccUmzPNgAJjN
	uint8_t ___BfGikzYXODBhJWbccUmzPNgAJjN_4;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_JsSrNMiUWvPaoUYnokkJplOPpIs::ZdxNPrFvibjxZCDAFwWLoADXmOO
	uint8_t ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_JsSrNMiUWvPaoUYnokkJplOPpIs::lbxzKBvrbNcnDcVvpWWCKOWJRDOM
	uint8_t ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6;
	// System.Int16 gvrkMEBkQRXiZPPNldGUfPXGyiXV_JsSrNMiUWvPaoUYnokkJplOPpIs::YoIVBUOGrzDHUpJJHxXhsIPTqVX
	int16_t ___YoIVBUOGrzDHUpJJHxXhsIPTqVX_7;
	// System.Int16 gvrkMEBkQRXiZPPNldGUfPXGyiXV_JsSrNMiUWvPaoUYnokkJplOPpIs::fefuBVpjOXbPneAFgnpstaUWSLV
	int16_t ___fefuBVpjOXbPneAFgnpstaUWSLV_8;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return static_cast<int32_t>(offsetof(JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7, ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2)); }
	inline int32_t get_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() const { return ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline int32_t* get_address_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return &___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline void set_ZXOWQoAgWuCntwkclqxZSCDQZSd_2(int32_t value)
	{
		___ZXOWQoAgWuCntwkclqxZSCDQZSd_2 = value;
	}

	inline static int32_t get_offset_of_XbjzjtekkFzjyzBDCNprtMTvRMx_3() { return static_cast<int32_t>(offsetof(JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7, ___XbjzjtekkFzjyzBDCNprtMTvRMx_3)); }
	inline uint8_t get_XbjzjtekkFzjyzBDCNprtMTvRMx_3() const { return ___XbjzjtekkFzjyzBDCNprtMTvRMx_3; }
	inline uint8_t* get_address_of_XbjzjtekkFzjyzBDCNprtMTvRMx_3() { return &___XbjzjtekkFzjyzBDCNprtMTvRMx_3; }
	inline void set_XbjzjtekkFzjyzBDCNprtMTvRMx_3(uint8_t value)
	{
		___XbjzjtekkFzjyzBDCNprtMTvRMx_3 = value;
	}

	inline static int32_t get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_4() { return static_cast<int32_t>(offsetof(JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7, ___BfGikzYXODBhJWbccUmzPNgAJjN_4)); }
	inline uint8_t get_BfGikzYXODBhJWbccUmzPNgAJjN_4() const { return ___BfGikzYXODBhJWbccUmzPNgAJjN_4; }
	inline uint8_t* get_address_of_BfGikzYXODBhJWbccUmzPNgAJjN_4() { return &___BfGikzYXODBhJWbccUmzPNgAJjN_4; }
	inline void set_BfGikzYXODBhJWbccUmzPNgAJjN_4(uint8_t value)
	{
		___BfGikzYXODBhJWbccUmzPNgAJjN_4 = value;
	}

	inline static int32_t get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() { return static_cast<int32_t>(offsetof(JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7, ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5)); }
	inline uint8_t get_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() const { return ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5; }
	inline uint8_t* get_address_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() { return &___ZdxNPrFvibjxZCDAFwWLoADXmOO_5; }
	inline void set_ZdxNPrFvibjxZCDAFwWLoADXmOO_5(uint8_t value)
	{
		___ZdxNPrFvibjxZCDAFwWLoADXmOO_5 = value;
	}

	inline static int32_t get_offset_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() { return static_cast<int32_t>(offsetof(JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7, ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6)); }
	inline uint8_t get_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() const { return ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6; }
	inline uint8_t* get_address_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() { return &___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6; }
	inline void set_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6(uint8_t value)
	{
		___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6 = value;
	}

	inline static int32_t get_offset_of_YoIVBUOGrzDHUpJJHxXhsIPTqVX_7() { return static_cast<int32_t>(offsetof(JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7, ___YoIVBUOGrzDHUpJJHxXhsIPTqVX_7)); }
	inline int16_t get_YoIVBUOGrzDHUpJJHxXhsIPTqVX_7() const { return ___YoIVBUOGrzDHUpJJHxXhsIPTqVX_7; }
	inline int16_t* get_address_of_YoIVBUOGrzDHUpJJHxXhsIPTqVX_7() { return &___YoIVBUOGrzDHUpJJHxXhsIPTqVX_7; }
	inline void set_YoIVBUOGrzDHUpJJHxXhsIPTqVX_7(int16_t value)
	{
		___YoIVBUOGrzDHUpJJHxXhsIPTqVX_7 = value;
	}

	inline static int32_t get_offset_of_fefuBVpjOXbPneAFgnpstaUWSLV_8() { return static_cast<int32_t>(offsetof(JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7, ___fefuBVpjOXbPneAFgnpstaUWSLV_8)); }
	inline int16_t get_fefuBVpjOXbPneAFgnpstaUWSLV_8() const { return ___fefuBVpjOXbPneAFgnpstaUWSLV_8; }
	inline int16_t* get_address_of_fefuBVpjOXbPneAFgnpstaUWSLV_8() { return &___fefuBVpjOXbPneAFgnpstaUWSLV_8; }
	inline void set_fefuBVpjOXbPneAFgnpstaUWSLV_8(int16_t value)
	{
		___fefuBVpjOXbPneAFgnpstaUWSLV_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSSRNMIUWVPAOUYNOKKJPLOPPIS_T5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7_H
#ifndef NDQBNOTBLOSBJECRMEBQLJAXOIR_T45A7DACB04A60DE3316D0CC63C13D6E532F98E45_H
#define NDQBNOTBLOSBJECRMEBQLJAXOIR_T45A7DACB04A60DE3316D0CC63C13D6E532F98E45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_NDQBNoTBlOsBJEcRmEBQLJaXoIr
struct  NDQBNoTBlOsBJEcRmEBQLJaXoIr_t45A7DACB04A60DE3316D0CC63C13D6E532F98E45 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_NDQBNoTBlOsBJEcRmEBQLJaXoIr::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_NDQBNoTBlOsBJEcRmEBQLJaXoIr::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(NDQBNoTBlOsBJEcRmEBQLJaXoIr_t45A7DACB04A60DE3316D0CC63C13D6E532F98E45, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(NDQBNoTBlOsBJEcRmEBQLJaXoIr_t45A7DACB04A60DE3316D0CC63C13D6E532F98E45, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDQBNOTBLOSBJECRMEBQLJAXOIR_T45A7DACB04A60DE3316D0CC63C13D6E532F98E45_H
#ifndef NTNEQKVVHZJJJAIPKYADPIUBYHC_T533D3E9A605971771B256E7F1D1A399D298CF67A_H
#define NTNEQKVVHZJJJAIPKYADPIUBYHC_T533D3E9A605971771B256E7F1D1A399D298CF67A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_NtNEqkvvHzJjJAIPkyadPIubYhC
struct  NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_NtNEqkvvHzJjJAIPkyadPIubYhC::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_NtNEqkvvHzJjJAIPkyadPIubYhC::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.Int64 gvrkMEBkQRXiZPPNldGUfPXGyiXV_NtNEqkvvHzJjJAIPkyadPIubYhC::pdPsFQqEdLGUVvbWeUdQOqKnLLk
	int64_t ___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2;
	// System.Single gvrkMEBkQRXiZPPNldGUfPXGyiXV_NtNEqkvvHzJjJAIPkyadPIubYhC::vcxVnsZAdtFdxHDnEAdvlLzolNO
	float ___vcxVnsZAdtFdxHDnEAdvlLzolNO_3;
	// System.Single gvrkMEBkQRXiZPPNldGUfPXGyiXV_NtNEqkvvHzJjJAIPkyadPIubYhC::BjsLAHUTiYpFFQHPyuxDpPpFPMK
	float ___BjsLAHUTiYpFFQHPyuxDpPpFPMK_4;
	// System.Single gvrkMEBkQRXiZPPNldGUfPXGyiXV_NtNEqkvvHzJjJAIPkyadPIubYhC::IPHGhnvmvweeXFsNSmpniuYFNHcM
	float ___IPHGhnvmvweeXFsNSmpniuYFNHcM_5;
	// System.Single gvrkMEBkQRXiZPPNldGUfPXGyiXV_NtNEqkvvHzJjJAIPkyadPIubYhC::ycICvmDyXClaVDHBtBHHgVBlmCs
	float ___ycICvmDyXClaVDHBtBHHgVBlmCs_6;
	// System.UInt16 gvrkMEBkQRXiZPPNldGUfPXGyiXV_NtNEqkvvHzJjJAIPkyadPIubYhC::KAuIbltThGGoDXvARunxJofUqEd
	uint16_t ___KAuIbltThGGoDXvARunxJofUqEd_7;
	// System.UInt16 gvrkMEBkQRXiZPPNldGUfPXGyiXV_NtNEqkvvHzJjJAIPkyadPIubYhC::ijDYEGjuhTUlGZCjrQEIeQUbThP
	uint16_t ___ijDYEGjuhTUlGZCjrQEIeQUbThP_8;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2() { return static_cast<int32_t>(offsetof(NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A, ___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2)); }
	inline int64_t get_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2() const { return ___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2; }
	inline int64_t* get_address_of_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2() { return &___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2; }
	inline void set_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2(int64_t value)
	{
		___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2 = value;
	}

	inline static int32_t get_offset_of_vcxVnsZAdtFdxHDnEAdvlLzolNO_3() { return static_cast<int32_t>(offsetof(NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A, ___vcxVnsZAdtFdxHDnEAdvlLzolNO_3)); }
	inline float get_vcxVnsZAdtFdxHDnEAdvlLzolNO_3() const { return ___vcxVnsZAdtFdxHDnEAdvlLzolNO_3; }
	inline float* get_address_of_vcxVnsZAdtFdxHDnEAdvlLzolNO_3() { return &___vcxVnsZAdtFdxHDnEAdvlLzolNO_3; }
	inline void set_vcxVnsZAdtFdxHDnEAdvlLzolNO_3(float value)
	{
		___vcxVnsZAdtFdxHDnEAdvlLzolNO_3 = value;
	}

	inline static int32_t get_offset_of_BjsLAHUTiYpFFQHPyuxDpPpFPMK_4() { return static_cast<int32_t>(offsetof(NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A, ___BjsLAHUTiYpFFQHPyuxDpPpFPMK_4)); }
	inline float get_BjsLAHUTiYpFFQHPyuxDpPpFPMK_4() const { return ___BjsLAHUTiYpFFQHPyuxDpPpFPMK_4; }
	inline float* get_address_of_BjsLAHUTiYpFFQHPyuxDpPpFPMK_4() { return &___BjsLAHUTiYpFFQHPyuxDpPpFPMK_4; }
	inline void set_BjsLAHUTiYpFFQHPyuxDpPpFPMK_4(float value)
	{
		___BjsLAHUTiYpFFQHPyuxDpPpFPMK_4 = value;
	}

	inline static int32_t get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_5() { return static_cast<int32_t>(offsetof(NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A, ___IPHGhnvmvweeXFsNSmpniuYFNHcM_5)); }
	inline float get_IPHGhnvmvweeXFsNSmpniuYFNHcM_5() const { return ___IPHGhnvmvweeXFsNSmpniuYFNHcM_5; }
	inline float* get_address_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_5() { return &___IPHGhnvmvweeXFsNSmpniuYFNHcM_5; }
	inline void set_IPHGhnvmvweeXFsNSmpniuYFNHcM_5(float value)
	{
		___IPHGhnvmvweeXFsNSmpniuYFNHcM_5 = value;
	}

	inline static int32_t get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_6() { return static_cast<int32_t>(offsetof(NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A, ___ycICvmDyXClaVDHBtBHHgVBlmCs_6)); }
	inline float get_ycICvmDyXClaVDHBtBHHgVBlmCs_6() const { return ___ycICvmDyXClaVDHBtBHHgVBlmCs_6; }
	inline float* get_address_of_ycICvmDyXClaVDHBtBHHgVBlmCs_6() { return &___ycICvmDyXClaVDHBtBHHgVBlmCs_6; }
	inline void set_ycICvmDyXClaVDHBtBHHgVBlmCs_6(float value)
	{
		___ycICvmDyXClaVDHBtBHHgVBlmCs_6 = value;
	}

	inline static int32_t get_offset_of_KAuIbltThGGoDXvARunxJofUqEd_7() { return static_cast<int32_t>(offsetof(NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A, ___KAuIbltThGGoDXvARunxJofUqEd_7)); }
	inline uint16_t get_KAuIbltThGGoDXvARunxJofUqEd_7() const { return ___KAuIbltThGGoDXvARunxJofUqEd_7; }
	inline uint16_t* get_address_of_KAuIbltThGGoDXvARunxJofUqEd_7() { return &___KAuIbltThGGoDXvARunxJofUqEd_7; }
	inline void set_KAuIbltThGGoDXvARunxJofUqEd_7(uint16_t value)
	{
		___KAuIbltThGGoDXvARunxJofUqEd_7 = value;
	}

	inline static int32_t get_offset_of_ijDYEGjuhTUlGZCjrQEIeQUbThP_8() { return static_cast<int32_t>(offsetof(NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A, ___ijDYEGjuhTUlGZCjrQEIeQUbThP_8)); }
	inline uint16_t get_ijDYEGjuhTUlGZCjrQEIeQUbThP_8() const { return ___ijDYEGjuhTUlGZCjrQEIeQUbThP_8; }
	inline uint16_t* get_address_of_ijDYEGjuhTUlGZCjrQEIeQUbThP_8() { return &___ijDYEGjuhTUlGZCjrQEIeQUbThP_8; }
	inline void set_ijDYEGjuhTUlGZCjrQEIeQUbThP_8(uint16_t value)
	{
		___ijDYEGjuhTUlGZCjrQEIeQUbThP_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTNEQKVVHZJJJAIPKYADPIUBYHC_T533D3E9A605971771B256E7F1D1A399D298CF67A_H
#ifndef NUILWZWYVWKSUEGYJVMBRZBRPDB_T548FC98C7A261E12B46365964198415A5130672A_H
#define NUILWZWYVWKSUEGYJVMBRZBRPDB_T548FC98C7A261E12B46365964198415A5130672A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_NuilWzwYVWksUeGyjvmBrzbRpDb
struct  NuilWzwYVWksUeGyjvmBrzbRpDb_t548FC98C7A261E12B46365964198415A5130672A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUILWZWYVWKSUEGYJVMBRZBRPDB_T548FC98C7A261E12B46365964198415A5130672A_H
#ifndef OPZOLOVCSBBRRQTBZHILIDWDFWXS_T9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B_H
#define OPZOLOVCSBBRRQTBZHILIDWDFWXS_T9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_OPZOlOvCSbbrRqTbZhILIdWdFWXS
struct  OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_OPZOlOvCSbbrRqTbZhILIdWdFWXS::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_OPZOlOvCSbbrRqTbZhILIdWdFWXS::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_OPZOlOvCSbbrRqTbZhILIdWdFWXS::NkqzOUnpKxOLEIHqzuEvIwFSEWZ
	uint32_t ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_OPZOlOvCSbbrRqTbZhILIdWdFWXS::TfvRQYcdiDtBgzDqHlgvfCimrmM
	uint8_t ___TfvRQYcdiDtBgzDqHlgvfCimrmM_3;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_OPZOlOvCSbbrRqTbZhILIdWdFWXS::BfGikzYXODBhJWbccUmzPNgAJjN
	uint8_t ___BfGikzYXODBhJWbccUmzPNgAJjN_4;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_OPZOlOvCSbbrRqTbZhILIdWdFWXS::ZdxNPrFvibjxZCDAFwWLoADXmOO
	uint8_t ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_OPZOlOvCSbbrRqTbZhILIdWdFWXS::lbxzKBvrbNcnDcVvpWWCKOWJRDOM
	uint8_t ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_OPZOlOvCSbbrRqTbZhILIdWdFWXS::JCCrdLSGQdggHRDYqAACGkbYovTE
	int32_t ___JCCrdLSGQdggHRDYqAACGkbYovTE_7;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_OPZOlOvCSbbrRqTbZhILIdWdFWXS::BXVWEQMXuThJrooJLjbaKoYxuZu
	int32_t ___BXVWEQMXuThJrooJLjbaKoYxuZu_8;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return static_cast<int32_t>(offsetof(OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B, ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2)); }
	inline uint32_t get_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() const { return ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline uint32_t* get_address_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return &___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline void set_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2(uint32_t value)
	{
		___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2 = value;
	}

	inline static int32_t get_offset_of_TfvRQYcdiDtBgzDqHlgvfCimrmM_3() { return static_cast<int32_t>(offsetof(OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B, ___TfvRQYcdiDtBgzDqHlgvfCimrmM_3)); }
	inline uint8_t get_TfvRQYcdiDtBgzDqHlgvfCimrmM_3() const { return ___TfvRQYcdiDtBgzDqHlgvfCimrmM_3; }
	inline uint8_t* get_address_of_TfvRQYcdiDtBgzDqHlgvfCimrmM_3() { return &___TfvRQYcdiDtBgzDqHlgvfCimrmM_3; }
	inline void set_TfvRQYcdiDtBgzDqHlgvfCimrmM_3(uint8_t value)
	{
		___TfvRQYcdiDtBgzDqHlgvfCimrmM_3 = value;
	}

	inline static int32_t get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_4() { return static_cast<int32_t>(offsetof(OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B, ___BfGikzYXODBhJWbccUmzPNgAJjN_4)); }
	inline uint8_t get_BfGikzYXODBhJWbccUmzPNgAJjN_4() const { return ___BfGikzYXODBhJWbccUmzPNgAJjN_4; }
	inline uint8_t* get_address_of_BfGikzYXODBhJWbccUmzPNgAJjN_4() { return &___BfGikzYXODBhJWbccUmzPNgAJjN_4; }
	inline void set_BfGikzYXODBhJWbccUmzPNgAJjN_4(uint8_t value)
	{
		___BfGikzYXODBhJWbccUmzPNgAJjN_4 = value;
	}

	inline static int32_t get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() { return static_cast<int32_t>(offsetof(OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B, ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5)); }
	inline uint8_t get_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() const { return ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5; }
	inline uint8_t* get_address_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() { return &___ZdxNPrFvibjxZCDAFwWLoADXmOO_5; }
	inline void set_ZdxNPrFvibjxZCDAFwWLoADXmOO_5(uint8_t value)
	{
		___ZdxNPrFvibjxZCDAFwWLoADXmOO_5 = value;
	}

	inline static int32_t get_offset_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() { return static_cast<int32_t>(offsetof(OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B, ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6)); }
	inline uint8_t get_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() const { return ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6; }
	inline uint8_t* get_address_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() { return &___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6; }
	inline void set_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6(uint8_t value)
	{
		___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6 = value;
	}

	inline static int32_t get_offset_of_JCCrdLSGQdggHRDYqAACGkbYovTE_7() { return static_cast<int32_t>(offsetof(OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B, ___JCCrdLSGQdggHRDYqAACGkbYovTE_7)); }
	inline int32_t get_JCCrdLSGQdggHRDYqAACGkbYovTE_7() const { return ___JCCrdLSGQdggHRDYqAACGkbYovTE_7; }
	inline int32_t* get_address_of_JCCrdLSGQdggHRDYqAACGkbYovTE_7() { return &___JCCrdLSGQdggHRDYqAACGkbYovTE_7; }
	inline void set_JCCrdLSGQdggHRDYqAACGkbYovTE_7(int32_t value)
	{
		___JCCrdLSGQdggHRDYqAACGkbYovTE_7 = value;
	}

	inline static int32_t get_offset_of_BXVWEQMXuThJrooJLjbaKoYxuZu_8() { return static_cast<int32_t>(offsetof(OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B, ___BXVWEQMXuThJrooJLjbaKoYxuZu_8)); }
	inline int32_t get_BXVWEQMXuThJrooJLjbaKoYxuZu_8() const { return ___BXVWEQMXuThJrooJLjbaKoYxuZu_8; }
	inline int32_t* get_address_of_BXVWEQMXuThJrooJLjbaKoYxuZu_8() { return &___BXVWEQMXuThJrooJLjbaKoYxuZu_8; }
	inline void set_BXVWEQMXuThJrooJLjbaKoYxuZu_8(int32_t value)
	{
		___BXVWEQMXuThJrooJLjbaKoYxuZu_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPZOLOVCSBBRRQTBZHILIDWDFWXS_T9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B_H
#ifndef POESIXQIOONZTYJIGIYOYJUWCUO_T0058692460D618BAF0F68903A4E3ED5BD0B3857E_H
#define POESIXQIOONZTYJIGIYOYJUWCUO_T0058692460D618BAF0F68903A4E3ED5BD0B3857E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_PoESixqIoOnzTYjigIYOyJuWcuO
struct  PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_PoESixqIoOnzTYjigIYOyJuWcuO::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_PoESixqIoOnzTYjigIYOyJuWcuO::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_PoESixqIoOnzTYjigIYOyJuWcuO::ZXOWQoAgWuCntwkclqxZSCDQZSd
	int32_t ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_PoESixqIoOnzTYjigIYOyJuWcuO::SkTLbmFuuQqlUrgpLBTMhHVWQDIr
	uint8_t ___SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_PoESixqIoOnzTYjigIYOyJuWcuO::BfGikzYXODBhJWbccUmzPNgAJjN
	uint8_t ___BfGikzYXODBhJWbccUmzPNgAJjN_4;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_PoESixqIoOnzTYjigIYOyJuWcuO::ZdxNPrFvibjxZCDAFwWLoADXmOO
	uint8_t ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_PoESixqIoOnzTYjigIYOyJuWcuO::lbxzKBvrbNcnDcVvpWWCKOWJRDOM
	uint8_t ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6;
	// System.Int16 gvrkMEBkQRXiZPPNldGUfPXGyiXV_PoESixqIoOnzTYjigIYOyJuWcuO::ujWJxEJcSVHSdnQYrtHnXDWQpnZ
	int16_t ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7;
	// System.UInt16 gvrkMEBkQRXiZPPNldGUfPXGyiXV_PoESixqIoOnzTYjigIYOyJuWcuO::rcnYqJxaOwhrLxwckBmxkdQUgyvz
	uint16_t ___rcnYqJxaOwhrLxwckBmxkdQUgyvz_8;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return static_cast<int32_t>(offsetof(PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E, ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2)); }
	inline int32_t get_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() const { return ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline int32_t* get_address_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return &___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline void set_ZXOWQoAgWuCntwkclqxZSCDQZSd_2(int32_t value)
	{
		___ZXOWQoAgWuCntwkclqxZSCDQZSd_2 = value;
	}

	inline static int32_t get_offset_of_SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3() { return static_cast<int32_t>(offsetof(PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E, ___SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3)); }
	inline uint8_t get_SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3() const { return ___SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3; }
	inline uint8_t* get_address_of_SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3() { return &___SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3; }
	inline void set_SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3(uint8_t value)
	{
		___SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3 = value;
	}

	inline static int32_t get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_4() { return static_cast<int32_t>(offsetof(PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E, ___BfGikzYXODBhJWbccUmzPNgAJjN_4)); }
	inline uint8_t get_BfGikzYXODBhJWbccUmzPNgAJjN_4() const { return ___BfGikzYXODBhJWbccUmzPNgAJjN_4; }
	inline uint8_t* get_address_of_BfGikzYXODBhJWbccUmzPNgAJjN_4() { return &___BfGikzYXODBhJWbccUmzPNgAJjN_4; }
	inline void set_BfGikzYXODBhJWbccUmzPNgAJjN_4(uint8_t value)
	{
		___BfGikzYXODBhJWbccUmzPNgAJjN_4 = value;
	}

	inline static int32_t get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() { return static_cast<int32_t>(offsetof(PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E, ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5)); }
	inline uint8_t get_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() const { return ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5; }
	inline uint8_t* get_address_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() { return &___ZdxNPrFvibjxZCDAFwWLoADXmOO_5; }
	inline void set_ZdxNPrFvibjxZCDAFwWLoADXmOO_5(uint8_t value)
	{
		___ZdxNPrFvibjxZCDAFwWLoADXmOO_5 = value;
	}

	inline static int32_t get_offset_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() { return static_cast<int32_t>(offsetof(PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E, ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6)); }
	inline uint8_t get_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() const { return ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6; }
	inline uint8_t* get_address_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() { return &___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6; }
	inline void set_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6(uint8_t value)
	{
		___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6 = value;
	}

	inline static int32_t get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7() { return static_cast<int32_t>(offsetof(PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E, ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7)); }
	inline int16_t get_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7() const { return ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7; }
	inline int16_t* get_address_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7() { return &___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7; }
	inline void set_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7(int16_t value)
	{
		___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7 = value;
	}

	inline static int32_t get_offset_of_rcnYqJxaOwhrLxwckBmxkdQUgyvz_8() { return static_cast<int32_t>(offsetof(PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E, ___rcnYqJxaOwhrLxwckBmxkdQUgyvz_8)); }
	inline uint16_t get_rcnYqJxaOwhrLxwckBmxkdQUgyvz_8() const { return ___rcnYqJxaOwhrLxwckBmxkdQUgyvz_8; }
	inline uint16_t* get_address_of_rcnYqJxaOwhrLxwckBmxkdQUgyvz_8() { return &___rcnYqJxaOwhrLxwckBmxkdQUgyvz_8; }
	inline void set_rcnYqJxaOwhrLxwckBmxkdQUgyvz_8(uint16_t value)
	{
		___rcnYqJxaOwhrLxwckBmxkdQUgyvz_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POESIXQIOONZTYJIGIYOYJUWCUO_T0058692460D618BAF0F68903A4E3ED5BD0B3857E_H
#ifndef QPTPOKHMTAOFLDDXOBXERWOHGYAE_TF7B59678BFE350B1F54AE0C2586209BB47960287_H
#define QPTPOKHMTAOFLDDXOBXERWOHGYAE_TF7B59678BFE350B1F54AE0C2586209BB47960287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_QpTpOKhmTAOfldDXoBXERWoHGYae
struct  QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_QpTpOKhmTAOfldDXoBXERWoHGYae::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_QpTpOKhmTAOfldDXoBXERWoHGYae::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_QpTpOKhmTAOfldDXoBXERWoHGYae::ZXOWQoAgWuCntwkclqxZSCDQZSd
	int32_t ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_QpTpOKhmTAOfldDXoBXERWoHGYae::csbagSaGlXaYrxByCxECLgSeBxGq
	uint8_t ___csbagSaGlXaYrxByCxECLgSeBxGq_3;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_QpTpOKhmTAOfldDXoBXERWoHGYae::IyCUGfzcPoSzeXqQnApzwoMSQTs
	uint8_t ___IyCUGfzcPoSzeXqQnApzwoMSQTs_4;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_QpTpOKhmTAOfldDXoBXERWoHGYae::BfGikzYXODBhJWbccUmzPNgAJjN
	uint8_t ___BfGikzYXODBhJWbccUmzPNgAJjN_5;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_QpTpOKhmTAOfldDXoBXERWoHGYae::ZdxNPrFvibjxZCDAFwWLoADXmOO
	uint8_t ___ZdxNPrFvibjxZCDAFwWLoADXmOO_6;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return static_cast<int32_t>(offsetof(QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287, ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2)); }
	inline int32_t get_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() const { return ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline int32_t* get_address_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return &___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline void set_ZXOWQoAgWuCntwkclqxZSCDQZSd_2(int32_t value)
	{
		___ZXOWQoAgWuCntwkclqxZSCDQZSd_2 = value;
	}

	inline static int32_t get_offset_of_csbagSaGlXaYrxByCxECLgSeBxGq_3() { return static_cast<int32_t>(offsetof(QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287, ___csbagSaGlXaYrxByCxECLgSeBxGq_3)); }
	inline uint8_t get_csbagSaGlXaYrxByCxECLgSeBxGq_3() const { return ___csbagSaGlXaYrxByCxECLgSeBxGq_3; }
	inline uint8_t* get_address_of_csbagSaGlXaYrxByCxECLgSeBxGq_3() { return &___csbagSaGlXaYrxByCxECLgSeBxGq_3; }
	inline void set_csbagSaGlXaYrxByCxECLgSeBxGq_3(uint8_t value)
	{
		___csbagSaGlXaYrxByCxECLgSeBxGq_3 = value;
	}

	inline static int32_t get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_4() { return static_cast<int32_t>(offsetof(QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287, ___IyCUGfzcPoSzeXqQnApzwoMSQTs_4)); }
	inline uint8_t get_IyCUGfzcPoSzeXqQnApzwoMSQTs_4() const { return ___IyCUGfzcPoSzeXqQnApzwoMSQTs_4; }
	inline uint8_t* get_address_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_4() { return &___IyCUGfzcPoSzeXqQnApzwoMSQTs_4; }
	inline void set_IyCUGfzcPoSzeXqQnApzwoMSQTs_4(uint8_t value)
	{
		___IyCUGfzcPoSzeXqQnApzwoMSQTs_4 = value;
	}

	inline static int32_t get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_5() { return static_cast<int32_t>(offsetof(QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287, ___BfGikzYXODBhJWbccUmzPNgAJjN_5)); }
	inline uint8_t get_BfGikzYXODBhJWbccUmzPNgAJjN_5() const { return ___BfGikzYXODBhJWbccUmzPNgAJjN_5; }
	inline uint8_t* get_address_of_BfGikzYXODBhJWbccUmzPNgAJjN_5() { return &___BfGikzYXODBhJWbccUmzPNgAJjN_5; }
	inline void set_BfGikzYXODBhJWbccUmzPNgAJjN_5(uint8_t value)
	{
		___BfGikzYXODBhJWbccUmzPNgAJjN_5 = value;
	}

	inline static int32_t get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_6() { return static_cast<int32_t>(offsetof(QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287, ___ZdxNPrFvibjxZCDAFwWLoADXmOO_6)); }
	inline uint8_t get_ZdxNPrFvibjxZCDAFwWLoADXmOO_6() const { return ___ZdxNPrFvibjxZCDAFwWLoADXmOO_6; }
	inline uint8_t* get_address_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_6() { return &___ZdxNPrFvibjxZCDAFwWLoADXmOO_6; }
	inline void set_ZdxNPrFvibjxZCDAFwWLoADXmOO_6(uint8_t value)
	{
		___ZdxNPrFvibjxZCDAFwWLoADXmOO_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QPTPOKHMTAOFLDDXOBXERWOHGYAE_TF7B59678BFE350B1F54AE0C2586209BB47960287_H
#ifndef TJBFDUJLUUNJSERTGBQWLVNUXCBN_TF7575DDBE7C1E0576AE53D90CA237CE702106E9C_H
#define TJBFDUJLUUNJSERTGBQWLVNUXCBN_TF7575DDBE7C1E0576AE53D90CA237CE702106E9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_TJBFDUJLuUnjSERTgbqWlvNUxcBN
struct  TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_TJBFDUJLuUnjSERTgbqWlvNUxcBN::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TJBFDUJLuUnjSERTgbqWlvNUxcBN::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TJBFDUJLuUnjSERTgbqWlvNUxcBN::NkqzOUnpKxOLEIHqzuEvIwFSEWZ
	uint32_t ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TJBFDUJLuUnjSERTgbqWlvNUxcBN::ZXOWQoAgWuCntwkclqxZSCDQZSd
	uint32_t ___ZXOWQoAgWuCntwkclqxZSCDQZSd_3;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TJBFDUJLuUnjSERTgbqWlvNUxcBN::IyCUGfzcPoSzeXqQnApzwoMSQTs
	uint32_t ___IyCUGfzcPoSzeXqQnApzwoMSQTs_4;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TJBFDUJLuUnjSERTgbqWlvNUxcBN::IPHGhnvmvweeXFsNSmpniuYFNHcM
	int32_t ___IPHGhnvmvweeXFsNSmpniuYFNHcM_5;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TJBFDUJLuUnjSERTgbqWlvNUxcBN::ycICvmDyXClaVDHBtBHHgVBlmCs
	int32_t ___ycICvmDyXClaVDHBtBHHgVBlmCs_6;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TJBFDUJLuUnjSERTgbqWlvNUxcBN::YoIVBUOGrzDHUpJJHxXhsIPTqVX
	int32_t ___YoIVBUOGrzDHUpJJHxXhsIPTqVX_7;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TJBFDUJLuUnjSERTgbqWlvNUxcBN::fefuBVpjOXbPneAFgnpstaUWSLV
	int32_t ___fefuBVpjOXbPneAFgnpstaUWSLV_8;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return static_cast<int32_t>(offsetof(TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C, ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2)); }
	inline uint32_t get_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() const { return ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline uint32_t* get_address_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return &___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline void set_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2(uint32_t value)
	{
		___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2 = value;
	}

	inline static int32_t get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_3() { return static_cast<int32_t>(offsetof(TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C, ___ZXOWQoAgWuCntwkclqxZSCDQZSd_3)); }
	inline uint32_t get_ZXOWQoAgWuCntwkclqxZSCDQZSd_3() const { return ___ZXOWQoAgWuCntwkclqxZSCDQZSd_3; }
	inline uint32_t* get_address_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_3() { return &___ZXOWQoAgWuCntwkclqxZSCDQZSd_3; }
	inline void set_ZXOWQoAgWuCntwkclqxZSCDQZSd_3(uint32_t value)
	{
		___ZXOWQoAgWuCntwkclqxZSCDQZSd_3 = value;
	}

	inline static int32_t get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_4() { return static_cast<int32_t>(offsetof(TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C, ___IyCUGfzcPoSzeXqQnApzwoMSQTs_4)); }
	inline uint32_t get_IyCUGfzcPoSzeXqQnApzwoMSQTs_4() const { return ___IyCUGfzcPoSzeXqQnApzwoMSQTs_4; }
	inline uint32_t* get_address_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_4() { return &___IyCUGfzcPoSzeXqQnApzwoMSQTs_4; }
	inline void set_IyCUGfzcPoSzeXqQnApzwoMSQTs_4(uint32_t value)
	{
		___IyCUGfzcPoSzeXqQnApzwoMSQTs_4 = value;
	}

	inline static int32_t get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_5() { return static_cast<int32_t>(offsetof(TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C, ___IPHGhnvmvweeXFsNSmpniuYFNHcM_5)); }
	inline int32_t get_IPHGhnvmvweeXFsNSmpniuYFNHcM_5() const { return ___IPHGhnvmvweeXFsNSmpniuYFNHcM_5; }
	inline int32_t* get_address_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_5() { return &___IPHGhnvmvweeXFsNSmpniuYFNHcM_5; }
	inline void set_IPHGhnvmvweeXFsNSmpniuYFNHcM_5(int32_t value)
	{
		___IPHGhnvmvweeXFsNSmpniuYFNHcM_5 = value;
	}

	inline static int32_t get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_6() { return static_cast<int32_t>(offsetof(TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C, ___ycICvmDyXClaVDHBtBHHgVBlmCs_6)); }
	inline int32_t get_ycICvmDyXClaVDHBtBHHgVBlmCs_6() const { return ___ycICvmDyXClaVDHBtBHHgVBlmCs_6; }
	inline int32_t* get_address_of_ycICvmDyXClaVDHBtBHHgVBlmCs_6() { return &___ycICvmDyXClaVDHBtBHHgVBlmCs_6; }
	inline void set_ycICvmDyXClaVDHBtBHHgVBlmCs_6(int32_t value)
	{
		___ycICvmDyXClaVDHBtBHHgVBlmCs_6 = value;
	}

	inline static int32_t get_offset_of_YoIVBUOGrzDHUpJJHxXhsIPTqVX_7() { return static_cast<int32_t>(offsetof(TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C, ___YoIVBUOGrzDHUpJJHxXhsIPTqVX_7)); }
	inline int32_t get_YoIVBUOGrzDHUpJJHxXhsIPTqVX_7() const { return ___YoIVBUOGrzDHUpJJHxXhsIPTqVX_7; }
	inline int32_t* get_address_of_YoIVBUOGrzDHUpJJHxXhsIPTqVX_7() { return &___YoIVBUOGrzDHUpJJHxXhsIPTqVX_7; }
	inline void set_YoIVBUOGrzDHUpJJHxXhsIPTqVX_7(int32_t value)
	{
		___YoIVBUOGrzDHUpJJHxXhsIPTqVX_7 = value;
	}

	inline static int32_t get_offset_of_fefuBVpjOXbPneAFgnpstaUWSLV_8() { return static_cast<int32_t>(offsetof(TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C, ___fefuBVpjOXbPneAFgnpstaUWSLV_8)); }
	inline int32_t get_fefuBVpjOXbPneAFgnpstaUWSLV_8() const { return ___fefuBVpjOXbPneAFgnpstaUWSLV_8; }
	inline int32_t* get_address_of_fefuBVpjOXbPneAFgnpstaUWSLV_8() { return &___fefuBVpjOXbPneAFgnpstaUWSLV_8; }
	inline void set_fefuBVpjOXbPneAFgnpstaUWSLV_8(int32_t value)
	{
		___fefuBVpjOXbPneAFgnpstaUWSLV_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TJBFDUJLUUNJSERTGBQWLVNUXCBN_TF7575DDBE7C1E0576AE53D90CA237CE702106E9C_H
#ifndef TWCQEOKPJAFFKKNKIMWXJAJKGSQX_T801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8_H
#define TWCQEOKPJAFFKKNKIMWXJAJKGSQX_T801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_TWCQEOkPJAfFkkNkImwxJAjkgSqX
struct  TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_TWCQEOkPJAfFkkNkImwxJAjkgSqX::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TWCQEOkPJAfFkkNkImwxJAjkgSqX::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TWCQEOkPJAfFkkNkImwxJAjkgSqX::NkqzOUnpKxOLEIHqzuEvIwFSEWZ
	uint32_t ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TWCQEOkPJAfFkkNkImwxJAjkgSqX::wKqdKqPaFDoabmkqRipBJbznWve
	int32_t ___wKqdKqPaFDoabmkqRipBJbznWve_3;
	// System.IntPtr gvrkMEBkQRXiZPPNldGUfPXGyiXV_TWCQEOkPJAfFkkNkImwxJAjkgSqX::JCCrdLSGQdggHRDYqAACGkbYovTE
	intptr_t ___JCCrdLSGQdggHRDYqAACGkbYovTE_4;
	// System.IntPtr gvrkMEBkQRXiZPPNldGUfPXGyiXV_TWCQEOkPJAfFkkNkImwxJAjkgSqX::BXVWEQMXuThJrooJLjbaKoYxuZu
	intptr_t ___BXVWEQMXuThJrooJLjbaKoYxuZu_5;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return static_cast<int32_t>(offsetof(TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8, ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2)); }
	inline uint32_t get_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() const { return ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline uint32_t* get_address_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return &___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline void set_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2(uint32_t value)
	{
		___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2 = value;
	}

	inline static int32_t get_offset_of_wKqdKqPaFDoabmkqRipBJbznWve_3() { return static_cast<int32_t>(offsetof(TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8, ___wKqdKqPaFDoabmkqRipBJbznWve_3)); }
	inline int32_t get_wKqdKqPaFDoabmkqRipBJbznWve_3() const { return ___wKqdKqPaFDoabmkqRipBJbznWve_3; }
	inline int32_t* get_address_of_wKqdKqPaFDoabmkqRipBJbznWve_3() { return &___wKqdKqPaFDoabmkqRipBJbznWve_3; }
	inline void set_wKqdKqPaFDoabmkqRipBJbznWve_3(int32_t value)
	{
		___wKqdKqPaFDoabmkqRipBJbznWve_3 = value;
	}

	inline static int32_t get_offset_of_JCCrdLSGQdggHRDYqAACGkbYovTE_4() { return static_cast<int32_t>(offsetof(TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8, ___JCCrdLSGQdggHRDYqAACGkbYovTE_4)); }
	inline intptr_t get_JCCrdLSGQdggHRDYqAACGkbYovTE_4() const { return ___JCCrdLSGQdggHRDYqAACGkbYovTE_4; }
	inline intptr_t* get_address_of_JCCrdLSGQdggHRDYqAACGkbYovTE_4() { return &___JCCrdLSGQdggHRDYqAACGkbYovTE_4; }
	inline void set_JCCrdLSGQdggHRDYqAACGkbYovTE_4(intptr_t value)
	{
		___JCCrdLSGQdggHRDYqAACGkbYovTE_4 = value;
	}

	inline static int32_t get_offset_of_BXVWEQMXuThJrooJLjbaKoYxuZu_5() { return static_cast<int32_t>(offsetof(TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8, ___BXVWEQMXuThJrooJLjbaKoYxuZu_5)); }
	inline intptr_t get_BXVWEQMXuThJrooJLjbaKoYxuZu_5() const { return ___BXVWEQMXuThJrooJLjbaKoYxuZu_5; }
	inline intptr_t* get_address_of_BXVWEQMXuThJrooJLjbaKoYxuZu_5() { return &___BXVWEQMXuThJrooJLjbaKoYxuZu_5; }
	inline void set_BXVWEQMXuThJrooJLjbaKoYxuZu_5(intptr_t value)
	{
		___BXVWEQMXuThJrooJLjbaKoYxuZu_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWCQEOKPJAFFKKNKIMWXJAJKGSQX_T801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8_H
#ifndef TXMCBCAOFZOXYNUVYHZLNYYQOLCB_TF7CCD8C10A8BD371E967558358FEAFBA067AEC62_H
#define TXMCBCAOFZOXYNUVYHZLNYYQOLCB_TF7CCD8C10A8BD371E967558358FEAFBA067AEC62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_TXmcBcaOfZoXYnUVYHZlNyYQOLcB
struct  TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_TXmcBcaOfZoXYnUVYHZlNyYQOLcB::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TXmcBcaOfZoXYnUVYHZlNyYQOLcB::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TXmcBcaOfZoXYnUVYHZlNyYQOLcB::NkqzOUnpKxOLEIHqzuEvIwFSEWZ
	uint32_t ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TXmcBcaOfZoXYnUVYHZlNyYQOLcB::ZXOWQoAgWuCntwkclqxZSCDQZSd
	uint32_t ___ZXOWQoAgWuCntwkclqxZSCDQZSd_3;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TXmcBcaOfZoXYnUVYHZlNyYQOLcB::IPHGhnvmvweeXFsNSmpniuYFNHcM
	int32_t ___IPHGhnvmvweeXFsNSmpniuYFNHcM_4;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_TXmcBcaOfZoXYnUVYHZlNyYQOLcB::ycICvmDyXClaVDHBtBHHgVBlmCs
	int32_t ___ycICvmDyXClaVDHBtBHHgVBlmCs_5;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return static_cast<int32_t>(offsetof(TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62, ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2)); }
	inline uint32_t get_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() const { return ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline uint32_t* get_address_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return &___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline void set_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2(uint32_t value)
	{
		___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2 = value;
	}

	inline static int32_t get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_3() { return static_cast<int32_t>(offsetof(TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62, ___ZXOWQoAgWuCntwkclqxZSCDQZSd_3)); }
	inline uint32_t get_ZXOWQoAgWuCntwkclqxZSCDQZSd_3() const { return ___ZXOWQoAgWuCntwkclqxZSCDQZSd_3; }
	inline uint32_t* get_address_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_3() { return &___ZXOWQoAgWuCntwkclqxZSCDQZSd_3; }
	inline void set_ZXOWQoAgWuCntwkclqxZSCDQZSd_3(uint32_t value)
	{
		___ZXOWQoAgWuCntwkclqxZSCDQZSd_3 = value;
	}

	inline static int32_t get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_4() { return static_cast<int32_t>(offsetof(TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62, ___IPHGhnvmvweeXFsNSmpniuYFNHcM_4)); }
	inline int32_t get_IPHGhnvmvweeXFsNSmpniuYFNHcM_4() const { return ___IPHGhnvmvweeXFsNSmpniuYFNHcM_4; }
	inline int32_t* get_address_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_4() { return &___IPHGhnvmvweeXFsNSmpniuYFNHcM_4; }
	inline void set_IPHGhnvmvweeXFsNSmpniuYFNHcM_4(int32_t value)
	{
		___IPHGhnvmvweeXFsNSmpniuYFNHcM_4 = value;
	}

	inline static int32_t get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_5() { return static_cast<int32_t>(offsetof(TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62, ___ycICvmDyXClaVDHBtBHHgVBlmCs_5)); }
	inline int32_t get_ycICvmDyXClaVDHBtBHHgVBlmCs_5() const { return ___ycICvmDyXClaVDHBtBHHgVBlmCs_5; }
	inline int32_t* get_address_of_ycICvmDyXClaVDHBtBHHgVBlmCs_5() { return &___ycICvmDyXClaVDHBtBHHgVBlmCs_5; }
	inline void set_ycICvmDyXClaVDHBtBHHgVBlmCs_5(int32_t value)
	{
		___ycICvmDyXClaVDHBtBHHgVBlmCs_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TXMCBCAOFZOXYNUVYHZLNYYQOLCB_TF7CCD8C10A8BD371E967558358FEAFBA067AEC62_H
#ifndef URRWTJPTTHJUUEKFBFCFSKKKIZI_T95677B57949B3940BEE03C06F6DA325036AE3D61_H
#define URRWTJPTTHJUUEKFBFCFSKKKIZI_T95677B57949B3940BEE03C06F6DA325036AE3D61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_URRWTjPTthjUUeKFbfCfsKkkiZI
struct  URRWTjPTthjUUeKFbfCfsKkkiZI_t95677B57949B3940BEE03C06F6DA325036AE3D61  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URRWTJPTTHJUUEKFBFCFSKKKIZI_T95677B57949B3940BEE03C06F6DA325036AE3D61_H
#ifndef VKUCXQVSMAEMYTTDWEVZLTQEGYK_T40E5E49C17B8992C29B6E46D2CBDD10BDA91B7D8_H
#define VKUCXQVSMAEMYTTDWEVZLTQEGYK_T40E5E49C17B8992C29B6E46D2CBDD10BDA91B7D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_VKuCxQVsMAEmyTTdwEvzLtQeGyK
struct  VKuCxQVsMAEmyTTdwEvzLtQeGyK_t40E5E49C17B8992C29B6E46D2CBDD10BDA91B7D8 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_VKuCxQVsMAEmyTTdwEvzLtQeGyK::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_VKuCxQVsMAEmyTTdwEvzLtQeGyK::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(VKuCxQVsMAEmyTTdwEvzLtQeGyK_t40E5E49C17B8992C29B6E46D2CBDD10BDA91B7D8, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(VKuCxQVsMAEmyTTdwEvzLtQeGyK_t40E5E49C17B8992C29B6E46D2CBDD10BDA91B7D8, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VKUCXQVSMAEMYTTDWEVZLTQEGYK_T40E5E49C17B8992C29B6E46D2CBDD10BDA91B7D8_H
#ifndef ECLHPUFWJRDUHNCTKWDUJPTJKLF_T8520523D67DE64D00205144F8C3992FCC29B537A_H
#define ECLHPUFWJRDUHNCTKWDUJPTJKLF_T8520523D67DE64D00205144F8C3992FCC29B537A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_eCLHpuFWjrdUHnCTkwDUJptJKlF
struct  eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_eCLHpuFWjrdUHnCTkwDUJptJKlF::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_eCLHpuFWjrdUHnCTkwDUJptJKlF::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_eCLHpuFWjrdUHnCTkwDUJptJKlF::ZXOWQoAgWuCntwkclqxZSCDQZSd
	int32_t ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_eCLHpuFWjrdUHnCTkwDUJptJKlF::SkTLbmFuuQqlUrgpLBTMhHVWQDIr
	uint8_t ___SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_eCLHpuFWjrdUHnCTkwDUJptJKlF::BfGikzYXODBhJWbccUmzPNgAJjN
	uint8_t ___BfGikzYXODBhJWbccUmzPNgAJjN_4;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_eCLHpuFWjrdUHnCTkwDUJptJKlF::ZdxNPrFvibjxZCDAFwWLoADXmOO
	uint8_t ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_eCLHpuFWjrdUHnCTkwDUJptJKlF::lbxzKBvrbNcnDcVvpWWCKOWJRDOM
	uint8_t ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6;
	// System.Int16 gvrkMEBkQRXiZPPNldGUfPXGyiXV_eCLHpuFWjrdUHnCTkwDUJptJKlF::ujWJxEJcSVHSdnQYrtHnXDWQpnZ
	int16_t ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7;
	// System.UInt16 gvrkMEBkQRXiZPPNldGUfPXGyiXV_eCLHpuFWjrdUHnCTkwDUJptJKlF::rcnYqJxaOwhrLxwckBmxkdQUgyvz
	uint16_t ___rcnYqJxaOwhrLxwckBmxkdQUgyvz_8;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return static_cast<int32_t>(offsetof(eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A, ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2)); }
	inline int32_t get_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() const { return ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline int32_t* get_address_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return &___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline void set_ZXOWQoAgWuCntwkclqxZSCDQZSd_2(int32_t value)
	{
		___ZXOWQoAgWuCntwkclqxZSCDQZSd_2 = value;
	}

	inline static int32_t get_offset_of_SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3() { return static_cast<int32_t>(offsetof(eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A, ___SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3)); }
	inline uint8_t get_SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3() const { return ___SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3; }
	inline uint8_t* get_address_of_SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3() { return &___SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3; }
	inline void set_SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3(uint8_t value)
	{
		___SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3 = value;
	}

	inline static int32_t get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_4() { return static_cast<int32_t>(offsetof(eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A, ___BfGikzYXODBhJWbccUmzPNgAJjN_4)); }
	inline uint8_t get_BfGikzYXODBhJWbccUmzPNgAJjN_4() const { return ___BfGikzYXODBhJWbccUmzPNgAJjN_4; }
	inline uint8_t* get_address_of_BfGikzYXODBhJWbccUmzPNgAJjN_4() { return &___BfGikzYXODBhJWbccUmzPNgAJjN_4; }
	inline void set_BfGikzYXODBhJWbccUmzPNgAJjN_4(uint8_t value)
	{
		___BfGikzYXODBhJWbccUmzPNgAJjN_4 = value;
	}

	inline static int32_t get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() { return static_cast<int32_t>(offsetof(eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A, ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5)); }
	inline uint8_t get_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() const { return ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5; }
	inline uint8_t* get_address_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() { return &___ZdxNPrFvibjxZCDAFwWLoADXmOO_5; }
	inline void set_ZdxNPrFvibjxZCDAFwWLoADXmOO_5(uint8_t value)
	{
		___ZdxNPrFvibjxZCDAFwWLoADXmOO_5 = value;
	}

	inline static int32_t get_offset_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() { return static_cast<int32_t>(offsetof(eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A, ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6)); }
	inline uint8_t get_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() const { return ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6; }
	inline uint8_t* get_address_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() { return &___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6; }
	inline void set_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6(uint8_t value)
	{
		___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6 = value;
	}

	inline static int32_t get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7() { return static_cast<int32_t>(offsetof(eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A, ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7)); }
	inline int16_t get_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7() const { return ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7; }
	inline int16_t* get_address_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7() { return &___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7; }
	inline void set_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7(int16_t value)
	{
		___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7 = value;
	}

	inline static int32_t get_offset_of_rcnYqJxaOwhrLxwckBmxkdQUgyvz_8() { return static_cast<int32_t>(offsetof(eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A, ___rcnYqJxaOwhrLxwckBmxkdQUgyvz_8)); }
	inline uint16_t get_rcnYqJxaOwhrLxwckBmxkdQUgyvz_8() const { return ___rcnYqJxaOwhrLxwckBmxkdQUgyvz_8; }
	inline uint16_t* get_address_of_rcnYqJxaOwhrLxwckBmxkdQUgyvz_8() { return &___rcnYqJxaOwhrLxwckBmxkdQUgyvz_8; }
	inline void set_rcnYqJxaOwhrLxwckBmxkdQUgyvz_8(uint16_t value)
	{
		___rcnYqJxaOwhrLxwckBmxkdQUgyvz_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECLHPUFWJRDUHNCTKWDUJPTJKLF_T8520523D67DE64D00205144F8C3992FCC29B537A_H
#ifndef GXJEJSEZMQNSTKMBAONMESGVJMLK_TB57C34DAE50B8D11AF1F0CFB9E6A7CE14D1AA9B6_H
#define GXJEJSEZMQNSTKMBAONMESGVJMLK_TB57C34DAE50B8D11AF1F0CFB9E6A7CE14D1AA9B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_gXjEJSEzMqnStKMbAONmeSGVJmLk
struct  gXjEJSEzMqnStKMbAONmeSGVJmLk_tB57C34DAE50B8D11AF1F0CFB9E6A7CE14D1AA9B6 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_gXjEJSEzMqnStKMbAONmeSGVJmLk::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_gXjEJSEzMqnStKMbAONmeSGVJmLk::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(gXjEJSEzMqnStKMbAONmeSGVJmLk_tB57C34DAE50B8D11AF1F0CFB9E6A7CE14D1AA9B6, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(gXjEJSEzMqnStKMbAONmeSGVJmLk_tB57C34DAE50B8D11AF1F0CFB9E6A7CE14D1AA9B6, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GXJEJSEZMQNSTKMBAONMESGVJMLK_TB57C34DAE50B8D11AF1F0CFB9E6A7CE14D1AA9B6_H
#ifndef HHRQYTMYBQEBCXXHKLGAEEKULCT_T35500CC806EBB8E7E8386D51AFCC5C002FFE5BD4_H
#define HHRQYTMYBQEBCXXHKLGAEEKULCT_T35500CC806EBB8E7E8386D51AFCC5C002FFE5BD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_hhRqyTmYbqEBcxxHkLGAeeKuLcT
struct  hhRqyTmYbqEBcxxHkLGAeeKuLcT_t35500CC806EBB8E7E8386D51AFCC5C002FFE5BD4 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_hhRqyTmYbqEBcxxHkLGAeeKuLcT::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_hhRqyTmYbqEBcxxHkLGAeeKuLcT::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.IntPtr gvrkMEBkQRXiZPPNldGUfPXGyiXV_hhRqyTmYbqEBcxxHkLGAeeKuLcT::wVefQBdoQiPKrKrrWaBgrbMVAwre
	intptr_t ___wVefQBdoQiPKrKrrWaBgrbMVAwre_2;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(hhRqyTmYbqEBcxxHkLGAeeKuLcT_t35500CC806EBB8E7E8386D51AFCC5C002FFE5BD4, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(hhRqyTmYbqEBcxxHkLGAeeKuLcT_t35500CC806EBB8E7E8386D51AFCC5C002FFE5BD4, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_wVefQBdoQiPKrKrrWaBgrbMVAwre_2() { return static_cast<int32_t>(offsetof(hhRqyTmYbqEBcxxHkLGAeeKuLcT_t35500CC806EBB8E7E8386D51AFCC5C002FFE5BD4, ___wVefQBdoQiPKrKrrWaBgrbMVAwre_2)); }
	inline intptr_t get_wVefQBdoQiPKrKrrWaBgrbMVAwre_2() const { return ___wVefQBdoQiPKrKrrWaBgrbMVAwre_2; }
	inline intptr_t* get_address_of_wVefQBdoQiPKrKrrWaBgrbMVAwre_2() { return &___wVefQBdoQiPKrKrrWaBgrbMVAwre_2; }
	inline void set_wVefQBdoQiPKrKrrWaBgrbMVAwre_2(intptr_t value)
	{
		___wVefQBdoQiPKrKrrWaBgrbMVAwre_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HHRQYTMYBQEBCXXHKLGAEEKULCT_T35500CC806EBB8E7E8386D51AFCC5C002FFE5BD4_H
#ifndef KPSJUWRMSPEIZDCUDTWKGGMUQHZ_TEAC79B0FA254D95AD6F34B3EB910BF2B130134DE_H
#define KPSJUWRMSPEIZDCUDTWKGGMUQHZ_TEAC79B0FA254D95AD6F34B3EB910BF2B130134DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_kpsJuWrmSpeizdCUDTWKGgMUqHZ
struct  kpsJuWrmSpeizdCUDTWKGgMUqHZ_tEAC79B0FA254D95AD6F34B3EB910BF2B130134DE 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_kpsJuWrmSpeizdCUDTWKGgMUqHZ::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_kpsJuWrmSpeizdCUDTWKGgMUqHZ::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.IntPtr gvrkMEBkQRXiZPPNldGUfPXGyiXV_kpsJuWrmSpeizdCUDTWKGgMUqHZ::RXiIQIWsCHgXoujlGlykSAGJygs
	intptr_t ___RXiIQIWsCHgXoujlGlykSAGJygs_2;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(kpsJuWrmSpeizdCUDTWKGgMUqHZ_tEAC79B0FA254D95AD6F34B3EB910BF2B130134DE, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(kpsJuWrmSpeizdCUDTWKGgMUqHZ_tEAC79B0FA254D95AD6F34B3EB910BF2B130134DE, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_RXiIQIWsCHgXoujlGlykSAGJygs_2() { return static_cast<int32_t>(offsetof(kpsJuWrmSpeizdCUDTWKGgMUqHZ_tEAC79B0FA254D95AD6F34B3EB910BF2B130134DE, ___RXiIQIWsCHgXoujlGlykSAGJygs_2)); }
	inline intptr_t get_RXiIQIWsCHgXoujlGlykSAGJygs_2() const { return ___RXiIQIWsCHgXoujlGlykSAGJygs_2; }
	inline intptr_t* get_address_of_RXiIQIWsCHgXoujlGlykSAGJygs_2() { return &___RXiIQIWsCHgXoujlGlykSAGJygs_2; }
	inline void set_RXiIQIWsCHgXoujlGlykSAGJygs_2(intptr_t value)
	{
		___RXiIQIWsCHgXoujlGlykSAGJygs_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KPSJUWRMSPEIZDCUDTWKGGMUQHZ_TEAC79B0FA254D95AD6F34B3EB910BF2B130134DE_H
#ifndef OFJVNENQCARQYBQDTGKEKRZMBLE_TF0DD22883960F89C9077D66214D33DCFD74BEE55_H
#define OFJVNENQCARQYBQDTGKEKRZMBLE_TF0DD22883960F89C9077D66214D33DCFD74BEE55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_ofjvnEnqcArQYBqdtgKEKrZmblE
struct  ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_ofjvnEnqcArQYBqdtgKEKrZmblE::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_ofjvnEnqcArQYBqdtgKEKrZmblE::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.Int64 gvrkMEBkQRXiZPPNldGUfPXGyiXV_ofjvnEnqcArQYBqdtgKEKrZmblE::pdPsFQqEdLGUVvbWeUdQOqKnLLk
	int64_t ___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2;
	// System.Int64 gvrkMEBkQRXiZPPNldGUfPXGyiXV_ofjvnEnqcArQYBqdtgKEKrZmblE::figUHZpamkMwMcBcFCxvFEGezpWG
	int64_t ___figUHZpamkMwMcBcFCxvFEGezpWG_3;
	// System.Single gvrkMEBkQRXiZPPNldGUfPXGyiXV_ofjvnEnqcArQYBqdtgKEKrZmblE::IPHGhnvmvweeXFsNSmpniuYFNHcM
	float ___IPHGhnvmvweeXFsNSmpniuYFNHcM_4;
	// System.Single gvrkMEBkQRXiZPPNldGUfPXGyiXV_ofjvnEnqcArQYBqdtgKEKrZmblE::ycICvmDyXClaVDHBtBHHgVBlmCs
	float ___ycICvmDyXClaVDHBtBHHgVBlmCs_5;
	// System.Single gvrkMEBkQRXiZPPNldGUfPXGyiXV_ofjvnEnqcArQYBqdtgKEKrZmblE::wCvyujpPzllFaNLVlAidehkVJQfr
	float ___wCvyujpPzllFaNLVlAidehkVJQfr_6;
	// System.Single gvrkMEBkQRXiZPPNldGUfPXGyiXV_ofjvnEnqcArQYBqdtgKEKrZmblE::zYigHpHMdfbVRBdTNqJLRlhpVtg
	float ___zYigHpHMdfbVRBdTNqJLRlhpVtg_7;
	// System.Single gvrkMEBkQRXiZPPNldGUfPXGyiXV_ofjvnEnqcArQYBqdtgKEKrZmblE::TqfnCtqiMsHsZAmsVfRQfULPGnGz
	float ___TqfnCtqiMsHsZAmsVfRQfULPGnGz_8;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2() { return static_cast<int32_t>(offsetof(ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55, ___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2)); }
	inline int64_t get_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2() const { return ___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2; }
	inline int64_t* get_address_of_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2() { return &___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2; }
	inline void set_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2(int64_t value)
	{
		___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2 = value;
	}

	inline static int32_t get_offset_of_figUHZpamkMwMcBcFCxvFEGezpWG_3() { return static_cast<int32_t>(offsetof(ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55, ___figUHZpamkMwMcBcFCxvFEGezpWG_3)); }
	inline int64_t get_figUHZpamkMwMcBcFCxvFEGezpWG_3() const { return ___figUHZpamkMwMcBcFCxvFEGezpWG_3; }
	inline int64_t* get_address_of_figUHZpamkMwMcBcFCxvFEGezpWG_3() { return &___figUHZpamkMwMcBcFCxvFEGezpWG_3; }
	inline void set_figUHZpamkMwMcBcFCxvFEGezpWG_3(int64_t value)
	{
		___figUHZpamkMwMcBcFCxvFEGezpWG_3 = value;
	}

	inline static int32_t get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_4() { return static_cast<int32_t>(offsetof(ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55, ___IPHGhnvmvweeXFsNSmpniuYFNHcM_4)); }
	inline float get_IPHGhnvmvweeXFsNSmpniuYFNHcM_4() const { return ___IPHGhnvmvweeXFsNSmpniuYFNHcM_4; }
	inline float* get_address_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_4() { return &___IPHGhnvmvweeXFsNSmpniuYFNHcM_4; }
	inline void set_IPHGhnvmvweeXFsNSmpniuYFNHcM_4(float value)
	{
		___IPHGhnvmvweeXFsNSmpniuYFNHcM_4 = value;
	}

	inline static int32_t get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_5() { return static_cast<int32_t>(offsetof(ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55, ___ycICvmDyXClaVDHBtBHHgVBlmCs_5)); }
	inline float get_ycICvmDyXClaVDHBtBHHgVBlmCs_5() const { return ___ycICvmDyXClaVDHBtBHHgVBlmCs_5; }
	inline float* get_address_of_ycICvmDyXClaVDHBtBHHgVBlmCs_5() { return &___ycICvmDyXClaVDHBtBHHgVBlmCs_5; }
	inline void set_ycICvmDyXClaVDHBtBHHgVBlmCs_5(float value)
	{
		___ycICvmDyXClaVDHBtBHHgVBlmCs_5 = value;
	}

	inline static int32_t get_offset_of_wCvyujpPzllFaNLVlAidehkVJQfr_6() { return static_cast<int32_t>(offsetof(ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55, ___wCvyujpPzllFaNLVlAidehkVJQfr_6)); }
	inline float get_wCvyujpPzllFaNLVlAidehkVJQfr_6() const { return ___wCvyujpPzllFaNLVlAidehkVJQfr_6; }
	inline float* get_address_of_wCvyujpPzllFaNLVlAidehkVJQfr_6() { return &___wCvyujpPzllFaNLVlAidehkVJQfr_6; }
	inline void set_wCvyujpPzllFaNLVlAidehkVJQfr_6(float value)
	{
		___wCvyujpPzllFaNLVlAidehkVJQfr_6 = value;
	}

	inline static int32_t get_offset_of_zYigHpHMdfbVRBdTNqJLRlhpVtg_7() { return static_cast<int32_t>(offsetof(ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55, ___zYigHpHMdfbVRBdTNqJLRlhpVtg_7)); }
	inline float get_zYigHpHMdfbVRBdTNqJLRlhpVtg_7() const { return ___zYigHpHMdfbVRBdTNqJLRlhpVtg_7; }
	inline float* get_address_of_zYigHpHMdfbVRBdTNqJLRlhpVtg_7() { return &___zYigHpHMdfbVRBdTNqJLRlhpVtg_7; }
	inline void set_zYigHpHMdfbVRBdTNqJLRlhpVtg_7(float value)
	{
		___zYigHpHMdfbVRBdTNqJLRlhpVtg_7 = value;
	}

	inline static int32_t get_offset_of_TqfnCtqiMsHsZAmsVfRQfULPGnGz_8() { return static_cast<int32_t>(offsetof(ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55, ___TqfnCtqiMsHsZAmsVfRQfULPGnGz_8)); }
	inline float get_TqfnCtqiMsHsZAmsVfRQfULPGnGz_8() const { return ___TqfnCtqiMsHsZAmsVfRQfULPGnGz_8; }
	inline float* get_address_of_TqfnCtqiMsHsZAmsVfRQfULPGnGz_8() { return &___TqfnCtqiMsHsZAmsVfRQfULPGnGz_8; }
	inline void set_TqfnCtqiMsHsZAmsVfRQfULPGnGz_8(float value)
	{
		___TqfnCtqiMsHsZAmsVfRQfULPGnGz_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFJVNENQCARQYBQDTGKEKRZMBLE_TF0DD22883960F89C9077D66214D33DCFD74BEE55_H
#ifndef PNGXOKFNJPJLFYOVPHTRMLSPWHZ_T6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43_H
#define PNGXOKFNJPJLFYOVPHTRMLSPWHZ_T6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_pNgxOKFNJPJlFYOVphTRMlSPwhz
struct  pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_pNgxOKFNJPJlFYOVphTRMlSPwhz::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_pNgxOKFNJPJlFYOVphTRMlSPwhz::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_pNgxOKFNJPJlFYOVphTRMlSPwhz::NkqzOUnpKxOLEIHqzuEvIwFSEWZ
	uint32_t ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_pNgxOKFNJPJlFYOVphTRMlSPwhz::ZXOWQoAgWuCntwkclqxZSCDQZSd
	uint32_t ___ZXOWQoAgWuCntwkclqxZSCDQZSd_3;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_pNgxOKFNJPJlFYOVphTRMlSPwhz::csbagSaGlXaYrxByCxECLgSeBxGq
	uint8_t ___csbagSaGlXaYrxByCxECLgSeBxGq_4;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_pNgxOKFNJPJlFYOVphTRMlSPwhz::IyCUGfzcPoSzeXqQnApzwoMSQTs
	uint8_t ___IyCUGfzcPoSzeXqQnApzwoMSQTs_5;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_pNgxOKFNJPJlFYOVphTRMlSPwhz::ZaXEmLWJbJqNTzeAQNxcrKINBnt
	uint8_t ___ZaXEmLWJbJqNTzeAQNxcrKINBnt_6;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_pNgxOKFNJPJlFYOVphTRMlSPwhz::BfGikzYXODBhJWbccUmzPNgAJjN
	uint8_t ___BfGikzYXODBhJWbccUmzPNgAJjN_7;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_pNgxOKFNJPJlFYOVphTRMlSPwhz::IPHGhnvmvweeXFsNSmpniuYFNHcM
	int32_t ___IPHGhnvmvweeXFsNSmpniuYFNHcM_8;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_pNgxOKFNJPJlFYOVphTRMlSPwhz::ycICvmDyXClaVDHBtBHHgVBlmCs
	int32_t ___ycICvmDyXClaVDHBtBHHgVBlmCs_9;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return static_cast<int32_t>(offsetof(pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43, ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2)); }
	inline uint32_t get_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() const { return ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline uint32_t* get_address_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return &___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline void set_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2(uint32_t value)
	{
		___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2 = value;
	}

	inline static int32_t get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_3() { return static_cast<int32_t>(offsetof(pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43, ___ZXOWQoAgWuCntwkclqxZSCDQZSd_3)); }
	inline uint32_t get_ZXOWQoAgWuCntwkclqxZSCDQZSd_3() const { return ___ZXOWQoAgWuCntwkclqxZSCDQZSd_3; }
	inline uint32_t* get_address_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_3() { return &___ZXOWQoAgWuCntwkclqxZSCDQZSd_3; }
	inline void set_ZXOWQoAgWuCntwkclqxZSCDQZSd_3(uint32_t value)
	{
		___ZXOWQoAgWuCntwkclqxZSCDQZSd_3 = value;
	}

	inline static int32_t get_offset_of_csbagSaGlXaYrxByCxECLgSeBxGq_4() { return static_cast<int32_t>(offsetof(pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43, ___csbagSaGlXaYrxByCxECLgSeBxGq_4)); }
	inline uint8_t get_csbagSaGlXaYrxByCxECLgSeBxGq_4() const { return ___csbagSaGlXaYrxByCxECLgSeBxGq_4; }
	inline uint8_t* get_address_of_csbagSaGlXaYrxByCxECLgSeBxGq_4() { return &___csbagSaGlXaYrxByCxECLgSeBxGq_4; }
	inline void set_csbagSaGlXaYrxByCxECLgSeBxGq_4(uint8_t value)
	{
		___csbagSaGlXaYrxByCxECLgSeBxGq_4 = value;
	}

	inline static int32_t get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_5() { return static_cast<int32_t>(offsetof(pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43, ___IyCUGfzcPoSzeXqQnApzwoMSQTs_5)); }
	inline uint8_t get_IyCUGfzcPoSzeXqQnApzwoMSQTs_5() const { return ___IyCUGfzcPoSzeXqQnApzwoMSQTs_5; }
	inline uint8_t* get_address_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_5() { return &___IyCUGfzcPoSzeXqQnApzwoMSQTs_5; }
	inline void set_IyCUGfzcPoSzeXqQnApzwoMSQTs_5(uint8_t value)
	{
		___IyCUGfzcPoSzeXqQnApzwoMSQTs_5 = value;
	}

	inline static int32_t get_offset_of_ZaXEmLWJbJqNTzeAQNxcrKINBnt_6() { return static_cast<int32_t>(offsetof(pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43, ___ZaXEmLWJbJqNTzeAQNxcrKINBnt_6)); }
	inline uint8_t get_ZaXEmLWJbJqNTzeAQNxcrKINBnt_6() const { return ___ZaXEmLWJbJqNTzeAQNxcrKINBnt_6; }
	inline uint8_t* get_address_of_ZaXEmLWJbJqNTzeAQNxcrKINBnt_6() { return &___ZaXEmLWJbJqNTzeAQNxcrKINBnt_6; }
	inline void set_ZaXEmLWJbJqNTzeAQNxcrKINBnt_6(uint8_t value)
	{
		___ZaXEmLWJbJqNTzeAQNxcrKINBnt_6 = value;
	}

	inline static int32_t get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_7() { return static_cast<int32_t>(offsetof(pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43, ___BfGikzYXODBhJWbccUmzPNgAJjN_7)); }
	inline uint8_t get_BfGikzYXODBhJWbccUmzPNgAJjN_7() const { return ___BfGikzYXODBhJWbccUmzPNgAJjN_7; }
	inline uint8_t* get_address_of_BfGikzYXODBhJWbccUmzPNgAJjN_7() { return &___BfGikzYXODBhJWbccUmzPNgAJjN_7; }
	inline void set_BfGikzYXODBhJWbccUmzPNgAJjN_7(uint8_t value)
	{
		___BfGikzYXODBhJWbccUmzPNgAJjN_7 = value;
	}

	inline static int32_t get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_8() { return static_cast<int32_t>(offsetof(pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43, ___IPHGhnvmvweeXFsNSmpniuYFNHcM_8)); }
	inline int32_t get_IPHGhnvmvweeXFsNSmpniuYFNHcM_8() const { return ___IPHGhnvmvweeXFsNSmpniuYFNHcM_8; }
	inline int32_t* get_address_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_8() { return &___IPHGhnvmvweeXFsNSmpniuYFNHcM_8; }
	inline void set_IPHGhnvmvweeXFsNSmpniuYFNHcM_8(int32_t value)
	{
		___IPHGhnvmvweeXFsNSmpniuYFNHcM_8 = value;
	}

	inline static int32_t get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_9() { return static_cast<int32_t>(offsetof(pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43, ___ycICvmDyXClaVDHBtBHHgVBlmCs_9)); }
	inline int32_t get_ycICvmDyXClaVDHBtBHHgVBlmCs_9() const { return ___ycICvmDyXClaVDHBtBHHgVBlmCs_9; }
	inline int32_t* get_address_of_ycICvmDyXClaVDHBtBHHgVBlmCs_9() { return &___ycICvmDyXClaVDHBtBHHgVBlmCs_9; }
	inline void set_ycICvmDyXClaVDHBtBHHgVBlmCs_9(int32_t value)
	{
		___ycICvmDyXClaVDHBtBHHgVBlmCs_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PNGXOKFNJPJLFYOVPHTRMLSPWHZ_T6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43_H
#ifndef QJTGMNJCFKAUPDTCGWIMOUSASUP_TEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3_H
#define QJTGMNJCFKAUPDTCGWIMOUSASUP_TEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_qjTGMNjcFKAUPDTCGWIMouSasUP
struct  qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_qjTGMNjcFKAUPDTCGWIMouSasUP::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_qjTGMNjcFKAUPDTCGWIMouSasUP::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.Int64 gvrkMEBkQRXiZPPNldGUfPXGyiXV_qjTGMNjcFKAUPDTCGWIMouSasUP::pdPsFQqEdLGUVvbWeUdQOqKnLLk
	int64_t ___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2;
	// System.Int64 gvrkMEBkQRXiZPPNldGUfPXGyiXV_qjTGMNjcFKAUPDTCGWIMouSasUP::ktvAhGgThCYiOvCVXNNYQGLQLfdY
	int64_t ___ktvAhGgThCYiOvCVXNNYQGLQLfdY_3;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_qjTGMNjcFKAUPDTCGWIMouSasUP::KAuIbltThGGoDXvARunxJofUqEd
	uint32_t ___KAuIbltThGGoDXvARunxJofUqEd_4;
	// System.Single gvrkMEBkQRXiZPPNldGUfPXGyiXV_qjTGMNjcFKAUPDTCGWIMouSasUP::dPOHGhbKcJeNtFkPLrsbEVlByKNa
	float ___dPOHGhbKcJeNtFkPLrsbEVlByKNa_5;
	// System.Single gvrkMEBkQRXiZPPNldGUfPXGyiXV_qjTGMNjcFKAUPDTCGWIMouSasUP::IPHGhnvmvweeXFsNSmpniuYFNHcM
	float ___IPHGhnvmvweeXFsNSmpniuYFNHcM_6;
	// System.Single gvrkMEBkQRXiZPPNldGUfPXGyiXV_qjTGMNjcFKAUPDTCGWIMouSasUP::ycICvmDyXClaVDHBtBHHgVBlmCs
	float ___ycICvmDyXClaVDHBtBHHgVBlmCs_7;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2() { return static_cast<int32_t>(offsetof(qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3, ___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2)); }
	inline int64_t get_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2() const { return ___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2; }
	inline int64_t* get_address_of_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2() { return &___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2; }
	inline void set_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2(int64_t value)
	{
		___pdPsFQqEdLGUVvbWeUdQOqKnLLk_2 = value;
	}

	inline static int32_t get_offset_of_ktvAhGgThCYiOvCVXNNYQGLQLfdY_3() { return static_cast<int32_t>(offsetof(qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3, ___ktvAhGgThCYiOvCVXNNYQGLQLfdY_3)); }
	inline int64_t get_ktvAhGgThCYiOvCVXNNYQGLQLfdY_3() const { return ___ktvAhGgThCYiOvCVXNNYQGLQLfdY_3; }
	inline int64_t* get_address_of_ktvAhGgThCYiOvCVXNNYQGLQLfdY_3() { return &___ktvAhGgThCYiOvCVXNNYQGLQLfdY_3; }
	inline void set_ktvAhGgThCYiOvCVXNNYQGLQLfdY_3(int64_t value)
	{
		___ktvAhGgThCYiOvCVXNNYQGLQLfdY_3 = value;
	}

	inline static int32_t get_offset_of_KAuIbltThGGoDXvARunxJofUqEd_4() { return static_cast<int32_t>(offsetof(qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3, ___KAuIbltThGGoDXvARunxJofUqEd_4)); }
	inline uint32_t get_KAuIbltThGGoDXvARunxJofUqEd_4() const { return ___KAuIbltThGGoDXvARunxJofUqEd_4; }
	inline uint32_t* get_address_of_KAuIbltThGGoDXvARunxJofUqEd_4() { return &___KAuIbltThGGoDXvARunxJofUqEd_4; }
	inline void set_KAuIbltThGGoDXvARunxJofUqEd_4(uint32_t value)
	{
		___KAuIbltThGGoDXvARunxJofUqEd_4 = value;
	}

	inline static int32_t get_offset_of_dPOHGhbKcJeNtFkPLrsbEVlByKNa_5() { return static_cast<int32_t>(offsetof(qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3, ___dPOHGhbKcJeNtFkPLrsbEVlByKNa_5)); }
	inline float get_dPOHGhbKcJeNtFkPLrsbEVlByKNa_5() const { return ___dPOHGhbKcJeNtFkPLrsbEVlByKNa_5; }
	inline float* get_address_of_dPOHGhbKcJeNtFkPLrsbEVlByKNa_5() { return &___dPOHGhbKcJeNtFkPLrsbEVlByKNa_5; }
	inline void set_dPOHGhbKcJeNtFkPLrsbEVlByKNa_5(float value)
	{
		___dPOHGhbKcJeNtFkPLrsbEVlByKNa_5 = value;
	}

	inline static int32_t get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_6() { return static_cast<int32_t>(offsetof(qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3, ___IPHGhnvmvweeXFsNSmpniuYFNHcM_6)); }
	inline float get_IPHGhnvmvweeXFsNSmpniuYFNHcM_6() const { return ___IPHGhnvmvweeXFsNSmpniuYFNHcM_6; }
	inline float* get_address_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_6() { return &___IPHGhnvmvweeXFsNSmpniuYFNHcM_6; }
	inline void set_IPHGhnvmvweeXFsNSmpniuYFNHcM_6(float value)
	{
		___IPHGhnvmvweeXFsNSmpniuYFNHcM_6 = value;
	}

	inline static int32_t get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_7() { return static_cast<int32_t>(offsetof(qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3, ___ycICvmDyXClaVDHBtBHHgVBlmCs_7)); }
	inline float get_ycICvmDyXClaVDHBtBHHgVBlmCs_7() const { return ___ycICvmDyXClaVDHBtBHHgVBlmCs_7; }
	inline float* get_address_of_ycICvmDyXClaVDHBtBHHgVBlmCs_7() { return &___ycICvmDyXClaVDHBtBHHgVBlmCs_7; }
	inline void set_ycICvmDyXClaVDHBtBHHgVBlmCs_7(float value)
	{
		___ycICvmDyXClaVDHBtBHHgVBlmCs_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QJTGMNJCFKAUPDTCGWIMOUSASUP_TEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3_H
#ifndef QPCCNCBVDYTPDGTFWQEMFAEEKSDB_T983CEE3A5CE71A9C79D04A32E9C25732068BE4B8_H
#define QPCCNCBVDYTPDGTFWQEMFAEEKSDB_T983CEE3A5CE71A9C79D04A32E9C25732068BE4B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_qpcCNcBvDyTpDgTFwQEMfAeeKsdB
struct  qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_qpcCNcBvDyTpDgTFwQEMfAeeKsdB::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_qpcCNcBvDyTpDgTFwQEMfAeeKsdB::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_qpcCNcBvDyTpDgTFwQEMfAeeKsdB::NkqzOUnpKxOLEIHqzuEvIwFSEWZ
	uint32_t ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2;
	// System.Char[] gvrkMEBkQRXiZPPNldGUfPXGyiXV_qpcCNcBvDyTpDgTFwQEMfAeeKsdB::GbhogeXOuqAWXhmgfRAmudFOsEia
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___GbhogeXOuqAWXhmgfRAmudFOsEia_3;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_qpcCNcBvDyTpDgTFwQEMfAeeKsdB::UeDsJytcqoNApcyYIRGUvxpadRB
	int32_t ___UeDsJytcqoNApcyYIRGUvxpadRB_4;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_qpcCNcBvDyTpDgTFwQEMfAeeKsdB::TlEmNMFynCEISyuvbnTPLeTdELX
	int32_t ___TlEmNMFynCEISyuvbnTPLeTdELX_5;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return static_cast<int32_t>(offsetof(qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8, ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2)); }
	inline uint32_t get_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() const { return ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline uint32_t* get_address_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return &___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline void set_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2(uint32_t value)
	{
		___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2 = value;
	}

	inline static int32_t get_offset_of_GbhogeXOuqAWXhmgfRAmudFOsEia_3() { return static_cast<int32_t>(offsetof(qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8, ___GbhogeXOuqAWXhmgfRAmudFOsEia_3)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_GbhogeXOuqAWXhmgfRAmudFOsEia_3() const { return ___GbhogeXOuqAWXhmgfRAmudFOsEia_3; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_GbhogeXOuqAWXhmgfRAmudFOsEia_3() { return &___GbhogeXOuqAWXhmgfRAmudFOsEia_3; }
	inline void set_GbhogeXOuqAWXhmgfRAmudFOsEia_3(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___GbhogeXOuqAWXhmgfRAmudFOsEia_3 = value;
		Il2CppCodeGenWriteBarrier((&___GbhogeXOuqAWXhmgfRAmudFOsEia_3), value);
	}

	inline static int32_t get_offset_of_UeDsJytcqoNApcyYIRGUvxpadRB_4() { return static_cast<int32_t>(offsetof(qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8, ___UeDsJytcqoNApcyYIRGUvxpadRB_4)); }
	inline int32_t get_UeDsJytcqoNApcyYIRGUvxpadRB_4() const { return ___UeDsJytcqoNApcyYIRGUvxpadRB_4; }
	inline int32_t* get_address_of_UeDsJytcqoNApcyYIRGUvxpadRB_4() { return &___UeDsJytcqoNApcyYIRGUvxpadRB_4; }
	inline void set_UeDsJytcqoNApcyYIRGUvxpadRB_4(int32_t value)
	{
		___UeDsJytcqoNApcyYIRGUvxpadRB_4 = value;
	}

	inline static int32_t get_offset_of_TlEmNMFynCEISyuvbnTPLeTdELX_5() { return static_cast<int32_t>(offsetof(qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8, ___TlEmNMFynCEISyuvbnTPLeTdELX_5)); }
	inline int32_t get_TlEmNMFynCEISyuvbnTPLeTdELX_5() const { return ___TlEmNMFynCEISyuvbnTPLeTdELX_5; }
	inline int32_t* get_address_of_TlEmNMFynCEISyuvbnTPLeTdELX_5() { return &___TlEmNMFynCEISyuvbnTPLeTdELX_5; }
	inline void set_TlEmNMFynCEISyuvbnTPLeTdELX_5(int32_t value)
	{
		___TlEmNMFynCEISyuvbnTPLeTdELX_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of gvrkMEBkQRXiZPPNldGUfPXGyiXV/qpcCNcBvDyTpDgTFwQEMfAeeKsdB
struct qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8_marshaled_pinvoke
{
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	uint32_t ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2;
	uint8_t ___GbhogeXOuqAWXhmgfRAmudFOsEia_3[32];
	int32_t ___UeDsJytcqoNApcyYIRGUvxpadRB_4;
	int32_t ___TlEmNMFynCEISyuvbnTPLeTdELX_5;
};
// Native definition for COM marshalling of gvrkMEBkQRXiZPPNldGUfPXGyiXV/qpcCNcBvDyTpDgTFwQEMfAeeKsdB
struct qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8_marshaled_com
{
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	uint32_t ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2;
	uint8_t ___GbhogeXOuqAWXhmgfRAmudFOsEia_3[32];
	int32_t ___UeDsJytcqoNApcyYIRGUvxpadRB_4;
	int32_t ___TlEmNMFynCEISyuvbnTPLeTdELX_5;
};
#endif // QPCCNCBVDYTPDGTFWQEMFAEEKSDB_T983CEE3A5CE71A9C79D04A32E9C25732068BE4B8_H
#ifndef RSLCIHBFLHYIVCEVOVZGMAZUGCEW_T28FEF23AC71F37882271DAE1876C5FEB21E9633D_H
#define RSLCIHBFLHYIVCEVOVZGMAZUGCEW_T28FEF23AC71F37882271DAE1876C5FEB21E9633D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_rSlcihBflhYIvceVOVZgmAZugcEW
struct  rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_rSlcihBflhYIvceVOVZgmAZugcEW::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_rSlcihBflhYIvceVOVZgmAZugcEW::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_rSlcihBflhYIvceVOVZgmAZugcEW::ZXOWQoAgWuCntwkclqxZSCDQZSd
	int32_t ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return static_cast<int32_t>(offsetof(rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D, ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2)); }
	inline int32_t get_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() const { return ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline int32_t* get_address_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return &___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline void set_ZXOWQoAgWuCntwkclqxZSCDQZSd_2(int32_t value)
	{
		___ZXOWQoAgWuCntwkclqxZSCDQZSd_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSLCIHBFLHYIVCEVOVZGMAZUGCEW_T28FEF23AC71F37882271DAE1876C5FEB21E9633D_H
#ifndef VNUZDXGBBKHEGOMPOYLCSJNIALT_T7FFB4FAF7D913429B8DE36EC4479B852D5DFE457_H
#define VNUZDXGBBKHEGOMPOYLCSJNIALT_T7FFB4FAF7D913429B8DE36EC4479B852D5DFE457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_vnUZdxGbbKheGOMPoylCsjnialt
struct  vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_vnUZdxGbbKheGOMPoylCsjnialt::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_vnUZdxGbbKheGOMPoylCsjnialt::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_vnUZdxGbbKheGOMPoylCsjnialt::ZXOWQoAgWuCntwkclqxZSCDQZSd
	int32_t ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_vnUZdxGbbKheGOMPoylCsjnialt::tnahePnlIzETNObNeoddamMSBEbG
	uint8_t ___tnahePnlIzETNObNeoddamMSBEbG_3;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_vnUZdxGbbKheGOMPoylCsjnialt::ujWJxEJcSVHSdnQYrtHnXDWQpnZ
	uint8_t ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_4;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_vnUZdxGbbKheGOMPoylCsjnialt::BfGikzYXODBhJWbccUmzPNgAJjN
	uint8_t ___BfGikzYXODBhJWbccUmzPNgAJjN_5;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_vnUZdxGbbKheGOMPoylCsjnialt::ZdxNPrFvibjxZCDAFwWLoADXmOO
	uint8_t ___ZdxNPrFvibjxZCDAFwWLoADXmOO_6;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return static_cast<int32_t>(offsetof(vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457, ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2)); }
	inline int32_t get_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() const { return ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline int32_t* get_address_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return &___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline void set_ZXOWQoAgWuCntwkclqxZSCDQZSd_2(int32_t value)
	{
		___ZXOWQoAgWuCntwkclqxZSCDQZSd_2 = value;
	}

	inline static int32_t get_offset_of_tnahePnlIzETNObNeoddamMSBEbG_3() { return static_cast<int32_t>(offsetof(vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457, ___tnahePnlIzETNObNeoddamMSBEbG_3)); }
	inline uint8_t get_tnahePnlIzETNObNeoddamMSBEbG_3() const { return ___tnahePnlIzETNObNeoddamMSBEbG_3; }
	inline uint8_t* get_address_of_tnahePnlIzETNObNeoddamMSBEbG_3() { return &___tnahePnlIzETNObNeoddamMSBEbG_3; }
	inline void set_tnahePnlIzETNObNeoddamMSBEbG_3(uint8_t value)
	{
		___tnahePnlIzETNObNeoddamMSBEbG_3 = value;
	}

	inline static int32_t get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_4() { return static_cast<int32_t>(offsetof(vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457, ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_4)); }
	inline uint8_t get_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_4() const { return ___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_4; }
	inline uint8_t* get_address_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_4() { return &___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_4; }
	inline void set_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_4(uint8_t value)
	{
		___ujWJxEJcSVHSdnQYrtHnXDWQpnZ_4 = value;
	}

	inline static int32_t get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_5() { return static_cast<int32_t>(offsetof(vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457, ___BfGikzYXODBhJWbccUmzPNgAJjN_5)); }
	inline uint8_t get_BfGikzYXODBhJWbccUmzPNgAJjN_5() const { return ___BfGikzYXODBhJWbccUmzPNgAJjN_5; }
	inline uint8_t* get_address_of_BfGikzYXODBhJWbccUmzPNgAJjN_5() { return &___BfGikzYXODBhJWbccUmzPNgAJjN_5; }
	inline void set_BfGikzYXODBhJWbccUmzPNgAJjN_5(uint8_t value)
	{
		___BfGikzYXODBhJWbccUmzPNgAJjN_5 = value;
	}

	inline static int32_t get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_6() { return static_cast<int32_t>(offsetof(vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457, ___ZdxNPrFvibjxZCDAFwWLoADXmOO_6)); }
	inline uint8_t get_ZdxNPrFvibjxZCDAFwWLoADXmOO_6() const { return ___ZdxNPrFvibjxZCDAFwWLoADXmOO_6; }
	inline uint8_t* get_address_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_6() { return &___ZdxNPrFvibjxZCDAFwWLoADXmOO_6; }
	inline void set_ZdxNPrFvibjxZCDAFwWLoADXmOO_6(uint8_t value)
	{
		___ZdxNPrFvibjxZCDAFwWLoADXmOO_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VNUZDXGBBKHEGOMPOYLCSJNIALT_T7FFB4FAF7D913429B8DE36EC4479B852D5DFE457_H
#ifndef XLNIQSPMGFOLDYZKWDWGBKIAPEW_T05F6835A321E8A5CF357A90CA40197A389D95CFD_H
#define XLNIQSPMGFOLDYZKWDWGBKIAPEW_T05F6835A321E8A5CF357A90CA40197A389D95CFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_xLNiqspMGfoLDyZKWDwgBkIaPew
struct  xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_xLNiqspMGfoLDyZKWDwgBkIaPew::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_xLNiqspMGfoLDyZKWDwgBkIaPew::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.Int32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_xLNiqspMGfoLDyZKWDwgBkIaPew::ZXOWQoAgWuCntwkclqxZSCDQZSd
	int32_t ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_xLNiqspMGfoLDyZKWDwgBkIaPew::csbagSaGlXaYrxByCxECLgSeBxGq
	uint8_t ___csbagSaGlXaYrxByCxECLgSeBxGq_3;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_xLNiqspMGfoLDyZKWDwgBkIaPew::IyCUGfzcPoSzeXqQnApzwoMSQTs
	uint8_t ___IyCUGfzcPoSzeXqQnApzwoMSQTs_4;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_xLNiqspMGfoLDyZKWDwgBkIaPew::BfGikzYXODBhJWbccUmzPNgAJjN
	uint8_t ___BfGikzYXODBhJWbccUmzPNgAJjN_5;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_xLNiqspMGfoLDyZKWDwgBkIaPew::ZdxNPrFvibjxZCDAFwWLoADXmOO
	uint8_t ___ZdxNPrFvibjxZCDAFwWLoADXmOO_6;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return static_cast<int32_t>(offsetof(xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD, ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2)); }
	inline int32_t get_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() const { return ___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline int32_t* get_address_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() { return &___ZXOWQoAgWuCntwkclqxZSCDQZSd_2; }
	inline void set_ZXOWQoAgWuCntwkclqxZSCDQZSd_2(int32_t value)
	{
		___ZXOWQoAgWuCntwkclqxZSCDQZSd_2 = value;
	}

	inline static int32_t get_offset_of_csbagSaGlXaYrxByCxECLgSeBxGq_3() { return static_cast<int32_t>(offsetof(xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD, ___csbagSaGlXaYrxByCxECLgSeBxGq_3)); }
	inline uint8_t get_csbagSaGlXaYrxByCxECLgSeBxGq_3() const { return ___csbagSaGlXaYrxByCxECLgSeBxGq_3; }
	inline uint8_t* get_address_of_csbagSaGlXaYrxByCxECLgSeBxGq_3() { return &___csbagSaGlXaYrxByCxECLgSeBxGq_3; }
	inline void set_csbagSaGlXaYrxByCxECLgSeBxGq_3(uint8_t value)
	{
		___csbagSaGlXaYrxByCxECLgSeBxGq_3 = value;
	}

	inline static int32_t get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_4() { return static_cast<int32_t>(offsetof(xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD, ___IyCUGfzcPoSzeXqQnApzwoMSQTs_4)); }
	inline uint8_t get_IyCUGfzcPoSzeXqQnApzwoMSQTs_4() const { return ___IyCUGfzcPoSzeXqQnApzwoMSQTs_4; }
	inline uint8_t* get_address_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_4() { return &___IyCUGfzcPoSzeXqQnApzwoMSQTs_4; }
	inline void set_IyCUGfzcPoSzeXqQnApzwoMSQTs_4(uint8_t value)
	{
		___IyCUGfzcPoSzeXqQnApzwoMSQTs_4 = value;
	}

	inline static int32_t get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_5() { return static_cast<int32_t>(offsetof(xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD, ___BfGikzYXODBhJWbccUmzPNgAJjN_5)); }
	inline uint8_t get_BfGikzYXODBhJWbccUmzPNgAJjN_5() const { return ___BfGikzYXODBhJWbccUmzPNgAJjN_5; }
	inline uint8_t* get_address_of_BfGikzYXODBhJWbccUmzPNgAJjN_5() { return &___BfGikzYXODBhJWbccUmzPNgAJjN_5; }
	inline void set_BfGikzYXODBhJWbccUmzPNgAJjN_5(uint8_t value)
	{
		___BfGikzYXODBhJWbccUmzPNgAJjN_5 = value;
	}

	inline static int32_t get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_6() { return static_cast<int32_t>(offsetof(xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD, ___ZdxNPrFvibjxZCDAFwWLoADXmOO_6)); }
	inline uint8_t get_ZdxNPrFvibjxZCDAFwWLoADXmOO_6() const { return ___ZdxNPrFvibjxZCDAFwWLoADXmOO_6; }
	inline uint8_t* get_address_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_6() { return &___ZdxNPrFvibjxZCDAFwWLoADXmOO_6; }
	inline void set_ZdxNPrFvibjxZCDAFwWLoADXmOO_6(uint8_t value)
	{
		___ZdxNPrFvibjxZCDAFwWLoADXmOO_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XLNIQSPMGFOLDYZKWDWGBKIAPEW_T05F6835A321E8A5CF357A90CA40197A389D95CFD_H
#ifndef LNGIGGEOZTFHOKVPOBLVJBATVLYD_T36643A533B08EAB0B35FCE169CD422956CFC6489_H
#define LNGIGGEOZTFHOKVPOBLVJBATVLYD_T36643A533B08EAB0B35FCE169CD422956CFC6489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd
struct  lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489  : public RuntimeObject
{
public:
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::avoCosfGauXluftrTLZWMUMjMSvo
	int32_t ___avoCosfGauXluftrTLZWMUMjMSvo_0;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::vHhTUeEjpjXweHfcvokTPubqCBh
	int32_t ___vHhTUeEjpjXweHfcvokTPubqCBh_1;
	// System.Guid qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::mMvnhxixERRYbVLPXEwyRrtXYwt
	Guid_t  ___mMvnhxixERRYbVLPXEwyRrtXYwt_2;
	// System.String qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::espHBjclwGrxdYkIErbggrUHnMo
	String_t* ___espHBjclwGrxdYkIErbggrUHnMo_3;
	// MUJAEshscmeXKPKBeiQXLdyHSgZ qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::uyozhMHHhGNcfEwvclTjPxbbvxZ
	RuntimeObject* ___uyozhMHHhGNcfEwvclTjPxbbvxZ_4;
	// qgjNESsSnzSlMpHwBtxQPCMNOwq qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::hBqzkAFqJcxfwCrSYqTdWPhwRxi
	int32_t ___hBqzkAFqJcxfwCrSYqTdWPhwRxi_5;
	// System.String qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::amhGqEYcVGlMzaBhudMaalhDCbSB
	String_t* ___amhGqEYcVGlMzaBhudMaalhDCbSB_6;
	// System.String qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::zTBxQtRhsiyWjlXakgtukSbwLlp
	String_t* ___zTBxQtRhsiyWjlXakgtukSbwLlp_7;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::KclFaRHPVyrRHSfCxDIVgvgADJgN
	int32_t ___KclFaRHPVyrRHSfCxDIVgvgADJgN_8;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::qNNdzuExLuNfrdyOzaNcsNVrpIQc
	int32_t ___qNNdzuExLuNfrdyOzaNcsNVrpIQc_9;
	// System.Guid qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::RhEAYdViDLywbAAxYxVbxUVxiMM
	Guid_t  ___RhEAYdViDLywbAAxYxVbxUVxiMM_10;
	// Rewired.PidVid qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::yauaFjfmZQzNovSFRGthYyFJrrj
	PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D  ___yauaFjfmZQzNovSFRGthYyFJrrj_11;
	// System.Guid qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::bLJDeZdmqhplurpuKUnpKCpNysd
	Guid_t  ___bLJDeZdmqhplurpuKUnpKCpNysd_12;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::NQcfCUdrzCInuZTLuEEoKhWjImdc
	int32_t ___NQcfCUdrzCInuZTLuEEoKhWjImdc_13;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::BIMocAZqmOYbcFTwmpmMNqQvglvi
	int32_t ___BIMocAZqmOYbcFTwmpmMNqQvglvi_14;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::AfHFnomuXngvpySlBnOGFBvmcpy
	int32_t ___AfHFnomuXngvpySlBnOGFBvmcpy_15;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::UdWYvkBYkFbtQVrPZwZuKINGjC
	int32_t ___UdWYvkBYkFbtQVrPZwZuKINGjC_16;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::VYSASJjqEIsmJcmnIHADUYEaRjQC
	int32_t ___VYSASJjqEIsmJcmnIHADUYEaRjQC_17;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::aWhbUpAtiftmjIGzRSoPwmEnzb
	int32_t ___aWhbUpAtiftmjIGzRSoPwmEnzb_18;
	// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::ibgNLLocbIyPDBErUPhHUTjDZKC
	bool ___ibgNLLocbIyPDBErUPhHUTjDZKC_19;
	// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::ZsEfMISqALipCaWIrZdQwCvMLkx
	bool ___ZsEfMISqALipCaWIrZdQwCvMLkx_20;
	// System.Int32 qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::YJKKCodZAAdHLAznZhhKAnRrzmS
	int32_t ___YJKKCodZAAdHLAznZhhKAnRrzmS_21;
	// System.Single[] qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::iKsRvufntavyFjWZbUibgspoftw
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___iKsRvufntavyFjWZbUibgspoftw_22;
	// System.Boolean[] qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::YuSoQESZTtPuhZKVJFBavCSGQsh
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___YuSoQESZTtPuhZKVJFBavCSGQsh_23;
	// Rewired.HardwareJoystickMap_InputManager qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::motlnrXwRclwbbdquGWkOEYsYFy
	HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 * ___motlnrXwRclwbbdquGWkOEYsYFy_24;
	// System.Func`2<Rewired.BridgedControllerHWInfo,Rewired.HardwareJoystickMap_InputManager> qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::QEAEcDsIVKSxlRksWqQtzkbPdqrh
	Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_25;
	// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::JKdifYgUghWnBkRBRChckuzXKWst
	bool ___JKdifYgUghWnBkRBRChckuzXKWst_26;
	// System.Boolean qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::uTrFqghzwmUUNLMxoQSRVgfDfsj
	bool ___uTrFqghzwmUUNLMxoQSRVgfDfsj_27;
	// Rewired.Controller_Extension qQSKQqvJRxFNPvSjTomQuZOntrH_lNgIgGeozTFHoKvpOBlVjbAtvLyd::pwFxxEkPXgSZokcVFZSbwgxzpUm
	Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * ___pwFxxEkPXgSZokcVFZSbwgxzpUm_28;

public:
	inline static int32_t get_offset_of_avoCosfGauXluftrTLZWMUMjMSvo_0() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___avoCosfGauXluftrTLZWMUMjMSvo_0)); }
	inline int32_t get_avoCosfGauXluftrTLZWMUMjMSvo_0() const { return ___avoCosfGauXluftrTLZWMUMjMSvo_0; }
	inline int32_t* get_address_of_avoCosfGauXluftrTLZWMUMjMSvo_0() { return &___avoCosfGauXluftrTLZWMUMjMSvo_0; }
	inline void set_avoCosfGauXluftrTLZWMUMjMSvo_0(int32_t value)
	{
		___avoCosfGauXluftrTLZWMUMjMSvo_0 = value;
	}

	inline static int32_t get_offset_of_vHhTUeEjpjXweHfcvokTPubqCBh_1() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___vHhTUeEjpjXweHfcvokTPubqCBh_1)); }
	inline int32_t get_vHhTUeEjpjXweHfcvokTPubqCBh_1() const { return ___vHhTUeEjpjXweHfcvokTPubqCBh_1; }
	inline int32_t* get_address_of_vHhTUeEjpjXweHfcvokTPubqCBh_1() { return &___vHhTUeEjpjXweHfcvokTPubqCBh_1; }
	inline void set_vHhTUeEjpjXweHfcvokTPubqCBh_1(int32_t value)
	{
		___vHhTUeEjpjXweHfcvokTPubqCBh_1 = value;
	}

	inline static int32_t get_offset_of_mMvnhxixERRYbVLPXEwyRrtXYwt_2() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___mMvnhxixERRYbVLPXEwyRrtXYwt_2)); }
	inline Guid_t  get_mMvnhxixERRYbVLPXEwyRrtXYwt_2() const { return ___mMvnhxixERRYbVLPXEwyRrtXYwt_2; }
	inline Guid_t * get_address_of_mMvnhxixERRYbVLPXEwyRrtXYwt_2() { return &___mMvnhxixERRYbVLPXEwyRrtXYwt_2; }
	inline void set_mMvnhxixERRYbVLPXEwyRrtXYwt_2(Guid_t  value)
	{
		___mMvnhxixERRYbVLPXEwyRrtXYwt_2 = value;
	}

	inline static int32_t get_offset_of_espHBjclwGrxdYkIErbggrUHnMo_3() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___espHBjclwGrxdYkIErbggrUHnMo_3)); }
	inline String_t* get_espHBjclwGrxdYkIErbggrUHnMo_3() const { return ___espHBjclwGrxdYkIErbggrUHnMo_3; }
	inline String_t** get_address_of_espHBjclwGrxdYkIErbggrUHnMo_3() { return &___espHBjclwGrxdYkIErbggrUHnMo_3; }
	inline void set_espHBjclwGrxdYkIErbggrUHnMo_3(String_t* value)
	{
		___espHBjclwGrxdYkIErbggrUHnMo_3 = value;
		Il2CppCodeGenWriteBarrier((&___espHBjclwGrxdYkIErbggrUHnMo_3), value);
	}

	inline static int32_t get_offset_of_uyozhMHHhGNcfEwvclTjPxbbvxZ_4() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___uyozhMHHhGNcfEwvclTjPxbbvxZ_4)); }
	inline RuntimeObject* get_uyozhMHHhGNcfEwvclTjPxbbvxZ_4() const { return ___uyozhMHHhGNcfEwvclTjPxbbvxZ_4; }
	inline RuntimeObject** get_address_of_uyozhMHHhGNcfEwvclTjPxbbvxZ_4() { return &___uyozhMHHhGNcfEwvclTjPxbbvxZ_4; }
	inline void set_uyozhMHHhGNcfEwvclTjPxbbvxZ_4(RuntimeObject* value)
	{
		___uyozhMHHhGNcfEwvclTjPxbbvxZ_4 = value;
		Il2CppCodeGenWriteBarrier((&___uyozhMHHhGNcfEwvclTjPxbbvxZ_4), value);
	}

	inline static int32_t get_offset_of_hBqzkAFqJcxfwCrSYqTdWPhwRxi_5() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___hBqzkAFqJcxfwCrSYqTdWPhwRxi_5)); }
	inline int32_t get_hBqzkAFqJcxfwCrSYqTdWPhwRxi_5() const { return ___hBqzkAFqJcxfwCrSYqTdWPhwRxi_5; }
	inline int32_t* get_address_of_hBqzkAFqJcxfwCrSYqTdWPhwRxi_5() { return &___hBqzkAFqJcxfwCrSYqTdWPhwRxi_5; }
	inline void set_hBqzkAFqJcxfwCrSYqTdWPhwRxi_5(int32_t value)
	{
		___hBqzkAFqJcxfwCrSYqTdWPhwRxi_5 = value;
	}

	inline static int32_t get_offset_of_amhGqEYcVGlMzaBhudMaalhDCbSB_6() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___amhGqEYcVGlMzaBhudMaalhDCbSB_6)); }
	inline String_t* get_amhGqEYcVGlMzaBhudMaalhDCbSB_6() const { return ___amhGqEYcVGlMzaBhudMaalhDCbSB_6; }
	inline String_t** get_address_of_amhGqEYcVGlMzaBhudMaalhDCbSB_6() { return &___amhGqEYcVGlMzaBhudMaalhDCbSB_6; }
	inline void set_amhGqEYcVGlMzaBhudMaalhDCbSB_6(String_t* value)
	{
		___amhGqEYcVGlMzaBhudMaalhDCbSB_6 = value;
		Il2CppCodeGenWriteBarrier((&___amhGqEYcVGlMzaBhudMaalhDCbSB_6), value);
	}

	inline static int32_t get_offset_of_zTBxQtRhsiyWjlXakgtukSbwLlp_7() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___zTBxQtRhsiyWjlXakgtukSbwLlp_7)); }
	inline String_t* get_zTBxQtRhsiyWjlXakgtukSbwLlp_7() const { return ___zTBxQtRhsiyWjlXakgtukSbwLlp_7; }
	inline String_t** get_address_of_zTBxQtRhsiyWjlXakgtukSbwLlp_7() { return &___zTBxQtRhsiyWjlXakgtukSbwLlp_7; }
	inline void set_zTBxQtRhsiyWjlXakgtukSbwLlp_7(String_t* value)
	{
		___zTBxQtRhsiyWjlXakgtukSbwLlp_7 = value;
		Il2CppCodeGenWriteBarrier((&___zTBxQtRhsiyWjlXakgtukSbwLlp_7), value);
	}

	inline static int32_t get_offset_of_KclFaRHPVyrRHSfCxDIVgvgADJgN_8() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___KclFaRHPVyrRHSfCxDIVgvgADJgN_8)); }
	inline int32_t get_KclFaRHPVyrRHSfCxDIVgvgADJgN_8() const { return ___KclFaRHPVyrRHSfCxDIVgvgADJgN_8; }
	inline int32_t* get_address_of_KclFaRHPVyrRHSfCxDIVgvgADJgN_8() { return &___KclFaRHPVyrRHSfCxDIVgvgADJgN_8; }
	inline void set_KclFaRHPVyrRHSfCxDIVgvgADJgN_8(int32_t value)
	{
		___KclFaRHPVyrRHSfCxDIVgvgADJgN_8 = value;
	}

	inline static int32_t get_offset_of_qNNdzuExLuNfrdyOzaNcsNVrpIQc_9() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___qNNdzuExLuNfrdyOzaNcsNVrpIQc_9)); }
	inline int32_t get_qNNdzuExLuNfrdyOzaNcsNVrpIQc_9() const { return ___qNNdzuExLuNfrdyOzaNcsNVrpIQc_9; }
	inline int32_t* get_address_of_qNNdzuExLuNfrdyOzaNcsNVrpIQc_9() { return &___qNNdzuExLuNfrdyOzaNcsNVrpIQc_9; }
	inline void set_qNNdzuExLuNfrdyOzaNcsNVrpIQc_9(int32_t value)
	{
		___qNNdzuExLuNfrdyOzaNcsNVrpIQc_9 = value;
	}

	inline static int32_t get_offset_of_RhEAYdViDLywbAAxYxVbxUVxiMM_10() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___RhEAYdViDLywbAAxYxVbxUVxiMM_10)); }
	inline Guid_t  get_RhEAYdViDLywbAAxYxVbxUVxiMM_10() const { return ___RhEAYdViDLywbAAxYxVbxUVxiMM_10; }
	inline Guid_t * get_address_of_RhEAYdViDLywbAAxYxVbxUVxiMM_10() { return &___RhEAYdViDLywbAAxYxVbxUVxiMM_10; }
	inline void set_RhEAYdViDLywbAAxYxVbxUVxiMM_10(Guid_t  value)
	{
		___RhEAYdViDLywbAAxYxVbxUVxiMM_10 = value;
	}

	inline static int32_t get_offset_of_yauaFjfmZQzNovSFRGthYyFJrrj_11() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___yauaFjfmZQzNovSFRGthYyFJrrj_11)); }
	inline PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D  get_yauaFjfmZQzNovSFRGthYyFJrrj_11() const { return ___yauaFjfmZQzNovSFRGthYyFJrrj_11; }
	inline PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D * get_address_of_yauaFjfmZQzNovSFRGthYyFJrrj_11() { return &___yauaFjfmZQzNovSFRGthYyFJrrj_11; }
	inline void set_yauaFjfmZQzNovSFRGthYyFJrrj_11(PidVid_tE8C67908FCF264C7D49D40BC7BF32EDC9DADD93D  value)
	{
		___yauaFjfmZQzNovSFRGthYyFJrrj_11 = value;
	}

	inline static int32_t get_offset_of_bLJDeZdmqhplurpuKUnpKCpNysd_12() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___bLJDeZdmqhplurpuKUnpKCpNysd_12)); }
	inline Guid_t  get_bLJDeZdmqhplurpuKUnpKCpNysd_12() const { return ___bLJDeZdmqhplurpuKUnpKCpNysd_12; }
	inline Guid_t * get_address_of_bLJDeZdmqhplurpuKUnpKCpNysd_12() { return &___bLJDeZdmqhplurpuKUnpKCpNysd_12; }
	inline void set_bLJDeZdmqhplurpuKUnpKCpNysd_12(Guid_t  value)
	{
		___bLJDeZdmqhplurpuKUnpKCpNysd_12 = value;
	}

	inline static int32_t get_offset_of_NQcfCUdrzCInuZTLuEEoKhWjImdc_13() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___NQcfCUdrzCInuZTLuEEoKhWjImdc_13)); }
	inline int32_t get_NQcfCUdrzCInuZTLuEEoKhWjImdc_13() const { return ___NQcfCUdrzCInuZTLuEEoKhWjImdc_13; }
	inline int32_t* get_address_of_NQcfCUdrzCInuZTLuEEoKhWjImdc_13() { return &___NQcfCUdrzCInuZTLuEEoKhWjImdc_13; }
	inline void set_NQcfCUdrzCInuZTLuEEoKhWjImdc_13(int32_t value)
	{
		___NQcfCUdrzCInuZTLuEEoKhWjImdc_13 = value;
	}

	inline static int32_t get_offset_of_BIMocAZqmOYbcFTwmpmMNqQvglvi_14() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___BIMocAZqmOYbcFTwmpmMNqQvglvi_14)); }
	inline int32_t get_BIMocAZqmOYbcFTwmpmMNqQvglvi_14() const { return ___BIMocAZqmOYbcFTwmpmMNqQvglvi_14; }
	inline int32_t* get_address_of_BIMocAZqmOYbcFTwmpmMNqQvglvi_14() { return &___BIMocAZqmOYbcFTwmpmMNqQvglvi_14; }
	inline void set_BIMocAZqmOYbcFTwmpmMNqQvglvi_14(int32_t value)
	{
		___BIMocAZqmOYbcFTwmpmMNqQvglvi_14 = value;
	}

	inline static int32_t get_offset_of_AfHFnomuXngvpySlBnOGFBvmcpy_15() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___AfHFnomuXngvpySlBnOGFBvmcpy_15)); }
	inline int32_t get_AfHFnomuXngvpySlBnOGFBvmcpy_15() const { return ___AfHFnomuXngvpySlBnOGFBvmcpy_15; }
	inline int32_t* get_address_of_AfHFnomuXngvpySlBnOGFBvmcpy_15() { return &___AfHFnomuXngvpySlBnOGFBvmcpy_15; }
	inline void set_AfHFnomuXngvpySlBnOGFBvmcpy_15(int32_t value)
	{
		___AfHFnomuXngvpySlBnOGFBvmcpy_15 = value;
	}

	inline static int32_t get_offset_of_UdWYvkBYkFbtQVrPZwZuKINGjC_16() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___UdWYvkBYkFbtQVrPZwZuKINGjC_16)); }
	inline int32_t get_UdWYvkBYkFbtQVrPZwZuKINGjC_16() const { return ___UdWYvkBYkFbtQVrPZwZuKINGjC_16; }
	inline int32_t* get_address_of_UdWYvkBYkFbtQVrPZwZuKINGjC_16() { return &___UdWYvkBYkFbtQVrPZwZuKINGjC_16; }
	inline void set_UdWYvkBYkFbtQVrPZwZuKINGjC_16(int32_t value)
	{
		___UdWYvkBYkFbtQVrPZwZuKINGjC_16 = value;
	}

	inline static int32_t get_offset_of_VYSASJjqEIsmJcmnIHADUYEaRjQC_17() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___VYSASJjqEIsmJcmnIHADUYEaRjQC_17)); }
	inline int32_t get_VYSASJjqEIsmJcmnIHADUYEaRjQC_17() const { return ___VYSASJjqEIsmJcmnIHADUYEaRjQC_17; }
	inline int32_t* get_address_of_VYSASJjqEIsmJcmnIHADUYEaRjQC_17() { return &___VYSASJjqEIsmJcmnIHADUYEaRjQC_17; }
	inline void set_VYSASJjqEIsmJcmnIHADUYEaRjQC_17(int32_t value)
	{
		___VYSASJjqEIsmJcmnIHADUYEaRjQC_17 = value;
	}

	inline static int32_t get_offset_of_aWhbUpAtiftmjIGzRSoPwmEnzb_18() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___aWhbUpAtiftmjIGzRSoPwmEnzb_18)); }
	inline int32_t get_aWhbUpAtiftmjIGzRSoPwmEnzb_18() const { return ___aWhbUpAtiftmjIGzRSoPwmEnzb_18; }
	inline int32_t* get_address_of_aWhbUpAtiftmjIGzRSoPwmEnzb_18() { return &___aWhbUpAtiftmjIGzRSoPwmEnzb_18; }
	inline void set_aWhbUpAtiftmjIGzRSoPwmEnzb_18(int32_t value)
	{
		___aWhbUpAtiftmjIGzRSoPwmEnzb_18 = value;
	}

	inline static int32_t get_offset_of_ibgNLLocbIyPDBErUPhHUTjDZKC_19() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___ibgNLLocbIyPDBErUPhHUTjDZKC_19)); }
	inline bool get_ibgNLLocbIyPDBErUPhHUTjDZKC_19() const { return ___ibgNLLocbIyPDBErUPhHUTjDZKC_19; }
	inline bool* get_address_of_ibgNLLocbIyPDBErUPhHUTjDZKC_19() { return &___ibgNLLocbIyPDBErUPhHUTjDZKC_19; }
	inline void set_ibgNLLocbIyPDBErUPhHUTjDZKC_19(bool value)
	{
		___ibgNLLocbIyPDBErUPhHUTjDZKC_19 = value;
	}

	inline static int32_t get_offset_of_ZsEfMISqALipCaWIrZdQwCvMLkx_20() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___ZsEfMISqALipCaWIrZdQwCvMLkx_20)); }
	inline bool get_ZsEfMISqALipCaWIrZdQwCvMLkx_20() const { return ___ZsEfMISqALipCaWIrZdQwCvMLkx_20; }
	inline bool* get_address_of_ZsEfMISqALipCaWIrZdQwCvMLkx_20() { return &___ZsEfMISqALipCaWIrZdQwCvMLkx_20; }
	inline void set_ZsEfMISqALipCaWIrZdQwCvMLkx_20(bool value)
	{
		___ZsEfMISqALipCaWIrZdQwCvMLkx_20 = value;
	}

	inline static int32_t get_offset_of_YJKKCodZAAdHLAznZhhKAnRrzmS_21() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___YJKKCodZAAdHLAznZhhKAnRrzmS_21)); }
	inline int32_t get_YJKKCodZAAdHLAznZhhKAnRrzmS_21() const { return ___YJKKCodZAAdHLAznZhhKAnRrzmS_21; }
	inline int32_t* get_address_of_YJKKCodZAAdHLAznZhhKAnRrzmS_21() { return &___YJKKCodZAAdHLAznZhhKAnRrzmS_21; }
	inline void set_YJKKCodZAAdHLAznZhhKAnRrzmS_21(int32_t value)
	{
		___YJKKCodZAAdHLAznZhhKAnRrzmS_21 = value;
	}

	inline static int32_t get_offset_of_iKsRvufntavyFjWZbUibgspoftw_22() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___iKsRvufntavyFjWZbUibgspoftw_22)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_iKsRvufntavyFjWZbUibgspoftw_22() const { return ___iKsRvufntavyFjWZbUibgspoftw_22; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_iKsRvufntavyFjWZbUibgspoftw_22() { return &___iKsRvufntavyFjWZbUibgspoftw_22; }
	inline void set_iKsRvufntavyFjWZbUibgspoftw_22(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___iKsRvufntavyFjWZbUibgspoftw_22 = value;
		Il2CppCodeGenWriteBarrier((&___iKsRvufntavyFjWZbUibgspoftw_22), value);
	}

	inline static int32_t get_offset_of_YuSoQESZTtPuhZKVJFBavCSGQsh_23() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___YuSoQESZTtPuhZKVJFBavCSGQsh_23)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_YuSoQESZTtPuhZKVJFBavCSGQsh_23() const { return ___YuSoQESZTtPuhZKVJFBavCSGQsh_23; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_YuSoQESZTtPuhZKVJFBavCSGQsh_23() { return &___YuSoQESZTtPuhZKVJFBavCSGQsh_23; }
	inline void set_YuSoQESZTtPuhZKVJFBavCSGQsh_23(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___YuSoQESZTtPuhZKVJFBavCSGQsh_23 = value;
		Il2CppCodeGenWriteBarrier((&___YuSoQESZTtPuhZKVJFBavCSGQsh_23), value);
	}

	inline static int32_t get_offset_of_motlnrXwRclwbbdquGWkOEYsYFy_24() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___motlnrXwRclwbbdquGWkOEYsYFy_24)); }
	inline HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 * get_motlnrXwRclwbbdquGWkOEYsYFy_24() const { return ___motlnrXwRclwbbdquGWkOEYsYFy_24; }
	inline HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 ** get_address_of_motlnrXwRclwbbdquGWkOEYsYFy_24() { return &___motlnrXwRclwbbdquGWkOEYsYFy_24; }
	inline void set_motlnrXwRclwbbdquGWkOEYsYFy_24(HardwareJoystickMap_InputManager_tCBD3AB46AC703AF5780510DD95E1627388F6D9A0 * value)
	{
		___motlnrXwRclwbbdquGWkOEYsYFy_24 = value;
		Il2CppCodeGenWriteBarrier((&___motlnrXwRclwbbdquGWkOEYsYFy_24), value);
	}

	inline static int32_t get_offset_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_25() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_25)); }
	inline Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * get_QEAEcDsIVKSxlRksWqQtzkbPdqrh_25() const { return ___QEAEcDsIVKSxlRksWqQtzkbPdqrh_25; }
	inline Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF ** get_address_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_25() { return &___QEAEcDsIVKSxlRksWqQtzkbPdqrh_25; }
	inline void set_QEAEcDsIVKSxlRksWqQtzkbPdqrh_25(Func_2_tDC021E3C9128A5869D895014BECA43AB8EA43CEF * value)
	{
		___QEAEcDsIVKSxlRksWqQtzkbPdqrh_25 = value;
		Il2CppCodeGenWriteBarrier((&___QEAEcDsIVKSxlRksWqQtzkbPdqrh_25), value);
	}

	inline static int32_t get_offset_of_JKdifYgUghWnBkRBRChckuzXKWst_26() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___JKdifYgUghWnBkRBRChckuzXKWst_26)); }
	inline bool get_JKdifYgUghWnBkRBRChckuzXKWst_26() const { return ___JKdifYgUghWnBkRBRChckuzXKWst_26; }
	inline bool* get_address_of_JKdifYgUghWnBkRBRChckuzXKWst_26() { return &___JKdifYgUghWnBkRBRChckuzXKWst_26; }
	inline void set_JKdifYgUghWnBkRBRChckuzXKWst_26(bool value)
	{
		___JKdifYgUghWnBkRBRChckuzXKWst_26 = value;
	}

	inline static int32_t get_offset_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_27() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___uTrFqghzwmUUNLMxoQSRVgfDfsj_27)); }
	inline bool get_uTrFqghzwmUUNLMxoQSRVgfDfsj_27() const { return ___uTrFqghzwmUUNLMxoQSRVgfDfsj_27; }
	inline bool* get_address_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_27() { return &___uTrFqghzwmUUNLMxoQSRVgfDfsj_27; }
	inline void set_uTrFqghzwmUUNLMxoQSRVgfDfsj_27(bool value)
	{
		___uTrFqghzwmUUNLMxoQSRVgfDfsj_27 = value;
	}

	inline static int32_t get_offset_of_pwFxxEkPXgSZokcVFZSbwgxzpUm_28() { return static_cast<int32_t>(offsetof(lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489, ___pwFxxEkPXgSZokcVFZSbwgxzpUm_28)); }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * get_pwFxxEkPXgSZokcVFZSbwgxzpUm_28() const { return ___pwFxxEkPXgSZokcVFZSbwgxzpUm_28; }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD ** get_address_of_pwFxxEkPXgSZokcVFZSbwgxzpUm_28() { return &___pwFxxEkPXgSZokcVFZSbwgxzpUm_28; }
	inline void set_pwFxxEkPXgSZokcVFZSbwgxzpUm_28(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * value)
	{
		___pwFxxEkPXgSZokcVFZSbwgxzpUm_28 = value;
		Il2CppCodeGenWriteBarrier((&___pwFxxEkPXgSZokcVFZSbwgxzpUm_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LNGIGGEOZTFHOKVPOBLVJBATVLYD_T36643A533B08EAB0B35FCE169CD422956CFC6489_H
#ifndef HSTSYMGCWOHMJHDSTXXVYTWOOPS_TE971DE592BCB2FA4DEFAD66F81CF054B19AC7235_H
#define HSTSYMGCWOHMJHDSTXXVYTWOOPS_TE971DE592BCB2FA4DEFAD66F81CF054B19AC7235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSources.SDL2.SDL2InputSource_HSTSymgCWOHMjHdStxXvytwOOps
struct  HSTSymgCWOHMjHdStxXvytwOOps_tE971DE592BCB2FA4DEFAD66F81CF054B19AC7235  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSTSYMGCWOHMJHDSTXXVYTWOOPS_TE971DE592BCB2FA4DEFAD66F81CF054B19AC7235_H
#ifndef SCQQBEYGTPFZVHIREBLSXZGPUGI_TBB049F5394A5E47B148D20B4B3255754F5F1157B_H
#define SCQQBEYGTPFZVHIREBLSXZGPUGI_TBB049F5394A5E47B148D20B4B3255754F5F1157B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSources.SDL2.SDL2InputSource_SCQqbeyGtpFZvHIREBLSXZgPUgi
struct  SCQqbeyGtpFZvHIREBLSXZgPUgi_tBB049F5394A5E47B148D20B4B3255754F5F1157B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCQQBEYGTPFZVHIREBLSXZGPUGI_TBB049F5394A5E47B148D20B4B3255754F5F1157B_H
#ifndef MSPXMIWJNMLLEKNWBNLNXLKWEGUK_T82684D4A5C90A79EB8A55071BFD9AC830563CDBC_H
#define MSPXMIWJNMLLEKNWBNLNXLKWEGUK_T82684D4A5C90A79EB8A55071BFD9AC830563CDBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSources.SDL2.SDL2InputSource_mSpXmiwJNMLLEknWBnlNXlKweguk
struct  mSpXmiwJNMLLEknWBnlNXlKweguk_t82684D4A5C90A79EB8A55071BFD9AC830563CDBC  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MSPXMIWJNMLLEKNWBNLNXLKWEGUK_T82684D4A5C90A79EB8A55071BFD9AC830563CDBC_H
#ifndef WYIDIFUVUUXBMXICVFTZXDHORQT_T9793B783AA7E7FD2AA8A17EE2FE5F1989BF5E02E_H
#define WYIDIFUVUUXBMXICVFTZXDHORQT_T9793B783AA7E7FD2AA8A17EE2FE5F1989BF5E02E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSources.SDL2.SDL2InputSource_wyiDiFuVUUXBMxIcVftzXDHorqT
struct  wyiDiFuVUUXBMxIcVftzXDHorqT_t9793B783AA7E7FD2AA8A17EE2FE5F1989BF5E02E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WYIDIFUVUUXBMXICVFTZXDHORQT_T9793B783AA7E7FD2AA8A17EE2FE5F1989BF5E02E_H
#ifndef CNOUGKQTGKPKOLZDUANACZSCSJI_TA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9_H
#define CNOUGKQTGKPKOLZDUANACZSCSJI_TA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi
struct  CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ElementAssignmentConflictCheck Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::RXwTkvaioBOXLvHfDHkciCcOgzY
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___RXwTkvaioBOXLvHfDHkciCcOgzY_3;
	// Rewired.ElementAssignmentConflictCheck Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::CdduzXmnfyAqWtnFBTQwIrsXYmO
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___CdduzXmnfyAqWtnFBTQwIrsXYmO_4;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_5;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_8;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::TLUFNqPThThRSpKtpbHMfphhHIql
	bool ___TLUFNqPThThRSpKtpbHMfphhHIql_9;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::GsRgbEdsthhdHbQCaXWTDtLBvoPC
	bool ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10;
	// System.Collections.Generic.IList`1<Rewired.Player> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::dmZRAIkVjUvKcmdJPcXZiYIkiWR
	RuntimeObject* ___dmZRAIkVjUvKcmdJPcXZiYIkiWR_11;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::eniqMbpmkEymlMgBiZFLwORgmDl
	int32_t ___eniqMbpmkEymlMgBiZFLwORgmDl_12;
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::CIeRisDKgBhtRtQeaqjBCBznbdk
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___CIeRisDKgBhtRtQeaqjBCBznbdk_13;
	// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_14;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_CNoUgkqtgkpKOLZDuanACzscSJi::hRHaoNoWJlONxyPDGLaddQVapJQ
	RuntimeObject* ___hRHaoNoWJlONxyPDGLaddQVapJQ_15;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_3() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___RXwTkvaioBOXLvHfDHkciCcOgzY_3)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_RXwTkvaioBOXLvHfDHkciCcOgzY_3() const { return ___RXwTkvaioBOXLvHfDHkciCcOgzY_3; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_RXwTkvaioBOXLvHfDHkciCcOgzY_3() { return &___RXwTkvaioBOXLvHfDHkciCcOgzY_3; }
	inline void set_RXwTkvaioBOXLvHfDHkciCcOgzY_3(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___RXwTkvaioBOXLvHfDHkciCcOgzY_3 = value;
	}

	inline static int32_t get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_4() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___CdduzXmnfyAqWtnFBTQwIrsXYmO_4)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_CdduzXmnfyAqWtnFBTQwIrsXYmO_4() const { return ___CdduzXmnfyAqWtnFBTQwIrsXYmO_4; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_4() { return &___CdduzXmnfyAqWtnFBTQwIrsXYmO_4; }
	inline void set_CdduzXmnfyAqWtnFBTQwIrsXYmO_4(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___CdduzXmnfyAqWtnFBTQwIrsXYmO_4 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_5() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_5)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_5() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_5; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_5() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_5; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_5(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_5 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_8() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___CVShsOfomYgQbeMNmgtRkoanKAb_8)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_8() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_8; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_8() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_8; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_8(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_8 = value;
	}

	inline static int32_t get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_9() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___TLUFNqPThThRSpKtpbHMfphhHIql_9)); }
	inline bool get_TLUFNqPThThRSpKtpbHMfphhHIql_9() const { return ___TLUFNqPThThRSpKtpbHMfphhHIql_9; }
	inline bool* get_address_of_TLUFNqPThThRSpKtpbHMfphhHIql_9() { return &___TLUFNqPThThRSpKtpbHMfphhHIql_9; }
	inline void set_TLUFNqPThThRSpKtpbHMfphhHIql_9(bool value)
	{
		___TLUFNqPThThRSpKtpbHMfphhHIql_9 = value;
	}

	inline static int32_t get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10)); }
	inline bool get_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10() const { return ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10; }
	inline bool* get_address_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10() { return &___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10; }
	inline void set_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10(bool value)
	{
		___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10 = value;
	}

	inline static int32_t get_offset_of_dmZRAIkVjUvKcmdJPcXZiYIkiWR_11() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___dmZRAIkVjUvKcmdJPcXZiYIkiWR_11)); }
	inline RuntimeObject* get_dmZRAIkVjUvKcmdJPcXZiYIkiWR_11() const { return ___dmZRAIkVjUvKcmdJPcXZiYIkiWR_11; }
	inline RuntimeObject** get_address_of_dmZRAIkVjUvKcmdJPcXZiYIkiWR_11() { return &___dmZRAIkVjUvKcmdJPcXZiYIkiWR_11; }
	inline void set_dmZRAIkVjUvKcmdJPcXZiYIkiWR_11(RuntimeObject* value)
	{
		___dmZRAIkVjUvKcmdJPcXZiYIkiWR_11 = value;
		Il2CppCodeGenWriteBarrier((&___dmZRAIkVjUvKcmdJPcXZiYIkiWR_11), value);
	}

	inline static int32_t get_offset_of_eniqMbpmkEymlMgBiZFLwORgmDl_12() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___eniqMbpmkEymlMgBiZFLwORgmDl_12)); }
	inline int32_t get_eniqMbpmkEymlMgBiZFLwORgmDl_12() const { return ___eniqMbpmkEymlMgBiZFLwORgmDl_12; }
	inline int32_t* get_address_of_eniqMbpmkEymlMgBiZFLwORgmDl_12() { return &___eniqMbpmkEymlMgBiZFLwORgmDl_12; }
	inline void set_eniqMbpmkEymlMgBiZFLwORgmDl_12(int32_t value)
	{
		___eniqMbpmkEymlMgBiZFLwORgmDl_12 = value;
	}

	inline static int32_t get_offset_of_CIeRisDKgBhtRtQeaqjBCBznbdk_13() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___CIeRisDKgBhtRtQeaqjBCBznbdk_13)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_CIeRisDKgBhtRtQeaqjBCBznbdk_13() const { return ___CIeRisDKgBhtRtQeaqjBCBznbdk_13; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_CIeRisDKgBhtRtQeaqjBCBznbdk_13() { return &___CIeRisDKgBhtRtQeaqjBCBznbdk_13; }
	inline void set_CIeRisDKgBhtRtQeaqjBCBznbdk_13(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___CIeRisDKgBhtRtQeaqjBCBznbdk_13 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_14() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_14)); }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_14() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_14; }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_14() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_14; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_14(ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_14 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_14), value);
	}

	inline static int32_t get_offset_of_hRHaoNoWJlONxyPDGLaddQVapJQ_15() { return static_cast<int32_t>(offsetof(CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9, ___hRHaoNoWJlONxyPDGLaddQVapJQ_15)); }
	inline RuntimeObject* get_hRHaoNoWJlONxyPDGLaddQVapJQ_15() const { return ___hRHaoNoWJlONxyPDGLaddQVapJQ_15; }
	inline RuntimeObject** get_address_of_hRHaoNoWJlONxyPDGLaddQVapJQ_15() { return &___hRHaoNoWJlONxyPDGLaddQVapJQ_15; }
	inline void set_hRHaoNoWJlONxyPDGLaddQVapJQ_15(RuntimeObject* value)
	{
		___hRHaoNoWJlONxyPDGLaddQVapJQ_15 = value;
		Il2CppCodeGenWriteBarrier((&___hRHaoNoWJlONxyPDGLaddQVapJQ_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CNOUGKQTGKPKOLZDUANACZSCSJI_TA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9_H
#ifndef JAHXPUXPAKAQULWLTGLXFNDONJSG_TFF3F890918CC2394148B9100D2E0FA61946D7CC0_H
#define JAHXPUXPAKAQULWLTGLXFNDONJSG_TFF3F890918CC2394148B9100D2E0FA61946D7CC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg
struct  JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::TSxIVLfBUyELziEzwQrAMRmzGqrD
	int32_t ___TSxIVLfBUyELziEzwQrAMRmzGqrD_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::DoBlgWmOMuusUcBpsKccsmEFmgP
	int32_t ___DoBlgWmOMuusUcBpsKccsmEFmgP_4;
	// Rewired.KeyboardMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::aXrADafiNQkJnrgOhDBJQpEVncQ
	KeyboardMap_t03C5D3775649F3FEBBE43728D8FB16C32E61EBBA * ___aXrADafiNQkJnrgOhDBJQpEVncQ_5;
	// Rewired.KeyboardMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::tKMifAOuGrDOrEhrNbSnJWJHjcU
	KeyboardMap_t03C5D3775649F3FEBBE43728D8FB16C32E61EBBA * ___tKMifAOuGrDOrEhrNbSnJWJHjcU_6;
	// Rewired.ActionElementMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::OFOCHvgjhTiUuiMSfLxKRWhcDSN
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_7;
	// Rewired.ActionElementMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::ISUlQumxWpBMGxJqFBsCyVmpAjn
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___ISUlQumxWpBMGxJqFBsCyVmpAjn_8;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_9;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_10;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_12;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::TLUFNqPThThRSpKtpbHMfphhHIql
	bool ___TLUFNqPThThRSpKtpbHMfphhHIql_13;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::GsRgbEdsthhdHbQCaXWTDtLBvoPC
	bool ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_14;
	// System.Collections.Generic.IList`1<Rewired.Player> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::bsNKDEZlAuQUJoGEPzpoBCCNfRo
	RuntimeObject* ___bsNKDEZlAuQUJoGEPzpoBCCNfRo_15;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::uMeBkqfeFhSzPdshlBqemBqpelq
	int32_t ___uMeBkqfeFhSzPdshlBqemBqpelq_16;
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::NIbAjgqYgduWFNJPPNQWGAXXFWx
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NIbAjgqYgduWFNJPPNQWGAXXFWx_17;
	// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_18;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JAhXPuxpAkaquLwLTgLxFNdoNjSg::RMAbZLXlKHjqnjikzXemsVwVkcHN
	RuntimeObject* ___RMAbZLXlKHjqnjikzXemsVwVkcHN_19;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_3() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___TSxIVLfBUyELziEzwQrAMRmzGqrD_3)); }
	inline int32_t get_TSxIVLfBUyELziEzwQrAMRmzGqrD_3() const { return ___TSxIVLfBUyELziEzwQrAMRmzGqrD_3; }
	inline int32_t* get_address_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_3() { return &___TSxIVLfBUyELziEzwQrAMRmzGqrD_3; }
	inline void set_TSxIVLfBUyELziEzwQrAMRmzGqrD_3(int32_t value)
	{
		___TSxIVLfBUyELziEzwQrAMRmzGqrD_3 = value;
	}

	inline static int32_t get_offset_of_DoBlgWmOMuusUcBpsKccsmEFmgP_4() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___DoBlgWmOMuusUcBpsKccsmEFmgP_4)); }
	inline int32_t get_DoBlgWmOMuusUcBpsKccsmEFmgP_4() const { return ___DoBlgWmOMuusUcBpsKccsmEFmgP_4; }
	inline int32_t* get_address_of_DoBlgWmOMuusUcBpsKccsmEFmgP_4() { return &___DoBlgWmOMuusUcBpsKccsmEFmgP_4; }
	inline void set_DoBlgWmOMuusUcBpsKccsmEFmgP_4(int32_t value)
	{
		___DoBlgWmOMuusUcBpsKccsmEFmgP_4 = value;
	}

	inline static int32_t get_offset_of_aXrADafiNQkJnrgOhDBJQpEVncQ_5() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___aXrADafiNQkJnrgOhDBJQpEVncQ_5)); }
	inline KeyboardMap_t03C5D3775649F3FEBBE43728D8FB16C32E61EBBA * get_aXrADafiNQkJnrgOhDBJQpEVncQ_5() const { return ___aXrADafiNQkJnrgOhDBJQpEVncQ_5; }
	inline KeyboardMap_t03C5D3775649F3FEBBE43728D8FB16C32E61EBBA ** get_address_of_aXrADafiNQkJnrgOhDBJQpEVncQ_5() { return &___aXrADafiNQkJnrgOhDBJQpEVncQ_5; }
	inline void set_aXrADafiNQkJnrgOhDBJQpEVncQ_5(KeyboardMap_t03C5D3775649F3FEBBE43728D8FB16C32E61EBBA * value)
	{
		___aXrADafiNQkJnrgOhDBJQpEVncQ_5 = value;
		Il2CppCodeGenWriteBarrier((&___aXrADafiNQkJnrgOhDBJQpEVncQ_5), value);
	}

	inline static int32_t get_offset_of_tKMifAOuGrDOrEhrNbSnJWJHjcU_6() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___tKMifAOuGrDOrEhrNbSnJWJHjcU_6)); }
	inline KeyboardMap_t03C5D3775649F3FEBBE43728D8FB16C32E61EBBA * get_tKMifAOuGrDOrEhrNbSnJWJHjcU_6() const { return ___tKMifAOuGrDOrEhrNbSnJWJHjcU_6; }
	inline KeyboardMap_t03C5D3775649F3FEBBE43728D8FB16C32E61EBBA ** get_address_of_tKMifAOuGrDOrEhrNbSnJWJHjcU_6() { return &___tKMifAOuGrDOrEhrNbSnJWJHjcU_6; }
	inline void set_tKMifAOuGrDOrEhrNbSnJWJHjcU_6(KeyboardMap_t03C5D3775649F3FEBBE43728D8FB16C32E61EBBA * value)
	{
		___tKMifAOuGrDOrEhrNbSnJWJHjcU_6 = value;
		Il2CppCodeGenWriteBarrier((&___tKMifAOuGrDOrEhrNbSnJWJHjcU_6), value);
	}

	inline static int32_t get_offset_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_7() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_7)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_OFOCHvgjhTiUuiMSfLxKRWhcDSN_7() const { return ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_7; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_7() { return &___OFOCHvgjhTiUuiMSfLxKRWhcDSN_7; }
	inline void set_OFOCHvgjhTiUuiMSfLxKRWhcDSN_7(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___OFOCHvgjhTiUuiMSfLxKRWhcDSN_7 = value;
		Il2CppCodeGenWriteBarrier((&___OFOCHvgjhTiUuiMSfLxKRWhcDSN_7), value);
	}

	inline static int32_t get_offset_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_8() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___ISUlQumxWpBMGxJqFBsCyVmpAjn_8)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_ISUlQumxWpBMGxJqFBsCyVmpAjn_8() const { return ___ISUlQumxWpBMGxJqFBsCyVmpAjn_8; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_8() { return &___ISUlQumxWpBMGxJqFBsCyVmpAjn_8; }
	inline void set_ISUlQumxWpBMGxJqFBsCyVmpAjn_8(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___ISUlQumxWpBMGxJqFBsCyVmpAjn_8 = value;
		Il2CppCodeGenWriteBarrier((&___ISUlQumxWpBMGxJqFBsCyVmpAjn_8), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_9() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_9)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_9() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_9; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_9() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_9; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_9(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_9 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_10() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_10)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_10() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_10; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_10() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_10; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_10(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_10 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_12() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___CVShsOfomYgQbeMNmgtRkoanKAb_12)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_12() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_12; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_12() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_12; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_12(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_12 = value;
	}

	inline static int32_t get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_13() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___TLUFNqPThThRSpKtpbHMfphhHIql_13)); }
	inline bool get_TLUFNqPThThRSpKtpbHMfphhHIql_13() const { return ___TLUFNqPThThRSpKtpbHMfphhHIql_13; }
	inline bool* get_address_of_TLUFNqPThThRSpKtpbHMfphhHIql_13() { return &___TLUFNqPThThRSpKtpbHMfphhHIql_13; }
	inline void set_TLUFNqPThThRSpKtpbHMfphhHIql_13(bool value)
	{
		___TLUFNqPThThRSpKtpbHMfphhHIql_13 = value;
	}

	inline static int32_t get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_14() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_14)); }
	inline bool get_GsRgbEdsthhdHbQCaXWTDtLBvoPC_14() const { return ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_14; }
	inline bool* get_address_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_14() { return &___GsRgbEdsthhdHbQCaXWTDtLBvoPC_14; }
	inline void set_GsRgbEdsthhdHbQCaXWTDtLBvoPC_14(bool value)
	{
		___GsRgbEdsthhdHbQCaXWTDtLBvoPC_14 = value;
	}

	inline static int32_t get_offset_of_bsNKDEZlAuQUJoGEPzpoBCCNfRo_15() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___bsNKDEZlAuQUJoGEPzpoBCCNfRo_15)); }
	inline RuntimeObject* get_bsNKDEZlAuQUJoGEPzpoBCCNfRo_15() const { return ___bsNKDEZlAuQUJoGEPzpoBCCNfRo_15; }
	inline RuntimeObject** get_address_of_bsNKDEZlAuQUJoGEPzpoBCCNfRo_15() { return &___bsNKDEZlAuQUJoGEPzpoBCCNfRo_15; }
	inline void set_bsNKDEZlAuQUJoGEPzpoBCCNfRo_15(RuntimeObject* value)
	{
		___bsNKDEZlAuQUJoGEPzpoBCCNfRo_15 = value;
		Il2CppCodeGenWriteBarrier((&___bsNKDEZlAuQUJoGEPzpoBCCNfRo_15), value);
	}

	inline static int32_t get_offset_of_uMeBkqfeFhSzPdshlBqemBqpelq_16() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___uMeBkqfeFhSzPdshlBqemBqpelq_16)); }
	inline int32_t get_uMeBkqfeFhSzPdshlBqemBqpelq_16() const { return ___uMeBkqfeFhSzPdshlBqemBqpelq_16; }
	inline int32_t* get_address_of_uMeBkqfeFhSzPdshlBqemBqpelq_16() { return &___uMeBkqfeFhSzPdshlBqemBqpelq_16; }
	inline void set_uMeBkqfeFhSzPdshlBqemBqpelq_16(int32_t value)
	{
		___uMeBkqfeFhSzPdshlBqemBqpelq_16 = value;
	}

	inline static int32_t get_offset_of_NIbAjgqYgduWFNJPPNQWGAXXFWx_17() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___NIbAjgqYgduWFNJPPNQWGAXXFWx_17)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NIbAjgqYgduWFNJPPNQWGAXXFWx_17() const { return ___NIbAjgqYgduWFNJPPNQWGAXXFWx_17; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NIbAjgqYgduWFNJPPNQWGAXXFWx_17() { return &___NIbAjgqYgduWFNJPPNQWGAXXFWx_17; }
	inline void set_NIbAjgqYgduWFNJPPNQWGAXXFWx_17(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NIbAjgqYgduWFNJPPNQWGAXXFWx_17 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_18() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_18)); }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_18() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_18; }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_18() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_18; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_18(ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_18 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_18), value);
	}

	inline static int32_t get_offset_of_RMAbZLXlKHjqnjikzXemsVwVkcHN_19() { return static_cast<int32_t>(offsetof(JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0, ___RMAbZLXlKHjqnjikzXemsVwVkcHN_19)); }
	inline RuntimeObject* get_RMAbZLXlKHjqnjikzXemsVwVkcHN_19() const { return ___RMAbZLXlKHjqnjikzXemsVwVkcHN_19; }
	inline RuntimeObject** get_address_of_RMAbZLXlKHjqnjikzXemsVwVkcHN_19() { return &___RMAbZLXlKHjqnjikzXemsVwVkcHN_19; }
	inline void set_RMAbZLXlKHjqnjikzXemsVwVkcHN_19(RuntimeObject* value)
	{
		___RMAbZLXlKHjqnjikzXemsVwVkcHN_19 = value;
		Il2CppCodeGenWriteBarrier((&___RMAbZLXlKHjqnjikzXemsVwVkcHN_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAHXPUXPAKAQULWLTGLXFNDONJSG_TFF3F890918CC2394148B9100D2E0FA61946D7CC0_H
#ifndef JQZDNIGNFQCZRCJVVSEGJPQTAPHC_TFF5D06E1B005145129B2D7DD428D52C2B4C00746_H
#define JQZDNIGNFQCZRCJVVSEGJPQTAPHC_TFF5D06E1B005145129B2D7DD428D52C2B4C00746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc
struct  JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::TSxIVLfBUyELziEzwQrAMRmzGqrD
	int32_t ___TSxIVLfBUyELziEzwQrAMRmzGqrD_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::DoBlgWmOMuusUcBpsKccsmEFmgP
	int32_t ___DoBlgWmOMuusUcBpsKccsmEFmgP_4;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::xJPYTeJZICkFpIWTwbMsXQVEALx
	int32_t ___xJPYTeJZICkFpIWTwbMsXQVEALx_5;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::kilUCbTWeCfCdDqQSHFWpvHLrBl
	int32_t ___kilUCbTWeCfCdDqQSHFWpvHLrBl_6;
	// Rewired.JoystickMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::YtyCpfuMjInPTabavmllkqElczPA
	JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * ___YtyCpfuMjInPTabavmllkqElczPA_7;
	// Rewired.JoystickMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::RjbgzWvPMFXOEdOLgPwRLLJVLIr
	JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * ___RjbgzWvPMFXOEdOLgPwRLLJVLIr_8;
	// Rewired.ActionElementMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::OFOCHvgjhTiUuiMSfLxKRWhcDSN
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_9;
	// Rewired.ActionElementMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::ISUlQumxWpBMGxJqFBsCyVmpAjn
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___ISUlQumxWpBMGxJqFBsCyVmpAjn_10;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_11;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_12;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_14;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::TLUFNqPThThRSpKtpbHMfphhHIql
	bool ___TLUFNqPThThRSpKtpbHMfphhHIql_15;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::GsRgbEdsthhdHbQCaXWTDtLBvoPC
	bool ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_16;
	// System.Collections.Generic.IList`1<Rewired.Player> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::bKjUZqMHYbTsHXEGFfmLqDeZgwz
	RuntimeObject* ___bKjUZqMHYbTsHXEGFfmLqDeZgwz_17;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::wzhtPpbPsLSdcQjSDfoZDUUrQsy
	int32_t ___wzhtPpbPsLSdcQjSDfoZDUUrQsy_18;
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::noCcVHSPuFvPuZSYGWTvfkGIGFf
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___noCcVHSPuFvPuZSYGWTvfkGIGFf_19;
	// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_20;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_JQzdNignFqczrcjvvseGJpQtaPhc::sWhONswpHNEGMJZjXZGrrmQEirr
	RuntimeObject* ___sWhONswpHNEGMJZjXZGrrmQEirr_21;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_3() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___TSxIVLfBUyELziEzwQrAMRmzGqrD_3)); }
	inline int32_t get_TSxIVLfBUyELziEzwQrAMRmzGqrD_3() const { return ___TSxIVLfBUyELziEzwQrAMRmzGqrD_3; }
	inline int32_t* get_address_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_3() { return &___TSxIVLfBUyELziEzwQrAMRmzGqrD_3; }
	inline void set_TSxIVLfBUyELziEzwQrAMRmzGqrD_3(int32_t value)
	{
		___TSxIVLfBUyELziEzwQrAMRmzGqrD_3 = value;
	}

	inline static int32_t get_offset_of_DoBlgWmOMuusUcBpsKccsmEFmgP_4() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___DoBlgWmOMuusUcBpsKccsmEFmgP_4)); }
	inline int32_t get_DoBlgWmOMuusUcBpsKccsmEFmgP_4() const { return ___DoBlgWmOMuusUcBpsKccsmEFmgP_4; }
	inline int32_t* get_address_of_DoBlgWmOMuusUcBpsKccsmEFmgP_4() { return &___DoBlgWmOMuusUcBpsKccsmEFmgP_4; }
	inline void set_DoBlgWmOMuusUcBpsKccsmEFmgP_4(int32_t value)
	{
		___DoBlgWmOMuusUcBpsKccsmEFmgP_4 = value;
	}

	inline static int32_t get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_5() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___xJPYTeJZICkFpIWTwbMsXQVEALx_5)); }
	inline int32_t get_xJPYTeJZICkFpIWTwbMsXQVEALx_5() const { return ___xJPYTeJZICkFpIWTwbMsXQVEALx_5; }
	inline int32_t* get_address_of_xJPYTeJZICkFpIWTwbMsXQVEALx_5() { return &___xJPYTeJZICkFpIWTwbMsXQVEALx_5; }
	inline void set_xJPYTeJZICkFpIWTwbMsXQVEALx_5(int32_t value)
	{
		___xJPYTeJZICkFpIWTwbMsXQVEALx_5 = value;
	}

	inline static int32_t get_offset_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_6() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___kilUCbTWeCfCdDqQSHFWpvHLrBl_6)); }
	inline int32_t get_kilUCbTWeCfCdDqQSHFWpvHLrBl_6() const { return ___kilUCbTWeCfCdDqQSHFWpvHLrBl_6; }
	inline int32_t* get_address_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_6() { return &___kilUCbTWeCfCdDqQSHFWpvHLrBl_6; }
	inline void set_kilUCbTWeCfCdDqQSHFWpvHLrBl_6(int32_t value)
	{
		___kilUCbTWeCfCdDqQSHFWpvHLrBl_6 = value;
	}

	inline static int32_t get_offset_of_YtyCpfuMjInPTabavmllkqElczPA_7() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___YtyCpfuMjInPTabavmllkqElczPA_7)); }
	inline JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * get_YtyCpfuMjInPTabavmllkqElczPA_7() const { return ___YtyCpfuMjInPTabavmllkqElczPA_7; }
	inline JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 ** get_address_of_YtyCpfuMjInPTabavmllkqElczPA_7() { return &___YtyCpfuMjInPTabavmllkqElczPA_7; }
	inline void set_YtyCpfuMjInPTabavmllkqElczPA_7(JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * value)
	{
		___YtyCpfuMjInPTabavmllkqElczPA_7 = value;
		Il2CppCodeGenWriteBarrier((&___YtyCpfuMjInPTabavmllkqElczPA_7), value);
	}

	inline static int32_t get_offset_of_RjbgzWvPMFXOEdOLgPwRLLJVLIr_8() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___RjbgzWvPMFXOEdOLgPwRLLJVLIr_8)); }
	inline JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * get_RjbgzWvPMFXOEdOLgPwRLLJVLIr_8() const { return ___RjbgzWvPMFXOEdOLgPwRLLJVLIr_8; }
	inline JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 ** get_address_of_RjbgzWvPMFXOEdOLgPwRLLJVLIr_8() { return &___RjbgzWvPMFXOEdOLgPwRLLJVLIr_8; }
	inline void set_RjbgzWvPMFXOEdOLgPwRLLJVLIr_8(JoystickMap_tACC09DDD73FB3831A7B4D797C10DFB7FD2F8BB24 * value)
	{
		___RjbgzWvPMFXOEdOLgPwRLLJVLIr_8 = value;
		Il2CppCodeGenWriteBarrier((&___RjbgzWvPMFXOEdOLgPwRLLJVLIr_8), value);
	}

	inline static int32_t get_offset_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_9() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_9)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_OFOCHvgjhTiUuiMSfLxKRWhcDSN_9() const { return ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_9; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_9() { return &___OFOCHvgjhTiUuiMSfLxKRWhcDSN_9; }
	inline void set_OFOCHvgjhTiUuiMSfLxKRWhcDSN_9(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___OFOCHvgjhTiUuiMSfLxKRWhcDSN_9 = value;
		Il2CppCodeGenWriteBarrier((&___OFOCHvgjhTiUuiMSfLxKRWhcDSN_9), value);
	}

	inline static int32_t get_offset_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_10() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___ISUlQumxWpBMGxJqFBsCyVmpAjn_10)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_ISUlQumxWpBMGxJqFBsCyVmpAjn_10() const { return ___ISUlQumxWpBMGxJqFBsCyVmpAjn_10; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_10() { return &___ISUlQumxWpBMGxJqFBsCyVmpAjn_10; }
	inline void set_ISUlQumxWpBMGxJqFBsCyVmpAjn_10(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___ISUlQumxWpBMGxJqFBsCyVmpAjn_10 = value;
		Il2CppCodeGenWriteBarrier((&___ISUlQumxWpBMGxJqFBsCyVmpAjn_10), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_11() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_11)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_11() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_11; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_11() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_11; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_11(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_11 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_12() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_12)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_12() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_12; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_12() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_12; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_12(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_12 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_14() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___CVShsOfomYgQbeMNmgtRkoanKAb_14)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_14() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_14; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_14() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_14; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_14(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_14 = value;
	}

	inline static int32_t get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_15() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___TLUFNqPThThRSpKtpbHMfphhHIql_15)); }
	inline bool get_TLUFNqPThThRSpKtpbHMfphhHIql_15() const { return ___TLUFNqPThThRSpKtpbHMfphhHIql_15; }
	inline bool* get_address_of_TLUFNqPThThRSpKtpbHMfphhHIql_15() { return &___TLUFNqPThThRSpKtpbHMfphhHIql_15; }
	inline void set_TLUFNqPThThRSpKtpbHMfphhHIql_15(bool value)
	{
		___TLUFNqPThThRSpKtpbHMfphhHIql_15 = value;
	}

	inline static int32_t get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_16() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_16)); }
	inline bool get_GsRgbEdsthhdHbQCaXWTDtLBvoPC_16() const { return ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_16; }
	inline bool* get_address_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_16() { return &___GsRgbEdsthhdHbQCaXWTDtLBvoPC_16; }
	inline void set_GsRgbEdsthhdHbQCaXWTDtLBvoPC_16(bool value)
	{
		___GsRgbEdsthhdHbQCaXWTDtLBvoPC_16 = value;
	}

	inline static int32_t get_offset_of_bKjUZqMHYbTsHXEGFfmLqDeZgwz_17() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___bKjUZqMHYbTsHXEGFfmLqDeZgwz_17)); }
	inline RuntimeObject* get_bKjUZqMHYbTsHXEGFfmLqDeZgwz_17() const { return ___bKjUZqMHYbTsHXEGFfmLqDeZgwz_17; }
	inline RuntimeObject** get_address_of_bKjUZqMHYbTsHXEGFfmLqDeZgwz_17() { return &___bKjUZqMHYbTsHXEGFfmLqDeZgwz_17; }
	inline void set_bKjUZqMHYbTsHXEGFfmLqDeZgwz_17(RuntimeObject* value)
	{
		___bKjUZqMHYbTsHXEGFfmLqDeZgwz_17 = value;
		Il2CppCodeGenWriteBarrier((&___bKjUZqMHYbTsHXEGFfmLqDeZgwz_17), value);
	}

	inline static int32_t get_offset_of_wzhtPpbPsLSdcQjSDfoZDUUrQsy_18() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___wzhtPpbPsLSdcQjSDfoZDUUrQsy_18)); }
	inline int32_t get_wzhtPpbPsLSdcQjSDfoZDUUrQsy_18() const { return ___wzhtPpbPsLSdcQjSDfoZDUUrQsy_18; }
	inline int32_t* get_address_of_wzhtPpbPsLSdcQjSDfoZDUUrQsy_18() { return &___wzhtPpbPsLSdcQjSDfoZDUUrQsy_18; }
	inline void set_wzhtPpbPsLSdcQjSDfoZDUUrQsy_18(int32_t value)
	{
		___wzhtPpbPsLSdcQjSDfoZDUUrQsy_18 = value;
	}

	inline static int32_t get_offset_of_noCcVHSPuFvPuZSYGWTvfkGIGFf_19() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___noCcVHSPuFvPuZSYGWTvfkGIGFf_19)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_noCcVHSPuFvPuZSYGWTvfkGIGFf_19() const { return ___noCcVHSPuFvPuZSYGWTvfkGIGFf_19; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_noCcVHSPuFvPuZSYGWTvfkGIGFf_19() { return &___noCcVHSPuFvPuZSYGWTvfkGIGFf_19; }
	inline void set_noCcVHSPuFvPuZSYGWTvfkGIGFf_19(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___noCcVHSPuFvPuZSYGWTvfkGIGFf_19 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_20() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_20)); }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_20() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_20; }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_20() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_20; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_20(ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_20 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_20), value);
	}

	inline static int32_t get_offset_of_sWhONswpHNEGMJZjXZGrrmQEirr_21() { return static_cast<int32_t>(offsetof(JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746, ___sWhONswpHNEGMJZjXZGrrmQEirr_21)); }
	inline RuntimeObject* get_sWhONswpHNEGMJZjXZGrrmQEirr_21() const { return ___sWhONswpHNEGMJZjXZGrrmQEirr_21; }
	inline RuntimeObject** get_address_of_sWhONswpHNEGMJZjXZGrrmQEirr_21() { return &___sWhONswpHNEGMJZjXZGrrmQEirr_21; }
	inline void set_sWhONswpHNEGMJZjXZGrrmQEirr_21(RuntimeObject* value)
	{
		___sWhONswpHNEGMJZjXZGrrmQEirr_21 = value;
		Il2CppCodeGenWriteBarrier((&___sWhONswpHNEGMJZjXZGrrmQEirr_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JQZDNIGNFQCZRCJVVSEGJPQTAPHC_TFF5D06E1B005145129B2D7DD428D52C2B4C00746_H
#ifndef OLMYBIZELJYMEUKKKZCAYUGCRCW_TDE41E2E34F5857309066828A1F7B7105A4A2A462_H
#define OLMYBIZELJYMEUKKKZCAYUGCRCW_TDE41E2E34F5857309066828A1F7B7105A4A2A462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw
struct  OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::TSxIVLfBUyELziEzwQrAMRmzGqrD
	int32_t ___TSxIVLfBUyELziEzwQrAMRmzGqrD_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::DoBlgWmOMuusUcBpsKccsmEFmgP
	int32_t ___DoBlgWmOMuusUcBpsKccsmEFmgP_4;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::xJPYTeJZICkFpIWTwbMsXQVEALx
	int32_t ___xJPYTeJZICkFpIWTwbMsXQVEALx_5;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::kilUCbTWeCfCdDqQSHFWpvHLrBl
	int32_t ___kilUCbTWeCfCdDqQSHFWpvHLrBl_6;
	// Rewired.CustomControllerMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::WigXBlhRQEAAKnPZjEeeNdMahyT
	CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * ___WigXBlhRQEAAKnPZjEeeNdMahyT_7;
	// Rewired.CustomControllerMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::hqJogBNPbbjuJUDUhZmRhiFNzfx
	CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * ___hqJogBNPbbjuJUDUhZmRhiFNzfx_8;
	// Rewired.ActionElementMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::OFOCHvgjhTiUuiMSfLxKRWhcDSN
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_9;
	// Rewired.ActionElementMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::ISUlQumxWpBMGxJqFBsCyVmpAjn
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___ISUlQumxWpBMGxJqFBsCyVmpAjn_10;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_11;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_12;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_14;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::TLUFNqPThThRSpKtpbHMfphhHIql
	bool ___TLUFNqPThThRSpKtpbHMfphhHIql_15;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::GsRgbEdsthhdHbQCaXWTDtLBvoPC
	bool ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_16;
	// System.Collections.Generic.IList`1<Rewired.Player> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::QljwEiMqTqTwBZqpaHrnqQjxQMB
	RuntimeObject* ___QljwEiMqTqTwBZqpaHrnqQjxQMB_17;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::onoURHsEjHZzeEEizHTuPVhquEU
	int32_t ___onoURHsEjHZzeEEizHTuPVhquEU_18;
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::gAygEHCgtsXRJGTqpaFpqsnjGYMU
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___gAygEHCgtsXRJGTqpaFpqsnjGYMU_19;
	// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_20;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_OLmyBIZeljYmEuKKkZcAyuGcrcw::IQffKHKdJVcAMiqXIHpHcMMqBjig
	RuntimeObject* ___IQffKHKdJVcAMiqXIHpHcMMqBjig_21;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_3() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___TSxIVLfBUyELziEzwQrAMRmzGqrD_3)); }
	inline int32_t get_TSxIVLfBUyELziEzwQrAMRmzGqrD_3() const { return ___TSxIVLfBUyELziEzwQrAMRmzGqrD_3; }
	inline int32_t* get_address_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_3() { return &___TSxIVLfBUyELziEzwQrAMRmzGqrD_3; }
	inline void set_TSxIVLfBUyELziEzwQrAMRmzGqrD_3(int32_t value)
	{
		___TSxIVLfBUyELziEzwQrAMRmzGqrD_3 = value;
	}

	inline static int32_t get_offset_of_DoBlgWmOMuusUcBpsKccsmEFmgP_4() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___DoBlgWmOMuusUcBpsKccsmEFmgP_4)); }
	inline int32_t get_DoBlgWmOMuusUcBpsKccsmEFmgP_4() const { return ___DoBlgWmOMuusUcBpsKccsmEFmgP_4; }
	inline int32_t* get_address_of_DoBlgWmOMuusUcBpsKccsmEFmgP_4() { return &___DoBlgWmOMuusUcBpsKccsmEFmgP_4; }
	inline void set_DoBlgWmOMuusUcBpsKccsmEFmgP_4(int32_t value)
	{
		___DoBlgWmOMuusUcBpsKccsmEFmgP_4 = value;
	}

	inline static int32_t get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_5() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___xJPYTeJZICkFpIWTwbMsXQVEALx_5)); }
	inline int32_t get_xJPYTeJZICkFpIWTwbMsXQVEALx_5() const { return ___xJPYTeJZICkFpIWTwbMsXQVEALx_5; }
	inline int32_t* get_address_of_xJPYTeJZICkFpIWTwbMsXQVEALx_5() { return &___xJPYTeJZICkFpIWTwbMsXQVEALx_5; }
	inline void set_xJPYTeJZICkFpIWTwbMsXQVEALx_5(int32_t value)
	{
		___xJPYTeJZICkFpIWTwbMsXQVEALx_5 = value;
	}

	inline static int32_t get_offset_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_6() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___kilUCbTWeCfCdDqQSHFWpvHLrBl_6)); }
	inline int32_t get_kilUCbTWeCfCdDqQSHFWpvHLrBl_6() const { return ___kilUCbTWeCfCdDqQSHFWpvHLrBl_6; }
	inline int32_t* get_address_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_6() { return &___kilUCbTWeCfCdDqQSHFWpvHLrBl_6; }
	inline void set_kilUCbTWeCfCdDqQSHFWpvHLrBl_6(int32_t value)
	{
		___kilUCbTWeCfCdDqQSHFWpvHLrBl_6 = value;
	}

	inline static int32_t get_offset_of_WigXBlhRQEAAKnPZjEeeNdMahyT_7() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___WigXBlhRQEAAKnPZjEeeNdMahyT_7)); }
	inline CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * get_WigXBlhRQEAAKnPZjEeeNdMahyT_7() const { return ___WigXBlhRQEAAKnPZjEeeNdMahyT_7; }
	inline CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A ** get_address_of_WigXBlhRQEAAKnPZjEeeNdMahyT_7() { return &___WigXBlhRQEAAKnPZjEeeNdMahyT_7; }
	inline void set_WigXBlhRQEAAKnPZjEeeNdMahyT_7(CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * value)
	{
		___WigXBlhRQEAAKnPZjEeeNdMahyT_7 = value;
		Il2CppCodeGenWriteBarrier((&___WigXBlhRQEAAKnPZjEeeNdMahyT_7), value);
	}

	inline static int32_t get_offset_of_hqJogBNPbbjuJUDUhZmRhiFNzfx_8() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___hqJogBNPbbjuJUDUhZmRhiFNzfx_8)); }
	inline CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * get_hqJogBNPbbjuJUDUhZmRhiFNzfx_8() const { return ___hqJogBNPbbjuJUDUhZmRhiFNzfx_8; }
	inline CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A ** get_address_of_hqJogBNPbbjuJUDUhZmRhiFNzfx_8() { return &___hqJogBNPbbjuJUDUhZmRhiFNzfx_8; }
	inline void set_hqJogBNPbbjuJUDUhZmRhiFNzfx_8(CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * value)
	{
		___hqJogBNPbbjuJUDUhZmRhiFNzfx_8 = value;
		Il2CppCodeGenWriteBarrier((&___hqJogBNPbbjuJUDUhZmRhiFNzfx_8), value);
	}

	inline static int32_t get_offset_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_9() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_9)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_OFOCHvgjhTiUuiMSfLxKRWhcDSN_9() const { return ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_9; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_9() { return &___OFOCHvgjhTiUuiMSfLxKRWhcDSN_9; }
	inline void set_OFOCHvgjhTiUuiMSfLxKRWhcDSN_9(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___OFOCHvgjhTiUuiMSfLxKRWhcDSN_9 = value;
		Il2CppCodeGenWriteBarrier((&___OFOCHvgjhTiUuiMSfLxKRWhcDSN_9), value);
	}

	inline static int32_t get_offset_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_10() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___ISUlQumxWpBMGxJqFBsCyVmpAjn_10)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_ISUlQumxWpBMGxJqFBsCyVmpAjn_10() const { return ___ISUlQumxWpBMGxJqFBsCyVmpAjn_10; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_10() { return &___ISUlQumxWpBMGxJqFBsCyVmpAjn_10; }
	inline void set_ISUlQumxWpBMGxJqFBsCyVmpAjn_10(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___ISUlQumxWpBMGxJqFBsCyVmpAjn_10 = value;
		Il2CppCodeGenWriteBarrier((&___ISUlQumxWpBMGxJqFBsCyVmpAjn_10), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_11() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_11)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_11() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_11; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_11() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_11; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_11(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_11 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_12() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_12)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_12() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_12; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_12() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_12; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_12(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_12 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_14() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___CVShsOfomYgQbeMNmgtRkoanKAb_14)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_14() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_14; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_14() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_14; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_14(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_14 = value;
	}

	inline static int32_t get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_15() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___TLUFNqPThThRSpKtpbHMfphhHIql_15)); }
	inline bool get_TLUFNqPThThRSpKtpbHMfphhHIql_15() const { return ___TLUFNqPThThRSpKtpbHMfphhHIql_15; }
	inline bool* get_address_of_TLUFNqPThThRSpKtpbHMfphhHIql_15() { return &___TLUFNqPThThRSpKtpbHMfphhHIql_15; }
	inline void set_TLUFNqPThThRSpKtpbHMfphhHIql_15(bool value)
	{
		___TLUFNqPThThRSpKtpbHMfphhHIql_15 = value;
	}

	inline static int32_t get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_16() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_16)); }
	inline bool get_GsRgbEdsthhdHbQCaXWTDtLBvoPC_16() const { return ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_16; }
	inline bool* get_address_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_16() { return &___GsRgbEdsthhdHbQCaXWTDtLBvoPC_16; }
	inline void set_GsRgbEdsthhdHbQCaXWTDtLBvoPC_16(bool value)
	{
		___GsRgbEdsthhdHbQCaXWTDtLBvoPC_16 = value;
	}

	inline static int32_t get_offset_of_QljwEiMqTqTwBZqpaHrnqQjxQMB_17() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___QljwEiMqTqTwBZqpaHrnqQjxQMB_17)); }
	inline RuntimeObject* get_QljwEiMqTqTwBZqpaHrnqQjxQMB_17() const { return ___QljwEiMqTqTwBZqpaHrnqQjxQMB_17; }
	inline RuntimeObject** get_address_of_QljwEiMqTqTwBZqpaHrnqQjxQMB_17() { return &___QljwEiMqTqTwBZqpaHrnqQjxQMB_17; }
	inline void set_QljwEiMqTqTwBZqpaHrnqQjxQMB_17(RuntimeObject* value)
	{
		___QljwEiMqTqTwBZqpaHrnqQjxQMB_17 = value;
		Il2CppCodeGenWriteBarrier((&___QljwEiMqTqTwBZqpaHrnqQjxQMB_17), value);
	}

	inline static int32_t get_offset_of_onoURHsEjHZzeEEizHTuPVhquEU_18() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___onoURHsEjHZzeEEizHTuPVhquEU_18)); }
	inline int32_t get_onoURHsEjHZzeEEizHTuPVhquEU_18() const { return ___onoURHsEjHZzeEEizHTuPVhquEU_18; }
	inline int32_t* get_address_of_onoURHsEjHZzeEEizHTuPVhquEU_18() { return &___onoURHsEjHZzeEEizHTuPVhquEU_18; }
	inline void set_onoURHsEjHZzeEEizHTuPVhquEU_18(int32_t value)
	{
		___onoURHsEjHZzeEEizHTuPVhquEU_18 = value;
	}

	inline static int32_t get_offset_of_gAygEHCgtsXRJGTqpaFpqsnjGYMU_19() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___gAygEHCgtsXRJGTqpaFpqsnjGYMU_19)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_gAygEHCgtsXRJGTqpaFpqsnjGYMU_19() const { return ___gAygEHCgtsXRJGTqpaFpqsnjGYMU_19; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_gAygEHCgtsXRJGTqpaFpqsnjGYMU_19() { return &___gAygEHCgtsXRJGTqpaFpqsnjGYMU_19; }
	inline void set_gAygEHCgtsXRJGTqpaFpqsnjGYMU_19(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___gAygEHCgtsXRJGTqpaFpqsnjGYMU_19 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_20() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_20)); }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_20() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_20; }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_20() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_20; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_20(ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_20 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_20), value);
	}

	inline static int32_t get_offset_of_IQffKHKdJVcAMiqXIHpHcMMqBjig_21() { return static_cast<int32_t>(offsetof(OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462, ___IQffKHKdJVcAMiqXIHpHcMMqBjig_21)); }
	inline RuntimeObject* get_IQffKHKdJVcAMiqXIHpHcMMqBjig_21() const { return ___IQffKHKdJVcAMiqXIHpHcMMqBjig_21; }
	inline RuntimeObject** get_address_of_IQffKHKdJVcAMiqXIHpHcMMqBjig_21() { return &___IQffKHKdJVcAMiqXIHpHcMMqBjig_21; }
	inline void set_IQffKHKdJVcAMiqXIHpHcMMqBjig_21(RuntimeObject* value)
	{
		___IQffKHKdJVcAMiqXIHpHcMMqBjig_21 = value;
		Il2CppCodeGenWriteBarrier((&___IQffKHKdJVcAMiqXIHpHcMMqBjig_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OLMYBIZELJYMEUKKKZCAYUGCRCW_TDE41E2E34F5857309066828A1F7B7105A4A2A462_H
#ifndef RBWRZCMTLGDJYIJPVDDGXZYGGKH_T565B3A874A45DAAB42A171790B091D4CC5D3ADB9_H
#define RBWRZCMTLGDJYIJPVDDGXZYGGKH_T565B3A874A45DAAB42A171790B091D4CC5D3ADB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh
struct  RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ElementAssignmentConflictCheck Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::RXwTkvaioBOXLvHfDHkciCcOgzY
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___RXwTkvaioBOXLvHfDHkciCcOgzY_3;
	// Rewired.ElementAssignmentConflictCheck Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::CdduzXmnfyAqWtnFBTQwIrsXYmO
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___CdduzXmnfyAqWtnFBTQwIrsXYmO_4;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_5;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_8;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::TLUFNqPThThRSpKtpbHMfphhHIql
	bool ___TLUFNqPThThRSpKtpbHMfphhHIql_9;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::GsRgbEdsthhdHbQCaXWTDtLBvoPC
	bool ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10;
	// System.Collections.Generic.IList`1<Rewired.Player> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::VlEnMERJfcRXvYVAIopLjkseFixC
	RuntimeObject* ___VlEnMERJfcRXvYVAIopLjkseFixC_11;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::YKbcXcHeYDJiWnszacnrGIAhyNPK
	int32_t ___YKbcXcHeYDJiWnszacnrGIAhyNPK_12;
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::WcxnYCuzRGCnNdBRgHLUzseFzQZ
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___WcxnYCuzRGCnNdBRgHLUzseFzQZ_13;
	// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_14;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_RbWRzcmtlGDJyIjpVDDGXZYGgKh::IZhWSiuleSmkrYdnNJbtOVuwIiA
	RuntimeObject* ___IZhWSiuleSmkrYdnNJbtOVuwIiA_15;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_3() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___RXwTkvaioBOXLvHfDHkciCcOgzY_3)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_RXwTkvaioBOXLvHfDHkciCcOgzY_3() const { return ___RXwTkvaioBOXLvHfDHkciCcOgzY_3; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_RXwTkvaioBOXLvHfDHkciCcOgzY_3() { return &___RXwTkvaioBOXLvHfDHkciCcOgzY_3; }
	inline void set_RXwTkvaioBOXLvHfDHkciCcOgzY_3(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___RXwTkvaioBOXLvHfDHkciCcOgzY_3 = value;
	}

	inline static int32_t get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_4() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___CdduzXmnfyAqWtnFBTQwIrsXYmO_4)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_CdduzXmnfyAqWtnFBTQwIrsXYmO_4() const { return ___CdduzXmnfyAqWtnFBTQwIrsXYmO_4; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_4() { return &___CdduzXmnfyAqWtnFBTQwIrsXYmO_4; }
	inline void set_CdduzXmnfyAqWtnFBTQwIrsXYmO_4(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___CdduzXmnfyAqWtnFBTQwIrsXYmO_4 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_5() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_5)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_5() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_5; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_5() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_5; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_5(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_5 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_8() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___CVShsOfomYgQbeMNmgtRkoanKAb_8)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_8() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_8; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_8() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_8; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_8(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_8 = value;
	}

	inline static int32_t get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_9() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___TLUFNqPThThRSpKtpbHMfphhHIql_9)); }
	inline bool get_TLUFNqPThThRSpKtpbHMfphhHIql_9() const { return ___TLUFNqPThThRSpKtpbHMfphhHIql_9; }
	inline bool* get_address_of_TLUFNqPThThRSpKtpbHMfphhHIql_9() { return &___TLUFNqPThThRSpKtpbHMfphhHIql_9; }
	inline void set_TLUFNqPThThRSpKtpbHMfphhHIql_9(bool value)
	{
		___TLUFNqPThThRSpKtpbHMfphhHIql_9 = value;
	}

	inline static int32_t get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10)); }
	inline bool get_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10() const { return ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10; }
	inline bool* get_address_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10() { return &___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10; }
	inline void set_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10(bool value)
	{
		___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10 = value;
	}

	inline static int32_t get_offset_of_VlEnMERJfcRXvYVAIopLjkseFixC_11() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___VlEnMERJfcRXvYVAIopLjkseFixC_11)); }
	inline RuntimeObject* get_VlEnMERJfcRXvYVAIopLjkseFixC_11() const { return ___VlEnMERJfcRXvYVAIopLjkseFixC_11; }
	inline RuntimeObject** get_address_of_VlEnMERJfcRXvYVAIopLjkseFixC_11() { return &___VlEnMERJfcRXvYVAIopLjkseFixC_11; }
	inline void set_VlEnMERJfcRXvYVAIopLjkseFixC_11(RuntimeObject* value)
	{
		___VlEnMERJfcRXvYVAIopLjkseFixC_11 = value;
		Il2CppCodeGenWriteBarrier((&___VlEnMERJfcRXvYVAIopLjkseFixC_11), value);
	}

	inline static int32_t get_offset_of_YKbcXcHeYDJiWnszacnrGIAhyNPK_12() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___YKbcXcHeYDJiWnszacnrGIAhyNPK_12)); }
	inline int32_t get_YKbcXcHeYDJiWnszacnrGIAhyNPK_12() const { return ___YKbcXcHeYDJiWnszacnrGIAhyNPK_12; }
	inline int32_t* get_address_of_YKbcXcHeYDJiWnszacnrGIAhyNPK_12() { return &___YKbcXcHeYDJiWnszacnrGIAhyNPK_12; }
	inline void set_YKbcXcHeYDJiWnszacnrGIAhyNPK_12(int32_t value)
	{
		___YKbcXcHeYDJiWnszacnrGIAhyNPK_12 = value;
	}

	inline static int32_t get_offset_of_WcxnYCuzRGCnNdBRgHLUzseFzQZ_13() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___WcxnYCuzRGCnNdBRgHLUzseFzQZ_13)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_WcxnYCuzRGCnNdBRgHLUzseFzQZ_13() const { return ___WcxnYCuzRGCnNdBRgHLUzseFzQZ_13; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_WcxnYCuzRGCnNdBRgHLUzseFzQZ_13() { return &___WcxnYCuzRGCnNdBRgHLUzseFzQZ_13; }
	inline void set_WcxnYCuzRGCnNdBRgHLUzseFzQZ_13(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___WcxnYCuzRGCnNdBRgHLUzseFzQZ_13 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_14() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_14)); }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_14() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_14; }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_14() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_14; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_14(ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_14 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_14), value);
	}

	inline static int32_t get_offset_of_IZhWSiuleSmkrYdnNJbtOVuwIiA_15() { return static_cast<int32_t>(offsetof(RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9, ___IZhWSiuleSmkrYdnNJbtOVuwIiA_15)); }
	inline RuntimeObject* get_IZhWSiuleSmkrYdnNJbtOVuwIiA_15() const { return ___IZhWSiuleSmkrYdnNJbtOVuwIiA_15; }
	inline RuntimeObject** get_address_of_IZhWSiuleSmkrYdnNJbtOVuwIiA_15() { return &___IZhWSiuleSmkrYdnNJbtOVuwIiA_15; }
	inline void set_IZhWSiuleSmkrYdnNJbtOVuwIiA_15(RuntimeObject* value)
	{
		___IZhWSiuleSmkrYdnNJbtOVuwIiA_15 = value;
		Il2CppCodeGenWriteBarrier((&___IZhWSiuleSmkrYdnNJbtOVuwIiA_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RBWRZCMTLGDJYIJPVDDGXZYGGKH_T565B3A874A45DAAB42A171790B091D4CC5D3ADB9_H
#ifndef BDJXBCKTYFCYWVBXEOBWGPTTFVX_T3E6E52617792E5467E39ED3B73CF6B5212EFCD8C_H
#define BDJXBCKTYFCYWVBXEOBWGPTTFVX_T3E6E52617792E5467E39ED3B73CF6B5212EFCD8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx
struct  bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::TSxIVLfBUyELziEzwQrAMRmzGqrD
	int32_t ___TSxIVLfBUyELziEzwQrAMRmzGqrD_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::DoBlgWmOMuusUcBpsKccsmEFmgP
	int32_t ___DoBlgWmOMuusUcBpsKccsmEFmgP_4;
	// Rewired.MouseMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::kbjsbIbtGtRzFETStDXQFVzQesi
	MouseMap_t58C438E7108115F44D16041A902ECCBD89BD2008 * ___kbjsbIbtGtRzFETStDXQFVzQesi_5;
	// Rewired.MouseMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::CxTcbsASmwiVdEzXBlyvuVcnKaaw
	MouseMap_t58C438E7108115F44D16041A902ECCBD89BD2008 * ___CxTcbsASmwiVdEzXBlyvuVcnKaaw_6;
	// Rewired.ActionElementMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::OFOCHvgjhTiUuiMSfLxKRWhcDSN
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_7;
	// Rewired.ActionElementMap Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::ISUlQumxWpBMGxJqFBsCyVmpAjn
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___ISUlQumxWpBMGxJqFBsCyVmpAjn_8;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_9;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_10;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_12;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::TLUFNqPThThRSpKtpbHMfphhHIql
	bool ___TLUFNqPThThRSpKtpbHMfphhHIql_13;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::GsRgbEdsthhdHbQCaXWTDtLBvoPC
	bool ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_14;
	// System.Collections.Generic.IList`1<Rewired.Player> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::RmKYSukKWpayCjCNpIaEEefVQniq
	RuntimeObject* ___RmKYSukKWpayCjCNpIaEEefVQniq_15;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::nMKUaBUMYWExQhyhfnfRFjaEMpBA
	int32_t ___nMKUaBUMYWExQhyhfnfRFjaEMpBA_16;
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::rGzUQICrxYINoJVAtjlWiMXrTBF
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___rGzUQICrxYINoJVAtjlWiMXrTBF_17;
	// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_18;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_bdJxBckTyfcYwVBXEobWgPTTfVx::qZgxZgIGnppMbiffnIhqUypVcJb
	RuntimeObject* ___qZgxZgIGnppMbiffnIhqUypVcJb_19;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_3() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___TSxIVLfBUyELziEzwQrAMRmzGqrD_3)); }
	inline int32_t get_TSxIVLfBUyELziEzwQrAMRmzGqrD_3() const { return ___TSxIVLfBUyELziEzwQrAMRmzGqrD_3; }
	inline int32_t* get_address_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_3() { return &___TSxIVLfBUyELziEzwQrAMRmzGqrD_3; }
	inline void set_TSxIVLfBUyELziEzwQrAMRmzGqrD_3(int32_t value)
	{
		___TSxIVLfBUyELziEzwQrAMRmzGqrD_3 = value;
	}

	inline static int32_t get_offset_of_DoBlgWmOMuusUcBpsKccsmEFmgP_4() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___DoBlgWmOMuusUcBpsKccsmEFmgP_4)); }
	inline int32_t get_DoBlgWmOMuusUcBpsKccsmEFmgP_4() const { return ___DoBlgWmOMuusUcBpsKccsmEFmgP_4; }
	inline int32_t* get_address_of_DoBlgWmOMuusUcBpsKccsmEFmgP_4() { return &___DoBlgWmOMuusUcBpsKccsmEFmgP_4; }
	inline void set_DoBlgWmOMuusUcBpsKccsmEFmgP_4(int32_t value)
	{
		___DoBlgWmOMuusUcBpsKccsmEFmgP_4 = value;
	}

	inline static int32_t get_offset_of_kbjsbIbtGtRzFETStDXQFVzQesi_5() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___kbjsbIbtGtRzFETStDXQFVzQesi_5)); }
	inline MouseMap_t58C438E7108115F44D16041A902ECCBD89BD2008 * get_kbjsbIbtGtRzFETStDXQFVzQesi_5() const { return ___kbjsbIbtGtRzFETStDXQFVzQesi_5; }
	inline MouseMap_t58C438E7108115F44D16041A902ECCBD89BD2008 ** get_address_of_kbjsbIbtGtRzFETStDXQFVzQesi_5() { return &___kbjsbIbtGtRzFETStDXQFVzQesi_5; }
	inline void set_kbjsbIbtGtRzFETStDXQFVzQesi_5(MouseMap_t58C438E7108115F44D16041A902ECCBD89BD2008 * value)
	{
		___kbjsbIbtGtRzFETStDXQFVzQesi_5 = value;
		Il2CppCodeGenWriteBarrier((&___kbjsbIbtGtRzFETStDXQFVzQesi_5), value);
	}

	inline static int32_t get_offset_of_CxTcbsASmwiVdEzXBlyvuVcnKaaw_6() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___CxTcbsASmwiVdEzXBlyvuVcnKaaw_6)); }
	inline MouseMap_t58C438E7108115F44D16041A902ECCBD89BD2008 * get_CxTcbsASmwiVdEzXBlyvuVcnKaaw_6() const { return ___CxTcbsASmwiVdEzXBlyvuVcnKaaw_6; }
	inline MouseMap_t58C438E7108115F44D16041A902ECCBD89BD2008 ** get_address_of_CxTcbsASmwiVdEzXBlyvuVcnKaaw_6() { return &___CxTcbsASmwiVdEzXBlyvuVcnKaaw_6; }
	inline void set_CxTcbsASmwiVdEzXBlyvuVcnKaaw_6(MouseMap_t58C438E7108115F44D16041A902ECCBD89BD2008 * value)
	{
		___CxTcbsASmwiVdEzXBlyvuVcnKaaw_6 = value;
		Il2CppCodeGenWriteBarrier((&___CxTcbsASmwiVdEzXBlyvuVcnKaaw_6), value);
	}

	inline static int32_t get_offset_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_7() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_7)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_OFOCHvgjhTiUuiMSfLxKRWhcDSN_7() const { return ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_7; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_7() { return &___OFOCHvgjhTiUuiMSfLxKRWhcDSN_7; }
	inline void set_OFOCHvgjhTiUuiMSfLxKRWhcDSN_7(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___OFOCHvgjhTiUuiMSfLxKRWhcDSN_7 = value;
		Il2CppCodeGenWriteBarrier((&___OFOCHvgjhTiUuiMSfLxKRWhcDSN_7), value);
	}

	inline static int32_t get_offset_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_8() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___ISUlQumxWpBMGxJqFBsCyVmpAjn_8)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_ISUlQumxWpBMGxJqFBsCyVmpAjn_8() const { return ___ISUlQumxWpBMGxJqFBsCyVmpAjn_8; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_8() { return &___ISUlQumxWpBMGxJqFBsCyVmpAjn_8; }
	inline void set_ISUlQumxWpBMGxJqFBsCyVmpAjn_8(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___ISUlQumxWpBMGxJqFBsCyVmpAjn_8 = value;
		Il2CppCodeGenWriteBarrier((&___ISUlQumxWpBMGxJqFBsCyVmpAjn_8), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_9() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_9)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_9() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_9; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_9() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_9; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_9(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_9 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_10() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_10)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_10() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_10; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_10() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_10; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_10(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_10 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_12() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___CVShsOfomYgQbeMNmgtRkoanKAb_12)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_12() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_12; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_12() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_12; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_12(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_12 = value;
	}

	inline static int32_t get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_13() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___TLUFNqPThThRSpKtpbHMfphhHIql_13)); }
	inline bool get_TLUFNqPThThRSpKtpbHMfphhHIql_13() const { return ___TLUFNqPThThRSpKtpbHMfphhHIql_13; }
	inline bool* get_address_of_TLUFNqPThThRSpKtpbHMfphhHIql_13() { return &___TLUFNqPThThRSpKtpbHMfphhHIql_13; }
	inline void set_TLUFNqPThThRSpKtpbHMfphhHIql_13(bool value)
	{
		___TLUFNqPThThRSpKtpbHMfphhHIql_13 = value;
	}

	inline static int32_t get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_14() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_14)); }
	inline bool get_GsRgbEdsthhdHbQCaXWTDtLBvoPC_14() const { return ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_14; }
	inline bool* get_address_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_14() { return &___GsRgbEdsthhdHbQCaXWTDtLBvoPC_14; }
	inline void set_GsRgbEdsthhdHbQCaXWTDtLBvoPC_14(bool value)
	{
		___GsRgbEdsthhdHbQCaXWTDtLBvoPC_14 = value;
	}

	inline static int32_t get_offset_of_RmKYSukKWpayCjCNpIaEEefVQniq_15() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___RmKYSukKWpayCjCNpIaEEefVQniq_15)); }
	inline RuntimeObject* get_RmKYSukKWpayCjCNpIaEEefVQniq_15() const { return ___RmKYSukKWpayCjCNpIaEEefVQniq_15; }
	inline RuntimeObject** get_address_of_RmKYSukKWpayCjCNpIaEEefVQniq_15() { return &___RmKYSukKWpayCjCNpIaEEefVQniq_15; }
	inline void set_RmKYSukKWpayCjCNpIaEEefVQniq_15(RuntimeObject* value)
	{
		___RmKYSukKWpayCjCNpIaEEefVQniq_15 = value;
		Il2CppCodeGenWriteBarrier((&___RmKYSukKWpayCjCNpIaEEefVQniq_15), value);
	}

	inline static int32_t get_offset_of_nMKUaBUMYWExQhyhfnfRFjaEMpBA_16() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___nMKUaBUMYWExQhyhfnfRFjaEMpBA_16)); }
	inline int32_t get_nMKUaBUMYWExQhyhfnfRFjaEMpBA_16() const { return ___nMKUaBUMYWExQhyhfnfRFjaEMpBA_16; }
	inline int32_t* get_address_of_nMKUaBUMYWExQhyhfnfRFjaEMpBA_16() { return &___nMKUaBUMYWExQhyhfnfRFjaEMpBA_16; }
	inline void set_nMKUaBUMYWExQhyhfnfRFjaEMpBA_16(int32_t value)
	{
		___nMKUaBUMYWExQhyhfnfRFjaEMpBA_16 = value;
	}

	inline static int32_t get_offset_of_rGzUQICrxYINoJVAtjlWiMXrTBF_17() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___rGzUQICrxYINoJVAtjlWiMXrTBF_17)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_rGzUQICrxYINoJVAtjlWiMXrTBF_17() const { return ___rGzUQICrxYINoJVAtjlWiMXrTBF_17; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_rGzUQICrxYINoJVAtjlWiMXrTBF_17() { return &___rGzUQICrxYINoJVAtjlWiMXrTBF_17; }
	inline void set_rGzUQICrxYINoJVAtjlWiMXrTBF_17(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___rGzUQICrxYINoJVAtjlWiMXrTBF_17 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_18() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_18)); }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_18() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_18; }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_18() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_18; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_18(ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_18 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_18), value);
	}

	inline static int32_t get_offset_of_qZgxZgIGnppMbiffnIhqUypVcJb_19() { return static_cast<int32_t>(offsetof(bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C, ___qZgxZgIGnppMbiffnIhqUypVcJb_19)); }
	inline RuntimeObject* get_qZgxZgIGnppMbiffnIhqUypVcJb_19() const { return ___qZgxZgIGnppMbiffnIhqUypVcJb_19; }
	inline RuntimeObject** get_address_of_qZgxZgIGnppMbiffnIhqUypVcJb_19() { return &___qZgxZgIGnppMbiffnIhqUypVcJb_19; }
	inline void set_qZgxZgIGnppMbiffnIhqUypVcJb_19(RuntimeObject* value)
	{
		___qZgxZgIGnppMbiffnIhqUypVcJb_19 = value;
		Il2CppCodeGenWriteBarrier((&___qZgxZgIGnppMbiffnIhqUypVcJb_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BDJXBCKTYFCYWVBXEOBWGPTTFVX_T3E6E52617792E5467E39ED3B73CF6B5212EFCD8C_H
#ifndef OKWNHVVLPXIPUZCPVFLDMOWYQTE_TB3CDABCD106ED26DC61730F4F5C8397E78F38E47_H
#define OKWNHVVLPXIPUZCPVFLDMOWYQTE_TB3CDABCD106ED26DC61730F4F5C8397E78F38E47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte
struct  okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ElementAssignmentConflictCheck Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::RXwTkvaioBOXLvHfDHkciCcOgzY
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___RXwTkvaioBOXLvHfDHkciCcOgzY_3;
	// Rewired.ElementAssignmentConflictCheck Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::CdduzXmnfyAqWtnFBTQwIrsXYmO
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___CdduzXmnfyAqWtnFBTQwIrsXYmO_4;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_5;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_8;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::TLUFNqPThThRSpKtpbHMfphhHIql
	bool ___TLUFNqPThThRSpKtpbHMfphhHIql_9;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::GsRgbEdsthhdHbQCaXWTDtLBvoPC
	bool ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10;
	// System.Collections.Generic.IList`1<Rewired.Player> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::zsqPQQkoIaugWTqooEEkmuYmBXy
	RuntimeObject* ___zsqPQQkoIaugWTqooEEkmuYmBXy_11;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::qBSWTwFKBFQQxNPvIFeSsWlfCdL
	int32_t ___qBSWTwFKBFQQxNPvIFeSsWlfCdL_12;
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::naVxvbejmtjAMGXkyXWPQlkjMKFe
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___naVxvbejmtjAMGXkyXWPQlkjMKFe_13;
	// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_14;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_okwnHvvLpxipuZcpVfLdmOWyqte::AkiDYCiMmPsWOImxmReybvqfaGPI
	RuntimeObject* ___AkiDYCiMmPsWOImxmReybvqfaGPI_15;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_3() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___RXwTkvaioBOXLvHfDHkciCcOgzY_3)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_RXwTkvaioBOXLvHfDHkciCcOgzY_3() const { return ___RXwTkvaioBOXLvHfDHkciCcOgzY_3; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_RXwTkvaioBOXLvHfDHkciCcOgzY_3() { return &___RXwTkvaioBOXLvHfDHkciCcOgzY_3; }
	inline void set_RXwTkvaioBOXLvHfDHkciCcOgzY_3(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___RXwTkvaioBOXLvHfDHkciCcOgzY_3 = value;
	}

	inline static int32_t get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_4() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___CdduzXmnfyAqWtnFBTQwIrsXYmO_4)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_CdduzXmnfyAqWtnFBTQwIrsXYmO_4() const { return ___CdduzXmnfyAqWtnFBTQwIrsXYmO_4; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_4() { return &___CdduzXmnfyAqWtnFBTQwIrsXYmO_4; }
	inline void set_CdduzXmnfyAqWtnFBTQwIrsXYmO_4(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___CdduzXmnfyAqWtnFBTQwIrsXYmO_4 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_5() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_5)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_5() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_5; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_5() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_5; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_5(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_5 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_8() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___CVShsOfomYgQbeMNmgtRkoanKAb_8)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_8() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_8; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_8() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_8; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_8(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_8 = value;
	}

	inline static int32_t get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_9() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___TLUFNqPThThRSpKtpbHMfphhHIql_9)); }
	inline bool get_TLUFNqPThThRSpKtpbHMfphhHIql_9() const { return ___TLUFNqPThThRSpKtpbHMfphhHIql_9; }
	inline bool* get_address_of_TLUFNqPThThRSpKtpbHMfphhHIql_9() { return &___TLUFNqPThThRSpKtpbHMfphhHIql_9; }
	inline void set_TLUFNqPThThRSpKtpbHMfphhHIql_9(bool value)
	{
		___TLUFNqPThThRSpKtpbHMfphhHIql_9 = value;
	}

	inline static int32_t get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10)); }
	inline bool get_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10() const { return ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10; }
	inline bool* get_address_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10() { return &___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10; }
	inline void set_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10(bool value)
	{
		___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10 = value;
	}

	inline static int32_t get_offset_of_zsqPQQkoIaugWTqooEEkmuYmBXy_11() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___zsqPQQkoIaugWTqooEEkmuYmBXy_11)); }
	inline RuntimeObject* get_zsqPQQkoIaugWTqooEEkmuYmBXy_11() const { return ___zsqPQQkoIaugWTqooEEkmuYmBXy_11; }
	inline RuntimeObject** get_address_of_zsqPQQkoIaugWTqooEEkmuYmBXy_11() { return &___zsqPQQkoIaugWTqooEEkmuYmBXy_11; }
	inline void set_zsqPQQkoIaugWTqooEEkmuYmBXy_11(RuntimeObject* value)
	{
		___zsqPQQkoIaugWTqooEEkmuYmBXy_11 = value;
		Il2CppCodeGenWriteBarrier((&___zsqPQQkoIaugWTqooEEkmuYmBXy_11), value);
	}

	inline static int32_t get_offset_of_qBSWTwFKBFQQxNPvIFeSsWlfCdL_12() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___qBSWTwFKBFQQxNPvIFeSsWlfCdL_12)); }
	inline int32_t get_qBSWTwFKBFQQxNPvIFeSsWlfCdL_12() const { return ___qBSWTwFKBFQQxNPvIFeSsWlfCdL_12; }
	inline int32_t* get_address_of_qBSWTwFKBFQQxNPvIFeSsWlfCdL_12() { return &___qBSWTwFKBFQQxNPvIFeSsWlfCdL_12; }
	inline void set_qBSWTwFKBFQQxNPvIFeSsWlfCdL_12(int32_t value)
	{
		___qBSWTwFKBFQQxNPvIFeSsWlfCdL_12 = value;
	}

	inline static int32_t get_offset_of_naVxvbejmtjAMGXkyXWPQlkjMKFe_13() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___naVxvbejmtjAMGXkyXWPQlkjMKFe_13)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_naVxvbejmtjAMGXkyXWPQlkjMKFe_13() const { return ___naVxvbejmtjAMGXkyXWPQlkjMKFe_13; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_naVxvbejmtjAMGXkyXWPQlkjMKFe_13() { return &___naVxvbejmtjAMGXkyXWPQlkjMKFe_13; }
	inline void set_naVxvbejmtjAMGXkyXWPQlkjMKFe_13(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___naVxvbejmtjAMGXkyXWPQlkjMKFe_13 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_14() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_14)); }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_14() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_14; }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_14() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_14; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_14(ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_14 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_14), value);
	}

	inline static int32_t get_offset_of_AkiDYCiMmPsWOImxmReybvqfaGPI_15() { return static_cast<int32_t>(offsetof(okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47, ___AkiDYCiMmPsWOImxmReybvqfaGPI_15)); }
	inline RuntimeObject* get_AkiDYCiMmPsWOImxmReybvqfaGPI_15() const { return ___AkiDYCiMmPsWOImxmReybvqfaGPI_15; }
	inline RuntimeObject** get_address_of_AkiDYCiMmPsWOImxmReybvqfaGPI_15() { return &___AkiDYCiMmPsWOImxmReybvqfaGPI_15; }
	inline void set_AkiDYCiMmPsWOImxmReybvqfaGPI_15(RuntimeObject* value)
	{
		___AkiDYCiMmPsWOImxmReybvqfaGPI_15 = value;
		Il2CppCodeGenWriteBarrier((&___AkiDYCiMmPsWOImxmReybvqfaGPI_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OKWNHVVLPXIPUZCPVFLDMOWYQTE_TB3CDABCD106ED26DC61730F4F5C8397E78F38E47_H
#ifndef QXHFQQBGEYMGFTJYIXRUKCZHRECP_TB540CEBBF22A56BA958AAD6A57B134835AAACFA1_H
#define QXHFQQBGEYMGFTJYIXRUKCZHRECP_TB540CEBBF22A56BA958AAD6A57B134835AAACFA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP
struct  qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ElementAssignmentConflictCheck Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::RXwTkvaioBOXLvHfDHkciCcOgzY
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___RXwTkvaioBOXLvHfDHkciCcOgzY_3;
	// Rewired.ElementAssignmentConflictCheck Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::CdduzXmnfyAqWtnFBTQwIrsXYmO
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___CdduzXmnfyAqWtnFBTQwIrsXYmO_4;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_5;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_8;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::TLUFNqPThThRSpKtpbHMfphhHIql
	bool ___TLUFNqPThThRSpKtpbHMfphhHIql_9;
	// System.Boolean Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::GsRgbEdsthhdHbQCaXWTDtLBvoPC
	bool ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10;
	// System.Collections.Generic.IList`1<Rewired.Player> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::pjuakmhoKxwCeKjqXCixQCshBsG
	RuntimeObject* ___pjuakmhoKxwCeKjqXCixQCshBsG_11;
	// System.Int32 Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::JRVxexbSpXMgYerJjSbfICkccTlH
	int32_t ___JRVxexbSpXMgYerJjSbfICkccTlH_12;
	// Rewired.ElementAssignmentConflictInfo Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::lXMTDyjNZuQWTiaDRjjKZxjgLCm
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___lXMTDyjNZuQWTiaDRjjKZxjgLCm_13;
	// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_14;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.ReInput_ControllerHelper_ConflictCheckingHelper_qXhFqqBgeymgFtjyIXRuKCZHrECP::KpQnLzSuaGpebiUgmOuCQoazIQAK
	RuntimeObject* ___KpQnLzSuaGpebiUgmOuCQoazIQAK_15;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_3() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___RXwTkvaioBOXLvHfDHkciCcOgzY_3)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_RXwTkvaioBOXLvHfDHkciCcOgzY_3() const { return ___RXwTkvaioBOXLvHfDHkciCcOgzY_3; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_RXwTkvaioBOXLvHfDHkciCcOgzY_3() { return &___RXwTkvaioBOXLvHfDHkciCcOgzY_3; }
	inline void set_RXwTkvaioBOXLvHfDHkciCcOgzY_3(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___RXwTkvaioBOXLvHfDHkciCcOgzY_3 = value;
	}

	inline static int32_t get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_4() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___CdduzXmnfyAqWtnFBTQwIrsXYmO_4)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_CdduzXmnfyAqWtnFBTQwIrsXYmO_4() const { return ___CdduzXmnfyAqWtnFBTQwIrsXYmO_4; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_4() { return &___CdduzXmnfyAqWtnFBTQwIrsXYmO_4; }
	inline void set_CdduzXmnfyAqWtnFBTQwIrsXYmO_4(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___CdduzXmnfyAqWtnFBTQwIrsXYmO_4 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_5() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_5)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_5() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_5; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_5() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_5; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_5(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_5 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_6 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_8() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___CVShsOfomYgQbeMNmgtRkoanKAb_8)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_8() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_8; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_8() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_8; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_8(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_8 = value;
	}

	inline static int32_t get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_9() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___TLUFNqPThThRSpKtpbHMfphhHIql_9)); }
	inline bool get_TLUFNqPThThRSpKtpbHMfphhHIql_9() const { return ___TLUFNqPThThRSpKtpbHMfphhHIql_9; }
	inline bool* get_address_of_TLUFNqPThThRSpKtpbHMfphhHIql_9() { return &___TLUFNqPThThRSpKtpbHMfphhHIql_9; }
	inline void set_TLUFNqPThThRSpKtpbHMfphhHIql_9(bool value)
	{
		___TLUFNqPThThRSpKtpbHMfphhHIql_9 = value;
	}

	inline static int32_t get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10)); }
	inline bool get_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10() const { return ___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10; }
	inline bool* get_address_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10() { return &___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10; }
	inline void set_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10(bool value)
	{
		___GsRgbEdsthhdHbQCaXWTDtLBvoPC_10 = value;
	}

	inline static int32_t get_offset_of_pjuakmhoKxwCeKjqXCixQCshBsG_11() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___pjuakmhoKxwCeKjqXCixQCshBsG_11)); }
	inline RuntimeObject* get_pjuakmhoKxwCeKjqXCixQCshBsG_11() const { return ___pjuakmhoKxwCeKjqXCixQCshBsG_11; }
	inline RuntimeObject** get_address_of_pjuakmhoKxwCeKjqXCixQCshBsG_11() { return &___pjuakmhoKxwCeKjqXCixQCshBsG_11; }
	inline void set_pjuakmhoKxwCeKjqXCixQCshBsG_11(RuntimeObject* value)
	{
		___pjuakmhoKxwCeKjqXCixQCshBsG_11 = value;
		Il2CppCodeGenWriteBarrier((&___pjuakmhoKxwCeKjqXCixQCshBsG_11), value);
	}

	inline static int32_t get_offset_of_JRVxexbSpXMgYerJjSbfICkccTlH_12() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___JRVxexbSpXMgYerJjSbfICkccTlH_12)); }
	inline int32_t get_JRVxexbSpXMgYerJjSbfICkccTlH_12() const { return ___JRVxexbSpXMgYerJjSbfICkccTlH_12; }
	inline int32_t* get_address_of_JRVxexbSpXMgYerJjSbfICkccTlH_12() { return &___JRVxexbSpXMgYerJjSbfICkccTlH_12; }
	inline void set_JRVxexbSpXMgYerJjSbfICkccTlH_12(int32_t value)
	{
		___JRVxexbSpXMgYerJjSbfICkccTlH_12 = value;
	}

	inline static int32_t get_offset_of_lXMTDyjNZuQWTiaDRjjKZxjgLCm_13() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___lXMTDyjNZuQWTiaDRjjKZxjgLCm_13)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_lXMTDyjNZuQWTiaDRjjKZxjgLCm_13() const { return ___lXMTDyjNZuQWTiaDRjjKZxjgLCm_13; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_lXMTDyjNZuQWTiaDRjjKZxjgLCm_13() { return &___lXMTDyjNZuQWTiaDRjjKZxjgLCm_13; }
	inline void set_lXMTDyjNZuQWTiaDRjjKZxjgLCm_13(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___lXMTDyjNZuQWTiaDRjjKZxjgLCm_13 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_14() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_14)); }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_14() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_14; }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_14() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_14; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_14(ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_14 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_14), value);
	}

	inline static int32_t get_offset_of_KpQnLzSuaGpebiUgmOuCQoazIQAK_15() { return static_cast<int32_t>(offsetof(qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1, ___KpQnLzSuaGpebiUgmOuCQoazIQAK_15)); }
	inline RuntimeObject* get_KpQnLzSuaGpebiUgmOuCQoazIQAK_15() const { return ___KpQnLzSuaGpebiUgmOuCQoazIQAK_15; }
	inline RuntimeObject** get_address_of_KpQnLzSuaGpebiUgmOuCQoazIQAK_15() { return &___KpQnLzSuaGpebiUgmOuCQoazIQAK_15; }
	inline void set_KpQnLzSuaGpebiUgmOuCQoazIQAK_15(RuntimeObject* value)
	{
		___KpQnLzSuaGpebiUgmOuCQoazIQAK_15 = value;
		Il2CppCodeGenWriteBarrier((&___KpQnLzSuaGpebiUgmOuCQoazIQAK_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QXHFQQBGEYMGFTJYIXRUKCZHRECP_TB540CEBBF22A56BA958AAD6A57B134835AAACFA1_H
#ifndef GUBWLQHGSSHLFYRPAOWCDKSFLVW_TF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3_H
#define GUBWLQHGSSHLFYRPAOWCDKSFLVW_TF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_GubwlqhGsshLfyrPaOWCdkSFlVw
struct  GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_GubwlqhGsshLfyrPaOWCdkSFlVw::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_GubwlqhGsshLfyrPaOWCdkSFlVw::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_GubwlqhGsshLfyrPaOWCdkSFlVw::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Collections.Generic.IList`1<Rewired.CustomController> Rewired.ReInput_ControllerHelper_PollingHelper_GubwlqhGsshLfyrPaOWCdkSFlVw::ecVfKCwjYJvDOdgndXSGWqsTXPr
	RuntimeObject* ___ecVfKCwjYJvDOdgndXSGWqsTXPr_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_GubwlqhGsshLfyrPaOWCdkSFlVw::GAbAVVCqauwUxkAiKlsNvuchSwW
	int32_t ___GAbAVVCqauwUxkAiKlsNvuchSwW_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_GubwlqhGsshLfyrPaOWCdkSFlVw::zIFLSDHAPprCQOhpfMbxFRZcnok
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___zIFLSDHAPprCQOhpfMbxFRZcnok_5;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_GubwlqhGsshLfyrPaOWCdkSFlVw::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_GubwlqhGsshLfyrPaOWCdkSFlVw::uwGhQmbUmcbCeCzOmRDTWrmDVwD
	RuntimeObject* ___uwGhQmbUmcbCeCzOmRDTWrmDVwD_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_ecVfKCwjYJvDOdgndXSGWqsTXPr_3() { return static_cast<int32_t>(offsetof(GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3, ___ecVfKCwjYJvDOdgndXSGWqsTXPr_3)); }
	inline RuntimeObject* get_ecVfKCwjYJvDOdgndXSGWqsTXPr_3() const { return ___ecVfKCwjYJvDOdgndXSGWqsTXPr_3; }
	inline RuntimeObject** get_address_of_ecVfKCwjYJvDOdgndXSGWqsTXPr_3() { return &___ecVfKCwjYJvDOdgndXSGWqsTXPr_3; }
	inline void set_ecVfKCwjYJvDOdgndXSGWqsTXPr_3(RuntimeObject* value)
	{
		___ecVfKCwjYJvDOdgndXSGWqsTXPr_3 = value;
		Il2CppCodeGenWriteBarrier((&___ecVfKCwjYJvDOdgndXSGWqsTXPr_3), value);
	}

	inline static int32_t get_offset_of_GAbAVVCqauwUxkAiKlsNvuchSwW_4() { return static_cast<int32_t>(offsetof(GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3, ___GAbAVVCqauwUxkAiKlsNvuchSwW_4)); }
	inline int32_t get_GAbAVVCqauwUxkAiKlsNvuchSwW_4() const { return ___GAbAVVCqauwUxkAiKlsNvuchSwW_4; }
	inline int32_t* get_address_of_GAbAVVCqauwUxkAiKlsNvuchSwW_4() { return &___GAbAVVCqauwUxkAiKlsNvuchSwW_4; }
	inline void set_GAbAVVCqauwUxkAiKlsNvuchSwW_4(int32_t value)
	{
		___GAbAVVCqauwUxkAiKlsNvuchSwW_4 = value;
	}

	inline static int32_t get_offset_of_zIFLSDHAPprCQOhpfMbxFRZcnok_5() { return static_cast<int32_t>(offsetof(GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3, ___zIFLSDHAPprCQOhpfMbxFRZcnok_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_zIFLSDHAPprCQOhpfMbxFRZcnok_5() const { return ___zIFLSDHAPprCQOhpfMbxFRZcnok_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_zIFLSDHAPprCQOhpfMbxFRZcnok_5() { return &___zIFLSDHAPprCQOhpfMbxFRZcnok_5; }
	inline void set_zIFLSDHAPprCQOhpfMbxFRZcnok_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___zIFLSDHAPprCQOhpfMbxFRZcnok_5 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return static_cast<int32_t>(offsetof(GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_6 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_6), value);
	}

	inline static int32_t get_offset_of_uwGhQmbUmcbCeCzOmRDTWrmDVwD_7() { return static_cast<int32_t>(offsetof(GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3, ___uwGhQmbUmcbCeCzOmRDTWrmDVwD_7)); }
	inline RuntimeObject* get_uwGhQmbUmcbCeCzOmRDTWrmDVwD_7() const { return ___uwGhQmbUmcbCeCzOmRDTWrmDVwD_7; }
	inline RuntimeObject** get_address_of_uwGhQmbUmcbCeCzOmRDTWrmDVwD_7() { return &___uwGhQmbUmcbCeCzOmRDTWrmDVwD_7; }
	inline void set_uwGhQmbUmcbCeCzOmRDTWrmDVwD_7(RuntimeObject* value)
	{
		___uwGhQmbUmcbCeCzOmRDTWrmDVwD_7 = value;
		Il2CppCodeGenWriteBarrier((&___uwGhQmbUmcbCeCzOmRDTWrmDVwD_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUBWLQHGSSHLFYRPAOWCDKSFLVW_TF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3_H
#ifndef WAZHSCHCEGFXFOCKZTNZKMZWEVO_TC06959DFE653AA7C842463E76DD8F9FBDDB87FA9_H
#define WAZHSCHCEGFXFOCKZTNZKMZWEVO_TC06959DFE653AA7C842463E76DD8F9FBDDB87FA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_WaZHscHCegfXfOCkztNzKmzwEVO
struct  WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_WaZHscHCegfXfOCkztNzKmzwEVO::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_WaZHscHCegfXfOCkztNzKmzwEVO::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_WaZHscHCegfXfOCkztNzKmzwEVO::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Collections.Generic.IList`1<Rewired.CustomController> Rewired.ReInput_ControllerHelper_PollingHelper_WaZHscHCegfXfOCkztNzKmzwEVO::WbMHeDGCeTqztrpHHhmZBHAgUEYy
	RuntimeObject* ___WbMHeDGCeTqztrpHHhmZBHAgUEYy_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_WaZHscHCegfXfOCkztNzKmzwEVO::ZpXhDjiTNyrLJrMWlIiVHjURjIm
	int32_t ___ZpXhDjiTNyrLJrMWlIiVHjURjIm_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_WaZHscHCegfXfOCkztNzKmzwEVO::hMnvSQnHAFQYRoaIAGTtBjKBCsp
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___hMnvSQnHAFQYRoaIAGTtBjKBCsp_5;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_WaZHscHCegfXfOCkztNzKmzwEVO::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_WaZHscHCegfXfOCkztNzKmzwEVO::LnweSeVDxIunxbncolytFHkEVSK
	RuntimeObject* ___LnweSeVDxIunxbncolytFHkEVSK_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_WbMHeDGCeTqztrpHHhmZBHAgUEYy_3() { return static_cast<int32_t>(offsetof(WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9, ___WbMHeDGCeTqztrpHHhmZBHAgUEYy_3)); }
	inline RuntimeObject* get_WbMHeDGCeTqztrpHHhmZBHAgUEYy_3() const { return ___WbMHeDGCeTqztrpHHhmZBHAgUEYy_3; }
	inline RuntimeObject** get_address_of_WbMHeDGCeTqztrpHHhmZBHAgUEYy_3() { return &___WbMHeDGCeTqztrpHHhmZBHAgUEYy_3; }
	inline void set_WbMHeDGCeTqztrpHHhmZBHAgUEYy_3(RuntimeObject* value)
	{
		___WbMHeDGCeTqztrpHHhmZBHAgUEYy_3 = value;
		Il2CppCodeGenWriteBarrier((&___WbMHeDGCeTqztrpHHhmZBHAgUEYy_3), value);
	}

	inline static int32_t get_offset_of_ZpXhDjiTNyrLJrMWlIiVHjURjIm_4() { return static_cast<int32_t>(offsetof(WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9, ___ZpXhDjiTNyrLJrMWlIiVHjURjIm_4)); }
	inline int32_t get_ZpXhDjiTNyrLJrMWlIiVHjURjIm_4() const { return ___ZpXhDjiTNyrLJrMWlIiVHjURjIm_4; }
	inline int32_t* get_address_of_ZpXhDjiTNyrLJrMWlIiVHjURjIm_4() { return &___ZpXhDjiTNyrLJrMWlIiVHjURjIm_4; }
	inline void set_ZpXhDjiTNyrLJrMWlIiVHjURjIm_4(int32_t value)
	{
		___ZpXhDjiTNyrLJrMWlIiVHjURjIm_4 = value;
	}

	inline static int32_t get_offset_of_hMnvSQnHAFQYRoaIAGTtBjKBCsp_5() { return static_cast<int32_t>(offsetof(WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9, ___hMnvSQnHAFQYRoaIAGTtBjKBCsp_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_hMnvSQnHAFQYRoaIAGTtBjKBCsp_5() const { return ___hMnvSQnHAFQYRoaIAGTtBjKBCsp_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_hMnvSQnHAFQYRoaIAGTtBjKBCsp_5() { return &___hMnvSQnHAFQYRoaIAGTtBjKBCsp_5; }
	inline void set_hMnvSQnHAFQYRoaIAGTtBjKBCsp_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___hMnvSQnHAFQYRoaIAGTtBjKBCsp_5 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return static_cast<int32_t>(offsetof(WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_6 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_6), value);
	}

	inline static int32_t get_offset_of_LnweSeVDxIunxbncolytFHkEVSK_7() { return static_cast<int32_t>(offsetof(WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9, ___LnweSeVDxIunxbncolytFHkEVSK_7)); }
	inline RuntimeObject* get_LnweSeVDxIunxbncolytFHkEVSK_7() const { return ___LnweSeVDxIunxbncolytFHkEVSK_7; }
	inline RuntimeObject** get_address_of_LnweSeVDxIunxbncolytFHkEVSK_7() { return &___LnweSeVDxIunxbncolytFHkEVSK_7; }
	inline void set_LnweSeVDxIunxbncolytFHkEVSK_7(RuntimeObject* value)
	{
		___LnweSeVDxIunxbncolytFHkEVSK_7 = value;
		Il2CppCodeGenWriteBarrier((&___LnweSeVDxIunxbncolytFHkEVSK_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAZHSCHCEGFXFOCKZTNZKMZWEVO_TC06959DFE653AA7C842463E76DD8F9FBDDB87FA9_H
#ifndef GXSGPDJSDVWAGAKGLDJJVBGWMPD_TE122FF1DDF57B2885BCB67A5CAB321227F33682A_H
#define GXSGPDJSDVWAGAKGLDJJVBGWMPD_TE122FF1DDF57B2885BCB67A5CAB321227F33682A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_gxSGPdJSDVWAGakgLdJJvBGWmPD
struct  gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_gxSGPdJSDVWAGakgLdJJvBGWmPD::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_gxSGPdJSDVWAGakgLdJJvBGWmPD::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_gxSGPdJSDVWAGakgLdJJvBGWmPD::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Collections.Generic.IList`1<Rewired.CustomController> Rewired.ReInput_ControllerHelper_PollingHelper_gxSGPdJSDVWAGakgLdJJvBGWmPD::JrRgkaHxoLRVgtmeuYKrRIFtXnVW
	RuntimeObject* ___JrRgkaHxoLRVgtmeuYKrRIFtXnVW_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_gxSGPdJSDVWAGakgLdJJvBGWmPD::KBqIMUDUgsWqaoVxTgcCXnsNhPd
	int32_t ___KBqIMUDUgsWqaoVxTgcCXnsNhPd_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_gxSGPdJSDVWAGakgLdJJvBGWmPD::UMEgPuPnmRmOfcQAubdAHRnEzyYI
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___UMEgPuPnmRmOfcQAubdAHRnEzyYI_5;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_gxSGPdJSDVWAGakgLdJJvBGWmPD::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_gxSGPdJSDVWAGakgLdJJvBGWmPD::gygTNkRxZGmHjziBespiNVAKnFL
	RuntimeObject* ___gygTNkRxZGmHjziBespiNVAKnFL_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_JrRgkaHxoLRVgtmeuYKrRIFtXnVW_3() { return static_cast<int32_t>(offsetof(gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A, ___JrRgkaHxoLRVgtmeuYKrRIFtXnVW_3)); }
	inline RuntimeObject* get_JrRgkaHxoLRVgtmeuYKrRIFtXnVW_3() const { return ___JrRgkaHxoLRVgtmeuYKrRIFtXnVW_3; }
	inline RuntimeObject** get_address_of_JrRgkaHxoLRVgtmeuYKrRIFtXnVW_3() { return &___JrRgkaHxoLRVgtmeuYKrRIFtXnVW_3; }
	inline void set_JrRgkaHxoLRVgtmeuYKrRIFtXnVW_3(RuntimeObject* value)
	{
		___JrRgkaHxoLRVgtmeuYKrRIFtXnVW_3 = value;
		Il2CppCodeGenWriteBarrier((&___JrRgkaHxoLRVgtmeuYKrRIFtXnVW_3), value);
	}

	inline static int32_t get_offset_of_KBqIMUDUgsWqaoVxTgcCXnsNhPd_4() { return static_cast<int32_t>(offsetof(gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A, ___KBqIMUDUgsWqaoVxTgcCXnsNhPd_4)); }
	inline int32_t get_KBqIMUDUgsWqaoVxTgcCXnsNhPd_4() const { return ___KBqIMUDUgsWqaoVxTgcCXnsNhPd_4; }
	inline int32_t* get_address_of_KBqIMUDUgsWqaoVxTgcCXnsNhPd_4() { return &___KBqIMUDUgsWqaoVxTgcCXnsNhPd_4; }
	inline void set_KBqIMUDUgsWqaoVxTgcCXnsNhPd_4(int32_t value)
	{
		___KBqIMUDUgsWqaoVxTgcCXnsNhPd_4 = value;
	}

	inline static int32_t get_offset_of_UMEgPuPnmRmOfcQAubdAHRnEzyYI_5() { return static_cast<int32_t>(offsetof(gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A, ___UMEgPuPnmRmOfcQAubdAHRnEzyYI_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_UMEgPuPnmRmOfcQAubdAHRnEzyYI_5() const { return ___UMEgPuPnmRmOfcQAubdAHRnEzyYI_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_UMEgPuPnmRmOfcQAubdAHRnEzyYI_5() { return &___UMEgPuPnmRmOfcQAubdAHRnEzyYI_5; }
	inline void set_UMEgPuPnmRmOfcQAubdAHRnEzyYI_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___UMEgPuPnmRmOfcQAubdAHRnEzyYI_5 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return static_cast<int32_t>(offsetof(gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_6 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_6), value);
	}

	inline static int32_t get_offset_of_gygTNkRxZGmHjziBespiNVAKnFL_7() { return static_cast<int32_t>(offsetof(gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A, ___gygTNkRxZGmHjziBespiNVAKnFL_7)); }
	inline RuntimeObject* get_gygTNkRxZGmHjziBespiNVAKnFL_7() const { return ___gygTNkRxZGmHjziBespiNVAKnFL_7; }
	inline RuntimeObject** get_address_of_gygTNkRxZGmHjziBespiNVAKnFL_7() { return &___gygTNkRxZGmHjziBespiNVAKnFL_7; }
	inline void set_gygTNkRxZGmHjziBespiNVAKnFL_7(RuntimeObject* value)
	{
		___gygTNkRxZGmHjziBespiNVAKnFL_7 = value;
		Il2CppCodeGenWriteBarrier((&___gygTNkRxZGmHjziBespiNVAKnFL_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GXSGPDJSDVWAGAKGLDJJVBGWMPD_TE122FF1DDF57B2885BCB67A5CAB321227F33682A_H
#ifndef HSLDMEWGSJAUSIENDJGPYQCGIQUG_T218C7058E04084BA91430B50B19B6BBA2DDECF62_H
#define HSLDMEWGSJAUSIENDJGPYQCGIQUG_T218C7058E04084BA91430B50B19B6BBA2DDECF62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_hSLDmeWgsjAusiendJgPyQcGiqUG
struct  hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_hSLDmeWgsjAusiendJgPyQcGiqUG::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_hSLDmeWgsjAusiendJgPyQcGiqUG::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_hSLDmeWgsjAusiendJgPyQcGiqUG::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Collections.Generic.IList`1<Rewired.Joystick> Rewired.ReInput_ControllerHelper_PollingHelper_hSLDmeWgsjAusiendJgPyQcGiqUG::SuIMwsBbcUZLGhqNQGVITQFmMcP
	RuntimeObject* ___SuIMwsBbcUZLGhqNQGVITQFmMcP_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_hSLDmeWgsjAusiendJgPyQcGiqUG::tnzQdzfTtfgncibXTcLnqGJmJen
	int32_t ___tnzQdzfTtfgncibXTcLnqGJmJen_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_hSLDmeWgsjAusiendJgPyQcGiqUG::lNSFjKqFeSbuhoIQjAmpEVFFcIrc
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___lNSFjKqFeSbuhoIQjAmpEVFFcIrc_5;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_hSLDmeWgsjAusiendJgPyQcGiqUG::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_hSLDmeWgsjAusiendJgPyQcGiqUG::BxrkKJNmWBaBYDcqFoeFgiqbiUj
	RuntimeObject* ___BxrkKJNmWBaBYDcqFoeFgiqbiUj_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_SuIMwsBbcUZLGhqNQGVITQFmMcP_3() { return static_cast<int32_t>(offsetof(hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62, ___SuIMwsBbcUZLGhqNQGVITQFmMcP_3)); }
	inline RuntimeObject* get_SuIMwsBbcUZLGhqNQGVITQFmMcP_3() const { return ___SuIMwsBbcUZLGhqNQGVITQFmMcP_3; }
	inline RuntimeObject** get_address_of_SuIMwsBbcUZLGhqNQGVITQFmMcP_3() { return &___SuIMwsBbcUZLGhqNQGVITQFmMcP_3; }
	inline void set_SuIMwsBbcUZLGhqNQGVITQFmMcP_3(RuntimeObject* value)
	{
		___SuIMwsBbcUZLGhqNQGVITQFmMcP_3 = value;
		Il2CppCodeGenWriteBarrier((&___SuIMwsBbcUZLGhqNQGVITQFmMcP_3), value);
	}

	inline static int32_t get_offset_of_tnzQdzfTtfgncibXTcLnqGJmJen_4() { return static_cast<int32_t>(offsetof(hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62, ___tnzQdzfTtfgncibXTcLnqGJmJen_4)); }
	inline int32_t get_tnzQdzfTtfgncibXTcLnqGJmJen_4() const { return ___tnzQdzfTtfgncibXTcLnqGJmJen_4; }
	inline int32_t* get_address_of_tnzQdzfTtfgncibXTcLnqGJmJen_4() { return &___tnzQdzfTtfgncibXTcLnqGJmJen_4; }
	inline void set_tnzQdzfTtfgncibXTcLnqGJmJen_4(int32_t value)
	{
		___tnzQdzfTtfgncibXTcLnqGJmJen_4 = value;
	}

	inline static int32_t get_offset_of_lNSFjKqFeSbuhoIQjAmpEVFFcIrc_5() { return static_cast<int32_t>(offsetof(hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62, ___lNSFjKqFeSbuhoIQjAmpEVFFcIrc_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_lNSFjKqFeSbuhoIQjAmpEVFFcIrc_5() const { return ___lNSFjKqFeSbuhoIQjAmpEVFFcIrc_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_lNSFjKqFeSbuhoIQjAmpEVFFcIrc_5() { return &___lNSFjKqFeSbuhoIQjAmpEVFFcIrc_5; }
	inline void set_lNSFjKqFeSbuhoIQjAmpEVFFcIrc_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___lNSFjKqFeSbuhoIQjAmpEVFFcIrc_5 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return static_cast<int32_t>(offsetof(hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_6 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_6), value);
	}

	inline static int32_t get_offset_of_BxrkKJNmWBaBYDcqFoeFgiqbiUj_7() { return static_cast<int32_t>(offsetof(hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62, ___BxrkKJNmWBaBYDcqFoeFgiqbiUj_7)); }
	inline RuntimeObject* get_BxrkKJNmWBaBYDcqFoeFgiqbiUj_7() const { return ___BxrkKJNmWBaBYDcqFoeFgiqbiUj_7; }
	inline RuntimeObject** get_address_of_BxrkKJNmWBaBYDcqFoeFgiqbiUj_7() { return &___BxrkKJNmWBaBYDcqFoeFgiqbiUj_7; }
	inline void set_BxrkKJNmWBaBYDcqFoeFgiqbiUj_7(RuntimeObject* value)
	{
		___BxrkKJNmWBaBYDcqFoeFgiqbiUj_7 = value;
		Il2CppCodeGenWriteBarrier((&___BxrkKJNmWBaBYDcqFoeFgiqbiUj_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSLDMEWGSJAUSIENDJGPYQCGIQUG_T218C7058E04084BA91430B50B19B6BBA2DDECF62_H
#ifndef HWIGODJUHYKEQDCNGUBGQQZOROE_TAD431ECCC5E45E80FDDCAE139410C0AA664C9594_H
#define HWIGODJUHYKEQDCNGUBGQQZOROE_TAD431ECCC5E45E80FDDCAE139410C0AA664C9594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_hwIgOdJuHYKeqdCNgubgQQzOrOe
struct  hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_hwIgOdJuHYKeqdCNgubgQQzOrOe::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_hwIgOdJuHYKeqdCNgubgQQzOrOe::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_hwIgOdJuHYKeqdCNgubgQQzOrOe::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Collections.Generic.IList`1<Rewired.CustomController> Rewired.ReInput_ControllerHelper_PollingHelper_hwIgOdJuHYKeqdCNgubgQQzOrOe::goIbDVgpvEutUFqWyUXpuJkiCTVS
	RuntimeObject* ___goIbDVgpvEutUFqWyUXpuJkiCTVS_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_hwIgOdJuHYKeqdCNgubgQQzOrOe::ywSeTEzssQbiwQJBJoAPPgfkgAY
	int32_t ___ywSeTEzssQbiwQJBJoAPPgfkgAY_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_hwIgOdJuHYKeqdCNgubgQQzOrOe::YfnzGrihuaRNzXSsiibwYuqCTdQ
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___YfnzGrihuaRNzXSsiibwYuqCTdQ_5;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_hwIgOdJuHYKeqdCNgubgQQzOrOe::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_hwIgOdJuHYKeqdCNgubgQQzOrOe::kCjuHwxFHLERkCAJTcXTjgMdlVE
	RuntimeObject* ___kCjuHwxFHLERkCAJTcXTjgMdlVE_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_goIbDVgpvEutUFqWyUXpuJkiCTVS_3() { return static_cast<int32_t>(offsetof(hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594, ___goIbDVgpvEutUFqWyUXpuJkiCTVS_3)); }
	inline RuntimeObject* get_goIbDVgpvEutUFqWyUXpuJkiCTVS_3() const { return ___goIbDVgpvEutUFqWyUXpuJkiCTVS_3; }
	inline RuntimeObject** get_address_of_goIbDVgpvEutUFqWyUXpuJkiCTVS_3() { return &___goIbDVgpvEutUFqWyUXpuJkiCTVS_3; }
	inline void set_goIbDVgpvEutUFqWyUXpuJkiCTVS_3(RuntimeObject* value)
	{
		___goIbDVgpvEutUFqWyUXpuJkiCTVS_3 = value;
		Il2CppCodeGenWriteBarrier((&___goIbDVgpvEutUFqWyUXpuJkiCTVS_3), value);
	}

	inline static int32_t get_offset_of_ywSeTEzssQbiwQJBJoAPPgfkgAY_4() { return static_cast<int32_t>(offsetof(hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594, ___ywSeTEzssQbiwQJBJoAPPgfkgAY_4)); }
	inline int32_t get_ywSeTEzssQbiwQJBJoAPPgfkgAY_4() const { return ___ywSeTEzssQbiwQJBJoAPPgfkgAY_4; }
	inline int32_t* get_address_of_ywSeTEzssQbiwQJBJoAPPgfkgAY_4() { return &___ywSeTEzssQbiwQJBJoAPPgfkgAY_4; }
	inline void set_ywSeTEzssQbiwQJBJoAPPgfkgAY_4(int32_t value)
	{
		___ywSeTEzssQbiwQJBJoAPPgfkgAY_4 = value;
	}

	inline static int32_t get_offset_of_YfnzGrihuaRNzXSsiibwYuqCTdQ_5() { return static_cast<int32_t>(offsetof(hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594, ___YfnzGrihuaRNzXSsiibwYuqCTdQ_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_YfnzGrihuaRNzXSsiibwYuqCTdQ_5() const { return ___YfnzGrihuaRNzXSsiibwYuqCTdQ_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_YfnzGrihuaRNzXSsiibwYuqCTdQ_5() { return &___YfnzGrihuaRNzXSsiibwYuqCTdQ_5; }
	inline void set_YfnzGrihuaRNzXSsiibwYuqCTdQ_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___YfnzGrihuaRNzXSsiibwYuqCTdQ_5 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return static_cast<int32_t>(offsetof(hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_6 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_6), value);
	}

	inline static int32_t get_offset_of_kCjuHwxFHLERkCAJTcXTjgMdlVE_7() { return static_cast<int32_t>(offsetof(hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594, ___kCjuHwxFHLERkCAJTcXTjgMdlVE_7)); }
	inline RuntimeObject* get_kCjuHwxFHLERkCAJTcXTjgMdlVE_7() const { return ___kCjuHwxFHLERkCAJTcXTjgMdlVE_7; }
	inline RuntimeObject** get_address_of_kCjuHwxFHLERkCAJTcXTjgMdlVE_7() { return &___kCjuHwxFHLERkCAJTcXTjgMdlVE_7; }
	inline void set_kCjuHwxFHLERkCAJTcXTjgMdlVE_7(RuntimeObject* value)
	{
		___kCjuHwxFHLERkCAJTcXTjgMdlVE_7 = value;
		Il2CppCodeGenWriteBarrier((&___kCjuHwxFHLERkCAJTcXTjgMdlVE_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HWIGODJUHYKEQDCNGUBGQQZOROE_TAD431ECCC5E45E80FDDCAE139410C0AA664C9594_H
#ifndef WPTWMWMDWEAXMEIHLBQIPIVTJVGA_TFC54795E71D69A315C72CEF928655EE9DC54D4B1_H
#define WPTWMWMDWEAXMEIHLBQIPIVTJVGA_TFC54795E71D69A315C72CEF928655EE9DC54D4B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_wPTWmWMdweaXmEihlbqiPIvTjVGa
struct  wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_wPTWmWMdweaXmEihlbqiPIvTjVGa::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_wPTWmWMdweaXmEihlbqiPIvTjVGa::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_wPTWmWMdweaXmEihlbqiPIvTjVGa::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Collections.Generic.IList`1<Rewired.CustomController> Rewired.ReInput_ControllerHelper_PollingHelper_wPTWmWMdweaXmEihlbqiPIvTjVGa::boPDfJqwPeTdoZztcumuxTIgoST
	RuntimeObject* ___boPDfJqwPeTdoZztcumuxTIgoST_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_wPTWmWMdweaXmEihlbqiPIvTjVGa::FJwvShZPOLNXNucTHGOTJlqLDHSc
	int32_t ___FJwvShZPOLNXNucTHGOTJlqLDHSc_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_wPTWmWMdweaXmEihlbqiPIvTjVGa::WdNNTQdPMQeqQYMebCOybxxbGVkr
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___WdNNTQdPMQeqQYMebCOybxxbGVkr_5;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_wPTWmWMdweaXmEihlbqiPIvTjVGa::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_wPTWmWMdweaXmEihlbqiPIvTjVGa::asbdPiIRAXtYLhQAvtCtEExwGcp
	RuntimeObject* ___asbdPiIRAXtYLhQAvtCtEExwGcp_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_boPDfJqwPeTdoZztcumuxTIgoST_3() { return static_cast<int32_t>(offsetof(wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1, ___boPDfJqwPeTdoZztcumuxTIgoST_3)); }
	inline RuntimeObject* get_boPDfJqwPeTdoZztcumuxTIgoST_3() const { return ___boPDfJqwPeTdoZztcumuxTIgoST_3; }
	inline RuntimeObject** get_address_of_boPDfJqwPeTdoZztcumuxTIgoST_3() { return &___boPDfJqwPeTdoZztcumuxTIgoST_3; }
	inline void set_boPDfJqwPeTdoZztcumuxTIgoST_3(RuntimeObject* value)
	{
		___boPDfJqwPeTdoZztcumuxTIgoST_3 = value;
		Il2CppCodeGenWriteBarrier((&___boPDfJqwPeTdoZztcumuxTIgoST_3), value);
	}

	inline static int32_t get_offset_of_FJwvShZPOLNXNucTHGOTJlqLDHSc_4() { return static_cast<int32_t>(offsetof(wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1, ___FJwvShZPOLNXNucTHGOTJlqLDHSc_4)); }
	inline int32_t get_FJwvShZPOLNXNucTHGOTJlqLDHSc_4() const { return ___FJwvShZPOLNXNucTHGOTJlqLDHSc_4; }
	inline int32_t* get_address_of_FJwvShZPOLNXNucTHGOTJlqLDHSc_4() { return &___FJwvShZPOLNXNucTHGOTJlqLDHSc_4; }
	inline void set_FJwvShZPOLNXNucTHGOTJlqLDHSc_4(int32_t value)
	{
		___FJwvShZPOLNXNucTHGOTJlqLDHSc_4 = value;
	}

	inline static int32_t get_offset_of_WdNNTQdPMQeqQYMebCOybxxbGVkr_5() { return static_cast<int32_t>(offsetof(wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1, ___WdNNTQdPMQeqQYMebCOybxxbGVkr_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_WdNNTQdPMQeqQYMebCOybxxbGVkr_5() const { return ___WdNNTQdPMQeqQYMebCOybxxbGVkr_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_WdNNTQdPMQeqQYMebCOybxxbGVkr_5() { return &___WdNNTQdPMQeqQYMebCOybxxbGVkr_5; }
	inline void set_WdNNTQdPMQeqQYMebCOybxxbGVkr_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___WdNNTQdPMQeqQYMebCOybxxbGVkr_5 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return static_cast<int32_t>(offsetof(wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_6 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_6), value);
	}

	inline static int32_t get_offset_of_asbdPiIRAXtYLhQAvtCtEExwGcp_7() { return static_cast<int32_t>(offsetof(wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1, ___asbdPiIRAXtYLhQAvtCtEExwGcp_7)); }
	inline RuntimeObject* get_asbdPiIRAXtYLhQAvtCtEExwGcp_7() const { return ___asbdPiIRAXtYLhQAvtCtEExwGcp_7; }
	inline RuntimeObject** get_address_of_asbdPiIRAXtYLhQAvtCtEExwGcp_7() { return &___asbdPiIRAXtYLhQAvtCtEExwGcp_7; }
	inline void set_asbdPiIRAXtYLhQAvtCtEExwGcp_7(RuntimeObject* value)
	{
		___asbdPiIRAXtYLhQAvtCtEExwGcp_7 = value;
		Il2CppCodeGenWriteBarrier((&___asbdPiIRAXtYLhQAvtCtEExwGcp_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WPTWMWMDWEAXMEIHLBQIPIVTJVGA_TFC54795E71D69A315C72CEF928655EE9DC54D4B1_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef VIHAQNNVJFGTSIZZVDNBXPXKCJWN_T0284CDE88A16E6F3D2374205C61C13AAF40E69D9_H
#define VIHAQNNVJFGTSIZZVDNBXPXKCJWN_T0284CDE88A16E6F3D2374205C61C13AAF40E69D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VIhaqNNVJFgtSIZzvDNBXPXkcJWN
struct  VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9  : public OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533
{
public:
	// System.Int32 VIhaqNNVJFgtSIZzvDNBXPXkcJWN::tOaTpOwzcPDCHdCDIyYaeuASgAM
	int32_t ___tOaTpOwzcPDCHdCDIyYaeuASgAM_11;
	// System.Int32 VIhaqNNVJFgtSIZzvDNBXPXkcJWN::itTafNyeQoucrbhPkPsSqRewAEI
	int32_t ___itTafNyeQoucrbhPkPsSqRewAEI_12;
	// System.Int32 VIhaqNNVJFgtSIZzvDNBXPXkcJWN::ZWYAwyuulqYogftJTkDBnLhijEo
	int32_t ___ZWYAwyuulqYogftJTkDBnLhijEo_13;
	// System.Int32 VIhaqNNVJFgtSIZzvDNBXPXkcJWN::BQhIlyGtJuwhVjMyFqaFVZMDOIY
	int32_t ___BQhIlyGtJuwhVjMyFqaFVZMDOIY_14;
	// System.Int16[] VIhaqNNVJFgtSIZzvDNBXPXkcJWN::CGnOFilUdwgpDeadACHppWhTHbH
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___CGnOFilUdwgpDeadACHppWhTHbH_15;
	// Rewired.ButtonLoopSet VIhaqNNVJFgtSIZzvDNBXPXkcJWN::gsGCoHGepWkEDIBokgAUxNPLZsGv
	ButtonLoopSet_t043F70DD0163AEE263E80CB7B1AA6C550C46F0AC * ___gsGCoHGepWkEDIBokgAUxNPLZsGv_16;
	// System.Int16[] VIhaqNNVJFgtSIZzvDNBXPXkcJWN::fjJgMqsiEShgwfdbppebypKrcGwO
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___fjJgMqsiEShgwfdbppebypKrcGwO_17;
	// System.Int16[] VIhaqNNVJFgtSIZzvDNBXPXkcJWN::cznagYJJEGkibwvDkNZVUkbOnma
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___cznagYJJEGkibwvDkNZVUkbOnma_18;
	// System.Boolean VIhaqNNVJFgtSIZzvDNBXPXkcJWN::uTrFqghzwmUUNLMxoQSRVgfDfsj
	bool ___uTrFqghzwmUUNLMxoQSRVgfDfsj_19;

public:
	inline static int32_t get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_11() { return static_cast<int32_t>(offsetof(VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9, ___tOaTpOwzcPDCHdCDIyYaeuASgAM_11)); }
	inline int32_t get_tOaTpOwzcPDCHdCDIyYaeuASgAM_11() const { return ___tOaTpOwzcPDCHdCDIyYaeuASgAM_11; }
	inline int32_t* get_address_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_11() { return &___tOaTpOwzcPDCHdCDIyYaeuASgAM_11; }
	inline void set_tOaTpOwzcPDCHdCDIyYaeuASgAM_11(int32_t value)
	{
		___tOaTpOwzcPDCHdCDIyYaeuASgAM_11 = value;
	}

	inline static int32_t get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_12() { return static_cast<int32_t>(offsetof(VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9, ___itTafNyeQoucrbhPkPsSqRewAEI_12)); }
	inline int32_t get_itTafNyeQoucrbhPkPsSqRewAEI_12() const { return ___itTafNyeQoucrbhPkPsSqRewAEI_12; }
	inline int32_t* get_address_of_itTafNyeQoucrbhPkPsSqRewAEI_12() { return &___itTafNyeQoucrbhPkPsSqRewAEI_12; }
	inline void set_itTafNyeQoucrbhPkPsSqRewAEI_12(int32_t value)
	{
		___itTafNyeQoucrbhPkPsSqRewAEI_12 = value;
	}

	inline static int32_t get_offset_of_ZWYAwyuulqYogftJTkDBnLhijEo_13() { return static_cast<int32_t>(offsetof(VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9, ___ZWYAwyuulqYogftJTkDBnLhijEo_13)); }
	inline int32_t get_ZWYAwyuulqYogftJTkDBnLhijEo_13() const { return ___ZWYAwyuulqYogftJTkDBnLhijEo_13; }
	inline int32_t* get_address_of_ZWYAwyuulqYogftJTkDBnLhijEo_13() { return &___ZWYAwyuulqYogftJTkDBnLhijEo_13; }
	inline void set_ZWYAwyuulqYogftJTkDBnLhijEo_13(int32_t value)
	{
		___ZWYAwyuulqYogftJTkDBnLhijEo_13 = value;
	}

	inline static int32_t get_offset_of_BQhIlyGtJuwhVjMyFqaFVZMDOIY_14() { return static_cast<int32_t>(offsetof(VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9, ___BQhIlyGtJuwhVjMyFqaFVZMDOIY_14)); }
	inline int32_t get_BQhIlyGtJuwhVjMyFqaFVZMDOIY_14() const { return ___BQhIlyGtJuwhVjMyFqaFVZMDOIY_14; }
	inline int32_t* get_address_of_BQhIlyGtJuwhVjMyFqaFVZMDOIY_14() { return &___BQhIlyGtJuwhVjMyFqaFVZMDOIY_14; }
	inline void set_BQhIlyGtJuwhVjMyFqaFVZMDOIY_14(int32_t value)
	{
		___BQhIlyGtJuwhVjMyFqaFVZMDOIY_14 = value;
	}

	inline static int32_t get_offset_of_CGnOFilUdwgpDeadACHppWhTHbH_15() { return static_cast<int32_t>(offsetof(VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9, ___CGnOFilUdwgpDeadACHppWhTHbH_15)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_CGnOFilUdwgpDeadACHppWhTHbH_15() const { return ___CGnOFilUdwgpDeadACHppWhTHbH_15; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_CGnOFilUdwgpDeadACHppWhTHbH_15() { return &___CGnOFilUdwgpDeadACHppWhTHbH_15; }
	inline void set_CGnOFilUdwgpDeadACHppWhTHbH_15(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___CGnOFilUdwgpDeadACHppWhTHbH_15 = value;
		Il2CppCodeGenWriteBarrier((&___CGnOFilUdwgpDeadACHppWhTHbH_15), value);
	}

	inline static int32_t get_offset_of_gsGCoHGepWkEDIBokgAUxNPLZsGv_16() { return static_cast<int32_t>(offsetof(VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9, ___gsGCoHGepWkEDIBokgAUxNPLZsGv_16)); }
	inline ButtonLoopSet_t043F70DD0163AEE263E80CB7B1AA6C550C46F0AC * get_gsGCoHGepWkEDIBokgAUxNPLZsGv_16() const { return ___gsGCoHGepWkEDIBokgAUxNPLZsGv_16; }
	inline ButtonLoopSet_t043F70DD0163AEE263E80CB7B1AA6C550C46F0AC ** get_address_of_gsGCoHGepWkEDIBokgAUxNPLZsGv_16() { return &___gsGCoHGepWkEDIBokgAUxNPLZsGv_16; }
	inline void set_gsGCoHGepWkEDIBokgAUxNPLZsGv_16(ButtonLoopSet_t043F70DD0163AEE263E80CB7B1AA6C550C46F0AC * value)
	{
		___gsGCoHGepWkEDIBokgAUxNPLZsGv_16 = value;
		Il2CppCodeGenWriteBarrier((&___gsGCoHGepWkEDIBokgAUxNPLZsGv_16), value);
	}

	inline static int32_t get_offset_of_fjJgMqsiEShgwfdbppebypKrcGwO_17() { return static_cast<int32_t>(offsetof(VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9, ___fjJgMqsiEShgwfdbppebypKrcGwO_17)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_fjJgMqsiEShgwfdbppebypKrcGwO_17() const { return ___fjJgMqsiEShgwfdbppebypKrcGwO_17; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_fjJgMqsiEShgwfdbppebypKrcGwO_17() { return &___fjJgMqsiEShgwfdbppebypKrcGwO_17; }
	inline void set_fjJgMqsiEShgwfdbppebypKrcGwO_17(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___fjJgMqsiEShgwfdbppebypKrcGwO_17 = value;
		Il2CppCodeGenWriteBarrier((&___fjJgMqsiEShgwfdbppebypKrcGwO_17), value);
	}

	inline static int32_t get_offset_of_cznagYJJEGkibwvDkNZVUkbOnma_18() { return static_cast<int32_t>(offsetof(VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9, ___cznagYJJEGkibwvDkNZVUkbOnma_18)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_cznagYJJEGkibwvDkNZVUkbOnma_18() const { return ___cznagYJJEGkibwvDkNZVUkbOnma_18; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_cznagYJJEGkibwvDkNZVUkbOnma_18() { return &___cznagYJJEGkibwvDkNZVUkbOnma_18; }
	inline void set_cznagYJJEGkibwvDkNZVUkbOnma_18(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___cznagYJJEGkibwvDkNZVUkbOnma_18 = value;
		Il2CppCodeGenWriteBarrier((&___cznagYJJEGkibwvDkNZVUkbOnma_18), value);
	}

	inline static int32_t get_offset_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_19() { return static_cast<int32_t>(offsetof(VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9, ___uTrFqghzwmUUNLMxoQSRVgfDfsj_19)); }
	inline bool get_uTrFqghzwmUUNLMxoQSRVgfDfsj_19() const { return ___uTrFqghzwmUUNLMxoQSRVgfDfsj_19; }
	inline bool* get_address_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_19() { return &___uTrFqghzwmUUNLMxoQSRVgfDfsj_19; }
	inline void set_uTrFqghzwmUUNLMxoQSRVgfDfsj_19(bool value)
	{
		___uTrFqghzwmUUNLMxoQSRVgfDfsj_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIHAQNNVJFGTSIZZVDNBXPXKCJWN_T0284CDE88A16E6F3D2374205C61C13AAF40E69D9_H
#ifndef XINYVXTRFXEKDVRLANFMYUWAYZV_TFC161390B4FF558F7F27D77244EC0FA850BF5C40_H
#define XINYVXTRFXEKDVRLANFMYUWAYZV_TFC161390B4FF558F7F27D77244EC0FA850BF5C40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XinyVXTRfxekDvrLaNfmyUwaYzV
struct  XinyVXTRfxekDvrLaNfmyUwaYzV_tFC161390B4FF558F7F27D77244EC0FA850BF5C40  : public KDIcNAKNsuNvLiMepNxDAEccLSR_tA3DD4DB1AB688F1291A2B02EC7E3CFD5A0829709
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XINYVXTRFXEKDVRLANFMYUWAYZV_TFC161390B4FF558F7F27D77244EC0FA850BF5C40_H
#ifndef YDWYOODKZMMIFKVNTMZZPZDKTYT_T8E03656705A41147983FF05F894F534859833196_H
#define YDWYOODKZMMIFKVNTMZZPZDKTYT_T8E03656705A41147983FF05F894F534859833196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YdwyOOdkZmMIFKVNTmZzPZDktYt
struct  YdwyOOdkZmMIFKVNTmZzPZDktYt_t8E03656705A41147983FF05F894F534859833196  : public KDIcNAKNsuNvLiMepNxDAEccLSR_tA3DD4DB1AB688F1291A2B02EC7E3CFD5A0829709
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YDWYOODKZMMIFKVNTMZZPZDKTYT_T8E03656705A41147983FF05F894F534859833196_H
#ifndef DGUHLNCMLHMYCDCMYEWGUDIAQPKX_TADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A_H
#define DGUHLNCMLHMYCDCMYEWGUDIAQPKX_TADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_DgUhlnCmLHmyCdCMyeWgudIaqpKx
struct  DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_DgUhlnCmLHmyCdCMyeWgudIaqpKx::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_0;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_DgUhlnCmLHmyCdCMyeWgudIaqpKx::HRIDVrOGCGQQtYMmaCfxzAAVaqs
	uint32_t ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1;
	// System.UInt32 gvrkMEBkQRXiZPPNldGUfPXGyiXV_DgUhlnCmLHmyCdCMyeWgudIaqpKx::NkqzOUnpKxOLEIHqzuEvIwFSEWZ
	uint32_t ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_DgUhlnCmLHmyCdCMyeWgudIaqpKx::IyCUGfzcPoSzeXqQnApzwoMSQTs
	uint8_t ___IyCUGfzcPoSzeXqQnApzwoMSQTs_3;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_DgUhlnCmLHmyCdCMyeWgudIaqpKx::NZprZllnPiLWyPKXEjljqjKNXuz
	uint8_t ___NZprZllnPiLWyPKXEjljqjKNXuz_4;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_DgUhlnCmLHmyCdCMyeWgudIaqpKx::ZdxNPrFvibjxZCDAFwWLoADXmOO
	uint8_t ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5;
	// System.Byte gvrkMEBkQRXiZPPNldGUfPXGyiXV_DgUhlnCmLHmyCdCMyeWgudIaqpKx::lbxzKBvrbNcnDcVvpWWCKOWJRDOM
	uint8_t ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6;
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_CJxQDhrlajjtvIBURZzUvXUWUbo gvrkMEBkQRXiZPPNldGUfPXGyiXV_DgUhlnCmLHmyCdCMyeWgudIaqpKx::LdQqoliuFDJnxKCXeTnZQrzENmL
	CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A  ___LdQqoliuFDJnxKCXeTnZQrzENmL_7;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return static_cast<int32_t>(offsetof(DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A, ___umQghCmsfCFpiiISpgIfIkmOcxB_0)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_0() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_0; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_0(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_0 = value;
	}

	inline static int32_t get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return static_cast<int32_t>(offsetof(DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A, ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1)); }
	inline uint32_t get_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() const { return ___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline uint32_t* get_address_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() { return &___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1; }
	inline void set_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1(uint32_t value)
	{
		___HRIDVrOGCGQQtYMmaCfxzAAVaqs_1 = value;
	}

	inline static int32_t get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return static_cast<int32_t>(offsetof(DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A, ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2)); }
	inline uint32_t get_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() const { return ___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline uint32_t* get_address_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() { return &___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2; }
	inline void set_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2(uint32_t value)
	{
		___NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2 = value;
	}

	inline static int32_t get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_3() { return static_cast<int32_t>(offsetof(DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A, ___IyCUGfzcPoSzeXqQnApzwoMSQTs_3)); }
	inline uint8_t get_IyCUGfzcPoSzeXqQnApzwoMSQTs_3() const { return ___IyCUGfzcPoSzeXqQnApzwoMSQTs_3; }
	inline uint8_t* get_address_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_3() { return &___IyCUGfzcPoSzeXqQnApzwoMSQTs_3; }
	inline void set_IyCUGfzcPoSzeXqQnApzwoMSQTs_3(uint8_t value)
	{
		___IyCUGfzcPoSzeXqQnApzwoMSQTs_3 = value;
	}

	inline static int32_t get_offset_of_NZprZllnPiLWyPKXEjljqjKNXuz_4() { return static_cast<int32_t>(offsetof(DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A, ___NZprZllnPiLWyPKXEjljqjKNXuz_4)); }
	inline uint8_t get_NZprZllnPiLWyPKXEjljqjKNXuz_4() const { return ___NZprZllnPiLWyPKXEjljqjKNXuz_4; }
	inline uint8_t* get_address_of_NZprZllnPiLWyPKXEjljqjKNXuz_4() { return &___NZprZllnPiLWyPKXEjljqjKNXuz_4; }
	inline void set_NZprZllnPiLWyPKXEjljqjKNXuz_4(uint8_t value)
	{
		___NZprZllnPiLWyPKXEjljqjKNXuz_4 = value;
	}

	inline static int32_t get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() { return static_cast<int32_t>(offsetof(DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A, ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5)); }
	inline uint8_t get_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() const { return ___ZdxNPrFvibjxZCDAFwWLoADXmOO_5; }
	inline uint8_t* get_address_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() { return &___ZdxNPrFvibjxZCDAFwWLoADXmOO_5; }
	inline void set_ZdxNPrFvibjxZCDAFwWLoADXmOO_5(uint8_t value)
	{
		___ZdxNPrFvibjxZCDAFwWLoADXmOO_5 = value;
	}

	inline static int32_t get_offset_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() { return static_cast<int32_t>(offsetof(DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A, ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6)); }
	inline uint8_t get_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() const { return ___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6; }
	inline uint8_t* get_address_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() { return &___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6; }
	inline void set_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6(uint8_t value)
	{
		___lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6 = value;
	}

	inline static int32_t get_offset_of_LdQqoliuFDJnxKCXeTnZQrzENmL_7() { return static_cast<int32_t>(offsetof(DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A, ___LdQqoliuFDJnxKCXeTnZQrzENmL_7)); }
	inline CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A  get_LdQqoliuFDJnxKCXeTnZQrzENmL_7() const { return ___LdQqoliuFDJnxKCXeTnZQrzENmL_7; }
	inline CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A * get_address_of_LdQqoliuFDJnxKCXeTnZQrzENmL_7() { return &___LdQqoliuFDJnxKCXeTnZQrzENmL_7; }
	inline void set_LdQqoliuFDJnxKCXeTnZQrzENmL_7(CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A  value)
	{
		___LdQqoliuFDJnxKCXeTnZQrzENmL_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DGUHLNCMLHMYCDCMYEWGUDIAQPKX_TADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A_H
#ifndef YFETHIHCXPOKCXRKFPITSMRYYAV_T298F3A1DBA894A1D6CB374D029A41D355872E97C_H
#define YFETHIHCXPOKCXRKFPITSMRYYAV_T298F3A1DBA894A1D6CB374D029A41D355872E97C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gvrkMEBkQRXiZPPNldGUfPXGyiXV_yFEtHIhcxPOKCXrKFpITsmryyaV
struct  yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C 
{
public:
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_nOobeSrFgOqBcHXHXKMtXHhJGas gvrkMEBkQRXiZPPNldGUfPXGyiXV_yFEtHIhcxPOKCXrKFpITsmryyaV::umQghCmsfCFpiiISpgIfIkmOcxB
	uint32_t ___umQghCmsfCFpiiISpgIfIkmOcxB_1;
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_PoESixqIoOnzTYjigIYOyJuWcuO gvrkMEBkQRXiZPPNldGUfPXGyiXV_yFEtHIhcxPOKCXrKFpITsmryyaV::SVwdWTVDBhPwSUmnjUWSqtqsoJB
	PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E  ___SVwdWTVDBhPwSUmnjUWSqtqsoJB_2;
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_JsSrNMiUWvPaoUYnokkJplOPpIs gvrkMEBkQRXiZPPNldGUfPXGyiXV_yFEtHIhcxPOKCXrKFpITsmryyaV::QMJZUMSHcwNerFwvvKHDkmLFfyY
	JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7  ___QMJZUMSHcwNerFwvvKHDkmLFfyY_3;
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_vnUZdxGbbKheGOMPoylCsjnialt gvrkMEBkQRXiZPPNldGUfPXGyiXV_yFEtHIhcxPOKCXrKFpITsmryyaV::CswBtlDeNeFLQjWnXwOXEBObaOjC
	vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457  ___CswBtlDeNeFLQjWnXwOXEBObaOjC_4;
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_xLNiqspMGfoLDyZKWDwgBkIaPew gvrkMEBkQRXiZPPNldGUfPXGyiXV_yFEtHIhcxPOKCXrKFpITsmryyaV::mCHiRPjvgBJsFyjkeNVSKqdXPgnk
	xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD  ___mCHiRPjvgBJsFyjkeNVSKqdXPgnk_5;
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_HdoHFYadSWiBUzqWnduNfIPFVazE gvrkMEBkQRXiZPPNldGUfPXGyiXV_yFEtHIhcxPOKCXrKFpITsmryyaV::bPsRdrmftJHejLOgJTuTCFACGs
	HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554  ___bPsRdrmftJHejLOgJTuTCFACGs_6;
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_eCLHpuFWjrdUHnCTkwDUJptJKlF gvrkMEBkQRXiZPPNldGUfPXGyiXV_yFEtHIhcxPOKCXrKFpITsmryyaV::wSBHiuDfzRgllDAmllAFEAycDtDb
	eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A  ___wSBHiuDfzRgllDAmllAFEAycDtDb_7;
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_QpTpOKhmTAOfldDXoBXERWoHGYae gvrkMEBkQRXiZPPNldGUfPXGyiXV_yFEtHIhcxPOKCXrKFpITsmryyaV::YiMJltQnEfZXHYswbcydPFFUPFr
	QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287  ___YiMJltQnEfZXHYswbcydPFFUPFr_8;
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_rSlcihBflhYIvceVOVZgmAZugcEW gvrkMEBkQRXiZPPNldGUfPXGyiXV_yFEtHIhcxPOKCXrKFpITsmryyaV::NwfCHMEtrKDekteRIBiyrFAXKhWi
	rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D  ___NwfCHMEtrKDekteRIBiyrFAXKhWi_9;

public:
	inline static int32_t get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_1() { return static_cast<int32_t>(offsetof(yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C, ___umQghCmsfCFpiiISpgIfIkmOcxB_1)); }
	inline uint32_t get_umQghCmsfCFpiiISpgIfIkmOcxB_1() const { return ___umQghCmsfCFpiiISpgIfIkmOcxB_1; }
	inline uint32_t* get_address_of_umQghCmsfCFpiiISpgIfIkmOcxB_1() { return &___umQghCmsfCFpiiISpgIfIkmOcxB_1; }
	inline void set_umQghCmsfCFpiiISpgIfIkmOcxB_1(uint32_t value)
	{
		___umQghCmsfCFpiiISpgIfIkmOcxB_1 = value;
	}

	inline static int32_t get_offset_of_SVwdWTVDBhPwSUmnjUWSqtqsoJB_2() { return static_cast<int32_t>(offsetof(yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C, ___SVwdWTVDBhPwSUmnjUWSqtqsoJB_2)); }
	inline PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E  get_SVwdWTVDBhPwSUmnjUWSqtqsoJB_2() const { return ___SVwdWTVDBhPwSUmnjUWSqtqsoJB_2; }
	inline PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E * get_address_of_SVwdWTVDBhPwSUmnjUWSqtqsoJB_2() { return &___SVwdWTVDBhPwSUmnjUWSqtqsoJB_2; }
	inline void set_SVwdWTVDBhPwSUmnjUWSqtqsoJB_2(PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E  value)
	{
		___SVwdWTVDBhPwSUmnjUWSqtqsoJB_2 = value;
	}

	inline static int32_t get_offset_of_QMJZUMSHcwNerFwvvKHDkmLFfyY_3() { return static_cast<int32_t>(offsetof(yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C, ___QMJZUMSHcwNerFwvvKHDkmLFfyY_3)); }
	inline JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7  get_QMJZUMSHcwNerFwvvKHDkmLFfyY_3() const { return ___QMJZUMSHcwNerFwvvKHDkmLFfyY_3; }
	inline JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7 * get_address_of_QMJZUMSHcwNerFwvvKHDkmLFfyY_3() { return &___QMJZUMSHcwNerFwvvKHDkmLFfyY_3; }
	inline void set_QMJZUMSHcwNerFwvvKHDkmLFfyY_3(JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7  value)
	{
		___QMJZUMSHcwNerFwvvKHDkmLFfyY_3 = value;
	}

	inline static int32_t get_offset_of_CswBtlDeNeFLQjWnXwOXEBObaOjC_4() { return static_cast<int32_t>(offsetof(yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C, ___CswBtlDeNeFLQjWnXwOXEBObaOjC_4)); }
	inline vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457  get_CswBtlDeNeFLQjWnXwOXEBObaOjC_4() const { return ___CswBtlDeNeFLQjWnXwOXEBObaOjC_4; }
	inline vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457 * get_address_of_CswBtlDeNeFLQjWnXwOXEBObaOjC_4() { return &___CswBtlDeNeFLQjWnXwOXEBObaOjC_4; }
	inline void set_CswBtlDeNeFLQjWnXwOXEBObaOjC_4(vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457  value)
	{
		___CswBtlDeNeFLQjWnXwOXEBObaOjC_4 = value;
	}

	inline static int32_t get_offset_of_mCHiRPjvgBJsFyjkeNVSKqdXPgnk_5() { return static_cast<int32_t>(offsetof(yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C, ___mCHiRPjvgBJsFyjkeNVSKqdXPgnk_5)); }
	inline xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD  get_mCHiRPjvgBJsFyjkeNVSKqdXPgnk_5() const { return ___mCHiRPjvgBJsFyjkeNVSKqdXPgnk_5; }
	inline xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD * get_address_of_mCHiRPjvgBJsFyjkeNVSKqdXPgnk_5() { return &___mCHiRPjvgBJsFyjkeNVSKqdXPgnk_5; }
	inline void set_mCHiRPjvgBJsFyjkeNVSKqdXPgnk_5(xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD  value)
	{
		___mCHiRPjvgBJsFyjkeNVSKqdXPgnk_5 = value;
	}

	inline static int32_t get_offset_of_bPsRdrmftJHejLOgJTuTCFACGs_6() { return static_cast<int32_t>(offsetof(yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C, ___bPsRdrmftJHejLOgJTuTCFACGs_6)); }
	inline HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554  get_bPsRdrmftJHejLOgJTuTCFACGs_6() const { return ___bPsRdrmftJHejLOgJTuTCFACGs_6; }
	inline HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554 * get_address_of_bPsRdrmftJHejLOgJTuTCFACGs_6() { return &___bPsRdrmftJHejLOgJTuTCFACGs_6; }
	inline void set_bPsRdrmftJHejLOgJTuTCFACGs_6(HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554  value)
	{
		___bPsRdrmftJHejLOgJTuTCFACGs_6 = value;
	}

	inline static int32_t get_offset_of_wSBHiuDfzRgllDAmllAFEAycDtDb_7() { return static_cast<int32_t>(offsetof(yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C, ___wSBHiuDfzRgllDAmllAFEAycDtDb_7)); }
	inline eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A  get_wSBHiuDfzRgllDAmllAFEAycDtDb_7() const { return ___wSBHiuDfzRgllDAmllAFEAycDtDb_7; }
	inline eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A * get_address_of_wSBHiuDfzRgllDAmllAFEAycDtDb_7() { return &___wSBHiuDfzRgllDAmllAFEAycDtDb_7; }
	inline void set_wSBHiuDfzRgllDAmllAFEAycDtDb_7(eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A  value)
	{
		___wSBHiuDfzRgllDAmllAFEAycDtDb_7 = value;
	}

	inline static int32_t get_offset_of_YiMJltQnEfZXHYswbcydPFFUPFr_8() { return static_cast<int32_t>(offsetof(yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C, ___YiMJltQnEfZXHYswbcydPFFUPFr_8)); }
	inline QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287  get_YiMJltQnEfZXHYswbcydPFFUPFr_8() const { return ___YiMJltQnEfZXHYswbcydPFFUPFr_8; }
	inline QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287 * get_address_of_YiMJltQnEfZXHYswbcydPFFUPFr_8() { return &___YiMJltQnEfZXHYswbcydPFFUPFr_8; }
	inline void set_YiMJltQnEfZXHYswbcydPFFUPFr_8(QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287  value)
	{
		___YiMJltQnEfZXHYswbcydPFFUPFr_8 = value;
	}

	inline static int32_t get_offset_of_NwfCHMEtrKDekteRIBiyrFAXKhWi_9() { return static_cast<int32_t>(offsetof(yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C, ___NwfCHMEtrKDekteRIBiyrFAXKhWi_9)); }
	inline rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D  get_NwfCHMEtrKDekteRIBiyrFAXKhWi_9() const { return ___NwfCHMEtrKDekteRIBiyrFAXKhWi_9; }
	inline rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D * get_address_of_NwfCHMEtrKDekteRIBiyrFAXKhWi_9() { return &___NwfCHMEtrKDekteRIBiyrFAXKhWi_9; }
	inline void set_NwfCHMEtrKDekteRIBiyrFAXKhWi_9(rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D  value)
	{
		___NwfCHMEtrKDekteRIBiyrFAXKhWi_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YFETHIHCXPOKCXRKFPITSMRYYAV_T298F3A1DBA894A1D6CB374D029A41D355872E97C_H
#ifndef NRJFJBKPXMOOTHNHWKMZDERIGXMR_TB1BF01420858D9DD80021FC48A2613BDEA02C91A_H
#define NRJFJBKPXMOOTHNHWKMZDERIGXMR_TB1BF01420858D9DD80021FC48A2613BDEA02C91A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NRJfjbKpxmoothnHwkmZDERiGXmR
struct  NRJfjbKpxmoothnHwkmZDERiGXmR_tB1BF01420858D9DD80021FC48A2613BDEA02C91A  : public YdwyOOdkZmMIFKVNTmZzPZDktYt_t8E03656705A41147983FF05F894F534859833196
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NRJFJBKPXMOOTHNHWKMZDERIGXMR_TB1BF01420858D9DD80021FC48A2613BDEA02C91A_H
#ifndef SDL2INPUTSOURCE_T8D429B4D964DFFEFFD201C897AB0EE90EC232E85_H
#define SDL2INPUTSOURCE_T8D429B4D964DFFEFFD201C897AB0EE90EC232E85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSources.SDL2.SDL2InputSource
struct  SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85  : public RuntimeObject
{
public:
	// System.Boolean Rewired.InputSources.SDL2.SDL2InputSource::GPamRvCbOttEEABtEfVCeWQVUod
	bool ___GPamRvCbOttEEABtEfVCeWQVUod_1;
	// System.Boolean Rewired.InputSources.SDL2.SDL2InputSource::DNyylDGzMjSsoFljrEWFZpbwUTp
	bool ___DNyylDGzMjSsoFljrEWFZpbwUTp_2;
	// System.Boolean Rewired.InputSources.SDL2.SDL2InputSource::cAcffkahmtipZtjyREtLadTZZhr
	bool ___cAcffkahmtipZtjyREtLadTZZhr_3;
	// System.Boolean Rewired.InputSources.SDL2.SDL2InputSource::JfNLsrbeYBZuaaMZBfeVwFCxYsI
	bool ___JfNLsrbeYBZuaaMZBfeVwFCxYsI_4;
	// System.Boolean Rewired.InputSources.SDL2.SDL2InputSource::oGbqxjxXGgvgXNLPUMbdLzEDsmT
	bool ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5;
	// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,VIhaqNNVJFgtSIZzvDNBXPXkcJWN> Rewired.InputSources.SDL2.SDL2InputSource::lZlHpVlqScCxOayHMQQNLIJUPtMA
	ADictionary_2_t57BF542F20A63D9C2A5896F5B39567CA3038D7D5 * ___lZlHpVlqScCxOayHMQQNLIJUPtMA_6;
	// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,jqtkirjooyDJUdKqWkqxpLraQfy> Rewired.InputSources.SDL2.SDL2InputSource::XNHrBLdqaHflVjeoKkJynhXGVvLA
	ADictionary_2_tC50755225741EF63FB74BAB10788403F768DCD75 * ___XNHrBLdqaHflVjeoKkJynhXGVvLA_7;
	// gvrkMEBkQRXiZPPNldGUfPXGyiXV_yFEtHIhcxPOKCXrKFpITsmryyaV Rewired.InputSources.SDL2.SDL2InputSource::gsAadmwQcEvbHoCFJlqywgbODlWD
	yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C  ___gsAadmwQcEvbHoCFJlqywgbODlWD_8;
	// Rewired.Utils.Classes.Data.NativeBuffer Rewired.InputSources.SDL2.SDL2InputSource::XrEDRFmNCoDoeWCIjRaYWVBoSJm
	NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * ___XrEDRFmNCoDoeWCIjRaYWVBoSJm_9;
	// System.Action Rewired.InputSources.SDL2.SDL2InputSource::NCzIETftWWWbURtjIpkADaGECAGO
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___NCzIETftWWWbURtjIpkADaGECAGO_10;
	// System.Boolean Rewired.InputSources.SDL2.SDL2InputSource::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_11;

public:
	inline static int32_t get_offset_of_GPamRvCbOttEEABtEfVCeWQVUod_1() { return static_cast<int32_t>(offsetof(SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85, ___GPamRvCbOttEEABtEfVCeWQVUod_1)); }
	inline bool get_GPamRvCbOttEEABtEfVCeWQVUod_1() const { return ___GPamRvCbOttEEABtEfVCeWQVUod_1; }
	inline bool* get_address_of_GPamRvCbOttEEABtEfVCeWQVUod_1() { return &___GPamRvCbOttEEABtEfVCeWQVUod_1; }
	inline void set_GPamRvCbOttEEABtEfVCeWQVUod_1(bool value)
	{
		___GPamRvCbOttEEABtEfVCeWQVUod_1 = value;
	}

	inline static int32_t get_offset_of_DNyylDGzMjSsoFljrEWFZpbwUTp_2() { return static_cast<int32_t>(offsetof(SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85, ___DNyylDGzMjSsoFljrEWFZpbwUTp_2)); }
	inline bool get_DNyylDGzMjSsoFljrEWFZpbwUTp_2() const { return ___DNyylDGzMjSsoFljrEWFZpbwUTp_2; }
	inline bool* get_address_of_DNyylDGzMjSsoFljrEWFZpbwUTp_2() { return &___DNyylDGzMjSsoFljrEWFZpbwUTp_2; }
	inline void set_DNyylDGzMjSsoFljrEWFZpbwUTp_2(bool value)
	{
		___DNyylDGzMjSsoFljrEWFZpbwUTp_2 = value;
	}

	inline static int32_t get_offset_of_cAcffkahmtipZtjyREtLadTZZhr_3() { return static_cast<int32_t>(offsetof(SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85, ___cAcffkahmtipZtjyREtLadTZZhr_3)); }
	inline bool get_cAcffkahmtipZtjyREtLadTZZhr_3() const { return ___cAcffkahmtipZtjyREtLadTZZhr_3; }
	inline bool* get_address_of_cAcffkahmtipZtjyREtLadTZZhr_3() { return &___cAcffkahmtipZtjyREtLadTZZhr_3; }
	inline void set_cAcffkahmtipZtjyREtLadTZZhr_3(bool value)
	{
		___cAcffkahmtipZtjyREtLadTZZhr_3 = value;
	}

	inline static int32_t get_offset_of_JfNLsrbeYBZuaaMZBfeVwFCxYsI_4() { return static_cast<int32_t>(offsetof(SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85, ___JfNLsrbeYBZuaaMZBfeVwFCxYsI_4)); }
	inline bool get_JfNLsrbeYBZuaaMZBfeVwFCxYsI_4() const { return ___JfNLsrbeYBZuaaMZBfeVwFCxYsI_4; }
	inline bool* get_address_of_JfNLsrbeYBZuaaMZBfeVwFCxYsI_4() { return &___JfNLsrbeYBZuaaMZBfeVwFCxYsI_4; }
	inline void set_JfNLsrbeYBZuaaMZBfeVwFCxYsI_4(bool value)
	{
		___JfNLsrbeYBZuaaMZBfeVwFCxYsI_4 = value;
	}

	inline static int32_t get_offset_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5() { return static_cast<int32_t>(offsetof(SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85, ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5)); }
	inline bool get_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5() const { return ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5; }
	inline bool* get_address_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5() { return &___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5; }
	inline void set_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5(bool value)
	{
		___oGbqxjxXGgvgXNLPUMbdLzEDsmT_5 = value;
	}

	inline static int32_t get_offset_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_6() { return static_cast<int32_t>(offsetof(SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85, ___lZlHpVlqScCxOayHMQQNLIJUPtMA_6)); }
	inline ADictionary_2_t57BF542F20A63D9C2A5896F5B39567CA3038D7D5 * get_lZlHpVlqScCxOayHMQQNLIJUPtMA_6() const { return ___lZlHpVlqScCxOayHMQQNLIJUPtMA_6; }
	inline ADictionary_2_t57BF542F20A63D9C2A5896F5B39567CA3038D7D5 ** get_address_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_6() { return &___lZlHpVlqScCxOayHMQQNLIJUPtMA_6; }
	inline void set_lZlHpVlqScCxOayHMQQNLIJUPtMA_6(ADictionary_2_t57BF542F20A63D9C2A5896F5B39567CA3038D7D5 * value)
	{
		___lZlHpVlqScCxOayHMQQNLIJUPtMA_6 = value;
		Il2CppCodeGenWriteBarrier((&___lZlHpVlqScCxOayHMQQNLIJUPtMA_6), value);
	}

	inline static int32_t get_offset_of_XNHrBLdqaHflVjeoKkJynhXGVvLA_7() { return static_cast<int32_t>(offsetof(SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85, ___XNHrBLdqaHflVjeoKkJynhXGVvLA_7)); }
	inline ADictionary_2_tC50755225741EF63FB74BAB10788403F768DCD75 * get_XNHrBLdqaHflVjeoKkJynhXGVvLA_7() const { return ___XNHrBLdqaHflVjeoKkJynhXGVvLA_7; }
	inline ADictionary_2_tC50755225741EF63FB74BAB10788403F768DCD75 ** get_address_of_XNHrBLdqaHflVjeoKkJynhXGVvLA_7() { return &___XNHrBLdqaHflVjeoKkJynhXGVvLA_7; }
	inline void set_XNHrBLdqaHflVjeoKkJynhXGVvLA_7(ADictionary_2_tC50755225741EF63FB74BAB10788403F768DCD75 * value)
	{
		___XNHrBLdqaHflVjeoKkJynhXGVvLA_7 = value;
		Il2CppCodeGenWriteBarrier((&___XNHrBLdqaHflVjeoKkJynhXGVvLA_7), value);
	}

	inline static int32_t get_offset_of_gsAadmwQcEvbHoCFJlqywgbODlWD_8() { return static_cast<int32_t>(offsetof(SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85, ___gsAadmwQcEvbHoCFJlqywgbODlWD_8)); }
	inline yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C  get_gsAadmwQcEvbHoCFJlqywgbODlWD_8() const { return ___gsAadmwQcEvbHoCFJlqywgbODlWD_8; }
	inline yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C * get_address_of_gsAadmwQcEvbHoCFJlqywgbODlWD_8() { return &___gsAadmwQcEvbHoCFJlqywgbODlWD_8; }
	inline void set_gsAadmwQcEvbHoCFJlqywgbODlWD_8(yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C  value)
	{
		___gsAadmwQcEvbHoCFJlqywgbODlWD_8 = value;
	}

	inline static int32_t get_offset_of_XrEDRFmNCoDoeWCIjRaYWVBoSJm_9() { return static_cast<int32_t>(offsetof(SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85, ___XrEDRFmNCoDoeWCIjRaYWVBoSJm_9)); }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * get_XrEDRFmNCoDoeWCIjRaYWVBoSJm_9() const { return ___XrEDRFmNCoDoeWCIjRaYWVBoSJm_9; }
	inline NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 ** get_address_of_XrEDRFmNCoDoeWCIjRaYWVBoSJm_9() { return &___XrEDRFmNCoDoeWCIjRaYWVBoSJm_9; }
	inline void set_XrEDRFmNCoDoeWCIjRaYWVBoSJm_9(NativeBuffer_t8286D68043C990040A4F881140AB359A283A0E11 * value)
	{
		___XrEDRFmNCoDoeWCIjRaYWVBoSJm_9 = value;
		Il2CppCodeGenWriteBarrier((&___XrEDRFmNCoDoeWCIjRaYWVBoSJm_9), value);
	}

	inline static int32_t get_offset_of_NCzIETftWWWbURtjIpkADaGECAGO_10() { return static_cast<int32_t>(offsetof(SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85, ___NCzIETftWWWbURtjIpkADaGECAGO_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_NCzIETftWWWbURtjIpkADaGECAGO_10() const { return ___NCzIETftWWWbURtjIpkADaGECAGO_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_NCzIETftWWWbURtjIpkADaGECAGO_10() { return &___NCzIETftWWWbURtjIpkADaGECAGO_10; }
	inline void set_NCzIETftWWWbURtjIpkADaGECAGO_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___NCzIETftWWWbURtjIpkADaGECAGO_10 = value;
		Il2CppCodeGenWriteBarrier((&___NCzIETftWWWbURtjIpkADaGECAGO_10), value);
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_11() { return static_cast<int32_t>(offsetof(SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85, ___SeCUoinDywZmqZDHRKupOdOaTke_11)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_11() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_11; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_11() { return &___SeCUoinDywZmqZDHRKupOdOaTke_11; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_11(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SDL2INPUTSOURCE_T8D429B4D964DFFEFFD201C897AB0EE90EC232E85_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef JQTKIRJOOYDJUDKQWKQXPLRAQFY_T213101B9F71F7C7BB298482EA3235B897A1411DB_H
#define JQTKIRJOOYDJUDKQWKQXPLRAQFY_T213101B9F71F7C7BB298482EA3235B897A1411DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// jqtkirjooyDJUdKqWkqxpLraQfy
struct  jqtkirjooyDJUdKqWkqxpLraQfy_t213101B9F71F7C7BB298482EA3235B897A1411DB  : public VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JQTKIRJOOYDJUDKQWKQXPLRAQFY_T213101B9F71F7C7BB298482EA3235B897A1411DB_H
#ifndef DEBUGINFORMATION_TD518788A1E39C90E65449F61C26B891F341BE87C_H
#define DEBUGINFORMATION_TD518788A1E39C90E65449F61C26B891F341BE87C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.Tools.DebugInformation
struct  DebugInformation_tD518788A1E39C90E65449F61C26B891F341BE87C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.Boolean> Rewired.Dev.Tools.DebugInformation::aDmWIjXnlIjJQIAHoPsUjSuQYqU
	RuntimeObject* ___aDmWIjXnlIjJQIAHoPsUjSuQYqU_7;

public:
	inline static int32_t get_offset_of_aDmWIjXnlIjJQIAHoPsUjSuQYqU_7() { return static_cast<int32_t>(offsetof(DebugInformation_tD518788A1E39C90E65449F61C26B891F341BE87C, ___aDmWIjXnlIjJQIAHoPsUjSuQYqU_7)); }
	inline RuntimeObject* get_aDmWIjXnlIjJQIAHoPsUjSuQYqU_7() const { return ___aDmWIjXnlIjJQIAHoPsUjSuQYqU_7; }
	inline RuntimeObject** get_address_of_aDmWIjXnlIjJQIAHoPsUjSuQYqU_7() { return &___aDmWIjXnlIjJQIAHoPsUjSuQYqU_7; }
	inline void set_aDmWIjXnlIjJQIAHoPsUjSuQYqU_7(RuntimeObject* value)
	{
		___aDmWIjXnlIjJQIAHoPsUjSuQYqU_7 = value;
		Il2CppCodeGenWriteBarrier((&___aDmWIjXnlIjJQIAHoPsUjSuQYqU_7), value);
	}
};

struct DebugInformation_tD518788A1E39C90E65449F61C26B891F341BE87C_StaticFields
{
public:
	// UnityEngine.Vector2 Rewired.Dev.Tools.DebugInformation::QnRgdIGiakoKCAnglGTFgUpdOOh
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___QnRgdIGiakoKCAnglGTFgUpdOOh_8;
	// System.Comparison`1<Rewired.InputAction> Rewired.Dev.Tools.DebugInformation::xuKcWqHPyZZLojTegosyfuffNaVL
	Comparison_1_t2F2BB3FA13CDC653407D0BEDA5FD4696B0B3BA4D * ___xuKcWqHPyZZLojTegosyfuffNaVL_9;

public:
	inline static int32_t get_offset_of_QnRgdIGiakoKCAnglGTFgUpdOOh_8() { return static_cast<int32_t>(offsetof(DebugInformation_tD518788A1E39C90E65449F61C26B891F341BE87C_StaticFields, ___QnRgdIGiakoKCAnglGTFgUpdOOh_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_QnRgdIGiakoKCAnglGTFgUpdOOh_8() const { return ___QnRgdIGiakoKCAnglGTFgUpdOOh_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_QnRgdIGiakoKCAnglGTFgUpdOOh_8() { return &___QnRgdIGiakoKCAnglGTFgUpdOOh_8; }
	inline void set_QnRgdIGiakoKCAnglGTFgUpdOOh_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___QnRgdIGiakoKCAnglGTFgUpdOOh_8 = value;
	}

	inline static int32_t get_offset_of_xuKcWqHPyZZLojTegosyfuffNaVL_9() { return static_cast<int32_t>(offsetof(DebugInformation_tD518788A1E39C90E65449F61C26B891F341BE87C_StaticFields, ___xuKcWqHPyZZLojTegosyfuffNaVL_9)); }
	inline Comparison_1_t2F2BB3FA13CDC653407D0BEDA5FD4696B0B3BA4D * get_xuKcWqHPyZZLojTegosyfuffNaVL_9() const { return ___xuKcWqHPyZZLojTegosyfuffNaVL_9; }
	inline Comparison_1_t2F2BB3FA13CDC653407D0BEDA5FD4696B0B3BA4D ** get_address_of_xuKcWqHPyZZLojTegosyfuffNaVL_9() { return &___xuKcWqHPyZZLojTegosyfuffNaVL_9; }
	inline void set_xuKcWqHPyZZLojTegosyfuffNaVL_9(Comparison_1_t2F2BB3FA13CDC653407D0BEDA5FD4696B0B3BA4D * value)
	{
		___xuKcWqHPyZZLojTegosyfuffNaVL_9 = value;
		Il2CppCodeGenWriteBarrier((&___xuKcWqHPyZZLojTegosyfuffNaVL_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGINFORMATION_TD518788A1E39C90E65449F61C26B891F341BE87C_H
#ifndef DIRECTINPUTJOYSTICKELEMENTIDENTIFIER_T763CDEC4A6B42080C935F8B08EA0603A43F2CAC8_H
#define DIRECTINPUTJOYSTICKELEMENTIDENTIFIER_T763CDEC4A6B42080C935F8B08EA0603A43F2CAC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.Tools.DirectInputJoystickElementIdentifier
struct  DirectInputJoystickElementIdentifier_t763CDEC4A6B42080C935F8B08EA0603A43F2CAC8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.Interfaces.IElementIdentifierTool Rewired.Dev.Tools.DirectInputJoystickElementIdentifier::svxDhybcftjpWbcQyWPnISfozPvo
	RuntimeObject* ___svxDhybcftjpWbcQyWPnISfozPvo_4;

public:
	inline static int32_t get_offset_of_svxDhybcftjpWbcQyWPnISfozPvo_4() { return static_cast<int32_t>(offsetof(DirectInputJoystickElementIdentifier_t763CDEC4A6B42080C935F8B08EA0603A43F2CAC8, ___svxDhybcftjpWbcQyWPnISfozPvo_4)); }
	inline RuntimeObject* get_svxDhybcftjpWbcQyWPnISfozPvo_4() const { return ___svxDhybcftjpWbcQyWPnISfozPvo_4; }
	inline RuntimeObject** get_address_of_svxDhybcftjpWbcQyWPnISfozPvo_4() { return &___svxDhybcftjpWbcQyWPnISfozPvo_4; }
	inline void set_svxDhybcftjpWbcQyWPnISfozPvo_4(RuntimeObject* value)
	{
		___svxDhybcftjpWbcQyWPnISfozPvo_4 = value;
		Il2CppCodeGenWriteBarrier((&___svxDhybcftjpWbcQyWPnISfozPvo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTINPUTJOYSTICKELEMENTIDENTIFIER_T763CDEC4A6B42080C935F8B08EA0603A43F2CAC8_H
#ifndef JOYSTICKELEMENTIDENTIFIER_TADF70D5A3B3F0A34E5E51F1912E77A57137D43F1_H
#define JOYSTICKELEMENTIDENTIFIER_TADF70D5A3B3F0A34E5E51F1912E77A57137D43F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.Tools.JoystickElementIdentifier
struct  JoystickElementIdentifier_tADF70D5A3B3F0A34E5E51F1912E77A57137D43F1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.Interfaces.IElementIdentifierTool Rewired.Dev.Tools.JoystickElementIdentifier::svxDhybcftjpWbcQyWPnISfozPvo
	RuntimeObject* ___svxDhybcftjpWbcQyWPnISfozPvo_4;

public:
	inline static int32_t get_offset_of_svxDhybcftjpWbcQyWPnISfozPvo_4() { return static_cast<int32_t>(offsetof(JoystickElementIdentifier_tADF70D5A3B3F0A34E5E51F1912E77A57137D43F1, ___svxDhybcftjpWbcQyWPnISfozPvo_4)); }
	inline RuntimeObject* get_svxDhybcftjpWbcQyWPnISfozPvo_4() const { return ___svxDhybcftjpWbcQyWPnISfozPvo_4; }
	inline RuntimeObject** get_address_of_svxDhybcftjpWbcQyWPnISfozPvo_4() { return &___svxDhybcftjpWbcQyWPnISfozPvo_4; }
	inline void set_svxDhybcftjpWbcQyWPnISfozPvo_4(RuntimeObject* value)
	{
		___svxDhybcftjpWbcQyWPnISfozPvo_4 = value;
		Il2CppCodeGenWriteBarrier((&___svxDhybcftjpWbcQyWPnISfozPvo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKELEMENTIDENTIFIER_TADF70D5A3B3F0A34E5E51F1912E77A57137D43F1_H
#ifndef OSXJOYSTICKELEMENTIDENTIFIER_T952B7ACD1D799321678D3EAFCB85B30DAA0417AD_H
#define OSXJOYSTICKELEMENTIDENTIFIER_T952B7ACD1D799321678D3EAFCB85B30DAA0417AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.Tools.OSXJoystickElementIdentifier
struct  OSXJoystickElementIdentifier_t952B7ACD1D799321678D3EAFCB85B30DAA0417AD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.Interfaces.IElementIdentifierTool Rewired.Dev.Tools.OSXJoystickElementIdentifier::svxDhybcftjpWbcQyWPnISfozPvo
	RuntimeObject* ___svxDhybcftjpWbcQyWPnISfozPvo_4;

public:
	inline static int32_t get_offset_of_svxDhybcftjpWbcQyWPnISfozPvo_4() { return static_cast<int32_t>(offsetof(OSXJoystickElementIdentifier_t952B7ACD1D799321678D3EAFCB85B30DAA0417AD, ___svxDhybcftjpWbcQyWPnISfozPvo_4)); }
	inline RuntimeObject* get_svxDhybcftjpWbcQyWPnISfozPvo_4() const { return ___svxDhybcftjpWbcQyWPnISfozPvo_4; }
	inline RuntimeObject** get_address_of_svxDhybcftjpWbcQyWPnISfozPvo_4() { return &___svxDhybcftjpWbcQyWPnISfozPvo_4; }
	inline void set_svxDhybcftjpWbcQyWPnISfozPvo_4(RuntimeObject* value)
	{
		___svxDhybcftjpWbcQyWPnISfozPvo_4 = value;
		Il2CppCodeGenWriteBarrier((&___svxDhybcftjpWbcQyWPnISfozPvo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSXJOYSTICKELEMENTIDENTIFIER_T952B7ACD1D799321678D3EAFCB85B30DAA0417AD_H
#ifndef RAWINPUTJOYSTICKELEMENTIDENTIFIER_T1A2007A30A62574D626091A0E6CD3A80769280CC_H
#define RAWINPUTJOYSTICKELEMENTIDENTIFIER_T1A2007A30A62574D626091A0E6CD3A80769280CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.Tools.RawInputJoystickElementIdentifier
struct  RawInputJoystickElementIdentifier_t1A2007A30A62574D626091A0E6CD3A80769280CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.Interfaces.IElementIdentifierTool Rewired.Dev.Tools.RawInputJoystickElementIdentifier::svxDhybcftjpWbcQyWPnISfozPvo
	RuntimeObject* ___svxDhybcftjpWbcQyWPnISfozPvo_4;

public:
	inline static int32_t get_offset_of_svxDhybcftjpWbcQyWPnISfozPvo_4() { return static_cast<int32_t>(offsetof(RawInputJoystickElementIdentifier_t1A2007A30A62574D626091A0E6CD3A80769280CC, ___svxDhybcftjpWbcQyWPnISfozPvo_4)); }
	inline RuntimeObject* get_svxDhybcftjpWbcQyWPnISfozPvo_4() const { return ___svxDhybcftjpWbcQyWPnISfozPvo_4; }
	inline RuntimeObject** get_address_of_svxDhybcftjpWbcQyWPnISfozPvo_4() { return &___svxDhybcftjpWbcQyWPnISfozPvo_4; }
	inline void set_svxDhybcftjpWbcQyWPnISfozPvo_4(RuntimeObject* value)
	{
		___svxDhybcftjpWbcQyWPnISfozPvo_4 = value;
		Il2CppCodeGenWriteBarrier((&___svxDhybcftjpWbcQyWPnISfozPvo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWINPUTJOYSTICKELEMENTIDENTIFIER_T1A2007A30A62574D626091A0E6CD3A80769280CC_H
#ifndef UNITYJOYSTICKELEMENTIDENTIFIER_T53DD3E56E1B182F4E93E1A9614B54E9517F59A69_H
#define UNITYJOYSTICKELEMENTIDENTIFIER_T53DD3E56E1B182F4E93E1A9614B54E9517F59A69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Dev.Tools.UnityJoystickElementIdentifier
struct  UnityJoystickElementIdentifier_t53DD3E56E1B182F4E93E1A9614B54E9517F59A69  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.Interfaces.IElementIdentifierTool Rewired.Dev.Tools.UnityJoystickElementIdentifier::svxDhybcftjpWbcQyWPnISfozPvo
	RuntimeObject* ___svxDhybcftjpWbcQyWPnISfozPvo_4;

public:
	inline static int32_t get_offset_of_svxDhybcftjpWbcQyWPnISfozPvo_4() { return static_cast<int32_t>(offsetof(UnityJoystickElementIdentifier_t53DD3E56E1B182F4E93E1A9614B54E9517F59A69, ___svxDhybcftjpWbcQyWPnISfozPvo_4)); }
	inline RuntimeObject* get_svxDhybcftjpWbcQyWPnISfozPvo_4() const { return ___svxDhybcftjpWbcQyWPnISfozPvo_4; }
	inline RuntimeObject** get_address_of_svxDhybcftjpWbcQyWPnISfozPvo_4() { return &___svxDhybcftjpWbcQyWPnISfozPvo_4; }
	inline void set_svxDhybcftjpWbcQyWPnISfozPvo_4(RuntimeObject* value)
	{
		___svxDhybcftjpWbcQyWPnISfozPvo_4 = value;
		Il2CppCodeGenWriteBarrier((&___svxDhybcftjpWbcQyWPnISfozPvo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYJOYSTICKELEMENTIDENTIFIER_T53DD3E56E1B182F4E93E1A9614B54E9517F59A69_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4800 = { sizeof (hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4800[8] = 
{
	hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62::get_offset_of_SuIMwsBbcUZLGhqNQGVITQFmMcP_3(),
	hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62::get_offset_of_tnzQdzfTtfgncibXTcLnqGJmJen_4(),
	hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62::get_offset_of_lNSFjKqFeSbuhoIQjAmpEVFFcIrc_5(),
	hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(),
	hSLDmeWgsjAusiendJgPyQcGiqUG_t218C7058E04084BA91430B50B19B6BBA2DDECF62::get_offset_of_BxrkKJNmWBaBYDcqFoeFgiqbiUj_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4801 = { sizeof (wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4801[8] = 
{
	wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1::get_offset_of_boPDfJqwPeTdoZztcumuxTIgoST_3(),
	wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1::get_offset_of_FJwvShZPOLNXNucTHGOTJlqLDHSc_4(),
	wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1::get_offset_of_WdNNTQdPMQeqQYMebCOybxxbGVkr_5(),
	wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(),
	wPTWmWMdweaXmEihlbqiPIvTjVGa_tFC54795E71D69A315C72CEF928655EE9DC54D4B1::get_offset_of_asbdPiIRAXtYLhQAvtCtEExwGcp_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4802 = { sizeof (hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4802[8] = 
{
	hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594::get_offset_of_goIbDVgpvEutUFqWyUXpuJkiCTVS_3(),
	hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594::get_offset_of_ywSeTEzssQbiwQJBJoAPPgfkgAY_4(),
	hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594::get_offset_of_YfnzGrihuaRNzXSsiibwYuqCTdQ_5(),
	hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(),
	hwIgOdJuHYKeqdCNgubgQQzOrOe_tAD431ECCC5E45E80FDDCAE139410C0AA664C9594::get_offset_of_kCjuHwxFHLERkCAJTcXTjgMdlVE_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4803 = { sizeof (GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4803[8] = 
{
	GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3::get_offset_of_ecVfKCwjYJvDOdgndXSGWqsTXPr_3(),
	GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3::get_offset_of_GAbAVVCqauwUxkAiKlsNvuchSwW_4(),
	GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3::get_offset_of_zIFLSDHAPprCQOhpfMbxFRZcnok_5(),
	GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(),
	GubwlqhGsshLfyrPaOWCdkSFlVw_tF87CF04CDDB8E1933BEDEDBDDC701703557CE8E3::get_offset_of_uwGhQmbUmcbCeCzOmRDTWrmDVwD_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4804 = { sizeof (WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4804[8] = 
{
	WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9::get_offset_of_WbMHeDGCeTqztrpHHhmZBHAgUEYy_3(),
	WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9::get_offset_of_ZpXhDjiTNyrLJrMWlIiVHjURjIm_4(),
	WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9::get_offset_of_hMnvSQnHAFQYRoaIAGTtBjKBCsp_5(),
	WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(),
	WaZHscHCegfXfOCkztNzKmzwEVO_tC06959DFE653AA7C842463E76DD8F9FBDDB87FA9::get_offset_of_LnweSeVDxIunxbncolytFHkEVSK_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4805 = { sizeof (gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4805[8] = 
{
	gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A::get_offset_of_JrRgkaHxoLRVgtmeuYKrRIFtXnVW_3(),
	gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A::get_offset_of_KBqIMUDUgsWqaoVxTgcCXnsNhPd_4(),
	gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A::get_offset_of_UMEgPuPnmRmOfcQAubdAHRnEzyYI_5(),
	gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(),
	gxSGPdJSDVWAGakgLdJJvBGWmPD_tE122FF1DDF57B2885BCB67A5CAB321227F33682A::get_offset_of_gygTNkRxZGmHjziBespiNVAKnFL_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4806 = { sizeof (ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531), -1, sizeof(ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4806[1] = 
{
	ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531_StaticFields::get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4807 = { sizeof (JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4807[22] = 
{
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_3(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_DoBlgWmOMuusUcBpsKccsmEFmgP_4(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_5(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_6(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_YtyCpfuMjInPTabavmllkqElczPA_7(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_RjbgzWvPMFXOEdOLgPwRLLJVLIr_8(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_9(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_10(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_11(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_12(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_14(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_15(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_16(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_bKjUZqMHYbTsHXEGFfmLqDeZgwz_17(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_wzhtPpbPsLSdcQjSDfoZDUUrQsy_18(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_noCcVHSPuFvPuZSYGWTvfkGIGFf_19(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_20(),
	JQzdNignFqczrcjvvseGJpQtaPhc_tFF5D06E1B005145129B2D7DD428D52C2B4C00746::get_offset_of_sWhONswpHNEGMJZjXZGrrmQEirr_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4808 = { sizeof (qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4808[16] = 
{
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_3(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_4(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_5(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_8(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_9(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_pjuakmhoKxwCeKjqXCixQCshBsG_11(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_JRVxexbSpXMgYerJjSbfICkccTlH_12(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_lXMTDyjNZuQWTiaDRjjKZxjgLCm_13(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_14(),
	qXhFqqBgeymgFtjyIXRuKCZHrECP_tB540CEBBF22A56BA958AAD6A57B134835AAACFA1::get_offset_of_KpQnLzSuaGpebiUgmOuCQoazIQAK_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4809 = { sizeof (JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4809[20] = 
{
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_3(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_DoBlgWmOMuusUcBpsKccsmEFmgP_4(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_aXrADafiNQkJnrgOhDBJQpEVncQ_5(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_tKMifAOuGrDOrEhrNbSnJWJHjcU_6(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_7(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_8(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_9(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_10(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_12(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_13(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_14(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_bsNKDEZlAuQUJoGEPzpoBCCNfRo_15(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_uMeBkqfeFhSzPdshlBqemBqpelq_16(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_NIbAjgqYgduWFNJPPNQWGAXXFWx_17(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_18(),
	JAhXPuxpAkaquLwLTgLxFNdoNjSg_tFF3F890918CC2394148B9100D2E0FA61946D7CC0::get_offset_of_RMAbZLXlKHjqnjikzXemsVwVkcHN_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4810 = { sizeof (okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4810[16] = 
{
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_3(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_4(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_5(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_8(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_9(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_zsqPQQkoIaugWTqooEEkmuYmBXy_11(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_qBSWTwFKBFQQxNPvIFeSsWlfCdL_12(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_naVxvbejmtjAMGXkyXWPQlkjMKFe_13(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_14(),
	okwnHvvLpxipuZcpVfLdmOWyqte_tB3CDABCD106ED26DC61730F4F5C8397E78F38E47::get_offset_of_AkiDYCiMmPsWOImxmReybvqfaGPI_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4811 = { sizeof (bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4811[20] = 
{
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_3(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_DoBlgWmOMuusUcBpsKccsmEFmgP_4(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_kbjsbIbtGtRzFETStDXQFVzQesi_5(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_CxTcbsASmwiVdEzXBlyvuVcnKaaw_6(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_7(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_8(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_9(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_10(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_11(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_12(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_13(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_14(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_RmKYSukKWpayCjCNpIaEEefVQniq_15(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_nMKUaBUMYWExQhyhfnfRFjaEMpBA_16(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_rGzUQICrxYINoJVAtjlWiMXrTBF_17(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_18(),
	bdJxBckTyfcYwVBXEobWgPTTfVx_t3E6E52617792E5467E39ED3B73CF6B5212EFCD8C::get_offset_of_qZgxZgIGnppMbiffnIhqUypVcJb_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4812 = { sizeof (CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4812[16] = 
{
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_3(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_4(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_5(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_8(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_9(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_dmZRAIkVjUvKcmdJPcXZiYIkiWR_11(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_eniqMbpmkEymlMgBiZFLwORgmDl_12(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_CIeRisDKgBhtRtQeaqjBCBznbdk_13(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_14(),
	CNoUgkqtgkpKOLZDuanACzscSJi_tA4B0684E239A7007DE6B29EEA37C1321D1DC5AF9::get_offset_of_hRHaoNoWJlONxyPDGLaddQVapJQ_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4813 = { sizeof (OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4813[22] = 
{
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_3(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_DoBlgWmOMuusUcBpsKccsmEFmgP_4(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_5(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_6(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_WigXBlhRQEAAKnPZjEeeNdMahyT_7(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_hqJogBNPbbjuJUDUhZmRhiFNzfx_8(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_9(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_10(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_11(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_12(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_13(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_14(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_15(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_16(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_QljwEiMqTqTwBZqpaHrnqQjxQMB_17(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_onoURHsEjHZzeEEizHTuPVhquEU_18(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_gAygEHCgtsXRJGTqpaFpqsnjGYMU_19(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_20(),
	OLmyBIZeljYmEuKKkZcAyuGcrcw_tDE41E2E34F5857309066828A1F7B7105A4A2A462::get_offset_of_IQffKHKdJVcAMiqXIHpHcMMqBjig_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4814 = { sizeof (RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4814[16] = 
{
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_3(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_4(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_5(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_6(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_7(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_8(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_TLUFNqPThThRSpKtpbHMfphhHIql_9(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_GsRgbEdsthhdHbQCaXWTDtLBvoPC_10(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_VlEnMERJfcRXvYVAIopLjkseFixC_11(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_YKbcXcHeYDJiWnszacnrGIAhyNPK_12(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_WcxnYCuzRGCnNdBRgHLUzseFzQZ_13(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_14(),
	RbWRzcmtlGDJyIjpVDDGXZYGgKh_t565B3A874A45DAAB42A171790B091D4CC5D3ADB9::get_offset_of_IZhWSiuleSmkrYdnNJbtOVuwIiA_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4815 = { sizeof (MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6), -1, sizeof(MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4815[1] = 
{
	MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6_StaticFields::get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4816 = { sizeof (PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412), -1, sizeof(PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4816[1] = 
{
	PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412_StaticFields::get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4817 = { sizeof (TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B), -1, sizeof(TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4817[1] = 
{
	TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B_StaticFields::get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4818 = { sizeof (pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4818[5] = 
{
	pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6::get_offset_of_nHlVzWkGpFfUWPJtIRsEsNgLDELG_0(),
	pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6::get_offset_of_sDEzAjQAWfwrpOqYgXBvfAdrDfhg_1(),
	pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6::get_offset_of_WzMPJskYTcPesnNpifYmyBmyoWg_2(),
	pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6::get_offset_of_hmLGQccjeUxxLDHBmtORSRecFvq_3(),
	pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6::get_offset_of_QkiftyHpjHSxNjtYSUvKFpWPMbWG_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4819 = { sizeof (HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4819[9] = 
{
	HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8::get_offset_of_TWKNVXwJTlddHEpGDbHCUyznGXe_0(),
	HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8::get_offset_of_DdAQuNXlVlFtzKHQSEFqDCRtRgt_1(),
	HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8::get_offset_of_FIYhvJgqZRRePhbHOATKhehYewa_2(),
	HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8::get_offset_of_oeJpjybqWWdBoUSIPTRbUtisjll_3(),
	HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8::get_offset_of_NsVTlLVHdMBMZiGpdgoZcLGKPHci_4(),
	HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8::get_offset_of_NtGqRWkVRBBUHEeZvzqBCkzqIAL_5(),
	HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8::get_offset_of_QVhKynZjqsHvJXTGAiWaOYyGjHk_6(),
	HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8::get_offset_of_nVwnLcGgetYmaZOKTSgvAOhbQHP_7(),
	HqnQFyrBLcPwCVhzxoyPgeBoOXL_tFB45CC57DDC04D5886D6BD9C880DEC14A3CB70C8::get_offset_of_UTKDHGiHcPBEaJGmcNIhonLlQYfK_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4820 = { sizeof (prGSMirOcDXXYKABICizKVJUfDY_t71FA8DFC2A0814DE269004402D1B9E4E9C6DAF72), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4821 = { sizeof (UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9), -1, sizeof(UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4821[1] = 
{
	UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9_StaticFields::get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4822 = { sizeof (MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E), -1, sizeof(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4822[15] = 
{
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E::get_offset_of_WGCdlWCIlNjPpctEurlSzZFfPFD_0(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E::get_offset_of_KfeqNphxbTPTdykssKVqYrxbxrc_1(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E::get_offset_of_nFOWbEPonqKitfalvGFCfOPbQQG_2(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E::get_offset_of_nyArQoHIYEElDwAvhKPFdbxYiY_3(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E::get_offset_of_kEjtQBWtabLkjEDzeMaeglYiBmU_4(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E::get_offset_of_AwkUOamRZcvdzKinYnrvOZuIqNl_5(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E::get_offset_of_aYUPMDUNGHqiWFWonBtZAAqDYJi_6(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E::get_offset_of_HQyzNoPvdEapYtzbEmvKxIBiXNW_7(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E::get_offset_of_QKVefOfCLeFRMPSTgLRChgteCu_8(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields::get_offset_of_FehaJfEEjpeYLfpmUMzkFAbCRwsA_9(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields::get_offset_of_GMPHKVIYVSfxfzouOasvunIymPL_10(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields::get_offset_of_mMURPWMpQTiDTaELQZQNlGDUORm_11(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields::get_offset_of_rRiHYDzLJkdCLwJhYbxMYZlGox_12(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields::get_offset_of_EkGbeTUTBdDIoDPcAfixArdMdHyG_13(),
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E_StaticFields::get_offset_of_OJQStWFoTlmLdwbYTlSqDmvhecEF_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4823 = { sizeof (gvrkMEBkQRXiZPPNldGUfPXGyiXV_t0C525DFA1DF74B163B9F31FA2D3AB8D36CBD703F), -1, sizeof(gvrkMEBkQRXiZPPNldGUfPXGyiXV_t0C525DFA1DF74B163B9F31FA2D3AB8D36CBD703F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4823[3] = 
{
	0,
	0,
	gvrkMEBkQRXiZPPNldGUfPXGyiXV_t0C525DFA1DF74B163B9F31FA2D3AB8D36CBD703F_StaticFields::get_offset_of_apqaknfudBKPnUptdxzZmITDZBs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4824 = { sizeof (HVopYEEANXJLkUzfnvduRxvDbht_t95A50A2BC62012F0E55DB118812809287F428C01)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4824[10] = 
{
	HVopYEEANXJLkUzfnvduRxvDbht_t95A50A2BC62012F0E55DB118812809287F428C01::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4825 = { sizeof (nOobeSrFgOqBcHXHXKMtXHhJGas_tF15A216F8E24D6E9AE3A08910336666E8F1E9777)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4825[43] = 
{
	nOobeSrFgOqBcHXHXKMtXHhJGas_tF15A216F8E24D6E9AE3A08910336666E8F1E9777::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4826 = { sizeof (gXjEJSEzMqnStKMbAONmeSGVJmLk_tB57C34DAE50B8D11AF1F0CFB9E6A7CE14D1AA9B6)+ sizeof (RuntimeObject), sizeof(gXjEJSEzMqnStKMbAONmeSGVJmLk_tB57C34DAE50B8D11AF1F0CFB9E6A7CE14D1AA9B6 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4826[2] = 
{
	gXjEJSEzMqnStKMbAONmeSGVJmLk_tB57C34DAE50B8D11AF1F0CFB9E6A7CE14D1AA9B6::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	gXjEJSEzMqnStKMbAONmeSGVJmLk_tB57C34DAE50B8D11AF1F0CFB9E6A7CE14D1AA9B6::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4827 = { sizeof (OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B)+ sizeof (RuntimeObject), sizeof(OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B ), 0, 0 };
extern const int32_t g_FieldOffsetTable4827[9] = 
{
	OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B::get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B::get_offset_of_TfvRQYcdiDtBgzDqHlgvfCimrmM_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B::get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B::get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B::get_offset_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B::get_offset_of_JCCrdLSGQdggHRDYqAACGkbYovTE_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OPZOlOvCSbbrRqTbZhILIdWdFWXS_t9B07FA9A4C3FC7344D92729CF6A5C0BA6ACC198B::get_offset_of_BXVWEQMXuThJrooJLjbaKoYxuZu_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4828 = { sizeof (DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A)+ sizeof (RuntimeObject), sizeof(DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A ), 0, 0 };
extern const int32_t g_FieldOffsetTable4828[8] = 
{
	DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A::get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A::get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A::get_offset_of_NZprZllnPiLWyPKXEjljqjKNXuz_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A::get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A::get_offset_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DgUhlnCmLHmyCdCMyeWgudIaqpKx_tADCE04D8F2CDEB3A0B84C2A59B762079FB6F882A::get_offset_of_LdQqoliuFDJnxKCXeTnZQrzENmL_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4829 = { sizeof (qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8)+ sizeof (RuntimeObject), sizeof(qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4829[6] = 
{
	qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8::get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8::get_offset_of_GbhogeXOuqAWXhmgfRAmudFOsEia_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8::get_offset_of_UeDsJytcqoNApcyYIRGUvxpadRB_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	qpcCNcBvDyTpDgTFwQEMfAeeKsdB_t983CEE3A5CE71A9C79D04A32E9C25732068BE4B8::get_offset_of_TlEmNMFynCEISyuvbnTPLeTdELX_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4830 = { sizeof (TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C)+ sizeof (RuntimeObject), sizeof(TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C ), 0, 0 };
extern const int32_t g_FieldOffsetTable4830[9] = 
{
	TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C::get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C::get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C::get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C::get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C::get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C::get_offset_of_YoIVBUOGrzDHUpJJHxXhsIPTqVX_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TJBFDUJLuUnjSERTgbqWlvNUxcBN_tF7575DDBE7C1E0576AE53D90CA237CE702106E9C::get_offset_of_fefuBVpjOXbPneAFgnpstaUWSLV_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4831 = { sizeof (pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43)+ sizeof (RuntimeObject), sizeof(pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4831[10] = 
{
	pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43::get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43::get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43::get_offset_of_csbagSaGlXaYrxByCxECLgSeBxGq_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43::get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43::get_offset_of_ZaXEmLWJbJqNTzeAQNxcrKINBnt_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43::get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43::get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	pNgxOKFNJPJlFYOVphTRMlSPwhz_t6371EFE4FFE1D2461585A7D6F18A3A559F3A7C43::get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4832 = { sizeof (TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62)+ sizeof (RuntimeObject), sizeof(TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4832[6] = 
{
	TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62::get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62::get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62::get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TXmcBcaOfZoXYnUVYHZlNyYQOLcB_tF7CCD8C10A8BD371E967558358FEAFBA067AEC62::get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4833 = { sizeof (PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E)+ sizeof (RuntimeObject), sizeof(PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E ), 0, 0 };
extern const int32_t g_FieldOffsetTable4833[9] = 
{
	PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E::get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E::get_offset_of_SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E::get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E::get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E::get_offset_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E::get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoESixqIoOnzTYjigIYOyJuWcuO_t0058692460D618BAF0F68903A4E3ED5BD0B3857E::get_offset_of_rcnYqJxaOwhrLxwckBmxkdQUgyvz_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4834 = { sizeof (JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7)+ sizeof (RuntimeObject), sizeof(JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4834[9] = 
{
	JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7::get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7::get_offset_of_XbjzjtekkFzjyzBDCNprtMTvRMx_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7::get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7::get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7::get_offset_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7::get_offset_of_YoIVBUOGrzDHUpJJHxXhsIPTqVX_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsSrNMiUWvPaoUYnokkJplOPpIs_t5A2D089478D4FB57F0818EC67EE83BBB97D6B5E7::get_offset_of_fefuBVpjOXbPneAFgnpstaUWSLV_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4835 = { sizeof (vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457)+ sizeof (RuntimeObject), sizeof(vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4835[7] = 
{
	vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457::get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457::get_offset_of_tnahePnlIzETNObNeoddamMSBEbG_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457::get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457::get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	vnUZdxGbbKheGOMPoylCsjnialt_t7FFB4FAF7D913429B8DE36EC4479B852D5DFE457::get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4836 = { sizeof (xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD)+ sizeof (RuntimeObject), sizeof(xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD ), 0, 0 };
extern const int32_t g_FieldOffsetTable4836[7] = 
{
	xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD::get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD::get_offset_of_csbagSaGlXaYrxByCxECLgSeBxGq_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD::get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD::get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	xLNiqspMGfoLDyZKWDwgBkIaPew_t05F6835A321E8A5CF357A90CA40197A389D95CFD::get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4837 = { sizeof (HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554)+ sizeof (RuntimeObject), sizeof(HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4837[3] = 
{
	HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HdoHFYadSWiBUzqWnduNfIPFVazE_tCFC0698F326902D5EDDF9992104023027410A554::get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4838 = { sizeof (eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A)+ sizeof (RuntimeObject), sizeof(eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A ), 0, 0 };
extern const int32_t g_FieldOffsetTable4838[9] = 
{
	eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A::get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A::get_offset_of_SkTLbmFuuQqlUrgpLBTMhHVWQDIr_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A::get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A::get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A::get_offset_of_lbxzKBvrbNcnDcVvpWWCKOWJRDOM_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A::get_offset_of_ujWJxEJcSVHSdnQYrtHnXDWQpnZ_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	eCLHpuFWjrdUHnCTkwDUJptJKlF_t8520523D67DE64D00205144F8C3992FCC29B537A::get_offset_of_rcnYqJxaOwhrLxwckBmxkdQUgyvz_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4839 = { sizeof (QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287)+ sizeof (RuntimeObject), sizeof(QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4839[7] = 
{
	QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287::get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287::get_offset_of_csbagSaGlXaYrxByCxECLgSeBxGq_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287::get_offset_of_IyCUGfzcPoSzeXqQnApzwoMSQTs_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287::get_offset_of_BfGikzYXODBhJWbccUmzPNgAJjN_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QpTpOKhmTAOfldDXoBXERWoHGYae_tF7B59678BFE350B1F54AE0C2586209BB47960287::get_offset_of_ZdxNPrFvibjxZCDAFwWLoADXmOO_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4840 = { sizeof (rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D)+ sizeof (RuntimeObject), sizeof(rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D ), 0, 0 };
extern const int32_t g_FieldOffsetTable4840[3] = 
{
	rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	rSlcihBflhYIvceVOVZgmAZugcEW_t28FEF23AC71F37882271DAE1876C5FEB21E9633D::get_offset_of_ZXOWQoAgWuCntwkclqxZSCDQZSd_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4841 = { sizeof (ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55)+ sizeof (RuntimeObject), sizeof(ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4841[9] = 
{
	ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55::get_offset_of_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55::get_offset_of_figUHZpamkMwMcBcFCxvFEGezpWG_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55::get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55::get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55::get_offset_of_wCvyujpPzllFaNLVlAidehkVJQfr_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55::get_offset_of_zYigHpHMdfbVRBdTNqJLRlhpVtg_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ofjvnEnqcArQYBqdtgKEKrZmblE_tF0DD22883960F89C9077D66214D33DCFD74BEE55::get_offset_of_TqfnCtqiMsHsZAmsVfRQfULPGnGz_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4842 = { sizeof (NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A)+ sizeof (RuntimeObject), sizeof(NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A ), 0, 0 };
extern const int32_t g_FieldOffsetTable4842[9] = 
{
	NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A::get_offset_of_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A::get_offset_of_vcxVnsZAdtFdxHDnEAdvlLzolNO_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A::get_offset_of_BjsLAHUTiYpFFQHPyuxDpPpFPMK_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A::get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A::get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A::get_offset_of_KAuIbltThGGoDXvARunxJofUqEd_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NtNEqkvvHzJjJAIPkyadPIubYhC_t533D3E9A605971771B256E7F1D1A399D298CF67A::get_offset_of_ijDYEGjuhTUlGZCjrQEIeQUbThP_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4843 = { sizeof (qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3)+ sizeof (RuntimeObject), sizeof(qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4843[8] = 
{
	qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3::get_offset_of_pdPsFQqEdLGUVvbWeUdQOqKnLLk_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3::get_offset_of_ktvAhGgThCYiOvCVXNNYQGLQLfdY_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3::get_offset_of_KAuIbltThGGoDXvARunxJofUqEd_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3::get_offset_of_dPOHGhbKcJeNtFkPLrsbEVlByKNa_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3::get_offset_of_IPHGhnvmvweeXFsNSmpniuYFNHcM_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	qjTGMNjcFKAUPDTCGWIMouSasUP_tEC3374BA3618EA5CF2A1AF5BA7D650BC2C3FACB3::get_offset_of_ycICvmDyXClaVDHBtBHHgVBlmCs_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4844 = { sizeof (kpsJuWrmSpeizdCUDTWKGgMUqHZ_tEAC79B0FA254D95AD6F34B3EB910BF2B130134DE)+ sizeof (RuntimeObject), sizeof(kpsJuWrmSpeizdCUDTWKGgMUqHZ_tEAC79B0FA254D95AD6F34B3EB910BF2B130134DE ), 0, 0 };
extern const int32_t g_FieldOffsetTable4844[3] = 
{
	kpsJuWrmSpeizdCUDTWKGgMUqHZ_tEAC79B0FA254D95AD6F34B3EB910BF2B130134DE::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	kpsJuWrmSpeizdCUDTWKGgMUqHZ_tEAC79B0FA254D95AD6F34B3EB910BF2B130134DE::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	kpsJuWrmSpeizdCUDTWKGgMUqHZ_tEAC79B0FA254D95AD6F34B3EB910BF2B130134DE::get_offset_of_RXiIQIWsCHgXoujlGlykSAGJygs_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4845 = { sizeof (VKuCxQVsMAEmyTTdwEvzLtQeGyK_t40E5E49C17B8992C29B6E46D2CBDD10BDA91B7D8)+ sizeof (RuntimeObject), sizeof(VKuCxQVsMAEmyTTdwEvzLtQeGyK_t40E5E49C17B8992C29B6E46D2CBDD10BDA91B7D8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4845[2] = 
{
	VKuCxQVsMAEmyTTdwEvzLtQeGyK_t40E5E49C17B8992C29B6E46D2CBDD10BDA91B7D8::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VKuCxQVsMAEmyTTdwEvzLtQeGyK_t40E5E49C17B8992C29B6E46D2CBDD10BDA91B7D8::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4846 = { sizeof (NDQBNoTBlOsBJEcRmEBQLJaXoIr_t45A7DACB04A60DE3316D0CC63C13D6E532F98E45)+ sizeof (RuntimeObject), sizeof(NDQBNoTBlOsBJEcRmEBQLJaXoIr_t45A7DACB04A60DE3316D0CC63C13D6E532F98E45 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4846[2] = 
{
	NDQBNoTBlOsBJEcRmEBQLJaXoIr_t45A7DACB04A60DE3316D0CC63C13D6E532F98E45::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NDQBNoTBlOsBJEcRmEBQLJaXoIr_t45A7DACB04A60DE3316D0CC63C13D6E532F98E45::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4847 = { sizeof (TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8)+ sizeof (RuntimeObject), sizeof(TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4847[6] = 
{
	TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8::get_offset_of_NkqzOUnpKxOLEIHqzuEvIwFSEWZ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8::get_offset_of_wKqdKqPaFDoabmkqRipBJbznWve_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8::get_offset_of_JCCrdLSGQdggHRDYqAACGkbYovTE_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TWCQEOkPJAfFkkNkImwxJAjkgSqX_t801E01FE2FDE0F437BA6E293CAD74A5139EF7FD8::get_offset_of_BXVWEQMXuThJrooJLjbaKoYxuZu_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4848 = { sizeof (hhRqyTmYbqEBcxxHkLGAeeKuLcT_t35500CC806EBB8E7E8386D51AFCC5C002FFE5BD4)+ sizeof (RuntimeObject), sizeof(hhRqyTmYbqEBcxxHkLGAeeKuLcT_t35500CC806EBB8E7E8386D51AFCC5C002FFE5BD4 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4848[3] = 
{
	hhRqyTmYbqEBcxxHkLGAeeKuLcT_t35500CC806EBB8E7E8386D51AFCC5C002FFE5BD4::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	hhRqyTmYbqEBcxxHkLGAeeKuLcT_t35500CC806EBB8E7E8386D51AFCC5C002FFE5BD4::get_offset_of_HRIDVrOGCGQQtYMmaCfxzAAVaqs_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	hhRqyTmYbqEBcxxHkLGAeeKuLcT_t35500CC806EBB8E7E8386D51AFCC5C002FFE5BD4::get_offset_of_wVefQBdoQiPKrKrrWaBgrbMVAwre_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4849 = { sizeof (yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C)+ sizeof (RuntimeObject), sizeof(yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C ), 0, 0 };
extern const int32_t g_FieldOffsetTable4849[10] = 
{
	0,
	yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C::get_offset_of_SVwdWTVDBhPwSUmnjUWSqtqsoJB_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C::get_offset_of_QMJZUMSHcwNerFwvvKHDkmLFfyY_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C::get_offset_of_CswBtlDeNeFLQjWnXwOXEBObaOjC_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C::get_offset_of_mCHiRPjvgBJsFyjkeNVSKqdXPgnk_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C::get_offset_of_bPsRdrmftJHejLOgJTuTCFACGs_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C::get_offset_of_wSBHiuDfzRgllDAmllAFEAycDtDb_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C::get_offset_of_YiMJltQnEfZXHYswbcydPFFUPFr_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	yFEtHIhcxPOKCXrKFpITsmryyaV_t298F3A1DBA894A1D6CB374D029A41D355872E97C::get_offset_of_NwfCHMEtrKDekteRIBiyrFAXKhWi_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4850 = { sizeof (CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A)+ sizeof (RuntimeObject), sizeof(CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A ), 0, 0 };
extern const int32_t g_FieldOffsetTable4850[4] = 
{
	CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A::get_offset_of_MjAkTUfgJOwhLMrDUgbOBSkgoJh_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A::get_offset_of_anEZvqTOCnVJRFCZbeXXfTPchXyD_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A::get_offset_of_eoSBvxdKQXbBQxZCRzHwAmstPuKH_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CJxQDhrlajjtvIBURZzUvXUWUbo_tC6BB0EBB602B4DD74194B923C3E533E15CF5DE9A::get_offset_of_tpBctiuLUtOkwlAyUwwLNoEbBRW_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4851 = { sizeof (LHFcKEsNXygGejIjuHtBhpgXxFm_tC5E7CAB207379A9CC35EC5CE2E58CC202232E31A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4851[243] = 
{
	LHFcKEsNXygGejIjuHtBhpgXxFm_tC5E7CAB207379A9CC35EC5CE2E58CC202232E31A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4852 = { sizeof (URRWTjPTthjUUeKFbfCfsKkkiZI_t95677B57949B3940BEE03C06F6DA325036AE3D61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4852[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4853 = { sizeof (NuilWzwYVWksUeGyjvmBrzbRpDb_t548FC98C7A261E12B46365964198415A5130672A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4853[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4854 = { sizeof (IntPtrWrapper_tC0106F75B7B197945867CAE3172E1F8D6419CDF7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4854[1] = 
{
	IntPtrWrapper_tC0106F75B7B197945867CAE3172E1F8D6419CDF7::get_offset_of_oGMSBvcDAaFiWyWelymrQnRYvlG_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4855 = { sizeof (KDIcNAKNsuNvLiMepNxDAEccLSR_tA3DD4DB1AB688F1291A2B02EC7E3CFD5A0829709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4856 = { sizeof (YdwyOOdkZmMIFKVNTmZzPZDktYt_t8E03656705A41147983FF05F894F534859833196), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4857 = { sizeof (NRJfjbKpxmoothnHwkmZDERiGXmR_tB1BF01420858D9DD80021FC48A2613BDEA02C91A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4858 = { sizeof (XinyVXTRfxekDvrLaNfmyUwaYzV_tFC161390B4FF558F7F27D77244EC0FA850BF5C40), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4859 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4860 = { sizeof (OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4860[11] = 
{
	OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533::get_offset_of_sUZJrREVbDgnurDWKIIUlQKqWwq_0(),
	OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533::get_offset_of_umQghCmsfCFpiiISpgIfIkmOcxB_1(),
	OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533::get_offset_of_ohwKaeWdBHSsqiXAUCNnodIzwsY_2(),
	OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533::get_offset_of_dyaUNLHBIeUikBzFJeaTDfszbXPG_3(),
	OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533::get_offset_of_ffNWJUuxPxuFMIZszOmCdhFAkUp_4(),
	OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533::get_offset_of_RnljUVWuXLIugvtjrPQPUGngyWX_5(),
	OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533::get_offset_of_bgdXMWDMghwHHNuDKlbXfHeIkOO_6(),
	OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533::get_offset_of_QNbgrVHxgRFXwNcjxsxdFGgPDyIT_7(),
	OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533::get_offset_of_YJKKCodZAAdHLAznZhhKAnRrzmS_8(),
	OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533::get_offset_of_PlgzRPAQDKiUowbwIUFftEMvgpZ_9(),
	OBseVQmynUNRMiejCOveEisbrKY_t344CF2A67444B48D7C343EFAE6FA77658C73D533::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4861 = { sizeof (jpOdckCKEIweHeVgKkrPQyjNezi_t762B967C0BA200484BEAB356DDE26EBC18B7D25E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4861[23] = 
{
	jpOdckCKEIweHeVgKkrPQyjNezi_t762B967C0BA200484BEAB356DDE26EBC18B7D25E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4862 = { sizeof (MBvfZmieGEGukQujbekkbfrRyOF_tAAAE57B3F8C1999A05711B9BAD359271AC95FBAC)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4862[5] = 
{
	MBvfZmieGEGukQujbekkbfrRyOF_tAAAE57B3F8C1999A05711B9BAD359271AC95FBAC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4863 = { sizeof (IChXVdPvWhSSbdzlSYeTZTMYrdS_tBD87306C8F04931182AF282F823D03CCE7174316)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4863[6] = 
{
	IChXVdPvWhSSbdzlSYeTZTMYrdS_tBD87306C8F04931182AF282F823D03CCE7174316::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4864 = { sizeof (FqPHkOCAePhjWmNNlBQZdTcjYOsS_t2BF2BA133B1B8F66920F86CAA3DC54E8DAB83F0D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4864[9] = 
{
	FqPHkOCAePhjWmNNlBQZdTcjYOsS_t2BF2BA133B1B8F66920F86CAA3DC54E8DAB83F0D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4865 = { sizeof (HMkCBnHBWiLpjGRZSgxrJAWYBzwJ_tB883F030C68CFF0B7BC4ED35A6CDFEF96AB51CCE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4865[18] = 
{
	HMkCBnHBWiLpjGRZSgxrJAWYBzwJ_tB883F030C68CFF0B7BC4ED35A6CDFEF96AB51CCE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4866 = { sizeof (ydiMzuFfVMUntoIIJEytBOmbAlJ_tD984A3F451D6EC1352F9810F5C89709BB0F53387)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4866[3] = 
{
	ydiMzuFfVMUntoIIJEytBOmbAlJ_tD984A3F451D6EC1352F9810F5C89709BB0F53387::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4867 = { sizeof (qgjNESsSnzSlMpHwBtxQPCMNOwq_t763F7594985FF852608F0BE4356621CC5A42DE8D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4867[5] = 
{
	qgjNESsSnzSlMpHwBtxQPCMNOwq_t763F7594985FF852608F0BE4356621CC5A42DE8D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4868 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4869 = { sizeof (VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4869[9] = 
{
	VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9::get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_11(),
	VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9::get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_12(),
	VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9::get_offset_of_ZWYAwyuulqYogftJTkDBnLhijEo_13(),
	VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9::get_offset_of_BQhIlyGtJuwhVjMyFqaFVZMDOIY_14(),
	VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9::get_offset_of_CGnOFilUdwgpDeadACHppWhTHbH_15(),
	VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9::get_offset_of_gsGCoHGepWkEDIBokgAUxNPLZsGv_16(),
	VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9::get_offset_of_fjJgMqsiEShgwfdbppebypKrcGwO_17(),
	VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9::get_offset_of_cznagYJJEGkibwvDkNZVUkbOnma_18(),
	VIhaqNNVJFgtSIZzvDNBXPXkcJWN_t0284CDE88A16E6F3D2374205C61C13AAF40E69D9::get_offset_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4870 = { sizeof (jqtkirjooyDJUdKqWkqxpLraQfy_t213101B9F71F7C7BB298482EA3235B897A1411DB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4871 = { sizeof (qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4871[13] = 
{
	0,
	qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B::get_offset_of_ytEpmEgckhdtBXUMvGmTeXChJTg_6(),
	qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B::get_offset_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_7(),
	qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B::get_offset_of_GZolyQbnhDcIglfHmqdnClfPLwP_8(),
	qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B::get_offset_of_iswBkwCyDabLmfxdAYpLbXBWfvHN_9(),
	qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B::get_offset_of_cteekJgqyXKoMIQGdQUhjuJXMhSq_10(),
	qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B::get_offset_of_nEiQhTKZDBYqGTkqzeEgrkzxQan_11(),
	qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B::get_offset_of_fVwZoOiZNlMKSCdQWxpuPNZKDrf_12(),
	qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B::get_offset_of_GPamRvCbOttEEABtEfVCeWQVUod_13(),
	qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B::get_offset_of_cAcffkahmtipZtjyREtLadTZZhr_14(),
	qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B::get_offset_of_JfNLsrbeYBZuaaMZBfeVwFCxYsI_15(),
	qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B::get_offset_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_16(),
	qQSKQqvJRxFNPvSjTomQuZOntrH_t90D9ECA2A0A9866470299ACBD3CED42FB841B98B::get_offset_of_ZGtOAcUCAuhJuSLEHTELVSEYNBN_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4872 = { sizeof (lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4872[29] = 
{
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_avoCosfGauXluftrTLZWMUMjMSvo_0(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_vHhTUeEjpjXweHfcvokTPubqCBh_1(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_mMvnhxixERRYbVLPXEwyRrtXYwt_2(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_espHBjclwGrxdYkIErbggrUHnMo_3(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_uyozhMHHhGNcfEwvclTjPxbbvxZ_4(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_hBqzkAFqJcxfwCrSYqTdWPhwRxi_5(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_amhGqEYcVGlMzaBhudMaalhDCbSB_6(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_zTBxQtRhsiyWjlXakgtukSbwLlp_7(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_KclFaRHPVyrRHSfCxDIVgvgADJgN_8(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_qNNdzuExLuNfrdyOzaNcsNVrpIQc_9(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_RhEAYdViDLywbAAxYxVbxUVxiMM_10(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_yauaFjfmZQzNovSFRGthYyFJrrj_11(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_bLJDeZdmqhplurpuKUnpKCpNysd_12(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_NQcfCUdrzCInuZTLuEEoKhWjImdc_13(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_BIMocAZqmOYbcFTwmpmMNqQvglvi_14(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_AfHFnomuXngvpySlBnOGFBvmcpy_15(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_UdWYvkBYkFbtQVrPZwZuKINGjC_16(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_VYSASJjqEIsmJcmnIHADUYEaRjQC_17(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_aWhbUpAtiftmjIGzRSoPwmEnzb_18(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_ibgNLLocbIyPDBErUPhHUTjDZKC_19(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_ZsEfMISqALipCaWIrZdQwCvMLkx_20(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_YJKKCodZAAdHLAznZhhKAnRrzmS_21(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_iKsRvufntavyFjWZbUibgspoftw_22(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_YuSoQESZTtPuhZKVJFBavCSGQsh_23(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_motlnrXwRclwbbdquGWkOEYsYFy_24(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_QEAEcDsIVKSxlRksWqQtzkbPdqrh_25(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_JKdifYgUghWnBkRBRChckuzXKWst_26(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_uTrFqghzwmUUNLMxoQSRVgfDfsj_27(),
	lNgIgGeozTFHoKvpOBlVjbAtvLyd_t36643A533B08EAB0B35FCE169CD422956CFC6489::get_offset_of_pwFxxEkPXgSZokcVFZSbwgxzpUm_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4873 = { sizeof (tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4873[1] = 
{
	tLqZqETDWvmFaUyFhoSscPGCHa_tA4EB400BF4EDD99C5E5B153D9D3638ED5B60F773::get_offset_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4874 = { sizeof (zSsAVpfNUYBPgVxqQMqbVEPVqJCO_t2FC14182EE0D01A0915FBE74BC6FE5FE071F7B68)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4874[3] = 
{
	zSsAVpfNUYBPgVxqQMqbVEPVqJCO_t2FC14182EE0D01A0915FBE74BC6FE5FE071F7B68::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4875 = { sizeof (QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4875[7] = 
{
	QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4::get_offset_of_nvorGNbxnIKRTEQakQRxoDaDbkF_0(),
	QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4::get_offset_of_nlEaopEverUmqALsSlwZYfPupTlP_1(),
	QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4::get_offset_of_bLJDeZdmqhplurpuKUnpKCpNysd_2(),
	QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4::get_offset_of_fYYSPmKTkyNnvcLPwwaCPYkcWqv_3(),
	QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4::get_offset_of_UdWYvkBYkFbtQVrPZwZuKINGjC_4(),
	QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4::get_offset_of_VYSASJjqEIsmJcmnIHADUYEaRjQC_5(),
	QnHiHbnYGCBSFowNhmjvNvYtpXM_t3E46BD6CE57136454CD812EB6A63FF2ADCC0C6F4::get_offset_of_aWhbUpAtiftmjIGzRSoPwmEnzb_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4876 = { sizeof (sSmyCZxlYXGixusuhxfHceqeCTr_t23709CD7AD3923255200D20AD2F6D7232F5C3B26), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4877 = { sizeof (SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4877[12] = 
{
	0,
	SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85::get_offset_of_GPamRvCbOttEEABtEfVCeWQVUod_1(),
	SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85::get_offset_of_DNyylDGzMjSsoFljrEWFZpbwUTp_2(),
	SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85::get_offset_of_cAcffkahmtipZtjyREtLadTZZhr_3(),
	SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85::get_offset_of_JfNLsrbeYBZuaaMZBfeVwFCxYsI_4(),
	SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85::get_offset_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_5(),
	SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85::get_offset_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_6(),
	SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85::get_offset_of_XNHrBLdqaHflVjeoKkJynhXGVvLA_7(),
	SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85::get_offset_of_gsAadmwQcEvbHoCFJlqywgbODlWD_8(),
	SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85::get_offset_of_XrEDRFmNCoDoeWCIjRaYWVBoSJm_9(),
	SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85::get_offset_of_NCzIETftWWWbURtjIpkADaGECAGO_10(),
	SDL2InputSource_t8D429B4D964DFFEFFD201C897AB0EE90EC232E85::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4878 = { sizeof (HSTSymgCWOHMjHdStxXvytwOOps_tE971DE592BCB2FA4DEFAD66F81CF054B19AC7235), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4879 = { sizeof (wyiDiFuVUUXBMxIcVftzXDHorqT_t9793B783AA7E7FD2AA8A17EE2FE5F1989BF5E02E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4880 = { sizeof (mSpXmiwJNMLLEknWBnlNXlKweguk_t82684D4A5C90A79EB8A55071BFD9AC830563CDBC), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4881 = { sizeof (SCQqbeyGtpFZvHIREBLSXZgPUgi_tBB049F5394A5E47B148D20B4B3255754F5F1157B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4882 = { sizeof (owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4882[15] = 
{
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_JTlYHbNuRvgezaodmDszddIQvhCD_0(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_1(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_MSRWTMYePuQljcKSSgqgZVJnxaO_2(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_PILNSwvruoIylrlWZVyEHrlXRBt_3(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_zTBxQtRhsiyWjlXakgtukSbwLlp_4(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_RZDzxZHLcHGhqfWjRESXQZBgUPw_5(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_DyFaiKFDPJYHiKaiDwSFsqplZQr_6(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_7(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_8(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_ZWYAwyuulqYogftJTkDBnLhijEo_9(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_BQhIlyGtJuwhVjMyFqaFVZMDOIY_10(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_yauaFjfmZQzNovSFRGthYyFJrrj_11(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_nlEaopEverUmqALsSlwZYfPupTlP_12(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_qNNdzuExLuNfrdyOzaNcsNVrpIQc_13(),
	owCsobmYStIRvNUHffmjMFLdZEz_tA61D69AED297CF98D852CC503479D02181B74628::get_offset_of_KclFaRHPVyrRHSfCxDIVgvgADJgN_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4883 = { sizeof (mPQCgvJVqccLNQdbbCYXrpOszqv_tC2A3F8E7F287CF9FB37D2D1A1ECE971704C02252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4884 = { sizeof (DebugInformation_tD518788A1E39C90E65449F61C26B891F341BE87C), -1, sizeof(DebugInformation_tD518788A1E39C90E65449F61C26B891F341BE87C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4884[6] = 
{
	0,
	0,
	0,
	DebugInformation_tD518788A1E39C90E65449F61C26B891F341BE87C::get_offset_of_aDmWIjXnlIjJQIAHoPsUjSuQYqU_7(),
	DebugInformation_tD518788A1E39C90E65449F61C26B891F341BE87C_StaticFields::get_offset_of_QnRgdIGiakoKCAnglGTFgUpdOOh_8(),
	DebugInformation_tD518788A1E39C90E65449F61C26B891F341BE87C_StaticFields::get_offset_of_xuKcWqHPyZZLojTegosyfuffNaVL_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4885 = { sizeof (FLTzelIMaReBqqwAlotpdtoqUOK_t7B8E84D261131F81F9CB39918B4CCA94CD9517C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4885[1] = 
{
	FLTzelIMaReBqqwAlotpdtoqUOK_t7B8E84D261131F81F9CB39918B4CCA94CD9517C6::get_offset_of_IZxaLXvQArYNNUktxzvawNmZpkj_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4886 = { sizeof (PNOULhjPFHAvIIYXkBbDJmOxcjQ_t8C79150C2E842D92DD73EC11D2E1A54C1583F1CB), -1, sizeof(PNOULhjPFHAvIIYXkBbDJmOxcjQ_t8C79150C2E842D92DD73EC11D2E1A54C1583F1CB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4886[1] = 
{
	PNOULhjPFHAvIIYXkBbDJmOxcjQ_t8C79150C2E842D92DD73EC11D2E1A54C1583F1CB_StaticFields::get_offset_of_wpuUcfVnfsggekXSGAdRLcxuPvec_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4887 = { sizeof (KGCeLmPShpTaYNvjfwSOtyhDrqk_t9A56EFA94D7770D79F2AFA3C2D84A42F97E51F6E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4888 = { sizeof (mImfYjRnIGQWWaSoPiKbCoGdLOb_tE836FB405B4F6F3AACBE2D8BA9F6E7C26F4BDF99), -1, sizeof(mImfYjRnIGQWWaSoPiKbCoGdLOb_tE836FB405B4F6F3AACBE2D8BA9F6E7C26F4BDF99_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4888[2] = 
{
	mImfYjRnIGQWWaSoPiKbCoGdLOb_tE836FB405B4F6F3AACBE2D8BA9F6E7C26F4BDF99_StaticFields::get_offset_of_zMcnnBFtPTJiMXPQbYfzZbFGcSfi_0(),
	mImfYjRnIGQWWaSoPiKbCoGdLOb_tE836FB405B4F6F3AACBE2D8BA9F6E7C26F4BDF99_StaticFields::get_offset_of_jZGZszBGmrMHTMrsAGOwSgYfhFd_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4889 = { sizeof (MnwygNQCnSFAUaAlhkvZjhpOqiCX_tBBC8F63C8A5D931070FEC280ABB619D506033D2C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4889[5] = 
{
	MnwygNQCnSFAUaAlhkvZjhpOqiCX_tBBC8F63C8A5D931070FEC280ABB619D506033D2C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4890 = { sizeof (eIOucXQXPidleIRrXPQzebZFhcX_t037D03A46FB198374E670DD87D8ADBE71D8E808D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4890[1] = 
{
	eIOucXQXPidleIRrXPQzebZFhcX_t037D03A46FB198374E670DD87D8ADBE71D8E808D::get_offset_of_NutGqeIClygQlMZLGvMceGuLZh_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4891 = { sizeof (DirectInputJoystickElementIdentifier_t763CDEC4A6B42080C935F8B08EA0603A43F2CAC8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4891[1] = 
{
	DirectInputJoystickElementIdentifier_t763CDEC4A6B42080C935F8B08EA0603A43F2CAC8::get_offset_of_svxDhybcftjpWbcQyWPnISfozPvo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4892 = { sizeof (JoystickElementIdentifier_tADF70D5A3B3F0A34E5E51F1912E77A57137D43F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4892[1] = 
{
	JoystickElementIdentifier_tADF70D5A3B3F0A34E5E51F1912E77A57137D43F1::get_offset_of_svxDhybcftjpWbcQyWPnISfozPvo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4893 = { sizeof (OSXJoystickElementIdentifier_t952B7ACD1D799321678D3EAFCB85B30DAA0417AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4893[1] = 
{
	OSXJoystickElementIdentifier_t952B7ACD1D799321678D3EAFCB85B30DAA0417AD::get_offset_of_svxDhybcftjpWbcQyWPnISfozPvo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4894 = { sizeof (RawInputJoystickElementIdentifier_t1A2007A30A62574D626091A0E6CD3A80769280CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4894[1] = 
{
	RawInputJoystickElementIdentifier_t1A2007A30A62574D626091A0E6CD3A80769280CC::get_offset_of_svxDhybcftjpWbcQyWPnISfozPvo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4895 = { sizeof (UnityJoystickElementIdentifier_t53DD3E56E1B182F4E93E1A9614B54E9517F59A69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4895[1] = 
{
	UnityJoystickElementIdentifier_t53DD3E56E1B182F4E93E1A9614B54E9517F59A69::get_offset_of_svxDhybcftjpWbcQyWPnISfozPvo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4896 = { sizeof (RapfTflmdcOdagLxUfWgWRYFgIR_t4D7AFB2D4817B66B8260A2ABFC09E59BA9638DD5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4896[3] = 
{
	RapfTflmdcOdagLxUfWgWRYFgIR_t4D7AFB2D4817B66B8260A2ABFC09E59BA9638DD5::get_offset_of_GbhogeXOuqAWXhmgfRAmudFOsEia_0(),
	RapfTflmdcOdagLxUfWgWRYFgIR_t4D7AFB2D4817B66B8260A2ABFC09E59BA9638DD5::get_offset_of_ifzUzDIkGKOLpvauQZqGqyDgvJM_1(),
	RapfTflmdcOdagLxUfWgWRYFgIR_t4D7AFB2D4817B66B8260A2ABFC09E59BA9638DD5::get_offset_of_gTshMfebAuuRWoIiARFpiGoZoPk_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4897 = { sizeof (EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4897[3] = 
{
	EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0::get_offset_of_YIgjjtvsNpmLAzEahZSayMqwKAO_0(),
	EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0::get_offset_of_FXaTAHAQGPmajGVCvwXILbcNwbj_1(),
	EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0::get_offset_of_HWmOewXpuebOsZDStBejWuBUGqK_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4898 = { sizeof (FOBdOrKrxGfXvkOuUmdtNzedDCs_t3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4898[2] = 
{
	FOBdOrKrxGfXvkOuUmdtNzedDCs_t3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309::get_offset_of_lZlHpVlqScCxOayHMQQNLIJUPtMA_0(),
	FOBdOrKrxGfXvkOuUmdtNzedDCs_t3FAC0368AD1CD8F42C7E8A026E46F6A8327A2309::get_offset_of_vcFHuVHhWhJcpPiVLFZqonrrcV_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4899 = { sizeof (KCtXOMZUwpdfWoEclFRMgBlAMdPd_t30709A985B3D262DC097B4F3845C4185ED11F107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4899[3] = 
{
	KCtXOMZUwpdfWoEclFRMgBlAMdPd_t30709A985B3D262DC097B4F3845C4185ED11F107::get_offset_of_JTlYHbNuRvgezaodmDszddIQvhCD_0(),
	KCtXOMZUwpdfWoEclFRMgBlAMdPd_t30709A985B3D262DC097B4F3845C4185ED11F107::get_offset_of_jygrDMKtEfPwNxIkfLKMOCmUyrS_1(),
	KCtXOMZUwpdfWoEclFRMgBlAMdPd_t30709A985B3D262DC097B4F3845C4185ED11F107::get_offset_of_ojmYnkCHKzVQFExRGawmjxOAwip_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
