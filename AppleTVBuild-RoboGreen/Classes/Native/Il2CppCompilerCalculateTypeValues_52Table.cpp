﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// APIElevationDataController
struct APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C;
// APILoginController
struct APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05;
// APIProjectorDataController
struct APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98;
// APIPuttController
struct APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392;
// APIPuttForm.BallLocation
struct BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42;
// APIPuttForm.HoleLocation
struct HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64;
// APIPuttForm.SendPuttFormData[]
struct SendPuttFormDataU5BU5D_t6F3307DAA38194E23DEAD2EDA67A16DC5DB3EA15;
// CourseManager
struct CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8;
// DG.Tweening.Tween
struct Tween_t119487E0AB84EF563521F1043116BDBAE68AC437;
// Doozy.Engine.UI.UIPopup
struct UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228;
// Doozy.Engine.UI.UIView
struct UIView_t65D38836246049951821D4E45B1C81947591EBDF;
// ElevationTransferData
struct ElevationTransferData_t8EC1998F633665D38764D8DFF602677469984883;
// EnvMapAnimator
struct EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73;
// LoginValidateCredentials
struct LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692;
// ProjectorController
struct ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6;
// ProjectorStatusData
struct ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD;
// PuttInteractive
struct PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60;
// PuttSimulation
struct PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD;
// Rewired.Player
struct Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F;
// SimulationManager
struct SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9;
// Strackaline.ArrowColor
struct ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E;
// Strackaline.ArrowLocation
struct ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41;
// Strackaline.Arrow[]
struct ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40;
// Strackaline.Ball
struct Ball_t094765B02879225F8C595564E40A823B70AEE45E;
// Strackaline.Configurator[]
struct ConfiguratorU5BU5D_t4686BDCF6BA019E7D7CB4D4739199419814D1C45;
// Strackaline.Contour
struct Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42;
// Strackaline.ContourPathPoints[]
struct ContourPathPointsU5BU5D_t119B08689BCAFFD57F33E4CD5C72B1107920CC69;
// Strackaline.ContourPath[]
struct ContourPathU5BU5D_t0BC6D8CC1096FE5F0C231898B48C1F802775146E;
// Strackaline.Course
struct Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A;
// Strackaline.CourseData[]
struct CourseDataU5BU5D_t915E20A4DFCFDB9869A4E24EFA4E117038F601EC;
// Strackaline.CourseGreens
struct CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D;
// Strackaline.CourseSettings
struct CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352;
// Strackaline.Green
struct Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6;
// Strackaline.GreenActivity
struct GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B;
// Strackaline.GreenArrow
struct GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A;
// Strackaline.GreenCameraSettings
struct GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA;
// Strackaline.GreenGroup[]
struct GreenGroupU5BU5D_t234E8DF95534A84DF11AB456DD4053B38FCB207D;
// Strackaline.GreenPutt[]
struct GreenPuttU5BU5D_t338BD91014B3FBBEE954DF83C7966249D4A09D4E;
// Strackaline.Green[]
struct GreenU5BU5D_t15AF86FEF7A53720C1A0C6658F9BEF084F966BA1;
// Strackaline.Hole
struct Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB;
// Strackaline.HoleContainerSettings
struct HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175;
// Strackaline.HoleLocation[]
struct HoleLocationU5BU5D_tDE014607400D5140EDEB18539A0F84DF3E28E3C2;
// Strackaline.Instruction
struct Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C;
// Strackaline.JackData
struct JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1;
// Strackaline.Jack[]
struct JackU5BU5D_tFF5E434F45443DDB4153C743C370D479DEB623B6;
// Strackaline.Location
struct Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6;
// Strackaline.Point[]
struct PointU5BU5D_t4C35F8E2A134BA9FD1B58D2549F98E19EC3751C9;
// Strackaline.ProjectorData
struct ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88;
// Strackaline.Putt
struct Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49;
// Strackaline.PuttVariation
struct PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31;
// Strackaline.Putt[]
struct PuttU5BU5D_tA831E1874EAE75DBA6EBD968078DAA9B1E8D5E52;
// Strackaline.Region
struct Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E;
// Strackaline.Target
struct Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139;
// Strackaline.User
struct User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<CI.HttpClient.HttpResponseMessage>
struct Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.List`1<PuttSimulation>
struct List_1_t494E45893FDE2DD89DD40901A1C26C18540C2BCD;
// System.Collections.Generic.List`1<Strackaline.Arrow>
struct List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34;
// System.Collections.Generic.List`1<Strackaline.ContourPath>
struct List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.GameObject>>>
struct List_1_tDE730FA6B64BDCCA2AA49AE7E1CCF8EA93EA09CF;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct List_1_t6A863775481DD4E1BDD024A35AC5E83118209168;
// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData>
struct List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.FontWeight[]
struct FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C;
// TMPro.TMP_InputField
struct TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487;
// TMPro.TMP_Text
struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.LineRenderer
struct LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UserSubmitData
struct UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF;




#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CELEVATIONDATAREQUESTU3ED__12_TEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4_H
#define U3CELEVATIONDATAREQUESTU3ED__12_TEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIElevationDataController_<ElevationDataRequest>d__12
struct  U3CElevationDataRequestU3Ed__12_tEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4  : public RuntimeObject
{
public:
	// System.Int32 APIElevationDataController_<ElevationDataRequest>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APIElevationDataController_<ElevationDataRequest>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// APIElevationDataController APIElevationDataController_<ElevationDataRequest>d__12::<>4__this
	APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C * ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest APIElevationDataController_<ElevationDataRequest>d__12::<>s__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CU3Es__1_3;
	// Strackaline.ProjectorData APIElevationDataController_<ElevationDataRequest>d__12::<projectorData>5__2
	ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 * ___U3CprojectorDataU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CElevationDataRequestU3Ed__12_tEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CElevationDataRequestU3Ed__12_tEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CElevationDataRequestU3Ed__12_tEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4, ___U3CU3E4__this_2)); }
	inline APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_3() { return static_cast<int32_t>(offsetof(U3CElevationDataRequestU3Ed__12_tEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4, ___U3CU3Es__1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CU3Es__1_3() const { return ___U3CU3Es__1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CU3Es__1_3() { return &___U3CU3Es__1_3; }
	inline void set_U3CU3Es__1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CU3Es__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_3), value);
	}

	inline static int32_t get_offset_of_U3CprojectorDataU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CElevationDataRequestU3Ed__12_tEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4, ___U3CprojectorDataU3E5__2_4)); }
	inline ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 * get_U3CprojectorDataU3E5__2_4() const { return ___U3CprojectorDataU3E5__2_4; }
	inline ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 ** get_address_of_U3CprojectorDataU3E5__2_4() { return &___U3CprojectorDataU3E5__2_4; }
	inline void set_U3CprojectorDataU3E5__2_4(ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 * value)
	{
		___U3CprojectorDataU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprojectorDataU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CELEVATIONDATAREQUESTU3ED__12_TEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4_H
#ifndef U3CPINGAPIFORELEVATIONDATAU3ED__10_TCF5C602CD51E83EAF07398096A056A0BA00AB0F1_H
#define U3CPINGAPIFORELEVATIONDATAU3ED__10_TCF5C602CD51E83EAF07398096A056A0BA00AB0F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIElevationDataController_<PingAPIForElevationData>d__10
struct  U3CPingAPIForElevationDataU3Ed__10_tCF5C602CD51E83EAF07398096A056A0BA00AB0F1  : public RuntimeObject
{
public:
	// System.Int32 APIElevationDataController_<PingAPIForElevationData>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APIElevationDataController_<PingAPIForElevationData>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// APIElevationDataController APIElevationDataController_<PingAPIForElevationData>d__10::<>4__this
	APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPingAPIForElevationDataU3Ed__10_tCF5C602CD51E83EAF07398096A056A0BA00AB0F1, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPingAPIForElevationDataU3Ed__10_tCF5C602CD51E83EAF07398096A056A0BA00AB0F1, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CPingAPIForElevationDataU3Ed__10_tCF5C602CD51E83EAF07398096A056A0BA00AB0F1, ___U3CU3E4__this_2)); }
	inline APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPINGAPIFORELEVATIONDATAU3ED__10_TCF5C602CD51E83EAF07398096A056A0BA00AB0F1_H
#ifndef U3CU3EC_T388472ACBBACB811A7689249633ADE5FF21C0B9F_H
#define U3CU3EC_T388472ACBBACB811A7689249633ADE5FF21C0B9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APILoginController_<>c
struct  U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F_StaticFields
{
public:
	// APILoginController_<>c APILoginController_<>c::<>9
	U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F * ___U3CU3E9_0;
	// System.Net.Security.RemoteCertificateValidationCallback APILoginController_<>c::<>9__5_0
	RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * ___U3CU3E9__5_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F_StaticFields, ___U3CU3E9__5_0_1)); }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * get_U3CU3E9__5_0_1() const { return ___U3CU3E9__5_0_1; }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E ** get_address_of_U3CU3E9__5_0_1() { return &___U3CU3E9__5_0_1; }
	inline void set_U3CU3E9__5_0_1(RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * value)
	{
		___U3CU3E9__5_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T388472ACBBACB811A7689249633ADE5FF21C0B9F_H
#ifndef U3CLOGINREQUESTU3ED__7_TDB055AD3C80AB8544320D3196905113F84F45A7B_H
#define U3CLOGINREQUESTU3ED__7_TDB055AD3C80AB8544320D3196905113F84F45A7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APILoginController_<LoginRequest>d__7
struct  U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B  : public RuntimeObject
{
public:
	// System.Int32 APILoginController_<LoginRequest>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APILoginController_<LoginRequest>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UserSubmitData APILoginController_<LoginRequest>d__7::_userSubmitData
	UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF * ____userSubmitData_2;
	// APILoginController APILoginController_<LoginRequest>d__7::<>4__this
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05 * ___U3CU3E4__this_3;
	// UnityEngine.Networking.UnityWebRequest APILoginController_<LoginRequest>d__7::<>s__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CU3Es__1_4;
	// Strackaline.User APILoginController_<LoginRequest>d__7::<user>5__2
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * ___U3CuserU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of__userSubmitData_2() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B, ____userSubmitData_2)); }
	inline UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF * get__userSubmitData_2() const { return ____userSubmitData_2; }
	inline UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF ** get_address_of__userSubmitData_2() { return &____userSubmitData_2; }
	inline void set__userSubmitData_2(UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF * value)
	{
		____userSubmitData_2 = value;
		Il2CppCodeGenWriteBarrier((&____userSubmitData_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B, ___U3CU3E4__this_3)); }
	inline APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_4() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B, ___U3CU3Es__1_4)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CU3Es__1_4() const { return ___U3CU3Es__1_4; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CU3Es__1_4() { return &___U3CU3Es__1_4; }
	inline void set_U3CU3Es__1_4(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CU3Es__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_4), value);
	}

	inline static int32_t get_offset_of_U3CuserU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B, ___U3CuserU3E5__2_5)); }
	inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * get_U3CuserU3E5__2_5() const { return ___U3CuserU3E5__2_5; }
	inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 ** get_address_of_U3CuserU3E5__2_5() { return &___U3CuserU3E5__2_5; }
	inline void set_U3CuserU3E5__2_5(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * value)
	{
		___U3CuserU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOGINREQUESTU3ED__7_TDB055AD3C80AB8544320D3196905113F84F45A7B_H
#ifndef U3CPINGAPIFORNEWPUTTDATAU3ED__16_T1BFFD75823E5055966D106156800C1B8A7F2229B_H
#define U3CPINGAPIFORNEWPUTTDATAU3ED__16_T1BFFD75823E5055966D106156800C1B8A7F2229B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIProjectorDataController_<PingAPIForNewPuttData>d__16
struct  U3CPingAPIForNewPuttDataU3Ed__16_t1BFFD75823E5055966D106156800C1B8A7F2229B  : public RuntimeObject
{
public:
	// System.Int32 APIProjectorDataController_<PingAPIForNewPuttData>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APIProjectorDataController_<PingAPIForNewPuttData>d__16::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// APIProjectorDataController APIProjectorDataController_<PingAPIForNewPuttData>d__16::<>4__this
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPingAPIForNewPuttDataU3Ed__16_t1BFFD75823E5055966D106156800C1B8A7F2229B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPingAPIForNewPuttDataU3Ed__16_t1BFFD75823E5055966D106156800C1B8A7F2229B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CPingAPIForNewPuttDataU3Ed__16_t1BFFD75823E5055966D106156800C1B8A7F2229B, ___U3CU3E4__this_2)); }
	inline APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPINGAPIFORNEWPUTTDATAU3ED__16_T1BFFD75823E5055966D106156800C1B8A7F2229B_H
#ifndef U3CPROJECTORDATAREQUESTU3ED__18_T3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853_H
#define U3CPROJECTORDATAREQUESTU3ED__18_T3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIProjectorDataController_<ProjectorDataRequest>d__18
struct  U3CProjectorDataRequestU3Ed__18_t3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853  : public RuntimeObject
{
public:
	// System.Int32 APIProjectorDataController_<ProjectorDataRequest>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APIProjectorDataController_<ProjectorDataRequest>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// APIProjectorDataController APIProjectorDataController_<ProjectorDataRequest>d__18::<>4__this
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98 * ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest APIProjectorDataController_<ProjectorDataRequest>d__18::<>s__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CU3Es__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProjectorDataRequestU3Ed__18_t3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CProjectorDataRequestU3Ed__18_t3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CProjectorDataRequestU3Ed__18_t3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853, ___U3CU3E4__this_2)); }
	inline APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_3() { return static_cast<int32_t>(offsetof(U3CProjectorDataRequestU3Ed__18_t3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853, ___U3CU3Es__1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CU3Es__1_3() const { return ___U3CU3Es__1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CU3Es__1_3() { return &___U3CU3Es__1_3; }
	inline void set_U3CU3Es__1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CU3Es__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROJECTORDATAREQUESTU3ED__18_T3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853_H
#ifndef U3CWAITFORGETPUTTU3ED__12_TE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9_H
#define U3CWAITFORGETPUTTU3ED__12_TE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttController_<WaitForGetPutt>d__12
struct  U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9  : public RuntimeObject
{
public:
	// System.Int32 APIPuttController_<WaitForGetPutt>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object APIPuttController_<WaitForGetPutt>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// APIPuttController APIPuttController_<WaitForGetPutt>d__12::<>4__this
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392 * ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest APIPuttController_<WaitForGetPutt>d__12::<>s__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CU3Es__1_3;
	// Strackaline.PuttVariation APIPuttController_<WaitForGetPutt>d__12::<puttVariation>5__2
	PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31 * ___U3CputtVariationU3E5__2_4;
	// System.Single APIPuttController_<WaitForGetPutt>d__12::<xball>5__3
	float ___U3CxballU3E5__3_5;
	// System.Single APIPuttController_<WaitForGetPutt>d__12::<zball>5__4
	float ___U3CzballU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9, ___U3CU3E4__this_2)); }
	inline APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_3() { return static_cast<int32_t>(offsetof(U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9, ___U3CU3Es__1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CU3Es__1_3() const { return ___U3CU3Es__1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CU3Es__1_3() { return &___U3CU3Es__1_3; }
	inline void set_U3CU3Es__1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CU3Es__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_3), value);
	}

	inline static int32_t get_offset_of_U3CputtVariationU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9, ___U3CputtVariationU3E5__2_4)); }
	inline PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31 * get_U3CputtVariationU3E5__2_4() const { return ___U3CputtVariationU3E5__2_4; }
	inline PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31 ** get_address_of_U3CputtVariationU3E5__2_4() { return &___U3CputtVariationU3E5__2_4; }
	inline void set_U3CputtVariationU3E5__2_4(PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31 * value)
	{
		___U3CputtVariationU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CputtVariationU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CxballU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9, ___U3CxballU3E5__3_5)); }
	inline float get_U3CxballU3E5__3_5() const { return ___U3CxballU3E5__3_5; }
	inline float* get_address_of_U3CxballU3E5__3_5() { return &___U3CxballU3E5__3_5; }
	inline void set_U3CxballU3E5__3_5(float value)
	{
		___U3CxballU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CzballU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9, ___U3CzballU3E5__4_6)); }
	inline float get_U3CzballU3E5__4_6() const { return ___U3CzballU3E5__4_6; }
	inline float* get_address_of_U3CzballU3E5__4_6() { return &___U3CzballU3E5__4_6; }
	inline void set_U3CzballU3E5__4_6(float value)
	{
		___U3CzballU3E5__4_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORGETPUTTU3ED__12_TE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9_H
#ifndef BALLLOCATION_T778FBC13F63FD3C5816B575D57CD5FE28B50CD42_H
#define BALLLOCATION_T778FBC13F63FD3C5816B575D57CD5FE28B50CD42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttForm.BallLocation
struct  BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42  : public RuntimeObject
{
public:
	// System.Single APIPuttForm.BallLocation::x
	float ___x_0;
	// System.Single APIPuttForm.BallLocation::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLLOCATION_T778FBC13F63FD3C5816B575D57CD5FE28B50CD42_H
#ifndef HOLELOCATION_T6CB13532CE5B20E8F0B636B3560C912B92FB8C64_H
#define HOLELOCATION_T6CB13532CE5B20E8F0B636B3560C912B92FB8C64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttForm.HoleLocation
struct  HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64  : public RuntimeObject
{
public:
	// System.Single APIPuttForm.HoleLocation::x
	float ___x_0;
	// System.Single APIPuttForm.HoleLocation::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLELOCATION_T6CB13532CE5B20E8F0B636B3560C912B92FB8C64_H
#ifndef SENDPUTTFORM_T4B1C3142A12A2243174E9F84EC7DFEB2F7723E02_H
#define SENDPUTTFORM_T4B1C3142A12A2243174E9F84EC7DFEB2F7723E02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttForm.SendPuttForm
struct  SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02  : public RuntimeObject
{
public:
	// System.Int32 APIPuttForm.SendPuttForm::greenId
	int32_t ___greenId_0;
	// System.Single APIPuttForm.SendPuttForm::rotation
	float ___rotation_1;
	// APIPuttForm.SendPuttFormData[] APIPuttForm.SendPuttForm::data
	SendPuttFormDataU5BU5D_t6F3307DAA38194E23DEAD2EDA67A16DC5DB3EA15* ___data_2;

public:
	inline static int32_t get_offset_of_greenId_0() { return static_cast<int32_t>(offsetof(SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02, ___greenId_0)); }
	inline int32_t get_greenId_0() const { return ___greenId_0; }
	inline int32_t* get_address_of_greenId_0() { return &___greenId_0; }
	inline void set_greenId_0(int32_t value)
	{
		___greenId_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02, ___rotation_1)); }
	inline float get_rotation_1() const { return ___rotation_1; }
	inline float* get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(float value)
	{
		___rotation_1 = value;
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02, ___data_2)); }
	inline SendPuttFormDataU5BU5D_t6F3307DAA38194E23DEAD2EDA67A16DC5DB3EA15* get_data_2() const { return ___data_2; }
	inline SendPuttFormDataU5BU5D_t6F3307DAA38194E23DEAD2EDA67A16DC5DB3EA15** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(SendPuttFormDataU5BU5D_t6F3307DAA38194E23DEAD2EDA67A16DC5DB3EA15* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDPUTTFORM_T4B1C3142A12A2243174E9F84EC7DFEB2F7723E02_H
#ifndef SENDPUTTFORMDATA_T2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE_H
#define SENDPUTTFORMDATA_T2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttForm.SendPuttFormData
struct  SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE  : public RuntimeObject
{
public:
	// System.Int32 APIPuttForm.SendPuttFormData::stimp
	int32_t ___stimp_0;
	// System.Single APIPuttForm.SendPuttFormData::firmness
	float ___firmness_1;
	// System.Boolean APIPuttForm.SendPuttFormData::isMetric
	bool ___isMetric_2;
	// APIPuttForm.HoleLocation APIPuttForm.SendPuttFormData::holeLocation
	HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64 * ___holeLocation_3;
	// APIPuttForm.BallLocation APIPuttForm.SendPuttFormData::ballLocation
	BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42 * ___ballLocation_4;

public:
	inline static int32_t get_offset_of_stimp_0() { return static_cast<int32_t>(offsetof(SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE, ___stimp_0)); }
	inline int32_t get_stimp_0() const { return ___stimp_0; }
	inline int32_t* get_address_of_stimp_0() { return &___stimp_0; }
	inline void set_stimp_0(int32_t value)
	{
		___stimp_0 = value;
	}

	inline static int32_t get_offset_of_firmness_1() { return static_cast<int32_t>(offsetof(SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE, ___firmness_1)); }
	inline float get_firmness_1() const { return ___firmness_1; }
	inline float* get_address_of_firmness_1() { return &___firmness_1; }
	inline void set_firmness_1(float value)
	{
		___firmness_1 = value;
	}

	inline static int32_t get_offset_of_isMetric_2() { return static_cast<int32_t>(offsetof(SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE, ___isMetric_2)); }
	inline bool get_isMetric_2() const { return ___isMetric_2; }
	inline bool* get_address_of_isMetric_2() { return &___isMetric_2; }
	inline void set_isMetric_2(bool value)
	{
		___isMetric_2 = value;
	}

	inline static int32_t get_offset_of_holeLocation_3() { return static_cast<int32_t>(offsetof(SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE, ___holeLocation_3)); }
	inline HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64 * get_holeLocation_3() const { return ___holeLocation_3; }
	inline HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64 ** get_address_of_holeLocation_3() { return &___holeLocation_3; }
	inline void set_holeLocation_3(HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64 * value)
	{
		___holeLocation_3 = value;
		Il2CppCodeGenWriteBarrier((&___holeLocation_3), value);
	}

	inline static int32_t get_offset_of_ballLocation_4() { return static_cast<int32_t>(offsetof(SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE, ___ballLocation_4)); }
	inline BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42 * get_ballLocation_4() const { return ___ballLocation_4; }
	inline BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42 ** get_address_of_ballLocation_4() { return &___ballLocation_4; }
	inline void set_ballLocation_4(BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42 * value)
	{
		___ballLocation_4 = value;
		Il2CppCodeGenWriteBarrier((&___ballLocation_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDPUTTFORMDATA_T2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE_H
#ifndef ELEVATIONTRANSFERDATA_T8EC1998F633665D38764D8DFF602677469984883_H
#define ELEVATIONTRANSFERDATA_T8EC1998F633665D38764D8DFF602677469984883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ElevationTransferData
struct  ElevationTransferData_t8EC1998F633665D38764D8DFF602677469984883  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Strackaline.Arrow> ElevationTransferData::arrowCollectedList
	List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 * ___arrowCollectedList_0;
	// System.Collections.Generic.List`1<Strackaline.ContourPath> ElevationTransferData::contourCollectedList
	List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D * ___contourCollectedList_1;

public:
	inline static int32_t get_offset_of_arrowCollectedList_0() { return static_cast<int32_t>(offsetof(ElevationTransferData_t8EC1998F633665D38764D8DFF602677469984883, ___arrowCollectedList_0)); }
	inline List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 * get_arrowCollectedList_0() const { return ___arrowCollectedList_0; }
	inline List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 ** get_address_of_arrowCollectedList_0() { return &___arrowCollectedList_0; }
	inline void set_arrowCollectedList_0(List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 * value)
	{
		___arrowCollectedList_0 = value;
		Il2CppCodeGenWriteBarrier((&___arrowCollectedList_0), value);
	}

	inline static int32_t get_offset_of_contourCollectedList_1() { return static_cast<int32_t>(offsetof(ElevationTransferData_t8EC1998F633665D38764D8DFF602677469984883, ___contourCollectedList_1)); }
	inline List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D * get_contourCollectedList_1() const { return ___contourCollectedList_1; }
	inline List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D ** get_address_of_contourCollectedList_1() { return &___contourCollectedList_1; }
	inline void set_contourCollectedList_1(List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D * value)
	{
		___contourCollectedList_1 = value;
		Il2CppCodeGenWriteBarrier((&___contourCollectedList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEVATIONTRANSFERDATA_T8EC1998F633665D38764D8DFF602677469984883_H
#ifndef U3CU3EC_T8D0D338667B7ED7008F8BC357EC849EA153D4F86_H
#define U3CU3EC_T8D0D338667B7ED7008F8BC357EC849EA153D4F86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleSceneManagerController_<>c
struct  U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields
{
public:
	// ExampleSceneManagerController_<>c ExampleSceneManagerController_<>c::<>9
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86 * ___U3CU3E9_0;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> ExampleSceneManagerController_<>c::<>9__3_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__3_0_1;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> ExampleSceneManagerController_<>c::<>9__6_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__6_0_2;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> ExampleSceneManagerController_<>c::<>9__7_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__7_0_3;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> ExampleSceneManagerController_<>c::<>9__8_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__8_0_4;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> ExampleSceneManagerController_<>c::<>9__9_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__9_0_5;
	// System.Action`1<CI.HttpClient.HttpResponseMessage> ExampleSceneManagerController_<>c::<>9__10_0
	Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * ___U3CU3E9__10_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9__6_0_2)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__6_0_2() const { return ___U3CU3E9__6_0_2; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__6_0_2() { return &___U3CU3E9__6_0_2; }
	inline void set_U3CU3E9__6_0_2(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__6_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9__7_0_3)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__7_0_3() const { return ___U3CU3E9__7_0_3; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__7_0_3() { return &___U3CU3E9__7_0_3; }
	inline void set_U3CU3E9__7_0_3(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__7_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9__8_0_4)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__8_0_4() const { return ___U3CU3E9__8_0_4; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__8_0_4() { return &___U3CU3E9__8_0_4; }
	inline void set_U3CU3E9__8_0_4(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__8_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9__9_0_5)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__9_0_5() const { return ___U3CU3E9__9_0_5; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__9_0_5() { return &___U3CU3E9__9_0_5; }
	inline void set_U3CU3E9__9_0_5(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__9_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields, ___U3CU3E9__10_0_6)); }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * get_U3CU3E9__10_0_6() const { return ___U3CU3E9__10_0_6; }
	inline Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 ** get_address_of_U3CU3E9__10_0_6() { return &___U3CU3E9__10_0_6; }
	inline void set_U3CU3E9__10_0_6(Action_1_t43F8066BC33CD72EB284C1051B83898DD8F229C0 * value)
	{
		___U3CU3E9__10_0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T8D0D338667B7ED7008F8BC357EC849EA153D4F86_H
#ifndef EXTENSIONMETHODS_T9C246E3C94E8653839FE7FBBE6549E8030A5B14B_H
#define EXTENSIONMETHODS_T9C246E3C94E8653839FE7FBBE6549E8030A5B14B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExtensionMethods
struct  ExtensionMethods_t9C246E3C94E8653839FE7FBBE6549E8030A5B14B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONMETHODS_T9C246E3C94E8653839FE7FBBE6549E8030A5B14B_H
#ifndef PROJECTORSTATUSDATA_T62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD_H
#define PROJECTORSTATUSDATA_T62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectorStatusData
struct  ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD  : public RuntimeObject
{
public:
	// Strackaline.JackData ProjectorStatusData::jackData
	JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1 * ___jackData_0;
	// Strackaline.GreenActivity ProjectorStatusData::greenActivity
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B * ___greenActivity_1;

public:
	inline static int32_t get_offset_of_jackData_0() { return static_cast<int32_t>(offsetof(ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD, ___jackData_0)); }
	inline JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1 * get_jackData_0() const { return ___jackData_0; }
	inline JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1 ** get_address_of_jackData_0() { return &___jackData_0; }
	inline void set_jackData_0(JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1 * value)
	{
		___jackData_0 = value;
		Il2CppCodeGenWriteBarrier((&___jackData_0), value);
	}

	inline static int32_t get_offset_of_greenActivity_1() { return static_cast<int32_t>(offsetof(ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD, ___greenActivity_1)); }
	inline GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B * get_greenActivity_1() const { return ___greenActivity_1; }
	inline GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B ** get_address_of_greenActivity_1() { return &___greenActivity_1; }
	inline void set_greenActivity_1(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B * value)
	{
		___greenActivity_1 = value;
		Il2CppCodeGenWriteBarrier((&___greenActivity_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTORSTATUSDATA_T62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD_H
#ifndef ARROW_T143EA52C1A6FE56EE828767447D0B30EAAEFF520_H
#define ARROW_T143EA52C1A6FE56EE828767447D0B30EAAEFF520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Arrow
struct  Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520  : public RuntimeObject
{
public:
	// Strackaline.ArrowLocation Strackaline.Arrow::point
	ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41 * ___point_0;
	// System.Single Strackaline.Arrow::slope
	float ___slope_1;
	// System.Single Strackaline.Arrow::rotation
	float ___rotation_2;
	// System.Int32 Strackaline.Arrow::level
	int32_t ___level_3;
	// Strackaline.ArrowColor Strackaline.Arrow::color
	ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E * ___color_4;

public:
	inline static int32_t get_offset_of_point_0() { return static_cast<int32_t>(offsetof(Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520, ___point_0)); }
	inline ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41 * get_point_0() const { return ___point_0; }
	inline ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41 ** get_address_of_point_0() { return &___point_0; }
	inline void set_point_0(ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41 * value)
	{
		___point_0 = value;
		Il2CppCodeGenWriteBarrier((&___point_0), value);
	}

	inline static int32_t get_offset_of_slope_1() { return static_cast<int32_t>(offsetof(Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520, ___slope_1)); }
	inline float get_slope_1() const { return ___slope_1; }
	inline float* get_address_of_slope_1() { return &___slope_1; }
	inline void set_slope_1(float value)
	{
		___slope_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520, ___rotation_2)); }
	inline float get_rotation_2() const { return ___rotation_2; }
	inline float* get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(float value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_level_3() { return static_cast<int32_t>(offsetof(Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520, ___level_3)); }
	inline int32_t get_level_3() const { return ___level_3; }
	inline int32_t* get_address_of_level_3() { return &___level_3; }
	inline void set_level_3(int32_t value)
	{
		___level_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520, ___color_4)); }
	inline ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E * get_color_4() const { return ___color_4; }
	inline ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E ** get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E * value)
	{
		___color_4 = value;
		Il2CppCodeGenWriteBarrier((&___color_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROW_T143EA52C1A6FE56EE828767447D0B30EAAEFF520_H
#ifndef ARROWCOLOR_T450822419497E236F3DAF47E7CC8FFCE3DBC0F7E_H
#define ARROWCOLOR_T450822419497E236F3DAF47E7CC8FFCE3DBC0F7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.ArrowColor
struct  ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E  : public RuntimeObject
{
public:
	// System.Single Strackaline.ArrowColor::r
	float ___r_0;
	// System.Single Strackaline.ArrowColor::g
	float ___g_1;
	// System.Single Strackaline.ArrowColor::b
	float ___b_2;
	// System.Single Strackaline.ArrowColor::a
	float ___a_3;
	// System.String Strackaline.ArrowColor::hex
	String_t* ___hex_4;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}

	inline static int32_t get_offset_of_hex_4() { return static_cast<int32_t>(offsetof(ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E, ___hex_4)); }
	inline String_t* get_hex_4() const { return ___hex_4; }
	inline String_t** get_address_of_hex_4() { return &___hex_4; }
	inline void set_hex_4(String_t* value)
	{
		___hex_4 = value;
		Il2CppCodeGenWriteBarrier((&___hex_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWCOLOR_T450822419497E236F3DAF47E7CC8FFCE3DBC0F7E_H
#ifndef ARROWLOCATION_T9D29EA9D14075CD21D7C77E9821E5044E1418B41_H
#define ARROWLOCATION_T9D29EA9D14075CD21D7C77E9821E5044E1418B41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.ArrowLocation
struct  ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41  : public RuntimeObject
{
public:
	// System.Single Strackaline.ArrowLocation::x
	float ___x_0;
	// System.Single Strackaline.ArrowLocation::y
	float ___y_1;
	// System.Single Strackaline.ArrowLocation::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWLOCATION_T9D29EA9D14075CD21D7C77E9821E5044E1418B41_H
#ifndef BALL_T094765B02879225F8C595564E40A823B70AEE45E_H
#define BALL_T094765B02879225F8C595564E40A823B70AEE45E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Ball
struct  Ball_t094765B02879225F8C595564E40A823B70AEE45E  : public RuntimeObject
{
public:
	// System.Single Strackaline.Ball::x
	float ___x_0;
	// System.Single Strackaline.Ball::y
	float ___y_1;
	// System.Single Strackaline.Ball::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Ball_t094765B02879225F8C595564E40A823B70AEE45E, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Ball_t094765B02879225F8C595564E40A823B70AEE45E, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Ball_t094765B02879225F8C595564E40A823B70AEE45E, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALL_T094765B02879225F8C595564E40A823B70AEE45E_H
#ifndef CONFIGURATOR_T63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F_H
#define CONFIGURATOR_T63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Configurator
struct  Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.Configurator::puttTrainerId
	int32_t ___puttTrainerId_0;
	// System.Int32 Strackaline.Configurator::configurationId
	int32_t ___configurationId_1;
	// System.String Strackaline.Configurator::createdOn
	String_t* ___createdOn_2;
	// System.String Strackaline.Configurator::configurationData
	String_t* ___configurationData_3;

public:
	inline static int32_t get_offset_of_puttTrainerId_0() { return static_cast<int32_t>(offsetof(Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F, ___puttTrainerId_0)); }
	inline int32_t get_puttTrainerId_0() const { return ___puttTrainerId_0; }
	inline int32_t* get_address_of_puttTrainerId_0() { return &___puttTrainerId_0; }
	inline void set_puttTrainerId_0(int32_t value)
	{
		___puttTrainerId_0 = value;
	}

	inline static int32_t get_offset_of_configurationId_1() { return static_cast<int32_t>(offsetof(Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F, ___configurationId_1)); }
	inline int32_t get_configurationId_1() const { return ___configurationId_1; }
	inline int32_t* get_address_of_configurationId_1() { return &___configurationId_1; }
	inline void set_configurationId_1(int32_t value)
	{
		___configurationId_1 = value;
	}

	inline static int32_t get_offset_of_createdOn_2() { return static_cast<int32_t>(offsetof(Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F, ___createdOn_2)); }
	inline String_t* get_createdOn_2() const { return ___createdOn_2; }
	inline String_t** get_address_of_createdOn_2() { return &___createdOn_2; }
	inline void set_createdOn_2(String_t* value)
	{
		___createdOn_2 = value;
		Il2CppCodeGenWriteBarrier((&___createdOn_2), value);
	}

	inline static int32_t get_offset_of_configurationData_3() { return static_cast<int32_t>(offsetof(Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F, ___configurationData_3)); }
	inline String_t* get_configurationData_3() const { return ___configurationData_3; }
	inline String_t** get_address_of_configurationData_3() { return &___configurationData_3; }
	inline void set_configurationData_3(String_t* value)
	{
		___configurationData_3 = value;
		Il2CppCodeGenWriteBarrier((&___configurationData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATOR_T63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F_H
#ifndef CONTOUR_T3A197E26E2CB50405BBE91F99D606941420D7B42_H
#define CONTOUR_T3A197E26E2CB50405BBE91F99D606941420D7B42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Contour
struct  Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42  : public RuntimeObject
{
public:
	// Strackaline.ContourPath[] Strackaline.Contour::contourPaths
	ContourPathU5BU5D_t0BC6D8CC1096FE5F0C231898B48C1F802775146E* ___contourPaths_0;

public:
	inline static int32_t get_offset_of_contourPaths_0() { return static_cast<int32_t>(offsetof(Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42, ___contourPaths_0)); }
	inline ContourPathU5BU5D_t0BC6D8CC1096FE5F0C231898B48C1F802775146E* get_contourPaths_0() const { return ___contourPaths_0; }
	inline ContourPathU5BU5D_t0BC6D8CC1096FE5F0C231898B48C1F802775146E** get_address_of_contourPaths_0() { return &___contourPaths_0; }
	inline void set_contourPaths_0(ContourPathU5BU5D_t0BC6D8CC1096FE5F0C231898B48C1F802775146E* value)
	{
		___contourPaths_0 = value;
		Il2CppCodeGenWriteBarrier((&___contourPaths_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTOUR_T3A197E26E2CB50405BBE91F99D606941420D7B42_H
#ifndef CONTOURPATH_T1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6_H
#define CONTOURPATH_T1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.ContourPath
struct  ContourPath_t1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6  : public RuntimeObject
{
public:
	// Strackaline.ContourPathPoints[] Strackaline.ContourPath::path
	ContourPathPointsU5BU5D_t119B08689BCAFFD57F33E4CD5C72B1107920CC69* ___path_0;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(ContourPath_t1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6, ___path_0)); }
	inline ContourPathPointsU5BU5D_t119B08689BCAFFD57F33E4CD5C72B1107920CC69* get_path_0() const { return ___path_0; }
	inline ContourPathPointsU5BU5D_t119B08689BCAFFD57F33E4CD5C72B1107920CC69** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(ContourPathPointsU5BU5D_t119B08689BCAFFD57F33E4CD5C72B1107920CC69* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier((&___path_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTOURPATH_T1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6_H
#ifndef CONTOURPATHPOINTS_T61D9DB02F98FF719B11F2A3D66302CB6E44515F6_H
#define CONTOURPATHPOINTS_T61D9DB02F98FF719B11F2A3D66302CB6E44515F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.ContourPathPoints
struct  ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6  : public RuntimeObject
{
public:
	// System.Single Strackaline.ContourPathPoints::x
	float ___x_0;
	// System.Single Strackaline.ContourPathPoints::y
	float ___y_1;
	// System.Single Strackaline.ContourPathPoints::z
	float ___z_2;
	// System.Int32 Strackaline.ContourPathPoints::index
	int32_t ___index_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTOURPATHPOINTS_T61D9DB02F98FF719B11F2A3D66302CB6E44515F6_H
#ifndef COURSE_TDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A_H
#define COURSE_TDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Course
struct  Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A  : public RuntimeObject
{
public:
	// Strackaline.CourseData[] Strackaline.Course::value
	CourseDataU5BU5D_t915E20A4DFCFDB9869A4E24EFA4E117038F601EC* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A, ___value_0)); }
	inline CourseDataU5BU5D_t915E20A4DFCFDB9869A4E24EFA4E117038F601EC* get_value_0() const { return ___value_0; }
	inline CourseDataU5BU5D_t915E20A4DFCFDB9869A4E24EFA4E117038F601EC** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(CourseDataU5BU5D_t915E20A4DFCFDB9869A4E24EFA4E117038F601EC* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSE_TDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A_H
#ifndef COURSEDATA_T1F181044BC7EDA690F8E4E0C71C4F392C60A10DF_H
#define COURSEDATA_T1F181044BC7EDA690F8E4E0C71C4F392C60A10DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.CourseData
struct  CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.CourseData::courseId
	int32_t ___courseId_0;
	// System.Int32 Strackaline.CourseData::sourceId
	int32_t ___sourceId_1;
	// System.String Strackaline.CourseData::name
	String_t* ___name_2;
	// System.String Strackaline.CourseData::address
	String_t* ___address_3;
	// System.String Strackaline.CourseData::city
	String_t* ___city_4;
	// System.String Strackaline.CourseData::region
	String_t* ___region_5;
	// System.String Strackaline.CourseData::postalCode
	String_t* ___postalCode_6;
	// System.String Strackaline.CourseData::countryCode
	String_t* ___countryCode_7;
	// System.String Strackaline.CourseData::profileImage
	String_t* ___profileImage_8;
	// System.Double Strackaline.CourseData::latitude
	double ___latitude_9;
	// System.Double Strackaline.CourseData::longitude
	double ___longitude_10;
	// System.Boolean Strackaline.CourseData::isActive
	bool ___isActive_11;
	// Strackaline.CourseSettings Strackaline.CourseData::settings
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352 * ___settings_12;

public:
	inline static int32_t get_offset_of_courseId_0() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___courseId_0)); }
	inline int32_t get_courseId_0() const { return ___courseId_0; }
	inline int32_t* get_address_of_courseId_0() { return &___courseId_0; }
	inline void set_courseId_0(int32_t value)
	{
		___courseId_0 = value;
	}

	inline static int32_t get_offset_of_sourceId_1() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___sourceId_1)); }
	inline int32_t get_sourceId_1() const { return ___sourceId_1; }
	inline int32_t* get_address_of_sourceId_1() { return &___sourceId_1; }
	inline void set_sourceId_1(int32_t value)
	{
		___sourceId_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_address_3() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___address_3)); }
	inline String_t* get_address_3() const { return ___address_3; }
	inline String_t** get_address_of_address_3() { return &___address_3; }
	inline void set_address_3(String_t* value)
	{
		___address_3 = value;
		Il2CppCodeGenWriteBarrier((&___address_3), value);
	}

	inline static int32_t get_offset_of_city_4() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___city_4)); }
	inline String_t* get_city_4() const { return ___city_4; }
	inline String_t** get_address_of_city_4() { return &___city_4; }
	inline void set_city_4(String_t* value)
	{
		___city_4 = value;
		Il2CppCodeGenWriteBarrier((&___city_4), value);
	}

	inline static int32_t get_offset_of_region_5() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___region_5)); }
	inline String_t* get_region_5() const { return ___region_5; }
	inline String_t** get_address_of_region_5() { return &___region_5; }
	inline void set_region_5(String_t* value)
	{
		___region_5 = value;
		Il2CppCodeGenWriteBarrier((&___region_5), value);
	}

	inline static int32_t get_offset_of_postalCode_6() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___postalCode_6)); }
	inline String_t* get_postalCode_6() const { return ___postalCode_6; }
	inline String_t** get_address_of_postalCode_6() { return &___postalCode_6; }
	inline void set_postalCode_6(String_t* value)
	{
		___postalCode_6 = value;
		Il2CppCodeGenWriteBarrier((&___postalCode_6), value);
	}

	inline static int32_t get_offset_of_countryCode_7() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___countryCode_7)); }
	inline String_t* get_countryCode_7() const { return ___countryCode_7; }
	inline String_t** get_address_of_countryCode_7() { return &___countryCode_7; }
	inline void set_countryCode_7(String_t* value)
	{
		___countryCode_7 = value;
		Il2CppCodeGenWriteBarrier((&___countryCode_7), value);
	}

	inline static int32_t get_offset_of_profileImage_8() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___profileImage_8)); }
	inline String_t* get_profileImage_8() const { return ___profileImage_8; }
	inline String_t** get_address_of_profileImage_8() { return &___profileImage_8; }
	inline void set_profileImage_8(String_t* value)
	{
		___profileImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___profileImage_8), value);
	}

	inline static int32_t get_offset_of_latitude_9() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___latitude_9)); }
	inline double get_latitude_9() const { return ___latitude_9; }
	inline double* get_address_of_latitude_9() { return &___latitude_9; }
	inline void set_latitude_9(double value)
	{
		___latitude_9 = value;
	}

	inline static int32_t get_offset_of_longitude_10() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___longitude_10)); }
	inline double get_longitude_10() const { return ___longitude_10; }
	inline double* get_address_of_longitude_10() { return &___longitude_10; }
	inline void set_longitude_10(double value)
	{
		___longitude_10 = value;
	}

	inline static int32_t get_offset_of_isActive_11() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___isActive_11)); }
	inline bool get_isActive_11() const { return ___isActive_11; }
	inline bool* get_address_of_isActive_11() { return &___isActive_11; }
	inline void set_isActive_11(bool value)
	{
		___isActive_11 = value;
	}

	inline static int32_t get_offset_of_settings_12() { return static_cast<int32_t>(offsetof(CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF, ___settings_12)); }
	inline CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352 * get_settings_12() const { return ___settings_12; }
	inline CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352 ** get_address_of_settings_12() { return &___settings_12; }
	inline void set_settings_12(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352 * value)
	{
		___settings_12 = value;
		Il2CppCodeGenWriteBarrier((&___settings_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSEDATA_T1F181044BC7EDA690F8E4E0C71C4F392C60A10DF_H
#ifndef COURSEGREENS_TA5A23B34F0E3B9114AB312F86C40E6A63492CE8D_H
#define COURSEGREENS_TA5A23B34F0E3B9114AB312F86C40E6A63492CE8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.CourseGreens
struct  CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.CourseGreens::courseId
	int32_t ___courseId_0;
	// System.String Strackaline.CourseGreens::name
	String_t* ___name_1;
	// Strackaline.GreenGroup[] Strackaline.CourseGreens::groups
	GreenGroupU5BU5D_t234E8DF95534A84DF11AB456DD4053B38FCB207D* ___groups_2;

public:
	inline static int32_t get_offset_of_courseId_0() { return static_cast<int32_t>(offsetof(CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D, ___courseId_0)); }
	inline int32_t get_courseId_0() const { return ___courseId_0; }
	inline int32_t* get_address_of_courseId_0() { return &___courseId_0; }
	inline void set_courseId_0(int32_t value)
	{
		___courseId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_groups_2() { return static_cast<int32_t>(offsetof(CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D, ___groups_2)); }
	inline GreenGroupU5BU5D_t234E8DF95534A84DF11AB456DD4053B38FCB207D* get_groups_2() const { return ___groups_2; }
	inline GreenGroupU5BU5D_t234E8DF95534A84DF11AB456DD4053B38FCB207D** get_address_of_groups_2() { return &___groups_2; }
	inline void set_groups_2(GreenGroupU5BU5D_t234E8DF95534A84DF11AB456DD4053B38FCB207D* value)
	{
		___groups_2 = value;
		Il2CppCodeGenWriteBarrier((&___groups_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSEGREENS_TA5A23B34F0E3B9114AB312F86C40E6A63492CE8D_H
#ifndef COURSESETTINGS_T5B916EF8B384B1B741AD022E2C29A533ADBE7352_H
#define COURSESETTINGS_T5B916EF8B384B1B741AD022E2C29A533ADBE7352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.CourseSettings
struct  CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352  : public RuntimeObject
{
public:
	// System.Boolean Strackaline.CourseSettings::isHoleLocationSoftware
	bool ___isHoleLocationSoftware_0;
	// System.Int32 Strackaline.CourseSettings::holoVersion
	int32_t ___holoVersion_1;
	// System.Boolean Strackaline.CourseSettings::isLockedFromSale
	bool ___isLockedFromSale_2;
	// System.String Strackaline.CourseSettings::customLockMessage
	String_t* ___customLockMessage_3;
	// System.Int32 Strackaline.CourseSettings::lastPinsheetTemplateId
	int32_t ___lastPinsheetTemplateId_4;
	// System.Boolean Strackaline.CourseSettings::isCourseDataLocked
	bool ___isCourseDataLocked_5;
	// System.Boolean Strackaline.CourseSettings::isGreenDataLocked
	bool ___isGreenDataLocked_6;
	// System.Boolean Strackaline.CourseSettings::isGpsDataLocked
	bool ___isGpsDataLocked_7;

public:
	inline static int32_t get_offset_of_isHoleLocationSoftware_0() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___isHoleLocationSoftware_0)); }
	inline bool get_isHoleLocationSoftware_0() const { return ___isHoleLocationSoftware_0; }
	inline bool* get_address_of_isHoleLocationSoftware_0() { return &___isHoleLocationSoftware_0; }
	inline void set_isHoleLocationSoftware_0(bool value)
	{
		___isHoleLocationSoftware_0 = value;
	}

	inline static int32_t get_offset_of_holoVersion_1() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___holoVersion_1)); }
	inline int32_t get_holoVersion_1() const { return ___holoVersion_1; }
	inline int32_t* get_address_of_holoVersion_1() { return &___holoVersion_1; }
	inline void set_holoVersion_1(int32_t value)
	{
		___holoVersion_1 = value;
	}

	inline static int32_t get_offset_of_isLockedFromSale_2() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___isLockedFromSale_2)); }
	inline bool get_isLockedFromSale_2() const { return ___isLockedFromSale_2; }
	inline bool* get_address_of_isLockedFromSale_2() { return &___isLockedFromSale_2; }
	inline void set_isLockedFromSale_2(bool value)
	{
		___isLockedFromSale_2 = value;
	}

	inline static int32_t get_offset_of_customLockMessage_3() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___customLockMessage_3)); }
	inline String_t* get_customLockMessage_3() const { return ___customLockMessage_3; }
	inline String_t** get_address_of_customLockMessage_3() { return &___customLockMessage_3; }
	inline void set_customLockMessage_3(String_t* value)
	{
		___customLockMessage_3 = value;
		Il2CppCodeGenWriteBarrier((&___customLockMessage_3), value);
	}

	inline static int32_t get_offset_of_lastPinsheetTemplateId_4() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___lastPinsheetTemplateId_4)); }
	inline int32_t get_lastPinsheetTemplateId_4() const { return ___lastPinsheetTemplateId_4; }
	inline int32_t* get_address_of_lastPinsheetTemplateId_4() { return &___lastPinsheetTemplateId_4; }
	inline void set_lastPinsheetTemplateId_4(int32_t value)
	{
		___lastPinsheetTemplateId_4 = value;
	}

	inline static int32_t get_offset_of_isCourseDataLocked_5() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___isCourseDataLocked_5)); }
	inline bool get_isCourseDataLocked_5() const { return ___isCourseDataLocked_5; }
	inline bool* get_address_of_isCourseDataLocked_5() { return &___isCourseDataLocked_5; }
	inline void set_isCourseDataLocked_5(bool value)
	{
		___isCourseDataLocked_5 = value;
	}

	inline static int32_t get_offset_of_isGreenDataLocked_6() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___isGreenDataLocked_6)); }
	inline bool get_isGreenDataLocked_6() const { return ___isGreenDataLocked_6; }
	inline bool* get_address_of_isGreenDataLocked_6() { return &___isGreenDataLocked_6; }
	inline void set_isGreenDataLocked_6(bool value)
	{
		___isGreenDataLocked_6 = value;
	}

	inline static int32_t get_offset_of_isGpsDataLocked_7() { return static_cast<int32_t>(offsetof(CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352, ___isGpsDataLocked_7)); }
	inline bool get_isGpsDataLocked_7() const { return ___isGpsDataLocked_7; }
	inline bool* get_address_of_isGpsDataLocked_7() { return &___isGpsDataLocked_7; }
	inline void set_isGpsDataLocked_7(bool value)
	{
		___isGpsDataLocked_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSESETTINGS_T5B916EF8B384B1B741AD022E2C29A533ADBE7352_H
#ifndef GREEN_TF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6_H
#define GREEN_TF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Green
struct  Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6  : public RuntimeObject
{
public:
	// Strackaline.HoleLocation[] Strackaline.Green::holeLocations
	HoleLocationU5BU5D_tDE014607400D5140EDEB18539A0F84DF3E28E3C2* ___holeLocations_0;
	// System.String Strackaline.Green::name
	String_t* ___name_1;
	// System.Single Strackaline.Green::rotation
	float ___rotation_2;
	// System.Int32 Strackaline.Green::greenId
	int32_t ___greenId_3;
	// System.Int32 Strackaline.Green::groupId
	int32_t ___groupId_4;
	// System.Int32 Strackaline.Green::number
	int32_t ___number_5;
	// System.Single Strackaline.Green::latitude
	float ___latitude_6;
	// System.Single Strackaline.Green::longitude
	float ___longitude_7;
	// System.Int32 Strackaline.Green::trueNorth
	int32_t ___trueNorth_8;
	// System.String Strackaline.Green::DXFFile
	String_t* ___DXFFile_9;

public:
	inline static int32_t get_offset_of_holeLocations_0() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___holeLocations_0)); }
	inline HoleLocationU5BU5D_tDE014607400D5140EDEB18539A0F84DF3E28E3C2* get_holeLocations_0() const { return ___holeLocations_0; }
	inline HoleLocationU5BU5D_tDE014607400D5140EDEB18539A0F84DF3E28E3C2** get_address_of_holeLocations_0() { return &___holeLocations_0; }
	inline void set_holeLocations_0(HoleLocationU5BU5D_tDE014607400D5140EDEB18539A0F84DF3E28E3C2* value)
	{
		___holeLocations_0 = value;
		Il2CppCodeGenWriteBarrier((&___holeLocations_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___rotation_2)); }
	inline float get_rotation_2() const { return ___rotation_2; }
	inline float* get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(float value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_greenId_3() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___greenId_3)); }
	inline int32_t get_greenId_3() const { return ___greenId_3; }
	inline int32_t* get_address_of_greenId_3() { return &___greenId_3; }
	inline void set_greenId_3(int32_t value)
	{
		___greenId_3 = value;
	}

	inline static int32_t get_offset_of_groupId_4() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___groupId_4)); }
	inline int32_t get_groupId_4() const { return ___groupId_4; }
	inline int32_t* get_address_of_groupId_4() { return &___groupId_4; }
	inline void set_groupId_4(int32_t value)
	{
		___groupId_4 = value;
	}

	inline static int32_t get_offset_of_number_5() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___number_5)); }
	inline int32_t get_number_5() const { return ___number_5; }
	inline int32_t* get_address_of_number_5() { return &___number_5; }
	inline void set_number_5(int32_t value)
	{
		___number_5 = value;
	}

	inline static int32_t get_offset_of_latitude_6() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___latitude_6)); }
	inline float get_latitude_6() const { return ___latitude_6; }
	inline float* get_address_of_latitude_6() { return &___latitude_6; }
	inline void set_latitude_6(float value)
	{
		___latitude_6 = value;
	}

	inline static int32_t get_offset_of_longitude_7() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___longitude_7)); }
	inline float get_longitude_7() const { return ___longitude_7; }
	inline float* get_address_of_longitude_7() { return &___longitude_7; }
	inline void set_longitude_7(float value)
	{
		___longitude_7 = value;
	}

	inline static int32_t get_offset_of_trueNorth_8() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___trueNorth_8)); }
	inline int32_t get_trueNorth_8() const { return ___trueNorth_8; }
	inline int32_t* get_address_of_trueNorth_8() { return &___trueNorth_8; }
	inline void set_trueNorth_8(int32_t value)
	{
		___trueNorth_8 = value;
	}

	inline static int32_t get_offset_of_DXFFile_9() { return static_cast<int32_t>(offsetof(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6, ___DXFFile_9)); }
	inline String_t* get_DXFFile_9() const { return ___DXFFile_9; }
	inline String_t** get_address_of_DXFFile_9() { return &___DXFFile_9; }
	inline void set_DXFFile_9(String_t* value)
	{
		___DXFFile_9 = value;
		Il2CppCodeGenWriteBarrier((&___DXFFile_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREEN_TF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6_H
#ifndef GREENACTIVITY_T842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B_H
#define GREENACTIVITY_T842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.GreenActivity
struct  GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B  : public RuntimeObject
{
public:
	// System.String Strackaline.GreenActivity::greenId
	String_t* ___greenId_0;
	// System.Single Strackaline.GreenActivity::holeXPos
	float ___holeXPos_1;
	// System.Single Strackaline.GreenActivity::holeYPos
	float ___holeYPos_2;
	// System.Boolean Strackaline.GreenActivity::isPlaying
	bool ___isPlaying_3;
	// System.Boolean Strackaline.GreenActivity::isLooping
	bool ___isLooping_4;
	// System.Int32 Strackaline.GreenActivity::speed
	int32_t ___speed_5;
	// Strackaline.GreenCameraSettings Strackaline.GreenActivity::greenCameraSettings
	GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA * ___greenCameraSettings_6;
	// Strackaline.HoleContainerSettings Strackaline.GreenActivity::holeContainerSettings
	HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175 * ___holeContainerSettings_7;
	// Strackaline.GreenPutt[] Strackaline.GreenActivity::greenPutts
	GreenPuttU5BU5D_t338BD91014B3FBBEE954DF83C7966249D4A09D4E* ___greenPutts_8;

public:
	inline static int32_t get_offset_of_greenId_0() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___greenId_0)); }
	inline String_t* get_greenId_0() const { return ___greenId_0; }
	inline String_t** get_address_of_greenId_0() { return &___greenId_0; }
	inline void set_greenId_0(String_t* value)
	{
		___greenId_0 = value;
		Il2CppCodeGenWriteBarrier((&___greenId_0), value);
	}

	inline static int32_t get_offset_of_holeXPos_1() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___holeXPos_1)); }
	inline float get_holeXPos_1() const { return ___holeXPos_1; }
	inline float* get_address_of_holeXPos_1() { return &___holeXPos_1; }
	inline void set_holeXPos_1(float value)
	{
		___holeXPos_1 = value;
	}

	inline static int32_t get_offset_of_holeYPos_2() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___holeYPos_2)); }
	inline float get_holeYPos_2() const { return ___holeYPos_2; }
	inline float* get_address_of_holeYPos_2() { return &___holeYPos_2; }
	inline void set_holeYPos_2(float value)
	{
		___holeYPos_2 = value;
	}

	inline static int32_t get_offset_of_isPlaying_3() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___isPlaying_3)); }
	inline bool get_isPlaying_3() const { return ___isPlaying_3; }
	inline bool* get_address_of_isPlaying_3() { return &___isPlaying_3; }
	inline void set_isPlaying_3(bool value)
	{
		___isPlaying_3 = value;
	}

	inline static int32_t get_offset_of_isLooping_4() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___isLooping_4)); }
	inline bool get_isLooping_4() const { return ___isLooping_4; }
	inline bool* get_address_of_isLooping_4() { return &___isLooping_4; }
	inline void set_isLooping_4(bool value)
	{
		___isLooping_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___speed_5)); }
	inline int32_t get_speed_5() const { return ___speed_5; }
	inline int32_t* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(int32_t value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_greenCameraSettings_6() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___greenCameraSettings_6)); }
	inline GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA * get_greenCameraSettings_6() const { return ___greenCameraSettings_6; }
	inline GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA ** get_address_of_greenCameraSettings_6() { return &___greenCameraSettings_6; }
	inline void set_greenCameraSettings_6(GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA * value)
	{
		___greenCameraSettings_6 = value;
		Il2CppCodeGenWriteBarrier((&___greenCameraSettings_6), value);
	}

	inline static int32_t get_offset_of_holeContainerSettings_7() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___holeContainerSettings_7)); }
	inline HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175 * get_holeContainerSettings_7() const { return ___holeContainerSettings_7; }
	inline HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175 ** get_address_of_holeContainerSettings_7() { return &___holeContainerSettings_7; }
	inline void set_holeContainerSettings_7(HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175 * value)
	{
		___holeContainerSettings_7 = value;
		Il2CppCodeGenWriteBarrier((&___holeContainerSettings_7), value);
	}

	inline static int32_t get_offset_of_greenPutts_8() { return static_cast<int32_t>(offsetof(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B, ___greenPutts_8)); }
	inline GreenPuttU5BU5D_t338BD91014B3FBBEE954DF83C7966249D4A09D4E* get_greenPutts_8() const { return ___greenPutts_8; }
	inline GreenPuttU5BU5D_t338BD91014B3FBBEE954DF83C7966249D4A09D4E** get_address_of_greenPutts_8() { return &___greenPutts_8; }
	inline void set_greenPutts_8(GreenPuttU5BU5D_t338BD91014B3FBBEE954DF83C7966249D4A09D4E* value)
	{
		___greenPutts_8 = value;
		Il2CppCodeGenWriteBarrier((&___greenPutts_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENACTIVITY_T842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B_H
#ifndef GREENARROW_T65F2D62E92115DC1945EAB666D741EB94A8BF86A_H
#define GREENARROW_T65F2D62E92115DC1945EAB666D741EB94A8BF86A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.GreenArrow
struct  GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A  : public RuntimeObject
{
public:
	// Strackaline.Arrow[] Strackaline.GreenArrow::arrows1
	ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* ___arrows1_0;
	// Strackaline.Arrow[] Strackaline.GreenArrow::arrows2
	ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* ___arrows2_1;
	// Strackaline.Arrow[] Strackaline.GreenArrow::arrows3
	ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* ___arrows3_2;

public:
	inline static int32_t get_offset_of_arrows1_0() { return static_cast<int32_t>(offsetof(GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A, ___arrows1_0)); }
	inline ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* get_arrows1_0() const { return ___arrows1_0; }
	inline ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40** get_address_of_arrows1_0() { return &___arrows1_0; }
	inline void set_arrows1_0(ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* value)
	{
		___arrows1_0 = value;
		Il2CppCodeGenWriteBarrier((&___arrows1_0), value);
	}

	inline static int32_t get_offset_of_arrows2_1() { return static_cast<int32_t>(offsetof(GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A, ___arrows2_1)); }
	inline ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* get_arrows2_1() const { return ___arrows2_1; }
	inline ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40** get_address_of_arrows2_1() { return &___arrows2_1; }
	inline void set_arrows2_1(ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* value)
	{
		___arrows2_1 = value;
		Il2CppCodeGenWriteBarrier((&___arrows2_1), value);
	}

	inline static int32_t get_offset_of_arrows3_2() { return static_cast<int32_t>(offsetof(GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A, ___arrows3_2)); }
	inline ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* get_arrows3_2() const { return ___arrows3_2; }
	inline ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40** get_address_of_arrows3_2() { return &___arrows3_2; }
	inline void set_arrows3_2(ArrowU5BU5D_tE923FA8F224867CBEA6B5379602E5A55C8ADEE40* value)
	{
		___arrows3_2 = value;
		Il2CppCodeGenWriteBarrier((&___arrows3_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENARROW_T65F2D62E92115DC1945EAB666D741EB94A8BF86A_H
#ifndef GREENCAMERASETTINGS_T2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA_H
#define GREENCAMERASETTINGS_T2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.GreenCameraSettings
struct  GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA  : public RuntimeObject
{
public:
	// System.Single Strackaline.GreenCameraSettings::xPos
	float ___xPos_0;
	// System.Single Strackaline.GreenCameraSettings::yPos
	float ___yPos_1;
	// System.Single Strackaline.GreenCameraSettings::zPos
	float ___zPos_2;
	// System.Single Strackaline.GreenCameraSettings::size
	float ___size_3;
	// System.Single Strackaline.GreenCameraSettings::yRot
	float ___yRot_4;

public:
	inline static int32_t get_offset_of_xPos_0() { return static_cast<int32_t>(offsetof(GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA, ___xPos_0)); }
	inline float get_xPos_0() const { return ___xPos_0; }
	inline float* get_address_of_xPos_0() { return &___xPos_0; }
	inline void set_xPos_0(float value)
	{
		___xPos_0 = value;
	}

	inline static int32_t get_offset_of_yPos_1() { return static_cast<int32_t>(offsetof(GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA, ___yPos_1)); }
	inline float get_yPos_1() const { return ___yPos_1; }
	inline float* get_address_of_yPos_1() { return &___yPos_1; }
	inline void set_yPos_1(float value)
	{
		___yPos_1 = value;
	}

	inline static int32_t get_offset_of_zPos_2() { return static_cast<int32_t>(offsetof(GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA, ___zPos_2)); }
	inline float get_zPos_2() const { return ___zPos_2; }
	inline float* get_address_of_zPos_2() { return &___zPos_2; }
	inline void set_zPos_2(float value)
	{
		___zPos_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA, ___size_3)); }
	inline float get_size_3() const { return ___size_3; }
	inline float* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(float value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_yRot_4() { return static_cast<int32_t>(offsetof(GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA, ___yRot_4)); }
	inline float get_yRot_4() const { return ___yRot_4; }
	inline float* get_address_of_yRot_4() { return &___yRot_4; }
	inline void set_yRot_4(float value)
	{
		___yRot_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENCAMERASETTINGS_T2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA_H
#ifndef GREENPUTT_T85D3648F7CB55611A8F377FFF5FAA424C6591BFF_H
#define GREENPUTT_T85D3648F7CB55611A8F377FFF5FAA424C6591BFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.GreenPutt
struct  GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF  : public RuntimeObject
{
public:
	// System.Single Strackaline.GreenPutt::ballXPos
	float ___ballXPos_0;
	// System.Single Strackaline.GreenPutt::ballYPos
	float ___ballYPos_1;

public:
	inline static int32_t get_offset_of_ballXPos_0() { return static_cast<int32_t>(offsetof(GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF, ___ballXPos_0)); }
	inline float get_ballXPos_0() const { return ___ballXPos_0; }
	inline float* get_address_of_ballXPos_0() { return &___ballXPos_0; }
	inline void set_ballXPos_0(float value)
	{
		___ballXPos_0 = value;
	}

	inline static int32_t get_offset_of_ballYPos_1() { return static_cast<int32_t>(offsetof(GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF, ___ballYPos_1)); }
	inline float get_ballYPos_1() const { return ___ballYPos_1; }
	inline float* get_address_of_ballYPos_1() { return &___ballYPos_1; }
	inline void set_ballYPos_1(float value)
	{
		___ballYPos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENPUTT_T85D3648F7CB55611A8F377FFF5FAA424C6591BFF_H
#ifndef HOLE_T080CFDB711495EFE21793D6BF4AEB1D9DB9569AB_H
#define HOLE_T080CFDB711495EFE21793D6BF4AEB1D9DB9569AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Hole
struct  Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB  : public RuntimeObject
{
public:
	// System.Single Strackaline.Hole::x
	float ___x_0;
	// System.Single Strackaline.Hole::y
	float ___y_1;
	// System.Single Strackaline.Hole::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLE_T080CFDB711495EFE21793D6BF4AEB1D9DB9569AB_H
#ifndef HOLECONTAINERSETTINGS_TE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175_H
#define HOLECONTAINERSETTINGS_TE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.HoleContainerSettings
struct  HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175  : public RuntimeObject
{
public:
	// System.Single Strackaline.HoleContainerSettings::xPos
	float ___xPos_0;
	// System.Single Strackaline.HoleContainerSettings::yPos
	float ___yPos_1;
	// System.Single Strackaline.HoleContainerSettings::zPos
	float ___zPos_2;
	// System.Single Strackaline.HoleContainerSettings::holeRectYRotation
	float ___holeRectYRotation_3;

public:
	inline static int32_t get_offset_of_xPos_0() { return static_cast<int32_t>(offsetof(HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175, ___xPos_0)); }
	inline float get_xPos_0() const { return ___xPos_0; }
	inline float* get_address_of_xPos_0() { return &___xPos_0; }
	inline void set_xPos_0(float value)
	{
		___xPos_0 = value;
	}

	inline static int32_t get_offset_of_yPos_1() { return static_cast<int32_t>(offsetof(HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175, ___yPos_1)); }
	inline float get_yPos_1() const { return ___yPos_1; }
	inline float* get_address_of_yPos_1() { return &___yPos_1; }
	inline void set_yPos_1(float value)
	{
		___yPos_1 = value;
	}

	inline static int32_t get_offset_of_zPos_2() { return static_cast<int32_t>(offsetof(HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175, ___zPos_2)); }
	inline float get_zPos_2() const { return ___zPos_2; }
	inline float* get_address_of_zPos_2() { return &___zPos_2; }
	inline void set_zPos_2(float value)
	{
		___zPos_2 = value;
	}

	inline static int32_t get_offset_of_holeRectYRotation_3() { return static_cast<int32_t>(offsetof(HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175, ___holeRectYRotation_3)); }
	inline float get_holeRectYRotation_3() const { return ___holeRectYRotation_3; }
	inline float* get_address_of_holeRectYRotation_3() { return &___holeRectYRotation_3; }
	inline void set_holeRectYRotation_3(float value)
	{
		___holeRectYRotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLECONTAINERSETTINGS_TE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175_H
#ifndef HOLELOCATION_T135B1EC1A08FDDCCA91211A88ED834758DE5F3BB_H
#define HOLELOCATION_T135B1EC1A08FDDCCA91211A88ED834758DE5F3BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.HoleLocation
struct  HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.HoleLocation::id
	int32_t ___id_0;
	// System.Int32 Strackaline.HoleLocation::greenId
	int32_t ___greenId_1;
	// System.Single Strackaline.HoleLocation::rotation
	float ___rotation_2;
	// System.Single Strackaline.HoleLocation::x
	float ___x_3;
	// System.Single Strackaline.HoleLocation::y
	float ___y_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_greenId_1() { return static_cast<int32_t>(offsetof(HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB, ___greenId_1)); }
	inline int32_t get_greenId_1() const { return ___greenId_1; }
	inline int32_t* get_address_of_greenId_1() { return &___greenId_1; }
	inline void set_greenId_1(int32_t value)
	{
		___greenId_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB, ___rotation_2)); }
	inline float get_rotation_2() const { return ___rotation_2; }
	inline float* get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(float value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_x_3() { return static_cast<int32_t>(offsetof(HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB, ___x_3)); }
	inline float get_x_3() const { return ___x_3; }
	inline float* get_address_of_x_3() { return &___x_3; }
	inline void set_x_3(float value)
	{
		___x_3 = value;
	}

	inline static int32_t get_offset_of_y_4() { return static_cast<int32_t>(offsetof(HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB, ___y_4)); }
	inline float get_y_4() const { return ___y_4; }
	inline float* get_address_of_y_4() { return &___y_4; }
	inline void set_y_4(float value)
	{
		___y_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLELOCATION_T135B1EC1A08FDDCCA91211A88ED834758DE5F3BB_H
#ifndef JACK_T01FBC933A347E5DC16285B8B823E26570C8692DC_H
#define JACK_T01FBC933A347E5DC16285B8B823E26570C8692DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Jack
struct  Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.Jack::index
	int32_t ___index_0;
	// System.Single Strackaline.Jack::elevation
	float ___elevation_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_elevation_1() { return static_cast<int32_t>(offsetof(Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC, ___elevation_1)); }
	inline float get_elevation_1() const { return ___elevation_1; }
	inline float* get_address_of_elevation_1() { return &___elevation_1; }
	inline void set_elevation_1(float value)
	{
		___elevation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JACK_T01FBC933A347E5DC16285B8B823E26570C8692DC_H
#ifndef JACKDATA_T12425D7D49BAFF551ACC09A9DE16CA779F3740D1_H
#define JACKDATA_T12425D7D49BAFF551ACC09A9DE16CA779F3740D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.JackData
struct  JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1  : public RuntimeObject
{
public:
	// Strackaline.Jack[] Strackaline.JackData::jacks
	JackU5BU5D_tFF5E434F45443DDB4153C743C370D479DEB623B6* ___jacks_0;
	// System.Int32 Strackaline.JackData::jackId
	int32_t ___jackId_1;
	// System.String Strackaline.JackData::location
	String_t* ___location_2;

public:
	inline static int32_t get_offset_of_jacks_0() { return static_cast<int32_t>(offsetof(JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1, ___jacks_0)); }
	inline JackU5BU5D_tFF5E434F45443DDB4153C743C370D479DEB623B6* get_jacks_0() const { return ___jacks_0; }
	inline JackU5BU5D_tFF5E434F45443DDB4153C743C370D479DEB623B6** get_address_of_jacks_0() { return &___jacks_0; }
	inline void set_jacks_0(JackU5BU5D_tFF5E434F45443DDB4153C743C370D479DEB623B6* value)
	{
		___jacks_0 = value;
		Il2CppCodeGenWriteBarrier((&___jacks_0), value);
	}

	inline static int32_t get_offset_of_jackId_1() { return static_cast<int32_t>(offsetof(JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1, ___jackId_1)); }
	inline int32_t get_jackId_1() const { return ___jackId_1; }
	inline int32_t* get_address_of_jackId_1() { return &___jackId_1; }
	inline void set_jackId_1(int32_t value)
	{
		___jackId_1 = value;
	}

	inline static int32_t get_offset_of_location_2() { return static_cast<int32_t>(offsetof(JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1, ___location_2)); }
	inline String_t* get_location_2() const { return ___location_2; }
	inline String_t** get_address_of_location_2() { return &___location_2; }
	inline void set_location_2(String_t* value)
	{
		___location_2 = value;
		Il2CppCodeGenWriteBarrier((&___location_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JACKDATA_T12425D7D49BAFF551ACC09A9DE16CA779F3740D1_H
#ifndef LOCATION_T54F881D6DBE8E237E8697450BB8C10F1BBE668B6_H
#define LOCATION_T54F881D6DBE8E237E8697450BB8C10F1BBE668B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Location
struct  Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6  : public RuntimeObject
{
public:
	// System.Single Strackaline.Location::x
	float ___x_0;
	// System.Single Strackaline.Location::y
	float ___y_1;
	// System.Single Strackaline.Location::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATION_T54F881D6DBE8E237E8697450BB8C10F1BBE668B6_H
#ifndef POINT_TD538CCF5709B7B64D27871B2B0EC83FEC97E93C2_H
#define POINT_TD538CCF5709B7B64D27871B2B0EC83FEC97E93C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Point
struct  Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2  : public RuntimeObject
{
public:
	// System.Single Strackaline.Point::speed
	float ___speed_0;
	// Strackaline.Location Strackaline.Point::location
	Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6 * ___location_1;
	// System.Single Strackaline.Point::slope
	float ___slope_2;

public:
	inline static int32_t get_offset_of_speed_0() { return static_cast<int32_t>(offsetof(Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2, ___speed_0)); }
	inline float get_speed_0() const { return ___speed_0; }
	inline float* get_address_of_speed_0() { return &___speed_0; }
	inline void set_speed_0(float value)
	{
		___speed_0 = value;
	}

	inline static int32_t get_offset_of_location_1() { return static_cast<int32_t>(offsetof(Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2, ___location_1)); }
	inline Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6 * get_location_1() const { return ___location_1; }
	inline Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6 ** get_address_of_location_1() { return &___location_1; }
	inline void set_location_1(Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6 * value)
	{
		___location_1 = value;
		Il2CppCodeGenWriteBarrier((&___location_1), value);
	}

	inline static int32_t get_offset_of_slope_2() { return static_cast<int32_t>(offsetof(Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2, ___slope_2)); }
	inline float get_slope_2() const { return ___slope_2; }
	inline float* get_address_of_slope_2() { return &___slope_2; }
	inline void set_slope_2(float value)
	{
		___slope_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT_TD538CCF5709B7B64D27871B2B0EC83FEC97E93C2_H
#ifndef PROJECTORDATA_T578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88_H
#define PROJECTORDATA_T578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.ProjectorData
struct  ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88  : public RuntimeObject
{
public:
	// Strackaline.Configurator[] Strackaline.ProjectorData::value
	ConfiguratorU5BU5D_t4686BDCF6BA019E7D7CB4D4739199419814D1C45* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88, ___value_0)); }
	inline ConfiguratorU5BU5D_t4686BDCF6BA019E7D7CB4D4739199419814D1C45* get_value_0() const { return ___value_0; }
	inline ConfiguratorU5BU5D_t4686BDCF6BA019E7D7CB4D4739199419814D1C45** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(ConfiguratorU5BU5D_t4686BDCF6BA019E7D7CB4D4739199419814D1C45* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTORDATA_T578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88_H
#ifndef PUTT_T747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49_H
#define PUTT_T747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Putt
struct  Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.Putt::puttID
	int32_t ___puttID_0;
	// System.Boolean Strackaline.Putt::isValidPutt
	bool ___isValidPutt_1;
	// System.String Strackaline.Putt::message
	String_t* ___message_2;
	// Strackaline.Ball Strackaline.Putt::ball
	Ball_t094765B02879225F8C595564E40A823B70AEE45E * ___ball_3;
	// Strackaline.Hole Strackaline.Putt::hole
	Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB * ___hole_4;
	// Strackaline.Target Strackaline.Putt::target
	Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139 * ___target_5;
	// System.Single Strackaline.Putt::ballToHoleDistance
	float ___ballToHoleDistance_6;
	// System.Single Strackaline.Putt::puttingDistance
	float ___puttingDistance_7;
	// System.Single Strackaline.Putt::correctionAngle
	float ___correctionAngle_8;
	// System.Single Strackaline.Putt::elevationChange
	float ___elevationChange_9;
	// System.Single Strackaline.Putt::speedFactor
	float ___speedFactor_10;
	// Strackaline.Point[] Strackaline.Putt::points
	PointU5BU5D_t4C35F8E2A134BA9FD1B58D2549F98E19EC3751C9* ___points_11;
	// Strackaline.Instruction Strackaline.Putt::instructions
	Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C * ___instructions_12;

public:
	inline static int32_t get_offset_of_puttID_0() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___puttID_0)); }
	inline int32_t get_puttID_0() const { return ___puttID_0; }
	inline int32_t* get_address_of_puttID_0() { return &___puttID_0; }
	inline void set_puttID_0(int32_t value)
	{
		___puttID_0 = value;
	}

	inline static int32_t get_offset_of_isValidPutt_1() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___isValidPutt_1)); }
	inline bool get_isValidPutt_1() const { return ___isValidPutt_1; }
	inline bool* get_address_of_isValidPutt_1() { return &___isValidPutt_1; }
	inline void set_isValidPutt_1(bool value)
	{
		___isValidPutt_1 = value;
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_ball_3() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___ball_3)); }
	inline Ball_t094765B02879225F8C595564E40A823B70AEE45E * get_ball_3() const { return ___ball_3; }
	inline Ball_t094765B02879225F8C595564E40A823B70AEE45E ** get_address_of_ball_3() { return &___ball_3; }
	inline void set_ball_3(Ball_t094765B02879225F8C595564E40A823B70AEE45E * value)
	{
		___ball_3 = value;
		Il2CppCodeGenWriteBarrier((&___ball_3), value);
	}

	inline static int32_t get_offset_of_hole_4() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___hole_4)); }
	inline Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB * get_hole_4() const { return ___hole_4; }
	inline Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB ** get_address_of_hole_4() { return &___hole_4; }
	inline void set_hole_4(Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB * value)
	{
		___hole_4 = value;
		Il2CppCodeGenWriteBarrier((&___hole_4), value);
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___target_5)); }
	inline Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139 * get_target_5() const { return ___target_5; }
	inline Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier((&___target_5), value);
	}

	inline static int32_t get_offset_of_ballToHoleDistance_6() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___ballToHoleDistance_6)); }
	inline float get_ballToHoleDistance_6() const { return ___ballToHoleDistance_6; }
	inline float* get_address_of_ballToHoleDistance_6() { return &___ballToHoleDistance_6; }
	inline void set_ballToHoleDistance_6(float value)
	{
		___ballToHoleDistance_6 = value;
	}

	inline static int32_t get_offset_of_puttingDistance_7() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___puttingDistance_7)); }
	inline float get_puttingDistance_7() const { return ___puttingDistance_7; }
	inline float* get_address_of_puttingDistance_7() { return &___puttingDistance_7; }
	inline void set_puttingDistance_7(float value)
	{
		___puttingDistance_7 = value;
	}

	inline static int32_t get_offset_of_correctionAngle_8() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___correctionAngle_8)); }
	inline float get_correctionAngle_8() const { return ___correctionAngle_8; }
	inline float* get_address_of_correctionAngle_8() { return &___correctionAngle_8; }
	inline void set_correctionAngle_8(float value)
	{
		___correctionAngle_8 = value;
	}

	inline static int32_t get_offset_of_elevationChange_9() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___elevationChange_9)); }
	inline float get_elevationChange_9() const { return ___elevationChange_9; }
	inline float* get_address_of_elevationChange_9() { return &___elevationChange_9; }
	inline void set_elevationChange_9(float value)
	{
		___elevationChange_9 = value;
	}

	inline static int32_t get_offset_of_speedFactor_10() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___speedFactor_10)); }
	inline float get_speedFactor_10() const { return ___speedFactor_10; }
	inline float* get_address_of_speedFactor_10() { return &___speedFactor_10; }
	inline void set_speedFactor_10(float value)
	{
		___speedFactor_10 = value;
	}

	inline static int32_t get_offset_of_points_11() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___points_11)); }
	inline PointU5BU5D_t4C35F8E2A134BA9FD1B58D2549F98E19EC3751C9* get_points_11() const { return ___points_11; }
	inline PointU5BU5D_t4C35F8E2A134BA9FD1B58D2549F98E19EC3751C9** get_address_of_points_11() { return &___points_11; }
	inline void set_points_11(PointU5BU5D_t4C35F8E2A134BA9FD1B58D2549F98E19EC3751C9* value)
	{
		___points_11 = value;
		Il2CppCodeGenWriteBarrier((&___points_11), value);
	}

	inline static int32_t get_offset_of_instructions_12() { return static_cast<int32_t>(offsetof(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49, ___instructions_12)); }
	inline Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C * get_instructions_12() const { return ___instructions_12; }
	inline Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C ** get_address_of_instructions_12() { return &___instructions_12; }
	inline void set_instructions_12(Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C * value)
	{
		___instructions_12 = value;
		Il2CppCodeGenWriteBarrier((&___instructions_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTT_T747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49_H
#ifndef PUTTVARIATION_T619DA0827B8D99CA80136275A4EADE66A1097A31_H
#define PUTTVARIATION_T619DA0827B8D99CA80136275A4EADE66A1097A31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.PuttVariation
struct  PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31  : public RuntimeObject
{
public:
	// Strackaline.Putt[] Strackaline.PuttVariation::value
	PuttU5BU5D_tA831E1874EAE75DBA6EBD968078DAA9B1E8D5E52* ___value_0;
	// System.Int32 Strackaline.PuttVariation::puttVariationID
	int32_t ___puttVariationID_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31, ___value_0)); }
	inline PuttU5BU5D_tA831E1874EAE75DBA6EBD968078DAA9B1E8D5E52* get_value_0() const { return ___value_0; }
	inline PuttU5BU5D_tA831E1874EAE75DBA6EBD968078DAA9B1E8D5E52** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(PuttU5BU5D_tA831E1874EAE75DBA6EBD968078DAA9B1E8D5E52* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_puttVariationID_1() { return static_cast<int32_t>(offsetof(PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31, ___puttVariationID_1)); }
	inline int32_t get_puttVariationID_1() const { return ___puttVariationID_1; }
	inline int32_t* get_address_of_puttVariationID_1() { return &___puttVariationID_1; }
	inline void set_puttVariationID_1(int32_t value)
	{
		___puttVariationID_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTVARIATION_T619DA0827B8D99CA80136275A4EADE66A1097A31_H
#ifndef TARGET_T7CBFB33F47C01EFEF533D160596EB06BB2A9B139_H
#define TARGET_T7CBFB33F47C01EFEF533D160596EB06BB2A9B139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Target
struct  Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139  : public RuntimeObject
{
public:
	// System.Single Strackaline.Target::x
	float ___x_0;
	// System.Single Strackaline.Target::y
	float ___y_1;
	// System.Single Strackaline.Target::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGET_T7CBFB33F47C01EFEF533D160596EB06BB2A9B139_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef TEXTUREPACKER_T2549189919276EB833F83EA937AB992420E1B199_H
#define TEXTUREPACKER_T2549189919276EB833F83EA937AB992420E1B199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker
struct  TexturePacker_t2549189919276EB833F83EA937AB992420E1B199  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPACKER_T2549189919276EB833F83EA937AB992420E1B199_H
#ifndef SPRITEDATAOBJECT_T3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E_H
#define SPRITEDATAOBJECT_T3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteDataObject
struct  SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker_SpriteData> TMPro.SpriteAssetUtilities.TexturePacker_SpriteDataObject::frames
	List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3 * ___frames_0;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E, ___frames_0)); }
	inline List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3 * get_frames_0() const { return ___frames_0; }
	inline List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3 ** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3 * value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEDATAOBJECT_T3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E_H
#ifndef USERSUBMITDATA_T87D189F6E674E0679D39FE190DB49929F41D48BF_H
#define USERSUBMITDATA_T87D189F6E674E0679D39FE190DB49929F41D48BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserSubmitData
struct  UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF  : public RuntimeObject
{
public:
	// System.String UserSubmitData::userName
	String_t* ___userName_0;
	// System.String UserSubmitData::password
	String_t* ___password_1;

public:
	inline static int32_t get_offset_of_userName_0() { return static_cast<int32_t>(offsetof(UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF, ___userName_0)); }
	inline String_t* get_userName_0() const { return ___userName_0; }
	inline String_t** get_address_of_userName_0() { return &___userName_0; }
	inline void set_userName_0(String_t* value)
	{
		___userName_0 = value;
		Il2CppCodeGenWriteBarrier((&___userName_0), value);
	}

	inline static int32_t get_offset_of_password_1() { return static_cast<int32_t>(offsetof(UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF, ___password_1)); }
	inline String_t* get_password_1() const { return ___password_1; }
	inline String_t** get_address_of_password_1() { return &___password_1; }
	inline void set_password_1(String_t* value)
	{
		___password_1 = value;
		Il2CppCodeGenWriteBarrier((&___password_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERSUBMITDATA_T87D189F6E674E0679D39FE190DB49929F41D48BF_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#define __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#define __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40
struct  __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#define MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fontAsset_1)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___material_3)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_3() const { return ___material_3; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fallbackMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifndef SPRITEFRAME_TDB681A7461FA0C10DA42E9A984BDDD0199AB2C04_H
#define SPRITEFRAME_TDB681A7461FA0C10DA42E9A984BDDD0199AB2C04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame
struct  SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::x
	float ___x_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::y
	float ___y_1;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::w
	float ___w_2;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::h
	float ___h_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___w_2)); }
	inline float get_w_2() const { return ___w_2; }
	inline float* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(float value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___h_3)); }
	inline float get_h_3() const { return ___h_3; }
	inline float* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(float value)
	{
		___h_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEFRAME_TDB681A7461FA0C10DA42E9A984BDDD0199AB2C04_H
#ifndef SPRITESIZE_T143F23923B1D48E84CB38DCDD532F408936AB67E_H
#define SPRITESIZE_T143F23923B1D48E84CB38DCDD532F408936AB67E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize
struct  SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize::w
	float ___w_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize::h
	float ___h_1;

public:
	inline static int32_t get_offset_of_w_0() { return static_cast<int32_t>(offsetof(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E, ___w_0)); }
	inline float get_w_0() const { return ___w_0; }
	inline float* get_address_of_w_0() { return &___w_0; }
	inline void set_w_0(float value)
	{
		___w_0 = value;
	}

	inline static int32_t get_offset_of_h_1() { return static_cast<int32_t>(offsetof(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E, ___h_1)); }
	inline float get_h_1() const { return ___h_1; }
	inline float* get_address_of_h_1() { return &___h_1; }
	inline void set_h_1(float value)
	{
		___h_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESIZE_T143F23923B1D48E84CB38DCDD532F408936AB67E_H
#ifndef TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#define TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontStyleStack
struct  TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 
{
public:
	// System.Byte TMPro.TMP_FontStyleStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_FontStyleStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_FontStyleStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_FontStyleStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_FontStyleStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_FontStyleStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_FontStyleStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_FontStyleStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_FontStyleStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_FontStyleStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#ifndef TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#define TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LinkInfo
struct  TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468 
{
public:
	// TMPro.TMP_Text TMPro.TMP_LinkInfo::textComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	// System.Int32 TMPro.TMP_LinkInfo::hashCode
	int32_t ___hashCode_1;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdFirstCharacterIndex
	int32_t ___linkIdFirstCharacterIndex_2;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdLength
	int32_t ___linkIdLength_3;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextfirstCharacterIndex
	int32_t ___linkTextfirstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextLength
	int32_t ___linkTextLength_5;
	// System.Char[] TMPro.TMP_LinkInfo::linkID
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___linkID_6;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___textComponent_0)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_linkIdFirstCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkIdFirstCharacterIndex_2)); }
	inline int32_t get_linkIdFirstCharacterIndex_2() const { return ___linkIdFirstCharacterIndex_2; }
	inline int32_t* get_address_of_linkIdFirstCharacterIndex_2() { return &___linkIdFirstCharacterIndex_2; }
	inline void set_linkIdFirstCharacterIndex_2(int32_t value)
	{
		___linkIdFirstCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_linkIdLength_3() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkIdLength_3)); }
	inline int32_t get_linkIdLength_3() const { return ___linkIdLength_3; }
	inline int32_t* get_address_of_linkIdLength_3() { return &___linkIdLength_3; }
	inline void set_linkIdLength_3(int32_t value)
	{
		___linkIdLength_3 = value;
	}

	inline static int32_t get_offset_of_linkTextfirstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkTextfirstCharacterIndex_4)); }
	inline int32_t get_linkTextfirstCharacterIndex_4() const { return ___linkTextfirstCharacterIndex_4; }
	inline int32_t* get_address_of_linkTextfirstCharacterIndex_4() { return &___linkTextfirstCharacterIndex_4; }
	inline void set_linkTextfirstCharacterIndex_4(int32_t value)
	{
		___linkTextfirstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_linkTextLength_5() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkTextLength_5)); }
	inline int32_t get_linkTextLength_5() const { return ___linkTextLength_5; }
	inline int32_t* get_address_of_linkTextLength_5() { return &___linkTextLength_5; }
	inline void set_linkTextLength_5(int32_t value)
	{
		___linkTextLength_5 = value;
	}

	inline static int32_t get_offset_of_linkID_6() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkID_6)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_linkID_6() const { return ___linkID_6; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_linkID_6() { return &___linkID_6; }
	inline void set_linkID_6(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___linkID_6 = value;
		Il2CppCodeGenWriteBarrier((&___linkID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468_marshaled_pinvoke
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
// Native definition for COM marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468_marshaled_com
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
#endif // TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#ifndef TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#define TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PageInfo
struct  TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24 
{
public:
	// System.Int32 TMPro.TMP_PageInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 TMPro.TMP_PageInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Single TMPro.TMP_PageInfo::ascender
	float ___ascender_2;
	// System.Single TMPro.TMP_PageInfo::baseLine
	float ___baseLine_3;
	// System.Single TMPro.TMP_PageInfo::descender
	float ___descender_4;

public:
	inline static int32_t get_offset_of_firstCharacterIndex_0() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___firstCharacterIndex_0)); }
	inline int32_t get_firstCharacterIndex_0() const { return ___firstCharacterIndex_0; }
	inline int32_t* get_address_of_firstCharacterIndex_0() { return &___firstCharacterIndex_0; }
	inline void set_firstCharacterIndex_0(int32_t value)
	{
		___firstCharacterIndex_0 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___lastCharacterIndex_1)); }
	inline int32_t get_lastCharacterIndex_1() const { return ___lastCharacterIndex_1; }
	inline int32_t* get_address_of_lastCharacterIndex_1() { return &___lastCharacterIndex_1; }
	inline void set_lastCharacterIndex_1(int32_t value)
	{
		___lastCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_ascender_2() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___ascender_2)); }
	inline float get_ascender_2() const { return ___ascender_2; }
	inline float* get_address_of_ascender_2() { return &___ascender_2; }
	inline void set_ascender_2(float value)
	{
		___ascender_2 = value;
	}

	inline static int32_t get_offset_of_baseLine_3() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___baseLine_3)); }
	inline float get_baseLine_3() const { return ___baseLine_3; }
	inline float* get_address_of_baseLine_3() { return &___baseLine_3; }
	inline void set_baseLine_3(float value)
	{
		___baseLine_3 = value;
	}

	inline static int32_t get_offset_of_descender_4() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___descender_4)); }
	inline float get_descender_4() const { return ___descender_4; }
	inline float* get_address_of_descender_4() { return &___descender_4; }
	inline void set_descender_4(float value)
	{
		___descender_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TC6AC19F231736A53E4E73CA41E8BE25BCCE04293_H
#define TMP_RICHTEXTTAGSTACK_1_TC6AC19F231736A53E4E73CA41E8BE25BCCE04293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<System.Int32>
struct  TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293, ___m_ItemStack_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TC6AC19F231736A53E4E73CA41E8BE25BCCE04293_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TD5CFAF6390F82194115C110DC92A2CFB29529106_H
#define TMP_RICHTEXTTAGSTACK_1_TD5CFAF6390F82194115C110DC92A2CFB29529106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<System.Single>
struct  TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	float ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106, ___m_ItemStack_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106, ___m_DefaultItem_3)); }
	inline float get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline float* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(float value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TD5CFAF6390F82194115C110DC92A2CFB29529106_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#define TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_ItemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_DefaultItem_3)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_DefaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#ifndef TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#define TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteInfo
struct  TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB 
{
public:
	// System.Int32 TMPro.TMP_SpriteInfo::spriteIndex
	int32_t ___spriteIndex_0;
	// System.Int32 TMPro.TMP_SpriteInfo::characterIndex
	int32_t ___characterIndex_1;
	// System.Int32 TMPro.TMP_SpriteInfo::vertexIndex
	int32_t ___vertexIndex_2;

public:
	inline static int32_t get_offset_of_spriteIndex_0() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___spriteIndex_0)); }
	inline int32_t get_spriteIndex_0() const { return ___spriteIndex_0; }
	inline int32_t* get_address_of_spriteIndex_0() { return &___spriteIndex_0; }
	inline void set_spriteIndex_0(int32_t value)
	{
		___spriteIndex_0 = value;
	}

	inline static int32_t get_offset_of_characterIndex_1() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___characterIndex_1)); }
	inline int32_t get_characterIndex_1() const { return ___characterIndex_1; }
	inline int32_t* get_address_of_characterIndex_1() { return &___characterIndex_1; }
	inline void set_characterIndex_1(int32_t value)
	{
		___characterIndex_1 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_2() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___vertexIndex_2)); }
	inline int32_t get_vertexIndex_2() const { return ___vertexIndex_2; }
	inline int32_t* get_address_of_vertexIndex_2() { return &___vertexIndex_2; }
	inline void set_vertexIndex_2(int32_t value)
	{
		___vertexIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#ifndef TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#define TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_WordInfo
struct  TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90 
{
public:
	// TMPro.TMP_Text TMPro.TMP_WordInfo::textComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	// System.Int32 TMPro.TMP_WordInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_1;
	// System.Int32 TMPro.TMP_WordInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_2;
	// System.Int32 TMPro.TMP_WordInfo::characterCount
	int32_t ___characterCount_3;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___textComponent_0)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_firstCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___firstCharacterIndex_1)); }
	inline int32_t get_firstCharacterIndex_1() const { return ___firstCharacterIndex_1; }
	inline int32_t* get_address_of_firstCharacterIndex_1() { return &___firstCharacterIndex_1; }
	inline void set_firstCharacterIndex_1(int32_t value)
	{
		___firstCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___lastCharacterIndex_2)); }
	inline int32_t get_lastCharacterIndex_2() const { return ___lastCharacterIndex_2; }
	inline int32_t* get_address_of_lastCharacterIndex_2() { return &___lastCharacterIndex_2; }
	inline void set_lastCharacterIndex_2(int32_t value)
	{
		___lastCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90_marshaled_pinvoke
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
// Native definition for COM marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90_marshaled_com
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
#endif // TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#ifndef TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#define TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagAttribute
struct  TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5 
{
public:
	// System.Int32 TMPro.TagAttribute::startIndex
	int32_t ___startIndex_0;
	// System.Int32 TMPro.TagAttribute::length
	int32_t ___length_1;
	// System.Int32 TMPro.TagAttribute::hashCode
	int32_t ___hashCode_2;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::9E6378168821DBABB7EE3D0154346480FAC8AEF1
	__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields, ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  get_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 * get_address_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  value)
	{
		___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields, ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  get_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D * get_address_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  value)
	{
		___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#ifndef U3CSTARTU3ED__4_T4F04AC75A036699A1B76C790F7523FEDB4A4569C_H
#define U3CSTARTU3ED__4_T4F04AC75A036699A1B76C790F7523FEDB4A4569C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator_<Start>d__4
struct  U3CStartU3Ed__4_t4F04AC75A036699A1B76C790F7523FEDB4A4569C  : public RuntimeObject
{
public:
	// System.Int32 EnvMapAnimator_<Start>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object EnvMapAnimator_<Start>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// EnvMapAnimator EnvMapAnimator_<Start>d__4::<>4__this
	EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73 * ___U3CU3E4__this_2;
	// UnityEngine.Matrix4x4 EnvMapAnimator_<Start>d__4::<matrix>5__1
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___U3CmatrixU3E5__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t4F04AC75A036699A1B76C790F7523FEDB4A4569C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t4F04AC75A036699A1B76C790F7523FEDB4A4569C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t4F04AC75A036699A1B76C790F7523FEDB4A4569C, ___U3CU3E4__this_2)); }
	inline EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t4F04AC75A036699A1B76C790F7523FEDB4A4569C, ___U3CmatrixU3E5__1_3)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_U3CmatrixU3E5__1_3() const { return ___U3CmatrixU3E5__1_3; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_U3CmatrixU3E5__1_3() { return &___U3CmatrixU3E5__1_3; }
	inline void set_U3CmatrixU3E5__1_3(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___U3CmatrixU3E5__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__4_T4F04AC75A036699A1B76C790F7523FEDB4A4569C_H
#ifndef DIRECTION_TC9AAD2ECFDDAE56D03AFC52A63D632079A965845_H
#define DIRECTION_TC9AAD2ECFDDAE56D03AFC52A63D632079A965845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuttRythm_Direction
struct  Direction_tC9AAD2ECFDDAE56D03AFC52A63D632079A965845 
{
public:
	// System.Int32 PuttRythm_Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tC9AAD2ECFDDAE56D03AFC52A63D632079A965845, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_TC9AAD2ECFDDAE56D03AFC52A63D632079A965845_H
#ifndef PUTTLINESPEED_T65D6B12615B69CD468407A6B02DFE4360724B3DD_H
#define PUTTLINESPEED_T65D6B12615B69CD468407A6B02DFE4360724B3DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimulationManager_PuttLineSpeed
struct  PuttLineSpeed_t65D6B12615B69CD468407A6B02DFE4360724B3DD 
{
public:
	// System.Int32 SimulationManager_PuttLineSpeed::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PuttLineSpeed_t65D6B12615B69CD468407A6B02DFE4360724B3DD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTLINESPEED_T65D6B12615B69CD468407A6B02DFE4360724B3DD_H
#ifndef GREENGROUPTYPE_TD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42_H
#define GREENGROUPTYPE_TD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.GreenGroup_GreenGroupType
struct  GreenGroupType_tD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42 
{
public:
	// System.Int32 Strackaline.GreenGroup_GreenGroupType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GreenGroupType_tD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENGROUPTYPE_TD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42_H
#ifndef EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#define EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifndef FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#define FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifndef FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#define FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontWeight
struct  FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C 
{
public:
	// System.Int32 TMPro.FontWeight::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#ifndef MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#define MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Mesh_Extents
struct  Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8 
{
public:
	// UnityEngine.Vector2 TMPro.Mesh_Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Mesh_Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#ifndef SPRITEASSETIMPORTFORMATS_T9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E_H
#define SPRITEASSETIMPORTFORMATS_T9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.SpriteAssetImportFormats
struct  SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E 
{
public:
	// System.Int32 TMPro.SpriteAssetUtilities.SpriteAssetImportFormats::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEASSETIMPORTFORMATS_T9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E_H
#ifndef SPRITEDATA_T8ADAD39500AF244CC913ED9B1F964DF04B4E5728_H
#define SPRITEDATA_T8ADAD39500AF244CC913ED9B1F964DF04B4E5728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteData
struct  SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728 
{
public:
	// System.String TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::filename
	String_t* ___filename_0;
	// TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::frame
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___frame_1;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::rotated
	bool ___rotated_2;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::trimmed
	bool ___trimmed_3;
	// TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::spriteSourceSize
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___spriteSourceSize_4;
	// TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::sourceSize
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  ___sourceSize_5;
	// UnityEngine.Vector2 TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::pivot
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_6;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___filename_0), value);
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___frame_1)); }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  get_frame_1() const { return ___frame_1; }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 * get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  value)
	{
		___frame_1 = value;
	}

	inline static int32_t get_offset_of_rotated_2() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___rotated_2)); }
	inline bool get_rotated_2() const { return ___rotated_2; }
	inline bool* get_address_of_rotated_2() { return &___rotated_2; }
	inline void set_rotated_2(bool value)
	{
		___rotated_2 = value;
	}

	inline static int32_t get_offset_of_trimmed_3() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___trimmed_3)); }
	inline bool get_trimmed_3() const { return ___trimmed_3; }
	inline bool* get_address_of_trimmed_3() { return &___trimmed_3; }
	inline void set_trimmed_3(bool value)
	{
		___trimmed_3 = value;
	}

	inline static int32_t get_offset_of_spriteSourceSize_4() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___spriteSourceSize_4)); }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  get_spriteSourceSize_4() const { return ___spriteSourceSize_4; }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 * get_address_of_spriteSourceSize_4() { return &___spriteSourceSize_4; }
	inline void set_spriteSourceSize_4(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  value)
	{
		___spriteSourceSize_4 = value;
	}

	inline static int32_t get_offset_of_sourceSize_5() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___sourceSize_5)); }
	inline SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  get_sourceSize_5() const { return ___sourceSize_5; }
	inline SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E * get_address_of_sourceSize_5() { return &___sourceSize_5; }
	inline void set_sourceSize_5(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  value)
	{
		___sourceSize_5 = value;
	}

	inline static int32_t get_offset_of_pivot_6() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___pivot_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_pivot_6() const { return ___pivot_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_pivot_6() { return &___pivot_6; }
	inline void set_pivot_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___pivot_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728_marshaled_pinvoke
{
	char* ___filename_0;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___spriteSourceSize_4;
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  ___sourceSize_5;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_6;
};
// Native definition for COM marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728_marshaled_com
{
	Il2CppChar* ___filename_0;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___spriteSourceSize_4;
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  ___sourceSize_5;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_6;
};
#endif // SPRITEDATA_T8ADAD39500AF244CC913ED9B1F964DF04B4E5728_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#define TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference>
struct  TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_ItemStack_0)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_DefaultItem_3)); }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F * get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#define TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32>
struct  TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_ItemStack_0)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_DefaultItem_3)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#ifndef TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#define TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_VertexDataUpdateFlags
struct  TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142 
{
public:
	// System.Int32 TMPro.TMP_VertexDataUpdateFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#ifndef TAGUNITTYPE_T5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF_H
#define TAGUNITTYPE_T5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagUnitType
struct  TagUnitType_t5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF 
{
public:
	// System.Int32 TMPro.TagUnitType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TagUnitType_t5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGUNITTYPE_T5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF_H
#ifndef TAGVALUETYPE_TB0AE4FE83F0293DDD337886C8D5268947F25F5CC_H
#define TAGVALUETYPE_TB0AE4FE83F0293DDD337886C8D5268947F25F5CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagValueType
struct  TagValueType_tB0AE4FE83F0293DDD337886C8D5268947F25F5CC 
{
public:
	// System.Int32 TMPro.TagValueType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TagValueType_tB0AE4FE83F0293DDD337886C8D5268947F25F5CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGVALUETYPE_TB0AE4FE83F0293DDD337886C8D5268947F25F5CC_H
#ifndef TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#define TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifndef VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#define VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topLeft_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topRight_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topRight_1() const { return ___topRight_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomLeft_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomRight_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef GREENGROUP_TC6C5A79BEB814B131AA6A0A209437F2DEAA19E97_H
#define GREENGROUP_TC6C5A79BEB814B131AA6A0A209437F2DEAA19E97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.GreenGroup
struct  GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.GreenGroup::groupId
	int32_t ___groupId_0;
	// System.String Strackaline.GreenGroup::name
	String_t* ___name_1;
	// System.Int32 Strackaline.GreenGroup::sortOrder
	int32_t ___sortOrder_2;
	// System.Single Strackaline.GreenGroup::stimp
	float ___stimp_3;
	// Strackaline.Green[] Strackaline.GreenGroup::greens
	GreenU5BU5D_t15AF86FEF7A53720C1A0C6658F9BEF084F966BA1* ___greens_4;
	// Strackaline.GreenGroup_GreenGroupType Strackaline.GreenGroup::greenGroupType
	int32_t ___greenGroupType_5;

public:
	inline static int32_t get_offset_of_groupId_0() { return static_cast<int32_t>(offsetof(GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97, ___groupId_0)); }
	inline int32_t get_groupId_0() const { return ___groupId_0; }
	inline int32_t* get_address_of_groupId_0() { return &___groupId_0; }
	inline void set_groupId_0(int32_t value)
	{
		___groupId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_sortOrder_2() { return static_cast<int32_t>(offsetof(GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97, ___sortOrder_2)); }
	inline int32_t get_sortOrder_2() const { return ___sortOrder_2; }
	inline int32_t* get_address_of_sortOrder_2() { return &___sortOrder_2; }
	inline void set_sortOrder_2(int32_t value)
	{
		___sortOrder_2 = value;
	}

	inline static int32_t get_offset_of_stimp_3() { return static_cast<int32_t>(offsetof(GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97, ___stimp_3)); }
	inline float get_stimp_3() const { return ___stimp_3; }
	inline float* get_address_of_stimp_3() { return &___stimp_3; }
	inline void set_stimp_3(float value)
	{
		___stimp_3 = value;
	}

	inline static int32_t get_offset_of_greens_4() { return static_cast<int32_t>(offsetof(GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97, ___greens_4)); }
	inline GreenU5BU5D_t15AF86FEF7A53720C1A0C6658F9BEF084F966BA1* get_greens_4() const { return ___greens_4; }
	inline GreenU5BU5D_t15AF86FEF7A53720C1A0C6658F9BEF084F966BA1** get_address_of_greens_4() { return &___greens_4; }
	inline void set_greens_4(GreenU5BU5D_t15AF86FEF7A53720C1A0C6658F9BEF084F966BA1* value)
	{
		___greens_4 = value;
		Il2CppCodeGenWriteBarrier((&___greens_4), value);
	}

	inline static int32_t get_offset_of_greenGroupType_5() { return static_cast<int32_t>(offsetof(GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97, ___greenGroupType_5)); }
	inline int32_t get_greenGroupType_5() const { return ___greenGroupType_5; }
	inline int32_t* get_address_of_greenGroupType_5() { return &___greenGroupType_5; }
	inline void set_greenGroupType_5(int32_t value)
	{
		___greenGroupType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GREENGROUP_TC6C5A79BEB814B131AA6A0A209437F2DEAA19E97_H
#ifndef RICHTEXTTAGATTRIBUTE_T381E96CA7820A787C5D88B6DA0181DFA85ADBA98_H
#define RICHTEXTTAGATTRIBUTE_T381E96CA7820A787C5D88B6DA0181DFA85ADBA98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.RichTextTagAttribute
struct  RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98 
{
public:
	// System.Int32 TMPro.RichTextTagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// System.Int32 TMPro.RichTextTagAttribute::valueHashCode
	int32_t ___valueHashCode_1;
	// TMPro.TagValueType TMPro.RichTextTagAttribute::valueType
	int32_t ___valueType_2;
	// System.Int32 TMPro.RichTextTagAttribute::valueStartIndex
	int32_t ___valueStartIndex_3;
	// System.Int32 TMPro.RichTextTagAttribute::valueLength
	int32_t ___valueLength_4;
	// TMPro.TagUnitType TMPro.RichTextTagAttribute::unitType
	int32_t ___unitType_5;

public:
	inline static int32_t get_offset_of_nameHashCode_0() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___nameHashCode_0)); }
	inline int32_t get_nameHashCode_0() const { return ___nameHashCode_0; }
	inline int32_t* get_address_of_nameHashCode_0() { return &___nameHashCode_0; }
	inline void set_nameHashCode_0(int32_t value)
	{
		___nameHashCode_0 = value;
	}

	inline static int32_t get_offset_of_valueHashCode_1() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___valueHashCode_1)); }
	inline int32_t get_valueHashCode_1() const { return ___valueHashCode_1; }
	inline int32_t* get_address_of_valueHashCode_1() { return &___valueHashCode_1; }
	inline void set_valueHashCode_1(int32_t value)
	{
		___valueHashCode_1 = value;
	}

	inline static int32_t get_offset_of_valueType_2() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___valueType_2)); }
	inline int32_t get_valueType_2() const { return ___valueType_2; }
	inline int32_t* get_address_of_valueType_2() { return &___valueType_2; }
	inline void set_valueType_2(int32_t value)
	{
		___valueType_2 = value;
	}

	inline static int32_t get_offset_of_valueStartIndex_3() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___valueStartIndex_3)); }
	inline int32_t get_valueStartIndex_3() const { return ___valueStartIndex_3; }
	inline int32_t* get_address_of_valueStartIndex_3() { return &___valueStartIndex_3; }
	inline void set_valueStartIndex_3(int32_t value)
	{
		___valueStartIndex_3 = value;
	}

	inline static int32_t get_offset_of_valueLength_4() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___valueLength_4)); }
	inline int32_t get_valueLength_4() const { return ___valueLength_4; }
	inline int32_t* get_address_of_valueLength_4() { return &___valueLength_4; }
	inline void set_valueLength_4(int32_t value)
	{
		___valueLength_4 = value;
	}

	inline static int32_t get_offset_of_unitType_5() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___unitType_5)); }
	inline int32_t get_unitType_5() const { return ___unitType_5; }
	inline int32_t* get_address_of_unitType_5() { return &___unitType_5; }
	inline void set_unitType_5(int32_t value)
	{
		___unitType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RICHTEXTTAGATTRIBUTE_T381E96CA7820A787C5D88B6DA0181DFA85ADBA98_H
#ifndef TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#define TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineExtents_19)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___lineExtents_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#define TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight>
struct  TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_ItemStack_0)); }
	inline FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#define TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_ItemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#define WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	// TMPro.TMP_FontStyleStack TMPro.WordWrapState::basicStyleStack
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___sizeStack_39;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___indentStack_40;
	// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight> TMPro.WordWrapState::fontWeightStack
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___styleStack_42;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___baselineStack_43;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___actionStack_44;
	// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___textInfo_27)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineInfo_28)); }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___vertexColor_29)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColor_30)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColor_31)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColor_32)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___basicStyleStack_33)); }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorStack_34)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColorStack_35)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColorStack_36)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColorStack_37)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorGradientStack_38)); }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___sizeStack_39)); }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___indentStack_40)); }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontWeightStack_41)); }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___styleStack_42)); }
	inline TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineStack_43)); }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___actionStack_44)); }
	inline TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___materialReferenceStack_45)); }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineJustificationStack_46)); }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterial_50)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___meshExtents_52)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___sizeStack_39;
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___indentStack_40;
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___styleStack_42;
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___baselineStack_43;
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___actionStack_44;
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___sizeStack_39;
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___indentStack_40;
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___styleStack_42;
	TMP_RichTextTagStack_1_tD5CFAF6390F82194115C110DC92A2CFB29529106  ___baselineStack_43;
	TMP_RichTextTagStack_1_tC6AC19F231736A53E4E73CA41E8BE25BCCE04293  ___actionStack_44;
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef APIELEVATIONDATACONTROLLER_T90F9B07D67904040CF36F73280C8FD926BBCE49C_H
#define APIELEVATIONDATACONTROLLER_T90F9B07D67904040CF36F73280C8FD926BBCE49C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIElevationDataController
struct  APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String APIElevationDataController::GetProjecctorDataURL
	String_t* ___GetProjecctorDataURL_5;
	// UnityEngine.Networking.UnityWebRequest APIElevationDataController::apiRequest
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___apiRequest_6;
	// System.Collections.IEnumerator APIElevationDataController::PingAPIForElevationDataCO
	RuntimeObject* ___PingAPIForElevationDataCO_7;
	// System.Boolean APIElevationDataController::checkForNewData
	bool ___checkForNewData_8;
	// ElevationTransferData APIElevationDataController::elevationTransferData
	ElevationTransferData_t8EC1998F633665D38764D8DFF602677469984883 * ___elevationTransferData_9;

public:
	inline static int32_t get_offset_of_GetProjecctorDataURL_5() { return static_cast<int32_t>(offsetof(APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C, ___GetProjecctorDataURL_5)); }
	inline String_t* get_GetProjecctorDataURL_5() const { return ___GetProjecctorDataURL_5; }
	inline String_t** get_address_of_GetProjecctorDataURL_5() { return &___GetProjecctorDataURL_5; }
	inline void set_GetProjecctorDataURL_5(String_t* value)
	{
		___GetProjecctorDataURL_5 = value;
		Il2CppCodeGenWriteBarrier((&___GetProjecctorDataURL_5), value);
	}

	inline static int32_t get_offset_of_apiRequest_6() { return static_cast<int32_t>(offsetof(APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C, ___apiRequest_6)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_apiRequest_6() const { return ___apiRequest_6; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_apiRequest_6() { return &___apiRequest_6; }
	inline void set_apiRequest_6(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___apiRequest_6 = value;
		Il2CppCodeGenWriteBarrier((&___apiRequest_6), value);
	}

	inline static int32_t get_offset_of_PingAPIForElevationDataCO_7() { return static_cast<int32_t>(offsetof(APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C, ___PingAPIForElevationDataCO_7)); }
	inline RuntimeObject* get_PingAPIForElevationDataCO_7() const { return ___PingAPIForElevationDataCO_7; }
	inline RuntimeObject** get_address_of_PingAPIForElevationDataCO_7() { return &___PingAPIForElevationDataCO_7; }
	inline void set_PingAPIForElevationDataCO_7(RuntimeObject* value)
	{
		___PingAPIForElevationDataCO_7 = value;
		Il2CppCodeGenWriteBarrier((&___PingAPIForElevationDataCO_7), value);
	}

	inline static int32_t get_offset_of_checkForNewData_8() { return static_cast<int32_t>(offsetof(APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C, ___checkForNewData_8)); }
	inline bool get_checkForNewData_8() const { return ___checkForNewData_8; }
	inline bool* get_address_of_checkForNewData_8() { return &___checkForNewData_8; }
	inline void set_checkForNewData_8(bool value)
	{
		___checkForNewData_8 = value;
	}

	inline static int32_t get_offset_of_elevationTransferData_9() { return static_cast<int32_t>(offsetof(APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C, ___elevationTransferData_9)); }
	inline ElevationTransferData_t8EC1998F633665D38764D8DFF602677469984883 * get_elevationTransferData_9() const { return ___elevationTransferData_9; }
	inline ElevationTransferData_t8EC1998F633665D38764D8DFF602677469984883 ** get_address_of_elevationTransferData_9() { return &___elevationTransferData_9; }
	inline void set_elevationTransferData_9(ElevationTransferData_t8EC1998F633665D38764D8DFF602677469984883 * value)
	{
		___elevationTransferData_9 = value;
		Il2CppCodeGenWriteBarrier((&___elevationTransferData_9), value);
	}
};

struct APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C_StaticFields
{
public:
	// System.Action APIElevationDataController::APIElevationDataLoaded
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___APIElevationDataLoaded_4;

public:
	inline static int32_t get_offset_of_APIElevationDataLoaded_4() { return static_cast<int32_t>(offsetof(APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C_StaticFields, ___APIElevationDataLoaded_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_APIElevationDataLoaded_4() const { return ___APIElevationDataLoaded_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_APIElevationDataLoaded_4() { return &___APIElevationDataLoaded_4; }
	inline void set_APIElevationDataLoaded_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___APIElevationDataLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___APIElevationDataLoaded_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIELEVATIONDATACONTROLLER_T90F9B07D67904040CF36F73280C8FD926BBCE49C_H
#ifndef APILOGINCONTROLLER_T9C537B953844B6722CF1B3F15F834C717904DC05_H
#define APILOGINCONTROLLER_T9C537B953844B6722CF1B3F15F834C717904DC05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APILoginController
struct  APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String APILoginController::PostLoginURL
	String_t* ___PostLoginURL_4;
	// UnityEngine.Networking.UnityWebRequest APILoginController::apiRequest
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___apiRequest_5;
	// System.String APILoginController::userNameForTesting
	String_t* ___userNameForTesting_6;
	// System.String APILoginController::passwordForTesting
	String_t* ___passwordForTesting_7;
	// System.Boolean APILoginController::testingLOGIN
	bool ___testingLOGIN_8;

public:
	inline static int32_t get_offset_of_PostLoginURL_4() { return static_cast<int32_t>(offsetof(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05, ___PostLoginURL_4)); }
	inline String_t* get_PostLoginURL_4() const { return ___PostLoginURL_4; }
	inline String_t** get_address_of_PostLoginURL_4() { return &___PostLoginURL_4; }
	inline void set_PostLoginURL_4(String_t* value)
	{
		___PostLoginURL_4 = value;
		Il2CppCodeGenWriteBarrier((&___PostLoginURL_4), value);
	}

	inline static int32_t get_offset_of_apiRequest_5() { return static_cast<int32_t>(offsetof(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05, ___apiRequest_5)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_apiRequest_5() const { return ___apiRequest_5; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_apiRequest_5() { return &___apiRequest_5; }
	inline void set_apiRequest_5(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___apiRequest_5 = value;
		Il2CppCodeGenWriteBarrier((&___apiRequest_5), value);
	}

	inline static int32_t get_offset_of_userNameForTesting_6() { return static_cast<int32_t>(offsetof(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05, ___userNameForTesting_6)); }
	inline String_t* get_userNameForTesting_6() const { return ___userNameForTesting_6; }
	inline String_t** get_address_of_userNameForTesting_6() { return &___userNameForTesting_6; }
	inline void set_userNameForTesting_6(String_t* value)
	{
		___userNameForTesting_6 = value;
		Il2CppCodeGenWriteBarrier((&___userNameForTesting_6), value);
	}

	inline static int32_t get_offset_of_passwordForTesting_7() { return static_cast<int32_t>(offsetof(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05, ___passwordForTesting_7)); }
	inline String_t* get_passwordForTesting_7() const { return ___passwordForTesting_7; }
	inline String_t** get_address_of_passwordForTesting_7() { return &___passwordForTesting_7; }
	inline void set_passwordForTesting_7(String_t* value)
	{
		___passwordForTesting_7 = value;
		Il2CppCodeGenWriteBarrier((&___passwordForTesting_7), value);
	}

	inline static int32_t get_offset_of_testingLOGIN_8() { return static_cast<int32_t>(offsetof(APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05, ___testingLOGIN_8)); }
	inline bool get_testingLOGIN_8() const { return ___testingLOGIN_8; }
	inline bool* get_address_of_testingLOGIN_8() { return &___testingLOGIN_8; }
	inline void set_testingLOGIN_8(bool value)
	{
		___testingLOGIN_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APILOGINCONTROLLER_T9C537B953844B6722CF1B3F15F834C717904DC05_H
#ifndef APIPROJECTORDATACONTROLLER_T992753079E529F3824A8D7D6E56F345B34DCDA98_H
#define APIPROJECTORDATACONTROLLER_T992753079E529F3824A8D7D6E56F345B34DCDA98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIProjectorDataController
struct  APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String APIProjectorDataController::GetProjecctorDataURL
	String_t* ___GetProjecctorDataURL_6;
	// UnityEngine.Networking.UnityWebRequest APIProjectorDataController::apiRequest
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___apiRequest_7;
	// Strackaline.ProjectorData APIProjectorDataController::projectorData
	ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 * ___projectorData_8;
	// ProjectorController APIProjectorDataController::projectorController
	ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 * ___projectorController_9;
	// CourseManager APIProjectorDataController::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_10;
	// System.Boolean APIProjectorDataController::checkForNewData
	bool ___checkForNewData_11;
	// UnityEngine.GameObject APIProjectorDataController::masterCanvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___masterCanvas_12;
	// System.Collections.IEnumerator APIProjectorDataController::PingAPIForNewPuttDataCO
	RuntimeObject* ___PingAPIForNewPuttDataCO_13;

public:
	inline static int32_t get_offset_of_GetProjecctorDataURL_6() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___GetProjecctorDataURL_6)); }
	inline String_t* get_GetProjecctorDataURL_6() const { return ___GetProjecctorDataURL_6; }
	inline String_t** get_address_of_GetProjecctorDataURL_6() { return &___GetProjecctorDataURL_6; }
	inline void set_GetProjecctorDataURL_6(String_t* value)
	{
		___GetProjecctorDataURL_6 = value;
		Il2CppCodeGenWriteBarrier((&___GetProjecctorDataURL_6), value);
	}

	inline static int32_t get_offset_of_apiRequest_7() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___apiRequest_7)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_apiRequest_7() const { return ___apiRequest_7; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_apiRequest_7() { return &___apiRequest_7; }
	inline void set_apiRequest_7(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___apiRequest_7 = value;
		Il2CppCodeGenWriteBarrier((&___apiRequest_7), value);
	}

	inline static int32_t get_offset_of_projectorData_8() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___projectorData_8)); }
	inline ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 * get_projectorData_8() const { return ___projectorData_8; }
	inline ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 ** get_address_of_projectorData_8() { return &___projectorData_8; }
	inline void set_projectorData_8(ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 * value)
	{
		___projectorData_8 = value;
		Il2CppCodeGenWriteBarrier((&___projectorData_8), value);
	}

	inline static int32_t get_offset_of_projectorController_9() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___projectorController_9)); }
	inline ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 * get_projectorController_9() const { return ___projectorController_9; }
	inline ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 ** get_address_of_projectorController_9() { return &___projectorController_9; }
	inline void set_projectorController_9(ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 * value)
	{
		___projectorController_9 = value;
		Il2CppCodeGenWriteBarrier((&___projectorController_9), value);
	}

	inline static int32_t get_offset_of_courseManager_10() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___courseManager_10)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_10() const { return ___courseManager_10; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_10() { return &___courseManager_10; }
	inline void set_courseManager_10(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_10 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_10), value);
	}

	inline static int32_t get_offset_of_checkForNewData_11() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___checkForNewData_11)); }
	inline bool get_checkForNewData_11() const { return ___checkForNewData_11; }
	inline bool* get_address_of_checkForNewData_11() { return &___checkForNewData_11; }
	inline void set_checkForNewData_11(bool value)
	{
		___checkForNewData_11 = value;
	}

	inline static int32_t get_offset_of_masterCanvas_12() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___masterCanvas_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_masterCanvas_12() const { return ___masterCanvas_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_masterCanvas_12() { return &___masterCanvas_12; }
	inline void set_masterCanvas_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___masterCanvas_12 = value;
		Il2CppCodeGenWriteBarrier((&___masterCanvas_12), value);
	}

	inline static int32_t get_offset_of_PingAPIForNewPuttDataCO_13() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98, ___PingAPIForNewPuttDataCO_13)); }
	inline RuntimeObject* get_PingAPIForNewPuttDataCO_13() const { return ___PingAPIForNewPuttDataCO_13; }
	inline RuntimeObject** get_address_of_PingAPIForNewPuttDataCO_13() { return &___PingAPIForNewPuttDataCO_13; }
	inline void set_PingAPIForNewPuttDataCO_13(RuntimeObject* value)
	{
		___PingAPIForNewPuttDataCO_13 = value;
		Il2CppCodeGenWriteBarrier((&___PingAPIForNewPuttDataCO_13), value);
	}
};

struct APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98_StaticFields
{
public:
	// System.Action APIProjectorDataController::ProjectoDataLoaded
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ProjectoDataLoaded_4;
	// System.Action APIProjectorDataController::ProjectoDataNeedsReload
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ProjectoDataNeedsReload_5;

public:
	inline static int32_t get_offset_of_ProjectoDataLoaded_4() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98_StaticFields, ___ProjectoDataLoaded_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ProjectoDataLoaded_4() const { return ___ProjectoDataLoaded_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ProjectoDataLoaded_4() { return &___ProjectoDataLoaded_4; }
	inline void set_ProjectoDataLoaded_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ProjectoDataLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___ProjectoDataLoaded_4), value);
	}

	inline static int32_t get_offset_of_ProjectoDataNeedsReload_5() { return static_cast<int32_t>(offsetof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98_StaticFields, ___ProjectoDataNeedsReload_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ProjectoDataNeedsReload_5() const { return ___ProjectoDataNeedsReload_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ProjectoDataNeedsReload_5() { return &___ProjectoDataNeedsReload_5; }
	inline void set_ProjectoDataNeedsReload_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ProjectoDataNeedsReload_5 = value;
		Il2CppCodeGenWriteBarrier((&___ProjectoDataNeedsReload_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIPROJECTORDATACONTROLLER_T992753079E529F3824A8D7D6E56F345B34DCDA98_H
#ifndef APIPUTTCONTROLLER_TF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392_H
#define APIPUTTCONTROLLER_TF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIPuttController
struct  APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String APIPuttController::apiBaseURL
	String_t* ___apiBaseURL_4;
	// System.String APIPuttController::permanentToken
	String_t* ___permanentToken_5;
	// UnityEngine.GameObject APIPuttController::preloader
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___preloader_6;
	// UnityEngine.Networking.UnityWebRequest APIPuttController::apiRequest
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___apiRequest_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> APIPuttController::postHeaderDict
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___postHeaderDict_8;
	// CourseManager APIPuttController::courseManager
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * ___courseManager_9;
	// UnityEngine.GameObject APIPuttController::puttInteractivePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttInteractivePrefab_10;
	// UnityEngine.GameObject APIPuttController::puttContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttContainer_11;

public:
	inline static int32_t get_offset_of_apiBaseURL_4() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___apiBaseURL_4)); }
	inline String_t* get_apiBaseURL_4() const { return ___apiBaseURL_4; }
	inline String_t** get_address_of_apiBaseURL_4() { return &___apiBaseURL_4; }
	inline void set_apiBaseURL_4(String_t* value)
	{
		___apiBaseURL_4 = value;
		Il2CppCodeGenWriteBarrier((&___apiBaseURL_4), value);
	}

	inline static int32_t get_offset_of_permanentToken_5() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___permanentToken_5)); }
	inline String_t* get_permanentToken_5() const { return ___permanentToken_5; }
	inline String_t** get_address_of_permanentToken_5() { return &___permanentToken_5; }
	inline void set_permanentToken_5(String_t* value)
	{
		___permanentToken_5 = value;
		Il2CppCodeGenWriteBarrier((&___permanentToken_5), value);
	}

	inline static int32_t get_offset_of_preloader_6() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___preloader_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_preloader_6() const { return ___preloader_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_preloader_6() { return &___preloader_6; }
	inline void set_preloader_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___preloader_6 = value;
		Il2CppCodeGenWriteBarrier((&___preloader_6), value);
	}

	inline static int32_t get_offset_of_apiRequest_7() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___apiRequest_7)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_apiRequest_7() const { return ___apiRequest_7; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_apiRequest_7() { return &___apiRequest_7; }
	inline void set_apiRequest_7(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___apiRequest_7 = value;
		Il2CppCodeGenWriteBarrier((&___apiRequest_7), value);
	}

	inline static int32_t get_offset_of_postHeaderDict_8() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___postHeaderDict_8)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_postHeaderDict_8() const { return ___postHeaderDict_8; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_postHeaderDict_8() { return &___postHeaderDict_8; }
	inline void set_postHeaderDict_8(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___postHeaderDict_8 = value;
		Il2CppCodeGenWriteBarrier((&___postHeaderDict_8), value);
	}

	inline static int32_t get_offset_of_courseManager_9() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___courseManager_9)); }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * get_courseManager_9() const { return ___courseManager_9; }
	inline CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 ** get_address_of_courseManager_9() { return &___courseManager_9; }
	inline void set_courseManager_9(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8 * value)
	{
		___courseManager_9 = value;
		Il2CppCodeGenWriteBarrier((&___courseManager_9), value);
	}

	inline static int32_t get_offset_of_puttInteractivePrefab_10() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___puttInteractivePrefab_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttInteractivePrefab_10() const { return ___puttInteractivePrefab_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttInteractivePrefab_10() { return &___puttInteractivePrefab_10; }
	inline void set_puttInteractivePrefab_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttInteractivePrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___puttInteractivePrefab_10), value);
	}

	inline static int32_t get_offset_of_puttContainer_11() { return static_cast<int32_t>(offsetof(APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392, ___puttContainer_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttContainer_11() const { return ___puttContainer_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttContainer_11() { return &___puttContainer_11; }
	inline void set_puttContainer_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttContainer_11 = value;
		Il2CppCodeGenWriteBarrier((&___puttContainer_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIPUTTCONTROLLER_TF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392_H
#ifndef ALERTPOPUPCONTROLLER_TF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_H
#define ALERTPOPUPCONTROLLER_TF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlertPopupController
struct  AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String AlertPopupController::popupName
	String_t* ___popupName_5;
	// Doozy.Engine.UI.UIPopup AlertPopupController::alertPopup
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * ___alertPopup_6;

public:
	inline static int32_t get_offset_of_popupName_5() { return static_cast<int32_t>(offsetof(AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC, ___popupName_5)); }
	inline String_t* get_popupName_5() const { return ___popupName_5; }
	inline String_t** get_address_of_popupName_5() { return &___popupName_5; }
	inline void set_popupName_5(String_t* value)
	{
		___popupName_5 = value;
		Il2CppCodeGenWriteBarrier((&___popupName_5), value);
	}

	inline static int32_t get_offset_of_alertPopup_6() { return static_cast<int32_t>(offsetof(AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC, ___alertPopup_6)); }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * get_alertPopup_6() const { return ___alertPopup_6; }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 ** get_address_of_alertPopup_6() { return &___alertPopup_6; }
	inline void set_alertPopup_6(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * value)
	{
		___alertPopup_6 = value;
		Il2CppCodeGenWriteBarrier((&___alertPopup_6), value);
	}
};

struct AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_StaticFields
{
public:
	// AlertPopupController AlertPopupController::inst
	AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC * ___inst_4;

public:
	inline static int32_t get_offset_of_inst_4() { return static_cast<int32_t>(offsetof(AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_StaticFields, ___inst_4)); }
	inline AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC * get_inst_4() const { return ___inst_4; }
	inline AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC ** get_address_of_inst_4() { return &___inst_4; }
	inline void set_inst_4(AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC * value)
	{
		___inst_4 = value;
		Il2CppCodeGenWriteBarrier((&___inst_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTPOPUPCONTROLLER_TF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_H
#ifndef APPLETVREMOTECONTROLLER_T8891BD4501BE3E080090472A39EBD50DCA5BB54E_H
#define APPLETVREMOTECONTROLLER_T8891BD4501BE3E080090472A39EBD50DCA5BB54E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppleTVRemoteController
struct  AppleTVRemoteController_t8891BD4501BE3E080090472A39EBD50DCA5BB54E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 AppleTVRemoteController::playerId
	int32_t ___playerId_4;
	// Rewired.Player AppleTVRemoteController::player
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___player_5;
	// System.Boolean AppleTVRemoteController::selected
	bool ___selected_6;
	// System.Boolean AppleTVRemoteController::showDashboard
	bool ___showDashboard_7;

public:
	inline static int32_t get_offset_of_playerId_4() { return static_cast<int32_t>(offsetof(AppleTVRemoteController_t8891BD4501BE3E080090472A39EBD50DCA5BB54E, ___playerId_4)); }
	inline int32_t get_playerId_4() const { return ___playerId_4; }
	inline int32_t* get_address_of_playerId_4() { return &___playerId_4; }
	inline void set_playerId_4(int32_t value)
	{
		___playerId_4 = value;
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(AppleTVRemoteController_t8891BD4501BE3E080090472A39EBD50DCA5BB54E, ___player_5)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_player_5() const { return ___player_5; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}

	inline static int32_t get_offset_of_selected_6() { return static_cast<int32_t>(offsetof(AppleTVRemoteController_t8891BD4501BE3E080090472A39EBD50DCA5BB54E, ___selected_6)); }
	inline bool get_selected_6() const { return ___selected_6; }
	inline bool* get_address_of_selected_6() { return &___selected_6; }
	inline void set_selected_6(bool value)
	{
		___selected_6 = value;
	}

	inline static int32_t get_offset_of_showDashboard_7() { return static_cast<int32_t>(offsetof(AppleTVRemoteController_t8891BD4501BE3E080090472A39EBD50DCA5BB54E, ___showDashboard_7)); }
	inline bool get_showDashboard_7() const { return ___showDashboard_7; }
	inline bool* get_address_of_showDashboard_7() { return &___showDashboard_7; }
	inline void set_showDashboard_7(bool value)
	{
		___showDashboard_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLETVREMOTECONTROLLER_T8891BD4501BE3E080090472A39EBD50DCA5BB54E_H
#ifndef ARROWMANAGER_TBA7E066B687896A443C971DD1DA156F7ED4FB983_H
#define ARROWMANAGER_TBA7E066B687896A443C971DD1DA156F7ED4FB983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArrowManager
struct  ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject ArrowManager::arrowPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___arrowPrefab_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ArrowManager::arrowList
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___arrowList_6;
	// UnityEngine.Color ArrowManager::arrowColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___arrowColor_7;
	// UnityEngine.Color ArrowManager::arrowPrefColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___arrowPrefColor_8;
	// UnityEngine.GameObject ArrowManager::arrowContGO
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___arrowContGO_9;
	// System.Boolean ArrowManager::isArrowAnimating
	bool ___isArrowAnimating_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ArrowManager::arrowLevel1List
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___arrowLevel1List_11;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.GameObject>>> ArrowManager::arrowLevelsList
	List_1_tDE730FA6B64BDCCA2AA49AE7E1CCF8EA93EA09CF * ___arrowLevelsList_12;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.GameObject>> ArrowManager::arrowsAllLevelsList
	List_1_t6A863775481DD4E1BDD024A35AC5E83118209168 * ___arrowsAllLevelsList_13;
	// UnityEngine.GameObject ArrowManager::arrowCont
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___arrowCont_14;
	// System.Single ArrowManager::maxArrowSpeed
	float ___maxArrowSpeed_15;
	// System.Collections.Generic.List`1<Strackaline.Arrow> ArrowManager::arrowCollectedList
	List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 * ___arrowCollectedList_16;

public:
	inline static int32_t get_offset_of_arrowPrefab_5() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowPrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_arrowPrefab_5() const { return ___arrowPrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_arrowPrefab_5() { return &___arrowPrefab_5; }
	inline void set_arrowPrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___arrowPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___arrowPrefab_5), value);
	}

	inline static int32_t get_offset_of_arrowList_6() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowList_6)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_arrowList_6() const { return ___arrowList_6; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_arrowList_6() { return &___arrowList_6; }
	inline void set_arrowList_6(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___arrowList_6 = value;
		Il2CppCodeGenWriteBarrier((&___arrowList_6), value);
	}

	inline static int32_t get_offset_of_arrowColor_7() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_arrowColor_7() const { return ___arrowColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_arrowColor_7() { return &___arrowColor_7; }
	inline void set_arrowColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___arrowColor_7 = value;
	}

	inline static int32_t get_offset_of_arrowPrefColor_8() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowPrefColor_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_arrowPrefColor_8() const { return ___arrowPrefColor_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_arrowPrefColor_8() { return &___arrowPrefColor_8; }
	inline void set_arrowPrefColor_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___arrowPrefColor_8 = value;
	}

	inline static int32_t get_offset_of_arrowContGO_9() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowContGO_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_arrowContGO_9() const { return ___arrowContGO_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_arrowContGO_9() { return &___arrowContGO_9; }
	inline void set_arrowContGO_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___arrowContGO_9 = value;
		Il2CppCodeGenWriteBarrier((&___arrowContGO_9), value);
	}

	inline static int32_t get_offset_of_isArrowAnimating_10() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___isArrowAnimating_10)); }
	inline bool get_isArrowAnimating_10() const { return ___isArrowAnimating_10; }
	inline bool* get_address_of_isArrowAnimating_10() { return &___isArrowAnimating_10; }
	inline void set_isArrowAnimating_10(bool value)
	{
		___isArrowAnimating_10 = value;
	}

	inline static int32_t get_offset_of_arrowLevel1List_11() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowLevel1List_11)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_arrowLevel1List_11() const { return ___arrowLevel1List_11; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_arrowLevel1List_11() { return &___arrowLevel1List_11; }
	inline void set_arrowLevel1List_11(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___arrowLevel1List_11 = value;
		Il2CppCodeGenWriteBarrier((&___arrowLevel1List_11), value);
	}

	inline static int32_t get_offset_of_arrowLevelsList_12() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowLevelsList_12)); }
	inline List_1_tDE730FA6B64BDCCA2AA49AE7E1CCF8EA93EA09CF * get_arrowLevelsList_12() const { return ___arrowLevelsList_12; }
	inline List_1_tDE730FA6B64BDCCA2AA49AE7E1CCF8EA93EA09CF ** get_address_of_arrowLevelsList_12() { return &___arrowLevelsList_12; }
	inline void set_arrowLevelsList_12(List_1_tDE730FA6B64BDCCA2AA49AE7E1CCF8EA93EA09CF * value)
	{
		___arrowLevelsList_12 = value;
		Il2CppCodeGenWriteBarrier((&___arrowLevelsList_12), value);
	}

	inline static int32_t get_offset_of_arrowsAllLevelsList_13() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowsAllLevelsList_13)); }
	inline List_1_t6A863775481DD4E1BDD024A35AC5E83118209168 * get_arrowsAllLevelsList_13() const { return ___arrowsAllLevelsList_13; }
	inline List_1_t6A863775481DD4E1BDD024A35AC5E83118209168 ** get_address_of_arrowsAllLevelsList_13() { return &___arrowsAllLevelsList_13; }
	inline void set_arrowsAllLevelsList_13(List_1_t6A863775481DD4E1BDD024A35AC5E83118209168 * value)
	{
		___arrowsAllLevelsList_13 = value;
		Il2CppCodeGenWriteBarrier((&___arrowsAllLevelsList_13), value);
	}

	inline static int32_t get_offset_of_arrowCont_14() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowCont_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_arrowCont_14() const { return ___arrowCont_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_arrowCont_14() { return &___arrowCont_14; }
	inline void set_arrowCont_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___arrowCont_14 = value;
		Il2CppCodeGenWriteBarrier((&___arrowCont_14), value);
	}

	inline static int32_t get_offset_of_maxArrowSpeed_15() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___maxArrowSpeed_15)); }
	inline float get_maxArrowSpeed_15() const { return ___maxArrowSpeed_15; }
	inline float* get_address_of_maxArrowSpeed_15() { return &___maxArrowSpeed_15; }
	inline void set_maxArrowSpeed_15(float value)
	{
		___maxArrowSpeed_15 = value;
	}

	inline static int32_t get_offset_of_arrowCollectedList_16() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983, ___arrowCollectedList_16)); }
	inline List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 * get_arrowCollectedList_16() const { return ___arrowCollectedList_16; }
	inline List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 ** get_address_of_arrowCollectedList_16() { return &___arrowCollectedList_16; }
	inline void set_arrowCollectedList_16(List_1_tD1E96F3FB3EFB1CA790139CE7ECC9812A123AA34 * value)
	{
		___arrowCollectedList_16 = value;
		Il2CppCodeGenWriteBarrier((&___arrowCollectedList_16), value);
	}
};

struct ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983_StaticFields
{
public:
	// System.Action ArrowManager::ArrowObjectsCreated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ArrowObjectsCreated_4;

public:
	inline static int32_t get_offset_of_ArrowObjectsCreated_4() { return static_cast<int32_t>(offsetof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983_StaticFields, ___ArrowObjectsCreated_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ArrowObjectsCreated_4() const { return ___ArrowObjectsCreated_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ArrowObjectsCreated_4() { return &___ArrowObjectsCreated_4; }
	inline void set_ArrowObjectsCreated_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ArrowObjectsCreated_4 = value;
		Il2CppCodeGenWriteBarrier((&___ArrowObjectsCreated_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWMANAGER_TBA7E066B687896A443C971DD1DA156F7ED4FB983_H
#ifndef ARROWPREFABCONTROLLER_T71199822C519DC78B745CA1375ECBA96C329C5C5_H
#define ARROWPREFABCONTROLLER_T71199822C519DC78B745CA1375ECBA96C329C5C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArrowPrefabController
struct  ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single ArrowPrefabController::speed
	float ___speed_4;
	// System.Boolean ArrowPrefabController::isDoneFading
	bool ___isDoneFading_5;
	// System.Boolean ArrowPrefabController::isDoneAnimating
	bool ___isDoneAnimating_6;
	// System.Single ArrowPrefabController::slope
	float ___slope_7;
	// UnityEngine.Color ArrowPrefabController::slopeColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___slopeColor_8;
	// System.String ArrowPrefabController::hex
	String_t* ___hex_9;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_isDoneFading_5() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___isDoneFading_5)); }
	inline bool get_isDoneFading_5() const { return ___isDoneFading_5; }
	inline bool* get_address_of_isDoneFading_5() { return &___isDoneFading_5; }
	inline void set_isDoneFading_5(bool value)
	{
		___isDoneFading_5 = value;
	}

	inline static int32_t get_offset_of_isDoneAnimating_6() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___isDoneAnimating_6)); }
	inline bool get_isDoneAnimating_6() const { return ___isDoneAnimating_6; }
	inline bool* get_address_of_isDoneAnimating_6() { return &___isDoneAnimating_6; }
	inline void set_isDoneAnimating_6(bool value)
	{
		___isDoneAnimating_6 = value;
	}

	inline static int32_t get_offset_of_slope_7() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___slope_7)); }
	inline float get_slope_7() const { return ___slope_7; }
	inline float* get_address_of_slope_7() { return &___slope_7; }
	inline void set_slope_7(float value)
	{
		___slope_7 = value;
	}

	inline static int32_t get_offset_of_slopeColor_8() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___slopeColor_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_slopeColor_8() const { return ___slopeColor_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_slopeColor_8() { return &___slopeColor_8; }
	inline void set_slopeColor_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___slopeColor_8 = value;
	}

	inline static int32_t get_offset_of_hex_9() { return static_cast<int32_t>(offsetof(ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5, ___hex_9)); }
	inline String_t* get_hex_9() const { return ___hex_9; }
	inline String_t** get_address_of_hex_9() { return &___hex_9; }
	inline void set_hex_9(String_t* value)
	{
		___hex_9 = value;
		Il2CppCodeGenWriteBarrier((&___hex_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWPREFABCONTROLLER_T71199822C519DC78B745CA1375ECBA96C329C5C5_H
#ifndef CHATCONTROLLER_T49D7A1D868EED265CD37C4469FAFCF44235384FB_H
#define CHATCONTROLLER_T49D7A1D868EED265CD37C4469FAFCF44235384FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatController
struct  ChatController_t49D7A1D868EED265CD37C4469FAFCF44235384FB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TMP_InputField ChatController::TMP_ChatInput
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ___TMP_ChatInput_4;
	// TMPro.TMP_Text ChatController::TMP_ChatOutput
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___TMP_ChatOutput_5;
	// UnityEngine.UI.Scrollbar ChatController::ChatScrollbar
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * ___ChatScrollbar_6;

public:
	inline static int32_t get_offset_of_TMP_ChatInput_4() { return static_cast<int32_t>(offsetof(ChatController_t49D7A1D868EED265CD37C4469FAFCF44235384FB, ___TMP_ChatInput_4)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get_TMP_ChatInput_4() const { return ___TMP_ChatInput_4; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of_TMP_ChatInput_4() { return &___TMP_ChatInput_4; }
	inline void set_TMP_ChatInput_4(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		___TMP_ChatInput_4 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatInput_4), value);
	}

	inline static int32_t get_offset_of_TMP_ChatOutput_5() { return static_cast<int32_t>(offsetof(ChatController_t49D7A1D868EED265CD37C4469FAFCF44235384FB, ___TMP_ChatOutput_5)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_TMP_ChatOutput_5() const { return ___TMP_ChatOutput_5; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_TMP_ChatOutput_5() { return &___TMP_ChatOutput_5; }
	inline void set_TMP_ChatOutput_5(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___TMP_ChatOutput_5 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatOutput_5), value);
	}

	inline static int32_t get_offset_of_ChatScrollbar_6() { return static_cast<int32_t>(offsetof(ChatController_t49D7A1D868EED265CD37C4469FAFCF44235384FB, ___ChatScrollbar_6)); }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * get_ChatScrollbar_6() const { return ___ChatScrollbar_6; }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 ** get_address_of_ChatScrollbar_6() { return &___ChatScrollbar_6; }
	inline void set_ChatScrollbar_6(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * value)
	{
		___ChatScrollbar_6 = value;
		Il2CppCodeGenWriteBarrier((&___ChatScrollbar_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATCONTROLLER_T49D7A1D868EED265CD37C4469FAFCF44235384FB_H
#ifndef CONTOURMANAGER_TD5BAD0C4F239F537CB6335A335723DA5D397D278_H
#define CONTOURMANAGER_TD5BAD0C4F239F537CB6335A335723DA5D397D278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContourManager
struct  ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ContourManager::contourLineList
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___contourLineList_4;
	// UnityEngine.GameObject ContourManager::contourCont
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___contourCont_5;
	// UnityEngine.Material ContourManager::lineMat
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lineMat_6;
	// System.Collections.Generic.List`1<Strackaline.ContourPath> ContourManager::contourPaths
	List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D * ___contourPaths_7;

public:
	inline static int32_t get_offset_of_contourLineList_4() { return static_cast<int32_t>(offsetof(ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278, ___contourLineList_4)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_contourLineList_4() const { return ___contourLineList_4; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_contourLineList_4() { return &___contourLineList_4; }
	inline void set_contourLineList_4(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___contourLineList_4 = value;
		Il2CppCodeGenWriteBarrier((&___contourLineList_4), value);
	}

	inline static int32_t get_offset_of_contourCont_5() { return static_cast<int32_t>(offsetof(ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278, ___contourCont_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_contourCont_5() const { return ___contourCont_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_contourCont_5() { return &___contourCont_5; }
	inline void set_contourCont_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___contourCont_5 = value;
		Il2CppCodeGenWriteBarrier((&___contourCont_5), value);
	}

	inline static int32_t get_offset_of_lineMat_6() { return static_cast<int32_t>(offsetof(ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278, ___lineMat_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lineMat_6() const { return ___lineMat_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lineMat_6() { return &___lineMat_6; }
	inline void set_lineMat_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lineMat_6 = value;
		Il2CppCodeGenWriteBarrier((&___lineMat_6), value);
	}

	inline static int32_t get_offset_of_contourPaths_7() { return static_cast<int32_t>(offsetof(ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278, ___contourPaths_7)); }
	inline List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D * get_contourPaths_7() const { return ___contourPaths_7; }
	inline List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D ** get_address_of_contourPaths_7() { return &___contourPaths_7; }
	inline void set_contourPaths_7(List_1_t8DCC5FC82A746FA0248FC1E25C925FA5E040B28D * value)
	{
		___contourPaths_7 = value;
		Il2CppCodeGenWriteBarrier((&___contourPaths_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTOURMANAGER_TD5BAD0C4F239F537CB6335A335723DA5D397D278_H
#ifndef COURSEMANAGER_TB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8_H
#define COURSEMANAGER_TB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CourseManager
struct  CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Strackaline.Region CourseManager::region
	Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E * ___region_4;
	// Strackaline.Course CourseManager::course
	Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A * ___course_5;
	// Strackaline.CourseGreens CourseManager::courseGreens
	CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D * ___courseGreens_6;
	// Strackaline.Green CourseManager::green
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * ___green_7;
	// Strackaline.Region CourseManager::currRegion
	Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E * ___currRegion_8;
	// Strackaline.Course CourseManager::currCourse
	Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A * ___currCourse_9;
	// Strackaline.Green CourseManager::currGreen
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * ___currGreen_10;
	// UnityEngine.GameObject CourseManager::greenModelLoaded
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___greenModelLoaded_11;
	// Strackaline.GreenArrow CourseManager::arrowsData
	GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A * ___arrowsData_12;
	// Strackaline.Contour CourseManager::contourData
	Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42 * ___contourData_13;
	// System.String CourseManager::selectedRegionCode
	String_t* ___selectedRegionCode_14;
	// System.String CourseManager::selectedCourseID
	String_t* ___selectedCourseID_15;
	// System.String CourseManager::selectedGroupID
	String_t* ___selectedGroupID_16;
	// System.String CourseManager::selectedGreenID
	String_t* ___selectedGreenID_17;

public:
	inline static int32_t get_offset_of_region_4() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___region_4)); }
	inline Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E * get_region_4() const { return ___region_4; }
	inline Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E ** get_address_of_region_4() { return &___region_4; }
	inline void set_region_4(Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E * value)
	{
		___region_4 = value;
		Il2CppCodeGenWriteBarrier((&___region_4), value);
	}

	inline static int32_t get_offset_of_course_5() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___course_5)); }
	inline Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A * get_course_5() const { return ___course_5; }
	inline Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A ** get_address_of_course_5() { return &___course_5; }
	inline void set_course_5(Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A * value)
	{
		___course_5 = value;
		Il2CppCodeGenWriteBarrier((&___course_5), value);
	}

	inline static int32_t get_offset_of_courseGreens_6() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___courseGreens_6)); }
	inline CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D * get_courseGreens_6() const { return ___courseGreens_6; }
	inline CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D ** get_address_of_courseGreens_6() { return &___courseGreens_6; }
	inline void set_courseGreens_6(CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D * value)
	{
		___courseGreens_6 = value;
		Il2CppCodeGenWriteBarrier((&___courseGreens_6), value);
	}

	inline static int32_t get_offset_of_green_7() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___green_7)); }
	inline Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * get_green_7() const { return ___green_7; }
	inline Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 ** get_address_of_green_7() { return &___green_7; }
	inline void set_green_7(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * value)
	{
		___green_7 = value;
		Il2CppCodeGenWriteBarrier((&___green_7), value);
	}

	inline static int32_t get_offset_of_currRegion_8() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___currRegion_8)); }
	inline Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E * get_currRegion_8() const { return ___currRegion_8; }
	inline Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E ** get_address_of_currRegion_8() { return &___currRegion_8; }
	inline void set_currRegion_8(Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E * value)
	{
		___currRegion_8 = value;
		Il2CppCodeGenWriteBarrier((&___currRegion_8), value);
	}

	inline static int32_t get_offset_of_currCourse_9() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___currCourse_9)); }
	inline Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A * get_currCourse_9() const { return ___currCourse_9; }
	inline Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A ** get_address_of_currCourse_9() { return &___currCourse_9; }
	inline void set_currCourse_9(Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A * value)
	{
		___currCourse_9 = value;
		Il2CppCodeGenWriteBarrier((&___currCourse_9), value);
	}

	inline static int32_t get_offset_of_currGreen_10() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___currGreen_10)); }
	inline Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * get_currGreen_10() const { return ___currGreen_10; }
	inline Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 ** get_address_of_currGreen_10() { return &___currGreen_10; }
	inline void set_currGreen_10(Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6 * value)
	{
		___currGreen_10 = value;
		Il2CppCodeGenWriteBarrier((&___currGreen_10), value);
	}

	inline static int32_t get_offset_of_greenModelLoaded_11() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___greenModelLoaded_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_greenModelLoaded_11() const { return ___greenModelLoaded_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_greenModelLoaded_11() { return &___greenModelLoaded_11; }
	inline void set_greenModelLoaded_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___greenModelLoaded_11 = value;
		Il2CppCodeGenWriteBarrier((&___greenModelLoaded_11), value);
	}

	inline static int32_t get_offset_of_arrowsData_12() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___arrowsData_12)); }
	inline GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A * get_arrowsData_12() const { return ___arrowsData_12; }
	inline GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A ** get_address_of_arrowsData_12() { return &___arrowsData_12; }
	inline void set_arrowsData_12(GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A * value)
	{
		___arrowsData_12 = value;
		Il2CppCodeGenWriteBarrier((&___arrowsData_12), value);
	}

	inline static int32_t get_offset_of_contourData_13() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___contourData_13)); }
	inline Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42 * get_contourData_13() const { return ___contourData_13; }
	inline Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42 ** get_address_of_contourData_13() { return &___contourData_13; }
	inline void set_contourData_13(Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42 * value)
	{
		___contourData_13 = value;
		Il2CppCodeGenWriteBarrier((&___contourData_13), value);
	}

	inline static int32_t get_offset_of_selectedRegionCode_14() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___selectedRegionCode_14)); }
	inline String_t* get_selectedRegionCode_14() const { return ___selectedRegionCode_14; }
	inline String_t** get_address_of_selectedRegionCode_14() { return &___selectedRegionCode_14; }
	inline void set_selectedRegionCode_14(String_t* value)
	{
		___selectedRegionCode_14 = value;
		Il2CppCodeGenWriteBarrier((&___selectedRegionCode_14), value);
	}

	inline static int32_t get_offset_of_selectedCourseID_15() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___selectedCourseID_15)); }
	inline String_t* get_selectedCourseID_15() const { return ___selectedCourseID_15; }
	inline String_t** get_address_of_selectedCourseID_15() { return &___selectedCourseID_15; }
	inline void set_selectedCourseID_15(String_t* value)
	{
		___selectedCourseID_15 = value;
		Il2CppCodeGenWriteBarrier((&___selectedCourseID_15), value);
	}

	inline static int32_t get_offset_of_selectedGroupID_16() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___selectedGroupID_16)); }
	inline String_t* get_selectedGroupID_16() const { return ___selectedGroupID_16; }
	inline String_t** get_address_of_selectedGroupID_16() { return &___selectedGroupID_16; }
	inline void set_selectedGroupID_16(String_t* value)
	{
		___selectedGroupID_16 = value;
		Il2CppCodeGenWriteBarrier((&___selectedGroupID_16), value);
	}

	inline static int32_t get_offset_of_selectedGreenID_17() { return static_cast<int32_t>(offsetof(CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8, ___selectedGreenID_17)); }
	inline String_t* get_selectedGreenID_17() const { return ___selectedGreenID_17; }
	inline String_t** get_address_of_selectedGreenID_17() { return &___selectedGreenID_17; }
	inline void set_selectedGreenID_17(String_t* value)
	{
		___selectedGreenID_17 = value;
		Il2CppCodeGenWriteBarrier((&___selectedGreenID_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COURSEMANAGER_TB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8_H
#ifndef DASHBOARDVIEWCONTROLLER_TC062AACB63F690BF7B0AD65AA38609BCBA698670_H
#define DASHBOARDVIEWCONTROLLER_TC062AACB63F690BF7B0AD65AA38609BCBA698670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DashboardViewController
struct  DashboardViewController_tC062AACB63F690BF7B0AD65AA38609BCBA698670  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean DashboardViewController::btnSelected
	bool ___btnSelected_4;
	// TMPro.TextMeshProUGUI DashboardViewController::userNameTxt
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___userNameTxt_5;
	// TMPro.TextMeshProUGUI DashboardViewController::locationTxt
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___locationTxt_6;
	// System.Boolean DashboardViewController::showDashboard
	bool ___showDashboard_7;

public:
	inline static int32_t get_offset_of_btnSelected_4() { return static_cast<int32_t>(offsetof(DashboardViewController_tC062AACB63F690BF7B0AD65AA38609BCBA698670, ___btnSelected_4)); }
	inline bool get_btnSelected_4() const { return ___btnSelected_4; }
	inline bool* get_address_of_btnSelected_4() { return &___btnSelected_4; }
	inline void set_btnSelected_4(bool value)
	{
		___btnSelected_4 = value;
	}

	inline static int32_t get_offset_of_userNameTxt_5() { return static_cast<int32_t>(offsetof(DashboardViewController_tC062AACB63F690BF7B0AD65AA38609BCBA698670, ___userNameTxt_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_userNameTxt_5() const { return ___userNameTxt_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_userNameTxt_5() { return &___userNameTxt_5; }
	inline void set_userNameTxt_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___userNameTxt_5 = value;
		Il2CppCodeGenWriteBarrier((&___userNameTxt_5), value);
	}

	inline static int32_t get_offset_of_locationTxt_6() { return static_cast<int32_t>(offsetof(DashboardViewController_tC062AACB63F690BF7B0AD65AA38609BCBA698670, ___locationTxt_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_locationTxt_6() const { return ___locationTxt_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_locationTxt_6() { return &___locationTxt_6; }
	inline void set_locationTxt_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___locationTxt_6 = value;
		Il2CppCodeGenWriteBarrier((&___locationTxt_6), value);
	}

	inline static int32_t get_offset_of_showDashboard_7() { return static_cast<int32_t>(offsetof(DashboardViewController_tC062AACB63F690BF7B0AD65AA38609BCBA698670, ___showDashboard_7)); }
	inline bool get_showDashboard_7() const { return ___showDashboard_7; }
	inline bool* get_address_of_showDashboard_7() { return &___showDashboard_7; }
	inline void set_showDashboard_7(bool value)
	{
		___showDashboard_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DASHBOARDVIEWCONTROLLER_TC062AACB63F690BF7B0AD65AA38609BCBA698670_H
#ifndef ELEVATIONDATACONTROLLER_T83856E53D5F85C91D3B681D07D8BAB6433A0FB8A_H
#define ELEVATIONDATACONTROLLER_T83856E53D5F85C91D3B681D07D8BAB6433A0FB8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ElevationDataController
struct  ElevationDataController_t83856E53D5F85C91D3B681D07D8BAB6433A0FB8A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Strackaline.ProjectorData ElevationDataController::projectorData
	ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 * ___projectorData_4;
	// System.Int32 ElevationDataController::projectorElevationDataConfigId
	int32_t ___projectorElevationDataConfigId_5;

public:
	inline static int32_t get_offset_of_projectorData_4() { return static_cast<int32_t>(offsetof(ElevationDataController_t83856E53D5F85C91D3B681D07D8BAB6433A0FB8A, ___projectorData_4)); }
	inline ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 * get_projectorData_4() const { return ___projectorData_4; }
	inline ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 ** get_address_of_projectorData_4() { return &___projectorData_4; }
	inline void set_projectorData_4(ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88 * value)
	{
		___projectorData_4 = value;
		Il2CppCodeGenWriteBarrier((&___projectorData_4), value);
	}

	inline static int32_t get_offset_of_projectorElevationDataConfigId_5() { return static_cast<int32_t>(offsetof(ElevationDataController_t83856E53D5F85C91D3B681D07D8BAB6433A0FB8A, ___projectorElevationDataConfigId_5)); }
	inline int32_t get_projectorElevationDataConfigId_5() const { return ___projectorElevationDataConfigId_5; }
	inline int32_t* get_address_of_projectorElevationDataConfigId_5() { return &___projectorElevationDataConfigId_5; }
	inline void set_projectorElevationDataConfigId_5(int32_t value)
	{
		___projectorElevationDataConfigId_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEVATIONDATACONTROLLER_T83856E53D5F85C91D3B681D07D8BAB6433A0FB8A_H
#ifndef ENVMAPANIMATOR_T32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73_H
#define ENVMAPANIMATOR_T32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator
struct  EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 EnvMapAnimator::RotationSpeeds
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RotationSpeeds_4;
	// TMPro.TMP_Text EnvMapAnimator::m_textMeshPro
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_textMeshPro_5;
	// UnityEngine.Material EnvMapAnimator::m_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_material_6;

public:
	inline static int32_t get_offset_of_RotationSpeeds_4() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73, ___RotationSpeeds_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RotationSpeeds_4() const { return ___RotationSpeeds_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RotationSpeeds_4() { return &___RotationSpeeds_4; }
	inline void set_RotationSpeeds_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RotationSpeeds_4 = value;
	}

	inline static int32_t get_offset_of_m_textMeshPro_5() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73, ___m_textMeshPro_5)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_textMeshPro_5() const { return ___m_textMeshPro_5; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_textMeshPro_5() { return &___m_textMeshPro_5; }
	inline void set_m_textMeshPro_5(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_textMeshPro_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_5), value);
	}

	inline static int32_t get_offset_of_m_material_6() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73, ___m_material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_material_6() const { return ___m_material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_material_6() { return &___m_material_6; }
	inline void set_m_material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVMAPANIMATOR_T32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73_H
#ifndef EXAMPLESCENEMANAGERCONTROLLER_T3673702F15DE3FB4A8EB1F2069831FF52BCEDA17_H
#define EXAMPLESCENEMANAGERCONTROLLER_T3673702F15DE3FB4A8EB1F2069831FF52BCEDA17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleSceneManagerController
struct  ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text ExampleSceneManagerController::LeftText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___LeftText_4;
	// UnityEngine.UI.Text ExampleSceneManagerController::RightText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___RightText_5;
	// UnityEngine.UI.Slider ExampleSceneManagerController::ProgressSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___ProgressSlider_6;

public:
	inline static int32_t get_offset_of_LeftText_4() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17, ___LeftText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_LeftText_4() const { return ___LeftText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_LeftText_4() { return &___LeftText_4; }
	inline void set_LeftText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___LeftText_4 = value;
		Il2CppCodeGenWriteBarrier((&___LeftText_4), value);
	}

	inline static int32_t get_offset_of_RightText_5() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17, ___RightText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_RightText_5() const { return ___RightText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_RightText_5() { return &___RightText_5; }
	inline void set_RightText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___RightText_5 = value;
		Il2CppCodeGenWriteBarrier((&___RightText_5), value);
	}

	inline static int32_t get_offset_of_ProgressSlider_6() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17, ___ProgressSlider_6)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_ProgressSlider_6() const { return ___ProgressSlider_6; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_ProgressSlider_6() { return &___ProgressSlider_6; }
	inline void set_ProgressSlider_6(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___ProgressSlider_6 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressSlider_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLESCENEMANAGERCONTROLLER_T3673702F15DE3FB4A8EB1F2069831FF52BCEDA17_H
#ifndef GAMEMANAGER_TAC830B937D5E37F47803FE8AB44CAB0762B77B89_H
#define GAMEMANAGER_TAC830B937D5E37F47803FE8AB44CAB0762B77B89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_TAC830B937D5E37F47803FE8AB44CAB0762B77B89_H
#ifndef HOLECONTAINERCONTROLLER_TE08BC9A154E37BB8F9906C1663B669FD94BD22AB_H
#define HOLECONTAINERCONTROLLER_TE08BC9A154E37BB8F9906C1663B669FD94BD22AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoleContainerController
struct  HoleContainerController_tE08BC9A154E37BB8F9906C1663B669FD94BD22AB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// ProjectorController HoleContainerController::projectorController
	ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 * ___projectorController_4;
	// UnityEngine.GameObject HoleContainerController::holeContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holeContainer_5;
	// UnityEngine.GameObject HoleContainerController::holeRect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holeRect_6;

public:
	inline static int32_t get_offset_of_projectorController_4() { return static_cast<int32_t>(offsetof(HoleContainerController_tE08BC9A154E37BB8F9906C1663B669FD94BD22AB, ___projectorController_4)); }
	inline ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 * get_projectorController_4() const { return ___projectorController_4; }
	inline ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 ** get_address_of_projectorController_4() { return &___projectorController_4; }
	inline void set_projectorController_4(ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 * value)
	{
		___projectorController_4 = value;
		Il2CppCodeGenWriteBarrier((&___projectorController_4), value);
	}

	inline static int32_t get_offset_of_holeContainer_5() { return static_cast<int32_t>(offsetof(HoleContainerController_tE08BC9A154E37BB8F9906C1663B669FD94BD22AB, ___holeContainer_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holeContainer_5() const { return ___holeContainer_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holeContainer_5() { return &___holeContainer_5; }
	inline void set_holeContainer_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holeContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___holeContainer_5), value);
	}

	inline static int32_t get_offset_of_holeRect_6() { return static_cast<int32_t>(offsetof(HoleContainerController_tE08BC9A154E37BB8F9906C1663B669FD94BD22AB, ___holeRect_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holeRect_6() const { return ___holeRect_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holeRect_6() { return &___holeRect_6; }
	inline void set_holeRect_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holeRect_6 = value;
		Il2CppCodeGenWriteBarrier((&___holeRect_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLECONTAINERCONTROLLER_TE08BC9A154E37BB8F9906C1663B669FD94BD22AB_H
#ifndef LINETOHOLE_T605CC45F1100D29849A8AD5A49793FCD292403B8_H
#define LINETOHOLE_T605CC45F1100D29849A8AD5A49793FCD292403B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LineToHole
struct  LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Material LineToHole::lineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lineMaterial_4;
	// UnityEngine.GameObject LineToHole::lineToHoleGO
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lineToHoleGO_5;
	// System.Int32 LineToHole::indexOfAnimationPoint
	int32_t ___indexOfAnimationPoint_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> LineToHole::animatedLineToHoleList
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___animatedLineToHoleList_7;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> LineToHole::puttPointsFull
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___puttPointsFull_8;
	// System.Boolean LineToHole::animateLineToHole
	bool ___animateLineToHole_9;
	// UnityEngine.LineRenderer LineToHole::lineRenderer
	LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * ___lineRenderer_10;
	// SimulationManager LineToHole::simulationManager
	SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9 * ___simulationManager_11;
	// ProjectorController LineToHole::projectorController
	ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 * ___projectorController_12;

public:
	inline static int32_t get_offset_of_lineMaterial_4() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___lineMaterial_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lineMaterial_4() const { return ___lineMaterial_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lineMaterial_4() { return &___lineMaterial_4; }
	inline void set_lineMaterial_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lineMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineMaterial_4), value);
	}

	inline static int32_t get_offset_of_lineToHoleGO_5() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___lineToHoleGO_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_lineToHoleGO_5() const { return ___lineToHoleGO_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_lineToHoleGO_5() { return &___lineToHoleGO_5; }
	inline void set_lineToHoleGO_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___lineToHoleGO_5 = value;
		Il2CppCodeGenWriteBarrier((&___lineToHoleGO_5), value);
	}

	inline static int32_t get_offset_of_indexOfAnimationPoint_6() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___indexOfAnimationPoint_6)); }
	inline int32_t get_indexOfAnimationPoint_6() const { return ___indexOfAnimationPoint_6; }
	inline int32_t* get_address_of_indexOfAnimationPoint_6() { return &___indexOfAnimationPoint_6; }
	inline void set_indexOfAnimationPoint_6(int32_t value)
	{
		___indexOfAnimationPoint_6 = value;
	}

	inline static int32_t get_offset_of_animatedLineToHoleList_7() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___animatedLineToHoleList_7)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_animatedLineToHoleList_7() const { return ___animatedLineToHoleList_7; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_animatedLineToHoleList_7() { return &___animatedLineToHoleList_7; }
	inline void set_animatedLineToHoleList_7(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___animatedLineToHoleList_7 = value;
		Il2CppCodeGenWriteBarrier((&___animatedLineToHoleList_7), value);
	}

	inline static int32_t get_offset_of_puttPointsFull_8() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___puttPointsFull_8)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_puttPointsFull_8() const { return ___puttPointsFull_8; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_puttPointsFull_8() { return &___puttPointsFull_8; }
	inline void set_puttPointsFull_8(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___puttPointsFull_8 = value;
		Il2CppCodeGenWriteBarrier((&___puttPointsFull_8), value);
	}

	inline static int32_t get_offset_of_animateLineToHole_9() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___animateLineToHole_9)); }
	inline bool get_animateLineToHole_9() const { return ___animateLineToHole_9; }
	inline bool* get_address_of_animateLineToHole_9() { return &___animateLineToHole_9; }
	inline void set_animateLineToHole_9(bool value)
	{
		___animateLineToHole_9 = value;
	}

	inline static int32_t get_offset_of_lineRenderer_10() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___lineRenderer_10)); }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * get_lineRenderer_10() const { return ___lineRenderer_10; }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 ** get_address_of_lineRenderer_10() { return &___lineRenderer_10; }
	inline void set_lineRenderer_10(LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * value)
	{
		___lineRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderer_10), value);
	}

	inline static int32_t get_offset_of_simulationManager_11() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___simulationManager_11)); }
	inline SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9 * get_simulationManager_11() const { return ___simulationManager_11; }
	inline SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9 ** get_address_of_simulationManager_11() { return &___simulationManager_11; }
	inline void set_simulationManager_11(SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9 * value)
	{
		___simulationManager_11 = value;
		Il2CppCodeGenWriteBarrier((&___simulationManager_11), value);
	}

	inline static int32_t get_offset_of_projectorController_12() { return static_cast<int32_t>(offsetof(LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8, ___projectorController_12)); }
	inline ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 * get_projectorController_12() const { return ___projectorController_12; }
	inline ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 ** get_address_of_projectorController_12() { return &___projectorController_12; }
	inline void set_projectorController_12(ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 * value)
	{
		___projectorController_12 = value;
		Il2CppCodeGenWriteBarrier((&___projectorController_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETOHOLE_T605CC45F1100D29849A8AD5A49793FCD292403B8_H
#ifndef LINETOTARGET_T8E738E8246127456BF830AA8D789CC666488491E_H
#define LINETOTARGET_T8E738E8246127456BF830AA8D789CC666488491E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LineToTarget
struct  LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> LineToTarget::pathToTargetMarkerList
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___pathToTargetMarkerList_4;
	// UnityEngine.Vector3 LineToTarget::startLinePosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startLinePosition_5;
	// UnityEngine.Vector3 LineToTarget::endLinePosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___endLinePosition_6;
	// UnityEngine.GameObject LineToTarget::targetLineSegmentPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targetLineSegmentPrefab_7;
	// UnityEngine.GameObject LineToTarget::targetPrefabInst
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targetPrefabInst_8;
	// UnityEngine.GameObject LineToTarget::targetPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targetPrefab_9;
	// PuttSimulation LineToTarget::puttSimulation
	PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD * ___puttSimulation_10;

public:
	inline static int32_t get_offset_of_pathToTargetMarkerList_4() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___pathToTargetMarkerList_4)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_pathToTargetMarkerList_4() const { return ___pathToTargetMarkerList_4; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_pathToTargetMarkerList_4() { return &___pathToTargetMarkerList_4; }
	inline void set_pathToTargetMarkerList_4(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___pathToTargetMarkerList_4 = value;
		Il2CppCodeGenWriteBarrier((&___pathToTargetMarkerList_4), value);
	}

	inline static int32_t get_offset_of_startLinePosition_5() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___startLinePosition_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startLinePosition_5() const { return ___startLinePosition_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startLinePosition_5() { return &___startLinePosition_5; }
	inline void set_startLinePosition_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startLinePosition_5 = value;
	}

	inline static int32_t get_offset_of_endLinePosition_6() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___endLinePosition_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_endLinePosition_6() const { return ___endLinePosition_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_endLinePosition_6() { return &___endLinePosition_6; }
	inline void set_endLinePosition_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___endLinePosition_6 = value;
	}

	inline static int32_t get_offset_of_targetLineSegmentPrefab_7() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___targetLineSegmentPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targetLineSegmentPrefab_7() const { return ___targetLineSegmentPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targetLineSegmentPrefab_7() { return &___targetLineSegmentPrefab_7; }
	inline void set_targetLineSegmentPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targetLineSegmentPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___targetLineSegmentPrefab_7), value);
	}

	inline static int32_t get_offset_of_targetPrefabInst_8() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___targetPrefabInst_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targetPrefabInst_8() const { return ___targetPrefabInst_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targetPrefabInst_8() { return &___targetPrefabInst_8; }
	inline void set_targetPrefabInst_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targetPrefabInst_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetPrefabInst_8), value);
	}

	inline static int32_t get_offset_of_targetPrefab_9() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___targetPrefab_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targetPrefab_9() const { return ___targetPrefab_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targetPrefab_9() { return &___targetPrefab_9; }
	inline void set_targetPrefab_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targetPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___targetPrefab_9), value);
	}

	inline static int32_t get_offset_of_puttSimulation_10() { return static_cast<int32_t>(offsetof(LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E, ___puttSimulation_10)); }
	inline PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD * get_puttSimulation_10() const { return ___puttSimulation_10; }
	inline PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD ** get_address_of_puttSimulation_10() { return &___puttSimulation_10; }
	inline void set_puttSimulation_10(PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD * value)
	{
		___puttSimulation_10 = value;
		Il2CppCodeGenWriteBarrier((&___puttSimulation_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETOTARGET_T8E738E8246127456BF830AA8D789CC666488491E_H
#ifndef LOGINVALIDATECREDENTIALS_T7BACED90D9DBFF5BC46589062D86BA9BA6A63692_H
#define LOGINVALIDATECREDENTIALS_T7BACED90D9DBFF5BC46589062D86BA9BA6A63692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginValidateCredentials
struct  LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TMP_InputField LoginValidateCredentials::userNameIF
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ___userNameIF_5;
	// TMPro.TMP_InputField LoginValidateCredentials::passwordIF
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ___passwordIF_6;
	// UnityEngine.UI.Button LoginValidateCredentials::submitBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___submitBtn_7;
	// Strackaline.User LoginValidateCredentials::user
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * ___user_8;

public:
	inline static int32_t get_offset_of_userNameIF_5() { return static_cast<int32_t>(offsetof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692, ___userNameIF_5)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get_userNameIF_5() const { return ___userNameIF_5; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of_userNameIF_5() { return &___userNameIF_5; }
	inline void set_userNameIF_5(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		___userNameIF_5 = value;
		Il2CppCodeGenWriteBarrier((&___userNameIF_5), value);
	}

	inline static int32_t get_offset_of_passwordIF_6() { return static_cast<int32_t>(offsetof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692, ___passwordIF_6)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get_passwordIF_6() const { return ___passwordIF_6; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of_passwordIF_6() { return &___passwordIF_6; }
	inline void set_passwordIF_6(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		___passwordIF_6 = value;
		Il2CppCodeGenWriteBarrier((&___passwordIF_6), value);
	}

	inline static int32_t get_offset_of_submitBtn_7() { return static_cast<int32_t>(offsetof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692, ___submitBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_submitBtn_7() const { return ___submitBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_submitBtn_7() { return &___submitBtn_7; }
	inline void set_submitBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___submitBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&___submitBtn_7), value);
	}

	inline static int32_t get_offset_of_user_8() { return static_cast<int32_t>(offsetof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692, ___user_8)); }
	inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * get_user_8() const { return ___user_8; }
	inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 ** get_address_of_user_8() { return &___user_8; }
	inline void set_user_8(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * value)
	{
		___user_8 = value;
		Il2CppCodeGenWriteBarrier((&___user_8), value);
	}
};

struct LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692_StaticFields
{
public:
	// System.Action LoginValidateCredentials::UserDataSet
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___UserDataSet_4;

public:
	inline static int32_t get_offset_of_UserDataSet_4() { return static_cast<int32_t>(offsetof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692_StaticFields, ___UserDataSet_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_UserDataSet_4() const { return ___UserDataSet_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_UserDataSet_4() { return &___UserDataSet_4; }
	inline void set_UserDataSet_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___UserDataSet_4 = value;
		Il2CppCodeGenWriteBarrier((&___UserDataSet_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINVALIDATECREDENTIALS_T7BACED90D9DBFF5BC46589062D86BA9BA6A63692_H
#ifndef LOGINVIEWCONTROLLER_T888A1D2D4AD363F23E8CD99AB0EDC73916209833_H
#define LOGINVIEWCONTROLLER_T888A1D2D4AD363F23E8CD99AB0EDC73916209833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginViewController
struct  LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// LoginValidateCredentials LoginViewController::loginValidateCredentials
	LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692 * ___loginValidateCredentials_4;

public:
	inline static int32_t get_offset_of_loginValidateCredentials_4() { return static_cast<int32_t>(offsetof(LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833, ___loginValidateCredentials_4)); }
	inline LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692 * get_loginValidateCredentials_4() const { return ___loginValidateCredentials_4; }
	inline LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692 ** get_address_of_loginValidateCredentials_4() { return &___loginValidateCredentials_4; }
	inline void set_loginValidateCredentials_4(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692 * value)
	{
		___loginValidateCredentials_4 = value;
		Il2CppCodeGenWriteBarrier((&___loginValidateCredentials_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINVIEWCONTROLLER_T888A1D2D4AD363F23E8CD99AB0EDC73916209833_H
#ifndef PROJECTORCONTROLLER_T6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6_H
#define PROJECTORCONTROLLER_T6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectorController
struct  ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// ProjectorStatusData ProjectorController::projectorStatusData
	ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD * ___projectorStatusData_4;
	// Strackaline.GreenActivity ProjectorController::greenActivity
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B * ___greenActivity_5;
	// System.Int32 ProjectorController::configurationId
	int32_t ___configurationId_6;

public:
	inline static int32_t get_offset_of_projectorStatusData_4() { return static_cast<int32_t>(offsetof(ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6, ___projectorStatusData_4)); }
	inline ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD * get_projectorStatusData_4() const { return ___projectorStatusData_4; }
	inline ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD ** get_address_of_projectorStatusData_4() { return &___projectorStatusData_4; }
	inline void set_projectorStatusData_4(ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD * value)
	{
		___projectorStatusData_4 = value;
		Il2CppCodeGenWriteBarrier((&___projectorStatusData_4), value);
	}

	inline static int32_t get_offset_of_greenActivity_5() { return static_cast<int32_t>(offsetof(ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6, ___greenActivity_5)); }
	inline GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B * get_greenActivity_5() const { return ___greenActivity_5; }
	inline GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B ** get_address_of_greenActivity_5() { return &___greenActivity_5; }
	inline void set_greenActivity_5(GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B * value)
	{
		___greenActivity_5 = value;
		Il2CppCodeGenWriteBarrier((&___greenActivity_5), value);
	}

	inline static int32_t get_offset_of_configurationId_6() { return static_cast<int32_t>(offsetof(ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6, ___configurationId_6)); }
	inline int32_t get_configurationId_6() const { return ___configurationId_6; }
	inline int32_t* get_address_of_configurationId_6() { return &___configurationId_6; }
	inline void set_configurationId_6(int32_t value)
	{
		___configurationId_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTORCONTROLLER_T6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6_H
#ifndef PUTTASSISTANCE_T800C93BC51C0BC9E14594D4F93FC855297A7E431_H
#define PUTTASSISTANCE_T800C93BC51C0BC9E14594D4F93FC855297A7E431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuttAssistance
struct  PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PuttAssistance::puttRhythmPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttRhythmPrefab_4;
	// PuttInteractive PuttAssistance::puttInteractive
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60 * ___puttInteractive_5;
	// UnityEngine.GameObject PuttAssistance::puttStanceBoxInst
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttStanceBoxInst_6;
	// UnityEngine.GameObject PuttAssistance::puttRhythmIcon
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttRhythmIcon_7;
	// UnityEngine.GameObject PuttAssistance::stanceRightHandedLine
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stanceRightHandedLine_8;
	// UnityEngine.GameObject PuttAssistance::stanceLeftHandedLine
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stanceLeftHandedLine_9;
	// UnityEngine.Vector3 PuttAssistance::directionOfHoleLocation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___directionOfHoleLocation_10;

public:
	inline static int32_t get_offset_of_puttRhythmPrefab_4() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___puttRhythmPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttRhythmPrefab_4() const { return ___puttRhythmPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttRhythmPrefab_4() { return &___puttRhythmPrefab_4; }
	inline void set_puttRhythmPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttRhythmPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___puttRhythmPrefab_4), value);
	}

	inline static int32_t get_offset_of_puttInteractive_5() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___puttInteractive_5)); }
	inline PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60 * get_puttInteractive_5() const { return ___puttInteractive_5; }
	inline PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60 ** get_address_of_puttInteractive_5() { return &___puttInteractive_5; }
	inline void set_puttInteractive_5(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60 * value)
	{
		___puttInteractive_5 = value;
		Il2CppCodeGenWriteBarrier((&___puttInteractive_5), value);
	}

	inline static int32_t get_offset_of_puttStanceBoxInst_6() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___puttStanceBoxInst_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttStanceBoxInst_6() const { return ___puttStanceBoxInst_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttStanceBoxInst_6() { return &___puttStanceBoxInst_6; }
	inline void set_puttStanceBoxInst_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttStanceBoxInst_6 = value;
		Il2CppCodeGenWriteBarrier((&___puttStanceBoxInst_6), value);
	}

	inline static int32_t get_offset_of_puttRhythmIcon_7() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___puttRhythmIcon_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttRhythmIcon_7() const { return ___puttRhythmIcon_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttRhythmIcon_7() { return &___puttRhythmIcon_7; }
	inline void set_puttRhythmIcon_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttRhythmIcon_7 = value;
		Il2CppCodeGenWriteBarrier((&___puttRhythmIcon_7), value);
	}

	inline static int32_t get_offset_of_stanceRightHandedLine_8() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___stanceRightHandedLine_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stanceRightHandedLine_8() const { return ___stanceRightHandedLine_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stanceRightHandedLine_8() { return &___stanceRightHandedLine_8; }
	inline void set_stanceRightHandedLine_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stanceRightHandedLine_8 = value;
		Il2CppCodeGenWriteBarrier((&___stanceRightHandedLine_8), value);
	}

	inline static int32_t get_offset_of_stanceLeftHandedLine_9() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___stanceLeftHandedLine_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stanceLeftHandedLine_9() const { return ___stanceLeftHandedLine_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stanceLeftHandedLine_9() { return &___stanceLeftHandedLine_9; }
	inline void set_stanceLeftHandedLine_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stanceLeftHandedLine_9 = value;
		Il2CppCodeGenWriteBarrier((&___stanceLeftHandedLine_9), value);
	}

	inline static int32_t get_offset_of_directionOfHoleLocation_10() { return static_cast<int32_t>(offsetof(PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431, ___directionOfHoleLocation_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_directionOfHoleLocation_10() const { return ___directionOfHoleLocation_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_directionOfHoleLocation_10() { return &___directionOfHoleLocation_10; }
	inline void set_directionOfHoleLocation_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___directionOfHoleLocation_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTASSISTANCE_T800C93BC51C0BC9E14594D4F93FC855297A7E431_H
#ifndef PUTTINTERACTIVE_TED9BF5B0E0860C6574B4368FB70D0CF56C8EED60_H
#define PUTTINTERACTIVE_TED9BF5B0E0860C6574B4368FB70D0CF56C8EED60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuttInteractive
struct  PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Strackaline.PuttVariation PuttInteractive::_puttVariation
	PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31 * ____puttVariation_4;
	// UnityEngine.GameObject PuttInteractive::ballDragInput
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ballDragInput_5;
	// UnityEngine.GameObject PuttInteractive::holeDragInput
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holeDragInput_6;
	// UnityEngine.Vector3 PuttInteractive::ballLocation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___ballLocation_7;
	// UnityEngine.Vector3 PuttInteractive::holeLocation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___holeLocation_8;
	// System.Boolean PuttInteractive::isActive
	bool ___isActive_9;
	// System.Collections.Generic.List`1<PuttSimulation> PuttInteractive::puttSimulationsList
	List_1_t494E45893FDE2DD89DD40901A1C26C18540C2BCD * ___puttSimulationsList_10;
	// UnityEngine.GameObject PuttInteractive::puttSimulationPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttSimulationPrefab_11;
	// UnityEngine.Material PuttInteractive::invalidPuttMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___invalidPuttMaterial_12;
	// UnityEngine.Material PuttInteractive::validPuttMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___validPuttMaterial_13;

public:
	inline static int32_t get_offset_of__puttVariation_4() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ____puttVariation_4)); }
	inline PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31 * get__puttVariation_4() const { return ____puttVariation_4; }
	inline PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31 ** get_address_of__puttVariation_4() { return &____puttVariation_4; }
	inline void set__puttVariation_4(PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31 * value)
	{
		____puttVariation_4 = value;
		Il2CppCodeGenWriteBarrier((&____puttVariation_4), value);
	}

	inline static int32_t get_offset_of_ballDragInput_5() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___ballDragInput_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ballDragInput_5() const { return ___ballDragInput_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ballDragInput_5() { return &___ballDragInput_5; }
	inline void set_ballDragInput_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ballDragInput_5 = value;
		Il2CppCodeGenWriteBarrier((&___ballDragInput_5), value);
	}

	inline static int32_t get_offset_of_holeDragInput_6() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___holeDragInput_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holeDragInput_6() const { return ___holeDragInput_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holeDragInput_6() { return &___holeDragInput_6; }
	inline void set_holeDragInput_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holeDragInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___holeDragInput_6), value);
	}

	inline static int32_t get_offset_of_ballLocation_7() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___ballLocation_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_ballLocation_7() const { return ___ballLocation_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_ballLocation_7() { return &___ballLocation_7; }
	inline void set_ballLocation_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___ballLocation_7 = value;
	}

	inline static int32_t get_offset_of_holeLocation_8() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___holeLocation_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_holeLocation_8() const { return ___holeLocation_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_holeLocation_8() { return &___holeLocation_8; }
	inline void set_holeLocation_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___holeLocation_8 = value;
	}

	inline static int32_t get_offset_of_isActive_9() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___isActive_9)); }
	inline bool get_isActive_9() const { return ___isActive_9; }
	inline bool* get_address_of_isActive_9() { return &___isActive_9; }
	inline void set_isActive_9(bool value)
	{
		___isActive_9 = value;
	}

	inline static int32_t get_offset_of_puttSimulationsList_10() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___puttSimulationsList_10)); }
	inline List_1_t494E45893FDE2DD89DD40901A1C26C18540C2BCD * get_puttSimulationsList_10() const { return ___puttSimulationsList_10; }
	inline List_1_t494E45893FDE2DD89DD40901A1C26C18540C2BCD ** get_address_of_puttSimulationsList_10() { return &___puttSimulationsList_10; }
	inline void set_puttSimulationsList_10(List_1_t494E45893FDE2DD89DD40901A1C26C18540C2BCD * value)
	{
		___puttSimulationsList_10 = value;
		Il2CppCodeGenWriteBarrier((&___puttSimulationsList_10), value);
	}

	inline static int32_t get_offset_of_puttSimulationPrefab_11() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___puttSimulationPrefab_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttSimulationPrefab_11() const { return ___puttSimulationPrefab_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttSimulationPrefab_11() { return &___puttSimulationPrefab_11; }
	inline void set_puttSimulationPrefab_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttSimulationPrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___puttSimulationPrefab_11), value);
	}

	inline static int32_t get_offset_of_invalidPuttMaterial_12() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___invalidPuttMaterial_12)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_invalidPuttMaterial_12() const { return ___invalidPuttMaterial_12; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_invalidPuttMaterial_12() { return &___invalidPuttMaterial_12; }
	inline void set_invalidPuttMaterial_12(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___invalidPuttMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___invalidPuttMaterial_12), value);
	}

	inline static int32_t get_offset_of_validPuttMaterial_13() { return static_cast<int32_t>(offsetof(PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60, ___validPuttMaterial_13)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_validPuttMaterial_13() const { return ___validPuttMaterial_13; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_validPuttMaterial_13() { return &___validPuttMaterial_13; }
	inline void set_validPuttMaterial_13(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___validPuttMaterial_13 = value;
		Il2CppCodeGenWriteBarrier((&___validPuttMaterial_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTINTERACTIVE_TED9BF5B0E0860C6574B4368FB70D0CF56C8EED60_H
#ifndef PUTTMANAGER_TFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_H
#define PUTTMANAGER_TFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuttManager
struct  PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// ProjectorController PuttManager::projectorController
	ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 * ___projectorController_4;
	// UnityEngine.GameObject PuttManager::holePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holePrefab_5;
	// UnityEngine.GameObject PuttManager::holeInst
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holeInst_6;
	// UnityEngine.GameObject PuttManager::puttInteractivePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttInteractivePrefab_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PuttManager::puttInteractiveList
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___puttInteractiveList_8;

public:
	inline static int32_t get_offset_of_projectorController_4() { return static_cast<int32_t>(offsetof(PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41, ___projectorController_4)); }
	inline ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 * get_projectorController_4() const { return ___projectorController_4; }
	inline ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 ** get_address_of_projectorController_4() { return &___projectorController_4; }
	inline void set_projectorController_4(ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6 * value)
	{
		___projectorController_4 = value;
		Il2CppCodeGenWriteBarrier((&___projectorController_4), value);
	}

	inline static int32_t get_offset_of_holePrefab_5() { return static_cast<int32_t>(offsetof(PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41, ___holePrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holePrefab_5() const { return ___holePrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holePrefab_5() { return &___holePrefab_5; }
	inline void set_holePrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holePrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___holePrefab_5), value);
	}

	inline static int32_t get_offset_of_holeInst_6() { return static_cast<int32_t>(offsetof(PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41, ___holeInst_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holeInst_6() const { return ___holeInst_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holeInst_6() { return &___holeInst_6; }
	inline void set_holeInst_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holeInst_6 = value;
		Il2CppCodeGenWriteBarrier((&___holeInst_6), value);
	}

	inline static int32_t get_offset_of_puttInteractivePrefab_7() { return static_cast<int32_t>(offsetof(PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41, ___puttInteractivePrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttInteractivePrefab_7() const { return ___puttInteractivePrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttInteractivePrefab_7() { return &___puttInteractivePrefab_7; }
	inline void set_puttInteractivePrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttInteractivePrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___puttInteractivePrefab_7), value);
	}

	inline static int32_t get_offset_of_puttInteractiveList_8() { return static_cast<int32_t>(offsetof(PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41, ___puttInteractiveList_8)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_puttInteractiveList_8() const { return ___puttInteractiveList_8; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_puttInteractiveList_8() { return &___puttInteractiveList_8; }
	inline void set_puttInteractiveList_8(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___puttInteractiveList_8 = value;
		Il2CppCodeGenWriteBarrier((&___puttInteractiveList_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTMANAGER_TFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41_H
#ifndef PUTTRYTHM_T4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0_H
#define PUTTRYTHM_T4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuttRythm
struct  PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PuttRythm::puttRythmGO
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___puttRythmGO_4;
	// DG.Tweening.Tween PuttRythm::tween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___tween_5;
	// UnityEngine.Transform PuttRythm::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_6;
	// UnityEngine.Vector3 PuttRythm::targetPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPos_7;
	// UnityEngine.Vector3 PuttRythm::origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin_8;
	// UnityEngine.GameObject PuttRythm::targeBehindBallCube
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targeBehindBallCube_9;
	// UnityEngine.GameObject PuttRythm::targetHoleCube
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targetHoleCube_10;
	// System.Single PuttRythm::speed
	float ___speed_11;
	// UnityEngine.Vector3 PuttRythm::puttRythmLineVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___puttRythmLineVector_12;
	// UnityEngine.GameObject PuttRythm::stanceRightHandedLine
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stanceRightHandedLine_13;
	// UnityEngine.GameObject PuttRythm::stanceLeftHandedLine
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stanceLeftHandedLine_14;
	// PuttRythm_Direction PuttRythm::direction
	int32_t ___direction_15;

public:
	inline static int32_t get_offset_of_puttRythmGO_4() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___puttRythmGO_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_puttRythmGO_4() const { return ___puttRythmGO_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_puttRythmGO_4() { return &___puttRythmGO_4; }
	inline void set_puttRythmGO_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___puttRythmGO_4 = value;
		Il2CppCodeGenWriteBarrier((&___puttRythmGO_4), value);
	}

	inline static int32_t get_offset_of_tween_5() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___tween_5)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_tween_5() const { return ___tween_5; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_tween_5() { return &___tween_5; }
	inline void set_tween_5(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___tween_5 = value;
		Il2CppCodeGenWriteBarrier((&___tween_5), value);
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___target_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_6() const { return ___target_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_targetPos_7() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___targetPos_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPos_7() const { return ___targetPos_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPos_7() { return &___targetPos_7; }
	inline void set_targetPos_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPos_7 = value;
	}

	inline static int32_t get_offset_of_origin_8() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___origin_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_origin_8() const { return ___origin_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_origin_8() { return &___origin_8; }
	inline void set_origin_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___origin_8 = value;
	}

	inline static int32_t get_offset_of_targeBehindBallCube_9() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___targeBehindBallCube_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targeBehindBallCube_9() const { return ___targeBehindBallCube_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targeBehindBallCube_9() { return &___targeBehindBallCube_9; }
	inline void set_targeBehindBallCube_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targeBehindBallCube_9 = value;
		Il2CppCodeGenWriteBarrier((&___targeBehindBallCube_9), value);
	}

	inline static int32_t get_offset_of_targetHoleCube_10() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___targetHoleCube_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targetHoleCube_10() const { return ___targetHoleCube_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targetHoleCube_10() { return &___targetHoleCube_10; }
	inline void set_targetHoleCube_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targetHoleCube_10 = value;
		Il2CppCodeGenWriteBarrier((&___targetHoleCube_10), value);
	}

	inline static int32_t get_offset_of_speed_11() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___speed_11)); }
	inline float get_speed_11() const { return ___speed_11; }
	inline float* get_address_of_speed_11() { return &___speed_11; }
	inline void set_speed_11(float value)
	{
		___speed_11 = value;
	}

	inline static int32_t get_offset_of_puttRythmLineVector_12() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___puttRythmLineVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_puttRythmLineVector_12() const { return ___puttRythmLineVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_puttRythmLineVector_12() { return &___puttRythmLineVector_12; }
	inline void set_puttRythmLineVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___puttRythmLineVector_12 = value;
	}

	inline static int32_t get_offset_of_stanceRightHandedLine_13() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___stanceRightHandedLine_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stanceRightHandedLine_13() const { return ___stanceRightHandedLine_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stanceRightHandedLine_13() { return &___stanceRightHandedLine_13; }
	inline void set_stanceRightHandedLine_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stanceRightHandedLine_13 = value;
		Il2CppCodeGenWriteBarrier((&___stanceRightHandedLine_13), value);
	}

	inline static int32_t get_offset_of_stanceLeftHandedLine_14() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___stanceLeftHandedLine_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stanceLeftHandedLine_14() const { return ___stanceLeftHandedLine_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stanceLeftHandedLine_14() { return &___stanceLeftHandedLine_14; }
	inline void set_stanceLeftHandedLine_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stanceLeftHandedLine_14 = value;
		Il2CppCodeGenWriteBarrier((&___stanceLeftHandedLine_14), value);
	}

	inline static int32_t get_offset_of_direction_15() { return static_cast<int32_t>(offsetof(PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0, ___direction_15)); }
	inline int32_t get_direction_15() const { return ___direction_15; }
	inline int32_t* get_address_of_direction_15() { return &___direction_15; }
	inline void set_direction_15(int32_t value)
	{
		___direction_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTRYTHM_T4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0_H
#ifndef PUTTSIMULATION_T57B97DFD11598230F458B4832CB409B16F7A53AD_H
#define PUTTSIMULATION_T57B97DFD11598230F458B4832CB409B16F7A53AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuttSimulation
struct  PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Strackaline.Putt PuttSimulation::putt
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49 * ___putt_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> PuttSimulation::puttPointsFull
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___puttPointsFull_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> PuttSimulation::puttPointsHalved
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___puttPointsHalved_6;

public:
	inline static int32_t get_offset_of_putt_4() { return static_cast<int32_t>(offsetof(PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD, ___putt_4)); }
	inline Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49 * get_putt_4() const { return ___putt_4; }
	inline Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49 ** get_address_of_putt_4() { return &___putt_4; }
	inline void set_putt_4(Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49 * value)
	{
		___putt_4 = value;
		Il2CppCodeGenWriteBarrier((&___putt_4), value);
	}

	inline static int32_t get_offset_of_puttPointsFull_5() { return static_cast<int32_t>(offsetof(PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD, ___puttPointsFull_5)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_puttPointsFull_5() const { return ___puttPointsFull_5; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_puttPointsFull_5() { return &___puttPointsFull_5; }
	inline void set_puttPointsFull_5(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___puttPointsFull_5 = value;
		Il2CppCodeGenWriteBarrier((&___puttPointsFull_5), value);
	}

	inline static int32_t get_offset_of_puttPointsHalved_6() { return static_cast<int32_t>(offsetof(PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD, ___puttPointsHalved_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_puttPointsHalved_6() const { return ___puttPointsHalved_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_puttPointsHalved_6() { return &___puttPointsHalved_6; }
	inline void set_puttPointsHalved_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___puttPointsHalved_6 = value;
		Il2CppCodeGenWriteBarrier((&___puttPointsHalved_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTTSIMULATION_T57B97DFD11598230F458B4832CB409B16F7A53AD_H
#ifndef ROBOGREENSETTINGS_TAF3E81EEEC76595CB4A63EC2E4AD8D99297B7773_H
#define ROBOGREENSETTINGS_TAF3E81EEEC76595CB4A63EC2E4AD8D99297B7773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoboGreenSettings
struct  RoboGreenSettings_tAF3E81EEEC76595CB4A63EC2E4AD8D99297B7773  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROBOGREENSETTINGS_TAF3E81EEEC76595CB4A63EC2E4AD8D99297B7773_H
#ifndef SIMULATIONMANAGER_T1D7922488B2B05F2C32B9A52B64F2445555C17B9_H
#define SIMULATIONMANAGER_T1D7922488B2B05F2C32B9A52B64F2445555C17B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimulationManager
struct  SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean SimulationManager::loopPutt
	bool ___loopPutt_4;
	// SimulationManager_PuttLineSpeed SimulationManager::puttLineSpeed
	int32_t ___puttLineSpeed_5;

public:
	inline static int32_t get_offset_of_loopPutt_4() { return static_cast<int32_t>(offsetof(SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9, ___loopPutt_4)); }
	inline bool get_loopPutt_4() const { return ___loopPutt_4; }
	inline bool* get_address_of_loopPutt_4() { return &___loopPutt_4; }
	inline void set_loopPutt_4(bool value)
	{
		___loopPutt_4 = value;
	}

	inline static int32_t get_offset_of_puttLineSpeed_5() { return static_cast<int32_t>(offsetof(SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9, ___puttLineSpeed_5)); }
	inline int32_t get_puttLineSpeed_5() const { return ___puttLineSpeed_5; }
	inline int32_t* get_address_of_puttLineSpeed_5() { return &___puttLineSpeed_5; }
	inline void set_puttLineSpeed_5(int32_t value)
	{
		___puttLineSpeed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMULATIONMANAGER_T1D7922488B2B05F2C32B9A52B64F2445555C17B9_H
#ifndef USERCONTROLLER_TFD917E07B535D607AFD7056993FB736FD366E600_H
#define USERCONTROLLER_TFD917E07B535D607AFD7056993FB736FD366E600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserController
struct  UserController_tFD917E07B535D607AFD7056993FB736FD366E600  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Doozy.Engine.UI.UIView UserController::login
	UIView_t65D38836246049951821D4E45B1C81947591EBDF * ___login_5;

public:
	inline static int32_t get_offset_of_login_5() { return static_cast<int32_t>(offsetof(UserController_tFD917E07B535D607AFD7056993FB736FD366E600, ___login_5)); }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF * get_login_5() const { return ___login_5; }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF ** get_address_of_login_5() { return &___login_5; }
	inline void set_login_5(UIView_t65D38836246049951821D4E45B1C81947591EBDF * value)
	{
		___login_5 = value;
		Il2CppCodeGenWriteBarrier((&___login_5), value);
	}
};

struct UserController_tFD917E07B535D607AFD7056993FB736FD366E600_StaticFields
{
public:
	// Strackaline.User UserController::user
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * ___user_4;

public:
	inline static int32_t get_offset_of_user_4() { return static_cast<int32_t>(offsetof(UserController_tFD917E07B535D607AFD7056993FB736FD366E600_StaticFields, ___user_4)); }
	inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * get_user_4() const { return ___user_4; }
	inline User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 ** get_address_of_user_4() { return &___user_4; }
	inline void set_user_4(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371 * value)
	{
		___user_4 = value;
		Il2CppCodeGenWriteBarrier((&___user_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERCONTROLLER_TFD917E07B535D607AFD7056993FB736FD366E600_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5200 = { sizeof (TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5200[8] = 
{
	TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5201 = { sizeof (VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A)+ sizeof (RuntimeObject), sizeof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A ), 0, 0 };
extern const int32_t g_FieldOffsetTable5201[4] = 
{
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_topLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_topRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_bottomLeft_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_bottomRight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5202 = { sizeof (TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24)+ sizeof (RuntimeObject), sizeof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5202[5] = 
{
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_firstCharacterIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_lastCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_ascender_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_baseLine_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_descender_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5203 = { sizeof (TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5203[7] = 
{
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_hashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkIdFirstCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkIdLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkTextfirstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkTextLength_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkID_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5204 = { sizeof (TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5204[4] = 
{
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_firstCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_lastCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_characterCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5205 = { sizeof (TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB)+ sizeof (RuntimeObject), sizeof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB ), 0, 0 };
extern const int32_t g_FieldOffsetTable5205[3] = 
{
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_spriteIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_characterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_vertexIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5206 = { sizeof (Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3)+ sizeof (RuntimeObject), sizeof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5206[2] = 
{
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5207 = { sizeof (Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8)+ sizeof (RuntimeObject), sizeof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5207[2] = 
{
	Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5208 = { sizeof (WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5208[55] = 
{
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previous_WordBreak_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_total_CharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_CharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_SpriteCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_LinkCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lastVisibleCharIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineNumber_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxCapHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxAscender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxDescender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxLineAscender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxLineDescender_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previousLineAscender_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_xAdvance_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_preferredWidth_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_preferredHeight_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previousLineScale_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_wordCount_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontStyle_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontScale_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontScaleMultiplier_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentFontSize_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_baselineOffset_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineOffset_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_textInfo_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineInfo_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_vertexColor_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_basicStyleStack_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_colorStack_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_underlineColorStack_35() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_strikethroughColorStack_36() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_highlightColorStack_37() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_colorGradientStack_38() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_sizeStack_39() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_indentStack_40() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontWeightStack_41() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_styleStack_42() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_baselineStack_43() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_actionStack_44() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_materialReferenceStack_45() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineJustificationStack_46() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_spriteAnimationID_47() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentFontAsset_48() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentSpriteAsset_49() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentMaterial_50() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentMaterialIndex_51() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_meshExtents_52() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_tagNoParsing_53() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_isNonBreakingSpace_54() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5209 = { sizeof (TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5)+ sizeof (RuntimeObject), sizeof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5209[3] = 
{
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_startIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_hashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5210 = { sizeof (RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98)+ sizeof (RuntimeObject), sizeof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5210[6] = 
{
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_nameHashCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_valueHashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_valueType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_valueStartIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_valueLength_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_unitType_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5211 = { sizeof (SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5211[3] = 
{
	SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5212 = { sizeof (TexturePacker_t2549189919276EB833F83EA937AB992420E1B199), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5213 = { sizeof (SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04)+ sizeof (RuntimeObject), sizeof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5213[4] = 
{
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_w_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_h_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5214 = { sizeof (SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E)+ sizeof (RuntimeObject), sizeof(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E ), 0, 0 };
extern const int32_t g_FieldOffsetTable5214[2] = 
{
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E::get_offset_of_w_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E::get_offset_of_h_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5215 = { sizeof (SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728)+ sizeof (RuntimeObject), sizeof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5215[7] = 
{
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_filename_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_frame_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_rotated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_trimmed_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_spriteSourceSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_sourceSize_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_pivot_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5216 = { sizeof (SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5216[1] = 
{
	SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E::get_offset_of_frames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5217 = { sizeof (U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380), -1, sizeof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5217[2] = 
{
	U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields::get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields::get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5218 = { sizeof (__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5219 = { sizeof (__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5220 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5221 = { sizeof (ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5221[3] = 
{
	ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17::get_offset_of_LeftText_4(),
	ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17::get_offset_of_RightText_5(),
	ExampleSceneManagerController_t3673702F15DE3FB4A8EB1F2069831FF52BCEDA17::get_offset_of_ProgressSlider_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5222 = { sizeof (U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86), -1, sizeof(U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5222[7] = 
{
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9__6_0_2(),
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9__7_0_3(),
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9__8_0_4(),
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9__9_0_5(),
	U3CU3Ec_t8D0D338667B7ED7008F8BC357EC849EA153D4F86_StaticFields::get_offset_of_U3CU3E9__10_0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5223 = { sizeof (ChatController_t49D7A1D868EED265CD37C4469FAFCF44235384FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5223[3] = 
{
	ChatController_t49D7A1D868EED265CD37C4469FAFCF44235384FB::get_offset_of_TMP_ChatInput_4(),
	ChatController_t49D7A1D868EED265CD37C4469FAFCF44235384FB::get_offset_of_TMP_ChatOutput_5(),
	ChatController_t49D7A1D868EED265CD37C4469FAFCF44235384FB::get_offset_of_ChatScrollbar_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5224 = { sizeof (EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5224[3] = 
{
	EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73::get_offset_of_RotationSpeeds_4(),
	EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73::get_offset_of_m_textMeshPro_5(),
	EnvMapAnimator_t32AABBB2CDB4C83E2DE3B1387090D0D7C6B9EF73::get_offset_of_m_material_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5225 = { sizeof (U3CStartU3Ed__4_t4F04AC75A036699A1B76C790F7523FEDB4A4569C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5225[4] = 
{
	U3CStartU3Ed__4_t4F04AC75A036699A1B76C790F7523FEDB4A4569C::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__4_t4F04AC75A036699A1B76C790F7523FEDB4A4569C::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__4_t4F04AC75A036699A1B76C790F7523FEDB4A4569C::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__4_t4F04AC75A036699A1B76C790F7523FEDB4A4569C::get_offset_of_U3CmatrixU3E5__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5226 = { sizeof (ElevationDataController_t83856E53D5F85C91D3B681D07D8BAB6433A0FB8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5226[2] = 
{
	ElevationDataController_t83856E53D5F85C91D3B681D07D8BAB6433A0FB8A::get_offset_of_projectorData_4(),
	ElevationDataController_t83856E53D5F85C91D3B681D07D8BAB6433A0FB8A::get_offset_of_projectorElevationDataConfigId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5227 = { sizeof (APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C), -1, sizeof(APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5227[6] = 
{
	APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C_StaticFields::get_offset_of_APIElevationDataLoaded_4(),
	APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C::get_offset_of_GetProjecctorDataURL_5(),
	APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C::get_offset_of_apiRequest_6(),
	APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C::get_offset_of_PingAPIForElevationDataCO_7(),
	APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C::get_offset_of_checkForNewData_8(),
	APIElevationDataController_t90F9B07D67904040CF36F73280C8FD926BBCE49C::get_offset_of_elevationTransferData_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5228 = { sizeof (U3CPingAPIForElevationDataU3Ed__10_tCF5C602CD51E83EAF07398096A056A0BA00AB0F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5228[3] = 
{
	U3CPingAPIForElevationDataU3Ed__10_tCF5C602CD51E83EAF07398096A056A0BA00AB0F1::get_offset_of_U3CU3E1__state_0(),
	U3CPingAPIForElevationDataU3Ed__10_tCF5C602CD51E83EAF07398096A056A0BA00AB0F1::get_offset_of_U3CU3E2__current_1(),
	U3CPingAPIForElevationDataU3Ed__10_tCF5C602CD51E83EAF07398096A056A0BA00AB0F1::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5229 = { sizeof (U3CElevationDataRequestU3Ed__12_tEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5229[5] = 
{
	U3CElevationDataRequestU3Ed__12_tEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4::get_offset_of_U3CU3E1__state_0(),
	U3CElevationDataRequestU3Ed__12_tEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4::get_offset_of_U3CU3E2__current_1(),
	U3CElevationDataRequestU3Ed__12_tEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4::get_offset_of_U3CU3E4__this_2(),
	U3CElevationDataRequestU3Ed__12_tEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4::get_offset_of_U3CU3Es__1_3(),
	U3CElevationDataRequestU3Ed__12_tEB09E2EB66D27B2C07B3DCB570456D9C68D5E3A4::get_offset_of_U3CprojectorDataU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5230 = { sizeof (APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98), -1, sizeof(APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5230[10] = 
{
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98_StaticFields::get_offset_of_ProjectoDataLoaded_4(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98_StaticFields::get_offset_of_ProjectoDataNeedsReload_5(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_GetProjecctorDataURL_6(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_apiRequest_7(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_projectorData_8(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_projectorController_9(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_courseManager_10(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_checkForNewData_11(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_masterCanvas_12(),
	APIProjectorDataController_t992753079E529F3824A8D7D6E56F345B34DCDA98::get_offset_of_PingAPIForNewPuttDataCO_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5231 = { sizeof (U3CPingAPIForNewPuttDataU3Ed__16_t1BFFD75823E5055966D106156800C1B8A7F2229B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5231[3] = 
{
	U3CPingAPIForNewPuttDataU3Ed__16_t1BFFD75823E5055966D106156800C1B8A7F2229B::get_offset_of_U3CU3E1__state_0(),
	U3CPingAPIForNewPuttDataU3Ed__16_t1BFFD75823E5055966D106156800C1B8A7F2229B::get_offset_of_U3CU3E2__current_1(),
	U3CPingAPIForNewPuttDataU3Ed__16_t1BFFD75823E5055966D106156800C1B8A7F2229B::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5232 = { sizeof (U3CProjectorDataRequestU3Ed__18_t3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5232[4] = 
{
	U3CProjectorDataRequestU3Ed__18_t3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853::get_offset_of_U3CU3E1__state_0(),
	U3CProjectorDataRequestU3Ed__18_t3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853::get_offset_of_U3CU3E2__current_1(),
	U3CProjectorDataRequestU3Ed__18_t3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853::get_offset_of_U3CU3E4__this_2(),
	U3CProjectorDataRequestU3Ed__18_t3846BA4EAB29CC0AEA299D6CD6AEC27B5372C853::get_offset_of_U3CU3Es__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5233 = { sizeof (APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5233[8] = 
{
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_apiBaseURL_4(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_permanentToken_5(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_preloader_6(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_apiRequest_7(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_postHeaderDict_8(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_courseManager_9(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_puttInteractivePrefab_10(),
	APIPuttController_tF6ED9B38D1759CD3F030A0CE76B0B0BE7DC66392::get_offset_of_puttContainer_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5234 = { sizeof (U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5234[7] = 
{
	U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9::get_offset_of_U3CU3E4__this_2(),
	U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9::get_offset_of_U3CU3Es__1_3(),
	U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9::get_offset_of_U3CputtVariationU3E5__2_4(),
	U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9::get_offset_of_U3CxballU3E5__3_5(),
	U3CWaitForGetPuttU3Ed__12_tE11B9B206ABDAD37D74CB44C15AE7AC163BF45F9::get_offset_of_U3CzballU3E5__4_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5235 = { sizeof (AppleTVRemoteController_t8891BD4501BE3E080090472A39EBD50DCA5BB54E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5235[4] = 
{
	AppleTVRemoteController_t8891BD4501BE3E080090472A39EBD50DCA5BB54E::get_offset_of_playerId_4(),
	AppleTVRemoteController_t8891BD4501BE3E080090472A39EBD50DCA5BB54E::get_offset_of_player_5(),
	AppleTVRemoteController_t8891BD4501BE3E080090472A39EBD50DCA5BB54E::get_offset_of_selected_6(),
	AppleTVRemoteController_t8891BD4501BE3E080090472A39EBD50DCA5BB54E::get_offset_of_showDashboard_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5236 = { sizeof (ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983), -1, sizeof(ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5236[13] = 
{
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983_StaticFields::get_offset_of_ArrowObjectsCreated_4(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowPrefab_5(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowList_6(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowColor_7(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowPrefColor_8(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowContGO_9(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_isArrowAnimating_10(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowLevel1List_11(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowLevelsList_12(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowsAllLevelsList_13(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowCont_14(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_maxArrowSpeed_15(),
	ArrowManager_tBA7E066B687896A443C971DD1DA156F7ED4FB983::get_offset_of_arrowCollectedList_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5237 = { sizeof (ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5237[6] = 
{
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_speed_4(),
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_isDoneFading_5(),
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_isDoneAnimating_6(),
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_slope_7(),
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_slopeColor_8(),
	ArrowPrefabController_t71199822C519DC78B745CA1375ECBA96C329C5C5::get_offset_of_hex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5238 = { sizeof (ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5238[4] = 
{
	ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278::get_offset_of_contourLineList_4(),
	ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278::get_offset_of_contourCont_5(),
	ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278::get_offset_of_lineMat_6(),
	ContourManager_tD5BAD0C4F239F537CB6335A335723DA5D397D278::get_offset_of_contourPaths_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5239 = { sizeof (AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC), -1, sizeof(AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5239[3] = 
{
	AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC_StaticFields::get_offset_of_inst_4(),
	AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC::get_offset_of_popupName_5(),
	AlertPopupController_tF1448A8940A2AC43C3D2C0A866B07C35AE4B05DC::get_offset_of_alertPopup_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5240 = { sizeof (APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5240[5] = 
{
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05::get_offset_of_PostLoginURL_4(),
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05::get_offset_of_apiRequest_5(),
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05::get_offset_of_userNameForTesting_6(),
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05::get_offset_of_passwordForTesting_7(),
	APILoginController_t9C537B953844B6722CF1B3F15F834C717904DC05::get_offset_of_testingLOGIN_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5241 = { sizeof (U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F), -1, sizeof(U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5241[2] = 
{
	U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t388472ACBBACB811A7689249633ADE5FF21C0B9F_StaticFields::get_offset_of_U3CU3E9__5_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5242 = { sizeof (U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5242[6] = 
{
	U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B::get_offset_of_U3CU3E1__state_0(),
	U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B::get_offset_of_U3CU3E2__current_1(),
	U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B::get_offset_of__userSubmitData_2(),
	U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B::get_offset_of_U3CU3E4__this_3(),
	U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B::get_offset_of_U3CU3Es__1_4(),
	U3CLoginRequestU3Ed__7_tDB055AD3C80AB8544320D3196905113F84F45A7B::get_offset_of_U3CuserU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5243 = { sizeof (LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692), -1, sizeof(LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5243[5] = 
{
	LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692_StaticFields::get_offset_of_UserDataSet_4(),
	LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692::get_offset_of_userNameIF_5(),
	LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692::get_offset_of_passwordIF_6(),
	LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692::get_offset_of_submitBtn_7(),
	LoginValidateCredentials_t7BACED90D9DBFF5BC46589062D86BA9BA6A63692::get_offset_of_user_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5244 = { sizeof (CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5244[14] = 
{
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_region_4(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_course_5(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_courseGreens_6(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_green_7(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_currRegion_8(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_currCourse_9(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_currGreen_10(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_greenModelLoaded_11(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_arrowsData_12(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_contourData_13(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_selectedRegionCode_14(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_selectedCourseID_15(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_selectedGroupID_16(),
	CourseManager_tB8C12618ECDBFF60FF036B6225B60CD5EDE8AFD8::get_offset_of_selectedGreenID_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5245 = { sizeof (DashboardViewController_tC062AACB63F690BF7B0AD65AA38609BCBA698670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5245[4] = 
{
	DashboardViewController_tC062AACB63F690BF7B0AD65AA38609BCBA698670::get_offset_of_btnSelected_4(),
	DashboardViewController_tC062AACB63F690BF7B0AD65AA38609BCBA698670::get_offset_of_userNameTxt_5(),
	DashboardViewController_tC062AACB63F690BF7B0AD65AA38609BCBA698670::get_offset_of_locationTxt_6(),
	DashboardViewController_tC062AACB63F690BF7B0AD65AA38609BCBA698670::get_offset_of_showDashboard_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5246 = { sizeof (ExtensionMethods_t9C246E3C94E8653839FE7FBBE6549E8030A5B14B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5247 = { sizeof (GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5248 = { sizeof (HoleContainerController_tE08BC9A154E37BB8F9906C1663B669FD94BD22AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5248[3] = 
{
	HoleContainerController_tE08BC9A154E37BB8F9906C1663B669FD94BD22AB::get_offset_of_projectorController_4(),
	HoleContainerController_tE08BC9A154E37BB8F9906C1663B669FD94BD22AB::get_offset_of_holeContainer_5(),
	HoleContainerController_tE08BC9A154E37BB8F9906C1663B669FD94BD22AB::get_offset_of_holeRect_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5249 = { sizeof (LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5249[9] = 
{
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_lineMaterial_4(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_lineToHoleGO_5(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_indexOfAnimationPoint_6(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_animatedLineToHoleList_7(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_puttPointsFull_8(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_animateLineToHole_9(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_lineRenderer_10(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_simulationManager_11(),
	LineToHole_t605CC45F1100D29849A8AD5A49793FCD292403B8::get_offset_of_projectorController_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5250 = { sizeof (LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5250[7] = 
{
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_pathToTargetMarkerList_4(),
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_startLinePosition_5(),
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_endLinePosition_6(),
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_targetLineSegmentPrefab_7(),
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_targetPrefabInst_8(),
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_targetPrefab_9(),
	LineToTarget_t8E738E8246127456BF830AA8D789CC666488491E::get_offset_of_puttSimulation_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5251 = { sizeof (ElevationTransferData_t8EC1998F633665D38764D8DFF602677469984883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5251[2] = 
{
	ElevationTransferData_t8EC1998F633665D38764D8DFF602677469984883::get_offset_of_arrowCollectedList_0(),
	ElevationTransferData_t8EC1998F633665D38764D8DFF602677469984883::get_offset_of_contourCollectedList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5252 = { sizeof (ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5252[2] = 
{
	ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD::get_offset_of_jackData_0(),
	ProjectorStatusData_t62B1A70B654ACE58F01F2B11E82EADB5F3E7E2AD::get_offset_of_greenActivity_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5253 = { sizeof (UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5253[2] = 
{
	UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF::get_offset_of_userName_0(),
	UserSubmitData_t87D189F6E674E0679D39FE190DB49929F41D48BF::get_offset_of_password_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5254 = { sizeof (ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5254[3] = 
{
	ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6::get_offset_of_projectorStatusData_4(),
	ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6::get_offset_of_greenActivity_5(),
	ProjectorController_t6428849BF28D605FD8DD690DE78D8B5CA7A1B3D6::get_offset_of_configurationId_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5255 = { sizeof (PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5255[7] = 
{
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_puttRhythmPrefab_4(),
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_puttInteractive_5(),
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_puttStanceBoxInst_6(),
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_puttRhythmIcon_7(),
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_stanceRightHandedLine_8(),
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_stanceLeftHandedLine_9(),
	PuttAssistance_t800C93BC51C0BC9E14594D4F93FC855297A7E431::get_offset_of_directionOfHoleLocation_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5256 = { sizeof (PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5256[10] = 
{
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of__puttVariation_4(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_ballDragInput_5(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_holeDragInput_6(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_ballLocation_7(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_holeLocation_8(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_isActive_9(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_puttSimulationsList_10(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_puttSimulationPrefab_11(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_invalidPuttMaterial_12(),
	PuttInteractive_tED9BF5B0E0860C6574B4368FB70D0CF56C8EED60::get_offset_of_validPuttMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5257 = { sizeof (PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5257[5] = 
{
	PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41::get_offset_of_projectorController_4(),
	PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41::get_offset_of_holePrefab_5(),
	PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41::get_offset_of_holeInst_6(),
	PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41::get_offset_of_puttInteractivePrefab_7(),
	PuttManager_tFA3CD3761FA6A181F1D3B0C3DC8A1D40AD3FBA41::get_offset_of_puttInteractiveList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5258 = { sizeof (PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5258[12] = 
{
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_puttRythmGO_4(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_tween_5(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_target_6(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_targetPos_7(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_origin_8(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_targeBehindBallCube_9(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_targetHoleCube_10(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_speed_11(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_puttRythmLineVector_12(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_stanceRightHandedLine_13(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_stanceLeftHandedLine_14(),
	PuttRythm_t4B5FCBCBB154A05D704376EF35E9B5342BBAFBC0::get_offset_of_direction_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5259 = { sizeof (Direction_tC9AAD2ECFDDAE56D03AFC52A63D632079A965845)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5259[3] = 
{
	Direction_tC9AAD2ECFDDAE56D03AFC52A63D632079A965845::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5260 = { sizeof (PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5260[3] = 
{
	PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD::get_offset_of_putt_4(),
	PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD::get_offset_of_puttPointsFull_5(),
	PuttSimulation_t57B97DFD11598230F458B4832CB409B16F7A53AD::get_offset_of_puttPointsHalved_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5261 = { sizeof (RoboGreenSettings_tAF3E81EEEC76595CB4A63EC2E4AD8D99297B7773), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5262 = { sizeof (SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5262[2] = 
{
	SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9::get_offset_of_loopPutt_4(),
	SimulationManager_t1D7922488B2B05F2C32B9A52B64F2445555C17B9::get_offset_of_puttLineSpeed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5263 = { sizeof (PuttLineSpeed_t65D6B12615B69CD468407A6B02DFE4360724B3DD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5263[4] = 
{
	PuttLineSpeed_t65D6B12615B69CD468407A6B02DFE4360724B3DD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5264 = { sizeof (UserController_tFD917E07B535D607AFD7056993FB736FD366E600), -1, sizeof(UserController_tFD917E07B535D607AFD7056993FB736FD366E600_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5264[2] = 
{
	UserController_tFD917E07B535D607AFD7056993FB736FD366E600_StaticFields::get_offset_of_user_4(),
	UserController_tFD917E07B535D607AFD7056993FB736FD366E600::get_offset_of_login_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5265 = { sizeof (LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5265[1] = 
{
	LoginViewController_t888A1D2D4AD363F23E8CD99AB0EDC73916209833::get_offset_of_loginValidateCredentials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5266 = { sizeof (SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5266[3] = 
{
	SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02::get_offset_of_greenId_0(),
	SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02::get_offset_of_rotation_1(),
	SendPuttForm_t4B1C3142A12A2243174E9F84EC7DFEB2F7723E02::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5267 = { sizeof (SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5267[5] = 
{
	SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE::get_offset_of_stimp_0(),
	SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE::get_offset_of_firmness_1(),
	SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE::get_offset_of_isMetric_2(),
	SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE::get_offset_of_holeLocation_3(),
	SendPuttFormData_t2CC7FC1A38AD958CE8B9259E2DB9A339C5AE96BE::get_offset_of_ballLocation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5268 = { sizeof (HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5268[2] = 
{
	HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64::get_offset_of_x_0(),
	HoleLocation_t6CB13532CE5B20E8F0B636B3560C912B92FB8C64::get_offset_of_y_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5269 = { sizeof (BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5269[2] = 
{
	BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42::get_offset_of_x_0(),
	BallLocation_t778FBC13F63FD3C5816B575D57CD5FE28B50CD42::get_offset_of_y_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5270 = { sizeof (GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5270[3] = 
{
	GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A::get_offset_of_arrows1_0(),
	GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A::get_offset_of_arrows2_1(),
	GreenArrow_t65F2D62E92115DC1945EAB666D741EB94A8BF86A::get_offset_of_arrows3_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5271 = { sizeof (Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5271[5] = 
{
	Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520::get_offset_of_point_0(),
	Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520::get_offset_of_slope_1(),
	Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520::get_offset_of_rotation_2(),
	Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520::get_offset_of_level_3(),
	Arrow_t143EA52C1A6FE56EE828767447D0B30EAAEFF520::get_offset_of_color_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5272 = { sizeof (ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5272[5] = 
{
	ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E::get_offset_of_r_0(),
	ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E::get_offset_of_g_1(),
	ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E::get_offset_of_b_2(),
	ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E::get_offset_of_a_3(),
	ArrowColor_t450822419497E236F3DAF47E7CC8FFCE3DBC0F7E::get_offset_of_hex_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5273 = { sizeof (ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5273[3] = 
{
	ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41::get_offset_of_x_0(),
	ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41::get_offset_of_y_1(),
	ArrowLocation_t9D29EA9D14075CD21D7C77E9821E5044E1418B41::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5274 = { sizeof (Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5274[1] = 
{
	Contour_t3A197E26E2CB50405BBE91F99D606941420D7B42::get_offset_of_contourPaths_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5275 = { sizeof (ContourPath_t1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5275[1] = 
{
	ContourPath_t1150C6B9A640854B1BA023A4E04AEC5BB98C8DD6::get_offset_of_path_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5276 = { sizeof (ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5276[4] = 
{
	ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6::get_offset_of_x_0(),
	ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6::get_offset_of_y_1(),
	ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6::get_offset_of_z_2(),
	ContourPathPoints_t61D9DB02F98FF719B11F2A3D66302CB6E44515F6::get_offset_of_index_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5277 = { sizeof (Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5277[1] = 
{
	Course_tDA11180264FF7088F2B9854E1D4DBE0C4AC66E1A::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5278 = { sizeof (CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5278[13] = 
{
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_courseId_0(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_sourceId_1(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_name_2(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_address_3(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_city_4(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_region_5(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_postalCode_6(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_countryCode_7(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_profileImage_8(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_latitude_9(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_longitude_10(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_isActive_11(),
	CourseData_t1F181044BC7EDA690F8E4E0C71C4F392C60A10DF::get_offset_of_settings_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5279 = { sizeof (CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5279[8] = 
{
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_isHoleLocationSoftware_0(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_holoVersion_1(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_isLockedFromSale_2(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_customLockMessage_3(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_lastPinsheetTemplateId_4(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_isCourseDataLocked_5(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_isGreenDataLocked_6(),
	CourseSettings_t5B916EF8B384B1B741AD022E2C29A533ADBE7352::get_offset_of_isGpsDataLocked_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5280 = { sizeof (CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5280[3] = 
{
	CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D::get_offset_of_courseId_0(),
	CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D::get_offset_of_name_1(),
	CourseGreens_tA5A23B34F0E3B9114AB312F86C40E6A63492CE8D::get_offset_of_groups_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5281 = { sizeof (GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5281[6] = 
{
	GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97::get_offset_of_groupId_0(),
	GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97::get_offset_of_name_1(),
	GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97::get_offset_of_sortOrder_2(),
	GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97::get_offset_of_stimp_3(),
	GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97::get_offset_of_greens_4(),
	GreenGroup_tC6C5A79BEB814B131AA6A0A209437F2DEAA19E97::get_offset_of_greenGroupType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5282 = { sizeof (GreenGroupType_tD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5282[3] = 
{
	GreenGroupType_tD6BCAD14E5AFA54A2DB97CB57D1050C490FFAB42::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5283 = { sizeof (Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5283[10] = 
{
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_holeLocations_0(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_name_1(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_rotation_2(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_greenId_3(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_groupId_4(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_number_5(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_latitude_6(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_longitude_7(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_trueNorth_8(),
	Green_tF4FE5A7F6576E973AD9C1A0DD5C924011EBD37B6::get_offset_of_DXFFile_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5284 = { sizeof (HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5284[5] = 
{
	HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB::get_offset_of_id_0(),
	HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB::get_offset_of_greenId_1(),
	HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB::get_offset_of_rotation_2(),
	HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB::get_offset_of_x_3(),
	HoleLocation_t135B1EC1A08FDDCCA91211A88ED834758DE5F3BB::get_offset_of_y_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5285 = { sizeof (GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5285[9] = 
{
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_greenId_0(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_holeXPos_1(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_holeYPos_2(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_isPlaying_3(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_isLooping_4(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_speed_5(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_greenCameraSettings_6(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_holeContainerSettings_7(),
	GreenActivity_t842FBD6B515C371768BEA26F6D83FCD5CEDFAB7B::get_offset_of_greenPutts_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5286 = { sizeof (GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5286[2] = 
{
	GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF::get_offset_of_ballXPos_0(),
	GreenPutt_t85D3648F7CB55611A8F377FFF5FAA424C6591BFF::get_offset_of_ballYPos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5287 = { sizeof (GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5287[5] = 
{
	GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA::get_offset_of_xPos_0(),
	GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA::get_offset_of_yPos_1(),
	GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA::get_offset_of_zPos_2(),
	GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA::get_offset_of_size_3(),
	GreenCameraSettings_t2D2800BF31FABE94B38A6013C50A6ECECFF3BEEA::get_offset_of_yRot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5288 = { sizeof (HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5288[4] = 
{
	HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175::get_offset_of_xPos_0(),
	HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175::get_offset_of_yPos_1(),
	HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175::get_offset_of_zPos_2(),
	HoleContainerSettings_tE3AE6751FC41C4A17B6133B3AF19C2F7D8E8A175::get_offset_of_holeRectYRotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5289 = { sizeof (JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5289[3] = 
{
	JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1::get_offset_of_jacks_0(),
	JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1::get_offset_of_jackId_1(),
	JackData_t12425D7D49BAFF551ACC09A9DE16CA779F3740D1::get_offset_of_location_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5290 = { sizeof (Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5290[2] = 
{
	Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC::get_offset_of_index_0(),
	Jack_t01FBC933A347E5DC16285B8B823E26570C8692DC::get_offset_of_elevation_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5291 = { sizeof (ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5291[1] = 
{
	ProjectorData_t578B3935B7C9D6D25C4EFF6EC6610E2C27FC8F88::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5292 = { sizeof (Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5292[4] = 
{
	Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F::get_offset_of_puttTrainerId_0(),
	Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F::get_offset_of_configurationId_1(),
	Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F::get_offset_of_createdOn_2(),
	Configurator_t63CBBFC75C3BD6E930A9CDED1AB675AF1D77C59F::get_offset_of_configurationData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5293 = { sizeof (PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5293[2] = 
{
	PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31::get_offset_of_value_0(),
	PuttVariation_t619DA0827B8D99CA80136275A4EADE66A1097A31::get_offset_of_puttVariationID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5294 = { sizeof (Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5294[13] = 
{
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_puttID_0(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_isValidPutt_1(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_message_2(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_ball_3(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_hole_4(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_target_5(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_ballToHoleDistance_6(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_puttingDistance_7(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_correctionAngle_8(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_elevationChange_9(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_speedFactor_10(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_points_11(),
	Putt_t747ECA1F29D6DFDCA8CE1CF94D1DE19988449A49::get_offset_of_instructions_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5295 = { sizeof (Ball_t094765B02879225F8C595564E40A823B70AEE45E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5295[3] = 
{
	Ball_t094765B02879225F8C595564E40A823B70AEE45E::get_offset_of_x_0(),
	Ball_t094765B02879225F8C595564E40A823B70AEE45E::get_offset_of_y_1(),
	Ball_t094765B02879225F8C595564E40A823B70AEE45E::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5296 = { sizeof (Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5296[3] = 
{
	Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB::get_offset_of_x_0(),
	Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB::get_offset_of_y_1(),
	Hole_t080CFDB711495EFE21793D6BF4AEB1D9DB9569AB::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5297 = { sizeof (Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5297[3] = 
{
	Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139::get_offset_of_x_0(),
	Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139::get_offset_of_y_1(),
	Target_t7CBFB33F47C01EFEF533D160596EB06BB2A9B139::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5298 = { sizeof (Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5298[3] = 
{
	Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2::get_offset_of_speed_0(),
	Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2::get_offset_of_location_1(),
	Point_tD538CCF5709B7B64D27871B2B0EC83FEC97E93C2::get_offset_of_slope_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5299 = { sizeof (Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5299[3] = 
{
	Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6::get_offset_of_x_0(),
	Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6::get_offset_of_y_1(),
	Location_t54F881D6DBE8E237E8697450BB8C10F1BBE668B6::get_offset_of_z_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
