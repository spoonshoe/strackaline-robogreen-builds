﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Rewired.ActionElementMap
struct ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7;
// Rewired.Controller
struct Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671;
// Rewired.Controller/Axis
struct Axis_t1E0EEBF040B05CB6932311D5F7233C785BB6BEC2;
// Rewired.ControllerMap
struct ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB;
// Rewired.Data.ControllerDataFiles
struct ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5;
// Rewired.Data.UserData
struct UserData_t23CF13BC0883AADA4A3457583033292E49A77D48;
// Rewired.Data.UserDataStore_PlayerPrefs
struct UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5;
// Rewired.Data.UserDataStore_PlayerPrefs/ControllerAssignmentSaveInfo/JoystickInfo
struct JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B;
// Rewired.Data.UserDataStore_PlayerPrefs/ControllerAssignmentSaveInfo/JoystickInfo[]
struct JoystickInfoU5BU5D_tE4CC1CB6E1230C048D644CA8449E04563630E5E5;
// Rewired.Data.UserDataStore_PlayerPrefs/ControllerAssignmentSaveInfo/PlayerInfo[]
struct PlayerInfoU5BU5D_t87BCACC21E4D7575BE12251D9F1B2733D50A883F;
// Rewired.IControllerTemplateElement[]
struct IControllerTemplateElementU5BU5D_t1660B9BD48816BC80A9C52B74BFCF80922F1C060;
// Rewired.InputAction
struct InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA;
// Rewired.InputBehavior
struct InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B;
// Rewired.InputManager
struct InputManager_t42964CC63A93EEFEC8C958ED5DF1166DB01B09CA;
// Rewired.InputManager_Base
struct InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701;
// Rewired.Integration.UnityUI.PlayerPointerEventData
struct PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72;
// Rewired.Integration.UnityUI.RewiredPointerInputModule/MouseButtonEventData
struct MouseButtonEventData_tCDAEE9261DB005C8EBAA9EB979891497CCC71F6C;
// Rewired.Integration.UnityUI.RewiredPointerInputModule/MouseState
struct MouseState_tC2B3A40B96175F6EE1FEBD1693F38B717BABFD5C;
// Rewired.Integration.UnityUI.RewiredPointerInputModule/UnityInputSource
struct UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128;
// Rewired.Integration.UnityUI.RewiredStandaloneInputModule
struct RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7;
// Rewired.Joystick
struct Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39;
// Rewired.Player
struct Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F;
// Rewired.UI.ControlMapper.ButtonInfo[]
struct ButtonInfoU5BU5D_tC6815BFF2944D0E87BC214FC1089791ECC3ED47A;
// Rewired.UI.ControlMapper.CalibrationWindow
struct CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6;
// Rewired.UI.ControlMapper.CanvasScalerExt
struct CanvasScalerExt_t7474B4D2A98F6C8EB3964E748B7F093961CB7362;
// Rewired.UI.ControlMapper.CanvasScalerFitter/BreakPoint[]
struct BreakPointU5BU5D_t8672C763F902403BFE92AD4848B64288DD7E04FF;
// Rewired.UI.ControlMapper.ControlMapper
struct ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7;
// Rewired.UI.ControlMapper.ControlMapper/AxisCalibrator
struct AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237;
// Rewired.UI.ControlMapper.ControlMapper/GUIButton
struct GUIButton_tF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C;
// Rewired.UI.ControlMapper.ControlMapper/GUIInputField
struct GUIInputField_tF982ED4AB954FC21D0AE7F35EAB4C8234BDF4F20;
// Rewired.UI.ControlMapper.ControlMapper/GUILabel
struct GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8;
// Rewired.UI.ControlMapper.ControlMapper/GUIToggle
struct GUIToggle_tD90790241D2ED7478964399C10A03986E91C88F9;
// Rewired.UI.ControlMapper.ControlMapper/IndexedDictionary`2<System.Int32,Rewired.UI.ControlMapper.ControlMapper/GUIInputField>
struct IndexedDictionary_2_tD7799A910D100794D2BE1750660D330592FA52CD;
// Rewired.UI.ControlMapper.ControlMapper/IndexedDictionary`2<System.Int32,Rewired.UI.ControlMapper.ControlMapper/InputGridEntryList/ActionCategoryEntry>
struct IndexedDictionary_2_tEA0DBCB1C67F99E45E3AE022DAFD95EE0B098A28;
// Rewired.UI.ControlMapper.ControlMapper/IndexedDictionary`2<System.Int32,Rewired.UI.ControlMapper.ControlMapper/InputGridEntryList/FieldSet>
struct IndexedDictionary_2_t5E488B599074F5939A9A442837164D3B0AB374C2;
// Rewired.UI.ControlMapper.ControlMapper/IndexedDictionary`2<System.Int32,Rewired.UI.ControlMapper.ControlMapper/InputGridEntryList/MapCategoryEntry>
struct IndexedDictionary_2_t166D8745D1FC0F5B53B131E55CCFDFD7FEE548D7;
// Rewired.UI.ControlMapper.ControlMapper/InputActionSet
struct InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B;
// Rewired.UI.ControlMapper.ControlMapper/InputBehaviorSettings[]
struct InputBehaviorSettingsU5BU5D_tA249DB027445C51288AA79DD04BAA298DF37C949;
// Rewired.UI.ControlMapper.ControlMapper/InputGrid
struct InputGrid_t279399655219F94B8033968DF46749593CF22E63;
// Rewired.UI.ControlMapper.ControlMapper/InputGridEntryList
struct InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF;
// Rewired.UI.ControlMapper.ControlMapper/InputGridEntryList/MapCategoryEntry
struct MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906;
// Rewired.UI.ControlMapper.ControlMapper/InputMapping
struct InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A;
// Rewired.UI.ControlMapper.ControlMapper/MappingSet[]
struct MappingSetU5BU5D_t4DD35740FE8DAD1318EA6164A74EF2DDCA5B810A;
// Rewired.UI.ControlMapper.ControlMapper/Prefabs
struct Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A;
// Rewired.UI.ControlMapper.ControlMapper/References
struct References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163;
// Rewired.UI.ControlMapper.ControlMapper/WindowManager
struct WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4;
// Rewired.UI.ControlMapper.CustomButton
struct CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533;
// Rewired.UI.ControlMapper.InputFieldInfo
struct InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5;
// Rewired.UI.ControlMapper.LanguageData
struct LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B;
// Rewired.UI.ControlMapper.LanguageData/CustomEntry[]
struct CustomEntryU5BU5D_t40CEB5E7F2BCDECE988328E9E4875B2A8EE36E85;
// Rewired.UI.ControlMapper.ThemeSettings
struct ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF;
// Rewired.UI.ControlMapper.UIControlSet
struct UIControlSet_t0BE4C6F06B453187D70603E003EB07A252B5C083;
// Rewired.UI.ControlMapper.UIElementInfo
struct UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA;
// Rewired.UI.ControlMapper.UIGroup
struct UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46;
// Rewired.UI.ControlMapper.Window
struct Window_t05E229BCE867863C1A33354186F3E7641707AAA5;
// Rewired.UI.ControlMapper.Window/Timer
struct Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D;
// Rewired.UI.IMouseInputSource
struct IMouseInputSource_t9E056942EA978C3EEB4B6286DC2F277B4FB91351;
// Rewired.UI.ITouchInputSource
struct ITouchInputSource_tB9241E387F287F1232DE39EFA4B492B1CA0E5F34;
// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,Rewired.IControllerTemplateElement>
struct ADictionary_2_t68A8B0123876EA082FBDA6132BFFCD5DB743965D;
// Rewired.Utils.Classes.Data.ADictionary`2<System.String,Rewired.IControllerTemplateElement>
struct ADictionary_2_tD6BAFD11BFA92734BD04AC9CA5B5B7B493FAB1E7;
// Strackaline.MemberDetail
struct MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B;
// Strackaline.RegionData[]
struct RegionDataU5BU5D_t6B7197A1556950E72F70CA6FBE5821D8B8716119;
// Strackaline.Roles[]
struct RolesU5BU5D_t8488CDD55C543C867E336D439B8B2EABC84E077E;
// Strackaline.UserCourse[]
struct UserCourseU5BU5D_t7ED59DB157B099FEDE78B70F593E0F2DE12D39EE;
// Strackaline.UserPrinter[]
struct UserPrinterU5BU5D_tFA1FBB04FD3DF56366C2A05D605642AB3D1F9601;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Rewired.UI.ControlMapper.ButtonInfo>
struct Action_1_t1E32078CBBA151AEB509ACEF0FE466CDE0FA379C;
// System.Action`1<Rewired.UI.ControlMapper.InputFieldInfo>
struct Action_1_t7738608E1471A66AED779B33F76214E0494E0B0D;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<System.Int32>
struct Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735;
// System.Action`2<Rewired.UI.ControlMapper.ToggleInfo,System.Boolean>
struct Action_2_tCFB48941301BEC70B366F467446469E97B0B61C9;
// System.Action`2<System.Int32,Rewired.UI.ControlMapper.ButtonInfo>
struct Action_2_t1FA8C2434BE4B5143E2A6BBE9706302C61B7840C;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t7410D2490D6164955944AF42E135EB85B0551457;
// System.Action`2<System.UInt32,System.Boolean>
struct Action_2_t4FE7EF9326184201F0B18A3B64A3EDDAE7A50EF7;
// System.Action`3<System.Int32,System.Int32,System.Single>
struct Action_3_t3C1A9DB9F42AE7E5CF06D50EDFD2584132EE1BFF;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Data.Mapping.AxisCalibrationInfo>
struct Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03;
// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.UI.ControlMapper.InputBehaviorWindow/PropertyType>
struct Dictionary_2_t8C6A70CB8E2C9D90733F4EA648B407CA2CB72DBD;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Int32>>
struct Dictionary_2_t030E2D78EA98876F80F098D4E3C0AA3B18AC72EE;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Integration.UnityUI.PlayerPointerEventData>[]>
struct Dictionary_2_t7CE50FF82CD019CCD55558B77157FD9686A4CA0B;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo>
struct IEnumerator_1_tB5F8E3B1AA2E4BF2597F179C159DF6EFBC7DAE69;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t946CA250FDCDAF3493418F5354C7D62897FA0DE8;
// System.Collections.Generic.List`1<Rewired.Components.PlayerMouse>
struct List_1_t08E7DE11ECA859CD34DB357C00B14BE87AF37AE9;
// System.Collections.Generic.List`1<Rewired.Integration.UnityUI.RewiredPointerInputModule/ButtonState>
struct List_1_t426ED9CA95C514FB83B786ECD44A19712BBCD091;
// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.ControlMapper/GUIButton>
struct List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D;
// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.ControlMapper/GUIElement>
struct List_1_t9DD46FE95C9E0B2084FE46D957811DAD09B2B7DA;
// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.ControlMapper/InputGridEntryList/ActionEntry>
struct List_1_t1D7050D8FD8BB194442B96C5AAA4CAFBB2EE9611;
// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.InputBehaviorWindow/InputBehaviorInfo>
struct List_1_t31CA8E22671BC2966AA472F0A47F63DF3CEAE904;
// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.Window>
struct List_1_t36BA7DF9E75C719662F145AC61EA523A5EE33702;
// System.Collections.Generic.List`1<Rewired.UI.IMouseInputSource>
struct List_1_tA87F5F213628BE50512E7F7B610C2741B76D220F;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t64BA96BFC713F221050385E91C868CE455C245D6;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.UI.Button>
struct List_1_tE74BD634C2F383838544C1E81C81B7E136CE6BC5;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t48FDD3CF761FA5A5451E50F64C5529D419339F18;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.IControllerTemplateElement>
struct ReadOnlyCollection_1_tBD7D8D2E9C5F8B4B0C4FF78D2202E23AC1F7F65B;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;
// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t50D925F8E220FB47DA738411CEF9C57FF7E1DC43;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786;

struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC__DISPLAYCLASS73_0_T3BBDFC06998B23FE10ADE01C56EF996EFE2C1C63_H
#define U3CU3EC__DISPLAYCLASS73_0_T3BBDFC06998B23FE10ADE01C56EF996EFE2C1C63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserDataStore_PlayerPrefs_<>c__DisplayClass73_0
struct  U3CU3Ec__DisplayClass73_0_t3BBDFC06998B23FE10ADE01C56EF996EFE2C1C63  : public RuntimeObject
{
public:
	// Rewired.Joystick Rewired.Data.UserDataStore_PlayerPrefs_<>c__DisplayClass73_0::joystick
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___joystick_0;

public:
	inline static int32_t get_offset_of_joystick_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_t3BBDFC06998B23FE10ADE01C56EF996EFE2C1C63, ___joystick_0)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_joystick_0() const { return ___joystick_0; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_joystick_0() { return &___joystick_0; }
	inline void set_joystick_0(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___joystick_0 = value;
		Il2CppCodeGenWriteBarrier((&___joystick_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS73_0_T3BBDFC06998B23FE10ADE01C56EF996EFE2C1C63_H
#ifndef U3CU3EC__DISPLAYCLASS73_1_T8D81D1C492C6008D463C4CC24122A18BB8527FB8_H
#define U3CU3EC__DISPLAYCLASS73_1_T8D81D1C492C6008D463C4CC24122A18BB8527FB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserDataStore_PlayerPrefs_<>c__DisplayClass73_1
struct  U3CU3Ec__DisplayClass73_1_t8D81D1C492C6008D463C4CC24122A18BB8527FB8  : public RuntimeObject
{
public:
	// Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo_JoystickInfo Rewired.Data.UserDataStore_PlayerPrefs_<>c__DisplayClass73_1::joystickInfo
	JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B * ___joystickInfo_0;

public:
	inline static int32_t get_offset_of_joystickInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_1_t8D81D1C492C6008D463C4CC24122A18BB8527FB8, ___joystickInfo_0)); }
	inline JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B * get_joystickInfo_0() const { return ___joystickInfo_0; }
	inline JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B ** get_address_of_joystickInfo_0() { return &___joystickInfo_0; }
	inline void set_joystickInfo_0(JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B * value)
	{
		___joystickInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___joystickInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS73_1_T8D81D1C492C6008D463C4CC24122A18BB8527FB8_H
#ifndef U3CU3EC__DISPLAYCLASS73_2_TF3CBEE2F5D06F3E550BCC7800FCDA98D149C1B6F_H
#define U3CU3EC__DISPLAYCLASS73_2_TF3CBEE2F5D06F3E550BCC7800FCDA98D149C1B6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserDataStore_PlayerPrefs_<>c__DisplayClass73_2
struct  U3CU3Ec__DisplayClass73_2_tF3CBEE2F5D06F3E550BCC7800FCDA98D149C1B6F  : public RuntimeObject
{
public:
	// Rewired.Joystick Rewired.Data.UserDataStore_PlayerPrefs_<>c__DisplayClass73_2::match
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___match_0;

public:
	inline static int32_t get_offset_of_match_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_2_tF3CBEE2F5D06F3E550BCC7800FCDA98D149C1B6F, ___match_0)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_match_0() const { return ___match_0; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_match_0() { return &___match_0; }
	inline void set_match_0(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___match_0 = value;
		Il2CppCodeGenWriteBarrier((&___match_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS73_2_TF3CBEE2F5D06F3E550BCC7800FCDA98D149C1B6F_H
#ifndef U3CLOADJOYSTICKASSIGNMENTSDEFERREDU3ED__75_T5B42B566A1AF4CF8C40235945EFAE7441692EC22_H
#define U3CLOADJOYSTICKASSIGNMENTSDEFERREDU3ED__75_T5B42B566A1AF4CF8C40235945EFAE7441692EC22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserDataStore_PlayerPrefs_<LoadJoystickAssignmentsDeferred>d__75
struct  U3CLoadJoystickAssignmentsDeferredU3Ed__75_t5B42B566A1AF4CF8C40235945EFAE7441692EC22  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.UserDataStore_PlayerPrefs_<LoadJoystickAssignmentsDeferred>d__75::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Rewired.Data.UserDataStore_PlayerPrefs_<LoadJoystickAssignmentsDeferred>d__75::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Rewired.Data.UserDataStore_PlayerPrefs Rewired.Data.UserDataStore_PlayerPrefs_<LoadJoystickAssignmentsDeferred>d__75::<>4__this
	UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadJoystickAssignmentsDeferredU3Ed__75_t5B42B566A1AF4CF8C40235945EFAE7441692EC22, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadJoystickAssignmentsDeferredU3Ed__75_t5B42B566A1AF4CF8C40235945EFAE7441692EC22, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadJoystickAssignmentsDeferredU3Ed__75_t5B42B566A1AF4CF8C40235945EFAE7441692EC22, ___U3CU3E4__this_2)); }
	inline UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADJOYSTICKASSIGNMENTSDEFERREDU3ED__75_T5B42B566A1AF4CF8C40235945EFAE7441692EC22_H
#ifndef CONTROLLERASSIGNMENTSAVEINFO_T850E0C2E7A93E61299F9820AC1EBCF495396B04A_H
#define CONTROLLERASSIGNMENTSAVEINFO_T850E0C2E7A93E61299F9820AC1EBCF495396B04A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo
struct  ControllerAssignmentSaveInfo_t850E0C2E7A93E61299F9820AC1EBCF495396B04A  : public RuntimeObject
{
public:
	// Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo_PlayerInfo[] Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo::players
	PlayerInfoU5BU5D_t87BCACC21E4D7575BE12251D9F1B2733D50A883F* ___players_0;

public:
	inline static int32_t get_offset_of_players_0() { return static_cast<int32_t>(offsetof(ControllerAssignmentSaveInfo_t850E0C2E7A93E61299F9820AC1EBCF495396B04A, ___players_0)); }
	inline PlayerInfoU5BU5D_t87BCACC21E4D7575BE12251D9F1B2733D50A883F* get_players_0() const { return ___players_0; }
	inline PlayerInfoU5BU5D_t87BCACC21E4D7575BE12251D9F1B2733D50A883F** get_address_of_players_0() { return &___players_0; }
	inline void set_players_0(PlayerInfoU5BU5D_t87BCACC21E4D7575BE12251D9F1B2733D50A883F* value)
	{
		___players_0 = value;
		Il2CppCodeGenWriteBarrier((&___players_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERASSIGNMENTSAVEINFO_T850E0C2E7A93E61299F9820AC1EBCF495396B04A_H
#ifndef PLAYERINFO_TE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5_H
#define PLAYERINFO_TE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo_PlayerInfo
struct  PlayerInfo_tE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo_PlayerInfo::id
	int32_t ___id_0;
	// System.Boolean Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo_PlayerInfo::hasKeyboard
	bool ___hasKeyboard_1;
	// System.Boolean Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo_PlayerInfo::hasMouse
	bool ___hasMouse_2;
	// Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo_JoystickInfo[] Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo_PlayerInfo::joysticks
	JoystickInfoU5BU5D_tE4CC1CB6E1230C048D644CA8449E04563630E5E5* ___joysticks_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PlayerInfo_tE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_hasKeyboard_1() { return static_cast<int32_t>(offsetof(PlayerInfo_tE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5, ___hasKeyboard_1)); }
	inline bool get_hasKeyboard_1() const { return ___hasKeyboard_1; }
	inline bool* get_address_of_hasKeyboard_1() { return &___hasKeyboard_1; }
	inline void set_hasKeyboard_1(bool value)
	{
		___hasKeyboard_1 = value;
	}

	inline static int32_t get_offset_of_hasMouse_2() { return static_cast<int32_t>(offsetof(PlayerInfo_tE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5, ___hasMouse_2)); }
	inline bool get_hasMouse_2() const { return ___hasMouse_2; }
	inline bool* get_address_of_hasMouse_2() { return &___hasMouse_2; }
	inline void set_hasMouse_2(bool value)
	{
		___hasMouse_2 = value;
	}

	inline static int32_t get_offset_of_joysticks_3() { return static_cast<int32_t>(offsetof(PlayerInfo_tE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5, ___joysticks_3)); }
	inline JoystickInfoU5BU5D_tE4CC1CB6E1230C048D644CA8449E04563630E5E5* get_joysticks_3() const { return ___joysticks_3; }
	inline JoystickInfoU5BU5D_tE4CC1CB6E1230C048D644CA8449E04563630E5E5** get_address_of_joysticks_3() { return &___joysticks_3; }
	inline void set_joysticks_3(JoystickInfoU5BU5D_tE4CC1CB6E1230C048D644CA8449E04563630E5E5* value)
	{
		___joysticks_3 = value;
		Il2CppCodeGenWriteBarrier((&___joysticks_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERINFO_TE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5_H
#ifndef JOYSTICKASSIGNMENTHISTORYINFO_T6C9ADB9CEF1E3309D8C314C439D49F0513B802A4_H
#define JOYSTICKASSIGNMENTHISTORYINFO_T6C9ADB9CEF1E3309D8C314C439D49F0513B802A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserDataStore_PlayerPrefs_JoystickAssignmentHistoryInfo
struct  JoystickAssignmentHistoryInfo_t6C9ADB9CEF1E3309D8C314C439D49F0513B802A4  : public RuntimeObject
{
public:
	// Rewired.Joystick Rewired.Data.UserDataStore_PlayerPrefs_JoystickAssignmentHistoryInfo::joystick
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___joystick_0;
	// System.Int32 Rewired.Data.UserDataStore_PlayerPrefs_JoystickAssignmentHistoryInfo::oldJoystickId
	int32_t ___oldJoystickId_1;

public:
	inline static int32_t get_offset_of_joystick_0() { return static_cast<int32_t>(offsetof(JoystickAssignmentHistoryInfo_t6C9ADB9CEF1E3309D8C314C439D49F0513B802A4, ___joystick_0)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_joystick_0() const { return ___joystick_0; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_joystick_0() { return &___joystick_0; }
	inline void set_joystick_0(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___joystick_0 = value;
		Il2CppCodeGenWriteBarrier((&___joystick_0), value);
	}

	inline static int32_t get_offset_of_oldJoystickId_1() { return static_cast<int32_t>(offsetof(JoystickAssignmentHistoryInfo_t6C9ADB9CEF1E3309D8C314C439D49F0513B802A4, ___oldJoystickId_1)); }
	inline int32_t get_oldJoystickId_1() const { return ___oldJoystickId_1; }
	inline int32_t* get_address_of_oldJoystickId_1() { return &___oldJoystickId_1; }
	inline void set_oldJoystickId_1(int32_t value)
	{
		___oldJoystickId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKASSIGNMENTHISTORYINFO_T6C9ADB9CEF1E3309D8C314C439D49F0513B802A4_H
#ifndef BUTTONSTATE_T87909B61004D60C53EAA68984CA998CFEEFFC5DA_H
#define BUTTONSTATE_T87909B61004D60C53EAA68984CA998CFEEFFC5DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Integration.UnityUI.RewiredPointerInputModule_ButtonState
struct  ButtonState_t87909B61004D60C53EAA68984CA998CFEEFFC5DA  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Integration.UnityUI.RewiredPointerInputModule_ButtonState::m_Button
	int32_t ___m_Button_0;
	// Rewired.Integration.UnityUI.RewiredPointerInputModule_MouseButtonEventData Rewired.Integration.UnityUI.RewiredPointerInputModule_ButtonState::m_EventData
	MouseButtonEventData_tCDAEE9261DB005C8EBAA9EB979891497CCC71F6C * ___m_EventData_1;

public:
	inline static int32_t get_offset_of_m_Button_0() { return static_cast<int32_t>(offsetof(ButtonState_t87909B61004D60C53EAA68984CA998CFEEFFC5DA, ___m_Button_0)); }
	inline int32_t get_m_Button_0() const { return ___m_Button_0; }
	inline int32_t* get_address_of_m_Button_0() { return &___m_Button_0; }
	inline void set_m_Button_0(int32_t value)
	{
		___m_Button_0 = value;
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(ButtonState_t87909B61004D60C53EAA68984CA998CFEEFFC5DA, ___m_EventData_1)); }
	inline MouseButtonEventData_tCDAEE9261DB005C8EBAA9EB979891497CCC71F6C * get_m_EventData_1() const { return ___m_EventData_1; }
	inline MouseButtonEventData_tCDAEE9261DB005C8EBAA9EB979891497CCC71F6C ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(MouseButtonEventData_tCDAEE9261DB005C8EBAA9EB979891497CCC71F6C * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATE_T87909B61004D60C53EAA68984CA998CFEEFFC5DA_H
#ifndef MOUSESTATE_TC2B3A40B96175F6EE1FEBD1693F38B717BABFD5C_H
#define MOUSESTATE_TC2B3A40B96175F6EE1FEBD1693F38B717BABFD5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Integration.UnityUI.RewiredPointerInputModule_MouseState
struct  MouseState_tC2B3A40B96175F6EE1FEBD1693F38B717BABFD5C  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Rewired.Integration.UnityUI.RewiredPointerInputModule_ButtonState> Rewired.Integration.UnityUI.RewiredPointerInputModule_MouseState::m_TrackedButtons
	List_1_t426ED9CA95C514FB83B786ECD44A19712BBCD091 * ___m_TrackedButtons_0;

public:
	inline static int32_t get_offset_of_m_TrackedButtons_0() { return static_cast<int32_t>(offsetof(MouseState_tC2B3A40B96175F6EE1FEBD1693F38B717BABFD5C, ___m_TrackedButtons_0)); }
	inline List_1_t426ED9CA95C514FB83B786ECD44A19712BBCD091 * get_m_TrackedButtons_0() const { return ___m_TrackedButtons_0; }
	inline List_1_t426ED9CA95C514FB83B786ECD44A19712BBCD091 ** get_address_of_m_TrackedButtons_0() { return &___m_TrackedButtons_0; }
	inline void set_m_TrackedButtons_0(List_1_t426ED9CA95C514FB83B786ECD44A19712BBCD091 * value)
	{
		___m_TrackedButtons_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedButtons_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSESTATE_TC2B3A40B96175F6EE1FEBD1693F38B717BABFD5C_H
#ifndef PLAYERSETTING_T09C5D4DC5F57B8C6CEFCFDEF2A430FF4129C82BD_H
#define PLAYERSETTING_T09C5D4DC5F57B8C6CEFCFDEF2A430FF4129C82BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Integration.UnityUI.RewiredStandaloneInputModule_PlayerSetting
struct  PlayerSetting_t09C5D4DC5F57B8C6CEFCFDEF2A430FF4129C82BD  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Integration.UnityUI.RewiredStandaloneInputModule_PlayerSetting::playerId
	int32_t ___playerId_0;
	// System.Collections.Generic.List`1<Rewired.Components.PlayerMouse> Rewired.Integration.UnityUI.RewiredStandaloneInputModule_PlayerSetting::playerMice
	List_1_t08E7DE11ECA859CD34DB357C00B14BE87AF37AE9 * ___playerMice_1;

public:
	inline static int32_t get_offset_of_playerId_0() { return static_cast<int32_t>(offsetof(PlayerSetting_t09C5D4DC5F57B8C6CEFCFDEF2A430FF4129C82BD, ___playerId_0)); }
	inline int32_t get_playerId_0() const { return ___playerId_0; }
	inline int32_t* get_address_of_playerId_0() { return &___playerId_0; }
	inline void set_playerId_0(int32_t value)
	{
		___playerId_0 = value;
	}

	inline static int32_t get_offset_of_playerMice_1() { return static_cast<int32_t>(offsetof(PlayerSetting_t09C5D4DC5F57B8C6CEFCFDEF2A430FF4129C82BD, ___playerMice_1)); }
	inline List_1_t08E7DE11ECA859CD34DB357C00B14BE87AF37AE9 * get_playerMice_1() const { return ___playerMice_1; }
	inline List_1_t08E7DE11ECA859CD34DB357C00B14BE87AF37AE9 ** get_address_of_playerMice_1() { return &___playerMice_1; }
	inline void set_playerMice_1(List_1_t08E7DE11ECA859CD34DB357C00B14BE87AF37AE9 * value)
	{
		___playerMice_1 = value;
		Il2CppCodeGenWriteBarrier((&___playerMice_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSETTING_T09C5D4DC5F57B8C6CEFCFDEF2A430FF4129C82BD_H
#ifndef CONTROLLERTEMPLATEFACTORY_T80C1C4DF4B65C968B5BB8EA87F076206470DE9D1_H
#define CONTROLLERTEMPLATEFACTORY_T80C1C4DF4B65C968B5BB8EA87F076206470DE9D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Internal.ControllerTemplateFactory
struct  ControllerTemplateFactory_t80C1C4DF4B65C968B5BB8EA87F076206470DE9D1  : public RuntimeObject
{
public:

public:
};

struct ControllerTemplateFactory_t80C1C4DF4B65C968B5BB8EA87F076206470DE9D1_StaticFields
{
public:
	// System.Type[] Rewired.Internal.ControllerTemplateFactory::_defaultTemplateTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ____defaultTemplateTypes_0;
	// System.Type[] Rewired.Internal.ControllerTemplateFactory::_defaultTemplateInterfaceTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ____defaultTemplateInterfaceTypes_1;

public:
	inline static int32_t get_offset_of__defaultTemplateTypes_0() { return static_cast<int32_t>(offsetof(ControllerTemplateFactory_t80C1C4DF4B65C968B5BB8EA87F076206470DE9D1_StaticFields, ____defaultTemplateTypes_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get__defaultTemplateTypes_0() const { return ____defaultTemplateTypes_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of__defaultTemplateTypes_0() { return &____defaultTemplateTypes_0; }
	inline void set__defaultTemplateTypes_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		____defaultTemplateTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&____defaultTemplateTypes_0), value);
	}

	inline static int32_t get_offset_of__defaultTemplateInterfaceTypes_1() { return static_cast<int32_t>(offsetof(ControllerTemplateFactory_t80C1C4DF4B65C968B5BB8EA87F076206470DE9D1_StaticFields, ____defaultTemplateInterfaceTypes_1)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get__defaultTemplateInterfaceTypes_1() const { return ____defaultTemplateInterfaceTypes_1; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of__defaultTemplateInterfaceTypes_1() { return &____defaultTemplateInterfaceTypes_1; }
	inline void set__defaultTemplateInterfaceTypes_1(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		____defaultTemplateInterfaceTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&____defaultTemplateInterfaceTypes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATEFACTORY_T80C1C4DF4B65C968B5BB8EA87F076206470DE9D1_H
#ifndef U3CU3EC__DISPLAYCLASS41_0_T00E8CDDAF7F83AA87F029DA3DEC73126F5786F8F_H
#define U3CU3EC__DISPLAYCLASS41_0_T00E8CDDAF7F83AA87F029DA3DEC73126F5786F8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.CalibrationWindow_<>c__DisplayClass41_0
struct  U3CU3Ec__DisplayClass41_0_t00E8CDDAF7F83AA87F029DA3DEC73126F5786F8F  : public RuntimeObject
{
public:
	// System.Int32 Rewired.UI.ControlMapper.CalibrationWindow_<>c__DisplayClass41_0::index
	int32_t ___index_0;
	// UnityEngine.UI.Button Rewired.UI.ControlMapper.CalibrationWindow_<>c__DisplayClass41_0::button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___button_1;
	// Rewired.UI.ControlMapper.CalibrationWindow Rewired.UI.ControlMapper.CalibrationWindow_<>c__DisplayClass41_0::<>4__this
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_t00E8CDDAF7F83AA87F029DA3DEC73126F5786F8F, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_button_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_t00E8CDDAF7F83AA87F029DA3DEC73126F5786F8F, ___button_1)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_button_1() const { return ___button_1; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_button_1() { return &___button_1; }
	inline void set_button_1(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___button_1 = value;
		Il2CppCodeGenWriteBarrier((&___button_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_t00E8CDDAF7F83AA87F029DA3DEC73126F5786F8F, ___U3CU3E4__this_2)); }
	inline CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS41_0_T00E8CDDAF7F83AA87F029DA3DEC73126F5786F8F_H
#ifndef U3CU3EC__DISPLAYCLASS338_0_TC041F08935BE04F38DAFB200176A9F0EBA83CCF0_H
#define U3CU3EC__DISPLAYCLASS338_0_TC041F08935BE04F38DAFB200176A9F0EBA83CCF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass338_0
struct  U3CU3Ec__DisplayClass338_0_tC041F08935BE04F38DAFB200176A9F0EBA83CCF0  : public RuntimeObject
{
public:
	// Rewired.UI.ControlMapper.ControlMapper Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass338_0::<>4__this
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * ___U3CU3E4__this_0;
	// Rewired.UI.ControlMapper.Window Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass338_0::window
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * ___window_1;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass338_0::controllerId
	int32_t ___controllerId_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass338_0_tC041F08935BE04F38DAFB200176A9F0EBA83CCF0, ___U3CU3E4__this_0)); }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_window_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass338_0_tC041F08935BE04F38DAFB200176A9F0EBA83CCF0, ___window_1)); }
	inline Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * get_window_1() const { return ___window_1; }
	inline Window_t05E229BCE867863C1A33354186F3E7641707AAA5 ** get_address_of_window_1() { return &___window_1; }
	inline void set_window_1(Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * value)
	{
		___window_1 = value;
		Il2CppCodeGenWriteBarrier((&___window_1), value);
	}

	inline static int32_t get_offset_of_controllerId_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass338_0_tC041F08935BE04F38DAFB200176A9F0EBA83CCF0, ___controllerId_2)); }
	inline int32_t get_controllerId_2() const { return ___controllerId_2; }
	inline int32_t* get_address_of_controllerId_2() { return &___controllerId_2; }
	inline void set_controllerId_2(int32_t value)
	{
		___controllerId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS338_0_TC041F08935BE04F38DAFB200176A9F0EBA83CCF0_H
#ifndef U3CU3EC__DISPLAYCLASS339_0_T949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB_H
#define U3CU3EC__DISPLAYCLASS339_0_T949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass339_0
struct  U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB  : public RuntimeObject
{
public:
	// Rewired.UI.ControlMapper.ControlMapper Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass339_0::<>4__this
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * ___U3CU3E4__this_0;
	// Rewired.UI.ControlMapper.Window Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass339_0::window
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * ___window_1;
	// Rewired.UI.ControlMapper.InputFieldInfo Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass339_0::fieldInfo
	InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5 * ___fieldInfo_2;
	// Rewired.ControllerMap Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass339_0::map
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___map_3;
	// Rewired.ActionElementMap Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass339_0::aem
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___aem_4;
	// System.String Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass339_0::actionName
	String_t* ___actionName_5;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB, ___U3CU3E4__this_0)); }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_window_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB, ___window_1)); }
	inline Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * get_window_1() const { return ___window_1; }
	inline Window_t05E229BCE867863C1A33354186F3E7641707AAA5 ** get_address_of_window_1() { return &___window_1; }
	inline void set_window_1(Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * value)
	{
		___window_1 = value;
		Il2CppCodeGenWriteBarrier((&___window_1), value);
	}

	inline static int32_t get_offset_of_fieldInfo_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB, ___fieldInfo_2)); }
	inline InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5 * get_fieldInfo_2() const { return ___fieldInfo_2; }
	inline InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5 ** get_address_of_fieldInfo_2() { return &___fieldInfo_2; }
	inline void set_fieldInfo_2(InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5 * value)
	{
		___fieldInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_2), value);
	}

	inline static int32_t get_offset_of_map_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB, ___map_3)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_map_3() const { return ___map_3; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_map_3() { return &___map_3; }
	inline void set_map_3(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___map_3 = value;
		Il2CppCodeGenWriteBarrier((&___map_3), value);
	}

	inline static int32_t get_offset_of_aem_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB, ___aem_4)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_aem_4() const { return ___aem_4; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_aem_4() { return &___aem_4; }
	inline void set_aem_4(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___aem_4 = value;
		Il2CppCodeGenWriteBarrier((&___aem_4), value);
	}

	inline static int32_t get_offset_of_actionName_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB, ___actionName_5)); }
	inline String_t* get_actionName_5() const { return ___actionName_5; }
	inline String_t** get_address_of_actionName_5() { return &___actionName_5; }
	inline void set_actionName_5(String_t* value)
	{
		___actionName_5 = value;
		Il2CppCodeGenWriteBarrier((&___actionName_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS339_0_T949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB_H
#ifndef U3CU3EC__DISPLAYCLASS347_0_T4674A2E70097B9A296BA7B9BDA3C6FCC1BD359B4_H
#define U3CU3EC__DISPLAYCLASS347_0_T4674A2E70097B9A296BA7B9BDA3C6FCC1BD359B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass347_0
struct  U3CU3Ec__DisplayClass347_0_t4674A2E70097B9A296BA7B9BDA3C6FCC1BD359B4  : public RuntimeObject
{
public:
	// Rewired.UI.ControlMapper.ControlMapper Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass347_0::<>4__this
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * ___U3CU3E4__this_0;
	// Rewired.UI.ControlMapper.Window Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass347_0::window
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * ___window_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass347_0_t4674A2E70097B9A296BA7B9BDA3C6FCC1BD359B4, ___U3CU3E4__this_0)); }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_window_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass347_0_t4674A2E70097B9A296BA7B9BDA3C6FCC1BD359B4, ___window_1)); }
	inline Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * get_window_1() const { return ___window_1; }
	inline Window_t05E229BCE867863C1A33354186F3E7641707AAA5 ** get_address_of_window_1() { return &___window_1; }
	inline void set_window_1(Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * value)
	{
		___window_1 = value;
		Il2CppCodeGenWriteBarrier((&___window_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS347_0_T4674A2E70097B9A296BA7B9BDA3C6FCC1BD359B4_H
#ifndef U3CU3EC__DISPLAYCLASS400_0_T28223F937AE7412CA660B35BEDC2C07F47E714CF_H
#define U3CU3EC__DISPLAYCLASS400_0_T28223F937AE7412CA660B35BEDC2C07F47E714CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass400_0
struct  U3CU3Ec__DisplayClass400_0_t28223F937AE7412CA660B35BEDC2C07F47E714CF  : public RuntimeObject
{
public:
	// Rewired.UI.ControlMapper.ControlMapper Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass400_0::<>4__this
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * ___U3CU3E4__this_0;
	// Rewired.UI.ControlMapper.Window Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass400_0::window
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * ___window_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass400_0_t28223F937AE7412CA660B35BEDC2C07F47E714CF, ___U3CU3E4__this_0)); }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_window_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass400_0_t28223F937AE7412CA660B35BEDC2C07F47E714CF, ___window_1)); }
	inline Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * get_window_1() const { return ___window_1; }
	inline Window_t05E229BCE867863C1A33354186F3E7641707AAA5 ** get_address_of_window_1() { return &___window_1; }
	inline void set_window_1(Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * value)
	{
		___window_1 = value;
		Il2CppCodeGenWriteBarrier((&___window_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS400_0_T28223F937AE7412CA660B35BEDC2C07F47E714CF_H
#ifndef U3CU3EC__DISPLAYCLASS453_0_T2EEF9DCBDDD35A3C3D72D67474D20B1C2CD45D6D_H
#define U3CU3EC__DISPLAYCLASS453_0_T2EEF9DCBDDD35A3C3D72D67474D20B1C2CD45D6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass453_0
struct  U3CU3Ec__DisplayClass453_0_t2EEF9DCBDDD35A3C3D72D67474D20B1C2CD45D6D  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass453_0::aem
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___aem_0;

public:
	inline static int32_t get_offset_of_aem_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass453_0_t2EEF9DCBDDD35A3C3D72D67474D20B1C2CD45D6D, ___aem_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_aem_0() const { return ___aem_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_aem_0() { return &___aem_0; }
	inline void set_aem_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___aem_0 = value;
		Il2CppCodeGenWriteBarrier((&___aem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS453_0_T2EEF9DCBDDD35A3C3D72D67474D20B1C2CD45D6D_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_TA79798299BC1B8AA685BC65954389E15380910F9_H
#define U3CU3EC__DISPLAYCLASS7_0_TA79798299BC1B8AA685BC65954389E15380910F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_GUIButton_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_tA79798299BC1B8AA685BC65954389E15380910F9  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.UI.ControlMapper.ButtonInfo> Rewired.UI.ControlMapper.ControlMapper_GUIButton_<>c__DisplayClass7_0::callback
	Action_1_t1E32078CBBA151AEB509ACEF0FE466CDE0FA379C * ___callback_0;
	// Rewired.UI.ControlMapper.ControlMapper_GUIButton Rewired.UI.ControlMapper.ControlMapper_GUIButton_<>c__DisplayClass7_0::<>4__this
	GUIButton_tF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tA79798299BC1B8AA685BC65954389E15380910F9, ___callback_0)); }
	inline Action_1_t1E32078CBBA151AEB509ACEF0FE466CDE0FA379C * get_callback_0() const { return ___callback_0; }
	inline Action_1_t1E32078CBBA151AEB509ACEF0FE466CDE0FA379C ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t1E32078CBBA151AEB509ACEF0FE466CDE0FA379C * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tA79798299BC1B8AA685BC65954389E15380910F9, ___U3CU3E4__this_1)); }
	inline GUIButton_tF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline GUIButton_tF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(GUIButton_tF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_TA79798299BC1B8AA685BC65954389E15380910F9_H
#ifndef GUIELEMENT_T51DA7ED3E1073A18D53ABC6E454839C630CB71A9_H
#define GUIELEMENT_T51DA7ED3E1073A18D53ABC6E454839C630CB71A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_GUIElement
struct  GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_GUIElement::gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject_0;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.ControlMapper_GUIElement::text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___text_1;
	// UnityEngine.UI.Selectable Rewired.UI.ControlMapper.ControlMapper_GUIElement::selectable
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___selectable_2;
	// Rewired.UI.ControlMapper.UIElementInfo Rewired.UI.ControlMapper.ControlMapper_GUIElement::uiElementInfo
	UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA * ___uiElementInfo_3;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper_GUIElement::permanentStateSet
	bool ___permanentStateSet_4;
	// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.ControlMapper_GUIElement> Rewired.UI.ControlMapper.ControlMapper_GUIElement::children
	List_1_t9DD46FE95C9E0B2084FE46D957811DAD09B2B7DA * ___children_5;
	// UnityEngine.RectTransform Rewired.UI.ControlMapper.ControlMapper_GUIElement::<rectTransform>k__BackingField
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___U3CrectTransformU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9, ___gameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_0), value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9, ___text_1)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_text_1() const { return ___text_1; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((&___text_1), value);
	}

	inline static int32_t get_offset_of_selectable_2() { return static_cast<int32_t>(offsetof(GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9, ___selectable_2)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_selectable_2() const { return ___selectable_2; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_selectable_2() { return &___selectable_2; }
	inline void set_selectable_2(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___selectable_2 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_2), value);
	}

	inline static int32_t get_offset_of_uiElementInfo_3() { return static_cast<int32_t>(offsetof(GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9, ___uiElementInfo_3)); }
	inline UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA * get_uiElementInfo_3() const { return ___uiElementInfo_3; }
	inline UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA ** get_address_of_uiElementInfo_3() { return &___uiElementInfo_3; }
	inline void set_uiElementInfo_3(UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA * value)
	{
		___uiElementInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___uiElementInfo_3), value);
	}

	inline static int32_t get_offset_of_permanentStateSet_4() { return static_cast<int32_t>(offsetof(GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9, ___permanentStateSet_4)); }
	inline bool get_permanentStateSet_4() const { return ___permanentStateSet_4; }
	inline bool* get_address_of_permanentStateSet_4() { return &___permanentStateSet_4; }
	inline void set_permanentStateSet_4(bool value)
	{
		___permanentStateSet_4 = value;
	}

	inline static int32_t get_offset_of_children_5() { return static_cast<int32_t>(offsetof(GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9, ___children_5)); }
	inline List_1_t9DD46FE95C9E0B2084FE46D957811DAD09B2B7DA * get_children_5() const { return ___children_5; }
	inline List_1_t9DD46FE95C9E0B2084FE46D957811DAD09B2B7DA ** get_address_of_children_5() { return &___children_5; }
	inline void set_children_5(List_1_t9DD46FE95C9E0B2084FE46D957811DAD09B2B7DA * value)
	{
		___children_5 = value;
		Il2CppCodeGenWriteBarrier((&___children_5), value);
	}

	inline static int32_t get_offset_of_U3CrectTransformU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9, ___U3CrectTransformU3Ek__BackingField_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_U3CrectTransformU3Ek__BackingField_6() const { return ___U3CrectTransformU3Ek__BackingField_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_U3CrectTransformU3Ek__BackingField_6() { return &___U3CrectTransformU3Ek__BackingField_6; }
	inline void set_U3CrectTransformU3Ek__BackingField_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___U3CrectTransformU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrectTransformU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T51DA7ED3E1073A18D53ABC6E454839C630CB71A9_H
#ifndef U3CU3EC__DISPLAYCLASS19_0_TA6C5E62246A5BECED1570D6A4AD0F0BD0BB6AD25_H
#define U3CU3EC__DISPLAYCLASS19_0_TA6C5E62246A5BECED1570D6A4AD0F0BD0BB6AD25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_GUIInputField_<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_tA6C5E62246A5BECED1570D6A4AD0F0BD0BB6AD25  : public RuntimeObject
{
public:
	// System.Action`1<Rewired.UI.ControlMapper.InputFieldInfo> Rewired.UI.ControlMapper.ControlMapper_GUIInputField_<>c__DisplayClass19_0::callback
	Action_1_t7738608E1471A66AED779B33F76214E0494E0B0D * ___callback_0;
	// Rewired.UI.ControlMapper.ControlMapper_GUIInputField Rewired.UI.ControlMapper.ControlMapper_GUIInputField_<>c__DisplayClass19_0::<>4__this
	GUIInputField_tF982ED4AB954FC21D0AE7F35EAB4C8234BDF4F20 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_tA6C5E62246A5BECED1570D6A4AD0F0BD0BB6AD25, ___callback_0)); }
	inline Action_1_t7738608E1471A66AED779B33F76214E0494E0B0D * get_callback_0() const { return ___callback_0; }
	inline Action_1_t7738608E1471A66AED779B33F76214E0494E0B0D ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t7738608E1471A66AED779B33F76214E0494E0B0D * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_tA6C5E62246A5BECED1570D6A4AD0F0BD0BB6AD25, ___U3CU3E4__this_1)); }
	inline GUIInputField_tF982ED4AB954FC21D0AE7F35EAB4C8234BDF4F20 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline GUIInputField_tF982ED4AB954FC21D0AE7F35EAB4C8234BDF4F20 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(GUIInputField_tF982ED4AB954FC21D0AE7F35EAB4C8234BDF4F20 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS19_0_TA6C5E62246A5BECED1570D6A4AD0F0BD0BB6AD25_H
#ifndef GUILABEL_T30D8F39A3624BCF25878D6F4D164D4B6230758E8_H
#define GUILABEL_T30D8F39A3624BCF25878D6F4D164D4B6230758E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_GUILabel
struct  GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_GUILabel::<gameObject>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CgameObjectU3Ek__BackingField_0;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.ControlMapper_GUILabel::<text>k__BackingField
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___U3CtextU3Ek__BackingField_1;
	// UnityEngine.RectTransform Rewired.UI.ControlMapper.ControlMapper_GUILabel::<rectTransform>k__BackingField
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___U3CrectTransformU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CgameObjectU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8, ___U3CgameObjectU3Ek__BackingField_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CgameObjectU3Ek__BackingField_0() const { return ___U3CgameObjectU3Ek__BackingField_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CgameObjectU3Ek__BackingField_0() { return &___U3CgameObjectU3Ek__BackingField_0; }
	inline void set_U3CgameObjectU3Ek__BackingField_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CgameObjectU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameObjectU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8, ___U3CtextU3Ek__BackingField_1)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_U3CtextU3Ek__BackingField_1() const { return ___U3CtextU3Ek__BackingField_1; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_U3CtextU3Ek__BackingField_1() { return &___U3CtextU3Ek__BackingField_1; }
	inline void set_U3CtextU3Ek__BackingField_1(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___U3CtextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CrectTransformU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8, ___U3CrectTransformU3Ek__BackingField_2)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_U3CrectTransformU3Ek__BackingField_2() const { return ___U3CrectTransformU3Ek__BackingField_2; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_U3CrectTransformU3Ek__BackingField_2() { return &___U3CrectTransformU3Ek__BackingField_2; }
	inline void set_U3CrectTransformU3Ek__BackingField_2(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___U3CrectTransformU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrectTransformU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILABEL_T30D8F39A3624BCF25878D6F4D164D4B6230758E8_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T50AD2C3FB1B2CC9FB668EA518444ECDA8A057F13_H
#define U3CU3EC__DISPLAYCLASS10_0_T50AD2C3FB1B2CC9FB668EA518444ECDA8A057F13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_GUIToggle_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t50AD2C3FB1B2CC9FB668EA518444ECDA8A057F13  : public RuntimeObject
{
public:
	// System.Action`2<Rewired.UI.ControlMapper.ToggleInfo,System.Boolean> Rewired.UI.ControlMapper.ControlMapper_GUIToggle_<>c__DisplayClass10_0::callback
	Action_2_tCFB48941301BEC70B366F467446469E97B0B61C9 * ___callback_0;
	// Rewired.UI.ControlMapper.ControlMapper_GUIToggle Rewired.UI.ControlMapper.ControlMapper_GUIToggle_<>c__DisplayClass10_0::<>4__this
	GUIToggle_tD90790241D2ED7478964399C10A03986E91C88F9 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t50AD2C3FB1B2CC9FB668EA518444ECDA8A057F13, ___callback_0)); }
	inline Action_2_tCFB48941301BEC70B366F467446469E97B0B61C9 * get_callback_0() const { return ___callback_0; }
	inline Action_2_tCFB48941301BEC70B366F467446469E97B0B61C9 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_tCFB48941301BEC70B366F467446469E97B0B61C9 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t50AD2C3FB1B2CC9FB668EA518444ECDA8A057F13, ___U3CU3E4__this_1)); }
	inline GUIToggle_tD90790241D2ED7478964399C10A03986E91C88F9 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline GUIToggle_tD90790241D2ED7478964399C10A03986E91C88F9 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(GUIToggle_tD90790241D2ED7478964399C10A03986E91C88F9 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T50AD2C3FB1B2CC9FB668EA518444ECDA8A057F13_H
#ifndef INPUTBEHAVIORSETTINGS_T51F59440D044CD4B45491FEC471612E29467223A_H
#define INPUTBEHAVIORSETTINGS_T51F59440D044CD4B45491FEC471612E29467223A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings
struct  InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A  : public RuntimeObject
{
public:
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings::_inputBehaviorId
	int32_t ____inputBehaviorId_0;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings::_showJoystickAxisSensitivity
	bool ____showJoystickAxisSensitivity_1;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings::_showMouseXYAxisSensitivity
	bool ____showMouseXYAxisSensitivity_2;
	// System.String Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings::_labelLanguageKey
	String_t* ____labelLanguageKey_3;
	// System.String Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings::_joystickAxisSensitivityLabelLanguageKey
	String_t* ____joystickAxisSensitivityLabelLanguageKey_4;
	// System.String Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings::_mouseXYAxisSensitivityLabelLanguageKey
	String_t* ____mouseXYAxisSensitivityLabelLanguageKey_5;
	// UnityEngine.Sprite Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings::_joystickAxisSensitivityIcon
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ____joystickAxisSensitivityIcon_6;
	// UnityEngine.Sprite Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings::_mouseXYAxisSensitivityIcon
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ____mouseXYAxisSensitivityIcon_7;
	// System.Single Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings::_joystickAxisSensitivityMin
	float ____joystickAxisSensitivityMin_8;
	// System.Single Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings::_joystickAxisSensitivityMax
	float ____joystickAxisSensitivityMax_9;
	// System.Single Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings::_mouseXYAxisSensitivityMin
	float ____mouseXYAxisSensitivityMin_10;
	// System.Single Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings::_mouseXYAxisSensitivityMax
	float ____mouseXYAxisSensitivityMax_11;

public:
	inline static int32_t get_offset_of__inputBehaviorId_0() { return static_cast<int32_t>(offsetof(InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A, ____inputBehaviorId_0)); }
	inline int32_t get__inputBehaviorId_0() const { return ____inputBehaviorId_0; }
	inline int32_t* get_address_of__inputBehaviorId_0() { return &____inputBehaviorId_0; }
	inline void set__inputBehaviorId_0(int32_t value)
	{
		____inputBehaviorId_0 = value;
	}

	inline static int32_t get_offset_of__showJoystickAxisSensitivity_1() { return static_cast<int32_t>(offsetof(InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A, ____showJoystickAxisSensitivity_1)); }
	inline bool get__showJoystickAxisSensitivity_1() const { return ____showJoystickAxisSensitivity_1; }
	inline bool* get_address_of__showJoystickAxisSensitivity_1() { return &____showJoystickAxisSensitivity_1; }
	inline void set__showJoystickAxisSensitivity_1(bool value)
	{
		____showJoystickAxisSensitivity_1 = value;
	}

	inline static int32_t get_offset_of__showMouseXYAxisSensitivity_2() { return static_cast<int32_t>(offsetof(InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A, ____showMouseXYAxisSensitivity_2)); }
	inline bool get__showMouseXYAxisSensitivity_2() const { return ____showMouseXYAxisSensitivity_2; }
	inline bool* get_address_of__showMouseXYAxisSensitivity_2() { return &____showMouseXYAxisSensitivity_2; }
	inline void set__showMouseXYAxisSensitivity_2(bool value)
	{
		____showMouseXYAxisSensitivity_2 = value;
	}

	inline static int32_t get_offset_of__labelLanguageKey_3() { return static_cast<int32_t>(offsetof(InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A, ____labelLanguageKey_3)); }
	inline String_t* get__labelLanguageKey_3() const { return ____labelLanguageKey_3; }
	inline String_t** get_address_of__labelLanguageKey_3() { return &____labelLanguageKey_3; }
	inline void set__labelLanguageKey_3(String_t* value)
	{
		____labelLanguageKey_3 = value;
		Il2CppCodeGenWriteBarrier((&____labelLanguageKey_3), value);
	}

	inline static int32_t get_offset_of__joystickAxisSensitivityLabelLanguageKey_4() { return static_cast<int32_t>(offsetof(InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A, ____joystickAxisSensitivityLabelLanguageKey_4)); }
	inline String_t* get__joystickAxisSensitivityLabelLanguageKey_4() const { return ____joystickAxisSensitivityLabelLanguageKey_4; }
	inline String_t** get_address_of__joystickAxisSensitivityLabelLanguageKey_4() { return &____joystickAxisSensitivityLabelLanguageKey_4; }
	inline void set__joystickAxisSensitivityLabelLanguageKey_4(String_t* value)
	{
		____joystickAxisSensitivityLabelLanguageKey_4 = value;
		Il2CppCodeGenWriteBarrier((&____joystickAxisSensitivityLabelLanguageKey_4), value);
	}

	inline static int32_t get_offset_of__mouseXYAxisSensitivityLabelLanguageKey_5() { return static_cast<int32_t>(offsetof(InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A, ____mouseXYAxisSensitivityLabelLanguageKey_5)); }
	inline String_t* get__mouseXYAxisSensitivityLabelLanguageKey_5() const { return ____mouseXYAxisSensitivityLabelLanguageKey_5; }
	inline String_t** get_address_of__mouseXYAxisSensitivityLabelLanguageKey_5() { return &____mouseXYAxisSensitivityLabelLanguageKey_5; }
	inline void set__mouseXYAxisSensitivityLabelLanguageKey_5(String_t* value)
	{
		____mouseXYAxisSensitivityLabelLanguageKey_5 = value;
		Il2CppCodeGenWriteBarrier((&____mouseXYAxisSensitivityLabelLanguageKey_5), value);
	}

	inline static int32_t get_offset_of__joystickAxisSensitivityIcon_6() { return static_cast<int32_t>(offsetof(InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A, ____joystickAxisSensitivityIcon_6)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get__joystickAxisSensitivityIcon_6() const { return ____joystickAxisSensitivityIcon_6; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of__joystickAxisSensitivityIcon_6() { return &____joystickAxisSensitivityIcon_6; }
	inline void set__joystickAxisSensitivityIcon_6(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		____joystickAxisSensitivityIcon_6 = value;
		Il2CppCodeGenWriteBarrier((&____joystickAxisSensitivityIcon_6), value);
	}

	inline static int32_t get_offset_of__mouseXYAxisSensitivityIcon_7() { return static_cast<int32_t>(offsetof(InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A, ____mouseXYAxisSensitivityIcon_7)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get__mouseXYAxisSensitivityIcon_7() const { return ____mouseXYAxisSensitivityIcon_7; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of__mouseXYAxisSensitivityIcon_7() { return &____mouseXYAxisSensitivityIcon_7; }
	inline void set__mouseXYAxisSensitivityIcon_7(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		____mouseXYAxisSensitivityIcon_7 = value;
		Il2CppCodeGenWriteBarrier((&____mouseXYAxisSensitivityIcon_7), value);
	}

	inline static int32_t get_offset_of__joystickAxisSensitivityMin_8() { return static_cast<int32_t>(offsetof(InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A, ____joystickAxisSensitivityMin_8)); }
	inline float get__joystickAxisSensitivityMin_8() const { return ____joystickAxisSensitivityMin_8; }
	inline float* get_address_of__joystickAxisSensitivityMin_8() { return &____joystickAxisSensitivityMin_8; }
	inline void set__joystickAxisSensitivityMin_8(float value)
	{
		____joystickAxisSensitivityMin_8 = value;
	}

	inline static int32_t get_offset_of__joystickAxisSensitivityMax_9() { return static_cast<int32_t>(offsetof(InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A, ____joystickAxisSensitivityMax_9)); }
	inline float get__joystickAxisSensitivityMax_9() const { return ____joystickAxisSensitivityMax_9; }
	inline float* get_address_of__joystickAxisSensitivityMax_9() { return &____joystickAxisSensitivityMax_9; }
	inline void set__joystickAxisSensitivityMax_9(float value)
	{
		____joystickAxisSensitivityMax_9 = value;
	}

	inline static int32_t get_offset_of__mouseXYAxisSensitivityMin_10() { return static_cast<int32_t>(offsetof(InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A, ____mouseXYAxisSensitivityMin_10)); }
	inline float get__mouseXYAxisSensitivityMin_10() const { return ____mouseXYAxisSensitivityMin_10; }
	inline float* get_address_of__mouseXYAxisSensitivityMin_10() { return &____mouseXYAxisSensitivityMin_10; }
	inline void set__mouseXYAxisSensitivityMin_10(float value)
	{
		____mouseXYAxisSensitivityMin_10 = value;
	}

	inline static int32_t get_offset_of__mouseXYAxisSensitivityMax_11() { return static_cast<int32_t>(offsetof(InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A, ____mouseXYAxisSensitivityMax_11)); }
	inline float get__mouseXYAxisSensitivityMax_11() const { return ____mouseXYAxisSensitivityMax_11; }
	inline float* get_address_of__mouseXYAxisSensitivityMax_11() { return &____mouseXYAxisSensitivityMax_11; }
	inline void set__mouseXYAxisSensitivityMax_11(float value)
	{
		____mouseXYAxisSensitivityMax_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBEHAVIORSETTINGS_T51F59440D044CD4B45491FEC471612E29467223A_H
#ifndef INPUTGRID_T279399655219F94B8033968DF46749593CF22E63_H
#define INPUTGRID_T279399655219F94B8033968DF46749593CF22E63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_InputGrid
struct  InputGrid_t279399655219F94B8033968DF46749593CF22E63  : public RuntimeObject
{
public:
	// Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList Rewired.UI.ControlMapper.ControlMapper_InputGrid::list
	InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF * ___list_0;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Rewired.UI.ControlMapper.ControlMapper_InputGrid::groups
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___groups_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(InputGrid_t279399655219F94B8033968DF46749593CF22E63, ___list_0)); }
	inline InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF * get_list_0() const { return ___list_0; }
	inline InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_groups_1() { return static_cast<int32_t>(offsetof(InputGrid_t279399655219F94B8033968DF46749593CF22E63, ___groups_1)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_groups_1() const { return ___groups_1; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_groups_1() { return &___groups_1; }
	inline void set_groups_1(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___groups_1 = value;
		Il2CppCodeGenWriteBarrier((&___groups_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTGRID_T279399655219F94B8033968DF46749593CF22E63_H
#ifndef INPUTGRIDENTRYLIST_T24C84E92E3F06279512E824C16C7B0AD512140AF_H
#define INPUTGRIDENTRYLIST_T24C84E92E3F06279512E824C16C7B0AD512140AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList
struct  InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF  : public RuntimeObject
{
public:
	// Rewired.UI.ControlMapper.ControlMapper_IndexedDictionary`2<System.Int32,Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_MapCategoryEntry> Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList::entries
	IndexedDictionary_2_t166D8745D1FC0F5B53B131E55CCFDFD7FEE548D7 * ___entries_0;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF, ___entries_0)); }
	inline IndexedDictionary_2_t166D8745D1FC0F5B53B131E55CCFDFD7FEE548D7 * get_entries_0() const { return ___entries_0; }
	inline IndexedDictionary_2_t166D8745D1FC0F5B53B131E55CCFDFD7FEE548D7 ** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(IndexedDictionary_2_t166D8745D1FC0F5B53B131E55CCFDFD7FEE548D7 * value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTGRIDENTRYLIST_T24C84E92E3F06279512E824C16C7B0AD512140AF_H
#ifndef U3CGETACTIONSETSU3ED__18_T2BD57B2694DE2330809C37D5ED792CB7D8A7983D_H
#define U3CGETACTIONSETSU3ED__18_T2BD57B2694DE2330809C37D5ED792CB7D8A7983D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_<GetActionSets>d__18
struct  U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D  : public RuntimeObject
{
public:
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_<GetActionSets>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Rewired.UI.ControlMapper.ControlMapper_InputActionSet Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_<GetActionSets>d__18::<>2__current
	InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B * ___U3CU3E2__current_1;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_<GetActionSets>d__18::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_<GetActionSets>d__18::mapCategoryId
	int32_t ___mapCategoryId_3;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_<GetActionSets>d__18::<>3__mapCategoryId
	int32_t ___U3CU3E3__mapCategoryId_4;
	// Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_<GetActionSets>d__18::<>4__this
	InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF * ___U3CU3E4__this_5;
	// Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_MapCategoryEntry Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_<GetActionSets>d__18::<entry>5__1
	MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906 * ___U3CentryU3E5__1_6;
	// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_ActionEntry> Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_<GetActionSets>d__18::<list>5__2
	List_1_t1D7050D8FD8BB194442B96C5AAA4CAFBB2EE9611 * ___U3ClistU3E5__2_7;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_<GetActionSets>d__18::<count>5__3
	int32_t ___U3CcountU3E5__3_8;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_<GetActionSets>d__18::<i>5__4
	int32_t ___U3CiU3E5__4_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D, ___U3CU3E2__current_1)); }
	inline InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_mapCategoryId_3() { return static_cast<int32_t>(offsetof(U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D, ___mapCategoryId_3)); }
	inline int32_t get_mapCategoryId_3() const { return ___mapCategoryId_3; }
	inline int32_t* get_address_of_mapCategoryId_3() { return &___mapCategoryId_3; }
	inline void set_mapCategoryId_3(int32_t value)
	{
		___mapCategoryId_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__mapCategoryId_4() { return static_cast<int32_t>(offsetof(U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D, ___U3CU3E3__mapCategoryId_4)); }
	inline int32_t get_U3CU3E3__mapCategoryId_4() const { return ___U3CU3E3__mapCategoryId_4; }
	inline int32_t* get_address_of_U3CU3E3__mapCategoryId_4() { return &___U3CU3E3__mapCategoryId_4; }
	inline void set_U3CU3E3__mapCategoryId_4(int32_t value)
	{
		___U3CU3E3__mapCategoryId_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D, ___U3CU3E4__this_5)); }
	inline InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CentryU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D, ___U3CentryU3E5__1_6)); }
	inline MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906 * get_U3CentryU3E5__1_6() const { return ___U3CentryU3E5__1_6; }
	inline MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906 ** get_address_of_U3CentryU3E5__1_6() { return &___U3CentryU3E5__1_6; }
	inline void set_U3CentryU3E5__1_6(MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906 * value)
	{
		___U3CentryU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentryU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3ClistU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D, ___U3ClistU3E5__2_7)); }
	inline List_1_t1D7050D8FD8BB194442B96C5AAA4CAFBB2EE9611 * get_U3ClistU3E5__2_7() const { return ___U3ClistU3E5__2_7; }
	inline List_1_t1D7050D8FD8BB194442B96C5AAA4CAFBB2EE9611 ** get_address_of_U3ClistU3E5__2_7() { return &___U3ClistU3E5__2_7; }
	inline void set_U3ClistU3E5__2_7(List_1_t1D7050D8FD8BB194442B96C5AAA4CAFBB2EE9611 * value)
	{
		___U3ClistU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CcountU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D, ___U3CcountU3E5__3_8)); }
	inline int32_t get_U3CcountU3E5__3_8() const { return ___U3CcountU3E5__3_8; }
	inline int32_t* get_address_of_U3CcountU3E5__3_8() { return &___U3CcountU3E5__3_8; }
	inline void set_U3CcountU3E5__3_8(int32_t value)
	{
		___U3CcountU3E5__3_8 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__4_9() { return static_cast<int32_t>(offsetof(U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D, ___U3CiU3E5__4_9)); }
	inline int32_t get_U3CiU3E5__4_9() const { return ___U3CiU3E5__4_9; }
	inline int32_t* get_address_of_U3CiU3E5__4_9() { return &___U3CiU3E5__4_9; }
	inline void set_U3CiU3E5__4_9(int32_t value)
	{
		___U3CiU3E5__4_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETACTIONSETSU3ED__18_T2BD57B2694DE2330809C37D5ED792CB7D8A7983D_H
#ifndef ACTIONCATEGORYENTRY_TF8BCACB2FF877C3241621A673AC1287F385AF40F_H
#define ACTIONCATEGORYENTRY_TF8BCACB2FF877C3241621A673AC1287F385AF40F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_ActionCategoryEntry
struct  ActionCategoryEntry_tF8BCACB2FF877C3241621A673AC1287F385AF40F  : public RuntimeObject
{
public:
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_ActionCategoryEntry::actionCategoryId
	int32_t ___actionCategoryId_0;
	// Rewired.UI.ControlMapper.ControlMapper_GUILabel Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_ActionCategoryEntry::label
	GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8 * ___label_1;

public:
	inline static int32_t get_offset_of_actionCategoryId_0() { return static_cast<int32_t>(offsetof(ActionCategoryEntry_tF8BCACB2FF877C3241621A673AC1287F385AF40F, ___actionCategoryId_0)); }
	inline int32_t get_actionCategoryId_0() const { return ___actionCategoryId_0; }
	inline int32_t* get_address_of_actionCategoryId_0() { return &___actionCategoryId_0; }
	inline void set_actionCategoryId_0(int32_t value)
	{
		___actionCategoryId_0 = value;
	}

	inline static int32_t get_offset_of_label_1() { return static_cast<int32_t>(offsetof(ActionCategoryEntry_tF8BCACB2FF877C3241621A673AC1287F385AF40F, ___label_1)); }
	inline GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8 * get_label_1() const { return ___label_1; }
	inline GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8 ** get_address_of_label_1() { return &___label_1; }
	inline void set_label_1(GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8 * value)
	{
		___label_1 = value;
		Il2CppCodeGenWriteBarrier((&___label_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONCATEGORYENTRY_TF8BCACB2FF877C3241621A673AC1287F385AF40F_H
#ifndef FIELDSET_T32DFC4D88DB2D0C89C667DFBADA9D4A95B80169E_H
#define FIELDSET_T32DFC4D88DB2D0C89C667DFBADA9D4A95B80169E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_FieldSet
struct  FieldSet_t32DFC4D88DB2D0C89C667DFBADA9D4A95B80169E  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_FieldSet::groupContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___groupContainer_0;
	// Rewired.UI.ControlMapper.ControlMapper_IndexedDictionary`2<System.Int32,Rewired.UI.ControlMapper.ControlMapper_GUIInputField> Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_FieldSet::fields
	IndexedDictionary_2_tD7799A910D100794D2BE1750660D330592FA52CD * ___fields_1;

public:
	inline static int32_t get_offset_of_groupContainer_0() { return static_cast<int32_t>(offsetof(FieldSet_t32DFC4D88DB2D0C89C667DFBADA9D4A95B80169E, ___groupContainer_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_groupContainer_0() const { return ___groupContainer_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_groupContainer_0() { return &___groupContainer_0; }
	inline void set_groupContainer_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___groupContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___groupContainer_0), value);
	}

	inline static int32_t get_offset_of_fields_1() { return static_cast<int32_t>(offsetof(FieldSet_t32DFC4D88DB2D0C89C667DFBADA9D4A95B80169E, ___fields_1)); }
	inline IndexedDictionary_2_tD7799A910D100794D2BE1750660D330592FA52CD * get_fields_1() const { return ___fields_1; }
	inline IndexedDictionary_2_tD7799A910D100794D2BE1750660D330592FA52CD ** get_address_of_fields_1() { return &___fields_1; }
	inline void set_fields_1(IndexedDictionary_2_tD7799A910D100794D2BE1750660D330592FA52CD * value)
	{
		___fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDSET_T32DFC4D88DB2D0C89C667DFBADA9D4A95B80169E_H
#ifndef MAPCATEGORYENTRY_T19791ED0F3BB2E26853F251AF939D7B0AE943906_H
#define MAPCATEGORYENTRY_T19791ED0F3BB2E26853F251AF939D7B0AE943906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_MapCategoryEntry
struct  MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_ActionEntry> Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_MapCategoryEntry::_actionList
	List_1_t1D7050D8FD8BB194442B96C5AAA4CAFBB2EE9611 * ____actionList_0;
	// Rewired.UI.ControlMapper.ControlMapper_IndexedDictionary`2<System.Int32,Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_ActionCategoryEntry> Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_MapCategoryEntry::_actionCategoryList
	IndexedDictionary_2_tEA0DBCB1C67F99E45E3AE022DAFD95EE0B098A28 * ____actionCategoryList_1;
	// System.Single Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_MapCategoryEntry::_columnHeight
	float ____columnHeight_2;

public:
	inline static int32_t get_offset_of__actionList_0() { return static_cast<int32_t>(offsetof(MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906, ____actionList_0)); }
	inline List_1_t1D7050D8FD8BB194442B96C5AAA4CAFBB2EE9611 * get__actionList_0() const { return ____actionList_0; }
	inline List_1_t1D7050D8FD8BB194442B96C5AAA4CAFBB2EE9611 ** get_address_of__actionList_0() { return &____actionList_0; }
	inline void set__actionList_0(List_1_t1D7050D8FD8BB194442B96C5AAA4CAFBB2EE9611 * value)
	{
		____actionList_0 = value;
		Il2CppCodeGenWriteBarrier((&____actionList_0), value);
	}

	inline static int32_t get_offset_of__actionCategoryList_1() { return static_cast<int32_t>(offsetof(MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906, ____actionCategoryList_1)); }
	inline IndexedDictionary_2_tEA0DBCB1C67F99E45E3AE022DAFD95EE0B098A28 * get__actionCategoryList_1() const { return ____actionCategoryList_1; }
	inline IndexedDictionary_2_tEA0DBCB1C67F99E45E3AE022DAFD95EE0B098A28 ** get_address_of__actionCategoryList_1() { return &____actionCategoryList_1; }
	inline void set__actionCategoryList_1(IndexedDictionary_2_tEA0DBCB1C67F99E45E3AE022DAFD95EE0B098A28 * value)
	{
		____actionCategoryList_1 = value;
		Il2CppCodeGenWriteBarrier((&____actionCategoryList_1), value);
	}

	inline static int32_t get_offset_of__columnHeight_2() { return static_cast<int32_t>(offsetof(MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906, ____columnHeight_2)); }
	inline float get__columnHeight_2() const { return ____columnHeight_2; }
	inline float* get_address_of__columnHeight_2() { return &____columnHeight_2; }
	inline void set__columnHeight_2(float value)
	{
		____columnHeight_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPCATEGORYENTRY_T19791ED0F3BB2E26853F251AF939D7B0AE943906_H
#ifndef PREFABS_T48C9F4A7795730DDE4ADD53717898BD13CFC1B6A_H
#define PREFABS_T48C9F4A7795730DDE4ADD53717898BD13CFC1B6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_Prefabs
struct  Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_button
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____button_0;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_fitButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____fitButton_1;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_inputGridLabel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____inputGridLabel_2;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_inputGridHeaderLabel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____inputGridHeaderLabel_3;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_inputGridFieldButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____inputGridFieldButton_4;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_inputGridFieldInvertToggle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____inputGridFieldInvertToggle_5;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_window
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____window_6;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_windowTitleText
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____windowTitleText_7;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_windowContentText
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____windowContentText_8;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_fader
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____fader_9;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_calibrationWindow
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____calibrationWindow_10;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_inputBehaviorsWindow
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____inputBehaviorsWindow_11;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_centerStickGraphic
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____centerStickGraphic_12;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_Prefabs::_moveStickGraphic
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____moveStickGraphic_13;

public:
	inline static int32_t get_offset_of__button_0() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____button_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__button_0() const { return ____button_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__button_0() { return &____button_0; }
	inline void set__button_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____button_0 = value;
		Il2CppCodeGenWriteBarrier((&____button_0), value);
	}

	inline static int32_t get_offset_of__fitButton_1() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____fitButton_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__fitButton_1() const { return ____fitButton_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__fitButton_1() { return &____fitButton_1; }
	inline void set__fitButton_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____fitButton_1 = value;
		Il2CppCodeGenWriteBarrier((&____fitButton_1), value);
	}

	inline static int32_t get_offset_of__inputGridLabel_2() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____inputGridLabel_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__inputGridLabel_2() const { return ____inputGridLabel_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__inputGridLabel_2() { return &____inputGridLabel_2; }
	inline void set__inputGridLabel_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____inputGridLabel_2 = value;
		Il2CppCodeGenWriteBarrier((&____inputGridLabel_2), value);
	}

	inline static int32_t get_offset_of__inputGridHeaderLabel_3() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____inputGridHeaderLabel_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__inputGridHeaderLabel_3() const { return ____inputGridHeaderLabel_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__inputGridHeaderLabel_3() { return &____inputGridHeaderLabel_3; }
	inline void set__inputGridHeaderLabel_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____inputGridHeaderLabel_3 = value;
		Il2CppCodeGenWriteBarrier((&____inputGridHeaderLabel_3), value);
	}

	inline static int32_t get_offset_of__inputGridFieldButton_4() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____inputGridFieldButton_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__inputGridFieldButton_4() const { return ____inputGridFieldButton_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__inputGridFieldButton_4() { return &____inputGridFieldButton_4; }
	inline void set__inputGridFieldButton_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____inputGridFieldButton_4 = value;
		Il2CppCodeGenWriteBarrier((&____inputGridFieldButton_4), value);
	}

	inline static int32_t get_offset_of__inputGridFieldInvertToggle_5() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____inputGridFieldInvertToggle_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__inputGridFieldInvertToggle_5() const { return ____inputGridFieldInvertToggle_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__inputGridFieldInvertToggle_5() { return &____inputGridFieldInvertToggle_5; }
	inline void set__inputGridFieldInvertToggle_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____inputGridFieldInvertToggle_5 = value;
		Il2CppCodeGenWriteBarrier((&____inputGridFieldInvertToggle_5), value);
	}

	inline static int32_t get_offset_of__window_6() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____window_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__window_6() const { return ____window_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__window_6() { return &____window_6; }
	inline void set__window_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____window_6 = value;
		Il2CppCodeGenWriteBarrier((&____window_6), value);
	}

	inline static int32_t get_offset_of__windowTitleText_7() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____windowTitleText_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__windowTitleText_7() const { return ____windowTitleText_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__windowTitleText_7() { return &____windowTitleText_7; }
	inline void set__windowTitleText_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____windowTitleText_7 = value;
		Il2CppCodeGenWriteBarrier((&____windowTitleText_7), value);
	}

	inline static int32_t get_offset_of__windowContentText_8() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____windowContentText_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__windowContentText_8() const { return ____windowContentText_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__windowContentText_8() { return &____windowContentText_8; }
	inline void set__windowContentText_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____windowContentText_8 = value;
		Il2CppCodeGenWriteBarrier((&____windowContentText_8), value);
	}

	inline static int32_t get_offset_of__fader_9() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____fader_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__fader_9() const { return ____fader_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__fader_9() { return &____fader_9; }
	inline void set__fader_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____fader_9 = value;
		Il2CppCodeGenWriteBarrier((&____fader_9), value);
	}

	inline static int32_t get_offset_of__calibrationWindow_10() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____calibrationWindow_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__calibrationWindow_10() const { return ____calibrationWindow_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__calibrationWindow_10() { return &____calibrationWindow_10; }
	inline void set__calibrationWindow_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____calibrationWindow_10 = value;
		Il2CppCodeGenWriteBarrier((&____calibrationWindow_10), value);
	}

	inline static int32_t get_offset_of__inputBehaviorsWindow_11() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____inputBehaviorsWindow_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__inputBehaviorsWindow_11() const { return ____inputBehaviorsWindow_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__inputBehaviorsWindow_11() { return &____inputBehaviorsWindow_11; }
	inline void set__inputBehaviorsWindow_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____inputBehaviorsWindow_11 = value;
		Il2CppCodeGenWriteBarrier((&____inputBehaviorsWindow_11), value);
	}

	inline static int32_t get_offset_of__centerStickGraphic_12() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____centerStickGraphic_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__centerStickGraphic_12() const { return ____centerStickGraphic_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__centerStickGraphic_12() { return &____centerStickGraphic_12; }
	inline void set__centerStickGraphic_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____centerStickGraphic_12 = value;
		Il2CppCodeGenWriteBarrier((&____centerStickGraphic_12), value);
	}

	inline static int32_t get_offset_of__moveStickGraphic_13() { return static_cast<int32_t>(offsetof(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A, ____moveStickGraphic_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__moveStickGraphic_13() const { return ____moveStickGraphic_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__moveStickGraphic_13() { return &____moveStickGraphic_13; }
	inline void set__moveStickGraphic_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____moveStickGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((&____moveStickGraphic_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABS_T48C9F4A7795730DDE4ADD53717898BD13CFC1B6A_H
#ifndef REFERENCES_T63B1022C3D9DF66B0A7AEE0E3A102414E7455163_H
#define REFERENCES_T63B1022C3D9DF66B0A7AEE0E3A102414E7455163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_References
struct  References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163  : public RuntimeObject
{
public:
	// UnityEngine.Canvas Rewired.UI.ControlMapper.ControlMapper_References::_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ____canvas_0;
	// UnityEngine.CanvasGroup Rewired.UI.ControlMapper.ControlMapper_References::_mainCanvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ____mainCanvasGroup_1;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::_mainContent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____mainContent_2;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::_mainContentInner
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____mainContentInner_3;
	// Rewired.UI.ControlMapper.UIGroup Rewired.UI.ControlMapper.ControlMapper_References::_playersGroup
	UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * ____playersGroup_4;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::_controllerGroup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____controllerGroup_5;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::_controllerGroupLabelGroup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____controllerGroupLabelGroup_6;
	// Rewired.UI.ControlMapper.UIGroup Rewired.UI.ControlMapper.ControlMapper_References::_controllerSettingsGroup
	UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * ____controllerSettingsGroup_7;
	// Rewired.UI.ControlMapper.UIGroup Rewired.UI.ControlMapper.ControlMapper_References::_assignedControllersGroup
	UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * ____assignedControllersGroup_8;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::_settingsAndMapCategoriesGroup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____settingsAndMapCategoriesGroup_9;
	// Rewired.UI.ControlMapper.UIGroup Rewired.UI.ControlMapper.ControlMapper_References::_settingsGroup
	UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * ____settingsGroup_10;
	// Rewired.UI.ControlMapper.UIGroup Rewired.UI.ControlMapper.ControlMapper_References::_mapCategoriesGroup
	UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * ____mapCategoriesGroup_11;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::_inputGridGroup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____inputGridGroup_12;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::_inputGridContainer
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____inputGridContainer_13;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::_inputGridHeadersGroup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____inputGridHeadersGroup_14;
	// UnityEngine.UI.Scrollbar Rewired.UI.ControlMapper.ControlMapper_References::_inputGridVScrollbar
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * ____inputGridVScrollbar_15;
	// UnityEngine.UI.ScrollRect Rewired.UI.ControlMapper.ControlMapper_References::_inputGridScrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ____inputGridScrollRect_16;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::_inputGridInnerGroup
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____inputGridInnerGroup_17;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.ControlMapper_References::_controllerNameLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____controllerNameLabel_18;
	// UnityEngine.UI.Button Rewired.UI.ControlMapper.ControlMapper_References::_removeControllerButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____removeControllerButton_19;
	// UnityEngine.UI.Button Rewired.UI.ControlMapper.ControlMapper_References::_assignControllerButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____assignControllerButton_20;
	// UnityEngine.UI.Button Rewired.UI.ControlMapper.ControlMapper_References::_calibrateControllerButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____calibrateControllerButton_21;
	// UnityEngine.UI.Button Rewired.UI.ControlMapper.ControlMapper_References::_doneButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____doneButton_22;
	// UnityEngine.UI.Button Rewired.UI.ControlMapper.ControlMapper_References::_restoreDefaultsButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____restoreDefaultsButton_23;
	// UnityEngine.UI.Selectable Rewired.UI.ControlMapper.ControlMapper_References::_defaultSelection
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ____defaultSelection_24;
	// UnityEngine.GameObject[] Rewired.UI.ControlMapper.ControlMapper_References::_fixedSelectableUIElements
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ____fixedSelectableUIElements_25;
	// UnityEngine.UI.Image Rewired.UI.ControlMapper.ControlMapper_References::_mainBackgroundImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____mainBackgroundImage_26;
	// UnityEngine.UI.LayoutElement Rewired.UI.ControlMapper.ControlMapper_References::<inputGridLayoutElement>k__BackingField
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___U3CinputGridLayoutElementU3Ek__BackingField_27;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::<inputGridActionColumn>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CinputGridActionColumnU3Ek__BackingField_28;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::<inputGridKeyboardColumn>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CinputGridKeyboardColumnU3Ek__BackingField_29;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::<inputGridMouseColumn>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CinputGridMouseColumnU3Ek__BackingField_30;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::<inputGridControllerColumn>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CinputGridControllerColumnU3Ek__BackingField_31;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::<inputGridHeader1>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CinputGridHeader1U3Ek__BackingField_32;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::<inputGridHeader2>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CinputGridHeader2U3Ek__BackingField_33;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::<inputGridHeader3>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CinputGridHeader3U3Ek__BackingField_34;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_References::<inputGridHeader4>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CinputGridHeader4U3Ek__BackingField_35;

public:
	inline static int32_t get_offset_of__canvas_0() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____canvas_0)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get__canvas_0() const { return ____canvas_0; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of__canvas_0() { return &____canvas_0; }
	inline void set__canvas_0(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		____canvas_0 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_0), value);
	}

	inline static int32_t get_offset_of__mainCanvasGroup_1() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____mainCanvasGroup_1)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get__mainCanvasGroup_1() const { return ____mainCanvasGroup_1; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of__mainCanvasGroup_1() { return &____mainCanvasGroup_1; }
	inline void set__mainCanvasGroup_1(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		____mainCanvasGroup_1 = value;
		Il2CppCodeGenWriteBarrier((&____mainCanvasGroup_1), value);
	}

	inline static int32_t get_offset_of__mainContent_2() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____mainContent_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__mainContent_2() const { return ____mainContent_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__mainContent_2() { return &____mainContent_2; }
	inline void set__mainContent_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____mainContent_2 = value;
		Il2CppCodeGenWriteBarrier((&____mainContent_2), value);
	}

	inline static int32_t get_offset_of__mainContentInner_3() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____mainContentInner_3)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__mainContentInner_3() const { return ____mainContentInner_3; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__mainContentInner_3() { return &____mainContentInner_3; }
	inline void set__mainContentInner_3(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____mainContentInner_3 = value;
		Il2CppCodeGenWriteBarrier((&____mainContentInner_3), value);
	}

	inline static int32_t get_offset_of__playersGroup_4() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____playersGroup_4)); }
	inline UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * get__playersGroup_4() const { return ____playersGroup_4; }
	inline UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 ** get_address_of__playersGroup_4() { return &____playersGroup_4; }
	inline void set__playersGroup_4(UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * value)
	{
		____playersGroup_4 = value;
		Il2CppCodeGenWriteBarrier((&____playersGroup_4), value);
	}

	inline static int32_t get_offset_of__controllerGroup_5() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____controllerGroup_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__controllerGroup_5() const { return ____controllerGroup_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__controllerGroup_5() { return &____controllerGroup_5; }
	inline void set__controllerGroup_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____controllerGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&____controllerGroup_5), value);
	}

	inline static int32_t get_offset_of__controllerGroupLabelGroup_6() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____controllerGroupLabelGroup_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__controllerGroupLabelGroup_6() const { return ____controllerGroupLabelGroup_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__controllerGroupLabelGroup_6() { return &____controllerGroupLabelGroup_6; }
	inline void set__controllerGroupLabelGroup_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____controllerGroupLabelGroup_6 = value;
		Il2CppCodeGenWriteBarrier((&____controllerGroupLabelGroup_6), value);
	}

	inline static int32_t get_offset_of__controllerSettingsGroup_7() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____controllerSettingsGroup_7)); }
	inline UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * get__controllerSettingsGroup_7() const { return ____controllerSettingsGroup_7; }
	inline UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 ** get_address_of__controllerSettingsGroup_7() { return &____controllerSettingsGroup_7; }
	inline void set__controllerSettingsGroup_7(UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * value)
	{
		____controllerSettingsGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&____controllerSettingsGroup_7), value);
	}

	inline static int32_t get_offset_of__assignedControllersGroup_8() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____assignedControllersGroup_8)); }
	inline UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * get__assignedControllersGroup_8() const { return ____assignedControllersGroup_8; }
	inline UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 ** get_address_of__assignedControllersGroup_8() { return &____assignedControllersGroup_8; }
	inline void set__assignedControllersGroup_8(UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * value)
	{
		____assignedControllersGroup_8 = value;
		Il2CppCodeGenWriteBarrier((&____assignedControllersGroup_8), value);
	}

	inline static int32_t get_offset_of__settingsAndMapCategoriesGroup_9() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____settingsAndMapCategoriesGroup_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__settingsAndMapCategoriesGroup_9() const { return ____settingsAndMapCategoriesGroup_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__settingsAndMapCategoriesGroup_9() { return &____settingsAndMapCategoriesGroup_9; }
	inline void set__settingsAndMapCategoriesGroup_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____settingsAndMapCategoriesGroup_9 = value;
		Il2CppCodeGenWriteBarrier((&____settingsAndMapCategoriesGroup_9), value);
	}

	inline static int32_t get_offset_of__settingsGroup_10() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____settingsGroup_10)); }
	inline UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * get__settingsGroup_10() const { return ____settingsGroup_10; }
	inline UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 ** get_address_of__settingsGroup_10() { return &____settingsGroup_10; }
	inline void set__settingsGroup_10(UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * value)
	{
		____settingsGroup_10 = value;
		Il2CppCodeGenWriteBarrier((&____settingsGroup_10), value);
	}

	inline static int32_t get_offset_of__mapCategoriesGroup_11() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____mapCategoriesGroup_11)); }
	inline UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * get__mapCategoriesGroup_11() const { return ____mapCategoriesGroup_11; }
	inline UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 ** get_address_of__mapCategoriesGroup_11() { return &____mapCategoriesGroup_11; }
	inline void set__mapCategoriesGroup_11(UIGroup_t3E1723AB180AC374C3A733F4337345E9F9AECD46 * value)
	{
		____mapCategoriesGroup_11 = value;
		Il2CppCodeGenWriteBarrier((&____mapCategoriesGroup_11), value);
	}

	inline static int32_t get_offset_of__inputGridGroup_12() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____inputGridGroup_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__inputGridGroup_12() const { return ____inputGridGroup_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__inputGridGroup_12() { return &____inputGridGroup_12; }
	inline void set__inputGridGroup_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____inputGridGroup_12 = value;
		Il2CppCodeGenWriteBarrier((&____inputGridGroup_12), value);
	}

	inline static int32_t get_offset_of__inputGridContainer_13() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____inputGridContainer_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__inputGridContainer_13() const { return ____inputGridContainer_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__inputGridContainer_13() { return &____inputGridContainer_13; }
	inline void set__inputGridContainer_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____inputGridContainer_13 = value;
		Il2CppCodeGenWriteBarrier((&____inputGridContainer_13), value);
	}

	inline static int32_t get_offset_of__inputGridHeadersGroup_14() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____inputGridHeadersGroup_14)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__inputGridHeadersGroup_14() const { return ____inputGridHeadersGroup_14; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__inputGridHeadersGroup_14() { return &____inputGridHeadersGroup_14; }
	inline void set__inputGridHeadersGroup_14(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____inputGridHeadersGroup_14 = value;
		Il2CppCodeGenWriteBarrier((&____inputGridHeadersGroup_14), value);
	}

	inline static int32_t get_offset_of__inputGridVScrollbar_15() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____inputGridVScrollbar_15)); }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * get__inputGridVScrollbar_15() const { return ____inputGridVScrollbar_15; }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 ** get_address_of__inputGridVScrollbar_15() { return &____inputGridVScrollbar_15; }
	inline void set__inputGridVScrollbar_15(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * value)
	{
		____inputGridVScrollbar_15 = value;
		Il2CppCodeGenWriteBarrier((&____inputGridVScrollbar_15), value);
	}

	inline static int32_t get_offset_of__inputGridScrollRect_16() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____inputGridScrollRect_16)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get__inputGridScrollRect_16() const { return ____inputGridScrollRect_16; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of__inputGridScrollRect_16() { return &____inputGridScrollRect_16; }
	inline void set__inputGridScrollRect_16(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		____inputGridScrollRect_16 = value;
		Il2CppCodeGenWriteBarrier((&____inputGridScrollRect_16), value);
	}

	inline static int32_t get_offset_of__inputGridInnerGroup_17() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____inputGridInnerGroup_17)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__inputGridInnerGroup_17() const { return ____inputGridInnerGroup_17; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__inputGridInnerGroup_17() { return &____inputGridInnerGroup_17; }
	inline void set__inputGridInnerGroup_17(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____inputGridInnerGroup_17 = value;
		Il2CppCodeGenWriteBarrier((&____inputGridInnerGroup_17), value);
	}

	inline static int32_t get_offset_of__controllerNameLabel_18() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____controllerNameLabel_18)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__controllerNameLabel_18() const { return ____controllerNameLabel_18; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__controllerNameLabel_18() { return &____controllerNameLabel_18; }
	inline void set__controllerNameLabel_18(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____controllerNameLabel_18 = value;
		Il2CppCodeGenWriteBarrier((&____controllerNameLabel_18), value);
	}

	inline static int32_t get_offset_of__removeControllerButton_19() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____removeControllerButton_19)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__removeControllerButton_19() const { return ____removeControllerButton_19; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__removeControllerButton_19() { return &____removeControllerButton_19; }
	inline void set__removeControllerButton_19(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____removeControllerButton_19 = value;
		Il2CppCodeGenWriteBarrier((&____removeControllerButton_19), value);
	}

	inline static int32_t get_offset_of__assignControllerButton_20() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____assignControllerButton_20)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__assignControllerButton_20() const { return ____assignControllerButton_20; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__assignControllerButton_20() { return &____assignControllerButton_20; }
	inline void set__assignControllerButton_20(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____assignControllerButton_20 = value;
		Il2CppCodeGenWriteBarrier((&____assignControllerButton_20), value);
	}

	inline static int32_t get_offset_of__calibrateControllerButton_21() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____calibrateControllerButton_21)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__calibrateControllerButton_21() const { return ____calibrateControllerButton_21; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__calibrateControllerButton_21() { return &____calibrateControllerButton_21; }
	inline void set__calibrateControllerButton_21(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____calibrateControllerButton_21 = value;
		Il2CppCodeGenWriteBarrier((&____calibrateControllerButton_21), value);
	}

	inline static int32_t get_offset_of__doneButton_22() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____doneButton_22)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__doneButton_22() const { return ____doneButton_22; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__doneButton_22() { return &____doneButton_22; }
	inline void set__doneButton_22(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____doneButton_22 = value;
		Il2CppCodeGenWriteBarrier((&____doneButton_22), value);
	}

	inline static int32_t get_offset_of__restoreDefaultsButton_23() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____restoreDefaultsButton_23)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__restoreDefaultsButton_23() const { return ____restoreDefaultsButton_23; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__restoreDefaultsButton_23() { return &____restoreDefaultsButton_23; }
	inline void set__restoreDefaultsButton_23(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____restoreDefaultsButton_23 = value;
		Il2CppCodeGenWriteBarrier((&____restoreDefaultsButton_23), value);
	}

	inline static int32_t get_offset_of__defaultSelection_24() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____defaultSelection_24)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get__defaultSelection_24() const { return ____defaultSelection_24; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of__defaultSelection_24() { return &____defaultSelection_24; }
	inline void set__defaultSelection_24(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		____defaultSelection_24 = value;
		Il2CppCodeGenWriteBarrier((&____defaultSelection_24), value);
	}

	inline static int32_t get_offset_of__fixedSelectableUIElements_25() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____fixedSelectableUIElements_25)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get__fixedSelectableUIElements_25() const { return ____fixedSelectableUIElements_25; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of__fixedSelectableUIElements_25() { return &____fixedSelectableUIElements_25; }
	inline void set__fixedSelectableUIElements_25(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		____fixedSelectableUIElements_25 = value;
		Il2CppCodeGenWriteBarrier((&____fixedSelectableUIElements_25), value);
	}

	inline static int32_t get_offset_of__mainBackgroundImage_26() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ____mainBackgroundImage_26)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__mainBackgroundImage_26() const { return ____mainBackgroundImage_26; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__mainBackgroundImage_26() { return &____mainBackgroundImage_26; }
	inline void set__mainBackgroundImage_26(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____mainBackgroundImage_26 = value;
		Il2CppCodeGenWriteBarrier((&____mainBackgroundImage_26), value);
	}

	inline static int32_t get_offset_of_U3CinputGridLayoutElementU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ___U3CinputGridLayoutElementU3Ek__BackingField_27)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_U3CinputGridLayoutElementU3Ek__BackingField_27() const { return ___U3CinputGridLayoutElementU3Ek__BackingField_27; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_U3CinputGridLayoutElementU3Ek__BackingField_27() { return &___U3CinputGridLayoutElementU3Ek__BackingField_27; }
	inline void set_U3CinputGridLayoutElementU3Ek__BackingField_27(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___U3CinputGridLayoutElementU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputGridLayoutElementU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CinputGridActionColumnU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ___U3CinputGridActionColumnU3Ek__BackingField_28)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CinputGridActionColumnU3Ek__BackingField_28() const { return ___U3CinputGridActionColumnU3Ek__BackingField_28; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CinputGridActionColumnU3Ek__BackingField_28() { return &___U3CinputGridActionColumnU3Ek__BackingField_28; }
	inline void set_U3CinputGridActionColumnU3Ek__BackingField_28(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CinputGridActionColumnU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputGridActionColumnU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CinputGridKeyboardColumnU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ___U3CinputGridKeyboardColumnU3Ek__BackingField_29)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CinputGridKeyboardColumnU3Ek__BackingField_29() const { return ___U3CinputGridKeyboardColumnU3Ek__BackingField_29; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CinputGridKeyboardColumnU3Ek__BackingField_29() { return &___U3CinputGridKeyboardColumnU3Ek__BackingField_29; }
	inline void set_U3CinputGridKeyboardColumnU3Ek__BackingField_29(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CinputGridKeyboardColumnU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputGridKeyboardColumnU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CinputGridMouseColumnU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ___U3CinputGridMouseColumnU3Ek__BackingField_30)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CinputGridMouseColumnU3Ek__BackingField_30() const { return ___U3CinputGridMouseColumnU3Ek__BackingField_30; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CinputGridMouseColumnU3Ek__BackingField_30() { return &___U3CinputGridMouseColumnU3Ek__BackingField_30; }
	inline void set_U3CinputGridMouseColumnU3Ek__BackingField_30(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CinputGridMouseColumnU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputGridMouseColumnU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CinputGridControllerColumnU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ___U3CinputGridControllerColumnU3Ek__BackingField_31)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CinputGridControllerColumnU3Ek__BackingField_31() const { return ___U3CinputGridControllerColumnU3Ek__BackingField_31; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CinputGridControllerColumnU3Ek__BackingField_31() { return &___U3CinputGridControllerColumnU3Ek__BackingField_31; }
	inline void set_U3CinputGridControllerColumnU3Ek__BackingField_31(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CinputGridControllerColumnU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputGridControllerColumnU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_U3CinputGridHeader1U3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ___U3CinputGridHeader1U3Ek__BackingField_32)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CinputGridHeader1U3Ek__BackingField_32() const { return ___U3CinputGridHeader1U3Ek__BackingField_32; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CinputGridHeader1U3Ek__BackingField_32() { return &___U3CinputGridHeader1U3Ek__BackingField_32; }
	inline void set_U3CinputGridHeader1U3Ek__BackingField_32(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CinputGridHeader1U3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputGridHeader1U3Ek__BackingField_32), value);
	}

	inline static int32_t get_offset_of_U3CinputGridHeader2U3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ___U3CinputGridHeader2U3Ek__BackingField_33)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CinputGridHeader2U3Ek__BackingField_33() const { return ___U3CinputGridHeader2U3Ek__BackingField_33; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CinputGridHeader2U3Ek__BackingField_33() { return &___U3CinputGridHeader2U3Ek__BackingField_33; }
	inline void set_U3CinputGridHeader2U3Ek__BackingField_33(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CinputGridHeader2U3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputGridHeader2U3Ek__BackingField_33), value);
	}

	inline static int32_t get_offset_of_U3CinputGridHeader3U3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ___U3CinputGridHeader3U3Ek__BackingField_34)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CinputGridHeader3U3Ek__BackingField_34() const { return ___U3CinputGridHeader3U3Ek__BackingField_34; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CinputGridHeader3U3Ek__BackingField_34() { return &___U3CinputGridHeader3U3Ek__BackingField_34; }
	inline void set_U3CinputGridHeader3U3Ek__BackingField_34(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CinputGridHeader3U3Ek__BackingField_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputGridHeader3U3Ek__BackingField_34), value);
	}

	inline static int32_t get_offset_of_U3CinputGridHeader4U3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163, ___U3CinputGridHeader4U3Ek__BackingField_35)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CinputGridHeader4U3Ek__BackingField_35() const { return ___U3CinputGridHeader4U3Ek__BackingField_35; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CinputGridHeader4U3Ek__BackingField_35() { return &___U3CinputGridHeader4U3Ek__BackingField_35; }
	inline void set_U3CinputGridHeader4U3Ek__BackingField_35(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CinputGridHeader4U3Ek__BackingField_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputGridHeader4U3Ek__BackingField_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCES_T63B1022C3D9DF66B0A7AEE0E3A102414E7455163_H
#ifndef WINDOWMANAGER_T3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4_H
#define WINDOWMANAGER_T3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_WindowManager
struct  WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.Window> Rewired.UI.ControlMapper.ControlMapper_WindowManager::windows
	List_1_t36BA7DF9E75C719662F145AC61EA523A5EE33702 * ___windows_0;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_WindowManager::windowPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___windowPrefab_1;
	// UnityEngine.Transform Rewired.UI.ControlMapper.ControlMapper_WindowManager::parent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parent_2;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper_WindowManager::fader
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___fader_3;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_WindowManager::idCounter
	int32_t ___idCounter_4;

public:
	inline static int32_t get_offset_of_windows_0() { return static_cast<int32_t>(offsetof(WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4, ___windows_0)); }
	inline List_1_t36BA7DF9E75C719662F145AC61EA523A5EE33702 * get_windows_0() const { return ___windows_0; }
	inline List_1_t36BA7DF9E75C719662F145AC61EA523A5EE33702 ** get_address_of_windows_0() { return &___windows_0; }
	inline void set_windows_0(List_1_t36BA7DF9E75C719662F145AC61EA523A5EE33702 * value)
	{
		___windows_0 = value;
		Il2CppCodeGenWriteBarrier((&___windows_0), value);
	}

	inline static int32_t get_offset_of_windowPrefab_1() { return static_cast<int32_t>(offsetof(WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4, ___windowPrefab_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_windowPrefab_1() const { return ___windowPrefab_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_windowPrefab_1() { return &___windowPrefab_1; }
	inline void set_windowPrefab_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___windowPrefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___windowPrefab_1), value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4, ___parent_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_parent_2() const { return ___parent_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___parent_2), value);
	}

	inline static int32_t get_offset_of_fader_3() { return static_cast<int32_t>(offsetof(WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4, ___fader_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_fader_3() const { return ___fader_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_fader_3() { return &___fader_3; }
	inline void set_fader_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___fader_3 = value;
		Il2CppCodeGenWriteBarrier((&___fader_3), value);
	}

	inline static int32_t get_offset_of_idCounter_4() { return static_cast<int32_t>(offsetof(WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4, ___idCounter_4)); }
	inline int32_t get_idCounter_4() const { return ___idCounter_4; }
	inline int32_t* get_address_of_idCounter_4() { return &___idCounter_4; }
	inline void set_idCounter_4(int32_t value)
	{
		___idCounter_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWMANAGER_T3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4_H
#ifndef U3CONFINISHSUBMITU3ED__51_T3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9_H
#define U3CONFINISHSUBMITU3ED__51_T3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.CustomButton_<OnFinishSubmit>d__51
struct  U3COnFinishSubmitU3Ed__51_t3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9  : public RuntimeObject
{
public:
	// System.Int32 Rewired.UI.ControlMapper.CustomButton_<OnFinishSubmit>d__51::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Rewired.UI.ControlMapper.CustomButton_<OnFinishSubmit>d__51::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Rewired.UI.ControlMapper.CustomButton Rewired.UI.ControlMapper.CustomButton_<OnFinishSubmit>d__51::<>4__this
	CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533 * ___U3CU3E4__this_2;
	// System.Single Rewired.UI.ControlMapper.CustomButton_<OnFinishSubmit>d__51::<fadeTime>5__1
	float ___U3CfadeTimeU3E5__1_3;
	// System.Single Rewired.UI.ControlMapper.CustomButton_<OnFinishSubmit>d__51::<elapsedTime>5__2
	float ___U3CelapsedTimeU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnFinishSubmitU3Ed__51_t3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3COnFinishSubmitU3Ed__51_t3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnFinishSubmitU3Ed__51_t3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9, ___U3CU3E4__this_2)); }
	inline CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CfadeTimeU3E5__1_3() { return static_cast<int32_t>(offsetof(U3COnFinishSubmitU3Ed__51_t3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9, ___U3CfadeTimeU3E5__1_3)); }
	inline float get_U3CfadeTimeU3E5__1_3() const { return ___U3CfadeTimeU3E5__1_3; }
	inline float* get_address_of_U3CfadeTimeU3E5__1_3() { return &___U3CfadeTimeU3E5__1_3; }
	inline void set_U3CfadeTimeU3E5__1_3(float value)
	{
		___U3CfadeTimeU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3COnFinishSubmitU3Ed__51_t3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9, ___U3CelapsedTimeU3E5__2_4)); }
	inline float get_U3CelapsedTimeU3E5__2_4() const { return ___U3CelapsedTimeU3E5__2_4; }
	inline float* get_address_of_U3CelapsedTimeU3E5__2_4() { return &___U3CelapsedTimeU3E5__2_4; }
	inline void set_U3CelapsedTimeU3E5__2_4(float value)
	{
		___U3CelapsedTimeU3E5__2_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONFINISHSUBMITU3ED__51_T3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9_H
#ifndef U3CU3EC__DISPLAYCLASS26_0_T8A7360706353E21CFBD6D9FE041BECFC92951701_H
#define U3CU3EC__DISPLAYCLASS26_0_T8A7360706353E21CFBD6D9FE041BECFC92951701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.InputBehaviorWindow_<>c__DisplayClass26_0
struct  U3CU3Ec__DisplayClass26_0_t8A7360706353E21CFBD6D9FE041BECFC92951701  : public RuntimeObject
{
public:
	// System.Action`3<System.Int32,System.Int32,System.Single> Rewired.UI.ControlMapper.InputBehaviorWindow_<>c__DisplayClass26_0::valueChangedCallback
	Action_3_t3C1A9DB9F42AE7E5CF06D50EDFD2584132EE1BFF * ___valueChangedCallback_0;
	// System.Int32 Rewired.UI.ControlMapper.InputBehaviorWindow_<>c__DisplayClass26_0::inputBehaviorId
	int32_t ___inputBehaviorId_1;
	// System.Action`2<System.Int32,System.Int32> Rewired.UI.ControlMapper.InputBehaviorWindow_<>c__DisplayClass26_0::cancelCallback
	Action_2_t7410D2490D6164955944AF42E135EB85B0551457 * ___cancelCallback_2;

public:
	inline static int32_t get_offset_of_valueChangedCallback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_t8A7360706353E21CFBD6D9FE041BECFC92951701, ___valueChangedCallback_0)); }
	inline Action_3_t3C1A9DB9F42AE7E5CF06D50EDFD2584132EE1BFF * get_valueChangedCallback_0() const { return ___valueChangedCallback_0; }
	inline Action_3_t3C1A9DB9F42AE7E5CF06D50EDFD2584132EE1BFF ** get_address_of_valueChangedCallback_0() { return &___valueChangedCallback_0; }
	inline void set_valueChangedCallback_0(Action_3_t3C1A9DB9F42AE7E5CF06D50EDFD2584132EE1BFF * value)
	{
		___valueChangedCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedCallback_0), value);
	}

	inline static int32_t get_offset_of_inputBehaviorId_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_t8A7360706353E21CFBD6D9FE041BECFC92951701, ___inputBehaviorId_1)); }
	inline int32_t get_inputBehaviorId_1() const { return ___inputBehaviorId_1; }
	inline int32_t* get_address_of_inputBehaviorId_1() { return &___inputBehaviorId_1; }
	inline void set_inputBehaviorId_1(int32_t value)
	{
		___inputBehaviorId_1 = value;
	}

	inline static int32_t get_offset_of_cancelCallback_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_t8A7360706353E21CFBD6D9FE041BECFC92951701, ___cancelCallback_2)); }
	inline Action_2_t7410D2490D6164955944AF42E135EB85B0551457 * get_cancelCallback_2() const { return ___cancelCallback_2; }
	inline Action_2_t7410D2490D6164955944AF42E135EB85B0551457 ** get_address_of_cancelCallback_2() { return &___cancelCallback_2; }
	inline void set_cancelCallback_2(Action_2_t7410D2490D6164955944AF42E135EB85B0551457 * value)
	{
		___cancelCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___cancelCallback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS26_0_T8A7360706353E21CFBD6D9FE041BECFC92951701_H
#ifndef INPUTBEHAVIORINFO_T386E3681EC6A58C540468D3E2FFF1E4A9A386686_H
#define INPUTBEHAVIORINFO_T386E3681EC6A58C540468D3E2FFF1E4A9A386686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.InputBehaviorWindow_InputBehaviorInfo
struct  InputBehaviorInfo_t386E3681EC6A58C540468D3E2FFF1E4A9A386686  : public RuntimeObject
{
public:
	// Rewired.InputBehavior Rewired.UI.ControlMapper.InputBehaviorWindow_InputBehaviorInfo::_inputBehavior
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B * ____inputBehavior_0;
	// Rewired.UI.ControlMapper.UIControlSet Rewired.UI.ControlMapper.InputBehaviorWindow_InputBehaviorInfo::_controlSet
	UIControlSet_t0BE4C6F06B453187D70603E003EB07A252B5C083 * ____controlSet_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.UI.ControlMapper.InputBehaviorWindow_PropertyType> Rewired.UI.ControlMapper.InputBehaviorWindow_InputBehaviorInfo::idToProperty
	Dictionary_2_t8C6A70CB8E2C9D90733F4EA648B407CA2CB72DBD * ___idToProperty_2;
	// Rewired.InputBehavior Rewired.UI.ControlMapper.InputBehaviorWindow_InputBehaviorInfo::copyOfOriginal
	InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B * ___copyOfOriginal_3;

public:
	inline static int32_t get_offset_of__inputBehavior_0() { return static_cast<int32_t>(offsetof(InputBehaviorInfo_t386E3681EC6A58C540468D3E2FFF1E4A9A386686, ____inputBehavior_0)); }
	inline InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B * get__inputBehavior_0() const { return ____inputBehavior_0; }
	inline InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B ** get_address_of__inputBehavior_0() { return &____inputBehavior_0; }
	inline void set__inputBehavior_0(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B * value)
	{
		____inputBehavior_0 = value;
		Il2CppCodeGenWriteBarrier((&____inputBehavior_0), value);
	}

	inline static int32_t get_offset_of__controlSet_1() { return static_cast<int32_t>(offsetof(InputBehaviorInfo_t386E3681EC6A58C540468D3E2FFF1E4A9A386686, ____controlSet_1)); }
	inline UIControlSet_t0BE4C6F06B453187D70603E003EB07A252B5C083 * get__controlSet_1() const { return ____controlSet_1; }
	inline UIControlSet_t0BE4C6F06B453187D70603E003EB07A252B5C083 ** get_address_of__controlSet_1() { return &____controlSet_1; }
	inline void set__controlSet_1(UIControlSet_t0BE4C6F06B453187D70603E003EB07A252B5C083 * value)
	{
		____controlSet_1 = value;
		Il2CppCodeGenWriteBarrier((&____controlSet_1), value);
	}

	inline static int32_t get_offset_of_idToProperty_2() { return static_cast<int32_t>(offsetof(InputBehaviorInfo_t386E3681EC6A58C540468D3E2FFF1E4A9A386686, ___idToProperty_2)); }
	inline Dictionary_2_t8C6A70CB8E2C9D90733F4EA648B407CA2CB72DBD * get_idToProperty_2() const { return ___idToProperty_2; }
	inline Dictionary_2_t8C6A70CB8E2C9D90733F4EA648B407CA2CB72DBD ** get_address_of_idToProperty_2() { return &___idToProperty_2; }
	inline void set_idToProperty_2(Dictionary_2_t8C6A70CB8E2C9D90733F4EA648B407CA2CB72DBD * value)
	{
		___idToProperty_2 = value;
		Il2CppCodeGenWriteBarrier((&___idToProperty_2), value);
	}

	inline static int32_t get_offset_of_copyOfOriginal_3() { return static_cast<int32_t>(offsetof(InputBehaviorInfo_t386E3681EC6A58C540468D3E2FFF1E4A9A386686, ___copyOfOriginal_3)); }
	inline InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B * get_copyOfOriginal_3() const { return ___copyOfOriginal_3; }
	inline InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B ** get_address_of_copyOfOriginal_3() { return &___copyOfOriginal_3; }
	inline void set_copyOfOriginal_3(InputBehavior_tA56DA8273BC72A9DC75DCC1C2EAD2C513A6F7D4B * value)
	{
		___copyOfOriginal_3 = value;
		Il2CppCodeGenWriteBarrier((&___copyOfOriginal_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBEHAVIORINFO_T386E3681EC6A58C540468D3E2FFF1E4A9A386686_H
#ifndef CUSTOMENTRY_TE994DCD35C117E456147357110B75848AF99FBFD_H
#define CUSTOMENTRY_TE994DCD35C117E456147357110B75848AF99FBFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.LanguageData_CustomEntry
struct  CustomEntry_tE994DCD35C117E456147357110B75848AF99FBFD  : public RuntimeObject
{
public:
	// System.String Rewired.UI.ControlMapper.LanguageData_CustomEntry::key
	String_t* ___key_0;
	// System.String Rewired.UI.ControlMapper.LanguageData_CustomEntry::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(CustomEntry_tE994DCD35C117E456147357110B75848AF99FBFD, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(CustomEntry_tE994DCD35C117E456147357110B75848AF99FBFD, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMENTRY_TE994DCD35C117E456147357110B75848AF99FBFD_H
#ifndef EXTERNALTOOLS_T3F3B878EE09D19AD46984E0E3F2BF98F497CCAF4_H
#define EXTERNALTOOLS_T3F3B878EE09D19AD46984E0E3F2BF98F497CCAF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.ExternalTools
struct  ExternalTools_t3F3B878EE09D19AD46984E0E3F2BF98F497CCAF4  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Utils.ExternalTools::_isEditorPaused
	bool ____isEditorPaused_0;
	// System.Action`1<System.Boolean> Rewired.Utils.ExternalTools::_EditorPausedStateChangedEvent
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ____EditorPausedStateChangedEvent_1;
	// System.Action`2<System.UInt32,System.Boolean> Rewired.Utils.ExternalTools::XboxOneInput_OnGamepadStateChange
	Action_2_t4FE7EF9326184201F0B18A3B64A3EDDAE7A50EF7 * ___XboxOneInput_OnGamepadStateChange_2;

public:
	inline static int32_t get_offset_of__isEditorPaused_0() { return static_cast<int32_t>(offsetof(ExternalTools_t3F3B878EE09D19AD46984E0E3F2BF98F497CCAF4, ____isEditorPaused_0)); }
	inline bool get__isEditorPaused_0() const { return ____isEditorPaused_0; }
	inline bool* get_address_of__isEditorPaused_0() { return &____isEditorPaused_0; }
	inline void set__isEditorPaused_0(bool value)
	{
		____isEditorPaused_0 = value;
	}

	inline static int32_t get_offset_of__EditorPausedStateChangedEvent_1() { return static_cast<int32_t>(offsetof(ExternalTools_t3F3B878EE09D19AD46984E0E3F2BF98F497CCAF4, ____EditorPausedStateChangedEvent_1)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get__EditorPausedStateChangedEvent_1() const { return ____EditorPausedStateChangedEvent_1; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of__EditorPausedStateChangedEvent_1() { return &____EditorPausedStateChangedEvent_1; }
	inline void set__EditorPausedStateChangedEvent_1(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		____EditorPausedStateChangedEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&____EditorPausedStateChangedEvent_1), value);
	}

	inline static int32_t get_offset_of_XboxOneInput_OnGamepadStateChange_2() { return static_cast<int32_t>(offsetof(ExternalTools_t3F3B878EE09D19AD46984E0E3F2BF98F497CCAF4, ___XboxOneInput_OnGamepadStateChange_2)); }
	inline Action_2_t4FE7EF9326184201F0B18A3B64A3EDDAE7A50EF7 * get_XboxOneInput_OnGamepadStateChange_2() const { return ___XboxOneInput_OnGamepadStateChange_2; }
	inline Action_2_t4FE7EF9326184201F0B18A3B64A3EDDAE7A50EF7 ** get_address_of_XboxOneInput_OnGamepadStateChange_2() { return &___XboxOneInput_OnGamepadStateChange_2; }
	inline void set_XboxOneInput_OnGamepadStateChange_2(Action_2_t4FE7EF9326184201F0B18A3B64A3EDDAE7A50EF7 * value)
	{
		___XboxOneInput_OnGamepadStateChange_2 = value;
		Il2CppCodeGenWriteBarrier((&___XboxOneInput_OnGamepadStateChange_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNALTOOLS_T3F3B878EE09D19AD46984E0E3F2BF98F497CCAF4_H
#ifndef INSTRUCTION_T84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C_H
#define INSTRUCTION_T84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Instruction
struct  Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C  : public RuntimeObject
{
public:
	// System.String Strackaline.Instruction::aim
	String_t* ___aim_0;
	// System.String Strackaline.Instruction::weight
	String_t* ___weight_1;
	// System.String Strackaline.Instruction::toHole
	String_t* ___toHole_2;
	// System.String Strackaline.Instruction::elevation
	String_t* ___elevation_3;

public:
	inline static int32_t get_offset_of_aim_0() { return static_cast<int32_t>(offsetof(Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C, ___aim_0)); }
	inline String_t* get_aim_0() const { return ___aim_0; }
	inline String_t** get_address_of_aim_0() { return &___aim_0; }
	inline void set_aim_0(String_t* value)
	{
		___aim_0 = value;
		Il2CppCodeGenWriteBarrier((&___aim_0), value);
	}

	inline static int32_t get_offset_of_weight_1() { return static_cast<int32_t>(offsetof(Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C, ___weight_1)); }
	inline String_t* get_weight_1() const { return ___weight_1; }
	inline String_t** get_address_of_weight_1() { return &___weight_1; }
	inline void set_weight_1(String_t* value)
	{
		___weight_1 = value;
		Il2CppCodeGenWriteBarrier((&___weight_1), value);
	}

	inline static int32_t get_offset_of_toHole_2() { return static_cast<int32_t>(offsetof(Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C, ___toHole_2)); }
	inline String_t* get_toHole_2() const { return ___toHole_2; }
	inline String_t** get_address_of_toHole_2() { return &___toHole_2; }
	inline void set_toHole_2(String_t* value)
	{
		___toHole_2 = value;
		Il2CppCodeGenWriteBarrier((&___toHole_2), value);
	}

	inline static int32_t get_offset_of_elevation_3() { return static_cast<int32_t>(offsetof(Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C, ___elevation_3)); }
	inline String_t* get_elevation_3() const { return ___elevation_3; }
	inline String_t** get_address_of_elevation_3() { return &___elevation_3; }
	inline void set_elevation_3(String_t* value)
	{
		___elevation_3 = value;
		Il2CppCodeGenWriteBarrier((&___elevation_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTRUCTION_T84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C_H
#ifndef MEMBERDETAIL_T135B5CC18053FA64F1747F427A6CECBEE234F08B_H
#define MEMBERDETAIL_T135B5CC18053FA64F1747F427A6CECBEE234F08B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.MemberDetail
struct  MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B  : public RuntimeObject
{
public:
	// System.String Strackaline.MemberDetail::emailAddress
	String_t* ___emailAddress_0;
	// Strackaline.Roles[] Strackaline.MemberDetail::roles
	RolesU5BU5D_t8488CDD55C543C867E336D439B8B2EABC84E077E* ___roles_1;
	// Strackaline.UserCourse[] Strackaline.MemberDetail::courses
	UserCourseU5BU5D_t7ED59DB157B099FEDE78B70F593E0F2DE12D39EE* ___courses_2;
	// Strackaline.UserPrinter[] Strackaline.MemberDetail::printers
	UserPrinterU5BU5D_tFA1FBB04FD3DF56366C2A05D605642AB3D1F9601* ___printers_3;

public:
	inline static int32_t get_offset_of_emailAddress_0() { return static_cast<int32_t>(offsetof(MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B, ___emailAddress_0)); }
	inline String_t* get_emailAddress_0() const { return ___emailAddress_0; }
	inline String_t** get_address_of_emailAddress_0() { return &___emailAddress_0; }
	inline void set_emailAddress_0(String_t* value)
	{
		___emailAddress_0 = value;
		Il2CppCodeGenWriteBarrier((&___emailAddress_0), value);
	}

	inline static int32_t get_offset_of_roles_1() { return static_cast<int32_t>(offsetof(MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B, ___roles_1)); }
	inline RolesU5BU5D_t8488CDD55C543C867E336D439B8B2EABC84E077E* get_roles_1() const { return ___roles_1; }
	inline RolesU5BU5D_t8488CDD55C543C867E336D439B8B2EABC84E077E** get_address_of_roles_1() { return &___roles_1; }
	inline void set_roles_1(RolesU5BU5D_t8488CDD55C543C867E336D439B8B2EABC84E077E* value)
	{
		___roles_1 = value;
		Il2CppCodeGenWriteBarrier((&___roles_1), value);
	}

	inline static int32_t get_offset_of_courses_2() { return static_cast<int32_t>(offsetof(MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B, ___courses_2)); }
	inline UserCourseU5BU5D_t7ED59DB157B099FEDE78B70F593E0F2DE12D39EE* get_courses_2() const { return ___courses_2; }
	inline UserCourseU5BU5D_t7ED59DB157B099FEDE78B70F593E0F2DE12D39EE** get_address_of_courses_2() { return &___courses_2; }
	inline void set_courses_2(UserCourseU5BU5D_t7ED59DB157B099FEDE78B70F593E0F2DE12D39EE* value)
	{
		___courses_2 = value;
		Il2CppCodeGenWriteBarrier((&___courses_2), value);
	}

	inline static int32_t get_offset_of_printers_3() { return static_cast<int32_t>(offsetof(MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B, ___printers_3)); }
	inline UserPrinterU5BU5D_tFA1FBB04FD3DF56366C2A05D605642AB3D1F9601* get_printers_3() const { return ___printers_3; }
	inline UserPrinterU5BU5D_tFA1FBB04FD3DF56366C2A05D605642AB3D1F9601** get_address_of_printers_3() { return &___printers_3; }
	inline void set_printers_3(UserPrinterU5BU5D_tFA1FBB04FD3DF56366C2A05D605642AB3D1F9601* value)
	{
		___printers_3 = value;
		Il2CppCodeGenWriteBarrier((&___printers_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERDETAIL_T135B5CC18053FA64F1747F427A6CECBEE234F08B_H
#ifndef REGION_T6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E_H
#define REGION_T6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Region
struct  Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E  : public RuntimeObject
{
public:
	// Strackaline.RegionData[] Strackaline.Region::value
	RegionDataU5BU5D_t6B7197A1556950E72F70CA6FBE5821D8B8716119* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E, ___value_0)); }
	inline RegionDataU5BU5D_t6B7197A1556950E72F70CA6FBE5821D8B8716119* get_value_0() const { return ___value_0; }
	inline RegionDataU5BU5D_t6B7197A1556950E72F70CA6FBE5821D8B8716119** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RegionDataU5BU5D_t6B7197A1556950E72F70CA6FBE5821D8B8716119* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGION_T6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E_H
#ifndef REGIONDATA_T79DE0DE7F085973DEFB23878C33B7C301777829F_H
#define REGIONDATA_T79DE0DE7F085973DEFB23878C33B7C301777829F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.RegionData
struct  RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F  : public RuntimeObject
{
public:
	// System.String Strackaline.RegionData::name
	String_t* ___name_0;
	// System.String Strackaline.RegionData::regionCode
	String_t* ___regionCode_1;
	// System.String Strackaline.RegionData::countryCode
	String_t* ___countryCode_2;
	// System.String Strackaline.RegionData::courseCount
	String_t* ___courseCount_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_regionCode_1() { return static_cast<int32_t>(offsetof(RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F, ___regionCode_1)); }
	inline String_t* get_regionCode_1() const { return ___regionCode_1; }
	inline String_t** get_address_of_regionCode_1() { return &___regionCode_1; }
	inline void set_regionCode_1(String_t* value)
	{
		___regionCode_1 = value;
		Il2CppCodeGenWriteBarrier((&___regionCode_1), value);
	}

	inline static int32_t get_offset_of_countryCode_2() { return static_cast<int32_t>(offsetof(RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F, ___countryCode_2)); }
	inline String_t* get_countryCode_2() const { return ___countryCode_2; }
	inline String_t** get_address_of_countryCode_2() { return &___countryCode_2; }
	inline void set_countryCode_2(String_t* value)
	{
		___countryCode_2 = value;
		Il2CppCodeGenWriteBarrier((&___countryCode_2), value);
	}

	inline static int32_t get_offset_of_courseCount_3() { return static_cast<int32_t>(offsetof(RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F, ___courseCount_3)); }
	inline String_t* get_courseCount_3() const { return ___courseCount_3; }
	inline String_t** get_address_of_courseCount_3() { return &___courseCount_3; }
	inline void set_courseCount_3(String_t* value)
	{
		___courseCount_3 = value;
		Il2CppCodeGenWriteBarrier((&___courseCount_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONDATA_T79DE0DE7F085973DEFB23878C33B7C301777829F_H
#ifndef ROLES_T1C83194AC6C88701A7E6EBA86C531898D094B34F_H
#define ROLES_T1C83194AC6C88701A7E6EBA86C531898D094B34F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.Roles
struct  Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.Roles::roleId
	int32_t ___roleId_0;
	// System.String Strackaline.Roles::name
	String_t* ___name_1;
	// System.String Strackaline.Roles::domain
	String_t* ___domain_2;

public:
	inline static int32_t get_offset_of_roleId_0() { return static_cast<int32_t>(offsetof(Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F, ___roleId_0)); }
	inline int32_t get_roleId_0() const { return ___roleId_0; }
	inline int32_t* get_address_of_roleId_0() { return &___roleId_0; }
	inline void set_roleId_0(int32_t value)
	{
		___roleId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_domain_2() { return static_cast<int32_t>(offsetof(Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F, ___domain_2)); }
	inline String_t* get_domain_2() const { return ___domain_2; }
	inline String_t** get_address_of_domain_2() { return &___domain_2; }
	inline void set_domain_2(String_t* value)
	{
		___domain_2 = value;
		Il2CppCodeGenWriteBarrier((&___domain_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROLES_T1C83194AC6C88701A7E6EBA86C531898D094B34F_H
#ifndef USER_TDA60B60B80C63C1D6DD68A774400AB1263BD2371_H
#define USER_TDA60B60B80C63C1D6DD68A774400AB1263BD2371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.User
struct  User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371  : public RuntimeObject
{
public:
	// System.String Strackaline.User::name
	String_t* ___name_0;
	// System.String Strackaline.User::roboGreenID
	String_t* ___roboGreenID_1;
	// System.Int32 Strackaline.User::memberId
	int32_t ___memberId_2;
	// System.String Strackaline.User::token
	String_t* ___token_3;
	// Strackaline.MemberDetail Strackaline.User::memberDetail
	MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B * ___memberDetail_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_roboGreenID_1() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___roboGreenID_1)); }
	inline String_t* get_roboGreenID_1() const { return ___roboGreenID_1; }
	inline String_t** get_address_of_roboGreenID_1() { return &___roboGreenID_1; }
	inline void set_roboGreenID_1(String_t* value)
	{
		___roboGreenID_1 = value;
		Il2CppCodeGenWriteBarrier((&___roboGreenID_1), value);
	}

	inline static int32_t get_offset_of_memberId_2() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___memberId_2)); }
	inline int32_t get_memberId_2() const { return ___memberId_2; }
	inline int32_t* get_address_of_memberId_2() { return &___memberId_2; }
	inline void set_memberId_2(int32_t value)
	{
		___memberId_2 = value;
	}

	inline static int32_t get_offset_of_token_3() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___token_3)); }
	inline String_t* get_token_3() const { return ___token_3; }
	inline String_t** get_address_of_token_3() { return &___token_3; }
	inline void set_token_3(String_t* value)
	{
		___token_3 = value;
		Il2CppCodeGenWriteBarrier((&___token_3), value);
	}

	inline static int32_t get_offset_of_memberDetail_4() { return static_cast<int32_t>(offsetof(User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371, ___memberDetail_4)); }
	inline MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B * get_memberDetail_4() const { return ___memberDetail_4; }
	inline MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B ** get_address_of_memberDetail_4() { return &___memberDetail_4; }
	inline void set_memberDetail_4(MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B * value)
	{
		___memberDetail_4 = value;
		Il2CppCodeGenWriteBarrier((&___memberDetail_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USER_TDA60B60B80C63C1D6DD68A774400AB1263BD2371_H
#ifndef USERCOURSE_T7034C2CD56492ABBB9662949E17FF739DD286D70_H
#define USERCOURSE_T7034C2CD56492ABBB9662949E17FF739DD286D70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.UserCourse
struct  UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.UserCourse::courseId
	int32_t ___courseId_0;
	// System.Int32 Strackaline.UserCourse::sourceId
	int32_t ___sourceId_1;
	// System.String Strackaline.UserCourse::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_courseId_0() { return static_cast<int32_t>(offsetof(UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70, ___courseId_0)); }
	inline int32_t get_courseId_0() const { return ___courseId_0; }
	inline int32_t* get_address_of_courseId_0() { return &___courseId_0; }
	inline void set_courseId_0(int32_t value)
	{
		___courseId_0 = value;
	}

	inline static int32_t get_offset_of_sourceId_1() { return static_cast<int32_t>(offsetof(UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70, ___sourceId_1)); }
	inline int32_t get_sourceId_1() const { return ___sourceId_1; }
	inline int32_t* get_address_of_sourceId_1() { return &___sourceId_1; }
	inline void set_sourceId_1(int32_t value)
	{
		___sourceId_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERCOURSE_T7034C2CD56492ABBB9662949E17FF739DD286D70_H
#ifndef USERPRINTER_TF6F16EB31422E9716816C97ED8D440F25328A838_H
#define USERPRINTER_TF6F16EB31422E9716816C97ED8D440F25328A838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strackaline.UserPrinter
struct  UserPrinter_tF6F16EB31422E9716816C97ED8D440F25328A838  : public RuntimeObject
{
public:
	// System.Int32 Strackaline.UserPrinter::printerId
	int32_t ___printerId_0;
	// System.String Strackaline.UserPrinter::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_printerId_0() { return static_cast<int32_t>(offsetof(UserPrinter_tF6F16EB31422E9716816C97ED8D440F25328A838, ___printerId_0)); }
	inline int32_t get_printerId_0() const { return ___printerId_0; }
	inline int32_t* get_address_of_printerId_0() { return &___printerId_0; }
	inline void set_printerId_0(int32_t value)
	{
		___printerId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(UserPrinter_tF6F16EB31422E9716816C97ED8D440F25328A838, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERPRINTER_TF6F16EB31422E9716816C97ED8D440F25328A838_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ABSTRACTEVENTDATA_T636F385820C291DAE25897BCEB4FBCADDA3B75F6_H
#define ABSTRACTEVENTDATA_T636F385820C291DAE25897BCEB4FBCADDA3B75F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T636F385820C291DAE25897BCEB4FBCADDA3B75F6_H
#ifndef GUIBUTTON_TF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C_H
#define GUIBUTTON_TF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_GUIButton
struct  GUIButton_tF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C  : public GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIBUTTON_TF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C_H
#ifndef GUIINPUTFIELD_TF982ED4AB954FC21D0AE7F35EAB4C8234BDF4F20_H
#define GUIINPUTFIELD_TF982ED4AB954FC21D0AE7F35EAB4C8234BDF4F20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_GUIInputField
struct  GUIInputField_tF982ED4AB954FC21D0AE7F35EAB4C8234BDF4F20  : public GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9
{
public:
	// Rewired.UI.ControlMapper.ControlMapper_GUIToggle Rewired.UI.ControlMapper.ControlMapper_GUIInputField::<toggle>k__BackingField
	GUIToggle_tD90790241D2ED7478964399C10A03986E91C88F9 * ___U3CtoggleU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CtoggleU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GUIInputField_tF982ED4AB954FC21D0AE7F35EAB4C8234BDF4F20, ___U3CtoggleU3Ek__BackingField_7)); }
	inline GUIToggle_tD90790241D2ED7478964399C10A03986E91C88F9 * get_U3CtoggleU3Ek__BackingField_7() const { return ___U3CtoggleU3Ek__BackingField_7; }
	inline GUIToggle_tD90790241D2ED7478964399C10A03986E91C88F9 ** get_address_of_U3CtoggleU3Ek__BackingField_7() { return &___U3CtoggleU3Ek__BackingField_7; }
	inline void set_U3CtoggleU3Ek__BackingField_7(GUIToggle_tD90790241D2ED7478964399C10A03986E91C88F9 * value)
	{
		___U3CtoggleU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtoggleU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIINPUTFIELD_TF982ED4AB954FC21D0AE7F35EAB4C8234BDF4F20_H
#ifndef GUITOGGLE_TD90790241D2ED7478964399C10A03986E91C88F9_H
#define GUITOGGLE_TD90790241D2ED7478964399C10A03986E91C88F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_GUIToggle
struct  GUIToggle_tD90790241D2ED7478964399C10A03986E91C88F9  : public GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITOGGLE_TD90790241D2ED7478964399C10A03986E91C88F9_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#define DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifndef BASEEVENTDATA_T46C9D2AE3183A742EDE89944AF64A23DBF1B80A5_H
#define BASEEVENTDATA_T46C9D2AE3183A742EDE89944AF64A23DBF1B80A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5  : public AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5, ___m_EventSystem_1)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T46C9D2AE3183A742EDE89944AF64A23DBF1B80A5_H
#ifndef SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#define SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_HighlightedSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_PressedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_DisabledSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#define AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisRange
struct  AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652 
{
public:
	// System.Int32 Rewired.AxisRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifndef AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#define AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisSensitivityType
struct  AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A 
{
public:
	// System.Int32 Rewired.AxisSensitivityType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisSensitivityType_tB27D85F7BA693F651B640A24CDE6F530ABCB364A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSENSITIVITYTYPE_TB27D85F7BA693F651B640A24CDE6F530ABCB364A_H
#ifndef CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#define CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerElementType
struct  ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA 
{
public:
	// System.Int32 Rewired.ControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifndef CONTROLLERTEMPLATE_TB3D9364CC27D096A6AAE8B9960440B43ECAC8B98_H
#define CONTROLLERTEMPLATE_TB3D9364CC27D096A6AAE8B9960440B43ECAC8B98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerTemplate
struct  ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98  : public RuntimeObject
{
public:
	// System.String Rewired.ControllerTemplate::xzFIhZhMHNeQYGDUhfMfvvqdZQds
	String_t* ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_0;
	// System.Guid Rewired.ControllerTemplate::IVAjJNOdTXXjgnowouzEZvvrKDh
	Guid_t  ___IVAjJNOdTXXjgnowouzEZvvrKDh_1;
	// Rewired.Controller Rewired.ControllerTemplate::gcFDOBoqpaBweHMwNlbneFOEEAm
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___gcFDOBoqpaBweHMwNlbneFOEEAm_2;
	// Rewired.Utils.Classes.Data.ADictionary`2<System.Int32,Rewired.IControllerTemplateElement> Rewired.ControllerTemplate::NOXxtGrpSqMcIFZTsrJcnLlMjrR
	ADictionary_2_t68A8B0123876EA082FBDA6132BFFCD5DB743965D * ___NOXxtGrpSqMcIFZTsrJcnLlMjrR_3;
	// Rewired.Utils.Classes.Data.ADictionary`2<System.String,Rewired.IControllerTemplateElement> Rewired.ControllerTemplate::jQsjEfMifTDFgHVpZohgCksDsnei
	ADictionary_2_tD6BAFD11BFA92734BD04AC9CA5B5B7B493FAB1E7 * ___jQsjEfMifTDFgHVpZohgCksDsnei_4;
	// Rewired.IControllerTemplateElement[] Rewired.ControllerTemplate::tqrCjCDiVROjgZQMTxdrZcCDZcA
	IControllerTemplateElementU5BU5D_t1660B9BD48816BC80A9C52B74BFCF80922F1C060* ___tqrCjCDiVROjgZQMTxdrZcCDZcA_5;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.IControllerTemplateElement> Rewired.ControllerTemplate::HeiaDLCywuXsOrgGQhDKAJyFIubw
	ReadOnlyCollection_1_tBD7D8D2E9C5F8B4B0C4FF78D2202E23AC1F7F65B * ___HeiaDLCywuXsOrgGQhDKAJyFIubw_6;
	// System.Int32 Rewired.ControllerTemplate::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_7;

public:
	inline static int32_t get_offset_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_0() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_0)); }
	inline String_t* get_xzFIhZhMHNeQYGDUhfMfvvqdZQds_0() const { return ___xzFIhZhMHNeQYGDUhfMfvvqdZQds_0; }
	inline String_t** get_address_of_xzFIhZhMHNeQYGDUhfMfvvqdZQds_0() { return &___xzFIhZhMHNeQYGDUhfMfvvqdZQds_0; }
	inline void set_xzFIhZhMHNeQYGDUhfMfvvqdZQds_0(String_t* value)
	{
		___xzFIhZhMHNeQYGDUhfMfvvqdZQds_0 = value;
		Il2CppCodeGenWriteBarrier((&___xzFIhZhMHNeQYGDUhfMfvvqdZQds_0), value);
	}

	inline static int32_t get_offset_of_IVAjJNOdTXXjgnowouzEZvvrKDh_1() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___IVAjJNOdTXXjgnowouzEZvvrKDh_1)); }
	inline Guid_t  get_IVAjJNOdTXXjgnowouzEZvvrKDh_1() const { return ___IVAjJNOdTXXjgnowouzEZvvrKDh_1; }
	inline Guid_t * get_address_of_IVAjJNOdTXXjgnowouzEZvvrKDh_1() { return &___IVAjJNOdTXXjgnowouzEZvvrKDh_1; }
	inline void set_IVAjJNOdTXXjgnowouzEZvvrKDh_1(Guid_t  value)
	{
		___IVAjJNOdTXXjgnowouzEZvvrKDh_1 = value;
	}

	inline static int32_t get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_2() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___gcFDOBoqpaBweHMwNlbneFOEEAm_2)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_gcFDOBoqpaBweHMwNlbneFOEEAm_2() const { return ___gcFDOBoqpaBweHMwNlbneFOEEAm_2; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_gcFDOBoqpaBweHMwNlbneFOEEAm_2() { return &___gcFDOBoqpaBweHMwNlbneFOEEAm_2; }
	inline void set_gcFDOBoqpaBweHMwNlbneFOEEAm_2(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___gcFDOBoqpaBweHMwNlbneFOEEAm_2 = value;
		Il2CppCodeGenWriteBarrier((&___gcFDOBoqpaBweHMwNlbneFOEEAm_2), value);
	}

	inline static int32_t get_offset_of_NOXxtGrpSqMcIFZTsrJcnLlMjrR_3() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___NOXxtGrpSqMcIFZTsrJcnLlMjrR_3)); }
	inline ADictionary_2_t68A8B0123876EA082FBDA6132BFFCD5DB743965D * get_NOXxtGrpSqMcIFZTsrJcnLlMjrR_3() const { return ___NOXxtGrpSqMcIFZTsrJcnLlMjrR_3; }
	inline ADictionary_2_t68A8B0123876EA082FBDA6132BFFCD5DB743965D ** get_address_of_NOXxtGrpSqMcIFZTsrJcnLlMjrR_3() { return &___NOXxtGrpSqMcIFZTsrJcnLlMjrR_3; }
	inline void set_NOXxtGrpSqMcIFZTsrJcnLlMjrR_3(ADictionary_2_t68A8B0123876EA082FBDA6132BFFCD5DB743965D * value)
	{
		___NOXxtGrpSqMcIFZTsrJcnLlMjrR_3 = value;
		Il2CppCodeGenWriteBarrier((&___NOXxtGrpSqMcIFZTsrJcnLlMjrR_3), value);
	}

	inline static int32_t get_offset_of_jQsjEfMifTDFgHVpZohgCksDsnei_4() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___jQsjEfMifTDFgHVpZohgCksDsnei_4)); }
	inline ADictionary_2_tD6BAFD11BFA92734BD04AC9CA5B5B7B493FAB1E7 * get_jQsjEfMifTDFgHVpZohgCksDsnei_4() const { return ___jQsjEfMifTDFgHVpZohgCksDsnei_4; }
	inline ADictionary_2_tD6BAFD11BFA92734BD04AC9CA5B5B7B493FAB1E7 ** get_address_of_jQsjEfMifTDFgHVpZohgCksDsnei_4() { return &___jQsjEfMifTDFgHVpZohgCksDsnei_4; }
	inline void set_jQsjEfMifTDFgHVpZohgCksDsnei_4(ADictionary_2_tD6BAFD11BFA92734BD04AC9CA5B5B7B493FAB1E7 * value)
	{
		___jQsjEfMifTDFgHVpZohgCksDsnei_4 = value;
		Il2CppCodeGenWriteBarrier((&___jQsjEfMifTDFgHVpZohgCksDsnei_4), value);
	}

	inline static int32_t get_offset_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_5() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___tqrCjCDiVROjgZQMTxdrZcCDZcA_5)); }
	inline IControllerTemplateElementU5BU5D_t1660B9BD48816BC80A9C52B74BFCF80922F1C060* get_tqrCjCDiVROjgZQMTxdrZcCDZcA_5() const { return ___tqrCjCDiVROjgZQMTxdrZcCDZcA_5; }
	inline IControllerTemplateElementU5BU5D_t1660B9BD48816BC80A9C52B74BFCF80922F1C060** get_address_of_tqrCjCDiVROjgZQMTxdrZcCDZcA_5() { return &___tqrCjCDiVROjgZQMTxdrZcCDZcA_5; }
	inline void set_tqrCjCDiVROjgZQMTxdrZcCDZcA_5(IControllerTemplateElementU5BU5D_t1660B9BD48816BC80A9C52B74BFCF80922F1C060* value)
	{
		___tqrCjCDiVROjgZQMTxdrZcCDZcA_5 = value;
		Il2CppCodeGenWriteBarrier((&___tqrCjCDiVROjgZQMTxdrZcCDZcA_5), value);
	}

	inline static int32_t get_offset_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_6() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___HeiaDLCywuXsOrgGQhDKAJyFIubw_6)); }
	inline ReadOnlyCollection_1_tBD7D8D2E9C5F8B4B0C4FF78D2202E23AC1F7F65B * get_HeiaDLCywuXsOrgGQhDKAJyFIubw_6() const { return ___HeiaDLCywuXsOrgGQhDKAJyFIubw_6; }
	inline ReadOnlyCollection_1_tBD7D8D2E9C5F8B4B0C4FF78D2202E23AC1F7F65B ** get_address_of_HeiaDLCywuXsOrgGQhDKAJyFIubw_6() { return &___HeiaDLCywuXsOrgGQhDKAJyFIubw_6; }
	inline void set_HeiaDLCywuXsOrgGQhDKAJyFIubw_6(ReadOnlyCollection_1_tBD7D8D2E9C5F8B4B0C4FF78D2202E23AC1F7F65B * value)
	{
		___HeiaDLCywuXsOrgGQhDKAJyFIubw_6 = value;
		Il2CppCodeGenWriteBarrier((&___HeiaDLCywuXsOrgGQhDKAJyFIubw_6), value);
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_7() { return static_cast<int32_t>(offsetof(ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_7)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_7() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_7; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_7() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_7; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_7(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTEMPLATE_TB3D9364CC27D096A6AAE8B9960440B43ECAC8B98_H
#ifndef CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#define CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerType
struct  ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4 
{
public:
	// System.Int32 Rewired.ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifndef JOYSTICKINFO_T3B8053247F0090A5FD17FB477A67B9E2FD21DD1B_H
#define JOYSTICKINFO_T3B8053247F0090A5FD17FB477A67B9E2FD21DD1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo_JoystickInfo
struct  JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B  : public RuntimeObject
{
public:
	// System.Guid Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo_JoystickInfo::instanceGuid
	Guid_t  ___instanceGuid_0;
	// System.String Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo_JoystickInfo::hardwareIdentifier
	String_t* ___hardwareIdentifier_1;
	// System.Int32 Rewired.Data.UserDataStore_PlayerPrefs_ControllerAssignmentSaveInfo_JoystickInfo::id
	int32_t ___id_2;

public:
	inline static int32_t get_offset_of_instanceGuid_0() { return static_cast<int32_t>(offsetof(JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B, ___instanceGuid_0)); }
	inline Guid_t  get_instanceGuid_0() const { return ___instanceGuid_0; }
	inline Guid_t * get_address_of_instanceGuid_0() { return &___instanceGuid_0; }
	inline void set_instanceGuid_0(Guid_t  value)
	{
		___instanceGuid_0 = value;
	}

	inline static int32_t get_offset_of_hardwareIdentifier_1() { return static_cast<int32_t>(offsetof(JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B, ___hardwareIdentifier_1)); }
	inline String_t* get_hardwareIdentifier_1() const { return ___hardwareIdentifier_1; }
	inline String_t** get_address_of_hardwareIdentifier_1() { return &___hardwareIdentifier_1; }
	inline void set_hardwareIdentifier_1(String_t* value)
	{
		___hardwareIdentifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___hardwareIdentifier_1), value);
	}

	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B, ___id_2)); }
	inline int32_t get_id_2() const { return ___id_2; }
	inline int32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(int32_t value)
	{
		___id_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKINFO_T3B8053247F0090A5FD17FB477A67B9E2FD21DD1B_H
#ifndef ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#define ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentType
struct  ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394 
{
public:
	// System.Int32 Rewired.ElementAssignmentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#ifndef POINTEREVENTTYPE_T25709B330F51FD9CC5E147F69A30D7B66301FECC_H
#define POINTEREVENTTYPE_T25709B330F51FD9CC5E147F69A30D7B66301FECC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Integration.UnityUI.PointerEventType
struct  PointerEventType_t25709B330F51FD9CC5E147F69A30D7B66301FECC 
{
public:
	// System.Int32 Rewired.Integration.UnityUI.PointerEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PointerEventType_t25709B330F51FD9CC5E147F69A30D7B66301FECC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTTYPE_T25709B330F51FD9CC5E147F69A30D7B66301FECC_H
#ifndef UNITYINPUTSOURCE_T1EE49D6C3A027078D2C0643F34B72F1D713B1128_H
#define UNITYINPUTSOURCE_T1EE49D6C3A027078D2C0643F34B72F1D713B1128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Integration.UnityUI.RewiredPointerInputModule_UnityInputSource
struct  UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 Rewired.Integration.UnityUI.RewiredPointerInputModule_UnityInputSource::m_MousePosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_MousePosition_0;
	// UnityEngine.Vector2 Rewired.Integration.UnityUI.RewiredPointerInputModule_UnityInputSource::m_MousePositionPrev
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_MousePositionPrev_1;
	// System.Int32 Rewired.Integration.UnityUI.RewiredPointerInputModule_UnityInputSource::m_LastUpdatedFrame
	int32_t ___m_LastUpdatedFrame_2;

public:
	inline static int32_t get_offset_of_m_MousePosition_0() { return static_cast<int32_t>(offsetof(UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128, ___m_MousePosition_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_MousePosition_0() const { return ___m_MousePosition_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_MousePosition_0() { return &___m_MousePosition_0; }
	inline void set_m_MousePosition_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_MousePosition_0 = value;
	}

	inline static int32_t get_offset_of_m_MousePositionPrev_1() { return static_cast<int32_t>(offsetof(UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128, ___m_MousePositionPrev_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_MousePositionPrev_1() const { return ___m_MousePositionPrev_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_MousePositionPrev_1() { return &___m_MousePositionPrev_1; }
	inline void set_m_MousePositionPrev_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_MousePositionPrev_1 = value;
	}

	inline static int32_t get_offset_of_m_LastUpdatedFrame_2() { return static_cast<int32_t>(offsetof(UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128, ___m_LastUpdatedFrame_2)); }
	inline int32_t get_m_LastUpdatedFrame_2() const { return ___m_LastUpdatedFrame_2; }
	inline int32_t* get_address_of_m_LastUpdatedFrame_2() { return &___m_LastUpdatedFrame_2; }
	inline void set_m_LastUpdatedFrame_2(int32_t value)
	{
		___m_LastUpdatedFrame_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYINPUTSOURCE_T1EE49D6C3A027078D2C0643F34B72F1D713B1128_H
#ifndef MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#define MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKeyFlags
struct  ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651 
{
public:
	// System.Int32 Rewired.ModifierKeyFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifndef EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#define EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.EditorPlatform
struct  EditorPlatform_tBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC 
{
public:
	// System.Int32 Rewired.Platforms.EditorPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EditorPlatform_tBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#ifndef PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#define PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Platform
struct  Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD 
{
public:
	// System.Int32 Rewired.Platforms.Platform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#ifndef SCRIPTINGAPILEVEL_TEB2EB61369C8FB6E95CEC69128BE55B51D53A975_H
#define SCRIPTINGAPILEVEL_TEB2EB61369C8FB6E95CEC69128BE55B51D53A975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.ScriptingAPILevel
struct  ScriptingAPILevel_tEB2EB61369C8FB6E95CEC69128BE55B51D53A975 
{
public:
	// System.Int32 Rewired.Platforms.ScriptingAPILevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScriptingAPILevel_tEB2EB61369C8FB6E95CEC69128BE55B51D53A975, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGAPILEVEL_TEB2EB61369C8FB6E95CEC69128BE55B51D53A975_H
#ifndef SCRIPTINGBACKEND_T82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA_H
#define SCRIPTINGBACKEND_T82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.ScriptingBackend
struct  ScriptingBackend_t82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA 
{
public:
	// System.Int32 Rewired.Platforms.ScriptingBackend::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScriptingBackend_t82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGBACKEND_T82E312DC63FCDA6BD0BA0843D7BE29AEF8F5B7AA_H
#ifndef WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#define WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebplayerPlatform
struct  WebplayerPlatform_tCE2DAFDB23E582591F2C39C73279153D7E0C3A82 
{
public:
	// System.Int32 Rewired.Platforms.WebplayerPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebplayerPlatform_tCE2DAFDB23E582591F2C39C73279153D7E0C3A82, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#ifndef POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#define POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Pole
struct  Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C 
{
public:
	// System.Int32 Rewired.Pole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifndef BUTTONIDENTIFIER_T229D85569162CB66A697F71025B678733FC782AF_H
#define BUTTONIDENTIFIER_T229D85569162CB66A697F71025B678733FC782AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.CalibrationWindow_ButtonIdentifier
struct  ButtonIdentifier_t229D85569162CB66A697F71025B678733FC782AF 
{
public:
	// System.Int32 Rewired.UI.ControlMapper.CalibrationWindow_ButtonIdentifier::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonIdentifier_t229D85569162CB66A697F71025B678733FC782AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONIDENTIFIER_T229D85569162CB66A697F71025B678733FC782AF_H
#ifndef BREAKPOINT_T1D2111C77944DFE7FF80A105F85B099572C274A6_H
#define BREAKPOINT_T1D2111C77944DFE7FF80A105F85B099572C274A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.CanvasScalerFitter_BreakPoint
struct  BreakPoint_t1D2111C77944DFE7FF80A105F85B099572C274A6  : public RuntimeObject
{
public:
	// System.String Rewired.UI.ControlMapper.CanvasScalerFitter_BreakPoint::name
	String_t* ___name_0;
	// System.Single Rewired.UI.ControlMapper.CanvasScalerFitter_BreakPoint::screenAspectRatio
	float ___screenAspectRatio_1;
	// UnityEngine.Vector2 Rewired.UI.ControlMapper.CanvasScalerFitter_BreakPoint::referenceResolution
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___referenceResolution_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(BreakPoint_t1D2111C77944DFE7FF80A105F85B099572C274A6, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_screenAspectRatio_1() { return static_cast<int32_t>(offsetof(BreakPoint_t1D2111C77944DFE7FF80A105F85B099572C274A6, ___screenAspectRatio_1)); }
	inline float get_screenAspectRatio_1() const { return ___screenAspectRatio_1; }
	inline float* get_address_of_screenAspectRatio_1() { return &___screenAspectRatio_1; }
	inline void set_screenAspectRatio_1(float value)
	{
		___screenAspectRatio_1 = value;
	}

	inline static int32_t get_offset_of_referenceResolution_2() { return static_cast<int32_t>(offsetof(BreakPoint_t1D2111C77944DFE7FF80A105F85B099572C274A6, ___referenceResolution_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_referenceResolution_2() const { return ___referenceResolution_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_referenceResolution_2() { return &___referenceResolution_2; }
	inline void set_referenceResolution_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___referenceResolution_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BREAKPOINT_T1D2111C77944DFE7FF80A105F85B099572C274A6_H
#ifndef LAYOUTELEMENTSIZETYPE_TF37F8C6EA5EA1D90AAFCE69FEF8FC911F87BC99C_H
#define LAYOUTELEMENTSIZETYPE_TF37F8C6EA5EA1D90AAFCE69FEF8FC911F87BC99C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_LayoutElementSizeType
struct  LayoutElementSizeType_tF37F8C6EA5EA1D90AAFCE69FEF8FC911F87BC99C 
{
public:
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_LayoutElementSizeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LayoutElementSizeType_tF37F8C6EA5EA1D90AAFCE69FEF8FC911F87BC99C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTELEMENTSIZETYPE_TF37F8C6EA5EA1D90AAFCE69FEF8FC911F87BC99C_H
#ifndef ACTIONLISTMODE_TD5639A744267D253836F3DEEA35F075724B7A36D_H
#define ACTIONLISTMODE_TD5639A744267D253836F3DEEA35F075724B7A36D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_MappingSet_ActionListMode
struct  ActionListMode_tD5639A744267D253836F3DEEA35F075724B7A36D 
{
public:
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_MappingSet_ActionListMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionListMode_tD5639A744267D253836F3DEEA35F075724B7A36D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONLISTMODE_TD5639A744267D253836F3DEEA35F075724B7A36D_H
#ifndef WINDOWTYPE_T7ED3BE837E148848A28B2F77643AC474BA0F23A6_H
#define WINDOWTYPE_T7ED3BE837E148848A28B2F77643AC474BA0F23A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_WindowType
struct  WindowType_t7ED3BE837E148848A28B2F77643AC474BA0F23A6 
{
public:
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_WindowType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WindowType_t7ED3BE837E148848A28B2F77643AC474BA0F23A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWTYPE_T7ED3BE837E148848A28B2F77643AC474BA0F23A6_H
#ifndef BUTTONIDENTIFIER_T51F6013AF45864B8A437880FAF31D3A2E9AFFF48_H
#define BUTTONIDENTIFIER_T51F6013AF45864B8A437880FAF31D3A2E9AFFF48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.InputBehaviorWindow_ButtonIdentifier
struct  ButtonIdentifier_t51F6013AF45864B8A437880FAF31D3A2E9AFFF48 
{
public:
	// System.Int32 Rewired.UI.ControlMapper.InputBehaviorWindow_ButtonIdentifier::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonIdentifier_t51F6013AF45864B8A437880FAF31D3A2E9AFFF48, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONIDENTIFIER_T51F6013AF45864B8A437880FAF31D3A2E9AFFF48_H
#ifndef PROPERTYTYPE_T93D0A8A37A95CEDEE518EAF68F00B83E59575E5B_H
#define PROPERTYTYPE_T93D0A8A37A95CEDEE518EAF68F00B83E59575E5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.InputBehaviorWindow_PropertyType
struct  PropertyType_t93D0A8A37A95CEDEE518EAF68F00B83E59575E5B 
{
public:
	// System.Int32 Rewired.UI.ControlMapper.InputBehaviorWindow_PropertyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyType_t93D0A8A37A95CEDEE518EAF68F00B83E59575E5B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T93D0A8A37A95CEDEE518EAF68F00B83E59575E5B_H
#ifndef ANIMATIONCURVE_TD2F265379583AAF1BF8D84F1BB8DB12980FA504C_H
#define ANIMATIONCURVE_TD2F265379583AAF1BF8D84F1BB8DB12980FA504C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_TD2F265379583AAF1BF8D84F1BB8DB12980FA504C_H
#ifndef FRAMEPRESSSTATE_T14175B3126231E1E65C038FBC84A1C6A24E3E79E_H
#define FRAMEPRESSSTATE_T14175B3126231E1E65C038FBC84A1C6A24E3E79E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData_FramePressState
struct  FramePressState_t14175B3126231E1E65C038FBC84A1C6A24E3E79E 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData_FramePressState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FramePressState_t14175B3126231E1E65C038FBC84A1C6A24E3E79E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEPRESSSTATE_T14175B3126231E1E65C038FBC84A1C6A24E3E79E_H
#ifndef INPUTBUTTON_TCC7470F9FD2AFE525243394F0215B47D4BF86AB0_H
#define INPUTBUTTON_TCC7470F9FD2AFE525243394F0215B47D4BF86AB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData_InputButton
struct  InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData_InputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_TCC7470F9FD2AFE525243394F0215B47D4BF86AB0_H
#ifndef RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#define RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___m_GameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___module_1)); }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldPosition_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldNormal_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___screenPosition_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_pinvoke
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_com
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef SCALEMODE_T38950B182EA5E1C8589AB5E02F36FEABB8A5CAA6_H
#define SCALEMODE_T38950B182EA5E1C8589AB5E02F36FEABB8A5CAA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler_ScaleMode
struct  ScaleMode_t38950B182EA5E1C8589AB5E02F36FEABB8A5CAA6 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler_ScaleMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScaleMode_t38950B182EA5E1C8589AB5E02F36FEABB8A5CAA6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEMODE_T38950B182EA5E1C8589AB5E02F36FEABB8A5CAA6_H
#ifndef SCREENMATCHMODE_T61C3A62F8F54F705D47C2C37B06DC8083238C133_H
#define SCREENMATCHMODE_T61C3A62F8F54F705D47C2C37B06DC8083238C133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler_ScreenMatchMode
struct  ScreenMatchMode_t61C3A62F8F54F705D47C2C37B06DC8083238C133 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler_ScreenMatchMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenMatchMode_t61C3A62F8F54F705D47C2C37B06DC8083238C133, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENMATCHMODE_T61C3A62F8F54F705D47C2C37B06DC8083238C133_H
#ifndef UNIT_TD24A4DB24016D1A6B46579640E170359F76F8313_H
#define UNIT_TD24A4DB24016D1A6B46579640E170359F76F8313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler_Unit
struct  Unit_tD24A4DB24016D1A6B46579640E170359F76F8313 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler_Unit::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Unit_tD24A4DB24016D1A6B46579640E170359F76F8313, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_TD24A4DB24016D1A6B46579640E170359F76F8313_H
#ifndef COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#define COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_NormalColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_HighlightedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_PressedColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_DisabledColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifndef MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#define MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation_Mode
struct  Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26 
{
public:
	// System.Int32 UnityEngine.UI.Navigation_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#ifndef SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#define SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable_SelectionState
struct  SelectionState_tF089B96B46A592693753CBF23C52A3887632D210 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_tF089B96B46A592693753CBF23C52A3887632D210, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#ifndef TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#define TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable_Transition
struct  Transition_tA9261C608B54C52324084A0B080E7A3E0548A181 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tA9261C608B54C52324084A0B080E7A3E0548A181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifndef DIRECTION_TAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E_H
#define DIRECTION_TAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider_Direction
struct  Direction_tAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E 
{
public:
	// System.Int32 UnityEngine.UI.Slider_Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_TAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E_H
#ifndef TOGGLETRANSITION_T45980EB1352FF47B2D8D8EBC90385AB68939046D_H
#define TOGGLETRANSITION_T45980EB1352FF47B2D8D8EBC90385AB68939046D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle_ToggleTransition
struct  ToggleTransition_t45980EB1352FF47B2D8D8EBC90385AB68939046D 
{
public:
	// System.Int32 UnityEngine.UI.Toggle_ToggleTransition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ToggleTransition_t45980EB1352FF47B2D8D8EBC90385AB68939046D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLETRANSITION_T45980EB1352FF47B2D8D8EBC90385AB68939046D_H
#ifndef AXISCALIBRATIONDATA_T2929DB24F1620CF33F77A2431FFB5C3889C31171_H
#define AXISCALIBRATIONDATA_T2929DB24F1620CF33F77A2431FFB5C3889C31171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisCalibrationData
struct  AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171 
{
public:
	// System.Boolean Rewired.AxisCalibrationData::enabled
	bool ___enabled_0;
	// System.Single Rewired.AxisCalibrationData::deadZone
	float ___deadZone_1;
	// System.Single Rewired.AxisCalibrationData::zero
	float ___zero_2;
	// System.Single Rewired.AxisCalibrationData::min
	float ___min_3;
	// System.Single Rewired.AxisCalibrationData::max
	float ___max_4;
	// System.Boolean Rewired.AxisCalibrationData::invert
	bool ___invert_5;
	// Rewired.AxisSensitivityType Rewired.AxisCalibrationData::sensitivityType
	int32_t ___sensitivityType_6;
	// System.Single Rewired.AxisCalibrationData::sensitivity
	float ___sensitivity_7;
	// UnityEngine.AnimationCurve Rewired.AxisCalibrationData::sensitivityCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___sensitivityCurve_8;
	// System.Boolean Rewired.AxisCalibrationData::applyRangeCalibration
	bool ___applyRangeCalibration_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Data.Mapping.AxisCalibrationInfo> Rewired.AxisCalibrationData::calibrations
	Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * ___calibrations_10;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_deadZone_1() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___deadZone_1)); }
	inline float get_deadZone_1() const { return ___deadZone_1; }
	inline float* get_address_of_deadZone_1() { return &___deadZone_1; }
	inline void set_deadZone_1(float value)
	{
		___deadZone_1 = value;
	}

	inline static int32_t get_offset_of_zero_2() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___zero_2)); }
	inline float get_zero_2() const { return ___zero_2; }
	inline float* get_address_of_zero_2() { return &___zero_2; }
	inline void set_zero_2(float value)
	{
		___zero_2 = value;
	}

	inline static int32_t get_offset_of_min_3() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___min_3)); }
	inline float get_min_3() const { return ___min_3; }
	inline float* get_address_of_min_3() { return &___min_3; }
	inline void set_min_3(float value)
	{
		___min_3 = value;
	}

	inline static int32_t get_offset_of_max_4() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___max_4)); }
	inline float get_max_4() const { return ___max_4; }
	inline float* get_address_of_max_4() { return &___max_4; }
	inline void set_max_4(float value)
	{
		___max_4 = value;
	}

	inline static int32_t get_offset_of_invert_5() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___invert_5)); }
	inline bool get_invert_5() const { return ___invert_5; }
	inline bool* get_address_of_invert_5() { return &___invert_5; }
	inline void set_invert_5(bool value)
	{
		___invert_5 = value;
	}

	inline static int32_t get_offset_of_sensitivityType_6() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___sensitivityType_6)); }
	inline int32_t get_sensitivityType_6() const { return ___sensitivityType_6; }
	inline int32_t* get_address_of_sensitivityType_6() { return &___sensitivityType_6; }
	inline void set_sensitivityType_6(int32_t value)
	{
		___sensitivityType_6 = value;
	}

	inline static int32_t get_offset_of_sensitivity_7() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___sensitivity_7)); }
	inline float get_sensitivity_7() const { return ___sensitivity_7; }
	inline float* get_address_of_sensitivity_7() { return &___sensitivity_7; }
	inline void set_sensitivity_7(float value)
	{
		___sensitivity_7 = value;
	}

	inline static int32_t get_offset_of_sensitivityCurve_8() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___sensitivityCurve_8)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_sensitivityCurve_8() const { return ___sensitivityCurve_8; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_sensitivityCurve_8() { return &___sensitivityCurve_8; }
	inline void set_sensitivityCurve_8(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___sensitivityCurve_8 = value;
		Il2CppCodeGenWriteBarrier((&___sensitivityCurve_8), value);
	}

	inline static int32_t get_offset_of_applyRangeCalibration_9() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___applyRangeCalibration_9)); }
	inline bool get_applyRangeCalibration_9() const { return ___applyRangeCalibration_9; }
	inline bool* get_address_of_applyRangeCalibration_9() { return &___applyRangeCalibration_9; }
	inline void set_applyRangeCalibration_9(bool value)
	{
		___applyRangeCalibration_9 = value;
	}

	inline static int32_t get_offset_of_calibrations_10() { return static_cast<int32_t>(offsetof(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171, ___calibrations_10)); }
	inline Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * get_calibrations_10() const { return ___calibrations_10; }
	inline Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 ** get_address_of_calibrations_10() { return &___calibrations_10; }
	inline void set_calibrations_10(Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * value)
	{
		___calibrations_10 = value;
		Il2CppCodeGenWriteBarrier((&___calibrations_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.AxisCalibrationData
struct AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171_marshaled_pinvoke
{
	int32_t ___enabled_0;
	float ___deadZone_1;
	float ___zero_2;
	float ___min_3;
	float ___max_4;
	int32_t ___invert_5;
	int32_t ___sensitivityType_6;
	float ___sensitivity_7;
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_pinvoke ___sensitivityCurve_8;
	int32_t ___applyRangeCalibration_9;
	Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * ___calibrations_10;
};
// Native definition for COM marshalling of Rewired.AxisCalibrationData
struct AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171_marshaled_com
{
	int32_t ___enabled_0;
	float ___deadZone_1;
	float ___zero_2;
	float ___min_3;
	float ___max_4;
	int32_t ___invert_5;
	int32_t ___sensitivityType_6;
	float ___sensitivity_7;
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_com* ___sensitivityCurve_8;
	int32_t ___applyRangeCalibration_9;
	Dictionary_2_t19D642DECCCCC3B0BB44566152F916D3C07C4C03 * ___calibrations_10;
};
#endif // AXISCALIBRATIONDATA_T2929DB24F1620CF33F77A2431FFB5C3889C31171_H
#ifndef CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#define CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerPollingInfo
struct  ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 
{
public:
	// System.Boolean Rewired.ControllerPollingInfo::XxREeDCHebPEeYaAATiXyFbnbIU
	bool ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	// System.Int32 Rewired.ControllerPollingInfo::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	// System.Int32 Rewired.ControllerPollingInfo::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	// System.String Rewired.ControllerPollingInfo::tUeBKTuOCBEsklhkCXrBJHMCCni
	String_t* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	// Rewired.ControllerType Rewired.ControllerPollingInfo::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	// Rewired.ControllerElementType Rewired.ControllerPollingInfo::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	// System.Int32 Rewired.ControllerPollingInfo::JcIOHtlyyDcisHJclHvGjyIzQHK
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	// Rewired.Pole Rewired.ControllerPollingInfo::gOvKiYbtKZOeaPYXdsADyDYKhyR
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	// System.String Rewired.ControllerPollingInfo::BpnqUtABZEySqFzPQqIktIgZaUw
	String_t* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	// System.Int32 Rewired.ControllerPollingInfo::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	// UnityEngine.KeyCode Rewired.ControllerPollingInfo::UBcIxoTNwLQLduWHPwWDYgtuvif
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;

public:
	inline static int32_t get_offset_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___XxREeDCHebPEeYaAATiXyFbnbIU_0)); }
	inline bool get_XxREeDCHebPEeYaAATiXyFbnbIU_0() const { return ___XxREeDCHebPEeYaAATiXyFbnbIU_0; }
	inline bool* get_address_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() { return &___XxREeDCHebPEeYaAATiXyFbnbIU_0; }
	inline void set_XxREeDCHebPEeYaAATiXyFbnbIU_0(bool value)
	{
		___XxREeDCHebPEeYaAATiXyFbnbIU_0 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___FCIziTxDWAnyHATWwZeMEmiXvdc_1)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_1() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_1(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_1 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___CdiTZueJOweHxLVLesdWcZkZxNV_2)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_2() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_2(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_2 = value;
	}

	inline static int32_t get_offset_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___tUeBKTuOCBEsklhkCXrBJHMCCni_3)); }
	inline String_t* get_tUeBKTuOCBEsklhkCXrBJHMCCni_3() const { return ___tUeBKTuOCBEsklhkCXrBJHMCCni_3; }
	inline String_t** get_address_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() { return &___tUeBKTuOCBEsklhkCXrBJHMCCni_3; }
	inline void set_tUeBKTuOCBEsklhkCXrBJHMCCni_3(String_t* value)
	{
		___tUeBKTuOCBEsklhkCXrBJHMCCni_3 = value;
		Il2CppCodeGenWriteBarrier((&___tUeBKTuOCBEsklhkCXrBJHMCCni_3), value);
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4 = value;
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___RmmmEkVblcqxoqGYhifgdSESkSn_5)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_5() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_5; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_5; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_5(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_5 = value;
	}

	inline static int32_t get_offset_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___JcIOHtlyyDcisHJclHvGjyIzQHK_6)); }
	inline int32_t get_JcIOHtlyyDcisHJclHvGjyIzQHK_6() const { return ___JcIOHtlyyDcisHJclHvGjyIzQHK_6; }
	inline int32_t* get_address_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() { return &___JcIOHtlyyDcisHJclHvGjyIzQHK_6; }
	inline void set_JcIOHtlyyDcisHJclHvGjyIzQHK_6(int32_t value)
	{
		___JcIOHtlyyDcisHJclHvGjyIzQHK_6 = value;
	}

	inline static int32_t get_offset_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7)); }
	inline int32_t get_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() const { return ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7; }
	inline int32_t* get_address_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() { return &___gOvKiYbtKZOeaPYXdsADyDYKhyR_7; }
	inline void set_gOvKiYbtKZOeaPYXdsADyDYKhyR_7(int32_t value)
	{
		___gOvKiYbtKZOeaPYXdsADyDYKhyR_7 = value;
	}

	inline static int32_t get_offset_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___BpnqUtABZEySqFzPQqIktIgZaUw_8)); }
	inline String_t* get_BpnqUtABZEySqFzPQqIktIgZaUw_8() const { return ___BpnqUtABZEySqFzPQqIktIgZaUw_8; }
	inline String_t** get_address_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() { return &___BpnqUtABZEySqFzPQqIktIgZaUw_8; }
	inline void set_BpnqUtABZEySqFzPQqIktIgZaUw_8(String_t* value)
	{
		___BpnqUtABZEySqFzPQqIktIgZaUw_8 = value;
		Il2CppCodeGenWriteBarrier((&___BpnqUtABZEySqFzPQqIktIgZaUw_8), value);
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___FrsWYgeIWMnjOcXDysagwZGcCMF_9)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_9() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_9; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_9; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_9(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_9 = value;
	}

	inline static int32_t get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___UBcIxoTNwLQLduWHPwWDYgtuvif_10)); }
	inline int32_t get_UBcIxoTNwLQLduWHPwWDYgtuvif_10() const { return ___UBcIxoTNwLQLduWHPwWDYgtuvif_10; }
	inline int32_t* get_address_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() { return &___UBcIxoTNwLQLduWHPwWDYgtuvif_10; }
	inline void set_UBcIxoTNwLQLduWHPwWDYgtuvif_10(int32_t value)
	{
		___UBcIxoTNwLQLduWHPwWDYgtuvif_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ControllerPollingInfo
struct ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_pinvoke
{
	int32_t ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	char* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	char* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;
};
// Native definition for COM marshalling of Rewired.ControllerPollingInfo
struct ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_com
{
	int32_t ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	Il2CppChar* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	Il2CppChar* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;
};
#endif // CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#ifndef ELEMENTASSIGNMENT_TB4E14263A80B282326670C629692DBEE4AF1CC8C_H
#define ELEMENTASSIGNMENT_TB4E14263A80B282326670C629692DBEE4AF1CC8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignment
struct  ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C 
{
public:
	// Rewired.ElementAssignmentType Rewired.ElementAssignment::type
	int32_t ___type_0;
	// System.Int32 Rewired.ElementAssignment::elementMapId
	int32_t ___elementMapId_1;
	// System.Int32 Rewired.ElementAssignment::elementIdentifierId
	int32_t ___elementIdentifierId_2;
	// Rewired.AxisRange Rewired.ElementAssignment::axisRange
	int32_t ___axisRange_3;
	// UnityEngine.KeyCode Rewired.ElementAssignment::keyboardKey
	int32_t ___keyboardKey_4;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignment::modifierKeyFlags
	int32_t ___modifierKeyFlags_5;
	// System.Int32 Rewired.ElementAssignment::actionId
	int32_t ___actionId_6;
	// Rewired.Pole Rewired.ElementAssignment::axisContribution
	int32_t ___axisContribution_7;
	// System.Boolean Rewired.ElementAssignment::invert
	bool ___invert_8;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_elementMapId_1() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___elementMapId_1)); }
	inline int32_t get_elementMapId_1() const { return ___elementMapId_1; }
	inline int32_t* get_address_of_elementMapId_1() { return &___elementMapId_1; }
	inline void set_elementMapId_1(int32_t value)
	{
		___elementMapId_1 = value;
	}

	inline static int32_t get_offset_of_elementIdentifierId_2() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___elementIdentifierId_2)); }
	inline int32_t get_elementIdentifierId_2() const { return ___elementIdentifierId_2; }
	inline int32_t* get_address_of_elementIdentifierId_2() { return &___elementIdentifierId_2; }
	inline void set_elementIdentifierId_2(int32_t value)
	{
		___elementIdentifierId_2 = value;
	}

	inline static int32_t get_offset_of_axisRange_3() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___axisRange_3)); }
	inline int32_t get_axisRange_3() const { return ___axisRange_3; }
	inline int32_t* get_address_of_axisRange_3() { return &___axisRange_3; }
	inline void set_axisRange_3(int32_t value)
	{
		___axisRange_3 = value;
	}

	inline static int32_t get_offset_of_keyboardKey_4() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___keyboardKey_4)); }
	inline int32_t get_keyboardKey_4() const { return ___keyboardKey_4; }
	inline int32_t* get_address_of_keyboardKey_4() { return &___keyboardKey_4; }
	inline void set_keyboardKey_4(int32_t value)
	{
		___keyboardKey_4 = value;
	}

	inline static int32_t get_offset_of_modifierKeyFlags_5() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___modifierKeyFlags_5)); }
	inline int32_t get_modifierKeyFlags_5() const { return ___modifierKeyFlags_5; }
	inline int32_t* get_address_of_modifierKeyFlags_5() { return &___modifierKeyFlags_5; }
	inline void set_modifierKeyFlags_5(int32_t value)
	{
		___modifierKeyFlags_5 = value;
	}

	inline static int32_t get_offset_of_actionId_6() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___actionId_6)); }
	inline int32_t get_actionId_6() const { return ___actionId_6; }
	inline int32_t* get_address_of_actionId_6() { return &___actionId_6; }
	inline void set_actionId_6(int32_t value)
	{
		___actionId_6 = value;
	}

	inline static int32_t get_offset_of_axisContribution_7() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___axisContribution_7)); }
	inline int32_t get_axisContribution_7() const { return ___axisContribution_7; }
	inline int32_t* get_address_of_axisContribution_7() { return &___axisContribution_7; }
	inline void set_axisContribution_7(int32_t value)
	{
		___axisContribution_7 = value;
	}

	inline static int32_t get_offset_of_invert_8() { return static_cast<int32_t>(offsetof(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C, ___invert_8)); }
	inline bool get_invert_8() const { return ___invert_8; }
	inline bool* get_address_of_invert_8() { return &___invert_8; }
	inline void set_invert_8(bool value)
	{
		___invert_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignment
struct ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C_marshaled_pinvoke
{
	int32_t ___type_0;
	int32_t ___elementMapId_1;
	int32_t ___elementIdentifierId_2;
	int32_t ___axisRange_3;
	int32_t ___keyboardKey_4;
	int32_t ___modifierKeyFlags_5;
	int32_t ___actionId_6;
	int32_t ___axisContribution_7;
	int32_t ___invert_8;
};
// Native definition for COM marshalling of Rewired.ElementAssignment
struct ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C_marshaled_com
{
	int32_t ___type_0;
	int32_t ___elementMapId_1;
	int32_t ___elementIdentifierId_2;
	int32_t ___axisRange_3;
	int32_t ___keyboardKey_4;
	int32_t ___modifierKeyFlags_5;
	int32_t ___actionId_6;
	int32_t ___axisContribution_7;
	int32_t ___invert_8;
};
#endif // ELEMENTASSIGNMENT_TB4E14263A80B282326670C629692DBEE4AF1CC8C_H
#ifndef ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#define ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentConflictCheck
struct  ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 
{
public:
	// Rewired.ElementAssignmentType Rewired.ElementAssignmentConflictCheck::PReGOawJrxHuYbkYlXhFoNBbppd
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	// Rewired.ControllerType Rewired.ElementAssignmentConflictCheck::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::kNupzNtNRIxDZONVUSwnnBWcpIT
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::fAMwdVQKMkdvbVWXbkkCQoSadHid
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::TXYPyQJDwYiLBxGVhCRAhruZxByr
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	// Rewired.AxisRange Rewired.ElementAssignmentConflictCheck::xTkhpClvSKvoiLfYNzcXIDURAPx
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	// UnityEngine.KeyCode Rewired.ElementAssignmentConflictCheck::UBcIxoTNwLQLduWHPwWDYgtuvif
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignmentConflictCheck::rUpHewPnhPrBuIDeIasWkyvRDkW
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	// Rewired.Pole Rewired.ElementAssignmentConflictCheck::magSqBEVFaJkoQfcAvXLVDzmcnZB
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	// System.Boolean Rewired.ElementAssignmentConflictCheck::GfvPtFqTnCIcrWebHAQzqOoKceI
	bool ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;

public:
	inline static int32_t get_offset_of_PReGOawJrxHuYbkYlXhFoNBbppd_0() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___PReGOawJrxHuYbkYlXhFoNBbppd_0)); }
	inline int32_t get_PReGOawJrxHuYbkYlXhFoNBbppd_0() const { return ___PReGOawJrxHuYbkYlXhFoNBbppd_0; }
	inline int32_t* get_address_of_PReGOawJrxHuYbkYlXhFoNBbppd_0() { return &___PReGOawJrxHuYbkYlXhFoNBbppd_0; }
	inline void set_PReGOawJrxHuYbkYlXhFoNBbppd_0(int32_t value)
	{
		___PReGOawJrxHuYbkYlXhFoNBbppd_0 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___FCIziTxDWAnyHATWwZeMEmiXvdc_1)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_1() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_1(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_1 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___CdiTZueJOweHxLVLesdWcZkZxNV_3)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_3() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_3; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_3; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_3(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_3 = value;
	}

	inline static int32_t get_offset_of_kNupzNtNRIxDZONVUSwnnBWcpIT_4() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___kNupzNtNRIxDZONVUSwnnBWcpIT_4)); }
	inline int32_t get_kNupzNtNRIxDZONVUSwnnBWcpIT_4() const { return ___kNupzNtNRIxDZONVUSwnnBWcpIT_4; }
	inline int32_t* get_address_of_kNupzNtNRIxDZONVUSwnnBWcpIT_4() { return &___kNupzNtNRIxDZONVUSwnnBWcpIT_4; }
	inline void set_kNupzNtNRIxDZONVUSwnnBWcpIT_4(int32_t value)
	{
		___kNupzNtNRIxDZONVUSwnnBWcpIT_4 = value;
	}

	inline static int32_t get_offset_of_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5)); }
	inline int32_t get_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() const { return ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5; }
	inline int32_t* get_address_of_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() { return &___fAMwdVQKMkdvbVWXbkkCQoSadHid_5; }
	inline void set_fAMwdVQKMkdvbVWXbkkCQoSadHid_5(int32_t value)
	{
		___fAMwdVQKMkdvbVWXbkkCQoSadHid_5 = value;
	}

	inline static int32_t get_offset_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6)); }
	inline int32_t get_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() const { return ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline int32_t* get_address_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return &___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline void set_TXYPyQJDwYiLBxGVhCRAhruZxByr_6(int32_t value)
	{
		___TXYPyQJDwYiLBxGVhCRAhruZxByr_6 = value;
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_7() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___FrsWYgeIWMnjOcXDysagwZGcCMF_7)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_7() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_7; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_7() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_7; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_7(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_7 = value;
	}

	inline static int32_t get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_8() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___xTkhpClvSKvoiLfYNzcXIDURAPx_8)); }
	inline int32_t get_xTkhpClvSKvoiLfYNzcXIDURAPx_8() const { return ___xTkhpClvSKvoiLfYNzcXIDURAPx_8; }
	inline int32_t* get_address_of_xTkhpClvSKvoiLfYNzcXIDURAPx_8() { return &___xTkhpClvSKvoiLfYNzcXIDURAPx_8; }
	inline void set_xTkhpClvSKvoiLfYNzcXIDURAPx_8(int32_t value)
	{
		___xTkhpClvSKvoiLfYNzcXIDURAPx_8 = value;
	}

	inline static int32_t get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_9() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___UBcIxoTNwLQLduWHPwWDYgtuvif_9)); }
	inline int32_t get_UBcIxoTNwLQLduWHPwWDYgtuvif_9() const { return ___UBcIxoTNwLQLduWHPwWDYgtuvif_9; }
	inline int32_t* get_address_of_UBcIxoTNwLQLduWHPwWDYgtuvif_9() { return &___UBcIxoTNwLQLduWHPwWDYgtuvif_9; }
	inline void set_UBcIxoTNwLQLduWHPwWDYgtuvif_9(int32_t value)
	{
		___UBcIxoTNwLQLduWHPwWDYgtuvif_9 = value;
	}

	inline static int32_t get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___rUpHewPnhPrBuIDeIasWkyvRDkW_10)); }
	inline int32_t get_rUpHewPnhPrBuIDeIasWkyvRDkW_10() const { return ___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline int32_t* get_address_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return &___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline void set_rUpHewPnhPrBuIDeIasWkyvRDkW_10(int32_t value)
	{
		___rUpHewPnhPrBuIDeIasWkyvRDkW_10 = value;
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___LmADKPFcmVAiabETiHqQTGSgvmcg_11)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_11() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_11(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_11 = value;
	}

	inline static int32_t get_offset_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12)); }
	inline int32_t get_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() const { return ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12; }
	inline int32_t* get_address_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() { return &___magSqBEVFaJkoQfcAvXLVDzmcnZB_12; }
	inline void set_magSqBEVFaJkoQfcAvXLVDzmcnZB_12(int32_t value)
	{
		___magSqBEVFaJkoQfcAvXLVDzmcnZB_12 = value;
	}

	inline static int32_t get_offset_of_GfvPtFqTnCIcrWebHAQzqOoKceI_13() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___GfvPtFqTnCIcrWebHAQzqOoKceI_13)); }
	inline bool get_GfvPtFqTnCIcrWebHAQzqOoKceI_13() const { return ___GfvPtFqTnCIcrWebHAQzqOoKceI_13; }
	inline bool* get_address_of_GfvPtFqTnCIcrWebHAQzqOoKceI_13() { return &___GfvPtFqTnCIcrWebHAQzqOoKceI_13; }
	inline void set_GfvPtFqTnCIcrWebHAQzqOoKceI_13(bool value)
	{
		___GfvPtFqTnCIcrWebHAQzqOoKceI_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignmentConflictCheck
struct ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_marshaled_pinvoke
{
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	int32_t ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;
};
// Native definition for COM marshalling of Rewired.ElementAssignmentConflictCheck
struct ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_marshaled_com
{
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	int32_t ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;
};
#endif // ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#ifndef ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#define ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentConflictInfo
struct  ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D 
{
public:
	// System.Boolean Rewired.ElementAssignmentConflictInfo::rLCeavZDeONdAmfQvjLvpIKqsIQ
	bool ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	// System.Boolean Rewired.ElementAssignmentConflictInfo::uUoPCsKlKtgHCnSDBCrTHvhFbFXy
	bool ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	// Rewired.ControllerType Rewired.ElementAssignmentConflictInfo::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::kNupzNtNRIxDZONVUSwnnBWcpIT
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::TXYPyQJDwYiLBxGVhCRAhruZxByr
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	// Rewired.ControllerElementType Rewired.ElementAssignmentConflictInfo::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	// UnityEngine.KeyCode Rewired.ElementAssignmentConflictInfo::SkzHHHthNTcRwnaarBvUnkLqLeI
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignmentConflictInfo::rUpHewPnhPrBuIDeIasWkyvRDkW
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;

public:
	inline static int32_t get_offset_of_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0)); }
	inline bool get_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() const { return ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0; }
	inline bool* get_address_of_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() { return &___rLCeavZDeONdAmfQvjLvpIKqsIQ_0; }
	inline void set_rLCeavZDeONdAmfQvjLvpIKqsIQ_0(bool value)
	{
		___rLCeavZDeONdAmfQvjLvpIKqsIQ_0 = value;
	}

	inline static int32_t get_offset_of_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1)); }
	inline bool get_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() const { return ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1; }
	inline bool* get_address_of_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() { return &___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1; }
	inline void set_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1(bool value)
	{
		___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___FCIziTxDWAnyHATWwZeMEmiXvdc_2)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_2() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_2(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_2 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___CdiTZueJOweHxLVLesdWcZkZxNV_4)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_4() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_4; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_4; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_4(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_4 = value;
	}

	inline static int32_t get_offset_of_kNupzNtNRIxDZONVUSwnnBWcpIT_5() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___kNupzNtNRIxDZONVUSwnnBWcpIT_5)); }
	inline int32_t get_kNupzNtNRIxDZONVUSwnnBWcpIT_5() const { return ___kNupzNtNRIxDZONVUSwnnBWcpIT_5; }
	inline int32_t* get_address_of_kNupzNtNRIxDZONVUSwnnBWcpIT_5() { return &___kNupzNtNRIxDZONVUSwnnBWcpIT_5; }
	inline void set_kNupzNtNRIxDZONVUSwnnBWcpIT_5(int32_t value)
	{
		___kNupzNtNRIxDZONVUSwnnBWcpIT_5 = value;
	}

	inline static int32_t get_offset_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6)); }
	inline int32_t get_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() const { return ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline int32_t* get_address_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return &___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline void set_TXYPyQJDwYiLBxGVhCRAhruZxByr_6(int32_t value)
	{
		___TXYPyQJDwYiLBxGVhCRAhruZxByr_6 = value;
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_7() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___RmmmEkVblcqxoqGYhifgdSESkSn_7)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_7() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_7; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_7() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_7; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_7(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_7 = value;
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_8() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___FrsWYgeIWMnjOcXDysagwZGcCMF_8)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_8() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_8; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_8() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_8; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_8(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_8 = value;
	}

	inline static int32_t get_offset_of_SkzHHHthNTcRwnaarBvUnkLqLeI_9() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___SkzHHHthNTcRwnaarBvUnkLqLeI_9)); }
	inline int32_t get_SkzHHHthNTcRwnaarBvUnkLqLeI_9() const { return ___SkzHHHthNTcRwnaarBvUnkLqLeI_9; }
	inline int32_t* get_address_of_SkzHHHthNTcRwnaarBvUnkLqLeI_9() { return &___SkzHHHthNTcRwnaarBvUnkLqLeI_9; }
	inline void set_SkzHHHthNTcRwnaarBvUnkLqLeI_9(int32_t value)
	{
		___SkzHHHthNTcRwnaarBvUnkLqLeI_9 = value;
	}

	inline static int32_t get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___rUpHewPnhPrBuIDeIasWkyvRDkW_10)); }
	inline int32_t get_rUpHewPnhPrBuIDeIasWkyvRDkW_10() const { return ___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline int32_t* get_address_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return &___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline void set_rUpHewPnhPrBuIDeIasWkyvRDkW_10(int32_t value)
	{
		___rUpHewPnhPrBuIDeIasWkyvRDkW_10 = value;
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___LmADKPFcmVAiabETiHqQTGSgvmcg_11)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_11() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_11(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignmentConflictInfo
struct ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_marshaled_pinvoke
{
	int32_t ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	int32_t ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
};
// Native definition for COM marshalling of Rewired.ElementAssignmentConflictInfo
struct ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_marshaled_com
{
	int32_t ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	int32_t ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
};
#endif // ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#ifndef FLIGHTPEDALSTEMPLATE_T8952CFD27FA33E01A13B49C83069731FDEA315FF_H
#define FLIGHTPEDALSTEMPLATE_T8952CFD27FA33E01A13B49C83069731FDEA315FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.FlightPedalsTemplate
struct  FlightPedalsTemplate_t8952CFD27FA33E01A13B49C83069731FDEA315FF  : public ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98
{
public:

public:
};

struct FlightPedalsTemplate_t8952CFD27FA33E01A13B49C83069731FDEA315FF_StaticFields
{
public:
	// System.Guid Rewired.FlightPedalsTemplate::typeGuid
	Guid_t  ___typeGuid_8;

public:
	inline static int32_t get_offset_of_typeGuid_8() { return static_cast<int32_t>(offsetof(FlightPedalsTemplate_t8952CFD27FA33E01A13B49C83069731FDEA315FF_StaticFields, ___typeGuid_8)); }
	inline Guid_t  get_typeGuid_8() const { return ___typeGuid_8; }
	inline Guid_t * get_address_of_typeGuid_8() { return &___typeGuid_8; }
	inline void set_typeGuid_8(Guid_t  value)
	{
		___typeGuid_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLIGHTPEDALSTEMPLATE_T8952CFD27FA33E01A13B49C83069731FDEA315FF_H
#ifndef FLIGHTYOKETEMPLATE_T339D83B5785E12D828E1AA369870716E4F606653_H
#define FLIGHTYOKETEMPLATE_T339D83B5785E12D828E1AA369870716E4F606653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.FlightYokeTemplate
struct  FlightYokeTemplate_t339D83B5785E12D828E1AA369870716E4F606653  : public ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98
{
public:

public:
};

struct FlightYokeTemplate_t339D83B5785E12D828E1AA369870716E4F606653_StaticFields
{
public:
	// System.Guid Rewired.FlightYokeTemplate::typeGuid
	Guid_t  ___typeGuid_8;

public:
	inline static int32_t get_offset_of_typeGuid_8() { return static_cast<int32_t>(offsetof(FlightYokeTemplate_t339D83B5785E12D828E1AA369870716E4F606653_StaticFields, ___typeGuid_8)); }
	inline Guid_t  get_typeGuid_8() const { return ___typeGuid_8; }
	inline Guid_t * get_address_of_typeGuid_8() { return &___typeGuid_8; }
	inline void set_typeGuid_8(Guid_t  value)
	{
		___typeGuid_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLIGHTYOKETEMPLATE_T339D83B5785E12D828E1AA369870716E4F606653_H
#ifndef GAMEPADTEMPLATE_T2FDAF01C8487C65FD8CA327631F2C4F082AC6573_H
#define GAMEPADTEMPLATE_T2FDAF01C8487C65FD8CA327631F2C4F082AC6573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.GamepadTemplate
struct  GamepadTemplate_t2FDAF01C8487C65FD8CA327631F2C4F082AC6573  : public ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98
{
public:

public:
};

struct GamepadTemplate_t2FDAF01C8487C65FD8CA327631F2C4F082AC6573_StaticFields
{
public:
	// System.Guid Rewired.GamepadTemplate::typeGuid
	Guid_t  ___typeGuid_8;

public:
	inline static int32_t get_offset_of_typeGuid_8() { return static_cast<int32_t>(offsetof(GamepadTemplate_t2FDAF01C8487C65FD8CA327631F2C4F082AC6573_StaticFields, ___typeGuid_8)); }
	inline Guid_t  get_typeGuid_8() const { return ___typeGuid_8; }
	inline Guid_t * get_address_of_typeGuid_8() { return &___typeGuid_8; }
	inline void set_typeGuid_8(Guid_t  value)
	{
		___typeGuid_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEPADTEMPLATE_T2FDAF01C8487C65FD8CA327631F2C4F082AC6573_H
#ifndef HOTASTEMPLATE_T07DBFB706D71D8288EE25DE3649D9DEA06FE55C1_H
#define HOTASTEMPLATE_T07DBFB706D71D8288EE25DE3649D9DEA06FE55C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.HOTASTemplate
struct  HOTASTemplate_t07DBFB706D71D8288EE25DE3649D9DEA06FE55C1  : public ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98
{
public:

public:
};

struct HOTASTemplate_t07DBFB706D71D8288EE25DE3649D9DEA06FE55C1_StaticFields
{
public:
	// System.Guid Rewired.HOTASTemplate::typeGuid
	Guid_t  ___typeGuid_8;

public:
	inline static int32_t get_offset_of_typeGuid_8() { return static_cast<int32_t>(offsetof(HOTASTemplate_t07DBFB706D71D8288EE25DE3649D9DEA06FE55C1_StaticFields, ___typeGuid_8)); }
	inline Guid_t  get_typeGuid_8() const { return ___typeGuid_8; }
	inline Guid_t * get_address_of_typeGuid_8() { return &___typeGuid_8; }
	inline void set_typeGuid_8(Guid_t  value)
	{
		___typeGuid_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOTASTEMPLATE_T07DBFB706D71D8288EE25DE3649D9DEA06FE55C1_H
#ifndef MOUSEBUTTONEVENTDATA_TCDAEE9261DB005C8EBAA9EB979891497CCC71F6C_H
#define MOUSEBUTTONEVENTDATA_TCDAEE9261DB005C8EBAA9EB979891497CCC71F6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Integration.UnityUI.RewiredPointerInputModule_MouseButtonEventData
struct  MouseButtonEventData_tCDAEE9261DB005C8EBAA9EB979891497CCC71F6C  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData_FramePressState Rewired.Integration.UnityUI.RewiredPointerInputModule_MouseButtonEventData::buttonState
	int32_t ___buttonState_0;
	// Rewired.Integration.UnityUI.PlayerPointerEventData Rewired.Integration.UnityUI.RewiredPointerInputModule_MouseButtonEventData::buttonData
	PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72 * ___buttonData_1;

public:
	inline static int32_t get_offset_of_buttonState_0() { return static_cast<int32_t>(offsetof(MouseButtonEventData_tCDAEE9261DB005C8EBAA9EB979891497CCC71F6C, ___buttonState_0)); }
	inline int32_t get_buttonState_0() const { return ___buttonState_0; }
	inline int32_t* get_address_of_buttonState_0() { return &___buttonState_0; }
	inline void set_buttonState_0(int32_t value)
	{
		___buttonState_0 = value;
	}

	inline static int32_t get_offset_of_buttonData_1() { return static_cast<int32_t>(offsetof(MouseButtonEventData_tCDAEE9261DB005C8EBAA9EB979891497CCC71F6C, ___buttonData_1)); }
	inline PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72 * get_buttonData_1() const { return ___buttonData_1; }
	inline PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72 ** get_address_of_buttonData_1() { return &___buttonData_1; }
	inline void set_buttonData_1(PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72 * value)
	{
		___buttonData_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttonData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONEVENTDATA_TCDAEE9261DB005C8EBAA9EB979891497CCC71F6C_H
#ifndef RACINGWHEELTEMPLATE_T8A4529334B04EE28E38A989CF16A7F0F2CFCBD25_H
#define RACINGWHEELTEMPLATE_T8A4529334B04EE28E38A989CF16A7F0F2CFCBD25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.RacingWheelTemplate
struct  RacingWheelTemplate_t8A4529334B04EE28E38A989CF16A7F0F2CFCBD25  : public ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98
{
public:

public:
};

struct RacingWheelTemplate_t8A4529334B04EE28E38A989CF16A7F0F2CFCBD25_StaticFields
{
public:
	// System.Guid Rewired.RacingWheelTemplate::typeGuid
	Guid_t  ___typeGuid_8;

public:
	inline static int32_t get_offset_of_typeGuid_8() { return static_cast<int32_t>(offsetof(RacingWheelTemplate_t8A4529334B04EE28E38A989CF16A7F0F2CFCBD25_StaticFields, ___typeGuid_8)); }
	inline Guid_t  get_typeGuid_8() const { return ___typeGuid_8; }
	inline Guid_t * get_address_of_typeGuid_8() { return &___typeGuid_8; }
	inline void set_typeGuid_8(Guid_t  value)
	{
		___typeGuid_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RACINGWHEELTEMPLATE_T8A4529334B04EE28E38A989CF16A7F0F2CFCBD25_H
#ifndef SIXDOFCONTROLLERTEMPLATE_TE4DC0CA993BF4EB37DAC595EB0CF8215D987B2B6_H
#define SIXDOFCONTROLLERTEMPLATE_TE4DC0CA993BF4EB37DAC595EB0CF8215D987B2B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.SixDofControllerTemplate
struct  SixDofControllerTemplate_tE4DC0CA993BF4EB37DAC595EB0CF8215D987B2B6  : public ControllerTemplate_tB3D9364CC27D096A6AAE8B9960440B43ECAC8B98
{
public:

public:
};

struct SixDofControllerTemplate_tE4DC0CA993BF4EB37DAC595EB0CF8215D987B2B6_StaticFields
{
public:
	// System.Guid Rewired.SixDofControllerTemplate::typeGuid
	Guid_t  ___typeGuid_8;

public:
	inline static int32_t get_offset_of_typeGuid_8() { return static_cast<int32_t>(offsetof(SixDofControllerTemplate_tE4DC0CA993BF4EB37DAC595EB0CF8215D987B2B6_StaticFields, ___typeGuid_8)); }
	inline Guid_t  get_typeGuid_8() const { return ___typeGuid_8; }
	inline Guid_t * get_address_of_typeGuid_8() { return &___typeGuid_8; }
	inline void set_typeGuid_8(Guid_t  value)
	{
		___typeGuid_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIXDOFCONTROLLERTEMPLATE_TE4DC0CA993BF4EB37DAC595EB0CF8215D987B2B6_H
#ifndef INPUTACTIONSET_T99A900AB1469B785BC7A427E6E780DE2FCEC428B_H
#define INPUTACTIONSET_T99A900AB1469B785BC7A427E6E780DE2FCEC428B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_InputActionSet
struct  InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B  : public RuntimeObject
{
public:
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_InputActionSet::_actionId
	int32_t ____actionId_0;
	// Rewired.AxisRange Rewired.UI.ControlMapper.ControlMapper_InputActionSet::_axisRange
	int32_t ____axisRange_1;

public:
	inline static int32_t get_offset_of__actionId_0() { return static_cast<int32_t>(offsetof(InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B, ____actionId_0)); }
	inline int32_t get__actionId_0() const { return ____actionId_0; }
	inline int32_t* get_address_of__actionId_0() { return &____actionId_0; }
	inline void set__actionId_0(int32_t value)
	{
		____actionId_0 = value;
	}

	inline static int32_t get_offset_of__axisRange_1() { return static_cast<int32_t>(offsetof(InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B, ____axisRange_1)); }
	inline int32_t get__axisRange_1() const { return ____axisRange_1; }
	inline int32_t* get_address_of__axisRange_1() { return &____axisRange_1; }
	inline void set__axisRange_1(int32_t value)
	{
		____axisRange_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTACTIONSET_T99A900AB1469B785BC7A427E6E780DE2FCEC428B_H
#ifndef ACTIONENTRY_T78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25_H
#define ACTIONENTRY_T78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_ActionEntry
struct  ActionEntry_t78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25  : public RuntimeObject
{
public:
	// Rewired.UI.ControlMapper.ControlMapper_IndexedDictionary`2<System.Int32,Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_FieldSet> Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_ActionEntry::fieldSets
	IndexedDictionary_2_t5E488B599074F5939A9A442837164D3B0AB374C2 * ___fieldSets_0;
	// Rewired.UI.ControlMapper.ControlMapper_GUILabel Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_ActionEntry::label
	GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8 * ___label_1;
	// Rewired.InputAction Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_ActionEntry::action
	InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * ___action_2;
	// Rewired.AxisRange Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_ActionEntry::axisRange
	int32_t ___axisRange_3;
	// Rewired.UI.ControlMapper.ControlMapper_InputActionSet Rewired.UI.ControlMapper.ControlMapper_InputGridEntryList_ActionEntry::actionSet
	InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B * ___actionSet_4;

public:
	inline static int32_t get_offset_of_fieldSets_0() { return static_cast<int32_t>(offsetof(ActionEntry_t78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25, ___fieldSets_0)); }
	inline IndexedDictionary_2_t5E488B599074F5939A9A442837164D3B0AB374C2 * get_fieldSets_0() const { return ___fieldSets_0; }
	inline IndexedDictionary_2_t5E488B599074F5939A9A442837164D3B0AB374C2 ** get_address_of_fieldSets_0() { return &___fieldSets_0; }
	inline void set_fieldSets_0(IndexedDictionary_2_t5E488B599074F5939A9A442837164D3B0AB374C2 * value)
	{
		___fieldSets_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldSets_0), value);
	}

	inline static int32_t get_offset_of_label_1() { return static_cast<int32_t>(offsetof(ActionEntry_t78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25, ___label_1)); }
	inline GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8 * get_label_1() const { return ___label_1; }
	inline GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8 ** get_address_of_label_1() { return &___label_1; }
	inline void set_label_1(GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8 * value)
	{
		___label_1 = value;
		Il2CppCodeGenWriteBarrier((&___label_1), value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(ActionEntry_t78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25, ___action_2)); }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * get_action_2() const { return ___action_2; }
	inline InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(InputAction_t9328AD30AB7C7F9301F671FEF8C0D73AF1FAD7DA * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_axisRange_3() { return static_cast<int32_t>(offsetof(ActionEntry_t78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25, ___axisRange_3)); }
	inline int32_t get_axisRange_3() const { return ___axisRange_3; }
	inline int32_t* get_address_of_axisRange_3() { return &___axisRange_3; }
	inline void set_axisRange_3(int32_t value)
	{
		___axisRange_3 = value;
	}

	inline static int32_t get_offset_of_actionSet_4() { return static_cast<int32_t>(offsetof(ActionEntry_t78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25, ___actionSet_4)); }
	inline InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B * get_actionSet_4() const { return ___actionSet_4; }
	inline InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B ** get_address_of_actionSet_4() { return &___actionSet_4; }
	inline void set_actionSet_4(InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B * value)
	{
		___actionSet_4 = value;
		Il2CppCodeGenWriteBarrier((&___actionSet_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONENTRY_T78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25_H
#ifndef MAPPINGSET_T54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04_H
#define MAPPINGSET_T54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_MappingSet
struct  MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04  : public RuntimeObject
{
public:
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_MappingSet::_mapCategoryId
	int32_t ____mapCategoryId_0;
	// Rewired.UI.ControlMapper.ControlMapper_MappingSet_ActionListMode Rewired.UI.ControlMapper.ControlMapper_MappingSet::_actionListMode
	int32_t ____actionListMode_1;
	// System.Int32[] Rewired.UI.ControlMapper.ControlMapper_MappingSet::_actionCategoryIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____actionCategoryIds_2;
	// System.Int32[] Rewired.UI.ControlMapper.ControlMapper_MappingSet::_actionIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____actionIds_3;
	// System.Collections.Generic.IList`1<System.Int32> Rewired.UI.ControlMapper.ControlMapper_MappingSet::_actionCategoryIdsReadOnly
	RuntimeObject* ____actionCategoryIdsReadOnly_4;
	// System.Collections.Generic.IList`1<System.Int32> Rewired.UI.ControlMapper.ControlMapper_MappingSet::_actionIdsReadOnly
	RuntimeObject* ____actionIdsReadOnly_5;

public:
	inline static int32_t get_offset_of__mapCategoryId_0() { return static_cast<int32_t>(offsetof(MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04, ____mapCategoryId_0)); }
	inline int32_t get__mapCategoryId_0() const { return ____mapCategoryId_0; }
	inline int32_t* get_address_of__mapCategoryId_0() { return &____mapCategoryId_0; }
	inline void set__mapCategoryId_0(int32_t value)
	{
		____mapCategoryId_0 = value;
	}

	inline static int32_t get_offset_of__actionListMode_1() { return static_cast<int32_t>(offsetof(MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04, ____actionListMode_1)); }
	inline int32_t get__actionListMode_1() const { return ____actionListMode_1; }
	inline int32_t* get_address_of__actionListMode_1() { return &____actionListMode_1; }
	inline void set__actionListMode_1(int32_t value)
	{
		____actionListMode_1 = value;
	}

	inline static int32_t get_offset_of__actionCategoryIds_2() { return static_cast<int32_t>(offsetof(MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04, ____actionCategoryIds_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__actionCategoryIds_2() const { return ____actionCategoryIds_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__actionCategoryIds_2() { return &____actionCategoryIds_2; }
	inline void set__actionCategoryIds_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____actionCategoryIds_2 = value;
		Il2CppCodeGenWriteBarrier((&____actionCategoryIds_2), value);
	}

	inline static int32_t get_offset_of__actionIds_3() { return static_cast<int32_t>(offsetof(MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04, ____actionIds_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__actionIds_3() const { return ____actionIds_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__actionIds_3() { return &____actionIds_3; }
	inline void set__actionIds_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____actionIds_3 = value;
		Il2CppCodeGenWriteBarrier((&____actionIds_3), value);
	}

	inline static int32_t get_offset_of__actionCategoryIdsReadOnly_4() { return static_cast<int32_t>(offsetof(MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04, ____actionCategoryIdsReadOnly_4)); }
	inline RuntimeObject* get__actionCategoryIdsReadOnly_4() const { return ____actionCategoryIdsReadOnly_4; }
	inline RuntimeObject** get_address_of__actionCategoryIdsReadOnly_4() { return &____actionCategoryIdsReadOnly_4; }
	inline void set__actionCategoryIdsReadOnly_4(RuntimeObject* value)
	{
		____actionCategoryIdsReadOnly_4 = value;
		Il2CppCodeGenWriteBarrier((&____actionCategoryIdsReadOnly_4), value);
	}

	inline static int32_t get_offset_of__actionIdsReadOnly_5() { return static_cast<int32_t>(offsetof(MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04, ____actionIdsReadOnly_5)); }
	inline RuntimeObject* get__actionIdsReadOnly_5() const { return ____actionIdsReadOnly_5; }
	inline RuntimeObject** get_address_of__actionIdsReadOnly_5() { return &____actionIdsReadOnly_5; }
	inline void set__actionIdsReadOnly_5(RuntimeObject* value)
	{
		____actionIdsReadOnly_5 = value;
		Il2CppCodeGenWriteBarrier((&____actionIdsReadOnly_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGSET_T54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef POINTEREVENTDATA_TC18994283B7753E430E316A62D9E45BA6D644C63_H
#define POINTEREVENTDATA_TC18994283B7753E430E316A62D9E45BA6D644C63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63  : public BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData_InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___m_PointerPress_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___hovered_9)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_TC18994283B7753E430E316A62D9E45BA6D644C63_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#define NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 
{
public:
	// UnityEngine.UI.Navigation_Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnUp_1)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnDown_2)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnLeft_3)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnRight_4)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#ifndef PLAYERPOINTEREVENTDATA_T568A431E46421BCA3C7EEC563BBFA6E0BB189D72_H
#define PLAYERPOINTEREVENTDATA_T568A431E46421BCA3C7EEC563BBFA6E0BB189D72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Integration.UnityUI.PlayerPointerEventData
struct  PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72  : public PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63
{
public:
	// System.Int32 Rewired.Integration.UnityUI.PlayerPointerEventData::<playerId>k__BackingField
	int32_t ___U3CplayerIdU3Ek__BackingField_23;
	// System.Int32 Rewired.Integration.UnityUI.PlayerPointerEventData::<inputSourceIndex>k__BackingField
	int32_t ___U3CinputSourceIndexU3Ek__BackingField_24;
	// Rewired.UI.IMouseInputSource Rewired.Integration.UnityUI.PlayerPointerEventData::<mouseSource>k__BackingField
	RuntimeObject* ___U3CmouseSourceU3Ek__BackingField_25;
	// Rewired.UI.ITouchInputSource Rewired.Integration.UnityUI.PlayerPointerEventData::<touchSource>k__BackingField
	RuntimeObject* ___U3CtouchSourceU3Ek__BackingField_26;
	// Rewired.Integration.UnityUI.PointerEventType Rewired.Integration.UnityUI.PlayerPointerEventData::<sourceType>k__BackingField
	int32_t ___U3CsourceTypeU3Ek__BackingField_27;
	// System.Int32 Rewired.Integration.UnityUI.PlayerPointerEventData::<buttonIndex>k__BackingField
	int32_t ___U3CbuttonIndexU3Ek__BackingField_28;

public:
	inline static int32_t get_offset_of_U3CplayerIdU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72, ___U3CplayerIdU3Ek__BackingField_23)); }
	inline int32_t get_U3CplayerIdU3Ek__BackingField_23() const { return ___U3CplayerIdU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3CplayerIdU3Ek__BackingField_23() { return &___U3CplayerIdU3Ek__BackingField_23; }
	inline void set_U3CplayerIdU3Ek__BackingField_23(int32_t value)
	{
		___U3CplayerIdU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CinputSourceIndexU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72, ___U3CinputSourceIndexU3Ek__BackingField_24)); }
	inline int32_t get_U3CinputSourceIndexU3Ek__BackingField_24() const { return ___U3CinputSourceIndexU3Ek__BackingField_24; }
	inline int32_t* get_address_of_U3CinputSourceIndexU3Ek__BackingField_24() { return &___U3CinputSourceIndexU3Ek__BackingField_24; }
	inline void set_U3CinputSourceIndexU3Ek__BackingField_24(int32_t value)
	{
		___U3CinputSourceIndexU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CmouseSourceU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72, ___U3CmouseSourceU3Ek__BackingField_25)); }
	inline RuntimeObject* get_U3CmouseSourceU3Ek__BackingField_25() const { return ___U3CmouseSourceU3Ek__BackingField_25; }
	inline RuntimeObject** get_address_of_U3CmouseSourceU3Ek__BackingField_25() { return &___U3CmouseSourceU3Ek__BackingField_25; }
	inline void set_U3CmouseSourceU3Ek__BackingField_25(RuntimeObject* value)
	{
		___U3CmouseSourceU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmouseSourceU3Ek__BackingField_25), value);
	}

	inline static int32_t get_offset_of_U3CtouchSourceU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72, ___U3CtouchSourceU3Ek__BackingField_26)); }
	inline RuntimeObject* get_U3CtouchSourceU3Ek__BackingField_26() const { return ___U3CtouchSourceU3Ek__BackingField_26; }
	inline RuntimeObject** get_address_of_U3CtouchSourceU3Ek__BackingField_26() { return &___U3CtouchSourceU3Ek__BackingField_26; }
	inline void set_U3CtouchSourceU3Ek__BackingField_26(RuntimeObject* value)
	{
		___U3CtouchSourceU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtouchSourceU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CsourceTypeU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72, ___U3CsourceTypeU3Ek__BackingField_27)); }
	inline int32_t get_U3CsourceTypeU3Ek__BackingField_27() const { return ___U3CsourceTypeU3Ek__BackingField_27; }
	inline int32_t* get_address_of_U3CsourceTypeU3Ek__BackingField_27() { return &___U3CsourceTypeU3Ek__BackingField_27; }
	inline void set_U3CsourceTypeU3Ek__BackingField_27(int32_t value)
	{
		___U3CsourceTypeU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonIndexU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72, ___U3CbuttonIndexU3Ek__BackingField_28)); }
	inline int32_t get_U3CbuttonIndexU3Ek__BackingField_28() const { return ___U3CbuttonIndexU3Ek__BackingField_28; }
	inline int32_t* get_address_of_U3CbuttonIndexU3Ek__BackingField_28() { return &___U3CbuttonIndexU3Ek__BackingField_28; }
	inline void set_U3CbuttonIndexU3Ek__BackingField_28(int32_t value)
	{
		___U3CbuttonIndexU3Ek__BackingField_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPOINTEREVENTDATA_T568A431E46421BCA3C7EEC563BBFA6E0BB189D72_H
#ifndef U3CU3EC__DISPLAYCLASS346_0_TE8414A504024A743399EA1C32D6978A7288844D6_H
#define U3CU3EC__DISPLAYCLASS346_0_TE8414A504024A743399EA1C32D6978A7288844D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass346_0
struct  U3CU3Ec__DisplayClass346_0_tE8414A504024A743399EA1C32D6978A7288844D6  : public RuntimeObject
{
public:
	// Rewired.UI.ControlMapper.ControlMapper Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass346_0::<>4__this
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * ___U3CU3E4__this_0;
	// Rewired.UI.ControlMapper.Window Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass346_0::window
	Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * ___window_1;
	// Rewired.ElementAssignment Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass346_0::assignment
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  ___assignment_2;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper_<>c__DisplayClass346_0::skipOtherPlayers
	bool ___skipOtherPlayers_3;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass346_0_tE8414A504024A743399EA1C32D6978A7288844D6, ___U3CU3E4__this_0)); }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_window_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass346_0_tE8414A504024A743399EA1C32D6978A7288844D6, ___window_1)); }
	inline Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * get_window_1() const { return ___window_1; }
	inline Window_t05E229BCE867863C1A33354186F3E7641707AAA5 ** get_address_of_window_1() { return &___window_1; }
	inline void set_window_1(Window_t05E229BCE867863C1A33354186F3E7641707AAA5 * value)
	{
		___window_1 = value;
		Il2CppCodeGenWriteBarrier((&___window_1), value);
	}

	inline static int32_t get_offset_of_assignment_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass346_0_tE8414A504024A743399EA1C32D6978A7288844D6, ___assignment_2)); }
	inline ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  get_assignment_2() const { return ___assignment_2; }
	inline ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C * get_address_of_assignment_2() { return &___assignment_2; }
	inline void set_assignment_2(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  value)
	{
		___assignment_2 = value;
	}

	inline static int32_t get_offset_of_skipOtherPlayers_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass346_0_tE8414A504024A743399EA1C32D6978A7288844D6, ___skipOtherPlayers_3)); }
	inline bool get_skipOtherPlayers_3() const { return ___skipOtherPlayers_3; }
	inline bool* get_address_of_skipOtherPlayers_3() { return &___skipOtherPlayers_3; }
	inline void set_skipOtherPlayers_3(bool value)
	{
		___skipOtherPlayers_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS346_0_TE8414A504024A743399EA1C32D6978A7288844D6_H
#ifndef U3CELEMENTASSIGNMENTCONFLICTSU3ED__408_TEEA91562F994AE9AEE1D720018BA491F85A5BBD0_H
#define U3CELEMENTASSIGNMENTCONFLICTSU3ED__408_TEEA91562F994AE9AEE1D720018BA491F85A5BBD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408
struct  U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0  : public RuntimeObject
{
public:
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Rewired.ElementAssignmentConflictInfo Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<>2__current
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___U3CU3E2__current_1;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Rewired.Player Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::player
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___player_3;
	// Rewired.Player Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<>3__player
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___U3CU3E3__player_4;
	// Rewired.UI.ControlMapper.ControlMapper_InputMapping Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::mapping
	InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A * ___mapping_5;
	// Rewired.UI.ControlMapper.ControlMapper_InputMapping Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<>3__mapping
	InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A * ___U3CU3E3__mapping_6;
	// Rewired.ElementAssignment Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::assignment
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  ___assignment_7;
	// Rewired.ElementAssignment Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<>3__assignment
	ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  ___U3CU3E3__assignment_8;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::skipOtherPlayers
	bool ___skipOtherPlayers_9;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<>3__skipOtherPlayers
	bool ___U3CU3E3__skipOtherPlayers_10;
	// Rewired.UI.ControlMapper.ControlMapper Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<>4__this
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * ___U3CU3E4__this_11;
	// Rewired.ElementAssignmentConflictCheck Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<conflictCheck>5__1
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___U3CconflictCheckU3E5__1_12;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<>s__2
	RuntimeObject* ___U3CU3Es__2_13;
	// Rewired.ElementAssignmentConflictInfo Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<conflict>5__3
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___U3CconflictU3E5__3_14;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<>s__4
	RuntimeObject* ___U3CU3Es__4_15;
	// Rewired.ElementAssignmentConflictInfo Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<conflict>5__5
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___U3CconflictU3E5__5_16;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<>s__6
	RuntimeObject* ___U3CU3Es__6_17;
	// Rewired.ElementAssignmentConflictInfo Rewired.UI.ControlMapper.ControlMapper_<ElementAssignmentConflicts>d__408::<conflict>5__7
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___U3CconflictU3E5__7_18;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CU3E2__current_1)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___player_3)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_player_3() const { return ___player_3; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__player_4() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CU3E3__player_4)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_U3CU3E3__player_4() const { return ___U3CU3E3__player_4; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_U3CU3E3__player_4() { return &___U3CU3E3__player_4; }
	inline void set_U3CU3E3__player_4(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___U3CU3E3__player_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__player_4), value);
	}

	inline static int32_t get_offset_of_mapping_5() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___mapping_5)); }
	inline InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A * get_mapping_5() const { return ___mapping_5; }
	inline InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A ** get_address_of_mapping_5() { return &___mapping_5; }
	inline void set_mapping_5(InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A * value)
	{
		___mapping_5 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__mapping_6() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CU3E3__mapping_6)); }
	inline InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A * get_U3CU3E3__mapping_6() const { return ___U3CU3E3__mapping_6; }
	inline InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A ** get_address_of_U3CU3E3__mapping_6() { return &___U3CU3E3__mapping_6; }
	inline void set_U3CU3E3__mapping_6(InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A * value)
	{
		___U3CU3E3__mapping_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__mapping_6), value);
	}

	inline static int32_t get_offset_of_assignment_7() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___assignment_7)); }
	inline ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  get_assignment_7() const { return ___assignment_7; }
	inline ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C * get_address_of_assignment_7() { return &___assignment_7; }
	inline void set_assignment_7(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  value)
	{
		___assignment_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__assignment_8() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CU3E3__assignment_8)); }
	inline ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  get_U3CU3E3__assignment_8() const { return ___U3CU3E3__assignment_8; }
	inline ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C * get_address_of_U3CU3E3__assignment_8() { return &___U3CU3E3__assignment_8; }
	inline void set_U3CU3E3__assignment_8(ElementAssignment_tB4E14263A80B282326670C629692DBEE4AF1CC8C  value)
	{
		___U3CU3E3__assignment_8 = value;
	}

	inline static int32_t get_offset_of_skipOtherPlayers_9() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___skipOtherPlayers_9)); }
	inline bool get_skipOtherPlayers_9() const { return ___skipOtherPlayers_9; }
	inline bool* get_address_of_skipOtherPlayers_9() { return &___skipOtherPlayers_9; }
	inline void set_skipOtherPlayers_9(bool value)
	{
		___skipOtherPlayers_9 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__skipOtherPlayers_10() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CU3E3__skipOtherPlayers_10)); }
	inline bool get_U3CU3E3__skipOtherPlayers_10() const { return ___U3CU3E3__skipOtherPlayers_10; }
	inline bool* get_address_of_U3CU3E3__skipOtherPlayers_10() { return &___U3CU3E3__skipOtherPlayers_10; }
	inline void set_U3CU3E3__skipOtherPlayers_10(bool value)
	{
		___U3CU3E3__skipOtherPlayers_10 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_11() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CU3E4__this_11)); }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * get_U3CU3E4__this_11() const { return ___U3CU3E4__this_11; }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 ** get_address_of_U3CU3E4__this_11() { return &___U3CU3E4__this_11; }
	inline void set_U3CU3E4__this_11(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * value)
	{
		___U3CU3E4__this_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_11), value);
	}

	inline static int32_t get_offset_of_U3CconflictCheckU3E5__1_12() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CconflictCheckU3E5__1_12)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_U3CconflictCheckU3E5__1_12() const { return ___U3CconflictCheckU3E5__1_12; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_U3CconflictCheckU3E5__1_12() { return &___U3CconflictCheckU3E5__1_12; }
	inline void set_U3CconflictCheckU3E5__1_12(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___U3CconflictCheckU3E5__1_12 = value;
	}

	inline static int32_t get_offset_of_U3CU3Es__2_13() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CU3Es__2_13)); }
	inline RuntimeObject* get_U3CU3Es__2_13() const { return ___U3CU3Es__2_13; }
	inline RuntimeObject** get_address_of_U3CU3Es__2_13() { return &___U3CU3Es__2_13; }
	inline void set_U3CU3Es__2_13(RuntimeObject* value)
	{
		___U3CU3Es__2_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__2_13), value);
	}

	inline static int32_t get_offset_of_U3CconflictU3E5__3_14() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CconflictU3E5__3_14)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_U3CconflictU3E5__3_14() const { return ___U3CconflictU3E5__3_14; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_U3CconflictU3E5__3_14() { return &___U3CconflictU3E5__3_14; }
	inline void set_U3CconflictU3E5__3_14(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___U3CconflictU3E5__3_14 = value;
	}

	inline static int32_t get_offset_of_U3CU3Es__4_15() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CU3Es__4_15)); }
	inline RuntimeObject* get_U3CU3Es__4_15() const { return ___U3CU3Es__4_15; }
	inline RuntimeObject** get_address_of_U3CU3Es__4_15() { return &___U3CU3Es__4_15; }
	inline void set_U3CU3Es__4_15(RuntimeObject* value)
	{
		___U3CU3Es__4_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__4_15), value);
	}

	inline static int32_t get_offset_of_U3CconflictU3E5__5_16() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CconflictU3E5__5_16)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_U3CconflictU3E5__5_16() const { return ___U3CconflictU3E5__5_16; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_U3CconflictU3E5__5_16() { return &___U3CconflictU3E5__5_16; }
	inline void set_U3CconflictU3E5__5_16(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___U3CconflictU3E5__5_16 = value;
	}

	inline static int32_t get_offset_of_U3CU3Es__6_17() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CU3Es__6_17)); }
	inline RuntimeObject* get_U3CU3Es__6_17() const { return ___U3CU3Es__6_17; }
	inline RuntimeObject** get_address_of_U3CU3Es__6_17() { return &___U3CU3Es__6_17; }
	inline void set_U3CU3Es__6_17(RuntimeObject* value)
	{
		___U3CU3Es__6_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__6_17), value);
	}

	inline static int32_t get_offset_of_U3CconflictU3E5__7_18() { return static_cast<int32_t>(offsetof(U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0, ___U3CconflictU3E5__7_18)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_U3CconflictU3E5__7_18() const { return ___U3CconflictU3E5__7_18; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_U3CconflictU3E5__7_18() { return &___U3CconflictU3E5__7_18; }
	inline void set_U3CconflictU3E5__7_18(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___U3CconflictU3E5__7_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CELEMENTASSIGNMENTCONFLICTSU3ED__408_TEEA91562F994AE9AEE1D720018BA491F85A5BBD0_H
#ifndef AXISCALIBRATOR_T02095EA040609684DCE9299C1B8C6CB31647A237_H
#define AXISCALIBRATOR_T02095EA040609684DCE9299C1B8C6CB31647A237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_AxisCalibrator
struct  AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237  : public RuntimeObject
{
public:
	// Rewired.AxisCalibrationData Rewired.UI.ControlMapper.ControlMapper_AxisCalibrator::data
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171  ___data_0;
	// Rewired.Joystick Rewired.UI.ControlMapper.ControlMapper_AxisCalibrator::joystick
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___joystick_1;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_AxisCalibrator::axisIndex
	int32_t ___axisIndex_2;
	// Rewired.Controller_Axis Rewired.UI.ControlMapper.ControlMapper_AxisCalibrator::axis
	Axis_t1E0EEBF040B05CB6932311D5F7233C785BB6BEC2 * ___axis_3;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper_AxisCalibrator::firstRun
	bool ___firstRun_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237, ___data_0)); }
	inline AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171  get_data_0() const { return ___data_0; }
	inline AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171 * get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171  value)
	{
		___data_0 = value;
	}

	inline static int32_t get_offset_of_joystick_1() { return static_cast<int32_t>(offsetof(AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237, ___joystick_1)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_joystick_1() const { return ___joystick_1; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_joystick_1() { return &___joystick_1; }
	inline void set_joystick_1(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___joystick_1 = value;
		Il2CppCodeGenWriteBarrier((&___joystick_1), value);
	}

	inline static int32_t get_offset_of_axisIndex_2() { return static_cast<int32_t>(offsetof(AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237, ___axisIndex_2)); }
	inline int32_t get_axisIndex_2() const { return ___axisIndex_2; }
	inline int32_t* get_address_of_axisIndex_2() { return &___axisIndex_2; }
	inline void set_axisIndex_2(int32_t value)
	{
		___axisIndex_2 = value;
	}

	inline static int32_t get_offset_of_axis_3() { return static_cast<int32_t>(offsetof(AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237, ___axis_3)); }
	inline Axis_t1E0EEBF040B05CB6932311D5F7233C785BB6BEC2 * get_axis_3() const { return ___axis_3; }
	inline Axis_t1E0EEBF040B05CB6932311D5F7233C785BB6BEC2 ** get_address_of_axis_3() { return &___axis_3; }
	inline void set_axis_3(Axis_t1E0EEBF040B05CB6932311D5F7233C785BB6BEC2 * value)
	{
		___axis_3 = value;
		Il2CppCodeGenWriteBarrier((&___axis_3), value);
	}

	inline static int32_t get_offset_of_firstRun_4() { return static_cast<int32_t>(offsetof(AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237, ___firstRun_4)); }
	inline bool get_firstRun_4() const { return ___firstRun_4; }
	inline bool* get_address_of_firstRun_4() { return &___firstRun_4; }
	inline void set_firstRun_4(bool value)
	{
		___firstRun_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCALIBRATOR_T02095EA040609684DCE9299C1B8C6CB31647A237_H
#ifndef INPUTMAPPING_T84EA554839852D43F0B5C38908686A331A3EEA0A_H
#define INPUTMAPPING_T84EA554839852D43F0B5C38908686A331A3EEA0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper_InputMapping
struct  InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A  : public RuntimeObject
{
public:
	// System.String Rewired.UI.ControlMapper.ControlMapper_InputMapping::<actionName>k__BackingField
	String_t* ___U3CactionNameU3Ek__BackingField_0;
	// Rewired.UI.ControlMapper.InputFieldInfo Rewired.UI.ControlMapper.ControlMapper_InputMapping::<fieldInfo>k__BackingField
	InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5 * ___U3CfieldInfoU3Ek__BackingField_1;
	// Rewired.ControllerMap Rewired.UI.ControlMapper.ControlMapper_InputMapping::<map>k__BackingField
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___U3CmapU3Ek__BackingField_2;
	// Rewired.ActionElementMap Rewired.UI.ControlMapper.ControlMapper_InputMapping::<aem>k__BackingField
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___U3CaemU3Ek__BackingField_3;
	// Rewired.ControllerType Rewired.UI.ControlMapper.ControlMapper_InputMapping::<controllerType>k__BackingField
	int32_t ___U3CcontrollerTypeU3Ek__BackingField_4;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper_InputMapping::<controllerId>k__BackingField
	int32_t ___U3CcontrollerIdU3Ek__BackingField_5;
	// Rewired.ControllerPollingInfo Rewired.UI.ControlMapper.ControlMapper_InputMapping::<pollingInfo>k__BackingField
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___U3CpollingInfoU3Ek__BackingField_6;
	// Rewired.ModifierKeyFlags Rewired.UI.ControlMapper.ControlMapper_InputMapping::<modifierKeyFlags>k__BackingField
	int32_t ___U3CmodifierKeyFlagsU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CactionNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A, ___U3CactionNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CactionNameU3Ek__BackingField_0() const { return ___U3CactionNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CactionNameU3Ek__BackingField_0() { return &___U3CactionNameU3Ek__BackingField_0; }
	inline void set_U3CactionNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CactionNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CactionNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CfieldInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A, ___U3CfieldInfoU3Ek__BackingField_1)); }
	inline InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5 * get_U3CfieldInfoU3Ek__BackingField_1() const { return ___U3CfieldInfoU3Ek__BackingField_1; }
	inline InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5 ** get_address_of_U3CfieldInfoU3Ek__BackingField_1() { return &___U3CfieldInfoU3Ek__BackingField_1; }
	inline void set_U3CfieldInfoU3Ek__BackingField_1(InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5 * value)
	{
		___U3CfieldInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfieldInfoU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CmapU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A, ___U3CmapU3Ek__BackingField_2)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_U3CmapU3Ek__BackingField_2() const { return ___U3CmapU3Ek__BackingField_2; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_U3CmapU3Ek__BackingField_2() { return &___U3CmapU3Ek__BackingField_2; }
	inline void set_U3CmapU3Ek__BackingField_2(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___U3CmapU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmapU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CaemU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A, ___U3CaemU3Ek__BackingField_3)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_U3CaemU3Ek__BackingField_3() const { return ___U3CaemU3Ek__BackingField_3; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_U3CaemU3Ek__BackingField_3() { return &___U3CaemU3Ek__BackingField_3; }
	inline void set_U3CaemU3Ek__BackingField_3(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___U3CaemU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaemU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CcontrollerTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A, ___U3CcontrollerTypeU3Ek__BackingField_4)); }
	inline int32_t get_U3CcontrollerTypeU3Ek__BackingField_4() const { return ___U3CcontrollerTypeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CcontrollerTypeU3Ek__BackingField_4() { return &___U3CcontrollerTypeU3Ek__BackingField_4; }
	inline void set_U3CcontrollerTypeU3Ek__BackingField_4(int32_t value)
	{
		___U3CcontrollerTypeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CcontrollerIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A, ___U3CcontrollerIdU3Ek__BackingField_5)); }
	inline int32_t get_U3CcontrollerIdU3Ek__BackingField_5() const { return ___U3CcontrollerIdU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CcontrollerIdU3Ek__BackingField_5() { return &___U3CcontrollerIdU3Ek__BackingField_5; }
	inline void set_U3CcontrollerIdU3Ek__BackingField_5(int32_t value)
	{
		___U3CcontrollerIdU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CpollingInfoU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A, ___U3CpollingInfoU3Ek__BackingField_6)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_U3CpollingInfoU3Ek__BackingField_6() const { return ___U3CpollingInfoU3Ek__BackingField_6; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_U3CpollingInfoU3Ek__BackingField_6() { return &___U3CpollingInfoU3Ek__BackingField_6; }
	inline void set_U3CpollingInfoU3Ek__BackingField_6(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___U3CpollingInfoU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CmodifierKeyFlagsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A, ___U3CmodifierKeyFlagsU3Ek__BackingField_7)); }
	inline int32_t get_U3CmodifierKeyFlagsU3Ek__BackingField_7() const { return ___U3CmodifierKeyFlagsU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CmodifierKeyFlagsU3Ek__BackingField_7() { return &___U3CmodifierKeyFlagsU3Ek__BackingField_7; }
	inline void set_U3CmodifierKeyFlagsU3Ek__BackingField_7(int32_t value)
	{
		___U3CmodifierKeyFlagsU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMAPPING_T84EA554839852D43F0B5C38908686A331A3EEA0A_H
#ifndef LANGUAGEDATA_TAC36375FDC13175378678CDE03A64E3856634A4B_H
#define LANGUAGEDATA_TAC36375FDC13175378678CDE03A64E3856634A4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.LanguageData
struct  LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String Rewired.UI.ControlMapper.LanguageData::_yes
	String_t* ____yes_4;
	// System.String Rewired.UI.ControlMapper.LanguageData::_no
	String_t* ____no_5;
	// System.String Rewired.UI.ControlMapper.LanguageData::_add
	String_t* ____add_6;
	// System.String Rewired.UI.ControlMapper.LanguageData::_replace
	String_t* ____replace_7;
	// System.String Rewired.UI.ControlMapper.LanguageData::_remove
	String_t* ____remove_8;
	// System.String Rewired.UI.ControlMapper.LanguageData::_swap
	String_t* ____swap_9;
	// System.String Rewired.UI.ControlMapper.LanguageData::_cancel
	String_t* ____cancel_10;
	// System.String Rewired.UI.ControlMapper.LanguageData::_none
	String_t* ____none_11;
	// System.String Rewired.UI.ControlMapper.LanguageData::_okay
	String_t* ____okay_12;
	// System.String Rewired.UI.ControlMapper.LanguageData::_done
	String_t* ____done_13;
	// System.String Rewired.UI.ControlMapper.LanguageData::_default
	String_t* ____default_14;
	// System.String Rewired.UI.ControlMapper.LanguageData::_assignControllerWindowTitle
	String_t* ____assignControllerWindowTitle_15;
	// System.String Rewired.UI.ControlMapper.LanguageData::_assignControllerWindowMessage
	String_t* ____assignControllerWindowMessage_16;
	// System.String Rewired.UI.ControlMapper.LanguageData::_controllerAssignmentConflictWindowTitle
	String_t* ____controllerAssignmentConflictWindowTitle_17;
	// System.String Rewired.UI.ControlMapper.LanguageData::_controllerAssignmentConflictWindowMessage
	String_t* ____controllerAssignmentConflictWindowMessage_18;
	// System.String Rewired.UI.ControlMapper.LanguageData::_elementAssignmentPrePollingWindowMessage
	String_t* ____elementAssignmentPrePollingWindowMessage_19;
	// System.String Rewired.UI.ControlMapper.LanguageData::_joystickElementAssignmentPollingWindowMessage
	String_t* ____joystickElementAssignmentPollingWindowMessage_20;
	// System.String Rewired.UI.ControlMapper.LanguageData::_joystickElementAssignmentPollingWindowMessage_fullAxisFieldOnly
	String_t* ____joystickElementAssignmentPollingWindowMessage_fullAxisFieldOnly_21;
	// System.String Rewired.UI.ControlMapper.LanguageData::_keyboardElementAssignmentPollingWindowMessage
	String_t* ____keyboardElementAssignmentPollingWindowMessage_22;
	// System.String Rewired.UI.ControlMapper.LanguageData::_mouseElementAssignmentPollingWindowMessage
	String_t* ____mouseElementAssignmentPollingWindowMessage_23;
	// System.String Rewired.UI.ControlMapper.LanguageData::_mouseElementAssignmentPollingWindowMessage_fullAxisFieldOnly
	String_t* ____mouseElementAssignmentPollingWindowMessage_fullAxisFieldOnly_24;
	// System.String Rewired.UI.ControlMapper.LanguageData::_elementAssignmentConflictWindowMessage
	String_t* ____elementAssignmentConflictWindowMessage_25;
	// System.String Rewired.UI.ControlMapper.LanguageData::_elementAlreadyInUseBlocked
	String_t* ____elementAlreadyInUseBlocked_26;
	// System.String Rewired.UI.ControlMapper.LanguageData::_elementAlreadyInUseCanReplace
	String_t* ____elementAlreadyInUseCanReplace_27;
	// System.String Rewired.UI.ControlMapper.LanguageData::_elementAlreadyInUseCanReplace_conflictAllowed
	String_t* ____elementAlreadyInUseCanReplace_conflictAllowed_28;
	// System.String Rewired.UI.ControlMapper.LanguageData::_mouseAssignmentConflictWindowTitle
	String_t* ____mouseAssignmentConflictWindowTitle_29;
	// System.String Rewired.UI.ControlMapper.LanguageData::_mouseAssignmentConflictWindowMessage
	String_t* ____mouseAssignmentConflictWindowMessage_30;
	// System.String Rewired.UI.ControlMapper.LanguageData::_calibrateControllerWindowTitle
	String_t* ____calibrateControllerWindowTitle_31;
	// System.String Rewired.UI.ControlMapper.LanguageData::_calibrateAxisStep1WindowTitle
	String_t* ____calibrateAxisStep1WindowTitle_32;
	// System.String Rewired.UI.ControlMapper.LanguageData::_calibrateAxisStep1WindowMessage
	String_t* ____calibrateAxisStep1WindowMessage_33;
	// System.String Rewired.UI.ControlMapper.LanguageData::_calibrateAxisStep2WindowTitle
	String_t* ____calibrateAxisStep2WindowTitle_34;
	// System.String Rewired.UI.ControlMapper.LanguageData::_calibrateAxisStep2WindowMessage
	String_t* ____calibrateAxisStep2WindowMessage_35;
	// System.String Rewired.UI.ControlMapper.LanguageData::_inputBehaviorSettingsWindowTitle
	String_t* ____inputBehaviorSettingsWindowTitle_36;
	// System.String Rewired.UI.ControlMapper.LanguageData::_restoreDefaultsWindowTitle
	String_t* ____restoreDefaultsWindowTitle_37;
	// System.String Rewired.UI.ControlMapper.LanguageData::_restoreDefaultsWindowMessage_onePlayer
	String_t* ____restoreDefaultsWindowMessage_onePlayer_38;
	// System.String Rewired.UI.ControlMapper.LanguageData::_restoreDefaultsWindowMessage_multiPlayer
	String_t* ____restoreDefaultsWindowMessage_multiPlayer_39;
	// System.String Rewired.UI.ControlMapper.LanguageData::_actionColumnLabel
	String_t* ____actionColumnLabel_40;
	// System.String Rewired.UI.ControlMapper.LanguageData::_keyboardColumnLabel
	String_t* ____keyboardColumnLabel_41;
	// System.String Rewired.UI.ControlMapper.LanguageData::_mouseColumnLabel
	String_t* ____mouseColumnLabel_42;
	// System.String Rewired.UI.ControlMapper.LanguageData::_controllerColumnLabel
	String_t* ____controllerColumnLabel_43;
	// System.String Rewired.UI.ControlMapper.LanguageData::_removeControllerButtonLabel
	String_t* ____removeControllerButtonLabel_44;
	// System.String Rewired.UI.ControlMapper.LanguageData::_calibrateControllerButtonLabel
	String_t* ____calibrateControllerButtonLabel_45;
	// System.String Rewired.UI.ControlMapper.LanguageData::_assignControllerButtonLabel
	String_t* ____assignControllerButtonLabel_46;
	// System.String Rewired.UI.ControlMapper.LanguageData::_inputBehaviorSettingsButtonLabel
	String_t* ____inputBehaviorSettingsButtonLabel_47;
	// System.String Rewired.UI.ControlMapper.LanguageData::_doneButtonLabel
	String_t* ____doneButtonLabel_48;
	// System.String Rewired.UI.ControlMapper.LanguageData::_restoreDefaultsButtonLabel
	String_t* ____restoreDefaultsButtonLabel_49;
	// System.String Rewired.UI.ControlMapper.LanguageData::_playersGroupLabel
	String_t* ____playersGroupLabel_50;
	// System.String Rewired.UI.ControlMapper.LanguageData::_controllerSettingsGroupLabel
	String_t* ____controllerSettingsGroupLabel_51;
	// System.String Rewired.UI.ControlMapper.LanguageData::_assignedControllersGroupLabel
	String_t* ____assignedControllersGroupLabel_52;
	// System.String Rewired.UI.ControlMapper.LanguageData::_settingsGroupLabel
	String_t* ____settingsGroupLabel_53;
	// System.String Rewired.UI.ControlMapper.LanguageData::_mapCategoriesGroupLabel
	String_t* ____mapCategoriesGroupLabel_54;
	// System.String Rewired.UI.ControlMapper.LanguageData::_calibrateWindow_deadZoneSliderLabel
	String_t* ____calibrateWindow_deadZoneSliderLabel_55;
	// System.String Rewired.UI.ControlMapper.LanguageData::_calibrateWindow_zeroSliderLabel
	String_t* ____calibrateWindow_zeroSliderLabel_56;
	// System.String Rewired.UI.ControlMapper.LanguageData::_calibrateWindow_sensitivitySliderLabel
	String_t* ____calibrateWindow_sensitivitySliderLabel_57;
	// System.String Rewired.UI.ControlMapper.LanguageData::_calibrateWindow_invertToggleLabel
	String_t* ____calibrateWindow_invertToggleLabel_58;
	// System.String Rewired.UI.ControlMapper.LanguageData::_calibrateWindow_calibrateButtonLabel
	String_t* ____calibrateWindow_calibrateButtonLabel_59;
	// Rewired.UI.ControlMapper.LanguageData_CustomEntry[] Rewired.UI.ControlMapper.LanguageData::_customEntries
	CustomEntryU5BU5D_t40CEB5E7F2BCDECE988328E9E4875B2A8EE36E85* ____customEntries_60;
	// System.Boolean Rewired.UI.ControlMapper.LanguageData::_initialized
	bool ____initialized_61;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Rewired.UI.ControlMapper.LanguageData::customDict
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___customDict_62;

public:
	inline static int32_t get_offset_of__yes_4() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____yes_4)); }
	inline String_t* get__yes_4() const { return ____yes_4; }
	inline String_t** get_address_of__yes_4() { return &____yes_4; }
	inline void set__yes_4(String_t* value)
	{
		____yes_4 = value;
		Il2CppCodeGenWriteBarrier((&____yes_4), value);
	}

	inline static int32_t get_offset_of__no_5() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____no_5)); }
	inline String_t* get__no_5() const { return ____no_5; }
	inline String_t** get_address_of__no_5() { return &____no_5; }
	inline void set__no_5(String_t* value)
	{
		____no_5 = value;
		Il2CppCodeGenWriteBarrier((&____no_5), value);
	}

	inline static int32_t get_offset_of__add_6() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____add_6)); }
	inline String_t* get__add_6() const { return ____add_6; }
	inline String_t** get_address_of__add_6() { return &____add_6; }
	inline void set__add_6(String_t* value)
	{
		____add_6 = value;
		Il2CppCodeGenWriteBarrier((&____add_6), value);
	}

	inline static int32_t get_offset_of__replace_7() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____replace_7)); }
	inline String_t* get__replace_7() const { return ____replace_7; }
	inline String_t** get_address_of__replace_7() { return &____replace_7; }
	inline void set__replace_7(String_t* value)
	{
		____replace_7 = value;
		Il2CppCodeGenWriteBarrier((&____replace_7), value);
	}

	inline static int32_t get_offset_of__remove_8() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____remove_8)); }
	inline String_t* get__remove_8() const { return ____remove_8; }
	inline String_t** get_address_of__remove_8() { return &____remove_8; }
	inline void set__remove_8(String_t* value)
	{
		____remove_8 = value;
		Il2CppCodeGenWriteBarrier((&____remove_8), value);
	}

	inline static int32_t get_offset_of__swap_9() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____swap_9)); }
	inline String_t* get__swap_9() const { return ____swap_9; }
	inline String_t** get_address_of__swap_9() { return &____swap_9; }
	inline void set__swap_9(String_t* value)
	{
		____swap_9 = value;
		Il2CppCodeGenWriteBarrier((&____swap_9), value);
	}

	inline static int32_t get_offset_of__cancel_10() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____cancel_10)); }
	inline String_t* get__cancel_10() const { return ____cancel_10; }
	inline String_t** get_address_of__cancel_10() { return &____cancel_10; }
	inline void set__cancel_10(String_t* value)
	{
		____cancel_10 = value;
		Il2CppCodeGenWriteBarrier((&____cancel_10), value);
	}

	inline static int32_t get_offset_of__none_11() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____none_11)); }
	inline String_t* get__none_11() const { return ____none_11; }
	inline String_t** get_address_of__none_11() { return &____none_11; }
	inline void set__none_11(String_t* value)
	{
		____none_11 = value;
		Il2CppCodeGenWriteBarrier((&____none_11), value);
	}

	inline static int32_t get_offset_of__okay_12() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____okay_12)); }
	inline String_t* get__okay_12() const { return ____okay_12; }
	inline String_t** get_address_of__okay_12() { return &____okay_12; }
	inline void set__okay_12(String_t* value)
	{
		____okay_12 = value;
		Il2CppCodeGenWriteBarrier((&____okay_12), value);
	}

	inline static int32_t get_offset_of__done_13() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____done_13)); }
	inline String_t* get__done_13() const { return ____done_13; }
	inline String_t** get_address_of__done_13() { return &____done_13; }
	inline void set__done_13(String_t* value)
	{
		____done_13 = value;
		Il2CppCodeGenWriteBarrier((&____done_13), value);
	}

	inline static int32_t get_offset_of__default_14() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____default_14)); }
	inline String_t* get__default_14() const { return ____default_14; }
	inline String_t** get_address_of__default_14() { return &____default_14; }
	inline void set__default_14(String_t* value)
	{
		____default_14 = value;
		Il2CppCodeGenWriteBarrier((&____default_14), value);
	}

	inline static int32_t get_offset_of__assignControllerWindowTitle_15() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____assignControllerWindowTitle_15)); }
	inline String_t* get__assignControllerWindowTitle_15() const { return ____assignControllerWindowTitle_15; }
	inline String_t** get_address_of__assignControllerWindowTitle_15() { return &____assignControllerWindowTitle_15; }
	inline void set__assignControllerWindowTitle_15(String_t* value)
	{
		____assignControllerWindowTitle_15 = value;
		Il2CppCodeGenWriteBarrier((&____assignControllerWindowTitle_15), value);
	}

	inline static int32_t get_offset_of__assignControllerWindowMessage_16() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____assignControllerWindowMessage_16)); }
	inline String_t* get__assignControllerWindowMessage_16() const { return ____assignControllerWindowMessage_16; }
	inline String_t** get_address_of__assignControllerWindowMessage_16() { return &____assignControllerWindowMessage_16; }
	inline void set__assignControllerWindowMessage_16(String_t* value)
	{
		____assignControllerWindowMessage_16 = value;
		Il2CppCodeGenWriteBarrier((&____assignControllerWindowMessage_16), value);
	}

	inline static int32_t get_offset_of__controllerAssignmentConflictWindowTitle_17() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____controllerAssignmentConflictWindowTitle_17)); }
	inline String_t* get__controllerAssignmentConflictWindowTitle_17() const { return ____controllerAssignmentConflictWindowTitle_17; }
	inline String_t** get_address_of__controllerAssignmentConflictWindowTitle_17() { return &____controllerAssignmentConflictWindowTitle_17; }
	inline void set__controllerAssignmentConflictWindowTitle_17(String_t* value)
	{
		____controllerAssignmentConflictWindowTitle_17 = value;
		Il2CppCodeGenWriteBarrier((&____controllerAssignmentConflictWindowTitle_17), value);
	}

	inline static int32_t get_offset_of__controllerAssignmentConflictWindowMessage_18() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____controllerAssignmentConflictWindowMessage_18)); }
	inline String_t* get__controllerAssignmentConflictWindowMessage_18() const { return ____controllerAssignmentConflictWindowMessage_18; }
	inline String_t** get_address_of__controllerAssignmentConflictWindowMessage_18() { return &____controllerAssignmentConflictWindowMessage_18; }
	inline void set__controllerAssignmentConflictWindowMessage_18(String_t* value)
	{
		____controllerAssignmentConflictWindowMessage_18 = value;
		Il2CppCodeGenWriteBarrier((&____controllerAssignmentConflictWindowMessage_18), value);
	}

	inline static int32_t get_offset_of__elementAssignmentPrePollingWindowMessage_19() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____elementAssignmentPrePollingWindowMessage_19)); }
	inline String_t* get__elementAssignmentPrePollingWindowMessage_19() const { return ____elementAssignmentPrePollingWindowMessage_19; }
	inline String_t** get_address_of__elementAssignmentPrePollingWindowMessage_19() { return &____elementAssignmentPrePollingWindowMessage_19; }
	inline void set__elementAssignmentPrePollingWindowMessage_19(String_t* value)
	{
		____elementAssignmentPrePollingWindowMessage_19 = value;
		Il2CppCodeGenWriteBarrier((&____elementAssignmentPrePollingWindowMessage_19), value);
	}

	inline static int32_t get_offset_of__joystickElementAssignmentPollingWindowMessage_20() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____joystickElementAssignmentPollingWindowMessage_20)); }
	inline String_t* get__joystickElementAssignmentPollingWindowMessage_20() const { return ____joystickElementAssignmentPollingWindowMessage_20; }
	inline String_t** get_address_of__joystickElementAssignmentPollingWindowMessage_20() { return &____joystickElementAssignmentPollingWindowMessage_20; }
	inline void set__joystickElementAssignmentPollingWindowMessage_20(String_t* value)
	{
		____joystickElementAssignmentPollingWindowMessage_20 = value;
		Il2CppCodeGenWriteBarrier((&____joystickElementAssignmentPollingWindowMessage_20), value);
	}

	inline static int32_t get_offset_of__joystickElementAssignmentPollingWindowMessage_fullAxisFieldOnly_21() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____joystickElementAssignmentPollingWindowMessage_fullAxisFieldOnly_21)); }
	inline String_t* get__joystickElementAssignmentPollingWindowMessage_fullAxisFieldOnly_21() const { return ____joystickElementAssignmentPollingWindowMessage_fullAxisFieldOnly_21; }
	inline String_t** get_address_of__joystickElementAssignmentPollingWindowMessage_fullAxisFieldOnly_21() { return &____joystickElementAssignmentPollingWindowMessage_fullAxisFieldOnly_21; }
	inline void set__joystickElementAssignmentPollingWindowMessage_fullAxisFieldOnly_21(String_t* value)
	{
		____joystickElementAssignmentPollingWindowMessage_fullAxisFieldOnly_21 = value;
		Il2CppCodeGenWriteBarrier((&____joystickElementAssignmentPollingWindowMessage_fullAxisFieldOnly_21), value);
	}

	inline static int32_t get_offset_of__keyboardElementAssignmentPollingWindowMessage_22() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____keyboardElementAssignmentPollingWindowMessage_22)); }
	inline String_t* get__keyboardElementAssignmentPollingWindowMessage_22() const { return ____keyboardElementAssignmentPollingWindowMessage_22; }
	inline String_t** get_address_of__keyboardElementAssignmentPollingWindowMessage_22() { return &____keyboardElementAssignmentPollingWindowMessage_22; }
	inline void set__keyboardElementAssignmentPollingWindowMessage_22(String_t* value)
	{
		____keyboardElementAssignmentPollingWindowMessage_22 = value;
		Il2CppCodeGenWriteBarrier((&____keyboardElementAssignmentPollingWindowMessage_22), value);
	}

	inline static int32_t get_offset_of__mouseElementAssignmentPollingWindowMessage_23() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____mouseElementAssignmentPollingWindowMessage_23)); }
	inline String_t* get__mouseElementAssignmentPollingWindowMessage_23() const { return ____mouseElementAssignmentPollingWindowMessage_23; }
	inline String_t** get_address_of__mouseElementAssignmentPollingWindowMessage_23() { return &____mouseElementAssignmentPollingWindowMessage_23; }
	inline void set__mouseElementAssignmentPollingWindowMessage_23(String_t* value)
	{
		____mouseElementAssignmentPollingWindowMessage_23 = value;
		Il2CppCodeGenWriteBarrier((&____mouseElementAssignmentPollingWindowMessage_23), value);
	}

	inline static int32_t get_offset_of__mouseElementAssignmentPollingWindowMessage_fullAxisFieldOnly_24() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____mouseElementAssignmentPollingWindowMessage_fullAxisFieldOnly_24)); }
	inline String_t* get__mouseElementAssignmentPollingWindowMessage_fullAxisFieldOnly_24() const { return ____mouseElementAssignmentPollingWindowMessage_fullAxisFieldOnly_24; }
	inline String_t** get_address_of__mouseElementAssignmentPollingWindowMessage_fullAxisFieldOnly_24() { return &____mouseElementAssignmentPollingWindowMessage_fullAxisFieldOnly_24; }
	inline void set__mouseElementAssignmentPollingWindowMessage_fullAxisFieldOnly_24(String_t* value)
	{
		____mouseElementAssignmentPollingWindowMessage_fullAxisFieldOnly_24 = value;
		Il2CppCodeGenWriteBarrier((&____mouseElementAssignmentPollingWindowMessage_fullAxisFieldOnly_24), value);
	}

	inline static int32_t get_offset_of__elementAssignmentConflictWindowMessage_25() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____elementAssignmentConflictWindowMessage_25)); }
	inline String_t* get__elementAssignmentConflictWindowMessage_25() const { return ____elementAssignmentConflictWindowMessage_25; }
	inline String_t** get_address_of__elementAssignmentConflictWindowMessage_25() { return &____elementAssignmentConflictWindowMessage_25; }
	inline void set__elementAssignmentConflictWindowMessage_25(String_t* value)
	{
		____elementAssignmentConflictWindowMessage_25 = value;
		Il2CppCodeGenWriteBarrier((&____elementAssignmentConflictWindowMessage_25), value);
	}

	inline static int32_t get_offset_of__elementAlreadyInUseBlocked_26() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____elementAlreadyInUseBlocked_26)); }
	inline String_t* get__elementAlreadyInUseBlocked_26() const { return ____elementAlreadyInUseBlocked_26; }
	inline String_t** get_address_of__elementAlreadyInUseBlocked_26() { return &____elementAlreadyInUseBlocked_26; }
	inline void set__elementAlreadyInUseBlocked_26(String_t* value)
	{
		____elementAlreadyInUseBlocked_26 = value;
		Il2CppCodeGenWriteBarrier((&____elementAlreadyInUseBlocked_26), value);
	}

	inline static int32_t get_offset_of__elementAlreadyInUseCanReplace_27() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____elementAlreadyInUseCanReplace_27)); }
	inline String_t* get__elementAlreadyInUseCanReplace_27() const { return ____elementAlreadyInUseCanReplace_27; }
	inline String_t** get_address_of__elementAlreadyInUseCanReplace_27() { return &____elementAlreadyInUseCanReplace_27; }
	inline void set__elementAlreadyInUseCanReplace_27(String_t* value)
	{
		____elementAlreadyInUseCanReplace_27 = value;
		Il2CppCodeGenWriteBarrier((&____elementAlreadyInUseCanReplace_27), value);
	}

	inline static int32_t get_offset_of__elementAlreadyInUseCanReplace_conflictAllowed_28() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____elementAlreadyInUseCanReplace_conflictAllowed_28)); }
	inline String_t* get__elementAlreadyInUseCanReplace_conflictAllowed_28() const { return ____elementAlreadyInUseCanReplace_conflictAllowed_28; }
	inline String_t** get_address_of__elementAlreadyInUseCanReplace_conflictAllowed_28() { return &____elementAlreadyInUseCanReplace_conflictAllowed_28; }
	inline void set__elementAlreadyInUseCanReplace_conflictAllowed_28(String_t* value)
	{
		____elementAlreadyInUseCanReplace_conflictAllowed_28 = value;
		Il2CppCodeGenWriteBarrier((&____elementAlreadyInUseCanReplace_conflictAllowed_28), value);
	}

	inline static int32_t get_offset_of__mouseAssignmentConflictWindowTitle_29() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____mouseAssignmentConflictWindowTitle_29)); }
	inline String_t* get__mouseAssignmentConflictWindowTitle_29() const { return ____mouseAssignmentConflictWindowTitle_29; }
	inline String_t** get_address_of__mouseAssignmentConflictWindowTitle_29() { return &____mouseAssignmentConflictWindowTitle_29; }
	inline void set__mouseAssignmentConflictWindowTitle_29(String_t* value)
	{
		____mouseAssignmentConflictWindowTitle_29 = value;
		Il2CppCodeGenWriteBarrier((&____mouseAssignmentConflictWindowTitle_29), value);
	}

	inline static int32_t get_offset_of__mouseAssignmentConflictWindowMessage_30() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____mouseAssignmentConflictWindowMessage_30)); }
	inline String_t* get__mouseAssignmentConflictWindowMessage_30() const { return ____mouseAssignmentConflictWindowMessage_30; }
	inline String_t** get_address_of__mouseAssignmentConflictWindowMessage_30() { return &____mouseAssignmentConflictWindowMessage_30; }
	inline void set__mouseAssignmentConflictWindowMessage_30(String_t* value)
	{
		____mouseAssignmentConflictWindowMessage_30 = value;
		Il2CppCodeGenWriteBarrier((&____mouseAssignmentConflictWindowMessage_30), value);
	}

	inline static int32_t get_offset_of__calibrateControllerWindowTitle_31() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____calibrateControllerWindowTitle_31)); }
	inline String_t* get__calibrateControllerWindowTitle_31() const { return ____calibrateControllerWindowTitle_31; }
	inline String_t** get_address_of__calibrateControllerWindowTitle_31() { return &____calibrateControllerWindowTitle_31; }
	inline void set__calibrateControllerWindowTitle_31(String_t* value)
	{
		____calibrateControllerWindowTitle_31 = value;
		Il2CppCodeGenWriteBarrier((&____calibrateControllerWindowTitle_31), value);
	}

	inline static int32_t get_offset_of__calibrateAxisStep1WindowTitle_32() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____calibrateAxisStep1WindowTitle_32)); }
	inline String_t* get__calibrateAxisStep1WindowTitle_32() const { return ____calibrateAxisStep1WindowTitle_32; }
	inline String_t** get_address_of__calibrateAxisStep1WindowTitle_32() { return &____calibrateAxisStep1WindowTitle_32; }
	inline void set__calibrateAxisStep1WindowTitle_32(String_t* value)
	{
		____calibrateAxisStep1WindowTitle_32 = value;
		Il2CppCodeGenWriteBarrier((&____calibrateAxisStep1WindowTitle_32), value);
	}

	inline static int32_t get_offset_of__calibrateAxisStep1WindowMessage_33() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____calibrateAxisStep1WindowMessage_33)); }
	inline String_t* get__calibrateAxisStep1WindowMessage_33() const { return ____calibrateAxisStep1WindowMessage_33; }
	inline String_t** get_address_of__calibrateAxisStep1WindowMessage_33() { return &____calibrateAxisStep1WindowMessage_33; }
	inline void set__calibrateAxisStep1WindowMessage_33(String_t* value)
	{
		____calibrateAxisStep1WindowMessage_33 = value;
		Il2CppCodeGenWriteBarrier((&____calibrateAxisStep1WindowMessage_33), value);
	}

	inline static int32_t get_offset_of__calibrateAxisStep2WindowTitle_34() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____calibrateAxisStep2WindowTitle_34)); }
	inline String_t* get__calibrateAxisStep2WindowTitle_34() const { return ____calibrateAxisStep2WindowTitle_34; }
	inline String_t** get_address_of__calibrateAxisStep2WindowTitle_34() { return &____calibrateAxisStep2WindowTitle_34; }
	inline void set__calibrateAxisStep2WindowTitle_34(String_t* value)
	{
		____calibrateAxisStep2WindowTitle_34 = value;
		Il2CppCodeGenWriteBarrier((&____calibrateAxisStep2WindowTitle_34), value);
	}

	inline static int32_t get_offset_of__calibrateAxisStep2WindowMessage_35() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____calibrateAxisStep2WindowMessage_35)); }
	inline String_t* get__calibrateAxisStep2WindowMessage_35() const { return ____calibrateAxisStep2WindowMessage_35; }
	inline String_t** get_address_of__calibrateAxisStep2WindowMessage_35() { return &____calibrateAxisStep2WindowMessage_35; }
	inline void set__calibrateAxisStep2WindowMessage_35(String_t* value)
	{
		____calibrateAxisStep2WindowMessage_35 = value;
		Il2CppCodeGenWriteBarrier((&____calibrateAxisStep2WindowMessage_35), value);
	}

	inline static int32_t get_offset_of__inputBehaviorSettingsWindowTitle_36() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____inputBehaviorSettingsWindowTitle_36)); }
	inline String_t* get__inputBehaviorSettingsWindowTitle_36() const { return ____inputBehaviorSettingsWindowTitle_36; }
	inline String_t** get_address_of__inputBehaviorSettingsWindowTitle_36() { return &____inputBehaviorSettingsWindowTitle_36; }
	inline void set__inputBehaviorSettingsWindowTitle_36(String_t* value)
	{
		____inputBehaviorSettingsWindowTitle_36 = value;
		Il2CppCodeGenWriteBarrier((&____inputBehaviorSettingsWindowTitle_36), value);
	}

	inline static int32_t get_offset_of__restoreDefaultsWindowTitle_37() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____restoreDefaultsWindowTitle_37)); }
	inline String_t* get__restoreDefaultsWindowTitle_37() const { return ____restoreDefaultsWindowTitle_37; }
	inline String_t** get_address_of__restoreDefaultsWindowTitle_37() { return &____restoreDefaultsWindowTitle_37; }
	inline void set__restoreDefaultsWindowTitle_37(String_t* value)
	{
		____restoreDefaultsWindowTitle_37 = value;
		Il2CppCodeGenWriteBarrier((&____restoreDefaultsWindowTitle_37), value);
	}

	inline static int32_t get_offset_of__restoreDefaultsWindowMessage_onePlayer_38() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____restoreDefaultsWindowMessage_onePlayer_38)); }
	inline String_t* get__restoreDefaultsWindowMessage_onePlayer_38() const { return ____restoreDefaultsWindowMessage_onePlayer_38; }
	inline String_t** get_address_of__restoreDefaultsWindowMessage_onePlayer_38() { return &____restoreDefaultsWindowMessage_onePlayer_38; }
	inline void set__restoreDefaultsWindowMessage_onePlayer_38(String_t* value)
	{
		____restoreDefaultsWindowMessage_onePlayer_38 = value;
		Il2CppCodeGenWriteBarrier((&____restoreDefaultsWindowMessage_onePlayer_38), value);
	}

	inline static int32_t get_offset_of__restoreDefaultsWindowMessage_multiPlayer_39() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____restoreDefaultsWindowMessage_multiPlayer_39)); }
	inline String_t* get__restoreDefaultsWindowMessage_multiPlayer_39() const { return ____restoreDefaultsWindowMessage_multiPlayer_39; }
	inline String_t** get_address_of__restoreDefaultsWindowMessage_multiPlayer_39() { return &____restoreDefaultsWindowMessage_multiPlayer_39; }
	inline void set__restoreDefaultsWindowMessage_multiPlayer_39(String_t* value)
	{
		____restoreDefaultsWindowMessage_multiPlayer_39 = value;
		Il2CppCodeGenWriteBarrier((&____restoreDefaultsWindowMessage_multiPlayer_39), value);
	}

	inline static int32_t get_offset_of__actionColumnLabel_40() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____actionColumnLabel_40)); }
	inline String_t* get__actionColumnLabel_40() const { return ____actionColumnLabel_40; }
	inline String_t** get_address_of__actionColumnLabel_40() { return &____actionColumnLabel_40; }
	inline void set__actionColumnLabel_40(String_t* value)
	{
		____actionColumnLabel_40 = value;
		Il2CppCodeGenWriteBarrier((&____actionColumnLabel_40), value);
	}

	inline static int32_t get_offset_of__keyboardColumnLabel_41() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____keyboardColumnLabel_41)); }
	inline String_t* get__keyboardColumnLabel_41() const { return ____keyboardColumnLabel_41; }
	inline String_t** get_address_of__keyboardColumnLabel_41() { return &____keyboardColumnLabel_41; }
	inline void set__keyboardColumnLabel_41(String_t* value)
	{
		____keyboardColumnLabel_41 = value;
		Il2CppCodeGenWriteBarrier((&____keyboardColumnLabel_41), value);
	}

	inline static int32_t get_offset_of__mouseColumnLabel_42() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____mouseColumnLabel_42)); }
	inline String_t* get__mouseColumnLabel_42() const { return ____mouseColumnLabel_42; }
	inline String_t** get_address_of__mouseColumnLabel_42() { return &____mouseColumnLabel_42; }
	inline void set__mouseColumnLabel_42(String_t* value)
	{
		____mouseColumnLabel_42 = value;
		Il2CppCodeGenWriteBarrier((&____mouseColumnLabel_42), value);
	}

	inline static int32_t get_offset_of__controllerColumnLabel_43() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____controllerColumnLabel_43)); }
	inline String_t* get__controllerColumnLabel_43() const { return ____controllerColumnLabel_43; }
	inline String_t** get_address_of__controllerColumnLabel_43() { return &____controllerColumnLabel_43; }
	inline void set__controllerColumnLabel_43(String_t* value)
	{
		____controllerColumnLabel_43 = value;
		Il2CppCodeGenWriteBarrier((&____controllerColumnLabel_43), value);
	}

	inline static int32_t get_offset_of__removeControllerButtonLabel_44() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____removeControllerButtonLabel_44)); }
	inline String_t* get__removeControllerButtonLabel_44() const { return ____removeControllerButtonLabel_44; }
	inline String_t** get_address_of__removeControllerButtonLabel_44() { return &____removeControllerButtonLabel_44; }
	inline void set__removeControllerButtonLabel_44(String_t* value)
	{
		____removeControllerButtonLabel_44 = value;
		Il2CppCodeGenWriteBarrier((&____removeControllerButtonLabel_44), value);
	}

	inline static int32_t get_offset_of__calibrateControllerButtonLabel_45() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____calibrateControllerButtonLabel_45)); }
	inline String_t* get__calibrateControllerButtonLabel_45() const { return ____calibrateControllerButtonLabel_45; }
	inline String_t** get_address_of__calibrateControllerButtonLabel_45() { return &____calibrateControllerButtonLabel_45; }
	inline void set__calibrateControllerButtonLabel_45(String_t* value)
	{
		____calibrateControllerButtonLabel_45 = value;
		Il2CppCodeGenWriteBarrier((&____calibrateControllerButtonLabel_45), value);
	}

	inline static int32_t get_offset_of__assignControllerButtonLabel_46() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____assignControllerButtonLabel_46)); }
	inline String_t* get__assignControllerButtonLabel_46() const { return ____assignControllerButtonLabel_46; }
	inline String_t** get_address_of__assignControllerButtonLabel_46() { return &____assignControllerButtonLabel_46; }
	inline void set__assignControllerButtonLabel_46(String_t* value)
	{
		____assignControllerButtonLabel_46 = value;
		Il2CppCodeGenWriteBarrier((&____assignControllerButtonLabel_46), value);
	}

	inline static int32_t get_offset_of__inputBehaviorSettingsButtonLabel_47() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____inputBehaviorSettingsButtonLabel_47)); }
	inline String_t* get__inputBehaviorSettingsButtonLabel_47() const { return ____inputBehaviorSettingsButtonLabel_47; }
	inline String_t** get_address_of__inputBehaviorSettingsButtonLabel_47() { return &____inputBehaviorSettingsButtonLabel_47; }
	inline void set__inputBehaviorSettingsButtonLabel_47(String_t* value)
	{
		____inputBehaviorSettingsButtonLabel_47 = value;
		Il2CppCodeGenWriteBarrier((&____inputBehaviorSettingsButtonLabel_47), value);
	}

	inline static int32_t get_offset_of__doneButtonLabel_48() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____doneButtonLabel_48)); }
	inline String_t* get__doneButtonLabel_48() const { return ____doneButtonLabel_48; }
	inline String_t** get_address_of__doneButtonLabel_48() { return &____doneButtonLabel_48; }
	inline void set__doneButtonLabel_48(String_t* value)
	{
		____doneButtonLabel_48 = value;
		Il2CppCodeGenWriteBarrier((&____doneButtonLabel_48), value);
	}

	inline static int32_t get_offset_of__restoreDefaultsButtonLabel_49() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____restoreDefaultsButtonLabel_49)); }
	inline String_t* get__restoreDefaultsButtonLabel_49() const { return ____restoreDefaultsButtonLabel_49; }
	inline String_t** get_address_of__restoreDefaultsButtonLabel_49() { return &____restoreDefaultsButtonLabel_49; }
	inline void set__restoreDefaultsButtonLabel_49(String_t* value)
	{
		____restoreDefaultsButtonLabel_49 = value;
		Il2CppCodeGenWriteBarrier((&____restoreDefaultsButtonLabel_49), value);
	}

	inline static int32_t get_offset_of__playersGroupLabel_50() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____playersGroupLabel_50)); }
	inline String_t* get__playersGroupLabel_50() const { return ____playersGroupLabel_50; }
	inline String_t** get_address_of__playersGroupLabel_50() { return &____playersGroupLabel_50; }
	inline void set__playersGroupLabel_50(String_t* value)
	{
		____playersGroupLabel_50 = value;
		Il2CppCodeGenWriteBarrier((&____playersGroupLabel_50), value);
	}

	inline static int32_t get_offset_of__controllerSettingsGroupLabel_51() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____controllerSettingsGroupLabel_51)); }
	inline String_t* get__controllerSettingsGroupLabel_51() const { return ____controllerSettingsGroupLabel_51; }
	inline String_t** get_address_of__controllerSettingsGroupLabel_51() { return &____controllerSettingsGroupLabel_51; }
	inline void set__controllerSettingsGroupLabel_51(String_t* value)
	{
		____controllerSettingsGroupLabel_51 = value;
		Il2CppCodeGenWriteBarrier((&____controllerSettingsGroupLabel_51), value);
	}

	inline static int32_t get_offset_of__assignedControllersGroupLabel_52() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____assignedControllersGroupLabel_52)); }
	inline String_t* get__assignedControllersGroupLabel_52() const { return ____assignedControllersGroupLabel_52; }
	inline String_t** get_address_of__assignedControllersGroupLabel_52() { return &____assignedControllersGroupLabel_52; }
	inline void set__assignedControllersGroupLabel_52(String_t* value)
	{
		____assignedControllersGroupLabel_52 = value;
		Il2CppCodeGenWriteBarrier((&____assignedControllersGroupLabel_52), value);
	}

	inline static int32_t get_offset_of__settingsGroupLabel_53() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____settingsGroupLabel_53)); }
	inline String_t* get__settingsGroupLabel_53() const { return ____settingsGroupLabel_53; }
	inline String_t** get_address_of__settingsGroupLabel_53() { return &____settingsGroupLabel_53; }
	inline void set__settingsGroupLabel_53(String_t* value)
	{
		____settingsGroupLabel_53 = value;
		Il2CppCodeGenWriteBarrier((&____settingsGroupLabel_53), value);
	}

	inline static int32_t get_offset_of__mapCategoriesGroupLabel_54() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____mapCategoriesGroupLabel_54)); }
	inline String_t* get__mapCategoriesGroupLabel_54() const { return ____mapCategoriesGroupLabel_54; }
	inline String_t** get_address_of__mapCategoriesGroupLabel_54() { return &____mapCategoriesGroupLabel_54; }
	inline void set__mapCategoriesGroupLabel_54(String_t* value)
	{
		____mapCategoriesGroupLabel_54 = value;
		Il2CppCodeGenWriteBarrier((&____mapCategoriesGroupLabel_54), value);
	}

	inline static int32_t get_offset_of__calibrateWindow_deadZoneSliderLabel_55() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____calibrateWindow_deadZoneSliderLabel_55)); }
	inline String_t* get__calibrateWindow_deadZoneSliderLabel_55() const { return ____calibrateWindow_deadZoneSliderLabel_55; }
	inline String_t** get_address_of__calibrateWindow_deadZoneSliderLabel_55() { return &____calibrateWindow_deadZoneSliderLabel_55; }
	inline void set__calibrateWindow_deadZoneSliderLabel_55(String_t* value)
	{
		____calibrateWindow_deadZoneSliderLabel_55 = value;
		Il2CppCodeGenWriteBarrier((&____calibrateWindow_deadZoneSliderLabel_55), value);
	}

	inline static int32_t get_offset_of__calibrateWindow_zeroSliderLabel_56() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____calibrateWindow_zeroSliderLabel_56)); }
	inline String_t* get__calibrateWindow_zeroSliderLabel_56() const { return ____calibrateWindow_zeroSliderLabel_56; }
	inline String_t** get_address_of__calibrateWindow_zeroSliderLabel_56() { return &____calibrateWindow_zeroSliderLabel_56; }
	inline void set__calibrateWindow_zeroSliderLabel_56(String_t* value)
	{
		____calibrateWindow_zeroSliderLabel_56 = value;
		Il2CppCodeGenWriteBarrier((&____calibrateWindow_zeroSliderLabel_56), value);
	}

	inline static int32_t get_offset_of__calibrateWindow_sensitivitySliderLabel_57() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____calibrateWindow_sensitivitySliderLabel_57)); }
	inline String_t* get__calibrateWindow_sensitivitySliderLabel_57() const { return ____calibrateWindow_sensitivitySliderLabel_57; }
	inline String_t** get_address_of__calibrateWindow_sensitivitySliderLabel_57() { return &____calibrateWindow_sensitivitySliderLabel_57; }
	inline void set__calibrateWindow_sensitivitySliderLabel_57(String_t* value)
	{
		____calibrateWindow_sensitivitySliderLabel_57 = value;
		Il2CppCodeGenWriteBarrier((&____calibrateWindow_sensitivitySliderLabel_57), value);
	}

	inline static int32_t get_offset_of__calibrateWindow_invertToggleLabel_58() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____calibrateWindow_invertToggleLabel_58)); }
	inline String_t* get__calibrateWindow_invertToggleLabel_58() const { return ____calibrateWindow_invertToggleLabel_58; }
	inline String_t** get_address_of__calibrateWindow_invertToggleLabel_58() { return &____calibrateWindow_invertToggleLabel_58; }
	inline void set__calibrateWindow_invertToggleLabel_58(String_t* value)
	{
		____calibrateWindow_invertToggleLabel_58 = value;
		Il2CppCodeGenWriteBarrier((&____calibrateWindow_invertToggleLabel_58), value);
	}

	inline static int32_t get_offset_of__calibrateWindow_calibrateButtonLabel_59() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____calibrateWindow_calibrateButtonLabel_59)); }
	inline String_t* get__calibrateWindow_calibrateButtonLabel_59() const { return ____calibrateWindow_calibrateButtonLabel_59; }
	inline String_t** get_address_of__calibrateWindow_calibrateButtonLabel_59() { return &____calibrateWindow_calibrateButtonLabel_59; }
	inline void set__calibrateWindow_calibrateButtonLabel_59(String_t* value)
	{
		____calibrateWindow_calibrateButtonLabel_59 = value;
		Il2CppCodeGenWriteBarrier((&____calibrateWindow_calibrateButtonLabel_59), value);
	}

	inline static int32_t get_offset_of__customEntries_60() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____customEntries_60)); }
	inline CustomEntryU5BU5D_t40CEB5E7F2BCDECE988328E9E4875B2A8EE36E85* get__customEntries_60() const { return ____customEntries_60; }
	inline CustomEntryU5BU5D_t40CEB5E7F2BCDECE988328E9E4875B2A8EE36E85** get_address_of__customEntries_60() { return &____customEntries_60; }
	inline void set__customEntries_60(CustomEntryU5BU5D_t40CEB5E7F2BCDECE988328E9E4875B2A8EE36E85* value)
	{
		____customEntries_60 = value;
		Il2CppCodeGenWriteBarrier((&____customEntries_60), value);
	}

	inline static int32_t get_offset_of__initialized_61() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ____initialized_61)); }
	inline bool get__initialized_61() const { return ____initialized_61; }
	inline bool* get_address_of__initialized_61() { return &____initialized_61; }
	inline void set__initialized_61(bool value)
	{
		____initialized_61 = value;
	}

	inline static int32_t get_offset_of_customDict_62() { return static_cast<int32_t>(offsetof(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B, ___customDict_62)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_customDict_62() const { return ___customDict_62; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_customDict_62() { return &___customDict_62; }
	inline void set_customDict_62(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___customDict_62 = value;
		Il2CppCodeGenWriteBarrier((&___customDict_62), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEDATA_TAC36375FDC13175378678CDE03A64E3856634A4B_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef USERDATASTORE_TA04E79BDF387346606C6F4379B4FD0935BA176C1_H
#define USERDATASTORE_TA04E79BDF387346606C6F4379B4FD0935BA176C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserDataStore
struct  UserDataStore_tA04E79BDF387346606C6F4379B4FD0935BA176C1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDATASTORE_TA04E79BDF387346606C6F4379B4FD0935BA176C1_H
#ifndef INPUTMANAGER_BASE_T59B15491EE0FD742B61D3285150943B4A6D35701_H
#define INPUTMANAGER_BASE_T59B15491EE0FD742B61D3285150943B4A6D35701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputManager_Base
struct  InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Rewired.InputManager_Base::_dontDestroyOnLoad
	bool ____dontDestroyOnLoad_4;
	// Rewired.Data.UserData Rewired.InputManager_Base::_userData
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ____userData_5;
	// Rewired.Data.ControllerDataFiles Rewired.InputManager_Base::_controllerDataFiles
	ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5 * ____controllerDataFiles_6;
	// System.Boolean Rewired.InputManager_Base::isCompiling
	bool ___isCompiling_7;
	// System.Boolean Rewired.InputManager_Base::initialized
	bool ___initialized_8;
	// System.Boolean Rewired.InputManager_Base::criticalError
	bool ___criticalError_9;
	// Rewired.Platforms.EditorPlatform Rewired.InputManager_Base::editorPlatform
	int32_t ___editorPlatform_10;
	// Rewired.Platforms.Platform Rewired.InputManager_Base::platform
	int32_t ___platform_11;
	// Rewired.Platforms.WebplayerPlatform Rewired.InputManager_Base::webplayerPlatform
	int32_t ___webplayerPlatform_12;
	// System.Boolean Rewired.InputManager_Base::isEditor
	bool ___isEditor_13;
	// System.Boolean Rewired.InputManager_Base::_detectedPlatformInEditor
	bool ____detectedPlatformInEditor_14;
	// Rewired.Platforms.ScriptingBackend Rewired.InputManager_Base::scriptingBackend
	int32_t ___scriptingBackend_15;
	// Rewired.Platforms.ScriptingAPILevel Rewired.InputManager_Base::scriptingAPILevel
	int32_t ___scriptingAPILevel_16;
	// System.Boolean Rewired.InputManager_Base::_duplicateRIMError
	bool ____duplicateRIMError_17;
	// System.Boolean Rewired.InputManager_Base::_isAwake
	bool ____isAwake_18;

public:
	inline static int32_t get_offset_of__dontDestroyOnLoad_4() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ____dontDestroyOnLoad_4)); }
	inline bool get__dontDestroyOnLoad_4() const { return ____dontDestroyOnLoad_4; }
	inline bool* get_address_of__dontDestroyOnLoad_4() { return &____dontDestroyOnLoad_4; }
	inline void set__dontDestroyOnLoad_4(bool value)
	{
		____dontDestroyOnLoad_4 = value;
	}

	inline static int32_t get_offset_of__userData_5() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ____userData_5)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get__userData_5() const { return ____userData_5; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of__userData_5() { return &____userData_5; }
	inline void set__userData_5(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		____userData_5 = value;
		Il2CppCodeGenWriteBarrier((&____userData_5), value);
	}

	inline static int32_t get_offset_of__controllerDataFiles_6() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ____controllerDataFiles_6)); }
	inline ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5 * get__controllerDataFiles_6() const { return ____controllerDataFiles_6; }
	inline ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5 ** get_address_of__controllerDataFiles_6() { return &____controllerDataFiles_6; }
	inline void set__controllerDataFiles_6(ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5 * value)
	{
		____controllerDataFiles_6 = value;
		Il2CppCodeGenWriteBarrier((&____controllerDataFiles_6), value);
	}

	inline static int32_t get_offset_of_isCompiling_7() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___isCompiling_7)); }
	inline bool get_isCompiling_7() const { return ___isCompiling_7; }
	inline bool* get_address_of_isCompiling_7() { return &___isCompiling_7; }
	inline void set_isCompiling_7(bool value)
	{
		___isCompiling_7 = value;
	}

	inline static int32_t get_offset_of_initialized_8() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___initialized_8)); }
	inline bool get_initialized_8() const { return ___initialized_8; }
	inline bool* get_address_of_initialized_8() { return &___initialized_8; }
	inline void set_initialized_8(bool value)
	{
		___initialized_8 = value;
	}

	inline static int32_t get_offset_of_criticalError_9() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___criticalError_9)); }
	inline bool get_criticalError_9() const { return ___criticalError_9; }
	inline bool* get_address_of_criticalError_9() { return &___criticalError_9; }
	inline void set_criticalError_9(bool value)
	{
		___criticalError_9 = value;
	}

	inline static int32_t get_offset_of_editorPlatform_10() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___editorPlatform_10)); }
	inline int32_t get_editorPlatform_10() const { return ___editorPlatform_10; }
	inline int32_t* get_address_of_editorPlatform_10() { return &___editorPlatform_10; }
	inline void set_editorPlatform_10(int32_t value)
	{
		___editorPlatform_10 = value;
	}

	inline static int32_t get_offset_of_platform_11() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___platform_11)); }
	inline int32_t get_platform_11() const { return ___platform_11; }
	inline int32_t* get_address_of_platform_11() { return &___platform_11; }
	inline void set_platform_11(int32_t value)
	{
		___platform_11 = value;
	}

	inline static int32_t get_offset_of_webplayerPlatform_12() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___webplayerPlatform_12)); }
	inline int32_t get_webplayerPlatform_12() const { return ___webplayerPlatform_12; }
	inline int32_t* get_address_of_webplayerPlatform_12() { return &___webplayerPlatform_12; }
	inline void set_webplayerPlatform_12(int32_t value)
	{
		___webplayerPlatform_12 = value;
	}

	inline static int32_t get_offset_of_isEditor_13() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___isEditor_13)); }
	inline bool get_isEditor_13() const { return ___isEditor_13; }
	inline bool* get_address_of_isEditor_13() { return &___isEditor_13; }
	inline void set_isEditor_13(bool value)
	{
		___isEditor_13 = value;
	}

	inline static int32_t get_offset_of__detectedPlatformInEditor_14() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ____detectedPlatformInEditor_14)); }
	inline bool get__detectedPlatformInEditor_14() const { return ____detectedPlatformInEditor_14; }
	inline bool* get_address_of__detectedPlatformInEditor_14() { return &____detectedPlatformInEditor_14; }
	inline void set__detectedPlatformInEditor_14(bool value)
	{
		____detectedPlatformInEditor_14 = value;
	}

	inline static int32_t get_offset_of_scriptingBackend_15() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___scriptingBackend_15)); }
	inline int32_t get_scriptingBackend_15() const { return ___scriptingBackend_15; }
	inline int32_t* get_address_of_scriptingBackend_15() { return &___scriptingBackend_15; }
	inline void set_scriptingBackend_15(int32_t value)
	{
		___scriptingBackend_15 = value;
	}

	inline static int32_t get_offset_of_scriptingAPILevel_16() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ___scriptingAPILevel_16)); }
	inline int32_t get_scriptingAPILevel_16() const { return ___scriptingAPILevel_16; }
	inline int32_t* get_address_of_scriptingAPILevel_16() { return &___scriptingAPILevel_16; }
	inline void set_scriptingAPILevel_16(int32_t value)
	{
		___scriptingAPILevel_16 = value;
	}

	inline static int32_t get_offset_of__duplicateRIMError_17() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ____duplicateRIMError_17)); }
	inline bool get__duplicateRIMError_17() const { return ____duplicateRIMError_17; }
	inline bool* get_address_of__duplicateRIMError_17() { return &____duplicateRIMError_17; }
	inline void set__duplicateRIMError_17(bool value)
	{
		____duplicateRIMError_17 = value;
	}

	inline static int32_t get_offset_of__isAwake_18() { return static_cast<int32_t>(offsetof(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701, ____isAwake_18)); }
	inline bool get__isAwake_18() const { return ____isAwake_18; }
	inline bool* get_address_of__isAwake_18() { return &____isAwake_18; }
	inline void set__isAwake_18(bool value)
	{
		____isAwake_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMANAGER_BASE_T59B15491EE0FD742B61D3285150943B4A6D35701_H
#ifndef CANVASSCALERFITTER_TAEAB47D5FA2B24DE79CC881678ABE0E084C3B938_H
#define CANVASSCALERFITTER_TAEAB47D5FA2B24DE79CC881678ABE0E084C3B938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.CanvasScalerFitter
struct  CanvasScalerFitter_tAEAB47D5FA2B24DE79CC881678ABE0E084C3B938  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.UI.ControlMapper.CanvasScalerFitter_BreakPoint[] Rewired.UI.ControlMapper.CanvasScalerFitter::breakPoints
	BreakPointU5BU5D_t8672C763F902403BFE92AD4848B64288DD7E04FF* ___breakPoints_4;
	// Rewired.UI.ControlMapper.CanvasScalerExt Rewired.UI.ControlMapper.CanvasScalerFitter::canvasScaler
	CanvasScalerExt_t7474B4D2A98F6C8EB3964E748B7F093961CB7362 * ___canvasScaler_5;
	// System.Int32 Rewired.UI.ControlMapper.CanvasScalerFitter::screenWidth
	int32_t ___screenWidth_6;
	// System.Int32 Rewired.UI.ControlMapper.CanvasScalerFitter::screenHeight
	int32_t ___screenHeight_7;
	// System.Action Rewired.UI.ControlMapper.CanvasScalerFitter::ScreenSizeChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ScreenSizeChanged_8;

public:
	inline static int32_t get_offset_of_breakPoints_4() { return static_cast<int32_t>(offsetof(CanvasScalerFitter_tAEAB47D5FA2B24DE79CC881678ABE0E084C3B938, ___breakPoints_4)); }
	inline BreakPointU5BU5D_t8672C763F902403BFE92AD4848B64288DD7E04FF* get_breakPoints_4() const { return ___breakPoints_4; }
	inline BreakPointU5BU5D_t8672C763F902403BFE92AD4848B64288DD7E04FF** get_address_of_breakPoints_4() { return &___breakPoints_4; }
	inline void set_breakPoints_4(BreakPointU5BU5D_t8672C763F902403BFE92AD4848B64288DD7E04FF* value)
	{
		___breakPoints_4 = value;
		Il2CppCodeGenWriteBarrier((&___breakPoints_4), value);
	}

	inline static int32_t get_offset_of_canvasScaler_5() { return static_cast<int32_t>(offsetof(CanvasScalerFitter_tAEAB47D5FA2B24DE79CC881678ABE0E084C3B938, ___canvasScaler_5)); }
	inline CanvasScalerExt_t7474B4D2A98F6C8EB3964E748B7F093961CB7362 * get_canvasScaler_5() const { return ___canvasScaler_5; }
	inline CanvasScalerExt_t7474B4D2A98F6C8EB3964E748B7F093961CB7362 ** get_address_of_canvasScaler_5() { return &___canvasScaler_5; }
	inline void set_canvasScaler_5(CanvasScalerExt_t7474B4D2A98F6C8EB3964E748B7F093961CB7362 * value)
	{
		___canvasScaler_5 = value;
		Il2CppCodeGenWriteBarrier((&___canvasScaler_5), value);
	}

	inline static int32_t get_offset_of_screenWidth_6() { return static_cast<int32_t>(offsetof(CanvasScalerFitter_tAEAB47D5FA2B24DE79CC881678ABE0E084C3B938, ___screenWidth_6)); }
	inline int32_t get_screenWidth_6() const { return ___screenWidth_6; }
	inline int32_t* get_address_of_screenWidth_6() { return &___screenWidth_6; }
	inline void set_screenWidth_6(int32_t value)
	{
		___screenWidth_6 = value;
	}

	inline static int32_t get_offset_of_screenHeight_7() { return static_cast<int32_t>(offsetof(CanvasScalerFitter_tAEAB47D5FA2B24DE79CC881678ABE0E084C3B938, ___screenHeight_7)); }
	inline int32_t get_screenHeight_7() const { return ___screenHeight_7; }
	inline int32_t* get_address_of_screenHeight_7() { return &___screenHeight_7; }
	inline void set_screenHeight_7(int32_t value)
	{
		___screenHeight_7 = value;
	}

	inline static int32_t get_offset_of_ScreenSizeChanged_8() { return static_cast<int32_t>(offsetof(CanvasScalerFitter_tAEAB47D5FA2B24DE79CC881678ABE0E084C3B938, ___ScreenSizeChanged_8)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ScreenSizeChanged_8() const { return ___ScreenSizeChanged_8; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ScreenSizeChanged_8() { return &___ScreenSizeChanged_8; }
	inline void set_ScreenSizeChanged_8(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ScreenSizeChanged_8 = value;
		Il2CppCodeGenWriteBarrier((&___ScreenSizeChanged_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASSCALERFITTER_TAEAB47D5FA2B24DE79CC881678ABE0E084C3B938_H
#ifndef CONTROLMAPPER_T5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7_H
#define CONTROLMAPPER_T5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ControlMapper
struct  ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Rewired.InputManager Rewired.UI.ControlMapper.ControlMapper::_rewiredInputManager
	InputManager_t42964CC63A93EEFEC8C958ED5DF1166DB01B09CA * ____rewiredInputManager_14;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_dontDestroyOnLoad
	bool ____dontDestroyOnLoad_15;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_openOnStart
	bool ____openOnStart_16;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_keyboardMapDefaultLayout
	int32_t ____keyboardMapDefaultLayout_17;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_mouseMapDefaultLayout
	int32_t ____mouseMapDefaultLayout_18;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_joystickMapDefaultLayout
	int32_t ____joystickMapDefaultLayout_19;
	// Rewired.UI.ControlMapper.ControlMapper_MappingSet[] Rewired.UI.ControlMapper.ControlMapper::_mappingSets
	MappingSetU5BU5D_t4DD35740FE8DAD1318EA6164A74EF2DDCA5B810A* ____mappingSets_20;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showPlayers
	bool ____showPlayers_21;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showControllers
	bool ____showControllers_22;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showKeyboard
	bool ____showKeyboard_23;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showMouse
	bool ____showMouse_24;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_maxControllersPerPlayer
	int32_t ____maxControllersPerPlayer_25;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showActionCategoryLabels
	bool ____showActionCategoryLabels_26;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_keyboardInputFieldCount
	int32_t ____keyboardInputFieldCount_27;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_mouseInputFieldCount
	int32_t ____mouseInputFieldCount_28;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_controllerInputFieldCount
	int32_t ____controllerInputFieldCount_29;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showFullAxisInputFields
	bool ____showFullAxisInputFields_30;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showSplitAxisInputFields
	bool ____showSplitAxisInputFields_31;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_allowElementAssignmentConflicts
	bool ____allowElementAssignmentConflicts_32;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_allowElementAssignmentSwap
	bool ____allowElementAssignmentSwap_33;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_actionLabelWidth
	int32_t ____actionLabelWidth_34;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_keyboardColMaxWidth
	int32_t ____keyboardColMaxWidth_35;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_mouseColMaxWidth
	int32_t ____mouseColMaxWidth_36;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_controllerColMaxWidth
	int32_t ____controllerColMaxWidth_37;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_inputRowHeight
	int32_t ____inputRowHeight_38;
	// UnityEngine.RectOffset Rewired.UI.ControlMapper.ControlMapper::_inputRowPadding
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ____inputRowPadding_39;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_inputRowFieldSpacing
	int32_t ____inputRowFieldSpacing_40;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_inputColumnSpacing
	int32_t ____inputColumnSpacing_41;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_inputRowCategorySpacing
	int32_t ____inputRowCategorySpacing_42;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_invertToggleWidth
	int32_t ____invertToggleWidth_43;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_defaultWindowWidth
	int32_t ____defaultWindowWidth_44;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_defaultWindowHeight
	int32_t ____defaultWindowHeight_45;
	// System.Single Rewired.UI.ControlMapper.ControlMapper::_controllerAssignmentTimeout
	float ____controllerAssignmentTimeout_46;
	// System.Single Rewired.UI.ControlMapper.ControlMapper::_preInputAssignmentTimeout
	float ____preInputAssignmentTimeout_47;
	// System.Single Rewired.UI.ControlMapper.ControlMapper::_inputAssignmentTimeout
	float ____inputAssignmentTimeout_48;
	// System.Single Rewired.UI.ControlMapper.ControlMapper::_axisCalibrationTimeout
	float ____axisCalibrationTimeout_49;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_ignoreMouseXAxisAssignment
	bool ____ignoreMouseXAxisAssignment_50;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_ignoreMouseYAxisAssignment
	bool ____ignoreMouseYAxisAssignment_51;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_screenToggleAction
	int32_t ____screenToggleAction_52;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_screenOpenAction
	int32_t ____screenOpenAction_53;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_screenCloseAction
	int32_t ____screenCloseAction_54;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::_universalCancelAction
	int32_t ____universalCancelAction_55;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_universalCancelClosesScreen
	bool ____universalCancelClosesScreen_56;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showInputBehaviorSettings
	bool ____showInputBehaviorSettings_57;
	// Rewired.UI.ControlMapper.ControlMapper_InputBehaviorSettings[] Rewired.UI.ControlMapper.ControlMapper::_inputBehaviorSettings
	InputBehaviorSettingsU5BU5D_tA249DB027445C51288AA79DD04BAA298DF37C949* ____inputBehaviorSettings_58;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_useThemeSettings
	bool ____useThemeSettings_59;
	// Rewired.UI.ControlMapper.ThemeSettings Rewired.UI.ControlMapper.ControlMapper::_themeSettings
	ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF * ____themeSettings_60;
	// Rewired.UI.ControlMapper.LanguageData Rewired.UI.ControlMapper.ControlMapper::_language
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B * ____language_61;
	// Rewired.UI.ControlMapper.ControlMapper_Prefabs Rewired.UI.ControlMapper.ControlMapper::prefabs
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A * ___prefabs_62;
	// Rewired.UI.ControlMapper.ControlMapper_References Rewired.UI.ControlMapper.ControlMapper::references
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163 * ___references_63;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showPlayersGroupLabel
	bool ____showPlayersGroupLabel_64;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showControllerGroupLabel
	bool ____showControllerGroupLabel_65;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showAssignedControllersGroupLabel
	bool ____showAssignedControllersGroupLabel_66;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showSettingsGroupLabel
	bool ____showSettingsGroupLabel_67;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showMapCategoriesGroupLabel
	bool ____showMapCategoriesGroupLabel_68;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showControllerNameLabel
	bool ____showControllerNameLabel_69;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::_showAssignedControllers
	bool ____showAssignedControllers_70;
	// System.Action Rewired.UI.ControlMapper.ControlMapper::_ScreenClosedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____ScreenClosedEvent_71;
	// System.Action Rewired.UI.ControlMapper.ControlMapper::_ScreenOpenedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____ScreenOpenedEvent_72;
	// System.Action Rewired.UI.ControlMapper.ControlMapper::_PopupWindowOpenedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____PopupWindowOpenedEvent_73;
	// System.Action Rewired.UI.ControlMapper.ControlMapper::_PopupWindowClosedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____PopupWindowClosedEvent_74;
	// System.Action Rewired.UI.ControlMapper.ControlMapper::_InputPollingStartedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____InputPollingStartedEvent_75;
	// System.Action Rewired.UI.ControlMapper.ControlMapper::_InputPollingEndedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____InputPollingEndedEvent_76;
	// UnityEngine.Events.UnityEvent Rewired.UI.ControlMapper.ControlMapper::_onScreenClosed
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ____onScreenClosed_77;
	// UnityEngine.Events.UnityEvent Rewired.UI.ControlMapper.ControlMapper::_onScreenOpened
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ____onScreenOpened_78;
	// UnityEngine.Events.UnityEvent Rewired.UI.ControlMapper.ControlMapper::_onPopupWindowClosed
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ____onPopupWindowClosed_79;
	// UnityEngine.Events.UnityEvent Rewired.UI.ControlMapper.ControlMapper::_onPopupWindowOpened
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ____onPopupWindowOpened_80;
	// UnityEngine.Events.UnityEvent Rewired.UI.ControlMapper.ControlMapper::_onInputPollingStarted
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ____onInputPollingStarted_81;
	// UnityEngine.Events.UnityEvent Rewired.UI.ControlMapper.ControlMapper::_onInputPollingEnded
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ____onInputPollingEnded_82;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::initialized
	bool ___initialized_84;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::playerCount
	int32_t ___playerCount_85;
	// Rewired.UI.ControlMapper.ControlMapper_InputGrid Rewired.UI.ControlMapper.ControlMapper::inputGrid
	InputGrid_t279399655219F94B8033968DF46749593CF22E63 * ___inputGrid_86;
	// Rewired.UI.ControlMapper.ControlMapper_WindowManager Rewired.UI.ControlMapper.ControlMapper::windowManager
	WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4 * ___windowManager_87;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::currentPlayerId
	int32_t ___currentPlayerId_88;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::currentMapCategoryId
	int32_t ___currentMapCategoryId_89;
	// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.ControlMapper_GUIButton> Rewired.UI.ControlMapper.ControlMapper::playerButtons
	List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D * ___playerButtons_90;
	// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.ControlMapper_GUIButton> Rewired.UI.ControlMapper.ControlMapper::mapCategoryButtons
	List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D * ___mapCategoryButtons_91;
	// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.ControlMapper_GUIButton> Rewired.UI.ControlMapper.ControlMapper::assignedControllerButtons
	List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D * ___assignedControllerButtons_92;
	// Rewired.UI.ControlMapper.ControlMapper_GUIButton Rewired.UI.ControlMapper.ControlMapper::assignedControllerButtonsPlaceholder
	GUIButton_tF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C * ___assignedControllerButtonsPlaceholder_93;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Rewired.UI.ControlMapper.ControlMapper::miscInstantiatedObjects
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___miscInstantiatedObjects_94;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper::canvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_95;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.ControlMapper::lastUISelection
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lastUISelection_96;
	// System.Int32 Rewired.UI.ControlMapper.ControlMapper::currentJoystickId
	int32_t ___currentJoystickId_97;
	// System.Single Rewired.UI.ControlMapper.ControlMapper::blockInputOnFocusEndTime
	float ___blockInputOnFocusEndTime_98;
	// System.Boolean Rewired.UI.ControlMapper.ControlMapper::isPollingForInput
	bool ___isPollingForInput_99;
	// Rewired.UI.ControlMapper.ControlMapper_InputMapping Rewired.UI.ControlMapper.ControlMapper::pendingInputMapping
	InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A * ___pendingInputMapping_100;
	// Rewired.UI.ControlMapper.ControlMapper_AxisCalibrator Rewired.UI.ControlMapper.ControlMapper::pendingAxisCalibration
	AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237 * ___pendingAxisCalibration_101;
	// System.Action`1<Rewired.UI.ControlMapper.InputFieldInfo> Rewired.UI.ControlMapper.ControlMapper::inputFieldActivatedDelegate
	Action_1_t7738608E1471A66AED779B33F76214E0494E0B0D * ___inputFieldActivatedDelegate_102;
	// System.Action`2<Rewired.UI.ControlMapper.ToggleInfo,System.Boolean> Rewired.UI.ControlMapper.ControlMapper::inputFieldInvertToggleStateChangedDelegate
	Action_2_tCFB48941301BEC70B366F467446469E97B0B61C9 * ___inputFieldInvertToggleStateChangedDelegate_103;
	// System.Action Rewired.UI.ControlMapper.ControlMapper::_restoreDefaultsDelegate
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ____restoreDefaultsDelegate_104;

public:
	inline static int32_t get_offset_of__rewiredInputManager_14() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____rewiredInputManager_14)); }
	inline InputManager_t42964CC63A93EEFEC8C958ED5DF1166DB01B09CA * get__rewiredInputManager_14() const { return ____rewiredInputManager_14; }
	inline InputManager_t42964CC63A93EEFEC8C958ED5DF1166DB01B09CA ** get_address_of__rewiredInputManager_14() { return &____rewiredInputManager_14; }
	inline void set__rewiredInputManager_14(InputManager_t42964CC63A93EEFEC8C958ED5DF1166DB01B09CA * value)
	{
		____rewiredInputManager_14 = value;
		Il2CppCodeGenWriteBarrier((&____rewiredInputManager_14), value);
	}

	inline static int32_t get_offset_of__dontDestroyOnLoad_15() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____dontDestroyOnLoad_15)); }
	inline bool get__dontDestroyOnLoad_15() const { return ____dontDestroyOnLoad_15; }
	inline bool* get_address_of__dontDestroyOnLoad_15() { return &____dontDestroyOnLoad_15; }
	inline void set__dontDestroyOnLoad_15(bool value)
	{
		____dontDestroyOnLoad_15 = value;
	}

	inline static int32_t get_offset_of__openOnStart_16() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____openOnStart_16)); }
	inline bool get__openOnStart_16() const { return ____openOnStart_16; }
	inline bool* get_address_of__openOnStart_16() { return &____openOnStart_16; }
	inline void set__openOnStart_16(bool value)
	{
		____openOnStart_16 = value;
	}

	inline static int32_t get_offset_of__keyboardMapDefaultLayout_17() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____keyboardMapDefaultLayout_17)); }
	inline int32_t get__keyboardMapDefaultLayout_17() const { return ____keyboardMapDefaultLayout_17; }
	inline int32_t* get_address_of__keyboardMapDefaultLayout_17() { return &____keyboardMapDefaultLayout_17; }
	inline void set__keyboardMapDefaultLayout_17(int32_t value)
	{
		____keyboardMapDefaultLayout_17 = value;
	}

	inline static int32_t get_offset_of__mouseMapDefaultLayout_18() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____mouseMapDefaultLayout_18)); }
	inline int32_t get__mouseMapDefaultLayout_18() const { return ____mouseMapDefaultLayout_18; }
	inline int32_t* get_address_of__mouseMapDefaultLayout_18() { return &____mouseMapDefaultLayout_18; }
	inline void set__mouseMapDefaultLayout_18(int32_t value)
	{
		____mouseMapDefaultLayout_18 = value;
	}

	inline static int32_t get_offset_of__joystickMapDefaultLayout_19() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____joystickMapDefaultLayout_19)); }
	inline int32_t get__joystickMapDefaultLayout_19() const { return ____joystickMapDefaultLayout_19; }
	inline int32_t* get_address_of__joystickMapDefaultLayout_19() { return &____joystickMapDefaultLayout_19; }
	inline void set__joystickMapDefaultLayout_19(int32_t value)
	{
		____joystickMapDefaultLayout_19 = value;
	}

	inline static int32_t get_offset_of__mappingSets_20() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____mappingSets_20)); }
	inline MappingSetU5BU5D_t4DD35740FE8DAD1318EA6164A74EF2DDCA5B810A* get__mappingSets_20() const { return ____mappingSets_20; }
	inline MappingSetU5BU5D_t4DD35740FE8DAD1318EA6164A74EF2DDCA5B810A** get_address_of__mappingSets_20() { return &____mappingSets_20; }
	inline void set__mappingSets_20(MappingSetU5BU5D_t4DD35740FE8DAD1318EA6164A74EF2DDCA5B810A* value)
	{
		____mappingSets_20 = value;
		Il2CppCodeGenWriteBarrier((&____mappingSets_20), value);
	}

	inline static int32_t get_offset_of__showPlayers_21() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showPlayers_21)); }
	inline bool get__showPlayers_21() const { return ____showPlayers_21; }
	inline bool* get_address_of__showPlayers_21() { return &____showPlayers_21; }
	inline void set__showPlayers_21(bool value)
	{
		____showPlayers_21 = value;
	}

	inline static int32_t get_offset_of__showControllers_22() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showControllers_22)); }
	inline bool get__showControllers_22() const { return ____showControllers_22; }
	inline bool* get_address_of__showControllers_22() { return &____showControllers_22; }
	inline void set__showControllers_22(bool value)
	{
		____showControllers_22 = value;
	}

	inline static int32_t get_offset_of__showKeyboard_23() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showKeyboard_23)); }
	inline bool get__showKeyboard_23() const { return ____showKeyboard_23; }
	inline bool* get_address_of__showKeyboard_23() { return &____showKeyboard_23; }
	inline void set__showKeyboard_23(bool value)
	{
		____showKeyboard_23 = value;
	}

	inline static int32_t get_offset_of__showMouse_24() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showMouse_24)); }
	inline bool get__showMouse_24() const { return ____showMouse_24; }
	inline bool* get_address_of__showMouse_24() { return &____showMouse_24; }
	inline void set__showMouse_24(bool value)
	{
		____showMouse_24 = value;
	}

	inline static int32_t get_offset_of__maxControllersPerPlayer_25() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____maxControllersPerPlayer_25)); }
	inline int32_t get__maxControllersPerPlayer_25() const { return ____maxControllersPerPlayer_25; }
	inline int32_t* get_address_of__maxControllersPerPlayer_25() { return &____maxControllersPerPlayer_25; }
	inline void set__maxControllersPerPlayer_25(int32_t value)
	{
		____maxControllersPerPlayer_25 = value;
	}

	inline static int32_t get_offset_of__showActionCategoryLabels_26() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showActionCategoryLabels_26)); }
	inline bool get__showActionCategoryLabels_26() const { return ____showActionCategoryLabels_26; }
	inline bool* get_address_of__showActionCategoryLabels_26() { return &____showActionCategoryLabels_26; }
	inline void set__showActionCategoryLabels_26(bool value)
	{
		____showActionCategoryLabels_26 = value;
	}

	inline static int32_t get_offset_of__keyboardInputFieldCount_27() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____keyboardInputFieldCount_27)); }
	inline int32_t get__keyboardInputFieldCount_27() const { return ____keyboardInputFieldCount_27; }
	inline int32_t* get_address_of__keyboardInputFieldCount_27() { return &____keyboardInputFieldCount_27; }
	inline void set__keyboardInputFieldCount_27(int32_t value)
	{
		____keyboardInputFieldCount_27 = value;
	}

	inline static int32_t get_offset_of__mouseInputFieldCount_28() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____mouseInputFieldCount_28)); }
	inline int32_t get__mouseInputFieldCount_28() const { return ____mouseInputFieldCount_28; }
	inline int32_t* get_address_of__mouseInputFieldCount_28() { return &____mouseInputFieldCount_28; }
	inline void set__mouseInputFieldCount_28(int32_t value)
	{
		____mouseInputFieldCount_28 = value;
	}

	inline static int32_t get_offset_of__controllerInputFieldCount_29() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____controllerInputFieldCount_29)); }
	inline int32_t get__controllerInputFieldCount_29() const { return ____controllerInputFieldCount_29; }
	inline int32_t* get_address_of__controllerInputFieldCount_29() { return &____controllerInputFieldCount_29; }
	inline void set__controllerInputFieldCount_29(int32_t value)
	{
		____controllerInputFieldCount_29 = value;
	}

	inline static int32_t get_offset_of__showFullAxisInputFields_30() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showFullAxisInputFields_30)); }
	inline bool get__showFullAxisInputFields_30() const { return ____showFullAxisInputFields_30; }
	inline bool* get_address_of__showFullAxisInputFields_30() { return &____showFullAxisInputFields_30; }
	inline void set__showFullAxisInputFields_30(bool value)
	{
		____showFullAxisInputFields_30 = value;
	}

	inline static int32_t get_offset_of__showSplitAxisInputFields_31() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showSplitAxisInputFields_31)); }
	inline bool get__showSplitAxisInputFields_31() const { return ____showSplitAxisInputFields_31; }
	inline bool* get_address_of__showSplitAxisInputFields_31() { return &____showSplitAxisInputFields_31; }
	inline void set__showSplitAxisInputFields_31(bool value)
	{
		____showSplitAxisInputFields_31 = value;
	}

	inline static int32_t get_offset_of__allowElementAssignmentConflicts_32() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____allowElementAssignmentConflicts_32)); }
	inline bool get__allowElementAssignmentConflicts_32() const { return ____allowElementAssignmentConflicts_32; }
	inline bool* get_address_of__allowElementAssignmentConflicts_32() { return &____allowElementAssignmentConflicts_32; }
	inline void set__allowElementAssignmentConflicts_32(bool value)
	{
		____allowElementAssignmentConflicts_32 = value;
	}

	inline static int32_t get_offset_of__allowElementAssignmentSwap_33() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____allowElementAssignmentSwap_33)); }
	inline bool get__allowElementAssignmentSwap_33() const { return ____allowElementAssignmentSwap_33; }
	inline bool* get_address_of__allowElementAssignmentSwap_33() { return &____allowElementAssignmentSwap_33; }
	inline void set__allowElementAssignmentSwap_33(bool value)
	{
		____allowElementAssignmentSwap_33 = value;
	}

	inline static int32_t get_offset_of__actionLabelWidth_34() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____actionLabelWidth_34)); }
	inline int32_t get__actionLabelWidth_34() const { return ____actionLabelWidth_34; }
	inline int32_t* get_address_of__actionLabelWidth_34() { return &____actionLabelWidth_34; }
	inline void set__actionLabelWidth_34(int32_t value)
	{
		____actionLabelWidth_34 = value;
	}

	inline static int32_t get_offset_of__keyboardColMaxWidth_35() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____keyboardColMaxWidth_35)); }
	inline int32_t get__keyboardColMaxWidth_35() const { return ____keyboardColMaxWidth_35; }
	inline int32_t* get_address_of__keyboardColMaxWidth_35() { return &____keyboardColMaxWidth_35; }
	inline void set__keyboardColMaxWidth_35(int32_t value)
	{
		____keyboardColMaxWidth_35 = value;
	}

	inline static int32_t get_offset_of__mouseColMaxWidth_36() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____mouseColMaxWidth_36)); }
	inline int32_t get__mouseColMaxWidth_36() const { return ____mouseColMaxWidth_36; }
	inline int32_t* get_address_of__mouseColMaxWidth_36() { return &____mouseColMaxWidth_36; }
	inline void set__mouseColMaxWidth_36(int32_t value)
	{
		____mouseColMaxWidth_36 = value;
	}

	inline static int32_t get_offset_of__controllerColMaxWidth_37() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____controllerColMaxWidth_37)); }
	inline int32_t get__controllerColMaxWidth_37() const { return ____controllerColMaxWidth_37; }
	inline int32_t* get_address_of__controllerColMaxWidth_37() { return &____controllerColMaxWidth_37; }
	inline void set__controllerColMaxWidth_37(int32_t value)
	{
		____controllerColMaxWidth_37 = value;
	}

	inline static int32_t get_offset_of__inputRowHeight_38() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____inputRowHeight_38)); }
	inline int32_t get__inputRowHeight_38() const { return ____inputRowHeight_38; }
	inline int32_t* get_address_of__inputRowHeight_38() { return &____inputRowHeight_38; }
	inline void set__inputRowHeight_38(int32_t value)
	{
		____inputRowHeight_38 = value;
	}

	inline static int32_t get_offset_of__inputRowPadding_39() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____inputRowPadding_39)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get__inputRowPadding_39() const { return ____inputRowPadding_39; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of__inputRowPadding_39() { return &____inputRowPadding_39; }
	inline void set__inputRowPadding_39(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		____inputRowPadding_39 = value;
		Il2CppCodeGenWriteBarrier((&____inputRowPadding_39), value);
	}

	inline static int32_t get_offset_of__inputRowFieldSpacing_40() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____inputRowFieldSpacing_40)); }
	inline int32_t get__inputRowFieldSpacing_40() const { return ____inputRowFieldSpacing_40; }
	inline int32_t* get_address_of__inputRowFieldSpacing_40() { return &____inputRowFieldSpacing_40; }
	inline void set__inputRowFieldSpacing_40(int32_t value)
	{
		____inputRowFieldSpacing_40 = value;
	}

	inline static int32_t get_offset_of__inputColumnSpacing_41() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____inputColumnSpacing_41)); }
	inline int32_t get__inputColumnSpacing_41() const { return ____inputColumnSpacing_41; }
	inline int32_t* get_address_of__inputColumnSpacing_41() { return &____inputColumnSpacing_41; }
	inline void set__inputColumnSpacing_41(int32_t value)
	{
		____inputColumnSpacing_41 = value;
	}

	inline static int32_t get_offset_of__inputRowCategorySpacing_42() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____inputRowCategorySpacing_42)); }
	inline int32_t get__inputRowCategorySpacing_42() const { return ____inputRowCategorySpacing_42; }
	inline int32_t* get_address_of__inputRowCategorySpacing_42() { return &____inputRowCategorySpacing_42; }
	inline void set__inputRowCategorySpacing_42(int32_t value)
	{
		____inputRowCategorySpacing_42 = value;
	}

	inline static int32_t get_offset_of__invertToggleWidth_43() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____invertToggleWidth_43)); }
	inline int32_t get__invertToggleWidth_43() const { return ____invertToggleWidth_43; }
	inline int32_t* get_address_of__invertToggleWidth_43() { return &____invertToggleWidth_43; }
	inline void set__invertToggleWidth_43(int32_t value)
	{
		____invertToggleWidth_43 = value;
	}

	inline static int32_t get_offset_of__defaultWindowWidth_44() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____defaultWindowWidth_44)); }
	inline int32_t get__defaultWindowWidth_44() const { return ____defaultWindowWidth_44; }
	inline int32_t* get_address_of__defaultWindowWidth_44() { return &____defaultWindowWidth_44; }
	inline void set__defaultWindowWidth_44(int32_t value)
	{
		____defaultWindowWidth_44 = value;
	}

	inline static int32_t get_offset_of__defaultWindowHeight_45() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____defaultWindowHeight_45)); }
	inline int32_t get__defaultWindowHeight_45() const { return ____defaultWindowHeight_45; }
	inline int32_t* get_address_of__defaultWindowHeight_45() { return &____defaultWindowHeight_45; }
	inline void set__defaultWindowHeight_45(int32_t value)
	{
		____defaultWindowHeight_45 = value;
	}

	inline static int32_t get_offset_of__controllerAssignmentTimeout_46() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____controllerAssignmentTimeout_46)); }
	inline float get__controllerAssignmentTimeout_46() const { return ____controllerAssignmentTimeout_46; }
	inline float* get_address_of__controllerAssignmentTimeout_46() { return &____controllerAssignmentTimeout_46; }
	inline void set__controllerAssignmentTimeout_46(float value)
	{
		____controllerAssignmentTimeout_46 = value;
	}

	inline static int32_t get_offset_of__preInputAssignmentTimeout_47() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____preInputAssignmentTimeout_47)); }
	inline float get__preInputAssignmentTimeout_47() const { return ____preInputAssignmentTimeout_47; }
	inline float* get_address_of__preInputAssignmentTimeout_47() { return &____preInputAssignmentTimeout_47; }
	inline void set__preInputAssignmentTimeout_47(float value)
	{
		____preInputAssignmentTimeout_47 = value;
	}

	inline static int32_t get_offset_of__inputAssignmentTimeout_48() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____inputAssignmentTimeout_48)); }
	inline float get__inputAssignmentTimeout_48() const { return ____inputAssignmentTimeout_48; }
	inline float* get_address_of__inputAssignmentTimeout_48() { return &____inputAssignmentTimeout_48; }
	inline void set__inputAssignmentTimeout_48(float value)
	{
		____inputAssignmentTimeout_48 = value;
	}

	inline static int32_t get_offset_of__axisCalibrationTimeout_49() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____axisCalibrationTimeout_49)); }
	inline float get__axisCalibrationTimeout_49() const { return ____axisCalibrationTimeout_49; }
	inline float* get_address_of__axisCalibrationTimeout_49() { return &____axisCalibrationTimeout_49; }
	inline void set__axisCalibrationTimeout_49(float value)
	{
		____axisCalibrationTimeout_49 = value;
	}

	inline static int32_t get_offset_of__ignoreMouseXAxisAssignment_50() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____ignoreMouseXAxisAssignment_50)); }
	inline bool get__ignoreMouseXAxisAssignment_50() const { return ____ignoreMouseXAxisAssignment_50; }
	inline bool* get_address_of__ignoreMouseXAxisAssignment_50() { return &____ignoreMouseXAxisAssignment_50; }
	inline void set__ignoreMouseXAxisAssignment_50(bool value)
	{
		____ignoreMouseXAxisAssignment_50 = value;
	}

	inline static int32_t get_offset_of__ignoreMouseYAxisAssignment_51() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____ignoreMouseYAxisAssignment_51)); }
	inline bool get__ignoreMouseYAxisAssignment_51() const { return ____ignoreMouseYAxisAssignment_51; }
	inline bool* get_address_of__ignoreMouseYAxisAssignment_51() { return &____ignoreMouseYAxisAssignment_51; }
	inline void set__ignoreMouseYAxisAssignment_51(bool value)
	{
		____ignoreMouseYAxisAssignment_51 = value;
	}

	inline static int32_t get_offset_of__screenToggleAction_52() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____screenToggleAction_52)); }
	inline int32_t get__screenToggleAction_52() const { return ____screenToggleAction_52; }
	inline int32_t* get_address_of__screenToggleAction_52() { return &____screenToggleAction_52; }
	inline void set__screenToggleAction_52(int32_t value)
	{
		____screenToggleAction_52 = value;
	}

	inline static int32_t get_offset_of__screenOpenAction_53() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____screenOpenAction_53)); }
	inline int32_t get__screenOpenAction_53() const { return ____screenOpenAction_53; }
	inline int32_t* get_address_of__screenOpenAction_53() { return &____screenOpenAction_53; }
	inline void set__screenOpenAction_53(int32_t value)
	{
		____screenOpenAction_53 = value;
	}

	inline static int32_t get_offset_of__screenCloseAction_54() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____screenCloseAction_54)); }
	inline int32_t get__screenCloseAction_54() const { return ____screenCloseAction_54; }
	inline int32_t* get_address_of__screenCloseAction_54() { return &____screenCloseAction_54; }
	inline void set__screenCloseAction_54(int32_t value)
	{
		____screenCloseAction_54 = value;
	}

	inline static int32_t get_offset_of__universalCancelAction_55() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____universalCancelAction_55)); }
	inline int32_t get__universalCancelAction_55() const { return ____universalCancelAction_55; }
	inline int32_t* get_address_of__universalCancelAction_55() { return &____universalCancelAction_55; }
	inline void set__universalCancelAction_55(int32_t value)
	{
		____universalCancelAction_55 = value;
	}

	inline static int32_t get_offset_of__universalCancelClosesScreen_56() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____universalCancelClosesScreen_56)); }
	inline bool get__universalCancelClosesScreen_56() const { return ____universalCancelClosesScreen_56; }
	inline bool* get_address_of__universalCancelClosesScreen_56() { return &____universalCancelClosesScreen_56; }
	inline void set__universalCancelClosesScreen_56(bool value)
	{
		____universalCancelClosesScreen_56 = value;
	}

	inline static int32_t get_offset_of__showInputBehaviorSettings_57() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showInputBehaviorSettings_57)); }
	inline bool get__showInputBehaviorSettings_57() const { return ____showInputBehaviorSettings_57; }
	inline bool* get_address_of__showInputBehaviorSettings_57() { return &____showInputBehaviorSettings_57; }
	inline void set__showInputBehaviorSettings_57(bool value)
	{
		____showInputBehaviorSettings_57 = value;
	}

	inline static int32_t get_offset_of__inputBehaviorSettings_58() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____inputBehaviorSettings_58)); }
	inline InputBehaviorSettingsU5BU5D_tA249DB027445C51288AA79DD04BAA298DF37C949* get__inputBehaviorSettings_58() const { return ____inputBehaviorSettings_58; }
	inline InputBehaviorSettingsU5BU5D_tA249DB027445C51288AA79DD04BAA298DF37C949** get_address_of__inputBehaviorSettings_58() { return &____inputBehaviorSettings_58; }
	inline void set__inputBehaviorSettings_58(InputBehaviorSettingsU5BU5D_tA249DB027445C51288AA79DD04BAA298DF37C949* value)
	{
		____inputBehaviorSettings_58 = value;
		Il2CppCodeGenWriteBarrier((&____inputBehaviorSettings_58), value);
	}

	inline static int32_t get_offset_of__useThemeSettings_59() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____useThemeSettings_59)); }
	inline bool get__useThemeSettings_59() const { return ____useThemeSettings_59; }
	inline bool* get_address_of__useThemeSettings_59() { return &____useThemeSettings_59; }
	inline void set__useThemeSettings_59(bool value)
	{
		____useThemeSettings_59 = value;
	}

	inline static int32_t get_offset_of__themeSettings_60() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____themeSettings_60)); }
	inline ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF * get__themeSettings_60() const { return ____themeSettings_60; }
	inline ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF ** get_address_of__themeSettings_60() { return &____themeSettings_60; }
	inline void set__themeSettings_60(ThemeSettings_tA2883FBE792D84E1948703D519AFDDA052B97BEF * value)
	{
		____themeSettings_60 = value;
		Il2CppCodeGenWriteBarrier((&____themeSettings_60), value);
	}

	inline static int32_t get_offset_of__language_61() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____language_61)); }
	inline LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B * get__language_61() const { return ____language_61; }
	inline LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B ** get_address_of__language_61() { return &____language_61; }
	inline void set__language_61(LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B * value)
	{
		____language_61 = value;
		Il2CppCodeGenWriteBarrier((&____language_61), value);
	}

	inline static int32_t get_offset_of_prefabs_62() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___prefabs_62)); }
	inline Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A * get_prefabs_62() const { return ___prefabs_62; }
	inline Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A ** get_address_of_prefabs_62() { return &___prefabs_62; }
	inline void set_prefabs_62(Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A * value)
	{
		___prefabs_62 = value;
		Il2CppCodeGenWriteBarrier((&___prefabs_62), value);
	}

	inline static int32_t get_offset_of_references_63() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___references_63)); }
	inline References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163 * get_references_63() const { return ___references_63; }
	inline References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163 ** get_address_of_references_63() { return &___references_63; }
	inline void set_references_63(References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163 * value)
	{
		___references_63 = value;
		Il2CppCodeGenWriteBarrier((&___references_63), value);
	}

	inline static int32_t get_offset_of__showPlayersGroupLabel_64() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showPlayersGroupLabel_64)); }
	inline bool get__showPlayersGroupLabel_64() const { return ____showPlayersGroupLabel_64; }
	inline bool* get_address_of__showPlayersGroupLabel_64() { return &____showPlayersGroupLabel_64; }
	inline void set__showPlayersGroupLabel_64(bool value)
	{
		____showPlayersGroupLabel_64 = value;
	}

	inline static int32_t get_offset_of__showControllerGroupLabel_65() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showControllerGroupLabel_65)); }
	inline bool get__showControllerGroupLabel_65() const { return ____showControllerGroupLabel_65; }
	inline bool* get_address_of__showControllerGroupLabel_65() { return &____showControllerGroupLabel_65; }
	inline void set__showControllerGroupLabel_65(bool value)
	{
		____showControllerGroupLabel_65 = value;
	}

	inline static int32_t get_offset_of__showAssignedControllersGroupLabel_66() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showAssignedControllersGroupLabel_66)); }
	inline bool get__showAssignedControllersGroupLabel_66() const { return ____showAssignedControllersGroupLabel_66; }
	inline bool* get_address_of__showAssignedControllersGroupLabel_66() { return &____showAssignedControllersGroupLabel_66; }
	inline void set__showAssignedControllersGroupLabel_66(bool value)
	{
		____showAssignedControllersGroupLabel_66 = value;
	}

	inline static int32_t get_offset_of__showSettingsGroupLabel_67() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showSettingsGroupLabel_67)); }
	inline bool get__showSettingsGroupLabel_67() const { return ____showSettingsGroupLabel_67; }
	inline bool* get_address_of__showSettingsGroupLabel_67() { return &____showSettingsGroupLabel_67; }
	inline void set__showSettingsGroupLabel_67(bool value)
	{
		____showSettingsGroupLabel_67 = value;
	}

	inline static int32_t get_offset_of__showMapCategoriesGroupLabel_68() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showMapCategoriesGroupLabel_68)); }
	inline bool get__showMapCategoriesGroupLabel_68() const { return ____showMapCategoriesGroupLabel_68; }
	inline bool* get_address_of__showMapCategoriesGroupLabel_68() { return &____showMapCategoriesGroupLabel_68; }
	inline void set__showMapCategoriesGroupLabel_68(bool value)
	{
		____showMapCategoriesGroupLabel_68 = value;
	}

	inline static int32_t get_offset_of__showControllerNameLabel_69() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showControllerNameLabel_69)); }
	inline bool get__showControllerNameLabel_69() const { return ____showControllerNameLabel_69; }
	inline bool* get_address_of__showControllerNameLabel_69() { return &____showControllerNameLabel_69; }
	inline void set__showControllerNameLabel_69(bool value)
	{
		____showControllerNameLabel_69 = value;
	}

	inline static int32_t get_offset_of__showAssignedControllers_70() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____showAssignedControllers_70)); }
	inline bool get__showAssignedControllers_70() const { return ____showAssignedControllers_70; }
	inline bool* get_address_of__showAssignedControllers_70() { return &____showAssignedControllers_70; }
	inline void set__showAssignedControllers_70(bool value)
	{
		____showAssignedControllers_70 = value;
	}

	inline static int32_t get_offset_of__ScreenClosedEvent_71() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____ScreenClosedEvent_71)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__ScreenClosedEvent_71() const { return ____ScreenClosedEvent_71; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__ScreenClosedEvent_71() { return &____ScreenClosedEvent_71; }
	inline void set__ScreenClosedEvent_71(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____ScreenClosedEvent_71 = value;
		Il2CppCodeGenWriteBarrier((&____ScreenClosedEvent_71), value);
	}

	inline static int32_t get_offset_of__ScreenOpenedEvent_72() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____ScreenOpenedEvent_72)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__ScreenOpenedEvent_72() const { return ____ScreenOpenedEvent_72; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__ScreenOpenedEvent_72() { return &____ScreenOpenedEvent_72; }
	inline void set__ScreenOpenedEvent_72(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____ScreenOpenedEvent_72 = value;
		Il2CppCodeGenWriteBarrier((&____ScreenOpenedEvent_72), value);
	}

	inline static int32_t get_offset_of__PopupWindowOpenedEvent_73() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____PopupWindowOpenedEvent_73)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__PopupWindowOpenedEvent_73() const { return ____PopupWindowOpenedEvent_73; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__PopupWindowOpenedEvent_73() { return &____PopupWindowOpenedEvent_73; }
	inline void set__PopupWindowOpenedEvent_73(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____PopupWindowOpenedEvent_73 = value;
		Il2CppCodeGenWriteBarrier((&____PopupWindowOpenedEvent_73), value);
	}

	inline static int32_t get_offset_of__PopupWindowClosedEvent_74() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____PopupWindowClosedEvent_74)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__PopupWindowClosedEvent_74() const { return ____PopupWindowClosedEvent_74; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__PopupWindowClosedEvent_74() { return &____PopupWindowClosedEvent_74; }
	inline void set__PopupWindowClosedEvent_74(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____PopupWindowClosedEvent_74 = value;
		Il2CppCodeGenWriteBarrier((&____PopupWindowClosedEvent_74), value);
	}

	inline static int32_t get_offset_of__InputPollingStartedEvent_75() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____InputPollingStartedEvent_75)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__InputPollingStartedEvent_75() const { return ____InputPollingStartedEvent_75; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__InputPollingStartedEvent_75() { return &____InputPollingStartedEvent_75; }
	inline void set__InputPollingStartedEvent_75(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____InputPollingStartedEvent_75 = value;
		Il2CppCodeGenWriteBarrier((&____InputPollingStartedEvent_75), value);
	}

	inline static int32_t get_offset_of__InputPollingEndedEvent_76() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____InputPollingEndedEvent_76)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__InputPollingEndedEvent_76() const { return ____InputPollingEndedEvent_76; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__InputPollingEndedEvent_76() { return &____InputPollingEndedEvent_76; }
	inline void set__InputPollingEndedEvent_76(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____InputPollingEndedEvent_76 = value;
		Il2CppCodeGenWriteBarrier((&____InputPollingEndedEvent_76), value);
	}

	inline static int32_t get_offset_of__onScreenClosed_77() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____onScreenClosed_77)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get__onScreenClosed_77() const { return ____onScreenClosed_77; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of__onScreenClosed_77() { return &____onScreenClosed_77; }
	inline void set__onScreenClosed_77(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		____onScreenClosed_77 = value;
		Il2CppCodeGenWriteBarrier((&____onScreenClosed_77), value);
	}

	inline static int32_t get_offset_of__onScreenOpened_78() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____onScreenOpened_78)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get__onScreenOpened_78() const { return ____onScreenOpened_78; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of__onScreenOpened_78() { return &____onScreenOpened_78; }
	inline void set__onScreenOpened_78(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		____onScreenOpened_78 = value;
		Il2CppCodeGenWriteBarrier((&____onScreenOpened_78), value);
	}

	inline static int32_t get_offset_of__onPopupWindowClosed_79() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____onPopupWindowClosed_79)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get__onPopupWindowClosed_79() const { return ____onPopupWindowClosed_79; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of__onPopupWindowClosed_79() { return &____onPopupWindowClosed_79; }
	inline void set__onPopupWindowClosed_79(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		____onPopupWindowClosed_79 = value;
		Il2CppCodeGenWriteBarrier((&____onPopupWindowClosed_79), value);
	}

	inline static int32_t get_offset_of__onPopupWindowOpened_80() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____onPopupWindowOpened_80)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get__onPopupWindowOpened_80() const { return ____onPopupWindowOpened_80; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of__onPopupWindowOpened_80() { return &____onPopupWindowOpened_80; }
	inline void set__onPopupWindowOpened_80(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		____onPopupWindowOpened_80 = value;
		Il2CppCodeGenWriteBarrier((&____onPopupWindowOpened_80), value);
	}

	inline static int32_t get_offset_of__onInputPollingStarted_81() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____onInputPollingStarted_81)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get__onInputPollingStarted_81() const { return ____onInputPollingStarted_81; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of__onInputPollingStarted_81() { return &____onInputPollingStarted_81; }
	inline void set__onInputPollingStarted_81(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		____onInputPollingStarted_81 = value;
		Il2CppCodeGenWriteBarrier((&____onInputPollingStarted_81), value);
	}

	inline static int32_t get_offset_of__onInputPollingEnded_82() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____onInputPollingEnded_82)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get__onInputPollingEnded_82() const { return ____onInputPollingEnded_82; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of__onInputPollingEnded_82() { return &____onInputPollingEnded_82; }
	inline void set__onInputPollingEnded_82(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		____onInputPollingEnded_82 = value;
		Il2CppCodeGenWriteBarrier((&____onInputPollingEnded_82), value);
	}

	inline static int32_t get_offset_of_initialized_84() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___initialized_84)); }
	inline bool get_initialized_84() const { return ___initialized_84; }
	inline bool* get_address_of_initialized_84() { return &___initialized_84; }
	inline void set_initialized_84(bool value)
	{
		___initialized_84 = value;
	}

	inline static int32_t get_offset_of_playerCount_85() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___playerCount_85)); }
	inline int32_t get_playerCount_85() const { return ___playerCount_85; }
	inline int32_t* get_address_of_playerCount_85() { return &___playerCount_85; }
	inline void set_playerCount_85(int32_t value)
	{
		___playerCount_85 = value;
	}

	inline static int32_t get_offset_of_inputGrid_86() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___inputGrid_86)); }
	inline InputGrid_t279399655219F94B8033968DF46749593CF22E63 * get_inputGrid_86() const { return ___inputGrid_86; }
	inline InputGrid_t279399655219F94B8033968DF46749593CF22E63 ** get_address_of_inputGrid_86() { return &___inputGrid_86; }
	inline void set_inputGrid_86(InputGrid_t279399655219F94B8033968DF46749593CF22E63 * value)
	{
		___inputGrid_86 = value;
		Il2CppCodeGenWriteBarrier((&___inputGrid_86), value);
	}

	inline static int32_t get_offset_of_windowManager_87() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___windowManager_87)); }
	inline WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4 * get_windowManager_87() const { return ___windowManager_87; }
	inline WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4 ** get_address_of_windowManager_87() { return &___windowManager_87; }
	inline void set_windowManager_87(WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4 * value)
	{
		___windowManager_87 = value;
		Il2CppCodeGenWriteBarrier((&___windowManager_87), value);
	}

	inline static int32_t get_offset_of_currentPlayerId_88() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___currentPlayerId_88)); }
	inline int32_t get_currentPlayerId_88() const { return ___currentPlayerId_88; }
	inline int32_t* get_address_of_currentPlayerId_88() { return &___currentPlayerId_88; }
	inline void set_currentPlayerId_88(int32_t value)
	{
		___currentPlayerId_88 = value;
	}

	inline static int32_t get_offset_of_currentMapCategoryId_89() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___currentMapCategoryId_89)); }
	inline int32_t get_currentMapCategoryId_89() const { return ___currentMapCategoryId_89; }
	inline int32_t* get_address_of_currentMapCategoryId_89() { return &___currentMapCategoryId_89; }
	inline void set_currentMapCategoryId_89(int32_t value)
	{
		___currentMapCategoryId_89 = value;
	}

	inline static int32_t get_offset_of_playerButtons_90() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___playerButtons_90)); }
	inline List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D * get_playerButtons_90() const { return ___playerButtons_90; }
	inline List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D ** get_address_of_playerButtons_90() { return &___playerButtons_90; }
	inline void set_playerButtons_90(List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D * value)
	{
		___playerButtons_90 = value;
		Il2CppCodeGenWriteBarrier((&___playerButtons_90), value);
	}

	inline static int32_t get_offset_of_mapCategoryButtons_91() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___mapCategoryButtons_91)); }
	inline List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D * get_mapCategoryButtons_91() const { return ___mapCategoryButtons_91; }
	inline List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D ** get_address_of_mapCategoryButtons_91() { return &___mapCategoryButtons_91; }
	inline void set_mapCategoryButtons_91(List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D * value)
	{
		___mapCategoryButtons_91 = value;
		Il2CppCodeGenWriteBarrier((&___mapCategoryButtons_91), value);
	}

	inline static int32_t get_offset_of_assignedControllerButtons_92() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___assignedControllerButtons_92)); }
	inline List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D * get_assignedControllerButtons_92() const { return ___assignedControllerButtons_92; }
	inline List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D ** get_address_of_assignedControllerButtons_92() { return &___assignedControllerButtons_92; }
	inline void set_assignedControllerButtons_92(List_1_t21471C69FA0F4B2F571CD4F8DB6D6057AA2F102D * value)
	{
		___assignedControllerButtons_92 = value;
		Il2CppCodeGenWriteBarrier((&___assignedControllerButtons_92), value);
	}

	inline static int32_t get_offset_of_assignedControllerButtonsPlaceholder_93() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___assignedControllerButtonsPlaceholder_93)); }
	inline GUIButton_tF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C * get_assignedControllerButtonsPlaceholder_93() const { return ___assignedControllerButtonsPlaceholder_93; }
	inline GUIButton_tF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C ** get_address_of_assignedControllerButtonsPlaceholder_93() { return &___assignedControllerButtonsPlaceholder_93; }
	inline void set_assignedControllerButtonsPlaceholder_93(GUIButton_tF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C * value)
	{
		___assignedControllerButtonsPlaceholder_93 = value;
		Il2CppCodeGenWriteBarrier((&___assignedControllerButtonsPlaceholder_93), value);
	}

	inline static int32_t get_offset_of_miscInstantiatedObjects_94() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___miscInstantiatedObjects_94)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_miscInstantiatedObjects_94() const { return ___miscInstantiatedObjects_94; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_miscInstantiatedObjects_94() { return &___miscInstantiatedObjects_94; }
	inline void set_miscInstantiatedObjects_94(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___miscInstantiatedObjects_94 = value;
		Il2CppCodeGenWriteBarrier((&___miscInstantiatedObjects_94), value);
	}

	inline static int32_t get_offset_of_canvas_95() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___canvas_95)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_95() const { return ___canvas_95; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_95() { return &___canvas_95; }
	inline void set_canvas_95(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_95 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_95), value);
	}

	inline static int32_t get_offset_of_lastUISelection_96() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___lastUISelection_96)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_lastUISelection_96() const { return ___lastUISelection_96; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_lastUISelection_96() { return &___lastUISelection_96; }
	inline void set_lastUISelection_96(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___lastUISelection_96 = value;
		Il2CppCodeGenWriteBarrier((&___lastUISelection_96), value);
	}

	inline static int32_t get_offset_of_currentJoystickId_97() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___currentJoystickId_97)); }
	inline int32_t get_currentJoystickId_97() const { return ___currentJoystickId_97; }
	inline int32_t* get_address_of_currentJoystickId_97() { return &___currentJoystickId_97; }
	inline void set_currentJoystickId_97(int32_t value)
	{
		___currentJoystickId_97 = value;
	}

	inline static int32_t get_offset_of_blockInputOnFocusEndTime_98() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___blockInputOnFocusEndTime_98)); }
	inline float get_blockInputOnFocusEndTime_98() const { return ___blockInputOnFocusEndTime_98; }
	inline float* get_address_of_blockInputOnFocusEndTime_98() { return &___blockInputOnFocusEndTime_98; }
	inline void set_blockInputOnFocusEndTime_98(float value)
	{
		___blockInputOnFocusEndTime_98 = value;
	}

	inline static int32_t get_offset_of_isPollingForInput_99() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___isPollingForInput_99)); }
	inline bool get_isPollingForInput_99() const { return ___isPollingForInput_99; }
	inline bool* get_address_of_isPollingForInput_99() { return &___isPollingForInput_99; }
	inline void set_isPollingForInput_99(bool value)
	{
		___isPollingForInput_99 = value;
	}

	inline static int32_t get_offset_of_pendingInputMapping_100() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___pendingInputMapping_100)); }
	inline InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A * get_pendingInputMapping_100() const { return ___pendingInputMapping_100; }
	inline InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A ** get_address_of_pendingInputMapping_100() { return &___pendingInputMapping_100; }
	inline void set_pendingInputMapping_100(InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A * value)
	{
		___pendingInputMapping_100 = value;
		Il2CppCodeGenWriteBarrier((&___pendingInputMapping_100), value);
	}

	inline static int32_t get_offset_of_pendingAxisCalibration_101() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___pendingAxisCalibration_101)); }
	inline AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237 * get_pendingAxisCalibration_101() const { return ___pendingAxisCalibration_101; }
	inline AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237 ** get_address_of_pendingAxisCalibration_101() { return &___pendingAxisCalibration_101; }
	inline void set_pendingAxisCalibration_101(AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237 * value)
	{
		___pendingAxisCalibration_101 = value;
		Il2CppCodeGenWriteBarrier((&___pendingAxisCalibration_101), value);
	}

	inline static int32_t get_offset_of_inputFieldActivatedDelegate_102() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___inputFieldActivatedDelegate_102)); }
	inline Action_1_t7738608E1471A66AED779B33F76214E0494E0B0D * get_inputFieldActivatedDelegate_102() const { return ___inputFieldActivatedDelegate_102; }
	inline Action_1_t7738608E1471A66AED779B33F76214E0494E0B0D ** get_address_of_inputFieldActivatedDelegate_102() { return &___inputFieldActivatedDelegate_102; }
	inline void set_inputFieldActivatedDelegate_102(Action_1_t7738608E1471A66AED779B33F76214E0494E0B0D * value)
	{
		___inputFieldActivatedDelegate_102 = value;
		Il2CppCodeGenWriteBarrier((&___inputFieldActivatedDelegate_102), value);
	}

	inline static int32_t get_offset_of_inputFieldInvertToggleStateChangedDelegate_103() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ___inputFieldInvertToggleStateChangedDelegate_103)); }
	inline Action_2_tCFB48941301BEC70B366F467446469E97B0B61C9 * get_inputFieldInvertToggleStateChangedDelegate_103() const { return ___inputFieldInvertToggleStateChangedDelegate_103; }
	inline Action_2_tCFB48941301BEC70B366F467446469E97B0B61C9 ** get_address_of_inputFieldInvertToggleStateChangedDelegate_103() { return &___inputFieldInvertToggleStateChangedDelegate_103; }
	inline void set_inputFieldInvertToggleStateChangedDelegate_103(Action_2_tCFB48941301BEC70B366F467446469E97B0B61C9 * value)
	{
		___inputFieldInvertToggleStateChangedDelegate_103 = value;
		Il2CppCodeGenWriteBarrier((&___inputFieldInvertToggleStateChangedDelegate_103), value);
	}

	inline static int32_t get_offset_of__restoreDefaultsDelegate_104() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7, ____restoreDefaultsDelegate_104)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get__restoreDefaultsDelegate_104() const { return ____restoreDefaultsDelegate_104; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of__restoreDefaultsDelegate_104() { return &____restoreDefaultsDelegate_104; }
	inline void set__restoreDefaultsDelegate_104(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		____restoreDefaultsDelegate_104 = value;
		Il2CppCodeGenWriteBarrier((&____restoreDefaultsDelegate_104), value);
	}
};

struct ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7_StaticFields
{
public:
	// Rewired.UI.ControlMapper.ControlMapper Rewired.UI.ControlMapper.ControlMapper::Instance
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * ___Instance_83;

public:
	inline static int32_t get_offset_of_Instance_83() { return static_cast<int32_t>(offsetof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7_StaticFields, ___Instance_83)); }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * get_Instance_83() const { return ___Instance_83; }
	inline ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 ** get_address_of_Instance_83() { return &___Instance_83; }
	inline void set_Instance_83(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7 * value)
	{
		___Instance_83 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_83), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLMAPPER_T5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7_H
#ifndef INPUTROW_T3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF_H
#define INPUTROW_T3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.InputRow
struct  InputRow_t3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.InputRow::label
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___label_4;
	// Rewired.UI.ControlMapper.ButtonInfo[] Rewired.UI.ControlMapper.InputRow::<buttons>k__BackingField
	ButtonInfoU5BU5D_tC6815BFF2944D0E87BC214FC1089791ECC3ED47A* ___U3CbuttonsU3Ek__BackingField_5;
	// System.Int32 Rewired.UI.ControlMapper.InputRow::rowIndex
	int32_t ___rowIndex_6;
	// System.Action`2<System.Int32,Rewired.UI.ControlMapper.ButtonInfo> Rewired.UI.ControlMapper.InputRow::inputFieldActivatedCallback
	Action_2_t1FA8C2434BE4B5143E2A6BBE9706302C61B7840C * ___inputFieldActivatedCallback_7;

public:
	inline static int32_t get_offset_of_label_4() { return static_cast<int32_t>(offsetof(InputRow_t3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF, ___label_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_label_4() const { return ___label_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_label_4() { return &___label_4; }
	inline void set_label_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___label_4 = value;
		Il2CppCodeGenWriteBarrier((&___label_4), value);
	}

	inline static int32_t get_offset_of_U3CbuttonsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(InputRow_t3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF, ___U3CbuttonsU3Ek__BackingField_5)); }
	inline ButtonInfoU5BU5D_tC6815BFF2944D0E87BC214FC1089791ECC3ED47A* get_U3CbuttonsU3Ek__BackingField_5() const { return ___U3CbuttonsU3Ek__BackingField_5; }
	inline ButtonInfoU5BU5D_tC6815BFF2944D0E87BC214FC1089791ECC3ED47A** get_address_of_U3CbuttonsU3Ek__BackingField_5() { return &___U3CbuttonsU3Ek__BackingField_5; }
	inline void set_U3CbuttonsU3Ek__BackingField_5(ButtonInfoU5BU5D_tC6815BFF2944D0E87BC214FC1089791ECC3ED47A* value)
	{
		___U3CbuttonsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbuttonsU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_rowIndex_6() { return static_cast<int32_t>(offsetof(InputRow_t3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF, ___rowIndex_6)); }
	inline int32_t get_rowIndex_6() const { return ___rowIndex_6; }
	inline int32_t* get_address_of_rowIndex_6() { return &___rowIndex_6; }
	inline void set_rowIndex_6(int32_t value)
	{
		___rowIndex_6 = value;
	}

	inline static int32_t get_offset_of_inputFieldActivatedCallback_7() { return static_cast<int32_t>(offsetof(InputRow_t3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF, ___inputFieldActivatedCallback_7)); }
	inline Action_2_t1FA8C2434BE4B5143E2A6BBE9706302C61B7840C * get_inputFieldActivatedCallback_7() const { return ___inputFieldActivatedCallback_7; }
	inline Action_2_t1FA8C2434BE4B5143E2A6BBE9706302C61B7840C ** get_address_of_inputFieldActivatedCallback_7() { return &___inputFieldActivatedCallback_7; }
	inline void set_inputFieldActivatedCallback_7(Action_2_t1FA8C2434BE4B5143E2A6BBE9706302C61B7840C * value)
	{
		___inputFieldActivatedCallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___inputFieldActivatedCallback_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTROW_T3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF_H
#ifndef SCROLLRECTSELECTABLECHILD_TCE1174CD9F620F2909B5934EF0A61BCD9523946C_H
#define SCROLLRECTSELECTABLECHILD_TCE1174CD9F620F2909B5934EF0A61BCD9523946C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ScrollRectSelectableChild
struct  ScrollRectSelectableChild_tCE1174CD9F620F2909B5934EF0A61BCD9523946C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Rewired.UI.ControlMapper.ScrollRectSelectableChild::useCustomEdgePadding
	bool ___useCustomEdgePadding_4;
	// System.Single Rewired.UI.ControlMapper.ScrollRectSelectableChild::customEdgePadding
	float ___customEdgePadding_5;
	// UnityEngine.UI.ScrollRect Rewired.UI.ControlMapper.ScrollRectSelectableChild::parentScrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___parentScrollRect_6;
	// UnityEngine.UI.Selectable Rewired.UI.ControlMapper.ScrollRectSelectableChild::_selectable
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ____selectable_7;

public:
	inline static int32_t get_offset_of_useCustomEdgePadding_4() { return static_cast<int32_t>(offsetof(ScrollRectSelectableChild_tCE1174CD9F620F2909B5934EF0A61BCD9523946C, ___useCustomEdgePadding_4)); }
	inline bool get_useCustomEdgePadding_4() const { return ___useCustomEdgePadding_4; }
	inline bool* get_address_of_useCustomEdgePadding_4() { return &___useCustomEdgePadding_4; }
	inline void set_useCustomEdgePadding_4(bool value)
	{
		___useCustomEdgePadding_4 = value;
	}

	inline static int32_t get_offset_of_customEdgePadding_5() { return static_cast<int32_t>(offsetof(ScrollRectSelectableChild_tCE1174CD9F620F2909B5934EF0A61BCD9523946C, ___customEdgePadding_5)); }
	inline float get_customEdgePadding_5() const { return ___customEdgePadding_5; }
	inline float* get_address_of_customEdgePadding_5() { return &___customEdgePadding_5; }
	inline void set_customEdgePadding_5(float value)
	{
		___customEdgePadding_5 = value;
	}

	inline static int32_t get_offset_of_parentScrollRect_6() { return static_cast<int32_t>(offsetof(ScrollRectSelectableChild_tCE1174CD9F620F2909B5934EF0A61BCD9523946C, ___parentScrollRect_6)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_parentScrollRect_6() const { return ___parentScrollRect_6; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_parentScrollRect_6() { return &___parentScrollRect_6; }
	inline void set_parentScrollRect_6(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___parentScrollRect_6 = value;
		Il2CppCodeGenWriteBarrier((&___parentScrollRect_6), value);
	}

	inline static int32_t get_offset_of__selectable_7() { return static_cast<int32_t>(offsetof(ScrollRectSelectableChild_tCE1174CD9F620F2909B5934EF0A61BCD9523946C, ____selectable_7)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get__selectable_7() const { return ____selectable_7; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of__selectable_7() { return &____selectable_7; }
	inline void set__selectable_7(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		____selectable_7 = value;
		Il2CppCodeGenWriteBarrier((&____selectable_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECTSELECTABLECHILD_TCE1174CD9F620F2909B5934EF0A61BCD9523946C_H
#ifndef SCROLLBARVISIBILITYHELPER_T77F02C75917777EBD1C832ABEA3C6E518FDD08B6_H
#define SCROLLBARVISIBILITYHELPER_T77F02C75917777EBD1C832ABEA3C6E518FDD08B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ScrollbarVisibilityHelper
struct  ScrollbarVisibilityHelper_t77F02C75917777EBD1C832ABEA3C6E518FDD08B6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.ScrollRect Rewired.UI.ControlMapper.ScrollbarVisibilityHelper::scrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___scrollRect_4;

public:
	inline static int32_t get_offset_of_scrollRect_4() { return static_cast<int32_t>(offsetof(ScrollbarVisibilityHelper_t77F02C75917777EBD1C832ABEA3C6E518FDD08B6, ___scrollRect_4)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_scrollRect_4() const { return ___scrollRect_4; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_scrollRect_4() { return &___scrollRect_4; }
	inline void set_scrollRect_4(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___scrollRect_4 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBARVISIBILITYHELPER_T77F02C75917777EBD1C832ABEA3C6E518FDD08B6_H
#ifndef UIELEMENTINFO_T2DBC68E382F8428D528D5AF6D14EB38F998B3ECA_H
#define UIELEMENTINFO_T2DBC68E382F8428D528D5AF6D14EB38F998B3ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.UIElementInfo
struct  UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Rewired.UI.ControlMapper.UIElementInfo::identifier
	String_t* ___identifier_4;
	// System.Int32 Rewired.UI.ControlMapper.UIElementInfo::intData
	int32_t ___intData_5;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.UIElementInfo::text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___text_6;
	// System.Action`1<UnityEngine.GameObject> Rewired.UI.ControlMapper.UIElementInfo::OnSelectedEvent
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___OnSelectedEvent_7;

public:
	inline static int32_t get_offset_of_identifier_4() { return static_cast<int32_t>(offsetof(UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA, ___identifier_4)); }
	inline String_t* get_identifier_4() const { return ___identifier_4; }
	inline String_t** get_address_of_identifier_4() { return &___identifier_4; }
	inline void set_identifier_4(String_t* value)
	{
		___identifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_4), value);
	}

	inline static int32_t get_offset_of_intData_5() { return static_cast<int32_t>(offsetof(UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA, ___intData_5)); }
	inline int32_t get_intData_5() const { return ___intData_5; }
	inline int32_t* get_address_of_intData_5() { return &___intData_5; }
	inline void set_intData_5(int32_t value)
	{
		___intData_5 = value;
	}

	inline static int32_t get_offset_of_text_6() { return static_cast<int32_t>(offsetof(UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA, ___text_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_text_6() const { return ___text_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_text_6() { return &___text_6; }
	inline void set_text_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___text_6 = value;
		Il2CppCodeGenWriteBarrier((&___text_6), value);
	}

	inline static int32_t get_offset_of_OnSelectedEvent_7() { return static_cast<int32_t>(offsetof(UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA, ___OnSelectedEvent_7)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_OnSelectedEvent_7() const { return ___OnSelectedEvent_7; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_OnSelectedEvent_7() { return &___OnSelectedEvent_7; }
	inline void set_OnSelectedEvent_7(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___OnSelectedEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectedEvent_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIELEMENTINFO_T2DBC68E382F8428D528D5AF6D14EB38F998B3ECA_H
#ifndef WINDOW_T05E229BCE867863C1A33354186F3E7641707AAA5_H
#define WINDOW_T05E229BCE867863C1A33354186F3E7641707AAA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.Window
struct  Window_t05E229BCE867863C1A33354186F3E7641707AAA5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image Rewired.UI.ControlMapper.Window::backgroundImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___backgroundImage_4;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.Window::content
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___content_5;
	// System.Boolean Rewired.UI.ControlMapper.Window::_initialized
	bool ____initialized_6;
	// System.Int32 Rewired.UI.ControlMapper.Window::_id
	int32_t ____id_7;
	// UnityEngine.RectTransform Rewired.UI.ControlMapper.Window::_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____rectTransform_8;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.Window::_titleText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____titleText_9;
	// System.Collections.Generic.List`1<UnityEngine.UI.Text> Rewired.UI.ControlMapper.Window::_contentText
	List_1_t48FDD3CF761FA5A5451E50F64C5529D419339F18 * ____contentText_10;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.Window::_defaultUIElement
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____defaultUIElement_11;
	// System.Action`1<System.Int32> Rewired.UI.ControlMapper.Window::_updateCallback
	Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * ____updateCallback_12;
	// System.Func`2<System.Int32,System.Boolean> Rewired.UI.ControlMapper.Window::_isFocusedCallback
	Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * ____isFocusedCallback_13;
	// Rewired.UI.ControlMapper.Window_Timer Rewired.UI.ControlMapper.Window::_timer
	Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D * ____timer_14;
	// UnityEngine.CanvasGroup Rewired.UI.ControlMapper.Window::_canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ____canvasGroup_15;
	// UnityEngine.Events.UnityAction Rewired.UI.ControlMapper.Window::cancelCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___cancelCallback_16;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.Window::lastUISelection
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lastUISelection_17;

public:
	inline static int32_t get_offset_of_backgroundImage_4() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ___backgroundImage_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_backgroundImage_4() const { return ___backgroundImage_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_backgroundImage_4() { return &___backgroundImage_4; }
	inline void set_backgroundImage_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___backgroundImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundImage_4), value);
	}

	inline static int32_t get_offset_of_content_5() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ___content_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_content_5() const { return ___content_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_content_5() { return &___content_5; }
	inline void set_content_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___content_5 = value;
		Il2CppCodeGenWriteBarrier((&___content_5), value);
	}

	inline static int32_t get_offset_of__initialized_6() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____initialized_6)); }
	inline bool get__initialized_6() const { return ____initialized_6; }
	inline bool* get_address_of__initialized_6() { return &____initialized_6; }
	inline void set__initialized_6(bool value)
	{
		____initialized_6 = value;
	}

	inline static int32_t get_offset_of__id_7() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____id_7)); }
	inline int32_t get__id_7() const { return ____id_7; }
	inline int32_t* get_address_of__id_7() { return &____id_7; }
	inline void set__id_7(int32_t value)
	{
		____id_7 = value;
	}

	inline static int32_t get_offset_of__rectTransform_8() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____rectTransform_8)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__rectTransform_8() const { return ____rectTransform_8; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__rectTransform_8() { return &____rectTransform_8; }
	inline void set__rectTransform_8(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____rectTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_8), value);
	}

	inline static int32_t get_offset_of__titleText_9() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____titleText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__titleText_9() const { return ____titleText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__titleText_9() { return &____titleText_9; }
	inline void set__titleText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____titleText_9 = value;
		Il2CppCodeGenWriteBarrier((&____titleText_9), value);
	}

	inline static int32_t get_offset_of__contentText_10() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____contentText_10)); }
	inline List_1_t48FDD3CF761FA5A5451E50F64C5529D419339F18 * get__contentText_10() const { return ____contentText_10; }
	inline List_1_t48FDD3CF761FA5A5451E50F64C5529D419339F18 ** get_address_of__contentText_10() { return &____contentText_10; }
	inline void set__contentText_10(List_1_t48FDD3CF761FA5A5451E50F64C5529D419339F18 * value)
	{
		____contentText_10 = value;
		Il2CppCodeGenWriteBarrier((&____contentText_10), value);
	}

	inline static int32_t get_offset_of__defaultUIElement_11() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____defaultUIElement_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__defaultUIElement_11() const { return ____defaultUIElement_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__defaultUIElement_11() { return &____defaultUIElement_11; }
	inline void set__defaultUIElement_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____defaultUIElement_11 = value;
		Il2CppCodeGenWriteBarrier((&____defaultUIElement_11), value);
	}

	inline static int32_t get_offset_of__updateCallback_12() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____updateCallback_12)); }
	inline Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * get__updateCallback_12() const { return ____updateCallback_12; }
	inline Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B ** get_address_of__updateCallback_12() { return &____updateCallback_12; }
	inline void set__updateCallback_12(Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * value)
	{
		____updateCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&____updateCallback_12), value);
	}

	inline static int32_t get_offset_of__isFocusedCallback_13() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____isFocusedCallback_13)); }
	inline Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * get__isFocusedCallback_13() const { return ____isFocusedCallback_13; }
	inline Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 ** get_address_of__isFocusedCallback_13() { return &____isFocusedCallback_13; }
	inline void set__isFocusedCallback_13(Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * value)
	{
		____isFocusedCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&____isFocusedCallback_13), value);
	}

	inline static int32_t get_offset_of__timer_14() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____timer_14)); }
	inline Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D * get__timer_14() const { return ____timer_14; }
	inline Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D ** get_address_of__timer_14() { return &____timer_14; }
	inline void set__timer_14(Timer_tDCE4DB70509B3359154AF619B30AE8BA8C82D67D * value)
	{
		____timer_14 = value;
		Il2CppCodeGenWriteBarrier((&____timer_14), value);
	}

	inline static int32_t get_offset_of__canvasGroup_15() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ____canvasGroup_15)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get__canvasGroup_15() const { return ____canvasGroup_15; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of__canvasGroup_15() { return &____canvasGroup_15; }
	inline void set__canvasGroup_15(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		____canvasGroup_15 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_15), value);
	}

	inline static int32_t get_offset_of_cancelCallback_16() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ___cancelCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_cancelCallback_16() const { return ___cancelCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_cancelCallback_16() { return &___cancelCallback_16; }
	inline void set_cancelCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___cancelCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___cancelCallback_16), value);
	}

	inline static int32_t get_offset_of_lastUISelection_17() { return static_cast<int32_t>(offsetof(Window_t05E229BCE867863C1A33354186F3E7641707AAA5, ___lastUISelection_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_lastUISelection_17() const { return ___lastUISelection_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_lastUISelection_17() { return &___lastUISelection_17; }
	inline void set_lastUISelection_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___lastUISelection_17 = value;
		Il2CppCodeGenWriteBarrier((&___lastUISelection_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOW_T05E229BCE867863C1A33354186F3E7641707AAA5_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef USERDATASTORE_PLAYERPREFS_T91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5_H
#define USERDATASTORE_PLAYERPREFS_T91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.UserDataStore_PlayerPrefs
struct  UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5  : public UserDataStore_tA04E79BDF387346606C6F4379B4FD0935BA176C1
{
public:
	// System.Boolean Rewired.Data.UserDataStore_PlayerPrefs::isEnabled
	bool ___isEnabled_8;
	// System.Boolean Rewired.Data.UserDataStore_PlayerPrefs::loadDataOnStart
	bool ___loadDataOnStart_9;
	// System.Boolean Rewired.Data.UserDataStore_PlayerPrefs::loadJoystickAssignments
	bool ___loadJoystickAssignments_10;
	// System.Boolean Rewired.Data.UserDataStore_PlayerPrefs::loadKeyboardAssignments
	bool ___loadKeyboardAssignments_11;
	// System.Boolean Rewired.Data.UserDataStore_PlayerPrefs::loadMouseAssignments
	bool ___loadMouseAssignments_12;
	// System.String Rewired.Data.UserDataStore_PlayerPrefs::playerPrefsKeyPrefix
	String_t* ___playerPrefsKeyPrefix_13;
	// System.Boolean Rewired.Data.UserDataStore_PlayerPrefs::allowImpreciseJoystickAssignmentMatching
	bool ___allowImpreciseJoystickAssignmentMatching_14;
	// System.Boolean Rewired.Data.UserDataStore_PlayerPrefs::deferredJoystickAssignmentLoadPending
	bool ___deferredJoystickAssignmentLoadPending_15;
	// System.Boolean Rewired.Data.UserDataStore_PlayerPrefs::wasJoystickEverDetected
	bool ___wasJoystickEverDetected_16;
	// System.Collections.Generic.List`1<System.Int32> Rewired.Data.UserDataStore_PlayerPrefs::__allActionIds
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * _____allActionIds_17;
	// System.String Rewired.Data.UserDataStore_PlayerPrefs::__allActionIdsString
	String_t* _____allActionIdsString_18;

public:
	inline static int32_t get_offset_of_isEnabled_8() { return static_cast<int32_t>(offsetof(UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5, ___isEnabled_8)); }
	inline bool get_isEnabled_8() const { return ___isEnabled_8; }
	inline bool* get_address_of_isEnabled_8() { return &___isEnabled_8; }
	inline void set_isEnabled_8(bool value)
	{
		___isEnabled_8 = value;
	}

	inline static int32_t get_offset_of_loadDataOnStart_9() { return static_cast<int32_t>(offsetof(UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5, ___loadDataOnStart_9)); }
	inline bool get_loadDataOnStart_9() const { return ___loadDataOnStart_9; }
	inline bool* get_address_of_loadDataOnStart_9() { return &___loadDataOnStart_9; }
	inline void set_loadDataOnStart_9(bool value)
	{
		___loadDataOnStart_9 = value;
	}

	inline static int32_t get_offset_of_loadJoystickAssignments_10() { return static_cast<int32_t>(offsetof(UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5, ___loadJoystickAssignments_10)); }
	inline bool get_loadJoystickAssignments_10() const { return ___loadJoystickAssignments_10; }
	inline bool* get_address_of_loadJoystickAssignments_10() { return &___loadJoystickAssignments_10; }
	inline void set_loadJoystickAssignments_10(bool value)
	{
		___loadJoystickAssignments_10 = value;
	}

	inline static int32_t get_offset_of_loadKeyboardAssignments_11() { return static_cast<int32_t>(offsetof(UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5, ___loadKeyboardAssignments_11)); }
	inline bool get_loadKeyboardAssignments_11() const { return ___loadKeyboardAssignments_11; }
	inline bool* get_address_of_loadKeyboardAssignments_11() { return &___loadKeyboardAssignments_11; }
	inline void set_loadKeyboardAssignments_11(bool value)
	{
		___loadKeyboardAssignments_11 = value;
	}

	inline static int32_t get_offset_of_loadMouseAssignments_12() { return static_cast<int32_t>(offsetof(UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5, ___loadMouseAssignments_12)); }
	inline bool get_loadMouseAssignments_12() const { return ___loadMouseAssignments_12; }
	inline bool* get_address_of_loadMouseAssignments_12() { return &___loadMouseAssignments_12; }
	inline void set_loadMouseAssignments_12(bool value)
	{
		___loadMouseAssignments_12 = value;
	}

	inline static int32_t get_offset_of_playerPrefsKeyPrefix_13() { return static_cast<int32_t>(offsetof(UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5, ___playerPrefsKeyPrefix_13)); }
	inline String_t* get_playerPrefsKeyPrefix_13() const { return ___playerPrefsKeyPrefix_13; }
	inline String_t** get_address_of_playerPrefsKeyPrefix_13() { return &___playerPrefsKeyPrefix_13; }
	inline void set_playerPrefsKeyPrefix_13(String_t* value)
	{
		___playerPrefsKeyPrefix_13 = value;
		Il2CppCodeGenWriteBarrier((&___playerPrefsKeyPrefix_13), value);
	}

	inline static int32_t get_offset_of_allowImpreciseJoystickAssignmentMatching_14() { return static_cast<int32_t>(offsetof(UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5, ___allowImpreciseJoystickAssignmentMatching_14)); }
	inline bool get_allowImpreciseJoystickAssignmentMatching_14() const { return ___allowImpreciseJoystickAssignmentMatching_14; }
	inline bool* get_address_of_allowImpreciseJoystickAssignmentMatching_14() { return &___allowImpreciseJoystickAssignmentMatching_14; }
	inline void set_allowImpreciseJoystickAssignmentMatching_14(bool value)
	{
		___allowImpreciseJoystickAssignmentMatching_14 = value;
	}

	inline static int32_t get_offset_of_deferredJoystickAssignmentLoadPending_15() { return static_cast<int32_t>(offsetof(UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5, ___deferredJoystickAssignmentLoadPending_15)); }
	inline bool get_deferredJoystickAssignmentLoadPending_15() const { return ___deferredJoystickAssignmentLoadPending_15; }
	inline bool* get_address_of_deferredJoystickAssignmentLoadPending_15() { return &___deferredJoystickAssignmentLoadPending_15; }
	inline void set_deferredJoystickAssignmentLoadPending_15(bool value)
	{
		___deferredJoystickAssignmentLoadPending_15 = value;
	}

	inline static int32_t get_offset_of_wasJoystickEverDetected_16() { return static_cast<int32_t>(offsetof(UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5, ___wasJoystickEverDetected_16)); }
	inline bool get_wasJoystickEverDetected_16() const { return ___wasJoystickEverDetected_16; }
	inline bool* get_address_of_wasJoystickEverDetected_16() { return &___wasJoystickEverDetected_16; }
	inline void set_wasJoystickEverDetected_16(bool value)
	{
		___wasJoystickEverDetected_16 = value;
	}

	inline static int32_t get_offset_of___allActionIds_17() { return static_cast<int32_t>(offsetof(UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5, _____allActionIds_17)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get___allActionIds_17() const { return _____allActionIds_17; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of___allActionIds_17() { return &_____allActionIds_17; }
	inline void set___allActionIds_17(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		_____allActionIds_17 = value;
		Il2CppCodeGenWriteBarrier((&_____allActionIds_17), value);
	}

	inline static int32_t get_offset_of___allActionIdsString_18() { return static_cast<int32_t>(offsetof(UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5, _____allActionIdsString_18)); }
	inline String_t* get___allActionIdsString_18() const { return _____allActionIdsString_18; }
	inline String_t** get_address_of___allActionIdsString_18() { return &_____allActionIdsString_18; }
	inline void set___allActionIdsString_18(String_t* value)
	{
		_____allActionIdsString_18 = value;
		Il2CppCodeGenWriteBarrier((&_____allActionIdsString_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDATASTORE_PLAYERPREFS_T91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5_H
#ifndef INPUTMANAGER_T42964CC63A93EEFEC8C958ED5DF1166DB01B09CA_H
#define INPUTMANAGER_T42964CC63A93EEFEC8C958ED5DF1166DB01B09CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputManager
struct  InputManager_t42964CC63A93EEFEC8C958ED5DF1166DB01B09CA  : public InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMANAGER_T42964CC63A93EEFEC8C958ED5DF1166DB01B09CA_H
#ifndef BUTTONINFO_T09F7FD5F90E913E38AD44D748529C16955E0B9C3_H
#define BUTTONINFO_T09F7FD5F90E913E38AD44D748529C16955E0B9C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.ButtonInfo
struct  ButtonInfo_t09F7FD5F90E913E38AD44D748529C16955E0B9C3  : public UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONINFO_T09F7FD5F90E913E38AD44D748529C16955E0B9C3_H
#ifndef CALIBRATIONWINDOW_T3B201B7586ADF13BF56596085B279B38129E76E6_H
#define CALIBRATIONWINDOW_T3B201B7586ADF13BF56596085B279B38129E76E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.CalibrationWindow
struct  CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6  : public Window_t05E229BCE867863C1A33354186F3E7641707AAA5
{
public:
	// UnityEngine.RectTransform Rewired.UI.ControlMapper.CalibrationWindow::rightContentContainer
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rightContentContainer_20;
	// UnityEngine.RectTransform Rewired.UI.ControlMapper.CalibrationWindow::valueDisplayGroup
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___valueDisplayGroup_21;
	// UnityEngine.RectTransform Rewired.UI.ControlMapper.CalibrationWindow::calibratedValueMarker
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___calibratedValueMarker_22;
	// UnityEngine.RectTransform Rewired.UI.ControlMapper.CalibrationWindow::rawValueMarker
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rawValueMarker_23;
	// UnityEngine.RectTransform Rewired.UI.ControlMapper.CalibrationWindow::calibratedZeroMarker
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___calibratedZeroMarker_24;
	// UnityEngine.RectTransform Rewired.UI.ControlMapper.CalibrationWindow::deadzoneArea
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___deadzoneArea_25;
	// UnityEngine.UI.Slider Rewired.UI.ControlMapper.CalibrationWindow::deadzoneSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___deadzoneSlider_26;
	// UnityEngine.UI.Slider Rewired.UI.ControlMapper.CalibrationWindow::zeroSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___zeroSlider_27;
	// UnityEngine.UI.Slider Rewired.UI.ControlMapper.CalibrationWindow::sensitivitySlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___sensitivitySlider_28;
	// UnityEngine.UI.Toggle Rewired.UI.ControlMapper.CalibrationWindow::invertToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___invertToggle_29;
	// UnityEngine.RectTransform Rewired.UI.ControlMapper.CalibrationWindow::axisScrollAreaContent
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___axisScrollAreaContent_30;
	// UnityEngine.UI.Button Rewired.UI.ControlMapper.CalibrationWindow::doneButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___doneButton_31;
	// UnityEngine.UI.Button Rewired.UI.ControlMapper.CalibrationWindow::calibrateButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___calibrateButton_32;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.CalibrationWindow::doneButtonLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___doneButtonLabel_33;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.CalibrationWindow::cancelButtonLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___cancelButtonLabel_34;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.CalibrationWindow::defaultButtonLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___defaultButtonLabel_35;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.CalibrationWindow::deadzoneSliderLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___deadzoneSliderLabel_36;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.CalibrationWindow::zeroSliderLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___zeroSliderLabel_37;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.CalibrationWindow::sensitivitySliderLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___sensitivitySliderLabel_38;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.CalibrationWindow::invertToggleLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___invertToggleLabel_39;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.CalibrationWindow::calibrateButtonLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___calibrateButtonLabel_40;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.CalibrationWindow::axisButtonPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___axisButtonPrefab_41;
	// Rewired.Joystick Rewired.UI.ControlMapper.CalibrationWindow::joystick
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___joystick_42;
	// System.String Rewired.UI.ControlMapper.CalibrationWindow::origCalibrationData
	String_t* ___origCalibrationData_43;
	// System.Int32 Rewired.UI.ControlMapper.CalibrationWindow::selectedAxis
	int32_t ___selectedAxis_44;
	// Rewired.AxisCalibrationData Rewired.UI.ControlMapper.CalibrationWindow::origSelectedAxisCalibrationData
	AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171  ___origSelectedAxisCalibrationData_45;
	// System.Single Rewired.UI.ControlMapper.CalibrationWindow::displayAreaWidth
	float ___displayAreaWidth_46;
	// System.Collections.Generic.List`1<UnityEngine.UI.Button> Rewired.UI.ControlMapper.CalibrationWindow::axisButtons
	List_1_tE74BD634C2F383838544C1E81C81B7E136CE6BC5 * ___axisButtons_47;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Int32>> Rewired.UI.ControlMapper.CalibrationWindow::buttonCallbacks
	Dictionary_2_t030E2D78EA98876F80F098D4E3C0AA3B18AC72EE * ___buttonCallbacks_48;
	// System.Int32 Rewired.UI.ControlMapper.CalibrationWindow::playerId
	int32_t ___playerId_49;
	// Rewired.Integration.UnityUI.RewiredStandaloneInputModule Rewired.UI.ControlMapper.CalibrationWindow::rewiredStandaloneInputModule
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7 * ___rewiredStandaloneInputModule_50;
	// System.Int32 Rewired.UI.ControlMapper.CalibrationWindow::menuHorizActionId
	int32_t ___menuHorizActionId_51;
	// System.Int32 Rewired.UI.ControlMapper.CalibrationWindow::menuVertActionId
	int32_t ___menuVertActionId_52;
	// System.Single Rewired.UI.ControlMapper.CalibrationWindow::minSensitivity
	float ___minSensitivity_53;

public:
	inline static int32_t get_offset_of_rightContentContainer_20() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___rightContentContainer_20)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rightContentContainer_20() const { return ___rightContentContainer_20; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rightContentContainer_20() { return &___rightContentContainer_20; }
	inline void set_rightContentContainer_20(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rightContentContainer_20 = value;
		Il2CppCodeGenWriteBarrier((&___rightContentContainer_20), value);
	}

	inline static int32_t get_offset_of_valueDisplayGroup_21() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___valueDisplayGroup_21)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_valueDisplayGroup_21() const { return ___valueDisplayGroup_21; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_valueDisplayGroup_21() { return &___valueDisplayGroup_21; }
	inline void set_valueDisplayGroup_21(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___valueDisplayGroup_21 = value;
		Il2CppCodeGenWriteBarrier((&___valueDisplayGroup_21), value);
	}

	inline static int32_t get_offset_of_calibratedValueMarker_22() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___calibratedValueMarker_22)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_calibratedValueMarker_22() const { return ___calibratedValueMarker_22; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_calibratedValueMarker_22() { return &___calibratedValueMarker_22; }
	inline void set_calibratedValueMarker_22(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___calibratedValueMarker_22 = value;
		Il2CppCodeGenWriteBarrier((&___calibratedValueMarker_22), value);
	}

	inline static int32_t get_offset_of_rawValueMarker_23() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___rawValueMarker_23)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rawValueMarker_23() const { return ___rawValueMarker_23; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rawValueMarker_23() { return &___rawValueMarker_23; }
	inline void set_rawValueMarker_23(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rawValueMarker_23 = value;
		Il2CppCodeGenWriteBarrier((&___rawValueMarker_23), value);
	}

	inline static int32_t get_offset_of_calibratedZeroMarker_24() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___calibratedZeroMarker_24)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_calibratedZeroMarker_24() const { return ___calibratedZeroMarker_24; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_calibratedZeroMarker_24() { return &___calibratedZeroMarker_24; }
	inline void set_calibratedZeroMarker_24(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___calibratedZeroMarker_24 = value;
		Il2CppCodeGenWriteBarrier((&___calibratedZeroMarker_24), value);
	}

	inline static int32_t get_offset_of_deadzoneArea_25() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___deadzoneArea_25)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_deadzoneArea_25() const { return ___deadzoneArea_25; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_deadzoneArea_25() { return &___deadzoneArea_25; }
	inline void set_deadzoneArea_25(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___deadzoneArea_25 = value;
		Il2CppCodeGenWriteBarrier((&___deadzoneArea_25), value);
	}

	inline static int32_t get_offset_of_deadzoneSlider_26() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___deadzoneSlider_26)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_deadzoneSlider_26() const { return ___deadzoneSlider_26; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_deadzoneSlider_26() { return &___deadzoneSlider_26; }
	inline void set_deadzoneSlider_26(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___deadzoneSlider_26 = value;
		Il2CppCodeGenWriteBarrier((&___deadzoneSlider_26), value);
	}

	inline static int32_t get_offset_of_zeroSlider_27() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___zeroSlider_27)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_zeroSlider_27() const { return ___zeroSlider_27; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_zeroSlider_27() { return &___zeroSlider_27; }
	inline void set_zeroSlider_27(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___zeroSlider_27 = value;
		Il2CppCodeGenWriteBarrier((&___zeroSlider_27), value);
	}

	inline static int32_t get_offset_of_sensitivitySlider_28() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___sensitivitySlider_28)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_sensitivitySlider_28() const { return ___sensitivitySlider_28; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_sensitivitySlider_28() { return &___sensitivitySlider_28; }
	inline void set_sensitivitySlider_28(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___sensitivitySlider_28 = value;
		Il2CppCodeGenWriteBarrier((&___sensitivitySlider_28), value);
	}

	inline static int32_t get_offset_of_invertToggle_29() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___invertToggle_29)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_invertToggle_29() const { return ___invertToggle_29; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_invertToggle_29() { return &___invertToggle_29; }
	inline void set_invertToggle_29(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___invertToggle_29 = value;
		Il2CppCodeGenWriteBarrier((&___invertToggle_29), value);
	}

	inline static int32_t get_offset_of_axisScrollAreaContent_30() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___axisScrollAreaContent_30)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_axisScrollAreaContent_30() const { return ___axisScrollAreaContent_30; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_axisScrollAreaContent_30() { return &___axisScrollAreaContent_30; }
	inline void set_axisScrollAreaContent_30(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___axisScrollAreaContent_30 = value;
		Il2CppCodeGenWriteBarrier((&___axisScrollAreaContent_30), value);
	}

	inline static int32_t get_offset_of_doneButton_31() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___doneButton_31)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_doneButton_31() const { return ___doneButton_31; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_doneButton_31() { return &___doneButton_31; }
	inline void set_doneButton_31(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___doneButton_31 = value;
		Il2CppCodeGenWriteBarrier((&___doneButton_31), value);
	}

	inline static int32_t get_offset_of_calibrateButton_32() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___calibrateButton_32)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_calibrateButton_32() const { return ___calibrateButton_32; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_calibrateButton_32() { return &___calibrateButton_32; }
	inline void set_calibrateButton_32(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___calibrateButton_32 = value;
		Il2CppCodeGenWriteBarrier((&___calibrateButton_32), value);
	}

	inline static int32_t get_offset_of_doneButtonLabel_33() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___doneButtonLabel_33)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_doneButtonLabel_33() const { return ___doneButtonLabel_33; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_doneButtonLabel_33() { return &___doneButtonLabel_33; }
	inline void set_doneButtonLabel_33(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___doneButtonLabel_33 = value;
		Il2CppCodeGenWriteBarrier((&___doneButtonLabel_33), value);
	}

	inline static int32_t get_offset_of_cancelButtonLabel_34() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___cancelButtonLabel_34)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_cancelButtonLabel_34() const { return ___cancelButtonLabel_34; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_cancelButtonLabel_34() { return &___cancelButtonLabel_34; }
	inline void set_cancelButtonLabel_34(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___cancelButtonLabel_34 = value;
		Il2CppCodeGenWriteBarrier((&___cancelButtonLabel_34), value);
	}

	inline static int32_t get_offset_of_defaultButtonLabel_35() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___defaultButtonLabel_35)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_defaultButtonLabel_35() const { return ___defaultButtonLabel_35; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_defaultButtonLabel_35() { return &___defaultButtonLabel_35; }
	inline void set_defaultButtonLabel_35(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___defaultButtonLabel_35 = value;
		Il2CppCodeGenWriteBarrier((&___defaultButtonLabel_35), value);
	}

	inline static int32_t get_offset_of_deadzoneSliderLabel_36() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___deadzoneSliderLabel_36)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_deadzoneSliderLabel_36() const { return ___deadzoneSliderLabel_36; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_deadzoneSliderLabel_36() { return &___deadzoneSliderLabel_36; }
	inline void set_deadzoneSliderLabel_36(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___deadzoneSliderLabel_36 = value;
		Il2CppCodeGenWriteBarrier((&___deadzoneSliderLabel_36), value);
	}

	inline static int32_t get_offset_of_zeroSliderLabel_37() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___zeroSliderLabel_37)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_zeroSliderLabel_37() const { return ___zeroSliderLabel_37; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_zeroSliderLabel_37() { return &___zeroSliderLabel_37; }
	inline void set_zeroSliderLabel_37(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___zeroSliderLabel_37 = value;
		Il2CppCodeGenWriteBarrier((&___zeroSliderLabel_37), value);
	}

	inline static int32_t get_offset_of_sensitivitySliderLabel_38() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___sensitivitySliderLabel_38)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_sensitivitySliderLabel_38() const { return ___sensitivitySliderLabel_38; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_sensitivitySliderLabel_38() { return &___sensitivitySliderLabel_38; }
	inline void set_sensitivitySliderLabel_38(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___sensitivitySliderLabel_38 = value;
		Il2CppCodeGenWriteBarrier((&___sensitivitySliderLabel_38), value);
	}

	inline static int32_t get_offset_of_invertToggleLabel_39() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___invertToggleLabel_39)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_invertToggleLabel_39() const { return ___invertToggleLabel_39; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_invertToggleLabel_39() { return &___invertToggleLabel_39; }
	inline void set_invertToggleLabel_39(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___invertToggleLabel_39 = value;
		Il2CppCodeGenWriteBarrier((&___invertToggleLabel_39), value);
	}

	inline static int32_t get_offset_of_calibrateButtonLabel_40() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___calibrateButtonLabel_40)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_calibrateButtonLabel_40() const { return ___calibrateButtonLabel_40; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_calibrateButtonLabel_40() { return &___calibrateButtonLabel_40; }
	inline void set_calibrateButtonLabel_40(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___calibrateButtonLabel_40 = value;
		Il2CppCodeGenWriteBarrier((&___calibrateButtonLabel_40), value);
	}

	inline static int32_t get_offset_of_axisButtonPrefab_41() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___axisButtonPrefab_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_axisButtonPrefab_41() const { return ___axisButtonPrefab_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_axisButtonPrefab_41() { return &___axisButtonPrefab_41; }
	inline void set_axisButtonPrefab_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___axisButtonPrefab_41 = value;
		Il2CppCodeGenWriteBarrier((&___axisButtonPrefab_41), value);
	}

	inline static int32_t get_offset_of_joystick_42() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___joystick_42)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_joystick_42() const { return ___joystick_42; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_joystick_42() { return &___joystick_42; }
	inline void set_joystick_42(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___joystick_42 = value;
		Il2CppCodeGenWriteBarrier((&___joystick_42), value);
	}

	inline static int32_t get_offset_of_origCalibrationData_43() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___origCalibrationData_43)); }
	inline String_t* get_origCalibrationData_43() const { return ___origCalibrationData_43; }
	inline String_t** get_address_of_origCalibrationData_43() { return &___origCalibrationData_43; }
	inline void set_origCalibrationData_43(String_t* value)
	{
		___origCalibrationData_43 = value;
		Il2CppCodeGenWriteBarrier((&___origCalibrationData_43), value);
	}

	inline static int32_t get_offset_of_selectedAxis_44() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___selectedAxis_44)); }
	inline int32_t get_selectedAxis_44() const { return ___selectedAxis_44; }
	inline int32_t* get_address_of_selectedAxis_44() { return &___selectedAxis_44; }
	inline void set_selectedAxis_44(int32_t value)
	{
		___selectedAxis_44 = value;
	}

	inline static int32_t get_offset_of_origSelectedAxisCalibrationData_45() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___origSelectedAxisCalibrationData_45)); }
	inline AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171  get_origSelectedAxisCalibrationData_45() const { return ___origSelectedAxisCalibrationData_45; }
	inline AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171 * get_address_of_origSelectedAxisCalibrationData_45() { return &___origSelectedAxisCalibrationData_45; }
	inline void set_origSelectedAxisCalibrationData_45(AxisCalibrationData_t2929DB24F1620CF33F77A2431FFB5C3889C31171  value)
	{
		___origSelectedAxisCalibrationData_45 = value;
	}

	inline static int32_t get_offset_of_displayAreaWidth_46() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___displayAreaWidth_46)); }
	inline float get_displayAreaWidth_46() const { return ___displayAreaWidth_46; }
	inline float* get_address_of_displayAreaWidth_46() { return &___displayAreaWidth_46; }
	inline void set_displayAreaWidth_46(float value)
	{
		___displayAreaWidth_46 = value;
	}

	inline static int32_t get_offset_of_axisButtons_47() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___axisButtons_47)); }
	inline List_1_tE74BD634C2F383838544C1E81C81B7E136CE6BC5 * get_axisButtons_47() const { return ___axisButtons_47; }
	inline List_1_tE74BD634C2F383838544C1E81C81B7E136CE6BC5 ** get_address_of_axisButtons_47() { return &___axisButtons_47; }
	inline void set_axisButtons_47(List_1_tE74BD634C2F383838544C1E81C81B7E136CE6BC5 * value)
	{
		___axisButtons_47 = value;
		Il2CppCodeGenWriteBarrier((&___axisButtons_47), value);
	}

	inline static int32_t get_offset_of_buttonCallbacks_48() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___buttonCallbacks_48)); }
	inline Dictionary_2_t030E2D78EA98876F80F098D4E3C0AA3B18AC72EE * get_buttonCallbacks_48() const { return ___buttonCallbacks_48; }
	inline Dictionary_2_t030E2D78EA98876F80F098D4E3C0AA3B18AC72EE ** get_address_of_buttonCallbacks_48() { return &___buttonCallbacks_48; }
	inline void set_buttonCallbacks_48(Dictionary_2_t030E2D78EA98876F80F098D4E3C0AA3B18AC72EE * value)
	{
		___buttonCallbacks_48 = value;
		Il2CppCodeGenWriteBarrier((&___buttonCallbacks_48), value);
	}

	inline static int32_t get_offset_of_playerId_49() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___playerId_49)); }
	inline int32_t get_playerId_49() const { return ___playerId_49; }
	inline int32_t* get_address_of_playerId_49() { return &___playerId_49; }
	inline void set_playerId_49(int32_t value)
	{
		___playerId_49 = value;
	}

	inline static int32_t get_offset_of_rewiredStandaloneInputModule_50() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___rewiredStandaloneInputModule_50)); }
	inline RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7 * get_rewiredStandaloneInputModule_50() const { return ___rewiredStandaloneInputModule_50; }
	inline RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7 ** get_address_of_rewiredStandaloneInputModule_50() { return &___rewiredStandaloneInputModule_50; }
	inline void set_rewiredStandaloneInputModule_50(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7 * value)
	{
		___rewiredStandaloneInputModule_50 = value;
		Il2CppCodeGenWriteBarrier((&___rewiredStandaloneInputModule_50), value);
	}

	inline static int32_t get_offset_of_menuHorizActionId_51() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___menuHorizActionId_51)); }
	inline int32_t get_menuHorizActionId_51() const { return ___menuHorizActionId_51; }
	inline int32_t* get_address_of_menuHorizActionId_51() { return &___menuHorizActionId_51; }
	inline void set_menuHorizActionId_51(int32_t value)
	{
		___menuHorizActionId_51 = value;
	}

	inline static int32_t get_offset_of_menuVertActionId_52() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___menuVertActionId_52)); }
	inline int32_t get_menuVertActionId_52() const { return ___menuVertActionId_52; }
	inline int32_t* get_address_of_menuVertActionId_52() { return &___menuVertActionId_52; }
	inline void set_menuVertActionId_52(int32_t value)
	{
		___menuVertActionId_52 = value;
	}

	inline static int32_t get_offset_of_minSensitivity_53() { return static_cast<int32_t>(offsetof(CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6, ___minSensitivity_53)); }
	inline float get_minSensitivity_53() const { return ___minSensitivity_53; }
	inline float* get_address_of_minSensitivity_53() { return &___minSensitivity_53; }
	inline void set_minSensitivity_53(float value)
	{
		___minSensitivity_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALIBRATIONWINDOW_T3B201B7586ADF13BF56596085B279B38129E76E6_H
#ifndef INPUTBEHAVIORWINDOW_T1837B5E28A6EB46B7037D62A71003FD6CEA2369A_H
#define INPUTBEHAVIORWINDOW_T1837B5E28A6EB46B7037D62A71003FD6CEA2369A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.InputBehaviorWindow
struct  InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A  : public Window_t05E229BCE867863C1A33354186F3E7641707AAA5
{
public:
	// UnityEngine.RectTransform Rewired.UI.ControlMapper.InputBehaviorWindow::spawnTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___spawnTransform_19;
	// UnityEngine.UI.Button Rewired.UI.ControlMapper.InputBehaviorWindow::doneButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___doneButton_20;
	// UnityEngine.UI.Button Rewired.UI.ControlMapper.InputBehaviorWindow::cancelButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___cancelButton_21;
	// UnityEngine.UI.Button Rewired.UI.ControlMapper.InputBehaviorWindow::defaultButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___defaultButton_22;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.InputBehaviorWindow::doneButtonLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___doneButtonLabel_23;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.InputBehaviorWindow::cancelButtonLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___cancelButtonLabel_24;
	// UnityEngine.UI.Text Rewired.UI.ControlMapper.InputBehaviorWindow::defaultButtonLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___defaultButtonLabel_25;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.InputBehaviorWindow::uiControlSetPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___uiControlSetPrefab_26;
	// UnityEngine.GameObject Rewired.UI.ControlMapper.InputBehaviorWindow::uiSliderControlPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___uiSliderControlPrefab_27;
	// System.Collections.Generic.List`1<Rewired.UI.ControlMapper.InputBehaviorWindow_InputBehaviorInfo> Rewired.UI.ControlMapper.InputBehaviorWindow::inputBehaviorInfo
	List_1_t31CA8E22671BC2966AA472F0A47F63DF3CEAE904 * ___inputBehaviorInfo_28;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Int32>> Rewired.UI.ControlMapper.InputBehaviorWindow::buttonCallbacks
	Dictionary_2_t030E2D78EA98876F80F098D4E3C0AA3B18AC72EE * ___buttonCallbacks_29;
	// System.Int32 Rewired.UI.ControlMapper.InputBehaviorWindow::playerId
	int32_t ___playerId_30;

public:
	inline static int32_t get_offset_of_spawnTransform_19() { return static_cast<int32_t>(offsetof(InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A, ___spawnTransform_19)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_spawnTransform_19() const { return ___spawnTransform_19; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_spawnTransform_19() { return &___spawnTransform_19; }
	inline void set_spawnTransform_19(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___spawnTransform_19 = value;
		Il2CppCodeGenWriteBarrier((&___spawnTransform_19), value);
	}

	inline static int32_t get_offset_of_doneButton_20() { return static_cast<int32_t>(offsetof(InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A, ___doneButton_20)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_doneButton_20() const { return ___doneButton_20; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_doneButton_20() { return &___doneButton_20; }
	inline void set_doneButton_20(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___doneButton_20 = value;
		Il2CppCodeGenWriteBarrier((&___doneButton_20), value);
	}

	inline static int32_t get_offset_of_cancelButton_21() { return static_cast<int32_t>(offsetof(InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A, ___cancelButton_21)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_cancelButton_21() const { return ___cancelButton_21; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_cancelButton_21() { return &___cancelButton_21; }
	inline void set_cancelButton_21(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___cancelButton_21 = value;
		Il2CppCodeGenWriteBarrier((&___cancelButton_21), value);
	}

	inline static int32_t get_offset_of_defaultButton_22() { return static_cast<int32_t>(offsetof(InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A, ___defaultButton_22)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_defaultButton_22() const { return ___defaultButton_22; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_defaultButton_22() { return &___defaultButton_22; }
	inline void set_defaultButton_22(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___defaultButton_22 = value;
		Il2CppCodeGenWriteBarrier((&___defaultButton_22), value);
	}

	inline static int32_t get_offset_of_doneButtonLabel_23() { return static_cast<int32_t>(offsetof(InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A, ___doneButtonLabel_23)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_doneButtonLabel_23() const { return ___doneButtonLabel_23; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_doneButtonLabel_23() { return &___doneButtonLabel_23; }
	inline void set_doneButtonLabel_23(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___doneButtonLabel_23 = value;
		Il2CppCodeGenWriteBarrier((&___doneButtonLabel_23), value);
	}

	inline static int32_t get_offset_of_cancelButtonLabel_24() { return static_cast<int32_t>(offsetof(InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A, ___cancelButtonLabel_24)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_cancelButtonLabel_24() const { return ___cancelButtonLabel_24; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_cancelButtonLabel_24() { return &___cancelButtonLabel_24; }
	inline void set_cancelButtonLabel_24(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___cancelButtonLabel_24 = value;
		Il2CppCodeGenWriteBarrier((&___cancelButtonLabel_24), value);
	}

	inline static int32_t get_offset_of_defaultButtonLabel_25() { return static_cast<int32_t>(offsetof(InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A, ___defaultButtonLabel_25)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_defaultButtonLabel_25() const { return ___defaultButtonLabel_25; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_defaultButtonLabel_25() { return &___defaultButtonLabel_25; }
	inline void set_defaultButtonLabel_25(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___defaultButtonLabel_25 = value;
		Il2CppCodeGenWriteBarrier((&___defaultButtonLabel_25), value);
	}

	inline static int32_t get_offset_of_uiControlSetPrefab_26() { return static_cast<int32_t>(offsetof(InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A, ___uiControlSetPrefab_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_uiControlSetPrefab_26() const { return ___uiControlSetPrefab_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_uiControlSetPrefab_26() { return &___uiControlSetPrefab_26; }
	inline void set_uiControlSetPrefab_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___uiControlSetPrefab_26 = value;
		Il2CppCodeGenWriteBarrier((&___uiControlSetPrefab_26), value);
	}

	inline static int32_t get_offset_of_uiSliderControlPrefab_27() { return static_cast<int32_t>(offsetof(InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A, ___uiSliderControlPrefab_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_uiSliderControlPrefab_27() const { return ___uiSliderControlPrefab_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_uiSliderControlPrefab_27() { return &___uiSliderControlPrefab_27; }
	inline void set_uiSliderControlPrefab_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___uiSliderControlPrefab_27 = value;
		Il2CppCodeGenWriteBarrier((&___uiSliderControlPrefab_27), value);
	}

	inline static int32_t get_offset_of_inputBehaviorInfo_28() { return static_cast<int32_t>(offsetof(InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A, ___inputBehaviorInfo_28)); }
	inline List_1_t31CA8E22671BC2966AA472F0A47F63DF3CEAE904 * get_inputBehaviorInfo_28() const { return ___inputBehaviorInfo_28; }
	inline List_1_t31CA8E22671BC2966AA472F0A47F63DF3CEAE904 ** get_address_of_inputBehaviorInfo_28() { return &___inputBehaviorInfo_28; }
	inline void set_inputBehaviorInfo_28(List_1_t31CA8E22671BC2966AA472F0A47F63DF3CEAE904 * value)
	{
		___inputBehaviorInfo_28 = value;
		Il2CppCodeGenWriteBarrier((&___inputBehaviorInfo_28), value);
	}

	inline static int32_t get_offset_of_buttonCallbacks_29() { return static_cast<int32_t>(offsetof(InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A, ___buttonCallbacks_29)); }
	inline Dictionary_2_t030E2D78EA98876F80F098D4E3C0AA3B18AC72EE * get_buttonCallbacks_29() const { return ___buttonCallbacks_29; }
	inline Dictionary_2_t030E2D78EA98876F80F098D4E3C0AA3B18AC72EE ** get_address_of_buttonCallbacks_29() { return &___buttonCallbacks_29; }
	inline void set_buttonCallbacks_29(Dictionary_2_t030E2D78EA98876F80F098D4E3C0AA3B18AC72EE * value)
	{
		___buttonCallbacks_29 = value;
		Il2CppCodeGenWriteBarrier((&___buttonCallbacks_29), value);
	}

	inline static int32_t get_offset_of_playerId_30() { return static_cast<int32_t>(offsetof(InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A, ___playerId_30)); }
	inline int32_t get_playerId_30() const { return ___playerId_30; }
	inline int32_t* get_address_of_playerId_30() { return &___playerId_30; }
	inline void set_playerId_30(int32_t value)
	{
		___playerId_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBEHAVIORWINDOW_T1837B5E28A6EB46B7037D62A71003FD6CEA2369A_H
#ifndef INPUTFIELDINFO_T0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5_H
#define INPUTFIELDINFO_T0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.InputFieldInfo
struct  InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5  : public UIElementInfo_t2DBC68E382F8428D528D5AF6D14EB38F998B3ECA
{
public:
	// System.Int32 Rewired.UI.ControlMapper.InputFieldInfo::<actionId>k__BackingField
	int32_t ___U3CactionIdU3Ek__BackingField_8;
	// Rewired.AxisRange Rewired.UI.ControlMapper.InputFieldInfo::<axisRange>k__BackingField
	int32_t ___U3CaxisRangeU3Ek__BackingField_9;
	// System.Int32 Rewired.UI.ControlMapper.InputFieldInfo::<actionElementMapId>k__BackingField
	int32_t ___U3CactionElementMapIdU3Ek__BackingField_10;
	// Rewired.ControllerType Rewired.UI.ControlMapper.InputFieldInfo::<controllerType>k__BackingField
	int32_t ___U3CcontrollerTypeU3Ek__BackingField_11;
	// System.Int32 Rewired.UI.ControlMapper.InputFieldInfo::<controllerId>k__BackingField
	int32_t ___U3CcontrollerIdU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CactionIdU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5, ___U3CactionIdU3Ek__BackingField_8)); }
	inline int32_t get_U3CactionIdU3Ek__BackingField_8() const { return ___U3CactionIdU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CactionIdU3Ek__BackingField_8() { return &___U3CactionIdU3Ek__BackingField_8; }
	inline void set_U3CactionIdU3Ek__BackingField_8(int32_t value)
	{
		___U3CactionIdU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CaxisRangeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5, ___U3CaxisRangeU3Ek__BackingField_9)); }
	inline int32_t get_U3CaxisRangeU3Ek__BackingField_9() const { return ___U3CaxisRangeU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CaxisRangeU3Ek__BackingField_9() { return &___U3CaxisRangeU3Ek__BackingField_9; }
	inline void set_U3CaxisRangeU3Ek__BackingField_9(int32_t value)
	{
		___U3CaxisRangeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CactionElementMapIdU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5, ___U3CactionElementMapIdU3Ek__BackingField_10)); }
	inline int32_t get_U3CactionElementMapIdU3Ek__BackingField_10() const { return ___U3CactionElementMapIdU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CactionElementMapIdU3Ek__BackingField_10() { return &___U3CactionElementMapIdU3Ek__BackingField_10; }
	inline void set_U3CactionElementMapIdU3Ek__BackingField_10(int32_t value)
	{
		___U3CactionElementMapIdU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CcontrollerTypeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5, ___U3CcontrollerTypeU3Ek__BackingField_11)); }
	inline int32_t get_U3CcontrollerTypeU3Ek__BackingField_11() const { return ___U3CcontrollerTypeU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CcontrollerTypeU3Ek__BackingField_11() { return &___U3CcontrollerTypeU3Ek__BackingField_11; }
	inline void set_U3CcontrollerTypeU3Ek__BackingField_11(int32_t value)
	{
		___U3CcontrollerTypeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CcontrollerIdU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5, ___U3CcontrollerIdU3Ek__BackingField_12)); }
	inline int32_t get_U3CcontrollerIdU3Ek__BackingField_12() const { return ___U3CcontrollerIdU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CcontrollerIdU3Ek__BackingField_12() { return &___U3CcontrollerIdU3Ek__BackingField_12; }
	inline void set_U3CcontrollerIdU3Ek__BackingField_12(int32_t value)
	{
		___U3CcontrollerIdU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFIELDINFO_T0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5_H
#ifndef BASEINPUTMODULE_T904837FCFA79B6C3CED862FF85C9C5F8D6F32939_H
#define BASEINPUTMODULE_T904837FCFA79B6C3CED862FF85C9C5F8D6F32939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * ___m_RaycastResultCache_4;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 * ___m_AxisEventData_5;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___m_EventSystem_6;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * ___m_BaseEventData_7;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * ___m_InputOverride_8;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * ___m_DefaultInput_9;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_RaycastResultCache_4)); }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * get_m_RaycastResultCache_4() const { return ___m_RaycastResultCache_4; }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 ** get_address_of_m_RaycastResultCache_4() { return &___m_RaycastResultCache_4; }
	inline void set_m_RaycastResultCache_4(List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * value)
	{
		___m_RaycastResultCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_4), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_AxisEventData_5)); }
	inline AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 * get_m_AxisEventData_5() const { return ___m_AxisEventData_5; }
	inline AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 ** get_address_of_m_AxisEventData_5() { return &___m_AxisEventData_5; }
	inline void set_m_AxisEventData_5(AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 * value)
	{
		___m_AxisEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_5), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_EventSystem_6)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_m_EventSystem_6() const { return ___m_EventSystem_6; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_m_EventSystem_6() { return &___m_EventSystem_6; }
	inline void set_m_EventSystem_6(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___m_EventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_6), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_BaseEventData_7)); }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * get_m_BaseEventData_7() const { return ___m_BaseEventData_7; }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 ** get_address_of_m_BaseEventData_7() { return &___m_BaseEventData_7; }
	inline void set_m_BaseEventData_7(BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * value)
	{
		___m_BaseEventData_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_7), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_8() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_InputOverride_8)); }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * get_m_InputOverride_8() const { return ___m_InputOverride_8; }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 ** get_address_of_m_InputOverride_8() { return &___m_InputOverride_8; }
	inline void set_m_InputOverride_8(BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * value)
	{
		___m_InputOverride_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_8), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_9() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_DefaultInput_9)); }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * get_m_DefaultInput_9() const { return ___m_DefaultInput_9; }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 ** get_address_of_m_DefaultInput_9() { return &___m_DefaultInput_9; }
	inline void set_m_DefaultInput_9(BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * value)
	{
		___m_DefaultInput_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T904837FCFA79B6C3CED862FF85C9C5F8D6F32939_H
#ifndef CANVASSCALER_T304BA6F47EDB7402EBA405DD36CA7D6ADF723564_H
#define CANVASSCALER_T304BA6F47EDB7402EBA405DD36CA7D6ADF723564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler
struct  CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.CanvasScaler_ScaleMode UnityEngine.UI.CanvasScaler::m_UiScaleMode
	int32_t ___m_UiScaleMode_4;
	// System.Single UnityEngine.UI.CanvasScaler::m_ReferencePixelsPerUnit
	float ___m_ReferencePixelsPerUnit_5;
	// System.Single UnityEngine.UI.CanvasScaler::m_ScaleFactor
	float ___m_ScaleFactor_6;
	// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::m_ReferenceResolution
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_ReferenceResolution_7;
	// UnityEngine.UI.CanvasScaler_ScreenMatchMode UnityEngine.UI.CanvasScaler::m_ScreenMatchMode
	int32_t ___m_ScreenMatchMode_8;
	// System.Single UnityEngine.UI.CanvasScaler::m_MatchWidthOrHeight
	float ___m_MatchWidthOrHeight_9;
	// UnityEngine.UI.CanvasScaler_Unit UnityEngine.UI.CanvasScaler::m_PhysicalUnit
	int32_t ___m_PhysicalUnit_11;
	// System.Single UnityEngine.UI.CanvasScaler::m_FallbackScreenDPI
	float ___m_FallbackScreenDPI_12;
	// System.Single UnityEngine.UI.CanvasScaler::m_DefaultSpriteDPI
	float ___m_DefaultSpriteDPI_13;
	// System.Single UnityEngine.UI.CanvasScaler::m_DynamicPixelsPerUnit
	float ___m_DynamicPixelsPerUnit_14;
	// UnityEngine.Canvas UnityEngine.UI.CanvasScaler::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_15;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevScaleFactor
	float ___m_PrevScaleFactor_16;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevReferencePixelsPerUnit
	float ___m_PrevReferencePixelsPerUnit_17;

public:
	inline static int32_t get_offset_of_m_UiScaleMode_4() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_UiScaleMode_4)); }
	inline int32_t get_m_UiScaleMode_4() const { return ___m_UiScaleMode_4; }
	inline int32_t* get_address_of_m_UiScaleMode_4() { return &___m_UiScaleMode_4; }
	inline void set_m_UiScaleMode_4(int32_t value)
	{
		___m_UiScaleMode_4 = value;
	}

	inline static int32_t get_offset_of_m_ReferencePixelsPerUnit_5() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_ReferencePixelsPerUnit_5)); }
	inline float get_m_ReferencePixelsPerUnit_5() const { return ___m_ReferencePixelsPerUnit_5; }
	inline float* get_address_of_m_ReferencePixelsPerUnit_5() { return &___m_ReferencePixelsPerUnit_5; }
	inline void set_m_ReferencePixelsPerUnit_5(float value)
	{
		___m_ReferencePixelsPerUnit_5 = value;
	}

	inline static int32_t get_offset_of_m_ScaleFactor_6() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_ScaleFactor_6)); }
	inline float get_m_ScaleFactor_6() const { return ___m_ScaleFactor_6; }
	inline float* get_address_of_m_ScaleFactor_6() { return &___m_ScaleFactor_6; }
	inline void set_m_ScaleFactor_6(float value)
	{
		___m_ScaleFactor_6 = value;
	}

	inline static int32_t get_offset_of_m_ReferenceResolution_7() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_ReferenceResolution_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_ReferenceResolution_7() const { return ___m_ReferenceResolution_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_ReferenceResolution_7() { return &___m_ReferenceResolution_7; }
	inline void set_m_ReferenceResolution_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_ReferenceResolution_7 = value;
	}

	inline static int32_t get_offset_of_m_ScreenMatchMode_8() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_ScreenMatchMode_8)); }
	inline int32_t get_m_ScreenMatchMode_8() const { return ___m_ScreenMatchMode_8; }
	inline int32_t* get_address_of_m_ScreenMatchMode_8() { return &___m_ScreenMatchMode_8; }
	inline void set_m_ScreenMatchMode_8(int32_t value)
	{
		___m_ScreenMatchMode_8 = value;
	}

	inline static int32_t get_offset_of_m_MatchWidthOrHeight_9() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_MatchWidthOrHeight_9)); }
	inline float get_m_MatchWidthOrHeight_9() const { return ___m_MatchWidthOrHeight_9; }
	inline float* get_address_of_m_MatchWidthOrHeight_9() { return &___m_MatchWidthOrHeight_9; }
	inline void set_m_MatchWidthOrHeight_9(float value)
	{
		___m_MatchWidthOrHeight_9 = value;
	}

	inline static int32_t get_offset_of_m_PhysicalUnit_11() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_PhysicalUnit_11)); }
	inline int32_t get_m_PhysicalUnit_11() const { return ___m_PhysicalUnit_11; }
	inline int32_t* get_address_of_m_PhysicalUnit_11() { return &___m_PhysicalUnit_11; }
	inline void set_m_PhysicalUnit_11(int32_t value)
	{
		___m_PhysicalUnit_11 = value;
	}

	inline static int32_t get_offset_of_m_FallbackScreenDPI_12() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_FallbackScreenDPI_12)); }
	inline float get_m_FallbackScreenDPI_12() const { return ___m_FallbackScreenDPI_12; }
	inline float* get_address_of_m_FallbackScreenDPI_12() { return &___m_FallbackScreenDPI_12; }
	inline void set_m_FallbackScreenDPI_12(float value)
	{
		___m_FallbackScreenDPI_12 = value;
	}

	inline static int32_t get_offset_of_m_DefaultSpriteDPI_13() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_DefaultSpriteDPI_13)); }
	inline float get_m_DefaultSpriteDPI_13() const { return ___m_DefaultSpriteDPI_13; }
	inline float* get_address_of_m_DefaultSpriteDPI_13() { return &___m_DefaultSpriteDPI_13; }
	inline void set_m_DefaultSpriteDPI_13(float value)
	{
		___m_DefaultSpriteDPI_13 = value;
	}

	inline static int32_t get_offset_of_m_DynamicPixelsPerUnit_14() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_DynamicPixelsPerUnit_14)); }
	inline float get_m_DynamicPixelsPerUnit_14() const { return ___m_DynamicPixelsPerUnit_14; }
	inline float* get_address_of_m_DynamicPixelsPerUnit_14() { return &___m_DynamicPixelsPerUnit_14; }
	inline void set_m_DynamicPixelsPerUnit_14(float value)
	{
		___m_DynamicPixelsPerUnit_14 = value;
	}

	inline static int32_t get_offset_of_m_Canvas_15() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_Canvas_15)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_15() const { return ___m_Canvas_15; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_15() { return &___m_Canvas_15; }
	inline void set_m_Canvas_15(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_15), value);
	}

	inline static int32_t get_offset_of_m_PrevScaleFactor_16() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_PrevScaleFactor_16)); }
	inline float get_m_PrevScaleFactor_16() const { return ___m_PrevScaleFactor_16; }
	inline float* get_address_of_m_PrevScaleFactor_16() { return &___m_PrevScaleFactor_16; }
	inline void set_m_PrevScaleFactor_16(float value)
	{
		___m_PrevScaleFactor_16 = value;
	}

	inline static int32_t get_offset_of_m_PrevReferencePixelsPerUnit_17() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_PrevReferencePixelsPerUnit_17)); }
	inline float get_m_PrevReferencePixelsPerUnit_17() const { return ___m_PrevReferencePixelsPerUnit_17; }
	inline float* get_address_of_m_PrevReferencePixelsPerUnit_17() { return &___m_PrevReferencePixelsPerUnit_17; }
	inline void set_m_PrevReferencePixelsPerUnit_17(float value)
	{
		___m_PrevReferencePixelsPerUnit_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASSCALER_T304BA6F47EDB7402EBA405DD36CA7D6ADF723564_H
#ifndef SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#define SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  ___m_Navigation_5;
	// UnityEngine.UI.Selectable_Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable_SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Navigation_5)); }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Colors_7)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_SpriteState_8)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_TargetGraphic_11)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CanvasGroupCache_17)); }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_List_4)); }
	inline List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#ifndef REWIREDPOINTERINPUTMODULE_T8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A_H
#define REWIREDPOINTERINPUTMODULE_T8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Integration.UnityUI.RewiredPointerInputModule
struct  RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A  : public BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939
{
public:
	// System.Collections.Generic.List`1<Rewired.UI.IMouseInputSource> Rewired.Integration.UnityUI.RewiredPointerInputModule::m_MouseInputSourcesList
	List_1_tA87F5F213628BE50512E7F7B610C2741B76D220F * ___m_MouseInputSourcesList_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.Int32,Rewired.Integration.UnityUI.PlayerPointerEventData>[]> Rewired.Integration.UnityUI.RewiredPointerInputModule::m_PlayerPointerData
	Dictionary_2_t7CE50FF82CD019CCD55558B77157FD9686A4CA0B * ___m_PlayerPointerData_18;
	// Rewired.UI.ITouchInputSource Rewired.Integration.UnityUI.RewiredPointerInputModule::m_UserDefaultTouchInputSource
	RuntimeObject* ___m_UserDefaultTouchInputSource_19;
	// Rewired.Integration.UnityUI.RewiredPointerInputModule_UnityInputSource Rewired.Integration.UnityUI.RewiredPointerInputModule::__m_DefaultInputSource
	UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128 * _____m_DefaultInputSource_20;
	// Rewired.Integration.UnityUI.RewiredPointerInputModule_MouseState Rewired.Integration.UnityUI.RewiredPointerInputModule::m_MouseState
	MouseState_tC2B3A40B96175F6EE1FEBD1693F38B717BABFD5C * ___m_MouseState_21;

public:
	inline static int32_t get_offset_of_m_MouseInputSourcesList_17() { return static_cast<int32_t>(offsetof(RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A, ___m_MouseInputSourcesList_17)); }
	inline List_1_tA87F5F213628BE50512E7F7B610C2741B76D220F * get_m_MouseInputSourcesList_17() const { return ___m_MouseInputSourcesList_17; }
	inline List_1_tA87F5F213628BE50512E7F7B610C2741B76D220F ** get_address_of_m_MouseInputSourcesList_17() { return &___m_MouseInputSourcesList_17; }
	inline void set_m_MouseInputSourcesList_17(List_1_tA87F5F213628BE50512E7F7B610C2741B76D220F * value)
	{
		___m_MouseInputSourcesList_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseInputSourcesList_17), value);
	}

	inline static int32_t get_offset_of_m_PlayerPointerData_18() { return static_cast<int32_t>(offsetof(RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A, ___m_PlayerPointerData_18)); }
	inline Dictionary_2_t7CE50FF82CD019CCD55558B77157FD9686A4CA0B * get_m_PlayerPointerData_18() const { return ___m_PlayerPointerData_18; }
	inline Dictionary_2_t7CE50FF82CD019CCD55558B77157FD9686A4CA0B ** get_address_of_m_PlayerPointerData_18() { return &___m_PlayerPointerData_18; }
	inline void set_m_PlayerPointerData_18(Dictionary_2_t7CE50FF82CD019CCD55558B77157FD9686A4CA0B * value)
	{
		___m_PlayerPointerData_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerPointerData_18), value);
	}

	inline static int32_t get_offset_of_m_UserDefaultTouchInputSource_19() { return static_cast<int32_t>(offsetof(RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A, ___m_UserDefaultTouchInputSource_19)); }
	inline RuntimeObject* get_m_UserDefaultTouchInputSource_19() const { return ___m_UserDefaultTouchInputSource_19; }
	inline RuntimeObject** get_address_of_m_UserDefaultTouchInputSource_19() { return &___m_UserDefaultTouchInputSource_19; }
	inline void set_m_UserDefaultTouchInputSource_19(RuntimeObject* value)
	{
		___m_UserDefaultTouchInputSource_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_UserDefaultTouchInputSource_19), value);
	}

	inline static int32_t get_offset_of___m_DefaultInputSource_20() { return static_cast<int32_t>(offsetof(RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A, _____m_DefaultInputSource_20)); }
	inline UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128 * get___m_DefaultInputSource_20() const { return _____m_DefaultInputSource_20; }
	inline UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128 ** get_address_of___m_DefaultInputSource_20() { return &_____m_DefaultInputSource_20; }
	inline void set___m_DefaultInputSource_20(UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128 * value)
	{
		_____m_DefaultInputSource_20 = value;
		Il2CppCodeGenWriteBarrier((&_____m_DefaultInputSource_20), value);
	}

	inline static int32_t get_offset_of_m_MouseState_21() { return static_cast<int32_t>(offsetof(RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A, ___m_MouseState_21)); }
	inline MouseState_tC2B3A40B96175F6EE1FEBD1693F38B717BABFD5C * get_m_MouseState_21() const { return ___m_MouseState_21; }
	inline MouseState_tC2B3A40B96175F6EE1FEBD1693F38B717BABFD5C ** get_address_of_m_MouseState_21() { return &___m_MouseState_21; }
	inline void set_m_MouseState_21(MouseState_tC2B3A40B96175F6EE1FEBD1693F38B717BABFD5C * value)
	{
		___m_MouseState_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWIREDPOINTERINPUTMODULE_T8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A_H
#ifndef CANVASSCALEREXT_T7474B4D2A98F6C8EB3964E748B7F093961CB7362_H
#define CANVASSCALEREXT_T7474B4D2A98F6C8EB3964E748B7F093961CB7362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.CanvasScalerExt
struct  CanvasScalerExt_t7474B4D2A98F6C8EB3964E748B7F093961CB7362  : public CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASSCALEREXT_T7474B4D2A98F6C8EB3964E748B7F093961CB7362_H
#ifndef BUTTON_T1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_H
#define BUTTON_T1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button
struct  Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.UI.Button_ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * ___m_OnClick_18;

public:
	inline static int32_t get_offset_of_m_OnClick_18() { return static_cast<int32_t>(offsetof(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B, ___m_OnClick_18)); }
	inline ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * get_m_OnClick_18() const { return ___m_OnClick_18; }
	inline ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB ** get_address_of_m_OnClick_18() { return &___m_OnClick_18; }
	inline void set_m_OnClick_18(ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * value)
	{
		___m_OnClick_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnClick_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_H
#ifndef SLIDER_T0654A41304B5CE7074CA86F4E66CB681D0D52C09_H
#define SLIDER_T0654A41304B5CE7074CA86F4E66CB681D0D52C09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider
struct  Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_FillRect_18;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_HandleRect_19;
	// UnityEngine.UI.Slider_Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_20;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_21;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_22;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_23;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_24;
	// UnityEngine.UI.Slider_SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C * ___m_OnValueChanged_25;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_FillImage_26;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_FillTransform_27;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_FillContainerRect_28;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_HandleTransform_29;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_HandleContainerRect_30;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Offset_31;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_32;

public:
	inline static int32_t get_offset_of_m_FillRect_18() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_FillRect_18)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_FillRect_18() const { return ___m_FillRect_18; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_FillRect_18() { return &___m_FillRect_18; }
	inline void set_m_FillRect_18(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_FillRect_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillRect_18), value);
	}

	inline static int32_t get_offset_of_m_HandleRect_19() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_HandleRect_19)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_HandleRect_19() const { return ___m_HandleRect_19; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_HandleRect_19() { return &___m_HandleRect_19; }
	inline void set_m_HandleRect_19(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_HandleRect_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_19), value);
	}

	inline static int32_t get_offset_of_m_Direction_20() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_Direction_20)); }
	inline int32_t get_m_Direction_20() const { return ___m_Direction_20; }
	inline int32_t* get_address_of_m_Direction_20() { return &___m_Direction_20; }
	inline void set_m_Direction_20(int32_t value)
	{
		___m_Direction_20 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_21() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_MinValue_21)); }
	inline float get_m_MinValue_21() const { return ___m_MinValue_21; }
	inline float* get_address_of_m_MinValue_21() { return &___m_MinValue_21; }
	inline void set_m_MinValue_21(float value)
	{
		___m_MinValue_21 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_22() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_MaxValue_22)); }
	inline float get_m_MaxValue_22() const { return ___m_MaxValue_22; }
	inline float* get_address_of_m_MaxValue_22() { return &___m_MaxValue_22; }
	inline void set_m_MaxValue_22(float value)
	{
		___m_MaxValue_22 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_23() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_WholeNumbers_23)); }
	inline bool get_m_WholeNumbers_23() const { return ___m_WholeNumbers_23; }
	inline bool* get_address_of_m_WholeNumbers_23() { return &___m_WholeNumbers_23; }
	inline void set_m_WholeNumbers_23(bool value)
	{
		___m_WholeNumbers_23 = value;
	}

	inline static int32_t get_offset_of_m_Value_24() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_Value_24)); }
	inline float get_m_Value_24() const { return ___m_Value_24; }
	inline float* get_address_of_m_Value_24() { return &___m_Value_24; }
	inline void set_m_Value_24(float value)
	{
		___m_Value_24 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_25() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_OnValueChanged_25)); }
	inline SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C * get_m_OnValueChanged_25() const { return ___m_OnValueChanged_25; }
	inline SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C ** get_address_of_m_OnValueChanged_25() { return &___m_OnValueChanged_25; }
	inline void set_m_OnValueChanged_25(SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C * value)
	{
		___m_OnValueChanged_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_25), value);
	}

	inline static int32_t get_offset_of_m_FillImage_26() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_FillImage_26)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_FillImage_26() const { return ___m_FillImage_26; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_FillImage_26() { return &___m_FillImage_26; }
	inline void set_m_FillImage_26(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_FillImage_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillImage_26), value);
	}

	inline static int32_t get_offset_of_m_FillTransform_27() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_FillTransform_27)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_FillTransform_27() const { return ___m_FillTransform_27; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_FillTransform_27() { return &___m_FillTransform_27; }
	inline void set_m_FillTransform_27(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_FillTransform_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillTransform_27), value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_28() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_FillContainerRect_28)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_FillContainerRect_28() const { return ___m_FillContainerRect_28; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_FillContainerRect_28() { return &___m_FillContainerRect_28; }
	inline void set_m_FillContainerRect_28(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_FillContainerRect_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillContainerRect_28), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_29() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_HandleTransform_29)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_HandleTransform_29() const { return ___m_HandleTransform_29; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_HandleTransform_29() { return &___m_HandleTransform_29; }
	inline void set_m_HandleTransform_29(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_HandleTransform_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_29), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_30() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_HandleContainerRect_30)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_HandleContainerRect_30() const { return ___m_HandleContainerRect_30; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_HandleContainerRect_30() { return &___m_HandleContainerRect_30; }
	inline void set_m_HandleContainerRect_30(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_HandleContainerRect_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_30), value);
	}

	inline static int32_t get_offset_of_m_Offset_31() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_Offset_31)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Offset_31() const { return ___m_Offset_31; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Offset_31() { return &___m_Offset_31; }
	inline void set_m_Offset_31(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Offset_31 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_32() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_Tracker_32)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_32() const { return ___m_Tracker_32; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_32() { return &___m_Tracker_32; }
	inline void set_m_Tracker_32(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDER_T0654A41304B5CE7074CA86F4E66CB681D0D52C09_H
#ifndef TOGGLE_T9ADD572046F831945ED0E48A01B50FEA1CA52106_H
#define TOGGLE_T9ADD572046F831945ED0E48A01B50FEA1CA52106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle
struct  Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.UI.Toggle_ToggleTransition UnityEngine.UI.Toggle::toggleTransition
	int32_t ___toggleTransition_18;
	// UnityEngine.UI.Graphic UnityEngine.UI.Toggle::graphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___graphic_19;
	// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::m_Group
	ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786 * ___m_Group_20;
	// UnityEngine.UI.Toggle_ToggleEvent UnityEngine.UI.Toggle::onValueChanged
	ToggleEvent_t50D925F8E220FB47DA738411CEF9C57FF7E1DC43 * ___onValueChanged_21;
	// System.Boolean UnityEngine.UI.Toggle::m_IsOn
	bool ___m_IsOn_22;

public:
	inline static int32_t get_offset_of_toggleTransition_18() { return static_cast<int32_t>(offsetof(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106, ___toggleTransition_18)); }
	inline int32_t get_toggleTransition_18() const { return ___toggleTransition_18; }
	inline int32_t* get_address_of_toggleTransition_18() { return &___toggleTransition_18; }
	inline void set_toggleTransition_18(int32_t value)
	{
		___toggleTransition_18 = value;
	}

	inline static int32_t get_offset_of_graphic_19() { return static_cast<int32_t>(offsetof(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106, ___graphic_19)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_graphic_19() const { return ___graphic_19; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_graphic_19() { return &___graphic_19; }
	inline void set_graphic_19(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___graphic_19 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_19), value);
	}

	inline static int32_t get_offset_of_m_Group_20() { return static_cast<int32_t>(offsetof(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106, ___m_Group_20)); }
	inline ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786 * get_m_Group_20() const { return ___m_Group_20; }
	inline ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786 ** get_address_of_m_Group_20() { return &___m_Group_20; }
	inline void set_m_Group_20(ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786 * value)
	{
		___m_Group_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_Group_20), value);
	}

	inline static int32_t get_offset_of_onValueChanged_21() { return static_cast<int32_t>(offsetof(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106, ___onValueChanged_21)); }
	inline ToggleEvent_t50D925F8E220FB47DA738411CEF9C57FF7E1DC43 * get_onValueChanged_21() const { return ___onValueChanged_21; }
	inline ToggleEvent_t50D925F8E220FB47DA738411CEF9C57FF7E1DC43 ** get_address_of_onValueChanged_21() { return &___onValueChanged_21; }
	inline void set_onValueChanged_21(ToggleEvent_t50D925F8E220FB47DA738411CEF9C57FF7E1DC43 * value)
	{
		___onValueChanged_21 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_21), value);
	}

	inline static int32_t get_offset_of_m_IsOn_22() { return static_cast<int32_t>(offsetof(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106, ___m_IsOn_22)); }
	inline bool get_m_IsOn_22() const { return ___m_IsOn_22; }
	inline bool* get_address_of_m_IsOn_22() { return &___m_IsOn_22; }
	inline void set_m_IsOn_22(bool value)
	{
		___m_IsOn_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLE_T9ADD572046F831945ED0E48A01B50FEA1CA52106_H
#ifndef REWIREDSTANDALONEINPUTMODULE_T26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7_H
#define REWIREDSTANDALONEINPUTMODULE_T26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Integration.UnityUI.RewiredStandaloneInputModule
struct  RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7  : public RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A
{
public:
	// Rewired.InputManager_Base Rewired.Integration.UnityUI.RewiredStandaloneInputModule::rewiredInputManager
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * ___rewiredInputManager_26;
	// System.Boolean Rewired.Integration.UnityUI.RewiredStandaloneInputModule::useAllRewiredGamePlayers
	bool ___useAllRewiredGamePlayers_27;
	// System.Boolean Rewired.Integration.UnityUI.RewiredStandaloneInputModule::useRewiredSystemPlayer
	bool ___useRewiredSystemPlayer_28;
	// System.Int32[] Rewired.Integration.UnityUI.RewiredStandaloneInputModule::rewiredPlayerIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___rewiredPlayerIds_29;
	// System.Boolean Rewired.Integration.UnityUI.RewiredStandaloneInputModule::usePlayingPlayersOnly
	bool ___usePlayingPlayersOnly_30;
	// System.Collections.Generic.List`1<Rewired.Components.PlayerMouse> Rewired.Integration.UnityUI.RewiredStandaloneInputModule::playerMice
	List_1_t08E7DE11ECA859CD34DB357C00B14BE87AF37AE9 * ___playerMice_31;
	// System.Boolean Rewired.Integration.UnityUI.RewiredStandaloneInputModule::moveOneElementPerAxisPress
	bool ___moveOneElementPerAxisPress_32;
	// System.Boolean Rewired.Integration.UnityUI.RewiredStandaloneInputModule::setActionsById
	bool ___setActionsById_33;
	// System.Int32 Rewired.Integration.UnityUI.RewiredStandaloneInputModule::horizontalActionId
	int32_t ___horizontalActionId_34;
	// System.Int32 Rewired.Integration.UnityUI.RewiredStandaloneInputModule::verticalActionId
	int32_t ___verticalActionId_35;
	// System.Int32 Rewired.Integration.UnityUI.RewiredStandaloneInputModule::submitActionId
	int32_t ___submitActionId_36;
	// System.Int32 Rewired.Integration.UnityUI.RewiredStandaloneInputModule::cancelActionId
	int32_t ___cancelActionId_37;
	// System.String Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_HorizontalAxis
	String_t* ___m_HorizontalAxis_38;
	// System.String Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_VerticalAxis
	String_t* ___m_VerticalAxis_39;
	// System.String Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_SubmitButton
	String_t* ___m_SubmitButton_40;
	// System.String Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_CancelButton
	String_t* ___m_CancelButton_41;
	// System.Single Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_InputActionsPerSecond
	float ___m_InputActionsPerSecond_42;
	// System.Single Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_RepeatDelay
	float ___m_RepeatDelay_43;
	// System.Boolean Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_allowMouseInput
	bool ___m_allowMouseInput_44;
	// System.Boolean Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_allowMouseInputIfTouchSupported
	bool ___m_allowMouseInputIfTouchSupported_45;
	// System.Boolean Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_allowTouchInput
	bool ___m_allowTouchInput_46;
	// System.Boolean Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_47;
	// System.Int32[] Rewired.Integration.UnityUI.RewiredStandaloneInputModule::playerIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___playerIds_48;
	// System.Boolean Rewired.Integration.UnityUI.RewiredStandaloneInputModule::recompiling
	bool ___recompiling_49;
	// System.Boolean Rewired.Integration.UnityUI.RewiredStandaloneInputModule::isTouchSupported
	bool ___isTouchSupported_50;
	// System.Single Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_PrevActionTime
	float ___m_PrevActionTime_51;
	// UnityEngine.Vector2 Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_LastMoveVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_LastMoveVector_52;
	// System.Int32 Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_ConsecutiveMoveCount
	int32_t ___m_ConsecutiveMoveCount_53;
	// System.Boolean Rewired.Integration.UnityUI.RewiredStandaloneInputModule::m_HasFocus
	bool ___m_HasFocus_54;

public:
	inline static int32_t get_offset_of_rewiredInputManager_26() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___rewiredInputManager_26)); }
	inline InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * get_rewiredInputManager_26() const { return ___rewiredInputManager_26; }
	inline InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 ** get_address_of_rewiredInputManager_26() { return &___rewiredInputManager_26; }
	inline void set_rewiredInputManager_26(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * value)
	{
		___rewiredInputManager_26 = value;
		Il2CppCodeGenWriteBarrier((&___rewiredInputManager_26), value);
	}

	inline static int32_t get_offset_of_useAllRewiredGamePlayers_27() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___useAllRewiredGamePlayers_27)); }
	inline bool get_useAllRewiredGamePlayers_27() const { return ___useAllRewiredGamePlayers_27; }
	inline bool* get_address_of_useAllRewiredGamePlayers_27() { return &___useAllRewiredGamePlayers_27; }
	inline void set_useAllRewiredGamePlayers_27(bool value)
	{
		___useAllRewiredGamePlayers_27 = value;
	}

	inline static int32_t get_offset_of_useRewiredSystemPlayer_28() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___useRewiredSystemPlayer_28)); }
	inline bool get_useRewiredSystemPlayer_28() const { return ___useRewiredSystemPlayer_28; }
	inline bool* get_address_of_useRewiredSystemPlayer_28() { return &___useRewiredSystemPlayer_28; }
	inline void set_useRewiredSystemPlayer_28(bool value)
	{
		___useRewiredSystemPlayer_28 = value;
	}

	inline static int32_t get_offset_of_rewiredPlayerIds_29() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___rewiredPlayerIds_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_rewiredPlayerIds_29() const { return ___rewiredPlayerIds_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_rewiredPlayerIds_29() { return &___rewiredPlayerIds_29; }
	inline void set_rewiredPlayerIds_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___rewiredPlayerIds_29 = value;
		Il2CppCodeGenWriteBarrier((&___rewiredPlayerIds_29), value);
	}

	inline static int32_t get_offset_of_usePlayingPlayersOnly_30() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___usePlayingPlayersOnly_30)); }
	inline bool get_usePlayingPlayersOnly_30() const { return ___usePlayingPlayersOnly_30; }
	inline bool* get_address_of_usePlayingPlayersOnly_30() { return &___usePlayingPlayersOnly_30; }
	inline void set_usePlayingPlayersOnly_30(bool value)
	{
		___usePlayingPlayersOnly_30 = value;
	}

	inline static int32_t get_offset_of_playerMice_31() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___playerMice_31)); }
	inline List_1_t08E7DE11ECA859CD34DB357C00B14BE87AF37AE9 * get_playerMice_31() const { return ___playerMice_31; }
	inline List_1_t08E7DE11ECA859CD34DB357C00B14BE87AF37AE9 ** get_address_of_playerMice_31() { return &___playerMice_31; }
	inline void set_playerMice_31(List_1_t08E7DE11ECA859CD34DB357C00B14BE87AF37AE9 * value)
	{
		___playerMice_31 = value;
		Il2CppCodeGenWriteBarrier((&___playerMice_31), value);
	}

	inline static int32_t get_offset_of_moveOneElementPerAxisPress_32() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___moveOneElementPerAxisPress_32)); }
	inline bool get_moveOneElementPerAxisPress_32() const { return ___moveOneElementPerAxisPress_32; }
	inline bool* get_address_of_moveOneElementPerAxisPress_32() { return &___moveOneElementPerAxisPress_32; }
	inline void set_moveOneElementPerAxisPress_32(bool value)
	{
		___moveOneElementPerAxisPress_32 = value;
	}

	inline static int32_t get_offset_of_setActionsById_33() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___setActionsById_33)); }
	inline bool get_setActionsById_33() const { return ___setActionsById_33; }
	inline bool* get_address_of_setActionsById_33() { return &___setActionsById_33; }
	inline void set_setActionsById_33(bool value)
	{
		___setActionsById_33 = value;
	}

	inline static int32_t get_offset_of_horizontalActionId_34() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___horizontalActionId_34)); }
	inline int32_t get_horizontalActionId_34() const { return ___horizontalActionId_34; }
	inline int32_t* get_address_of_horizontalActionId_34() { return &___horizontalActionId_34; }
	inline void set_horizontalActionId_34(int32_t value)
	{
		___horizontalActionId_34 = value;
	}

	inline static int32_t get_offset_of_verticalActionId_35() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___verticalActionId_35)); }
	inline int32_t get_verticalActionId_35() const { return ___verticalActionId_35; }
	inline int32_t* get_address_of_verticalActionId_35() { return &___verticalActionId_35; }
	inline void set_verticalActionId_35(int32_t value)
	{
		___verticalActionId_35 = value;
	}

	inline static int32_t get_offset_of_submitActionId_36() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___submitActionId_36)); }
	inline int32_t get_submitActionId_36() const { return ___submitActionId_36; }
	inline int32_t* get_address_of_submitActionId_36() { return &___submitActionId_36; }
	inline void set_submitActionId_36(int32_t value)
	{
		___submitActionId_36 = value;
	}

	inline static int32_t get_offset_of_cancelActionId_37() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___cancelActionId_37)); }
	inline int32_t get_cancelActionId_37() const { return ___cancelActionId_37; }
	inline int32_t* get_address_of_cancelActionId_37() { return &___cancelActionId_37; }
	inline void set_cancelActionId_37(int32_t value)
	{
		___cancelActionId_37 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_38() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_HorizontalAxis_38)); }
	inline String_t* get_m_HorizontalAxis_38() const { return ___m_HorizontalAxis_38; }
	inline String_t** get_address_of_m_HorizontalAxis_38() { return &___m_HorizontalAxis_38; }
	inline void set_m_HorizontalAxis_38(String_t* value)
	{
		___m_HorizontalAxis_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalAxis_38), value);
	}

	inline static int32_t get_offset_of_m_VerticalAxis_39() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_VerticalAxis_39)); }
	inline String_t* get_m_VerticalAxis_39() const { return ___m_VerticalAxis_39; }
	inline String_t** get_address_of_m_VerticalAxis_39() { return &___m_VerticalAxis_39; }
	inline void set_m_VerticalAxis_39(String_t* value)
	{
		___m_VerticalAxis_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalAxis_39), value);
	}

	inline static int32_t get_offset_of_m_SubmitButton_40() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_SubmitButton_40)); }
	inline String_t* get_m_SubmitButton_40() const { return ___m_SubmitButton_40; }
	inline String_t** get_address_of_m_SubmitButton_40() { return &___m_SubmitButton_40; }
	inline void set_m_SubmitButton_40(String_t* value)
	{
		___m_SubmitButton_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_SubmitButton_40), value);
	}

	inline static int32_t get_offset_of_m_CancelButton_41() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_CancelButton_41)); }
	inline String_t* get_m_CancelButton_41() const { return ___m_CancelButton_41; }
	inline String_t** get_address_of_m_CancelButton_41() { return &___m_CancelButton_41; }
	inline void set_m_CancelButton_41(String_t* value)
	{
		___m_CancelButton_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_CancelButton_41), value);
	}

	inline static int32_t get_offset_of_m_InputActionsPerSecond_42() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_InputActionsPerSecond_42)); }
	inline float get_m_InputActionsPerSecond_42() const { return ___m_InputActionsPerSecond_42; }
	inline float* get_address_of_m_InputActionsPerSecond_42() { return &___m_InputActionsPerSecond_42; }
	inline void set_m_InputActionsPerSecond_42(float value)
	{
		___m_InputActionsPerSecond_42 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_43() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_RepeatDelay_43)); }
	inline float get_m_RepeatDelay_43() const { return ___m_RepeatDelay_43; }
	inline float* get_address_of_m_RepeatDelay_43() { return &___m_RepeatDelay_43; }
	inline void set_m_RepeatDelay_43(float value)
	{
		___m_RepeatDelay_43 = value;
	}

	inline static int32_t get_offset_of_m_allowMouseInput_44() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_allowMouseInput_44)); }
	inline bool get_m_allowMouseInput_44() const { return ___m_allowMouseInput_44; }
	inline bool* get_address_of_m_allowMouseInput_44() { return &___m_allowMouseInput_44; }
	inline void set_m_allowMouseInput_44(bool value)
	{
		___m_allowMouseInput_44 = value;
	}

	inline static int32_t get_offset_of_m_allowMouseInputIfTouchSupported_45() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_allowMouseInputIfTouchSupported_45)); }
	inline bool get_m_allowMouseInputIfTouchSupported_45() const { return ___m_allowMouseInputIfTouchSupported_45; }
	inline bool* get_address_of_m_allowMouseInputIfTouchSupported_45() { return &___m_allowMouseInputIfTouchSupported_45; }
	inline void set_m_allowMouseInputIfTouchSupported_45(bool value)
	{
		___m_allowMouseInputIfTouchSupported_45 = value;
	}

	inline static int32_t get_offset_of_m_allowTouchInput_46() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_allowTouchInput_46)); }
	inline bool get_m_allowTouchInput_46() const { return ___m_allowTouchInput_46; }
	inline bool* get_address_of_m_allowTouchInput_46() { return &___m_allowTouchInput_46; }
	inline void set_m_allowTouchInput_46(bool value)
	{
		___m_allowTouchInput_46 = value;
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_47() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_ForceModuleActive_47)); }
	inline bool get_m_ForceModuleActive_47() const { return ___m_ForceModuleActive_47; }
	inline bool* get_address_of_m_ForceModuleActive_47() { return &___m_ForceModuleActive_47; }
	inline void set_m_ForceModuleActive_47(bool value)
	{
		___m_ForceModuleActive_47 = value;
	}

	inline static int32_t get_offset_of_playerIds_48() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___playerIds_48)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_playerIds_48() const { return ___playerIds_48; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_playerIds_48() { return &___playerIds_48; }
	inline void set_playerIds_48(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___playerIds_48 = value;
		Il2CppCodeGenWriteBarrier((&___playerIds_48), value);
	}

	inline static int32_t get_offset_of_recompiling_49() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___recompiling_49)); }
	inline bool get_recompiling_49() const { return ___recompiling_49; }
	inline bool* get_address_of_recompiling_49() { return &___recompiling_49; }
	inline void set_recompiling_49(bool value)
	{
		___recompiling_49 = value;
	}

	inline static int32_t get_offset_of_isTouchSupported_50() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___isTouchSupported_50)); }
	inline bool get_isTouchSupported_50() const { return ___isTouchSupported_50; }
	inline bool* get_address_of_isTouchSupported_50() { return &___isTouchSupported_50; }
	inline void set_isTouchSupported_50(bool value)
	{
		___isTouchSupported_50 = value;
	}

	inline static int32_t get_offset_of_m_PrevActionTime_51() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_PrevActionTime_51)); }
	inline float get_m_PrevActionTime_51() const { return ___m_PrevActionTime_51; }
	inline float* get_address_of_m_PrevActionTime_51() { return &___m_PrevActionTime_51; }
	inline void set_m_PrevActionTime_51(float value)
	{
		___m_PrevActionTime_51 = value;
	}

	inline static int32_t get_offset_of_m_LastMoveVector_52() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_LastMoveVector_52)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_LastMoveVector_52() const { return ___m_LastMoveVector_52; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_LastMoveVector_52() { return &___m_LastMoveVector_52; }
	inline void set_m_LastMoveVector_52(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_LastMoveVector_52 = value;
	}

	inline static int32_t get_offset_of_m_ConsecutiveMoveCount_53() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_ConsecutiveMoveCount_53)); }
	inline int32_t get_m_ConsecutiveMoveCount_53() const { return ___m_ConsecutiveMoveCount_53; }
	inline int32_t* get_address_of_m_ConsecutiveMoveCount_53() { return &___m_ConsecutiveMoveCount_53; }
	inline void set_m_ConsecutiveMoveCount_53(int32_t value)
	{
		___m_ConsecutiveMoveCount_53 = value;
	}

	inline static int32_t get_offset_of_m_HasFocus_54() { return static_cast<int32_t>(offsetof(RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7, ___m_HasFocus_54)); }
	inline bool get_m_HasFocus_54() const { return ___m_HasFocus_54; }
	inline bool* get_address_of_m_HasFocus_54() { return &___m_HasFocus_54; }
	inline void set_m_HasFocus_54(bool value)
	{
		___m_HasFocus_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWIREDSTANDALONEINPUTMODULE_T26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7_H
#ifndef CUSTOMBUTTON_T9D3E192F0D1B801DD76E169A1A807BACA952A533_H
#define CUSTOMBUTTON_T9D3E192F0D1B801DD76E169A1A807BACA952A533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.CustomButton
struct  CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533  : public Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B
{
public:
	// UnityEngine.Sprite Rewired.UI.ControlMapper.CustomButton::_disabledHighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ____disabledHighlightedSprite_19;
	// UnityEngine.Color Rewired.UI.ControlMapper.CustomButton::_disabledHighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____disabledHighlightedColor_20;
	// System.String Rewired.UI.ControlMapper.CustomButton::_disabledHighlightedTrigger
	String_t* ____disabledHighlightedTrigger_21;
	// System.Boolean Rewired.UI.ControlMapper.CustomButton::_autoNavUp
	bool ____autoNavUp_22;
	// System.Boolean Rewired.UI.ControlMapper.CustomButton::_autoNavDown
	bool ____autoNavDown_23;
	// System.Boolean Rewired.UI.ControlMapper.CustomButton::_autoNavLeft
	bool ____autoNavLeft_24;
	// System.Boolean Rewired.UI.ControlMapper.CustomButton::_autoNavRight
	bool ____autoNavRight_25;
	// System.Boolean Rewired.UI.ControlMapper.CustomButton::isHighlightDisabled
	bool ___isHighlightDisabled_26;
	// UnityEngine.Events.UnityAction Rewired.UI.ControlMapper.CustomButton::_CancelEvent
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ____CancelEvent_27;

public:
	inline static int32_t get_offset_of__disabledHighlightedSprite_19() { return static_cast<int32_t>(offsetof(CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533, ____disabledHighlightedSprite_19)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get__disabledHighlightedSprite_19() const { return ____disabledHighlightedSprite_19; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of__disabledHighlightedSprite_19() { return &____disabledHighlightedSprite_19; }
	inline void set__disabledHighlightedSprite_19(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		____disabledHighlightedSprite_19 = value;
		Il2CppCodeGenWriteBarrier((&____disabledHighlightedSprite_19), value);
	}

	inline static int32_t get_offset_of__disabledHighlightedColor_20() { return static_cast<int32_t>(offsetof(CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533, ____disabledHighlightedColor_20)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__disabledHighlightedColor_20() const { return ____disabledHighlightedColor_20; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__disabledHighlightedColor_20() { return &____disabledHighlightedColor_20; }
	inline void set__disabledHighlightedColor_20(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____disabledHighlightedColor_20 = value;
	}

	inline static int32_t get_offset_of__disabledHighlightedTrigger_21() { return static_cast<int32_t>(offsetof(CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533, ____disabledHighlightedTrigger_21)); }
	inline String_t* get__disabledHighlightedTrigger_21() const { return ____disabledHighlightedTrigger_21; }
	inline String_t** get_address_of__disabledHighlightedTrigger_21() { return &____disabledHighlightedTrigger_21; }
	inline void set__disabledHighlightedTrigger_21(String_t* value)
	{
		____disabledHighlightedTrigger_21 = value;
		Il2CppCodeGenWriteBarrier((&____disabledHighlightedTrigger_21), value);
	}

	inline static int32_t get_offset_of__autoNavUp_22() { return static_cast<int32_t>(offsetof(CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533, ____autoNavUp_22)); }
	inline bool get__autoNavUp_22() const { return ____autoNavUp_22; }
	inline bool* get_address_of__autoNavUp_22() { return &____autoNavUp_22; }
	inline void set__autoNavUp_22(bool value)
	{
		____autoNavUp_22 = value;
	}

	inline static int32_t get_offset_of__autoNavDown_23() { return static_cast<int32_t>(offsetof(CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533, ____autoNavDown_23)); }
	inline bool get__autoNavDown_23() const { return ____autoNavDown_23; }
	inline bool* get_address_of__autoNavDown_23() { return &____autoNavDown_23; }
	inline void set__autoNavDown_23(bool value)
	{
		____autoNavDown_23 = value;
	}

	inline static int32_t get_offset_of__autoNavLeft_24() { return static_cast<int32_t>(offsetof(CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533, ____autoNavLeft_24)); }
	inline bool get__autoNavLeft_24() const { return ____autoNavLeft_24; }
	inline bool* get_address_of__autoNavLeft_24() { return &____autoNavLeft_24; }
	inline void set__autoNavLeft_24(bool value)
	{
		____autoNavLeft_24 = value;
	}

	inline static int32_t get_offset_of__autoNavRight_25() { return static_cast<int32_t>(offsetof(CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533, ____autoNavRight_25)); }
	inline bool get__autoNavRight_25() const { return ____autoNavRight_25; }
	inline bool* get_address_of__autoNavRight_25() { return &____autoNavRight_25; }
	inline void set__autoNavRight_25(bool value)
	{
		____autoNavRight_25 = value;
	}

	inline static int32_t get_offset_of_isHighlightDisabled_26() { return static_cast<int32_t>(offsetof(CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533, ___isHighlightDisabled_26)); }
	inline bool get_isHighlightDisabled_26() const { return ___isHighlightDisabled_26; }
	inline bool* get_address_of_isHighlightDisabled_26() { return &___isHighlightDisabled_26; }
	inline void set_isHighlightDisabled_26(bool value)
	{
		___isHighlightDisabled_26 = value;
	}

	inline static int32_t get_offset_of__CancelEvent_27() { return static_cast<int32_t>(offsetof(CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533, ____CancelEvent_27)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get__CancelEvent_27() const { return ____CancelEvent_27; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of__CancelEvent_27() { return &____CancelEvent_27; }
	inline void set__CancelEvent_27(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		____CancelEvent_27 = value;
		Il2CppCodeGenWriteBarrier((&____CancelEvent_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMBUTTON_T9D3E192F0D1B801DD76E169A1A807BACA952A533_H
#ifndef CUSTOMSLIDER_T0CAD108D88656B458641832AC93E466E05A258B0_H
#define CUSTOMSLIDER_T0CAD108D88656B458641832AC93E466E05A258B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.CustomSlider
struct  CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0  : public Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09
{
public:
	// UnityEngine.Sprite Rewired.UI.ControlMapper.CustomSlider::_disabledHighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ____disabledHighlightedSprite_33;
	// UnityEngine.Color Rewired.UI.ControlMapper.CustomSlider::_disabledHighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____disabledHighlightedColor_34;
	// System.String Rewired.UI.ControlMapper.CustomSlider::_disabledHighlightedTrigger
	String_t* ____disabledHighlightedTrigger_35;
	// System.Boolean Rewired.UI.ControlMapper.CustomSlider::_autoNavUp
	bool ____autoNavUp_36;
	// System.Boolean Rewired.UI.ControlMapper.CustomSlider::_autoNavDown
	bool ____autoNavDown_37;
	// System.Boolean Rewired.UI.ControlMapper.CustomSlider::_autoNavLeft
	bool ____autoNavLeft_38;
	// System.Boolean Rewired.UI.ControlMapper.CustomSlider::_autoNavRight
	bool ____autoNavRight_39;
	// System.Boolean Rewired.UI.ControlMapper.CustomSlider::isHighlightDisabled
	bool ___isHighlightDisabled_40;
	// UnityEngine.Events.UnityAction Rewired.UI.ControlMapper.CustomSlider::_CancelEvent
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ____CancelEvent_41;

public:
	inline static int32_t get_offset_of__disabledHighlightedSprite_33() { return static_cast<int32_t>(offsetof(CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0, ____disabledHighlightedSprite_33)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get__disabledHighlightedSprite_33() const { return ____disabledHighlightedSprite_33; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of__disabledHighlightedSprite_33() { return &____disabledHighlightedSprite_33; }
	inline void set__disabledHighlightedSprite_33(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		____disabledHighlightedSprite_33 = value;
		Il2CppCodeGenWriteBarrier((&____disabledHighlightedSprite_33), value);
	}

	inline static int32_t get_offset_of__disabledHighlightedColor_34() { return static_cast<int32_t>(offsetof(CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0, ____disabledHighlightedColor_34)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__disabledHighlightedColor_34() const { return ____disabledHighlightedColor_34; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__disabledHighlightedColor_34() { return &____disabledHighlightedColor_34; }
	inline void set__disabledHighlightedColor_34(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____disabledHighlightedColor_34 = value;
	}

	inline static int32_t get_offset_of__disabledHighlightedTrigger_35() { return static_cast<int32_t>(offsetof(CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0, ____disabledHighlightedTrigger_35)); }
	inline String_t* get__disabledHighlightedTrigger_35() const { return ____disabledHighlightedTrigger_35; }
	inline String_t** get_address_of__disabledHighlightedTrigger_35() { return &____disabledHighlightedTrigger_35; }
	inline void set__disabledHighlightedTrigger_35(String_t* value)
	{
		____disabledHighlightedTrigger_35 = value;
		Il2CppCodeGenWriteBarrier((&____disabledHighlightedTrigger_35), value);
	}

	inline static int32_t get_offset_of__autoNavUp_36() { return static_cast<int32_t>(offsetof(CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0, ____autoNavUp_36)); }
	inline bool get__autoNavUp_36() const { return ____autoNavUp_36; }
	inline bool* get_address_of__autoNavUp_36() { return &____autoNavUp_36; }
	inline void set__autoNavUp_36(bool value)
	{
		____autoNavUp_36 = value;
	}

	inline static int32_t get_offset_of__autoNavDown_37() { return static_cast<int32_t>(offsetof(CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0, ____autoNavDown_37)); }
	inline bool get__autoNavDown_37() const { return ____autoNavDown_37; }
	inline bool* get_address_of__autoNavDown_37() { return &____autoNavDown_37; }
	inline void set__autoNavDown_37(bool value)
	{
		____autoNavDown_37 = value;
	}

	inline static int32_t get_offset_of__autoNavLeft_38() { return static_cast<int32_t>(offsetof(CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0, ____autoNavLeft_38)); }
	inline bool get__autoNavLeft_38() const { return ____autoNavLeft_38; }
	inline bool* get_address_of__autoNavLeft_38() { return &____autoNavLeft_38; }
	inline void set__autoNavLeft_38(bool value)
	{
		____autoNavLeft_38 = value;
	}

	inline static int32_t get_offset_of__autoNavRight_39() { return static_cast<int32_t>(offsetof(CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0, ____autoNavRight_39)); }
	inline bool get__autoNavRight_39() const { return ____autoNavRight_39; }
	inline bool* get_address_of__autoNavRight_39() { return &____autoNavRight_39; }
	inline void set__autoNavRight_39(bool value)
	{
		____autoNavRight_39 = value;
	}

	inline static int32_t get_offset_of_isHighlightDisabled_40() { return static_cast<int32_t>(offsetof(CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0, ___isHighlightDisabled_40)); }
	inline bool get_isHighlightDisabled_40() const { return ___isHighlightDisabled_40; }
	inline bool* get_address_of_isHighlightDisabled_40() { return &___isHighlightDisabled_40; }
	inline void set_isHighlightDisabled_40(bool value)
	{
		___isHighlightDisabled_40 = value;
	}

	inline static int32_t get_offset_of__CancelEvent_41() { return static_cast<int32_t>(offsetof(CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0, ____CancelEvent_41)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get__CancelEvent_41() const { return ____CancelEvent_41; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of__CancelEvent_41() { return &____CancelEvent_41; }
	inline void set__CancelEvent_41(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		____CancelEvent_41 = value;
		Il2CppCodeGenWriteBarrier((&____CancelEvent_41), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMSLIDER_T0CAD108D88656B458641832AC93E466E05A258B0_H
#ifndef CUSTOMTOGGLE_TE7476FE1D2C6D949F00ABA84499194061484572E_H
#define CUSTOMTOGGLE_TE7476FE1D2C6D949F00ABA84499194061484572E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UI.ControlMapper.CustomToggle
struct  CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E  : public Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106
{
public:
	// UnityEngine.Sprite Rewired.UI.ControlMapper.CustomToggle::_disabledHighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ____disabledHighlightedSprite_23;
	// UnityEngine.Color Rewired.UI.ControlMapper.CustomToggle::_disabledHighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____disabledHighlightedColor_24;
	// System.String Rewired.UI.ControlMapper.CustomToggle::_disabledHighlightedTrigger
	String_t* ____disabledHighlightedTrigger_25;
	// System.Boolean Rewired.UI.ControlMapper.CustomToggle::_autoNavUp
	bool ____autoNavUp_26;
	// System.Boolean Rewired.UI.ControlMapper.CustomToggle::_autoNavDown
	bool ____autoNavDown_27;
	// System.Boolean Rewired.UI.ControlMapper.CustomToggle::_autoNavLeft
	bool ____autoNavLeft_28;
	// System.Boolean Rewired.UI.ControlMapper.CustomToggle::_autoNavRight
	bool ____autoNavRight_29;
	// System.Boolean Rewired.UI.ControlMapper.CustomToggle::isHighlightDisabled
	bool ___isHighlightDisabled_30;
	// UnityEngine.Events.UnityAction Rewired.UI.ControlMapper.CustomToggle::_CancelEvent
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ____CancelEvent_31;

public:
	inline static int32_t get_offset_of__disabledHighlightedSprite_23() { return static_cast<int32_t>(offsetof(CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E, ____disabledHighlightedSprite_23)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get__disabledHighlightedSprite_23() const { return ____disabledHighlightedSprite_23; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of__disabledHighlightedSprite_23() { return &____disabledHighlightedSprite_23; }
	inline void set__disabledHighlightedSprite_23(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		____disabledHighlightedSprite_23 = value;
		Il2CppCodeGenWriteBarrier((&____disabledHighlightedSprite_23), value);
	}

	inline static int32_t get_offset_of__disabledHighlightedColor_24() { return static_cast<int32_t>(offsetof(CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E, ____disabledHighlightedColor_24)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__disabledHighlightedColor_24() const { return ____disabledHighlightedColor_24; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__disabledHighlightedColor_24() { return &____disabledHighlightedColor_24; }
	inline void set__disabledHighlightedColor_24(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____disabledHighlightedColor_24 = value;
	}

	inline static int32_t get_offset_of__disabledHighlightedTrigger_25() { return static_cast<int32_t>(offsetof(CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E, ____disabledHighlightedTrigger_25)); }
	inline String_t* get__disabledHighlightedTrigger_25() const { return ____disabledHighlightedTrigger_25; }
	inline String_t** get_address_of__disabledHighlightedTrigger_25() { return &____disabledHighlightedTrigger_25; }
	inline void set__disabledHighlightedTrigger_25(String_t* value)
	{
		____disabledHighlightedTrigger_25 = value;
		Il2CppCodeGenWriteBarrier((&____disabledHighlightedTrigger_25), value);
	}

	inline static int32_t get_offset_of__autoNavUp_26() { return static_cast<int32_t>(offsetof(CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E, ____autoNavUp_26)); }
	inline bool get__autoNavUp_26() const { return ____autoNavUp_26; }
	inline bool* get_address_of__autoNavUp_26() { return &____autoNavUp_26; }
	inline void set__autoNavUp_26(bool value)
	{
		____autoNavUp_26 = value;
	}

	inline static int32_t get_offset_of__autoNavDown_27() { return static_cast<int32_t>(offsetof(CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E, ____autoNavDown_27)); }
	inline bool get__autoNavDown_27() const { return ____autoNavDown_27; }
	inline bool* get_address_of__autoNavDown_27() { return &____autoNavDown_27; }
	inline void set__autoNavDown_27(bool value)
	{
		____autoNavDown_27 = value;
	}

	inline static int32_t get_offset_of__autoNavLeft_28() { return static_cast<int32_t>(offsetof(CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E, ____autoNavLeft_28)); }
	inline bool get__autoNavLeft_28() const { return ____autoNavLeft_28; }
	inline bool* get_address_of__autoNavLeft_28() { return &____autoNavLeft_28; }
	inline void set__autoNavLeft_28(bool value)
	{
		____autoNavLeft_28 = value;
	}

	inline static int32_t get_offset_of__autoNavRight_29() { return static_cast<int32_t>(offsetof(CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E, ____autoNavRight_29)); }
	inline bool get__autoNavRight_29() const { return ____autoNavRight_29; }
	inline bool* get_address_of__autoNavRight_29() { return &____autoNavRight_29; }
	inline void set__autoNavRight_29(bool value)
	{
		____autoNavRight_29 = value;
	}

	inline static int32_t get_offset_of_isHighlightDisabled_30() { return static_cast<int32_t>(offsetof(CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E, ___isHighlightDisabled_30)); }
	inline bool get_isHighlightDisabled_30() const { return ___isHighlightDisabled_30; }
	inline bool* get_address_of_isHighlightDisabled_30() { return &___isHighlightDisabled_30; }
	inline void set_isHighlightDisabled_30(bool value)
	{
		___isHighlightDisabled_30 = value;
	}

	inline static int32_t get_offset_of__CancelEvent_31() { return static_cast<int32_t>(offsetof(CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E, ____CancelEvent_31)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get__CancelEvent_31() const { return ____CancelEvent_31; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of__CancelEvent_31() { return &____CancelEvent_31; }
	inline void set__CancelEvent_31(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		____CancelEvent_31 = value;
		Il2CppCodeGenWriteBarrier((&____CancelEvent_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMTOGGLE_TE7476FE1D2C6D949F00ABA84499194061484572E_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5300 = { sizeof (Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5300[4] = 
{
	Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C::get_offset_of_aim_0(),
	Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C::get_offset_of_weight_1(),
	Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C::get_offset_of_toHole_2(),
	Instruction_t84B8682C9B3B3FC82E24151EAFEEA97BC7B0FB4C::get_offset_of_elevation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5301 = { sizeof (Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5301[1] = 
{
	Region_t6EFDA95747258F99D0F1EABA7AB7AE8CECF1BF6E::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5302 = { sizeof (RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5302[4] = 
{
	RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F::get_offset_of_name_0(),
	RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F::get_offset_of_regionCode_1(),
	RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F::get_offset_of_countryCode_2(),
	RegionData_t79DE0DE7F085973DEFB23878C33B7C301777829F::get_offset_of_courseCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5303 = { sizeof (User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5303[5] = 
{
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371::get_offset_of_name_0(),
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371::get_offset_of_roboGreenID_1(),
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371::get_offset_of_memberId_2(),
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371::get_offset_of_token_3(),
	User_tDA60B60B80C63C1D6DD68A774400AB1263BD2371::get_offset_of_memberDetail_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5304 = { sizeof (MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5304[4] = 
{
	MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B::get_offset_of_emailAddress_0(),
	MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B::get_offset_of_roles_1(),
	MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B::get_offset_of_courses_2(),
	MemberDetail_t135B5CC18053FA64F1747F427A6CECBEE234F08B::get_offset_of_printers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5305 = { sizeof (Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5305[3] = 
{
	Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F::get_offset_of_roleId_0(),
	Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F::get_offset_of_name_1(),
	Roles_t1C83194AC6C88701A7E6EBA86C531898D094B34F::get_offset_of_domain_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5306 = { sizeof (UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5306[3] = 
{
	UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70::get_offset_of_courseId_0(),
	UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70::get_offset_of_sourceId_1(),
	UserCourse_t7034C2CD56492ABBB9662949E17FF739DD286D70::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5307 = { sizeof (UserPrinter_tF6F16EB31422E9716816C97ED8D440F25328A838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5307[2] = 
{
	UserPrinter_tF6F16EB31422E9716816C97ED8D440F25328A838::get_offset_of_printerId_0(),
	UserPrinter_tF6F16EB31422E9716816C97ED8D440F25328A838::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5308 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5309 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5310 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5311 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5312 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5313 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5314 = { sizeof (GamepadTemplate_t2FDAF01C8487C65FD8CA327631F2C4F082AC6573), -1, sizeof(GamepadTemplate_t2FDAF01C8487C65FD8CA327631F2C4F082AC6573_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5314[40] = 
{
	GamepadTemplate_t2FDAF01C8487C65FD8CA327631F2C4F082AC6573_StaticFields::get_offset_of_typeGuid_8(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5315 = { sizeof (RacingWheelTemplate_t8A4529334B04EE28E38A989CF16A7F0F2CFCBD25), -1, sizeof(RacingWheelTemplate_t8A4529334B04EE28E38A989CF16A7F0F2CFCBD25_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5315[47] = 
{
	RacingWheelTemplate_t8A4529334B04EE28E38A989CF16A7F0F2CFCBD25_StaticFields::get_offset_of_typeGuid_8(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5316 = { sizeof (HOTASTemplate_t07DBFB706D71D8288EE25DE3649D9DEA06FE55C1), -1, sizeof(HOTASTemplate_t07DBFB706D71D8288EE25DE3649D9DEA06FE55C1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5316[169] = 
{
	HOTASTemplate_t07DBFB706D71D8288EE25DE3649D9DEA06FE55C1_StaticFields::get_offset_of_typeGuid_8(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5317 = { sizeof (FlightYokeTemplate_t339D83B5785E12D828E1AA369870716E4F606653), -1, sizeof(FlightYokeTemplate_t339D83B5785E12D828E1AA369870716E4F606653_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5317[78] = 
{
	FlightYokeTemplate_t339D83B5785E12D828E1AA369870716E4F606653_StaticFields::get_offset_of_typeGuid_8(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5318 = { sizeof (FlightPedalsTemplate_t8952CFD27FA33E01A13B49C83069731FDEA315FF), -1, sizeof(FlightPedalsTemplate_t8952CFD27FA33E01A13B49C83069731FDEA315FF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5318[4] = 
{
	FlightPedalsTemplate_t8952CFD27FA33E01A13B49C83069731FDEA315FF_StaticFields::get_offset_of_typeGuid_8(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5319 = { sizeof (SixDofControllerTemplate_tE4DC0CA993BF4EB37DAC595EB0CF8215D987B2B6), -1, sizeof(SixDofControllerTemplate_tE4DC0CA993BF4EB37DAC595EB0CF8215D987B2B6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5319[68] = 
{
	SixDofControllerTemplate_tE4DC0CA993BF4EB37DAC595EB0CF8215D987B2B6_StaticFields::get_offset_of_typeGuid_8(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5320 = { sizeof (InputManager_t42964CC63A93EEFEC8C958ED5DF1166DB01B09CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5321 = { sizeof (ExternalTools_t3F3B878EE09D19AD46984E0E3F2BF98F497CCAF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5321[3] = 
{
	ExternalTools_t3F3B878EE09D19AD46984E0E3F2BF98F497CCAF4::get_offset_of__isEditorPaused_0(),
	ExternalTools_t3F3B878EE09D19AD46984E0E3F2BF98F497CCAF4::get_offset_of__EditorPausedStateChangedEvent_1(),
	ExternalTools_t3F3B878EE09D19AD46984E0E3F2BF98F497CCAF4::get_offset_of_XboxOneInput_OnGamepadStateChange_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5322 = { sizeof (UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5322[15] = 
{
	0,
	0,
	0,
	0,
	UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5::get_offset_of_isEnabled_8(),
	UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5::get_offset_of_loadDataOnStart_9(),
	UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5::get_offset_of_loadJoystickAssignments_10(),
	UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5::get_offset_of_loadKeyboardAssignments_11(),
	UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5::get_offset_of_loadMouseAssignments_12(),
	UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5::get_offset_of_playerPrefsKeyPrefix_13(),
	UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5::get_offset_of_allowImpreciseJoystickAssignmentMatching_14(),
	UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5::get_offset_of_deferredJoystickAssignmentLoadPending_15(),
	UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5::get_offset_of_wasJoystickEverDetected_16(),
	UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5::get_offset_of___allActionIds_17(),
	UserDataStore_PlayerPrefs_t91FEBBF9ABC4D3551E86E49CF53ED981FB6A47D5::get_offset_of___allActionIdsString_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5323 = { sizeof (ControllerAssignmentSaveInfo_t850E0C2E7A93E61299F9820AC1EBCF495396B04A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5323[1] = 
{
	ControllerAssignmentSaveInfo_t850E0C2E7A93E61299F9820AC1EBCF495396B04A::get_offset_of_players_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5324 = { sizeof (PlayerInfo_tE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5324[4] = 
{
	PlayerInfo_tE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5::get_offset_of_id_0(),
	PlayerInfo_tE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5::get_offset_of_hasKeyboard_1(),
	PlayerInfo_tE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5::get_offset_of_hasMouse_2(),
	PlayerInfo_tE42FC3ACB5EB8BAEAC6827F7E5E0D6C8133CEBA5::get_offset_of_joysticks_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5325 = { sizeof (JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5325[3] = 
{
	JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B::get_offset_of_instanceGuid_0(),
	JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B::get_offset_of_hardwareIdentifier_1(),
	JoystickInfo_t3B8053247F0090A5FD17FB477A67B9E2FD21DD1B::get_offset_of_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5326 = { sizeof (JoystickAssignmentHistoryInfo_t6C9ADB9CEF1E3309D8C314C439D49F0513B802A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5326[2] = 
{
	JoystickAssignmentHistoryInfo_t6C9ADB9CEF1E3309D8C314C439D49F0513B802A4::get_offset_of_joystick_0(),
	JoystickAssignmentHistoryInfo_t6C9ADB9CEF1E3309D8C314C439D49F0513B802A4::get_offset_of_oldJoystickId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5327 = { sizeof (U3CU3Ec__DisplayClass73_0_t3BBDFC06998B23FE10ADE01C56EF996EFE2C1C63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5327[1] = 
{
	U3CU3Ec__DisplayClass73_0_t3BBDFC06998B23FE10ADE01C56EF996EFE2C1C63::get_offset_of_joystick_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5328 = { sizeof (U3CU3Ec__DisplayClass73_1_t8D81D1C492C6008D463C4CC24122A18BB8527FB8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5328[1] = 
{
	U3CU3Ec__DisplayClass73_1_t8D81D1C492C6008D463C4CC24122A18BB8527FB8::get_offset_of_joystickInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5329 = { sizeof (U3CU3Ec__DisplayClass73_2_tF3CBEE2F5D06F3E550BCC7800FCDA98D149C1B6F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5329[1] = 
{
	U3CU3Ec__DisplayClass73_2_tF3CBEE2F5D06F3E550BCC7800FCDA98D149C1B6F::get_offset_of_match_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5330 = { sizeof (U3CLoadJoystickAssignmentsDeferredU3Ed__75_t5B42B566A1AF4CF8C40235945EFAE7441692EC22), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5330[3] = 
{
	U3CLoadJoystickAssignmentsDeferredU3Ed__75_t5B42B566A1AF4CF8C40235945EFAE7441692EC22::get_offset_of_U3CU3E1__state_0(),
	U3CLoadJoystickAssignmentsDeferredU3Ed__75_t5B42B566A1AF4CF8C40235945EFAE7441692EC22::get_offset_of_U3CU3E2__current_1(),
	U3CLoadJoystickAssignmentsDeferredU3Ed__75_t5B42B566A1AF4CF8C40235945EFAE7441692EC22::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5331 = { sizeof (ControllerTemplateFactory_t80C1C4DF4B65C968B5BB8EA87F076206470DE9D1), -1, sizeof(ControllerTemplateFactory_t80C1C4DF4B65C968B5BB8EA87F076206470DE9D1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5331[2] = 
{
	ControllerTemplateFactory_t80C1C4DF4B65C968B5BB8EA87F076206470DE9D1_StaticFields::get_offset_of__defaultTemplateTypes_0(),
	ControllerTemplateFactory_t80C1C4DF4B65C968B5BB8EA87F076206470DE9D1_StaticFields::get_offset_of__defaultTemplateInterfaceTypes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5332 = { sizeof (PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5332[6] = 
{
	PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72::get_offset_of_U3CplayerIdU3Ek__BackingField_23(),
	PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72::get_offset_of_U3CinputSourceIndexU3Ek__BackingField_24(),
	PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72::get_offset_of_U3CmouseSourceU3Ek__BackingField_25(),
	PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72::get_offset_of_U3CtouchSourceU3Ek__BackingField_26(),
	PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72::get_offset_of_U3CsourceTypeU3Ek__BackingField_27(),
	PlayerPointerEventData_t568A431E46421BCA3C7EEC563BBFA6E0BB189D72::get_offset_of_U3CbuttonIndexU3Ek__BackingField_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5333 = { sizeof (RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5333[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A::get_offset_of_m_MouseInputSourcesList_17(),
	RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A::get_offset_of_m_PlayerPointerData_18(),
	RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A::get_offset_of_m_UserDefaultTouchInputSource_19(),
	RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A::get_offset_of___m_DefaultInputSource_20(),
	RewiredPointerInputModule_t8D77A9A9FEE9FAA1EDFBA41C998EE3A7DA571C9A::get_offset_of_m_MouseState_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5334 = { sizeof (MouseState_tC2B3A40B96175F6EE1FEBD1693F38B717BABFD5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5334[1] = 
{
	MouseState_tC2B3A40B96175F6EE1FEBD1693F38B717BABFD5C::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5335 = { sizeof (MouseButtonEventData_tCDAEE9261DB005C8EBAA9EB979891497CCC71F6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5335[2] = 
{
	MouseButtonEventData_tCDAEE9261DB005C8EBAA9EB979891497CCC71F6C::get_offset_of_buttonState_0(),
	MouseButtonEventData_tCDAEE9261DB005C8EBAA9EB979891497CCC71F6C::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5336 = { sizeof (ButtonState_t87909B61004D60C53EAA68984CA998CFEEFFC5DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5336[2] = 
{
	ButtonState_t87909B61004D60C53EAA68984CA998CFEEFFC5DA::get_offset_of_m_Button_0(),
	ButtonState_t87909B61004D60C53EAA68984CA998CFEEFFC5DA::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5337 = { sizeof (UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5337[3] = 
{
	UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128::get_offset_of_m_MousePosition_0(),
	UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128::get_offset_of_m_MousePositionPrev_1(),
	UnityInputSource_t1EE49D6C3A027078D2C0643F34B72F1D713B1128::get_offset_of_m_LastUpdatedFrame_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5338 = { sizeof (PointerEventType_t25709B330F51FD9CC5E147F69A30D7B66301FECC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5338[3] = 
{
	PointerEventType_t25709B330F51FD9CC5E147F69A30D7B66301FECC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5339 = { sizeof (RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5339[33] = 
{
	0,
	0,
	0,
	0,
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_rewiredInputManager_26(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_useAllRewiredGamePlayers_27(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_useRewiredSystemPlayer_28(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_rewiredPlayerIds_29(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_usePlayingPlayersOnly_30(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_playerMice_31(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_moveOneElementPerAxisPress_32(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_setActionsById_33(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_horizontalActionId_34(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_verticalActionId_35(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_submitActionId_36(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_cancelActionId_37(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_HorizontalAxis_38(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_VerticalAxis_39(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_SubmitButton_40(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_CancelButton_41(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_InputActionsPerSecond_42(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_RepeatDelay_43(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_allowMouseInput_44(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_allowMouseInputIfTouchSupported_45(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_allowTouchInput_46(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_ForceModuleActive_47(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_playerIds_48(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_recompiling_49(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_isTouchSupported_50(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_PrevActionTime_51(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_LastMoveVector_52(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_ConsecutiveMoveCount_53(),
	RewiredStandaloneInputModule_t26C5AF948E55CA617ED74DAE2E91D5DE89DB70B7::get_offset_of_m_HasFocus_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5340 = { sizeof (PlayerSetting_t09C5D4DC5F57B8C6CEFCFDEF2A430FF4129C82BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5340[2] = 
{
	PlayerSetting_t09C5D4DC5F57B8C6CEFCFDEF2A430FF4129C82BD::get_offset_of_playerId_0(),
	PlayerSetting_t09C5D4DC5F57B8C6CEFCFDEF2A430FF4129C82BD::get_offset_of_playerMice_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5341 = { sizeof (ButtonInfo_t09F7FD5F90E913E38AD44D748529C16955E0B9C3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5342 = { sizeof (CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5342[36] = 
{
	0,
	0,
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_rightContentContainer_20(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_valueDisplayGroup_21(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_calibratedValueMarker_22(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_rawValueMarker_23(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_calibratedZeroMarker_24(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_deadzoneArea_25(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_deadzoneSlider_26(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_zeroSlider_27(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_sensitivitySlider_28(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_invertToggle_29(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_axisScrollAreaContent_30(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_doneButton_31(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_calibrateButton_32(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_doneButtonLabel_33(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_cancelButtonLabel_34(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_defaultButtonLabel_35(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_deadzoneSliderLabel_36(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_zeroSliderLabel_37(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_sensitivitySliderLabel_38(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_invertToggleLabel_39(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_calibrateButtonLabel_40(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_axisButtonPrefab_41(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_joystick_42(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_origCalibrationData_43(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_selectedAxis_44(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_origSelectedAxisCalibrationData_45(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_displayAreaWidth_46(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_axisButtons_47(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_buttonCallbacks_48(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_playerId_49(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_rewiredStandaloneInputModule_50(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_menuHorizActionId_51(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_menuVertActionId_52(),
	CalibrationWindow_t3B201B7586ADF13BF56596085B279B38129E76E6::get_offset_of_minSensitivity_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5343 = { sizeof (ButtonIdentifier_t229D85569162CB66A697F71025B678733FC782AF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5343[5] = 
{
	ButtonIdentifier_t229D85569162CB66A697F71025B678733FC782AF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5344 = { sizeof (U3CU3Ec__DisplayClass41_0_t00E8CDDAF7F83AA87F029DA3DEC73126F5786F8F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5344[3] = 
{
	U3CU3Ec__DisplayClass41_0_t00E8CDDAF7F83AA87F029DA3DEC73126F5786F8F::get_offset_of_index_0(),
	U3CU3Ec__DisplayClass41_0_t00E8CDDAF7F83AA87F029DA3DEC73126F5786F8F::get_offset_of_button_1(),
	U3CU3Ec__DisplayClass41_0_t00E8CDDAF7F83AA87F029DA3DEC73126F5786F8F::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5345 = { sizeof (CanvasScalerExt_t7474B4D2A98F6C8EB3964E748B7F093961CB7362), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5346 = { sizeof (CanvasScalerFitter_tAEAB47D5FA2B24DE79CC881678ABE0E084C3B938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5346[5] = 
{
	CanvasScalerFitter_tAEAB47D5FA2B24DE79CC881678ABE0E084C3B938::get_offset_of_breakPoints_4(),
	CanvasScalerFitter_tAEAB47D5FA2B24DE79CC881678ABE0E084C3B938::get_offset_of_canvasScaler_5(),
	CanvasScalerFitter_tAEAB47D5FA2B24DE79CC881678ABE0E084C3B938::get_offset_of_screenWidth_6(),
	CanvasScalerFitter_tAEAB47D5FA2B24DE79CC881678ABE0E084C3B938::get_offset_of_screenHeight_7(),
	CanvasScalerFitter_tAEAB47D5FA2B24DE79CC881678ABE0E084C3B938::get_offset_of_ScreenSizeChanged_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5347 = { sizeof (BreakPoint_t1D2111C77944DFE7FF80A105F85B099572C274A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5347[3] = 
{
	BreakPoint_t1D2111C77944DFE7FF80A105F85B099572C274A6::get_offset_of_name_0(),
	BreakPoint_t1D2111C77944DFE7FF80A105F85B099572C274A6::get_offset_of_screenAspectRatio_1(),
	BreakPoint_t1D2111C77944DFE7FF80A105F85B099572C274A6::get_offset_of_referenceResolution_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5348 = { sizeof (ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7), -1, sizeof(ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5348[101] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__rewiredInputManager_14(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__dontDestroyOnLoad_15(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__openOnStart_16(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__keyboardMapDefaultLayout_17(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__mouseMapDefaultLayout_18(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__joystickMapDefaultLayout_19(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__mappingSets_20(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showPlayers_21(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showControllers_22(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showKeyboard_23(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showMouse_24(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__maxControllersPerPlayer_25(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showActionCategoryLabels_26(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__keyboardInputFieldCount_27(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__mouseInputFieldCount_28(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__controllerInputFieldCount_29(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showFullAxisInputFields_30(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showSplitAxisInputFields_31(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__allowElementAssignmentConflicts_32(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__allowElementAssignmentSwap_33(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__actionLabelWidth_34(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__keyboardColMaxWidth_35(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__mouseColMaxWidth_36(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__controllerColMaxWidth_37(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__inputRowHeight_38(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__inputRowPadding_39(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__inputRowFieldSpacing_40(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__inputColumnSpacing_41(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__inputRowCategorySpacing_42(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__invertToggleWidth_43(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__defaultWindowWidth_44(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__defaultWindowHeight_45(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__controllerAssignmentTimeout_46(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__preInputAssignmentTimeout_47(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__inputAssignmentTimeout_48(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__axisCalibrationTimeout_49(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__ignoreMouseXAxisAssignment_50(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__ignoreMouseYAxisAssignment_51(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__screenToggleAction_52(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__screenOpenAction_53(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__screenCloseAction_54(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__universalCancelAction_55(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__universalCancelClosesScreen_56(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showInputBehaviorSettings_57(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__inputBehaviorSettings_58(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__useThemeSettings_59(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__themeSettings_60(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__language_61(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_prefabs_62(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_references_63(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showPlayersGroupLabel_64(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showControllerGroupLabel_65(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showAssignedControllersGroupLabel_66(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showSettingsGroupLabel_67(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showMapCategoriesGroupLabel_68(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showControllerNameLabel_69(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__showAssignedControllers_70(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__ScreenClosedEvent_71(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__ScreenOpenedEvent_72(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__PopupWindowOpenedEvent_73(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__PopupWindowClosedEvent_74(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__InputPollingStartedEvent_75(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__InputPollingEndedEvent_76(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__onScreenClosed_77(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__onScreenOpened_78(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__onPopupWindowClosed_79(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__onPopupWindowOpened_80(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__onInputPollingStarted_81(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__onInputPollingEnded_82(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7_StaticFields::get_offset_of_Instance_83(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_initialized_84(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_playerCount_85(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_inputGrid_86(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_windowManager_87(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_currentPlayerId_88(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_currentMapCategoryId_89(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_playerButtons_90(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_mapCategoryButtons_91(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_assignedControllerButtons_92(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_assignedControllerButtonsPlaceholder_93(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_miscInstantiatedObjects_94(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_canvas_95(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_lastUISelection_96(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_currentJoystickId_97(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_blockInputOnFocusEndTime_98(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_isPollingForInput_99(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_pendingInputMapping_100(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_pendingAxisCalibration_101(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_inputFieldActivatedDelegate_102(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of_inputFieldInvertToggleStateChangedDelegate_103(),
	ControlMapper_t5FD875EE667E95B2CA249A1C4B7F5F05B27F61F7::get_offset_of__restoreDefaultsDelegate_104(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5349 = { sizeof (GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5349[7] = 
{
	GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9::get_offset_of_gameObject_0(),
	GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9::get_offset_of_text_1(),
	GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9::get_offset_of_selectable_2(),
	GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9::get_offset_of_uiElementInfo_3(),
	GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9::get_offset_of_permanentStateSet_4(),
	GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9::get_offset_of_children_5(),
	GUIElement_t51DA7ED3E1073A18D53ABC6E454839C630CB71A9::get_offset_of_U3CrectTransformU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5350 = { sizeof (GUIButton_tF14C9F95FC308D0C330BDF4EEDB3B9F5ECBF705C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5351 = { sizeof (U3CU3Ec__DisplayClass7_0_tA79798299BC1B8AA685BC65954389E15380910F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5351[2] = 
{
	U3CU3Ec__DisplayClass7_0_tA79798299BC1B8AA685BC65954389E15380910F9::get_offset_of_callback_0(),
	U3CU3Ec__DisplayClass7_0_tA79798299BC1B8AA685BC65954389E15380910F9::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5352 = { sizeof (GUIInputField_tF982ED4AB954FC21D0AE7F35EAB4C8234BDF4F20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5352[1] = 
{
	GUIInputField_tF982ED4AB954FC21D0AE7F35EAB4C8234BDF4F20::get_offset_of_U3CtoggleU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5353 = { sizeof (U3CU3Ec__DisplayClass19_0_tA6C5E62246A5BECED1570D6A4AD0F0BD0BB6AD25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5353[2] = 
{
	U3CU3Ec__DisplayClass19_0_tA6C5E62246A5BECED1570D6A4AD0F0BD0BB6AD25::get_offset_of_callback_0(),
	U3CU3Ec__DisplayClass19_0_tA6C5E62246A5BECED1570D6A4AD0F0BD0BB6AD25::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5354 = { sizeof (GUIToggle_tD90790241D2ED7478964399C10A03986E91C88F9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5355 = { sizeof (U3CU3Ec__DisplayClass10_0_t50AD2C3FB1B2CC9FB668EA518444ECDA8A057F13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5355[2] = 
{
	U3CU3Ec__DisplayClass10_0_t50AD2C3FB1B2CC9FB668EA518444ECDA8A057F13::get_offset_of_callback_0(),
	U3CU3Ec__DisplayClass10_0_t50AD2C3FB1B2CC9FB668EA518444ECDA8A057F13::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5356 = { sizeof (GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5356[3] = 
{
	GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8::get_offset_of_U3CgameObjectU3Ek__BackingField_0(),
	GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8::get_offset_of_U3CtextU3Ek__BackingField_1(),
	GUILabel_t30D8F39A3624BCF25878D6F4D164D4B6230758E8::get_offset_of_U3CrectTransformU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5357 = { sizeof (MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5357[6] = 
{
	MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04::get_offset_of__mapCategoryId_0(),
	MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04::get_offset_of__actionListMode_1(),
	MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04::get_offset_of__actionCategoryIds_2(),
	MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04::get_offset_of__actionIds_3(),
	MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04::get_offset_of__actionCategoryIdsReadOnly_4(),
	MappingSet_t54B0C9EECC7B6CE826A2ADBAEB38A17DB6E50D04::get_offset_of__actionIdsReadOnly_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5358 = { sizeof (ActionListMode_tD5639A744267D253836F3DEEA35F075724B7A36D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5358[3] = 
{
	ActionListMode_tD5639A744267D253836F3DEEA35F075724B7A36D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5359 = { sizeof (InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5359[12] = 
{
	InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A::get_offset_of__inputBehaviorId_0(),
	InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A::get_offset_of__showJoystickAxisSensitivity_1(),
	InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A::get_offset_of__showMouseXYAxisSensitivity_2(),
	InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A::get_offset_of__labelLanguageKey_3(),
	InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A::get_offset_of__joystickAxisSensitivityLabelLanguageKey_4(),
	InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A::get_offset_of__mouseXYAxisSensitivityLabelLanguageKey_5(),
	InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A::get_offset_of__joystickAxisSensitivityIcon_6(),
	InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A::get_offset_of__mouseXYAxisSensitivityIcon_7(),
	InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A::get_offset_of__joystickAxisSensitivityMin_8(),
	InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A::get_offset_of__joystickAxisSensitivityMax_9(),
	InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A::get_offset_of__mouseXYAxisSensitivityMin_10(),
	InputBehaviorSettings_t51F59440D044CD4B45491FEC471612E29467223A::get_offset_of__mouseXYAxisSensitivityMax_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5360 = { sizeof (Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5360[14] = 
{
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__button_0(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__fitButton_1(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__inputGridLabel_2(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__inputGridHeaderLabel_3(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__inputGridFieldButton_4(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__inputGridFieldInvertToggle_5(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__window_6(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__windowTitleText_7(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__windowContentText_8(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__fader_9(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__calibrationWindow_10(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__inputBehaviorsWindow_11(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__centerStickGraphic_12(),
	Prefabs_t48C9F4A7795730DDE4ADD53717898BD13CFC1B6A::get_offset_of__moveStickGraphic_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5361 = { sizeof (References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5361[36] = 
{
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__canvas_0(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__mainCanvasGroup_1(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__mainContent_2(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__mainContentInner_3(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__playersGroup_4(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__controllerGroup_5(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__controllerGroupLabelGroup_6(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__controllerSettingsGroup_7(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__assignedControllersGroup_8(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__settingsAndMapCategoriesGroup_9(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__settingsGroup_10(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__mapCategoriesGroup_11(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__inputGridGroup_12(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__inputGridContainer_13(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__inputGridHeadersGroup_14(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__inputGridVScrollbar_15(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__inputGridScrollRect_16(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__inputGridInnerGroup_17(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__controllerNameLabel_18(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__removeControllerButton_19(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__assignControllerButton_20(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__calibrateControllerButton_21(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__doneButton_22(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__restoreDefaultsButton_23(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__defaultSelection_24(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__fixedSelectableUIElements_25(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of__mainBackgroundImage_26(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of_U3CinputGridLayoutElementU3Ek__BackingField_27(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of_U3CinputGridActionColumnU3Ek__BackingField_28(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of_U3CinputGridKeyboardColumnU3Ek__BackingField_29(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of_U3CinputGridMouseColumnU3Ek__BackingField_30(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of_U3CinputGridControllerColumnU3Ek__BackingField_31(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of_U3CinputGridHeader1U3Ek__BackingField_32(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of_U3CinputGridHeader2U3Ek__BackingField_33(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of_U3CinputGridHeader3U3Ek__BackingField_34(),
	References_t63B1022C3D9DF66B0A7AEE0E3A102414E7455163::get_offset_of_U3CinputGridHeader4U3Ek__BackingField_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5362 = { sizeof (InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5362[2] = 
{
	InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B::get_offset_of__actionId_0(),
	InputActionSet_t99A900AB1469B785BC7A427E6E780DE2FCEC428B::get_offset_of__axisRange_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5363 = { sizeof (InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5363[8] = 
{
	InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A::get_offset_of_U3CactionNameU3Ek__BackingField_0(),
	InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A::get_offset_of_U3CfieldInfoU3Ek__BackingField_1(),
	InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A::get_offset_of_U3CmapU3Ek__BackingField_2(),
	InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A::get_offset_of_U3CaemU3Ek__BackingField_3(),
	InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A::get_offset_of_U3CcontrollerTypeU3Ek__BackingField_4(),
	InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A::get_offset_of_U3CcontrollerIdU3Ek__BackingField_5(),
	InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A::get_offset_of_U3CpollingInfoU3Ek__BackingField_6(),
	InputMapping_t84EA554839852D43F0B5C38908686A331A3EEA0A::get_offset_of_U3CmodifierKeyFlagsU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5364 = { sizeof (AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5364[5] = 
{
	AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237::get_offset_of_data_0(),
	AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237::get_offset_of_joystick_1(),
	AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237::get_offset_of_axisIndex_2(),
	AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237::get_offset_of_axis_3(),
	AxisCalibrator_t02095EA040609684DCE9299C1B8C6CB31647A237::get_offset_of_firstRun_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5365 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5365[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5366 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5366[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5367 = { sizeof (LayoutElementSizeType_tF37F8C6EA5EA1D90AAFCE69FEF8FC911F87BC99C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5367[3] = 
{
	LayoutElementSizeType_tF37F8C6EA5EA1D90AAFCE69FEF8FC911F87BC99C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5368 = { sizeof (WindowType_t7ED3BE837E148848A28B2F77643AC474BA0F23A6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5368[12] = 
{
	WindowType_t7ED3BE837E148848A28B2F77643AC474BA0F23A6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5369 = { sizeof (InputGrid_t279399655219F94B8033968DF46749593CF22E63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5369[2] = 
{
	InputGrid_t279399655219F94B8033968DF46749593CF22E63::get_offset_of_list_0(),
	InputGrid_t279399655219F94B8033968DF46749593CF22E63::get_offset_of_groups_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5370 = { sizeof (InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5370[1] = 
{
	InputGridEntryList_t24C84E92E3F06279512E824C16C7B0AD512140AF::get_offset_of_entries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5371 = { sizeof (MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5371[3] = 
{
	MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906::get_offset_of__actionList_0(),
	MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906::get_offset_of__actionCategoryList_1(),
	MapCategoryEntry_t19791ED0F3BB2E26853F251AF939D7B0AE943906::get_offset_of__columnHeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5372 = { sizeof (ActionEntry_t78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5372[5] = 
{
	ActionEntry_t78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25::get_offset_of_fieldSets_0(),
	ActionEntry_t78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25::get_offset_of_label_1(),
	ActionEntry_t78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25::get_offset_of_action_2(),
	ActionEntry_t78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25::get_offset_of_axisRange_3(),
	ActionEntry_t78E6D1E3420733577BFAF9A0BE5A3EA01DFF5A25::get_offset_of_actionSet_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5373 = { sizeof (FieldSet_t32DFC4D88DB2D0C89C667DFBADA9D4A95B80169E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5373[2] = 
{
	FieldSet_t32DFC4D88DB2D0C89C667DFBADA9D4A95B80169E::get_offset_of_groupContainer_0(),
	FieldSet_t32DFC4D88DB2D0C89C667DFBADA9D4A95B80169E::get_offset_of_fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5374 = { sizeof (ActionCategoryEntry_tF8BCACB2FF877C3241621A673AC1287F385AF40F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5374[2] = 
{
	ActionCategoryEntry_tF8BCACB2FF877C3241621A673AC1287F385AF40F::get_offset_of_actionCategoryId_0(),
	ActionCategoryEntry_tF8BCACB2FF877C3241621A673AC1287F385AF40F::get_offset_of_label_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5375 = { sizeof (U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5375[10] = 
{
	U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D::get_offset_of_U3CU3E1__state_0(),
	U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D::get_offset_of_U3CU3E2__current_1(),
	U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D::get_offset_of_mapCategoryId_3(),
	U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D::get_offset_of_U3CU3E3__mapCategoryId_4(),
	U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D::get_offset_of_U3CU3E4__this_5(),
	U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D::get_offset_of_U3CentryU3E5__1_6(),
	U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D::get_offset_of_U3ClistU3E5__2_7(),
	U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D::get_offset_of_U3CcountU3E5__3_8(),
	U3CGetActionSetsU3Ed__18_t2BD57B2694DE2330809C37D5ED792CB7D8A7983D::get_offset_of_U3CiU3E5__4_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5376 = { sizeof (WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5376[5] = 
{
	WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4::get_offset_of_windows_0(),
	WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4::get_offset_of_windowPrefab_1(),
	WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4::get_offset_of_parent_2(),
	WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4::get_offset_of_fader_3(),
	WindowManager_t3C0519DFBA356D8CC4ED849951F7D3D64D38ECB4::get_offset_of_idCounter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5377 = { sizeof (U3CU3Ec__DisplayClass338_0_tC041F08935BE04F38DAFB200176A9F0EBA83CCF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5377[3] = 
{
	U3CU3Ec__DisplayClass338_0_tC041F08935BE04F38DAFB200176A9F0EBA83CCF0::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass338_0_tC041F08935BE04F38DAFB200176A9F0EBA83CCF0::get_offset_of_window_1(),
	U3CU3Ec__DisplayClass338_0_tC041F08935BE04F38DAFB200176A9F0EBA83CCF0::get_offset_of_controllerId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5378 = { sizeof (U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5378[6] = 
{
	U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB::get_offset_of_window_1(),
	U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB::get_offset_of_fieldInfo_2(),
	U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB::get_offset_of_map_3(),
	U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB::get_offset_of_aem_4(),
	U3CU3Ec__DisplayClass339_0_t949793F8D6D9E5CC3F44E842AE81143FAAA4D7FB::get_offset_of_actionName_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5379 = { sizeof (U3CU3Ec__DisplayClass346_0_tE8414A504024A743399EA1C32D6978A7288844D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5379[4] = 
{
	U3CU3Ec__DisplayClass346_0_tE8414A504024A743399EA1C32D6978A7288844D6::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass346_0_tE8414A504024A743399EA1C32D6978A7288844D6::get_offset_of_window_1(),
	U3CU3Ec__DisplayClass346_0_tE8414A504024A743399EA1C32D6978A7288844D6::get_offset_of_assignment_2(),
	U3CU3Ec__DisplayClass346_0_tE8414A504024A743399EA1C32D6978A7288844D6::get_offset_of_skipOtherPlayers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5380 = { sizeof (U3CU3Ec__DisplayClass347_0_t4674A2E70097B9A296BA7B9BDA3C6FCC1BD359B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5380[2] = 
{
	U3CU3Ec__DisplayClass347_0_t4674A2E70097B9A296BA7B9BDA3C6FCC1BD359B4::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass347_0_t4674A2E70097B9A296BA7B9BDA3C6FCC1BD359B4::get_offset_of_window_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5381 = { sizeof (U3CU3Ec__DisplayClass400_0_t28223F937AE7412CA660B35BEDC2C07F47E714CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5381[2] = 
{
	U3CU3Ec__DisplayClass400_0_t28223F937AE7412CA660B35BEDC2C07F47E714CF::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass400_0_t28223F937AE7412CA660B35BEDC2C07F47E714CF::get_offset_of_window_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5382 = { sizeof (U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5382[19] = 
{
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CU3E1__state_0(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CU3E2__current_1(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_player_3(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CU3E3__player_4(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_mapping_5(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CU3E3__mapping_6(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_assignment_7(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CU3E3__assignment_8(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_skipOtherPlayers_9(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CU3E3__skipOtherPlayers_10(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CU3E4__this_11(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CconflictCheckU3E5__1_12(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CU3Es__2_13(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CconflictU3E5__3_14(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CU3Es__4_15(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CconflictU3E5__5_16(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CU3Es__6_17(),
	U3CElementAssignmentConflictsU3Ed__408_tEEA91562F994AE9AEE1D720018BA491F85A5BBD0::get_offset_of_U3CconflictU3E5__7_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5383 = { sizeof (U3CU3Ec__DisplayClass453_0_t2EEF9DCBDDD35A3C3D72D67474D20B1C2CD45D6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5383[1] = 
{
	U3CU3Ec__DisplayClass453_0_t2EEF9DCBDDD35A3C3D72D67474D20B1C2CD45D6D::get_offset_of_aem_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5384 = { sizeof (CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5384[9] = 
{
	CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533::get_offset_of__disabledHighlightedSprite_19(),
	CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533::get_offset_of__disabledHighlightedColor_20(),
	CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533::get_offset_of__disabledHighlightedTrigger_21(),
	CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533::get_offset_of__autoNavUp_22(),
	CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533::get_offset_of__autoNavDown_23(),
	CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533::get_offset_of__autoNavLeft_24(),
	CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533::get_offset_of__autoNavRight_25(),
	CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533::get_offset_of_isHighlightDisabled_26(),
	CustomButton_t9D3E192F0D1B801DD76E169A1A807BACA952A533::get_offset_of__CancelEvent_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5385 = { sizeof (U3COnFinishSubmitU3Ed__51_t3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5385[5] = 
{
	U3COnFinishSubmitU3Ed__51_t3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9::get_offset_of_U3CU3E1__state_0(),
	U3COnFinishSubmitU3Ed__51_t3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9::get_offset_of_U3CU3E2__current_1(),
	U3COnFinishSubmitU3Ed__51_t3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9::get_offset_of_U3CU3E4__this_2(),
	U3COnFinishSubmitU3Ed__51_t3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9::get_offset_of_U3CfadeTimeU3E5__1_3(),
	U3COnFinishSubmitU3Ed__51_t3B37B4FA95A041C5ADDDDD81889DAD3FA42614D9::get_offset_of_U3CelapsedTimeU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5386 = { sizeof (CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5386[9] = 
{
	CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0::get_offset_of__disabledHighlightedSprite_33(),
	CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0::get_offset_of__disabledHighlightedColor_34(),
	CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0::get_offset_of__disabledHighlightedTrigger_35(),
	CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0::get_offset_of__autoNavUp_36(),
	CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0::get_offset_of__autoNavDown_37(),
	CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0::get_offset_of__autoNavLeft_38(),
	CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0::get_offset_of__autoNavRight_39(),
	CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0::get_offset_of_isHighlightDisabled_40(),
	CustomSlider_t0CAD108D88656B458641832AC93E466E05A258B0::get_offset_of__CancelEvent_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5387 = { sizeof (CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5387[9] = 
{
	CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E::get_offset_of__disabledHighlightedSprite_23(),
	CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E::get_offset_of__disabledHighlightedColor_24(),
	CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E::get_offset_of__disabledHighlightedTrigger_25(),
	CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E::get_offset_of__autoNavUp_26(),
	CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E::get_offset_of__autoNavDown_27(),
	CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E::get_offset_of__autoNavLeft_28(),
	CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E::get_offset_of__autoNavRight_29(),
	CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E::get_offset_of_isHighlightDisabled_30(),
	CustomToggle_tE7476FE1D2C6D949F00ABA84499194061484572E::get_offset_of__CancelEvent_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5388 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5389 = { sizeof (InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5389[13] = 
{
	0,
	InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A::get_offset_of_spawnTransform_19(),
	InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A::get_offset_of_doneButton_20(),
	InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A::get_offset_of_cancelButton_21(),
	InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A::get_offset_of_defaultButton_22(),
	InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A::get_offset_of_doneButtonLabel_23(),
	InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A::get_offset_of_cancelButtonLabel_24(),
	InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A::get_offset_of_defaultButtonLabel_25(),
	InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A::get_offset_of_uiControlSetPrefab_26(),
	InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A::get_offset_of_uiSliderControlPrefab_27(),
	InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A::get_offset_of_inputBehaviorInfo_28(),
	InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A::get_offset_of_buttonCallbacks_29(),
	InputBehaviorWindow_t1837B5E28A6EB46B7037D62A71003FD6CEA2369A::get_offset_of_playerId_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5390 = { sizeof (InputBehaviorInfo_t386E3681EC6A58C540468D3E2FFF1E4A9A386686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5390[4] = 
{
	InputBehaviorInfo_t386E3681EC6A58C540468D3E2FFF1E4A9A386686::get_offset_of__inputBehavior_0(),
	InputBehaviorInfo_t386E3681EC6A58C540468D3E2FFF1E4A9A386686::get_offset_of__controlSet_1(),
	InputBehaviorInfo_t386E3681EC6A58C540468D3E2FFF1E4A9A386686::get_offset_of_idToProperty_2(),
	InputBehaviorInfo_t386E3681EC6A58C540468D3E2FFF1E4A9A386686::get_offset_of_copyOfOriginal_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5391 = { sizeof (ButtonIdentifier_t51F6013AF45864B8A437880FAF31D3A2E9AFFF48)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5391[4] = 
{
	ButtonIdentifier_t51F6013AF45864B8A437880FAF31D3A2E9AFFF48::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5392 = { sizeof (PropertyType_t93D0A8A37A95CEDEE518EAF68F00B83E59575E5B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5392[3] = 
{
	PropertyType_t93D0A8A37A95CEDEE518EAF68F00B83E59575E5B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5393 = { sizeof (U3CU3Ec__DisplayClass26_0_t8A7360706353E21CFBD6D9FE041BECFC92951701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5393[3] = 
{
	U3CU3Ec__DisplayClass26_0_t8A7360706353E21CFBD6D9FE041BECFC92951701::get_offset_of_valueChangedCallback_0(),
	U3CU3Ec__DisplayClass26_0_t8A7360706353E21CFBD6D9FE041BECFC92951701::get_offset_of_inputBehaviorId_1(),
	U3CU3Ec__DisplayClass26_0_t8A7360706353E21CFBD6D9FE041BECFC92951701::get_offset_of_cancelCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5394 = { sizeof (InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5394[5] = 
{
	InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5::get_offset_of_U3CactionIdU3Ek__BackingField_8(),
	InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5::get_offset_of_U3CaxisRangeU3Ek__BackingField_9(),
	InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5::get_offset_of_U3CactionElementMapIdU3Ek__BackingField_10(),
	InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5::get_offset_of_U3CcontrollerTypeU3Ek__BackingField_11(),
	InputFieldInfo_t0BEE7FE4C35A62C23EB60ACA801A51D4AD1370A5::get_offset_of_U3CcontrollerIdU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5395 = { sizeof (InputRow_t3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5395[4] = 
{
	InputRow_t3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF::get_offset_of_label_4(),
	InputRow_t3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF::get_offset_of_U3CbuttonsU3Ek__BackingField_5(),
	InputRow_t3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF::get_offset_of_rowIndex_6(),
	InputRow_t3ADA393A75F52FD5615FA4D1D89E1D71D58AC3FF::get_offset_of_inputFieldActivatedCallback_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5396 = { sizeof (LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5396[59] = 
{
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__yes_4(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__no_5(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__add_6(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__replace_7(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__remove_8(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__swap_9(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__cancel_10(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__none_11(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__okay_12(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__done_13(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__default_14(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__assignControllerWindowTitle_15(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__assignControllerWindowMessage_16(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__controllerAssignmentConflictWindowTitle_17(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__controllerAssignmentConflictWindowMessage_18(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__elementAssignmentPrePollingWindowMessage_19(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__joystickElementAssignmentPollingWindowMessage_20(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__joystickElementAssignmentPollingWindowMessage_fullAxisFieldOnly_21(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__keyboardElementAssignmentPollingWindowMessage_22(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__mouseElementAssignmentPollingWindowMessage_23(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__mouseElementAssignmentPollingWindowMessage_fullAxisFieldOnly_24(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__elementAssignmentConflictWindowMessage_25(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__elementAlreadyInUseBlocked_26(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__elementAlreadyInUseCanReplace_27(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__elementAlreadyInUseCanReplace_conflictAllowed_28(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__mouseAssignmentConflictWindowTitle_29(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__mouseAssignmentConflictWindowMessage_30(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__calibrateControllerWindowTitle_31(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__calibrateAxisStep1WindowTitle_32(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__calibrateAxisStep1WindowMessage_33(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__calibrateAxisStep2WindowTitle_34(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__calibrateAxisStep2WindowMessage_35(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__inputBehaviorSettingsWindowTitle_36(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__restoreDefaultsWindowTitle_37(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__restoreDefaultsWindowMessage_onePlayer_38(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__restoreDefaultsWindowMessage_multiPlayer_39(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__actionColumnLabel_40(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__keyboardColumnLabel_41(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__mouseColumnLabel_42(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__controllerColumnLabel_43(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__removeControllerButtonLabel_44(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__calibrateControllerButtonLabel_45(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__assignControllerButtonLabel_46(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__inputBehaviorSettingsButtonLabel_47(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__doneButtonLabel_48(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__restoreDefaultsButtonLabel_49(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__playersGroupLabel_50(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__controllerSettingsGroupLabel_51(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__assignedControllersGroupLabel_52(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__settingsGroupLabel_53(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__mapCategoriesGroupLabel_54(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__calibrateWindow_deadZoneSliderLabel_55(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__calibrateWindow_zeroSliderLabel_56(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__calibrateWindow_sensitivitySliderLabel_57(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__calibrateWindow_invertToggleLabel_58(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__calibrateWindow_calibrateButtonLabel_59(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__customEntries_60(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of__initialized_61(),
	LanguageData_tAC36375FDC13175378678CDE03A64E3856634A4B::get_offset_of_customDict_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5397 = { sizeof (CustomEntry_tE994DCD35C117E456147357110B75848AF99FBFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5397[2] = 
{
	CustomEntry_tE994DCD35C117E456147357110B75848AF99FBFD::get_offset_of_key_0(),
	CustomEntry_tE994DCD35C117E456147357110B75848AF99FBFD::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5398 = { sizeof (ScrollbarVisibilityHelper_t77F02C75917777EBD1C832ABEA3C6E518FDD08B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5398[1] = 
{
	ScrollbarVisibilityHelper_t77F02C75917777EBD1C832ABEA3C6E518FDD08B6::get_offset_of_scrollRect_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5399 = { sizeof (ScrollRectSelectableChild_tCE1174CD9F620F2909B5934EF0A61BCD9523946C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5399[4] = 
{
	ScrollRectSelectableChild_tCE1174CD9F620F2909B5934EF0A61BCD9523946C::get_offset_of_useCustomEdgePadding_4(),
	ScrollRectSelectableChild_tCE1174CD9F620F2909B5934EF0A61BCD9523946C::get_offset_of_customEdgePadding_5(),
	ScrollRectSelectableChild_tCE1174CD9F620F2909B5934EF0A61BCD9523946C::get_offset_of_parentScrollRect_6(),
	ScrollRectSelectableChild_tCE1174CD9F620F2909B5934EF0A61BCD9523946C::get_offset_of__selectable_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
