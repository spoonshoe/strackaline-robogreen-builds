﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Rewired.Data.Mapping.CustomCalculation
struct CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038;
// Rewired.Data.Mapping.HardwareAxisInfo
struct HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4;
// Rewired.Data.Mapping.HardwareButtonInfo
struct HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273;
// Rewired.Data.Mapping.HardwareJoystickMap/AxisCalibrationInfoEntry[]
struct AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Custom/Axis
struct Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Custom/Axis[]
struct AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Custom/Button
struct Button_tBB6149DD7A45B873206F15797DC2B002019BF09C;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Custom/Button[]
struct ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Custom/CustomCalculationSourceData[]
struct CustomCalculationSourceDataU5BU5D_t9CB27E1BF97E6EEF9BF27CECA4DC1B02B002F8D0;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Fallback_Base
struct Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Fallback_Base/Axis
struct Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Fallback_Base/Axis[]
struct AxisU5BU5D_t88F2D8A47455D8334C098EF9135A5D118C79F63D;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Fallback_Base/Button
struct Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Fallback_Base/Button[]
struct ButtonU5BU5D_t26B11DC5F153644B133E626A8A304A1789164DCD;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Fallback_Base/CustomCalculationSourceData[]
struct CustomCalculationSourceDataU5BU5D_t3E25F9818C6BA4273D61F758395B67649F70CC38;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Fallback_Base/Elements
struct Elements_tA67426A050B3846F67FA5F1D602FC4752C236EA7;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Fallback_Base/MatchingCriteria
struct MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Fallback_Base[]
struct Platform_Fallback_BaseU5BU5D_t57FC29A1D770568738D7217ED1578A5FE963176F;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Linux_Base
struct Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Linux_Base/Axis
struct Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Linux_Base/Axis[]
struct AxisU5BU5D_t6D0AE6929E9536CD3A6FE8D76060F5A397CB15B4;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Linux_Base/Button
struct Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Linux_Base/Button[]
struct ButtonU5BU5D_t994BE4F6605F4647B2EC411577AD246F36469884;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Linux_Base/Elements
struct Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Linux_Base/MatchingCriteria
struct MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Linux_Base/MatchingCriteria/ElementCount[]
struct ElementCountU5BU5D_tA1D45617688A9FC87702229BE07F7187346472C6;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Linux_Base[]
struct Platform_Linux_BaseU5BU5D_t2E32C60C9AE700CFF2E4AB3FC8105FF63370C315;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_NintendoSwitch_Base
struct Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_NintendoSwitch_Base/Axis[]
struct AxisU5BU5D_t72E4B431F2EB0274312CAD56DFF38BF25C2299DE;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_NintendoSwitch_Base/Button[]
struct ButtonU5BU5D_tE2C1D485FA293714108314F60C99A96EE913FF3F;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_NintendoSwitch_Base/Elements
struct Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_NintendoSwitch_Base/MatchingCriteria
struct MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_OSX_Base
struct Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_OSX_Base/Axis
struct Axis_t766FE92C87B9298B56A4F07611581240BD735EFD;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_OSX_Base/Axis[]
struct AxisU5BU5D_tE985187DDC620CCDAAC8D4E2FC59E979A211FE90;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_OSX_Base/Button
struct Button_t841D3D6DD5392867B22FA416457754550B1B8F3A;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_OSX_Base/Button[]
struct ButtonU5BU5D_tA5A850BF0855D9A2C6F95A30C52D6EDF9C4C9101;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_OSX_Base/Elements
struct Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_OSX_Base/MatchingCriteria
struct MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_OSX_Base/MatchingCriteria/ElementCount[]
struct ElementCountU5BU5D_tFD1B6C7C083056931387BDBC24A6AE1A8B28CBEA;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_OSX_Base[]
struct Platform_OSX_BaseU5BU5D_tD648889E359C28A8F1A5698CCBB7B5A4EEE56B04;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Ouya_Base
struct Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Ouya_Base/Axis[]
struct AxisU5BU5D_t0A247C1FB163CBE2493DA8D44B47F1A42234D0B0;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Ouya_Base/Button[]
struct ButtonU5BU5D_t3D117F1380549EEBF8E6ED08B14152948A8F900D;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Ouya_Base/Elements
struct Elements_t337BF774C168903EA673A8A56D19C01534EA3196;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Ouya_Base/MatchingCriteria
struct MatchingCriteria_t7E3617113D979BC9EBEACC19768821EA3A4A3ECE;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_Ouya_Base[]
struct Platform_Ouya_BaseU5BU5D_t6463B5A1A1AA1104079C1095423FC5BDDE9C76FE;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_PS4_Base
struct Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_PS4_Base/Axis[]
struct AxisU5BU5D_t6DCE7F5005DDD9EFD5D45FB825A1555C4666E744;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_PS4_Base/Button[]
struct ButtonU5BU5D_tE984A31B7512246FB5B5F82116AC201DB1346B96;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_PS4_Base/Elements
struct Elements_t1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_PS4_Base/MatchingCriteria
struct MatchingCriteria_t728FB1BA60E0E29B55788562DE5AF371ACFC1A0C;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_PS4_Base[]
struct Platform_PS4_BaseU5BU5D_tF68B32F3AD14ACFAD4AA06B6A9B98B23C50CE612;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawInput_Base
struct Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawInput_Base/Axis[]
struct AxisU5BU5D_t0DA25F52128CD0DAAF70937E550CD33679168D66;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawInput_Base/Button[]
struct ButtonU5BU5D_t7DCB05F6CC92B7FA42DF10FC6734BAC2925F1DA1;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawInput_Base/Elements
struct Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawInput_Base[]
struct Platform_RawInput_BaseU5BU5D_t3E3DC3F59AEE259B5EFDEF78EA037CB9159B51BC;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawOrDirectInput/Axis_Base
struct Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawOrDirectInput/Button_Base
struct Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawOrDirectInput/CustomCalculationSourceData[]
struct CustomCalculationSourceDataU5BU5D_t034AAEDA41E4C78D3D656BBE7D542F430FDB5CC9;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_RawOrDirectInput/MatchingCriteria
struct MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WindowsUWP_Base
struct Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WindowsUWP_Base/Axis
struct Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WindowsUWP_Base/Axis[]
struct AxisU5BU5D_t422FE88AC2B5158414C55A3359ED8FCC9EBE92BC;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WindowsUWP_Base/Button
struct Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WindowsUWP_Base/Button[]
struct ButtonU5BU5D_t623D6E4399A0B08612A2AE21A137E2E5220BBCA0;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WindowsUWP_Base/Elements
struct Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WindowsUWP_Base/MatchingCriteria
struct MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WindowsUWP_Base/MatchingCriteria/ElementCount[]
struct ElementCountU5BU5D_tE428EE68916C80276BAFB23B00B444763324242D;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_WindowsUWP_Base[]
struct Platform_WindowsUWP_BaseU5BU5D_t3989D551C12994CFEDAF7AA6E39F7798853C4AB9;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XInput_Base
struct Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XInput_Base/Axis
struct Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XInput_Base/Axis[]
struct AxisU5BU5D_tE2E6FD9F2F8617EC8BC700A2AFFDA15A3459B4F2;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XInput_Base/Button
struct Button_tF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XInput_Base/Button[]
struct ButtonU5BU5D_t5A012C1E636329C7B26DEA0A59A715AB010FDABE;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XInput_Base/Elements
struct Elements_tC9F4725938858F8E86BF77F632A2F4B0B3B6B73F;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XInput_Base/MatchingCriteria
struct MatchingCriteria_t40557EF4BBBEA0BA5E20157EB81A34802805124A;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XInput_Base[]
struct Platform_XInput_BaseU5BU5D_tFBF2B1FCF43579AE240C52DF195C2B9C129B2712;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XboxOne_Base
struct Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XboxOne_Base/Axis[]
struct AxisU5BU5D_t1D5524808F860ADD8316300BD40968DECEFC10C9;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XboxOne_Base/Button[]
struct ButtonU5BU5D_tAAAAE4A765344C4A3D9A74AAD45536715942DE9E;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XboxOne_Base/Elements
struct Elements_tF1026C7DB7135259ABB2A130FE4C24B0C0260DF2;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XboxOne_Base/MatchingCriteria
struct MatchingCriteria_t8DBC25330E8572B4C927356459E8192B136CFBAD;
// Rewired.Data.Mapping.HardwareJoystickMap/Platform_XboxOne_Base[]
struct Platform_XboxOne_BaseU5BU5D_tAD510FA6BA296CB92BD20B7F4AED507764D5CE89;
// Rewired.Platforms.UnityButton[]
struct UnityButtonU5BU5D_t24D74C17E7990493573E9CF978882B03D3BA38DB;
// Rewired.Platforms.XInputDeviceSubType[]
struct XInputDeviceSubTypeU5BU5D_tF82422E233148C7071A9B7A36BB24F16E615E150;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ELEMENTS_BASE_T646A0ACACC7BEB9CE31C70CB74E59EF0312756F0_H
#define ELEMENTS_BASE_T646A0ACACC7BEB9CE31C70CB74E59EF0312756F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Elements_Base
struct  Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_BASE_T646A0ACACC7BEB9CE31C70CB74E59EF0312756F0_H
#ifndef MATCHINGCRITERIA_BASE_T9475B14364CDF4EB6C2585C9F29E9394954F3065_H
#define MATCHINGCRITERIA_BASE_T9475B14364CDF4EB6C2585C9F29E9394954F3065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base
struct  MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base::axisCount
	int32_t ___axisCount_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base::buttonCount
	int32_t ___buttonCount_1;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base::disabled
	bool ___disabled_2;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base::tag
	String_t* ___tag_3;

public:
	inline static int32_t get_offset_of_axisCount_0() { return static_cast<int32_t>(offsetof(MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065, ___axisCount_0)); }
	inline int32_t get_axisCount_0() const { return ___axisCount_0; }
	inline int32_t* get_address_of_axisCount_0() { return &___axisCount_0; }
	inline void set_axisCount_0(int32_t value)
	{
		___axisCount_0 = value;
	}

	inline static int32_t get_offset_of_buttonCount_1() { return static_cast<int32_t>(offsetof(MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065, ___buttonCount_1)); }
	inline int32_t get_buttonCount_1() const { return ___buttonCount_1; }
	inline int32_t* get_address_of_buttonCount_1() { return &___buttonCount_1; }
	inline void set_buttonCount_1(int32_t value)
	{
		___buttonCount_1 = value;
	}

	inline static int32_t get_offset_of_disabled_2() { return static_cast<int32_t>(offsetof(MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065, ___disabled_2)); }
	inline bool get_disabled_2() const { return ___disabled_2; }
	inline bool* get_address_of_disabled_2() { return &___disabled_2; }
	inline void set_disabled_2(bool value)
	{
		___disabled_2 = value;
	}

	inline static int32_t get_offset_of_tag_3() { return static_cast<int32_t>(offsetof(MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065, ___tag_3)); }
	inline String_t* get_tag_3() const { return ___tag_3; }
	inline String_t** get_address_of_tag_3() { return &___tag_3; }
	inline void set_tag_3(String_t* value)
	{
		___tag_3 = value;
		Il2CppCodeGenWriteBarrier((&___tag_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_BASE_T9475B14364CDF4EB6C2585C9F29E9394954F3065_H
#ifndef ELEMENTCOUNT_BASE_T73ADB57E490B282EC131BA5EAF3AFABDD618ECBC_H
#define ELEMENTCOUNT_BASE_T73ADB57E490B282EC131BA5EAF3AFABDD618ECBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base_ElementCount_Base
struct  ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base_ElementCount_Base::axisCount
	int32_t ___axisCount_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_MatchingCriteria_Base_ElementCount_Base::buttonCount
	int32_t ___buttonCount_1;

public:
	inline static int32_t get_offset_of_axisCount_0() { return static_cast<int32_t>(offsetof(ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC, ___axisCount_0)); }
	inline int32_t get_axisCount_0() const { return ___axisCount_0; }
	inline int32_t* get_address_of_axisCount_0() { return &___axisCount_0; }
	inline void set_axisCount_0(int32_t value)
	{
		___axisCount_0 = value;
	}

	inline static int32_t get_offset_of_buttonCount_1() { return static_cast<int32_t>(offsetof(ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC, ___buttonCount_1)); }
	inline int32_t get_buttonCount_1() const { return ___buttonCount_1; }
	inline int32_t* get_address_of_buttonCount_1() { return &___buttonCount_1; }
	inline void set_buttonCount_1(int32_t value)
	{
		___buttonCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTCOUNT_BASE_T73ADB57E490B282EC131BA5EAF3AFABDD618ECBC_H
#ifndef PLATFORM_T0E28F5AD2665BAE68E81D3FF477CA1E6E1574789_H
#define PLATFORM_T0E28F5AD2665BAE68E81D3FF477CA1E6E1574789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform
struct  Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789  : public RuntimeObject
{
public:
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_Platform::description
	String_t* ___description_0;

public:
	inline static int32_t get_offset_of_description_0() { return static_cast<int32_t>(offsetof(Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789, ___description_0)); }
	inline String_t* get_description_0() const { return ___description_0; }
	inline String_t** get_address_of_description_0() { return &___description_0; }
	inline void set_description_0(String_t* value)
	{
		___description_0 = value;
		Il2CppCodeGenWriteBarrier((&___description_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T0E28F5AD2665BAE68E81D3FF477CA1E6E1574789_H
#ifndef ELEMENT_T917DF3B3892BA48FD5AE1595F8C288DC67D972BF_H
#define ELEMENT_T917DF3B3892BA48FD5AE1595F8C288DC67D972BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element
struct  Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::elementIdentifier
	int32_t ___elementIdentifier_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::sourceType
	int32_t ___sourceType_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::sourceAxis
	int32_t ___sourceAxis_2;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::axisDeadZone
	float ___axisDeadZone_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::sourceButton
	int32_t ___sourceButton_4;
	// Rewired.Data.Mapping.CustomCalculation Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::customCalculation
	CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * ___customCalculation_5;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Element::customCalculationSourceData
	CustomCalculationSourceDataU5BU5D_t9CB27E1BF97E6EEF9BF27CECA4DC1B02B002F8D0* ___customCalculationSourceData_6;

public:
	inline static int32_t get_offset_of_elementIdentifier_0() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___elementIdentifier_0)); }
	inline int32_t get_elementIdentifier_0() const { return ___elementIdentifier_0; }
	inline int32_t* get_address_of_elementIdentifier_0() { return &___elementIdentifier_0; }
	inline void set_elementIdentifier_0(int32_t value)
	{
		___elementIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_sourceType_1() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___sourceType_1)); }
	inline int32_t get_sourceType_1() const { return ___sourceType_1; }
	inline int32_t* get_address_of_sourceType_1() { return &___sourceType_1; }
	inline void set_sourceType_1(int32_t value)
	{
		___sourceType_1 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_2() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___sourceAxis_2)); }
	inline int32_t get_sourceAxis_2() const { return ___sourceAxis_2; }
	inline int32_t* get_address_of_sourceAxis_2() { return &___sourceAxis_2; }
	inline void set_sourceAxis_2(int32_t value)
	{
		___sourceAxis_2 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_3() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___axisDeadZone_3)); }
	inline float get_axisDeadZone_3() const { return ___axisDeadZone_3; }
	inline float* get_address_of_axisDeadZone_3() { return &___axisDeadZone_3; }
	inline void set_axisDeadZone_3(float value)
	{
		___axisDeadZone_3 = value;
	}

	inline static int32_t get_offset_of_sourceButton_4() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___sourceButton_4)); }
	inline int32_t get_sourceButton_4() const { return ___sourceButton_4; }
	inline int32_t* get_address_of_sourceButton_4() { return &___sourceButton_4; }
	inline void set_sourceButton_4(int32_t value)
	{
		___sourceButton_4 = value;
	}

	inline static int32_t get_offset_of_customCalculation_5() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___customCalculation_5)); }
	inline CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * get_customCalculation_5() const { return ___customCalculation_5; }
	inline CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 ** get_address_of_customCalculation_5() { return &___customCalculation_5; }
	inline void set_customCalculation_5(CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * value)
	{
		___customCalculation_5 = value;
		Il2CppCodeGenWriteBarrier((&___customCalculation_5), value);
	}

	inline static int32_t get_offset_of_customCalculationSourceData_6() { return static_cast<int32_t>(offsetof(Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF, ___customCalculationSourceData_6)); }
	inline CustomCalculationSourceDataU5BU5D_t9CB27E1BF97E6EEF9BF27CECA4DC1B02B002F8D0* get_customCalculationSourceData_6() const { return ___customCalculationSourceData_6; }
	inline CustomCalculationSourceDataU5BU5D_t9CB27E1BF97E6EEF9BF27CECA4DC1B02B002F8D0** get_address_of_customCalculationSourceData_6() { return &___customCalculationSourceData_6; }
	inline void set_customCalculationSourceData_6(CustomCalculationSourceDataU5BU5D_t9CB27E1BF97E6EEF9BF27CECA4DC1B02B002F8D0* value)
	{
		___customCalculationSourceData_6 = value;
		Il2CppCodeGenWriteBarrier((&___customCalculationSourceData_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T917DF3B3892BA48FD5AE1595F8C288DC67D972BF_H
#ifndef CJMCDICVLFQVTGTVVATYEWUWNMEK_T3B6E76040BC37F3D9849A477EEE6E871233C299A_H
#define CJMCDICVLFQVTGTVVATYEWUWNMEK_T3B6E76040BC37F3D9849A477EEE6E871233C299A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_cjmCDIcVlfqVTgtvvATYeWUwNmEk
struct  cjmCDIcVlfqVTgtvvATYeWUwNmEk_t3B6E76040BC37F3D9849A477EEE6E871233C299A  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_cjmCDIcVlfqVTgtvvATYeWUwNmEk::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_cjmCDIcVlfqVTgtvvATYeWUwNmEk::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_cjmCDIcVlfqVTgtvvATYeWUwNmEk::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_cjmCDIcVlfqVTgtvvATYeWUwNmEk::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_cjmCDIcVlfqVTgtvvATYeWUwNmEk::IrGWWOzZMkuNYdSSpPyJJhKjHfv
	int32_t ___IrGWWOzZMkuNYdSSpPyJJhKjHfv_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(cjmCDIcVlfqVTgtvvATYeWUwNmEk_t3B6E76040BC37F3D9849A477EEE6E871233C299A, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(cjmCDIcVlfqVTgtvvATYeWUwNmEk_t3B6E76040BC37F3D9849A477EEE6E871233C299A, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(cjmCDIcVlfqVTgtvvATYeWUwNmEk_t3B6E76040BC37F3D9849A477EEE6E871233C299A, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(cjmCDIcVlfqVTgtvvATYeWUwNmEk_t3B6E76040BC37F3D9849A477EEE6E871233C299A, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_IrGWWOzZMkuNYdSSpPyJJhKjHfv_4() { return static_cast<int32_t>(offsetof(cjmCDIcVlfqVTgtvvATYeWUwNmEk_t3B6E76040BC37F3D9849A477EEE6E871233C299A, ___IrGWWOzZMkuNYdSSpPyJJhKjHfv_4)); }
	inline int32_t get_IrGWWOzZMkuNYdSSpPyJJhKjHfv_4() const { return ___IrGWWOzZMkuNYdSSpPyJJhKjHfv_4; }
	inline int32_t* get_address_of_IrGWWOzZMkuNYdSSpPyJJhKjHfv_4() { return &___IrGWWOzZMkuNYdSSpPyJJhKjHfv_4; }
	inline void set_IrGWWOzZMkuNYdSSpPyJJhKjHfv_4(int32_t value)
	{
		___IrGWWOzZMkuNYdSSpPyJJhKjHfv_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CJMCDICVLFQVTGTVVATYEWUWNMEK_T3B6E76040BC37F3D9849A477EEE6E871233C299A_H
#ifndef JXGXKZLNZIVRFWIQANNGLVZAGUR_T61740DA545A3090693AAEBB696CEB6049E5986FD_H
#define JXGXKZLNZIVRFWIQANNGLVZAGUR_T61740DA545A3090693AAEBB696CEB6049E5986FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_jxgXkzlNZIvRFWiqanngLVzagUr
struct  jxgXkzlNZIvRFWiqanngLVzagUr_t61740DA545A3090693AAEBB696CEB6049E5986FD  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_jxgXkzlNZIvRFWiqanngLVzagUr::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_jxgXkzlNZIvRFWiqanngLVzagUr::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_jxgXkzlNZIvRFWiqanngLVzagUr::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_jxgXkzlNZIvRFWiqanngLVzagUr::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_jxgXkzlNZIvRFWiqanngLVzagUr::AnysTbsSkKHFvOTieNBKYVpjqIa
	int32_t ___AnysTbsSkKHFvOTieNBKYVpjqIa_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(jxgXkzlNZIvRFWiqanngLVzagUr_t61740DA545A3090693AAEBB696CEB6049E5986FD, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(jxgXkzlNZIvRFWiqanngLVzagUr_t61740DA545A3090693AAEBB696CEB6049E5986FD, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(jxgXkzlNZIvRFWiqanngLVzagUr_t61740DA545A3090693AAEBB696CEB6049E5986FD, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(jxgXkzlNZIvRFWiqanngLVzagUr_t61740DA545A3090693AAEBB696CEB6049E5986FD, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_AnysTbsSkKHFvOTieNBKYVpjqIa_4() { return static_cast<int32_t>(offsetof(jxgXkzlNZIvRFWiqanngLVzagUr_t61740DA545A3090693AAEBB696CEB6049E5986FD, ___AnysTbsSkKHFvOTieNBKYVpjqIa_4)); }
	inline int32_t get_AnysTbsSkKHFvOTieNBKYVpjqIa_4() const { return ___AnysTbsSkKHFvOTieNBKYVpjqIa_4; }
	inline int32_t* get_address_of_AnysTbsSkKHFvOTieNBKYVpjqIa_4() { return &___AnysTbsSkKHFvOTieNBKYVpjqIa_4; }
	inline void set_AnysTbsSkKHFvOTieNBKYVpjqIa_4(int32_t value)
	{
		___AnysTbsSkKHFvOTieNBKYVpjqIa_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JXGXKZLNZIVRFWIQANNGLVZAGUR_T61740DA545A3090693AAEBB696CEB6049E5986FD_H
#ifndef ELEMENT_T82B7A20AC236A568752E7BBC63B844092DD83294_H
#define ELEMENT_T82B7A20AC236A568752E7BBC63B844092DD83294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Element
struct  Element_t82B7A20AC236A568752E7BBC63B844092DD83294  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T82B7A20AC236A568752E7BBC63B844092DD83294_H
#ifndef WGCZIXDOJYVUGFGXLCDWDEWRAOME_T53BEDB52829C96862405574998671D31735DC978_H
#define WGCZIXDOJYVUGFGXLCDWDEWRAOME_T53BEDB52829C96862405574998671D31735DC978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements_WGczixdOJyVUGFGxlCDWdEwraome
struct  WGczixdOJyVUGFGxlCDWdEwraome_t53BEDB52829C96862405574998671D31735DC978  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements_WGczixdOJyVUGFGxlCDWdEwraome::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements_WGczixdOJyVUGFGxlCDWdEwraome::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements_WGczixdOJyVUGFGxlCDWdEwraome::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements_WGczixdOJyVUGFGxlCDWdEwraome::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements_WGczixdOJyVUGFGxlCDWdEwraome::ypdBJEaRFrJxDBDuWFtMnHLTlSg
	int32_t ___ypdBJEaRFrJxDBDuWFtMnHLTlSg_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(WGczixdOJyVUGFGxlCDWdEwraome_t53BEDB52829C96862405574998671D31735DC978, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(WGczixdOJyVUGFGxlCDWdEwraome_t53BEDB52829C96862405574998671D31735DC978, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(WGczixdOJyVUGFGxlCDWdEwraome_t53BEDB52829C96862405574998671D31735DC978, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(WGczixdOJyVUGFGxlCDWdEwraome_t53BEDB52829C96862405574998671D31735DC978, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_ypdBJEaRFrJxDBDuWFtMnHLTlSg_4() { return static_cast<int32_t>(offsetof(WGczixdOJyVUGFGxlCDWdEwraome_t53BEDB52829C96862405574998671D31735DC978, ___ypdBJEaRFrJxDBDuWFtMnHLTlSg_4)); }
	inline int32_t get_ypdBJEaRFrJxDBDuWFtMnHLTlSg_4() const { return ___ypdBJEaRFrJxDBDuWFtMnHLTlSg_4; }
	inline int32_t* get_address_of_ypdBJEaRFrJxDBDuWFtMnHLTlSg_4() { return &___ypdBJEaRFrJxDBDuWFtMnHLTlSg_4; }
	inline void set_ypdBJEaRFrJxDBDuWFtMnHLTlSg_4(int32_t value)
	{
		___ypdBJEaRFrJxDBDuWFtMnHLTlSg_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WGCZIXDOJYVUGFGXLCDWDEWRAOME_T53BEDB52829C96862405574998671D31735DC978_H
#ifndef GIOBKUWSILJOFIXWORUMUOMIXGV_TDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC_H
#define GIOBKUWSILJOFIXWORUMUOMIXGV_TDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements_gIoBkuwSIlJOFixWOruMuOMixgv
struct  gIoBkuwSIlJOFixWOruMuOMixgv_tDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements_gIoBkuwSIlJOFixWOruMuOMixgv::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements_gIoBkuwSIlJOFixWOruMuOMixgv::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements_gIoBkuwSIlJOFixWOruMuOMixgv::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements_gIoBkuwSIlJOFixWOruMuOMixgv::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements_gIoBkuwSIlJOFixWOruMuOMixgv::OrbwoIzETVPsVWWlVhcktAHdUZp
	int32_t ___OrbwoIzETVPsVWWlVhcktAHdUZp_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(gIoBkuwSIlJOFixWOruMuOMixgv_tDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(gIoBkuwSIlJOFixWOruMuOMixgv_tDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(gIoBkuwSIlJOFixWOruMuOMixgv_tDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(gIoBkuwSIlJOFixWOruMuOMixgv_tDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_OrbwoIzETVPsVWWlVhcktAHdUZp_4() { return static_cast<int32_t>(offsetof(gIoBkuwSIlJOFixWOruMuOMixgv_tDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC, ___OrbwoIzETVPsVWWlVhcktAHdUZp_4)); }
	inline int32_t get_OrbwoIzETVPsVWWlVhcktAHdUZp_4() const { return ___OrbwoIzETVPsVWWlVhcktAHdUZp_4; }
	inline int32_t* get_address_of_OrbwoIzETVPsVWWlVhcktAHdUZp_4() { return &___OrbwoIzETVPsVWWlVhcktAHdUZp_4; }
	inline void set_OrbwoIzETVPsVWWlVhcktAHdUZp_4(int32_t value)
	{
		___OrbwoIzETVPsVWWlVhcktAHdUZp_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIOBKUWSILJOFIXWORUMUOMIXGV_TDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC_H
#ifndef JJCMMUAKTVLOAPNLKBXEEGSLVPX_T10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499_H
#define JJCMMUAKTVLOAPNLKBXEEGSLVPX_T10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_JjCMmuAkTvloAPNLkbXeEGSLvPX
struct  JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_JjCMmuAkTvloAPNLkbXeEGSLvPX::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_JjCMmuAkTvloAPNLkbXeEGSLvPX::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_JjCMmuAkTvloAPNLkbXeEGSLvPX::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_JjCMmuAkTvloAPNLkbXeEGSLvPX::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_JjCMmuAkTvloAPNLkbXeEGSLvPX::TSRwvcqGfNlHrfiluDIKLqwFFkQ
	int32_t ___TSRwvcqGfNlHrfiluDIKLqwFFkQ_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_JjCMmuAkTvloAPNLkbXeEGSLvPX::PATgLDdPRaJHzrCXtZSEauMYeqXg
	int32_t ___PATgLDdPRaJHzrCXtZSEauMYeqXg_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_TSRwvcqGfNlHrfiluDIKLqwFFkQ_4() { return static_cast<int32_t>(offsetof(JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499, ___TSRwvcqGfNlHrfiluDIKLqwFFkQ_4)); }
	inline int32_t get_TSRwvcqGfNlHrfiluDIKLqwFFkQ_4() const { return ___TSRwvcqGfNlHrfiluDIKLqwFFkQ_4; }
	inline int32_t* get_address_of_TSRwvcqGfNlHrfiluDIKLqwFFkQ_4() { return &___TSRwvcqGfNlHrfiluDIKLqwFFkQ_4; }
	inline void set_TSRwvcqGfNlHrfiluDIKLqwFFkQ_4(int32_t value)
	{
		___TSRwvcqGfNlHrfiluDIKLqwFFkQ_4 = value;
	}

	inline static int32_t get_offset_of_PATgLDdPRaJHzrCXtZSEauMYeqXg_5() { return static_cast<int32_t>(offsetof(JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499, ___PATgLDdPRaJHzrCXtZSEauMYeqXg_5)); }
	inline int32_t get_PATgLDdPRaJHzrCXtZSEauMYeqXg_5() const { return ___PATgLDdPRaJHzrCXtZSEauMYeqXg_5; }
	inline int32_t* get_address_of_PATgLDdPRaJHzrCXtZSEauMYeqXg_5() { return &___PATgLDdPRaJHzrCXtZSEauMYeqXg_5; }
	inline void set_PATgLDdPRaJHzrCXtZSEauMYeqXg_5(int32_t value)
	{
		___PATgLDdPRaJHzrCXtZSEauMYeqXg_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JJCMMUAKTVLOAPNLKBXEEGSLVPX_T10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499_H
#ifndef CWCMEOQERQRESVZZBDPUQKMBGZM_TBD3CBED09E123BB8F8840910AFC76684BE02F2EE_H
#define CWCMEOQERQRESVZZBDPUQKMBGZM_TBD3CBED09E123BB8F8840910AFC76684BE02F2EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_cwcmEoqerQrESvZzbDpuqKmbgZm
struct  cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_cwcmEoqerQrESvZzbDpuqKmbgZm::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_cwcmEoqerQrESvZzbDpuqKmbgZm::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_cwcmEoqerQrESvZzbDpuqKmbgZm::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_cwcmEoqerQrESvZzbDpuqKmbgZm::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_cwcmEoqerQrESvZzbDpuqKmbgZm::gQLitZPWLRDfDTLuNqDuRKdoENRb
	int32_t ___gQLitZPWLRDfDTLuNqDuRKdoENRb_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_cwcmEoqerQrESvZzbDpuqKmbgZm::eUjlzCvPnutsdjfeMyGQKnqxpbX
	int32_t ___eUjlzCvPnutsdjfeMyGQKnqxpbX_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_gQLitZPWLRDfDTLuNqDuRKdoENRb_4() { return static_cast<int32_t>(offsetof(cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE, ___gQLitZPWLRDfDTLuNqDuRKdoENRb_4)); }
	inline int32_t get_gQLitZPWLRDfDTLuNqDuRKdoENRb_4() const { return ___gQLitZPWLRDfDTLuNqDuRKdoENRb_4; }
	inline int32_t* get_address_of_gQLitZPWLRDfDTLuNqDuRKdoENRb_4() { return &___gQLitZPWLRDfDTLuNqDuRKdoENRb_4; }
	inline void set_gQLitZPWLRDfDTLuNqDuRKdoENRb_4(int32_t value)
	{
		___gQLitZPWLRDfDTLuNqDuRKdoENRb_4 = value;
	}

	inline static int32_t get_offset_of_eUjlzCvPnutsdjfeMyGQKnqxpbX_5() { return static_cast<int32_t>(offsetof(cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE, ___eUjlzCvPnutsdjfeMyGQKnqxpbX_5)); }
	inline int32_t get_eUjlzCvPnutsdjfeMyGQKnqxpbX_5() const { return ___eUjlzCvPnutsdjfeMyGQKnqxpbX_5; }
	inline int32_t* get_address_of_eUjlzCvPnutsdjfeMyGQKnqxpbX_5() { return &___eUjlzCvPnutsdjfeMyGQKnqxpbX_5; }
	inline void set_eUjlzCvPnutsdjfeMyGQKnqxpbX_5(int32_t value)
	{
		___eUjlzCvPnutsdjfeMyGQKnqxpbX_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CWCMEOQERQRESVZZBDPUQKMBGZM_TBD3CBED09E123BB8F8840910AFC76684BE02F2EE_H
#ifndef QXWZSTKCVOGOPEOKEZJJPYSHNYLB_TA79D0BE6704B2F8065F197D5B78F3607770A5FA4_H
#define QXWZSTKCVOGOPEOKEZJJPYSHNYLB_TA79D0BE6704B2F8065F197D5B78F3607770A5FA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_qxWzstKCvogOpEOkEZjjPyShnYlB
struct  qxWzstKCvogOpEOkEZjjPyShnYlB_tA79D0BE6704B2F8065F197D5B78F3607770A5FA4  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_qxWzstKCvogOpEOkEZjjPyShnYlB::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_qxWzstKCvogOpEOkEZjjPyShnYlB::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_qxWzstKCvogOpEOkEZjjPyShnYlB::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_qxWzstKCvogOpEOkEZjjPyShnYlB::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_qxWzstKCvogOpEOkEZjjPyShnYlB::xsDEqkxdlhIsWdiGNsvFuhyVxDVE
	int32_t ___xsDEqkxdlhIsWdiGNsvFuhyVxDVE_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(qxWzstKCvogOpEOkEZjjPyShnYlB_tA79D0BE6704B2F8065F197D5B78F3607770A5FA4, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(qxWzstKCvogOpEOkEZjjPyShnYlB_tA79D0BE6704B2F8065F197D5B78F3607770A5FA4, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(qxWzstKCvogOpEOkEZjjPyShnYlB_tA79D0BE6704B2F8065F197D5B78F3607770A5FA4, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(qxWzstKCvogOpEOkEZjjPyShnYlB_tA79D0BE6704B2F8065F197D5B78F3607770A5FA4, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_xsDEqkxdlhIsWdiGNsvFuhyVxDVE_4() { return static_cast<int32_t>(offsetof(qxWzstKCvogOpEOkEZjjPyShnYlB_tA79D0BE6704B2F8065F197D5B78F3607770A5FA4, ___xsDEqkxdlhIsWdiGNsvFuhyVxDVE_4)); }
	inline int32_t get_xsDEqkxdlhIsWdiGNsvFuhyVxDVE_4() const { return ___xsDEqkxdlhIsWdiGNsvFuhyVxDVE_4; }
	inline int32_t* get_address_of_xsDEqkxdlhIsWdiGNsvFuhyVxDVE_4() { return &___xsDEqkxdlhIsWdiGNsvFuhyVxDVE_4; }
	inline void set_xsDEqkxdlhIsWdiGNsvFuhyVxDVE_4(int32_t value)
	{
		___xsDEqkxdlhIsWdiGNsvFuhyVxDVE_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QXWZSTKCVOGOPEOKEZJJPYSHNYLB_TA79D0BE6704B2F8065F197D5B78F3607770A5FA4_H
#ifndef ELEMENT_T909158E9B20107B6BD16CD99ECF5F01BD9C2F463_H
#define ELEMENT_T909158E9B20107B6BD16CD99ECF5F01BD9C2F463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Element
struct  Element_t909158E9B20107B6BD16CD99ECF5F01BD9C2F463  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T909158E9B20107B6BD16CD99ECF5F01BD9C2F463_H
#ifndef HHOEBWKKGYWTKSPNVMLFDZYCGJAL_T73694E47339A6446F551AA3A87937C073FD300A5_H
#define HHOEBWKKGYWTKSPNVMLFDZYCGJAL_T73694E47339A6446F551AA3A87937C073FD300A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_HHOeBWkKgYwTkSpNVMLfdzycGjAL
struct  HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_HHOeBWkKgYwTkSpNVMLfdzycGjAL::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_HHOeBWkKgYwTkSpNVMLfdzycGjAL::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_HHOeBWkKgYwTkSpNVMLfdzycGjAL::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_HHOeBWkKgYwTkSpNVMLfdzycGjAL::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_HHOeBWkKgYwTkSpNVMLfdzycGjAL::fMShXwOCmAHSAzYhbRbLBQnMPaa
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD * ___fMShXwOCmAHSAzYhbRbLBQnMPaa_4;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_HHOeBWkKgYwTkSpNVMLfdzycGjAL::hHLtBCpipLanptvPCkNnpldMMFu
	AxisU5BU5D_tE985187DDC620CCDAAC8D4E2FC59E979A211FE90* ___hHLtBCpipLanptvPCkNnpldMMFu_5;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_HHOeBWkKgYwTkSpNVMLfdzycGjAL::kgdXDOuwBknTdVGsGtzXGYuZhje
	int32_t ___kgdXDOuwBknTdVGsGtzXGYuZhje_6;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t766FE92C87B9298B56A4F07611581240BD735EFD * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t766FE92C87B9298B56A4F07611581240BD735EFD ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_fMShXwOCmAHSAzYhbRbLBQnMPaa_4() { return static_cast<int32_t>(offsetof(HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5, ___fMShXwOCmAHSAzYhbRbLBQnMPaa_4)); }
	inline Axis_t766FE92C87B9298B56A4F07611581240BD735EFD * get_fMShXwOCmAHSAzYhbRbLBQnMPaa_4() const { return ___fMShXwOCmAHSAzYhbRbLBQnMPaa_4; }
	inline Axis_t766FE92C87B9298B56A4F07611581240BD735EFD ** get_address_of_fMShXwOCmAHSAzYhbRbLBQnMPaa_4() { return &___fMShXwOCmAHSAzYhbRbLBQnMPaa_4; }
	inline void set_fMShXwOCmAHSAzYhbRbLBQnMPaa_4(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD * value)
	{
		___fMShXwOCmAHSAzYhbRbLBQnMPaa_4 = value;
		Il2CppCodeGenWriteBarrier((&___fMShXwOCmAHSAzYhbRbLBQnMPaa_4), value);
	}

	inline static int32_t get_offset_of_hHLtBCpipLanptvPCkNnpldMMFu_5() { return static_cast<int32_t>(offsetof(HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5, ___hHLtBCpipLanptvPCkNnpldMMFu_5)); }
	inline AxisU5BU5D_tE985187DDC620CCDAAC8D4E2FC59E979A211FE90* get_hHLtBCpipLanptvPCkNnpldMMFu_5() const { return ___hHLtBCpipLanptvPCkNnpldMMFu_5; }
	inline AxisU5BU5D_tE985187DDC620CCDAAC8D4E2FC59E979A211FE90** get_address_of_hHLtBCpipLanptvPCkNnpldMMFu_5() { return &___hHLtBCpipLanptvPCkNnpldMMFu_5; }
	inline void set_hHLtBCpipLanptvPCkNnpldMMFu_5(AxisU5BU5D_tE985187DDC620CCDAAC8D4E2FC59E979A211FE90* value)
	{
		___hHLtBCpipLanptvPCkNnpldMMFu_5 = value;
		Il2CppCodeGenWriteBarrier((&___hHLtBCpipLanptvPCkNnpldMMFu_5), value);
	}

	inline static int32_t get_offset_of_kgdXDOuwBknTdVGsGtzXGYuZhje_6() { return static_cast<int32_t>(offsetof(HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5, ___kgdXDOuwBknTdVGsGtzXGYuZhje_6)); }
	inline int32_t get_kgdXDOuwBknTdVGsGtzXGYuZhje_6() const { return ___kgdXDOuwBknTdVGsGtzXGYuZhje_6; }
	inline int32_t* get_address_of_kgdXDOuwBknTdVGsGtzXGYuZhje_6() { return &___kgdXDOuwBknTdVGsGtzXGYuZhje_6; }
	inline void set_kgdXDOuwBknTdVGsGtzXGYuZhje_6(int32_t value)
	{
		___kgdXDOuwBknTdVGsGtzXGYuZhje_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HHOEBWKKGYWTKSPNVMLFDZYCGJAL_T73694E47339A6446F551AA3A87937C073FD300A5_H
#ifndef BRIIQFFDKSDZOMGTDDCCOAJRAQK_TA1F1528572935394D9B3ABD4C92EB96FCE2735E3_H
#define BRIIQFFDKSDZOMGTDDCCOAJRAQK_TA1F1528572935394D9B3ABD4C92EB96FCE2735E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_brIiqffdKsDZoMGTDDCCoajraQk
struct  brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_brIiqffdKsDZoMGTDDCCoajraQk::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_brIiqffdKsDZoMGTDDCCoajraQk::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_brIiqffdKsDZoMGTDDCCoajraQk::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_brIiqffdKsDZoMGTDDCCoajraQk::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_brIiqffdKsDZoMGTDDCCoajraQk::pmhJwtzrxTmDTdeZAcjZOlbVYWt
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A * ___pmhJwtzrxTmDTdeZAcjZOlbVYWt_4;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_brIiqffdKsDZoMGTDDCCoajraQk::mGffXviMGlJeIhFyFdNxAzlXIpkh
	ButtonU5BU5D_tA5A850BF0855D9A2C6F95A30C52D6EDF9C4C9101* ___mGffXviMGlJeIhFyFdNxAzlXIpkh_5;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements_brIiqffdKsDZoMGTDDCCoajraQk::yxxNlXTPZIjLPseZGjwaIuYuFMO
	int32_t ___yxxNlXTPZIjLPseZGjwaIuYuFMO_6;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_t841D3D6DD5392867B22FA416457754550B1B8F3A * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_t841D3D6DD5392867B22FA416457754550B1B8F3A ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_pmhJwtzrxTmDTdeZAcjZOlbVYWt_4() { return static_cast<int32_t>(offsetof(brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3, ___pmhJwtzrxTmDTdeZAcjZOlbVYWt_4)); }
	inline Button_t841D3D6DD5392867B22FA416457754550B1B8F3A * get_pmhJwtzrxTmDTdeZAcjZOlbVYWt_4() const { return ___pmhJwtzrxTmDTdeZAcjZOlbVYWt_4; }
	inline Button_t841D3D6DD5392867B22FA416457754550B1B8F3A ** get_address_of_pmhJwtzrxTmDTdeZAcjZOlbVYWt_4() { return &___pmhJwtzrxTmDTdeZAcjZOlbVYWt_4; }
	inline void set_pmhJwtzrxTmDTdeZAcjZOlbVYWt_4(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A * value)
	{
		___pmhJwtzrxTmDTdeZAcjZOlbVYWt_4 = value;
		Il2CppCodeGenWriteBarrier((&___pmhJwtzrxTmDTdeZAcjZOlbVYWt_4), value);
	}

	inline static int32_t get_offset_of_mGffXviMGlJeIhFyFdNxAzlXIpkh_5() { return static_cast<int32_t>(offsetof(brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3, ___mGffXviMGlJeIhFyFdNxAzlXIpkh_5)); }
	inline ButtonU5BU5D_tA5A850BF0855D9A2C6F95A30C52D6EDF9C4C9101* get_mGffXviMGlJeIhFyFdNxAzlXIpkh_5() const { return ___mGffXviMGlJeIhFyFdNxAzlXIpkh_5; }
	inline ButtonU5BU5D_tA5A850BF0855D9A2C6F95A30C52D6EDF9C4C9101** get_address_of_mGffXviMGlJeIhFyFdNxAzlXIpkh_5() { return &___mGffXviMGlJeIhFyFdNxAzlXIpkh_5; }
	inline void set_mGffXviMGlJeIhFyFdNxAzlXIpkh_5(ButtonU5BU5D_tA5A850BF0855D9A2C6F95A30C52D6EDF9C4C9101* value)
	{
		___mGffXviMGlJeIhFyFdNxAzlXIpkh_5 = value;
		Il2CppCodeGenWriteBarrier((&___mGffXviMGlJeIhFyFdNxAzlXIpkh_5), value);
	}

	inline static int32_t get_offset_of_yxxNlXTPZIjLPseZGjwaIuYuFMO_6() { return static_cast<int32_t>(offsetof(brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3, ___yxxNlXTPZIjLPseZGjwaIuYuFMO_6)); }
	inline int32_t get_yxxNlXTPZIjLPseZGjwaIuYuFMO_6() const { return ___yxxNlXTPZIjLPseZGjwaIuYuFMO_6; }
	inline int32_t* get_address_of_yxxNlXTPZIjLPseZGjwaIuYuFMO_6() { return &___yxxNlXTPZIjLPseZGjwaIuYuFMO_6; }
	inline void set_yxxNlXTPZIjLPseZGjwaIuYuFMO_6(int32_t value)
	{
		___yxxNlXTPZIjLPseZGjwaIuYuFMO_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRIIQFFDKSDZOMGTDDCCOAJRAQK_TA1F1528572935394D9B3ABD4C92EB96FCE2735E3_H
#ifndef LARLCGLIWTISCQUZQEVPAIYQFXU_TDFB76EDB7B2EF3C3317892645D86C472FBF2387B_H
#define LARLCGLIWTISCQUZQEVPAIYQFXU_TDFB76EDB7B2EF3C3317892645D86C472FBF2387B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_LaRlcglIWtIscquZqevpAiYQfXu
struct  LaRlcglIWtIscquZqevpAiYQfXu_tDFB76EDB7B2EF3C3317892645D86C472FBF2387B  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_LaRlcglIWtIscquZqevpAiYQfXu::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_LaRlcglIWtIscquZqevpAiYQfXu::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_LaRlcglIWtIscquZqevpAiYQfXu::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_LaRlcglIWtIscquZqevpAiYQfXu::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_LaRlcglIWtIscquZqevpAiYQfXu::tXqVgdQEhgiLhHvNIfvbvhdHAMnu
	int32_t ___tXqVgdQEhgiLhHvNIfvbvhdHAMnu_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(LaRlcglIWtIscquZqevpAiYQfXu_tDFB76EDB7B2EF3C3317892645D86C472FBF2387B, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t766FE92C87B9298B56A4F07611581240BD735EFD * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t766FE92C87B9298B56A4F07611581240BD735EFD ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(LaRlcglIWtIscquZqevpAiYQfXu_tDFB76EDB7B2EF3C3317892645D86C472FBF2387B, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(LaRlcglIWtIscquZqevpAiYQfXu_tDFB76EDB7B2EF3C3317892645D86C472FBF2387B, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(LaRlcglIWtIscquZqevpAiYQfXu_tDFB76EDB7B2EF3C3317892645D86C472FBF2387B, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_tXqVgdQEhgiLhHvNIfvbvhdHAMnu_4() { return static_cast<int32_t>(offsetof(LaRlcglIWtIscquZqevpAiYQfXu_tDFB76EDB7B2EF3C3317892645D86C472FBF2387B, ___tXqVgdQEhgiLhHvNIfvbvhdHAMnu_4)); }
	inline int32_t get_tXqVgdQEhgiLhHvNIfvbvhdHAMnu_4() const { return ___tXqVgdQEhgiLhHvNIfvbvhdHAMnu_4; }
	inline int32_t* get_address_of_tXqVgdQEhgiLhHvNIfvbvhdHAMnu_4() { return &___tXqVgdQEhgiLhHvNIfvbvhdHAMnu_4; }
	inline void set_tXqVgdQEhgiLhHvNIfvbvhdHAMnu_4(int32_t value)
	{
		___tXqVgdQEhgiLhHvNIfvbvhdHAMnu_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LARLCGLIWTISCQUZQEVPAIYQFXU_TDFB76EDB7B2EF3C3317892645D86C472FBF2387B_H
#ifndef AGRFXPBZSOKATFQDTBUJGJKHSQGI_TDF20763DE376F1AD74A0D2BA55304B4B861AC1F9_H
#define AGRFXPBZSOKATFQDTBUJGJKHSQGI_TDF20763DE376F1AD74A0D2BA55304B4B861AC1F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_agRfXpBZsokaTfQdtBuJgJkHSQGI
struct  agRfXpBZsokaTfQdtBuJgJkHSQGI_tDF20763DE376F1AD74A0D2BA55304B4B861AC1F9  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_agRfXpBZsokaTfQdtBuJgJkHSQGI::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_agRfXpBZsokaTfQdtBuJgJkHSQGI::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_agRfXpBZsokaTfQdtBuJgJkHSQGI::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_agRfXpBZsokaTfQdtBuJgJkHSQGI::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_agRfXpBZsokaTfQdtBuJgJkHSQGI::TaHsLDlzJZGOmiVAEbIVOMFVhWNC
	int32_t ___TaHsLDlzJZGOmiVAEbIVOMFVhWNC_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(agRfXpBZsokaTfQdtBuJgJkHSQGI_tDF20763DE376F1AD74A0D2BA55304B4B861AC1F9, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_t841D3D6DD5392867B22FA416457754550B1B8F3A * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_t841D3D6DD5392867B22FA416457754550B1B8F3A ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(agRfXpBZsokaTfQdtBuJgJkHSQGI_tDF20763DE376F1AD74A0D2BA55304B4B861AC1F9, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(agRfXpBZsokaTfQdtBuJgJkHSQGI_tDF20763DE376F1AD74A0D2BA55304B4B861AC1F9, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(agRfXpBZsokaTfQdtBuJgJkHSQGI_tDF20763DE376F1AD74A0D2BA55304B4B861AC1F9, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_TaHsLDlzJZGOmiVAEbIVOMFVhWNC_4() { return static_cast<int32_t>(offsetof(agRfXpBZsokaTfQdtBuJgJkHSQGI_tDF20763DE376F1AD74A0D2BA55304B4B861AC1F9, ___TaHsLDlzJZGOmiVAEbIVOMFVhWNC_4)); }
	inline int32_t get_TaHsLDlzJZGOmiVAEbIVOMFVhWNC_4() const { return ___TaHsLDlzJZGOmiVAEbIVOMFVhWNC_4; }
	inline int32_t* get_address_of_TaHsLDlzJZGOmiVAEbIVOMFVhWNC_4() { return &___TaHsLDlzJZGOmiVAEbIVOMFVhWNC_4; }
	inline void set_TaHsLDlzJZGOmiVAEbIVOMFVhWNC_4(int32_t value)
	{
		___TaHsLDlzJZGOmiVAEbIVOMFVhWNC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGRFXPBZSOKATFQDTBUJGJKHSQGI_TDF20763DE376F1AD74A0D2BA55304B4B861AC1F9_H
#ifndef LOAMAUSXBRHISLTPSNLPWKQKVEN_T8A685890B0D9497EF535CE09117131DA81E53C75_H
#define LOAMAUSXBRHISLTPSNLPWKQKVEN_T8A685890B0D9497EF535CE09117131DA81E53C75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_LoaMaUsXBrHIsltPSNLpwkqKVeN
struct  LoaMaUsXBrHIsltPSNLpwkqKVeN_t8A685890B0D9497EF535CE09117131DA81E53C75  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_LoaMaUsXBrHIsltPSNLpwkqKVeN::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_LoaMaUsXBrHIsltPSNLpwkqKVeN::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_LoaMaUsXBrHIsltPSNLpwkqKVeN::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_LoaMaUsXBrHIsltPSNLpwkqKVeN::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_LoaMaUsXBrHIsltPSNLpwkqKVeN::wmaFOcRiDRKegAnLKfvzccCrsxv
	int32_t ___wmaFOcRiDRKegAnLKfvzccCrsxv_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(LoaMaUsXBrHIsltPSNLpwkqKVeN_t8A685890B0D9497EF535CE09117131DA81E53C75, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(LoaMaUsXBrHIsltPSNLpwkqKVeN_t8A685890B0D9497EF535CE09117131DA81E53C75, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(LoaMaUsXBrHIsltPSNLpwkqKVeN_t8A685890B0D9497EF535CE09117131DA81E53C75, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(LoaMaUsXBrHIsltPSNLpwkqKVeN_t8A685890B0D9497EF535CE09117131DA81E53C75, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_wmaFOcRiDRKegAnLKfvzccCrsxv_4() { return static_cast<int32_t>(offsetof(LoaMaUsXBrHIsltPSNLpwkqKVeN_t8A685890B0D9497EF535CE09117131DA81E53C75, ___wmaFOcRiDRKegAnLKfvzccCrsxv_4)); }
	inline int32_t get_wmaFOcRiDRKegAnLKfvzccCrsxv_4() const { return ___wmaFOcRiDRKegAnLKfvzccCrsxv_4; }
	inline int32_t* get_address_of_wmaFOcRiDRKegAnLKfvzccCrsxv_4() { return &___wmaFOcRiDRKegAnLKfvzccCrsxv_4; }
	inline void set_wmaFOcRiDRKegAnLKfvzccCrsxv_4(int32_t value)
	{
		___wmaFOcRiDRKegAnLKfvzccCrsxv_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOAMAUSXBRHISLTPSNLPWKQKVEN_T8A685890B0D9497EF535CE09117131DA81E53C75_H
#ifndef OUNOGXDQLHUXJBBITHTZXMPFPHM_TBF131D48BB2A66BF4BBE373E0C17A89149439DA9_H
#define OUNOGXDQLHUXJBBITHTZXMPFPHM_TBF131D48BB2A66BF4BBE373E0C17A89149439DA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_ounOgxDqlHuXjbbITHTZxmpFpHm
struct  ounOgxDqlHuXjbbITHTZxmpFpHm_tBF131D48BB2A66BF4BBE373E0C17A89149439DA9  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_ounOgxDqlHuXjbbITHTZxmpFpHm::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_ounOgxDqlHuXjbbITHTZxmpFpHm::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_ounOgxDqlHuXjbbITHTZxmpFpHm::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_ounOgxDqlHuXjbbITHTZxmpFpHm::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_ounOgxDqlHuXjbbITHTZxmpFpHm::JPXiOSvHYiBazZZyXFtfrWwtopG
	int32_t ___JPXiOSvHYiBazZZyXFtfrWwtopG_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(ounOgxDqlHuXjbbITHTZxmpFpHm_tBF131D48BB2A66BF4BBE373E0C17A89149439DA9, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_tBB6149DD7A45B873206F15797DC2B002019BF09C ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(ounOgxDqlHuXjbbITHTZxmpFpHm_tBF131D48BB2A66BF4BBE373E0C17A89149439DA9, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(ounOgxDqlHuXjbbITHTZxmpFpHm_tBF131D48BB2A66BF4BBE373E0C17A89149439DA9, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(ounOgxDqlHuXjbbITHTZxmpFpHm_tBF131D48BB2A66BF4BBE373E0C17A89149439DA9, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_JPXiOSvHYiBazZZyXFtfrWwtopG_4() { return static_cast<int32_t>(offsetof(ounOgxDqlHuXjbbITHTZxmpFpHm_tBF131D48BB2A66BF4BBE373E0C17A89149439DA9, ___JPXiOSvHYiBazZZyXFtfrWwtopG_4)); }
	inline int32_t get_JPXiOSvHYiBazZZyXFtfrWwtopG_4() const { return ___JPXiOSvHYiBazZZyXFtfrWwtopG_4; }
	inline int32_t* get_address_of_JPXiOSvHYiBazZZyXFtfrWwtopG_4() { return &___JPXiOSvHYiBazZZyXFtfrWwtopG_4; }
	inline void set_JPXiOSvHYiBazZZyXFtfrWwtopG_4(int32_t value)
	{
		___JPXiOSvHYiBazZZyXFtfrWwtopG_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUNOGXDQLHUXJBBITHTZXMPFPHM_TBF131D48BB2A66BF4BBE373E0C17A89149439DA9_H
#ifndef AQOHTPKBETJPNISPRRLBHSLMPQC_T9974202F8DD362E787D9545C7CFE22CA90D4A5E9_H
#define AQOHTPKBETJPNISPRRLBHSLMPQC_T9974202F8DD362E787D9545C7CFE22CA90D4A5E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_aQoHTPKBETJPNiSpRRLBHslmPqc
struct  aQoHTPKBETJPNiSpRRLBHslmPqc_t9974202F8DD362E787D9545C7CFE22CA90D4A5E9  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_aQoHTPKBETJPNiSpRRLBHslmPqc::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_aQoHTPKBETJPNiSpRRLBHslmPqc::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_aQoHTPKBETJPNiSpRRLBHslmPqc::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_aQoHTPKBETJPNiSpRRLBHslmPqc::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_aQoHTPKBETJPNiSpRRLBHslmPqc::BTsbGVkyBHtFlWarfGmuUnxnaUeF
	int32_t ___BTsbGVkyBHtFlWarfGmuUnxnaUeF_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(aQoHTPKBETJPNiSpRRLBHslmPqc_t9974202F8DD362E787D9545C7CFE22CA90D4A5E9, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(aQoHTPKBETJPNiSpRRLBHslmPqc_t9974202F8DD362E787D9545C7CFE22CA90D4A5E9, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(aQoHTPKBETJPNiSpRRLBHslmPqc_t9974202F8DD362E787D9545C7CFE22CA90D4A5E9, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(aQoHTPKBETJPNiSpRRLBHslmPqc_t9974202F8DD362E787D9545C7CFE22CA90D4A5E9, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_BTsbGVkyBHtFlWarfGmuUnxnaUeF_4() { return static_cast<int32_t>(offsetof(aQoHTPKBETJPNiSpRRLBHslmPqc_t9974202F8DD362E787D9545C7CFE22CA90D4A5E9, ___BTsbGVkyBHtFlWarfGmuUnxnaUeF_4)); }
	inline int32_t get_BTsbGVkyBHtFlWarfGmuUnxnaUeF_4() const { return ___BTsbGVkyBHtFlWarfGmuUnxnaUeF_4; }
	inline int32_t* get_address_of_BTsbGVkyBHtFlWarfGmuUnxnaUeF_4() { return &___BTsbGVkyBHtFlWarfGmuUnxnaUeF_4; }
	inline void set_BTsbGVkyBHtFlWarfGmuUnxnaUeF_4(int32_t value)
	{
		___BTsbGVkyBHtFlWarfGmuUnxnaUeF_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AQOHTPKBETJPNISPRRLBHSLMPQC_T9974202F8DD362E787D9545C7CFE22CA90D4A5E9_H
#ifndef WSOEYLLCBDCKHDSVZPMMHOUJKYFN_TD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45_H
#define WSOEYLLCBDCKHDSVZPMMHOUJKYFN_TD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_wSoeyLLcbDCkHDSVzPMmHOuJkYfN
struct  wSoeyLLcbDCkHDSVzPMmHOuJkYfN_tD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_wSoeyLLcbDCkHDSVzPMmHOuJkYfN::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_wSoeyLLcbDCkHDSVzPMmHOuJkYfN::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_wSoeyLLcbDCkHDSVzPMmHOuJkYfN::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_wSoeyLLcbDCkHDSVzPMmHOuJkYfN::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_wSoeyLLcbDCkHDSVzPMmHOuJkYfN::tjEGQFIUcwJTYTHdBnBpHIbqxDB
	int32_t ___tjEGQFIUcwJTYTHdBnBpHIbqxDB_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(wSoeyLLcbDCkHDSVzPMmHOuJkYfN_tD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_tBB6149DD7A45B873206F15797DC2B002019BF09C ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(wSoeyLLcbDCkHDSVzPMmHOuJkYfN_tD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(wSoeyLLcbDCkHDSVzPMmHOuJkYfN_tD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(wSoeyLLcbDCkHDSVzPMmHOuJkYfN_tD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_tjEGQFIUcwJTYTHdBnBpHIbqxDB_4() { return static_cast<int32_t>(offsetof(wSoeyLLcbDCkHDSVzPMmHOuJkYfN_tD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45, ___tjEGQFIUcwJTYTHdBnBpHIbqxDB_4)); }
	inline int32_t get_tjEGQFIUcwJTYTHdBnBpHIbqxDB_4() const { return ___tjEGQFIUcwJTYTHdBnBpHIbqxDB_4; }
	inline int32_t* get_address_of_tjEGQFIUcwJTYTHdBnBpHIbqxDB_4() { return &___tjEGQFIUcwJTYTHdBnBpHIbqxDB_4; }
	inline void set_tjEGQFIUcwJTYTHdBnBpHIbqxDB_4(int32_t value)
	{
		___tjEGQFIUcwJTYTHdBnBpHIbqxDB_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WSOEYLLCBDCKHDSVZPMMHOUJKYFN_TD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45_H
#ifndef ACVLMOGGLDKOUPDTQPCQXIVQGDM_T1E02138B73D8E7AD4E096B0516E972034C3FF6CA_H
#define ACVLMOGGLDKOUPDTQPCQXIVQGDM_T1E02138B73D8E7AD4E096B0516E972034C3FF6CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements_AcvlMOggldKoUpDTQpCQxIVQgdM
struct  AcvlMOggldKoUpDTQpCQxIVQgdM_t1E02138B73D8E7AD4E096B0516E972034C3FF6CA  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements_AcvlMOggldKoUpDTQpCQxIVQgdM::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements_AcvlMOggldKoUpDTQpCQxIVQgdM::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements_AcvlMOggldKoUpDTQpCQxIVQgdM::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements_AcvlMOggldKoUpDTQpCQxIVQgdM::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements_AcvlMOggldKoUpDTQpCQxIVQgdM::oMDcJiLwPdVaAFmNNnpqnUvSUJd
	int32_t ___oMDcJiLwPdVaAFmNNnpqnUvSUJd_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(AcvlMOggldKoUpDTQpCQxIVQgdM_t1E02138B73D8E7AD4E096B0516E972034C3FF6CA, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(AcvlMOggldKoUpDTQpCQxIVQgdM_t1E02138B73D8E7AD4E096B0516E972034C3FF6CA, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(AcvlMOggldKoUpDTQpCQxIVQgdM_t1E02138B73D8E7AD4E096B0516E972034C3FF6CA, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(AcvlMOggldKoUpDTQpCQxIVQgdM_t1E02138B73D8E7AD4E096B0516E972034C3FF6CA, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_oMDcJiLwPdVaAFmNNnpqnUvSUJd_4() { return static_cast<int32_t>(offsetof(AcvlMOggldKoUpDTQpCQxIVQgdM_t1E02138B73D8E7AD4E096B0516E972034C3FF6CA, ___oMDcJiLwPdVaAFmNNnpqnUvSUJd_4)); }
	inline int32_t get_oMDcJiLwPdVaAFmNNnpqnUvSUJd_4() const { return ___oMDcJiLwPdVaAFmNNnpqnUvSUJd_4; }
	inline int32_t* get_address_of_oMDcJiLwPdVaAFmNNnpqnUvSUJd_4() { return &___oMDcJiLwPdVaAFmNNnpqnUvSUJd_4; }
	inline void set_oMDcJiLwPdVaAFmNNnpqnUvSUJd_4(int32_t value)
	{
		___oMDcJiLwPdVaAFmNNnpqnUvSUJd_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACVLMOGGLDKOUPDTQPCQXIVQGDM_T1E02138B73D8E7AD4E096B0516E972034C3FF6CA_H
#ifndef FSMGHSEDZRLFJDUXAERKIYDWEHNP_T802E03EBF9E138F1CB35CE01E014058519FA7223_H
#define FSMGHSEDZRLFJDUXAERKIYDWEHNP_T802E03EBF9E138F1CB35CE01E014058519FA7223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements_FSmGhsedzRlfJDUxaERkIydwEhNP
struct  FSmGhsedzRlfJDUxaERkIydwEhNP_t802E03EBF9E138F1CB35CE01E014058519FA7223  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements_FSmGhsedzRlfJDUxaERkIydwEhNP::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements_FSmGhsedzRlfJDUxaERkIydwEhNP::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements_FSmGhsedzRlfJDUxaERkIydwEhNP::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements_FSmGhsedzRlfJDUxaERkIydwEhNP::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements_FSmGhsedzRlfJDUxaERkIydwEhNP::vpBmBZQKTtXaqRCsqdgzeKCwohQk
	int32_t ___vpBmBZQKTtXaqRCsqdgzeKCwohQk_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(FSmGhsedzRlfJDUxaERkIydwEhNP_t802E03EBF9E138F1CB35CE01E014058519FA7223, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(FSmGhsedzRlfJDUxaERkIydwEhNP_t802E03EBF9E138F1CB35CE01E014058519FA7223, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(FSmGhsedzRlfJDUxaERkIydwEhNP_t802E03EBF9E138F1CB35CE01E014058519FA7223, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(FSmGhsedzRlfJDUxaERkIydwEhNP_t802E03EBF9E138F1CB35CE01E014058519FA7223, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_vpBmBZQKTtXaqRCsqdgzeKCwohQk_4() { return static_cast<int32_t>(offsetof(FSmGhsedzRlfJDUxaERkIydwEhNP_t802E03EBF9E138F1CB35CE01E014058519FA7223, ___vpBmBZQKTtXaqRCsqdgzeKCwohQk_4)); }
	inline int32_t get_vpBmBZQKTtXaqRCsqdgzeKCwohQk_4() const { return ___vpBmBZQKTtXaqRCsqdgzeKCwohQk_4; }
	inline int32_t* get_address_of_vpBmBZQKTtXaqRCsqdgzeKCwohQk_4() { return &___vpBmBZQKTtXaqRCsqdgzeKCwohQk_4; }
	inline void set_vpBmBZQKTtXaqRCsqdgzeKCwohQk_4(int32_t value)
	{
		___vpBmBZQKTtXaqRCsqdgzeKCwohQk_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMGHSEDZRLFJDUXAERKIYDWEHNP_T802E03EBF9E138F1CB35CE01E014058519FA7223_H
#ifndef IPZDZKISXPBVDVECFSEDABXIKCPC_TEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61_H
#define IPZDZKISXPBVDVECFSEDABXIKCPC_TEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_IPzDzkiSxPbvDVecfsEDabxIkCpc
struct  IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_IPzDzkiSxPbvDVecfsEDabxIkCpc::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_IPzDzkiSxPbvDVecfsEDabxIkCpc::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_IPzDzkiSxPbvDVecfsEDabxIkCpc::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_IPzDzkiSxPbvDVecfsEDabxIkCpc::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_IPzDzkiSxPbvDVecfsEDabxIkCpc::BXLOLMZBogcBmMGYWQumhNAcSYm
	int32_t ___BXLOLMZBogcBmMGYWQumhNAcSYm_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_IPzDzkiSxPbvDVecfsEDabxIkCpc::qDqFYLAqYOxxjQpeeugfBYOVEAT
	int32_t ___qDqFYLAqYOxxjQpeeugfBYOVEAT_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_BXLOLMZBogcBmMGYWQumhNAcSYm_4() { return static_cast<int32_t>(offsetof(IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61, ___BXLOLMZBogcBmMGYWQumhNAcSYm_4)); }
	inline int32_t get_BXLOLMZBogcBmMGYWQumhNAcSYm_4() const { return ___BXLOLMZBogcBmMGYWQumhNAcSYm_4; }
	inline int32_t* get_address_of_BXLOLMZBogcBmMGYWQumhNAcSYm_4() { return &___BXLOLMZBogcBmMGYWQumhNAcSYm_4; }
	inline void set_BXLOLMZBogcBmMGYWQumhNAcSYm_4(int32_t value)
	{
		___BXLOLMZBogcBmMGYWQumhNAcSYm_4 = value;
	}

	inline static int32_t get_offset_of_qDqFYLAqYOxxjQpeeugfBYOVEAT_5() { return static_cast<int32_t>(offsetof(IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61, ___qDqFYLAqYOxxjQpeeugfBYOVEAT_5)); }
	inline int32_t get_qDqFYLAqYOxxjQpeeugfBYOVEAT_5() const { return ___qDqFYLAqYOxxjQpeeugfBYOVEAT_5; }
	inline int32_t* get_address_of_qDqFYLAqYOxxjQpeeugfBYOVEAT_5() { return &___qDqFYLAqYOxxjQpeeugfBYOVEAT_5; }
	inline void set_qDqFYLAqYOxxjQpeeugfBYOVEAT_5(int32_t value)
	{
		___qDqFYLAqYOxxjQpeeugfBYOVEAT_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPZDZKISXPBVDVECFSEDABXIKCPC_TEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61_H
#ifndef NPBXBYKOXXMJHQFTYGFZKIKUKTUH_T0500A93E6852674F8754388994800AB506033D7C_H
#define NPBXBYKOXXMJHQFTYGFZKIKUKTUH_T0500A93E6852674F8754388994800AB506033D7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_NPbxByKOXxmjhqftYGFZkikukTUH
struct  NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_NPbxByKOXxmjhqftYGFZkikukTUH::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_NPbxByKOXxmjhqftYGFZkikukTUH::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_NPbxByKOXxmjhqftYGFZkikukTUH::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_NPbxByKOXxmjhqftYGFZkikukTUH::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_NPbxByKOXxmjhqftYGFZkikukTUH::odjjEEjTowHPzNXuzFzehxnukcuI
	int32_t ___odjjEEjTowHPzNXuzFzehxnukcuI_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_NPbxByKOXxmjhqftYGFZkikukTUH::yajjhtCdaFAuyeEjaMneAlOTqlgo
	int32_t ___yajjhtCdaFAuyeEjaMneAlOTqlgo_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_odjjEEjTowHPzNXuzFzehxnukcuI_4() { return static_cast<int32_t>(offsetof(NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C, ___odjjEEjTowHPzNXuzFzehxnukcuI_4)); }
	inline int32_t get_odjjEEjTowHPzNXuzFzehxnukcuI_4() const { return ___odjjEEjTowHPzNXuzFzehxnukcuI_4; }
	inline int32_t* get_address_of_odjjEEjTowHPzNXuzFzehxnukcuI_4() { return &___odjjEEjTowHPzNXuzFzehxnukcuI_4; }
	inline void set_odjjEEjTowHPzNXuzFzehxnukcuI_4(int32_t value)
	{
		___odjjEEjTowHPzNXuzFzehxnukcuI_4 = value;
	}

	inline static int32_t get_offset_of_yajjhtCdaFAuyeEjaMneAlOTqlgo_5() { return static_cast<int32_t>(offsetof(NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C, ___yajjhtCdaFAuyeEjaMneAlOTqlgo_5)); }
	inline int32_t get_yajjhtCdaFAuyeEjaMneAlOTqlgo_5() const { return ___yajjhtCdaFAuyeEjaMneAlOTqlgo_5; }
	inline int32_t* get_address_of_yajjhtCdaFAuyeEjaMneAlOTqlgo_5() { return &___yajjhtCdaFAuyeEjaMneAlOTqlgo_5; }
	inline void set_yajjhtCdaFAuyeEjaMneAlOTqlgo_5(int32_t value)
	{
		___yajjhtCdaFAuyeEjaMneAlOTqlgo_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NPBXBYKOXXMJHQFTYGFZKIKUKTUH_T0500A93E6852674F8754388994800AB506033D7C_H
#ifndef ELEMENT_T4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B_H
#define ELEMENT_T4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Element
struct  Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.CustomCalculation Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Element::customCalculation
	CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * ___customCalculation_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_CustomCalculationSourceData[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Element::customCalculationSourceData
	CustomCalculationSourceDataU5BU5D_t034AAEDA41E4C78D3D656BBE7D542F430FDB5CC9* ___customCalculationSourceData_1;

public:
	inline static int32_t get_offset_of_customCalculation_0() { return static_cast<int32_t>(offsetof(Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B, ___customCalculation_0)); }
	inline CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * get_customCalculation_0() const { return ___customCalculation_0; }
	inline CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 ** get_address_of_customCalculation_0() { return &___customCalculation_0; }
	inline void set_customCalculation_0(CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * value)
	{
		___customCalculation_0 = value;
		Il2CppCodeGenWriteBarrier((&___customCalculation_0), value);
	}

	inline static int32_t get_offset_of_customCalculationSourceData_1() { return static_cast<int32_t>(offsetof(Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B, ___customCalculationSourceData_1)); }
	inline CustomCalculationSourceDataU5BU5D_t034AAEDA41E4C78D3D656BBE7D542F430FDB5CC9* get_customCalculationSourceData_1() const { return ___customCalculationSourceData_1; }
	inline CustomCalculationSourceDataU5BU5D_t034AAEDA41E4C78D3D656BBE7D542F430FDB5CC9** get_address_of_customCalculationSourceData_1() { return &___customCalculationSourceData_1; }
	inline void set_customCalculationSourceData_1(CustomCalculationSourceDataU5BU5D_t034AAEDA41E4C78D3D656BBE7D542F430FDB5CC9* value)
	{
		___customCalculationSourceData_1 = value;
		Il2CppCodeGenWriteBarrier((&___customCalculationSourceData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B_H
#ifndef CBYRBUOFLWKKKFYWRACZWHNLKCT_T378F364C3D26696A028063BA83B584BDF834D34C_H
#define CBYRBUOFLWKKKFYWRACZWHNLKCT_T378F364C3D26696A028063BA83B584BDF834D34C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_CbYrBUoflWkKkFywRaCZWHNlKct
struct  CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_CbYrBUoflWkKkFywRaCZWHNlKct::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_CbYrBUoflWkKkFywRaCZWHNlKct::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_CbYrBUoflWkKkFywRaCZWHNlKct::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_CbYrBUoflWkKkFywRaCZWHNlKct::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_CbYrBUoflWkKkFywRaCZWHNlKct::qezRwTMBGSoPxCwVZyactNiNcZu
	int32_t ___qezRwTMBGSoPxCwVZyactNiNcZu_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_CbYrBUoflWkKkFywRaCZWHNlKct::iOmcUTkiFhvjVwTnokaAVyvPYjd
	int32_t ___iOmcUTkiFhvjVwTnokaAVyvPYjd_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_qezRwTMBGSoPxCwVZyactNiNcZu_4() { return static_cast<int32_t>(offsetof(CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C, ___qezRwTMBGSoPxCwVZyactNiNcZu_4)); }
	inline int32_t get_qezRwTMBGSoPxCwVZyactNiNcZu_4() const { return ___qezRwTMBGSoPxCwVZyactNiNcZu_4; }
	inline int32_t* get_address_of_qezRwTMBGSoPxCwVZyactNiNcZu_4() { return &___qezRwTMBGSoPxCwVZyactNiNcZu_4; }
	inline void set_qezRwTMBGSoPxCwVZyactNiNcZu_4(int32_t value)
	{
		___qezRwTMBGSoPxCwVZyactNiNcZu_4 = value;
	}

	inline static int32_t get_offset_of_iOmcUTkiFhvjVwTnokaAVyvPYjd_5() { return static_cast<int32_t>(offsetof(CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C, ___iOmcUTkiFhvjVwTnokaAVyvPYjd_5)); }
	inline int32_t get_iOmcUTkiFhvjVwTnokaAVyvPYjd_5() const { return ___iOmcUTkiFhvjVwTnokaAVyvPYjd_5; }
	inline int32_t* get_address_of_iOmcUTkiFhvjVwTnokaAVyvPYjd_5() { return &___iOmcUTkiFhvjVwTnokaAVyvPYjd_5; }
	inline void set_iOmcUTkiFhvjVwTnokaAVyvPYjd_5(int32_t value)
	{
		___iOmcUTkiFhvjVwTnokaAVyvPYjd_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBYRBUOFLWKKKFYWRACZWHNLKCT_T378F364C3D26696A028063BA83B584BDF834D34C_H
#ifndef ELEMENT_TED0CF25F632E14F6E9BA2C7306E99D690D242165_H
#define ELEMENT_TED0CF25F632E14F6E9BA2C7306E99D690D242165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Element
struct  Element_tED0CF25F632E14F6E9BA2C7306E99D690D242165  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_TED0CF25F632E14F6E9BA2C7306E99D690D242165_H
#ifndef EZRBMMEZJZCGNWLEWCXWAXYHEKVU_T67317305B1D1EC94CA3475BD79BC9B4340089DED_H
#define EZRBMMEZJZCGNWLEWCXWAXYHEKVU_T67317305B1D1EC94CA3475BD79BC9B4340089DED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements_EzrBmmezJZcgNWLEwCxWAxyHeKVu
struct  EzrBmmezJZcgNWLEwCxWAxyHeKVu_t67317305B1D1EC94CA3475BD79BC9B4340089DED  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements_EzrBmmezJZcgNWLEwCxWAxyHeKVu::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements_EzrBmmezJZcgNWLEwCxWAxyHeKVu::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements_EzrBmmezJZcgNWLEwCxWAxyHeKVu::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements_EzrBmmezJZcgNWLEwCxWAxyHeKVu::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements_EzrBmmezJZcgNWLEwCxWAxyHeKVu::PSbIQIpCKJBXibHiMTgFqKrtegiC
	int32_t ___PSbIQIpCKJBXibHiMTgFqKrtegiC_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(EzrBmmezJZcgNWLEwCxWAxyHeKVu_t67317305B1D1EC94CA3475BD79BC9B4340089DED, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(EzrBmmezJZcgNWLEwCxWAxyHeKVu_t67317305B1D1EC94CA3475BD79BC9B4340089DED, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(EzrBmmezJZcgNWLEwCxWAxyHeKVu_t67317305B1D1EC94CA3475BD79BC9B4340089DED, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(EzrBmmezJZcgNWLEwCxWAxyHeKVu_t67317305B1D1EC94CA3475BD79BC9B4340089DED, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_PSbIQIpCKJBXibHiMTgFqKrtegiC_4() { return static_cast<int32_t>(offsetof(EzrBmmezJZcgNWLEwCxWAxyHeKVu_t67317305B1D1EC94CA3475BD79BC9B4340089DED, ___PSbIQIpCKJBXibHiMTgFqKrtegiC_4)); }
	inline int32_t get_PSbIQIpCKJBXibHiMTgFqKrtegiC_4() const { return ___PSbIQIpCKJBXibHiMTgFqKrtegiC_4; }
	inline int32_t* get_address_of_PSbIQIpCKJBXibHiMTgFqKrtegiC_4() { return &___PSbIQIpCKJBXibHiMTgFqKrtegiC_4; }
	inline void set_PSbIQIpCKJBXibHiMTgFqKrtegiC_4(int32_t value)
	{
		___PSbIQIpCKJBXibHiMTgFqKrtegiC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EZRBMMEZJZCGNWLEWCXWAXYHEKVU_T67317305B1D1EC94CA3475BD79BC9B4340089DED_H
#ifndef AUIXXILSXEXKUMCHLGIRLQZPNRV_T8FDC6F1391128F1A019739F17E4CE89128965676_H
#define AUIXXILSXEXKUMCHLGIRLQZPNRV_T8FDC6F1391128F1A019739F17E4CE89128965676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements_auIXxilsxExkUmCHlGIrLqzPnrV
struct  auIXxilsxExkUmCHlGIrLqzPnrV_t8FDC6F1391128F1A019739F17E4CE89128965676  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements_auIXxilsxExkUmCHlGIrLqzPnrV::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements_auIXxilsxExkUmCHlGIrLqzPnrV::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements_auIXxilsxExkUmCHlGIrLqzPnrV::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements_auIXxilsxExkUmCHlGIrLqzPnrV::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements_auIXxilsxExkUmCHlGIrLqzPnrV::bezskRSuRBOvUGZWknHDblQTAeO
	int32_t ___bezskRSuRBOvUGZWknHDblQTAeO_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(auIXxilsxExkUmCHlGIrLqzPnrV_t8FDC6F1391128F1A019739F17E4CE89128965676, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(auIXxilsxExkUmCHlGIrLqzPnrV_t8FDC6F1391128F1A019739F17E4CE89128965676, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(auIXxilsxExkUmCHlGIrLqzPnrV_t8FDC6F1391128F1A019739F17E4CE89128965676, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(auIXxilsxExkUmCHlGIrLqzPnrV_t8FDC6F1391128F1A019739F17E4CE89128965676, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_bezskRSuRBOvUGZWknHDblQTAeO_4() { return static_cast<int32_t>(offsetof(auIXxilsxExkUmCHlGIrLqzPnrV_t8FDC6F1391128F1A019739F17E4CE89128965676, ___bezskRSuRBOvUGZWknHDblQTAeO_4)); }
	inline int32_t get_bezskRSuRBOvUGZWknHDblQTAeO_4() const { return ___bezskRSuRBOvUGZWknHDblQTAeO_4; }
	inline int32_t* get_address_of_bezskRSuRBOvUGZWknHDblQTAeO_4() { return &___bezskRSuRBOvUGZWknHDblQTAeO_4; }
	inline void set_bezskRSuRBOvUGZWknHDblQTAeO_4(int32_t value)
	{
		___bezskRSuRBOvUGZWknHDblQTAeO_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUIXXILSXEXKUMCHLGIRLQZPNRV_T8FDC6F1391128F1A019739F17E4CE89128965676_H
#ifndef YUXLUPCTQJDYISQSWESUTZQOHRG_T26151B60139C0718D9BCB78339D0EB246A83D601_H
#define YUXLUPCTQJDYISQSWESUTZQOHRG_T26151B60139C0718D9BCB78339D0EB246A83D601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_YuXlupCtQjDyISQsWesUTZQohRG
struct  YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_YuXlupCtQjDyISQsWesUTZQohRG::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_YuXlupCtQjDyISQsWesUTZQohRG::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_YuXlupCtQjDyISQsWesUTZQohRG::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_YuXlupCtQjDyISQsWesUTZQohRG::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_YuXlupCtQjDyISQsWesUTZQohRG::qqQOJqBmqlfRUbpwwFDysYTsitt
	int32_t ___qqQOJqBmqlfRUbpwwFDysYTsitt_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_YuXlupCtQjDyISQsWesUTZQohRG::AWruhtCZVwDWOojDzqRDFmWhfxmd
	int32_t ___AWruhtCZVwDWOojDzqRDFmWhfxmd_5;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_qqQOJqBmqlfRUbpwwFDysYTsitt_4() { return static_cast<int32_t>(offsetof(YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601, ___qqQOJqBmqlfRUbpwwFDysYTsitt_4)); }
	inline int32_t get_qqQOJqBmqlfRUbpwwFDysYTsitt_4() const { return ___qqQOJqBmqlfRUbpwwFDysYTsitt_4; }
	inline int32_t* get_address_of_qqQOJqBmqlfRUbpwwFDysYTsitt_4() { return &___qqQOJqBmqlfRUbpwwFDysYTsitt_4; }
	inline void set_qqQOJqBmqlfRUbpwwFDysYTsitt_4(int32_t value)
	{
		___qqQOJqBmqlfRUbpwwFDysYTsitt_4 = value;
	}

	inline static int32_t get_offset_of_AWruhtCZVwDWOojDzqRDFmWhfxmd_5() { return static_cast<int32_t>(offsetof(YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601, ___AWruhtCZVwDWOojDzqRDFmWhfxmd_5)); }
	inline int32_t get_AWruhtCZVwDWOojDzqRDFmWhfxmd_5() const { return ___AWruhtCZVwDWOojDzqRDFmWhfxmd_5; }
	inline int32_t* get_address_of_AWruhtCZVwDWOojDzqRDFmWhfxmd_5() { return &___AWruhtCZVwDWOojDzqRDFmWhfxmd_5; }
	inline void set_AWruhtCZVwDWOojDzqRDFmWhfxmd_5(int32_t value)
	{
		___AWruhtCZVwDWOojDzqRDFmWhfxmd_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YUXLUPCTQJDYISQSWESUTZQOHRG_T26151B60139C0718D9BCB78339D0EB246A83D601_H
#ifndef DKZKQUVGWEVROIIYOPIDIQHOFUI_T09C2988966CB4D7A016ADE4572343C49E06183CC_H
#define DKZKQUVGWEVROIIYOPIDIQHOFUI_T09C2988966CB4D7A016ADE4572343C49E06183CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_DkzkQUVgWEVroIiyopIDIqHOfUi
struct  DkzkQUVgWEVroIiyopIDIqHOfUi_t09C2988966CB4D7A016ADE4572343C49E06183CC  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_DkzkQUVgWEVroIiyopIDIqHOfUi::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_DkzkQUVgWEVroIiyopIDIqHOfUi::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_DkzkQUVgWEVroIiyopIDIqHOfUi::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_DkzkQUVgWEVroIiyopIDIqHOfUi::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_DkzkQUVgWEVroIiyopIDIqHOfUi::EChnlGrdrUcyXYSPZFpLOLQYgAx
	int32_t ___EChnlGrdrUcyXYSPZFpLOLQYgAx_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(DkzkQUVgWEVroIiyopIDIqHOfUi_t09C2988966CB4D7A016ADE4572343C49E06183CC, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(DkzkQUVgWEVroIiyopIDIqHOfUi_t09C2988966CB4D7A016ADE4572343C49E06183CC, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(DkzkQUVgWEVroIiyopIDIqHOfUi_t09C2988966CB4D7A016ADE4572343C49E06183CC, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(DkzkQUVgWEVroIiyopIDIqHOfUi_t09C2988966CB4D7A016ADE4572343C49E06183CC, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_EChnlGrdrUcyXYSPZFpLOLQYgAx_4() { return static_cast<int32_t>(offsetof(DkzkQUVgWEVroIiyopIDIqHOfUi_t09C2988966CB4D7A016ADE4572343C49E06183CC, ___EChnlGrdrUcyXYSPZFpLOLQYgAx_4)); }
	inline int32_t get_EChnlGrdrUcyXYSPZFpLOLQYgAx_4() const { return ___EChnlGrdrUcyXYSPZFpLOLQYgAx_4; }
	inline int32_t* get_address_of_EChnlGrdrUcyXYSPZFpLOLQYgAx_4() { return &___EChnlGrdrUcyXYSPZFpLOLQYgAx_4; }
	inline void set_EChnlGrdrUcyXYSPZFpLOLQYgAx_4(int32_t value)
	{
		___EChnlGrdrUcyXYSPZFpLOLQYgAx_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DKZKQUVGWEVROIIYOPIDIQHOFUI_T09C2988966CB4D7A016ADE4572343C49E06183CC_H
#ifndef OLAQCLUTWZUENKIDNDJPFWSPLUOD_TB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4_H
#define OLAQCLUTWZUENKIDNDJPFWSPLUOD_TB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_oLaQcLUTwZuenKIDnDjpFwSPluoD
struct  oLaQcLUTwZuenKIDnDjpFwSPluoD_tB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_oLaQcLUTwZuenKIDnDjpFwSPluoD::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_tF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_oLaQcLUTwZuenKIDnDjpFwSPluoD::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_oLaQcLUTwZuenKIDnDjpFwSPluoD::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_oLaQcLUTwZuenKIDnDjpFwSPluoD::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_oLaQcLUTwZuenKIDnDjpFwSPluoD::dzuPtKoIYbgBLPBoLcaghKtcmgZW
	int32_t ___dzuPtKoIYbgBLPBoLcaghKtcmgZW_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(oLaQcLUTwZuenKIDnDjpFwSPluoD_tB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_tF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_tF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_tF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(oLaQcLUTwZuenKIDnDjpFwSPluoD_tB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(oLaQcLUTwZuenKIDnDjpFwSPluoD_tB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(oLaQcLUTwZuenKIDnDjpFwSPluoD_tB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_dzuPtKoIYbgBLPBoLcaghKtcmgZW_4() { return static_cast<int32_t>(offsetof(oLaQcLUTwZuenKIDnDjpFwSPluoD_tB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4, ___dzuPtKoIYbgBLPBoLcaghKtcmgZW_4)); }
	inline int32_t get_dzuPtKoIYbgBLPBoLcaghKtcmgZW_4() const { return ___dzuPtKoIYbgBLPBoLcaghKtcmgZW_4; }
	inline int32_t* get_address_of_dzuPtKoIYbgBLPBoLcaghKtcmgZW_4() { return &___dzuPtKoIYbgBLPBoLcaghKtcmgZW_4; }
	inline void set_dzuPtKoIYbgBLPBoLcaghKtcmgZW_4(int32_t value)
	{
		___dzuPtKoIYbgBLPBoLcaghKtcmgZW_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OLAQCLUTWZUENKIDNDJPFWSPLUOD_TB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4_H
#ifndef OXYEOMMIFFKPZCKHLBKZBKWEKKI_TD631ECCAF554F1DB439E852B5EDBD1F724932A10_H
#define OXYEOMMIFFKPZCKHLBKZBKWEKKI_TD631ECCAF554F1DB439E852B5EDBD1F724932A10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_oxYEommIFFkPzcKHlbkZBKwEKki
struct  oxYEommIFFkPzcKHlbkZBKwEKki_tD631ECCAF554F1DB439E852B5EDBD1F724932A10  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_oxYEommIFFkPzcKHlbkZBKwEKki::NOArrsNdfKDShNFftfbPqYDzhpq
	Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_oxYEommIFFkPzcKHlbkZBKwEKki::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_oxYEommIFFkPzcKHlbkZBKwEKki::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_oxYEommIFFkPzcKHlbkZBKwEKki::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_oxYEommIFFkPzcKHlbkZBKwEKki::PzfgrQgniUFZfDJYGGshDnHPKlel
	int32_t ___PzfgrQgniUFZfDJYGGshDnHPKlel_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(oxYEommIFFkPzcKHlbkZBKwEKki_tD631ECCAF554F1DB439E852B5EDBD1F724932A10, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Button_tBB6149DD7A45B873206F15797DC2B002019BF09C ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(oxYEommIFFkPzcKHlbkZBKwEKki_tD631ECCAF554F1DB439E852B5EDBD1F724932A10, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(oxYEommIFFkPzcKHlbkZBKwEKki_tD631ECCAF554F1DB439E852B5EDBD1F724932A10, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(oxYEommIFFkPzcKHlbkZBKwEKki_tD631ECCAF554F1DB439E852B5EDBD1F724932A10, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_PzfgrQgniUFZfDJYGGshDnHPKlel_4() { return static_cast<int32_t>(offsetof(oxYEommIFFkPzcKHlbkZBKwEKki_tD631ECCAF554F1DB439E852B5EDBD1F724932A10, ___PzfgrQgniUFZfDJYGGshDnHPKlel_4)); }
	inline int32_t get_PzfgrQgniUFZfDJYGGshDnHPKlel_4() const { return ___PzfgrQgniUFZfDJYGGshDnHPKlel_4; }
	inline int32_t* get_address_of_PzfgrQgniUFZfDJYGGshDnHPKlel_4() { return &___PzfgrQgniUFZfDJYGGshDnHPKlel_4; }
	inline void set_PzfgrQgniUFZfDJYGGshDnHPKlel_4(int32_t value)
	{
		___PzfgrQgniUFZfDJYGGshDnHPKlel_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OXYEOMMIFFKPZCKHLBKZBKWEKKI_TD631ECCAF554F1DB439E852B5EDBD1F724932A10_H
#ifndef UFZTUEALFBXEUICWZWJYEUDLPLS_T7C532B5037E134428A485D9E012147AAFCEF71B1_H
#define UFZTUEALFBXEUICWZWJYEUDLPLS_T7C532B5037E134428A485D9E012147AAFCEF71B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_ufZtUEALFBXEuicwzwJyeUDlpLS
struct  ufZtUEALFBXEuicwzwJyeUDlpLS_t7C532B5037E134428A485D9E012147AAFCEF71B1  : public RuntimeObject
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_ufZtUEALFBXEuicwzwJyeUDlpLS::NOArrsNdfKDShNFftfbPqYDzhpq
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_ufZtUEALFBXEuicwzwJyeUDlpLS::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_ufZtUEALFBXEuicwzwJyeUDlpLS::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_ufZtUEALFBXEuicwzwJyeUDlpLS::DKaFTWTMhFeLQHDExnDMRxZfmROL
	Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_ufZtUEALFBXEuicwzwJyeUDlpLS::ytvEhjKyNpfGtxRwbRJRcsnETTb
	int32_t ___ytvEhjKyNpfGtxRwbRJRcsnETTb_4;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(ufZtUEALFBXEuicwzwJyeUDlpLS_t7C532B5037E134428A485D9E012147AAFCEF71B1, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(ufZtUEALFBXEuicwzwJyeUDlpLS_t7C532B5037E134428A485D9E012147AAFCEF71B1, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(ufZtUEALFBXEuicwzwJyeUDlpLS_t7C532B5037E134428A485D9E012147AAFCEF71B1, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(ufZtUEALFBXEuicwzwJyeUDlpLS_t7C532B5037E134428A485D9E012147AAFCEF71B1, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_ytvEhjKyNpfGtxRwbRJRcsnETTb_4() { return static_cast<int32_t>(offsetof(ufZtUEALFBXEuicwzwJyeUDlpLS_t7C532B5037E134428A485D9E012147AAFCEF71B1, ___ytvEhjKyNpfGtxRwbRJRcsnETTb_4)); }
	inline int32_t get_ytvEhjKyNpfGtxRwbRJRcsnETTb_4() const { return ___ytvEhjKyNpfGtxRwbRJRcsnETTb_4; }
	inline int32_t* get_address_of_ytvEhjKyNpfGtxRwbRJRcsnETTb_4() { return &___ytvEhjKyNpfGtxRwbRJRcsnETTb_4; }
	inline void set_ytvEhjKyNpfGtxRwbRJRcsnETTb_4(int32_t value)
	{
		___ytvEhjKyNpfGtxRwbRJRcsnETTb_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UFZTUEALFBXEUICWZWJYEUDLPLS_T7C532B5037E134428A485D9E012147AAFCEF71B1_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef PLATFORM_CUSTOM_T38C5DD986F65AF512626A73FA4EA96ECF76A0C77_H
#define PLATFORM_CUSTOM_T38C5DD986F65AF512626A73FA4EA96ECF76A0C77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom
struct  Platform_Custom_t38C5DD986F65AF512626A73FA4EA96ECF76A0C77  : public Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_CUSTOM_T38C5DD986F65AF512626A73FA4EA96ECF76A0C77_H
#ifndef ELEMENTS_TC57D226564DCD7C2CFC9AFA7A212201FCC98129B_H
#define ELEMENTS_TC57D226564DCD7C2CFC9AFA7A212201FCC98129B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Elements
struct  Elements_tC57D226564DCD7C2CFC9AFA7A212201FCC98129B  : public Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_TC57D226564DCD7C2CFC9AFA7A212201FCC98129B_H
#ifndef MATCHINGCRITERIA_T1A1DB371BD17248F7D375FC7FA3B8704BEB1C795_H
#define MATCHINGCRITERIA_T1A1DB371BD17248F7D375FC7FA3B8704BEB1C795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_MatchingCriteria
struct  MatchingCriteria_t1A1DB371BD17248F7D375FC7FA3B8704BEB1C795  : public MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_MatchingCriteria::alwaysMatch
	bool ___alwaysMatch_4;

public:
	inline static int32_t get_offset_of_alwaysMatch_4() { return static_cast<int32_t>(offsetof(MatchingCriteria_t1A1DB371BD17248F7D375FC7FA3B8704BEB1C795, ___alwaysMatch_4)); }
	inline bool get_alwaysMatch_4() const { return ___alwaysMatch_4; }
	inline bool* get_address_of_alwaysMatch_4() { return &___alwaysMatch_4; }
	inline void set_alwaysMatch_4(bool value)
	{
		___alwaysMatch_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_T1A1DB371BD17248F7D375FC7FA3B8704BEB1C795_H
#ifndef PLATFORM_FALLBACK_BASE_T2CAE633FFCFD2EEB508684550513E78535495DA4_H
#define PLATFORM_FALLBACK_BASE_T2CAE633FFCFD2EEB508684550513E78535495DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base
struct  Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4  : public Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base::matchingCriteria
	MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056 * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base::elements
	Elements_tA67426A050B3846F67FA5F1D602FC4752C236EA7 * ___elements_2;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4, ___matchingCriteria_1)); }
	inline MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056 * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056 ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056 * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4, ___elements_2)); }
	inline Elements_tA67426A050B3846F67FA5F1D602FC4752C236EA7 * get_elements_2() const { return ___elements_2; }
	inline Elements_tA67426A050B3846F67FA5F1D602FC4752C236EA7 ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_tA67426A050B3846F67FA5F1D602FC4752C236EA7 * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_FALLBACK_BASE_T2CAE633FFCFD2EEB508684550513E78535495DA4_H
#ifndef ELEMENTS_TA67426A050B3846F67FA5F1D602FC4752C236EA7_H
#define ELEMENTS_TA67426A050B3846F67FA5F1D602FC4752C236EA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Elements
struct  Elements_tA67426A050B3846F67FA5F1D602FC4752C236EA7  : public Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Elements::axes
	AxisU5BU5D_t88F2D8A47455D8334C098EF9135A5D118C79F63D* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Elements::buttons
	ButtonU5BU5D_t26B11DC5F153644B133E626A8A304A1789164DCD* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_tA67426A050B3846F67FA5F1D602FC4752C236EA7, ___axes_0)); }
	inline AxisU5BU5D_t88F2D8A47455D8334C098EF9135A5D118C79F63D* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_t88F2D8A47455D8334C098EF9135A5D118C79F63D** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_t88F2D8A47455D8334C098EF9135A5D118C79F63D* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_tA67426A050B3846F67FA5F1D602FC4752C236EA7, ___buttons_1)); }
	inline ButtonU5BU5D_t26B11DC5F153644B133E626A8A304A1789164DCD* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_t26B11DC5F153644B133E626A8A304A1789164DCD** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_t26B11DC5F153644B133E626A8A304A1789164DCD* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_TA67426A050B3846F67FA5F1D602FC4752C236EA7_H
#ifndef MATCHINGCRITERIA_T11F726FBD5F1B7F1B306F1F4EC04B260A52AF056_H
#define MATCHINGCRITERIA_T11F726FBD5F1B7F1B306F1F4EC04B260A52AF056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_MatchingCriteria
struct  MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056  : public MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_MatchingCriteria::alwaysMatch
	bool ___alwaysMatch_4;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_MatchingCriteria::productName_useRegex
	bool ___productName_useRegex_5;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_MatchingCriteria::productName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productName_6;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_MatchingCriteria::matchUnityVersion
	bool ___matchUnityVersion_7;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_MatchingCriteria::matchUnityVersion_min
	String_t* ___matchUnityVersion_min_8;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_MatchingCriteria::matchUnityVersion_max
	String_t* ___matchUnityVersion_max_9;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_MatchingCriteria::matchSysVersion
	bool ___matchSysVersion_10;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_MatchingCriteria::matchSysVersion_min
	String_t* ___matchSysVersion_min_11;
	// System.String Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_MatchingCriteria::matchSysVersion_max
	String_t* ___matchSysVersion_max_12;

public:
	inline static int32_t get_offset_of_alwaysMatch_4() { return static_cast<int32_t>(offsetof(MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056, ___alwaysMatch_4)); }
	inline bool get_alwaysMatch_4() const { return ___alwaysMatch_4; }
	inline bool* get_address_of_alwaysMatch_4() { return &___alwaysMatch_4; }
	inline void set_alwaysMatch_4(bool value)
	{
		___alwaysMatch_4 = value;
	}

	inline static int32_t get_offset_of_productName_useRegex_5() { return static_cast<int32_t>(offsetof(MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056, ___productName_useRegex_5)); }
	inline bool get_productName_useRegex_5() const { return ___productName_useRegex_5; }
	inline bool* get_address_of_productName_useRegex_5() { return &___productName_useRegex_5; }
	inline void set_productName_useRegex_5(bool value)
	{
		___productName_useRegex_5 = value;
	}

	inline static int32_t get_offset_of_productName_6() { return static_cast<int32_t>(offsetof(MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056, ___productName_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productName_6() const { return ___productName_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productName_6() { return &___productName_6; }
	inline void set_productName_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productName_6 = value;
		Il2CppCodeGenWriteBarrier((&___productName_6), value);
	}

	inline static int32_t get_offset_of_matchUnityVersion_7() { return static_cast<int32_t>(offsetof(MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056, ___matchUnityVersion_7)); }
	inline bool get_matchUnityVersion_7() const { return ___matchUnityVersion_7; }
	inline bool* get_address_of_matchUnityVersion_7() { return &___matchUnityVersion_7; }
	inline void set_matchUnityVersion_7(bool value)
	{
		___matchUnityVersion_7 = value;
	}

	inline static int32_t get_offset_of_matchUnityVersion_min_8() { return static_cast<int32_t>(offsetof(MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056, ___matchUnityVersion_min_8)); }
	inline String_t* get_matchUnityVersion_min_8() const { return ___matchUnityVersion_min_8; }
	inline String_t** get_address_of_matchUnityVersion_min_8() { return &___matchUnityVersion_min_8; }
	inline void set_matchUnityVersion_min_8(String_t* value)
	{
		___matchUnityVersion_min_8 = value;
		Il2CppCodeGenWriteBarrier((&___matchUnityVersion_min_8), value);
	}

	inline static int32_t get_offset_of_matchUnityVersion_max_9() { return static_cast<int32_t>(offsetof(MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056, ___matchUnityVersion_max_9)); }
	inline String_t* get_matchUnityVersion_max_9() const { return ___matchUnityVersion_max_9; }
	inline String_t** get_address_of_matchUnityVersion_max_9() { return &___matchUnityVersion_max_9; }
	inline void set_matchUnityVersion_max_9(String_t* value)
	{
		___matchUnityVersion_max_9 = value;
		Il2CppCodeGenWriteBarrier((&___matchUnityVersion_max_9), value);
	}

	inline static int32_t get_offset_of_matchSysVersion_10() { return static_cast<int32_t>(offsetof(MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056, ___matchSysVersion_10)); }
	inline bool get_matchSysVersion_10() const { return ___matchSysVersion_10; }
	inline bool* get_address_of_matchSysVersion_10() { return &___matchSysVersion_10; }
	inline void set_matchSysVersion_10(bool value)
	{
		___matchSysVersion_10 = value;
	}

	inline static int32_t get_offset_of_matchSysVersion_min_11() { return static_cast<int32_t>(offsetof(MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056, ___matchSysVersion_min_11)); }
	inline String_t* get_matchSysVersion_min_11() const { return ___matchSysVersion_min_11; }
	inline String_t** get_address_of_matchSysVersion_min_11() { return &___matchSysVersion_min_11; }
	inline void set_matchSysVersion_min_11(String_t* value)
	{
		___matchSysVersion_min_11 = value;
		Il2CppCodeGenWriteBarrier((&___matchSysVersion_min_11), value);
	}

	inline static int32_t get_offset_of_matchSysVersion_max_12() { return static_cast<int32_t>(offsetof(MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056, ___matchSysVersion_max_12)); }
	inline String_t* get_matchSysVersion_max_12() const { return ___matchSysVersion_max_12; }
	inline String_t** get_address_of_matchSysVersion_max_12() { return &___matchSysVersion_max_12; }
	inline void set_matchSysVersion_max_12(String_t* value)
	{
		___matchSysVersion_max_12 = value;
		Il2CppCodeGenWriteBarrier((&___matchSysVersion_max_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_T11F726FBD5F1B7F1B306F1F4EC04B260A52AF056_H
#ifndef PLATFORM_LINUX_BASE_T2DC043F85ECFFC3426B89E036ED8F4EA86D86E03_H
#define PLATFORM_LINUX_BASE_T2DC043F85ECFFC3426B89E036ED8F4EA86D86E03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base
struct  Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03  : public Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base::matchingCriteria
	MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2 * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base::elements
	Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF * ___elements_2;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03, ___matchingCriteria_1)); }
	inline MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2 * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2 ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2 * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03, ___elements_2)); }
	inline Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF * get_elements_2() const { return ___elements_2; }
	inline Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_LINUX_BASE_T2DC043F85ECFFC3426B89E036ED8F4EA86D86E03_H
#ifndef ELEMENTS_TB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF_H
#define ELEMENTS_TB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements
struct  Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF  : public Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements::axes
	AxisU5BU5D_t6D0AE6929E9536CD3A6FE8D76060F5A397CB15B4* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Elements::buttons
	ButtonU5BU5D_t994BE4F6605F4647B2EC411577AD246F36469884* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF, ___axes_0)); }
	inline AxisU5BU5D_t6D0AE6929E9536CD3A6FE8D76060F5A397CB15B4* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_t6D0AE6929E9536CD3A6FE8D76060F5A397CB15B4** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_t6D0AE6929E9536CD3A6FE8D76060F5A397CB15B4* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF, ___buttons_1)); }
	inline ButtonU5BU5D_t994BE4F6605F4647B2EC411577AD246F36469884* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_t994BE4F6605F4647B2EC411577AD246F36469884** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_t994BE4F6605F4647B2EC411577AD246F36469884* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_TB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF_H
#ifndef MATCHINGCRITERIA_T0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2_H
#define MATCHINGCRITERIA_T0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria
struct  MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2  : public MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria::hatCount
	int32_t ___hatCount_4;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria_ElementCount[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria::alternateElementCounts
	ElementCountU5BU5D_tA1D45617688A9FC87702229BE07F7187346472C6* ___alternateElementCounts_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria::manufacturer_useRegex
	bool ___manufacturer_useRegex_6;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria::productName_useRegex
	bool ___productName_useRegex_7;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria::systemName_useRegex
	bool ___systemName_useRegex_8;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria::manufacturer
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___manufacturer_9;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria::productName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productName_10;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria::systemName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___systemName_11;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria::productGUID
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productGUID_12;

public:
	inline static int32_t get_offset_of_hatCount_4() { return static_cast<int32_t>(offsetof(MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2, ___hatCount_4)); }
	inline int32_t get_hatCount_4() const { return ___hatCount_4; }
	inline int32_t* get_address_of_hatCount_4() { return &___hatCount_4; }
	inline void set_hatCount_4(int32_t value)
	{
		___hatCount_4 = value;
	}

	inline static int32_t get_offset_of_alternateElementCounts_5() { return static_cast<int32_t>(offsetof(MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2, ___alternateElementCounts_5)); }
	inline ElementCountU5BU5D_tA1D45617688A9FC87702229BE07F7187346472C6* get_alternateElementCounts_5() const { return ___alternateElementCounts_5; }
	inline ElementCountU5BU5D_tA1D45617688A9FC87702229BE07F7187346472C6** get_address_of_alternateElementCounts_5() { return &___alternateElementCounts_5; }
	inline void set_alternateElementCounts_5(ElementCountU5BU5D_tA1D45617688A9FC87702229BE07F7187346472C6* value)
	{
		___alternateElementCounts_5 = value;
		Il2CppCodeGenWriteBarrier((&___alternateElementCounts_5), value);
	}

	inline static int32_t get_offset_of_manufacturer_useRegex_6() { return static_cast<int32_t>(offsetof(MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2, ___manufacturer_useRegex_6)); }
	inline bool get_manufacturer_useRegex_6() const { return ___manufacturer_useRegex_6; }
	inline bool* get_address_of_manufacturer_useRegex_6() { return &___manufacturer_useRegex_6; }
	inline void set_manufacturer_useRegex_6(bool value)
	{
		___manufacturer_useRegex_6 = value;
	}

	inline static int32_t get_offset_of_productName_useRegex_7() { return static_cast<int32_t>(offsetof(MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2, ___productName_useRegex_7)); }
	inline bool get_productName_useRegex_7() const { return ___productName_useRegex_7; }
	inline bool* get_address_of_productName_useRegex_7() { return &___productName_useRegex_7; }
	inline void set_productName_useRegex_7(bool value)
	{
		___productName_useRegex_7 = value;
	}

	inline static int32_t get_offset_of_systemName_useRegex_8() { return static_cast<int32_t>(offsetof(MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2, ___systemName_useRegex_8)); }
	inline bool get_systemName_useRegex_8() const { return ___systemName_useRegex_8; }
	inline bool* get_address_of_systemName_useRegex_8() { return &___systemName_useRegex_8; }
	inline void set_systemName_useRegex_8(bool value)
	{
		___systemName_useRegex_8 = value;
	}

	inline static int32_t get_offset_of_manufacturer_9() { return static_cast<int32_t>(offsetof(MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2, ___manufacturer_9)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_manufacturer_9() const { return ___manufacturer_9; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_manufacturer_9() { return &___manufacturer_9; }
	inline void set_manufacturer_9(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___manufacturer_9 = value;
		Il2CppCodeGenWriteBarrier((&___manufacturer_9), value);
	}

	inline static int32_t get_offset_of_productName_10() { return static_cast<int32_t>(offsetof(MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2, ___productName_10)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productName_10() const { return ___productName_10; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productName_10() { return &___productName_10; }
	inline void set_productName_10(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productName_10 = value;
		Il2CppCodeGenWriteBarrier((&___productName_10), value);
	}

	inline static int32_t get_offset_of_systemName_11() { return static_cast<int32_t>(offsetof(MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2, ___systemName_11)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_systemName_11() const { return ___systemName_11; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_systemName_11() { return &___systemName_11; }
	inline void set_systemName_11(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___systemName_11 = value;
		Il2CppCodeGenWriteBarrier((&___systemName_11), value);
	}

	inline static int32_t get_offset_of_productGUID_12() { return static_cast<int32_t>(offsetof(MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2, ___productGUID_12)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productGUID_12() const { return ___productGUID_12; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productGUID_12() { return &___productGUID_12; }
	inline void set_productGUID_12(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productGUID_12 = value;
		Il2CppCodeGenWriteBarrier((&___productGUID_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_T0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2_H
#ifndef ELEMENTCOUNT_TA2D9511A33E8C5F5428D1E9150A3C1D15D4C9FA2_H
#define ELEMENTCOUNT_TA2D9511A33E8C5F5428D1E9150A3C1D15D4C9FA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria_ElementCount
struct  ElementCount_tA2D9511A33E8C5F5428D1E9150A3C1D15D4C9FA2  : public ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_MatchingCriteria_ElementCount::hatCount
	int32_t ___hatCount_2;

public:
	inline static int32_t get_offset_of_hatCount_2() { return static_cast<int32_t>(offsetof(ElementCount_tA2D9511A33E8C5F5428D1E9150A3C1D15D4C9FA2, ___hatCount_2)); }
	inline int32_t get_hatCount_2() const { return ___hatCount_2; }
	inline int32_t* get_address_of_hatCount_2() { return &___hatCount_2; }
	inline void set_hatCount_2(int32_t value)
	{
		___hatCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTCOUNT_TA2D9511A33E8C5F5428D1E9150A3C1D15D4C9FA2_H
#ifndef PLATFORM_OSX_BASE_T8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072_H
#define PLATFORM_OSX_BASE_T8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base
struct  Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072  : public Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base::matchingCriteria
	MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947 * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base::elements
	Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89 * ___elements_2;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072, ___matchingCriteria_1)); }
	inline MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947 * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947 ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947 * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072, ___elements_2)); }
	inline Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89 * get_elements_2() const { return ___elements_2; }
	inline Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89 ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89 * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_OSX_BASE_T8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072_H
#ifndef ELEMENTS_T9717AC13D64B797504FE631C8DD1346D3698BC89_H
#define ELEMENTS_T9717AC13D64B797504FE631C8DD1346D3698BC89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements
struct  Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89  : public Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements::axes
	AxisU5BU5D_tE985187DDC620CCDAAC8D4E2FC59E979A211FE90* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Elements::buttons
	ButtonU5BU5D_tA5A850BF0855D9A2C6F95A30C52D6EDF9C4C9101* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89, ___axes_0)); }
	inline AxisU5BU5D_tE985187DDC620CCDAAC8D4E2FC59E979A211FE90* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_tE985187DDC620CCDAAC8D4E2FC59E979A211FE90** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_tE985187DDC620CCDAAC8D4E2FC59E979A211FE90* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89, ___buttons_1)); }
	inline ButtonU5BU5D_tA5A850BF0855D9A2C6F95A30C52D6EDF9C4C9101* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_tA5A850BF0855D9A2C6F95A30C52D6EDF9C4C9101** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_tA5A850BF0855D9A2C6F95A30C52D6EDF9C4C9101* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_T9717AC13D64B797504FE631C8DD1346D3698BC89_H
#ifndef MATCHINGCRITERIA_T84C62377BE2DE6FD3D3182EE6BF2A9283B826947_H
#define MATCHINGCRITERIA_T84C62377BE2DE6FD3D3182EE6BF2A9283B826947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_MatchingCriteria
struct  MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947  : public MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_MatchingCriteria::hatCount
	int32_t ___hatCount_4;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_MatchingCriteria_ElementCount[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_MatchingCriteria::alternateElementCounts
	ElementCountU5BU5D_tFD1B6C7C083056931387BDBC24A6AE1A8B28CBEA* ___alternateElementCounts_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_MatchingCriteria::productName_useRegex
	bool ___productName_useRegex_6;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_MatchingCriteria::productName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productName_7;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_MatchingCriteria::manufacturer
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___manufacturer_8;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_MatchingCriteria::productId
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___productId_9;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_MatchingCriteria::vendorId
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___vendorId_10;

public:
	inline static int32_t get_offset_of_hatCount_4() { return static_cast<int32_t>(offsetof(MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947, ___hatCount_4)); }
	inline int32_t get_hatCount_4() const { return ___hatCount_4; }
	inline int32_t* get_address_of_hatCount_4() { return &___hatCount_4; }
	inline void set_hatCount_4(int32_t value)
	{
		___hatCount_4 = value;
	}

	inline static int32_t get_offset_of_alternateElementCounts_5() { return static_cast<int32_t>(offsetof(MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947, ___alternateElementCounts_5)); }
	inline ElementCountU5BU5D_tFD1B6C7C083056931387BDBC24A6AE1A8B28CBEA* get_alternateElementCounts_5() const { return ___alternateElementCounts_5; }
	inline ElementCountU5BU5D_tFD1B6C7C083056931387BDBC24A6AE1A8B28CBEA** get_address_of_alternateElementCounts_5() { return &___alternateElementCounts_5; }
	inline void set_alternateElementCounts_5(ElementCountU5BU5D_tFD1B6C7C083056931387BDBC24A6AE1A8B28CBEA* value)
	{
		___alternateElementCounts_5 = value;
		Il2CppCodeGenWriteBarrier((&___alternateElementCounts_5), value);
	}

	inline static int32_t get_offset_of_productName_useRegex_6() { return static_cast<int32_t>(offsetof(MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947, ___productName_useRegex_6)); }
	inline bool get_productName_useRegex_6() const { return ___productName_useRegex_6; }
	inline bool* get_address_of_productName_useRegex_6() { return &___productName_useRegex_6; }
	inline void set_productName_useRegex_6(bool value)
	{
		___productName_useRegex_6 = value;
	}

	inline static int32_t get_offset_of_productName_7() { return static_cast<int32_t>(offsetof(MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947, ___productName_7)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productName_7() const { return ___productName_7; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productName_7() { return &___productName_7; }
	inline void set_productName_7(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productName_7 = value;
		Il2CppCodeGenWriteBarrier((&___productName_7), value);
	}

	inline static int32_t get_offset_of_manufacturer_8() { return static_cast<int32_t>(offsetof(MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947, ___manufacturer_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_manufacturer_8() const { return ___manufacturer_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_manufacturer_8() { return &___manufacturer_8; }
	inline void set_manufacturer_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___manufacturer_8 = value;
		Il2CppCodeGenWriteBarrier((&___manufacturer_8), value);
	}

	inline static int32_t get_offset_of_productId_9() { return static_cast<int32_t>(offsetof(MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947, ___productId_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_productId_9() const { return ___productId_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_productId_9() { return &___productId_9; }
	inline void set_productId_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___productId_9 = value;
		Il2CppCodeGenWriteBarrier((&___productId_9), value);
	}

	inline static int32_t get_offset_of_vendorId_10() { return static_cast<int32_t>(offsetof(MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947, ___vendorId_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_vendorId_10() const { return ___vendorId_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_vendorId_10() { return &___vendorId_10; }
	inline void set_vendorId_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___vendorId_10 = value;
		Il2CppCodeGenWriteBarrier((&___vendorId_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_T84C62377BE2DE6FD3D3182EE6BF2A9283B826947_H
#ifndef ELEMENTCOUNT_TC23CFCA7053807F10B101C91E01E08827BA003DE_H
#define ELEMENTCOUNT_TC23CFCA7053807F10B101C91E01E08827BA003DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_MatchingCriteria_ElementCount
struct  ElementCount_tC23CFCA7053807F10B101C91E01E08827BA003DE  : public ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_MatchingCriteria_ElementCount::hatCount
	int32_t ___hatCount_2;

public:
	inline static int32_t get_offset_of_hatCount_2() { return static_cast<int32_t>(offsetof(ElementCount_tC23CFCA7053807F10B101C91E01E08827BA003DE, ___hatCount_2)); }
	inline int32_t get_hatCount_2() const { return ___hatCount_2; }
	inline int32_t* get_address_of_hatCount_2() { return &___hatCount_2; }
	inline void set_hatCount_2(int32_t value)
	{
		___hatCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTCOUNT_TC23CFCA7053807F10B101C91E01E08827BA003DE_H
#ifndef PLATFORM_RAWORDIRECTINPUT_T66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC_H
#define PLATFORM_RAWORDIRECTINPUT_T66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput
struct  Platform_RawOrDirectInput_t66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC  : public Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput::matchingCriteria
	MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3 * ___matchingCriteria_1;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_RawOrDirectInput_t66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC, ___matchingCriteria_1)); }
	inline MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3 * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3 ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_t22BEB7E8789181AE2B0EFFBBFFE8ECC904F039C3 * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_RAWORDIRECTINPUT_T66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC_H
#ifndef ELEMENTS_PLATFORM_BASE_TAA1373033B7FB6EA7E7C0298F2380C7D3EE67CE5_H
#define ELEMENTS_PLATFORM_BASE_TAA1373033B7FB6EA7E7C0298F2380C7D3EE67CE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Elements_Platform_Base
struct  Elements_Platform_Base_tAA1373033B7FB6EA7E7C0298F2380C7D3EE67CE5  : public Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_PLATFORM_BASE_TAA1373033B7FB6EA7E7C0298F2380C7D3EE67CE5_H
#ifndef PLATFORM_WINDOWSUWP_BASE_T96F496871DFB75DF20A4AB2FE3C5B25605F9D14B_H
#define PLATFORM_WINDOWSUWP_BASE_T96F496871DFB75DF20A4AB2FE3C5B25605F9D14B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base
struct  Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B  : public Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base::matchingCriteria
	MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020 * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base::elements
	Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B * ___elements_2;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B, ___matchingCriteria_1)); }
	inline MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020 * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020 ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020 * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B, ___elements_2)); }
	inline Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B * get_elements_2() const { return ___elements_2; }
	inline Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_WINDOWSUWP_BASE_T96F496871DFB75DF20A4AB2FE3C5B25605F9D14B_H
#ifndef ELEMENTS_TBC3CE538CE24E8D280C19F42558B77DDD918F29B_H
#define ELEMENTS_TBC3CE538CE24E8D280C19F42558B77DDD918F29B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements
struct  Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B  : public Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements::axes
	AxisU5BU5D_t422FE88AC2B5158414C55A3359ED8FCC9EBE92BC* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Elements::buttons
	ButtonU5BU5D_t623D6E4399A0B08612A2AE21A137E2E5220BBCA0* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B, ___axes_0)); }
	inline AxisU5BU5D_t422FE88AC2B5158414C55A3359ED8FCC9EBE92BC* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_t422FE88AC2B5158414C55A3359ED8FCC9EBE92BC** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_t422FE88AC2B5158414C55A3359ED8FCC9EBE92BC* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B, ___buttons_1)); }
	inline ButtonU5BU5D_t623D6E4399A0B08612A2AE21A137E2E5220BBCA0* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_t623D6E4399A0B08612A2AE21A137E2E5220BBCA0** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_t623D6E4399A0B08612A2AE21A137E2E5220BBCA0* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_TBC3CE538CE24E8D280C19F42558B77DDD918F29B_H
#ifndef MATCHINGCRITERIA_TC533CB63E9FD91010D2CB407E151B8FDEE098020_H
#define MATCHINGCRITERIA_TC533CB63E9FD91010D2CB407E151B8FDEE098020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_MatchingCriteria
struct  MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020  : public MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_MatchingCriteria::hatCount
	int32_t ___hatCount_4;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_MatchingCriteria_ElementCount[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_MatchingCriteria::alternateElementCounts
	ElementCountU5BU5D_tE428EE68916C80276BAFB23B00B444763324242D* ___alternateElementCounts_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_MatchingCriteria::manufacturer_useRegex
	bool ___manufacturer_useRegex_6;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_MatchingCriteria::productName_useRegex
	bool ___productName_useRegex_7;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_MatchingCriteria::manufacturer
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___manufacturer_8;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_MatchingCriteria::productName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productName_9;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_MatchingCriteria::productGUID
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productGUID_10;

public:
	inline static int32_t get_offset_of_hatCount_4() { return static_cast<int32_t>(offsetof(MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020, ___hatCount_4)); }
	inline int32_t get_hatCount_4() const { return ___hatCount_4; }
	inline int32_t* get_address_of_hatCount_4() { return &___hatCount_4; }
	inline void set_hatCount_4(int32_t value)
	{
		___hatCount_4 = value;
	}

	inline static int32_t get_offset_of_alternateElementCounts_5() { return static_cast<int32_t>(offsetof(MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020, ___alternateElementCounts_5)); }
	inline ElementCountU5BU5D_tE428EE68916C80276BAFB23B00B444763324242D* get_alternateElementCounts_5() const { return ___alternateElementCounts_5; }
	inline ElementCountU5BU5D_tE428EE68916C80276BAFB23B00B444763324242D** get_address_of_alternateElementCounts_5() { return &___alternateElementCounts_5; }
	inline void set_alternateElementCounts_5(ElementCountU5BU5D_tE428EE68916C80276BAFB23B00B444763324242D* value)
	{
		___alternateElementCounts_5 = value;
		Il2CppCodeGenWriteBarrier((&___alternateElementCounts_5), value);
	}

	inline static int32_t get_offset_of_manufacturer_useRegex_6() { return static_cast<int32_t>(offsetof(MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020, ___manufacturer_useRegex_6)); }
	inline bool get_manufacturer_useRegex_6() const { return ___manufacturer_useRegex_6; }
	inline bool* get_address_of_manufacturer_useRegex_6() { return &___manufacturer_useRegex_6; }
	inline void set_manufacturer_useRegex_6(bool value)
	{
		___manufacturer_useRegex_6 = value;
	}

	inline static int32_t get_offset_of_productName_useRegex_7() { return static_cast<int32_t>(offsetof(MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020, ___productName_useRegex_7)); }
	inline bool get_productName_useRegex_7() const { return ___productName_useRegex_7; }
	inline bool* get_address_of_productName_useRegex_7() { return &___productName_useRegex_7; }
	inline void set_productName_useRegex_7(bool value)
	{
		___productName_useRegex_7 = value;
	}

	inline static int32_t get_offset_of_manufacturer_8() { return static_cast<int32_t>(offsetof(MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020, ___manufacturer_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_manufacturer_8() const { return ___manufacturer_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_manufacturer_8() { return &___manufacturer_8; }
	inline void set_manufacturer_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___manufacturer_8 = value;
		Il2CppCodeGenWriteBarrier((&___manufacturer_8), value);
	}

	inline static int32_t get_offset_of_productName_9() { return static_cast<int32_t>(offsetof(MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020, ___productName_9)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productName_9() const { return ___productName_9; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productName_9() { return &___productName_9; }
	inline void set_productName_9(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productName_9 = value;
		Il2CppCodeGenWriteBarrier((&___productName_9), value);
	}

	inline static int32_t get_offset_of_productGUID_10() { return static_cast<int32_t>(offsetof(MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020, ___productGUID_10)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productGUID_10() const { return ___productGUID_10; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productGUID_10() { return &___productGUID_10; }
	inline void set_productGUID_10(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productGUID_10 = value;
		Il2CppCodeGenWriteBarrier((&___productGUID_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_TC533CB63E9FD91010D2CB407E151B8FDEE098020_H
#ifndef ELEMENTCOUNT_TEF47EBACC44F3DF2ADF137E38A160B967ED0DBAC_H
#define ELEMENTCOUNT_TEF47EBACC44F3DF2ADF137E38A160B967ED0DBAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_MatchingCriteria_ElementCount
struct  ElementCount_tEF47EBACC44F3DF2ADF137E38A160B967ED0DBAC  : public ElementCount_Base_t73ADB57E490B282EC131BA5EAF3AFABDD618ECBC
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_MatchingCriteria_ElementCount::hatCount
	int32_t ___hatCount_2;

public:
	inline static int32_t get_offset_of_hatCount_2() { return static_cast<int32_t>(offsetof(ElementCount_tEF47EBACC44F3DF2ADF137E38A160B967ED0DBAC, ___hatCount_2)); }
	inline int32_t get_hatCount_2() const { return ___hatCount_2; }
	inline int32_t* get_address_of_hatCount_2() { return &___hatCount_2; }
	inline void set_hatCount_2(int32_t value)
	{
		___hatCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTCOUNT_TEF47EBACC44F3DF2ADF137E38A160B967ED0DBAC_H
#ifndef PLATFORM_XINPUT_BASE_TEEAE2121567E48A96BEAAE00B6C2F3822D42E374_H
#define PLATFORM_XINPUT_BASE_TEEAE2121567E48A96BEAAE00B6C2F3822D42E374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base
struct  Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374  : public Platform_t0E28F5AD2665BAE68E81D3FF477CA1E6E1574789
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base::matchingCriteria
	MatchingCriteria_t40557EF4BBBEA0BA5E20157EB81A34802805124A * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base::elements
	Elements_tC9F4725938858F8E86BF77F632A2F4B0B3B6B73F * ___elements_2;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374, ___matchingCriteria_1)); }
	inline MatchingCriteria_t40557EF4BBBEA0BA5E20157EB81A34802805124A * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_t40557EF4BBBEA0BA5E20157EB81A34802805124A ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_t40557EF4BBBEA0BA5E20157EB81A34802805124A * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374, ___elements_2)); }
	inline Elements_tC9F4725938858F8E86BF77F632A2F4B0B3B6B73F * get_elements_2() const { return ___elements_2; }
	inline Elements_tC9F4725938858F8E86BF77F632A2F4B0B3B6B73F ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_tC9F4725938858F8E86BF77F632A2F4B0B3B6B73F * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_XINPUT_BASE_TEEAE2121567E48A96BEAAE00B6C2F3822D42E374_H
#ifndef ELEMENTS_TC9F4725938858F8E86BF77F632A2F4B0B3B6B73F_H
#define ELEMENTS_TC9F4725938858F8E86BF77F632A2F4B0B3B6B73F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Elements
struct  Elements_tC9F4725938858F8E86BF77F632A2F4B0B3B6B73F  : public Elements_Base_t646A0ACACC7BEB9CE31C70CB74E59EF0312756F0
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Elements::axes
	AxisU5BU5D_tE2E6FD9F2F8617EC8BC700A2AFFDA15A3459B4F2* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Elements::buttons
	ButtonU5BU5D_t5A012C1E636329C7B26DEA0A59A715AB010FDABE* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_tC9F4725938858F8E86BF77F632A2F4B0B3B6B73F, ___axes_0)); }
	inline AxisU5BU5D_tE2E6FD9F2F8617EC8BC700A2AFFDA15A3459B4F2* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_tE2E6FD9F2F8617EC8BC700A2AFFDA15A3459B4F2** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_tE2E6FD9F2F8617EC8BC700A2AFFDA15A3459B4F2* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_tC9F4725938858F8E86BF77F632A2F4B0B3B6B73F, ___buttons_1)); }
	inline ButtonU5BU5D_t5A012C1E636329C7B26DEA0A59A715AB010FDABE* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_t5A012C1E636329C7B26DEA0A59A715AB010FDABE** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_t5A012C1E636329C7B26DEA0A59A715AB010FDABE* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_TC9F4725938858F8E86BF77F632A2F4B0B3B6B73F_H
#ifndef MATCHINGCRITERIA_T40557EF4BBBEA0BA5E20157EB81A34802805124A_H
#define MATCHINGCRITERIA_T40557EF4BBBEA0BA5E20157EB81A34802805124A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_MatchingCriteria
struct  MatchingCriteria_t40557EF4BBBEA0BA5E20157EB81A34802805124A  : public MatchingCriteria_Base_t9475B14364CDF4EB6C2585C9F29E9394954F3065
{
public:
	// Rewired.Platforms.XInputDeviceSubType[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_MatchingCriteria::subType
	XInputDeviceSubTypeU5BU5D_tF82422E233148C7071A9B7A36BB24F16E615E150* ___subType_4;

public:
	inline static int32_t get_offset_of_subType_4() { return static_cast<int32_t>(offsetof(MatchingCriteria_t40557EF4BBBEA0BA5E20157EB81A34802805124A, ___subType_4)); }
	inline XInputDeviceSubTypeU5BU5D_tF82422E233148C7071A9B7A36BB24F16E615E150* get_subType_4() const { return ___subType_4; }
	inline XInputDeviceSubTypeU5BU5D_tF82422E233148C7071A9B7A36BB24F16E615E150** get_address_of_subType_4() { return &___subType_4; }
	inline void set_subType_4(XInputDeviceSubTypeU5BU5D_tF82422E233148C7071A9B7A36BB24F16E615E150* value)
	{
		___subType_4 = value;
		Il2CppCodeGenWriteBarrier((&___subType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_T40557EF4BBBEA0BA5E20157EB81A34802805124A_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#define AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisRange
struct  AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652 
{
public:
	// System.Int32 Rewired.AxisRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifndef AXISCALIBRATIONTYPE_TE13AFD356AB96E532C90470EC4A227DEF86B598C_H
#define AXISCALIBRATIONTYPE_TE13AFD356AB96E532C90470EC4A227DEF86B598C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.AxisCalibrationType
struct  AxisCalibrationType_tE13AFD356AB96E532C90470EC4A227DEF86B598C 
{
public:
	// System.Int32 Rewired.Data.Mapping.AxisCalibrationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisCalibrationType_tE13AFD356AB96E532C90470EC4A227DEF86B598C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCALIBRATIONTYPE_TE13AFD356AB96E532C90470EC4A227DEF86B598C_H
#ifndef AXISDIRECTION_TE4953CDA51E7D3BD480078C8768EBCD213C2EF49_H
#define AXISDIRECTION_TE4953CDA51E7D3BD480078C8768EBCD213C2EF49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.AxisDirection
struct  AxisDirection_tE4953CDA51E7D3BD480078C8768EBCD213C2EF49 
{
public:
	// System.Int32 Rewired.Data.Mapping.AxisDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisDirection_tE4953CDA51E7D3BD480078C8768EBCD213C2EF49, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISDIRECTION_TE4953CDA51E7D3BD480078C8768EBCD213C2EF49_H
#ifndef HARDWAREELEMENTSOURCETYPE_T440266C7790C9BE17B04056BE8C554EE04C34675_H
#define HARDWAREELEMENTSOURCETYPE_T440266C7790C9BE17B04056BE8C554EE04C34675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareElementSourceType
struct  HardwareElementSourceType_t440266C7790C9BE17B04056BE8C554EE04C34675 
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareElementSourceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HardwareElementSourceType_t440266C7790C9BE17B04056BE8C554EE04C34675, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWAREELEMENTSOURCETYPE_T440266C7790C9BE17B04056BE8C554EE04C34675_H
#ifndef HARDWAREELEMENTSOURCETYPEWITHHAT_TDC84EB6512EDD5A1BB8A15FFF31201CC0E318520_H
#define HARDWAREELEMENTSOURCETYPEWITHHAT_TDC84EB6512EDD5A1BB8A15FFF31201CC0E318520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat
struct  HardwareElementSourceTypeWithHat_tDC84EB6512EDD5A1BB8A15FFF31201CC0E318520 
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareElementSourceTypeWithHat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HardwareElementSourceTypeWithHat_tDC84EB6512EDD5A1BB8A15FFF31201CC0E318520, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWAREELEMENTSOURCETYPEWITHHAT_TDC84EB6512EDD5A1BB8A15FFF31201CC0E318520_H
#ifndef PLATFORM_FALLBACK_TC5968A83CBB360E30B21890E8F02D4FA0E56B313_H
#define PLATFORM_FALLBACK_TC5968A83CBB360E30B21890E8F02D4FA0E56B313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback
struct  Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313  : public Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback::variants
	Platform_Fallback_BaseU5BU5D_t57FC29A1D770568738D7217ED1578A5FE963176F* ___variants_3;

public:
	inline static int32_t get_offset_of_variants_3() { return static_cast<int32_t>(offsetof(Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313, ___variants_3)); }
	inline Platform_Fallback_BaseU5BU5D_t57FC29A1D770568738D7217ED1578A5FE963176F* get_variants_3() const { return ___variants_3; }
	inline Platform_Fallback_BaseU5BU5D_t57FC29A1D770568738D7217ED1578A5FE963176F** get_address_of_variants_3() { return &___variants_3; }
	inline void set_variants_3(Platform_Fallback_BaseU5BU5D_t57FC29A1D770568738D7217ED1578A5FE963176F* value)
	{
		___variants_3 = value;
		Il2CppCodeGenWriteBarrier((&___variants_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_FALLBACK_TC5968A83CBB360E30B21890E8F02D4FA0E56B313_H
#ifndef PLATFORM_LINUX_T5C50A8A935B1AE1D30D6F7912DA10AEAFFD53EB5_H
#define PLATFORM_LINUX_T5C50A8A935B1AE1D30D6F7912DA10AEAFFD53EB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux
struct  Platform_Linux_t5C50A8A935B1AE1D30D6F7912DA10AEAFFD53EB5  : public Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux::variants
	Platform_Linux_BaseU5BU5D_t2E32C60C9AE700CFF2E4AB3FC8105FF63370C315* ___variants_3;

public:
	inline static int32_t get_offset_of_variants_3() { return static_cast<int32_t>(offsetof(Platform_Linux_t5C50A8A935B1AE1D30D6F7912DA10AEAFFD53EB5, ___variants_3)); }
	inline Platform_Linux_BaseU5BU5D_t2E32C60C9AE700CFF2E4AB3FC8105FF63370C315* get_variants_3() const { return ___variants_3; }
	inline Platform_Linux_BaseU5BU5D_t2E32C60C9AE700CFF2E4AB3FC8105FF63370C315** get_address_of_variants_3() { return &___variants_3; }
	inline void set_variants_3(Platform_Linux_BaseU5BU5D_t2E32C60C9AE700CFF2E4AB3FC8105FF63370C315* value)
	{
		___variants_3 = value;
		Il2CppCodeGenWriteBarrier((&___variants_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_LINUX_T5C50A8A935B1AE1D30D6F7912DA10AEAFFD53EB5_H
#ifndef PLATFORM_NINTENDOSWITCH_BASE_T762348F60D107794795951289C5E38A71195D6EF_H
#define PLATFORM_NINTENDOSWITCH_BASE_T762348F60D107794795951289C5E38A71195D6EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base
struct  Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF  : public Platform_Custom_t38C5DD986F65AF512626A73FA4EA96ECF76A0C77
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base::matchingCriteria
	MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base::elements
	Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D * ___elements_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base::_axesOrigGame
	AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* ____axesOrigGame_3;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base::_buttonsOrigGame
	ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* ____buttonsOrigGame_4;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF, ___matchingCriteria_1)); }
	inline MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF, ___elements_2)); }
	inline Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D * get_elements_2() const { return ___elements_2; }
	inline Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}

	inline static int32_t get_offset_of__axesOrigGame_3() { return static_cast<int32_t>(offsetof(Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF, ____axesOrigGame_3)); }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* get__axesOrigGame_3() const { return ____axesOrigGame_3; }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8** get_address_of__axesOrigGame_3() { return &____axesOrigGame_3; }
	inline void set__axesOrigGame_3(AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* value)
	{
		____axesOrigGame_3 = value;
		Il2CppCodeGenWriteBarrier((&____axesOrigGame_3), value);
	}

	inline static int32_t get_offset_of__buttonsOrigGame_4() { return static_cast<int32_t>(offsetof(Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF, ____buttonsOrigGame_4)); }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* get__buttonsOrigGame_4() const { return ____buttonsOrigGame_4; }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E** get_address_of__buttonsOrigGame_4() { return &____buttonsOrigGame_4; }
	inline void set__buttonsOrigGame_4(ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* value)
	{
		____buttonsOrigGame_4 = value;
		Il2CppCodeGenWriteBarrier((&____buttonsOrigGame_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_NINTENDOSWITCH_BASE_T762348F60D107794795951289C5E38A71195D6EF_H
#ifndef ELEMENTS_T589C5928C7E1F0D4523754CE46B158FBB57A741D_H
#define ELEMENTS_T589C5928C7E1F0D4523754CE46B158FBB57A741D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_Elements
struct  Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D  : public Elements_tC57D226564DCD7C2CFC9AFA7A212201FCC98129B
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_Elements::axes
	AxisU5BU5D_t72E4B431F2EB0274312CAD56DFF38BF25C2299DE* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_Elements::buttons
	ButtonU5BU5D_tE2C1D485FA293714108314F60C99A96EE913FF3F* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D, ___axes_0)); }
	inline AxisU5BU5D_t72E4B431F2EB0274312CAD56DFF38BF25C2299DE* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_t72E4B431F2EB0274312CAD56DFF38BF25C2299DE** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_t72E4B431F2EB0274312CAD56DFF38BF25C2299DE* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D, ___buttons_1)); }
	inline ButtonU5BU5D_tE2C1D485FA293714108314F60C99A96EE913FF3F* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_tE2C1D485FA293714108314F60C99A96EE913FF3F** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_tE2C1D485FA293714108314F60C99A96EE913FF3F* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_T589C5928C7E1F0D4523754CE46B158FBB57A741D_H
#ifndef MATCHINGCRITERIA_TA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC_H
#define MATCHINGCRITERIA_TA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_MatchingCriteria
struct  MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC  : public MatchingCriteria_t1A1DB371BD17248F7D375FC7FA3B8704BEB1C795
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_MatchingCriteria::productName_useRegex
	bool ___productName_useRegex_5;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_MatchingCriteria::productName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productName_6;

public:
	inline static int32_t get_offset_of_productName_useRegex_5() { return static_cast<int32_t>(offsetof(MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC, ___productName_useRegex_5)); }
	inline bool get_productName_useRegex_5() const { return ___productName_useRegex_5; }
	inline bool* get_address_of_productName_useRegex_5() { return &___productName_useRegex_5; }
	inline void set_productName_useRegex_5(bool value)
	{
		___productName_useRegex_5 = value;
	}

	inline static int32_t get_offset_of_productName_6() { return static_cast<int32_t>(offsetof(MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC, ___productName_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productName_6() const { return ___productName_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productName_6() { return &___productName_6; }
	inline void set_productName_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productName_6 = value;
		Il2CppCodeGenWriteBarrier((&___productName_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_TA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC_H
#ifndef PLATFORM_OSX_T03A46AF339888AC6E808D1D31A7467AD14FCA357_H
#define PLATFORM_OSX_T03A46AF339888AC6E808D1D31A7467AD14FCA357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX
struct  Platform_OSX_t03A46AF339888AC6E808D1D31A7467AD14FCA357  : public Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX::variants
	Platform_OSX_BaseU5BU5D_tD648889E359C28A8F1A5698CCBB7B5A4EEE56B04* ___variants_3;

public:
	inline static int32_t get_offset_of_variants_3() { return static_cast<int32_t>(offsetof(Platform_OSX_t03A46AF339888AC6E808D1D31A7467AD14FCA357, ___variants_3)); }
	inline Platform_OSX_BaseU5BU5D_tD648889E359C28A8F1A5698CCBB7B5A4EEE56B04* get_variants_3() const { return ___variants_3; }
	inline Platform_OSX_BaseU5BU5D_tD648889E359C28A8F1A5698CCBB7B5A4EEE56B04** get_address_of_variants_3() { return &___variants_3; }
	inline void set_variants_3(Platform_OSX_BaseU5BU5D_tD648889E359C28A8F1A5698CCBB7B5A4EEE56B04* value)
	{
		___variants_3 = value;
		Il2CppCodeGenWriteBarrier((&___variants_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_OSX_T03A46AF339888AC6E808D1D31A7467AD14FCA357_H
#ifndef PLATFORM_OUYA_BASE_T1D89BF95095C7D9283F8C46FD472837B91830E12_H
#define PLATFORM_OUYA_BASE_T1D89BF95095C7D9283F8C46FD472837B91830E12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base
struct  Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12  : public Platform_Custom_t38C5DD986F65AF512626A73FA4EA96ECF76A0C77
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base::matchingCriteria
	MatchingCriteria_t7E3617113D979BC9EBEACC19768821EA3A4A3ECE * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base::elements
	Elements_t337BF774C168903EA673A8A56D19C01534EA3196 * ___elements_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base::_axesOrigGame
	AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* ____axesOrigGame_3;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base::_buttonsOrigGame
	ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* ____buttonsOrigGame_4;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12, ___matchingCriteria_1)); }
	inline MatchingCriteria_t7E3617113D979BC9EBEACC19768821EA3A4A3ECE * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_t7E3617113D979BC9EBEACC19768821EA3A4A3ECE ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_t7E3617113D979BC9EBEACC19768821EA3A4A3ECE * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12, ___elements_2)); }
	inline Elements_t337BF774C168903EA673A8A56D19C01534EA3196 * get_elements_2() const { return ___elements_2; }
	inline Elements_t337BF774C168903EA673A8A56D19C01534EA3196 ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_t337BF774C168903EA673A8A56D19C01534EA3196 * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}

	inline static int32_t get_offset_of__axesOrigGame_3() { return static_cast<int32_t>(offsetof(Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12, ____axesOrigGame_3)); }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* get__axesOrigGame_3() const { return ____axesOrigGame_3; }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8** get_address_of__axesOrigGame_3() { return &____axesOrigGame_3; }
	inline void set__axesOrigGame_3(AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* value)
	{
		____axesOrigGame_3 = value;
		Il2CppCodeGenWriteBarrier((&____axesOrigGame_3), value);
	}

	inline static int32_t get_offset_of__buttonsOrigGame_4() { return static_cast<int32_t>(offsetof(Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12, ____buttonsOrigGame_4)); }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* get__buttonsOrigGame_4() const { return ____buttonsOrigGame_4; }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E** get_address_of__buttonsOrigGame_4() { return &____buttonsOrigGame_4; }
	inline void set__buttonsOrigGame_4(ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* value)
	{
		____buttonsOrigGame_4 = value;
		Il2CppCodeGenWriteBarrier((&____buttonsOrigGame_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_OUYA_BASE_T1D89BF95095C7D9283F8C46FD472837B91830E12_H
#ifndef ELEMENTS_T337BF774C168903EA673A8A56D19C01534EA3196_H
#define ELEMENTS_T337BF774C168903EA673A8A56D19C01534EA3196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_Elements
struct  Elements_t337BF774C168903EA673A8A56D19C01534EA3196  : public Elements_tC57D226564DCD7C2CFC9AFA7A212201FCC98129B
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_Elements::axes
	AxisU5BU5D_t0A247C1FB163CBE2493DA8D44B47F1A42234D0B0* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_Elements::buttons
	ButtonU5BU5D_t3D117F1380549EEBF8E6ED08B14152948A8F900D* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_t337BF774C168903EA673A8A56D19C01534EA3196, ___axes_0)); }
	inline AxisU5BU5D_t0A247C1FB163CBE2493DA8D44B47F1A42234D0B0* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_t0A247C1FB163CBE2493DA8D44B47F1A42234D0B0** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_t0A247C1FB163CBE2493DA8D44B47F1A42234D0B0* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_t337BF774C168903EA673A8A56D19C01534EA3196, ___buttons_1)); }
	inline ButtonU5BU5D_t3D117F1380549EEBF8E6ED08B14152948A8F900D* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_t3D117F1380549EEBF8E6ED08B14152948A8F900D** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_t3D117F1380549EEBF8E6ED08B14152948A8F900D* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_T337BF774C168903EA673A8A56D19C01534EA3196_H
#ifndef MATCHINGCRITERIA_T7E3617113D979BC9EBEACC19768821EA3A4A3ECE_H
#define MATCHINGCRITERIA_T7E3617113D979BC9EBEACC19768821EA3A4A3ECE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_MatchingCriteria
struct  MatchingCriteria_t7E3617113D979BC9EBEACC19768821EA3A4A3ECE  : public MatchingCriteria_t1A1DB371BD17248F7D375FC7FA3B8704BEB1C795
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_T7E3617113D979BC9EBEACC19768821EA3A4A3ECE_H
#ifndef PLATFORM_PS4_BASE_TC174021797D00A227DC7EDF0B0B57145EB7D57A7_H
#define PLATFORM_PS4_BASE_TC174021797D00A227DC7EDF0B0B57145EB7D57A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base
struct  Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7  : public Platform_Custom_t38C5DD986F65AF512626A73FA4EA96ECF76A0C77
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base::matchingCriteria
	MatchingCriteria_t728FB1BA60E0E29B55788562DE5AF371ACFC1A0C * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base::elements
	Elements_t1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02 * ___elements_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base::_axesOrigGame
	AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* ____axesOrigGame_3;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base::_buttonsOrigGame
	ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* ____buttonsOrigGame_4;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7, ___matchingCriteria_1)); }
	inline MatchingCriteria_t728FB1BA60E0E29B55788562DE5AF371ACFC1A0C * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_t728FB1BA60E0E29B55788562DE5AF371ACFC1A0C ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_t728FB1BA60E0E29B55788562DE5AF371ACFC1A0C * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7, ___elements_2)); }
	inline Elements_t1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02 * get_elements_2() const { return ___elements_2; }
	inline Elements_t1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02 ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_t1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02 * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}

	inline static int32_t get_offset_of__axesOrigGame_3() { return static_cast<int32_t>(offsetof(Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7, ____axesOrigGame_3)); }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* get__axesOrigGame_3() const { return ____axesOrigGame_3; }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8** get_address_of__axesOrigGame_3() { return &____axesOrigGame_3; }
	inline void set__axesOrigGame_3(AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* value)
	{
		____axesOrigGame_3 = value;
		Il2CppCodeGenWriteBarrier((&____axesOrigGame_3), value);
	}

	inline static int32_t get_offset_of__buttonsOrigGame_4() { return static_cast<int32_t>(offsetof(Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7, ____buttonsOrigGame_4)); }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* get__buttonsOrigGame_4() const { return ____buttonsOrigGame_4; }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E** get_address_of__buttonsOrigGame_4() { return &____buttonsOrigGame_4; }
	inline void set__buttonsOrigGame_4(ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* value)
	{
		____buttonsOrigGame_4 = value;
		Il2CppCodeGenWriteBarrier((&____buttonsOrigGame_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_PS4_BASE_TC174021797D00A227DC7EDF0B0B57145EB7D57A7_H
#ifndef ELEMENTS_T1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02_H
#define ELEMENTS_T1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_Elements
struct  Elements_t1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02  : public Elements_tC57D226564DCD7C2CFC9AFA7A212201FCC98129B
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_Elements::axes
	AxisU5BU5D_t6DCE7F5005DDD9EFD5D45FB825A1555C4666E744* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_Elements::buttons
	ButtonU5BU5D_tE984A31B7512246FB5B5F82116AC201DB1346B96* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_t1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02, ___axes_0)); }
	inline AxisU5BU5D_t6DCE7F5005DDD9EFD5D45FB825A1555C4666E744* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_t6DCE7F5005DDD9EFD5D45FB825A1555C4666E744** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_t6DCE7F5005DDD9EFD5D45FB825A1555C4666E744* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_t1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02, ___buttons_1)); }
	inline ButtonU5BU5D_tE984A31B7512246FB5B5F82116AC201DB1346B96* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_tE984A31B7512246FB5B5F82116AC201DB1346B96** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_tE984A31B7512246FB5B5F82116AC201DB1346B96* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_T1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02_H
#ifndef MATCHINGCRITERIA_T728FB1BA60E0E29B55788562DE5AF371ACFC1A0C_H
#define MATCHINGCRITERIA_T728FB1BA60E0E29B55788562DE5AF371ACFC1A0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_MatchingCriteria
struct  MatchingCriteria_t728FB1BA60E0E29B55788562DE5AF371ACFC1A0C  : public MatchingCriteria_t1A1DB371BD17248F7D375FC7FA3B8704BEB1C795
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_MatchingCriteria::productName_useRegex
	bool ___productName_useRegex_5;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_MatchingCriteria::productName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productName_6;

public:
	inline static int32_t get_offset_of_productName_useRegex_5() { return static_cast<int32_t>(offsetof(MatchingCriteria_t728FB1BA60E0E29B55788562DE5AF371ACFC1A0C, ___productName_useRegex_5)); }
	inline bool get_productName_useRegex_5() const { return ___productName_useRegex_5; }
	inline bool* get_address_of_productName_useRegex_5() { return &___productName_useRegex_5; }
	inline void set_productName_useRegex_5(bool value)
	{
		___productName_useRegex_5 = value;
	}

	inline static int32_t get_offset_of_productName_6() { return static_cast<int32_t>(offsetof(MatchingCriteria_t728FB1BA60E0E29B55788562DE5AF371ACFC1A0C, ___productName_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productName_6() const { return ___productName_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productName_6() { return &___productName_6; }
	inline void set_productName_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productName_6 = value;
		Il2CppCodeGenWriteBarrier((&___productName_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_T728FB1BA60E0E29B55788562DE5AF371ACFC1A0C_H
#ifndef PLATFORM_RAWINPUT_BASE_TA0DFFECAF98212B21416DE596B7011684B6F5ADB_H
#define PLATFORM_RAWINPUT_BASE_TA0DFFECAF98212B21416DE596B7011684B6F5ADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base
struct  Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB  : public Platform_RawOrDirectInput_t66EE4F5A1C9F3A8F7F82E485316A53D1157DAFCC
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base::elements
	Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 * ___elements_2;

public:
	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB, ___elements_2)); }
	inline Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 * get_elements_2() const { return ___elements_2; }
	inline Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248 * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_RAWINPUT_BASE_TA0DFFECAF98212B21416DE596B7011684B6F5ADB_H
#ifndef ELEMENTS_TCF50568BAB138B70AC7D318DB26F28D8AD8C2248_H
#define ELEMENTS_TCF50568BAB138B70AC7D318DB26F28D8AD8C2248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements
struct  Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248  : public Elements_Platform_Base_tAA1373033B7FB6EA7E7C0298F2380C7D3EE67CE5
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements::axes
	AxisU5BU5D_t0DA25F52128CD0DAAF70937E550CD33679168D66* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Elements::buttons
	ButtonU5BU5D_t7DCB05F6CC92B7FA42DF10FC6734BAC2925F1DA1* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248, ___axes_0)); }
	inline AxisU5BU5D_t0DA25F52128CD0DAAF70937E550CD33679168D66* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_t0DA25F52128CD0DAAF70937E550CD33679168D66** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_t0DA25F52128CD0DAAF70937E550CD33679168D66* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248, ___buttons_1)); }
	inline ButtonU5BU5D_t7DCB05F6CC92B7FA42DF10FC6734BAC2925F1DA1* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_t7DCB05F6CC92B7FA42DF10FC6734BAC2925F1DA1** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_t7DCB05F6CC92B7FA42DF10FC6734BAC2925F1DA1* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_TCF50568BAB138B70AC7D318DB26F28D8AD8C2248_H
#ifndef PLATFORM_WINDOWSUWP_T79A50D7432962C9DD8B8CDFFAFC55B27BB32C06A_H
#define PLATFORM_WINDOWSUWP_T79A50D7432962C9DD8B8CDFFAFC55B27BB32C06A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP
struct  Platform_WindowsUWP_t79A50D7432962C9DD8B8CDFFAFC55B27BB32C06A  : public Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP::variants
	Platform_WindowsUWP_BaseU5BU5D_t3989D551C12994CFEDAF7AA6E39F7798853C4AB9* ___variants_3;

public:
	inline static int32_t get_offset_of_variants_3() { return static_cast<int32_t>(offsetof(Platform_WindowsUWP_t79A50D7432962C9DD8B8CDFFAFC55B27BB32C06A, ___variants_3)); }
	inline Platform_WindowsUWP_BaseU5BU5D_t3989D551C12994CFEDAF7AA6E39F7798853C4AB9* get_variants_3() const { return ___variants_3; }
	inline Platform_WindowsUWP_BaseU5BU5D_t3989D551C12994CFEDAF7AA6E39F7798853C4AB9** get_address_of_variants_3() { return &___variants_3; }
	inline void set_variants_3(Platform_WindowsUWP_BaseU5BU5D_t3989D551C12994CFEDAF7AA6E39F7798853C4AB9* value)
	{
		___variants_3 = value;
		Il2CppCodeGenWriteBarrier((&___variants_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_WINDOWSUWP_T79A50D7432962C9DD8B8CDFFAFC55B27BB32C06A_H
#ifndef PLATFORM_XINPUT_T44E2E6F0FD4F4A22A7FB6C031984FFC5F82BDC16_H
#define PLATFORM_XINPUT_T44E2E6F0FD4F4A22A7FB6C031984FFC5F82BDC16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput
struct  Platform_XInput_t44E2E6F0FD4F4A22A7FB6C031984FFC5F82BDC16  : public Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput::variants
	Platform_XInput_BaseU5BU5D_tFBF2B1FCF43579AE240C52DF195C2B9C129B2712* ___variants_3;

public:
	inline static int32_t get_offset_of_variants_3() { return static_cast<int32_t>(offsetof(Platform_XInput_t44E2E6F0FD4F4A22A7FB6C031984FFC5F82BDC16, ___variants_3)); }
	inline Platform_XInput_BaseU5BU5D_tFBF2B1FCF43579AE240C52DF195C2B9C129B2712* get_variants_3() const { return ___variants_3; }
	inline Platform_XInput_BaseU5BU5D_tFBF2B1FCF43579AE240C52DF195C2B9C129B2712** get_address_of_variants_3() { return &___variants_3; }
	inline void set_variants_3(Platform_XInput_BaseU5BU5D_tFBF2B1FCF43579AE240C52DF195C2B9C129B2712* value)
	{
		___variants_3 = value;
		Il2CppCodeGenWriteBarrier((&___variants_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_XINPUT_T44E2E6F0FD4F4A22A7FB6C031984FFC5F82BDC16_H
#ifndef PLATFORM_XBOXONE_BASE_TF2A2C472E98A1A55F98D8522E08CB62C089AC479_H
#define PLATFORM_XBOXONE_BASE_TF2A2C472E98A1A55F98D8522E08CB62C089AC479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base
struct  Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479  : public Platform_Custom_t38C5DD986F65AF512626A73FA4EA96ECF76A0C77
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_MatchingCriteria Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base::matchingCriteria
	MatchingCriteria_t8DBC25330E8572B4C927356459E8192B136CFBAD * ___matchingCriteria_1;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_Elements Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base::elements
	Elements_tF1026C7DB7135259ABB2A130FE4C24B0C0260DF2 * ___elements_2;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base::_axesOrigGame
	AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* ____axesOrigGame_3;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base::_buttonsOrigGame
	ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* ____buttonsOrigGame_4;

public:
	inline static int32_t get_offset_of_matchingCriteria_1() { return static_cast<int32_t>(offsetof(Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479, ___matchingCriteria_1)); }
	inline MatchingCriteria_t8DBC25330E8572B4C927356459E8192B136CFBAD * get_matchingCriteria_1() const { return ___matchingCriteria_1; }
	inline MatchingCriteria_t8DBC25330E8572B4C927356459E8192B136CFBAD ** get_address_of_matchingCriteria_1() { return &___matchingCriteria_1; }
	inline void set_matchingCriteria_1(MatchingCriteria_t8DBC25330E8572B4C927356459E8192B136CFBAD * value)
	{
		___matchingCriteria_1 = value;
		Il2CppCodeGenWriteBarrier((&___matchingCriteria_1), value);
	}

	inline static int32_t get_offset_of_elements_2() { return static_cast<int32_t>(offsetof(Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479, ___elements_2)); }
	inline Elements_tF1026C7DB7135259ABB2A130FE4C24B0C0260DF2 * get_elements_2() const { return ___elements_2; }
	inline Elements_tF1026C7DB7135259ABB2A130FE4C24B0C0260DF2 ** get_address_of_elements_2() { return &___elements_2; }
	inline void set_elements_2(Elements_tF1026C7DB7135259ABB2A130FE4C24B0C0260DF2 * value)
	{
		___elements_2 = value;
		Il2CppCodeGenWriteBarrier((&___elements_2), value);
	}

	inline static int32_t get_offset_of__axesOrigGame_3() { return static_cast<int32_t>(offsetof(Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479, ____axesOrigGame_3)); }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* get__axesOrigGame_3() const { return ____axesOrigGame_3; }
	inline AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8** get_address_of__axesOrigGame_3() { return &____axesOrigGame_3; }
	inline void set__axesOrigGame_3(AxisU5BU5D_t537AD634F7FA48AB4B73D573DE05C8DC4127F8F8* value)
	{
		____axesOrigGame_3 = value;
		Il2CppCodeGenWriteBarrier((&____axesOrigGame_3), value);
	}

	inline static int32_t get_offset_of__buttonsOrigGame_4() { return static_cast<int32_t>(offsetof(Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479, ____buttonsOrigGame_4)); }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* get__buttonsOrigGame_4() const { return ____buttonsOrigGame_4; }
	inline ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E** get_address_of__buttonsOrigGame_4() { return &____buttonsOrigGame_4; }
	inline void set__buttonsOrigGame_4(ButtonU5BU5D_t2F4FBA84B38C78F1E7A351A1A597FED3FC8AC49E* value)
	{
		____buttonsOrigGame_4 = value;
		Il2CppCodeGenWriteBarrier((&____buttonsOrigGame_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_XBOXONE_BASE_TF2A2C472E98A1A55F98D8522E08CB62C089AC479_H
#ifndef ELEMENTS_TF1026C7DB7135259ABB2A130FE4C24B0C0260DF2_H
#define ELEMENTS_TF1026C7DB7135259ABB2A130FE4C24B0C0260DF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_Elements
struct  Elements_tF1026C7DB7135259ABB2A130FE4C24B0C0260DF2  : public Elements_tC57D226564DCD7C2CFC9AFA7A212201FCC98129B
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_Axis[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_Elements::axes
	AxisU5BU5D_t1D5524808F860ADD8316300BD40968DECEFC10C9* ___axes_0;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_Button[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_Elements::buttons
	ButtonU5BU5D_tAAAAE4A765344C4A3D9A74AAD45536715942DE9E* ___buttons_1;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(Elements_tF1026C7DB7135259ABB2A130FE4C24B0C0260DF2, ___axes_0)); }
	inline AxisU5BU5D_t1D5524808F860ADD8316300BD40968DECEFC10C9* get_axes_0() const { return ___axes_0; }
	inline AxisU5BU5D_t1D5524808F860ADD8316300BD40968DECEFC10C9** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(AxisU5BU5D_t1D5524808F860ADD8316300BD40968DECEFC10C9* value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier((&___axes_0), value);
	}

	inline static int32_t get_offset_of_buttons_1() { return static_cast<int32_t>(offsetof(Elements_tF1026C7DB7135259ABB2A130FE4C24B0C0260DF2, ___buttons_1)); }
	inline ButtonU5BU5D_tAAAAE4A765344C4A3D9A74AAD45536715942DE9E* get_buttons_1() const { return ___buttons_1; }
	inline ButtonU5BU5D_tAAAAE4A765344C4A3D9A74AAD45536715942DE9E** get_address_of_buttons_1() { return &___buttons_1; }
	inline void set_buttons_1(ButtonU5BU5D_tAAAAE4A765344C4A3D9A74AAD45536715942DE9E* value)
	{
		___buttons_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTS_TF1026C7DB7135259ABB2A130FE4C24B0C0260DF2_H
#ifndef MATCHINGCRITERIA_T8DBC25330E8572B4C927356459E8192B136CFBAD_H
#define MATCHINGCRITERIA_T8DBC25330E8572B4C927356459E8192B136CFBAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_MatchingCriteria
struct  MatchingCriteria_t8DBC25330E8572B4C927356459E8192B136CFBAD  : public MatchingCriteria_t1A1DB371BD17248F7D375FC7FA3B8704BEB1C795
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_MatchingCriteria::productName_useRegex
	bool ___productName_useRegex_5;
	// System.String[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_MatchingCriteria::productName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___productName_6;

public:
	inline static int32_t get_offset_of_productName_useRegex_5() { return static_cast<int32_t>(offsetof(MatchingCriteria_t8DBC25330E8572B4C927356459E8192B136CFBAD, ___productName_useRegex_5)); }
	inline bool get_productName_useRegex_5() const { return ___productName_useRegex_5; }
	inline bool* get_address_of_productName_useRegex_5() { return &___productName_useRegex_5; }
	inline void set_productName_useRegex_5(bool value)
	{
		___productName_useRegex_5 = value;
	}

	inline static int32_t get_offset_of_productName_6() { return static_cast<int32_t>(offsetof(MatchingCriteria_t8DBC25330E8572B4C927356459E8192B136CFBAD, ___productName_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_productName_6() const { return ___productName_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_productName_6() { return &___productName_6; }
	inline void set_productName_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___productName_6 = value;
		Il2CppCodeGenWriteBarrier((&___productName_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINGCRITERIA_T8DBC25330E8572B4C927356459E8192B136CFBAD_H
#ifndef HATDIRECTION_T8B3787E56706986A258EEBCF16F989D2075785BA_H
#define HATDIRECTION_T8B3787E56706986A258EEBCF16F989D2075785BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HatDirection
struct  HatDirection_t8B3787E56706986A258EEBCF16F989D2075785BA 
{
public:
	// System.Int32 Rewired.Data.Mapping.HatDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HatDirection_t8B3787E56706986A258EEBCF16F989D2075785BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HATDIRECTION_T8B3787E56706986A258EEBCF16F989D2075785BA_H
#ifndef HATTYPE_T83B584B8E763A7B6924539BA31ECF4BD99E395F3_H
#define HATTYPE_T83B584B8E763A7B6924539BA31ECF4BD99E395F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HatType
struct  HatType_t83B584B8E763A7B6924539BA31ECF4BD99E395F3 
{
public:
	// System.Int32 Rewired.Data.Mapping.HatType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HatType_t83B584B8E763A7B6924539BA31ECF4BD99E395F3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HATTYPE_T83B584B8E763A7B6924539BA31ECF4BD99E395F3_H
#ifndef OSXAXIS_T6819D8CDEAB041DAE1EC0ECDB1B7E783303C4390_H
#define OSXAXIS_T6819D8CDEAB041DAE1EC0ECDB1B7E783303C4390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.OSXAxis
struct  OSXAxis_t6819D8CDEAB041DAE1EC0ECDB1B7E783303C4390 
{
public:
	// System.Int32 Rewired.Platforms.OSXAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OSXAxis_t6819D8CDEAB041DAE1EC0ECDB1B7E783303C4390, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSXAXIS_T6819D8CDEAB041DAE1EC0ECDB1B7E783303C4390_H
#ifndef UNITYAXIS_TC4D59F951ADCA039ADC817B7F75485D4916889A4_H
#define UNITYAXIS_TC4D59F951ADCA039ADC817B7F75485D4916889A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.UnityAxis
struct  UnityAxis_tC4D59F951ADCA039ADC817B7F75485D4916889A4 
{
public:
	// System.Int32 Rewired.Platforms.UnityAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityAxis_tC4D59F951ADCA039ADC817B7F75485D4916889A4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYAXIS_TC4D59F951ADCA039ADC817B7F75485D4916889A4_H
#ifndef UNITYBUTTON_TF9904F15BAA1343C62B5456C023515B928D77C9E_H
#define UNITYBUTTON_TF9904F15BAA1343C62B5456C023515B928D77C9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.UnityButton
struct  UnityButton_tF9904F15BAA1343C62B5456C023515B928D77C9E 
{
public:
	// System.Int32 Rewired.Platforms.UnityButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityButton_tF9904F15BAA1343C62B5456C023515B928D77C9E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYBUTTON_TF9904F15BAA1343C62B5456C023515B928D77C9E_H
#ifndef XINPUTAXIS_TCD509BEC708C15B9B97EAF69F723A854054B104F_H
#define XINPUTAXIS_TCD509BEC708C15B9B97EAF69F723A854054B104F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.XInputAxis
struct  XInputAxis_tCD509BEC708C15B9B97EAF69F723A854054B104F 
{
public:
	// System.Int32 Rewired.Platforms.XInputAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XInputAxis_tCD509BEC708C15B9B97EAF69F723A854054B104F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XINPUTAXIS_TCD509BEC708C15B9B97EAF69F723A854054B104F_H
#ifndef XINPUTBUTTON_T2C5775984CA580BFEDFFD7AA48C465A9B9F335F6_H
#define XINPUTBUTTON_T2C5775984CA580BFEDFFD7AA48C465A9B9F335F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.XInputButton
struct  XInputButton_t2C5775984CA580BFEDFFD7AA48C465A9B9F335F6 
{
public:
	// System.Int32 Rewired.Platforms.XInputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XInputButton_t2C5775984CA580BFEDFFD7AA48C465A9B9F335F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XINPUTBUTTON_T2C5775984CA580BFEDFFD7AA48C465A9B9F335F6_H
#ifndef POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#define POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Pole
struct  Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C 
{
public:
	// System.Int32 Rewired.Pole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef AXIS_T9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E_H
#define AXIS_T9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis
struct  Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E  : public Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::invert
	bool ___invert_7;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::sourceAxisRange
	int32_t ___sourceAxisRange_8;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::buttonAxisContribution
	int32_t ___buttonAxisContribution_9;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::calibrateAxis
	bool ___calibrateAxis_10;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::axisZero
	float ___axisZero_11;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::axisMin
	float ___axisMin_12;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::axisMax
	float ___axisMax_13;
	// Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::alternateCalibrations
	AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* ___alternateCalibrations_14;
	// Rewired.Data.Mapping.HardwareAxisInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Axis::axisInfo
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * ___axisInfo_15;

public:
	inline static int32_t get_offset_of_invert_7() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___invert_7)); }
	inline bool get_invert_7() const { return ___invert_7; }
	inline bool* get_address_of_invert_7() { return &___invert_7; }
	inline void set_invert_7(bool value)
	{
		___invert_7 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_8() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___sourceAxisRange_8)); }
	inline int32_t get_sourceAxisRange_8() const { return ___sourceAxisRange_8; }
	inline int32_t* get_address_of_sourceAxisRange_8() { return &___sourceAxisRange_8; }
	inline void set_sourceAxisRange_8(int32_t value)
	{
		___sourceAxisRange_8 = value;
	}

	inline static int32_t get_offset_of_buttonAxisContribution_9() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___buttonAxisContribution_9)); }
	inline int32_t get_buttonAxisContribution_9() const { return ___buttonAxisContribution_9; }
	inline int32_t* get_address_of_buttonAxisContribution_9() { return &___buttonAxisContribution_9; }
	inline void set_buttonAxisContribution_9(int32_t value)
	{
		___buttonAxisContribution_9 = value;
	}

	inline static int32_t get_offset_of_calibrateAxis_10() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___calibrateAxis_10)); }
	inline bool get_calibrateAxis_10() const { return ___calibrateAxis_10; }
	inline bool* get_address_of_calibrateAxis_10() { return &___calibrateAxis_10; }
	inline void set_calibrateAxis_10(bool value)
	{
		___calibrateAxis_10 = value;
	}

	inline static int32_t get_offset_of_axisZero_11() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___axisZero_11)); }
	inline float get_axisZero_11() const { return ___axisZero_11; }
	inline float* get_address_of_axisZero_11() { return &___axisZero_11; }
	inline void set_axisZero_11(float value)
	{
		___axisZero_11 = value;
	}

	inline static int32_t get_offset_of_axisMin_12() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___axisMin_12)); }
	inline float get_axisMin_12() const { return ___axisMin_12; }
	inline float* get_address_of_axisMin_12() { return &___axisMin_12; }
	inline void set_axisMin_12(float value)
	{
		___axisMin_12 = value;
	}

	inline static int32_t get_offset_of_axisMax_13() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___axisMax_13)); }
	inline float get_axisMax_13() const { return ___axisMax_13; }
	inline float* get_address_of_axisMax_13() { return &___axisMax_13; }
	inline void set_axisMax_13(float value)
	{
		___axisMax_13 = value;
	}

	inline static int32_t get_offset_of_alternateCalibrations_14() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___alternateCalibrations_14)); }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* get_alternateCalibrations_14() const { return ___alternateCalibrations_14; }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7** get_address_of_alternateCalibrations_14() { return &___alternateCalibrations_14; }
	inline void set_alternateCalibrations_14(AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* value)
	{
		___alternateCalibrations_14 = value;
		Il2CppCodeGenWriteBarrier((&___alternateCalibrations_14), value);
	}

	inline static int32_t get_offset_of_axisInfo_15() { return static_cast<int32_t>(offsetof(Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E, ___axisInfo_15)); }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * get_axisInfo_15() const { return ___axisInfo_15; }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 ** get_address_of_axisInfo_15() { return &___axisInfo_15; }
	inline void set_axisInfo_15(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * value)
	{
		___axisInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___axisInfo_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E_H
#ifndef BUTTON_TBB6149DD7A45B873206F15797DC2B002019BF09C_H
#define BUTTON_TBB6149DD7A45B873206F15797DC2B002019BF09C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button
struct  Button_tBB6149DD7A45B873206F15797DC2B002019BF09C  : public Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF
{
public:
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button::sourceAxisPole
	int32_t ___sourceAxisPole_7;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button::requireMultipleButtons
	bool ___requireMultipleButtons_8;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button::requiredButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___requiredButtons_9;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button::ignoreIfButtonsActive
	bool ___ignoreIfButtonsActive_10;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button::ignoreIfButtonsActiveButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ignoreIfButtonsActiveButtons_11;
	// Rewired.Data.Mapping.HardwareButtonInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_Button::buttonInfo
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * ___buttonInfo_12;

public:
	inline static int32_t get_offset_of_sourceAxisPole_7() { return static_cast<int32_t>(offsetof(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C, ___sourceAxisPole_7)); }
	inline int32_t get_sourceAxisPole_7() const { return ___sourceAxisPole_7; }
	inline int32_t* get_address_of_sourceAxisPole_7() { return &___sourceAxisPole_7; }
	inline void set_sourceAxisPole_7(int32_t value)
	{
		___sourceAxisPole_7 = value;
	}

	inline static int32_t get_offset_of_requireMultipleButtons_8() { return static_cast<int32_t>(offsetof(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C, ___requireMultipleButtons_8)); }
	inline bool get_requireMultipleButtons_8() const { return ___requireMultipleButtons_8; }
	inline bool* get_address_of_requireMultipleButtons_8() { return &___requireMultipleButtons_8; }
	inline void set_requireMultipleButtons_8(bool value)
	{
		___requireMultipleButtons_8 = value;
	}

	inline static int32_t get_offset_of_requiredButtons_9() { return static_cast<int32_t>(offsetof(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C, ___requiredButtons_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_requiredButtons_9() const { return ___requiredButtons_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_requiredButtons_9() { return &___requiredButtons_9; }
	inline void set_requiredButtons_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___requiredButtons_9 = value;
		Il2CppCodeGenWriteBarrier((&___requiredButtons_9), value);
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActive_10() { return static_cast<int32_t>(offsetof(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C, ___ignoreIfButtonsActive_10)); }
	inline bool get_ignoreIfButtonsActive_10() const { return ___ignoreIfButtonsActive_10; }
	inline bool* get_address_of_ignoreIfButtonsActive_10() { return &___ignoreIfButtonsActive_10; }
	inline void set_ignoreIfButtonsActive_10(bool value)
	{
		___ignoreIfButtonsActive_10 = value;
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActiveButtons_11() { return static_cast<int32_t>(offsetof(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C, ___ignoreIfButtonsActiveButtons_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ignoreIfButtonsActiveButtons_11() const { return ___ignoreIfButtonsActiveButtons_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ignoreIfButtonsActiveButtons_11() { return &___ignoreIfButtonsActiveButtons_11; }
	inline void set_ignoreIfButtonsActiveButtons_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ignoreIfButtonsActiveButtons_11 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreIfButtonsActiveButtons_11), value);
	}

	inline static int32_t get_offset_of_buttonInfo_12() { return static_cast<int32_t>(offsetof(Button_tBB6149DD7A45B873206F15797DC2B002019BF09C, ___buttonInfo_12)); }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * get_buttonInfo_12() const { return ___buttonInfo_12; }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 ** get_address_of_buttonInfo_12() { return &___buttonInfo_12; }
	inline void set_buttonInfo_12(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * value)
	{
		___buttonInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___buttonInfo_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_TBB6149DD7A45B873206F15797DC2B002019BF09C_H
#ifndef CUSTOMCALCULATIONSOURCEDATA_T7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19_H
#define CUSTOMCALCULATIONSOURCEDATA_T7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData
struct  CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData::sourceType
	int32_t ___sourceType_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData::sourceAxis
	int32_t ___sourceAxis_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData::sourceButton
	int32_t ___sourceButton_2;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData::sourceOtherAxis
	int32_t ___sourceOtherAxis_3;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData::sourceAxisRange
	int32_t ___sourceAxisRange_4;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData::axisDeadZone
	float ___axisDeadZone_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData::invert
	bool ___invert_6;
	// Rewired.Data.Mapping.AxisCalibrationType Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData::axisCalibrationType
	int32_t ___axisCalibrationType_7;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData::axisZero
	float ___axisZero_8;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData::axisMin
	float ___axisMin_9;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Custom_CustomCalculationSourceData::axisMax
	float ___axisMax_10;

public:
	inline static int32_t get_offset_of_sourceType_0() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19, ___sourceType_0)); }
	inline int32_t get_sourceType_0() const { return ___sourceType_0; }
	inline int32_t* get_address_of_sourceType_0() { return &___sourceType_0; }
	inline void set_sourceType_0(int32_t value)
	{
		___sourceType_0 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_1() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19, ___sourceAxis_1)); }
	inline int32_t get_sourceAxis_1() const { return ___sourceAxis_1; }
	inline int32_t* get_address_of_sourceAxis_1() { return &___sourceAxis_1; }
	inline void set_sourceAxis_1(int32_t value)
	{
		___sourceAxis_1 = value;
	}

	inline static int32_t get_offset_of_sourceButton_2() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19, ___sourceButton_2)); }
	inline int32_t get_sourceButton_2() const { return ___sourceButton_2; }
	inline int32_t* get_address_of_sourceButton_2() { return &___sourceButton_2; }
	inline void set_sourceButton_2(int32_t value)
	{
		___sourceButton_2 = value;
	}

	inline static int32_t get_offset_of_sourceOtherAxis_3() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19, ___sourceOtherAxis_3)); }
	inline int32_t get_sourceOtherAxis_3() const { return ___sourceOtherAxis_3; }
	inline int32_t* get_address_of_sourceOtherAxis_3() { return &___sourceOtherAxis_3; }
	inline void set_sourceOtherAxis_3(int32_t value)
	{
		___sourceOtherAxis_3 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_4() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19, ___sourceAxisRange_4)); }
	inline int32_t get_sourceAxisRange_4() const { return ___sourceAxisRange_4; }
	inline int32_t* get_address_of_sourceAxisRange_4() { return &___sourceAxisRange_4; }
	inline void set_sourceAxisRange_4(int32_t value)
	{
		___sourceAxisRange_4 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_5() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19, ___axisDeadZone_5)); }
	inline float get_axisDeadZone_5() const { return ___axisDeadZone_5; }
	inline float* get_address_of_axisDeadZone_5() { return &___axisDeadZone_5; }
	inline void set_axisDeadZone_5(float value)
	{
		___axisDeadZone_5 = value;
	}

	inline static int32_t get_offset_of_invert_6() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19, ___invert_6)); }
	inline bool get_invert_6() const { return ___invert_6; }
	inline bool* get_address_of_invert_6() { return &___invert_6; }
	inline void set_invert_6(bool value)
	{
		___invert_6 = value;
	}

	inline static int32_t get_offset_of_axisCalibrationType_7() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19, ___axisCalibrationType_7)); }
	inline int32_t get_axisCalibrationType_7() const { return ___axisCalibrationType_7; }
	inline int32_t* get_address_of_axisCalibrationType_7() { return &___axisCalibrationType_7; }
	inline void set_axisCalibrationType_7(int32_t value)
	{
		___axisCalibrationType_7 = value;
	}

	inline static int32_t get_offset_of_axisZero_8() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19, ___axisZero_8)); }
	inline float get_axisZero_8() const { return ___axisZero_8; }
	inline float* get_address_of_axisZero_8() { return &___axisZero_8; }
	inline void set_axisZero_8(float value)
	{
		___axisZero_8 = value;
	}

	inline static int32_t get_offset_of_axisMin_9() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19, ___axisMin_9)); }
	inline float get_axisMin_9() const { return ___axisMin_9; }
	inline float* get_address_of_axisMin_9() { return &___axisMin_9; }
	inline void set_axisMin_9(float value)
	{
		___axisMin_9 = value;
	}

	inline static int32_t get_offset_of_axisMax_10() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19, ___axisMax_10)); }
	inline float get_axisMax_10() const { return ___axisMax_10; }
	inline float* get_address_of_axisMax_10() { return &___axisMax_10; }
	inline void set_axisMax_10(float value)
	{
		___axisMax_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCALCULATIONSOURCEDATA_T7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19_H
#ifndef CUSTOMCALCULATIONSOURCEDATA_T947413414549A85A7CD81E30468F96A51CECF9B5_H
#define CUSTOMCALCULATIONSOURCEDATA_T947413414549A85A7CD81E30468F96A51CECF9B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_CustomCalculationSourceData
struct  CustomCalculationSourceData_t947413414549A85A7CD81E30468F96A51CECF9B5  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_CustomCalculationSourceData::sourceType
	int32_t ___sourceType_0;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_CustomCalculationSourceData::sourceElement
	int32_t ___sourceElement_1;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_CustomCalculationSourceData::sourceAxisRange
	int32_t ___sourceAxisRange_2;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_CustomCalculationSourceData::deadzone
	float ___deadzone_3;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_CustomCalculationSourceData::invert
	bool ___invert_4;

public:
	inline static int32_t get_offset_of_sourceType_0() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t947413414549A85A7CD81E30468F96A51CECF9B5, ___sourceType_0)); }
	inline int32_t get_sourceType_0() const { return ___sourceType_0; }
	inline int32_t* get_address_of_sourceType_0() { return &___sourceType_0; }
	inline void set_sourceType_0(int32_t value)
	{
		___sourceType_0 = value;
	}

	inline static int32_t get_offset_of_sourceElement_1() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t947413414549A85A7CD81E30468F96A51CECF9B5, ___sourceElement_1)); }
	inline int32_t get_sourceElement_1() const { return ___sourceElement_1; }
	inline int32_t* get_address_of_sourceElement_1() { return &___sourceElement_1; }
	inline void set_sourceElement_1(int32_t value)
	{
		___sourceElement_1 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_2() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t947413414549A85A7CD81E30468F96A51CECF9B5, ___sourceAxisRange_2)); }
	inline int32_t get_sourceAxisRange_2() const { return ___sourceAxisRange_2; }
	inline int32_t* get_address_of_sourceAxisRange_2() { return &___sourceAxisRange_2; }
	inline void set_sourceAxisRange_2(int32_t value)
	{
		___sourceAxisRange_2 = value;
	}

	inline static int32_t get_offset_of_deadzone_3() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t947413414549A85A7CD81E30468F96A51CECF9B5, ___deadzone_3)); }
	inline float get_deadzone_3() const { return ___deadzone_3; }
	inline float* get_address_of_deadzone_3() { return &___deadzone_3; }
	inline void set_deadzone_3(float value)
	{
		___deadzone_3 = value;
	}

	inline static int32_t get_offset_of_invert_4() { return static_cast<int32_t>(offsetof(CustomCalculationSourceData_t947413414549A85A7CD81E30468F96A51CECF9B5, ___invert_4)); }
	inline bool get_invert_4() const { return ___invert_4; }
	inline bool* get_address_of_invert_4() { return &___invert_4; }
	inline void set_invert_4(bool value)
	{
		___invert_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCALCULATIONSOURCEDATA_T947413414549A85A7CD81E30468F96A51CECF9B5_H
#ifndef ELEMENT_TB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E_H
#define ELEMENT_TB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Element
struct  Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Element::elementIdentifier
	int32_t ___elementIdentifier_0;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Element::sourceType
	int32_t ___sourceType_1;
	// Rewired.Platforms.UnityAxis Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Element::sourceAxis
	int32_t ___sourceAxis_2;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Element::axisDeadZone
	float ___axisDeadZone_3;
	// Rewired.Platforms.UnityButton Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Element::sourceButton
	int32_t ___sourceButton_4;
	// UnityEngine.KeyCode Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Element::sourceKeyCode
	int32_t ___sourceKeyCode_5;
	// Rewired.Data.Mapping.CustomCalculation Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Element::customCalculation
	CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * ___customCalculation_6;
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_CustomCalculationSourceData[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Element::customCalculationSourceData
	CustomCalculationSourceDataU5BU5D_t3E25F9818C6BA4273D61F758395B67649F70CC38* ___customCalculationSourceData_7;

public:
	inline static int32_t get_offset_of_elementIdentifier_0() { return static_cast<int32_t>(offsetof(Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E, ___elementIdentifier_0)); }
	inline int32_t get_elementIdentifier_0() const { return ___elementIdentifier_0; }
	inline int32_t* get_address_of_elementIdentifier_0() { return &___elementIdentifier_0; }
	inline void set_elementIdentifier_0(int32_t value)
	{
		___elementIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_sourceType_1() { return static_cast<int32_t>(offsetof(Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E, ___sourceType_1)); }
	inline int32_t get_sourceType_1() const { return ___sourceType_1; }
	inline int32_t* get_address_of_sourceType_1() { return &___sourceType_1; }
	inline void set_sourceType_1(int32_t value)
	{
		___sourceType_1 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_2() { return static_cast<int32_t>(offsetof(Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E, ___sourceAxis_2)); }
	inline int32_t get_sourceAxis_2() const { return ___sourceAxis_2; }
	inline int32_t* get_address_of_sourceAxis_2() { return &___sourceAxis_2; }
	inline void set_sourceAxis_2(int32_t value)
	{
		___sourceAxis_2 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_3() { return static_cast<int32_t>(offsetof(Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E, ___axisDeadZone_3)); }
	inline float get_axisDeadZone_3() const { return ___axisDeadZone_3; }
	inline float* get_address_of_axisDeadZone_3() { return &___axisDeadZone_3; }
	inline void set_axisDeadZone_3(float value)
	{
		___axisDeadZone_3 = value;
	}

	inline static int32_t get_offset_of_sourceButton_4() { return static_cast<int32_t>(offsetof(Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E, ___sourceButton_4)); }
	inline int32_t get_sourceButton_4() const { return ___sourceButton_4; }
	inline int32_t* get_address_of_sourceButton_4() { return &___sourceButton_4; }
	inline void set_sourceButton_4(int32_t value)
	{
		___sourceButton_4 = value;
	}

	inline static int32_t get_offset_of_sourceKeyCode_5() { return static_cast<int32_t>(offsetof(Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E, ___sourceKeyCode_5)); }
	inline int32_t get_sourceKeyCode_5() const { return ___sourceKeyCode_5; }
	inline int32_t* get_address_of_sourceKeyCode_5() { return &___sourceKeyCode_5; }
	inline void set_sourceKeyCode_5(int32_t value)
	{
		___sourceKeyCode_5 = value;
	}

	inline static int32_t get_offset_of_customCalculation_6() { return static_cast<int32_t>(offsetof(Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E, ___customCalculation_6)); }
	inline CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * get_customCalculation_6() const { return ___customCalculation_6; }
	inline CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 ** get_address_of_customCalculation_6() { return &___customCalculation_6; }
	inline void set_customCalculation_6(CustomCalculation_t9432ED2CFD91CF4D0ED647DFD85FCAA3BBDEF038 * value)
	{
		___customCalculation_6 = value;
		Il2CppCodeGenWriteBarrier((&___customCalculation_6), value);
	}

	inline static int32_t get_offset_of_customCalculationSourceData_7() { return static_cast<int32_t>(offsetof(Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E, ___customCalculationSourceData_7)); }
	inline CustomCalculationSourceDataU5BU5D_t3E25F9818C6BA4273D61F758395B67649F70CC38* get_customCalculationSourceData_7() const { return ___customCalculationSourceData_7; }
	inline CustomCalculationSourceDataU5BU5D_t3E25F9818C6BA4273D61F758395B67649F70CC38** get_address_of_customCalculationSourceData_7() { return &___customCalculationSourceData_7; }
	inline void set_customCalculationSourceData_7(CustomCalculationSourceDataU5BU5D_t3E25F9818C6BA4273D61F758395B67649F70CC38* value)
	{
		___customCalculationSourceData_7 = value;
		Il2CppCodeGenWriteBarrier((&___customCalculationSourceData_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_TB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E_H
#ifndef AXIS_T9E03950EA58556150FF5010612981B6A8B9FDC6F_H
#define AXIS_T9E03950EA58556150FF5010612981B6A8B9FDC6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis
struct  Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F  : public Element_t82B7A20AC236A568752E7BBC63B844092DD83294
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::elementIdentifier
	int32_t ___elementIdentifier_0;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::sourceType
	int32_t ___sourceType_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::sourceAxis
	int32_t ___sourceAxis_2;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::sourceAxisRange
	int32_t ___sourceAxisRange_3;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::invert
	bool ___invert_4;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::axisDeadZone
	float ___axisDeadZone_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::calibrateAxis
	bool ___calibrateAxis_6;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::axisZero
	float ___axisZero_7;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::axisMin
	float ___axisMin_8;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::axisMax
	float ___axisMax_9;
	// Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::alternateCalibrations
	AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* ___alternateCalibrations_10;
	// Rewired.Data.Mapping.HardwareAxisInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::axisInfo
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * ___axisInfo_11;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::sourceButton
	int32_t ___sourceButton_12;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::buttonAxisContribution
	int32_t ___buttonAxisContribution_13;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::sourceHat
	int32_t ___sourceHat_14;
	// Rewired.Data.Mapping.AxisDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::sourceHatDirection
	int32_t ___sourceHatDirection_15;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Axis::sourceHatRange
	int32_t ___sourceHatRange_16;

public:
	inline static int32_t get_offset_of_elementIdentifier_0() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___elementIdentifier_0)); }
	inline int32_t get_elementIdentifier_0() const { return ___elementIdentifier_0; }
	inline int32_t* get_address_of_elementIdentifier_0() { return &___elementIdentifier_0; }
	inline void set_elementIdentifier_0(int32_t value)
	{
		___elementIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_sourceType_1() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___sourceType_1)); }
	inline int32_t get_sourceType_1() const { return ___sourceType_1; }
	inline int32_t* get_address_of_sourceType_1() { return &___sourceType_1; }
	inline void set_sourceType_1(int32_t value)
	{
		___sourceType_1 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_2() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___sourceAxis_2)); }
	inline int32_t get_sourceAxis_2() const { return ___sourceAxis_2; }
	inline int32_t* get_address_of_sourceAxis_2() { return &___sourceAxis_2; }
	inline void set_sourceAxis_2(int32_t value)
	{
		___sourceAxis_2 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_3() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___sourceAxisRange_3)); }
	inline int32_t get_sourceAxisRange_3() const { return ___sourceAxisRange_3; }
	inline int32_t* get_address_of_sourceAxisRange_3() { return &___sourceAxisRange_3; }
	inline void set_sourceAxisRange_3(int32_t value)
	{
		___sourceAxisRange_3 = value;
	}

	inline static int32_t get_offset_of_invert_4() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___invert_4)); }
	inline bool get_invert_4() const { return ___invert_4; }
	inline bool* get_address_of_invert_4() { return &___invert_4; }
	inline void set_invert_4(bool value)
	{
		___invert_4 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_5() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___axisDeadZone_5)); }
	inline float get_axisDeadZone_5() const { return ___axisDeadZone_5; }
	inline float* get_address_of_axisDeadZone_5() { return &___axisDeadZone_5; }
	inline void set_axisDeadZone_5(float value)
	{
		___axisDeadZone_5 = value;
	}

	inline static int32_t get_offset_of_calibrateAxis_6() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___calibrateAxis_6)); }
	inline bool get_calibrateAxis_6() const { return ___calibrateAxis_6; }
	inline bool* get_address_of_calibrateAxis_6() { return &___calibrateAxis_6; }
	inline void set_calibrateAxis_6(bool value)
	{
		___calibrateAxis_6 = value;
	}

	inline static int32_t get_offset_of_axisZero_7() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___axisZero_7)); }
	inline float get_axisZero_7() const { return ___axisZero_7; }
	inline float* get_address_of_axisZero_7() { return &___axisZero_7; }
	inline void set_axisZero_7(float value)
	{
		___axisZero_7 = value;
	}

	inline static int32_t get_offset_of_axisMin_8() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___axisMin_8)); }
	inline float get_axisMin_8() const { return ___axisMin_8; }
	inline float* get_address_of_axisMin_8() { return &___axisMin_8; }
	inline void set_axisMin_8(float value)
	{
		___axisMin_8 = value;
	}

	inline static int32_t get_offset_of_axisMax_9() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___axisMax_9)); }
	inline float get_axisMax_9() const { return ___axisMax_9; }
	inline float* get_address_of_axisMax_9() { return &___axisMax_9; }
	inline void set_axisMax_9(float value)
	{
		___axisMax_9 = value;
	}

	inline static int32_t get_offset_of_alternateCalibrations_10() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___alternateCalibrations_10)); }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* get_alternateCalibrations_10() const { return ___alternateCalibrations_10; }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7** get_address_of_alternateCalibrations_10() { return &___alternateCalibrations_10; }
	inline void set_alternateCalibrations_10(AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* value)
	{
		___alternateCalibrations_10 = value;
		Il2CppCodeGenWriteBarrier((&___alternateCalibrations_10), value);
	}

	inline static int32_t get_offset_of_axisInfo_11() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___axisInfo_11)); }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * get_axisInfo_11() const { return ___axisInfo_11; }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 ** get_address_of_axisInfo_11() { return &___axisInfo_11; }
	inline void set_axisInfo_11(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * value)
	{
		___axisInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___axisInfo_11), value);
	}

	inline static int32_t get_offset_of_sourceButton_12() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___sourceButton_12)); }
	inline int32_t get_sourceButton_12() const { return ___sourceButton_12; }
	inline int32_t* get_address_of_sourceButton_12() { return &___sourceButton_12; }
	inline void set_sourceButton_12(int32_t value)
	{
		___sourceButton_12 = value;
	}

	inline static int32_t get_offset_of_buttonAxisContribution_13() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___buttonAxisContribution_13)); }
	inline int32_t get_buttonAxisContribution_13() const { return ___buttonAxisContribution_13; }
	inline int32_t* get_address_of_buttonAxisContribution_13() { return &___buttonAxisContribution_13; }
	inline void set_buttonAxisContribution_13(int32_t value)
	{
		___buttonAxisContribution_13 = value;
	}

	inline static int32_t get_offset_of_sourceHat_14() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___sourceHat_14)); }
	inline int32_t get_sourceHat_14() const { return ___sourceHat_14; }
	inline int32_t* get_address_of_sourceHat_14() { return &___sourceHat_14; }
	inline void set_sourceHat_14(int32_t value)
	{
		___sourceHat_14 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_15() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___sourceHatDirection_15)); }
	inline int32_t get_sourceHatDirection_15() const { return ___sourceHatDirection_15; }
	inline int32_t* get_address_of_sourceHatDirection_15() { return &___sourceHatDirection_15; }
	inline void set_sourceHatDirection_15(int32_t value)
	{
		___sourceHatDirection_15 = value;
	}

	inline static int32_t get_offset_of_sourceHatRange_16() { return static_cast<int32_t>(offsetof(Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F, ___sourceHatRange_16)); }
	inline int32_t get_sourceHatRange_16() const { return ___sourceHatRange_16; }
	inline int32_t* get_address_of_sourceHatRange_16() { return &___sourceHatRange_16; }
	inline void set_sourceHatRange_16(int32_t value)
	{
		___sourceHatRange_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T9E03950EA58556150FF5010612981B6A8B9FDC6F_H
#ifndef BUTTON_T2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B_H
#define BUTTON_T2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button
struct  Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B  : public Element_t82B7A20AC236A568752E7BBC63B844092DD83294
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::elementIdentifier
	int32_t ___elementIdentifier_0;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::sourceType
	int32_t ___sourceType_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::sourceButton
	int32_t ___sourceButton_2;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::sourceAxis
	int32_t ___sourceAxis_3;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::sourceAxisPole
	int32_t ___sourceAxisPole_4;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::axisDeadZone
	float ___axisDeadZone_5;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::sourceHat
	int32_t ___sourceHat_6;
	// Rewired.Data.Mapping.HatType Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::sourceHatType
	int32_t ___sourceHatType_7;
	// Rewired.Data.Mapping.HatDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::sourceHatDirection
	int32_t ___sourceHatDirection_8;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::requireMultipleButtons
	bool ___requireMultipleButtons_9;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::requiredButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___requiredButtons_10;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::ignoreIfButtonsActive
	bool ___ignoreIfButtonsActive_11;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::ignoreIfButtonsActiveButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ignoreIfButtonsActiveButtons_12;
	// Rewired.Data.Mapping.HardwareButtonInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_Linux_Base_Button::buttonInfo
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * ___buttonInfo_13;

public:
	inline static int32_t get_offset_of_elementIdentifier_0() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___elementIdentifier_0)); }
	inline int32_t get_elementIdentifier_0() const { return ___elementIdentifier_0; }
	inline int32_t* get_address_of_elementIdentifier_0() { return &___elementIdentifier_0; }
	inline void set_elementIdentifier_0(int32_t value)
	{
		___elementIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_sourceType_1() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___sourceType_1)); }
	inline int32_t get_sourceType_1() const { return ___sourceType_1; }
	inline int32_t* get_address_of_sourceType_1() { return &___sourceType_1; }
	inline void set_sourceType_1(int32_t value)
	{
		___sourceType_1 = value;
	}

	inline static int32_t get_offset_of_sourceButton_2() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___sourceButton_2)); }
	inline int32_t get_sourceButton_2() const { return ___sourceButton_2; }
	inline int32_t* get_address_of_sourceButton_2() { return &___sourceButton_2; }
	inline void set_sourceButton_2(int32_t value)
	{
		___sourceButton_2 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_3() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___sourceAxis_3)); }
	inline int32_t get_sourceAxis_3() const { return ___sourceAxis_3; }
	inline int32_t* get_address_of_sourceAxis_3() { return &___sourceAxis_3; }
	inline void set_sourceAxis_3(int32_t value)
	{
		___sourceAxis_3 = value;
	}

	inline static int32_t get_offset_of_sourceAxisPole_4() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___sourceAxisPole_4)); }
	inline int32_t get_sourceAxisPole_4() const { return ___sourceAxisPole_4; }
	inline int32_t* get_address_of_sourceAxisPole_4() { return &___sourceAxisPole_4; }
	inline void set_sourceAxisPole_4(int32_t value)
	{
		___sourceAxisPole_4 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_5() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___axisDeadZone_5)); }
	inline float get_axisDeadZone_5() const { return ___axisDeadZone_5; }
	inline float* get_address_of_axisDeadZone_5() { return &___axisDeadZone_5; }
	inline void set_axisDeadZone_5(float value)
	{
		___axisDeadZone_5 = value;
	}

	inline static int32_t get_offset_of_sourceHat_6() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___sourceHat_6)); }
	inline int32_t get_sourceHat_6() const { return ___sourceHat_6; }
	inline int32_t* get_address_of_sourceHat_6() { return &___sourceHat_6; }
	inline void set_sourceHat_6(int32_t value)
	{
		___sourceHat_6 = value;
	}

	inline static int32_t get_offset_of_sourceHatType_7() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___sourceHatType_7)); }
	inline int32_t get_sourceHatType_7() const { return ___sourceHatType_7; }
	inline int32_t* get_address_of_sourceHatType_7() { return &___sourceHatType_7; }
	inline void set_sourceHatType_7(int32_t value)
	{
		___sourceHatType_7 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_8() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___sourceHatDirection_8)); }
	inline int32_t get_sourceHatDirection_8() const { return ___sourceHatDirection_8; }
	inline int32_t* get_address_of_sourceHatDirection_8() { return &___sourceHatDirection_8; }
	inline void set_sourceHatDirection_8(int32_t value)
	{
		___sourceHatDirection_8 = value;
	}

	inline static int32_t get_offset_of_requireMultipleButtons_9() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___requireMultipleButtons_9)); }
	inline bool get_requireMultipleButtons_9() const { return ___requireMultipleButtons_9; }
	inline bool* get_address_of_requireMultipleButtons_9() { return &___requireMultipleButtons_9; }
	inline void set_requireMultipleButtons_9(bool value)
	{
		___requireMultipleButtons_9 = value;
	}

	inline static int32_t get_offset_of_requiredButtons_10() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___requiredButtons_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_requiredButtons_10() const { return ___requiredButtons_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_requiredButtons_10() { return &___requiredButtons_10; }
	inline void set_requiredButtons_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___requiredButtons_10 = value;
		Il2CppCodeGenWriteBarrier((&___requiredButtons_10), value);
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActive_11() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___ignoreIfButtonsActive_11)); }
	inline bool get_ignoreIfButtonsActive_11() const { return ___ignoreIfButtonsActive_11; }
	inline bool* get_address_of_ignoreIfButtonsActive_11() { return &___ignoreIfButtonsActive_11; }
	inline void set_ignoreIfButtonsActive_11(bool value)
	{
		___ignoreIfButtonsActive_11 = value;
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActiveButtons_12() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___ignoreIfButtonsActiveButtons_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ignoreIfButtonsActiveButtons_12() const { return ___ignoreIfButtonsActiveButtons_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ignoreIfButtonsActiveButtons_12() { return &___ignoreIfButtonsActiveButtons_12; }
	inline void set_ignoreIfButtonsActiveButtons_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ignoreIfButtonsActiveButtons_12 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreIfButtonsActiveButtons_12), value);
	}

	inline static int32_t get_offset_of_buttonInfo_13() { return static_cast<int32_t>(offsetof(Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B, ___buttonInfo_13)); }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * get_buttonInfo_13() const { return ___buttonInfo_13; }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 ** get_address_of_buttonInfo_13() { return &___buttonInfo_13; }
	inline void set_buttonInfo_13(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * value)
	{
		___buttonInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___buttonInfo_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B_H
#ifndef AXIS_T766FE92C87B9298B56A4F07611581240BD735EFD_H
#define AXIS_T766FE92C87B9298B56A4F07611581240BD735EFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis
struct  Axis_t766FE92C87B9298B56A4F07611581240BD735EFD  : public Element_t909158E9B20107B6BD16CD99ECF5F01BD9C2F463
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::elementIdentifier
	int32_t ___elementIdentifier_0;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::sourceType
	int32_t ___sourceType_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::sourceStick
	int32_t ___sourceStick_2;
	// Rewired.Platforms.OSXAxis Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::sourceAxis
	int32_t ___sourceAxis_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::sourceOtherAxis
	int32_t ___sourceOtherAxis_4;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::sourceAxisRange
	int32_t ___sourceAxisRange_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::invert
	bool ___invert_6;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::axisDeadZone
	float ___axisDeadZone_7;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::calibrateAxis
	bool ___calibrateAxis_8;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::axisZero
	float ___axisZero_9;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::axisMin
	float ___axisMin_10;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::axisMax
	float ___axisMax_11;
	// Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::alternateCalibrations
	AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* ___alternateCalibrations_12;
	// Rewired.Data.Mapping.HardwareAxisInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::axisInfo
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * ___axisInfo_13;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::sourceButton
	int32_t ___sourceButton_14;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::buttonAxisContribution
	int32_t ___buttonAxisContribution_15;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::sourceHat
	int32_t ___sourceHat_16;
	// Rewired.Data.Mapping.AxisDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::sourceHatDirection
	int32_t ___sourceHatDirection_17;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Axis::sourceHatRange
	int32_t ___sourceHatRange_18;

public:
	inline static int32_t get_offset_of_elementIdentifier_0() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___elementIdentifier_0)); }
	inline int32_t get_elementIdentifier_0() const { return ___elementIdentifier_0; }
	inline int32_t* get_address_of_elementIdentifier_0() { return &___elementIdentifier_0; }
	inline void set_elementIdentifier_0(int32_t value)
	{
		___elementIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_sourceType_1() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___sourceType_1)); }
	inline int32_t get_sourceType_1() const { return ___sourceType_1; }
	inline int32_t* get_address_of_sourceType_1() { return &___sourceType_1; }
	inline void set_sourceType_1(int32_t value)
	{
		___sourceType_1 = value;
	}

	inline static int32_t get_offset_of_sourceStick_2() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___sourceStick_2)); }
	inline int32_t get_sourceStick_2() const { return ___sourceStick_2; }
	inline int32_t* get_address_of_sourceStick_2() { return &___sourceStick_2; }
	inline void set_sourceStick_2(int32_t value)
	{
		___sourceStick_2 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_3() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___sourceAxis_3)); }
	inline int32_t get_sourceAxis_3() const { return ___sourceAxis_3; }
	inline int32_t* get_address_of_sourceAxis_3() { return &___sourceAxis_3; }
	inline void set_sourceAxis_3(int32_t value)
	{
		___sourceAxis_3 = value;
	}

	inline static int32_t get_offset_of_sourceOtherAxis_4() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___sourceOtherAxis_4)); }
	inline int32_t get_sourceOtherAxis_4() const { return ___sourceOtherAxis_4; }
	inline int32_t* get_address_of_sourceOtherAxis_4() { return &___sourceOtherAxis_4; }
	inline void set_sourceOtherAxis_4(int32_t value)
	{
		___sourceOtherAxis_4 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_5() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___sourceAxisRange_5)); }
	inline int32_t get_sourceAxisRange_5() const { return ___sourceAxisRange_5; }
	inline int32_t* get_address_of_sourceAxisRange_5() { return &___sourceAxisRange_5; }
	inline void set_sourceAxisRange_5(int32_t value)
	{
		___sourceAxisRange_5 = value;
	}

	inline static int32_t get_offset_of_invert_6() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___invert_6)); }
	inline bool get_invert_6() const { return ___invert_6; }
	inline bool* get_address_of_invert_6() { return &___invert_6; }
	inline void set_invert_6(bool value)
	{
		___invert_6 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_7() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___axisDeadZone_7)); }
	inline float get_axisDeadZone_7() const { return ___axisDeadZone_7; }
	inline float* get_address_of_axisDeadZone_7() { return &___axisDeadZone_7; }
	inline void set_axisDeadZone_7(float value)
	{
		___axisDeadZone_7 = value;
	}

	inline static int32_t get_offset_of_calibrateAxis_8() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___calibrateAxis_8)); }
	inline bool get_calibrateAxis_8() const { return ___calibrateAxis_8; }
	inline bool* get_address_of_calibrateAxis_8() { return &___calibrateAxis_8; }
	inline void set_calibrateAxis_8(bool value)
	{
		___calibrateAxis_8 = value;
	}

	inline static int32_t get_offset_of_axisZero_9() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___axisZero_9)); }
	inline float get_axisZero_9() const { return ___axisZero_9; }
	inline float* get_address_of_axisZero_9() { return &___axisZero_9; }
	inline void set_axisZero_9(float value)
	{
		___axisZero_9 = value;
	}

	inline static int32_t get_offset_of_axisMin_10() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___axisMin_10)); }
	inline float get_axisMin_10() const { return ___axisMin_10; }
	inline float* get_address_of_axisMin_10() { return &___axisMin_10; }
	inline void set_axisMin_10(float value)
	{
		___axisMin_10 = value;
	}

	inline static int32_t get_offset_of_axisMax_11() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___axisMax_11)); }
	inline float get_axisMax_11() const { return ___axisMax_11; }
	inline float* get_address_of_axisMax_11() { return &___axisMax_11; }
	inline void set_axisMax_11(float value)
	{
		___axisMax_11 = value;
	}

	inline static int32_t get_offset_of_alternateCalibrations_12() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___alternateCalibrations_12)); }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* get_alternateCalibrations_12() const { return ___alternateCalibrations_12; }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7** get_address_of_alternateCalibrations_12() { return &___alternateCalibrations_12; }
	inline void set_alternateCalibrations_12(AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* value)
	{
		___alternateCalibrations_12 = value;
		Il2CppCodeGenWriteBarrier((&___alternateCalibrations_12), value);
	}

	inline static int32_t get_offset_of_axisInfo_13() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___axisInfo_13)); }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * get_axisInfo_13() const { return ___axisInfo_13; }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 ** get_address_of_axisInfo_13() { return &___axisInfo_13; }
	inline void set_axisInfo_13(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * value)
	{
		___axisInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___axisInfo_13), value);
	}

	inline static int32_t get_offset_of_sourceButton_14() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___sourceButton_14)); }
	inline int32_t get_sourceButton_14() const { return ___sourceButton_14; }
	inline int32_t* get_address_of_sourceButton_14() { return &___sourceButton_14; }
	inline void set_sourceButton_14(int32_t value)
	{
		___sourceButton_14 = value;
	}

	inline static int32_t get_offset_of_buttonAxisContribution_15() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___buttonAxisContribution_15)); }
	inline int32_t get_buttonAxisContribution_15() const { return ___buttonAxisContribution_15; }
	inline int32_t* get_address_of_buttonAxisContribution_15() { return &___buttonAxisContribution_15; }
	inline void set_buttonAxisContribution_15(int32_t value)
	{
		___buttonAxisContribution_15 = value;
	}

	inline static int32_t get_offset_of_sourceHat_16() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___sourceHat_16)); }
	inline int32_t get_sourceHat_16() const { return ___sourceHat_16; }
	inline int32_t* get_address_of_sourceHat_16() { return &___sourceHat_16; }
	inline void set_sourceHat_16(int32_t value)
	{
		___sourceHat_16 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_17() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___sourceHatDirection_17)); }
	inline int32_t get_sourceHatDirection_17() const { return ___sourceHatDirection_17; }
	inline int32_t* get_address_of_sourceHatDirection_17() { return &___sourceHatDirection_17; }
	inline void set_sourceHatDirection_17(int32_t value)
	{
		___sourceHatDirection_17 = value;
	}

	inline static int32_t get_offset_of_sourceHatRange_18() { return static_cast<int32_t>(offsetof(Axis_t766FE92C87B9298B56A4F07611581240BD735EFD, ___sourceHatRange_18)); }
	inline int32_t get_sourceHatRange_18() const { return ___sourceHatRange_18; }
	inline int32_t* get_address_of_sourceHatRange_18() { return &___sourceHatRange_18; }
	inline void set_sourceHatRange_18(int32_t value)
	{
		___sourceHatRange_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T766FE92C87B9298B56A4F07611581240BD735EFD_H
#ifndef BUTTON_T841D3D6DD5392867B22FA416457754550B1B8F3A_H
#define BUTTON_T841D3D6DD5392867B22FA416457754550B1B8F3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button
struct  Button_t841D3D6DD5392867B22FA416457754550B1B8F3A  : public Element_t909158E9B20107B6BD16CD99ECF5F01BD9C2F463
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::elementIdentifier
	int32_t ___elementIdentifier_0;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::sourceType
	int32_t ___sourceType_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::sourceButton
	int32_t ___sourceButton_2;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::sourceStick
	int32_t ___sourceStick_3;
	// Rewired.Platforms.OSXAxis Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::sourceAxis
	int32_t ___sourceAxis_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::sourceOtherAxis
	int32_t ___sourceOtherAxis_5;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::sourceAxisPole
	int32_t ___sourceAxisPole_6;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::axisDeadZone
	float ___axisDeadZone_7;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::sourceHat
	int32_t ___sourceHat_8;
	// Rewired.Data.Mapping.HatType Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::sourceHatType
	int32_t ___sourceHatType_9;
	// Rewired.Data.Mapping.HatDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::sourceHatDirection
	int32_t ___sourceHatDirection_10;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::requireMultipleButtons
	bool ___requireMultipleButtons_11;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::requiredButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___requiredButtons_12;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::ignoreIfButtonsActive
	bool ___ignoreIfButtonsActive_13;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::ignoreIfButtonsActiveButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ignoreIfButtonsActiveButtons_14;
	// Rewired.Data.Mapping.HardwareButtonInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_OSX_Base_Button::buttonInfo
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * ___buttonInfo_15;

public:
	inline static int32_t get_offset_of_elementIdentifier_0() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___elementIdentifier_0)); }
	inline int32_t get_elementIdentifier_0() const { return ___elementIdentifier_0; }
	inline int32_t* get_address_of_elementIdentifier_0() { return &___elementIdentifier_0; }
	inline void set_elementIdentifier_0(int32_t value)
	{
		___elementIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_sourceType_1() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___sourceType_1)); }
	inline int32_t get_sourceType_1() const { return ___sourceType_1; }
	inline int32_t* get_address_of_sourceType_1() { return &___sourceType_1; }
	inline void set_sourceType_1(int32_t value)
	{
		___sourceType_1 = value;
	}

	inline static int32_t get_offset_of_sourceButton_2() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___sourceButton_2)); }
	inline int32_t get_sourceButton_2() const { return ___sourceButton_2; }
	inline int32_t* get_address_of_sourceButton_2() { return &___sourceButton_2; }
	inline void set_sourceButton_2(int32_t value)
	{
		___sourceButton_2 = value;
	}

	inline static int32_t get_offset_of_sourceStick_3() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___sourceStick_3)); }
	inline int32_t get_sourceStick_3() const { return ___sourceStick_3; }
	inline int32_t* get_address_of_sourceStick_3() { return &___sourceStick_3; }
	inline void set_sourceStick_3(int32_t value)
	{
		___sourceStick_3 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_4() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___sourceAxis_4)); }
	inline int32_t get_sourceAxis_4() const { return ___sourceAxis_4; }
	inline int32_t* get_address_of_sourceAxis_4() { return &___sourceAxis_4; }
	inline void set_sourceAxis_4(int32_t value)
	{
		___sourceAxis_4 = value;
	}

	inline static int32_t get_offset_of_sourceOtherAxis_5() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___sourceOtherAxis_5)); }
	inline int32_t get_sourceOtherAxis_5() const { return ___sourceOtherAxis_5; }
	inline int32_t* get_address_of_sourceOtherAxis_5() { return &___sourceOtherAxis_5; }
	inline void set_sourceOtherAxis_5(int32_t value)
	{
		___sourceOtherAxis_5 = value;
	}

	inline static int32_t get_offset_of_sourceAxisPole_6() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___sourceAxisPole_6)); }
	inline int32_t get_sourceAxisPole_6() const { return ___sourceAxisPole_6; }
	inline int32_t* get_address_of_sourceAxisPole_6() { return &___sourceAxisPole_6; }
	inline void set_sourceAxisPole_6(int32_t value)
	{
		___sourceAxisPole_6 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_7() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___axisDeadZone_7)); }
	inline float get_axisDeadZone_7() const { return ___axisDeadZone_7; }
	inline float* get_address_of_axisDeadZone_7() { return &___axisDeadZone_7; }
	inline void set_axisDeadZone_7(float value)
	{
		___axisDeadZone_7 = value;
	}

	inline static int32_t get_offset_of_sourceHat_8() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___sourceHat_8)); }
	inline int32_t get_sourceHat_8() const { return ___sourceHat_8; }
	inline int32_t* get_address_of_sourceHat_8() { return &___sourceHat_8; }
	inline void set_sourceHat_8(int32_t value)
	{
		___sourceHat_8 = value;
	}

	inline static int32_t get_offset_of_sourceHatType_9() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___sourceHatType_9)); }
	inline int32_t get_sourceHatType_9() const { return ___sourceHatType_9; }
	inline int32_t* get_address_of_sourceHatType_9() { return &___sourceHatType_9; }
	inline void set_sourceHatType_9(int32_t value)
	{
		___sourceHatType_9 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_10() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___sourceHatDirection_10)); }
	inline int32_t get_sourceHatDirection_10() const { return ___sourceHatDirection_10; }
	inline int32_t* get_address_of_sourceHatDirection_10() { return &___sourceHatDirection_10; }
	inline void set_sourceHatDirection_10(int32_t value)
	{
		___sourceHatDirection_10 = value;
	}

	inline static int32_t get_offset_of_requireMultipleButtons_11() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___requireMultipleButtons_11)); }
	inline bool get_requireMultipleButtons_11() const { return ___requireMultipleButtons_11; }
	inline bool* get_address_of_requireMultipleButtons_11() { return &___requireMultipleButtons_11; }
	inline void set_requireMultipleButtons_11(bool value)
	{
		___requireMultipleButtons_11 = value;
	}

	inline static int32_t get_offset_of_requiredButtons_12() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___requiredButtons_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_requiredButtons_12() const { return ___requiredButtons_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_requiredButtons_12() { return &___requiredButtons_12; }
	inline void set_requiredButtons_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___requiredButtons_12 = value;
		Il2CppCodeGenWriteBarrier((&___requiredButtons_12), value);
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActive_13() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___ignoreIfButtonsActive_13)); }
	inline bool get_ignoreIfButtonsActive_13() const { return ___ignoreIfButtonsActive_13; }
	inline bool* get_address_of_ignoreIfButtonsActive_13() { return &___ignoreIfButtonsActive_13; }
	inline void set_ignoreIfButtonsActive_13(bool value)
	{
		___ignoreIfButtonsActive_13 = value;
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActiveButtons_14() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___ignoreIfButtonsActiveButtons_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ignoreIfButtonsActiveButtons_14() const { return ___ignoreIfButtonsActiveButtons_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ignoreIfButtonsActiveButtons_14() { return &___ignoreIfButtonsActiveButtons_14; }
	inline void set_ignoreIfButtonsActiveButtons_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ignoreIfButtonsActiveButtons_14 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreIfButtonsActiveButtons_14), value);
	}

	inline static int32_t get_offset_of_buttonInfo_15() { return static_cast<int32_t>(offsetof(Button_t841D3D6DD5392867B22FA416457754550B1B8F3A, ___buttonInfo_15)); }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * get_buttonInfo_15() const { return ___buttonInfo_15; }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 ** get_address_of_buttonInfo_15() { return &___buttonInfo_15; }
	inline void set_buttonInfo_15(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * value)
	{
		___buttonInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___buttonInfo_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T841D3D6DD5392867B22FA416457754550B1B8F3A_H
#ifndef PLATFORM_OUYA_T6E1DA960C43511902B179E0E6746B8230AE60092_H
#define PLATFORM_OUYA_T6E1DA960C43511902B179E0E6746B8230AE60092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya
struct  Platform_Ouya_t6E1DA960C43511902B179E0E6746B8230AE60092  : public Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya::variants
	Platform_Ouya_BaseU5BU5D_t6463B5A1A1AA1104079C1095423FC5BDDE9C76FE* ___variants_5;

public:
	inline static int32_t get_offset_of_variants_5() { return static_cast<int32_t>(offsetof(Platform_Ouya_t6E1DA960C43511902B179E0E6746B8230AE60092, ___variants_5)); }
	inline Platform_Ouya_BaseU5BU5D_t6463B5A1A1AA1104079C1095423FC5BDDE9C76FE* get_variants_5() const { return ___variants_5; }
	inline Platform_Ouya_BaseU5BU5D_t6463B5A1A1AA1104079C1095423FC5BDDE9C76FE** get_address_of_variants_5() { return &___variants_5; }
	inline void set_variants_5(Platform_Ouya_BaseU5BU5D_t6463B5A1A1AA1104079C1095423FC5BDDE9C76FE* value)
	{
		___variants_5 = value;
		Il2CppCodeGenWriteBarrier((&___variants_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_OUYA_T6E1DA960C43511902B179E0E6746B8230AE60092_H
#ifndef PLATFORM_PS4_T5B4B7E5A8EF60DFB5E1F1CE6756ADD5BD5FB4FA1_H
#define PLATFORM_PS4_T5B4B7E5A8EF60DFB5E1F1CE6756ADD5BD5FB4FA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4
struct  Platform_PS4_t5B4B7E5A8EF60DFB5E1F1CE6756ADD5BD5FB4FA1  : public Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4::variants
	Platform_PS4_BaseU5BU5D_tF68B32F3AD14ACFAD4AA06B6A9B98B23C50CE612* ___variants_5;

public:
	inline static int32_t get_offset_of_variants_5() { return static_cast<int32_t>(offsetof(Platform_PS4_t5B4B7E5A8EF60DFB5E1F1CE6756ADD5BD5FB4FA1, ___variants_5)); }
	inline Platform_PS4_BaseU5BU5D_tF68B32F3AD14ACFAD4AA06B6A9B98B23C50CE612* get_variants_5() const { return ___variants_5; }
	inline Platform_PS4_BaseU5BU5D_tF68B32F3AD14ACFAD4AA06B6A9B98B23C50CE612** get_address_of_variants_5() { return &___variants_5; }
	inline void set_variants_5(Platform_PS4_BaseU5BU5D_tF68B32F3AD14ACFAD4AA06B6A9B98B23C50CE612* value)
	{
		___variants_5 = value;
		Il2CppCodeGenWriteBarrier((&___variants_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_PS4_T5B4B7E5A8EF60DFB5E1F1CE6756ADD5BD5FB4FA1_H
#ifndef PLATFORM_RAWINPUT_TFA8A4734C93ADCCF4CE7C2E3947003AB875E5FBD_H
#define PLATFORM_RAWINPUT_TFA8A4734C93ADCCF4CE7C2E3947003AB875E5FBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput
struct  Platform_RawInput_tFA8A4734C93ADCCF4CE7C2E3947003AB875E5FBD  : public Platform_RawInput_Base_tA0DFFECAF98212B21416DE596B7011684B6F5ADB
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput::variants
	Platform_RawInput_BaseU5BU5D_t3E3DC3F59AEE259B5EFDEF78EA037CB9159B51BC* ___variants_3;

public:
	inline static int32_t get_offset_of_variants_3() { return static_cast<int32_t>(offsetof(Platform_RawInput_tFA8A4734C93ADCCF4CE7C2E3947003AB875E5FBD, ___variants_3)); }
	inline Platform_RawInput_BaseU5BU5D_t3E3DC3F59AEE259B5EFDEF78EA037CB9159B51BC* get_variants_3() const { return ___variants_3; }
	inline Platform_RawInput_BaseU5BU5D_t3E3DC3F59AEE259B5EFDEF78EA037CB9159B51BC** get_address_of_variants_3() { return &___variants_3; }
	inline void set_variants_3(Platform_RawInput_BaseU5BU5D_t3E3DC3F59AEE259B5EFDEF78EA037CB9159B51BC* value)
	{
		___variants_3 = value;
		Il2CppCodeGenWriteBarrier((&___variants_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_RAWINPUT_TFA8A4734C93ADCCF4CE7C2E3947003AB875E5FBD_H
#ifndef AXIS_BASE_TDCD5E6CC6636C4ECA93D8DDE3302423259E63623_H
#define AXIS_BASE_TDCD5E6CC6636C4ECA93D8DDE3302423259E63623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base
struct  Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623  : public Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::elementIdentifier
	int32_t ___elementIdentifier_2;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceType
	int32_t ___sourceType_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceAxis
	int32_t ___sourceAxis_4;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceAxisRange
	int32_t ___sourceAxisRange_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::invert
	bool ___invert_6;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::axisDeadZone
	float ___axisDeadZone_7;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::calibrateAxis
	bool ___calibrateAxis_8;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::axisZero
	float ___axisZero_9;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::axisMin
	float ___axisMin_10;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::axisMax
	float ___axisMax_11;
	// Rewired.Data.Mapping.HardwareAxisInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::axisInfo
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * ___axisInfo_12;
	// Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::alternateCalibrations
	AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* ___alternateCalibrations_13;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceButton
	int32_t ___sourceButton_14;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::buttonAxisContribution
	int32_t ___buttonAxisContribution_15;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceHat
	int32_t ___sourceHat_16;
	// Rewired.Data.Mapping.AxisDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceHatDirection
	int32_t ___sourceHatDirection_17;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Axis_Base::sourceHatRange
	int32_t ___sourceHatRange_18;

public:
	inline static int32_t get_offset_of_elementIdentifier_2() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___elementIdentifier_2)); }
	inline int32_t get_elementIdentifier_2() const { return ___elementIdentifier_2; }
	inline int32_t* get_address_of_elementIdentifier_2() { return &___elementIdentifier_2; }
	inline void set_elementIdentifier_2(int32_t value)
	{
		___elementIdentifier_2 = value;
	}

	inline static int32_t get_offset_of_sourceType_3() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceType_3)); }
	inline int32_t get_sourceType_3() const { return ___sourceType_3; }
	inline int32_t* get_address_of_sourceType_3() { return &___sourceType_3; }
	inline void set_sourceType_3(int32_t value)
	{
		___sourceType_3 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_4() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceAxis_4)); }
	inline int32_t get_sourceAxis_4() const { return ___sourceAxis_4; }
	inline int32_t* get_address_of_sourceAxis_4() { return &___sourceAxis_4; }
	inline void set_sourceAxis_4(int32_t value)
	{
		___sourceAxis_4 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_5() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceAxisRange_5)); }
	inline int32_t get_sourceAxisRange_5() const { return ___sourceAxisRange_5; }
	inline int32_t* get_address_of_sourceAxisRange_5() { return &___sourceAxisRange_5; }
	inline void set_sourceAxisRange_5(int32_t value)
	{
		___sourceAxisRange_5 = value;
	}

	inline static int32_t get_offset_of_invert_6() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___invert_6)); }
	inline bool get_invert_6() const { return ___invert_6; }
	inline bool* get_address_of_invert_6() { return &___invert_6; }
	inline void set_invert_6(bool value)
	{
		___invert_6 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_7() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___axisDeadZone_7)); }
	inline float get_axisDeadZone_7() const { return ___axisDeadZone_7; }
	inline float* get_address_of_axisDeadZone_7() { return &___axisDeadZone_7; }
	inline void set_axisDeadZone_7(float value)
	{
		___axisDeadZone_7 = value;
	}

	inline static int32_t get_offset_of_calibrateAxis_8() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___calibrateAxis_8)); }
	inline bool get_calibrateAxis_8() const { return ___calibrateAxis_8; }
	inline bool* get_address_of_calibrateAxis_8() { return &___calibrateAxis_8; }
	inline void set_calibrateAxis_8(bool value)
	{
		___calibrateAxis_8 = value;
	}

	inline static int32_t get_offset_of_axisZero_9() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___axisZero_9)); }
	inline float get_axisZero_9() const { return ___axisZero_9; }
	inline float* get_address_of_axisZero_9() { return &___axisZero_9; }
	inline void set_axisZero_9(float value)
	{
		___axisZero_9 = value;
	}

	inline static int32_t get_offset_of_axisMin_10() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___axisMin_10)); }
	inline float get_axisMin_10() const { return ___axisMin_10; }
	inline float* get_address_of_axisMin_10() { return &___axisMin_10; }
	inline void set_axisMin_10(float value)
	{
		___axisMin_10 = value;
	}

	inline static int32_t get_offset_of_axisMax_11() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___axisMax_11)); }
	inline float get_axisMax_11() const { return ___axisMax_11; }
	inline float* get_address_of_axisMax_11() { return &___axisMax_11; }
	inline void set_axisMax_11(float value)
	{
		___axisMax_11 = value;
	}

	inline static int32_t get_offset_of_axisInfo_12() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___axisInfo_12)); }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * get_axisInfo_12() const { return ___axisInfo_12; }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 ** get_address_of_axisInfo_12() { return &___axisInfo_12; }
	inline void set_axisInfo_12(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * value)
	{
		___axisInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___axisInfo_12), value);
	}

	inline static int32_t get_offset_of_alternateCalibrations_13() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___alternateCalibrations_13)); }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* get_alternateCalibrations_13() const { return ___alternateCalibrations_13; }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7** get_address_of_alternateCalibrations_13() { return &___alternateCalibrations_13; }
	inline void set_alternateCalibrations_13(AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* value)
	{
		___alternateCalibrations_13 = value;
		Il2CppCodeGenWriteBarrier((&___alternateCalibrations_13), value);
	}

	inline static int32_t get_offset_of_sourceButton_14() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceButton_14)); }
	inline int32_t get_sourceButton_14() const { return ___sourceButton_14; }
	inline int32_t* get_address_of_sourceButton_14() { return &___sourceButton_14; }
	inline void set_sourceButton_14(int32_t value)
	{
		___sourceButton_14 = value;
	}

	inline static int32_t get_offset_of_buttonAxisContribution_15() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___buttonAxisContribution_15)); }
	inline int32_t get_buttonAxisContribution_15() const { return ___buttonAxisContribution_15; }
	inline int32_t* get_address_of_buttonAxisContribution_15() { return &___buttonAxisContribution_15; }
	inline void set_buttonAxisContribution_15(int32_t value)
	{
		___buttonAxisContribution_15 = value;
	}

	inline static int32_t get_offset_of_sourceHat_16() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceHat_16)); }
	inline int32_t get_sourceHat_16() const { return ___sourceHat_16; }
	inline int32_t* get_address_of_sourceHat_16() { return &___sourceHat_16; }
	inline void set_sourceHat_16(int32_t value)
	{
		___sourceHat_16 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_17() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceHatDirection_17)); }
	inline int32_t get_sourceHatDirection_17() const { return ___sourceHatDirection_17; }
	inline int32_t* get_address_of_sourceHatDirection_17() { return &___sourceHatDirection_17; }
	inline void set_sourceHatDirection_17(int32_t value)
	{
		___sourceHatDirection_17 = value;
	}

	inline static int32_t get_offset_of_sourceHatRange_18() { return static_cast<int32_t>(offsetof(Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623, ___sourceHatRange_18)); }
	inline int32_t get_sourceHatRange_18() const { return ___sourceHatRange_18; }
	inline int32_t* get_address_of_sourceHatRange_18() { return &___sourceHatRange_18; }
	inline void set_sourceHatRange_18(int32_t value)
	{
		___sourceHatRange_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_BASE_TDCD5E6CC6636C4ECA93D8DDE3302423259E63623_H
#ifndef BUTTON_BASE_TCA15C13FD1596A13A31E6ECCD63A077476969024_H
#define BUTTON_BASE_TCA15C13FD1596A13A31E6ECCD63A077476969024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base
struct  Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024  : public Element_t4BF2B5F62794DE9D567C4FBE1693703D28E1AD4B
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::elementIdentifier
	int32_t ___elementIdentifier_2;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceType
	int32_t ___sourceType_3;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceButton
	int32_t ___sourceButton_4;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceAxis
	int32_t ___sourceAxis_5;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceAxisPole
	int32_t ___sourceAxisPole_6;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::axisDeadZone
	float ___axisDeadZone_7;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceHat
	int32_t ___sourceHat_8;
	// Rewired.Data.Mapping.HatType Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceHatType
	int32_t ___sourceHatType_9;
	// Rewired.Data.Mapping.HatDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::sourceHatDirection
	int32_t ___sourceHatDirection_10;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::requireMultipleButtons
	bool ___requireMultipleButtons_11;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::requiredButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___requiredButtons_12;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::ignoreIfButtonsActive
	bool ___ignoreIfButtonsActive_13;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::ignoreIfButtonsActiveButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ignoreIfButtonsActiveButtons_14;
	// Rewired.Data.Mapping.HardwareButtonInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawOrDirectInput_Button_Base::buttonInfo
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * ___buttonInfo_15;

public:
	inline static int32_t get_offset_of_elementIdentifier_2() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___elementIdentifier_2)); }
	inline int32_t get_elementIdentifier_2() const { return ___elementIdentifier_2; }
	inline int32_t* get_address_of_elementIdentifier_2() { return &___elementIdentifier_2; }
	inline void set_elementIdentifier_2(int32_t value)
	{
		___elementIdentifier_2 = value;
	}

	inline static int32_t get_offset_of_sourceType_3() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceType_3)); }
	inline int32_t get_sourceType_3() const { return ___sourceType_3; }
	inline int32_t* get_address_of_sourceType_3() { return &___sourceType_3; }
	inline void set_sourceType_3(int32_t value)
	{
		___sourceType_3 = value;
	}

	inline static int32_t get_offset_of_sourceButton_4() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceButton_4)); }
	inline int32_t get_sourceButton_4() const { return ___sourceButton_4; }
	inline int32_t* get_address_of_sourceButton_4() { return &___sourceButton_4; }
	inline void set_sourceButton_4(int32_t value)
	{
		___sourceButton_4 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_5() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceAxis_5)); }
	inline int32_t get_sourceAxis_5() const { return ___sourceAxis_5; }
	inline int32_t* get_address_of_sourceAxis_5() { return &___sourceAxis_5; }
	inline void set_sourceAxis_5(int32_t value)
	{
		___sourceAxis_5 = value;
	}

	inline static int32_t get_offset_of_sourceAxisPole_6() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceAxisPole_6)); }
	inline int32_t get_sourceAxisPole_6() const { return ___sourceAxisPole_6; }
	inline int32_t* get_address_of_sourceAxisPole_6() { return &___sourceAxisPole_6; }
	inline void set_sourceAxisPole_6(int32_t value)
	{
		___sourceAxisPole_6 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_7() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___axisDeadZone_7)); }
	inline float get_axisDeadZone_7() const { return ___axisDeadZone_7; }
	inline float* get_address_of_axisDeadZone_7() { return &___axisDeadZone_7; }
	inline void set_axisDeadZone_7(float value)
	{
		___axisDeadZone_7 = value;
	}

	inline static int32_t get_offset_of_sourceHat_8() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceHat_8)); }
	inline int32_t get_sourceHat_8() const { return ___sourceHat_8; }
	inline int32_t* get_address_of_sourceHat_8() { return &___sourceHat_8; }
	inline void set_sourceHat_8(int32_t value)
	{
		___sourceHat_8 = value;
	}

	inline static int32_t get_offset_of_sourceHatType_9() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceHatType_9)); }
	inline int32_t get_sourceHatType_9() const { return ___sourceHatType_9; }
	inline int32_t* get_address_of_sourceHatType_9() { return &___sourceHatType_9; }
	inline void set_sourceHatType_9(int32_t value)
	{
		___sourceHatType_9 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_10() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___sourceHatDirection_10)); }
	inline int32_t get_sourceHatDirection_10() const { return ___sourceHatDirection_10; }
	inline int32_t* get_address_of_sourceHatDirection_10() { return &___sourceHatDirection_10; }
	inline void set_sourceHatDirection_10(int32_t value)
	{
		___sourceHatDirection_10 = value;
	}

	inline static int32_t get_offset_of_requireMultipleButtons_11() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___requireMultipleButtons_11)); }
	inline bool get_requireMultipleButtons_11() const { return ___requireMultipleButtons_11; }
	inline bool* get_address_of_requireMultipleButtons_11() { return &___requireMultipleButtons_11; }
	inline void set_requireMultipleButtons_11(bool value)
	{
		___requireMultipleButtons_11 = value;
	}

	inline static int32_t get_offset_of_requiredButtons_12() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___requiredButtons_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_requiredButtons_12() const { return ___requiredButtons_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_requiredButtons_12() { return &___requiredButtons_12; }
	inline void set_requiredButtons_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___requiredButtons_12 = value;
		Il2CppCodeGenWriteBarrier((&___requiredButtons_12), value);
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActive_13() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___ignoreIfButtonsActive_13)); }
	inline bool get_ignoreIfButtonsActive_13() const { return ___ignoreIfButtonsActive_13; }
	inline bool* get_address_of_ignoreIfButtonsActive_13() { return &___ignoreIfButtonsActive_13; }
	inline void set_ignoreIfButtonsActive_13(bool value)
	{
		___ignoreIfButtonsActive_13 = value;
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActiveButtons_14() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___ignoreIfButtonsActiveButtons_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ignoreIfButtonsActiveButtons_14() const { return ___ignoreIfButtonsActiveButtons_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ignoreIfButtonsActiveButtons_14() { return &___ignoreIfButtonsActiveButtons_14; }
	inline void set_ignoreIfButtonsActiveButtons_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ignoreIfButtonsActiveButtons_14 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreIfButtonsActiveButtons_14), value);
	}

	inline static int32_t get_offset_of_buttonInfo_15() { return static_cast<int32_t>(offsetof(Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024, ___buttonInfo_15)); }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * get_buttonInfo_15() const { return ___buttonInfo_15; }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 ** get_address_of_buttonInfo_15() { return &___buttonInfo_15; }
	inline void set_buttonInfo_15(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * value)
	{
		___buttonInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___buttonInfo_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_BASE_TCA15C13FD1596A13A31E6ECCD63A077476969024_H
#ifndef AXIS_T1177CDA7B0F08C57CA4568A5D34477A23B442312_H
#define AXIS_T1177CDA7B0F08C57CA4568A5D34477A23B442312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis
struct  Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312  : public Element_tED0CF25F632E14F6E9BA2C7306E99D690D242165
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::elementIdentifier
	int32_t ___elementIdentifier_0;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::sourceType
	int32_t ___sourceType_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::sourceAxis
	int32_t ___sourceAxis_2;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::sourceAxisRange
	int32_t ___sourceAxisRange_3;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::invert
	bool ___invert_4;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::axisDeadZone
	float ___axisDeadZone_5;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::calibrateAxis
	bool ___calibrateAxis_6;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::axisZero
	float ___axisZero_7;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::axisMin
	float ___axisMin_8;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::axisMax
	float ___axisMax_9;
	// Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::alternateCalibrations
	AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* ___alternateCalibrations_10;
	// Rewired.Data.Mapping.HardwareAxisInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::axisInfo
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * ___axisInfo_11;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::sourceButton
	int32_t ___sourceButton_12;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::buttonAxisContribution
	int32_t ___buttonAxisContribution_13;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::sourceHat
	int32_t ___sourceHat_14;
	// Rewired.Data.Mapping.AxisDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::sourceHatDirection
	int32_t ___sourceHatDirection_15;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Axis::sourceHatRange
	int32_t ___sourceHatRange_16;

public:
	inline static int32_t get_offset_of_elementIdentifier_0() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___elementIdentifier_0)); }
	inline int32_t get_elementIdentifier_0() const { return ___elementIdentifier_0; }
	inline int32_t* get_address_of_elementIdentifier_0() { return &___elementIdentifier_0; }
	inline void set_elementIdentifier_0(int32_t value)
	{
		___elementIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_sourceType_1() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___sourceType_1)); }
	inline int32_t get_sourceType_1() const { return ___sourceType_1; }
	inline int32_t* get_address_of_sourceType_1() { return &___sourceType_1; }
	inline void set_sourceType_1(int32_t value)
	{
		___sourceType_1 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_2() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___sourceAxis_2)); }
	inline int32_t get_sourceAxis_2() const { return ___sourceAxis_2; }
	inline int32_t* get_address_of_sourceAxis_2() { return &___sourceAxis_2; }
	inline void set_sourceAxis_2(int32_t value)
	{
		___sourceAxis_2 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_3() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___sourceAxisRange_3)); }
	inline int32_t get_sourceAxisRange_3() const { return ___sourceAxisRange_3; }
	inline int32_t* get_address_of_sourceAxisRange_3() { return &___sourceAxisRange_3; }
	inline void set_sourceAxisRange_3(int32_t value)
	{
		___sourceAxisRange_3 = value;
	}

	inline static int32_t get_offset_of_invert_4() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___invert_4)); }
	inline bool get_invert_4() const { return ___invert_4; }
	inline bool* get_address_of_invert_4() { return &___invert_4; }
	inline void set_invert_4(bool value)
	{
		___invert_4 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_5() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___axisDeadZone_5)); }
	inline float get_axisDeadZone_5() const { return ___axisDeadZone_5; }
	inline float* get_address_of_axisDeadZone_5() { return &___axisDeadZone_5; }
	inline void set_axisDeadZone_5(float value)
	{
		___axisDeadZone_5 = value;
	}

	inline static int32_t get_offset_of_calibrateAxis_6() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___calibrateAxis_6)); }
	inline bool get_calibrateAxis_6() const { return ___calibrateAxis_6; }
	inline bool* get_address_of_calibrateAxis_6() { return &___calibrateAxis_6; }
	inline void set_calibrateAxis_6(bool value)
	{
		___calibrateAxis_6 = value;
	}

	inline static int32_t get_offset_of_axisZero_7() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___axisZero_7)); }
	inline float get_axisZero_7() const { return ___axisZero_7; }
	inline float* get_address_of_axisZero_7() { return &___axisZero_7; }
	inline void set_axisZero_7(float value)
	{
		___axisZero_7 = value;
	}

	inline static int32_t get_offset_of_axisMin_8() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___axisMin_8)); }
	inline float get_axisMin_8() const { return ___axisMin_8; }
	inline float* get_address_of_axisMin_8() { return &___axisMin_8; }
	inline void set_axisMin_8(float value)
	{
		___axisMin_8 = value;
	}

	inline static int32_t get_offset_of_axisMax_9() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___axisMax_9)); }
	inline float get_axisMax_9() const { return ___axisMax_9; }
	inline float* get_address_of_axisMax_9() { return &___axisMax_9; }
	inline void set_axisMax_9(float value)
	{
		___axisMax_9 = value;
	}

	inline static int32_t get_offset_of_alternateCalibrations_10() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___alternateCalibrations_10)); }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* get_alternateCalibrations_10() const { return ___alternateCalibrations_10; }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7** get_address_of_alternateCalibrations_10() { return &___alternateCalibrations_10; }
	inline void set_alternateCalibrations_10(AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* value)
	{
		___alternateCalibrations_10 = value;
		Il2CppCodeGenWriteBarrier((&___alternateCalibrations_10), value);
	}

	inline static int32_t get_offset_of_axisInfo_11() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___axisInfo_11)); }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * get_axisInfo_11() const { return ___axisInfo_11; }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 ** get_address_of_axisInfo_11() { return &___axisInfo_11; }
	inline void set_axisInfo_11(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * value)
	{
		___axisInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___axisInfo_11), value);
	}

	inline static int32_t get_offset_of_sourceButton_12() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___sourceButton_12)); }
	inline int32_t get_sourceButton_12() const { return ___sourceButton_12; }
	inline int32_t* get_address_of_sourceButton_12() { return &___sourceButton_12; }
	inline void set_sourceButton_12(int32_t value)
	{
		___sourceButton_12 = value;
	}

	inline static int32_t get_offset_of_buttonAxisContribution_13() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___buttonAxisContribution_13)); }
	inline int32_t get_buttonAxisContribution_13() const { return ___buttonAxisContribution_13; }
	inline int32_t* get_address_of_buttonAxisContribution_13() { return &___buttonAxisContribution_13; }
	inline void set_buttonAxisContribution_13(int32_t value)
	{
		___buttonAxisContribution_13 = value;
	}

	inline static int32_t get_offset_of_sourceHat_14() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___sourceHat_14)); }
	inline int32_t get_sourceHat_14() const { return ___sourceHat_14; }
	inline int32_t* get_address_of_sourceHat_14() { return &___sourceHat_14; }
	inline void set_sourceHat_14(int32_t value)
	{
		___sourceHat_14 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_15() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___sourceHatDirection_15)); }
	inline int32_t get_sourceHatDirection_15() const { return ___sourceHatDirection_15; }
	inline int32_t* get_address_of_sourceHatDirection_15() { return &___sourceHatDirection_15; }
	inline void set_sourceHatDirection_15(int32_t value)
	{
		___sourceHatDirection_15 = value;
	}

	inline static int32_t get_offset_of_sourceHatRange_16() { return static_cast<int32_t>(offsetof(Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312, ___sourceHatRange_16)); }
	inline int32_t get_sourceHatRange_16() const { return ___sourceHatRange_16; }
	inline int32_t* get_address_of_sourceHatRange_16() { return &___sourceHatRange_16; }
	inline void set_sourceHatRange_16(int32_t value)
	{
		___sourceHatRange_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T1177CDA7B0F08C57CA4568A5D34477A23B442312_H
#ifndef BUTTON_TE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF_H
#define BUTTON_TE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button
struct  Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF  : public Element_tED0CF25F632E14F6E9BA2C7306E99D690D242165
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::elementIdentifier
	int32_t ___elementIdentifier_0;
	// Rewired.Data.Mapping.HardwareElementSourceTypeWithHat Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::sourceType
	int32_t ___sourceType_1;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::sourceButton
	int32_t ___sourceButton_2;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::sourceAxis
	int32_t ___sourceAxis_3;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::sourceAxisPole
	int32_t ___sourceAxisPole_4;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::axisDeadZone
	float ___axisDeadZone_5;
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::sourceHat
	int32_t ___sourceHat_6;
	// Rewired.Data.Mapping.HatType Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::sourceHatType
	int32_t ___sourceHatType_7;
	// Rewired.Data.Mapping.HatDirection Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::sourceHatDirection
	int32_t ___sourceHatDirection_8;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::requireMultipleButtons
	bool ___requireMultipleButtons_9;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::requiredButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___requiredButtons_10;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::ignoreIfButtonsActive
	bool ___ignoreIfButtonsActive_11;
	// System.Int32[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::ignoreIfButtonsActiveButtons
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ignoreIfButtonsActiveButtons_12;
	// Rewired.Data.Mapping.HardwareButtonInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_WindowsUWP_Base_Button::buttonInfo
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * ___buttonInfo_13;

public:
	inline static int32_t get_offset_of_elementIdentifier_0() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___elementIdentifier_0)); }
	inline int32_t get_elementIdentifier_0() const { return ___elementIdentifier_0; }
	inline int32_t* get_address_of_elementIdentifier_0() { return &___elementIdentifier_0; }
	inline void set_elementIdentifier_0(int32_t value)
	{
		___elementIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_sourceType_1() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___sourceType_1)); }
	inline int32_t get_sourceType_1() const { return ___sourceType_1; }
	inline int32_t* get_address_of_sourceType_1() { return &___sourceType_1; }
	inline void set_sourceType_1(int32_t value)
	{
		___sourceType_1 = value;
	}

	inline static int32_t get_offset_of_sourceButton_2() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___sourceButton_2)); }
	inline int32_t get_sourceButton_2() const { return ___sourceButton_2; }
	inline int32_t* get_address_of_sourceButton_2() { return &___sourceButton_2; }
	inline void set_sourceButton_2(int32_t value)
	{
		___sourceButton_2 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_3() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___sourceAxis_3)); }
	inline int32_t get_sourceAxis_3() const { return ___sourceAxis_3; }
	inline int32_t* get_address_of_sourceAxis_3() { return &___sourceAxis_3; }
	inline void set_sourceAxis_3(int32_t value)
	{
		___sourceAxis_3 = value;
	}

	inline static int32_t get_offset_of_sourceAxisPole_4() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___sourceAxisPole_4)); }
	inline int32_t get_sourceAxisPole_4() const { return ___sourceAxisPole_4; }
	inline int32_t* get_address_of_sourceAxisPole_4() { return &___sourceAxisPole_4; }
	inline void set_sourceAxisPole_4(int32_t value)
	{
		___sourceAxisPole_4 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_5() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___axisDeadZone_5)); }
	inline float get_axisDeadZone_5() const { return ___axisDeadZone_5; }
	inline float* get_address_of_axisDeadZone_5() { return &___axisDeadZone_5; }
	inline void set_axisDeadZone_5(float value)
	{
		___axisDeadZone_5 = value;
	}

	inline static int32_t get_offset_of_sourceHat_6() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___sourceHat_6)); }
	inline int32_t get_sourceHat_6() const { return ___sourceHat_6; }
	inline int32_t* get_address_of_sourceHat_6() { return &___sourceHat_6; }
	inline void set_sourceHat_6(int32_t value)
	{
		___sourceHat_6 = value;
	}

	inline static int32_t get_offset_of_sourceHatType_7() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___sourceHatType_7)); }
	inline int32_t get_sourceHatType_7() const { return ___sourceHatType_7; }
	inline int32_t* get_address_of_sourceHatType_7() { return &___sourceHatType_7; }
	inline void set_sourceHatType_7(int32_t value)
	{
		___sourceHatType_7 = value;
	}

	inline static int32_t get_offset_of_sourceHatDirection_8() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___sourceHatDirection_8)); }
	inline int32_t get_sourceHatDirection_8() const { return ___sourceHatDirection_8; }
	inline int32_t* get_address_of_sourceHatDirection_8() { return &___sourceHatDirection_8; }
	inline void set_sourceHatDirection_8(int32_t value)
	{
		___sourceHatDirection_8 = value;
	}

	inline static int32_t get_offset_of_requireMultipleButtons_9() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___requireMultipleButtons_9)); }
	inline bool get_requireMultipleButtons_9() const { return ___requireMultipleButtons_9; }
	inline bool* get_address_of_requireMultipleButtons_9() { return &___requireMultipleButtons_9; }
	inline void set_requireMultipleButtons_9(bool value)
	{
		___requireMultipleButtons_9 = value;
	}

	inline static int32_t get_offset_of_requiredButtons_10() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___requiredButtons_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_requiredButtons_10() const { return ___requiredButtons_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_requiredButtons_10() { return &___requiredButtons_10; }
	inline void set_requiredButtons_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___requiredButtons_10 = value;
		Il2CppCodeGenWriteBarrier((&___requiredButtons_10), value);
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActive_11() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___ignoreIfButtonsActive_11)); }
	inline bool get_ignoreIfButtonsActive_11() const { return ___ignoreIfButtonsActive_11; }
	inline bool* get_address_of_ignoreIfButtonsActive_11() { return &___ignoreIfButtonsActive_11; }
	inline void set_ignoreIfButtonsActive_11(bool value)
	{
		___ignoreIfButtonsActive_11 = value;
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActiveButtons_12() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___ignoreIfButtonsActiveButtons_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ignoreIfButtonsActiveButtons_12() const { return ___ignoreIfButtonsActiveButtons_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ignoreIfButtonsActiveButtons_12() { return &___ignoreIfButtonsActiveButtons_12; }
	inline void set_ignoreIfButtonsActiveButtons_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ignoreIfButtonsActiveButtons_12 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreIfButtonsActiveButtons_12), value);
	}

	inline static int32_t get_offset_of_buttonInfo_13() { return static_cast<int32_t>(offsetof(Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF, ___buttonInfo_13)); }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * get_buttonInfo_13() const { return ___buttonInfo_13; }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 ** get_address_of_buttonInfo_13() { return &___buttonInfo_13; }
	inline void set_buttonInfo_13(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * value)
	{
		___buttonInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___buttonInfo_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_TE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF_H
#ifndef ELEMENT_T14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF_H
#define ELEMENT_T14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Element
struct  Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Element::elementIdentifier
	int32_t ___elementIdentifier_0;
	// Rewired.Data.Mapping.HardwareElementSourceType Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Element::sourceType
	int32_t ___sourceType_1;
	// Rewired.Platforms.XInputButton Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Element::sourceButton
	int32_t ___sourceButton_2;
	// Rewired.Platforms.XInputAxis Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Element::sourceAxis
	int32_t ___sourceAxis_3;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Element::axisDeadZone
	float ___axisDeadZone_4;

public:
	inline static int32_t get_offset_of_elementIdentifier_0() { return static_cast<int32_t>(offsetof(Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF, ___elementIdentifier_0)); }
	inline int32_t get_elementIdentifier_0() const { return ___elementIdentifier_0; }
	inline int32_t* get_address_of_elementIdentifier_0() { return &___elementIdentifier_0; }
	inline void set_elementIdentifier_0(int32_t value)
	{
		___elementIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_sourceType_1() { return static_cast<int32_t>(offsetof(Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF, ___sourceType_1)); }
	inline int32_t get_sourceType_1() const { return ___sourceType_1; }
	inline int32_t* get_address_of_sourceType_1() { return &___sourceType_1; }
	inline void set_sourceType_1(int32_t value)
	{
		___sourceType_1 = value;
	}

	inline static int32_t get_offset_of_sourceButton_2() { return static_cast<int32_t>(offsetof(Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF, ___sourceButton_2)); }
	inline int32_t get_sourceButton_2() const { return ___sourceButton_2; }
	inline int32_t* get_address_of_sourceButton_2() { return &___sourceButton_2; }
	inline void set_sourceButton_2(int32_t value)
	{
		___sourceButton_2 = value;
	}

	inline static int32_t get_offset_of_sourceAxis_3() { return static_cast<int32_t>(offsetof(Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF, ___sourceAxis_3)); }
	inline int32_t get_sourceAxis_3() const { return ___sourceAxis_3; }
	inline int32_t* get_address_of_sourceAxis_3() { return &___sourceAxis_3; }
	inline void set_sourceAxis_3(int32_t value)
	{
		___sourceAxis_3 = value;
	}

	inline static int32_t get_offset_of_axisDeadZone_4() { return static_cast<int32_t>(offsetof(Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF, ___axisDeadZone_4)); }
	inline float get_axisDeadZone_4() const { return ___axisDeadZone_4; }
	inline float* get_address_of_axisDeadZone_4() { return &___axisDeadZone_4; }
	inline void set_axisDeadZone_4(float value)
	{
		___axisDeadZone_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF_H
#ifndef PLATFORM_XBOXONE_TF5ACC52C21E7D086DBC6B842E36075C1582AA2C4_H
#define PLATFORM_XBOXONE_TF5ACC52C21E7D086DBC6B842E36075C1582AA2C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne
struct  Platform_XboxOne_tF5ACC52C21E7D086DBC6B842E36075C1582AA2C4  : public Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479
{
public:
	// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne::variants
	Platform_XboxOne_BaseU5BU5D_tAD510FA6BA296CB92BD20B7F4AED507764D5CE89* ___variants_5;

public:
	inline static int32_t get_offset_of_variants_5() { return static_cast<int32_t>(offsetof(Platform_XboxOne_tF5ACC52C21E7D086DBC6B842E36075C1582AA2C4, ___variants_5)); }
	inline Platform_XboxOne_BaseU5BU5D_tAD510FA6BA296CB92BD20B7F4AED507764D5CE89* get_variants_5() const { return ___variants_5; }
	inline Platform_XboxOne_BaseU5BU5D_tAD510FA6BA296CB92BD20B7F4AED507764D5CE89** get_address_of_variants_5() { return &___variants_5; }
	inline void set_variants_5(Platform_XboxOne_BaseU5BU5D_tAD510FA6BA296CB92BD20B7F4AED507764D5CE89* value)
	{
		___variants_5 = value;
		Il2CppCodeGenWriteBarrier((&___variants_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_XBOXONE_TF5ACC52C21E7D086DBC6B842E36075C1582AA2C4_H
#ifndef AXIS_TAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC_H
#define AXIS_TAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Axis
struct  Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC  : public Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Axis::invert
	bool ___invert_8;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Axis::sourceAxisRange
	int32_t ___sourceAxisRange_9;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Axis::buttonAxisContribution
	int32_t ___buttonAxisContribution_10;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Axis::calibrateAxis
	bool ___calibrateAxis_11;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Axis::axisZero
	float ___axisZero_12;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Axis::axisMin
	float ___axisMin_13;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Axis::axisMax
	float ___axisMax_14;
	// Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Axis::alternateCalibrations
	AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* ___alternateCalibrations_15;
	// Rewired.Data.Mapping.HardwareAxisInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Axis::axisInfo
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * ___axisInfo_16;

public:
	inline static int32_t get_offset_of_invert_8() { return static_cast<int32_t>(offsetof(Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC, ___invert_8)); }
	inline bool get_invert_8() const { return ___invert_8; }
	inline bool* get_address_of_invert_8() { return &___invert_8; }
	inline void set_invert_8(bool value)
	{
		___invert_8 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_9() { return static_cast<int32_t>(offsetof(Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC, ___sourceAxisRange_9)); }
	inline int32_t get_sourceAxisRange_9() const { return ___sourceAxisRange_9; }
	inline int32_t* get_address_of_sourceAxisRange_9() { return &___sourceAxisRange_9; }
	inline void set_sourceAxisRange_9(int32_t value)
	{
		___sourceAxisRange_9 = value;
	}

	inline static int32_t get_offset_of_buttonAxisContribution_10() { return static_cast<int32_t>(offsetof(Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC, ___buttonAxisContribution_10)); }
	inline int32_t get_buttonAxisContribution_10() const { return ___buttonAxisContribution_10; }
	inline int32_t* get_address_of_buttonAxisContribution_10() { return &___buttonAxisContribution_10; }
	inline void set_buttonAxisContribution_10(int32_t value)
	{
		___buttonAxisContribution_10 = value;
	}

	inline static int32_t get_offset_of_calibrateAxis_11() { return static_cast<int32_t>(offsetof(Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC, ___calibrateAxis_11)); }
	inline bool get_calibrateAxis_11() const { return ___calibrateAxis_11; }
	inline bool* get_address_of_calibrateAxis_11() { return &___calibrateAxis_11; }
	inline void set_calibrateAxis_11(bool value)
	{
		___calibrateAxis_11 = value;
	}

	inline static int32_t get_offset_of_axisZero_12() { return static_cast<int32_t>(offsetof(Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC, ___axisZero_12)); }
	inline float get_axisZero_12() const { return ___axisZero_12; }
	inline float* get_address_of_axisZero_12() { return &___axisZero_12; }
	inline void set_axisZero_12(float value)
	{
		___axisZero_12 = value;
	}

	inline static int32_t get_offset_of_axisMin_13() { return static_cast<int32_t>(offsetof(Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC, ___axisMin_13)); }
	inline float get_axisMin_13() const { return ___axisMin_13; }
	inline float* get_address_of_axisMin_13() { return &___axisMin_13; }
	inline void set_axisMin_13(float value)
	{
		___axisMin_13 = value;
	}

	inline static int32_t get_offset_of_axisMax_14() { return static_cast<int32_t>(offsetof(Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC, ___axisMax_14)); }
	inline float get_axisMax_14() const { return ___axisMax_14; }
	inline float* get_address_of_axisMax_14() { return &___axisMax_14; }
	inline void set_axisMax_14(float value)
	{
		___axisMax_14 = value;
	}

	inline static int32_t get_offset_of_alternateCalibrations_15() { return static_cast<int32_t>(offsetof(Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC, ___alternateCalibrations_15)); }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* get_alternateCalibrations_15() const { return ___alternateCalibrations_15; }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7** get_address_of_alternateCalibrations_15() { return &___alternateCalibrations_15; }
	inline void set_alternateCalibrations_15(AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* value)
	{
		___alternateCalibrations_15 = value;
		Il2CppCodeGenWriteBarrier((&___alternateCalibrations_15), value);
	}

	inline static int32_t get_offset_of_axisInfo_16() { return static_cast<int32_t>(offsetof(Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC, ___axisInfo_16)); }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * get_axisInfo_16() const { return ___axisInfo_16; }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 ** get_address_of_axisInfo_16() { return &___axisInfo_16; }
	inline void set_axisInfo_16(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * value)
	{
		___axisInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___axisInfo_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_TAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC_H
#ifndef BUTTON_TF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A_H
#define BUTTON_TF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button
struct  Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A  : public Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E
{
public:
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::sourceAxisPole
	int32_t ___sourceAxisPole_8;
	// Rewired.Platforms.UnityAxis Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::unityHat_sourceAxis1
	int32_t ___unityHat_sourceAxis1_9;
	// Rewired.Platforms.UnityAxis Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::unityHat_sourceAxis2
	int32_t ___unityHat_sourceAxis2_10;
	// UnityEngine.Vector2 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::unityHat_isActiveAxisValues1
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___unityHat_isActiveAxisValues1_11;
	// UnityEngine.Vector2 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::unityHat_isActiveAxisValues2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___unityHat_isActiveAxisValues2_12;
	// UnityEngine.Vector2 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::unityHat_isActiveAxisValues3
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___unityHat_isActiveAxisValues3_13;
	// UnityEngine.Vector2 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::unityHat_zeroValues
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___unityHat_zeroValues_14;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::unityHat_checkNeverPressed
	bool ___unityHat_checkNeverPressed_15;
	// UnityEngine.Vector2 Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::unityHat_neverPressedZeroValues
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___unityHat_neverPressedZeroValues_16;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::requireMultipleButtons
	bool ___requireMultipleButtons_17;
	// Rewired.Platforms.UnityButton[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::requiredButtons
	UnityButtonU5BU5D_t24D74C17E7990493573E9CF978882B03D3BA38DB* ___requiredButtons_18;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::ignoreIfButtonsActive
	bool ___ignoreIfButtonsActive_19;
	// Rewired.Platforms.UnityButton[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::ignoreIfButtonsActiveButtons
	UnityButtonU5BU5D_t24D74C17E7990493573E9CF978882B03D3BA38DB* ___ignoreIfButtonsActiveButtons_20;
	// Rewired.Data.Mapping.HardwareButtonInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_Fallback_Base_Button::buttonInfo
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * ___buttonInfo_21;

public:
	inline static int32_t get_offset_of_sourceAxisPole_8() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___sourceAxisPole_8)); }
	inline int32_t get_sourceAxisPole_8() const { return ___sourceAxisPole_8; }
	inline int32_t* get_address_of_sourceAxisPole_8() { return &___sourceAxisPole_8; }
	inline void set_sourceAxisPole_8(int32_t value)
	{
		___sourceAxisPole_8 = value;
	}

	inline static int32_t get_offset_of_unityHat_sourceAxis1_9() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___unityHat_sourceAxis1_9)); }
	inline int32_t get_unityHat_sourceAxis1_9() const { return ___unityHat_sourceAxis1_9; }
	inline int32_t* get_address_of_unityHat_sourceAxis1_9() { return &___unityHat_sourceAxis1_9; }
	inline void set_unityHat_sourceAxis1_9(int32_t value)
	{
		___unityHat_sourceAxis1_9 = value;
	}

	inline static int32_t get_offset_of_unityHat_sourceAxis2_10() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___unityHat_sourceAxis2_10)); }
	inline int32_t get_unityHat_sourceAxis2_10() const { return ___unityHat_sourceAxis2_10; }
	inline int32_t* get_address_of_unityHat_sourceAxis2_10() { return &___unityHat_sourceAxis2_10; }
	inline void set_unityHat_sourceAxis2_10(int32_t value)
	{
		___unityHat_sourceAxis2_10 = value;
	}

	inline static int32_t get_offset_of_unityHat_isActiveAxisValues1_11() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___unityHat_isActiveAxisValues1_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_unityHat_isActiveAxisValues1_11() const { return ___unityHat_isActiveAxisValues1_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_unityHat_isActiveAxisValues1_11() { return &___unityHat_isActiveAxisValues1_11; }
	inline void set_unityHat_isActiveAxisValues1_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___unityHat_isActiveAxisValues1_11 = value;
	}

	inline static int32_t get_offset_of_unityHat_isActiveAxisValues2_12() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___unityHat_isActiveAxisValues2_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_unityHat_isActiveAxisValues2_12() const { return ___unityHat_isActiveAxisValues2_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_unityHat_isActiveAxisValues2_12() { return &___unityHat_isActiveAxisValues2_12; }
	inline void set_unityHat_isActiveAxisValues2_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___unityHat_isActiveAxisValues2_12 = value;
	}

	inline static int32_t get_offset_of_unityHat_isActiveAxisValues3_13() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___unityHat_isActiveAxisValues3_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_unityHat_isActiveAxisValues3_13() const { return ___unityHat_isActiveAxisValues3_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_unityHat_isActiveAxisValues3_13() { return &___unityHat_isActiveAxisValues3_13; }
	inline void set_unityHat_isActiveAxisValues3_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___unityHat_isActiveAxisValues3_13 = value;
	}

	inline static int32_t get_offset_of_unityHat_zeroValues_14() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___unityHat_zeroValues_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_unityHat_zeroValues_14() const { return ___unityHat_zeroValues_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_unityHat_zeroValues_14() { return &___unityHat_zeroValues_14; }
	inline void set_unityHat_zeroValues_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___unityHat_zeroValues_14 = value;
	}

	inline static int32_t get_offset_of_unityHat_checkNeverPressed_15() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___unityHat_checkNeverPressed_15)); }
	inline bool get_unityHat_checkNeverPressed_15() const { return ___unityHat_checkNeverPressed_15; }
	inline bool* get_address_of_unityHat_checkNeverPressed_15() { return &___unityHat_checkNeverPressed_15; }
	inline void set_unityHat_checkNeverPressed_15(bool value)
	{
		___unityHat_checkNeverPressed_15 = value;
	}

	inline static int32_t get_offset_of_unityHat_neverPressedZeroValues_16() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___unityHat_neverPressedZeroValues_16)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_unityHat_neverPressedZeroValues_16() const { return ___unityHat_neverPressedZeroValues_16; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_unityHat_neverPressedZeroValues_16() { return &___unityHat_neverPressedZeroValues_16; }
	inline void set_unityHat_neverPressedZeroValues_16(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___unityHat_neverPressedZeroValues_16 = value;
	}

	inline static int32_t get_offset_of_requireMultipleButtons_17() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___requireMultipleButtons_17)); }
	inline bool get_requireMultipleButtons_17() const { return ___requireMultipleButtons_17; }
	inline bool* get_address_of_requireMultipleButtons_17() { return &___requireMultipleButtons_17; }
	inline void set_requireMultipleButtons_17(bool value)
	{
		___requireMultipleButtons_17 = value;
	}

	inline static int32_t get_offset_of_requiredButtons_18() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___requiredButtons_18)); }
	inline UnityButtonU5BU5D_t24D74C17E7990493573E9CF978882B03D3BA38DB* get_requiredButtons_18() const { return ___requiredButtons_18; }
	inline UnityButtonU5BU5D_t24D74C17E7990493573E9CF978882B03D3BA38DB** get_address_of_requiredButtons_18() { return &___requiredButtons_18; }
	inline void set_requiredButtons_18(UnityButtonU5BU5D_t24D74C17E7990493573E9CF978882B03D3BA38DB* value)
	{
		___requiredButtons_18 = value;
		Il2CppCodeGenWriteBarrier((&___requiredButtons_18), value);
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActive_19() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___ignoreIfButtonsActive_19)); }
	inline bool get_ignoreIfButtonsActive_19() const { return ___ignoreIfButtonsActive_19; }
	inline bool* get_address_of_ignoreIfButtonsActive_19() { return &___ignoreIfButtonsActive_19; }
	inline void set_ignoreIfButtonsActive_19(bool value)
	{
		___ignoreIfButtonsActive_19 = value;
	}

	inline static int32_t get_offset_of_ignoreIfButtonsActiveButtons_20() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___ignoreIfButtonsActiveButtons_20)); }
	inline UnityButtonU5BU5D_t24D74C17E7990493573E9CF978882B03D3BA38DB* get_ignoreIfButtonsActiveButtons_20() const { return ___ignoreIfButtonsActiveButtons_20; }
	inline UnityButtonU5BU5D_t24D74C17E7990493573E9CF978882B03D3BA38DB** get_address_of_ignoreIfButtonsActiveButtons_20() { return &___ignoreIfButtonsActiveButtons_20; }
	inline void set_ignoreIfButtonsActiveButtons_20(UnityButtonU5BU5D_t24D74C17E7990493573E9CF978882B03D3BA38DB* value)
	{
		___ignoreIfButtonsActiveButtons_20 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreIfButtonsActiveButtons_20), value);
	}

	inline static int32_t get_offset_of_buttonInfo_21() { return static_cast<int32_t>(offsetof(Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A, ___buttonInfo_21)); }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * get_buttonInfo_21() const { return ___buttonInfo_21; }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 ** get_address_of_buttonInfo_21() { return &___buttonInfo_21; }
	inline void set_buttonInfo_21(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * value)
	{
		___buttonInfo_21 = value;
		Il2CppCodeGenWriteBarrier((&___buttonInfo_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_TF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A_H
#ifndef AXIS_T1C5F76253ECE39C5FB44D7E080FE2B8F91EA3B23_H
#define AXIS_T1C5F76253ECE39C5FB44D7E080FE2B8F91EA3B23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_Axis
struct  Axis_t1C5F76253ECE39C5FB44D7E080FE2B8F91EA3B23  : public Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T1C5F76253ECE39C5FB44D7E080FE2B8F91EA3B23_H
#ifndef BUTTON_T307B211449A4FDF5CAC5534717DB093734171F6E_H
#define BUTTON_T307B211449A4FDF5CAC5534717DB093734171F6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_NintendoSwitch_Base_Button
struct  Button_t307B211449A4FDF5CAC5534717DB093734171F6E  : public Button_tBB6149DD7A45B873206F15797DC2B002019BF09C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T307B211449A4FDF5CAC5534717DB093734171F6E_H
#ifndef AXIS_T19DD9B9FC74FD35EFE5B6EB3DDC76CEB8E6221EA_H
#define AXIS_T19DD9B9FC74FD35EFE5B6EB3DDC76CEB8E6221EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_Axis
struct  Axis_t19DD9B9FC74FD35EFE5B6EB3DDC76CEB8E6221EA  : public Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T19DD9B9FC74FD35EFE5B6EB3DDC76CEB8E6221EA_H
#ifndef BUTTON_TD29D85D68B3EB113ACAD47AAE9E25DB08DC5E8B3_H
#define BUTTON_TD29D85D68B3EB113ACAD47AAE9E25DB08DC5E8B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_Ouya_Base_Button
struct  Button_tD29D85D68B3EB113ACAD47AAE9E25DB08DC5E8B3  : public Button_tBB6149DD7A45B873206F15797DC2B002019BF09C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_TD29D85D68B3EB113ACAD47AAE9E25DB08DC5E8B3_H
#ifndef AXIS_T972C4630BDCBD8D4C00E4BF2E5DFD9F812447C87_H
#define AXIS_T972C4630BDCBD8D4C00E4BF2E5DFD9F812447C87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_Axis
struct  Axis_t972C4630BDCBD8D4C00E4BF2E5DFD9F812447C87  : public Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T972C4630BDCBD8D4C00E4BF2E5DFD9F812447C87_H
#ifndef BUTTON_T67E3C3174F037E7E08D5B053F1E30BDA666B914D_H
#define BUTTON_T67E3C3174F037E7E08D5B053F1E30BDA666B914D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_PS4_Base_Button
struct  Button_t67E3C3174F037E7E08D5B053F1E30BDA666B914D  : public Button_tBB6149DD7A45B873206F15797DC2B002019BF09C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T67E3C3174F037E7E08D5B053F1E30BDA666B914D_H
#ifndef AXIS_T9DA6DEF55593D1749F4194C265549E472CF58AC7_H
#define AXIS_T9DA6DEF55593D1749F4194C265549E472CF58AC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Axis
struct  Axis_t9DA6DEF55593D1749F4194C265549E472CF58AC7  : public Axis_Base_tDCD5E6CC6636C4ECA93D8DDE3302423259E63623
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Axis::sourceOtherAxis
	int32_t ___sourceOtherAxis_19;

public:
	inline static int32_t get_offset_of_sourceOtherAxis_19() { return static_cast<int32_t>(offsetof(Axis_t9DA6DEF55593D1749F4194C265549E472CF58AC7, ___sourceOtherAxis_19)); }
	inline int32_t get_sourceOtherAxis_19() const { return ___sourceOtherAxis_19; }
	inline int32_t* get_address_of_sourceOtherAxis_19() { return &___sourceOtherAxis_19; }
	inline void set_sourceOtherAxis_19(int32_t value)
	{
		___sourceOtherAxis_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T9DA6DEF55593D1749F4194C265549E472CF58AC7_H
#ifndef BUTTON_TF588F3091480DBDA3900D7F723740C38A185F387_H
#define BUTTON_TF588F3091480DBDA3900D7F723740C38A185F387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Button
struct  Button_tF588F3091480DBDA3900D7F723740C38A185F387  : public Button_Base_tCA15C13FD1596A13A31E6ECCD63A077476969024
{
public:
	// System.Int32 Rewired.Data.Mapping.HardwareJoystickMap_Platform_RawInput_Base_Button::sourceOtherAxis
	int32_t ___sourceOtherAxis_16;

public:
	inline static int32_t get_offset_of_sourceOtherAxis_16() { return static_cast<int32_t>(offsetof(Button_tF588F3091480DBDA3900D7F723740C38A185F387, ___sourceOtherAxis_16)); }
	inline int32_t get_sourceOtherAxis_16() const { return ___sourceOtherAxis_16; }
	inline int32_t* get_address_of_sourceOtherAxis_16() { return &___sourceOtherAxis_16; }
	inline void set_sourceOtherAxis_16(int32_t value)
	{
		___sourceOtherAxis_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_TF588F3091480DBDA3900D7F723740C38A185F387_H
#ifndef AXIS_T7E214BC82D06391F36479E7166FF0CB1D9518D44_H
#define AXIS_T7E214BC82D06391F36479E7166FF0CB1D9518D44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Axis
struct  Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44  : public Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF
{
public:
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Axis::invert
	bool ___invert_5;
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Axis::buttonAxisContribution
	int32_t ___buttonAxisContribution_6;
	// Rewired.AxisRange Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Axis::sourceAxisRange
	int32_t ___sourceAxisRange_7;
	// System.Boolean Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Axis::calibrateAxis
	bool ___calibrateAxis_8;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Axis::axisZero
	float ___axisZero_9;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Axis::axisMin
	float ___axisMin_10;
	// System.Single Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Axis::axisMax
	float ___axisMax_11;
	// Rewired.Data.Mapping.HardwareJoystickMap_AxisCalibrationInfoEntry[] Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Axis::alternateCalibrations
	AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* ___alternateCalibrations_12;
	// Rewired.Data.Mapping.HardwareAxisInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Axis::axisInfo
	HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * ___axisInfo_13;

public:
	inline static int32_t get_offset_of_invert_5() { return static_cast<int32_t>(offsetof(Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44, ___invert_5)); }
	inline bool get_invert_5() const { return ___invert_5; }
	inline bool* get_address_of_invert_5() { return &___invert_5; }
	inline void set_invert_5(bool value)
	{
		___invert_5 = value;
	}

	inline static int32_t get_offset_of_buttonAxisContribution_6() { return static_cast<int32_t>(offsetof(Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44, ___buttonAxisContribution_6)); }
	inline int32_t get_buttonAxisContribution_6() const { return ___buttonAxisContribution_6; }
	inline int32_t* get_address_of_buttonAxisContribution_6() { return &___buttonAxisContribution_6; }
	inline void set_buttonAxisContribution_6(int32_t value)
	{
		___buttonAxisContribution_6 = value;
	}

	inline static int32_t get_offset_of_sourceAxisRange_7() { return static_cast<int32_t>(offsetof(Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44, ___sourceAxisRange_7)); }
	inline int32_t get_sourceAxisRange_7() const { return ___sourceAxisRange_7; }
	inline int32_t* get_address_of_sourceAxisRange_7() { return &___sourceAxisRange_7; }
	inline void set_sourceAxisRange_7(int32_t value)
	{
		___sourceAxisRange_7 = value;
	}

	inline static int32_t get_offset_of_calibrateAxis_8() { return static_cast<int32_t>(offsetof(Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44, ___calibrateAxis_8)); }
	inline bool get_calibrateAxis_8() const { return ___calibrateAxis_8; }
	inline bool* get_address_of_calibrateAxis_8() { return &___calibrateAxis_8; }
	inline void set_calibrateAxis_8(bool value)
	{
		___calibrateAxis_8 = value;
	}

	inline static int32_t get_offset_of_axisZero_9() { return static_cast<int32_t>(offsetof(Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44, ___axisZero_9)); }
	inline float get_axisZero_9() const { return ___axisZero_9; }
	inline float* get_address_of_axisZero_9() { return &___axisZero_9; }
	inline void set_axisZero_9(float value)
	{
		___axisZero_9 = value;
	}

	inline static int32_t get_offset_of_axisMin_10() { return static_cast<int32_t>(offsetof(Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44, ___axisMin_10)); }
	inline float get_axisMin_10() const { return ___axisMin_10; }
	inline float* get_address_of_axisMin_10() { return &___axisMin_10; }
	inline void set_axisMin_10(float value)
	{
		___axisMin_10 = value;
	}

	inline static int32_t get_offset_of_axisMax_11() { return static_cast<int32_t>(offsetof(Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44, ___axisMax_11)); }
	inline float get_axisMax_11() const { return ___axisMax_11; }
	inline float* get_address_of_axisMax_11() { return &___axisMax_11; }
	inline void set_axisMax_11(float value)
	{
		___axisMax_11 = value;
	}

	inline static int32_t get_offset_of_alternateCalibrations_12() { return static_cast<int32_t>(offsetof(Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44, ___alternateCalibrations_12)); }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* get_alternateCalibrations_12() const { return ___alternateCalibrations_12; }
	inline AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7** get_address_of_alternateCalibrations_12() { return &___alternateCalibrations_12; }
	inline void set_alternateCalibrations_12(AxisCalibrationInfoEntryU5BU5D_t1D51402FE73B7A0DB57FF626C02DA2281FAEEFC7* value)
	{
		___alternateCalibrations_12 = value;
		Il2CppCodeGenWriteBarrier((&___alternateCalibrations_12), value);
	}

	inline static int32_t get_offset_of_axisInfo_13() { return static_cast<int32_t>(offsetof(Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44, ___axisInfo_13)); }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * get_axisInfo_13() const { return ___axisInfo_13; }
	inline HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 ** get_address_of_axisInfo_13() { return &___axisInfo_13; }
	inline void set_axisInfo_13(HardwareAxisInfo_tABFC6F993A6738A4283168A404AB2C5F413341F4 * value)
	{
		___axisInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___axisInfo_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T7E214BC82D06391F36479E7166FF0CB1D9518D44_H
#ifndef BUTTON_TF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7_H
#define BUTTON_TF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Button
struct  Button_tF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7  : public Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF
{
public:
	// Rewired.Pole Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Button::sourceAxisPole
	int32_t ___sourceAxisPole_5;
	// Rewired.Data.Mapping.HardwareButtonInfo Rewired.Data.Mapping.HardwareJoystickMap_Platform_XInput_Base_Button::buttonInfo
	HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * ___buttonInfo_6;

public:
	inline static int32_t get_offset_of_sourceAxisPole_5() { return static_cast<int32_t>(offsetof(Button_tF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7, ___sourceAxisPole_5)); }
	inline int32_t get_sourceAxisPole_5() const { return ___sourceAxisPole_5; }
	inline int32_t* get_address_of_sourceAxisPole_5() { return &___sourceAxisPole_5; }
	inline void set_sourceAxisPole_5(int32_t value)
	{
		___sourceAxisPole_5 = value;
	}

	inline static int32_t get_offset_of_buttonInfo_6() { return static_cast<int32_t>(offsetof(Button_tF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7, ___buttonInfo_6)); }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * get_buttonInfo_6() const { return ___buttonInfo_6; }
	inline HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 ** get_address_of_buttonInfo_6() { return &___buttonInfo_6; }
	inline void set_buttonInfo_6(HardwareButtonInfo_t9C07E8DF615040EE25315B98F1D28E5D08224273 * value)
	{
		___buttonInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___buttonInfo_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_TF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7_H
#ifndef AXIS_TB71D44914DCD3FECE37D201EA1FF52A497A41F05_H
#define AXIS_TB71D44914DCD3FECE37D201EA1FF52A497A41F05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_Axis
struct  Axis_tB71D44914DCD3FECE37D201EA1FF52A497A41F05  : public Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_TB71D44914DCD3FECE37D201EA1FF52A497A41F05_H
#ifndef BUTTON_T51AA16D2A4277E09E1154ACC2804E5ABC30AE26E_H
#define BUTTON_T51AA16D2A4277E09E1154ACC2804E5ABC30AE26E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Data.Mapping.HardwareJoystickMap_Platform_XboxOne_Base_Button
struct  Button_t51AA16D2A4277E09E1154ACC2804E5ABC30AE26E  : public Button_tBB6149DD7A45B873206F15797DC2B002019BF09C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T51AA16D2A4277E09E1154ACC2804E5ABC30AE26E_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4200 = { sizeof (Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4200[2] = 
{
	Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248::get_offset_of_axes_0(),
	Elements_tCF50568BAB138B70AC7D318DB26F28D8AD8C2248::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4201 = { sizeof (FSmGhsedzRlfJDUxaERkIydwEhNP_t802E03EBF9E138F1CB35CE01E014058519FA7223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4201[5] = 
{
	FSmGhsedzRlfJDUxaERkIydwEhNP_t802E03EBF9E138F1CB35CE01E014058519FA7223::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	FSmGhsedzRlfJDUxaERkIydwEhNP_t802E03EBF9E138F1CB35CE01E014058519FA7223::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	FSmGhsedzRlfJDUxaERkIydwEhNP_t802E03EBF9E138F1CB35CE01E014058519FA7223::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	FSmGhsedzRlfJDUxaERkIydwEhNP_t802E03EBF9E138F1CB35CE01E014058519FA7223::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	FSmGhsedzRlfJDUxaERkIydwEhNP_t802E03EBF9E138F1CB35CE01E014058519FA7223::get_offset_of_vpBmBZQKTtXaqRCsqdgzeKCwohQk_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4202 = { sizeof (AcvlMOggldKoUpDTQpCQxIVQgdM_t1E02138B73D8E7AD4E096B0516E972034C3FF6CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4202[5] = 
{
	AcvlMOggldKoUpDTQpCQxIVQgdM_t1E02138B73D8E7AD4E096B0516E972034C3FF6CA::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	AcvlMOggldKoUpDTQpCQxIVQgdM_t1E02138B73D8E7AD4E096B0516E972034C3FF6CA::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	AcvlMOggldKoUpDTQpCQxIVQgdM_t1E02138B73D8E7AD4E096B0516E972034C3FF6CA::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	AcvlMOggldKoUpDTQpCQxIVQgdM_t1E02138B73D8E7AD4E096B0516E972034C3FF6CA::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	AcvlMOggldKoUpDTQpCQxIVQgdM_t1E02138B73D8E7AD4E096B0516E972034C3FF6CA::get_offset_of_oMDcJiLwPdVaAFmNNnpqnUvSUJd_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4203 = { sizeof (Button_tF588F3091480DBDA3900D7F723740C38A185F387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4203[1] = 
{
	Button_tF588F3091480DBDA3900D7F723740C38A185F387::get_offset_of_sourceOtherAxis_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4204 = { sizeof (Axis_t9DA6DEF55593D1749F4194C265549E472CF58AC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4204[1] = 
{
	Axis_t9DA6DEF55593D1749F4194C265549E472CF58AC7::get_offset_of_sourceOtherAxis_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4205 = { sizeof (IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4205[6] = 
{
	IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61::get_offset_of_BXLOLMZBogcBmMGYWQumhNAcSYm_4(),
	IPzDzkiSxPbvDVecfsEDabxIkCpc_tEDE05FB2FA843503A8F540D31F8AC8B5F9F38C61::get_offset_of_qDqFYLAqYOxxjQpeeugfBYOVEAT_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4206 = { sizeof (NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4206[6] = 
{
	NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C::get_offset_of_odjjEEjTowHPzNXuzFzehxnukcuI_4(),
	NPbxByKOXxmjhqftYGFZkikukTUH_t0500A93E6852674F8754388994800AB506033D7C::get_offset_of_yajjhtCdaFAuyeEjaMneAlOTqlgo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4207 = { sizeof (Platform_RawInput_tFA8A4734C93ADCCF4CE7C2E3947003AB875E5FBD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4207[1] = 
{
	Platform_RawInput_tFA8A4734C93ADCCF4CE7C2E3947003AB875E5FBD::get_offset_of_variants_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4208 = { sizeof (Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4208[2] = 
{
	Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374::get_offset_of_matchingCriteria_1(),
	Platform_XInput_Base_tEEAE2121567E48A96BEAAE00B6C2F3822D42E374::get_offset_of_elements_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4209 = { sizeof (MatchingCriteria_t40557EF4BBBEA0BA5E20157EB81A34802805124A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4209[1] = 
{
	MatchingCriteria_t40557EF4BBBEA0BA5E20157EB81A34802805124A::get_offset_of_subType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4210 = { sizeof (Elements_tC9F4725938858F8E86BF77F632A2F4B0B3B6B73F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4210[2] = 
{
	Elements_tC9F4725938858F8E86BF77F632A2F4B0B3B6B73F::get_offset_of_axes_0(),
	Elements_tC9F4725938858F8E86BF77F632A2F4B0B3B6B73F::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4211 = { sizeof (Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4211[5] = 
{
	Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF::get_offset_of_elementIdentifier_0(),
	Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF::get_offset_of_sourceType_1(),
	Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF::get_offset_of_sourceButton_2(),
	Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF::get_offset_of_sourceAxis_3(),
	Element_t14A13FBBFBC970FC4BCD5541A5D1615803CAFBCF::get_offset_of_axisDeadZone_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4212 = { sizeof (Button_tF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4212[2] = 
{
	Button_tF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7::get_offset_of_sourceAxisPole_5(),
	Button_tF004E0D197AD1BC6B39DB271A62A48C16C2D2BD7::get_offset_of_buttonInfo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4213 = { sizeof (Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4213[9] = 
{
	Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44::get_offset_of_invert_5(),
	Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44::get_offset_of_buttonAxisContribution_6(),
	Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44::get_offset_of_sourceAxisRange_7(),
	Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44::get_offset_of_calibrateAxis_8(),
	Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44::get_offset_of_axisZero_9(),
	Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44::get_offset_of_axisMin_10(),
	Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44::get_offset_of_axisMax_11(),
	Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44::get_offset_of_alternateCalibrations_12(),
	Axis_t7E214BC82D06391F36479E7166FF0CB1D9518D44::get_offset_of_axisInfo_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4214 = { sizeof (DkzkQUVgWEVroIiyopIDIqHOfUi_t09C2988966CB4D7A016ADE4572343C49E06183CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4214[5] = 
{
	DkzkQUVgWEVroIiyopIDIqHOfUi_t09C2988966CB4D7A016ADE4572343C49E06183CC::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	DkzkQUVgWEVroIiyopIDIqHOfUi_t09C2988966CB4D7A016ADE4572343C49E06183CC::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	DkzkQUVgWEVroIiyopIDIqHOfUi_t09C2988966CB4D7A016ADE4572343C49E06183CC::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	DkzkQUVgWEVroIiyopIDIqHOfUi_t09C2988966CB4D7A016ADE4572343C49E06183CC::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	DkzkQUVgWEVroIiyopIDIqHOfUi_t09C2988966CB4D7A016ADE4572343C49E06183CC::get_offset_of_EChnlGrdrUcyXYSPZFpLOLQYgAx_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4215 = { sizeof (oLaQcLUTwZuenKIDnDjpFwSPluoD_tB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4215[5] = 
{
	oLaQcLUTwZuenKIDnDjpFwSPluoD_tB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	oLaQcLUTwZuenKIDnDjpFwSPluoD_tB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	oLaQcLUTwZuenKIDnDjpFwSPluoD_tB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	oLaQcLUTwZuenKIDnDjpFwSPluoD_tB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	oLaQcLUTwZuenKIDnDjpFwSPluoD_tB0D188DB95F1A3A99A7171849C7D0EFF4CCBF1E4::get_offset_of_dzuPtKoIYbgBLPBoLcaghKtcmgZW_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4216 = { sizeof (Platform_XInput_t44E2E6F0FD4F4A22A7FB6C031984FFC5F82BDC16), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4216[1] = 
{
	Platform_XInput_t44E2E6F0FD4F4A22A7FB6C031984FFC5F82BDC16::get_offset_of_variants_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4217 = { sizeof (Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4217[2] = 
{
	Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072::get_offset_of_matchingCriteria_1(),
	Platform_OSX_Base_t8972FA3D2B61B0C1E30DFFB3BB6A36AB3AD58072::get_offset_of_elements_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4218 = { sizeof (MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4218[7] = 
{
	MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947::get_offset_of_hatCount_4(),
	MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947::get_offset_of_alternateElementCounts_5(),
	MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947::get_offset_of_productName_useRegex_6(),
	MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947::get_offset_of_productName_7(),
	MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947::get_offset_of_manufacturer_8(),
	MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947::get_offset_of_productId_9(),
	MatchingCriteria_t84C62377BE2DE6FD3D3182EE6BF2A9283B826947::get_offset_of_vendorId_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4219 = { sizeof (ElementCount_tC23CFCA7053807F10B101C91E01E08827BA003DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4219[1] = 
{
	ElementCount_tC23CFCA7053807F10B101C91E01E08827BA003DE::get_offset_of_hatCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4220 = { sizeof (Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4220[2] = 
{
	Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89::get_offset_of_axes_0(),
	Elements_t9717AC13D64B797504FE631C8DD1346D3698BC89::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4221 = { sizeof (HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4221[7] = 
{
	HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5::get_offset_of_fMShXwOCmAHSAzYhbRbLBQnMPaa_4(),
	HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5::get_offset_of_hHLtBCpipLanptvPCkNnpldMMFu_5(),
	HHOeBWkKgYwTkSpNVMLfdzycGjAL_t73694E47339A6446F551AA3A87937C073FD300A5::get_offset_of_kgdXDOuwBknTdVGsGtzXGYuZhje_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4222 = { sizeof (brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4222[7] = 
{
	brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3::get_offset_of_pmhJwtzrxTmDTdeZAcjZOlbVYWt_4(),
	brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3::get_offset_of_mGffXviMGlJeIhFyFdNxAzlXIpkh_5(),
	brIiqffdKsDZoMGTDDCCoajraQk_tA1F1528572935394D9B3ABD4C92EB96FCE2735E3::get_offset_of_yxxNlXTPZIjLPseZGjwaIuYuFMO_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4223 = { sizeof (Element_t909158E9B20107B6BD16CD99ECF5F01BD9C2F463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4224 = { sizeof (Button_t841D3D6DD5392867B22FA416457754550B1B8F3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4224[16] = 
{
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_elementIdentifier_0(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_sourceType_1(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_sourceButton_2(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_sourceStick_3(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_sourceAxis_4(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_sourceOtherAxis_5(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_sourceAxisPole_6(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_axisDeadZone_7(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_sourceHat_8(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_sourceHatType_9(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_sourceHatDirection_10(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_requireMultipleButtons_11(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_requiredButtons_12(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_ignoreIfButtonsActive_13(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_ignoreIfButtonsActiveButtons_14(),
	Button_t841D3D6DD5392867B22FA416457754550B1B8F3A::get_offset_of_buttonInfo_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4225 = { sizeof (Axis_t766FE92C87B9298B56A4F07611581240BD735EFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4225[19] = 
{
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_elementIdentifier_0(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_sourceType_1(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_sourceStick_2(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_sourceAxis_3(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_sourceOtherAxis_4(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_sourceAxisRange_5(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_invert_6(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_axisDeadZone_7(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_calibrateAxis_8(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_axisZero_9(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_axisMin_10(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_axisMax_11(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_alternateCalibrations_12(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_axisInfo_13(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_sourceButton_14(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_buttonAxisContribution_15(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_sourceHat_16(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_sourceHatDirection_17(),
	Axis_t766FE92C87B9298B56A4F07611581240BD735EFD::get_offset_of_sourceHatRange_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4226 = { sizeof (LaRlcglIWtIscquZqevpAiYQfXu_tDFB76EDB7B2EF3C3317892645D86C472FBF2387B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4226[5] = 
{
	LaRlcglIWtIscquZqevpAiYQfXu_tDFB76EDB7B2EF3C3317892645D86C472FBF2387B::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	LaRlcglIWtIscquZqevpAiYQfXu_tDFB76EDB7B2EF3C3317892645D86C472FBF2387B::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	LaRlcglIWtIscquZqevpAiYQfXu_tDFB76EDB7B2EF3C3317892645D86C472FBF2387B::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	LaRlcglIWtIscquZqevpAiYQfXu_tDFB76EDB7B2EF3C3317892645D86C472FBF2387B::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	LaRlcglIWtIscquZqevpAiYQfXu_tDFB76EDB7B2EF3C3317892645D86C472FBF2387B::get_offset_of_tXqVgdQEhgiLhHvNIfvbvhdHAMnu_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4227 = { sizeof (agRfXpBZsokaTfQdtBuJgJkHSQGI_tDF20763DE376F1AD74A0D2BA55304B4B861AC1F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4227[5] = 
{
	agRfXpBZsokaTfQdtBuJgJkHSQGI_tDF20763DE376F1AD74A0D2BA55304B4B861AC1F9::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	agRfXpBZsokaTfQdtBuJgJkHSQGI_tDF20763DE376F1AD74A0D2BA55304B4B861AC1F9::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	agRfXpBZsokaTfQdtBuJgJkHSQGI_tDF20763DE376F1AD74A0D2BA55304B4B861AC1F9::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	agRfXpBZsokaTfQdtBuJgJkHSQGI_tDF20763DE376F1AD74A0D2BA55304B4B861AC1F9::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	agRfXpBZsokaTfQdtBuJgJkHSQGI_tDF20763DE376F1AD74A0D2BA55304B4B861AC1F9::get_offset_of_TaHsLDlzJZGOmiVAEbIVOMFVhWNC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4228 = { sizeof (Platform_OSX_t03A46AF339888AC6E808D1D31A7467AD14FCA357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4228[1] = 
{
	Platform_OSX_t03A46AF339888AC6E808D1D31A7467AD14FCA357::get_offset_of_variants_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4229 = { sizeof (Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4229[2] = 
{
	Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03::get_offset_of_matchingCriteria_1(),
	Platform_Linux_Base_t2DC043F85ECFFC3426B89E036ED8F4EA86D86E03::get_offset_of_elements_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4230 = { sizeof (MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4230[9] = 
{
	MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2::get_offset_of_hatCount_4(),
	MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2::get_offset_of_alternateElementCounts_5(),
	MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2::get_offset_of_manufacturer_useRegex_6(),
	MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2::get_offset_of_productName_useRegex_7(),
	MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2::get_offset_of_systemName_useRegex_8(),
	MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2::get_offset_of_manufacturer_9(),
	MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2::get_offset_of_productName_10(),
	MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2::get_offset_of_systemName_11(),
	MatchingCriteria_t0A8900A5A9A732A3B611D8B1A8BACAC11BE9E4E2::get_offset_of_productGUID_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4231 = { sizeof (ElementCount_tA2D9511A33E8C5F5428D1E9150A3C1D15D4C9FA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4231[1] = 
{
	ElementCount_tA2D9511A33E8C5F5428D1E9150A3C1D15D4C9FA2::get_offset_of_hatCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4232 = { sizeof (Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4232[2] = 
{
	Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF::get_offset_of_axes_0(),
	Elements_tB60EA7C8A77A0BEE10D7BAC3CDC4CA4D010A0AFF::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4233 = { sizeof (gIoBkuwSIlJOFixWOruMuOMixgv_tDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4233[5] = 
{
	gIoBkuwSIlJOFixWOruMuOMixgv_tDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	gIoBkuwSIlJOFixWOruMuOMixgv_tDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	gIoBkuwSIlJOFixWOruMuOMixgv_tDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	gIoBkuwSIlJOFixWOruMuOMixgv_tDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	gIoBkuwSIlJOFixWOruMuOMixgv_tDEE159FFCA7ADA0F1CEC7E68C6532893F9C8ACEC::get_offset_of_OrbwoIzETVPsVWWlVhcktAHdUZp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4234 = { sizeof (WGczixdOJyVUGFGxlCDWdEwraome_t53BEDB52829C96862405574998671D31735DC978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4234[5] = 
{
	WGczixdOJyVUGFGxlCDWdEwraome_t53BEDB52829C96862405574998671D31735DC978::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	WGczixdOJyVUGFGxlCDWdEwraome_t53BEDB52829C96862405574998671D31735DC978::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	WGczixdOJyVUGFGxlCDWdEwraome_t53BEDB52829C96862405574998671D31735DC978::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	WGczixdOJyVUGFGxlCDWdEwraome_t53BEDB52829C96862405574998671D31735DC978::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	WGczixdOJyVUGFGxlCDWdEwraome_t53BEDB52829C96862405574998671D31735DC978::get_offset_of_ypdBJEaRFrJxDBDuWFtMnHLTlSg_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4235 = { sizeof (Element_t82B7A20AC236A568752E7BBC63B844092DD83294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4236 = { sizeof (Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4236[14] = 
{
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_elementIdentifier_0(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_sourceType_1(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_sourceButton_2(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_sourceAxis_3(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_sourceAxisPole_4(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_axisDeadZone_5(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_sourceHat_6(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_sourceHatType_7(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_sourceHatDirection_8(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_requireMultipleButtons_9(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_requiredButtons_10(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_ignoreIfButtonsActive_11(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_ignoreIfButtonsActiveButtons_12(),
	Button_t2FDAADD40BD83F525B1DEE9ED8B02838DD4DE64B::get_offset_of_buttonInfo_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4237 = { sizeof (Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4237[17] = 
{
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_elementIdentifier_0(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_sourceType_1(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_sourceAxis_2(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_sourceAxisRange_3(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_invert_4(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_axisDeadZone_5(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_calibrateAxis_6(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_axisZero_7(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_axisMin_8(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_axisMax_9(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_alternateCalibrations_10(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_axisInfo_11(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_sourceButton_12(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_buttonAxisContribution_13(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_sourceHat_14(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_sourceHatDirection_15(),
	Axis_t9E03950EA58556150FF5010612981B6A8B9FDC6F::get_offset_of_sourceHatRange_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4238 = { sizeof (cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4238[6] = 
{
	cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE::get_offset_of_gQLitZPWLRDfDTLuNqDuRKdoENRb_4(),
	cwcmEoqerQrESvZzbDpuqKmbgZm_tBD3CBED09E123BB8F8840910AFC76684BE02F2EE::get_offset_of_eUjlzCvPnutsdjfeMyGQKnqxpbX_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4239 = { sizeof (JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4239[6] = 
{
	JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499::get_offset_of_TSRwvcqGfNlHrfiluDIKLqwFFkQ_4(),
	JjCMmuAkTvloAPNLkbXeEGSLvPX_t10CAA3D9CB49C93ECAB89FA1748E0BCC959F6499::get_offset_of_PATgLDdPRaJHzrCXtZSEauMYeqXg_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4240 = { sizeof (Platform_Linux_t5C50A8A935B1AE1D30D6F7912DA10AEAFFD53EB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4240[1] = 
{
	Platform_Linux_t5C50A8A935B1AE1D30D6F7912DA10AEAFFD53EB5::get_offset_of_variants_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4241 = { sizeof (Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4241[2] = 
{
	Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B::get_offset_of_matchingCriteria_1(),
	Platform_WindowsUWP_Base_t96F496871DFB75DF20A4AB2FE3C5B25605F9D14B::get_offset_of_elements_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4242 = { sizeof (MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4242[7] = 
{
	MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020::get_offset_of_hatCount_4(),
	MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020::get_offset_of_alternateElementCounts_5(),
	MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020::get_offset_of_manufacturer_useRegex_6(),
	MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020::get_offset_of_productName_useRegex_7(),
	MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020::get_offset_of_manufacturer_8(),
	MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020::get_offset_of_productName_9(),
	MatchingCriteria_tC533CB63E9FD91010D2CB407E151B8FDEE098020::get_offset_of_productGUID_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4243 = { sizeof (ElementCount_tEF47EBACC44F3DF2ADF137E38A160B967ED0DBAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4243[1] = 
{
	ElementCount_tEF47EBACC44F3DF2ADF137E38A160B967ED0DBAC::get_offset_of_hatCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4244 = { sizeof (Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4244[2] = 
{
	Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B::get_offset_of_axes_0(),
	Elements_tBC3CE538CE24E8D280C19F42558B77DDD918F29B::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4245 = { sizeof (EzrBmmezJZcgNWLEwCxWAxyHeKVu_t67317305B1D1EC94CA3475BD79BC9B4340089DED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4245[5] = 
{
	EzrBmmezJZcgNWLEwCxWAxyHeKVu_t67317305B1D1EC94CA3475BD79BC9B4340089DED::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	EzrBmmezJZcgNWLEwCxWAxyHeKVu_t67317305B1D1EC94CA3475BD79BC9B4340089DED::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	EzrBmmezJZcgNWLEwCxWAxyHeKVu_t67317305B1D1EC94CA3475BD79BC9B4340089DED::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	EzrBmmezJZcgNWLEwCxWAxyHeKVu_t67317305B1D1EC94CA3475BD79BC9B4340089DED::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	EzrBmmezJZcgNWLEwCxWAxyHeKVu_t67317305B1D1EC94CA3475BD79BC9B4340089DED::get_offset_of_PSbIQIpCKJBXibHiMTgFqKrtegiC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4246 = { sizeof (auIXxilsxExkUmCHlGIrLqzPnrV_t8FDC6F1391128F1A019739F17E4CE89128965676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4246[5] = 
{
	auIXxilsxExkUmCHlGIrLqzPnrV_t8FDC6F1391128F1A019739F17E4CE89128965676::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	auIXxilsxExkUmCHlGIrLqzPnrV_t8FDC6F1391128F1A019739F17E4CE89128965676::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	auIXxilsxExkUmCHlGIrLqzPnrV_t8FDC6F1391128F1A019739F17E4CE89128965676::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	auIXxilsxExkUmCHlGIrLqzPnrV_t8FDC6F1391128F1A019739F17E4CE89128965676::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	auIXxilsxExkUmCHlGIrLqzPnrV_t8FDC6F1391128F1A019739F17E4CE89128965676::get_offset_of_bezskRSuRBOvUGZWknHDblQTAeO_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4247 = { sizeof (Element_tED0CF25F632E14F6E9BA2C7306E99D690D242165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4248 = { sizeof (Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4248[14] = 
{
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_elementIdentifier_0(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_sourceType_1(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_sourceButton_2(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_sourceAxis_3(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_sourceAxisPole_4(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_axisDeadZone_5(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_sourceHat_6(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_sourceHatType_7(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_sourceHatDirection_8(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_requireMultipleButtons_9(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_requiredButtons_10(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_ignoreIfButtonsActive_11(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_ignoreIfButtonsActiveButtons_12(),
	Button_tE8FFF88AA16A4B8FCDD380E5BF1F67CBD066E8BF::get_offset_of_buttonInfo_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4249 = { sizeof (Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4249[17] = 
{
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_elementIdentifier_0(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_sourceType_1(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_sourceAxis_2(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_sourceAxisRange_3(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_invert_4(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_axisDeadZone_5(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_calibrateAxis_6(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_axisZero_7(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_axisMin_8(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_axisMax_9(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_alternateCalibrations_10(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_axisInfo_11(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_sourceButton_12(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_buttonAxisContribution_13(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_sourceHat_14(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_sourceHatDirection_15(),
	Axis_t1177CDA7B0F08C57CA4568A5D34477A23B442312::get_offset_of_sourceHatRange_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4250 = { sizeof (YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4250[6] = 
{
	YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601::get_offset_of_qqQOJqBmqlfRUbpwwFDysYTsitt_4(),
	YuXlupCtQjDyISQsWesUTZQohRG_t26151B60139C0718D9BCB78339D0EB246A83D601::get_offset_of_AWruhtCZVwDWOojDzqRDFmWhfxmd_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4251 = { sizeof (CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4251[6] = 
{
	CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C::get_offset_of_qezRwTMBGSoPxCwVZyactNiNcZu_4(),
	CbYrBUoflWkKkFywRaCZWHNlKct_t378F364C3D26696A028063BA83B584BDF834D34C::get_offset_of_iOmcUTkiFhvjVwTnokaAVyvPYjd_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4252 = { sizeof (Platform_WindowsUWP_t79A50D7432962C9DD8B8CDFFAFC55B27BB32C06A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4252[1] = 
{
	Platform_WindowsUWP_t79A50D7432962C9DD8B8CDFFAFC55B27BB32C06A::get_offset_of_variants_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4253 = { sizeof (Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4253[2] = 
{
	Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4::get_offset_of_matchingCriteria_1(),
	Platform_Fallback_Base_t2CAE633FFCFD2EEB508684550513E78535495DA4::get_offset_of_elements_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4254 = { sizeof (MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4254[9] = 
{
	MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056::get_offset_of_alwaysMatch_4(),
	MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056::get_offset_of_productName_useRegex_5(),
	MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056::get_offset_of_productName_6(),
	MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056::get_offset_of_matchUnityVersion_7(),
	MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056::get_offset_of_matchUnityVersion_min_8(),
	MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056::get_offset_of_matchUnityVersion_max_9(),
	MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056::get_offset_of_matchSysVersion_10(),
	MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056::get_offset_of_matchSysVersion_min_11(),
	MatchingCriteria_t11F726FBD5F1B7F1B306F1F4EC04B260A52AF056::get_offset_of_matchSysVersion_max_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4255 = { sizeof (Elements_tA67426A050B3846F67FA5F1D602FC4752C236EA7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4255[2] = 
{
	Elements_tA67426A050B3846F67FA5F1D602FC4752C236EA7::get_offset_of_axes_0(),
	Elements_tA67426A050B3846F67FA5F1D602FC4752C236EA7::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4256 = { sizeof (CustomCalculationSourceData_t947413414549A85A7CD81E30468F96A51CECF9B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4256[5] = 
{
	CustomCalculationSourceData_t947413414549A85A7CD81E30468F96A51CECF9B5::get_offset_of_sourceType_0(),
	CustomCalculationSourceData_t947413414549A85A7CD81E30468F96A51CECF9B5::get_offset_of_sourceElement_1(),
	CustomCalculationSourceData_t947413414549A85A7CD81E30468F96A51CECF9B5::get_offset_of_sourceAxisRange_2(),
	CustomCalculationSourceData_t947413414549A85A7CD81E30468F96A51CECF9B5::get_offset_of_deadzone_3(),
	CustomCalculationSourceData_t947413414549A85A7CD81E30468F96A51CECF9B5::get_offset_of_invert_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4257 = { sizeof (Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4257[8] = 
{
	Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E::get_offset_of_elementIdentifier_0(),
	Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E::get_offset_of_sourceType_1(),
	Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E::get_offset_of_sourceAxis_2(),
	Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E::get_offset_of_axisDeadZone_3(),
	Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E::get_offset_of_sourceButton_4(),
	Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E::get_offset_of_sourceKeyCode_5(),
	Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E::get_offset_of_customCalculation_6(),
	Element_tB2C88B2BA324E7E6F1715AF5F87E0452DFF06B6E::get_offset_of_customCalculationSourceData_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4258 = { sizeof (Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4258[14] = 
{
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_sourceAxisPole_8(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_unityHat_sourceAxis1_9(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_unityHat_sourceAxis2_10(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_unityHat_isActiveAxisValues1_11(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_unityHat_isActiveAxisValues2_12(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_unityHat_isActiveAxisValues3_13(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_unityHat_zeroValues_14(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_unityHat_checkNeverPressed_15(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_unityHat_neverPressedZeroValues_16(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_requireMultipleButtons_17(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_requiredButtons_18(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_ignoreIfButtonsActive_19(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_ignoreIfButtonsActiveButtons_20(),
	Button_tF87059D3DE32E69299B7FC6AF4DB9560EF5FA39A::get_offset_of_buttonInfo_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4259 = { sizeof (Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4259[9] = 
{
	Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC::get_offset_of_invert_8(),
	Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC::get_offset_of_sourceAxisRange_9(),
	Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC::get_offset_of_buttonAxisContribution_10(),
	Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC::get_offset_of_calibrateAxis_11(),
	Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC::get_offset_of_axisZero_12(),
	Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC::get_offset_of_axisMin_13(),
	Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC::get_offset_of_axisMax_14(),
	Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC::get_offset_of_alternateCalibrations_15(),
	Axis_tAFCB7D4B202EE7DEE0000218D94799C2B12DEFFC::get_offset_of_axisInfo_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4260 = { sizeof (jxgXkzlNZIvRFWiqanngLVzagUr_t61740DA545A3090693AAEBB696CEB6049E5986FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4260[5] = 
{
	jxgXkzlNZIvRFWiqanngLVzagUr_t61740DA545A3090693AAEBB696CEB6049E5986FD::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	jxgXkzlNZIvRFWiqanngLVzagUr_t61740DA545A3090693AAEBB696CEB6049E5986FD::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	jxgXkzlNZIvRFWiqanngLVzagUr_t61740DA545A3090693AAEBB696CEB6049E5986FD::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	jxgXkzlNZIvRFWiqanngLVzagUr_t61740DA545A3090693AAEBB696CEB6049E5986FD::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	jxgXkzlNZIvRFWiqanngLVzagUr_t61740DA545A3090693AAEBB696CEB6049E5986FD::get_offset_of_AnysTbsSkKHFvOTieNBKYVpjqIa_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4261 = { sizeof (cjmCDIcVlfqVTgtvvATYeWUwNmEk_t3B6E76040BC37F3D9849A477EEE6E871233C299A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4261[5] = 
{
	cjmCDIcVlfqVTgtvvATYeWUwNmEk_t3B6E76040BC37F3D9849A477EEE6E871233C299A::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	cjmCDIcVlfqVTgtvvATYeWUwNmEk_t3B6E76040BC37F3D9849A477EEE6E871233C299A::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	cjmCDIcVlfqVTgtvvATYeWUwNmEk_t3B6E76040BC37F3D9849A477EEE6E871233C299A::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	cjmCDIcVlfqVTgtvvATYeWUwNmEk_t3B6E76040BC37F3D9849A477EEE6E871233C299A::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	cjmCDIcVlfqVTgtvvATYeWUwNmEk_t3B6E76040BC37F3D9849A477EEE6E871233C299A::get_offset_of_IrGWWOzZMkuNYdSSpPyJJhKjHfv_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4262 = { sizeof (Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4262[1] = 
{
	Platform_Fallback_tC5968A83CBB360E30B21890E8F02D4FA0E56B313::get_offset_of_variants_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4263 = { sizeof (Platform_Custom_t38C5DD986F65AF512626A73FA4EA96ECF76A0C77), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4264 = { sizeof (MatchingCriteria_t1A1DB371BD17248F7D375FC7FA3B8704BEB1C795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4264[1] = 
{
	MatchingCriteria_t1A1DB371BD17248F7D375FC7FA3B8704BEB1C795::get_offset_of_alwaysMatch_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4265 = { sizeof (Elements_tC57D226564DCD7C2CFC9AFA7A212201FCC98129B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4266 = { sizeof (CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4266[11] = 
{
	CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19::get_offset_of_sourceType_0(),
	CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19::get_offset_of_sourceAxis_1(),
	CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19::get_offset_of_sourceButton_2(),
	CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19::get_offset_of_sourceOtherAxis_3(),
	CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19::get_offset_of_sourceAxisRange_4(),
	CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19::get_offset_of_axisDeadZone_5(),
	CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19::get_offset_of_invert_6(),
	CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19::get_offset_of_axisCalibrationType_7(),
	CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19::get_offset_of_axisZero_8(),
	CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19::get_offset_of_axisMin_9(),
	CustomCalculationSourceData_t7C1FB4962CF25826FC0CEAB942A9794A1DD9AC19::get_offset_of_axisMax_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4267 = { sizeof (Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4267[7] = 
{
	Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF::get_offset_of_elementIdentifier_0(),
	Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF::get_offset_of_sourceType_1(),
	Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF::get_offset_of_sourceAxis_2(),
	Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF::get_offset_of_axisDeadZone_3(),
	Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF::get_offset_of_sourceButton_4(),
	Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF::get_offset_of_customCalculation_5(),
	Element_t917DF3B3892BA48FD5AE1595F8C288DC67D972BF::get_offset_of_customCalculationSourceData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4268 = { sizeof (Button_tBB6149DD7A45B873206F15797DC2B002019BF09C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4268[6] = 
{
	Button_tBB6149DD7A45B873206F15797DC2B002019BF09C::get_offset_of_sourceAxisPole_7(),
	Button_tBB6149DD7A45B873206F15797DC2B002019BF09C::get_offset_of_requireMultipleButtons_8(),
	Button_tBB6149DD7A45B873206F15797DC2B002019BF09C::get_offset_of_requiredButtons_9(),
	Button_tBB6149DD7A45B873206F15797DC2B002019BF09C::get_offset_of_ignoreIfButtonsActive_10(),
	Button_tBB6149DD7A45B873206F15797DC2B002019BF09C::get_offset_of_ignoreIfButtonsActiveButtons_11(),
	Button_tBB6149DD7A45B873206F15797DC2B002019BF09C::get_offset_of_buttonInfo_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4269 = { sizeof (Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4269[9] = 
{
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E::get_offset_of_invert_7(),
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E::get_offset_of_sourceAxisRange_8(),
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E::get_offset_of_buttonAxisContribution_9(),
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E::get_offset_of_calibrateAxis_10(),
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E::get_offset_of_axisZero_11(),
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E::get_offset_of_axisMin_12(),
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E::get_offset_of_axisMax_13(),
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E::get_offset_of_alternateCalibrations_14(),
	Axis_t9C27EA3934B5EE703F9D49A69D5B6A01F9411E9E::get_offset_of_axisInfo_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4270 = { sizeof (Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4270[4] = 
{
	Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12::get_offset_of_matchingCriteria_1(),
	Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12::get_offset_of_elements_2(),
	Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12::get_offset_of__axesOrigGame_3(),
	Platform_Ouya_Base_t1D89BF95095C7D9283F8C46FD472837B91830E12::get_offset_of__buttonsOrigGame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4271 = { sizeof (MatchingCriteria_t7E3617113D979BC9EBEACC19768821EA3A4A3ECE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4272 = { sizeof (Elements_t337BF774C168903EA673A8A56D19C01534EA3196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4272[2] = 
{
	Elements_t337BF774C168903EA673A8A56D19C01534EA3196::get_offset_of_axes_0(),
	Elements_t337BF774C168903EA673A8A56D19C01534EA3196::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4273 = { sizeof (Button_tD29D85D68B3EB113ACAD47AAE9E25DB08DC5E8B3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4274 = { sizeof (Axis_t19DD9B9FC74FD35EFE5B6EB3DDC76CEB8E6221EA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4275 = { sizeof (LoaMaUsXBrHIsltPSNLpwkqKVeN_t8A685890B0D9497EF535CE09117131DA81E53C75), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4275[5] = 
{
	LoaMaUsXBrHIsltPSNLpwkqKVeN_t8A685890B0D9497EF535CE09117131DA81E53C75::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	LoaMaUsXBrHIsltPSNLpwkqKVeN_t8A685890B0D9497EF535CE09117131DA81E53C75::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	LoaMaUsXBrHIsltPSNLpwkqKVeN_t8A685890B0D9497EF535CE09117131DA81E53C75::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	LoaMaUsXBrHIsltPSNLpwkqKVeN_t8A685890B0D9497EF535CE09117131DA81E53C75::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	LoaMaUsXBrHIsltPSNLpwkqKVeN_t8A685890B0D9497EF535CE09117131DA81E53C75::get_offset_of_wmaFOcRiDRKegAnLKfvzccCrsxv_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4276 = { sizeof (ounOgxDqlHuXjbbITHTZxmpFpHm_tBF131D48BB2A66BF4BBE373E0C17A89149439DA9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4276[5] = 
{
	ounOgxDqlHuXjbbITHTZxmpFpHm_tBF131D48BB2A66BF4BBE373E0C17A89149439DA9::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	ounOgxDqlHuXjbbITHTZxmpFpHm_tBF131D48BB2A66BF4BBE373E0C17A89149439DA9::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	ounOgxDqlHuXjbbITHTZxmpFpHm_tBF131D48BB2A66BF4BBE373E0C17A89149439DA9::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	ounOgxDqlHuXjbbITHTZxmpFpHm_tBF131D48BB2A66BF4BBE373E0C17A89149439DA9::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	ounOgxDqlHuXjbbITHTZxmpFpHm_tBF131D48BB2A66BF4BBE373E0C17A89149439DA9::get_offset_of_JPXiOSvHYiBazZZyXFtfrWwtopG_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4277 = { sizeof (Platform_Ouya_t6E1DA960C43511902B179E0E6746B8230AE60092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4277[1] = 
{
	Platform_Ouya_t6E1DA960C43511902B179E0E6746B8230AE60092::get_offset_of_variants_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4278 = { sizeof (Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4278[4] = 
{
	Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479::get_offset_of_matchingCriteria_1(),
	Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479::get_offset_of_elements_2(),
	Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479::get_offset_of__axesOrigGame_3(),
	Platform_XboxOne_Base_tF2A2C472E98A1A55F98D8522E08CB62C089AC479::get_offset_of__buttonsOrigGame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4279 = { sizeof (MatchingCriteria_t8DBC25330E8572B4C927356459E8192B136CFBAD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4279[2] = 
{
	MatchingCriteria_t8DBC25330E8572B4C927356459E8192B136CFBAD::get_offset_of_productName_useRegex_5(),
	MatchingCriteria_t8DBC25330E8572B4C927356459E8192B136CFBAD::get_offset_of_productName_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4280 = { sizeof (Elements_tF1026C7DB7135259ABB2A130FE4C24B0C0260DF2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4280[2] = 
{
	Elements_tF1026C7DB7135259ABB2A130FE4C24B0C0260DF2::get_offset_of_axes_0(),
	Elements_tF1026C7DB7135259ABB2A130FE4C24B0C0260DF2::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4281 = { sizeof (Button_t51AA16D2A4277E09E1154ACC2804E5ABC30AE26E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4282 = { sizeof (Axis_tB71D44914DCD3FECE37D201EA1FF52A497A41F05), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4283 = { sizeof (ufZtUEALFBXEuicwzwJyeUDlpLS_t7C532B5037E134428A485D9E012147AAFCEF71B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4283[5] = 
{
	ufZtUEALFBXEuicwzwJyeUDlpLS_t7C532B5037E134428A485D9E012147AAFCEF71B1::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	ufZtUEALFBXEuicwzwJyeUDlpLS_t7C532B5037E134428A485D9E012147AAFCEF71B1::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	ufZtUEALFBXEuicwzwJyeUDlpLS_t7C532B5037E134428A485D9E012147AAFCEF71B1::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	ufZtUEALFBXEuicwzwJyeUDlpLS_t7C532B5037E134428A485D9E012147AAFCEF71B1::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	ufZtUEALFBXEuicwzwJyeUDlpLS_t7C532B5037E134428A485D9E012147AAFCEF71B1::get_offset_of_ytvEhjKyNpfGtxRwbRJRcsnETTb_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4284 = { sizeof (oxYEommIFFkPzcKHlbkZBKwEKki_tD631ECCAF554F1DB439E852B5EDBD1F724932A10), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4284[5] = 
{
	oxYEommIFFkPzcKHlbkZBKwEKki_tD631ECCAF554F1DB439E852B5EDBD1F724932A10::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	oxYEommIFFkPzcKHlbkZBKwEKki_tD631ECCAF554F1DB439E852B5EDBD1F724932A10::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	oxYEommIFFkPzcKHlbkZBKwEKki_tD631ECCAF554F1DB439E852B5EDBD1F724932A10::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	oxYEommIFFkPzcKHlbkZBKwEKki_tD631ECCAF554F1DB439E852B5EDBD1F724932A10::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	oxYEommIFFkPzcKHlbkZBKwEKki_tD631ECCAF554F1DB439E852B5EDBD1F724932A10::get_offset_of_PzfgrQgniUFZfDJYGGshDnHPKlel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4285 = { sizeof (Platform_XboxOne_tF5ACC52C21E7D086DBC6B842E36075C1582AA2C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4285[1] = 
{
	Platform_XboxOne_tF5ACC52C21E7D086DBC6B842E36075C1582AA2C4::get_offset_of_variants_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4286 = { sizeof (Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4286[4] = 
{
	Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7::get_offset_of_matchingCriteria_1(),
	Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7::get_offset_of_elements_2(),
	Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7::get_offset_of__axesOrigGame_3(),
	Platform_PS4_Base_tC174021797D00A227DC7EDF0B0B57145EB7D57A7::get_offset_of__buttonsOrigGame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4287 = { sizeof (MatchingCriteria_t728FB1BA60E0E29B55788562DE5AF371ACFC1A0C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4287[2] = 
{
	MatchingCriteria_t728FB1BA60E0E29B55788562DE5AF371ACFC1A0C::get_offset_of_productName_useRegex_5(),
	MatchingCriteria_t728FB1BA60E0E29B55788562DE5AF371ACFC1A0C::get_offset_of_productName_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4288 = { sizeof (Elements_t1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4288[2] = 
{
	Elements_t1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02::get_offset_of_axes_0(),
	Elements_t1DCC238A72D7024EBF135BB6FDEF844DBCAAFC02::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4289 = { sizeof (Button_t67E3C3174F037E7E08D5B053F1E30BDA666B914D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4290 = { sizeof (Axis_t972C4630BDCBD8D4C00E4BF2E5DFD9F812447C87), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4291 = { sizeof (aQoHTPKBETJPNiSpRRLBHslmPqc_t9974202F8DD362E787D9545C7CFE22CA90D4A5E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4291[5] = 
{
	aQoHTPKBETJPNiSpRRLBHslmPqc_t9974202F8DD362E787D9545C7CFE22CA90D4A5E9::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	aQoHTPKBETJPNiSpRRLBHslmPqc_t9974202F8DD362E787D9545C7CFE22CA90D4A5E9::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	aQoHTPKBETJPNiSpRRLBHslmPqc_t9974202F8DD362E787D9545C7CFE22CA90D4A5E9::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	aQoHTPKBETJPNiSpRRLBHslmPqc_t9974202F8DD362E787D9545C7CFE22CA90D4A5E9::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	aQoHTPKBETJPNiSpRRLBHslmPqc_t9974202F8DD362E787D9545C7CFE22CA90D4A5E9::get_offset_of_BTsbGVkyBHtFlWarfGmuUnxnaUeF_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4292 = { sizeof (wSoeyLLcbDCkHDSVzPMmHOuJkYfN_tD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4292[5] = 
{
	wSoeyLLcbDCkHDSVzPMmHOuJkYfN_tD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	wSoeyLLcbDCkHDSVzPMmHOuJkYfN_tD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	wSoeyLLcbDCkHDSVzPMmHOuJkYfN_tD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	wSoeyLLcbDCkHDSVzPMmHOuJkYfN_tD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	wSoeyLLcbDCkHDSVzPMmHOuJkYfN_tD1E97579EDAE28A6B62FFBE5B8BFD6D525B7AA45::get_offset_of_tjEGQFIUcwJTYTHdBnBpHIbqxDB_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4293 = { sizeof (Platform_PS4_t5B4B7E5A8EF60DFB5E1F1CE6756ADD5BD5FB4FA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4293[1] = 
{
	Platform_PS4_t5B4B7E5A8EF60DFB5E1F1CE6756ADD5BD5FB4FA1::get_offset_of_variants_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4294 = { sizeof (Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4294[4] = 
{
	Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF::get_offset_of_matchingCriteria_1(),
	Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF::get_offset_of_elements_2(),
	Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF::get_offset_of__axesOrigGame_3(),
	Platform_NintendoSwitch_Base_t762348F60D107794795951289C5E38A71195D6EF::get_offset_of__buttonsOrigGame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4295 = { sizeof (MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4295[2] = 
{
	MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC::get_offset_of_productName_useRegex_5(),
	MatchingCriteria_tA0686CFF2AEF6E1F3E8E6FD65D4D967D6F1E4BEC::get_offset_of_productName_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4296 = { sizeof (Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4296[2] = 
{
	Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D::get_offset_of_axes_0(),
	Elements_t589C5928C7E1F0D4523754CE46B158FBB57A741D::get_offset_of_buttons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4297 = { sizeof (Button_t307B211449A4FDF5CAC5534717DB093734171F6E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4298 = { sizeof (Axis_t1C5F76253ECE39C5FB44D7E080FE2B8F91EA3B23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4299 = { sizeof (qxWzstKCvogOpEOkEZjjPyShnYlB_tA79D0BE6704B2F8065F197D5B78F3607770A5FA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4299[5] = 
{
	qxWzstKCvogOpEOkEZjjPyShnYlB_tA79D0BE6704B2F8065F197D5B78F3607770A5FA4::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	qxWzstKCvogOpEOkEZjjPyShnYlB_tA79D0BE6704B2F8065F197D5B78F3607770A5FA4::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	qxWzstKCvogOpEOkEZjjPyShnYlB_tA79D0BE6704B2F8065F197D5B78F3607770A5FA4::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	qxWzstKCvogOpEOkEZjjPyShnYlB_tA79D0BE6704B2F8065F197D5B78F3607770A5FA4::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	qxWzstKCvogOpEOkEZjjPyShnYlB_tA79D0BE6704B2F8065F197D5B78F3607770A5FA4::get_offset_of_xsDEqkxdlhIsWdiGNsvFuhyVxDVE_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
