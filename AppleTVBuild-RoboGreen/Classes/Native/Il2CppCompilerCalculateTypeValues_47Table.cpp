﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// EaCqncmrbxTySfotaFWfqDQVLIS
struct EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0;
// JrKetNtIJINuYILTEYSwNUvgPiW
struct JrKetNtIJINuYILTEYSwNUvgPiW_t8C4387FB6F08F0E7108B290487E5D6DA10BF9FFB;
// LkmsImmStVEvsBvZKxyBsAPXXJ
struct LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA;
// Rewired.ActionElementMap
struct ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7;
// Rewired.Controller
struct Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671;
// Rewired.Controller/Extension
struct Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD;
// Rewired.ControllerMap
struct ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB;
// Rewired.ControllerMapEnabler
struct ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D;
// Rewired.ControllerMapLayoutManager
struct ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781;
// Rewired.ControllerMapWithAxes
struct ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36;
// Rewired.ControllerType[]
struct ControllerTypeU5BU5D_tF512B952A07962493A1287DDA9D63B7A14264D43;
// Rewired.CustomController
struct CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A;
// Rewired.CustomControllerMap
struct CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A;
// Rewired.Data.ConfigVars
struct ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41;
// Rewired.Data.ControllerDataFiles
struct ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5;
// Rewired.Data.UserData
struct UserData_t23CF13BC0883AADA4A3457583033292E49A77D48;
// Rewired.Data.UserDataStore
struct UserDataStore_tA04E79BDF387346606C6F4379B4FD0935BA176C1;
// Rewired.IControllerElementTarget
struct IControllerElementTarget_tBECEEA8A0AF0C16A3D41416E35B98CFD09422658;
// Rewired.InputManager_Base
struct InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701;
// Rewired.Interfaces.IControllerAssigner
struct IControllerAssigner_t2F25594B90305482F003AA886389A2B9F394BE73;
// Rewired.Interfaces.IControllerExtensionSource
struct IControllerExtensionSource_tB7B0912C0560F4E99E7927ED74A63303585AD9F3;
// Rewired.Joystick
struct Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39;
// Rewired.PlatformInputManager
struct PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A;
// Rewired.Platforms.Custom.CustomInputSource/Axis[]
struct AxisU5BU5D_tD22230ADED67D8E860DD8F4226514D79EDE55BEE;
// Rewired.Platforms.Custom.CustomInputSource/Button[]
struct ButtonU5BU5D_t181B5EB5ACB98CA3C44C3FB3DF613259B353B43A;
// Rewired.Platforms.PS4.IPS4ControllerExtensionSource
struct IPS4ControllerExtensionSource_tBF50EEB73EA1D95650832794EB8B872FEF86FFBA;
// Rewired.Platforms.PS4.Internal.LoggedInUser
struct LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3;
// Rewired.Platforms.PS4.PS4InputSource/ReRqQZNVnmKJXmrmWwzirxfUKzM/uHmqJGqYoMpKFcMgEPjnasPBJId
struct uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D;
// Rewired.Platforms.PS4.PS4InputSource/YUATmdkIYeDTVeeKKfwolrMGUrN
struct YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77;
// Rewired.Platforms.PS4.PS4InputSource/YUATmdkIYeDTVeeKKfwolrMGUrN/imtLMjZbMBHFikNPqUtEGzfQavzx[]
struct imtLMjZbMBHFikNPqUtEGzfQavzxU5BU5D_t71EACA48282696D2E452F7CC6DA5754755767D62;
// Rewired.Player
struct Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F;
// Rewired.Player/ControllerHelper
struct ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC;
// Rewired.Player/ControllerHelper/ConflictCheckingHelper
struct ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1;
// Rewired.Player/ControllerHelper/LBdawFihcNUlinDyhLfBKVmmxSHI
struct LBdawFihcNUlinDyhLfBKVmmxSHI_t860A90AC98798D8C5D33D9AD6A9FB1F5BE051B98;
// Rewired.Player/ControllerHelper/LBdawFihcNUlinDyhLfBKVmmxSHI[]
struct LBdawFihcNUlinDyhLfBKVmmxSHIU5BU5D_tA646A82DB1D771533D3E584B522435419C4BA379;
// Rewired.Player/ControllerHelper/MapHelper
struct MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92;
// Rewired.Player/ControllerHelper/PollingHelper
struct PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24;
// Rewired.ReInput/ConfigHelper
struct ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5;
// Rewired.ReInput/ControllerHelper
struct ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62;
// Rewired.ReInput/ControllerHelper/ConflictCheckingHelper
struct ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531;
// Rewired.ReInput/ControllerHelper/PollingHelper
struct PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A;
// Rewired.ReInput/MQKnbNqPnvBXaFjURWimREWJePx
struct MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E;
// Rewired.ReInput/MappingHelper
struct MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6;
// Rewired.ReInput/PlayerHelper
struct PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412;
// Rewired.ReInput/TimeHelper
struct TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B;
// Rewired.ReInput/UnityTouch
struct UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9;
// Rewired.ReInput/pqJryspmPjvziIiviMzAyYQWLVF
struct pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6;
// Rewired.Utils.Classes.Utility.TimerAbs
struct TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429;
// Rewired.Utils.Classes.Utility.TimerAbs[]
struct TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC;
// Rewired.Utils.Interfaces.IExternalTools
struct IExternalTools_t6236FC950A839A5A3925C74451CAF8304C28F92C;
// Rewired.Utils.SafeAction
struct SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD;
// Rewired.Utils.SafeAction`1<Rewired.ControllerStatusChangedEventArgs>
struct SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900;
// Rewired.Utils.TempListPool/TList`1<Rewired.ActionElementMap>
struct TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Rewired.Platforms.PS4.PS4InputSource/YUATmdkIYeDTVeeKKfwolrMGUrN/PYOLdyXwCbgsmWCZaTDfMJgndAl>
struct Action_1_tAA5E5642BEC9A7A50694B8466906C3DDBE2F8341;
// System.Action`1<Rewired.Platforms.PS4.PS4InputSource/YUATmdkIYeDTVeeKKfwolrMGUrN/ZFbPVMKgGgElciFwNeAzftnIMAqR>
struct Action_1_tEF2248283CB5F3A04F74CC55879CB868DF0FA49B;
// System.Action`1<Rewired.UpdateLoopType>
struct Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<System.Exception>
struct Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC;
// System.Action`1<System.Int32>
struct Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B;
// System.Action`1<UnityEngine.FullScreenMode>
struct Action_1_t4F62567B66CEDB08BB9CE965F45B0DA1C06F2204;
// System.Action`2<System.Int32,System.Boolean>
struct Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E;
// System.Action`3<System.Int32,System.Int32,System.Int32>
struct Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB;
// System.Action`4<System.Int32,System.Int32,System.Int32,System.Int32>
struct Action_4_t0513103ECC58D876DBBF435498FA75D2EAA7130D;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap>
struct IEnumerator_1_t62E0BA06CEF38BC2D5839771D2A23781A0945E7C;
// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo>
struct IEnumerator_1_t5448337C1643DF9D47E013DBFF871416D4D3F5DC;
// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo>
struct IEnumerator_1_tB5F8E3B1AA2E4BF2597F179C159DF6EFBC7DAE69;
// System.Collections.Generic.IList`1<Rewired.ControllerMap>
struct IList_1_t0AA780B699DC47233A67CE0B7A7905781C4E78B4;
// System.Collections.Generic.IList`1<Rewired.CustomController>
struct IList_1_t72F8D0A343B835033A661FC40376E4E35F64327B;
// System.Collections.Generic.IList`1<Rewired.Joystick>
struct IList_1_tF47B0A47132A7F677E03201FFE653320541BBA20;
// System.Collections.Generic.List`1<Rewired.ActionElementMap>
struct List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94;
// System.Collections.Generic.List`1<Rewired.Platforms.Custom.CustomInputSource/Joystick>
struct List_1_tE74CF06E458D02B7DFF8A66064E48B59E372F1F2;
// System.Collections.Generic.List`1<Rewired.Platforms.PS4.PS4InputSource/YUATmdkIYeDTVeeKKfwolrMGUrN/PYOLdyXwCbgsmWCZaTDfMJgndAl>
struct List_1_t696E2D4C0837937256916D2AB9A728C2EC56363B;
// System.Collections.Generic.List`1<Rewired.Platforms.PS4.PS4InputSource/YUATmdkIYeDTVeeKKfwolrMGUrN/ZFbPVMKgGgElciFwNeAzftnIMAqR>
struct List_1_tF43FF4C6B35940F952F4261B624D7CC17E9E87A7;
// System.Collections.Generic.List`1<Rewired.Player/ControllerHelper/GyRicTMqpYlIDAFpGCLnPVhblMN/UuxNpDFxmKnoSYsrSKsxwQtUnoq>
struct List_1_tB5FE22B09DC001511E810D14AE2AF6389FB53D6C;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Platforms.Custom.CustomInputSource/Axis>
struct ReadOnlyCollection_1_t4829C3B53F3992A9308E9687500F009CA89848AC;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Platforms.Custom.CustomInputSource/Button>
struct ReadOnlyCollection_1_t8D1C0984E4131D0AD8A662731FDECC171DD80B5F;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Platforms.Custom.CustomInputSource/Joystick>
struct ReadOnlyCollection_1_t3725131578433E1E4A019736E9F58AB979C5A48D;
// System.Func`1<Rewired.Platforms.PS4.PS4InputSource/YUATmdkIYeDTVeeKKfwolrMGUrN/imtLMjZbMBHFikNPqUtEGzfQavzx>
struct Func_1_tAA0F6C5612DAF65CB2AA348046536923DA35B777;
// System.Func`1<System.Boolean>
struct Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618;
// System.Func`2<System.Int32,UnityEngine.Vector3>
struct Func_2_tD6D032CBFCBE5DEFC45886FF3D22684D36923253;
// System.Func`2<System.Int32,UnityEngine.Vector4>
struct Func_2_tEE835526B7A0EE187C9950EDA1D2C97725D65C09;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// cteaRMjZkQaJnmQJcVTSoaSRpeZ
struct cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E;
// lUbzJtkApcIkohMrHhJAmcDImRRR
struct lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE;
// lVPGlCbkNvrVSnFFcfgwEyebHaly<Rewired.JoystickMap>
struct lVPGlCbkNvrVSnFFcfgwEyebHaly_tC68E73CDA2F1278DDA1B6B1CED2305F6EE35223A;
// nkGLVKXmQVVuJNEogVVmAwwQOJl
struct nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EXTENSION_TB52F54389416488161F1C79DFA089F142E5AD3FD_H
#define EXTENSION_TB52F54389416488161F1C79DFA089F142E5AD3FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Controller_Extension
struct  Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD  : public RuntimeObject
{
public:
	// Rewired.Controller Rewired.Controller_Extension::gcFDOBoqpaBweHMwNlbneFOEEAm
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___gcFDOBoqpaBweHMwNlbneFOEEAm_0;
	// Rewired.Interfaces.IControllerExtensionSource Rewired.Controller_Extension::gkqUnrSMeLSOlffXjmHcoYDAlQT
	RuntimeObject* ___gkqUnrSMeLSOlffXjmHcoYDAlQT_1;
	// System.Int32 Rewired.Controller_Extension::_reInputId
	int32_t ____reInputId_2;

public:
	inline static int32_t get_offset_of_gcFDOBoqpaBweHMwNlbneFOEEAm_0() { return static_cast<int32_t>(offsetof(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD, ___gcFDOBoqpaBweHMwNlbneFOEEAm_0)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_gcFDOBoqpaBweHMwNlbneFOEEAm_0() const { return ___gcFDOBoqpaBweHMwNlbneFOEEAm_0; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_gcFDOBoqpaBweHMwNlbneFOEEAm_0() { return &___gcFDOBoqpaBweHMwNlbneFOEEAm_0; }
	inline void set_gcFDOBoqpaBweHMwNlbneFOEEAm_0(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___gcFDOBoqpaBweHMwNlbneFOEEAm_0 = value;
		Il2CppCodeGenWriteBarrier((&___gcFDOBoqpaBweHMwNlbneFOEEAm_0), value);
	}

	inline static int32_t get_offset_of_gkqUnrSMeLSOlffXjmHcoYDAlQT_1() { return static_cast<int32_t>(offsetof(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD, ___gkqUnrSMeLSOlffXjmHcoYDAlQT_1)); }
	inline RuntimeObject* get_gkqUnrSMeLSOlffXjmHcoYDAlQT_1() const { return ___gkqUnrSMeLSOlffXjmHcoYDAlQT_1; }
	inline RuntimeObject** get_address_of_gkqUnrSMeLSOlffXjmHcoYDAlQT_1() { return &___gkqUnrSMeLSOlffXjmHcoYDAlQT_1; }
	inline void set_gkqUnrSMeLSOlffXjmHcoYDAlQT_1(RuntimeObject* value)
	{
		___gkqUnrSMeLSOlffXjmHcoYDAlQT_1 = value;
		Il2CppCodeGenWriteBarrier((&___gkqUnrSMeLSOlffXjmHcoYDAlQT_1), value);
	}

	inline static int32_t get_offset_of__reInputId_2() { return static_cast<int32_t>(offsetof(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD, ____reInputId_2)); }
	inline int32_t get__reInputId_2() const { return ____reInputId_2; }
	inline int32_t* get_address_of__reInputId_2() { return &____reInputId_2; }
	inline void set__reInputId_2(int32_t value)
	{
		____reInputId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSION_TB52F54389416488161F1C79DFA089F142E5AD3FD_H
#ifndef CONTROLLER_TFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B_H
#define CONTROLLER_TFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Custom.CustomInputSource_Controller
struct  Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Platforms.Custom.CustomInputSource_Controller::_isConnected
	bool ____isConnected_0;
	// System.String Rewired.Platforms.Custom.CustomInputSource_Controller::_deviceName
	String_t* ____deviceName_1;
	// System.String Rewired.Platforms.Custom.CustomInputSource_Controller::_customName
	String_t* ____customName_2;

public:
	inline static int32_t get_offset_of__isConnected_0() { return static_cast<int32_t>(offsetof(Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B, ____isConnected_0)); }
	inline bool get__isConnected_0() const { return ____isConnected_0; }
	inline bool* get_address_of__isConnected_0() { return &____isConnected_0; }
	inline void set__isConnected_0(bool value)
	{
		____isConnected_0 = value;
	}

	inline static int32_t get_offset_of__deviceName_1() { return static_cast<int32_t>(offsetof(Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B, ____deviceName_1)); }
	inline String_t* get__deviceName_1() const { return ____deviceName_1; }
	inline String_t** get_address_of__deviceName_1() { return &____deviceName_1; }
	inline void set__deviceName_1(String_t* value)
	{
		____deviceName_1 = value;
		Il2CppCodeGenWriteBarrier((&____deviceName_1), value);
	}

	inline static int32_t get_offset_of__customName_2() { return static_cast<int32_t>(offsetof(Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B, ____customName_2)); }
	inline String_t* get__customName_2() const { return ____customName_2; }
	inline String_t** get_address_of__customName_2() { return &____customName_2; }
	inline void set__customName_2(String_t* value)
	{
		____customName_2 = value;
		Il2CppCodeGenWriteBarrier((&____customName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLER_TFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B_H
#ifndef LOGGEDINUSER_T2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3_H
#define LOGGEDINUSER_T2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.Internal.LoggedInUser
struct  LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Platforms.PS4.Internal.LoggedInUser::status
	int32_t ___status_0;
	// System.Boolean Rewired.Platforms.PS4.Internal.LoggedInUser::primaryUser
	bool ___primaryUser_1;
	// System.Int32 Rewired.Platforms.PS4.Internal.LoggedInUser::userId
	int32_t ___userId_2;
	// System.Int32 Rewired.Platforms.PS4.Internal.LoggedInUser::color
	int32_t ___color_3;
	// System.String Rewired.Platforms.PS4.Internal.LoggedInUser::userName
	String_t* ___userName_4;
	// System.Int32 Rewired.Platforms.PS4.Internal.LoggedInUser::padHandle
	int32_t ___padHandle_5;
	// System.Int32 Rewired.Platforms.PS4.Internal.LoggedInUser::move0Handle
	int32_t ___move0Handle_6;
	// System.Int32 Rewired.Platforms.PS4.Internal.LoggedInUser::move1Handle
	int32_t ___move1Handle_7;
	// System.Int32 Rewired.Platforms.PS4.Internal.LoggedInUser::aimHandle
	int32_t ___aimHandle_8;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3, ___status_0)); }
	inline int32_t get_status_0() const { return ___status_0; }
	inline int32_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int32_t value)
	{
		___status_0 = value;
	}

	inline static int32_t get_offset_of_primaryUser_1() { return static_cast<int32_t>(offsetof(LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3, ___primaryUser_1)); }
	inline bool get_primaryUser_1() const { return ___primaryUser_1; }
	inline bool* get_address_of_primaryUser_1() { return &___primaryUser_1; }
	inline void set_primaryUser_1(bool value)
	{
		___primaryUser_1 = value;
	}

	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3, ___userId_2)); }
	inline int32_t get_userId_2() const { return ___userId_2; }
	inline int32_t* get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(int32_t value)
	{
		___userId_2 = value;
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3, ___color_3)); }
	inline int32_t get_color_3() const { return ___color_3; }
	inline int32_t* get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(int32_t value)
	{
		___color_3 = value;
	}

	inline static int32_t get_offset_of_userName_4() { return static_cast<int32_t>(offsetof(LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3, ___userName_4)); }
	inline String_t* get_userName_4() const { return ___userName_4; }
	inline String_t** get_address_of_userName_4() { return &___userName_4; }
	inline void set_userName_4(String_t* value)
	{
		___userName_4 = value;
		Il2CppCodeGenWriteBarrier((&___userName_4), value);
	}

	inline static int32_t get_offset_of_padHandle_5() { return static_cast<int32_t>(offsetof(LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3, ___padHandle_5)); }
	inline int32_t get_padHandle_5() const { return ___padHandle_5; }
	inline int32_t* get_address_of_padHandle_5() { return &___padHandle_5; }
	inline void set_padHandle_5(int32_t value)
	{
		___padHandle_5 = value;
	}

	inline static int32_t get_offset_of_move0Handle_6() { return static_cast<int32_t>(offsetof(LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3, ___move0Handle_6)); }
	inline int32_t get_move0Handle_6() const { return ___move0Handle_6; }
	inline int32_t* get_address_of_move0Handle_6() { return &___move0Handle_6; }
	inline void set_move0Handle_6(int32_t value)
	{
		___move0Handle_6 = value;
	}

	inline static int32_t get_offset_of_move1Handle_7() { return static_cast<int32_t>(offsetof(LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3, ___move1Handle_7)); }
	inline int32_t get_move1Handle_7() const { return ___move1Handle_7; }
	inline int32_t* get_address_of_move1Handle_7() { return &___move1Handle_7; }
	inline void set_move1Handle_7(int32_t value)
	{
		___move1Handle_7 = value;
	}

	inline static int32_t get_offset_of_aimHandle_8() { return static_cast<int32_t>(offsetof(LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3, ___aimHandle_8)); }
	inline int32_t get_aimHandle_8() const { return ___aimHandle_8; }
	inline int32_t* get_address_of_aimHandle_8() { return &___aimHandle_8; }
	inline void set_aimHandle_8(int32_t value)
	{
		___aimHandle_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGEDINUSER_T2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3_H
#ifndef NEBBIIEJZGOXLJGUIVQCLTDZVXC_T9E0C1572D8BD03485499BDE0D5E22C70529DA1D6_H
#define NEBBIIEJZGOXLJGUIVQCLTDZVXC_T9E0C1572D8BD03485499BDE0D5E22C70529DA1D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4ControllerExtension_neBBiiEJZGOxLJguIvQcLTdZvXc
struct  neBBiiEJZGOxLJguIvQcLTdZvXc_t9E0C1572D8BD03485499BDE0D5E22C70529DA1D6  : public RuntimeObject
{
public:
	// Rewired.Platforms.PS4.IPS4ControllerExtensionSource Rewired.Platforms.PS4.PS4ControllerExtension_neBBiiEJZGOxLJguIvQcLTdZvXc::zSCjKsgSKyXaLUKczdjHusCWAAC
	RuntimeObject* ___zSCjKsgSKyXaLUKczdjHusCWAAC_0;

public:
	inline static int32_t get_offset_of_zSCjKsgSKyXaLUKczdjHusCWAAC_0() { return static_cast<int32_t>(offsetof(neBBiiEJZGOxLJguIvQcLTdZvXc_t9E0C1572D8BD03485499BDE0D5E22C70529DA1D6, ___zSCjKsgSKyXaLUKczdjHusCWAAC_0)); }
	inline RuntimeObject* get_zSCjKsgSKyXaLUKczdjHusCWAAC_0() const { return ___zSCjKsgSKyXaLUKczdjHusCWAAC_0; }
	inline RuntimeObject** get_address_of_zSCjKsgSKyXaLUKczdjHusCWAAC_0() { return &___zSCjKsgSKyXaLUKczdjHusCWAAC_0; }
	inline void set_zSCjKsgSKyXaLUKczdjHusCWAAC_0(RuntimeObject* value)
	{
		___zSCjKsgSKyXaLUKczdjHusCWAAC_0 = value;
		Il2CppCodeGenWriteBarrier((&___zSCjKsgSKyXaLUKczdjHusCWAAC_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEBBIIEJZGOXLJGUIVQCLTDZVXC_T9E0C1572D8BD03485499BDE0D5E22C70529DA1D6_H
#ifndef UHMQJGQYOMPKFCMGEPJNASPBJID_T58930416A761A1E6E5BD3D32524AA74FBBFBB46D_H
#define UHMQJGQYOMPKFCMGEPJNASPBJID_T58930416A761A1E6E5BD3D32524AA74FBBFBB46D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_uHmqJGqYoMpKFcMgEPjnasPBJId
struct  uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_uHmqJGqYoMpKFcMgEPjnasPBJId::itTafNyeQoucrbhPkPsSqRewAEI
	int32_t ___itTafNyeQoucrbhPkPsSqRewAEI_0;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_uHmqJGqYoMpKFcMgEPjnasPBJId::tOaTpOwzcPDCHdCDIyYaeuASgAM
	int32_t ___tOaTpOwzcPDCHdCDIyYaeuASgAM_1;
	// System.Single Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_uHmqJGqYoMpKFcMgEPjnasPBJId::ccRBszFJOLqxWvGmvEVjaITEsmlB
	float ___ccRBszFJOLqxWvGmvEVjaITEsmlB_2;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_uHmqJGqYoMpKFcMgEPjnasPBJId::YJKKCodZAAdHLAznZhhKAnRrzmS
	int32_t ___YJKKCodZAAdHLAznZhhKAnRrzmS_3;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_uHmqJGqYoMpKFcMgEPjnasPBJId::wRYEGHNQAUJQPkQCpegDxMfPnyF
	int32_t ___wRYEGHNQAUJQPkQCpegDxMfPnyF_4;

public:
	inline static int32_t get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_0() { return static_cast<int32_t>(offsetof(uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D, ___itTafNyeQoucrbhPkPsSqRewAEI_0)); }
	inline int32_t get_itTafNyeQoucrbhPkPsSqRewAEI_0() const { return ___itTafNyeQoucrbhPkPsSqRewAEI_0; }
	inline int32_t* get_address_of_itTafNyeQoucrbhPkPsSqRewAEI_0() { return &___itTafNyeQoucrbhPkPsSqRewAEI_0; }
	inline void set_itTafNyeQoucrbhPkPsSqRewAEI_0(int32_t value)
	{
		___itTafNyeQoucrbhPkPsSqRewAEI_0 = value;
	}

	inline static int32_t get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_1() { return static_cast<int32_t>(offsetof(uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D, ___tOaTpOwzcPDCHdCDIyYaeuASgAM_1)); }
	inline int32_t get_tOaTpOwzcPDCHdCDIyYaeuASgAM_1() const { return ___tOaTpOwzcPDCHdCDIyYaeuASgAM_1; }
	inline int32_t* get_address_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_1() { return &___tOaTpOwzcPDCHdCDIyYaeuASgAM_1; }
	inline void set_tOaTpOwzcPDCHdCDIyYaeuASgAM_1(int32_t value)
	{
		___tOaTpOwzcPDCHdCDIyYaeuASgAM_1 = value;
	}

	inline static int32_t get_offset_of_ccRBszFJOLqxWvGmvEVjaITEsmlB_2() { return static_cast<int32_t>(offsetof(uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D, ___ccRBszFJOLqxWvGmvEVjaITEsmlB_2)); }
	inline float get_ccRBszFJOLqxWvGmvEVjaITEsmlB_2() const { return ___ccRBszFJOLqxWvGmvEVjaITEsmlB_2; }
	inline float* get_address_of_ccRBszFJOLqxWvGmvEVjaITEsmlB_2() { return &___ccRBszFJOLqxWvGmvEVjaITEsmlB_2; }
	inline void set_ccRBszFJOLqxWvGmvEVjaITEsmlB_2(float value)
	{
		___ccRBszFJOLqxWvGmvEVjaITEsmlB_2 = value;
	}

	inline static int32_t get_offset_of_YJKKCodZAAdHLAznZhhKAnRrzmS_3() { return static_cast<int32_t>(offsetof(uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D, ___YJKKCodZAAdHLAznZhhKAnRrzmS_3)); }
	inline int32_t get_YJKKCodZAAdHLAznZhhKAnRrzmS_3() const { return ___YJKKCodZAAdHLAznZhhKAnRrzmS_3; }
	inline int32_t* get_address_of_YJKKCodZAAdHLAznZhhKAnRrzmS_3() { return &___YJKKCodZAAdHLAznZhhKAnRrzmS_3; }
	inline void set_YJKKCodZAAdHLAznZhhKAnRrzmS_3(int32_t value)
	{
		___YJKKCodZAAdHLAznZhhKAnRrzmS_3 = value;
	}

	inline static int32_t get_offset_of_wRYEGHNQAUJQPkQCpegDxMfPnyF_4() { return static_cast<int32_t>(offsetof(uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D, ___wRYEGHNQAUJQPkQCpegDxMfPnyF_4)); }
	inline int32_t get_wRYEGHNQAUJQPkQCpegDxMfPnyF_4() const { return ___wRYEGHNQAUJQPkQCpegDxMfPnyF_4; }
	inline int32_t* get_address_of_wRYEGHNQAUJQPkQCpegDxMfPnyF_4() { return &___wRYEGHNQAUJQPkQCpegDxMfPnyF_4; }
	inline void set_wRYEGHNQAUJQPkQCpegDxMfPnyF_4(int32_t value)
	{
		___wRYEGHNQAUJQPkQCpegDxMfPnyF_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UHMQJGQYOMPKFCMGEPJNASPBJID_T58930416A761A1E6E5BD3D32524AA74FBBFBB46D_H
#ifndef YUATMDKIYEDTVEEKKFWOLRMGURN_T7FF3032418BD332E45EC548B8B7B6B766958AA77_H
#define YUATMDKIYEDTVEEKKFWOLRMGURN_T7FF3032418BD332E45EC548B8B7B6B766958AA77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN
struct  YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::QRPeTqgTMycFmacKSbFIvcNcyGo
	int32_t ___QRPeTqgTMycFmacKSbFIvcNcyGo_0;
	// System.Boolean Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::XOVxOqwmCFXMdtGZbdFPWpFTsTl
	bool ___XOVxOqwmCFXMdtGZbdFPWpFTsTl_1;
	// System.Int32[] Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::jUYiFkVkWPgXQbEAUrwkKsBeMRaJ
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___jUYiFkVkWPgXQbEAUrwkKsBeMRaJ_2;
	// System.Int32[] Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::BgRnmItikaXPerXhcvSYSxAmonR
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___BgRnmItikaXPerXhcvSYSxAmonR_3;
	// Rewired.Utils.Interfaces.IExternalTools Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::QbvyhOdowWTAxjeVZEkUyJRHPjq
	RuntimeObject* ___QbvyhOdowWTAxjeVZEkUyJRHPjq_4;
	// Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_imtLMjZbMBHFikNPqUtEGzfQavzx[] Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::tQPGaqjXdNaJedlKauqSntuhYwuQ
	imtLMjZbMBHFikNPqUtEGzfQavzxU5BU5D_t71EACA48282696D2E452F7CC6DA5754755767D62* ___tQPGaqjXdNaJedlKauqSntuhYwuQ_5;
	// Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_imtLMjZbMBHFikNPqUtEGzfQavzx[] Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::PSRvnRfQDraNQOZHkMFWjNnBnsh
	imtLMjZbMBHFikNPqUtEGzfQavzxU5BU5D_t71EACA48282696D2E452F7CC6DA5754755767D62* ___PSRvnRfQDraNQOZHkMFWjNnBnsh_6;
	// System.Collections.Generic.List`1<Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_PYOLdyXwCbgsmWCZaTDfMJgndAl> Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::PVvaGZfIVlrWuEwjgtaskHNFEpFh
	List_1_t696E2D4C0837937256916D2AB9A728C2EC56363B * ___PVvaGZfIVlrWuEwjgtaskHNFEpFh_7;
	// System.Collections.Generic.List`1<Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_ZFbPVMKgGgElciFwNeAzftnIMAqR> Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::sNerjSQOBhSIMSAFEuqMDxaQmBK
	List_1_tF43FF4C6B35940F952F4261B624D7CC17E9E87A7 * ___sNerjSQOBhSIMSAFEuqMDxaQmBK_8;
	// System.Action`1<Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_PYOLdyXwCbgsmWCZaTDfMJgndAl> Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::JVhoOrNuXIQFJsRhZCJOqfQeewlB
	Action_1_tAA5E5642BEC9A7A50694B8466906C3DDBE2F8341 * ___JVhoOrNuXIQFJsRhZCJOqfQeewlB_9;
	// System.Action`1<Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_ZFbPVMKgGgElciFwNeAzftnIMAqR> Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::gtrUwLnTdtgLUvCBUTRuYCiGUWX
	Action_1_tEF2248283CB5F3A04F74CC55879CB868DF0FA49B * ___gtrUwLnTdtgLUvCBUTRuYCiGUWX_10;

public:
	inline static int32_t get_offset_of_QRPeTqgTMycFmacKSbFIvcNcyGo_0() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77, ___QRPeTqgTMycFmacKSbFIvcNcyGo_0)); }
	inline int32_t get_QRPeTqgTMycFmacKSbFIvcNcyGo_0() const { return ___QRPeTqgTMycFmacKSbFIvcNcyGo_0; }
	inline int32_t* get_address_of_QRPeTqgTMycFmacKSbFIvcNcyGo_0() { return &___QRPeTqgTMycFmacKSbFIvcNcyGo_0; }
	inline void set_QRPeTqgTMycFmacKSbFIvcNcyGo_0(int32_t value)
	{
		___QRPeTqgTMycFmacKSbFIvcNcyGo_0 = value;
	}

	inline static int32_t get_offset_of_XOVxOqwmCFXMdtGZbdFPWpFTsTl_1() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77, ___XOVxOqwmCFXMdtGZbdFPWpFTsTl_1)); }
	inline bool get_XOVxOqwmCFXMdtGZbdFPWpFTsTl_1() const { return ___XOVxOqwmCFXMdtGZbdFPWpFTsTl_1; }
	inline bool* get_address_of_XOVxOqwmCFXMdtGZbdFPWpFTsTl_1() { return &___XOVxOqwmCFXMdtGZbdFPWpFTsTl_1; }
	inline void set_XOVxOqwmCFXMdtGZbdFPWpFTsTl_1(bool value)
	{
		___XOVxOqwmCFXMdtGZbdFPWpFTsTl_1 = value;
	}

	inline static int32_t get_offset_of_jUYiFkVkWPgXQbEAUrwkKsBeMRaJ_2() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77, ___jUYiFkVkWPgXQbEAUrwkKsBeMRaJ_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_jUYiFkVkWPgXQbEAUrwkKsBeMRaJ_2() const { return ___jUYiFkVkWPgXQbEAUrwkKsBeMRaJ_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_jUYiFkVkWPgXQbEAUrwkKsBeMRaJ_2() { return &___jUYiFkVkWPgXQbEAUrwkKsBeMRaJ_2; }
	inline void set_jUYiFkVkWPgXQbEAUrwkKsBeMRaJ_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___jUYiFkVkWPgXQbEAUrwkKsBeMRaJ_2 = value;
		Il2CppCodeGenWriteBarrier((&___jUYiFkVkWPgXQbEAUrwkKsBeMRaJ_2), value);
	}

	inline static int32_t get_offset_of_BgRnmItikaXPerXhcvSYSxAmonR_3() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77, ___BgRnmItikaXPerXhcvSYSxAmonR_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_BgRnmItikaXPerXhcvSYSxAmonR_3() const { return ___BgRnmItikaXPerXhcvSYSxAmonR_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_BgRnmItikaXPerXhcvSYSxAmonR_3() { return &___BgRnmItikaXPerXhcvSYSxAmonR_3; }
	inline void set_BgRnmItikaXPerXhcvSYSxAmonR_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___BgRnmItikaXPerXhcvSYSxAmonR_3 = value;
		Il2CppCodeGenWriteBarrier((&___BgRnmItikaXPerXhcvSYSxAmonR_3), value);
	}

	inline static int32_t get_offset_of_QbvyhOdowWTAxjeVZEkUyJRHPjq_4() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77, ___QbvyhOdowWTAxjeVZEkUyJRHPjq_4)); }
	inline RuntimeObject* get_QbvyhOdowWTAxjeVZEkUyJRHPjq_4() const { return ___QbvyhOdowWTAxjeVZEkUyJRHPjq_4; }
	inline RuntimeObject** get_address_of_QbvyhOdowWTAxjeVZEkUyJRHPjq_4() { return &___QbvyhOdowWTAxjeVZEkUyJRHPjq_4; }
	inline void set_QbvyhOdowWTAxjeVZEkUyJRHPjq_4(RuntimeObject* value)
	{
		___QbvyhOdowWTAxjeVZEkUyJRHPjq_4 = value;
		Il2CppCodeGenWriteBarrier((&___QbvyhOdowWTAxjeVZEkUyJRHPjq_4), value);
	}

	inline static int32_t get_offset_of_tQPGaqjXdNaJedlKauqSntuhYwuQ_5() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77, ___tQPGaqjXdNaJedlKauqSntuhYwuQ_5)); }
	inline imtLMjZbMBHFikNPqUtEGzfQavzxU5BU5D_t71EACA48282696D2E452F7CC6DA5754755767D62* get_tQPGaqjXdNaJedlKauqSntuhYwuQ_5() const { return ___tQPGaqjXdNaJedlKauqSntuhYwuQ_5; }
	inline imtLMjZbMBHFikNPqUtEGzfQavzxU5BU5D_t71EACA48282696D2E452F7CC6DA5754755767D62** get_address_of_tQPGaqjXdNaJedlKauqSntuhYwuQ_5() { return &___tQPGaqjXdNaJedlKauqSntuhYwuQ_5; }
	inline void set_tQPGaqjXdNaJedlKauqSntuhYwuQ_5(imtLMjZbMBHFikNPqUtEGzfQavzxU5BU5D_t71EACA48282696D2E452F7CC6DA5754755767D62* value)
	{
		___tQPGaqjXdNaJedlKauqSntuhYwuQ_5 = value;
		Il2CppCodeGenWriteBarrier((&___tQPGaqjXdNaJedlKauqSntuhYwuQ_5), value);
	}

	inline static int32_t get_offset_of_PSRvnRfQDraNQOZHkMFWjNnBnsh_6() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77, ___PSRvnRfQDraNQOZHkMFWjNnBnsh_6)); }
	inline imtLMjZbMBHFikNPqUtEGzfQavzxU5BU5D_t71EACA48282696D2E452F7CC6DA5754755767D62* get_PSRvnRfQDraNQOZHkMFWjNnBnsh_6() const { return ___PSRvnRfQDraNQOZHkMFWjNnBnsh_6; }
	inline imtLMjZbMBHFikNPqUtEGzfQavzxU5BU5D_t71EACA48282696D2E452F7CC6DA5754755767D62** get_address_of_PSRvnRfQDraNQOZHkMFWjNnBnsh_6() { return &___PSRvnRfQDraNQOZHkMFWjNnBnsh_6; }
	inline void set_PSRvnRfQDraNQOZHkMFWjNnBnsh_6(imtLMjZbMBHFikNPqUtEGzfQavzxU5BU5D_t71EACA48282696D2E452F7CC6DA5754755767D62* value)
	{
		___PSRvnRfQDraNQOZHkMFWjNnBnsh_6 = value;
		Il2CppCodeGenWriteBarrier((&___PSRvnRfQDraNQOZHkMFWjNnBnsh_6), value);
	}

	inline static int32_t get_offset_of_PVvaGZfIVlrWuEwjgtaskHNFEpFh_7() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77, ___PVvaGZfIVlrWuEwjgtaskHNFEpFh_7)); }
	inline List_1_t696E2D4C0837937256916D2AB9A728C2EC56363B * get_PVvaGZfIVlrWuEwjgtaskHNFEpFh_7() const { return ___PVvaGZfIVlrWuEwjgtaskHNFEpFh_7; }
	inline List_1_t696E2D4C0837937256916D2AB9A728C2EC56363B ** get_address_of_PVvaGZfIVlrWuEwjgtaskHNFEpFh_7() { return &___PVvaGZfIVlrWuEwjgtaskHNFEpFh_7; }
	inline void set_PVvaGZfIVlrWuEwjgtaskHNFEpFh_7(List_1_t696E2D4C0837937256916D2AB9A728C2EC56363B * value)
	{
		___PVvaGZfIVlrWuEwjgtaskHNFEpFh_7 = value;
		Il2CppCodeGenWriteBarrier((&___PVvaGZfIVlrWuEwjgtaskHNFEpFh_7), value);
	}

	inline static int32_t get_offset_of_sNerjSQOBhSIMSAFEuqMDxaQmBK_8() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77, ___sNerjSQOBhSIMSAFEuqMDxaQmBK_8)); }
	inline List_1_tF43FF4C6B35940F952F4261B624D7CC17E9E87A7 * get_sNerjSQOBhSIMSAFEuqMDxaQmBK_8() const { return ___sNerjSQOBhSIMSAFEuqMDxaQmBK_8; }
	inline List_1_tF43FF4C6B35940F952F4261B624D7CC17E9E87A7 ** get_address_of_sNerjSQOBhSIMSAFEuqMDxaQmBK_8() { return &___sNerjSQOBhSIMSAFEuqMDxaQmBK_8; }
	inline void set_sNerjSQOBhSIMSAFEuqMDxaQmBK_8(List_1_tF43FF4C6B35940F952F4261B624D7CC17E9E87A7 * value)
	{
		___sNerjSQOBhSIMSAFEuqMDxaQmBK_8 = value;
		Il2CppCodeGenWriteBarrier((&___sNerjSQOBhSIMSAFEuqMDxaQmBK_8), value);
	}

	inline static int32_t get_offset_of_JVhoOrNuXIQFJsRhZCJOqfQeewlB_9() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77, ___JVhoOrNuXIQFJsRhZCJOqfQeewlB_9)); }
	inline Action_1_tAA5E5642BEC9A7A50694B8466906C3DDBE2F8341 * get_JVhoOrNuXIQFJsRhZCJOqfQeewlB_9() const { return ___JVhoOrNuXIQFJsRhZCJOqfQeewlB_9; }
	inline Action_1_tAA5E5642BEC9A7A50694B8466906C3DDBE2F8341 ** get_address_of_JVhoOrNuXIQFJsRhZCJOqfQeewlB_9() { return &___JVhoOrNuXIQFJsRhZCJOqfQeewlB_9; }
	inline void set_JVhoOrNuXIQFJsRhZCJOqfQeewlB_9(Action_1_tAA5E5642BEC9A7A50694B8466906C3DDBE2F8341 * value)
	{
		___JVhoOrNuXIQFJsRhZCJOqfQeewlB_9 = value;
		Il2CppCodeGenWriteBarrier((&___JVhoOrNuXIQFJsRhZCJOqfQeewlB_9), value);
	}

	inline static int32_t get_offset_of_gtrUwLnTdtgLUvCBUTRuYCiGUWX_10() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77, ___gtrUwLnTdtgLUvCBUTRuYCiGUWX_10)); }
	inline Action_1_tEF2248283CB5F3A04F74CC55879CB868DF0FA49B * get_gtrUwLnTdtgLUvCBUTRuYCiGUWX_10() const { return ___gtrUwLnTdtgLUvCBUTRuYCiGUWX_10; }
	inline Action_1_tEF2248283CB5F3A04F74CC55879CB868DF0FA49B ** get_address_of_gtrUwLnTdtgLUvCBUTRuYCiGUWX_10() { return &___gtrUwLnTdtgLUvCBUTRuYCiGUWX_10; }
	inline void set_gtrUwLnTdtgLUvCBUTRuYCiGUWX_10(Action_1_tEF2248283CB5F3A04F74CC55879CB868DF0FA49B * value)
	{
		___gtrUwLnTdtgLUvCBUTRuYCiGUWX_10 = value;
		Il2CppCodeGenWriteBarrier((&___gtrUwLnTdtgLUvCBUTRuYCiGUWX_10), value);
	}
};

struct YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77_StaticFields
{
public:
	// System.Func`1<Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_imtLMjZbMBHFikNPqUtEGzfQavzx> Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::xuKcWqHPyZZLojTegosyfuffNaVL
	Func_1_tAA0F6C5612DAF65CB2AA348046536923DA35B777 * ___xuKcWqHPyZZLojTegosyfuffNaVL_11;
	// System.Func`1<Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_imtLMjZbMBHFikNPqUtEGzfQavzx> Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN::xgFcMzbcKwfNhKMwwFcokhPKEXIb
	Func_1_tAA0F6C5612DAF65CB2AA348046536923DA35B777 * ___xgFcMzbcKwfNhKMwwFcokhPKEXIb_12;

public:
	inline static int32_t get_offset_of_xuKcWqHPyZZLojTegosyfuffNaVL_11() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77_StaticFields, ___xuKcWqHPyZZLojTegosyfuffNaVL_11)); }
	inline Func_1_tAA0F6C5612DAF65CB2AA348046536923DA35B777 * get_xuKcWqHPyZZLojTegosyfuffNaVL_11() const { return ___xuKcWqHPyZZLojTegosyfuffNaVL_11; }
	inline Func_1_tAA0F6C5612DAF65CB2AA348046536923DA35B777 ** get_address_of_xuKcWqHPyZZLojTegosyfuffNaVL_11() { return &___xuKcWqHPyZZLojTegosyfuffNaVL_11; }
	inline void set_xuKcWqHPyZZLojTegosyfuffNaVL_11(Func_1_tAA0F6C5612DAF65CB2AA348046536923DA35B777 * value)
	{
		___xuKcWqHPyZZLojTegosyfuffNaVL_11 = value;
		Il2CppCodeGenWriteBarrier((&___xuKcWqHPyZZLojTegosyfuffNaVL_11), value);
	}

	inline static int32_t get_offset_of_xgFcMzbcKwfNhKMwwFcokhPKEXIb_12() { return static_cast<int32_t>(offsetof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77_StaticFields, ___xgFcMzbcKwfNhKMwwFcokhPKEXIb_12)); }
	inline Func_1_tAA0F6C5612DAF65CB2AA348046536923DA35B777 * get_xgFcMzbcKwfNhKMwwFcokhPKEXIb_12() const { return ___xgFcMzbcKwfNhKMwwFcokhPKEXIb_12; }
	inline Func_1_tAA0F6C5612DAF65CB2AA348046536923DA35B777 ** get_address_of_xgFcMzbcKwfNhKMwwFcokhPKEXIb_12() { return &___xgFcMzbcKwfNhKMwwFcokhPKEXIb_12; }
	inline void set_xgFcMzbcKwfNhKMwwFcokhPKEXIb_12(Func_1_tAA0F6C5612DAF65CB2AA348046536923DA35B777 * value)
	{
		___xgFcMzbcKwfNhKMwwFcokhPKEXIb_12 = value;
		Il2CppCodeGenWriteBarrier((&___xgFcMzbcKwfNhKMwwFcokhPKEXIb_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YUATMDKIYEDTVEEKKFWOLRMGURN_T7FF3032418BD332E45EC548B8B7B6B766958AA77_H
#ifndef IMTLMJZBMBHFIKNPQUTEGZFQAVZX_TE4B478119BDA06E0C67ADCBA34F5CEBD76F19215_H
#define IMTLMJZBMBHFIKNPQUTEGZFQAVZX_TE4B478119BDA06E0C67ADCBA34F5CEBD76F19215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_imtLMjZbMBHFikNPqUtEGzfQavzx
struct  imtLMjZbMBHFikNPqUtEGzfQavzx_tE4B478119BDA06E0C67ADCBA34F5CEBD76F19215  : public RuntimeObject
{
public:
	// System.Boolean Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_imtLMjZbMBHFikNPqUtEGzfQavzx::pTXqUwcYdENkAAHGFsRzWXKXrZB
	bool ___pTXqUwcYdENkAAHGFsRzWXKXrZB_0;
	// System.Boolean Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_imtLMjZbMBHFikNPqUtEGzfQavzx::CvmowhMEGZETWwhyZsLYVgJGqPp
	bool ___CvmowhMEGZETWwhyZsLYVgJGqPp_1;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_imtLMjZbMBHFikNPqUtEGzfQavzx::zSkfqYfrOXJXcopeOkTalFrxsHk
	int32_t ___zSkfqYfrOXJXcopeOkTalFrxsHk_2;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_imtLMjZbMBHFikNPqUtEGzfQavzx::GWNhMohgMSKpOKjKZrAVmDEaFCd
	int32_t ___GWNhMohgMSKpOKjKZrAVmDEaFCd_3;

public:
	inline static int32_t get_offset_of_pTXqUwcYdENkAAHGFsRzWXKXrZB_0() { return static_cast<int32_t>(offsetof(imtLMjZbMBHFikNPqUtEGzfQavzx_tE4B478119BDA06E0C67ADCBA34F5CEBD76F19215, ___pTXqUwcYdENkAAHGFsRzWXKXrZB_0)); }
	inline bool get_pTXqUwcYdENkAAHGFsRzWXKXrZB_0() const { return ___pTXqUwcYdENkAAHGFsRzWXKXrZB_0; }
	inline bool* get_address_of_pTXqUwcYdENkAAHGFsRzWXKXrZB_0() { return &___pTXqUwcYdENkAAHGFsRzWXKXrZB_0; }
	inline void set_pTXqUwcYdENkAAHGFsRzWXKXrZB_0(bool value)
	{
		___pTXqUwcYdENkAAHGFsRzWXKXrZB_0 = value;
	}

	inline static int32_t get_offset_of_CvmowhMEGZETWwhyZsLYVgJGqPp_1() { return static_cast<int32_t>(offsetof(imtLMjZbMBHFikNPqUtEGzfQavzx_tE4B478119BDA06E0C67ADCBA34F5CEBD76F19215, ___CvmowhMEGZETWwhyZsLYVgJGqPp_1)); }
	inline bool get_CvmowhMEGZETWwhyZsLYVgJGqPp_1() const { return ___CvmowhMEGZETWwhyZsLYVgJGqPp_1; }
	inline bool* get_address_of_CvmowhMEGZETWwhyZsLYVgJGqPp_1() { return &___CvmowhMEGZETWwhyZsLYVgJGqPp_1; }
	inline void set_CvmowhMEGZETWwhyZsLYVgJGqPp_1(bool value)
	{
		___CvmowhMEGZETWwhyZsLYVgJGqPp_1 = value;
	}

	inline static int32_t get_offset_of_zSkfqYfrOXJXcopeOkTalFrxsHk_2() { return static_cast<int32_t>(offsetof(imtLMjZbMBHFikNPqUtEGzfQavzx_tE4B478119BDA06E0C67ADCBA34F5CEBD76F19215, ___zSkfqYfrOXJXcopeOkTalFrxsHk_2)); }
	inline int32_t get_zSkfqYfrOXJXcopeOkTalFrxsHk_2() const { return ___zSkfqYfrOXJXcopeOkTalFrxsHk_2; }
	inline int32_t* get_address_of_zSkfqYfrOXJXcopeOkTalFrxsHk_2() { return &___zSkfqYfrOXJXcopeOkTalFrxsHk_2; }
	inline void set_zSkfqYfrOXJXcopeOkTalFrxsHk_2(int32_t value)
	{
		___zSkfqYfrOXJXcopeOkTalFrxsHk_2 = value;
	}

	inline static int32_t get_offset_of_GWNhMohgMSKpOKjKZrAVmDEaFCd_3() { return static_cast<int32_t>(offsetof(imtLMjZbMBHFikNPqUtEGzfQavzx_tE4B478119BDA06E0C67ADCBA34F5CEBD76F19215, ___GWNhMohgMSKpOKjKZrAVmDEaFCd_3)); }
	inline int32_t get_GWNhMohgMSKpOKjKZrAVmDEaFCd_3() const { return ___GWNhMohgMSKpOKjKZrAVmDEaFCd_3; }
	inline int32_t* get_address_of_GWNhMohgMSKpOKjKZrAVmDEaFCd_3() { return &___GWNhMohgMSKpOKjKZrAVmDEaFCd_3; }
	inline void set_GWNhMohgMSKpOKjKZrAVmDEaFCd_3(int32_t value)
	{
		___GWNhMohgMSKpOKjKZrAVmDEaFCd_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMTLMJZBMBHFIKNPQUTEGZFQAVZX_TE4B478119BDA06E0C67ADCBA34F5CEBD76F19215_H
#ifndef GYRICTMQPYLIDAFPGCLNPVHBLMN_TD6843F94E9C0E3CF940D28B26610C7B317AB3377_H
#define GYRICTMQPYLIDAFPGCLNPVHBLMN_TD6843F94E9C0E3CF940D28B26610C7B317AB3377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_GyRicTMqpYlIDAFpGCLnPVhblMN
struct  GyRicTMqpYlIDAFpGCLnPVhblMN_tD6843F94E9C0E3CF940D28B26610C7B317AB3377  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Rewired.Player_ControllerHelper_GyRicTMqpYlIDAFpGCLnPVhblMN_UuxNpDFxmKnoSYsrSKsxwQtUnoq> Rewired.Player_ControllerHelper_GyRicTMqpYlIDAFpGCLnPVhblMN::AHOFIsQodiSzMcHMSUpdlsGdKfH
	List_1_tB5FE22B09DC001511E810D14AE2AF6389FB53D6C * ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0;
	// Rewired.Player Rewired.Player_ControllerHelper_GyRicTMqpYlIDAFpGCLnPVhblMN::DelHerRPxmEUeLMSQbMNIPccOrs
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___DelHerRPxmEUeLMSQbMNIPccOrs_1;

public:
	inline static int32_t get_offset_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() { return static_cast<int32_t>(offsetof(GyRicTMqpYlIDAFpGCLnPVhblMN_tD6843F94E9C0E3CF940D28B26610C7B317AB3377, ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0)); }
	inline List_1_tB5FE22B09DC001511E810D14AE2AF6389FB53D6C * get_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() const { return ___AHOFIsQodiSzMcHMSUpdlsGdKfH_0; }
	inline List_1_tB5FE22B09DC001511E810D14AE2AF6389FB53D6C ** get_address_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0() { return &___AHOFIsQodiSzMcHMSUpdlsGdKfH_0; }
	inline void set_AHOFIsQodiSzMcHMSUpdlsGdKfH_0(List_1_tB5FE22B09DC001511E810D14AE2AF6389FB53D6C * value)
	{
		___AHOFIsQodiSzMcHMSUpdlsGdKfH_0 = value;
		Il2CppCodeGenWriteBarrier((&___AHOFIsQodiSzMcHMSUpdlsGdKfH_0), value);
	}

	inline static int32_t get_offset_of_DelHerRPxmEUeLMSQbMNIPccOrs_1() { return static_cast<int32_t>(offsetof(GyRicTMqpYlIDAFpGCLnPVhblMN_tD6843F94E9C0E3CF940D28B26610C7B317AB3377, ___DelHerRPxmEUeLMSQbMNIPccOrs_1)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_DelHerRPxmEUeLMSQbMNIPccOrs_1() const { return ___DelHerRPxmEUeLMSQbMNIPccOrs_1; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_DelHerRPxmEUeLMSQbMNIPccOrs_1() { return &___DelHerRPxmEUeLMSQbMNIPccOrs_1; }
	inline void set_DelHerRPxmEUeLMSQbMNIPccOrs_1(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___DelHerRPxmEUeLMSQbMNIPccOrs_1 = value;
		Il2CppCodeGenWriteBarrier((&___DelHerRPxmEUeLMSQbMNIPccOrs_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYRICTMQPYLIDAFPGCLNPVHBLMN_TD6843F94E9C0E3CF940D28B26610C7B317AB3377_H
#ifndef UUXNPDFXMKNOSYSRSKSXWQTUNOQ_TA04956336843A1F38733551F40594502C55158AB_H
#define UUXNPDFXMKNOSYSRSKSXWQTUNOQ_TA04956336843A1F38733551F40594502C55158AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_GyRicTMqpYlIDAFpGCLnPVhblMN_UuxNpDFxmKnoSYsrSKsxwQtUnoq
struct  UuxNpDFxmKnoSYsrSKsxwQtUnoq_tA04956336843A1F38733551F40594502C55158AB  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Player_ControllerHelper_GyRicTMqpYlIDAFpGCLnPVhblMN_UuxNpDFxmKnoSYsrSKsxwQtUnoq::hMBbQHSYpxrZxtjFPBFvFGsQNmg
	int32_t ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_0;
	// lVPGlCbkNvrVSnFFcfgwEyebHaly<Rewired.JoystickMap> Rewired.Player_ControllerHelper_GyRicTMqpYlIDAFpGCLnPVhblMN_UuxNpDFxmKnoSYsrSKsxwQtUnoq::IRMhfpXtgdEZHjFGxjGEgBwVQnrW
	lVPGlCbkNvrVSnFFcfgwEyebHaly_tC68E73CDA2F1278DDA1B6B1CED2305F6EE35223A * ___IRMhfpXtgdEZHjFGxjGEgBwVQnrW_1;
	// System.Single Rewired.Player_ControllerHelper_GyRicTMqpYlIDAFpGCLnPVhblMN_UuxNpDFxmKnoSYsrSKsxwQtUnoq::EakTGTTOWHojTaRbVQcQfpoXwjU
	float ___EakTGTTOWHojTaRbVQcQfpoXwjU_2;

public:
	inline static int32_t get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_0() { return static_cast<int32_t>(offsetof(UuxNpDFxmKnoSYsrSKsxwQtUnoq_tA04956336843A1F38733551F40594502C55158AB, ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_0)); }
	inline int32_t get_hMBbQHSYpxrZxtjFPBFvFGsQNmg_0() const { return ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_0; }
	inline int32_t* get_address_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_0() { return &___hMBbQHSYpxrZxtjFPBFvFGsQNmg_0; }
	inline void set_hMBbQHSYpxrZxtjFPBFvFGsQNmg_0(int32_t value)
	{
		___hMBbQHSYpxrZxtjFPBFvFGsQNmg_0 = value;
	}

	inline static int32_t get_offset_of_IRMhfpXtgdEZHjFGxjGEgBwVQnrW_1() { return static_cast<int32_t>(offsetof(UuxNpDFxmKnoSYsrSKsxwQtUnoq_tA04956336843A1F38733551F40594502C55158AB, ___IRMhfpXtgdEZHjFGxjGEgBwVQnrW_1)); }
	inline lVPGlCbkNvrVSnFFcfgwEyebHaly_tC68E73CDA2F1278DDA1B6B1CED2305F6EE35223A * get_IRMhfpXtgdEZHjFGxjGEgBwVQnrW_1() const { return ___IRMhfpXtgdEZHjFGxjGEgBwVQnrW_1; }
	inline lVPGlCbkNvrVSnFFcfgwEyebHaly_tC68E73CDA2F1278DDA1B6B1CED2305F6EE35223A ** get_address_of_IRMhfpXtgdEZHjFGxjGEgBwVQnrW_1() { return &___IRMhfpXtgdEZHjFGxjGEgBwVQnrW_1; }
	inline void set_IRMhfpXtgdEZHjFGxjGEgBwVQnrW_1(lVPGlCbkNvrVSnFFcfgwEyebHaly_tC68E73CDA2F1278DDA1B6B1CED2305F6EE35223A * value)
	{
		___IRMhfpXtgdEZHjFGxjGEgBwVQnrW_1 = value;
		Il2CppCodeGenWriteBarrier((&___IRMhfpXtgdEZHjFGxjGEgBwVQnrW_1), value);
	}

	inline static int32_t get_offset_of_EakTGTTOWHojTaRbVQcQfpoXwjU_2() { return static_cast<int32_t>(offsetof(UuxNpDFxmKnoSYsrSKsxwQtUnoq_tA04956336843A1F38733551F40594502C55158AB, ___EakTGTTOWHojTaRbVQcQfpoXwjU_2)); }
	inline float get_EakTGTTOWHojTaRbVQcQfpoXwjU_2() const { return ___EakTGTTOWHojTaRbVQcQfpoXwjU_2; }
	inline float* get_address_of_EakTGTTOWHojTaRbVQcQfpoXwjU_2() { return &___EakTGTTOWHojTaRbVQcQfpoXwjU_2; }
	inline void set_EakTGTTOWHojTaRbVQcQfpoXwjU_2(float value)
	{
		___EakTGTTOWHojTaRbVQcQfpoXwjU_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UUXNPDFXMKNOSYSRSKSXWQTUNOQ_TA04956336843A1F38733551F40594502C55158AB_H
#ifndef HHKDGJBXKPDLDTTIHEQFDZFOBJDI_TB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9_H
#define HHKDGJBXKPDLDTTIHEQFDZFOBJDI_TB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_HhKDgjBXKPDLdtTIHEqFdZFOBJDi
struct  HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9  : public RuntimeObject
{
public:
	// Rewired.Controller Rewired.Player_ControllerHelper_HhKDgjBXKPDLdtTIHEqFdZFOBJDi::NOArrsNdfKDShNFftfbPqYDzhpq
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_HhKDgjBXKPDLdtTIHEqFdZFOBJDi::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_HhKDgjBXKPDLdtTIHEqFdZFOBJDi::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper Rewired.Player_ControllerHelper_HhKDgjBXKPDLdtTIHEqFdZFOBJDi::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_HhKDgjBXKPDLdtTIHEqFdZFOBJDi::tJsKQNEqXVemDDJIXCshTUBAPRyM
	int32_t ___tJsKQNEqXVemDDJIXCshTUBAPRyM_4;
	// System.Collections.Generic.IList`1<Rewired.Joystick> Rewired.Player_ControllerHelper_HhKDgjBXKPDLdtTIHEqFdZFOBJDi::vmYQYvHTiaaVZFFcFKtCffENjjo
	RuntimeObject* ___vmYQYvHTiaaVZFFcFKtCffENjjo_5;
	// System.Int32 Rewired.Player_ControllerHelper_HhKDgjBXKPDLdtTIHEqFdZFOBJDi::ovZIilmHEFYPccoHmpoAPVWKsJT
	int32_t ___ovZIilmHEFYPccoHmpoAPVWKsJT_6;
	// System.Int32 Rewired.Player_ControllerHelper_HhKDgjBXKPDLdtTIHEqFdZFOBJDi::xavlzvxgHIwtBjtgQbjNTqeDVNH
	int32_t ___xavlzvxgHIwtBjtgQbjNTqeDVNH_7;
	// System.Collections.Generic.IList`1<Rewired.CustomController> Rewired.Player_ControllerHelper_HhKDgjBXKPDLdtTIHEqFdZFOBJDi::ktEOrCXXgBpIFwKvshCEnMEzpmG
	RuntimeObject* ___ktEOrCXXgBpIFwKvshCEnMEzpmG_8;
	// System.Int32 Rewired.Player_ControllerHelper_HhKDgjBXKPDLdtTIHEqFdZFOBJDi::CZPvdsJztOTzkufRzOxSFjclfQC
	int32_t ___CZPvdsJztOTzkufRzOxSFjclfQC_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_tJsKQNEqXVemDDJIXCshTUBAPRyM_4() { return static_cast<int32_t>(offsetof(HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9, ___tJsKQNEqXVemDDJIXCshTUBAPRyM_4)); }
	inline int32_t get_tJsKQNEqXVemDDJIXCshTUBAPRyM_4() const { return ___tJsKQNEqXVemDDJIXCshTUBAPRyM_4; }
	inline int32_t* get_address_of_tJsKQNEqXVemDDJIXCshTUBAPRyM_4() { return &___tJsKQNEqXVemDDJIXCshTUBAPRyM_4; }
	inline void set_tJsKQNEqXVemDDJIXCshTUBAPRyM_4(int32_t value)
	{
		___tJsKQNEqXVemDDJIXCshTUBAPRyM_4 = value;
	}

	inline static int32_t get_offset_of_vmYQYvHTiaaVZFFcFKtCffENjjo_5() { return static_cast<int32_t>(offsetof(HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9, ___vmYQYvHTiaaVZFFcFKtCffENjjo_5)); }
	inline RuntimeObject* get_vmYQYvHTiaaVZFFcFKtCffENjjo_5() const { return ___vmYQYvHTiaaVZFFcFKtCffENjjo_5; }
	inline RuntimeObject** get_address_of_vmYQYvHTiaaVZFFcFKtCffENjjo_5() { return &___vmYQYvHTiaaVZFFcFKtCffENjjo_5; }
	inline void set_vmYQYvHTiaaVZFFcFKtCffENjjo_5(RuntimeObject* value)
	{
		___vmYQYvHTiaaVZFFcFKtCffENjjo_5 = value;
		Il2CppCodeGenWriteBarrier((&___vmYQYvHTiaaVZFFcFKtCffENjjo_5), value);
	}

	inline static int32_t get_offset_of_ovZIilmHEFYPccoHmpoAPVWKsJT_6() { return static_cast<int32_t>(offsetof(HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9, ___ovZIilmHEFYPccoHmpoAPVWKsJT_6)); }
	inline int32_t get_ovZIilmHEFYPccoHmpoAPVWKsJT_6() const { return ___ovZIilmHEFYPccoHmpoAPVWKsJT_6; }
	inline int32_t* get_address_of_ovZIilmHEFYPccoHmpoAPVWKsJT_6() { return &___ovZIilmHEFYPccoHmpoAPVWKsJT_6; }
	inline void set_ovZIilmHEFYPccoHmpoAPVWKsJT_6(int32_t value)
	{
		___ovZIilmHEFYPccoHmpoAPVWKsJT_6 = value;
	}

	inline static int32_t get_offset_of_xavlzvxgHIwtBjtgQbjNTqeDVNH_7() { return static_cast<int32_t>(offsetof(HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9, ___xavlzvxgHIwtBjtgQbjNTqeDVNH_7)); }
	inline int32_t get_xavlzvxgHIwtBjtgQbjNTqeDVNH_7() const { return ___xavlzvxgHIwtBjtgQbjNTqeDVNH_7; }
	inline int32_t* get_address_of_xavlzvxgHIwtBjtgQbjNTqeDVNH_7() { return &___xavlzvxgHIwtBjtgQbjNTqeDVNH_7; }
	inline void set_xavlzvxgHIwtBjtgQbjNTqeDVNH_7(int32_t value)
	{
		___xavlzvxgHIwtBjtgQbjNTqeDVNH_7 = value;
	}

	inline static int32_t get_offset_of_ktEOrCXXgBpIFwKvshCEnMEzpmG_8() { return static_cast<int32_t>(offsetof(HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9, ___ktEOrCXXgBpIFwKvshCEnMEzpmG_8)); }
	inline RuntimeObject* get_ktEOrCXXgBpIFwKvshCEnMEzpmG_8() const { return ___ktEOrCXXgBpIFwKvshCEnMEzpmG_8; }
	inline RuntimeObject** get_address_of_ktEOrCXXgBpIFwKvshCEnMEzpmG_8() { return &___ktEOrCXXgBpIFwKvshCEnMEzpmG_8; }
	inline void set_ktEOrCXXgBpIFwKvshCEnMEzpmG_8(RuntimeObject* value)
	{
		___ktEOrCXXgBpIFwKvshCEnMEzpmG_8 = value;
		Il2CppCodeGenWriteBarrier((&___ktEOrCXXgBpIFwKvshCEnMEzpmG_8), value);
	}

	inline static int32_t get_offset_of_CZPvdsJztOTzkufRzOxSFjclfQC_9() { return static_cast<int32_t>(offsetof(HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9, ___CZPvdsJztOTzkufRzOxSFjclfQC_9)); }
	inline int32_t get_CZPvdsJztOTzkufRzOxSFjclfQC_9() const { return ___CZPvdsJztOTzkufRzOxSFjclfQC_9; }
	inline int32_t* get_address_of_CZPvdsJztOTzkufRzOxSFjclfQC_9() { return &___CZPvdsJztOTzkufRzOxSFjclfQC_9; }
	inline void set_CZPvdsJztOTzkufRzOxSFjclfQC_9(int32_t value)
	{
		___CZPvdsJztOTzkufRzOxSFjclfQC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HHKDGJBXKPDLDTTIHEQFDZFOBJDI_TB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9_H
#ifndef BGWKAJHNNSHLWJGQRMXHDSGALBYJ_T2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24_H
#define BGWKAJHNNSHLWJGQRMXHDSGALBYJ_T2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ
struct  BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_4;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::vJSqbRZAIniSbEUyqTHpxuFtYNw
	int32_t ___vJSqbRZAIniSbEUyqTHpxuFtYNw_8;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::VMDLPkTgOreHqOOGhLoNRYnwvXm
	int32_t ___VMDLPkTgOreHqOOGhLoNRYnwvXm_9;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::mLoJhLWnQrndurMnPgZbVzWqubv
	RuntimeObject* ___mLoJhLWnQrndurMnPgZbVzWqubv_10;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::wDDeUqbJHasblCouLgRvDsWeBjxX
	int32_t ___wDDeUqbJHasblCouLgRvDsWeBjxX_11;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::KXjZGRrVrxPNijmBloVSMdblfnD
	int32_t ___KXjZGRrVrxPNijmBloVSMdblfnD_12;
	// JrKetNtIJINuYILTEYSwNUvgPiW Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::jeFKojDJfJOTICGSoOZNdenYJzm
	RuntimeObject* ___jeFKojDJfJOTICGSoOZNdenYJzm_13;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::sGSfdVMWlUIzKizeKzVqIOPYzZL
	int32_t ___sGSfdVMWlUIzKizeKzVqIOPYzZL_14;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::KEefkDDHpJgSzZksycmQdqHFpQA
	int32_t ___KEefkDDHpJgSzZksycmQdqHFpQA_15;
	// Rewired.ControllerMap Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::LzJEikbMnyDvpBsogaRuCRFyooLn
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___LzJEikbMnyDvpBsogaRuCRFyooLn_16;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::WXFtcsBJoAEMLFABEPTRqAJvkUUF
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___WXFtcsBJoAEMLFABEPTRqAJvkUUF_17;
	// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap> Rewired.Player_ControllerHelper_MapHelper_BgwkajhnNshLWjGQRMxHdSGALByJ::xKgDcMKGQQaxGajsXNhNAbtUgoI
	RuntimeObject* ___xKgDcMKGQQaxGajsXNhNAbtUgoI_18;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___pTVJDeVeJlQThAZkFztHqDAWDUa_4)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_4() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_4; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_4; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_4(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_4 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_vJSqbRZAIniSbEUyqTHpxuFtYNw_8() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___vJSqbRZAIniSbEUyqTHpxuFtYNw_8)); }
	inline int32_t get_vJSqbRZAIniSbEUyqTHpxuFtYNw_8() const { return ___vJSqbRZAIniSbEUyqTHpxuFtYNw_8; }
	inline int32_t* get_address_of_vJSqbRZAIniSbEUyqTHpxuFtYNw_8() { return &___vJSqbRZAIniSbEUyqTHpxuFtYNw_8; }
	inline void set_vJSqbRZAIniSbEUyqTHpxuFtYNw_8(int32_t value)
	{
		___vJSqbRZAIniSbEUyqTHpxuFtYNw_8 = value;
	}

	inline static int32_t get_offset_of_VMDLPkTgOreHqOOGhLoNRYnwvXm_9() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___VMDLPkTgOreHqOOGhLoNRYnwvXm_9)); }
	inline int32_t get_VMDLPkTgOreHqOOGhLoNRYnwvXm_9() const { return ___VMDLPkTgOreHqOOGhLoNRYnwvXm_9; }
	inline int32_t* get_address_of_VMDLPkTgOreHqOOGhLoNRYnwvXm_9() { return &___VMDLPkTgOreHqOOGhLoNRYnwvXm_9; }
	inline void set_VMDLPkTgOreHqOOGhLoNRYnwvXm_9(int32_t value)
	{
		___VMDLPkTgOreHqOOGhLoNRYnwvXm_9 = value;
	}

	inline static int32_t get_offset_of_mLoJhLWnQrndurMnPgZbVzWqubv_10() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___mLoJhLWnQrndurMnPgZbVzWqubv_10)); }
	inline RuntimeObject* get_mLoJhLWnQrndurMnPgZbVzWqubv_10() const { return ___mLoJhLWnQrndurMnPgZbVzWqubv_10; }
	inline RuntimeObject** get_address_of_mLoJhLWnQrndurMnPgZbVzWqubv_10() { return &___mLoJhLWnQrndurMnPgZbVzWqubv_10; }
	inline void set_mLoJhLWnQrndurMnPgZbVzWqubv_10(RuntimeObject* value)
	{
		___mLoJhLWnQrndurMnPgZbVzWqubv_10 = value;
		Il2CppCodeGenWriteBarrier((&___mLoJhLWnQrndurMnPgZbVzWqubv_10), value);
	}

	inline static int32_t get_offset_of_wDDeUqbJHasblCouLgRvDsWeBjxX_11() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___wDDeUqbJHasblCouLgRvDsWeBjxX_11)); }
	inline int32_t get_wDDeUqbJHasblCouLgRvDsWeBjxX_11() const { return ___wDDeUqbJHasblCouLgRvDsWeBjxX_11; }
	inline int32_t* get_address_of_wDDeUqbJHasblCouLgRvDsWeBjxX_11() { return &___wDDeUqbJHasblCouLgRvDsWeBjxX_11; }
	inline void set_wDDeUqbJHasblCouLgRvDsWeBjxX_11(int32_t value)
	{
		___wDDeUqbJHasblCouLgRvDsWeBjxX_11 = value;
	}

	inline static int32_t get_offset_of_KXjZGRrVrxPNijmBloVSMdblfnD_12() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___KXjZGRrVrxPNijmBloVSMdblfnD_12)); }
	inline int32_t get_KXjZGRrVrxPNijmBloVSMdblfnD_12() const { return ___KXjZGRrVrxPNijmBloVSMdblfnD_12; }
	inline int32_t* get_address_of_KXjZGRrVrxPNijmBloVSMdblfnD_12() { return &___KXjZGRrVrxPNijmBloVSMdblfnD_12; }
	inline void set_KXjZGRrVrxPNijmBloVSMdblfnD_12(int32_t value)
	{
		___KXjZGRrVrxPNijmBloVSMdblfnD_12 = value;
	}

	inline static int32_t get_offset_of_jeFKojDJfJOTICGSoOZNdenYJzm_13() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___jeFKojDJfJOTICGSoOZNdenYJzm_13)); }
	inline RuntimeObject* get_jeFKojDJfJOTICGSoOZNdenYJzm_13() const { return ___jeFKojDJfJOTICGSoOZNdenYJzm_13; }
	inline RuntimeObject** get_address_of_jeFKojDJfJOTICGSoOZNdenYJzm_13() { return &___jeFKojDJfJOTICGSoOZNdenYJzm_13; }
	inline void set_jeFKojDJfJOTICGSoOZNdenYJzm_13(RuntimeObject* value)
	{
		___jeFKojDJfJOTICGSoOZNdenYJzm_13 = value;
		Il2CppCodeGenWriteBarrier((&___jeFKojDJfJOTICGSoOZNdenYJzm_13), value);
	}

	inline static int32_t get_offset_of_sGSfdVMWlUIzKizeKzVqIOPYzZL_14() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___sGSfdVMWlUIzKizeKzVqIOPYzZL_14)); }
	inline int32_t get_sGSfdVMWlUIzKizeKzVqIOPYzZL_14() const { return ___sGSfdVMWlUIzKizeKzVqIOPYzZL_14; }
	inline int32_t* get_address_of_sGSfdVMWlUIzKizeKzVqIOPYzZL_14() { return &___sGSfdVMWlUIzKizeKzVqIOPYzZL_14; }
	inline void set_sGSfdVMWlUIzKizeKzVqIOPYzZL_14(int32_t value)
	{
		___sGSfdVMWlUIzKizeKzVqIOPYzZL_14 = value;
	}

	inline static int32_t get_offset_of_KEefkDDHpJgSzZksycmQdqHFpQA_15() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___KEefkDDHpJgSzZksycmQdqHFpQA_15)); }
	inline int32_t get_KEefkDDHpJgSzZksycmQdqHFpQA_15() const { return ___KEefkDDHpJgSzZksycmQdqHFpQA_15; }
	inline int32_t* get_address_of_KEefkDDHpJgSzZksycmQdqHFpQA_15() { return &___KEefkDDHpJgSzZksycmQdqHFpQA_15; }
	inline void set_KEefkDDHpJgSzZksycmQdqHFpQA_15(int32_t value)
	{
		___KEefkDDHpJgSzZksycmQdqHFpQA_15 = value;
	}

	inline static int32_t get_offset_of_LzJEikbMnyDvpBsogaRuCRFyooLn_16() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___LzJEikbMnyDvpBsogaRuCRFyooLn_16)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_LzJEikbMnyDvpBsogaRuCRFyooLn_16() const { return ___LzJEikbMnyDvpBsogaRuCRFyooLn_16; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_LzJEikbMnyDvpBsogaRuCRFyooLn_16() { return &___LzJEikbMnyDvpBsogaRuCRFyooLn_16; }
	inline void set_LzJEikbMnyDvpBsogaRuCRFyooLn_16(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___LzJEikbMnyDvpBsogaRuCRFyooLn_16 = value;
		Il2CppCodeGenWriteBarrier((&___LzJEikbMnyDvpBsogaRuCRFyooLn_16), value);
	}

	inline static int32_t get_offset_of_WXFtcsBJoAEMLFABEPTRqAJvkUUF_17() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___WXFtcsBJoAEMLFABEPTRqAJvkUUF_17)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_WXFtcsBJoAEMLFABEPTRqAJvkUUF_17() const { return ___WXFtcsBJoAEMLFABEPTRqAJvkUUF_17; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_WXFtcsBJoAEMLFABEPTRqAJvkUUF_17() { return &___WXFtcsBJoAEMLFABEPTRqAJvkUUF_17; }
	inline void set_WXFtcsBJoAEMLFABEPTRqAJvkUUF_17(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___WXFtcsBJoAEMLFABEPTRqAJvkUUF_17 = value;
		Il2CppCodeGenWriteBarrier((&___WXFtcsBJoAEMLFABEPTRqAJvkUUF_17), value);
	}

	inline static int32_t get_offset_of_xKgDcMKGQQaxGajsXNhNAbtUgoI_18() { return static_cast<int32_t>(offsetof(BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24, ___xKgDcMKGQQaxGajsXNhNAbtUgoI_18)); }
	inline RuntimeObject* get_xKgDcMKGQQaxGajsXNhNAbtUgoI_18() const { return ___xKgDcMKGQQaxGajsXNhNAbtUgoI_18; }
	inline RuntimeObject** get_address_of_xKgDcMKGQQaxGajsXNhNAbtUgoI_18() { return &___xKgDcMKGQQaxGajsXNhNAbtUgoI_18; }
	inline void set_xKgDcMKGQQaxGajsXNhNAbtUgoI_18(RuntimeObject* value)
	{
		___xKgDcMKGQQaxGajsXNhNAbtUgoI_18 = value;
		Il2CppCodeGenWriteBarrier((&___xKgDcMKGQQaxGajsXNhNAbtUgoI_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BGWKAJHNNSHLWJGQRMXHDSGALBYJ_T2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24_H
#ifndef DFABLSCRLRBRVWJBXYOZFAFYTDH_T3C3FEF00DE6F60BE618C4D96A85A939092DB150E_H
#define DFABLSCRLRBRVWJBXYOZFAFYTDH_T3C3FEF00DE6F60BE618C4D96A85A939092DB150E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh
struct  DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_4;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::cVBRKPtFhHRCscACnCxtGAeLzXg
	int32_t ___cVBRKPtFhHRCscACnCxtGAeLzXg_8;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::nMKUaBUMYWExQhyhfnfRFjaEMpBA
	int32_t ___nMKUaBUMYWExQhyhfnfRFjaEMpBA_9;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::PfgbcgoHhhyIfiegGuBEgOgictI
	RuntimeObject* ___PfgbcgoHhhyIfiegGuBEgOgictI_10;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::lLWEQQnlLAUlHsMyqeTXXvKJhoA
	int32_t ___lLWEQQnlLAUlHsMyqeTXXvKJhoA_11;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::CMrDgpcbpazkOwYfedaGhXKgNIst
	int32_t ___CMrDgpcbpazkOwYfedaGhXKgNIst_12;
	// JrKetNtIJINuYILTEYSwNUvgPiW Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::FpHUITUNeHnEhECpuNZuoskuSNk
	RuntimeObject* ___FpHUITUNeHnEhECpuNZuoskuSNk_13;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::tTcitdrBJjkvLvdUOUxWpgdIiSr
	int32_t ___tTcitdrBJjkvLvdUOUxWpgdIiSr_14;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::rjXfKwjSOxyyymWpfVfucprygaKr
	int32_t ___rjXfKwjSOxyyymWpfVfucprygaKr_15;
	// Rewired.ControllerMap Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::AOEAGlHDRxpFjetZtJjCPtnxxCJg
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___AOEAGlHDRxpFjetZtJjCPtnxxCJg_16;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::xTUbhAOkkpcsTurvYuEJspXWOSX
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___xTUbhAOkkpcsTurvYuEJspXWOSX_17;
	// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap> Rewired.Player_ControllerHelper_MapHelper_DfAbLscRLRBRVwJBxyozfaFytDh::KsxNJCeXsgEfZvUrlDppORlBfAe
	RuntimeObject* ___KsxNJCeXsgEfZvUrlDppORlBfAe_18;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___pTVJDeVeJlQThAZkFztHqDAWDUa_4)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_4() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_4; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_4; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_4(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_4 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_cVBRKPtFhHRCscACnCxtGAeLzXg_8() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___cVBRKPtFhHRCscACnCxtGAeLzXg_8)); }
	inline int32_t get_cVBRKPtFhHRCscACnCxtGAeLzXg_8() const { return ___cVBRKPtFhHRCscACnCxtGAeLzXg_8; }
	inline int32_t* get_address_of_cVBRKPtFhHRCscACnCxtGAeLzXg_8() { return &___cVBRKPtFhHRCscACnCxtGAeLzXg_8; }
	inline void set_cVBRKPtFhHRCscACnCxtGAeLzXg_8(int32_t value)
	{
		___cVBRKPtFhHRCscACnCxtGAeLzXg_8 = value;
	}

	inline static int32_t get_offset_of_nMKUaBUMYWExQhyhfnfRFjaEMpBA_9() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___nMKUaBUMYWExQhyhfnfRFjaEMpBA_9)); }
	inline int32_t get_nMKUaBUMYWExQhyhfnfRFjaEMpBA_9() const { return ___nMKUaBUMYWExQhyhfnfRFjaEMpBA_9; }
	inline int32_t* get_address_of_nMKUaBUMYWExQhyhfnfRFjaEMpBA_9() { return &___nMKUaBUMYWExQhyhfnfRFjaEMpBA_9; }
	inline void set_nMKUaBUMYWExQhyhfnfRFjaEMpBA_9(int32_t value)
	{
		___nMKUaBUMYWExQhyhfnfRFjaEMpBA_9 = value;
	}

	inline static int32_t get_offset_of_PfgbcgoHhhyIfiegGuBEgOgictI_10() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___PfgbcgoHhhyIfiegGuBEgOgictI_10)); }
	inline RuntimeObject* get_PfgbcgoHhhyIfiegGuBEgOgictI_10() const { return ___PfgbcgoHhhyIfiegGuBEgOgictI_10; }
	inline RuntimeObject** get_address_of_PfgbcgoHhhyIfiegGuBEgOgictI_10() { return &___PfgbcgoHhhyIfiegGuBEgOgictI_10; }
	inline void set_PfgbcgoHhhyIfiegGuBEgOgictI_10(RuntimeObject* value)
	{
		___PfgbcgoHhhyIfiegGuBEgOgictI_10 = value;
		Il2CppCodeGenWriteBarrier((&___PfgbcgoHhhyIfiegGuBEgOgictI_10), value);
	}

	inline static int32_t get_offset_of_lLWEQQnlLAUlHsMyqeTXXvKJhoA_11() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___lLWEQQnlLAUlHsMyqeTXXvKJhoA_11)); }
	inline int32_t get_lLWEQQnlLAUlHsMyqeTXXvKJhoA_11() const { return ___lLWEQQnlLAUlHsMyqeTXXvKJhoA_11; }
	inline int32_t* get_address_of_lLWEQQnlLAUlHsMyqeTXXvKJhoA_11() { return &___lLWEQQnlLAUlHsMyqeTXXvKJhoA_11; }
	inline void set_lLWEQQnlLAUlHsMyqeTXXvKJhoA_11(int32_t value)
	{
		___lLWEQQnlLAUlHsMyqeTXXvKJhoA_11 = value;
	}

	inline static int32_t get_offset_of_CMrDgpcbpazkOwYfedaGhXKgNIst_12() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___CMrDgpcbpazkOwYfedaGhXKgNIst_12)); }
	inline int32_t get_CMrDgpcbpazkOwYfedaGhXKgNIst_12() const { return ___CMrDgpcbpazkOwYfedaGhXKgNIst_12; }
	inline int32_t* get_address_of_CMrDgpcbpazkOwYfedaGhXKgNIst_12() { return &___CMrDgpcbpazkOwYfedaGhXKgNIst_12; }
	inline void set_CMrDgpcbpazkOwYfedaGhXKgNIst_12(int32_t value)
	{
		___CMrDgpcbpazkOwYfedaGhXKgNIst_12 = value;
	}

	inline static int32_t get_offset_of_FpHUITUNeHnEhECpuNZuoskuSNk_13() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___FpHUITUNeHnEhECpuNZuoskuSNk_13)); }
	inline RuntimeObject* get_FpHUITUNeHnEhECpuNZuoskuSNk_13() const { return ___FpHUITUNeHnEhECpuNZuoskuSNk_13; }
	inline RuntimeObject** get_address_of_FpHUITUNeHnEhECpuNZuoskuSNk_13() { return &___FpHUITUNeHnEhECpuNZuoskuSNk_13; }
	inline void set_FpHUITUNeHnEhECpuNZuoskuSNk_13(RuntimeObject* value)
	{
		___FpHUITUNeHnEhECpuNZuoskuSNk_13 = value;
		Il2CppCodeGenWriteBarrier((&___FpHUITUNeHnEhECpuNZuoskuSNk_13), value);
	}

	inline static int32_t get_offset_of_tTcitdrBJjkvLvdUOUxWpgdIiSr_14() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___tTcitdrBJjkvLvdUOUxWpgdIiSr_14)); }
	inline int32_t get_tTcitdrBJjkvLvdUOUxWpgdIiSr_14() const { return ___tTcitdrBJjkvLvdUOUxWpgdIiSr_14; }
	inline int32_t* get_address_of_tTcitdrBJjkvLvdUOUxWpgdIiSr_14() { return &___tTcitdrBJjkvLvdUOUxWpgdIiSr_14; }
	inline void set_tTcitdrBJjkvLvdUOUxWpgdIiSr_14(int32_t value)
	{
		___tTcitdrBJjkvLvdUOUxWpgdIiSr_14 = value;
	}

	inline static int32_t get_offset_of_rjXfKwjSOxyyymWpfVfucprygaKr_15() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___rjXfKwjSOxyyymWpfVfucprygaKr_15)); }
	inline int32_t get_rjXfKwjSOxyyymWpfVfucprygaKr_15() const { return ___rjXfKwjSOxyyymWpfVfucprygaKr_15; }
	inline int32_t* get_address_of_rjXfKwjSOxyyymWpfVfucprygaKr_15() { return &___rjXfKwjSOxyyymWpfVfucprygaKr_15; }
	inline void set_rjXfKwjSOxyyymWpfVfucprygaKr_15(int32_t value)
	{
		___rjXfKwjSOxyyymWpfVfucprygaKr_15 = value;
	}

	inline static int32_t get_offset_of_AOEAGlHDRxpFjetZtJjCPtnxxCJg_16() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___AOEAGlHDRxpFjetZtJjCPtnxxCJg_16)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_AOEAGlHDRxpFjetZtJjCPtnxxCJg_16() const { return ___AOEAGlHDRxpFjetZtJjCPtnxxCJg_16; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_AOEAGlHDRxpFjetZtJjCPtnxxCJg_16() { return &___AOEAGlHDRxpFjetZtJjCPtnxxCJg_16; }
	inline void set_AOEAGlHDRxpFjetZtJjCPtnxxCJg_16(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___AOEAGlHDRxpFjetZtJjCPtnxxCJg_16 = value;
		Il2CppCodeGenWriteBarrier((&___AOEAGlHDRxpFjetZtJjCPtnxxCJg_16), value);
	}

	inline static int32_t get_offset_of_xTUbhAOkkpcsTurvYuEJspXWOSX_17() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___xTUbhAOkkpcsTurvYuEJspXWOSX_17)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_xTUbhAOkkpcsTurvYuEJspXWOSX_17() const { return ___xTUbhAOkkpcsTurvYuEJspXWOSX_17; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_xTUbhAOkkpcsTurvYuEJspXWOSX_17() { return &___xTUbhAOkkpcsTurvYuEJspXWOSX_17; }
	inline void set_xTUbhAOkkpcsTurvYuEJspXWOSX_17(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___xTUbhAOkkpcsTurvYuEJspXWOSX_17 = value;
		Il2CppCodeGenWriteBarrier((&___xTUbhAOkkpcsTurvYuEJspXWOSX_17), value);
	}

	inline static int32_t get_offset_of_KsxNJCeXsgEfZvUrlDppORlBfAe_18() { return static_cast<int32_t>(offsetof(DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E, ___KsxNJCeXsgEfZvUrlDppORlBfAe_18)); }
	inline RuntimeObject* get_KsxNJCeXsgEfZvUrlDppORlBfAe_18() const { return ___KsxNJCeXsgEfZvUrlDppORlBfAe_18; }
	inline RuntimeObject** get_address_of_KsxNJCeXsgEfZvUrlDppORlBfAe_18() { return &___KsxNJCeXsgEfZvUrlDppORlBfAe_18; }
	inline void set_KsxNJCeXsgEfZvUrlDppORlBfAe_18(RuntimeObject* value)
	{
		___KsxNJCeXsgEfZvUrlDppORlBfAe_18 = value;
		Il2CppCodeGenWriteBarrier((&___KsxNJCeXsgEfZvUrlDppORlBfAe_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DFABLSCRLRBRVWJBXYOZFAFYTDH_T3C3FEF00DE6F60BE618C4D96A85A939092DB150E_H
#ifndef RDKLDKRPPSREMBAMXQKINPPCBGIA_T1362DC6AF194550694E6B49F2DFECF6E9B0212D3_H
#define RDKLDKRPPSREMBAMXQKINPPCBGIA_T1362DC6AF194550694E6B49F2DFECF6E9B0212D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa
struct  RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_4;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::NwWdGFWvZmJcWWyNAHbepzNMkQx
	int32_t ___NwWdGFWvZmJcWWyNAHbepzNMkQx_8;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::szdBYzMOzBEiNOyTEALnijcajARk
	int32_t ___szdBYzMOzBEiNOyTEALnijcajARk_9;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::tqREfqwXDYJGtggikzdGDoPwqhd
	RuntimeObject* ___tqREfqwXDYJGtggikzdGDoPwqhd_10;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::ZzwGnlJTbtYiaCdkjhOVusqwdie
	int32_t ___ZzwGnlJTbtYiaCdkjhOVusqwdie_11;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::EQLtfLihFjkPplkttAUbffOWqMa
	int32_t ___EQLtfLihFjkPplkttAUbffOWqMa_12;
	// JrKetNtIJINuYILTEYSwNUvgPiW Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::qARSefdBrGwWJrjtUkHIBfunGxu
	RuntimeObject* ___qARSefdBrGwWJrjtUkHIBfunGxu_13;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::qlxSeKCbykqCuUffKSWUmhkebKI
	int32_t ___qlxSeKCbykqCuUffKSWUmhkebKI_14;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::BEzIxNkwBDphsEaOwjhYgOHTEXgL
	int32_t ___BEzIxNkwBDphsEaOwjhYgOHTEXgL_15;
	// Rewired.ControllerMapWithAxes Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::tdBXLuvRFzGrgLIVoumcTpHTLsz
	ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * ___tdBXLuvRFzGrgLIVoumcTpHTLsz_16;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::gydWPydMODbPNLvNfFVuhkewRzs
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___gydWPydMODbPNLvNfFVuhkewRzs_17;
	// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap> Rewired.Player_ControllerHelper_MapHelper_RdKLDKRppsreMbamXQKinppcbgIa::AkiDYCiMmPsWOImxmReybvqfaGPI
	RuntimeObject* ___AkiDYCiMmPsWOImxmReybvqfaGPI_18;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___pTVJDeVeJlQThAZkFztHqDAWDUa_4)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_4() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_4; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_4; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_4(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_4 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_5 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_NwWdGFWvZmJcWWyNAHbepzNMkQx_8() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___NwWdGFWvZmJcWWyNAHbepzNMkQx_8)); }
	inline int32_t get_NwWdGFWvZmJcWWyNAHbepzNMkQx_8() const { return ___NwWdGFWvZmJcWWyNAHbepzNMkQx_8; }
	inline int32_t* get_address_of_NwWdGFWvZmJcWWyNAHbepzNMkQx_8() { return &___NwWdGFWvZmJcWWyNAHbepzNMkQx_8; }
	inline void set_NwWdGFWvZmJcWWyNAHbepzNMkQx_8(int32_t value)
	{
		___NwWdGFWvZmJcWWyNAHbepzNMkQx_8 = value;
	}

	inline static int32_t get_offset_of_szdBYzMOzBEiNOyTEALnijcajARk_9() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___szdBYzMOzBEiNOyTEALnijcajARk_9)); }
	inline int32_t get_szdBYzMOzBEiNOyTEALnijcajARk_9() const { return ___szdBYzMOzBEiNOyTEALnijcajARk_9; }
	inline int32_t* get_address_of_szdBYzMOzBEiNOyTEALnijcajARk_9() { return &___szdBYzMOzBEiNOyTEALnijcajARk_9; }
	inline void set_szdBYzMOzBEiNOyTEALnijcajARk_9(int32_t value)
	{
		___szdBYzMOzBEiNOyTEALnijcajARk_9 = value;
	}

	inline static int32_t get_offset_of_tqREfqwXDYJGtggikzdGDoPwqhd_10() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___tqREfqwXDYJGtggikzdGDoPwqhd_10)); }
	inline RuntimeObject* get_tqREfqwXDYJGtggikzdGDoPwqhd_10() const { return ___tqREfqwXDYJGtggikzdGDoPwqhd_10; }
	inline RuntimeObject** get_address_of_tqREfqwXDYJGtggikzdGDoPwqhd_10() { return &___tqREfqwXDYJGtggikzdGDoPwqhd_10; }
	inline void set_tqREfqwXDYJGtggikzdGDoPwqhd_10(RuntimeObject* value)
	{
		___tqREfqwXDYJGtggikzdGDoPwqhd_10 = value;
		Il2CppCodeGenWriteBarrier((&___tqREfqwXDYJGtggikzdGDoPwqhd_10), value);
	}

	inline static int32_t get_offset_of_ZzwGnlJTbtYiaCdkjhOVusqwdie_11() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___ZzwGnlJTbtYiaCdkjhOVusqwdie_11)); }
	inline int32_t get_ZzwGnlJTbtYiaCdkjhOVusqwdie_11() const { return ___ZzwGnlJTbtYiaCdkjhOVusqwdie_11; }
	inline int32_t* get_address_of_ZzwGnlJTbtYiaCdkjhOVusqwdie_11() { return &___ZzwGnlJTbtYiaCdkjhOVusqwdie_11; }
	inline void set_ZzwGnlJTbtYiaCdkjhOVusqwdie_11(int32_t value)
	{
		___ZzwGnlJTbtYiaCdkjhOVusqwdie_11 = value;
	}

	inline static int32_t get_offset_of_EQLtfLihFjkPplkttAUbffOWqMa_12() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___EQLtfLihFjkPplkttAUbffOWqMa_12)); }
	inline int32_t get_EQLtfLihFjkPplkttAUbffOWqMa_12() const { return ___EQLtfLihFjkPplkttAUbffOWqMa_12; }
	inline int32_t* get_address_of_EQLtfLihFjkPplkttAUbffOWqMa_12() { return &___EQLtfLihFjkPplkttAUbffOWqMa_12; }
	inline void set_EQLtfLihFjkPplkttAUbffOWqMa_12(int32_t value)
	{
		___EQLtfLihFjkPplkttAUbffOWqMa_12 = value;
	}

	inline static int32_t get_offset_of_qARSefdBrGwWJrjtUkHIBfunGxu_13() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___qARSefdBrGwWJrjtUkHIBfunGxu_13)); }
	inline RuntimeObject* get_qARSefdBrGwWJrjtUkHIBfunGxu_13() const { return ___qARSefdBrGwWJrjtUkHIBfunGxu_13; }
	inline RuntimeObject** get_address_of_qARSefdBrGwWJrjtUkHIBfunGxu_13() { return &___qARSefdBrGwWJrjtUkHIBfunGxu_13; }
	inline void set_qARSefdBrGwWJrjtUkHIBfunGxu_13(RuntimeObject* value)
	{
		___qARSefdBrGwWJrjtUkHIBfunGxu_13 = value;
		Il2CppCodeGenWriteBarrier((&___qARSefdBrGwWJrjtUkHIBfunGxu_13), value);
	}

	inline static int32_t get_offset_of_qlxSeKCbykqCuUffKSWUmhkebKI_14() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___qlxSeKCbykqCuUffKSWUmhkebKI_14)); }
	inline int32_t get_qlxSeKCbykqCuUffKSWUmhkebKI_14() const { return ___qlxSeKCbykqCuUffKSWUmhkebKI_14; }
	inline int32_t* get_address_of_qlxSeKCbykqCuUffKSWUmhkebKI_14() { return &___qlxSeKCbykqCuUffKSWUmhkebKI_14; }
	inline void set_qlxSeKCbykqCuUffKSWUmhkebKI_14(int32_t value)
	{
		___qlxSeKCbykqCuUffKSWUmhkebKI_14 = value;
	}

	inline static int32_t get_offset_of_BEzIxNkwBDphsEaOwjhYgOHTEXgL_15() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___BEzIxNkwBDphsEaOwjhYgOHTEXgL_15)); }
	inline int32_t get_BEzIxNkwBDphsEaOwjhYgOHTEXgL_15() const { return ___BEzIxNkwBDphsEaOwjhYgOHTEXgL_15; }
	inline int32_t* get_address_of_BEzIxNkwBDphsEaOwjhYgOHTEXgL_15() { return &___BEzIxNkwBDphsEaOwjhYgOHTEXgL_15; }
	inline void set_BEzIxNkwBDphsEaOwjhYgOHTEXgL_15(int32_t value)
	{
		___BEzIxNkwBDphsEaOwjhYgOHTEXgL_15 = value;
	}

	inline static int32_t get_offset_of_tdBXLuvRFzGrgLIVoumcTpHTLsz_16() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___tdBXLuvRFzGrgLIVoumcTpHTLsz_16)); }
	inline ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * get_tdBXLuvRFzGrgLIVoumcTpHTLsz_16() const { return ___tdBXLuvRFzGrgLIVoumcTpHTLsz_16; }
	inline ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 ** get_address_of_tdBXLuvRFzGrgLIVoumcTpHTLsz_16() { return &___tdBXLuvRFzGrgLIVoumcTpHTLsz_16; }
	inline void set_tdBXLuvRFzGrgLIVoumcTpHTLsz_16(ControllerMapWithAxes_t6D095E9EE41B757831830413DC6FE457D1892C36 * value)
	{
		___tdBXLuvRFzGrgLIVoumcTpHTLsz_16 = value;
		Il2CppCodeGenWriteBarrier((&___tdBXLuvRFzGrgLIVoumcTpHTLsz_16), value);
	}

	inline static int32_t get_offset_of_gydWPydMODbPNLvNfFVuhkewRzs_17() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___gydWPydMODbPNLvNfFVuhkewRzs_17)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_gydWPydMODbPNLvNfFVuhkewRzs_17() const { return ___gydWPydMODbPNLvNfFVuhkewRzs_17; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_gydWPydMODbPNLvNfFVuhkewRzs_17() { return &___gydWPydMODbPNLvNfFVuhkewRzs_17; }
	inline void set_gydWPydMODbPNLvNfFVuhkewRzs_17(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___gydWPydMODbPNLvNfFVuhkewRzs_17 = value;
		Il2CppCodeGenWriteBarrier((&___gydWPydMODbPNLvNfFVuhkewRzs_17), value);
	}

	inline static int32_t get_offset_of_AkiDYCiMmPsWOImxmReybvqfaGPI_18() { return static_cast<int32_t>(offsetof(RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3, ___AkiDYCiMmPsWOImxmReybvqfaGPI_18)); }
	inline RuntimeObject* get_AkiDYCiMmPsWOImxmReybvqfaGPI_18() const { return ___AkiDYCiMmPsWOImxmReybvqfaGPI_18; }
	inline RuntimeObject** get_address_of_AkiDYCiMmPsWOImxmReybvqfaGPI_18() { return &___AkiDYCiMmPsWOImxmReybvqfaGPI_18; }
	inline void set_AkiDYCiMmPsWOImxmReybvqfaGPI_18(RuntimeObject* value)
	{
		___AkiDYCiMmPsWOImxmReybvqfaGPI_18 = value;
		Il2CppCodeGenWriteBarrier((&___AkiDYCiMmPsWOImxmReybvqfaGPI_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RDKLDKRPPSREMBAMXQKINPPCBGIA_T1362DC6AF194550694E6B49F2DFECF6E9B0212D3_H
#ifndef GMWKTNWENSUKFMJGOMVGIQQKOPO_T00CB891EE1212F0855B98F74E83DC7BF0C73C3EE_H
#define GMWKTNWENSUKFMJGOMVGIQQKOPO_T00CB891EE1212F0855B98F74E83DC7BF0C73C3EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo
struct  gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE  : public RuntimeObject
{
public:
	// Rewired.ControllerMap Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo::eNkkokAfmIBWECneBzfnnJQeZTkH
	int32_t ___eNkkokAfmIBWECneBzfnnJQeZTkH_4;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo::zJxTuhPSOnNBgdzueKjCOGymjoi
	int32_t ___zJxTuhPSOnNBgdzueKjCOGymjoi_5;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo::zNNFbghjzSLPGJYeYOqnJcmxPwWd
	RuntimeObject* ___zNNFbghjzSLPGJYeYOqnJcmxPwWd_6;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo::gulWLvYXmWNnBaLeKwoCphoAFmAh
	int32_t ___gulWLvYXmWNnBaLeKwoCphoAFmAh_7;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo::PMjzCYMDQygIaHIrdVBvizkQSow
	int32_t ___PMjzCYMDQygIaHIrdVBvizkQSow_8;
	// JrKetNtIJINuYILTEYSwNUvgPiW Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo::xUwYeaDbkiUXsxkwIbDfcjPUiMK
	RuntimeObject* ___xUwYeaDbkiUXsxkwIbDfcjPUiMK_9;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo::EaCFZJPkXOUOhVYjrJRNUuVgfYc
	int32_t ___EaCFZJPkXOUOhVYjrJRNUuVgfYc_10;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gmWkTnwensUkFmJGOmVGiQqkoPo::MwvgWQwHHdBetSfMUvcMtaciiKJ
	int32_t ___MwvgWQwHHdBetSfMUvcMtaciiKJ_11;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_eNkkokAfmIBWECneBzfnnJQeZTkH_4() { return static_cast<int32_t>(offsetof(gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE, ___eNkkokAfmIBWECneBzfnnJQeZTkH_4)); }
	inline int32_t get_eNkkokAfmIBWECneBzfnnJQeZTkH_4() const { return ___eNkkokAfmIBWECneBzfnnJQeZTkH_4; }
	inline int32_t* get_address_of_eNkkokAfmIBWECneBzfnnJQeZTkH_4() { return &___eNkkokAfmIBWECneBzfnnJQeZTkH_4; }
	inline void set_eNkkokAfmIBWECneBzfnnJQeZTkH_4(int32_t value)
	{
		___eNkkokAfmIBWECneBzfnnJQeZTkH_4 = value;
	}

	inline static int32_t get_offset_of_zJxTuhPSOnNBgdzueKjCOGymjoi_5() { return static_cast<int32_t>(offsetof(gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE, ___zJxTuhPSOnNBgdzueKjCOGymjoi_5)); }
	inline int32_t get_zJxTuhPSOnNBgdzueKjCOGymjoi_5() const { return ___zJxTuhPSOnNBgdzueKjCOGymjoi_5; }
	inline int32_t* get_address_of_zJxTuhPSOnNBgdzueKjCOGymjoi_5() { return &___zJxTuhPSOnNBgdzueKjCOGymjoi_5; }
	inline void set_zJxTuhPSOnNBgdzueKjCOGymjoi_5(int32_t value)
	{
		___zJxTuhPSOnNBgdzueKjCOGymjoi_5 = value;
	}

	inline static int32_t get_offset_of_zNNFbghjzSLPGJYeYOqnJcmxPwWd_6() { return static_cast<int32_t>(offsetof(gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE, ___zNNFbghjzSLPGJYeYOqnJcmxPwWd_6)); }
	inline RuntimeObject* get_zNNFbghjzSLPGJYeYOqnJcmxPwWd_6() const { return ___zNNFbghjzSLPGJYeYOqnJcmxPwWd_6; }
	inline RuntimeObject** get_address_of_zNNFbghjzSLPGJYeYOqnJcmxPwWd_6() { return &___zNNFbghjzSLPGJYeYOqnJcmxPwWd_6; }
	inline void set_zNNFbghjzSLPGJYeYOqnJcmxPwWd_6(RuntimeObject* value)
	{
		___zNNFbghjzSLPGJYeYOqnJcmxPwWd_6 = value;
		Il2CppCodeGenWriteBarrier((&___zNNFbghjzSLPGJYeYOqnJcmxPwWd_6), value);
	}

	inline static int32_t get_offset_of_gulWLvYXmWNnBaLeKwoCphoAFmAh_7() { return static_cast<int32_t>(offsetof(gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE, ___gulWLvYXmWNnBaLeKwoCphoAFmAh_7)); }
	inline int32_t get_gulWLvYXmWNnBaLeKwoCphoAFmAh_7() const { return ___gulWLvYXmWNnBaLeKwoCphoAFmAh_7; }
	inline int32_t* get_address_of_gulWLvYXmWNnBaLeKwoCphoAFmAh_7() { return &___gulWLvYXmWNnBaLeKwoCphoAFmAh_7; }
	inline void set_gulWLvYXmWNnBaLeKwoCphoAFmAh_7(int32_t value)
	{
		___gulWLvYXmWNnBaLeKwoCphoAFmAh_7 = value;
	}

	inline static int32_t get_offset_of_PMjzCYMDQygIaHIrdVBvizkQSow_8() { return static_cast<int32_t>(offsetof(gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE, ___PMjzCYMDQygIaHIrdVBvizkQSow_8)); }
	inline int32_t get_PMjzCYMDQygIaHIrdVBvizkQSow_8() const { return ___PMjzCYMDQygIaHIrdVBvizkQSow_8; }
	inline int32_t* get_address_of_PMjzCYMDQygIaHIrdVBvizkQSow_8() { return &___PMjzCYMDQygIaHIrdVBvizkQSow_8; }
	inline void set_PMjzCYMDQygIaHIrdVBvizkQSow_8(int32_t value)
	{
		___PMjzCYMDQygIaHIrdVBvizkQSow_8 = value;
	}

	inline static int32_t get_offset_of_xUwYeaDbkiUXsxkwIbDfcjPUiMK_9() { return static_cast<int32_t>(offsetof(gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE, ___xUwYeaDbkiUXsxkwIbDfcjPUiMK_9)); }
	inline RuntimeObject* get_xUwYeaDbkiUXsxkwIbDfcjPUiMK_9() const { return ___xUwYeaDbkiUXsxkwIbDfcjPUiMK_9; }
	inline RuntimeObject** get_address_of_xUwYeaDbkiUXsxkwIbDfcjPUiMK_9() { return &___xUwYeaDbkiUXsxkwIbDfcjPUiMK_9; }
	inline void set_xUwYeaDbkiUXsxkwIbDfcjPUiMK_9(RuntimeObject* value)
	{
		___xUwYeaDbkiUXsxkwIbDfcjPUiMK_9 = value;
		Il2CppCodeGenWriteBarrier((&___xUwYeaDbkiUXsxkwIbDfcjPUiMK_9), value);
	}

	inline static int32_t get_offset_of_EaCFZJPkXOUOhVYjrJRNUuVgfYc_10() { return static_cast<int32_t>(offsetof(gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE, ___EaCFZJPkXOUOhVYjrJRNUuVgfYc_10)); }
	inline int32_t get_EaCFZJPkXOUOhVYjrJRNUuVgfYc_10() const { return ___EaCFZJPkXOUOhVYjrJRNUuVgfYc_10; }
	inline int32_t* get_address_of_EaCFZJPkXOUOhVYjrJRNUuVgfYc_10() { return &___EaCFZJPkXOUOhVYjrJRNUuVgfYc_10; }
	inline void set_EaCFZJPkXOUOhVYjrJRNUuVgfYc_10(int32_t value)
	{
		___EaCFZJPkXOUOhVYjrJRNUuVgfYc_10 = value;
	}

	inline static int32_t get_offset_of_MwvgWQwHHdBetSfMUvcMtaciiKJ_11() { return static_cast<int32_t>(offsetof(gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE, ___MwvgWQwHHdBetSfMUvcMtaciiKJ_11)); }
	inline int32_t get_MwvgWQwHHdBetSfMUvcMtaciiKJ_11() const { return ___MwvgWQwHHdBetSfMUvcMtaciiKJ_11; }
	inline int32_t* get_address_of_MwvgWQwHHdBetSfMUvcMtaciiKJ_11() { return &___MwvgWQwHHdBetSfMUvcMtaciiKJ_11; }
	inline void set_MwvgWQwHHdBetSfMUvcMtaciiKJ_11(int32_t value)
	{
		___MwvgWQwHHdBetSfMUvcMtaciiKJ_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GMWKTNWENSUKFMJGOMVGIQQKOPO_T00CB891EE1212F0855B98F74E83DC7BF0C73C3EE_H
#ifndef VMPFTTIYFRGKAPBXFPZYHPESQIP_TB7325EB63EE089483E90CF238DE25CA7DE83E31E_H
#define VMPFTTIYFRGKAPBXFPZYHPESQIP_TB7325EB63EE089483E90CF238DE25CA7DE83E31E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip
struct  vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E  : public RuntimeObject
{
public:
	// Rewired.ControllerMap Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::OoFswlQbqBhHqGOCooflwYCYmYWF
	int32_t ___OoFswlQbqBhHqGOCooflwYCYmYWF_4;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::pyCASHftKaJjucJBlmeeWQrdKuRk
	int32_t ___pyCASHftKaJjucJBlmeeWQrdKuRk_5;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::fuTCNQGWJwEIEHYwHqJGmcbHooa
	int32_t ___fuTCNQGWJwEIEHYwHqJGmcbHooa_6;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::FJwvShZPOLNXNucTHGOTJlqLDHSc
	int32_t ___FJwvShZPOLNXNucTHGOTJlqLDHSc_7;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::oyCUksEjNbKBOfglcfdwgdMkcMC
	RuntimeObject* ___oyCUksEjNbKBOfglcfdwgdMkcMC_8;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::lqqScjdzncpCzBEMpvDgkJDdBVY
	int32_t ___lqqScjdzncpCzBEMpvDgkJDdBVY_9;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::BRgpmHvYLixyHASvvTNhHPJbEXH
	int32_t ___BRgpmHvYLixyHASvvTNhHPJbEXH_10;
	// JrKetNtIJINuYILTEYSwNUvgPiW Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::SnzkgPJFdCGooiTRIHVkihanwVMK
	RuntimeObject* ___SnzkgPJFdCGooiTRIHVkihanwVMK_11;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::CwhJXWuTnfToewlxmgBNbWvHAdZr
	int32_t ___CwhJXWuTnfToewlxmgBNbWvHAdZr_12;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::MEciOzoedizohxTqjjeHDfmkvBf
	int32_t ___MEciOzoedizohxTqjjeHDfmkvBf_13;
	// Rewired.ControllerMap Rewired.Player_ControllerHelper_MapHelper_vMPFtTiyfRGKaPBXFpzyHpeSQip::OBkynwRWyeiCGHMTWDjEKPyZwygf
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___OBkynwRWyeiCGHMTWDjEKPyZwygf_14;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_OoFswlQbqBhHqGOCooflwYCYmYWF_4() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___OoFswlQbqBhHqGOCooflwYCYmYWF_4)); }
	inline int32_t get_OoFswlQbqBhHqGOCooflwYCYmYWF_4() const { return ___OoFswlQbqBhHqGOCooflwYCYmYWF_4; }
	inline int32_t* get_address_of_OoFswlQbqBhHqGOCooflwYCYmYWF_4() { return &___OoFswlQbqBhHqGOCooflwYCYmYWF_4; }
	inline void set_OoFswlQbqBhHqGOCooflwYCYmYWF_4(int32_t value)
	{
		___OoFswlQbqBhHqGOCooflwYCYmYWF_4 = value;
	}

	inline static int32_t get_offset_of_pyCASHftKaJjucJBlmeeWQrdKuRk_5() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___pyCASHftKaJjucJBlmeeWQrdKuRk_5)); }
	inline int32_t get_pyCASHftKaJjucJBlmeeWQrdKuRk_5() const { return ___pyCASHftKaJjucJBlmeeWQrdKuRk_5; }
	inline int32_t* get_address_of_pyCASHftKaJjucJBlmeeWQrdKuRk_5() { return &___pyCASHftKaJjucJBlmeeWQrdKuRk_5; }
	inline void set_pyCASHftKaJjucJBlmeeWQrdKuRk_5(int32_t value)
	{
		___pyCASHftKaJjucJBlmeeWQrdKuRk_5 = value;
	}

	inline static int32_t get_offset_of_fuTCNQGWJwEIEHYwHqJGmcbHooa_6() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___fuTCNQGWJwEIEHYwHqJGmcbHooa_6)); }
	inline int32_t get_fuTCNQGWJwEIEHYwHqJGmcbHooa_6() const { return ___fuTCNQGWJwEIEHYwHqJGmcbHooa_6; }
	inline int32_t* get_address_of_fuTCNQGWJwEIEHYwHqJGmcbHooa_6() { return &___fuTCNQGWJwEIEHYwHqJGmcbHooa_6; }
	inline void set_fuTCNQGWJwEIEHYwHqJGmcbHooa_6(int32_t value)
	{
		___fuTCNQGWJwEIEHYwHqJGmcbHooa_6 = value;
	}

	inline static int32_t get_offset_of_FJwvShZPOLNXNucTHGOTJlqLDHSc_7() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___FJwvShZPOLNXNucTHGOTJlqLDHSc_7)); }
	inline int32_t get_FJwvShZPOLNXNucTHGOTJlqLDHSc_7() const { return ___FJwvShZPOLNXNucTHGOTJlqLDHSc_7; }
	inline int32_t* get_address_of_FJwvShZPOLNXNucTHGOTJlqLDHSc_7() { return &___FJwvShZPOLNXNucTHGOTJlqLDHSc_7; }
	inline void set_FJwvShZPOLNXNucTHGOTJlqLDHSc_7(int32_t value)
	{
		___FJwvShZPOLNXNucTHGOTJlqLDHSc_7 = value;
	}

	inline static int32_t get_offset_of_oyCUksEjNbKBOfglcfdwgdMkcMC_8() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___oyCUksEjNbKBOfglcfdwgdMkcMC_8)); }
	inline RuntimeObject* get_oyCUksEjNbKBOfglcfdwgdMkcMC_8() const { return ___oyCUksEjNbKBOfglcfdwgdMkcMC_8; }
	inline RuntimeObject** get_address_of_oyCUksEjNbKBOfglcfdwgdMkcMC_8() { return &___oyCUksEjNbKBOfglcfdwgdMkcMC_8; }
	inline void set_oyCUksEjNbKBOfglcfdwgdMkcMC_8(RuntimeObject* value)
	{
		___oyCUksEjNbKBOfglcfdwgdMkcMC_8 = value;
		Il2CppCodeGenWriteBarrier((&___oyCUksEjNbKBOfglcfdwgdMkcMC_8), value);
	}

	inline static int32_t get_offset_of_lqqScjdzncpCzBEMpvDgkJDdBVY_9() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___lqqScjdzncpCzBEMpvDgkJDdBVY_9)); }
	inline int32_t get_lqqScjdzncpCzBEMpvDgkJDdBVY_9() const { return ___lqqScjdzncpCzBEMpvDgkJDdBVY_9; }
	inline int32_t* get_address_of_lqqScjdzncpCzBEMpvDgkJDdBVY_9() { return &___lqqScjdzncpCzBEMpvDgkJDdBVY_9; }
	inline void set_lqqScjdzncpCzBEMpvDgkJDdBVY_9(int32_t value)
	{
		___lqqScjdzncpCzBEMpvDgkJDdBVY_9 = value;
	}

	inline static int32_t get_offset_of_BRgpmHvYLixyHASvvTNhHPJbEXH_10() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___BRgpmHvYLixyHASvvTNhHPJbEXH_10)); }
	inline int32_t get_BRgpmHvYLixyHASvvTNhHPJbEXH_10() const { return ___BRgpmHvYLixyHASvvTNhHPJbEXH_10; }
	inline int32_t* get_address_of_BRgpmHvYLixyHASvvTNhHPJbEXH_10() { return &___BRgpmHvYLixyHASvvTNhHPJbEXH_10; }
	inline void set_BRgpmHvYLixyHASvvTNhHPJbEXH_10(int32_t value)
	{
		___BRgpmHvYLixyHASvvTNhHPJbEXH_10 = value;
	}

	inline static int32_t get_offset_of_SnzkgPJFdCGooiTRIHVkihanwVMK_11() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___SnzkgPJFdCGooiTRIHVkihanwVMK_11)); }
	inline RuntimeObject* get_SnzkgPJFdCGooiTRIHVkihanwVMK_11() const { return ___SnzkgPJFdCGooiTRIHVkihanwVMK_11; }
	inline RuntimeObject** get_address_of_SnzkgPJFdCGooiTRIHVkihanwVMK_11() { return &___SnzkgPJFdCGooiTRIHVkihanwVMK_11; }
	inline void set_SnzkgPJFdCGooiTRIHVkihanwVMK_11(RuntimeObject* value)
	{
		___SnzkgPJFdCGooiTRIHVkihanwVMK_11 = value;
		Il2CppCodeGenWriteBarrier((&___SnzkgPJFdCGooiTRIHVkihanwVMK_11), value);
	}

	inline static int32_t get_offset_of_CwhJXWuTnfToewlxmgBNbWvHAdZr_12() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___CwhJXWuTnfToewlxmgBNbWvHAdZr_12)); }
	inline int32_t get_CwhJXWuTnfToewlxmgBNbWvHAdZr_12() const { return ___CwhJXWuTnfToewlxmgBNbWvHAdZr_12; }
	inline int32_t* get_address_of_CwhJXWuTnfToewlxmgBNbWvHAdZr_12() { return &___CwhJXWuTnfToewlxmgBNbWvHAdZr_12; }
	inline void set_CwhJXWuTnfToewlxmgBNbWvHAdZr_12(int32_t value)
	{
		___CwhJXWuTnfToewlxmgBNbWvHAdZr_12 = value;
	}

	inline static int32_t get_offset_of_MEciOzoedizohxTqjjeHDfmkvBf_13() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___MEciOzoedizohxTqjjeHDfmkvBf_13)); }
	inline int32_t get_MEciOzoedizohxTqjjeHDfmkvBf_13() const { return ___MEciOzoedizohxTqjjeHDfmkvBf_13; }
	inline int32_t* get_address_of_MEciOzoedizohxTqjjeHDfmkvBf_13() { return &___MEciOzoedizohxTqjjeHDfmkvBf_13; }
	inline void set_MEciOzoedizohxTqjjeHDfmkvBf_13(int32_t value)
	{
		___MEciOzoedizohxTqjjeHDfmkvBf_13 = value;
	}

	inline static int32_t get_offset_of_OBkynwRWyeiCGHMTWDjEKPyZwygf_14() { return static_cast<int32_t>(offsetof(vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E, ___OBkynwRWyeiCGHMTWDjEKPyZwygf_14)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_OBkynwRWyeiCGHMTWDjEKPyZwygf_14() const { return ___OBkynwRWyeiCGHMTWDjEKPyZwygf_14; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_OBkynwRWyeiCGHMTWDjEKPyZwygf_14() { return &___OBkynwRWyeiCGHMTWDjEKPyZwygf_14; }
	inline void set_OBkynwRWyeiCGHMTWDjEKPyZwygf_14(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___OBkynwRWyeiCGHMTWDjEKPyZwygf_14 = value;
		Il2CppCodeGenWriteBarrier((&___OBkynwRWyeiCGHMTWDjEKPyZwygf_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VMPFTTIYFRGKAPBXFPZYHPESQIP_TB7325EB63EE089483E90CF238DE25CA7DE83E31E_H
#ifndef ZGOFFAAKSCXWSECDXGDVDCQXYPJO_TC5195B3814C9F96610D57746DD1D2B124700A51B_H
#define ZGOFFAAKSCXWSECDXGDVDCQXYPJO_TC5195B3814C9F96610D57746DD1D2B124700A51B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_ZgoffaakscXwSecdxgDVDCqXYPjo
struct  ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B  : public RuntimeObject
{
public:
	// System.Int32 Rewired.Player_ControllerHelper_ZgoffaakscXwSecdxgDVDCqXYPjo::noBGYKSmBMZluLaZfVKyMJZCFFo
	int32_t ___noBGYKSmBMZluLaZfVKyMJZCFFo_0;
	// Rewired.ControllerType[] Rewired.Player_ControllerHelper_ZgoffaakscXwSecdxgDVDCqXYPjo::HwwSIfgySgdOsArnWHjmNWCEpPO
	ControllerTypeU5BU5D_tF512B952A07962493A1287DDA9D63B7A14264D43* ___HwwSIfgySgdOsArnWHjmNWCEpPO_1;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI[] Rewired.Player_ControllerHelper_ZgoffaakscXwSecdxgDVDCqXYPjo::erhKEnZojNIWZDQIgCzYuEvMtysj
	LBdawFihcNUlinDyhLfBKVmmxSHIU5BU5D_tA646A82DB1D771533D3E584B522435419C4BA379* ___erhKEnZojNIWZDQIgCzYuEvMtysj_2;

public:
	inline static int32_t get_offset_of_noBGYKSmBMZluLaZfVKyMJZCFFo_0() { return static_cast<int32_t>(offsetof(ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B, ___noBGYKSmBMZluLaZfVKyMJZCFFo_0)); }
	inline int32_t get_noBGYKSmBMZluLaZfVKyMJZCFFo_0() const { return ___noBGYKSmBMZluLaZfVKyMJZCFFo_0; }
	inline int32_t* get_address_of_noBGYKSmBMZluLaZfVKyMJZCFFo_0() { return &___noBGYKSmBMZluLaZfVKyMJZCFFo_0; }
	inline void set_noBGYKSmBMZluLaZfVKyMJZCFFo_0(int32_t value)
	{
		___noBGYKSmBMZluLaZfVKyMJZCFFo_0 = value;
	}

	inline static int32_t get_offset_of_HwwSIfgySgdOsArnWHjmNWCEpPO_1() { return static_cast<int32_t>(offsetof(ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B, ___HwwSIfgySgdOsArnWHjmNWCEpPO_1)); }
	inline ControllerTypeU5BU5D_tF512B952A07962493A1287DDA9D63B7A14264D43* get_HwwSIfgySgdOsArnWHjmNWCEpPO_1() const { return ___HwwSIfgySgdOsArnWHjmNWCEpPO_1; }
	inline ControllerTypeU5BU5D_tF512B952A07962493A1287DDA9D63B7A14264D43** get_address_of_HwwSIfgySgdOsArnWHjmNWCEpPO_1() { return &___HwwSIfgySgdOsArnWHjmNWCEpPO_1; }
	inline void set_HwwSIfgySgdOsArnWHjmNWCEpPO_1(ControllerTypeU5BU5D_tF512B952A07962493A1287DDA9D63B7A14264D43* value)
	{
		___HwwSIfgySgdOsArnWHjmNWCEpPO_1 = value;
		Il2CppCodeGenWriteBarrier((&___HwwSIfgySgdOsArnWHjmNWCEpPO_1), value);
	}

	inline static int32_t get_offset_of_erhKEnZojNIWZDQIgCzYuEvMtysj_2() { return static_cast<int32_t>(offsetof(ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B, ___erhKEnZojNIWZDQIgCzYuEvMtysj_2)); }
	inline LBdawFihcNUlinDyhLfBKVmmxSHIU5BU5D_tA646A82DB1D771533D3E584B522435419C4BA379* get_erhKEnZojNIWZDQIgCzYuEvMtysj_2() const { return ___erhKEnZojNIWZDQIgCzYuEvMtysj_2; }
	inline LBdawFihcNUlinDyhLfBKVmmxSHIU5BU5D_tA646A82DB1D771533D3E584B522435419C4BA379** get_address_of_erhKEnZojNIWZDQIgCzYuEvMtysj_2() { return &___erhKEnZojNIWZDQIgCzYuEvMtysj_2; }
	inline void set_erhKEnZojNIWZDQIgCzYuEvMtysj_2(LBdawFihcNUlinDyhLfBKVmmxSHIU5BU5D_tA646A82DB1D771533D3E584B522435419C4BA379* value)
	{
		___erhKEnZojNIWZDQIgCzYuEvMtysj_2 = value;
		Il2CppCodeGenWriteBarrier((&___erhKEnZojNIWZDQIgCzYuEvMtysj_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZGOFFAAKSCXWSECDXGDVDCQXYPJO_TC5195B3814C9F96610D57746DD1D2B124700A51B_H
#ifndef CODEHELPER_T8618ECFBF040A78FEDA26D398742608E1AA036AB_H
#define CODEHELPER_T8618ECFBF040A78FEDA26D398742608E1AA036AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Utils.Classes.CodeHelper
struct  CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEHELPER_T8618ECFBF040A78FEDA26D398742608E1AA036AB_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef VLEBHNCKPOPFOKOXDDCBDGTKHYD_TA01C4EADEDC175C2FAC271C09D7E5245A52EFCBB_H
#define VLEBHNCKPOPFOKOXDDCBDGTKHYD_TA01C4EADEDC175C2FAC271C09D7E5245A52EFCBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// vLEBhncKpOPFOKOXDdCbdgTKhyd
struct  vLEBhncKpOPFOKOXDdCbdgTKhyd_tA01C4EADEDC175C2FAC271C09D7E5245A52EFCBB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VLEBHNCKPOPFOKOXDDCBDGTKHYD_TA01C4EADEDC175C2FAC271C09D7E5245A52EFCBB_H
#ifndef PADDEVICECLASSEXTENDEDINFORMATION_TA89B04DE21E201757D5FB9CBF3A7DDA35B49A253_H
#define PADDEVICECLASSEXTENDEDINFORMATION_TA89B04DE21E201757D5FB9CBF3A7DDA35B49A253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.Internal.PadDeviceClassExtendedInformation
struct  PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253 
{
public:
	// System.Int32 Rewired.Platforms.PS4.Internal.PadDeviceClassExtendedInformation::deviceClass
	int32_t ___deviceClass_0;
	// System.Int32 Rewired.Platforms.PS4.Internal.PadDeviceClassExtendedInformation::reserved
	int32_t ___reserved_1;
	// System.Byte Rewired.Platforms.PS4.Internal.PadDeviceClassExtendedInformation::capability
	uint8_t ___capability_2;
	// System.Byte Rewired.Platforms.PS4.Internal.PadDeviceClassExtendedInformation::quantityOfSelectorSwitch
	uint8_t ___quantityOfSelectorSwitch_3;
	// System.UInt16 Rewired.Platforms.PS4.Internal.PadDeviceClassExtendedInformation::maxPhysicalWheelAngle
	uint16_t ___maxPhysicalWheelAngle_4;

public:
	inline static int32_t get_offset_of_deviceClass_0() { return static_cast<int32_t>(offsetof(PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253, ___deviceClass_0)); }
	inline int32_t get_deviceClass_0() const { return ___deviceClass_0; }
	inline int32_t* get_address_of_deviceClass_0() { return &___deviceClass_0; }
	inline void set_deviceClass_0(int32_t value)
	{
		___deviceClass_0 = value;
	}

	inline static int32_t get_offset_of_reserved_1() { return static_cast<int32_t>(offsetof(PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253, ___reserved_1)); }
	inline int32_t get_reserved_1() const { return ___reserved_1; }
	inline int32_t* get_address_of_reserved_1() { return &___reserved_1; }
	inline void set_reserved_1(int32_t value)
	{
		___reserved_1 = value;
	}

	inline static int32_t get_offset_of_capability_2() { return static_cast<int32_t>(offsetof(PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253, ___capability_2)); }
	inline uint8_t get_capability_2() const { return ___capability_2; }
	inline uint8_t* get_address_of_capability_2() { return &___capability_2; }
	inline void set_capability_2(uint8_t value)
	{
		___capability_2 = value;
	}

	inline static int32_t get_offset_of_quantityOfSelectorSwitch_3() { return static_cast<int32_t>(offsetof(PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253, ___quantityOfSelectorSwitch_3)); }
	inline uint8_t get_quantityOfSelectorSwitch_3() const { return ___quantityOfSelectorSwitch_3; }
	inline uint8_t* get_address_of_quantityOfSelectorSwitch_3() { return &___quantityOfSelectorSwitch_3; }
	inline void set_quantityOfSelectorSwitch_3(uint8_t value)
	{
		___quantityOfSelectorSwitch_3 = value;
	}

	inline static int32_t get_offset_of_maxPhysicalWheelAngle_4() { return static_cast<int32_t>(offsetof(PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253, ___maxPhysicalWheelAngle_4)); }
	inline uint16_t get_maxPhysicalWheelAngle_4() const { return ___maxPhysicalWheelAngle_4; }
	inline uint16_t* get_address_of_maxPhysicalWheelAngle_4() { return &___maxPhysicalWheelAngle_4; }
	inline void set_maxPhysicalWheelAngle_4(uint16_t value)
	{
		___maxPhysicalWheelAngle_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDEVICECLASSEXTENDEDINFORMATION_TA89B04DE21E201757D5FB9CBF3A7DDA35B49A253_H
#ifndef PADSTICKINFORMATION_T6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C_H
#define PADSTICKINFORMATION_T6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.Internal.PadStickInformation
struct  PadStickInformation_t6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C 
{
public:
	// System.Byte Rewired.Platforms.PS4.Internal.PadStickInformation::deadZoneLeft
	uint8_t ___deadZoneLeft_0;
	// System.Byte Rewired.Platforms.PS4.Internal.PadStickInformation::deadZoneRight
	uint8_t ___deadZoneRight_1;

public:
	inline static int32_t get_offset_of_deadZoneLeft_0() { return static_cast<int32_t>(offsetof(PadStickInformation_t6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C, ___deadZoneLeft_0)); }
	inline uint8_t get_deadZoneLeft_0() const { return ___deadZoneLeft_0; }
	inline uint8_t* get_address_of_deadZoneLeft_0() { return &___deadZoneLeft_0; }
	inline void set_deadZoneLeft_0(uint8_t value)
	{
		___deadZoneLeft_0 = value;
	}

	inline static int32_t get_offset_of_deadZoneRight_1() { return static_cast<int32_t>(offsetof(PadStickInformation_t6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C, ___deadZoneRight_1)); }
	inline uint8_t get_deadZoneRight_1() const { return ___deadZoneRight_1; }
	inline uint8_t* get_address_of_deadZoneRight_1() { return &___deadZoneRight_1; }
	inline void set_deadZoneRight_1(uint8_t value)
	{
		___deadZoneRight_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADSTICKINFORMATION_T6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C_H
#ifndef PADTOUCHPADINFORMATION_TBA2B76541DC960B1E2E508DF26FB887491C3B77D_H
#define PADTOUCHPADINFORMATION_TBA2B76541DC960B1E2E508DF26FB887491C3B77D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.Internal.PadTouchPadInformation
struct  PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D 
{
public:
	// System.Single Rewired.Platforms.PS4.Internal.PadTouchPadInformation::pixelDensity
	float ___pixelDensity_0;
	// System.UInt16 Rewired.Platforms.PS4.Internal.PadTouchPadInformation::resolutionX
	uint16_t ___resolutionX_1;
	// System.UInt16 Rewired.Platforms.PS4.Internal.PadTouchPadInformation::resolutionY
	uint16_t ___resolutionY_2;

public:
	inline static int32_t get_offset_of_pixelDensity_0() { return static_cast<int32_t>(offsetof(PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D, ___pixelDensity_0)); }
	inline float get_pixelDensity_0() const { return ___pixelDensity_0; }
	inline float* get_address_of_pixelDensity_0() { return &___pixelDensity_0; }
	inline void set_pixelDensity_0(float value)
	{
		___pixelDensity_0 = value;
	}

	inline static int32_t get_offset_of_resolutionX_1() { return static_cast<int32_t>(offsetof(PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D, ___resolutionX_1)); }
	inline uint16_t get_resolutionX_1() const { return ___resolutionX_1; }
	inline uint16_t* get_address_of_resolutionX_1() { return &___resolutionX_1; }
	inline void set_resolutionX_1(uint16_t value)
	{
		___resolutionX_1 = value;
	}

	inline static int32_t get_offset_of_resolutionY_2() { return static_cast<int32_t>(offsetof(PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D, ___resolutionY_2)); }
	inline uint16_t get_resolutionY_2() const { return ___resolutionY_2; }
	inline uint16_t* get_address_of_resolutionY_2() { return &___resolutionY_2; }
	inline void set_resolutionY_2(uint16_t value)
	{
		___resolutionY_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADTOUCHPADINFORMATION_TBA2B76541DC960B1E2E508DF26FB887491C3B77D_H
#ifndef PS4CONTROLLEREXTENSION_T027116553D089D8E7A625D4826DF5120FFCB07FE_H
#define PS4CONTROLLEREXTENSION_T027116553D089D8E7A625D4826DF5120FFCB07FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4ControllerExtension
struct  PS4ControllerExtension_t027116553D089D8E7A625D4826DF5120FFCB07FE  : public Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD
{
public:
	// Rewired.Utils.Classes.Utility.TimerAbs[] Rewired.Platforms.PS4.PS4ControllerExtension::CdVWyCtwdYqRItEtEoYUUDYOAqxD
	TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC* ___CdVWyCtwdYqRItEtEoYUUDYOAqxD_3;

public:
	inline static int32_t get_offset_of_CdVWyCtwdYqRItEtEoYUUDYOAqxD_3() { return static_cast<int32_t>(offsetof(PS4ControllerExtension_t027116553D089D8E7A625D4826DF5120FFCB07FE, ___CdVWyCtwdYqRItEtEoYUUDYOAqxD_3)); }
	inline TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC* get_CdVWyCtwdYqRItEtEoYUUDYOAqxD_3() const { return ___CdVWyCtwdYqRItEtEoYUUDYOAqxD_3; }
	inline TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC** get_address_of_CdVWyCtwdYqRItEtEoYUUDYOAqxD_3() { return &___CdVWyCtwdYqRItEtEoYUUDYOAqxD_3; }
	inline void set_CdVWyCtwdYqRItEtEoYUUDYOAqxD_3(TimerAbsU5BU5D_t47E1FDC02D39792ACF3C98B79D16173E67F4E4EC* value)
	{
		___CdVWyCtwdYqRItEtEoYUUDYOAqxD_3 = value;
		Il2CppCodeGenWriteBarrier((&___CdVWyCtwdYqRItEtEoYUUDYOAqxD_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PS4CONTROLLEREXTENSION_T027116553D089D8E7A625D4826DF5120FFCB07FE_H
#ifndef PYOLDYXWCBGSMWCZATDFMJGNDAL_T1997F390DB0D5007BB63F4BC303BDA66F1F503EF_H
#define PYOLDYXWCBGSMWCZATDFMJGNDAL_T1997F390DB0D5007BB63F4BC303BDA66F1F503EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_PYOLdyXwCbgsmWCZaTDfMJgndAl
struct  PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF 
{
public:
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_PYOLdyXwCbgsmWCZaTDfMJgndAl::TSxIVLfBUyELziEzwQrAMRmzGqrD
	int32_t ___TSxIVLfBUyELziEzwQrAMRmzGqrD_0;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_PYOLdyXwCbgsmWCZaTDfMJgndAl::zSkfqYfrOXJXcopeOkTalFrxsHk
	int32_t ___zSkfqYfrOXJXcopeOkTalFrxsHk_1;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_PYOLdyXwCbgsmWCZaTDfMJgndAl::GWNhMohgMSKpOKjKZrAVmDEaFCd
	int32_t ___GWNhMohgMSKpOKjKZrAVmDEaFCd_2;
	// System.Boolean Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_PYOLdyXwCbgsmWCZaTDfMJgndAl::pTXqUwcYdENkAAHGFsRzWXKXrZB
	bool ___pTXqUwcYdENkAAHGFsRzWXKXrZB_3;

public:
	inline static int32_t get_offset_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_0() { return static_cast<int32_t>(offsetof(PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF, ___TSxIVLfBUyELziEzwQrAMRmzGqrD_0)); }
	inline int32_t get_TSxIVLfBUyELziEzwQrAMRmzGqrD_0() const { return ___TSxIVLfBUyELziEzwQrAMRmzGqrD_0; }
	inline int32_t* get_address_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_0() { return &___TSxIVLfBUyELziEzwQrAMRmzGqrD_0; }
	inline void set_TSxIVLfBUyELziEzwQrAMRmzGqrD_0(int32_t value)
	{
		___TSxIVLfBUyELziEzwQrAMRmzGqrD_0 = value;
	}

	inline static int32_t get_offset_of_zSkfqYfrOXJXcopeOkTalFrxsHk_1() { return static_cast<int32_t>(offsetof(PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF, ___zSkfqYfrOXJXcopeOkTalFrxsHk_1)); }
	inline int32_t get_zSkfqYfrOXJXcopeOkTalFrxsHk_1() const { return ___zSkfqYfrOXJXcopeOkTalFrxsHk_1; }
	inline int32_t* get_address_of_zSkfqYfrOXJXcopeOkTalFrxsHk_1() { return &___zSkfqYfrOXJXcopeOkTalFrxsHk_1; }
	inline void set_zSkfqYfrOXJXcopeOkTalFrxsHk_1(int32_t value)
	{
		___zSkfqYfrOXJXcopeOkTalFrxsHk_1 = value;
	}

	inline static int32_t get_offset_of_GWNhMohgMSKpOKjKZrAVmDEaFCd_2() { return static_cast<int32_t>(offsetof(PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF, ___GWNhMohgMSKpOKjKZrAVmDEaFCd_2)); }
	inline int32_t get_GWNhMohgMSKpOKjKZrAVmDEaFCd_2() const { return ___GWNhMohgMSKpOKjKZrAVmDEaFCd_2; }
	inline int32_t* get_address_of_GWNhMohgMSKpOKjKZrAVmDEaFCd_2() { return &___GWNhMohgMSKpOKjKZrAVmDEaFCd_2; }
	inline void set_GWNhMohgMSKpOKjKZrAVmDEaFCd_2(int32_t value)
	{
		___GWNhMohgMSKpOKjKZrAVmDEaFCd_2 = value;
	}

	inline static int32_t get_offset_of_pTXqUwcYdENkAAHGFsRzWXKXrZB_3() { return static_cast<int32_t>(offsetof(PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF, ___pTXqUwcYdENkAAHGFsRzWXKXrZB_3)); }
	inline bool get_pTXqUwcYdENkAAHGFsRzWXKXrZB_3() const { return ___pTXqUwcYdENkAAHGFsRzWXKXrZB_3; }
	inline bool* get_address_of_pTXqUwcYdENkAAHGFsRzWXKXrZB_3() { return &___pTXqUwcYdENkAAHGFsRzWXKXrZB_3; }
	inline void set_pTXqUwcYdENkAAHGFsRzWXKXrZB_3(bool value)
	{
		___pTXqUwcYdENkAAHGFsRzWXKXrZB_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.Platforms.PS4.PS4InputSource/YUATmdkIYeDTVeeKKfwolrMGUrN/PYOLdyXwCbgsmWCZaTDfMJgndAl
struct PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF_marshaled_pinvoke
{
	int32_t ___TSxIVLfBUyELziEzwQrAMRmzGqrD_0;
	int32_t ___zSkfqYfrOXJXcopeOkTalFrxsHk_1;
	int32_t ___GWNhMohgMSKpOKjKZrAVmDEaFCd_2;
	int32_t ___pTXqUwcYdENkAAHGFsRzWXKXrZB_3;
};
// Native definition for COM marshalling of Rewired.Platforms.PS4.PS4InputSource/YUATmdkIYeDTVeeKKfwolrMGUrN/PYOLdyXwCbgsmWCZaTDfMJgndAl
struct PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF_marshaled_com
{
	int32_t ___TSxIVLfBUyELziEzwQrAMRmzGqrD_0;
	int32_t ___zSkfqYfrOXJXcopeOkTalFrxsHk_1;
	int32_t ___GWNhMohgMSKpOKjKZrAVmDEaFCd_2;
	int32_t ___pTXqUwcYdENkAAHGFsRzWXKXrZB_3;
};
#endif // PYOLDYXWCBGSMWCZATDFMJGNDAL_T1997F390DB0D5007BB63F4BC303BDA66F1F503EF_H
#ifndef ZFBPVMKGGGELCIFWNEAZFTNIMAQR_TAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6_H
#define ZFBPVMKGGGELCIFWNEAZFTNIMAQR_TAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_ZFbPVMKgGgElciFwNeAzftnIMAqR
struct  ZFbPVMKgGgElciFwNeAzftnIMAqR_tAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6 
{
public:
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_ZFbPVMKgGgElciFwNeAzftnIMAqR::TSxIVLfBUyELziEzwQrAMRmzGqrD
	int32_t ___TSxIVLfBUyELziEzwQrAMRmzGqrD_0;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_ZFbPVMKgGgElciFwNeAzftnIMAqR::zSkfqYfrOXJXcopeOkTalFrxsHk
	int32_t ___zSkfqYfrOXJXcopeOkTalFrxsHk_1;
	// System.Boolean Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_ZFbPVMKgGgElciFwNeAzftnIMAqR::pTXqUwcYdENkAAHGFsRzWXKXrZB
	bool ___pTXqUwcYdENkAAHGFsRzWXKXrZB_2;

public:
	inline static int32_t get_offset_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_0() { return static_cast<int32_t>(offsetof(ZFbPVMKgGgElciFwNeAzftnIMAqR_tAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6, ___TSxIVLfBUyELziEzwQrAMRmzGqrD_0)); }
	inline int32_t get_TSxIVLfBUyELziEzwQrAMRmzGqrD_0() const { return ___TSxIVLfBUyELziEzwQrAMRmzGqrD_0; }
	inline int32_t* get_address_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_0() { return &___TSxIVLfBUyELziEzwQrAMRmzGqrD_0; }
	inline void set_TSxIVLfBUyELziEzwQrAMRmzGqrD_0(int32_t value)
	{
		___TSxIVLfBUyELziEzwQrAMRmzGqrD_0 = value;
	}

	inline static int32_t get_offset_of_zSkfqYfrOXJXcopeOkTalFrxsHk_1() { return static_cast<int32_t>(offsetof(ZFbPVMKgGgElciFwNeAzftnIMAqR_tAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6, ___zSkfqYfrOXJXcopeOkTalFrxsHk_1)); }
	inline int32_t get_zSkfqYfrOXJXcopeOkTalFrxsHk_1() const { return ___zSkfqYfrOXJXcopeOkTalFrxsHk_1; }
	inline int32_t* get_address_of_zSkfqYfrOXJXcopeOkTalFrxsHk_1() { return &___zSkfqYfrOXJXcopeOkTalFrxsHk_1; }
	inline void set_zSkfqYfrOXJXcopeOkTalFrxsHk_1(int32_t value)
	{
		___zSkfqYfrOXJXcopeOkTalFrxsHk_1 = value;
	}

	inline static int32_t get_offset_of_pTXqUwcYdENkAAHGFsRzWXKXrZB_2() { return static_cast<int32_t>(offsetof(ZFbPVMKgGgElciFwNeAzftnIMAqR_tAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6, ___pTXqUwcYdENkAAHGFsRzWXKXrZB_2)); }
	inline bool get_pTXqUwcYdENkAAHGFsRzWXKXrZB_2() const { return ___pTXqUwcYdENkAAHGFsRzWXKXrZB_2; }
	inline bool* get_address_of_pTXqUwcYdENkAAHGFsRzWXKXrZB_2() { return &___pTXqUwcYdENkAAHGFsRzWXKXrZB_2; }
	inline void set_pTXqUwcYdENkAAHGFsRzWXKXrZB_2(bool value)
	{
		___pTXqUwcYdENkAAHGFsRzWXKXrZB_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.Platforms.PS4.PS4InputSource/YUATmdkIYeDTVeeKKfwolrMGUrN/ZFbPVMKgGgElciFwNeAzftnIMAqR
struct ZFbPVMKgGgElciFwNeAzftnIMAqR_tAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6_marshaled_pinvoke
{
	int32_t ___TSxIVLfBUyELziEzwQrAMRmzGqrD_0;
	int32_t ___zSkfqYfrOXJXcopeOkTalFrxsHk_1;
	int32_t ___pTXqUwcYdENkAAHGFsRzWXKXrZB_2;
};
// Native definition for COM marshalling of Rewired.Platforms.PS4.PS4InputSource/YUATmdkIYeDTVeeKKfwolrMGUrN/ZFbPVMKgGgElciFwNeAzftnIMAqR
struct ZFbPVMKgGgElciFwNeAzftnIMAqR_tAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6_marshaled_com
{
	int32_t ___TSxIVLfBUyELziEzwQrAMRmzGqrD_0;
	int32_t ___zSkfqYfrOXJXcopeOkTalFrxsHk_1;
	int32_t ___pTXqUwcYdENkAAHGFsRzWXKXrZB_2;
};
#endif // ZFBPVMKGGGELCIFWNEAZFTNIMAQR_TAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6_H
#ifndef MAPHELPER_T54C25B42AE16C488282F75EA3EAA33E0869FDC92_H
#define MAPHELPER_T54C25B42AE16C488282F75EA3EAA33E0869FDC92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper
struct  MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92  : public CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB
{
public:
	// cteaRMjZkQaJnmQJcVTSoaSRpeZ Rewired.Player_ControllerHelper_MapHelper::ZFfZVnVNyeelQEpFvDpXhcoeRLel
	cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E * ___ZFfZVnVNyeelQEpFvDpXhcoeRLel_0;
	// Rewired.Player Rewired.Player_ControllerHelper_MapHelper::DelHerRPxmEUeLMSQbMNIPccOrs
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___DelHerRPxmEUeLMSQbMNIPccOrs_1;
	// Rewired.Player_ControllerHelper Rewired.Player_ControllerHelper_MapHelper::vRRcfyGudPCBcdddZEsLFGidKVD
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * ___vRRcfyGudPCBcdddZEsLFGidKVD_2;
	// Rewired.ControllerMapEnabler Rewired.Player_ControllerHelper_MapHelper::LioZUEeJRwaCBYtJXIcadxcIGuff
	ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D * ___LioZUEeJRwaCBYtJXIcadxcIGuff_3;
	// Rewired.ControllerMapLayoutManager Rewired.Player_ControllerHelper_MapHelper::vljqUPZnwJgqfGjUYbLBgPpjcuwp
	ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781 * ___vljqUPZnwJgqfGjUYbLBgPpjcuwp_4;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_5;

public:
	inline static int32_t get_offset_of_ZFfZVnVNyeelQEpFvDpXhcoeRLel_0() { return static_cast<int32_t>(offsetof(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92, ___ZFfZVnVNyeelQEpFvDpXhcoeRLel_0)); }
	inline cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E * get_ZFfZVnVNyeelQEpFvDpXhcoeRLel_0() const { return ___ZFfZVnVNyeelQEpFvDpXhcoeRLel_0; }
	inline cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E ** get_address_of_ZFfZVnVNyeelQEpFvDpXhcoeRLel_0() { return &___ZFfZVnVNyeelQEpFvDpXhcoeRLel_0; }
	inline void set_ZFfZVnVNyeelQEpFvDpXhcoeRLel_0(cteaRMjZkQaJnmQJcVTSoaSRpeZ_t7A1BE12CFF3F4D23E893037C183E9BE7F45D910E * value)
	{
		___ZFfZVnVNyeelQEpFvDpXhcoeRLel_0 = value;
		Il2CppCodeGenWriteBarrier((&___ZFfZVnVNyeelQEpFvDpXhcoeRLel_0), value);
	}

	inline static int32_t get_offset_of_DelHerRPxmEUeLMSQbMNIPccOrs_1() { return static_cast<int32_t>(offsetof(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92, ___DelHerRPxmEUeLMSQbMNIPccOrs_1)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_DelHerRPxmEUeLMSQbMNIPccOrs_1() const { return ___DelHerRPxmEUeLMSQbMNIPccOrs_1; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_DelHerRPxmEUeLMSQbMNIPccOrs_1() { return &___DelHerRPxmEUeLMSQbMNIPccOrs_1; }
	inline void set_DelHerRPxmEUeLMSQbMNIPccOrs_1(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___DelHerRPxmEUeLMSQbMNIPccOrs_1 = value;
		Il2CppCodeGenWriteBarrier((&___DelHerRPxmEUeLMSQbMNIPccOrs_1), value);
	}

	inline static int32_t get_offset_of_vRRcfyGudPCBcdddZEsLFGidKVD_2() { return static_cast<int32_t>(offsetof(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92, ___vRRcfyGudPCBcdddZEsLFGidKVD_2)); }
	inline ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * get_vRRcfyGudPCBcdddZEsLFGidKVD_2() const { return ___vRRcfyGudPCBcdddZEsLFGidKVD_2; }
	inline ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC ** get_address_of_vRRcfyGudPCBcdddZEsLFGidKVD_2() { return &___vRRcfyGudPCBcdddZEsLFGidKVD_2; }
	inline void set_vRRcfyGudPCBcdddZEsLFGidKVD_2(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * value)
	{
		___vRRcfyGudPCBcdddZEsLFGidKVD_2 = value;
		Il2CppCodeGenWriteBarrier((&___vRRcfyGudPCBcdddZEsLFGidKVD_2), value);
	}

	inline static int32_t get_offset_of_LioZUEeJRwaCBYtJXIcadxcIGuff_3() { return static_cast<int32_t>(offsetof(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92, ___LioZUEeJRwaCBYtJXIcadxcIGuff_3)); }
	inline ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D * get_LioZUEeJRwaCBYtJXIcadxcIGuff_3() const { return ___LioZUEeJRwaCBYtJXIcadxcIGuff_3; }
	inline ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D ** get_address_of_LioZUEeJRwaCBYtJXIcadxcIGuff_3() { return &___LioZUEeJRwaCBYtJXIcadxcIGuff_3; }
	inline void set_LioZUEeJRwaCBYtJXIcadxcIGuff_3(ControllerMapEnabler_t72022B3413E35CE64277E9F8F6F64236C8F0148D * value)
	{
		___LioZUEeJRwaCBYtJXIcadxcIGuff_3 = value;
		Il2CppCodeGenWriteBarrier((&___LioZUEeJRwaCBYtJXIcadxcIGuff_3), value);
	}

	inline static int32_t get_offset_of_vljqUPZnwJgqfGjUYbLBgPpjcuwp_4() { return static_cast<int32_t>(offsetof(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92, ___vljqUPZnwJgqfGjUYbLBgPpjcuwp_4)); }
	inline ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781 * get_vljqUPZnwJgqfGjUYbLBgPpjcuwp_4() const { return ___vljqUPZnwJgqfGjUYbLBgPpjcuwp_4; }
	inline ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781 ** get_address_of_vljqUPZnwJgqfGjUYbLBgPpjcuwp_4() { return &___vljqUPZnwJgqfGjUYbLBgPpjcuwp_4; }
	inline void set_vljqUPZnwJgqfGjUYbLBgPpjcuwp_4(ControllerMapLayoutManager_t58D8D581E5DB2F37D020E1A35F0182997BE98781 * value)
	{
		___vljqUPZnwJgqfGjUYbLBgPpjcuwp_4 = value;
		Il2CppCodeGenWriteBarrier((&___vljqUPZnwJgqfGjUYbLBgPpjcuwp_4), value);
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_5() { return static_cast<int32_t>(offsetof(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_5)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_5() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_5; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_5() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_5; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_5(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPHELPER_T54C25B42AE16C488282F75EA3EAA33E0869FDC92_H
#ifndef POLLINGHELPER_TE345DB668E84431AA09C97B278D2F36D1716CB24_H
#define POLLINGHELPER_TE345DB668E84431AA09C97B278D2F36D1716CB24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper
struct  PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24  : public CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB
{
public:
	// Rewired.Player Rewired.Player_ControllerHelper_PollingHelper::DelHerRPxmEUeLMSQbMNIPccOrs
	Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * ___DelHerRPxmEUeLMSQbMNIPccOrs_0;
	// Rewired.Player_ControllerHelper Rewired.Player_ControllerHelper_PollingHelper::vRRcfyGudPCBcdddZEsLFGidKVD
	ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * ___vRRcfyGudPCBcdddZEsLFGidKVD_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper::MHfTbpbnrYCtsEVBnetgGDrfXxM
	int32_t ___MHfTbpbnrYCtsEVBnetgGDrfXxM_2;

public:
	inline static int32_t get_offset_of_DelHerRPxmEUeLMSQbMNIPccOrs_0() { return static_cast<int32_t>(offsetof(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24, ___DelHerRPxmEUeLMSQbMNIPccOrs_0)); }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * get_DelHerRPxmEUeLMSQbMNIPccOrs_0() const { return ___DelHerRPxmEUeLMSQbMNIPccOrs_0; }
	inline Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F ** get_address_of_DelHerRPxmEUeLMSQbMNIPccOrs_0() { return &___DelHerRPxmEUeLMSQbMNIPccOrs_0; }
	inline void set_DelHerRPxmEUeLMSQbMNIPccOrs_0(Player_t0A08FE589DD4B7B6093BF02C3790FF61B65CEB4F * value)
	{
		___DelHerRPxmEUeLMSQbMNIPccOrs_0 = value;
		Il2CppCodeGenWriteBarrier((&___DelHerRPxmEUeLMSQbMNIPccOrs_0), value);
	}

	inline static int32_t get_offset_of_vRRcfyGudPCBcdddZEsLFGidKVD_1() { return static_cast<int32_t>(offsetof(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24, ___vRRcfyGudPCBcdddZEsLFGidKVD_1)); }
	inline ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * get_vRRcfyGudPCBcdddZEsLFGidKVD_1() const { return ___vRRcfyGudPCBcdddZEsLFGidKVD_1; }
	inline ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC ** get_address_of_vRRcfyGudPCBcdddZEsLFGidKVD_1() { return &___vRRcfyGudPCBcdddZEsLFGidKVD_1; }
	inline void set_vRRcfyGudPCBcdddZEsLFGidKVD_1(ControllerHelper_tBC381D3F373CC57D36553FBA0843F6857C3C8DEC * value)
	{
		___vRRcfyGudPCBcdddZEsLFGidKVD_1 = value;
		Il2CppCodeGenWriteBarrier((&___vRRcfyGudPCBcdddZEsLFGidKVD_1), value);
	}

	inline static int32_t get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_2() { return static_cast<int32_t>(offsetof(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24, ___MHfTbpbnrYCtsEVBnetgGDrfXxM_2)); }
	inline int32_t get_MHfTbpbnrYCtsEVBnetgGDrfXxM_2() const { return ___MHfTbpbnrYCtsEVBnetgGDrfXxM_2; }
	inline int32_t* get_address_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_2() { return &___MHfTbpbnrYCtsEVBnetgGDrfXxM_2; }
	inline void set_MHfTbpbnrYCtsEVBnetgGDrfXxM_2(int32_t value)
	{
		___MHfTbpbnrYCtsEVBnetgGDrfXxM_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLLINGHELPER_TE345DB668E84431AA09C97B278D2F36D1716CB24_H
#ifndef CONFIGHELPER_T31FF7505349528864589F8EEFF5AD862845BE7B5_H
#define CONFIGHELPER_T31FF7505349528864589F8EEFF5AD862845BE7B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ConfigHelper
struct  ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5  : public CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB
{
public:

public:
};

struct ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5_StaticFields
{
public:
	// Rewired.ReInput_ConfigHelper Rewired.ReInput_ConfigHelper::FBhGsngGbrZoBDJQYQqNPKqLfYtD
	ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5 * ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0;

public:
	inline static int32_t get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return static_cast<int32_t>(offsetof(ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5_StaticFields, ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0)); }
	inline ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5 * get_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() const { return ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5 ** get_address_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return &___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline void set_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5 * value)
	{
		___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0 = value;
		Il2CppCodeGenWriteBarrier((&___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGHELPER_T31FF7505349528864589F8EEFF5AD862845BE7B5_H
#ifndef CONTROLLERHELPER_T937589B90E36653120519D111871F0C16D15FF62_H
#define CONTROLLERHELPER_T937589B90E36653120519D111871F0C16D15FF62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper
struct  ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62  : public CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB
{
public:
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper::polling
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___polling_1;
	// Rewired.ReInput_ControllerHelper_ConflictCheckingHelper Rewired.ReInput_ControllerHelper::conflictChecking
	ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * ___conflictChecking_2;

public:
	inline static int32_t get_offset_of_polling_1() { return static_cast<int32_t>(offsetof(ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62, ___polling_1)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_polling_1() const { return ___polling_1; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_polling_1() { return &___polling_1; }
	inline void set_polling_1(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___polling_1 = value;
		Il2CppCodeGenWriteBarrier((&___polling_1), value);
	}

	inline static int32_t get_offset_of_conflictChecking_2() { return static_cast<int32_t>(offsetof(ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62, ___conflictChecking_2)); }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * get_conflictChecking_2() const { return ___conflictChecking_2; }
	inline ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 ** get_address_of_conflictChecking_2() { return &___conflictChecking_2; }
	inline void set_conflictChecking_2(ConflictCheckingHelper_t86BCFF8098407F0F2AC252C2EDB520222E11E531 * value)
	{
		___conflictChecking_2 = value;
		Il2CppCodeGenWriteBarrier((&___conflictChecking_2), value);
	}
};

struct ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62_StaticFields
{
public:
	// Rewired.ReInput_ControllerHelper Rewired.ReInput_ControllerHelper::FBhGsngGbrZoBDJQYQqNPKqLfYtD
	ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62 * ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0;

public:
	inline static int32_t get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return static_cast<int32_t>(offsetof(ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62_StaticFields, ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0)); }
	inline ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62 * get_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() const { return ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62 ** get_address_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return &___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline void set_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62 * value)
	{
		___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0 = value;
		Il2CppCodeGenWriteBarrier((&___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERHELPER_T937589B90E36653120519D111871F0C16D15FF62_H
#ifndef POLLINGHELPER_T69EDA9E24833EACC807B10BC63FCE58E1090A29A_H
#define POLLINGHELPER_T69EDA9E24833EACC807B10BC63FCE58E1090A29A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper
struct  PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A  : public CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB
{
public:

public:
};

struct PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A_StaticFields
{
public:
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper::FBhGsngGbrZoBDJQYQqNPKqLfYtD
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0;

public:
	inline static int32_t get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return static_cast<int32_t>(offsetof(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A_StaticFields, ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() const { return ___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0() { return &___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0; }
	inline void set_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0 = value;
		Il2CppCodeGenWriteBarrier((&___FBhGsngGbrZoBDJQYQqNPKqLfYtD_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLLINGHELPER_T69EDA9E24833EACC807B10BC63FCE58E1090A29A_H
#ifndef ENUMERATOR_T1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC_H
#define ENUMERATOR_T1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<Rewired.ActionElementMap>
struct  Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC, ___list_0)); }
	inline List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * get_list_0() const { return ___list_0; }
	inline List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC, ___current_3)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_current_3() const { return ___current_3; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#define NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int64>
struct  Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 
{
public:
	// T System.Nullable`1::value
	int64_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#define AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.AxisRange
struct  AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652 
{
public:
	// System.Int32 Rewired.AxisRange::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisRange_tA638B2A1F196452BCDD82118BFBB4C3E6F60D652, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISRANGE_TA638B2A1F196452BCDD82118BFBB4C3E6F60D652_H
#ifndef CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#define CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerElementType
struct  ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA 
{
public:
	// System.Int32 Rewired.ControllerElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementType_t02BD9FCE85DCC570ED6DBB195E970B84462558BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTTYPE_T02BD9FCE85DCC570ED6DBB195E970B84462558BA_H
#ifndef CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#define CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerType
struct  ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4 
{
public:
	// System.Int32 Rewired.ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_t5D12288D0D40A6917EAF01DB5DE37090A586B2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T5D12288D0D40A6917EAF01DB5DE37090A586B2D4_H
#ifndef ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#define ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentType
struct  ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394 
{
public:
	// System.Int32 Rewired.ElementAssignmentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementAssignmentType_tE22DBF545C1DAB47CE4278D508FD6D78F2165394, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTASSIGNMENTTYPE_TE22DBF545C1DAB47CE4278D508FD6D78F2165394_H
#ifndef INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#define INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.InputSource
struct  InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B 
{
public:
	// System.Int32 Rewired.InputSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputSource_t704235FB75FAAFE4F5ADF403D5028663D7E75D3B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSOURCE_T704235FB75FAAFE4F5ADF403D5028663D7E75D3B_H
#ifndef MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#define MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ModifierKeyFlags
struct  ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651 
{
public:
	// System.Int32 Rewired.ModifierKeyFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModifierKeyFlags_t4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERKEYFLAGS_T4E8D341CA3EDC0A08C57C86EAB6FA1917CB65651_H
#ifndef JOYSTICK_T6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09_H
#define JOYSTICK_T6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Custom.CustomInputSource_Joystick
struct  Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09  : public Controller_tFFAE4FD2CE67B49F2DC8E1C1C35081EAAC9AF34B
{
public:
	// System.Nullable`1<System.Int64> Rewired.Platforms.Custom.CustomInputSource_Joystick::kNCtRZYNhfpgNEjWCvYWWaJdsPw
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___kNCtRZYNhfpgNEjWCvYWWaJdsPw_3;
	// System.Int32 Rewired.Platforms.Custom.CustomInputSource_Joystick::pVMgxVlhOWBbMBjIelvXUbEMRHTh
	int32_t ___pVMgxVlhOWBbMBjIelvXUbEMRHTh_4;
	// Rewired.Platforms.Custom.CustomInputSource_Axis[] Rewired.Platforms.Custom.CustomInputSource_Joystick::ewtEGQKoSuqZGbCfKTwTvaVIdwwn
	AxisU5BU5D_tD22230ADED67D8E860DD8F4226514D79EDE55BEE* ___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5;
	// Rewired.Platforms.Custom.CustomInputSource_Button[] Rewired.Platforms.Custom.CustomInputSource_Joystick::QjgkqxyMBXGPcdjZSGsOcXzPxUl
	ButtonU5BU5D_t181B5EB5ACB98CA3C44C3FB3DF613259B353B43A* ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_6;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Platforms.Custom.CustomInputSource_Axis> Rewired.Platforms.Custom.CustomInputSource_Joystick::ofSgeiiRBYviNkjEOCsJhBMuHgr
	ReadOnlyCollection_1_t4829C3B53F3992A9308E9687500F009CA89848AC * ___ofSgeiiRBYviNkjEOCsJhBMuHgr_7;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Platforms.Custom.CustomInputSource_Button> Rewired.Platforms.Custom.CustomInputSource_Joystick::xToauoyvRfSQtEIgZmpTaDMqGuB
	ReadOnlyCollection_1_t8D1C0984E4131D0AD8A662731FDECC171DD80B5F * ___xToauoyvRfSQtEIgZmpTaDMqGuB_8;
	// System.Boolean Rewired.Platforms.Custom.CustomInputSource_Joystick::OWbAvMhWYnpGDbwOiHPyDnDXREx
	bool ___OWbAvMhWYnpGDbwOiHPyDnDXREx_9;
	// Rewired.Controller_Extension Rewired.Platforms.Custom.CustomInputSource_Joystick::keBYOCobzoABWhYylPhjApVSLXnN
	Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * ___keBYOCobzoABWhYylPhjApVSLXnN_10;

public:
	inline static int32_t get_offset_of_kNCtRZYNhfpgNEjWCvYWWaJdsPw_3() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___kNCtRZYNhfpgNEjWCvYWWaJdsPw_3)); }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  get_kNCtRZYNhfpgNEjWCvYWWaJdsPw_3() const { return ___kNCtRZYNhfpgNEjWCvYWWaJdsPw_3; }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 * get_address_of_kNCtRZYNhfpgNEjWCvYWWaJdsPw_3() { return &___kNCtRZYNhfpgNEjWCvYWWaJdsPw_3; }
	inline void set_kNCtRZYNhfpgNEjWCvYWWaJdsPw_3(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  value)
	{
		___kNCtRZYNhfpgNEjWCvYWWaJdsPw_3 = value;
	}

	inline static int32_t get_offset_of_pVMgxVlhOWBbMBjIelvXUbEMRHTh_4() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___pVMgxVlhOWBbMBjIelvXUbEMRHTh_4)); }
	inline int32_t get_pVMgxVlhOWBbMBjIelvXUbEMRHTh_4() const { return ___pVMgxVlhOWBbMBjIelvXUbEMRHTh_4; }
	inline int32_t* get_address_of_pVMgxVlhOWBbMBjIelvXUbEMRHTh_4() { return &___pVMgxVlhOWBbMBjIelvXUbEMRHTh_4; }
	inline void set_pVMgxVlhOWBbMBjIelvXUbEMRHTh_4(int32_t value)
	{
		___pVMgxVlhOWBbMBjIelvXUbEMRHTh_4 = value;
	}

	inline static int32_t get_offset_of_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5)); }
	inline AxisU5BU5D_tD22230ADED67D8E860DD8F4226514D79EDE55BEE* get_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5() const { return ___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5; }
	inline AxisU5BU5D_tD22230ADED67D8E860DD8F4226514D79EDE55BEE** get_address_of_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5() { return &___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5; }
	inline void set_ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5(AxisU5BU5D_tD22230ADED67D8E860DD8F4226514D79EDE55BEE* value)
	{
		___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5 = value;
		Il2CppCodeGenWriteBarrier((&___ewtEGQKoSuqZGbCfKTwTvaVIdwwn_5), value);
	}

	inline static int32_t get_offset_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_6() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_6)); }
	inline ButtonU5BU5D_t181B5EB5ACB98CA3C44C3FB3DF613259B353B43A* get_QjgkqxyMBXGPcdjZSGsOcXzPxUl_6() const { return ___QjgkqxyMBXGPcdjZSGsOcXzPxUl_6; }
	inline ButtonU5BU5D_t181B5EB5ACB98CA3C44C3FB3DF613259B353B43A** get_address_of_QjgkqxyMBXGPcdjZSGsOcXzPxUl_6() { return &___QjgkqxyMBXGPcdjZSGsOcXzPxUl_6; }
	inline void set_QjgkqxyMBXGPcdjZSGsOcXzPxUl_6(ButtonU5BU5D_t181B5EB5ACB98CA3C44C3FB3DF613259B353B43A* value)
	{
		___QjgkqxyMBXGPcdjZSGsOcXzPxUl_6 = value;
		Il2CppCodeGenWriteBarrier((&___QjgkqxyMBXGPcdjZSGsOcXzPxUl_6), value);
	}

	inline static int32_t get_offset_of_ofSgeiiRBYviNkjEOCsJhBMuHgr_7() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___ofSgeiiRBYviNkjEOCsJhBMuHgr_7)); }
	inline ReadOnlyCollection_1_t4829C3B53F3992A9308E9687500F009CA89848AC * get_ofSgeiiRBYviNkjEOCsJhBMuHgr_7() const { return ___ofSgeiiRBYviNkjEOCsJhBMuHgr_7; }
	inline ReadOnlyCollection_1_t4829C3B53F3992A9308E9687500F009CA89848AC ** get_address_of_ofSgeiiRBYviNkjEOCsJhBMuHgr_7() { return &___ofSgeiiRBYviNkjEOCsJhBMuHgr_7; }
	inline void set_ofSgeiiRBYviNkjEOCsJhBMuHgr_7(ReadOnlyCollection_1_t4829C3B53F3992A9308E9687500F009CA89848AC * value)
	{
		___ofSgeiiRBYviNkjEOCsJhBMuHgr_7 = value;
		Il2CppCodeGenWriteBarrier((&___ofSgeiiRBYviNkjEOCsJhBMuHgr_7), value);
	}

	inline static int32_t get_offset_of_xToauoyvRfSQtEIgZmpTaDMqGuB_8() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___xToauoyvRfSQtEIgZmpTaDMqGuB_8)); }
	inline ReadOnlyCollection_1_t8D1C0984E4131D0AD8A662731FDECC171DD80B5F * get_xToauoyvRfSQtEIgZmpTaDMqGuB_8() const { return ___xToauoyvRfSQtEIgZmpTaDMqGuB_8; }
	inline ReadOnlyCollection_1_t8D1C0984E4131D0AD8A662731FDECC171DD80B5F ** get_address_of_xToauoyvRfSQtEIgZmpTaDMqGuB_8() { return &___xToauoyvRfSQtEIgZmpTaDMqGuB_8; }
	inline void set_xToauoyvRfSQtEIgZmpTaDMqGuB_8(ReadOnlyCollection_1_t8D1C0984E4131D0AD8A662731FDECC171DD80B5F * value)
	{
		___xToauoyvRfSQtEIgZmpTaDMqGuB_8 = value;
		Il2CppCodeGenWriteBarrier((&___xToauoyvRfSQtEIgZmpTaDMqGuB_8), value);
	}

	inline static int32_t get_offset_of_OWbAvMhWYnpGDbwOiHPyDnDXREx_9() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___OWbAvMhWYnpGDbwOiHPyDnDXREx_9)); }
	inline bool get_OWbAvMhWYnpGDbwOiHPyDnDXREx_9() const { return ___OWbAvMhWYnpGDbwOiHPyDnDXREx_9; }
	inline bool* get_address_of_OWbAvMhWYnpGDbwOiHPyDnDXREx_9() { return &___OWbAvMhWYnpGDbwOiHPyDnDXREx_9; }
	inline void set_OWbAvMhWYnpGDbwOiHPyDnDXREx_9(bool value)
	{
		___OWbAvMhWYnpGDbwOiHPyDnDXREx_9 = value;
	}

	inline static int32_t get_offset_of_keBYOCobzoABWhYylPhjApVSLXnN_10() { return static_cast<int32_t>(offsetof(Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09, ___keBYOCobzoABWhYylPhjApVSLXnN_10)); }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * get_keBYOCobzoABWhYylPhjApVSLXnN_10() const { return ___keBYOCobzoABWhYylPhjApVSLXnN_10; }
	inline Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD ** get_address_of_keBYOCobzoABWhYylPhjApVSLXnN_10() { return &___keBYOCobzoABWhYylPhjApVSLXnN_10; }
	inline void set_keBYOCobzoABWhYylPhjApVSLXnN_10(Extension_tB52F54389416488161F1C79DFA089F142E5AD3FD * value)
	{
		___keBYOCobzoABWhYylPhjApVSLXnN_10 = value;
		Il2CppCodeGenWriteBarrier((&___keBYOCobzoABWhYylPhjApVSLXnN_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09_H
#ifndef EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#define EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.EditorPlatform
struct  EditorPlatform_tBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC 
{
public:
	// System.Int32 Rewired.Platforms.EditorPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EditorPlatform_tBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORPLATFORM_TBA8BFF0D3CB0A8D8D9B811315F3D9A5BC9F38FEC_H
#ifndef PADCONTROLLERINFORMATION_T65AB8B52C8637880F29C122CA4E0CE678B5A80FC_H
#define PADCONTROLLERINFORMATION_T65AB8B52C8637880F29C122CA4E0CE678B5A80FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.Internal.PadControllerInformation
struct  PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC 
{
public:
	// Rewired.Platforms.PS4.Internal.PadTouchPadInformation Rewired.Platforms.PS4.Internal.PadControllerInformation::touchPadInfo
	PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D  ___touchPadInfo_0;
	// Rewired.Platforms.PS4.Internal.PadStickInformation Rewired.Platforms.PS4.Internal.PadControllerInformation::stickInfo
	PadStickInformation_t6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C  ___stickInfo_1;
	// System.Byte Rewired.Platforms.PS4.Internal.PadControllerInformation::connectionType
	uint8_t ___connectionType_2;
	// System.Byte Rewired.Platforms.PS4.Internal.PadControllerInformation::connectedCount
	uint8_t ___connectedCount_3;
	// System.Int32 Rewired.Platforms.PS4.Internal.PadControllerInformation::connected
	int32_t ___connected_4;
	// System.Int32 Rewired.Platforms.PS4.Internal.PadControllerInformation::deviceClass
	int32_t ___deviceClass_5;
	// System.Byte Rewired.Platforms.PS4.Internal.PadControllerInformation::reserve0
	uint8_t ___reserve0_6;
	// System.Byte Rewired.Platforms.PS4.Internal.PadControllerInformation::reserve1
	uint8_t ___reserve1_7;
	// System.Byte Rewired.Platforms.PS4.Internal.PadControllerInformation::reserve2
	uint8_t ___reserve2_8;
	// System.Byte Rewired.Platforms.PS4.Internal.PadControllerInformation::reserve3
	uint8_t ___reserve3_9;
	// System.Byte Rewired.Platforms.PS4.Internal.PadControllerInformation::reserve4
	uint8_t ___reserve4_10;
	// System.Byte Rewired.Platforms.PS4.Internal.PadControllerInformation::reserve5
	uint8_t ___reserve5_11;
	// System.Byte Rewired.Platforms.PS4.Internal.PadControllerInformation::reserve6
	uint8_t ___reserve6_12;
	// System.Byte Rewired.Platforms.PS4.Internal.PadControllerInformation::reserve7
	uint8_t ___reserve7_13;

public:
	inline static int32_t get_offset_of_touchPadInfo_0() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___touchPadInfo_0)); }
	inline PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D  get_touchPadInfo_0() const { return ___touchPadInfo_0; }
	inline PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D * get_address_of_touchPadInfo_0() { return &___touchPadInfo_0; }
	inline void set_touchPadInfo_0(PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D  value)
	{
		___touchPadInfo_0 = value;
	}

	inline static int32_t get_offset_of_stickInfo_1() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___stickInfo_1)); }
	inline PadStickInformation_t6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C  get_stickInfo_1() const { return ___stickInfo_1; }
	inline PadStickInformation_t6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C * get_address_of_stickInfo_1() { return &___stickInfo_1; }
	inline void set_stickInfo_1(PadStickInformation_t6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C  value)
	{
		___stickInfo_1 = value;
	}

	inline static int32_t get_offset_of_connectionType_2() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___connectionType_2)); }
	inline uint8_t get_connectionType_2() const { return ___connectionType_2; }
	inline uint8_t* get_address_of_connectionType_2() { return &___connectionType_2; }
	inline void set_connectionType_2(uint8_t value)
	{
		___connectionType_2 = value;
	}

	inline static int32_t get_offset_of_connectedCount_3() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___connectedCount_3)); }
	inline uint8_t get_connectedCount_3() const { return ___connectedCount_3; }
	inline uint8_t* get_address_of_connectedCount_3() { return &___connectedCount_3; }
	inline void set_connectedCount_3(uint8_t value)
	{
		___connectedCount_3 = value;
	}

	inline static int32_t get_offset_of_connected_4() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___connected_4)); }
	inline int32_t get_connected_4() const { return ___connected_4; }
	inline int32_t* get_address_of_connected_4() { return &___connected_4; }
	inline void set_connected_4(int32_t value)
	{
		___connected_4 = value;
	}

	inline static int32_t get_offset_of_deviceClass_5() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___deviceClass_5)); }
	inline int32_t get_deviceClass_5() const { return ___deviceClass_5; }
	inline int32_t* get_address_of_deviceClass_5() { return &___deviceClass_5; }
	inline void set_deviceClass_5(int32_t value)
	{
		___deviceClass_5 = value;
	}

	inline static int32_t get_offset_of_reserve0_6() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___reserve0_6)); }
	inline uint8_t get_reserve0_6() const { return ___reserve0_6; }
	inline uint8_t* get_address_of_reserve0_6() { return &___reserve0_6; }
	inline void set_reserve0_6(uint8_t value)
	{
		___reserve0_6 = value;
	}

	inline static int32_t get_offset_of_reserve1_7() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___reserve1_7)); }
	inline uint8_t get_reserve1_7() const { return ___reserve1_7; }
	inline uint8_t* get_address_of_reserve1_7() { return &___reserve1_7; }
	inline void set_reserve1_7(uint8_t value)
	{
		___reserve1_7 = value;
	}

	inline static int32_t get_offset_of_reserve2_8() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___reserve2_8)); }
	inline uint8_t get_reserve2_8() const { return ___reserve2_8; }
	inline uint8_t* get_address_of_reserve2_8() { return &___reserve2_8; }
	inline void set_reserve2_8(uint8_t value)
	{
		___reserve2_8 = value;
	}

	inline static int32_t get_offset_of_reserve3_9() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___reserve3_9)); }
	inline uint8_t get_reserve3_9() const { return ___reserve3_9; }
	inline uint8_t* get_address_of_reserve3_9() { return &___reserve3_9; }
	inline void set_reserve3_9(uint8_t value)
	{
		___reserve3_9 = value;
	}

	inline static int32_t get_offset_of_reserve4_10() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___reserve4_10)); }
	inline uint8_t get_reserve4_10() const { return ___reserve4_10; }
	inline uint8_t* get_address_of_reserve4_10() { return &___reserve4_10; }
	inline void set_reserve4_10(uint8_t value)
	{
		___reserve4_10 = value;
	}

	inline static int32_t get_offset_of_reserve5_11() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___reserve5_11)); }
	inline uint8_t get_reserve5_11() const { return ___reserve5_11; }
	inline uint8_t* get_address_of_reserve5_11() { return &___reserve5_11; }
	inline void set_reserve5_11(uint8_t value)
	{
		___reserve5_11 = value;
	}

	inline static int32_t get_offset_of_reserve6_12() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___reserve6_12)); }
	inline uint8_t get_reserve6_12() const { return ___reserve6_12; }
	inline uint8_t* get_address_of_reserve6_12() { return &___reserve6_12; }
	inline void set_reserve6_12(uint8_t value)
	{
		___reserve6_12 = value;
	}

	inline static int32_t get_offset_of_reserve7_13() { return static_cast<int32_t>(offsetof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC, ___reserve7_13)); }
	inline uint8_t get_reserve7_13() const { return ___reserve7_13; }
	inline uint8_t* get_address_of_reserve7_13() { return &___reserve7_13; }
	inline void set_reserve7_13(uint8_t value)
	{
		___reserve7_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADCONTROLLERINFORMATION_T65AB8B52C8637880F29C122CA4E0CE678B5A80FC_H
#ifndef PS4GAMEPADEXTENSION_TA4039D658BDA1F08F9AE228646102C7DE9ED9A47_H
#define PS4GAMEPADEXTENSION_TA4039D658BDA1F08F9AE228646102C7DE9ED9A47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4GamepadExtension
struct  PS4GamepadExtension_tA4039D658BDA1F08F9AE228646102C7DE9ED9A47  : public PS4ControllerExtension_t027116553D089D8E7A625D4826DF5120FFCB07FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PS4GAMEPADEXTENSION_TA4039D658BDA1F08F9AE228646102C7DE9ED9A47_H
#ifndef PS4GAMEPADMOTORTYPE_T9D22790C80C84BC0A22FFD0682A01D53C7C95BC8_H
#define PS4GAMEPADMOTORTYPE_T9D22790C80C84BC0A22FFD0682A01D53C7C95BC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4GamepadMotorType
struct  PS4GamepadMotorType_t9D22790C80C84BC0A22FFD0682A01D53C7C95BC8 
{
public:
	// System.Int32 Rewired.Platforms.PS4.PS4GamepadMotorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PS4GamepadMotorType_t9D22790C80C84BC0A22FFD0682A01D53C7C95BC8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PS4GAMEPADMOTORTYPE_T9D22790C80C84BC0A22FFD0682A01D53C7C95BC8_H
#ifndef CONTROLLERTYPE_T4C7A641934DBB047AA97D6BFF4B188AFA39D5A14_H
#define CONTROLLERTYPE_T4C7A641934DBB047AA97D6BFF4B188AFA39D5A14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_ControllerType
struct  ControllerType_t4C7A641934DBB047AA97D6BFF4B188AFA39D5A14 
{
public:
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_t4C7A641934DBB047AA97D6BFF4B188AFA39D5A14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERTYPE_T4C7A641934DBB047AA97D6BFF4B188AFA39D5A14_H
#ifndef QQVLGRRTXSDSUCHCHUGCEBVCWDD_T25289B63B5531BB2D243558E594B9D07FEAF1AA4_H
#define QQVLGRRTXSDSUCHCHUGCEBVCWDD_T25289B63B5531BB2D243558E594B9D07FEAF1AA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_qqVLgrRtXSDSUCHChUgcEBVcwDd
struct  qqVLgrRtXSDSUCHChUgcEBVcwDd_t25289B63B5531BB2D243558E594B9D07FEAF1AA4 
{
public:
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_qqVLgrRtXSDSUCHChUgcEBVcwDd::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(qqVLgrRtXSDSUCHChUgcEBVcwDd_t25289B63B5531BB2D243558E594B9D07FEAF1AA4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QQVLGRRTXSDSUCHCHUGCEBVCWDD_T25289B63B5531BB2D243558E594B9D07FEAF1AA4_H
#ifndef KLWFQFEHNGKKCCATNYPKZPCIVWP_TD45CBA99A77C0D67BB59A8B3E713FA67899715E3_H
#define KLWFQFEHNGKKCCATNYPKZPCIVWP_TD45CBA99A77C0D67BB59A8B3E713FA67899715E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_KlWFQfeHnGkKCcatnYpkzPcivWP
struct  KlWFQfeHnGkKCcatnYpkzPcivWP_tD45CBA99A77C0D67BB59A8B3E713FA67899715E3 
{
public:
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN_KlWFQfeHnGkKCcatnYpkzPcivWP::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KlWFQfeHnGkKCcatnYpkzPcivWP_tD45CBA99A77C0D67BB59A8B3E713FA67899715E3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KLWFQFEHNGKKCCATNYPKZPCIVWP_TD45CBA99A77C0D67BB59A8B3E713FA67899715E3_H
#ifndef PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#define PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Platform
struct  Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD 
{
public:
	// System.Int32 Rewired.Platforms.Platform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Platform_tCEB45784D4886D5E98F98C460FB53D00A2BD86AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_TCEB45784D4886D5E98F98C460FB53D00A2BD86AD_H
#ifndef WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#define WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.WebplayerPlatform
struct  WebplayerPlatform_tCE2DAFDB23E582591F2C39C73279153D7E0C3A82 
{
public:
	// System.Int32 Rewired.Platforms.WebplayerPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebplayerPlatform_tCE2DAFDB23E582591F2C39C73279153D7E0C3A82, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPLAYERPLATFORM_TCE2DAFDB23E582591F2C39C73279153D7E0C3A82_H
#ifndef OZYWVRHQBYXUVMAXTYLPPDFTGTK_T435B6195479C21B59EA721DD1208873F0878FCE5_H
#define OZYWVRHQBYXUVMAXTYLPPDFTGTK_T435B6195479C21B59EA721DD1208873F0878FCE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk
struct  ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.IControllerElementTarget Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::qJByvTGLorKeLizFyjtftOInyVr
	RuntimeObject* ___qJByvTGLorKeLizFyjtftOInyVr_4;
	// Rewired.IControllerElementTarget Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::SanVLbjjVuCJGdzcsHmJAMmULDaN
	RuntimeObject* ___SanVLbjjVuCJGdzcsHmJAMmULDaN_5;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::YIiyDAYOdHrCPEfxXABEMrhVIIXe
	bool ___YIiyDAYOdHrCPEfxXABEMrhVIIXe_6;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::rpBVoAOjatpRxhDzOjWKkXyzjmH
	bool ___rpBVoAOjatpRxhDzOjWKkXyzjmH_7;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_8;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11;
	// Rewired.Controller Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::PdPpJvbafbhutHErebhedpBvgcqA
	Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * ___PdPpJvbafbhutHErebhedpBvgcqA_12;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::SmYnMfHcfpeceinsLzULWLXqcdl
	RuntimeObject* ___SmYnMfHcfpeceinsLzULWLXqcdl_13;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::BhwDcIGwaSCzBcJVoVqHOOQdOIGv
	int32_t ___BhwDcIGwaSCzBcJVoVqHOOQdOIGv_14;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::weuKCoZAmvCvVioJahQuGnpAFJML
	int32_t ___weuKCoZAmvCvVioJahQuGnpAFJML_15;
	// JrKetNtIJINuYILTEYSwNUvgPiW Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::MffrcHpPtnkBaVREoXdvthgNpbj
	RuntimeObject* ___MffrcHpPtnkBaVREoXdvthgNpbj_16;
	// System.Collections.Generic.IList`1<Rewired.ControllerMap> Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::RfsvAmBMJrTOuTeemIwOAGGTbHJ
	RuntimeObject* ___RfsvAmBMJrTOuTeemIwOAGGTbHJ_17;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::LAHgYmXkprDMygwUGRhhkYhpjQz
	int32_t ___LAHgYmXkprDMygwUGRhhkYhpjQz_18;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::BAQaFFarWZvZRaVLfPrWBSfJKdqz
	int32_t ___BAQaFFarWZvZRaVLfPrWBSfJKdqz_19;
	// Rewired.ControllerMap Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::qPKqUoJEYXfxoFlGpDGRDUxHGpGp
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___qPKqUoJEYXfxoFlGpDGRDUxHGpGp_20;
	// Rewired.Utils.TempListPool_TList`1<Rewired.ActionElementMap> Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::qnyzmLlKIjBWsBItCynTkLPktojQ
	TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA * ___qnyzmLlKIjBWsBItCynTkLPktojQ_21;
	// System.Collections.Generic.List`1<Rewired.ActionElementMap> Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::PwphjbqhFYBgNlblbCunFlclFYG
	List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * ___PwphjbqhFYBgNlblbCunFlclFYG_22;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::SsLgDdKsoZjfwHynFGZqtFpuMaQN
	bool ___SsLgDdKsoZjfwHynFGZqtFpuMaQN_23;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::nhzvhDxVQckJidNZgcMYPImmKAB
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___nhzvhDxVQckJidNZgcMYPImmKAB_24;
	// System.Collections.Generic.List`1_Enumerator<Rewired.ActionElementMap> Rewired.Player_ControllerHelper_MapHelper_ozYWVrhqByXUVmaxtyLPPdFtgTk::FjaMvlDEramtbltDJipLxhnSGjk
	Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC  ___FjaMvlDEramtbltDJipLxhnSGjk_25;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_qJByvTGLorKeLizFyjtftOInyVr_4() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___qJByvTGLorKeLizFyjtftOInyVr_4)); }
	inline RuntimeObject* get_qJByvTGLorKeLizFyjtftOInyVr_4() const { return ___qJByvTGLorKeLizFyjtftOInyVr_4; }
	inline RuntimeObject** get_address_of_qJByvTGLorKeLizFyjtftOInyVr_4() { return &___qJByvTGLorKeLizFyjtftOInyVr_4; }
	inline void set_qJByvTGLorKeLizFyjtftOInyVr_4(RuntimeObject* value)
	{
		___qJByvTGLorKeLizFyjtftOInyVr_4 = value;
		Il2CppCodeGenWriteBarrier((&___qJByvTGLorKeLizFyjtftOInyVr_4), value);
	}

	inline static int32_t get_offset_of_SanVLbjjVuCJGdzcsHmJAMmULDaN_5() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___SanVLbjjVuCJGdzcsHmJAMmULDaN_5)); }
	inline RuntimeObject* get_SanVLbjjVuCJGdzcsHmJAMmULDaN_5() const { return ___SanVLbjjVuCJGdzcsHmJAMmULDaN_5; }
	inline RuntimeObject** get_address_of_SanVLbjjVuCJGdzcsHmJAMmULDaN_5() { return &___SanVLbjjVuCJGdzcsHmJAMmULDaN_5; }
	inline void set_SanVLbjjVuCJGdzcsHmJAMmULDaN_5(RuntimeObject* value)
	{
		___SanVLbjjVuCJGdzcsHmJAMmULDaN_5 = value;
		Il2CppCodeGenWriteBarrier((&___SanVLbjjVuCJGdzcsHmJAMmULDaN_5), value);
	}

	inline static int32_t get_offset_of_YIiyDAYOdHrCPEfxXABEMrhVIIXe_6() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___YIiyDAYOdHrCPEfxXABEMrhVIIXe_6)); }
	inline bool get_YIiyDAYOdHrCPEfxXABEMrhVIIXe_6() const { return ___YIiyDAYOdHrCPEfxXABEMrhVIIXe_6; }
	inline bool* get_address_of_YIiyDAYOdHrCPEfxXABEMrhVIIXe_6() { return &___YIiyDAYOdHrCPEfxXABEMrhVIIXe_6; }
	inline void set_YIiyDAYOdHrCPEfxXABEMrhVIIXe_6(bool value)
	{
		___YIiyDAYOdHrCPEfxXABEMrhVIIXe_6 = value;
	}

	inline static int32_t get_offset_of_rpBVoAOjatpRxhDzOjWKkXyzjmH_7() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___rpBVoAOjatpRxhDzOjWKkXyzjmH_7)); }
	inline bool get_rpBVoAOjatpRxhDzOjWKkXyzjmH_7() const { return ___rpBVoAOjatpRxhDzOjWKkXyzjmH_7; }
	inline bool* get_address_of_rpBVoAOjatpRxhDzOjWKkXyzjmH_7() { return &___rpBVoAOjatpRxhDzOjWKkXyzjmH_7; }
	inline void set_rpBVoAOjatpRxhDzOjWKkXyzjmH_7(bool value)
	{
		___rpBVoAOjatpRxhDzOjWKkXyzjmH_7 = value;
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_8() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___pTVJDeVeJlQThAZkFztHqDAWDUa_8)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_8() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_8; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_8() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_8; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_8(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_8 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_10; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_10(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_10 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11 = value;
	}

	inline static int32_t get_offset_of_PdPpJvbafbhutHErebhedpBvgcqA_12() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___PdPpJvbafbhutHErebhedpBvgcqA_12)); }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * get_PdPpJvbafbhutHErebhedpBvgcqA_12() const { return ___PdPpJvbafbhutHErebhedpBvgcqA_12; }
	inline Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 ** get_address_of_PdPpJvbafbhutHErebhedpBvgcqA_12() { return &___PdPpJvbafbhutHErebhedpBvgcqA_12; }
	inline void set_PdPpJvbafbhutHErebhedpBvgcqA_12(Controller_t5780C278CCFB7A81A0272F3B056B1EF463D9B671 * value)
	{
		___PdPpJvbafbhutHErebhedpBvgcqA_12 = value;
		Il2CppCodeGenWriteBarrier((&___PdPpJvbafbhutHErebhedpBvgcqA_12), value);
	}

	inline static int32_t get_offset_of_SmYnMfHcfpeceinsLzULWLXqcdl_13() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___SmYnMfHcfpeceinsLzULWLXqcdl_13)); }
	inline RuntimeObject* get_SmYnMfHcfpeceinsLzULWLXqcdl_13() const { return ___SmYnMfHcfpeceinsLzULWLXqcdl_13; }
	inline RuntimeObject** get_address_of_SmYnMfHcfpeceinsLzULWLXqcdl_13() { return &___SmYnMfHcfpeceinsLzULWLXqcdl_13; }
	inline void set_SmYnMfHcfpeceinsLzULWLXqcdl_13(RuntimeObject* value)
	{
		___SmYnMfHcfpeceinsLzULWLXqcdl_13 = value;
		Il2CppCodeGenWriteBarrier((&___SmYnMfHcfpeceinsLzULWLXqcdl_13), value);
	}

	inline static int32_t get_offset_of_BhwDcIGwaSCzBcJVoVqHOOQdOIGv_14() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___BhwDcIGwaSCzBcJVoVqHOOQdOIGv_14)); }
	inline int32_t get_BhwDcIGwaSCzBcJVoVqHOOQdOIGv_14() const { return ___BhwDcIGwaSCzBcJVoVqHOOQdOIGv_14; }
	inline int32_t* get_address_of_BhwDcIGwaSCzBcJVoVqHOOQdOIGv_14() { return &___BhwDcIGwaSCzBcJVoVqHOOQdOIGv_14; }
	inline void set_BhwDcIGwaSCzBcJVoVqHOOQdOIGv_14(int32_t value)
	{
		___BhwDcIGwaSCzBcJVoVqHOOQdOIGv_14 = value;
	}

	inline static int32_t get_offset_of_weuKCoZAmvCvVioJahQuGnpAFJML_15() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___weuKCoZAmvCvVioJahQuGnpAFJML_15)); }
	inline int32_t get_weuKCoZAmvCvVioJahQuGnpAFJML_15() const { return ___weuKCoZAmvCvVioJahQuGnpAFJML_15; }
	inline int32_t* get_address_of_weuKCoZAmvCvVioJahQuGnpAFJML_15() { return &___weuKCoZAmvCvVioJahQuGnpAFJML_15; }
	inline void set_weuKCoZAmvCvVioJahQuGnpAFJML_15(int32_t value)
	{
		___weuKCoZAmvCvVioJahQuGnpAFJML_15 = value;
	}

	inline static int32_t get_offset_of_MffrcHpPtnkBaVREoXdvthgNpbj_16() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___MffrcHpPtnkBaVREoXdvthgNpbj_16)); }
	inline RuntimeObject* get_MffrcHpPtnkBaVREoXdvthgNpbj_16() const { return ___MffrcHpPtnkBaVREoXdvthgNpbj_16; }
	inline RuntimeObject** get_address_of_MffrcHpPtnkBaVREoXdvthgNpbj_16() { return &___MffrcHpPtnkBaVREoXdvthgNpbj_16; }
	inline void set_MffrcHpPtnkBaVREoXdvthgNpbj_16(RuntimeObject* value)
	{
		___MffrcHpPtnkBaVREoXdvthgNpbj_16 = value;
		Il2CppCodeGenWriteBarrier((&___MffrcHpPtnkBaVREoXdvthgNpbj_16), value);
	}

	inline static int32_t get_offset_of_RfsvAmBMJrTOuTeemIwOAGGTbHJ_17() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___RfsvAmBMJrTOuTeemIwOAGGTbHJ_17)); }
	inline RuntimeObject* get_RfsvAmBMJrTOuTeemIwOAGGTbHJ_17() const { return ___RfsvAmBMJrTOuTeemIwOAGGTbHJ_17; }
	inline RuntimeObject** get_address_of_RfsvAmBMJrTOuTeemIwOAGGTbHJ_17() { return &___RfsvAmBMJrTOuTeemIwOAGGTbHJ_17; }
	inline void set_RfsvAmBMJrTOuTeemIwOAGGTbHJ_17(RuntimeObject* value)
	{
		___RfsvAmBMJrTOuTeemIwOAGGTbHJ_17 = value;
		Il2CppCodeGenWriteBarrier((&___RfsvAmBMJrTOuTeemIwOAGGTbHJ_17), value);
	}

	inline static int32_t get_offset_of_LAHgYmXkprDMygwUGRhhkYhpjQz_18() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___LAHgYmXkprDMygwUGRhhkYhpjQz_18)); }
	inline int32_t get_LAHgYmXkprDMygwUGRhhkYhpjQz_18() const { return ___LAHgYmXkprDMygwUGRhhkYhpjQz_18; }
	inline int32_t* get_address_of_LAHgYmXkprDMygwUGRhhkYhpjQz_18() { return &___LAHgYmXkprDMygwUGRhhkYhpjQz_18; }
	inline void set_LAHgYmXkprDMygwUGRhhkYhpjQz_18(int32_t value)
	{
		___LAHgYmXkprDMygwUGRhhkYhpjQz_18 = value;
	}

	inline static int32_t get_offset_of_BAQaFFarWZvZRaVLfPrWBSfJKdqz_19() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___BAQaFFarWZvZRaVLfPrWBSfJKdqz_19)); }
	inline int32_t get_BAQaFFarWZvZRaVLfPrWBSfJKdqz_19() const { return ___BAQaFFarWZvZRaVLfPrWBSfJKdqz_19; }
	inline int32_t* get_address_of_BAQaFFarWZvZRaVLfPrWBSfJKdqz_19() { return &___BAQaFFarWZvZRaVLfPrWBSfJKdqz_19; }
	inline void set_BAQaFFarWZvZRaVLfPrWBSfJKdqz_19(int32_t value)
	{
		___BAQaFFarWZvZRaVLfPrWBSfJKdqz_19 = value;
	}

	inline static int32_t get_offset_of_qPKqUoJEYXfxoFlGpDGRDUxHGpGp_20() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___qPKqUoJEYXfxoFlGpDGRDUxHGpGp_20)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_qPKqUoJEYXfxoFlGpDGRDUxHGpGp_20() const { return ___qPKqUoJEYXfxoFlGpDGRDUxHGpGp_20; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_qPKqUoJEYXfxoFlGpDGRDUxHGpGp_20() { return &___qPKqUoJEYXfxoFlGpDGRDUxHGpGp_20; }
	inline void set_qPKqUoJEYXfxoFlGpDGRDUxHGpGp_20(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___qPKqUoJEYXfxoFlGpDGRDUxHGpGp_20 = value;
		Il2CppCodeGenWriteBarrier((&___qPKqUoJEYXfxoFlGpDGRDUxHGpGp_20), value);
	}

	inline static int32_t get_offset_of_qnyzmLlKIjBWsBItCynTkLPktojQ_21() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___qnyzmLlKIjBWsBItCynTkLPktojQ_21)); }
	inline TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA * get_qnyzmLlKIjBWsBItCynTkLPktojQ_21() const { return ___qnyzmLlKIjBWsBItCynTkLPktojQ_21; }
	inline TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA ** get_address_of_qnyzmLlKIjBWsBItCynTkLPktojQ_21() { return &___qnyzmLlKIjBWsBItCynTkLPktojQ_21; }
	inline void set_qnyzmLlKIjBWsBItCynTkLPktojQ_21(TList_1_tD50BD7AD81DCF1ECCC37C9E0E0484AEBA3C76EBA * value)
	{
		___qnyzmLlKIjBWsBItCynTkLPktojQ_21 = value;
		Il2CppCodeGenWriteBarrier((&___qnyzmLlKIjBWsBItCynTkLPktojQ_21), value);
	}

	inline static int32_t get_offset_of_PwphjbqhFYBgNlblbCunFlclFYG_22() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___PwphjbqhFYBgNlblbCunFlclFYG_22)); }
	inline List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * get_PwphjbqhFYBgNlblbCunFlclFYG_22() const { return ___PwphjbqhFYBgNlblbCunFlclFYG_22; }
	inline List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 ** get_address_of_PwphjbqhFYBgNlblbCunFlclFYG_22() { return &___PwphjbqhFYBgNlblbCunFlclFYG_22; }
	inline void set_PwphjbqhFYBgNlblbCunFlclFYG_22(List_1_tE69D3AB3E1E849B62D12B5D407555CCD60F18E94 * value)
	{
		___PwphjbqhFYBgNlblbCunFlclFYG_22 = value;
		Il2CppCodeGenWriteBarrier((&___PwphjbqhFYBgNlblbCunFlclFYG_22), value);
	}

	inline static int32_t get_offset_of_SsLgDdKsoZjfwHynFGZqtFpuMaQN_23() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___SsLgDdKsoZjfwHynFGZqtFpuMaQN_23)); }
	inline bool get_SsLgDdKsoZjfwHynFGZqtFpuMaQN_23() const { return ___SsLgDdKsoZjfwHynFGZqtFpuMaQN_23; }
	inline bool* get_address_of_SsLgDdKsoZjfwHynFGZqtFpuMaQN_23() { return &___SsLgDdKsoZjfwHynFGZqtFpuMaQN_23; }
	inline void set_SsLgDdKsoZjfwHynFGZqtFpuMaQN_23(bool value)
	{
		___SsLgDdKsoZjfwHynFGZqtFpuMaQN_23 = value;
	}

	inline static int32_t get_offset_of_nhzvhDxVQckJidNZgcMYPImmKAB_24() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___nhzvhDxVQckJidNZgcMYPImmKAB_24)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_nhzvhDxVQckJidNZgcMYPImmKAB_24() const { return ___nhzvhDxVQckJidNZgcMYPImmKAB_24; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_nhzvhDxVQckJidNZgcMYPImmKAB_24() { return &___nhzvhDxVQckJidNZgcMYPImmKAB_24; }
	inline void set_nhzvhDxVQckJidNZgcMYPImmKAB_24(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___nhzvhDxVQckJidNZgcMYPImmKAB_24 = value;
		Il2CppCodeGenWriteBarrier((&___nhzvhDxVQckJidNZgcMYPImmKAB_24), value);
	}

	inline static int32_t get_offset_of_FjaMvlDEramtbltDJipLxhnSGjk_25() { return static_cast<int32_t>(offsetof(ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5, ___FjaMvlDEramtbltDJipLxhnSGjk_25)); }
	inline Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC  get_FjaMvlDEramtbltDJipLxhnSGjk_25() const { return ___FjaMvlDEramtbltDJipLxhnSGjk_25; }
	inline Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC * get_address_of_FjaMvlDEramtbltDJipLxhnSGjk_25() { return &___FjaMvlDEramtbltDJipLxhnSGjk_25; }
	inline void set_FjaMvlDEramtbltDJipLxhnSGjk_25(Enumerator_t1A82EAB4C9F5457B9347C73984979BF8E5CFAAEC  value)
	{
		___FjaMvlDEramtbltDJipLxhnSGjk_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OZYWVRHQBYXUVMAXTYLPPDFTGTK_T435B6195479C21B59EA721DD1208873F0878FCE5_H
#ifndef POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#define POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Pole
struct  Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C 
{
public:
	// System.Int32 Rewired.Pole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pole_t67AC5251C8FF2201B5D8F674A185F8786845BE3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLE_T67AC5251C8FF2201B5D8F674A185F8786845BE3C_H
#ifndef UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#define UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.UpdateLoopType
struct  UpdateLoopType_t407C8154C4B63048CA3DC1A59E6458956BC31F0A 
{
public:
	// System.Int32 Rewired.UpdateLoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateLoopType_t407C8154C4B63048CA3DC1A59E6458956BC31F0A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATELOOPTYPE_T407C8154C4B63048CA3DC1A59E6458956BC31F0A_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#define CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ControllerPollingInfo
struct  ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 
{
public:
	// System.Boolean Rewired.ControllerPollingInfo::XxREeDCHebPEeYaAATiXyFbnbIU
	bool ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	// System.Int32 Rewired.ControllerPollingInfo::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	// System.Int32 Rewired.ControllerPollingInfo::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	// System.String Rewired.ControllerPollingInfo::tUeBKTuOCBEsklhkCXrBJHMCCni
	String_t* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	// Rewired.ControllerType Rewired.ControllerPollingInfo::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	// Rewired.ControllerElementType Rewired.ControllerPollingInfo::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	// System.Int32 Rewired.ControllerPollingInfo::JcIOHtlyyDcisHJclHvGjyIzQHK
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	// Rewired.Pole Rewired.ControllerPollingInfo::gOvKiYbtKZOeaPYXdsADyDYKhyR
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	// System.String Rewired.ControllerPollingInfo::BpnqUtABZEySqFzPQqIktIgZaUw
	String_t* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	// System.Int32 Rewired.ControllerPollingInfo::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	// UnityEngine.KeyCode Rewired.ControllerPollingInfo::UBcIxoTNwLQLduWHPwWDYgtuvif
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;

public:
	inline static int32_t get_offset_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___XxREeDCHebPEeYaAATiXyFbnbIU_0)); }
	inline bool get_XxREeDCHebPEeYaAATiXyFbnbIU_0() const { return ___XxREeDCHebPEeYaAATiXyFbnbIU_0; }
	inline bool* get_address_of_XxREeDCHebPEeYaAATiXyFbnbIU_0() { return &___XxREeDCHebPEeYaAATiXyFbnbIU_0; }
	inline void set_XxREeDCHebPEeYaAATiXyFbnbIU_0(bool value)
	{
		___XxREeDCHebPEeYaAATiXyFbnbIU_0 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___FCIziTxDWAnyHATWwZeMEmiXvdc_1)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_1() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_1(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_1 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___CdiTZueJOweHxLVLesdWcZkZxNV_2)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_2() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_2() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_2; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_2(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_2 = value;
	}

	inline static int32_t get_offset_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___tUeBKTuOCBEsklhkCXrBJHMCCni_3)); }
	inline String_t* get_tUeBKTuOCBEsklhkCXrBJHMCCni_3() const { return ___tUeBKTuOCBEsklhkCXrBJHMCCni_3; }
	inline String_t** get_address_of_tUeBKTuOCBEsklhkCXrBJHMCCni_3() { return &___tUeBKTuOCBEsklhkCXrBJHMCCni_3; }
	inline void set_tUeBKTuOCBEsklhkCXrBJHMCCni_3(String_t* value)
	{
		___tUeBKTuOCBEsklhkCXrBJHMCCni_3 = value;
		Il2CppCodeGenWriteBarrier((&___tUeBKTuOCBEsklhkCXrBJHMCCni_3), value);
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_4(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4 = value;
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___RmmmEkVblcqxoqGYhifgdSESkSn_5)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_5() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_5; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_5() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_5; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_5(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_5 = value;
	}

	inline static int32_t get_offset_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___JcIOHtlyyDcisHJclHvGjyIzQHK_6)); }
	inline int32_t get_JcIOHtlyyDcisHJclHvGjyIzQHK_6() const { return ___JcIOHtlyyDcisHJclHvGjyIzQHK_6; }
	inline int32_t* get_address_of_JcIOHtlyyDcisHJclHvGjyIzQHK_6() { return &___JcIOHtlyyDcisHJclHvGjyIzQHK_6; }
	inline void set_JcIOHtlyyDcisHJclHvGjyIzQHK_6(int32_t value)
	{
		___JcIOHtlyyDcisHJclHvGjyIzQHK_6 = value;
	}

	inline static int32_t get_offset_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7)); }
	inline int32_t get_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() const { return ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7; }
	inline int32_t* get_address_of_gOvKiYbtKZOeaPYXdsADyDYKhyR_7() { return &___gOvKiYbtKZOeaPYXdsADyDYKhyR_7; }
	inline void set_gOvKiYbtKZOeaPYXdsADyDYKhyR_7(int32_t value)
	{
		___gOvKiYbtKZOeaPYXdsADyDYKhyR_7 = value;
	}

	inline static int32_t get_offset_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___BpnqUtABZEySqFzPQqIktIgZaUw_8)); }
	inline String_t* get_BpnqUtABZEySqFzPQqIktIgZaUw_8() const { return ___BpnqUtABZEySqFzPQqIktIgZaUw_8; }
	inline String_t** get_address_of_BpnqUtABZEySqFzPQqIktIgZaUw_8() { return &___BpnqUtABZEySqFzPQqIktIgZaUw_8; }
	inline void set_BpnqUtABZEySqFzPQqIktIgZaUw_8(String_t* value)
	{
		___BpnqUtABZEySqFzPQqIktIgZaUw_8 = value;
		Il2CppCodeGenWriteBarrier((&___BpnqUtABZEySqFzPQqIktIgZaUw_8), value);
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___FrsWYgeIWMnjOcXDysagwZGcCMF_9)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_9() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_9; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_9() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_9; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_9(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_9 = value;
	}

	inline static int32_t get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() { return static_cast<int32_t>(offsetof(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03, ___UBcIxoTNwLQLduWHPwWDYgtuvif_10)); }
	inline int32_t get_UBcIxoTNwLQLduWHPwWDYgtuvif_10() const { return ___UBcIxoTNwLQLduWHPwWDYgtuvif_10; }
	inline int32_t* get_address_of_UBcIxoTNwLQLduWHPwWDYgtuvif_10() { return &___UBcIxoTNwLQLduWHPwWDYgtuvif_10; }
	inline void set_UBcIxoTNwLQLduWHPwWDYgtuvif_10(int32_t value)
	{
		___UBcIxoTNwLQLduWHPwWDYgtuvif_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ControllerPollingInfo
struct ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_pinvoke
{
	int32_t ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	char* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	char* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;
};
// Native definition for COM marshalling of Rewired.ControllerPollingInfo
struct ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03_marshaled_com
{
	int32_t ___XxREeDCHebPEeYaAATiXyFbnbIU_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_2;
	Il2CppChar* ___tUeBKTuOCBEsklhkCXrBJHMCCni_3;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_4;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_5;
	int32_t ___JcIOHtlyyDcisHJclHvGjyIzQHK_6;
	int32_t ___gOvKiYbtKZOeaPYXdsADyDYKhyR_7;
	Il2CppChar* ___BpnqUtABZEySqFzPQqIktIgZaUw_8;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_9;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_10;
};
#endif // CONTROLLERPOLLINGINFO_T7933CFBCF4884BC44CB6B5947D4058434D658B03_H
#ifndef ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#define ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentConflictCheck
struct  ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 
{
public:
	// Rewired.ElementAssignmentType Rewired.ElementAssignmentConflictCheck::PReGOawJrxHuYbkYlXhFoNBbppd
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	// Rewired.ControllerType Rewired.ElementAssignmentConflictCheck::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::kNupzNtNRIxDZONVUSwnnBWcpIT
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::fAMwdVQKMkdvbVWXbkkCQoSadHid
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::TXYPyQJDwYiLBxGVhCRAhruZxByr
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	// Rewired.AxisRange Rewired.ElementAssignmentConflictCheck::xTkhpClvSKvoiLfYNzcXIDURAPx
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	// UnityEngine.KeyCode Rewired.ElementAssignmentConflictCheck::UBcIxoTNwLQLduWHPwWDYgtuvif
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignmentConflictCheck::rUpHewPnhPrBuIDeIasWkyvRDkW
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	// System.Int32 Rewired.ElementAssignmentConflictCheck::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	// Rewired.Pole Rewired.ElementAssignmentConflictCheck::magSqBEVFaJkoQfcAvXLVDzmcnZB
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	// System.Boolean Rewired.ElementAssignmentConflictCheck::GfvPtFqTnCIcrWebHAQzqOoKceI
	bool ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;

public:
	inline static int32_t get_offset_of_PReGOawJrxHuYbkYlXhFoNBbppd_0() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___PReGOawJrxHuYbkYlXhFoNBbppd_0)); }
	inline int32_t get_PReGOawJrxHuYbkYlXhFoNBbppd_0() const { return ___PReGOawJrxHuYbkYlXhFoNBbppd_0; }
	inline int32_t* get_address_of_PReGOawJrxHuYbkYlXhFoNBbppd_0() { return &___PReGOawJrxHuYbkYlXhFoNBbppd_0; }
	inline void set_PReGOawJrxHuYbkYlXhFoNBbppd_0(int32_t value)
	{
		___PReGOawJrxHuYbkYlXhFoNBbppd_0 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___FCIziTxDWAnyHATWwZeMEmiXvdc_1)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_1() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_1() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_1; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_1(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_1 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_2(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___CdiTZueJOweHxLVLesdWcZkZxNV_3)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_3() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_3; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_3() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_3; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_3(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_3 = value;
	}

	inline static int32_t get_offset_of_kNupzNtNRIxDZONVUSwnnBWcpIT_4() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___kNupzNtNRIxDZONVUSwnnBWcpIT_4)); }
	inline int32_t get_kNupzNtNRIxDZONVUSwnnBWcpIT_4() const { return ___kNupzNtNRIxDZONVUSwnnBWcpIT_4; }
	inline int32_t* get_address_of_kNupzNtNRIxDZONVUSwnnBWcpIT_4() { return &___kNupzNtNRIxDZONVUSwnnBWcpIT_4; }
	inline void set_kNupzNtNRIxDZONVUSwnnBWcpIT_4(int32_t value)
	{
		___kNupzNtNRIxDZONVUSwnnBWcpIT_4 = value;
	}

	inline static int32_t get_offset_of_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5)); }
	inline int32_t get_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() const { return ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5; }
	inline int32_t* get_address_of_fAMwdVQKMkdvbVWXbkkCQoSadHid_5() { return &___fAMwdVQKMkdvbVWXbkkCQoSadHid_5; }
	inline void set_fAMwdVQKMkdvbVWXbkkCQoSadHid_5(int32_t value)
	{
		___fAMwdVQKMkdvbVWXbkkCQoSadHid_5 = value;
	}

	inline static int32_t get_offset_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6)); }
	inline int32_t get_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() const { return ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline int32_t* get_address_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return &___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline void set_TXYPyQJDwYiLBxGVhCRAhruZxByr_6(int32_t value)
	{
		___TXYPyQJDwYiLBxGVhCRAhruZxByr_6 = value;
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_7() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___FrsWYgeIWMnjOcXDysagwZGcCMF_7)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_7() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_7; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_7() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_7; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_7(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_7 = value;
	}

	inline static int32_t get_offset_of_xTkhpClvSKvoiLfYNzcXIDURAPx_8() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___xTkhpClvSKvoiLfYNzcXIDURAPx_8)); }
	inline int32_t get_xTkhpClvSKvoiLfYNzcXIDURAPx_8() const { return ___xTkhpClvSKvoiLfYNzcXIDURAPx_8; }
	inline int32_t* get_address_of_xTkhpClvSKvoiLfYNzcXIDURAPx_8() { return &___xTkhpClvSKvoiLfYNzcXIDURAPx_8; }
	inline void set_xTkhpClvSKvoiLfYNzcXIDURAPx_8(int32_t value)
	{
		___xTkhpClvSKvoiLfYNzcXIDURAPx_8 = value;
	}

	inline static int32_t get_offset_of_UBcIxoTNwLQLduWHPwWDYgtuvif_9() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___UBcIxoTNwLQLduWHPwWDYgtuvif_9)); }
	inline int32_t get_UBcIxoTNwLQLduWHPwWDYgtuvif_9() const { return ___UBcIxoTNwLQLduWHPwWDYgtuvif_9; }
	inline int32_t* get_address_of_UBcIxoTNwLQLduWHPwWDYgtuvif_9() { return &___UBcIxoTNwLQLduWHPwWDYgtuvif_9; }
	inline void set_UBcIxoTNwLQLduWHPwWDYgtuvif_9(int32_t value)
	{
		___UBcIxoTNwLQLduWHPwWDYgtuvif_9 = value;
	}

	inline static int32_t get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___rUpHewPnhPrBuIDeIasWkyvRDkW_10)); }
	inline int32_t get_rUpHewPnhPrBuIDeIasWkyvRDkW_10() const { return ___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline int32_t* get_address_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return &___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline void set_rUpHewPnhPrBuIDeIasWkyvRDkW_10(int32_t value)
	{
		___rUpHewPnhPrBuIDeIasWkyvRDkW_10 = value;
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___LmADKPFcmVAiabETiHqQTGSgvmcg_11)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_11() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_11(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_11 = value;
	}

	inline static int32_t get_offset_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12)); }
	inline int32_t get_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() const { return ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12; }
	inline int32_t* get_address_of_magSqBEVFaJkoQfcAvXLVDzmcnZB_12() { return &___magSqBEVFaJkoQfcAvXLVDzmcnZB_12; }
	inline void set_magSqBEVFaJkoQfcAvXLVDzmcnZB_12(int32_t value)
	{
		___magSqBEVFaJkoQfcAvXLVDzmcnZB_12 = value;
	}

	inline static int32_t get_offset_of_GfvPtFqTnCIcrWebHAQzqOoKceI_13() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4, ___GfvPtFqTnCIcrWebHAQzqOoKceI_13)); }
	inline bool get_GfvPtFqTnCIcrWebHAQzqOoKceI_13() const { return ___GfvPtFqTnCIcrWebHAQzqOoKceI_13; }
	inline bool* get_address_of_GfvPtFqTnCIcrWebHAQzqOoKceI_13() { return &___GfvPtFqTnCIcrWebHAQzqOoKceI_13; }
	inline void set_GfvPtFqTnCIcrWebHAQzqOoKceI_13(bool value)
	{
		___GfvPtFqTnCIcrWebHAQzqOoKceI_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignmentConflictCheck
struct ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_marshaled_pinvoke
{
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	int32_t ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;
};
// Native definition for COM marshalling of Rewired.ElementAssignmentConflictCheck
struct ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_marshaled_com
{
	int32_t ___PReGOawJrxHuYbkYlXhFoNBbppd_0;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_1;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_2;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_3;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_4;
	int32_t ___fAMwdVQKMkdvbVWXbkkCQoSadHid_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_7;
	int32_t ___xTkhpClvSKvoiLfYNzcXIDURAPx_8;
	int32_t ___UBcIxoTNwLQLduWHPwWDYgtuvif_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
	int32_t ___magSqBEVFaJkoQfcAvXLVDzmcnZB_12;
	int32_t ___GfvPtFqTnCIcrWebHAQzqOoKceI_13;
};
#endif // ELEMENTASSIGNMENTCONFLICTCHECK_TA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4_H
#ifndef ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#define ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ElementAssignmentConflictInfo
struct  ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D 
{
public:
	// System.Boolean Rewired.ElementAssignmentConflictInfo::rLCeavZDeONdAmfQvjLvpIKqsIQ
	bool ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	// System.Boolean Rewired.ElementAssignmentConflictInfo::uUoPCsKlKtgHCnSDBCrTHvhFbFXy
	bool ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	// Rewired.ControllerType Rewired.ElementAssignmentConflictInfo::GzkgEnUrvFggqkkmzcVFZEsKFWEI
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::CdiTZueJOweHxLVLesdWcZkZxNV
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::kNupzNtNRIxDZONVUSwnnBWcpIT
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::TXYPyQJDwYiLBxGVhCRAhruZxByr
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	// Rewired.ControllerElementType Rewired.ElementAssignmentConflictInfo::RmmmEkVblcqxoqGYhifgdSESkSn
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::FrsWYgeIWMnjOcXDysagwZGcCMF
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	// UnityEngine.KeyCode Rewired.ElementAssignmentConflictInfo::SkzHHHthNTcRwnaarBvUnkLqLeI
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	// Rewired.ModifierKeyFlags Rewired.ElementAssignmentConflictInfo::rUpHewPnhPrBuIDeIasWkyvRDkW
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	// System.Int32 Rewired.ElementAssignmentConflictInfo::LmADKPFcmVAiabETiHqQTGSgvmcg
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;

public:
	inline static int32_t get_offset_of_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0)); }
	inline bool get_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() const { return ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0; }
	inline bool* get_address_of_rLCeavZDeONdAmfQvjLvpIKqsIQ_0() { return &___rLCeavZDeONdAmfQvjLvpIKqsIQ_0; }
	inline void set_rLCeavZDeONdAmfQvjLvpIKqsIQ_0(bool value)
	{
		___rLCeavZDeONdAmfQvjLvpIKqsIQ_0 = value;
	}

	inline static int32_t get_offset_of_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1)); }
	inline bool get_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() const { return ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1; }
	inline bool* get_address_of_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1() { return &___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1; }
	inline void set_uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1(bool value)
	{
		___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1 = value;
	}

	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___FCIziTxDWAnyHATWwZeMEmiXvdc_2)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_2() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_2() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_2; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_2(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_2 = value;
	}

	inline static int32_t get_offset_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3)); }
	inline int32_t get_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() const { return ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline int32_t* get_address_of_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3() { return &___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3; }
	inline void set_GzkgEnUrvFggqkkmzcVFZEsKFWEI_3(int32_t value)
	{
		___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3 = value;
	}

	inline static int32_t get_offset_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___CdiTZueJOweHxLVLesdWcZkZxNV_4)); }
	inline int32_t get_CdiTZueJOweHxLVLesdWcZkZxNV_4() const { return ___CdiTZueJOweHxLVLesdWcZkZxNV_4; }
	inline int32_t* get_address_of_CdiTZueJOweHxLVLesdWcZkZxNV_4() { return &___CdiTZueJOweHxLVLesdWcZkZxNV_4; }
	inline void set_CdiTZueJOweHxLVLesdWcZkZxNV_4(int32_t value)
	{
		___CdiTZueJOweHxLVLesdWcZkZxNV_4 = value;
	}

	inline static int32_t get_offset_of_kNupzNtNRIxDZONVUSwnnBWcpIT_5() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___kNupzNtNRIxDZONVUSwnnBWcpIT_5)); }
	inline int32_t get_kNupzNtNRIxDZONVUSwnnBWcpIT_5() const { return ___kNupzNtNRIxDZONVUSwnnBWcpIT_5; }
	inline int32_t* get_address_of_kNupzNtNRIxDZONVUSwnnBWcpIT_5() { return &___kNupzNtNRIxDZONVUSwnnBWcpIT_5; }
	inline void set_kNupzNtNRIxDZONVUSwnnBWcpIT_5(int32_t value)
	{
		___kNupzNtNRIxDZONVUSwnnBWcpIT_5 = value;
	}

	inline static int32_t get_offset_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6)); }
	inline int32_t get_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() const { return ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline int32_t* get_address_of_TXYPyQJDwYiLBxGVhCRAhruZxByr_6() { return &___TXYPyQJDwYiLBxGVhCRAhruZxByr_6; }
	inline void set_TXYPyQJDwYiLBxGVhCRAhruZxByr_6(int32_t value)
	{
		___TXYPyQJDwYiLBxGVhCRAhruZxByr_6 = value;
	}

	inline static int32_t get_offset_of_RmmmEkVblcqxoqGYhifgdSESkSn_7() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___RmmmEkVblcqxoqGYhifgdSESkSn_7)); }
	inline int32_t get_RmmmEkVblcqxoqGYhifgdSESkSn_7() const { return ___RmmmEkVblcqxoqGYhifgdSESkSn_7; }
	inline int32_t* get_address_of_RmmmEkVblcqxoqGYhifgdSESkSn_7() { return &___RmmmEkVblcqxoqGYhifgdSESkSn_7; }
	inline void set_RmmmEkVblcqxoqGYhifgdSESkSn_7(int32_t value)
	{
		___RmmmEkVblcqxoqGYhifgdSESkSn_7 = value;
	}

	inline static int32_t get_offset_of_FrsWYgeIWMnjOcXDysagwZGcCMF_8() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___FrsWYgeIWMnjOcXDysagwZGcCMF_8)); }
	inline int32_t get_FrsWYgeIWMnjOcXDysagwZGcCMF_8() const { return ___FrsWYgeIWMnjOcXDysagwZGcCMF_8; }
	inline int32_t* get_address_of_FrsWYgeIWMnjOcXDysagwZGcCMF_8() { return &___FrsWYgeIWMnjOcXDysagwZGcCMF_8; }
	inline void set_FrsWYgeIWMnjOcXDysagwZGcCMF_8(int32_t value)
	{
		___FrsWYgeIWMnjOcXDysagwZGcCMF_8 = value;
	}

	inline static int32_t get_offset_of_SkzHHHthNTcRwnaarBvUnkLqLeI_9() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___SkzHHHthNTcRwnaarBvUnkLqLeI_9)); }
	inline int32_t get_SkzHHHthNTcRwnaarBvUnkLqLeI_9() const { return ___SkzHHHthNTcRwnaarBvUnkLqLeI_9; }
	inline int32_t* get_address_of_SkzHHHthNTcRwnaarBvUnkLqLeI_9() { return &___SkzHHHthNTcRwnaarBvUnkLqLeI_9; }
	inline void set_SkzHHHthNTcRwnaarBvUnkLqLeI_9(int32_t value)
	{
		___SkzHHHthNTcRwnaarBvUnkLqLeI_9 = value;
	}

	inline static int32_t get_offset_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___rUpHewPnhPrBuIDeIasWkyvRDkW_10)); }
	inline int32_t get_rUpHewPnhPrBuIDeIasWkyvRDkW_10() const { return ___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline int32_t* get_address_of_rUpHewPnhPrBuIDeIasWkyvRDkW_10() { return &___rUpHewPnhPrBuIDeIasWkyvRDkW_10; }
	inline void set_rUpHewPnhPrBuIDeIasWkyvRDkW_10(int32_t value)
	{
		___rUpHewPnhPrBuIDeIasWkyvRDkW_10 = value;
	}

	inline static int32_t get_offset_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return static_cast<int32_t>(offsetof(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D, ___LmADKPFcmVAiabETiHqQTGSgvmcg_11)); }
	inline int32_t get_LmADKPFcmVAiabETiHqQTGSgvmcg_11() const { return ___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline int32_t* get_address_of_LmADKPFcmVAiabETiHqQTGSgvmcg_11() { return &___LmADKPFcmVAiabETiHqQTGSgvmcg_11; }
	inline void set_LmADKPFcmVAiabETiHqQTGSgvmcg_11(int32_t value)
	{
		___LmADKPFcmVAiabETiHqQTGSgvmcg_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Rewired.ElementAssignmentConflictInfo
struct ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_marshaled_pinvoke
{
	int32_t ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	int32_t ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
};
// Native definition for COM marshalling of Rewired.ElementAssignmentConflictInfo
struct ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_marshaled_com
{
	int32_t ___rLCeavZDeONdAmfQvjLvpIKqsIQ_0;
	int32_t ___uUoPCsKlKtgHCnSDBCrTHvhFbFXy_1;
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_2;
	int32_t ___GzkgEnUrvFggqkkmzcVFZEsKFWEI_3;
	int32_t ___CdiTZueJOweHxLVLesdWcZkZxNV_4;
	int32_t ___kNupzNtNRIxDZONVUSwnnBWcpIT_5;
	int32_t ___TXYPyQJDwYiLBxGVhCRAhruZxByr_6;
	int32_t ___RmmmEkVblcqxoqGYhifgdSESkSn_7;
	int32_t ___FrsWYgeIWMnjOcXDysagwZGcCMF_8;
	int32_t ___SkzHHHthNTcRwnaarBvUnkLqLeI_9;
	int32_t ___rUpHewPnhPrBuIDeIasWkyvRDkW_10;
	int32_t ___LmADKPFcmVAiabETiHqQTGSgvmcg_11;
};
#endif // ELEMENTASSIGNMENTCONFLICTINFO_TF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D_H
#ifndef CUSTOMINPUTSOURCE_T845A0FA665A9B8FFE97AD4828D69AECDF618DE24_H
#define CUSTOMINPUTSOURCE_T845A0FA665A9B8FFE97AD4828D69AECDF618DE24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.Custom.CustomInputSource
struct  CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24  : public RuntimeObject
{
public:
	// Rewired.InputSource Rewired.Platforms.Custom.CustomInputSource::ytEpmEgckhdtBXUMvGmTeXChJTg
	int32_t ___ytEpmEgckhdtBXUMvGmTeXChJTg_0;
	// System.Collections.Generic.List`1<Rewired.Platforms.Custom.CustomInputSource_Joystick> Rewired.Platforms.Custom.CustomInputSource::jiyuiOHkrKbDzzGVtQSJtFPdiuXH
	List_1_tE74CF06E458D02B7DFF8A66064E48B59E372F1F2 * ___jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Rewired.Platforms.Custom.CustomInputSource_Joystick> Rewired.Platforms.Custom.CustomInputSource::GlRIkUvAeSsFrGZYYfdWdAiahHaH
	ReadOnlyCollection_1_t3725131578433E1E4A019736E9F58AB979C5A48D * ___GlRIkUvAeSsFrGZYYfdWdAiahHaH_2;
	// System.Boolean Rewired.Platforms.Custom.CustomInputSource::OxqcyLtiNnnQZjHLJHrubHVLORyY
	bool ___OxqcyLtiNnnQZjHLJHrubHVLORyY_3;
	// System.Action Rewired.Platforms.Custom.CustomInputSource::dSCdvTfRJafJNqFqCvsZWnmZGkuH
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___dSCdvTfRJafJNqFqCvsZWnmZGkuH_4;
	// System.Action Rewired.Platforms.Custom.CustomInputSource::WtXxfgwPkvBzrhvGFrBKnZbGqaC
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___WtXxfgwPkvBzrhvGFrBKnZbGqaC_5;
	// System.Boolean Rewired.Platforms.Custom.CustomInputSource::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_6;

public:
	inline static int32_t get_offset_of_ytEpmEgckhdtBXUMvGmTeXChJTg_0() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___ytEpmEgckhdtBXUMvGmTeXChJTg_0)); }
	inline int32_t get_ytEpmEgckhdtBXUMvGmTeXChJTg_0() const { return ___ytEpmEgckhdtBXUMvGmTeXChJTg_0; }
	inline int32_t* get_address_of_ytEpmEgckhdtBXUMvGmTeXChJTg_0() { return &___ytEpmEgckhdtBXUMvGmTeXChJTg_0; }
	inline void set_ytEpmEgckhdtBXUMvGmTeXChJTg_0(int32_t value)
	{
		___ytEpmEgckhdtBXUMvGmTeXChJTg_0 = value;
	}

	inline static int32_t get_offset_of_jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1)); }
	inline List_1_tE74CF06E458D02B7DFF8A66064E48B59E372F1F2 * get_jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1() const { return ___jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1; }
	inline List_1_tE74CF06E458D02B7DFF8A66064E48B59E372F1F2 ** get_address_of_jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1() { return &___jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1; }
	inline void set_jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1(List_1_tE74CF06E458D02B7DFF8A66064E48B59E372F1F2 * value)
	{
		___jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1 = value;
		Il2CppCodeGenWriteBarrier((&___jiyuiOHkrKbDzzGVtQSJtFPdiuXH_1), value);
	}

	inline static int32_t get_offset_of_GlRIkUvAeSsFrGZYYfdWdAiahHaH_2() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___GlRIkUvAeSsFrGZYYfdWdAiahHaH_2)); }
	inline ReadOnlyCollection_1_t3725131578433E1E4A019736E9F58AB979C5A48D * get_GlRIkUvAeSsFrGZYYfdWdAiahHaH_2() const { return ___GlRIkUvAeSsFrGZYYfdWdAiahHaH_2; }
	inline ReadOnlyCollection_1_t3725131578433E1E4A019736E9F58AB979C5A48D ** get_address_of_GlRIkUvAeSsFrGZYYfdWdAiahHaH_2() { return &___GlRIkUvAeSsFrGZYYfdWdAiahHaH_2; }
	inline void set_GlRIkUvAeSsFrGZYYfdWdAiahHaH_2(ReadOnlyCollection_1_t3725131578433E1E4A019736E9F58AB979C5A48D * value)
	{
		___GlRIkUvAeSsFrGZYYfdWdAiahHaH_2 = value;
		Il2CppCodeGenWriteBarrier((&___GlRIkUvAeSsFrGZYYfdWdAiahHaH_2), value);
	}

	inline static int32_t get_offset_of_OxqcyLtiNnnQZjHLJHrubHVLORyY_3() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___OxqcyLtiNnnQZjHLJHrubHVLORyY_3)); }
	inline bool get_OxqcyLtiNnnQZjHLJHrubHVLORyY_3() const { return ___OxqcyLtiNnnQZjHLJHrubHVLORyY_3; }
	inline bool* get_address_of_OxqcyLtiNnnQZjHLJHrubHVLORyY_3() { return &___OxqcyLtiNnnQZjHLJHrubHVLORyY_3; }
	inline void set_OxqcyLtiNnnQZjHLJHrubHVLORyY_3(bool value)
	{
		___OxqcyLtiNnnQZjHLJHrubHVLORyY_3 = value;
	}

	inline static int32_t get_offset_of_dSCdvTfRJafJNqFqCvsZWnmZGkuH_4() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___dSCdvTfRJafJNqFqCvsZWnmZGkuH_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_dSCdvTfRJafJNqFqCvsZWnmZGkuH_4() const { return ___dSCdvTfRJafJNqFqCvsZWnmZGkuH_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_dSCdvTfRJafJNqFqCvsZWnmZGkuH_4() { return &___dSCdvTfRJafJNqFqCvsZWnmZGkuH_4; }
	inline void set_dSCdvTfRJafJNqFqCvsZWnmZGkuH_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___dSCdvTfRJafJNqFqCvsZWnmZGkuH_4 = value;
		Il2CppCodeGenWriteBarrier((&___dSCdvTfRJafJNqFqCvsZWnmZGkuH_4), value);
	}

	inline static int32_t get_offset_of_WtXxfgwPkvBzrhvGFrBKnZbGqaC_5() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___WtXxfgwPkvBzrhvGFrBKnZbGqaC_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_WtXxfgwPkvBzrhvGFrBKnZbGqaC_5() const { return ___WtXxfgwPkvBzrhvGFrBKnZbGqaC_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_WtXxfgwPkvBzrhvGFrBKnZbGqaC_5() { return &___WtXxfgwPkvBzrhvGFrBKnZbGqaC_5; }
	inline void set_WtXxfgwPkvBzrhvGFrBKnZbGqaC_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___WtXxfgwPkvBzrhvGFrBKnZbGqaC_5 = value;
		Il2CppCodeGenWriteBarrier((&___WtXxfgwPkvBzrhvGFrBKnZbGqaC_5), value);
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_6() { return static_cast<int32_t>(offsetof(CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24, ___SeCUoinDywZmqZDHRKupOdOaTke_6)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_6() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_6; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_6() { return &___SeCUoinDywZmqZDHRKupOdOaTke_6; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_6(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMINPUTSOURCE_T845A0FA665A9B8FFE97AD4828D69AECDF618DE24_H
#ifndef CONTROLLERINFORMATION_T44F7C14FE4C1B05D1A1AF215FD6FA245CDA36D2C_H
#define CONTROLLERINFORMATION_T44F7C14FE4C1B05D1A1AF215FD6FA245CDA36D2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.Internal.ControllerInformation
struct  ControllerInformation_t44F7C14FE4C1B05D1A1AF215FD6FA245CDA36D2C  : public RuntimeObject
{
public:
	// Rewired.Platforms.PS4.Internal.PadControllerInformation Rewired.Platforms.PS4.Internal.ControllerInformation::padControllerInformation
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC  ___padControllerInformation_0;
	// Rewired.Platforms.PS4.Internal.PadDeviceClassExtendedInformation Rewired.Platforms.PS4.Internal.ControllerInformation::padDeviceClassExtendedInformation
	PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253  ___padDeviceClassExtendedInformation_1;

public:
	inline static int32_t get_offset_of_padControllerInformation_0() { return static_cast<int32_t>(offsetof(ControllerInformation_t44F7C14FE4C1B05D1A1AF215FD6FA245CDA36D2C, ___padControllerInformation_0)); }
	inline PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC  get_padControllerInformation_0() const { return ___padControllerInformation_0; }
	inline PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC * get_address_of_padControllerInformation_0() { return &___padControllerInformation_0; }
	inline void set_padControllerInformation_0(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC  value)
	{
		___padControllerInformation_0 = value;
	}

	inline static int32_t get_offset_of_padDeviceClassExtendedInformation_1() { return static_cast<int32_t>(offsetof(ControllerInformation_t44F7C14FE4C1B05D1A1AF215FD6FA245CDA36D2C, ___padDeviceClassExtendedInformation_1)); }
	inline PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253  get_padDeviceClassExtendedInformation_1() const { return ___padDeviceClassExtendedInformation_1; }
	inline PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253 * get_address_of_padDeviceClassExtendedInformation_1() { return &___padDeviceClassExtendedInformation_1; }
	inline void set_padDeviceClassExtendedInformation_1(PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253  value)
	{
		___padDeviceClassExtendedInformation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERINFORMATION_T44F7C14FE4C1B05D1A1AF215FD6FA245CDA36D2C_H
#ifndef RERQQZNVNMKJXMRMWWZIRXFUKZM_TB8342C505CC902205234CB4CDFE12756108EB1F9_H
#define RERQQZNVNMKJXMRMWWZIRXFUKZM_TB8342C505CC902205234CB4CDFE12756108EB1F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM
struct  ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9  : public Joystick_t6CBF6DCF7C20EF9EDCCC216C9510DBF316FFBE09
{
public:
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::FCIziTxDWAnyHATWwZeMEmiXvdc
	int32_t ___FCIziTxDWAnyHATWwZeMEmiXvdc_13;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::bPrhJXsCXGJcROQSTqUrDFGQpAV
	int32_t ___bPrhJXsCXGJcROQSTqUrDFGQpAV_14;
	// System.Boolean Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::jbyLFeFlKavAMzMlQdBZSCTfOUY
	bool ___jbyLFeFlKavAMzMlQdBZSCTfOUY_15;
	// Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_uHmqJGqYoMpKFcMgEPjnasPBJId Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::lmBvgcupLOPeeGLFzFjqdjHceqJi
	uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D * ___lmBvgcupLOPeeGLFzFjqdjHceqJi_16;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::iEOKtQxQPtNlJgohLlwhCaQebAA
	int32_t ___iEOKtQxQPtNlJgohLlwhCaQebAA_17;
	// System.Single[] Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::DzGpnIrIiBgaZZutTXMWrCjIHVAh
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___DzGpnIrIiBgaZZutTXMWrCjIHVAh_18;
	// Rewired.Platforms.PS4.Internal.LoggedInUser Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::eqqhvTfqBBgUYHrZujhYBdEEMEV
	LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3 * ___eqqhvTfqBBgUYHrZujhYBdEEMEV_19;
	// Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_ControllerType Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::DzAgXbAsFcjwinjPLOBYLcFrhIr
	int32_t ___DzAgXbAsFcjwinjPLOBYLcFrhIr_20;
	// System.Func`2<System.Int32,System.Boolean> Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::jJcGAOgHrntzwBnkDtkgtVxXoTE
	Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * ___jJcGAOgHrntzwBnkDtkgtVxXoTE_21;
	// System.Action`3<System.Int32,System.Int32,System.Int32> Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::KuOBoKEXaXnsUmUWfalVpEQPBLC
	Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * ___KuOBoKEXaXnsUmUWfalVpEQPBLC_22;
	// System.Action`4<System.Int32,System.Int32,System.Int32,System.Int32> Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::NTsMKIHbzzFkBBFPaQqJQeytbvD
	Action_4_t0513103ECC58D876DBBF435498FA75D2EAA7130D * ___NTsMKIHbzzFkBBFPaQqJQeytbvD_23;
	// System.Action`1<System.Int32> Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::LFifznDPvhfNRBpdXfgbwaFSgGs
	Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * ___LFifznDPvhfNRBpdXfgbwaFSgGs_24;
	// System.Action`2<System.Int32,System.Boolean> Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::yiXKAHllyDmQlfZQccEGegWUimJ
	Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E * ___yiXKAHllyDmQlfZQccEGegWUimJ_25;
	// System.Action`2<System.Int32,System.Boolean> Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::egAXiAtTGeshJIBQsJWWhqgtlWu
	Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E * ___egAXiAtTGeshJIBQsJWWhqgtlWu_26;
	// System.Action`2<System.Int32,System.Boolean> Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::EKZEThysBIbRLLOmbBKIxvTvPIe
	Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E * ___EKZEThysBIbRLLOmbBKIxvTvPIe_27;
	// System.Action`1<System.Int32> Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::LvLzjztTIRIUWhsEUhlEmObmghR
	Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * ___LvLzjztTIRIUWhsEUhlEmObmghR_28;
	// System.Func`2<System.Int32,UnityEngine.Vector3> Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::vPmUStfFLBEocaEdvYkuqnSyeuL
	Func_2_tD6D032CBFCBE5DEFC45886FF3D22684D36923253 * ___vPmUStfFLBEocaEdvYkuqnSyeuL_29;
	// System.Func`2<System.Int32,UnityEngine.Vector3> Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::bjfPXCqUhdrdhnQIIWAFhpWImVr
	Func_2_tD6D032CBFCBE5DEFC45886FF3D22684D36923253 * ___bjfPXCqUhdrdhnQIIWAFhpWImVr_30;
	// System.Func`2<System.Int32,UnityEngine.Vector4> Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::XjwgwCgXRWvXlTjIVBujMcjgpcZI
	Func_2_tEE835526B7A0EE187C9950EDA1D2C97725D65C09 * ___XjwgwCgXRWvXlTjIVBujMcjgpcZI_31;

public:
	inline static int32_t get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_13() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___FCIziTxDWAnyHATWwZeMEmiXvdc_13)); }
	inline int32_t get_FCIziTxDWAnyHATWwZeMEmiXvdc_13() const { return ___FCIziTxDWAnyHATWwZeMEmiXvdc_13; }
	inline int32_t* get_address_of_FCIziTxDWAnyHATWwZeMEmiXvdc_13() { return &___FCIziTxDWAnyHATWwZeMEmiXvdc_13; }
	inline void set_FCIziTxDWAnyHATWwZeMEmiXvdc_13(int32_t value)
	{
		___FCIziTxDWAnyHATWwZeMEmiXvdc_13 = value;
	}

	inline static int32_t get_offset_of_bPrhJXsCXGJcROQSTqUrDFGQpAV_14() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___bPrhJXsCXGJcROQSTqUrDFGQpAV_14)); }
	inline int32_t get_bPrhJXsCXGJcROQSTqUrDFGQpAV_14() const { return ___bPrhJXsCXGJcROQSTqUrDFGQpAV_14; }
	inline int32_t* get_address_of_bPrhJXsCXGJcROQSTqUrDFGQpAV_14() { return &___bPrhJXsCXGJcROQSTqUrDFGQpAV_14; }
	inline void set_bPrhJXsCXGJcROQSTqUrDFGQpAV_14(int32_t value)
	{
		___bPrhJXsCXGJcROQSTqUrDFGQpAV_14 = value;
	}

	inline static int32_t get_offset_of_jbyLFeFlKavAMzMlQdBZSCTfOUY_15() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___jbyLFeFlKavAMzMlQdBZSCTfOUY_15)); }
	inline bool get_jbyLFeFlKavAMzMlQdBZSCTfOUY_15() const { return ___jbyLFeFlKavAMzMlQdBZSCTfOUY_15; }
	inline bool* get_address_of_jbyLFeFlKavAMzMlQdBZSCTfOUY_15() { return &___jbyLFeFlKavAMzMlQdBZSCTfOUY_15; }
	inline void set_jbyLFeFlKavAMzMlQdBZSCTfOUY_15(bool value)
	{
		___jbyLFeFlKavAMzMlQdBZSCTfOUY_15 = value;
	}

	inline static int32_t get_offset_of_lmBvgcupLOPeeGLFzFjqdjHceqJi_16() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___lmBvgcupLOPeeGLFzFjqdjHceqJi_16)); }
	inline uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D * get_lmBvgcupLOPeeGLFzFjqdjHceqJi_16() const { return ___lmBvgcupLOPeeGLFzFjqdjHceqJi_16; }
	inline uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D ** get_address_of_lmBvgcupLOPeeGLFzFjqdjHceqJi_16() { return &___lmBvgcupLOPeeGLFzFjqdjHceqJi_16; }
	inline void set_lmBvgcupLOPeeGLFzFjqdjHceqJi_16(uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D * value)
	{
		___lmBvgcupLOPeeGLFzFjqdjHceqJi_16 = value;
		Il2CppCodeGenWriteBarrier((&___lmBvgcupLOPeeGLFzFjqdjHceqJi_16), value);
	}

	inline static int32_t get_offset_of_iEOKtQxQPtNlJgohLlwhCaQebAA_17() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___iEOKtQxQPtNlJgohLlwhCaQebAA_17)); }
	inline int32_t get_iEOKtQxQPtNlJgohLlwhCaQebAA_17() const { return ___iEOKtQxQPtNlJgohLlwhCaQebAA_17; }
	inline int32_t* get_address_of_iEOKtQxQPtNlJgohLlwhCaQebAA_17() { return &___iEOKtQxQPtNlJgohLlwhCaQebAA_17; }
	inline void set_iEOKtQxQPtNlJgohLlwhCaQebAA_17(int32_t value)
	{
		___iEOKtQxQPtNlJgohLlwhCaQebAA_17 = value;
	}

	inline static int32_t get_offset_of_DzGpnIrIiBgaZZutTXMWrCjIHVAh_18() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___DzGpnIrIiBgaZZutTXMWrCjIHVAh_18)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_DzGpnIrIiBgaZZutTXMWrCjIHVAh_18() const { return ___DzGpnIrIiBgaZZutTXMWrCjIHVAh_18; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_DzGpnIrIiBgaZZutTXMWrCjIHVAh_18() { return &___DzGpnIrIiBgaZZutTXMWrCjIHVAh_18; }
	inline void set_DzGpnIrIiBgaZZutTXMWrCjIHVAh_18(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___DzGpnIrIiBgaZZutTXMWrCjIHVAh_18 = value;
		Il2CppCodeGenWriteBarrier((&___DzGpnIrIiBgaZZutTXMWrCjIHVAh_18), value);
	}

	inline static int32_t get_offset_of_eqqhvTfqBBgUYHrZujhYBdEEMEV_19() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___eqqhvTfqBBgUYHrZujhYBdEEMEV_19)); }
	inline LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3 * get_eqqhvTfqBBgUYHrZujhYBdEEMEV_19() const { return ___eqqhvTfqBBgUYHrZujhYBdEEMEV_19; }
	inline LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3 ** get_address_of_eqqhvTfqBBgUYHrZujhYBdEEMEV_19() { return &___eqqhvTfqBBgUYHrZujhYBdEEMEV_19; }
	inline void set_eqqhvTfqBBgUYHrZujhYBdEEMEV_19(LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3 * value)
	{
		___eqqhvTfqBBgUYHrZujhYBdEEMEV_19 = value;
		Il2CppCodeGenWriteBarrier((&___eqqhvTfqBBgUYHrZujhYBdEEMEV_19), value);
	}

	inline static int32_t get_offset_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_20() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___DzAgXbAsFcjwinjPLOBYLcFrhIr_20)); }
	inline int32_t get_DzAgXbAsFcjwinjPLOBYLcFrhIr_20() const { return ___DzAgXbAsFcjwinjPLOBYLcFrhIr_20; }
	inline int32_t* get_address_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_20() { return &___DzAgXbAsFcjwinjPLOBYLcFrhIr_20; }
	inline void set_DzAgXbAsFcjwinjPLOBYLcFrhIr_20(int32_t value)
	{
		___DzAgXbAsFcjwinjPLOBYLcFrhIr_20 = value;
	}

	inline static int32_t get_offset_of_jJcGAOgHrntzwBnkDtkgtVxXoTE_21() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___jJcGAOgHrntzwBnkDtkgtVxXoTE_21)); }
	inline Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * get_jJcGAOgHrntzwBnkDtkgtVxXoTE_21() const { return ___jJcGAOgHrntzwBnkDtkgtVxXoTE_21; }
	inline Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 ** get_address_of_jJcGAOgHrntzwBnkDtkgtVxXoTE_21() { return &___jJcGAOgHrntzwBnkDtkgtVxXoTE_21; }
	inline void set_jJcGAOgHrntzwBnkDtkgtVxXoTE_21(Func_2_tCFDBA11D752C6255254F6FE7B5D68152CA5D2618 * value)
	{
		___jJcGAOgHrntzwBnkDtkgtVxXoTE_21 = value;
		Il2CppCodeGenWriteBarrier((&___jJcGAOgHrntzwBnkDtkgtVxXoTE_21), value);
	}

	inline static int32_t get_offset_of_KuOBoKEXaXnsUmUWfalVpEQPBLC_22() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___KuOBoKEXaXnsUmUWfalVpEQPBLC_22)); }
	inline Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * get_KuOBoKEXaXnsUmUWfalVpEQPBLC_22() const { return ___KuOBoKEXaXnsUmUWfalVpEQPBLC_22; }
	inline Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB ** get_address_of_KuOBoKEXaXnsUmUWfalVpEQPBLC_22() { return &___KuOBoKEXaXnsUmUWfalVpEQPBLC_22; }
	inline void set_KuOBoKEXaXnsUmUWfalVpEQPBLC_22(Action_3_tD5DCE3F2E92C609438E48C7DA5B16BD6F78624DB * value)
	{
		___KuOBoKEXaXnsUmUWfalVpEQPBLC_22 = value;
		Il2CppCodeGenWriteBarrier((&___KuOBoKEXaXnsUmUWfalVpEQPBLC_22), value);
	}

	inline static int32_t get_offset_of_NTsMKIHbzzFkBBFPaQqJQeytbvD_23() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___NTsMKIHbzzFkBBFPaQqJQeytbvD_23)); }
	inline Action_4_t0513103ECC58D876DBBF435498FA75D2EAA7130D * get_NTsMKIHbzzFkBBFPaQqJQeytbvD_23() const { return ___NTsMKIHbzzFkBBFPaQqJQeytbvD_23; }
	inline Action_4_t0513103ECC58D876DBBF435498FA75D2EAA7130D ** get_address_of_NTsMKIHbzzFkBBFPaQqJQeytbvD_23() { return &___NTsMKIHbzzFkBBFPaQqJQeytbvD_23; }
	inline void set_NTsMKIHbzzFkBBFPaQqJQeytbvD_23(Action_4_t0513103ECC58D876DBBF435498FA75D2EAA7130D * value)
	{
		___NTsMKIHbzzFkBBFPaQqJQeytbvD_23 = value;
		Il2CppCodeGenWriteBarrier((&___NTsMKIHbzzFkBBFPaQqJQeytbvD_23), value);
	}

	inline static int32_t get_offset_of_LFifznDPvhfNRBpdXfgbwaFSgGs_24() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___LFifznDPvhfNRBpdXfgbwaFSgGs_24)); }
	inline Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * get_LFifznDPvhfNRBpdXfgbwaFSgGs_24() const { return ___LFifznDPvhfNRBpdXfgbwaFSgGs_24; }
	inline Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B ** get_address_of_LFifznDPvhfNRBpdXfgbwaFSgGs_24() { return &___LFifznDPvhfNRBpdXfgbwaFSgGs_24; }
	inline void set_LFifznDPvhfNRBpdXfgbwaFSgGs_24(Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * value)
	{
		___LFifznDPvhfNRBpdXfgbwaFSgGs_24 = value;
		Il2CppCodeGenWriteBarrier((&___LFifznDPvhfNRBpdXfgbwaFSgGs_24), value);
	}

	inline static int32_t get_offset_of_yiXKAHllyDmQlfZQccEGegWUimJ_25() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___yiXKAHllyDmQlfZQccEGegWUimJ_25)); }
	inline Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E * get_yiXKAHllyDmQlfZQccEGegWUimJ_25() const { return ___yiXKAHllyDmQlfZQccEGegWUimJ_25; }
	inline Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E ** get_address_of_yiXKAHllyDmQlfZQccEGegWUimJ_25() { return &___yiXKAHllyDmQlfZQccEGegWUimJ_25; }
	inline void set_yiXKAHllyDmQlfZQccEGegWUimJ_25(Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E * value)
	{
		___yiXKAHllyDmQlfZQccEGegWUimJ_25 = value;
		Il2CppCodeGenWriteBarrier((&___yiXKAHllyDmQlfZQccEGegWUimJ_25), value);
	}

	inline static int32_t get_offset_of_egAXiAtTGeshJIBQsJWWhqgtlWu_26() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___egAXiAtTGeshJIBQsJWWhqgtlWu_26)); }
	inline Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E * get_egAXiAtTGeshJIBQsJWWhqgtlWu_26() const { return ___egAXiAtTGeshJIBQsJWWhqgtlWu_26; }
	inline Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E ** get_address_of_egAXiAtTGeshJIBQsJWWhqgtlWu_26() { return &___egAXiAtTGeshJIBQsJWWhqgtlWu_26; }
	inline void set_egAXiAtTGeshJIBQsJWWhqgtlWu_26(Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E * value)
	{
		___egAXiAtTGeshJIBQsJWWhqgtlWu_26 = value;
		Il2CppCodeGenWriteBarrier((&___egAXiAtTGeshJIBQsJWWhqgtlWu_26), value);
	}

	inline static int32_t get_offset_of_EKZEThysBIbRLLOmbBKIxvTvPIe_27() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___EKZEThysBIbRLLOmbBKIxvTvPIe_27)); }
	inline Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E * get_EKZEThysBIbRLLOmbBKIxvTvPIe_27() const { return ___EKZEThysBIbRLLOmbBKIxvTvPIe_27; }
	inline Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E ** get_address_of_EKZEThysBIbRLLOmbBKIxvTvPIe_27() { return &___EKZEThysBIbRLLOmbBKIxvTvPIe_27; }
	inline void set_EKZEThysBIbRLLOmbBKIxvTvPIe_27(Action_2_t91AB74BCA1B44EA5E52150C123F86433263B1F7E * value)
	{
		___EKZEThysBIbRLLOmbBKIxvTvPIe_27 = value;
		Il2CppCodeGenWriteBarrier((&___EKZEThysBIbRLLOmbBKIxvTvPIe_27), value);
	}

	inline static int32_t get_offset_of_LvLzjztTIRIUWhsEUhlEmObmghR_28() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___LvLzjztTIRIUWhsEUhlEmObmghR_28)); }
	inline Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * get_LvLzjztTIRIUWhsEUhlEmObmghR_28() const { return ___LvLzjztTIRIUWhsEUhlEmObmghR_28; }
	inline Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B ** get_address_of_LvLzjztTIRIUWhsEUhlEmObmghR_28() { return &___LvLzjztTIRIUWhsEUhlEmObmghR_28; }
	inline void set_LvLzjztTIRIUWhsEUhlEmObmghR_28(Action_1_t9B7C5376025AEF32439C13FB3BA4BFF8F0F0477B * value)
	{
		___LvLzjztTIRIUWhsEUhlEmObmghR_28 = value;
		Il2CppCodeGenWriteBarrier((&___LvLzjztTIRIUWhsEUhlEmObmghR_28), value);
	}

	inline static int32_t get_offset_of_vPmUStfFLBEocaEdvYkuqnSyeuL_29() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___vPmUStfFLBEocaEdvYkuqnSyeuL_29)); }
	inline Func_2_tD6D032CBFCBE5DEFC45886FF3D22684D36923253 * get_vPmUStfFLBEocaEdvYkuqnSyeuL_29() const { return ___vPmUStfFLBEocaEdvYkuqnSyeuL_29; }
	inline Func_2_tD6D032CBFCBE5DEFC45886FF3D22684D36923253 ** get_address_of_vPmUStfFLBEocaEdvYkuqnSyeuL_29() { return &___vPmUStfFLBEocaEdvYkuqnSyeuL_29; }
	inline void set_vPmUStfFLBEocaEdvYkuqnSyeuL_29(Func_2_tD6D032CBFCBE5DEFC45886FF3D22684D36923253 * value)
	{
		___vPmUStfFLBEocaEdvYkuqnSyeuL_29 = value;
		Il2CppCodeGenWriteBarrier((&___vPmUStfFLBEocaEdvYkuqnSyeuL_29), value);
	}

	inline static int32_t get_offset_of_bjfPXCqUhdrdhnQIIWAFhpWImVr_30() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___bjfPXCqUhdrdhnQIIWAFhpWImVr_30)); }
	inline Func_2_tD6D032CBFCBE5DEFC45886FF3D22684D36923253 * get_bjfPXCqUhdrdhnQIIWAFhpWImVr_30() const { return ___bjfPXCqUhdrdhnQIIWAFhpWImVr_30; }
	inline Func_2_tD6D032CBFCBE5DEFC45886FF3D22684D36923253 ** get_address_of_bjfPXCqUhdrdhnQIIWAFhpWImVr_30() { return &___bjfPXCqUhdrdhnQIIWAFhpWImVr_30; }
	inline void set_bjfPXCqUhdrdhnQIIWAFhpWImVr_30(Func_2_tD6D032CBFCBE5DEFC45886FF3D22684D36923253 * value)
	{
		___bjfPXCqUhdrdhnQIIWAFhpWImVr_30 = value;
		Il2CppCodeGenWriteBarrier((&___bjfPXCqUhdrdhnQIIWAFhpWImVr_30), value);
	}

	inline static int32_t get_offset_of_XjwgwCgXRWvXlTjIVBujMcjgpcZI_31() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9, ___XjwgwCgXRWvXlTjIVBujMcjgpcZI_31)); }
	inline Func_2_tEE835526B7A0EE187C9950EDA1D2C97725D65C09 * get_XjwgwCgXRWvXlTjIVBujMcjgpcZI_31() const { return ___XjwgwCgXRWvXlTjIVBujMcjgpcZI_31; }
	inline Func_2_tEE835526B7A0EE187C9950EDA1D2C97725D65C09 ** get_address_of_XjwgwCgXRWvXlTjIVBujMcjgpcZI_31() { return &___XjwgwCgXRWvXlTjIVBujMcjgpcZI_31; }
	inline void set_XjwgwCgXRWvXlTjIVBujMcjgpcZI_31(Func_2_tEE835526B7A0EE187C9950EDA1D2C97725D65C09 * value)
	{
		___XjwgwCgXRWvXlTjIVBujMcjgpcZI_31 = value;
		Il2CppCodeGenWriteBarrier((&___XjwgwCgXRWvXlTjIVBujMcjgpcZI_31), value);
	}
};

struct ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9_StaticFields
{
public:
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM::BaGbTVZuvXfpUGTSlCjpUqLZijs
	int32_t ___BaGbTVZuvXfpUGTSlCjpUqLZijs_12;

public:
	inline static int32_t get_offset_of_BaGbTVZuvXfpUGTSlCjpUqLZijs_12() { return static_cast<int32_t>(offsetof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9_StaticFields, ___BaGbTVZuvXfpUGTSlCjpUqLZijs_12)); }
	inline int32_t get_BaGbTVZuvXfpUGTSlCjpUqLZijs_12() const { return ___BaGbTVZuvXfpUGTSlCjpUqLZijs_12; }
	inline int32_t* get_address_of_BaGbTVZuvXfpUGTSlCjpUqLZijs_12() { return &___BaGbTVZuvXfpUGTSlCjpUqLZijs_12; }
	inline void set_BaGbTVZuvXfpUGTSlCjpUqLZijs_12(int32_t value)
	{
		___BaGbTVZuvXfpUGTSlCjpUqLZijs_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RERQQZNVNMKJXMRMWWZIRXFUKZM_TB8342C505CC902205234CB4CDFE12756108EB1F9_H
#ifndef BQBDBEIHTPLYDARKMLIPARASUAVG_TBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E_H
#define BQBDBEIHTPLYDARKMLIPARASUAVG_TBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg
struct  BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::jeLkzDDlxjdyTfidRDslEjdPyAn
	int32_t ___jeLkzDDlxjdyTfidRDslEjdPyAn_4;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::wneHRsRblNFSFcNjImlYgNBNpoh
	int32_t ___wneHRsRblNFSFcNjImlYgNBNpoh_5;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_6;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::URBQFqtSekBfWszqNQJIXMsRucH
	RuntimeObject* ___URBQFqtSekBfWszqNQJIXMsRucH_10;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::SlDNJpcQAjgKJoILnkZeUDovCno
	int32_t ___SlDNJpcQAjgKJoILnkZeUDovCno_11;
	// System.Collections.Generic.IList`1<Rewired.ControllerMap> Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::ePUoGypcneMKtjguroqjnBOyQBC
	RuntimeObject* ___ePUoGypcneMKtjguroqjnBOyQBC_12;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::mGiQGeMaNbfPDjeDFiBLphiZpRmA
	int32_t ___mGiQGeMaNbfPDjeDFiBLphiZpRmA_13;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::WnPgZpymgxIBcygizPMSsmzZolF
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___WnPgZpymgxIBcygizPMSsmzZolF_14;
	// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap> Rewired.Player_ControllerHelper_MapHelper_BqbDbEihtPLyDArkMLiPARASUAvg::AGUbjJCTtqVVCLumaYuWgKSxiyKf
	RuntimeObject* ___AGUbjJCTtqVVCLumaYuWgKSxiyKf_15;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___jeLkzDDlxjdyTfidRDslEjdPyAn_4)); }
	inline int32_t get_jeLkzDDlxjdyTfidRDslEjdPyAn_4() const { return ___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline int32_t* get_address_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return &___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline void set_jeLkzDDlxjdyTfidRDslEjdPyAn_4(int32_t value)
	{
		___jeLkzDDlxjdyTfidRDslEjdPyAn_4 = value;
	}

	inline static int32_t get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___wneHRsRblNFSFcNjImlYgNBNpoh_5)); }
	inline int32_t get_wneHRsRblNFSFcNjImlYgNBNpoh_5() const { return ___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline int32_t* get_address_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return &___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline void set_wneHRsRblNFSFcNjImlYgNBNpoh_5(int32_t value)
	{
		___wneHRsRblNFSFcNjImlYgNBNpoh_5 = value;
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_6() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___pTVJDeVeJlQThAZkFztHqDAWDUa_6)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_6() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_6; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_6() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_6; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_6(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_6 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_8; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_8(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_8 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9 = value;
	}

	inline static int32_t get_offset_of_URBQFqtSekBfWszqNQJIXMsRucH_10() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___URBQFqtSekBfWszqNQJIXMsRucH_10)); }
	inline RuntimeObject* get_URBQFqtSekBfWszqNQJIXMsRucH_10() const { return ___URBQFqtSekBfWszqNQJIXMsRucH_10; }
	inline RuntimeObject** get_address_of_URBQFqtSekBfWszqNQJIXMsRucH_10() { return &___URBQFqtSekBfWszqNQJIXMsRucH_10; }
	inline void set_URBQFqtSekBfWszqNQJIXMsRucH_10(RuntimeObject* value)
	{
		___URBQFqtSekBfWszqNQJIXMsRucH_10 = value;
		Il2CppCodeGenWriteBarrier((&___URBQFqtSekBfWszqNQJIXMsRucH_10), value);
	}

	inline static int32_t get_offset_of_SlDNJpcQAjgKJoILnkZeUDovCno_11() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___SlDNJpcQAjgKJoILnkZeUDovCno_11)); }
	inline int32_t get_SlDNJpcQAjgKJoILnkZeUDovCno_11() const { return ___SlDNJpcQAjgKJoILnkZeUDovCno_11; }
	inline int32_t* get_address_of_SlDNJpcQAjgKJoILnkZeUDovCno_11() { return &___SlDNJpcQAjgKJoILnkZeUDovCno_11; }
	inline void set_SlDNJpcQAjgKJoILnkZeUDovCno_11(int32_t value)
	{
		___SlDNJpcQAjgKJoILnkZeUDovCno_11 = value;
	}

	inline static int32_t get_offset_of_ePUoGypcneMKtjguroqjnBOyQBC_12() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___ePUoGypcneMKtjguroqjnBOyQBC_12)); }
	inline RuntimeObject* get_ePUoGypcneMKtjguroqjnBOyQBC_12() const { return ___ePUoGypcneMKtjguroqjnBOyQBC_12; }
	inline RuntimeObject** get_address_of_ePUoGypcneMKtjguroqjnBOyQBC_12() { return &___ePUoGypcneMKtjguroqjnBOyQBC_12; }
	inline void set_ePUoGypcneMKtjguroqjnBOyQBC_12(RuntimeObject* value)
	{
		___ePUoGypcneMKtjguroqjnBOyQBC_12 = value;
		Il2CppCodeGenWriteBarrier((&___ePUoGypcneMKtjguroqjnBOyQBC_12), value);
	}

	inline static int32_t get_offset_of_mGiQGeMaNbfPDjeDFiBLphiZpRmA_13() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___mGiQGeMaNbfPDjeDFiBLphiZpRmA_13)); }
	inline int32_t get_mGiQGeMaNbfPDjeDFiBLphiZpRmA_13() const { return ___mGiQGeMaNbfPDjeDFiBLphiZpRmA_13; }
	inline int32_t* get_address_of_mGiQGeMaNbfPDjeDFiBLphiZpRmA_13() { return &___mGiQGeMaNbfPDjeDFiBLphiZpRmA_13; }
	inline void set_mGiQGeMaNbfPDjeDFiBLphiZpRmA_13(int32_t value)
	{
		___mGiQGeMaNbfPDjeDFiBLphiZpRmA_13 = value;
	}

	inline static int32_t get_offset_of_WnPgZpymgxIBcygizPMSsmzZolF_14() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___WnPgZpymgxIBcygizPMSsmzZolF_14)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_WnPgZpymgxIBcygizPMSsmzZolF_14() const { return ___WnPgZpymgxIBcygizPMSsmzZolF_14; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_WnPgZpymgxIBcygizPMSsmzZolF_14() { return &___WnPgZpymgxIBcygizPMSsmzZolF_14; }
	inline void set_WnPgZpymgxIBcygizPMSsmzZolF_14(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___WnPgZpymgxIBcygizPMSsmzZolF_14 = value;
		Il2CppCodeGenWriteBarrier((&___WnPgZpymgxIBcygizPMSsmzZolF_14), value);
	}

	inline static int32_t get_offset_of_AGUbjJCTtqVVCLumaYuWgKSxiyKf_15() { return static_cast<int32_t>(offsetof(BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E, ___AGUbjJCTtqVVCLumaYuWgKSxiyKf_15)); }
	inline RuntimeObject* get_AGUbjJCTtqVVCLumaYuWgKSxiyKf_15() const { return ___AGUbjJCTtqVVCLumaYuWgKSxiyKf_15; }
	inline RuntimeObject** get_address_of_AGUbjJCTtqVVCLumaYuWgKSxiyKf_15() { return &___AGUbjJCTtqVVCLumaYuWgKSxiyKf_15; }
	inline void set_AGUbjJCTtqVVCLumaYuWgKSxiyKf_15(RuntimeObject* value)
	{
		___AGUbjJCTtqVVCLumaYuWgKSxiyKf_15 = value;
		Il2CppCodeGenWriteBarrier((&___AGUbjJCTtqVVCLumaYuWgKSxiyKf_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BQBDBEIHTPLYDARKMLIPARASUAVG_TBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E_H
#ifndef LZNTEMOAFGCFZTKDXKOKEOBSVMC_T9993E595F142C71E41AC80B033CB598352BCCDF2_H
#define LZNTEMOAFGCFZTKDXKOKEOBSVMC_T9993E595F142C71E41AC80B033CB598352BCCDF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc
struct  LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::jeLkzDDlxjdyTfidRDslEjdPyAn
	int32_t ___jeLkzDDlxjdyTfidRDslEjdPyAn_4;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::wneHRsRblNFSFcNjImlYgNBNpoh
	int32_t ___wneHRsRblNFSFcNjImlYgNBNpoh_5;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::xJPYTeJZICkFpIWTwbMsXQVEALx
	int32_t ___xJPYTeJZICkFpIWTwbMsXQVEALx_6;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::kilUCbTWeCfCdDqQSHFWpvHLrBl
	int32_t ___kilUCbTWeCfCdDqQSHFWpvHLrBl_7;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_8;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::rOdCrMgoKIoahkqSSMCMtJBpwDO
	RuntimeObject* ___rOdCrMgoKIoahkqSSMCMtJBpwDO_12;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::sdDvxnFolHwwcrdOGVamLWCrEqo
	int32_t ___sdDvxnFolHwwcrdOGVamLWCrEqo_13;
	// System.Collections.Generic.IList`1<Rewired.ControllerMap> Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::AHSMAVBFTUzcGFcKShfsCnpLinS
	RuntimeObject* ___AHSMAVBFTUzcGFcKShfsCnpLinS_14;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::GqLpAZjfWeSypLuaMRNuestZemq
	int32_t ___GqLpAZjfWeSypLuaMRNuestZemq_15;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::CtDhkLgfHuehRmUChwupgTmIyYcL
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___CtDhkLgfHuehRmUChwupgTmIyYcL_16;
	// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap> Rewired.Player_ControllerHelper_MapHelper_LZntEMOAFGCFZtKDXkOkEobSvmc::ACzrLpCTdoWBkZpZWOfZxGsWgQf
	RuntimeObject* ___ACzrLpCTdoWBkZpZWOfZxGsWgQf_17;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___jeLkzDDlxjdyTfidRDslEjdPyAn_4)); }
	inline int32_t get_jeLkzDDlxjdyTfidRDslEjdPyAn_4() const { return ___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline int32_t* get_address_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return &___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline void set_jeLkzDDlxjdyTfidRDslEjdPyAn_4(int32_t value)
	{
		___jeLkzDDlxjdyTfidRDslEjdPyAn_4 = value;
	}

	inline static int32_t get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___wneHRsRblNFSFcNjImlYgNBNpoh_5)); }
	inline int32_t get_wneHRsRblNFSFcNjImlYgNBNpoh_5() const { return ___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline int32_t* get_address_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return &___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline void set_wneHRsRblNFSFcNjImlYgNBNpoh_5(int32_t value)
	{
		___wneHRsRblNFSFcNjImlYgNBNpoh_5 = value;
	}

	inline static int32_t get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_6() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___xJPYTeJZICkFpIWTwbMsXQVEALx_6)); }
	inline int32_t get_xJPYTeJZICkFpIWTwbMsXQVEALx_6() const { return ___xJPYTeJZICkFpIWTwbMsXQVEALx_6; }
	inline int32_t* get_address_of_xJPYTeJZICkFpIWTwbMsXQVEALx_6() { return &___xJPYTeJZICkFpIWTwbMsXQVEALx_6; }
	inline void set_xJPYTeJZICkFpIWTwbMsXQVEALx_6(int32_t value)
	{
		___xJPYTeJZICkFpIWTwbMsXQVEALx_6 = value;
	}

	inline static int32_t get_offset_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_7() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___kilUCbTWeCfCdDqQSHFWpvHLrBl_7)); }
	inline int32_t get_kilUCbTWeCfCdDqQSHFWpvHLrBl_7() const { return ___kilUCbTWeCfCdDqQSHFWpvHLrBl_7; }
	inline int32_t* get_address_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_7() { return &___kilUCbTWeCfCdDqQSHFWpvHLrBl_7; }
	inline void set_kilUCbTWeCfCdDqQSHFWpvHLrBl_7(int32_t value)
	{
		___kilUCbTWeCfCdDqQSHFWpvHLrBl_7 = value;
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_8() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___pTVJDeVeJlQThAZkFztHqDAWDUa_8)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_8() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_8; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_8() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_8; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_8(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_8 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_10; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_10(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_10 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11 = value;
	}

	inline static int32_t get_offset_of_rOdCrMgoKIoahkqSSMCMtJBpwDO_12() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___rOdCrMgoKIoahkqSSMCMtJBpwDO_12)); }
	inline RuntimeObject* get_rOdCrMgoKIoahkqSSMCMtJBpwDO_12() const { return ___rOdCrMgoKIoahkqSSMCMtJBpwDO_12; }
	inline RuntimeObject** get_address_of_rOdCrMgoKIoahkqSSMCMtJBpwDO_12() { return &___rOdCrMgoKIoahkqSSMCMtJBpwDO_12; }
	inline void set_rOdCrMgoKIoahkqSSMCMtJBpwDO_12(RuntimeObject* value)
	{
		___rOdCrMgoKIoahkqSSMCMtJBpwDO_12 = value;
		Il2CppCodeGenWriteBarrier((&___rOdCrMgoKIoahkqSSMCMtJBpwDO_12), value);
	}

	inline static int32_t get_offset_of_sdDvxnFolHwwcrdOGVamLWCrEqo_13() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___sdDvxnFolHwwcrdOGVamLWCrEqo_13)); }
	inline int32_t get_sdDvxnFolHwwcrdOGVamLWCrEqo_13() const { return ___sdDvxnFolHwwcrdOGVamLWCrEqo_13; }
	inline int32_t* get_address_of_sdDvxnFolHwwcrdOGVamLWCrEqo_13() { return &___sdDvxnFolHwwcrdOGVamLWCrEqo_13; }
	inline void set_sdDvxnFolHwwcrdOGVamLWCrEqo_13(int32_t value)
	{
		___sdDvxnFolHwwcrdOGVamLWCrEqo_13 = value;
	}

	inline static int32_t get_offset_of_AHSMAVBFTUzcGFcKShfsCnpLinS_14() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___AHSMAVBFTUzcGFcKShfsCnpLinS_14)); }
	inline RuntimeObject* get_AHSMAVBFTUzcGFcKShfsCnpLinS_14() const { return ___AHSMAVBFTUzcGFcKShfsCnpLinS_14; }
	inline RuntimeObject** get_address_of_AHSMAVBFTUzcGFcKShfsCnpLinS_14() { return &___AHSMAVBFTUzcGFcKShfsCnpLinS_14; }
	inline void set_AHSMAVBFTUzcGFcKShfsCnpLinS_14(RuntimeObject* value)
	{
		___AHSMAVBFTUzcGFcKShfsCnpLinS_14 = value;
		Il2CppCodeGenWriteBarrier((&___AHSMAVBFTUzcGFcKShfsCnpLinS_14), value);
	}

	inline static int32_t get_offset_of_GqLpAZjfWeSypLuaMRNuestZemq_15() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___GqLpAZjfWeSypLuaMRNuestZemq_15)); }
	inline int32_t get_GqLpAZjfWeSypLuaMRNuestZemq_15() const { return ___GqLpAZjfWeSypLuaMRNuestZemq_15; }
	inline int32_t* get_address_of_GqLpAZjfWeSypLuaMRNuestZemq_15() { return &___GqLpAZjfWeSypLuaMRNuestZemq_15; }
	inline void set_GqLpAZjfWeSypLuaMRNuestZemq_15(int32_t value)
	{
		___GqLpAZjfWeSypLuaMRNuestZemq_15 = value;
	}

	inline static int32_t get_offset_of_CtDhkLgfHuehRmUChwupgTmIyYcL_16() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___CtDhkLgfHuehRmUChwupgTmIyYcL_16)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_CtDhkLgfHuehRmUChwupgTmIyYcL_16() const { return ___CtDhkLgfHuehRmUChwupgTmIyYcL_16; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_CtDhkLgfHuehRmUChwupgTmIyYcL_16() { return &___CtDhkLgfHuehRmUChwupgTmIyYcL_16; }
	inline void set_CtDhkLgfHuehRmUChwupgTmIyYcL_16(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___CtDhkLgfHuehRmUChwupgTmIyYcL_16 = value;
		Il2CppCodeGenWriteBarrier((&___CtDhkLgfHuehRmUChwupgTmIyYcL_16), value);
	}

	inline static int32_t get_offset_of_ACzrLpCTdoWBkZpZWOfZxGsWgQf_17() { return static_cast<int32_t>(offsetof(LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2, ___ACzrLpCTdoWBkZpZWOfZxGsWgQf_17)); }
	inline RuntimeObject* get_ACzrLpCTdoWBkZpZWOfZxGsWgQf_17() const { return ___ACzrLpCTdoWBkZpZWOfZxGsWgQf_17; }
	inline RuntimeObject** get_address_of_ACzrLpCTdoWBkZpZWOfZxGsWgQf_17() { return &___ACzrLpCTdoWBkZpZWOfZxGsWgQf_17; }
	inline void set_ACzrLpCTdoWBkZpZWOfZxGsWgQf_17(RuntimeObject* value)
	{
		___ACzrLpCTdoWBkZpZWOfZxGsWgQf_17 = value;
		Il2CppCodeGenWriteBarrier((&___ACzrLpCTdoWBkZpZWOfZxGsWgQf_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LZNTEMOAFGCFZTKDXKOKEOBSVMC_T9993E595F142C71E41AC80B033CB598352BCCDF2_H
#ifndef LRWDWQJEFRFOZGSUHELARHOTGPUS_T5DECA78105C14A54766FA928B4A263AC4C18E981_H
#define LRWDWQJEFRFOZGSUHELARHOTGPUS_T5DECA78105C14A54766FA928B4A263AC4C18E981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS
struct  LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::jeLkzDDlxjdyTfidRDslEjdPyAn
	int32_t ___jeLkzDDlxjdyTfidRDslEjdPyAn_4;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::wneHRsRblNFSFcNjImlYgNBNpoh
	int32_t ___wneHRsRblNFSFcNjImlYgNBNpoh_5;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_6;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::qeXBiJGkJPOsYChNhIanovZIDZIv
	RuntimeObject* ___qeXBiJGkJPOsYChNhIanovZIDZIv_10;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::lZjUpbYVsHHZaOjywcFfeUKIvpP
	int32_t ___lZjUpbYVsHHZaOjywcFfeUKIvpP_11;
	// System.Collections.Generic.IList`1<Rewired.ControllerMap> Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::KNHspcwLKOHnkHqLpGFvwvmwgRFf
	RuntimeObject* ___KNHspcwLKOHnkHqLpGFvwvmwgRFf_12;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::WWpwEwCFOVfFCThcYDWwAVmSEfiP
	int32_t ___WWpwEwCFOVfFCThcYDWwAVmSEfiP_13;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::tDVTJhFpYkhMiEbIKaIynmfpvOh
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___tDVTJhFpYkhMiEbIKaIynmfpvOh_14;
	// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap> Rewired.Player_ControllerHelper_MapHelper_LrWDwqJEfrfOZGsuhELARHoTgpUS::lnJghGeJvBsfYDKqLafZBImysnCN
	RuntimeObject* ___lnJghGeJvBsfYDKqLafZBImysnCN_15;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___jeLkzDDlxjdyTfidRDslEjdPyAn_4)); }
	inline int32_t get_jeLkzDDlxjdyTfidRDslEjdPyAn_4() const { return ___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline int32_t* get_address_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return &___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline void set_jeLkzDDlxjdyTfidRDslEjdPyAn_4(int32_t value)
	{
		___jeLkzDDlxjdyTfidRDslEjdPyAn_4 = value;
	}

	inline static int32_t get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___wneHRsRblNFSFcNjImlYgNBNpoh_5)); }
	inline int32_t get_wneHRsRblNFSFcNjImlYgNBNpoh_5() const { return ___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline int32_t* get_address_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return &___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline void set_wneHRsRblNFSFcNjImlYgNBNpoh_5(int32_t value)
	{
		___wneHRsRblNFSFcNjImlYgNBNpoh_5 = value;
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_6() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___pTVJDeVeJlQThAZkFztHqDAWDUa_6)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_6() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_6; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_6() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_6; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_6(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_6 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_8; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_8(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_8 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9 = value;
	}

	inline static int32_t get_offset_of_qeXBiJGkJPOsYChNhIanovZIDZIv_10() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___qeXBiJGkJPOsYChNhIanovZIDZIv_10)); }
	inline RuntimeObject* get_qeXBiJGkJPOsYChNhIanovZIDZIv_10() const { return ___qeXBiJGkJPOsYChNhIanovZIDZIv_10; }
	inline RuntimeObject** get_address_of_qeXBiJGkJPOsYChNhIanovZIDZIv_10() { return &___qeXBiJGkJPOsYChNhIanovZIDZIv_10; }
	inline void set_qeXBiJGkJPOsYChNhIanovZIDZIv_10(RuntimeObject* value)
	{
		___qeXBiJGkJPOsYChNhIanovZIDZIv_10 = value;
		Il2CppCodeGenWriteBarrier((&___qeXBiJGkJPOsYChNhIanovZIDZIv_10), value);
	}

	inline static int32_t get_offset_of_lZjUpbYVsHHZaOjywcFfeUKIvpP_11() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___lZjUpbYVsHHZaOjywcFfeUKIvpP_11)); }
	inline int32_t get_lZjUpbYVsHHZaOjywcFfeUKIvpP_11() const { return ___lZjUpbYVsHHZaOjywcFfeUKIvpP_11; }
	inline int32_t* get_address_of_lZjUpbYVsHHZaOjywcFfeUKIvpP_11() { return &___lZjUpbYVsHHZaOjywcFfeUKIvpP_11; }
	inline void set_lZjUpbYVsHHZaOjywcFfeUKIvpP_11(int32_t value)
	{
		___lZjUpbYVsHHZaOjywcFfeUKIvpP_11 = value;
	}

	inline static int32_t get_offset_of_KNHspcwLKOHnkHqLpGFvwvmwgRFf_12() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___KNHspcwLKOHnkHqLpGFvwvmwgRFf_12)); }
	inline RuntimeObject* get_KNHspcwLKOHnkHqLpGFvwvmwgRFf_12() const { return ___KNHspcwLKOHnkHqLpGFvwvmwgRFf_12; }
	inline RuntimeObject** get_address_of_KNHspcwLKOHnkHqLpGFvwvmwgRFf_12() { return &___KNHspcwLKOHnkHqLpGFvwvmwgRFf_12; }
	inline void set_KNHspcwLKOHnkHqLpGFvwvmwgRFf_12(RuntimeObject* value)
	{
		___KNHspcwLKOHnkHqLpGFvwvmwgRFf_12 = value;
		Il2CppCodeGenWriteBarrier((&___KNHspcwLKOHnkHqLpGFvwvmwgRFf_12), value);
	}

	inline static int32_t get_offset_of_WWpwEwCFOVfFCThcYDWwAVmSEfiP_13() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___WWpwEwCFOVfFCThcYDWwAVmSEfiP_13)); }
	inline int32_t get_WWpwEwCFOVfFCThcYDWwAVmSEfiP_13() const { return ___WWpwEwCFOVfFCThcYDWwAVmSEfiP_13; }
	inline int32_t* get_address_of_WWpwEwCFOVfFCThcYDWwAVmSEfiP_13() { return &___WWpwEwCFOVfFCThcYDWwAVmSEfiP_13; }
	inline void set_WWpwEwCFOVfFCThcYDWwAVmSEfiP_13(int32_t value)
	{
		___WWpwEwCFOVfFCThcYDWwAVmSEfiP_13 = value;
	}

	inline static int32_t get_offset_of_tDVTJhFpYkhMiEbIKaIynmfpvOh_14() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___tDVTJhFpYkhMiEbIKaIynmfpvOh_14)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_tDVTJhFpYkhMiEbIKaIynmfpvOh_14() const { return ___tDVTJhFpYkhMiEbIKaIynmfpvOh_14; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_tDVTJhFpYkhMiEbIKaIynmfpvOh_14() { return &___tDVTJhFpYkhMiEbIKaIynmfpvOh_14; }
	inline void set_tDVTJhFpYkhMiEbIKaIynmfpvOh_14(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___tDVTJhFpYkhMiEbIKaIynmfpvOh_14 = value;
		Il2CppCodeGenWriteBarrier((&___tDVTJhFpYkhMiEbIKaIynmfpvOh_14), value);
	}

	inline static int32_t get_offset_of_lnJghGeJvBsfYDKqLafZBImysnCN_15() { return static_cast<int32_t>(offsetof(LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981, ___lnJghGeJvBsfYDKqLafZBImysnCN_15)); }
	inline RuntimeObject* get_lnJghGeJvBsfYDKqLafZBImysnCN_15() const { return ___lnJghGeJvBsfYDKqLafZBImysnCN_15; }
	inline RuntimeObject** get_address_of_lnJghGeJvBsfYDKqLafZBImysnCN_15() { return &___lnJghGeJvBsfYDKqLafZBImysnCN_15; }
	inline void set_lnJghGeJvBsfYDKqLafZBImysnCN_15(RuntimeObject* value)
	{
		___lnJghGeJvBsfYDKqLafZBImysnCN_15 = value;
		Il2CppCodeGenWriteBarrier((&___lnJghGeJvBsfYDKqLafZBImysnCN_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LRWDWQJEFRFOZGSUHELARHOTGPUS_T5DECA78105C14A54766FA928B4A263AC4C18E981_H
#ifndef MIHKPWYTQTURWZRGUQIMQNAUAWT_T328F86AC6593241423296E00584E8CAAD4FE84F2_H
#define MIHKPWYTQTURWZRGUQIMQNAUAWT_T328F86AC6593241423296E00584E8CAAD4FE84F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT
struct  MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::jeLkzDDlxjdyTfidRDslEjdPyAn
	int32_t ___jeLkzDDlxjdyTfidRDslEjdPyAn_4;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::wneHRsRblNFSFcNjImlYgNBNpoh
	int32_t ___wneHRsRblNFSFcNjImlYgNBNpoh_5;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_6;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::kvlmkOReOsuPlGtzVYsuzKQKzot
	RuntimeObject* ___kvlmkOReOsuPlGtzVYsuzKQKzot_10;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::BtZoJNsgrjeLvHlTejeztsvEagHQ
	int32_t ___BtZoJNsgrjeLvHlTejeztsvEagHQ_11;
	// System.Collections.Generic.IList`1<Rewired.ControllerMap> Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::vPSvjioqaGGGZNiUosulfFPocLI
	RuntimeObject* ___vPSvjioqaGGGZNiUosulfFPocLI_12;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::upHULLWDzLCbgIAwDhjpGPmfzNc
	int32_t ___upHULLWDzLCbgIAwDhjpGPmfzNc_13;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::LCSJwoIOgFUkeQxXqctTiThJJqh
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___LCSJwoIOgFUkeQxXqctTiThJJqh_14;
	// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap> Rewired.Player_ControllerHelper_MapHelper_MiHkPwytqtUrWzRgUqimqnAUAWT::ZoGyaSnOEvbuHTDVqGMYZZuaVlp
	RuntimeObject* ___ZoGyaSnOEvbuHTDVqGMYZZuaVlp_15;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___jeLkzDDlxjdyTfidRDslEjdPyAn_4)); }
	inline int32_t get_jeLkzDDlxjdyTfidRDslEjdPyAn_4() const { return ___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline int32_t* get_address_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return &___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline void set_jeLkzDDlxjdyTfidRDslEjdPyAn_4(int32_t value)
	{
		___jeLkzDDlxjdyTfidRDslEjdPyAn_4 = value;
	}

	inline static int32_t get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___wneHRsRblNFSFcNjImlYgNBNpoh_5)); }
	inline int32_t get_wneHRsRblNFSFcNjImlYgNBNpoh_5() const { return ___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline int32_t* get_address_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return &___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline void set_wneHRsRblNFSFcNjImlYgNBNpoh_5(int32_t value)
	{
		___wneHRsRblNFSFcNjImlYgNBNpoh_5 = value;
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_6() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___pTVJDeVeJlQThAZkFztHqDAWDUa_6)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_6() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_6; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_6() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_6; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_6(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_6 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_7 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_8; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_8; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_8(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_8 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_9 = value;
	}

	inline static int32_t get_offset_of_kvlmkOReOsuPlGtzVYsuzKQKzot_10() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___kvlmkOReOsuPlGtzVYsuzKQKzot_10)); }
	inline RuntimeObject* get_kvlmkOReOsuPlGtzVYsuzKQKzot_10() const { return ___kvlmkOReOsuPlGtzVYsuzKQKzot_10; }
	inline RuntimeObject** get_address_of_kvlmkOReOsuPlGtzVYsuzKQKzot_10() { return &___kvlmkOReOsuPlGtzVYsuzKQKzot_10; }
	inline void set_kvlmkOReOsuPlGtzVYsuzKQKzot_10(RuntimeObject* value)
	{
		___kvlmkOReOsuPlGtzVYsuzKQKzot_10 = value;
		Il2CppCodeGenWriteBarrier((&___kvlmkOReOsuPlGtzVYsuzKQKzot_10), value);
	}

	inline static int32_t get_offset_of_BtZoJNsgrjeLvHlTejeztsvEagHQ_11() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___BtZoJNsgrjeLvHlTejeztsvEagHQ_11)); }
	inline int32_t get_BtZoJNsgrjeLvHlTejeztsvEagHQ_11() const { return ___BtZoJNsgrjeLvHlTejeztsvEagHQ_11; }
	inline int32_t* get_address_of_BtZoJNsgrjeLvHlTejeztsvEagHQ_11() { return &___BtZoJNsgrjeLvHlTejeztsvEagHQ_11; }
	inline void set_BtZoJNsgrjeLvHlTejeztsvEagHQ_11(int32_t value)
	{
		___BtZoJNsgrjeLvHlTejeztsvEagHQ_11 = value;
	}

	inline static int32_t get_offset_of_vPSvjioqaGGGZNiUosulfFPocLI_12() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___vPSvjioqaGGGZNiUosulfFPocLI_12)); }
	inline RuntimeObject* get_vPSvjioqaGGGZNiUosulfFPocLI_12() const { return ___vPSvjioqaGGGZNiUosulfFPocLI_12; }
	inline RuntimeObject** get_address_of_vPSvjioqaGGGZNiUosulfFPocLI_12() { return &___vPSvjioqaGGGZNiUosulfFPocLI_12; }
	inline void set_vPSvjioqaGGGZNiUosulfFPocLI_12(RuntimeObject* value)
	{
		___vPSvjioqaGGGZNiUosulfFPocLI_12 = value;
		Il2CppCodeGenWriteBarrier((&___vPSvjioqaGGGZNiUosulfFPocLI_12), value);
	}

	inline static int32_t get_offset_of_upHULLWDzLCbgIAwDhjpGPmfzNc_13() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___upHULLWDzLCbgIAwDhjpGPmfzNc_13)); }
	inline int32_t get_upHULLWDzLCbgIAwDhjpGPmfzNc_13() const { return ___upHULLWDzLCbgIAwDhjpGPmfzNc_13; }
	inline int32_t* get_address_of_upHULLWDzLCbgIAwDhjpGPmfzNc_13() { return &___upHULLWDzLCbgIAwDhjpGPmfzNc_13; }
	inline void set_upHULLWDzLCbgIAwDhjpGPmfzNc_13(int32_t value)
	{
		___upHULLWDzLCbgIAwDhjpGPmfzNc_13 = value;
	}

	inline static int32_t get_offset_of_LCSJwoIOgFUkeQxXqctTiThJJqh_14() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___LCSJwoIOgFUkeQxXqctTiThJJqh_14)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_LCSJwoIOgFUkeQxXqctTiThJJqh_14() const { return ___LCSJwoIOgFUkeQxXqctTiThJJqh_14; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_LCSJwoIOgFUkeQxXqctTiThJJqh_14() { return &___LCSJwoIOgFUkeQxXqctTiThJJqh_14; }
	inline void set_LCSJwoIOgFUkeQxXqctTiThJJqh_14(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___LCSJwoIOgFUkeQxXqctTiThJJqh_14 = value;
		Il2CppCodeGenWriteBarrier((&___LCSJwoIOgFUkeQxXqctTiThJJqh_14), value);
	}

	inline static int32_t get_offset_of_ZoGyaSnOEvbuHTDVqGMYZZuaVlp_15() { return static_cast<int32_t>(offsetof(MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2, ___ZoGyaSnOEvbuHTDVqGMYZZuaVlp_15)); }
	inline RuntimeObject* get_ZoGyaSnOEvbuHTDVqGMYZZuaVlp_15() const { return ___ZoGyaSnOEvbuHTDVqGMYZZuaVlp_15; }
	inline RuntimeObject** get_address_of_ZoGyaSnOEvbuHTDVqGMYZZuaVlp_15() { return &___ZoGyaSnOEvbuHTDVqGMYZZuaVlp_15; }
	inline void set_ZoGyaSnOEvbuHTDVqGMYZZuaVlp_15(RuntimeObject* value)
	{
		___ZoGyaSnOEvbuHTDVqGMYZZuaVlp_15 = value;
		Il2CppCodeGenWriteBarrier((&___ZoGyaSnOEvbuHTDVqGMYZZuaVlp_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIHKPWYTQTURWZRGUQIMQNAUAWT_T328F86AC6593241423296E00584E8CAAD4FE84F2_H
#ifndef TOQRFXHZMSQLZBXCRGWIUBMBMFY_TA830AB0AC8FEAE08289B6EBE55B9B2E619169332_H
#define TOQRFXHZMSQLZBXCRGWIUBMBMFY_TA830AB0AC8FEAE08289B6EBE55B9B2E619169332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY
struct  TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332  : public RuntimeObject
{
public:
	// Rewired.ControllerMap Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::OoFswlQbqBhHqGOCooflwYCYmYWF
	int32_t ___OoFswlQbqBhHqGOCooflwYCYmYWF_4;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::pyCASHftKaJjucJBlmeeWQrdKuRk
	int32_t ___pyCASHftKaJjucJBlmeeWQrdKuRk_5;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::jeLkzDDlxjdyTfidRDslEjdPyAn
	int32_t ___jeLkzDDlxjdyTfidRDslEjdPyAn_6;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::wneHRsRblNFSFcNjImlYgNBNpoh
	int32_t ___wneHRsRblNFSFcNjImlYgNBNpoh_7;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::ADxKkzWMUBYYOEXoCnKRlJGONdp
	RuntimeObject* ___ADxKkzWMUBYYOEXoCnKRlJGONdp_8;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::onuUdLqyZglNFRXvVnmNGVhyvEk
	int32_t ___onuUdLqyZglNFRXvVnmNGVhyvEk_9;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::zXzjGKUSBUPcxIVQpiLUWniHLTD
	int32_t ___zXzjGKUSBUPcxIVQpiLUWniHLTD_10;
	// JrKetNtIJINuYILTEYSwNUvgPiW Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::ReIdYCkkTUyMvyPmUebDXmqIZIq
	RuntimeObject* ___ReIdYCkkTUyMvyPmUebDXmqIZIq_11;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::PaFIcIdtgpJEDjPWtuVHsnYMPWHh
	int32_t ___PaFIcIdtgpJEDjPWtuVHsnYMPWHh_12;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::XHPgOPOiyVEVTrtGpFVwhUYYVBLH
	int32_t ___XHPgOPOiyVEVTrtGpFVwhUYYVBLH_13;
	// Rewired.ControllerMap Rewired.Player_ControllerHelper_MapHelper_TOqrfxhzmSQlZbxCrgWIubmBMfY::WErGCEfmgpirqkWeueqfYlfBfKCt
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___WErGCEfmgpirqkWeueqfYlfBfKCt_14;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_OoFswlQbqBhHqGOCooflwYCYmYWF_4() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___OoFswlQbqBhHqGOCooflwYCYmYWF_4)); }
	inline int32_t get_OoFswlQbqBhHqGOCooflwYCYmYWF_4() const { return ___OoFswlQbqBhHqGOCooflwYCYmYWF_4; }
	inline int32_t* get_address_of_OoFswlQbqBhHqGOCooflwYCYmYWF_4() { return &___OoFswlQbqBhHqGOCooflwYCYmYWF_4; }
	inline void set_OoFswlQbqBhHqGOCooflwYCYmYWF_4(int32_t value)
	{
		___OoFswlQbqBhHqGOCooflwYCYmYWF_4 = value;
	}

	inline static int32_t get_offset_of_pyCASHftKaJjucJBlmeeWQrdKuRk_5() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___pyCASHftKaJjucJBlmeeWQrdKuRk_5)); }
	inline int32_t get_pyCASHftKaJjucJBlmeeWQrdKuRk_5() const { return ___pyCASHftKaJjucJBlmeeWQrdKuRk_5; }
	inline int32_t* get_address_of_pyCASHftKaJjucJBlmeeWQrdKuRk_5() { return &___pyCASHftKaJjucJBlmeeWQrdKuRk_5; }
	inline void set_pyCASHftKaJjucJBlmeeWQrdKuRk_5(int32_t value)
	{
		___pyCASHftKaJjucJBlmeeWQrdKuRk_5 = value;
	}

	inline static int32_t get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_6() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___jeLkzDDlxjdyTfidRDslEjdPyAn_6)); }
	inline int32_t get_jeLkzDDlxjdyTfidRDslEjdPyAn_6() const { return ___jeLkzDDlxjdyTfidRDslEjdPyAn_6; }
	inline int32_t* get_address_of_jeLkzDDlxjdyTfidRDslEjdPyAn_6() { return &___jeLkzDDlxjdyTfidRDslEjdPyAn_6; }
	inline void set_jeLkzDDlxjdyTfidRDslEjdPyAn_6(int32_t value)
	{
		___jeLkzDDlxjdyTfidRDslEjdPyAn_6 = value;
	}

	inline static int32_t get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_7() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___wneHRsRblNFSFcNjImlYgNBNpoh_7)); }
	inline int32_t get_wneHRsRblNFSFcNjImlYgNBNpoh_7() const { return ___wneHRsRblNFSFcNjImlYgNBNpoh_7; }
	inline int32_t* get_address_of_wneHRsRblNFSFcNjImlYgNBNpoh_7() { return &___wneHRsRblNFSFcNjImlYgNBNpoh_7; }
	inline void set_wneHRsRblNFSFcNjImlYgNBNpoh_7(int32_t value)
	{
		___wneHRsRblNFSFcNjImlYgNBNpoh_7 = value;
	}

	inline static int32_t get_offset_of_ADxKkzWMUBYYOEXoCnKRlJGONdp_8() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___ADxKkzWMUBYYOEXoCnKRlJGONdp_8)); }
	inline RuntimeObject* get_ADxKkzWMUBYYOEXoCnKRlJGONdp_8() const { return ___ADxKkzWMUBYYOEXoCnKRlJGONdp_8; }
	inline RuntimeObject** get_address_of_ADxKkzWMUBYYOEXoCnKRlJGONdp_8() { return &___ADxKkzWMUBYYOEXoCnKRlJGONdp_8; }
	inline void set_ADxKkzWMUBYYOEXoCnKRlJGONdp_8(RuntimeObject* value)
	{
		___ADxKkzWMUBYYOEXoCnKRlJGONdp_8 = value;
		Il2CppCodeGenWriteBarrier((&___ADxKkzWMUBYYOEXoCnKRlJGONdp_8), value);
	}

	inline static int32_t get_offset_of_onuUdLqyZglNFRXvVnmNGVhyvEk_9() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___onuUdLqyZglNFRXvVnmNGVhyvEk_9)); }
	inline int32_t get_onuUdLqyZglNFRXvVnmNGVhyvEk_9() const { return ___onuUdLqyZglNFRXvVnmNGVhyvEk_9; }
	inline int32_t* get_address_of_onuUdLqyZglNFRXvVnmNGVhyvEk_9() { return &___onuUdLqyZglNFRXvVnmNGVhyvEk_9; }
	inline void set_onuUdLqyZglNFRXvVnmNGVhyvEk_9(int32_t value)
	{
		___onuUdLqyZglNFRXvVnmNGVhyvEk_9 = value;
	}

	inline static int32_t get_offset_of_zXzjGKUSBUPcxIVQpiLUWniHLTD_10() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___zXzjGKUSBUPcxIVQpiLUWniHLTD_10)); }
	inline int32_t get_zXzjGKUSBUPcxIVQpiLUWniHLTD_10() const { return ___zXzjGKUSBUPcxIVQpiLUWniHLTD_10; }
	inline int32_t* get_address_of_zXzjGKUSBUPcxIVQpiLUWniHLTD_10() { return &___zXzjGKUSBUPcxIVQpiLUWniHLTD_10; }
	inline void set_zXzjGKUSBUPcxIVQpiLUWniHLTD_10(int32_t value)
	{
		___zXzjGKUSBUPcxIVQpiLUWniHLTD_10 = value;
	}

	inline static int32_t get_offset_of_ReIdYCkkTUyMvyPmUebDXmqIZIq_11() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___ReIdYCkkTUyMvyPmUebDXmqIZIq_11)); }
	inline RuntimeObject* get_ReIdYCkkTUyMvyPmUebDXmqIZIq_11() const { return ___ReIdYCkkTUyMvyPmUebDXmqIZIq_11; }
	inline RuntimeObject** get_address_of_ReIdYCkkTUyMvyPmUebDXmqIZIq_11() { return &___ReIdYCkkTUyMvyPmUebDXmqIZIq_11; }
	inline void set_ReIdYCkkTUyMvyPmUebDXmqIZIq_11(RuntimeObject* value)
	{
		___ReIdYCkkTUyMvyPmUebDXmqIZIq_11 = value;
		Il2CppCodeGenWriteBarrier((&___ReIdYCkkTUyMvyPmUebDXmqIZIq_11), value);
	}

	inline static int32_t get_offset_of_PaFIcIdtgpJEDjPWtuVHsnYMPWHh_12() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___PaFIcIdtgpJEDjPWtuVHsnYMPWHh_12)); }
	inline int32_t get_PaFIcIdtgpJEDjPWtuVHsnYMPWHh_12() const { return ___PaFIcIdtgpJEDjPWtuVHsnYMPWHh_12; }
	inline int32_t* get_address_of_PaFIcIdtgpJEDjPWtuVHsnYMPWHh_12() { return &___PaFIcIdtgpJEDjPWtuVHsnYMPWHh_12; }
	inline void set_PaFIcIdtgpJEDjPWtuVHsnYMPWHh_12(int32_t value)
	{
		___PaFIcIdtgpJEDjPWtuVHsnYMPWHh_12 = value;
	}

	inline static int32_t get_offset_of_XHPgOPOiyVEVTrtGpFVwhUYYVBLH_13() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___XHPgOPOiyVEVTrtGpFVwhUYYVBLH_13)); }
	inline int32_t get_XHPgOPOiyVEVTrtGpFVwhUYYVBLH_13() const { return ___XHPgOPOiyVEVTrtGpFVwhUYYVBLH_13; }
	inline int32_t* get_address_of_XHPgOPOiyVEVTrtGpFVwhUYYVBLH_13() { return &___XHPgOPOiyVEVTrtGpFVwhUYYVBLH_13; }
	inline void set_XHPgOPOiyVEVTrtGpFVwhUYYVBLH_13(int32_t value)
	{
		___XHPgOPOiyVEVTrtGpFVwhUYYVBLH_13 = value;
	}

	inline static int32_t get_offset_of_WErGCEfmgpirqkWeueqfYlfBfKCt_14() { return static_cast<int32_t>(offsetof(TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332, ___WErGCEfmgpirqkWeueqfYlfBfKCt_14)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_WErGCEfmgpirqkWeueqfYlfBfKCt_14() const { return ___WErGCEfmgpirqkWeueqfYlfBfKCt_14; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_WErGCEfmgpirqkWeueqfYlfBfKCt_14() { return &___WErGCEfmgpirqkWeueqfYlfBfKCt_14; }
	inline void set_WErGCEfmgpirqkWeueqfYlfBfKCt_14(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___WErGCEfmgpirqkWeueqfYlfBfKCt_14 = value;
		Il2CppCodeGenWriteBarrier((&___WErGCEfmgpirqkWeueqfYlfBfKCt_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOQRFXHZMSQLZBXCRGWIUBMBMFY_TA830AB0AC8FEAE08289B6EBE55B9B2E619169332_H
#ifndef ZCNVNAZJHOKDNSNBZHMCSAJVFZJ_T11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E_H
#define ZCNVNAZJHOKDNSNBZHMCSAJVFZJ_T11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj
struct  ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::jeLkzDDlxjdyTfidRDslEjdPyAn
	int32_t ___jeLkzDDlxjdyTfidRDslEjdPyAn_4;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::wneHRsRblNFSFcNjImlYgNBNpoh
	int32_t ___wneHRsRblNFSFcNjImlYgNBNpoh_5;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::xJPYTeJZICkFpIWTwbMsXQVEALx
	int32_t ___xJPYTeJZICkFpIWTwbMsXQVEALx_6;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::kilUCbTWeCfCdDqQSHFWpvHLrBl
	int32_t ___kilUCbTWeCfCdDqQSHFWpvHLrBl_7;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_8;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::MIpezzrjtDrOhbQbHuUvLlLITDa
	RuntimeObject* ___MIpezzrjtDrOhbQbHuUvLlLITDa_12;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::DASUOARpziUcAREfuWStVzgjFzp
	int32_t ___DASUOARpziUcAREfuWStVzgjFzp_13;
	// System.Collections.Generic.IList`1<Rewired.ControllerMap> Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::bLbTipzKwUeoEarbqhZQDsQXQzyr
	RuntimeObject* ___bLbTipzKwUeoEarbqhZQDsQXQzyr_14;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::QulqzhoQHxWQqNfCnLabvDfPYQW
	int32_t ___QulqzhoQHxWQqNfCnLabvDfPYQW_15;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::racLOdTkGZjnXYXGPCIWPxcsJFM
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___racLOdTkGZjnXYXGPCIWPxcsJFM_16;
	// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap> Rewired.Player_ControllerHelper_MapHelper_ZCnVNazJHokdnsnBzhmcsaJVfZj::CIPCeRrmpIUwjJWypjSJRrXojYT
	RuntimeObject* ___CIPCeRrmpIUwjJWypjSJRrXojYT_17;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___jeLkzDDlxjdyTfidRDslEjdPyAn_4)); }
	inline int32_t get_jeLkzDDlxjdyTfidRDslEjdPyAn_4() const { return ___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline int32_t* get_address_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return &___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline void set_jeLkzDDlxjdyTfidRDslEjdPyAn_4(int32_t value)
	{
		___jeLkzDDlxjdyTfidRDslEjdPyAn_4 = value;
	}

	inline static int32_t get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___wneHRsRblNFSFcNjImlYgNBNpoh_5)); }
	inline int32_t get_wneHRsRblNFSFcNjImlYgNBNpoh_5() const { return ___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline int32_t* get_address_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return &___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline void set_wneHRsRblNFSFcNjImlYgNBNpoh_5(int32_t value)
	{
		___wneHRsRblNFSFcNjImlYgNBNpoh_5 = value;
	}

	inline static int32_t get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_6() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___xJPYTeJZICkFpIWTwbMsXQVEALx_6)); }
	inline int32_t get_xJPYTeJZICkFpIWTwbMsXQVEALx_6() const { return ___xJPYTeJZICkFpIWTwbMsXQVEALx_6; }
	inline int32_t* get_address_of_xJPYTeJZICkFpIWTwbMsXQVEALx_6() { return &___xJPYTeJZICkFpIWTwbMsXQVEALx_6; }
	inline void set_xJPYTeJZICkFpIWTwbMsXQVEALx_6(int32_t value)
	{
		___xJPYTeJZICkFpIWTwbMsXQVEALx_6 = value;
	}

	inline static int32_t get_offset_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_7() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___kilUCbTWeCfCdDqQSHFWpvHLrBl_7)); }
	inline int32_t get_kilUCbTWeCfCdDqQSHFWpvHLrBl_7() const { return ___kilUCbTWeCfCdDqQSHFWpvHLrBl_7; }
	inline int32_t* get_address_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_7() { return &___kilUCbTWeCfCdDqQSHFWpvHLrBl_7; }
	inline void set_kilUCbTWeCfCdDqQSHFWpvHLrBl_7(int32_t value)
	{
		___kilUCbTWeCfCdDqQSHFWpvHLrBl_7 = value;
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_8() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___pTVJDeVeJlQThAZkFztHqDAWDUa_8)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_8() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_8; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_8() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_8; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_8(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_8 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_10; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_10(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_10 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11 = value;
	}

	inline static int32_t get_offset_of_MIpezzrjtDrOhbQbHuUvLlLITDa_12() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___MIpezzrjtDrOhbQbHuUvLlLITDa_12)); }
	inline RuntimeObject* get_MIpezzrjtDrOhbQbHuUvLlLITDa_12() const { return ___MIpezzrjtDrOhbQbHuUvLlLITDa_12; }
	inline RuntimeObject** get_address_of_MIpezzrjtDrOhbQbHuUvLlLITDa_12() { return &___MIpezzrjtDrOhbQbHuUvLlLITDa_12; }
	inline void set_MIpezzrjtDrOhbQbHuUvLlLITDa_12(RuntimeObject* value)
	{
		___MIpezzrjtDrOhbQbHuUvLlLITDa_12 = value;
		Il2CppCodeGenWriteBarrier((&___MIpezzrjtDrOhbQbHuUvLlLITDa_12), value);
	}

	inline static int32_t get_offset_of_DASUOARpziUcAREfuWStVzgjFzp_13() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___DASUOARpziUcAREfuWStVzgjFzp_13)); }
	inline int32_t get_DASUOARpziUcAREfuWStVzgjFzp_13() const { return ___DASUOARpziUcAREfuWStVzgjFzp_13; }
	inline int32_t* get_address_of_DASUOARpziUcAREfuWStVzgjFzp_13() { return &___DASUOARpziUcAREfuWStVzgjFzp_13; }
	inline void set_DASUOARpziUcAREfuWStVzgjFzp_13(int32_t value)
	{
		___DASUOARpziUcAREfuWStVzgjFzp_13 = value;
	}

	inline static int32_t get_offset_of_bLbTipzKwUeoEarbqhZQDsQXQzyr_14() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___bLbTipzKwUeoEarbqhZQDsQXQzyr_14)); }
	inline RuntimeObject* get_bLbTipzKwUeoEarbqhZQDsQXQzyr_14() const { return ___bLbTipzKwUeoEarbqhZQDsQXQzyr_14; }
	inline RuntimeObject** get_address_of_bLbTipzKwUeoEarbqhZQDsQXQzyr_14() { return &___bLbTipzKwUeoEarbqhZQDsQXQzyr_14; }
	inline void set_bLbTipzKwUeoEarbqhZQDsQXQzyr_14(RuntimeObject* value)
	{
		___bLbTipzKwUeoEarbqhZQDsQXQzyr_14 = value;
		Il2CppCodeGenWriteBarrier((&___bLbTipzKwUeoEarbqhZQDsQXQzyr_14), value);
	}

	inline static int32_t get_offset_of_QulqzhoQHxWQqNfCnLabvDfPYQW_15() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___QulqzhoQHxWQqNfCnLabvDfPYQW_15)); }
	inline int32_t get_QulqzhoQHxWQqNfCnLabvDfPYQW_15() const { return ___QulqzhoQHxWQqNfCnLabvDfPYQW_15; }
	inline int32_t* get_address_of_QulqzhoQHxWQqNfCnLabvDfPYQW_15() { return &___QulqzhoQHxWQqNfCnLabvDfPYQW_15; }
	inline void set_QulqzhoQHxWQqNfCnLabvDfPYQW_15(int32_t value)
	{
		___QulqzhoQHxWQqNfCnLabvDfPYQW_15 = value;
	}

	inline static int32_t get_offset_of_racLOdTkGZjnXYXGPCIWPxcsJFM_16() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___racLOdTkGZjnXYXGPCIWPxcsJFM_16)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_racLOdTkGZjnXYXGPCIWPxcsJFM_16() const { return ___racLOdTkGZjnXYXGPCIWPxcsJFM_16; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_racLOdTkGZjnXYXGPCIWPxcsJFM_16() { return &___racLOdTkGZjnXYXGPCIWPxcsJFM_16; }
	inline void set_racLOdTkGZjnXYXGPCIWPxcsJFM_16(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___racLOdTkGZjnXYXGPCIWPxcsJFM_16 = value;
		Il2CppCodeGenWriteBarrier((&___racLOdTkGZjnXYXGPCIWPxcsJFM_16), value);
	}

	inline static int32_t get_offset_of_CIPCeRrmpIUwjJWypjSJRrXojYT_17() { return static_cast<int32_t>(offsetof(ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E, ___CIPCeRrmpIUwjJWypjSJRrXojYT_17)); }
	inline RuntimeObject* get_CIPCeRrmpIUwjJWypjSJRrXojYT_17() const { return ___CIPCeRrmpIUwjJWypjSJRrXojYT_17; }
	inline RuntimeObject** get_address_of_CIPCeRrmpIUwjJWypjSJRrXojYT_17() { return &___CIPCeRrmpIUwjJWypjSJRrXojYT_17; }
	inline void set_CIPCeRrmpIUwjJWypjSJRrXojYT_17(RuntimeObject* value)
	{
		___CIPCeRrmpIUwjJWypjSJRrXojYT_17 = value;
		Il2CppCodeGenWriteBarrier((&___CIPCeRrmpIUwjJWypjSJRrXojYT_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZCNVNAZJHOKDNSNBZHMCSAJVFZJ_T11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E_H
#ifndef GTHDXUZSMDTRYDMUAQOKLEPUBEXA_T25444AFCC55788312A716F9211CC03E5E4746AC6_H
#define GTHDXUZSMDTRYDMUAQOKLEPUBEXA_T25444AFCC55788312A716F9211CC03E5E4746AC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa
struct  gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6  : public RuntimeObject
{
public:
	// Rewired.ControllerMap Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::jeLkzDDlxjdyTfidRDslEjdPyAn
	int32_t ___jeLkzDDlxjdyTfidRDslEjdPyAn_4;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::wneHRsRblNFSFcNjImlYgNBNpoh
	int32_t ___wneHRsRblNFSFcNjImlYgNBNpoh_5;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::xJPYTeJZICkFpIWTwbMsXQVEALx
	int32_t ___xJPYTeJZICkFpIWTwbMsXQVEALx_6;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::kilUCbTWeCfCdDqQSHFWpvHLrBl
	int32_t ___kilUCbTWeCfCdDqQSHFWpvHLrBl_7;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::OoFswlQbqBhHqGOCooflwYCYmYWF
	int32_t ___OoFswlQbqBhHqGOCooflwYCYmYWF_8;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::pyCASHftKaJjucJBlmeeWQrdKuRk
	int32_t ___pyCASHftKaJjucJBlmeeWQrdKuRk_9;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::KupTlJjwsvuCuogzorfWuijVDiT
	RuntimeObject* ___KupTlJjwsvuCuogzorfWuijVDiT_10;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::xJbmBCZhpIClOCoiaxRsbiCnXHoi
	int32_t ___xJbmBCZhpIClOCoiaxRsbiCnXHoi_11;
	// System.Collections.Generic.IList`1<Rewired.ControllerMap> Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::OQwqNMKQwTkZuhhYgLUhxgJHvLW
	RuntimeObject* ___OQwqNMKQwTkZuhhYgLUhxgJHvLW_12;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_gthDxuZSmdtryDmUAqOKLePUBeXa::BCUgAUbtQmCLlvbkbMEtzlSbnPk
	int32_t ___BCUgAUbtQmCLlvbkbMEtzlSbnPk_13;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___jeLkzDDlxjdyTfidRDslEjdPyAn_4)); }
	inline int32_t get_jeLkzDDlxjdyTfidRDslEjdPyAn_4() const { return ___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline int32_t* get_address_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return &___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline void set_jeLkzDDlxjdyTfidRDslEjdPyAn_4(int32_t value)
	{
		___jeLkzDDlxjdyTfidRDslEjdPyAn_4 = value;
	}

	inline static int32_t get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___wneHRsRblNFSFcNjImlYgNBNpoh_5)); }
	inline int32_t get_wneHRsRblNFSFcNjImlYgNBNpoh_5() const { return ___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline int32_t* get_address_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return &___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline void set_wneHRsRblNFSFcNjImlYgNBNpoh_5(int32_t value)
	{
		___wneHRsRblNFSFcNjImlYgNBNpoh_5 = value;
	}

	inline static int32_t get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_6() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___xJPYTeJZICkFpIWTwbMsXQVEALx_6)); }
	inline int32_t get_xJPYTeJZICkFpIWTwbMsXQVEALx_6() const { return ___xJPYTeJZICkFpIWTwbMsXQVEALx_6; }
	inline int32_t* get_address_of_xJPYTeJZICkFpIWTwbMsXQVEALx_6() { return &___xJPYTeJZICkFpIWTwbMsXQVEALx_6; }
	inline void set_xJPYTeJZICkFpIWTwbMsXQVEALx_6(int32_t value)
	{
		___xJPYTeJZICkFpIWTwbMsXQVEALx_6 = value;
	}

	inline static int32_t get_offset_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_7() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___kilUCbTWeCfCdDqQSHFWpvHLrBl_7)); }
	inline int32_t get_kilUCbTWeCfCdDqQSHFWpvHLrBl_7() const { return ___kilUCbTWeCfCdDqQSHFWpvHLrBl_7; }
	inline int32_t* get_address_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_7() { return &___kilUCbTWeCfCdDqQSHFWpvHLrBl_7; }
	inline void set_kilUCbTWeCfCdDqQSHFWpvHLrBl_7(int32_t value)
	{
		___kilUCbTWeCfCdDqQSHFWpvHLrBl_7 = value;
	}

	inline static int32_t get_offset_of_OoFswlQbqBhHqGOCooflwYCYmYWF_8() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___OoFswlQbqBhHqGOCooflwYCYmYWF_8)); }
	inline int32_t get_OoFswlQbqBhHqGOCooflwYCYmYWF_8() const { return ___OoFswlQbqBhHqGOCooflwYCYmYWF_8; }
	inline int32_t* get_address_of_OoFswlQbqBhHqGOCooflwYCYmYWF_8() { return &___OoFswlQbqBhHqGOCooflwYCYmYWF_8; }
	inline void set_OoFswlQbqBhHqGOCooflwYCYmYWF_8(int32_t value)
	{
		___OoFswlQbqBhHqGOCooflwYCYmYWF_8 = value;
	}

	inline static int32_t get_offset_of_pyCASHftKaJjucJBlmeeWQrdKuRk_9() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___pyCASHftKaJjucJBlmeeWQrdKuRk_9)); }
	inline int32_t get_pyCASHftKaJjucJBlmeeWQrdKuRk_9() const { return ___pyCASHftKaJjucJBlmeeWQrdKuRk_9; }
	inline int32_t* get_address_of_pyCASHftKaJjucJBlmeeWQrdKuRk_9() { return &___pyCASHftKaJjucJBlmeeWQrdKuRk_9; }
	inline void set_pyCASHftKaJjucJBlmeeWQrdKuRk_9(int32_t value)
	{
		___pyCASHftKaJjucJBlmeeWQrdKuRk_9 = value;
	}

	inline static int32_t get_offset_of_KupTlJjwsvuCuogzorfWuijVDiT_10() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___KupTlJjwsvuCuogzorfWuijVDiT_10)); }
	inline RuntimeObject* get_KupTlJjwsvuCuogzorfWuijVDiT_10() const { return ___KupTlJjwsvuCuogzorfWuijVDiT_10; }
	inline RuntimeObject** get_address_of_KupTlJjwsvuCuogzorfWuijVDiT_10() { return &___KupTlJjwsvuCuogzorfWuijVDiT_10; }
	inline void set_KupTlJjwsvuCuogzorfWuijVDiT_10(RuntimeObject* value)
	{
		___KupTlJjwsvuCuogzorfWuijVDiT_10 = value;
		Il2CppCodeGenWriteBarrier((&___KupTlJjwsvuCuogzorfWuijVDiT_10), value);
	}

	inline static int32_t get_offset_of_xJbmBCZhpIClOCoiaxRsbiCnXHoi_11() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___xJbmBCZhpIClOCoiaxRsbiCnXHoi_11)); }
	inline int32_t get_xJbmBCZhpIClOCoiaxRsbiCnXHoi_11() const { return ___xJbmBCZhpIClOCoiaxRsbiCnXHoi_11; }
	inline int32_t* get_address_of_xJbmBCZhpIClOCoiaxRsbiCnXHoi_11() { return &___xJbmBCZhpIClOCoiaxRsbiCnXHoi_11; }
	inline void set_xJbmBCZhpIClOCoiaxRsbiCnXHoi_11(int32_t value)
	{
		___xJbmBCZhpIClOCoiaxRsbiCnXHoi_11 = value;
	}

	inline static int32_t get_offset_of_OQwqNMKQwTkZuhhYgLUhxgJHvLW_12() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___OQwqNMKQwTkZuhhYgLUhxgJHvLW_12)); }
	inline RuntimeObject* get_OQwqNMKQwTkZuhhYgLUhxgJHvLW_12() const { return ___OQwqNMKQwTkZuhhYgLUhxgJHvLW_12; }
	inline RuntimeObject** get_address_of_OQwqNMKQwTkZuhhYgLUhxgJHvLW_12() { return &___OQwqNMKQwTkZuhhYgLUhxgJHvLW_12; }
	inline void set_OQwqNMKQwTkZuhhYgLUhxgJHvLW_12(RuntimeObject* value)
	{
		___OQwqNMKQwTkZuhhYgLUhxgJHvLW_12 = value;
		Il2CppCodeGenWriteBarrier((&___OQwqNMKQwTkZuhhYgLUhxgJHvLW_12), value);
	}

	inline static int32_t get_offset_of_BCUgAUbtQmCLlvbkbMEtzlSbnPk_13() { return static_cast<int32_t>(offsetof(gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6, ___BCUgAUbtQmCLlvbkbMEtzlSbnPk_13)); }
	inline int32_t get_BCUgAUbtQmCLlvbkbMEtzlSbnPk_13() const { return ___BCUgAUbtQmCLlvbkbMEtzlSbnPk_13; }
	inline int32_t* get_address_of_BCUgAUbtQmCLlvbkbMEtzlSbnPk_13() { return &___BCUgAUbtQmCLlvbkbMEtzlSbnPk_13; }
	inline void set_BCUgAUbtQmCLlvbkbMEtzlSbnPk_13(int32_t value)
	{
		___BCUgAUbtQmCLlvbkbMEtzlSbnPk_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GTHDXUZSMDTRYDMUAQOKLEPUBEXA_T25444AFCC55788312A716F9211CC03E5E4746AC6_H
#ifndef MNTFSKAMNQIMCLALZMMRGTAUFDB_T3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB_H
#define MNTFSKAMNQIMCLALZMMRGTAUFDB_T3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB
struct  mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB  : public RuntimeObject
{
public:
	// Rewired.ControllerMap Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB::jeLkzDDlxjdyTfidRDslEjdPyAn
	int32_t ___jeLkzDDlxjdyTfidRDslEjdPyAn_4;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB::wneHRsRblNFSFcNjImlYgNBNpoh
	int32_t ___wneHRsRblNFSFcNjImlYgNBNpoh_5;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB::SSdCMeFQuDZMiegrrjajcgDuZcvG
	RuntimeObject* ___SSdCMeFQuDZMiegrrjajcgDuZcvG_6;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB::aRokAlWvWdWjmNbtalyDWzCQaKE
	int32_t ___aRokAlWvWdWjmNbtalyDWzCQaKE_7;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB::tnzQdzfTtfgncibXTcLnqGJmJen
	int32_t ___tnzQdzfTtfgncibXTcLnqGJmJen_8;
	// JrKetNtIJINuYILTEYSwNUvgPiW Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB::GIXjFsWXRqPddpiHqJOLeiJNGwPH
	RuntimeObject* ___GIXjFsWXRqPddpiHqJOLeiJNGwPH_9;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB::XHHoEvWwsnpoTyNeLTlGDgMedCv
	int32_t ___XHHoEvWwsnpoTyNeLTlGDgMedCv_10;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_mNtFskAmnQiMclAlZmMRgtAUFdB::PJmuMUYjdZfCRrgLweWGDFfeSWeB
	int32_t ___PJmuMUYjdZfCRrgLweWGDFfeSWeB_11;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerMap_t19BEF397BA067C157A2EA9878680F85EE15085CB * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return static_cast<int32_t>(offsetof(mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB, ___jeLkzDDlxjdyTfidRDslEjdPyAn_4)); }
	inline int32_t get_jeLkzDDlxjdyTfidRDslEjdPyAn_4() const { return ___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline int32_t* get_address_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return &___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline void set_jeLkzDDlxjdyTfidRDslEjdPyAn_4(int32_t value)
	{
		___jeLkzDDlxjdyTfidRDslEjdPyAn_4 = value;
	}

	inline static int32_t get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return static_cast<int32_t>(offsetof(mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB, ___wneHRsRblNFSFcNjImlYgNBNpoh_5)); }
	inline int32_t get_wneHRsRblNFSFcNjImlYgNBNpoh_5() const { return ___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline int32_t* get_address_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return &___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline void set_wneHRsRblNFSFcNjImlYgNBNpoh_5(int32_t value)
	{
		___wneHRsRblNFSFcNjImlYgNBNpoh_5 = value;
	}

	inline static int32_t get_offset_of_SSdCMeFQuDZMiegrrjajcgDuZcvG_6() { return static_cast<int32_t>(offsetof(mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB, ___SSdCMeFQuDZMiegrrjajcgDuZcvG_6)); }
	inline RuntimeObject* get_SSdCMeFQuDZMiegrrjajcgDuZcvG_6() const { return ___SSdCMeFQuDZMiegrrjajcgDuZcvG_6; }
	inline RuntimeObject** get_address_of_SSdCMeFQuDZMiegrrjajcgDuZcvG_6() { return &___SSdCMeFQuDZMiegrrjajcgDuZcvG_6; }
	inline void set_SSdCMeFQuDZMiegrrjajcgDuZcvG_6(RuntimeObject* value)
	{
		___SSdCMeFQuDZMiegrrjajcgDuZcvG_6 = value;
		Il2CppCodeGenWriteBarrier((&___SSdCMeFQuDZMiegrrjajcgDuZcvG_6), value);
	}

	inline static int32_t get_offset_of_aRokAlWvWdWjmNbtalyDWzCQaKE_7() { return static_cast<int32_t>(offsetof(mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB, ___aRokAlWvWdWjmNbtalyDWzCQaKE_7)); }
	inline int32_t get_aRokAlWvWdWjmNbtalyDWzCQaKE_7() const { return ___aRokAlWvWdWjmNbtalyDWzCQaKE_7; }
	inline int32_t* get_address_of_aRokAlWvWdWjmNbtalyDWzCQaKE_7() { return &___aRokAlWvWdWjmNbtalyDWzCQaKE_7; }
	inline void set_aRokAlWvWdWjmNbtalyDWzCQaKE_7(int32_t value)
	{
		___aRokAlWvWdWjmNbtalyDWzCQaKE_7 = value;
	}

	inline static int32_t get_offset_of_tnzQdzfTtfgncibXTcLnqGJmJen_8() { return static_cast<int32_t>(offsetof(mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB, ___tnzQdzfTtfgncibXTcLnqGJmJen_8)); }
	inline int32_t get_tnzQdzfTtfgncibXTcLnqGJmJen_8() const { return ___tnzQdzfTtfgncibXTcLnqGJmJen_8; }
	inline int32_t* get_address_of_tnzQdzfTtfgncibXTcLnqGJmJen_8() { return &___tnzQdzfTtfgncibXTcLnqGJmJen_8; }
	inline void set_tnzQdzfTtfgncibXTcLnqGJmJen_8(int32_t value)
	{
		___tnzQdzfTtfgncibXTcLnqGJmJen_8 = value;
	}

	inline static int32_t get_offset_of_GIXjFsWXRqPddpiHqJOLeiJNGwPH_9() { return static_cast<int32_t>(offsetof(mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB, ___GIXjFsWXRqPddpiHqJOLeiJNGwPH_9)); }
	inline RuntimeObject* get_GIXjFsWXRqPddpiHqJOLeiJNGwPH_9() const { return ___GIXjFsWXRqPddpiHqJOLeiJNGwPH_9; }
	inline RuntimeObject** get_address_of_GIXjFsWXRqPddpiHqJOLeiJNGwPH_9() { return &___GIXjFsWXRqPddpiHqJOLeiJNGwPH_9; }
	inline void set_GIXjFsWXRqPddpiHqJOLeiJNGwPH_9(RuntimeObject* value)
	{
		___GIXjFsWXRqPddpiHqJOLeiJNGwPH_9 = value;
		Il2CppCodeGenWriteBarrier((&___GIXjFsWXRqPddpiHqJOLeiJNGwPH_9), value);
	}

	inline static int32_t get_offset_of_XHHoEvWwsnpoTyNeLTlGDgMedCv_10() { return static_cast<int32_t>(offsetof(mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB, ___XHHoEvWwsnpoTyNeLTlGDgMedCv_10)); }
	inline int32_t get_XHHoEvWwsnpoTyNeLTlGDgMedCv_10() const { return ___XHHoEvWwsnpoTyNeLTlGDgMedCv_10; }
	inline int32_t* get_address_of_XHHoEvWwsnpoTyNeLTlGDgMedCv_10() { return &___XHHoEvWwsnpoTyNeLTlGDgMedCv_10; }
	inline void set_XHHoEvWwsnpoTyNeLTlGDgMedCv_10(int32_t value)
	{
		___XHHoEvWwsnpoTyNeLTlGDgMedCv_10 = value;
	}

	inline static int32_t get_offset_of_PJmuMUYjdZfCRrgLweWGDFfeSWeB_11() { return static_cast<int32_t>(offsetof(mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB, ___PJmuMUYjdZfCRrgLweWGDFfeSWeB_11)); }
	inline int32_t get_PJmuMUYjdZfCRrgLweWGDFfeSWeB_11() const { return ___PJmuMUYjdZfCRrgLweWGDFfeSWeB_11; }
	inline int32_t* get_address_of_PJmuMUYjdZfCRrgLweWGDFfeSWeB_11() { return &___PJmuMUYjdZfCRrgLweWGDFfeSWeB_11; }
	inline void set_PJmuMUYjdZfCRrgLweWGDFfeSWeB_11(int32_t value)
	{
		___PJmuMUYjdZfCRrgLweWGDFfeSWeB_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MNTFSKAMNQIMCLALZMMRGTAUFDB_T3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB_H
#ifndef RVHHCHDAFNIPTBGONCDTDLPPNPZA_T45828CE0019C9DFC0A9BAF674A66B02E58F5202B_H
#define RVHHCHDAFNIPTBGONCDTDLPPNPZA_T45828CE0019C9DFC0A9BAF674A66B02E58F5202B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA
struct  rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B  : public RuntimeObject
{
public:
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::NOArrsNdfKDShNFftfbPqYDzhpq
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_MapHelper Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::DKaFTWTMhFeLQHDExnDMRxZfmROL
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::jeLkzDDlxjdyTfidRDslEjdPyAn
	int32_t ___jeLkzDDlxjdyTfidRDslEjdPyAn_4;
	// Rewired.ControllerType Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::wneHRsRblNFSFcNjImlYgNBNpoh
	int32_t ___wneHRsRblNFSFcNjImlYgNBNpoh_5;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::xJPYTeJZICkFpIWTwbMsXQVEALx
	int32_t ___xJPYTeJZICkFpIWTwbMsXQVEALx_6;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::kilUCbTWeCfCdDqQSHFWpvHLrBl
	int32_t ___kilUCbTWeCfCdDqQSHFWpvHLrBl_7;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::pTVJDeVeJlQThAZkFztHqDAWDUa
	int32_t ___pTVJDeVeJlQThAZkFztHqDAWDUa_8;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::jaqfFCICEjyQtCeRrGzrkTCNIcKx
	int32_t ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10;
	// System.Boolean Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11;
	// Rewired.Player_ControllerHelper_LBdawFihcNUlinDyhLfBKVmmxSHI Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::bixDTdhhWGRJCMtLFyBPbvYGXTT
	RuntimeObject* ___bixDTdhhWGRJCMtLFyBPbvYGXTT_12;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::pSrucIRCjCmJLnkfTWqDplqRPeh
	int32_t ___pSrucIRCjCmJLnkfTWqDplqRPeh_13;
	// System.Collections.Generic.IList`1<Rewired.ControllerMap> Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::HBPdylGmJOYFklrOiPYBaUpIOmN
	RuntimeObject* ___HBPdylGmJOYFklrOiPYBaUpIOmN_14;
	// System.Int32 Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::YeijNFyiBhBFcBBRlXccsNEEnZam
	int32_t ___YeijNFyiBhBFcBBRlXccsNEEnZam_15;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::lGVfuIaotuaJxIknLjgArOnxwKg
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___lGVfuIaotuaJxIknLjgArOnxwKg_16;
	// System.Collections.Generic.IEnumerator`1<Rewired.ActionElementMap> Rewired.Player_ControllerHelper_MapHelper_rvHhChdAfNiPTbGOnCDtdlPPnPZA::gxzAjugGiCLhjGlKynPliScLgwl
	RuntimeObject* ___gxzAjugGiCLhjGlKynPliScLgwl_17;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
		Il2CppCodeGenWriteBarrier((&___NOArrsNdfKDShNFftfbPqYDzhpq_0), value);
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___jeLkzDDlxjdyTfidRDslEjdPyAn_4)); }
	inline int32_t get_jeLkzDDlxjdyTfidRDslEjdPyAn_4() const { return ___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline int32_t* get_address_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4() { return &___jeLkzDDlxjdyTfidRDslEjdPyAn_4; }
	inline void set_jeLkzDDlxjdyTfidRDslEjdPyAn_4(int32_t value)
	{
		___jeLkzDDlxjdyTfidRDslEjdPyAn_4 = value;
	}

	inline static int32_t get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___wneHRsRblNFSFcNjImlYgNBNpoh_5)); }
	inline int32_t get_wneHRsRblNFSFcNjImlYgNBNpoh_5() const { return ___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline int32_t* get_address_of_wneHRsRblNFSFcNjImlYgNBNpoh_5() { return &___wneHRsRblNFSFcNjImlYgNBNpoh_5; }
	inline void set_wneHRsRblNFSFcNjImlYgNBNpoh_5(int32_t value)
	{
		___wneHRsRblNFSFcNjImlYgNBNpoh_5 = value;
	}

	inline static int32_t get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_6() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___xJPYTeJZICkFpIWTwbMsXQVEALx_6)); }
	inline int32_t get_xJPYTeJZICkFpIWTwbMsXQVEALx_6() const { return ___xJPYTeJZICkFpIWTwbMsXQVEALx_6; }
	inline int32_t* get_address_of_xJPYTeJZICkFpIWTwbMsXQVEALx_6() { return &___xJPYTeJZICkFpIWTwbMsXQVEALx_6; }
	inline void set_xJPYTeJZICkFpIWTwbMsXQVEALx_6(int32_t value)
	{
		___xJPYTeJZICkFpIWTwbMsXQVEALx_6 = value;
	}

	inline static int32_t get_offset_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_7() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___kilUCbTWeCfCdDqQSHFWpvHLrBl_7)); }
	inline int32_t get_kilUCbTWeCfCdDqQSHFWpvHLrBl_7() const { return ___kilUCbTWeCfCdDqQSHFWpvHLrBl_7; }
	inline int32_t* get_address_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_7() { return &___kilUCbTWeCfCdDqQSHFWpvHLrBl_7; }
	inline void set_kilUCbTWeCfCdDqQSHFWpvHLrBl_7(int32_t value)
	{
		___kilUCbTWeCfCdDqQSHFWpvHLrBl_7 = value;
	}

	inline static int32_t get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_8() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___pTVJDeVeJlQThAZkFztHqDAWDUa_8)); }
	inline int32_t get_pTVJDeVeJlQThAZkFztHqDAWDUa_8() const { return ___pTVJDeVeJlQThAZkFztHqDAWDUa_8; }
	inline int32_t* get_address_of_pTVJDeVeJlQThAZkFztHqDAWDUa_8() { return &___pTVJDeVeJlQThAZkFztHqDAWDUa_8; }
	inline void set_pTVJDeVeJlQThAZkFztHqDAWDUa_8(int32_t value)
	{
		___pTVJDeVeJlQThAZkFztHqDAWDUa_8 = value;
	}

	inline static int32_t get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9)); }
	inline int32_t get_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9() const { return ___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9; }
	inline int32_t* get_address_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9() { return &___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9; }
	inline void set_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9(int32_t value)
	{
		___jaqfFCICEjyQtCeRrGzrkTCNIcKx_9 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_10; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_10(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_10 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11 = value;
	}

	inline static int32_t get_offset_of_bixDTdhhWGRJCMtLFyBPbvYGXTT_12() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___bixDTdhhWGRJCMtLFyBPbvYGXTT_12)); }
	inline RuntimeObject* get_bixDTdhhWGRJCMtLFyBPbvYGXTT_12() const { return ___bixDTdhhWGRJCMtLFyBPbvYGXTT_12; }
	inline RuntimeObject** get_address_of_bixDTdhhWGRJCMtLFyBPbvYGXTT_12() { return &___bixDTdhhWGRJCMtLFyBPbvYGXTT_12; }
	inline void set_bixDTdhhWGRJCMtLFyBPbvYGXTT_12(RuntimeObject* value)
	{
		___bixDTdhhWGRJCMtLFyBPbvYGXTT_12 = value;
		Il2CppCodeGenWriteBarrier((&___bixDTdhhWGRJCMtLFyBPbvYGXTT_12), value);
	}

	inline static int32_t get_offset_of_pSrucIRCjCmJLnkfTWqDplqRPeh_13() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___pSrucIRCjCmJLnkfTWqDplqRPeh_13)); }
	inline int32_t get_pSrucIRCjCmJLnkfTWqDplqRPeh_13() const { return ___pSrucIRCjCmJLnkfTWqDplqRPeh_13; }
	inline int32_t* get_address_of_pSrucIRCjCmJLnkfTWqDplqRPeh_13() { return &___pSrucIRCjCmJLnkfTWqDplqRPeh_13; }
	inline void set_pSrucIRCjCmJLnkfTWqDplqRPeh_13(int32_t value)
	{
		___pSrucIRCjCmJLnkfTWqDplqRPeh_13 = value;
	}

	inline static int32_t get_offset_of_HBPdylGmJOYFklrOiPYBaUpIOmN_14() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___HBPdylGmJOYFklrOiPYBaUpIOmN_14)); }
	inline RuntimeObject* get_HBPdylGmJOYFklrOiPYBaUpIOmN_14() const { return ___HBPdylGmJOYFklrOiPYBaUpIOmN_14; }
	inline RuntimeObject** get_address_of_HBPdylGmJOYFklrOiPYBaUpIOmN_14() { return &___HBPdylGmJOYFklrOiPYBaUpIOmN_14; }
	inline void set_HBPdylGmJOYFklrOiPYBaUpIOmN_14(RuntimeObject* value)
	{
		___HBPdylGmJOYFklrOiPYBaUpIOmN_14 = value;
		Il2CppCodeGenWriteBarrier((&___HBPdylGmJOYFklrOiPYBaUpIOmN_14), value);
	}

	inline static int32_t get_offset_of_YeijNFyiBhBFcBBRlXccsNEEnZam_15() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___YeijNFyiBhBFcBBRlXccsNEEnZam_15)); }
	inline int32_t get_YeijNFyiBhBFcBBRlXccsNEEnZam_15() const { return ___YeijNFyiBhBFcBBRlXccsNEEnZam_15; }
	inline int32_t* get_address_of_YeijNFyiBhBFcBBRlXccsNEEnZam_15() { return &___YeijNFyiBhBFcBBRlXccsNEEnZam_15; }
	inline void set_YeijNFyiBhBFcBBRlXccsNEEnZam_15(int32_t value)
	{
		___YeijNFyiBhBFcBBRlXccsNEEnZam_15 = value;
	}

	inline static int32_t get_offset_of_lGVfuIaotuaJxIknLjgArOnxwKg_16() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___lGVfuIaotuaJxIknLjgArOnxwKg_16)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_lGVfuIaotuaJxIknLjgArOnxwKg_16() const { return ___lGVfuIaotuaJxIknLjgArOnxwKg_16; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_lGVfuIaotuaJxIknLjgArOnxwKg_16() { return &___lGVfuIaotuaJxIknLjgArOnxwKg_16; }
	inline void set_lGVfuIaotuaJxIknLjgArOnxwKg_16(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___lGVfuIaotuaJxIknLjgArOnxwKg_16 = value;
		Il2CppCodeGenWriteBarrier((&___lGVfuIaotuaJxIknLjgArOnxwKg_16), value);
	}

	inline static int32_t get_offset_of_gxzAjugGiCLhjGlKynPliScLgwl_17() { return static_cast<int32_t>(offsetof(rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B, ___gxzAjugGiCLhjGlKynPliScLgwl_17)); }
	inline RuntimeObject* get_gxzAjugGiCLhjGlKynPliScLgwl_17() const { return ___gxzAjugGiCLhjGlKynPliScLgwl_17; }
	inline RuntimeObject** get_address_of_gxzAjugGiCLhjGlKynPliScLgwl_17() { return &___gxzAjugGiCLhjGlKynPliScLgwl_17; }
	inline void set_gxzAjugGiCLhjGlKynPliScLgwl_17(RuntimeObject* value)
	{
		___gxzAjugGiCLhjGlKynPliScLgwl_17 = value;
		Il2CppCodeGenWriteBarrier((&___gxzAjugGiCLhjGlKynPliScLgwl_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RVHHCHDAFNIPTBGONCDTDLPPNPZA_T45828CE0019C9DFC0A9BAF674A66B02E58F5202B_H
#ifndef REINPUT_T660C845029E93C5E11722938F53BCA3AED33D0BE_H
#define REINPUT_T660C845029E93C5E11722938F53BCA3AED33D0BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput
struct  ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE  : public RuntimeObject
{
public:

public:
};

struct ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields
{
public:
	// Rewired.InputManager_Base Rewired.ReInput::BmbBIGhcPaQcJtPaDIuasDCjqQNz
	InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * ___BmbBIGhcPaQcJtPaDIuasDCjqQNz_7;
	// Rewired.PlatformInputManager Rewired.ReInput::OnpAheqwnSbTCzETUFrLEHROgfTo
	PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A * ___OnpAheqwnSbTCzETUFrLEHROgfTo_8;
	// nkGLVKXmQVVuJNEogVVmAwwQOJl Rewired.ReInput::IEqlUZsiLAwwHwjaJiWawGshrHQ
	nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22 * ___IEqlUZsiLAwwHwjaJiWawGshrHQ_9;
	// lUbzJtkApcIkohMrHhJAmcDImRRR Rewired.ReInput::RVkFnTmTOxSxyomVVDMNYyuaQAg
	lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE * ___RVkFnTmTOxSxyomVVDMNYyuaQAg_10;
	// LkmsImmStVEvsBvZKxyBsAPXXJ Rewired.ReInput::fsKUnkudBKaclGoZoLGqLplnpcq
	LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA * ___fsKUnkudBKaclGoZoLGqLplnpcq_11;
	// Rewired.Data.ControllerDataFiles Rewired.ReInput::jkjiciUYDTwUaFkcGqBpczqirBH
	ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5 * ___jkjiciUYDTwUaFkcGqBpczqirBH_12;
	// Rewired.Data.UserData Rewired.ReInput::kuvduxCaGZogunQJJYFMqUHXuo
	UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * ___kuvduxCaGZogunQJJYFMqUHXuo_13;
	// System.Boolean Rewired.ReInput::oGbqxjxXGgvgXNLPUMbdLzEDsmT
	bool ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_14;
	// Rewired.Data.ConfigVars Rewired.ReInput::AEpGUwNwMAsWjLqSLSkygEagFsW
	ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * ___AEpGUwNwMAsWjLqSLSkygEagFsW_15;
	// Rewired.UpdateLoopType Rewired.ReInput::ZfYcJdvFIDCHAofeyUpUZGLeLMy
	int32_t ___ZfYcJdvFIDCHAofeyUpUZGLeLMy_16;
	// System.Boolean Rewired.ReInput::efatONDtZGNgmLOHvbVBLBBuace
	bool ___efatONDtZGNgmLOHvbVBLBBuace_17;
	// Rewired.Platforms.Platform Rewired.ReInput::ebxMcikMAQQZUToudUFSEUFYfeA
	int32_t ___ebxMcikMAQQZUToudUFSEUFYfeA_18;
	// Rewired.Platforms.WebplayerPlatform Rewired.ReInput::WLJLSQeGpPKUuKjBWrWTbaFnDry
	int32_t ___WLJLSQeGpPKUuKjBWrWTbaFnDry_19;
	// Rewired.Platforms.EditorPlatform Rewired.ReInput::yUCTrJSSAVMiycEtyjhErnsZbYwh
	int32_t ___yUCTrJSSAVMiycEtyjhErnsZbYwh_20;
	// System.Boolean Rewired.ReInput::sqyjgtZgshnxlArNKRhZoKPXcspD
	bool ___sqyjgtZgshnxlArNKRhZoKPXcspD_21;
	// Rewired.Utils.Classes.Utility.TimerAbs Rewired.ReInput::RBQeQNGYzPPywsMagaNKPxDkXre
	TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * ___RBQeQNGYzPPywsMagaNKPxDkXre_22;
	// Rewired.ReInput_pqJryspmPjvziIiviMzAyYQWLVF Rewired.ReInput::QFntQDmTgBlbbKQmkWjoaxmXfDr
	pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6 * ___QFntQDmTgBlbbKQmkWjoaxmXfDr_23;
	// System.String Rewired.ReInput::qJWGQSjuXpqXJHLVBEZbgltbRhA
	String_t* ___qJWGQSjuXpqXJHLVBEZbgltbRhA_24;
	// System.Boolean Rewired.ReInput::nFsjgQIYeASozAWVpsmKWYrynGMt
	bool ___nFsjgQIYeASozAWVpsmKWYrynGMt_25;
	// System.Boolean Rewired.ReInput::IxkxAZDFhYdDnAQiwkHxbCHkabao
	bool ___IxkxAZDFhYdDnAQiwkHxbCHkabao_26;
	// System.Boolean Rewired.ReInput::kVozOJjgIMPdyuEiTtvdCQDrpVk
	bool ___kVozOJjgIMPdyuEiTtvdCQDrpVk_27;
	// System.Int32 Rewired.ReInput::QzLGqBuiJUSnYPDYiBGmAYvPcUAw
	int32_t ___QzLGqBuiJUSnYPDYiBGmAYvPcUAw_28;
	// System.Int32 Rewired.ReInput::_id
	int32_t ____id_29;
	// System.Int32 Rewired.ReInput::uYPBIvZJZOvpItwzdZbRubjNHBQ
	int32_t ___uYPBIvZJZOvpItwzdZbRubjNHBQ_30;
	// System.Int32 Rewired.ReInput::EDBzxspLLLJMCVwEjgVqreXxWCw
	int32_t ___EDBzxspLLLJMCVwEjgVqreXxWCw_31;
	// System.Boolean Rewired.ReInput::FidPeDgESHAdzaeFXcrFbXGDXZpE
	bool ___FidPeDgESHAdzaeFXcrFbXGDXZpE_32;
	// Rewired.ReInput_UnityTouch Rewired.ReInput::cuHMjPjtOdcyqFzxRQNlFdKcQrI
	UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9 * ___cuHMjPjtOdcyqFzxRQNlFdKcQrI_33;
	// Rewired.ReInput_PlayerHelper Rewired.ReInput::IcomarPgDQOtvFQAqkQIQWVwlZf
	PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412 * ___IcomarPgDQOtvFQAqkQIQWVwlZf_34;
	// Rewired.ReInput_ControllerHelper Rewired.ReInput::USvTrWNQsztrTDaJRlzzYsugcyN
	ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62 * ___USvTrWNQsztrTDaJRlzzYsugcyN_35;
	// Rewired.ReInput_MappingHelper Rewired.ReInput::cZhgkwtQTFgoqgpMoqypewKgXXZA
	MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * ___cZhgkwtQTFgoqgpMoqypewKgXXZA_36;
	// Rewired.ReInput_TimeHelper Rewired.ReInput::qBplpkUglPNgqwzfqaaAbLOGttB
	TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B * ___qBplpkUglPNgqwzfqaaAbLOGttB_37;
	// Rewired.ReInput_ConfigHelper Rewired.ReInput::ncvdGtGSDIImpbUXOKhmHLFdPjbR
	ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5 * ___ncvdGtGSDIImpbUXOKhmHLFdPjbR_38;
	// EaCqncmrbxTySfotaFWfqDQVLIS Rewired.ReInput::gqSFnpTCFawuMMNVGgKKIsYZUXL
	EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0 * ___gqSFnpTCFawuMMNVGgKKIsYZUXL_39;
	// Rewired.Data.UserDataStore Rewired.ReInput::TojkjqMwwMdhKJGKNsXcfEQqdqk
	UserDataStore_tA04E79BDF387346606C6F4379B4FD0935BA176C1 * ___TojkjqMwwMdhKJGKNsXcfEQqdqk_40;
	// Rewired.Interfaces.IControllerAssigner Rewired.ReInput::OdmAAKBtcbRSgXiuxOTrhpIzlofQ
	RuntimeObject* ___OdmAAKBtcbRSgXiuxOTrhpIzlofQ_41;
	// Rewired.ReInput_MQKnbNqPnvBXaFjURWimREWJePx Rewired.ReInput::ABhAFxjUUVHIXxvYBKqRrBnPSGY
	MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E * ___ABhAFxjUUVHIXxvYBKqRrBnPSGY_42;
	// Rewired.Utils.SafeAction`1<Rewired.ControllerStatusChangedEventArgs> Rewired.ReInput::wAGfrKtlxRtjFeYStOdjEyhnkFW
	SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900 * ___wAGfrKtlxRtjFeYStOdjEyhnkFW_43;
	// Rewired.Utils.SafeAction`1<Rewired.ControllerStatusChangedEventArgs> Rewired.ReInput::XKQeRyjlqgLlnGtmsVoUhRfEeEu
	SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900 * ___XKQeRyjlqgLlnGtmsVoUhRfEeEu_44;
	// Rewired.Utils.SafeAction`1<Rewired.ControllerStatusChangedEventArgs> Rewired.ReInput::HGNegOULkehGQCIplmMVqBQjWvm
	SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900 * ___HGNegOULkehGQCIplmMVqBQjWvm_45;
	// Rewired.Utils.SafeAction Rewired.ReInput::VCOpkyvHUubmUcZWHeKYunuBQXa
	SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * ___VCOpkyvHUubmUcZWHeKYunuBQXa_46;
	// Rewired.Utils.SafeAction Rewired.ReInput::BuGUijCbDBaDRmwDRglZiFsDvpqe
	SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * ___BuGUijCbDBaDRmwDRglZiFsDvpqe_47;
	// Rewired.Utils.SafeAction Rewired.ReInput::NkZtgCLxdOallQBmQLJnhwLpvNR
	SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * ___NkZtgCLxdOallQBmQLJnhwLpvNR_48;
	// Rewired.Utils.SafeAction Rewired.ReInput::LEeKGKpsAldSIdJikAAjfwxSeje
	SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * ___LEeKGKpsAldSIdJikAAjfwxSeje_49;
	// Rewired.Utils.SafeAction Rewired.ReInput::vuOZQDAdWNOZNtLPcsGWbHRvFwF
	SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * ___vuOZQDAdWNOZNtLPcsGWbHRvFwF_50;
	// System.Action`1<System.Boolean> Rewired.ReInput::_ApplicationFocusChangedEvent
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ____ApplicationFocusChangedEvent_51;
	// System.Action Rewired.ReInput::SjPvewVCAcAkUrlaNILvKkUJkKyE
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___SjPvewVCAcAkUrlaNILvKkUJkKyE_52;
	// System.Action`1<Rewired.UpdateLoopType> Rewired.ReInput::JrUdKzkIBWnUrmLThTqrbZhGkNnq
	Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899 * ___JrUdKzkIBWnUrmLThTqrbZhGkNnq_53;
	// System.Action`1<Rewired.UpdateLoopType> Rewired.ReInput::NYEbfVtXwPGnqWryNGfgyhMndBE
	Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899 * ___NYEbfVtXwPGnqWryNGfgyhMndBE_54;
	// System.Action`1<Rewired.UpdateLoopType> Rewired.ReInput::RKICUSyOXlFZjskVbqFIuVGlJls
	Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899 * ___RKICUSyOXlFZjskVbqFIuVGlJls_55;
	// System.Action Rewired.ReInput::vasXwzLRByGLnTqYebmSZAOcNcG
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___vasXwzLRByGLnTqYebmSZAOcNcG_56;
	// System.Action`1<System.Boolean> Rewired.ReInput::mqfPVmcbmLNZudOfQOcbRCCAgxYf
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___mqfPVmcbmLNZudOfQOcbRCCAgxYf_57;
	// System.Action`1<System.Boolean> Rewired.ReInput::PEZTdLWPjWbAMCHYtsIZabliquLe
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___PEZTdLWPjWbAMCHYtsIZabliquLe_58;
	// System.Action`1<System.Boolean> Rewired.ReInput::UzVqkwqMmsnMVrmgTvaiPTZgjzF
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___UzVqkwqMmsnMVrmgTvaiPTZgjzF_59;
	// System.Action`1<UnityEngine.FullScreenMode> Rewired.ReInput::UnZeCgFPYKrcOECMvOoUKxJBUBt
	Action_1_t4F62567B66CEDB08BB9CE965F45B0DA1C06F2204 * ___UnZeCgFPYKrcOECMvOoUKxJBUBt_60;
	// System.Action Rewired.ReInput::wmHipiZGjflaEWstQdFEaNZigaYl
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___wmHipiZGjflaEWstQdFEaNZigaYl_61;
	// System.Action`1<System.Boolean> Rewired.ReInput::VwrVtHVuzenuKikGQAjSkTWmEjp
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___VwrVtHVuzenuKikGQAjSkTWmEjp_62;
	// System.Single Rewired.ReInput::unscaledDeltaTime
	float ___unscaledDeltaTime_63;
	// System.Single Rewired.ReInput::unscaledTime
	float ___unscaledTime_64;
	// System.Single Rewired.ReInput::unscaledTimePrev
	float ___unscaledTimePrev_65;
	// System.UInt32 Rewired.ReInput::currentFrame
	uint32_t ___currentFrame_66;
	// System.UInt32 Rewired.ReInput::previousFrame
	uint32_t ___previousFrame_67;
	// System.UInt32 Rewired.ReInput::absFrame
	uint32_t ___absFrame_68;
	// System.Action`1<System.Exception> Rewired.ReInput::bIpOcSHJOaNbSfzUQfNHLjsIhGU
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___bIpOcSHJOaNbSfzUQfNHLjsIhGU_69;
	// System.Action`1<System.Exception> Rewired.ReInput::SeKbgRoSSdxvLKKSFGtzsUJVIEq
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___SeKbgRoSSdxvLKKSFGtzsUJVIEq_70;
	// System.Action`1<System.Exception> Rewired.ReInput::jqUTJMOeCBaPwenidONjsgqqmhF
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___jqUTJMOeCBaPwenidONjsgqqmhF_71;
	// System.Action`1<System.Exception> Rewired.ReInput::VxgfIwaEpxChiqoBUxMyAXuOvzah
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___VxgfIwaEpxChiqoBUxMyAXuOvzah_72;
	// System.Action`1<System.Exception> Rewired.ReInput::yGfaFeDTgjQCiyfEnFKwcIrTEKK
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___yGfaFeDTgjQCiyfEnFKwcIrTEKK_73;
	// System.Action`1<System.Exception> Rewired.ReInput::bNWtItxSTpScLiOAwzhSFiRFjTf
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___bNWtItxSTpScLiOAwzhSFiRFjTf_74;
	// System.Action`1<System.Exception> Rewired.ReInput::NmaaMQBbnFbXDfscDfYsWbItnQyf
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___NmaaMQBbnFbXDfscDfYsWbItnQyf_75;
	// System.Action`1<System.Exception> Rewired.ReInput::rMSAIWRRORgnlBNyMWCUdKjLpiE
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___rMSAIWRRORgnlBNyMWCUdKjLpiE_76;
	// System.Action`1<System.Exception> Rewired.ReInput::oMtJsfQwbTEDGJiLlRXXMThiarq
	Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * ___oMtJsfQwbTEDGJiLlRXXMThiarq_77;
	// System.Func`1<System.Boolean> Rewired.ReInput::XiZLABGROWZBxAnjRHXsnpGxypM
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___XiZLABGROWZBxAnjRHXsnpGxypM_78;

public:
	inline static int32_t get_offset_of_BmbBIGhcPaQcJtPaDIuasDCjqQNz_7() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___BmbBIGhcPaQcJtPaDIuasDCjqQNz_7)); }
	inline InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * get_BmbBIGhcPaQcJtPaDIuasDCjqQNz_7() const { return ___BmbBIGhcPaQcJtPaDIuasDCjqQNz_7; }
	inline InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 ** get_address_of_BmbBIGhcPaQcJtPaDIuasDCjqQNz_7() { return &___BmbBIGhcPaQcJtPaDIuasDCjqQNz_7; }
	inline void set_BmbBIGhcPaQcJtPaDIuasDCjqQNz_7(InputManager_Base_t59B15491EE0FD742B61D3285150943B4A6D35701 * value)
	{
		___BmbBIGhcPaQcJtPaDIuasDCjqQNz_7 = value;
		Il2CppCodeGenWriteBarrier((&___BmbBIGhcPaQcJtPaDIuasDCjqQNz_7), value);
	}

	inline static int32_t get_offset_of_OnpAheqwnSbTCzETUFrLEHROgfTo_8() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___OnpAheqwnSbTCzETUFrLEHROgfTo_8)); }
	inline PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A * get_OnpAheqwnSbTCzETUFrLEHROgfTo_8() const { return ___OnpAheqwnSbTCzETUFrLEHROgfTo_8; }
	inline PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A ** get_address_of_OnpAheqwnSbTCzETUFrLEHROgfTo_8() { return &___OnpAheqwnSbTCzETUFrLEHROgfTo_8; }
	inline void set_OnpAheqwnSbTCzETUFrLEHROgfTo_8(PlatformInputManager_tF891982F1ED83426874318B2057900C730AAA62A * value)
	{
		___OnpAheqwnSbTCzETUFrLEHROgfTo_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnpAheqwnSbTCzETUFrLEHROgfTo_8), value);
	}

	inline static int32_t get_offset_of_IEqlUZsiLAwwHwjaJiWawGshrHQ_9() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___IEqlUZsiLAwwHwjaJiWawGshrHQ_9)); }
	inline nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22 * get_IEqlUZsiLAwwHwjaJiWawGshrHQ_9() const { return ___IEqlUZsiLAwwHwjaJiWawGshrHQ_9; }
	inline nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22 ** get_address_of_IEqlUZsiLAwwHwjaJiWawGshrHQ_9() { return &___IEqlUZsiLAwwHwjaJiWawGshrHQ_9; }
	inline void set_IEqlUZsiLAwwHwjaJiWawGshrHQ_9(nkGLVKXmQVVuJNEogVVmAwwQOJl_t8CC5C80A3504882AE9928FFF16668EC435807D22 * value)
	{
		___IEqlUZsiLAwwHwjaJiWawGshrHQ_9 = value;
		Il2CppCodeGenWriteBarrier((&___IEqlUZsiLAwwHwjaJiWawGshrHQ_9), value);
	}

	inline static int32_t get_offset_of_RVkFnTmTOxSxyomVVDMNYyuaQAg_10() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___RVkFnTmTOxSxyomVVDMNYyuaQAg_10)); }
	inline lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE * get_RVkFnTmTOxSxyomVVDMNYyuaQAg_10() const { return ___RVkFnTmTOxSxyomVVDMNYyuaQAg_10; }
	inline lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE ** get_address_of_RVkFnTmTOxSxyomVVDMNYyuaQAg_10() { return &___RVkFnTmTOxSxyomVVDMNYyuaQAg_10; }
	inline void set_RVkFnTmTOxSxyomVVDMNYyuaQAg_10(lUbzJtkApcIkohMrHhJAmcDImRRR_tD9D698C83E139CBDDAEC47D478281011C3E60EEE * value)
	{
		___RVkFnTmTOxSxyomVVDMNYyuaQAg_10 = value;
		Il2CppCodeGenWriteBarrier((&___RVkFnTmTOxSxyomVVDMNYyuaQAg_10), value);
	}

	inline static int32_t get_offset_of_fsKUnkudBKaclGoZoLGqLplnpcq_11() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___fsKUnkudBKaclGoZoLGqLplnpcq_11)); }
	inline LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA * get_fsKUnkudBKaclGoZoLGqLplnpcq_11() const { return ___fsKUnkudBKaclGoZoLGqLplnpcq_11; }
	inline LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA ** get_address_of_fsKUnkudBKaclGoZoLGqLplnpcq_11() { return &___fsKUnkudBKaclGoZoLGqLplnpcq_11; }
	inline void set_fsKUnkudBKaclGoZoLGqLplnpcq_11(LkmsImmStVEvsBvZKxyBsAPXXJ_t77053E43086638C76E5D5A1B8C41882906E619FA * value)
	{
		___fsKUnkudBKaclGoZoLGqLplnpcq_11 = value;
		Il2CppCodeGenWriteBarrier((&___fsKUnkudBKaclGoZoLGqLplnpcq_11), value);
	}

	inline static int32_t get_offset_of_jkjiciUYDTwUaFkcGqBpczqirBH_12() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___jkjiciUYDTwUaFkcGqBpczqirBH_12)); }
	inline ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5 * get_jkjiciUYDTwUaFkcGqBpczqirBH_12() const { return ___jkjiciUYDTwUaFkcGqBpczqirBH_12; }
	inline ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5 ** get_address_of_jkjiciUYDTwUaFkcGqBpczqirBH_12() { return &___jkjiciUYDTwUaFkcGqBpczqirBH_12; }
	inline void set_jkjiciUYDTwUaFkcGqBpczqirBH_12(ControllerDataFiles_t233F2B7B2B552FD6E9DE5BF6B80F93AFCDF16CA5 * value)
	{
		___jkjiciUYDTwUaFkcGqBpczqirBH_12 = value;
		Il2CppCodeGenWriteBarrier((&___jkjiciUYDTwUaFkcGqBpczqirBH_12), value);
	}

	inline static int32_t get_offset_of_kuvduxCaGZogunQJJYFMqUHXuo_13() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___kuvduxCaGZogunQJJYFMqUHXuo_13)); }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * get_kuvduxCaGZogunQJJYFMqUHXuo_13() const { return ___kuvduxCaGZogunQJJYFMqUHXuo_13; }
	inline UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 ** get_address_of_kuvduxCaGZogunQJJYFMqUHXuo_13() { return &___kuvduxCaGZogunQJJYFMqUHXuo_13; }
	inline void set_kuvduxCaGZogunQJJYFMqUHXuo_13(UserData_t23CF13BC0883AADA4A3457583033292E49A77D48 * value)
	{
		___kuvduxCaGZogunQJJYFMqUHXuo_13 = value;
		Il2CppCodeGenWriteBarrier((&___kuvduxCaGZogunQJJYFMqUHXuo_13), value);
	}

	inline static int32_t get_offset_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_14() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_14)); }
	inline bool get_oGbqxjxXGgvgXNLPUMbdLzEDsmT_14() const { return ___oGbqxjxXGgvgXNLPUMbdLzEDsmT_14; }
	inline bool* get_address_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_14() { return &___oGbqxjxXGgvgXNLPUMbdLzEDsmT_14; }
	inline void set_oGbqxjxXGgvgXNLPUMbdLzEDsmT_14(bool value)
	{
		___oGbqxjxXGgvgXNLPUMbdLzEDsmT_14 = value;
	}

	inline static int32_t get_offset_of_AEpGUwNwMAsWjLqSLSkygEagFsW_15() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___AEpGUwNwMAsWjLqSLSkygEagFsW_15)); }
	inline ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * get_AEpGUwNwMAsWjLqSLSkygEagFsW_15() const { return ___AEpGUwNwMAsWjLqSLSkygEagFsW_15; }
	inline ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 ** get_address_of_AEpGUwNwMAsWjLqSLSkygEagFsW_15() { return &___AEpGUwNwMAsWjLqSLSkygEagFsW_15; }
	inline void set_AEpGUwNwMAsWjLqSLSkygEagFsW_15(ConfigVars_tD3FE004E260819ED3EEDD720972407A874379B41 * value)
	{
		___AEpGUwNwMAsWjLqSLSkygEagFsW_15 = value;
		Il2CppCodeGenWriteBarrier((&___AEpGUwNwMAsWjLqSLSkygEagFsW_15), value);
	}

	inline static int32_t get_offset_of_ZfYcJdvFIDCHAofeyUpUZGLeLMy_16() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___ZfYcJdvFIDCHAofeyUpUZGLeLMy_16)); }
	inline int32_t get_ZfYcJdvFIDCHAofeyUpUZGLeLMy_16() const { return ___ZfYcJdvFIDCHAofeyUpUZGLeLMy_16; }
	inline int32_t* get_address_of_ZfYcJdvFIDCHAofeyUpUZGLeLMy_16() { return &___ZfYcJdvFIDCHAofeyUpUZGLeLMy_16; }
	inline void set_ZfYcJdvFIDCHAofeyUpUZGLeLMy_16(int32_t value)
	{
		___ZfYcJdvFIDCHAofeyUpUZGLeLMy_16 = value;
	}

	inline static int32_t get_offset_of_efatONDtZGNgmLOHvbVBLBBuace_17() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___efatONDtZGNgmLOHvbVBLBBuace_17)); }
	inline bool get_efatONDtZGNgmLOHvbVBLBBuace_17() const { return ___efatONDtZGNgmLOHvbVBLBBuace_17; }
	inline bool* get_address_of_efatONDtZGNgmLOHvbVBLBBuace_17() { return &___efatONDtZGNgmLOHvbVBLBBuace_17; }
	inline void set_efatONDtZGNgmLOHvbVBLBBuace_17(bool value)
	{
		___efatONDtZGNgmLOHvbVBLBBuace_17 = value;
	}

	inline static int32_t get_offset_of_ebxMcikMAQQZUToudUFSEUFYfeA_18() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___ebxMcikMAQQZUToudUFSEUFYfeA_18)); }
	inline int32_t get_ebxMcikMAQQZUToudUFSEUFYfeA_18() const { return ___ebxMcikMAQQZUToudUFSEUFYfeA_18; }
	inline int32_t* get_address_of_ebxMcikMAQQZUToudUFSEUFYfeA_18() { return &___ebxMcikMAQQZUToudUFSEUFYfeA_18; }
	inline void set_ebxMcikMAQQZUToudUFSEUFYfeA_18(int32_t value)
	{
		___ebxMcikMAQQZUToudUFSEUFYfeA_18 = value;
	}

	inline static int32_t get_offset_of_WLJLSQeGpPKUuKjBWrWTbaFnDry_19() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___WLJLSQeGpPKUuKjBWrWTbaFnDry_19)); }
	inline int32_t get_WLJLSQeGpPKUuKjBWrWTbaFnDry_19() const { return ___WLJLSQeGpPKUuKjBWrWTbaFnDry_19; }
	inline int32_t* get_address_of_WLJLSQeGpPKUuKjBWrWTbaFnDry_19() { return &___WLJLSQeGpPKUuKjBWrWTbaFnDry_19; }
	inline void set_WLJLSQeGpPKUuKjBWrWTbaFnDry_19(int32_t value)
	{
		___WLJLSQeGpPKUuKjBWrWTbaFnDry_19 = value;
	}

	inline static int32_t get_offset_of_yUCTrJSSAVMiycEtyjhErnsZbYwh_20() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___yUCTrJSSAVMiycEtyjhErnsZbYwh_20)); }
	inline int32_t get_yUCTrJSSAVMiycEtyjhErnsZbYwh_20() const { return ___yUCTrJSSAVMiycEtyjhErnsZbYwh_20; }
	inline int32_t* get_address_of_yUCTrJSSAVMiycEtyjhErnsZbYwh_20() { return &___yUCTrJSSAVMiycEtyjhErnsZbYwh_20; }
	inline void set_yUCTrJSSAVMiycEtyjhErnsZbYwh_20(int32_t value)
	{
		___yUCTrJSSAVMiycEtyjhErnsZbYwh_20 = value;
	}

	inline static int32_t get_offset_of_sqyjgtZgshnxlArNKRhZoKPXcspD_21() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___sqyjgtZgshnxlArNKRhZoKPXcspD_21)); }
	inline bool get_sqyjgtZgshnxlArNKRhZoKPXcspD_21() const { return ___sqyjgtZgshnxlArNKRhZoKPXcspD_21; }
	inline bool* get_address_of_sqyjgtZgshnxlArNKRhZoKPXcspD_21() { return &___sqyjgtZgshnxlArNKRhZoKPXcspD_21; }
	inline void set_sqyjgtZgshnxlArNKRhZoKPXcspD_21(bool value)
	{
		___sqyjgtZgshnxlArNKRhZoKPXcspD_21 = value;
	}

	inline static int32_t get_offset_of_RBQeQNGYzPPywsMagaNKPxDkXre_22() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___RBQeQNGYzPPywsMagaNKPxDkXre_22)); }
	inline TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * get_RBQeQNGYzPPywsMagaNKPxDkXre_22() const { return ___RBQeQNGYzPPywsMagaNKPxDkXre_22; }
	inline TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 ** get_address_of_RBQeQNGYzPPywsMagaNKPxDkXre_22() { return &___RBQeQNGYzPPywsMagaNKPxDkXre_22; }
	inline void set_RBQeQNGYzPPywsMagaNKPxDkXre_22(TimerAbs_t89BDE70DB29168CBB45BE2FF477F04A84CC82429 * value)
	{
		___RBQeQNGYzPPywsMagaNKPxDkXre_22 = value;
		Il2CppCodeGenWriteBarrier((&___RBQeQNGYzPPywsMagaNKPxDkXre_22), value);
	}

	inline static int32_t get_offset_of_QFntQDmTgBlbbKQmkWjoaxmXfDr_23() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___QFntQDmTgBlbbKQmkWjoaxmXfDr_23)); }
	inline pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6 * get_QFntQDmTgBlbbKQmkWjoaxmXfDr_23() const { return ___QFntQDmTgBlbbKQmkWjoaxmXfDr_23; }
	inline pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6 ** get_address_of_QFntQDmTgBlbbKQmkWjoaxmXfDr_23() { return &___QFntQDmTgBlbbKQmkWjoaxmXfDr_23; }
	inline void set_QFntQDmTgBlbbKQmkWjoaxmXfDr_23(pqJryspmPjvziIiviMzAyYQWLVF_t23A934806527E77ED77ACDD6B97574DC00008DA6 * value)
	{
		___QFntQDmTgBlbbKQmkWjoaxmXfDr_23 = value;
		Il2CppCodeGenWriteBarrier((&___QFntQDmTgBlbbKQmkWjoaxmXfDr_23), value);
	}

	inline static int32_t get_offset_of_qJWGQSjuXpqXJHLVBEZbgltbRhA_24() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___qJWGQSjuXpqXJHLVBEZbgltbRhA_24)); }
	inline String_t* get_qJWGQSjuXpqXJHLVBEZbgltbRhA_24() const { return ___qJWGQSjuXpqXJHLVBEZbgltbRhA_24; }
	inline String_t** get_address_of_qJWGQSjuXpqXJHLVBEZbgltbRhA_24() { return &___qJWGQSjuXpqXJHLVBEZbgltbRhA_24; }
	inline void set_qJWGQSjuXpqXJHLVBEZbgltbRhA_24(String_t* value)
	{
		___qJWGQSjuXpqXJHLVBEZbgltbRhA_24 = value;
		Il2CppCodeGenWriteBarrier((&___qJWGQSjuXpqXJHLVBEZbgltbRhA_24), value);
	}

	inline static int32_t get_offset_of_nFsjgQIYeASozAWVpsmKWYrynGMt_25() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___nFsjgQIYeASozAWVpsmKWYrynGMt_25)); }
	inline bool get_nFsjgQIYeASozAWVpsmKWYrynGMt_25() const { return ___nFsjgQIYeASozAWVpsmKWYrynGMt_25; }
	inline bool* get_address_of_nFsjgQIYeASozAWVpsmKWYrynGMt_25() { return &___nFsjgQIYeASozAWVpsmKWYrynGMt_25; }
	inline void set_nFsjgQIYeASozAWVpsmKWYrynGMt_25(bool value)
	{
		___nFsjgQIYeASozAWVpsmKWYrynGMt_25 = value;
	}

	inline static int32_t get_offset_of_IxkxAZDFhYdDnAQiwkHxbCHkabao_26() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___IxkxAZDFhYdDnAQiwkHxbCHkabao_26)); }
	inline bool get_IxkxAZDFhYdDnAQiwkHxbCHkabao_26() const { return ___IxkxAZDFhYdDnAQiwkHxbCHkabao_26; }
	inline bool* get_address_of_IxkxAZDFhYdDnAQiwkHxbCHkabao_26() { return &___IxkxAZDFhYdDnAQiwkHxbCHkabao_26; }
	inline void set_IxkxAZDFhYdDnAQiwkHxbCHkabao_26(bool value)
	{
		___IxkxAZDFhYdDnAQiwkHxbCHkabao_26 = value;
	}

	inline static int32_t get_offset_of_kVozOJjgIMPdyuEiTtvdCQDrpVk_27() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___kVozOJjgIMPdyuEiTtvdCQDrpVk_27)); }
	inline bool get_kVozOJjgIMPdyuEiTtvdCQDrpVk_27() const { return ___kVozOJjgIMPdyuEiTtvdCQDrpVk_27; }
	inline bool* get_address_of_kVozOJjgIMPdyuEiTtvdCQDrpVk_27() { return &___kVozOJjgIMPdyuEiTtvdCQDrpVk_27; }
	inline void set_kVozOJjgIMPdyuEiTtvdCQDrpVk_27(bool value)
	{
		___kVozOJjgIMPdyuEiTtvdCQDrpVk_27 = value;
	}

	inline static int32_t get_offset_of_QzLGqBuiJUSnYPDYiBGmAYvPcUAw_28() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___QzLGqBuiJUSnYPDYiBGmAYvPcUAw_28)); }
	inline int32_t get_QzLGqBuiJUSnYPDYiBGmAYvPcUAw_28() const { return ___QzLGqBuiJUSnYPDYiBGmAYvPcUAw_28; }
	inline int32_t* get_address_of_QzLGqBuiJUSnYPDYiBGmAYvPcUAw_28() { return &___QzLGqBuiJUSnYPDYiBGmAYvPcUAw_28; }
	inline void set_QzLGqBuiJUSnYPDYiBGmAYvPcUAw_28(int32_t value)
	{
		___QzLGqBuiJUSnYPDYiBGmAYvPcUAw_28 = value;
	}

	inline static int32_t get_offset_of__id_29() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ____id_29)); }
	inline int32_t get__id_29() const { return ____id_29; }
	inline int32_t* get_address_of__id_29() { return &____id_29; }
	inline void set__id_29(int32_t value)
	{
		____id_29 = value;
	}

	inline static int32_t get_offset_of_uYPBIvZJZOvpItwzdZbRubjNHBQ_30() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___uYPBIvZJZOvpItwzdZbRubjNHBQ_30)); }
	inline int32_t get_uYPBIvZJZOvpItwzdZbRubjNHBQ_30() const { return ___uYPBIvZJZOvpItwzdZbRubjNHBQ_30; }
	inline int32_t* get_address_of_uYPBIvZJZOvpItwzdZbRubjNHBQ_30() { return &___uYPBIvZJZOvpItwzdZbRubjNHBQ_30; }
	inline void set_uYPBIvZJZOvpItwzdZbRubjNHBQ_30(int32_t value)
	{
		___uYPBIvZJZOvpItwzdZbRubjNHBQ_30 = value;
	}

	inline static int32_t get_offset_of_EDBzxspLLLJMCVwEjgVqreXxWCw_31() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___EDBzxspLLLJMCVwEjgVqreXxWCw_31)); }
	inline int32_t get_EDBzxspLLLJMCVwEjgVqreXxWCw_31() const { return ___EDBzxspLLLJMCVwEjgVqreXxWCw_31; }
	inline int32_t* get_address_of_EDBzxspLLLJMCVwEjgVqreXxWCw_31() { return &___EDBzxspLLLJMCVwEjgVqreXxWCw_31; }
	inline void set_EDBzxspLLLJMCVwEjgVqreXxWCw_31(int32_t value)
	{
		___EDBzxspLLLJMCVwEjgVqreXxWCw_31 = value;
	}

	inline static int32_t get_offset_of_FidPeDgESHAdzaeFXcrFbXGDXZpE_32() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___FidPeDgESHAdzaeFXcrFbXGDXZpE_32)); }
	inline bool get_FidPeDgESHAdzaeFXcrFbXGDXZpE_32() const { return ___FidPeDgESHAdzaeFXcrFbXGDXZpE_32; }
	inline bool* get_address_of_FidPeDgESHAdzaeFXcrFbXGDXZpE_32() { return &___FidPeDgESHAdzaeFXcrFbXGDXZpE_32; }
	inline void set_FidPeDgESHAdzaeFXcrFbXGDXZpE_32(bool value)
	{
		___FidPeDgESHAdzaeFXcrFbXGDXZpE_32 = value;
	}

	inline static int32_t get_offset_of_cuHMjPjtOdcyqFzxRQNlFdKcQrI_33() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___cuHMjPjtOdcyqFzxRQNlFdKcQrI_33)); }
	inline UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9 * get_cuHMjPjtOdcyqFzxRQNlFdKcQrI_33() const { return ___cuHMjPjtOdcyqFzxRQNlFdKcQrI_33; }
	inline UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9 ** get_address_of_cuHMjPjtOdcyqFzxRQNlFdKcQrI_33() { return &___cuHMjPjtOdcyqFzxRQNlFdKcQrI_33; }
	inline void set_cuHMjPjtOdcyqFzxRQNlFdKcQrI_33(UnityTouch_t854507D66472271756069BB218E8CEEE3AD9B1F9 * value)
	{
		___cuHMjPjtOdcyqFzxRQNlFdKcQrI_33 = value;
		Il2CppCodeGenWriteBarrier((&___cuHMjPjtOdcyqFzxRQNlFdKcQrI_33), value);
	}

	inline static int32_t get_offset_of_IcomarPgDQOtvFQAqkQIQWVwlZf_34() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___IcomarPgDQOtvFQAqkQIQWVwlZf_34)); }
	inline PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412 * get_IcomarPgDQOtvFQAqkQIQWVwlZf_34() const { return ___IcomarPgDQOtvFQAqkQIQWVwlZf_34; }
	inline PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412 ** get_address_of_IcomarPgDQOtvFQAqkQIQWVwlZf_34() { return &___IcomarPgDQOtvFQAqkQIQWVwlZf_34; }
	inline void set_IcomarPgDQOtvFQAqkQIQWVwlZf_34(PlayerHelper_t2C3BED5411C72051E42F567C5BA51C28FF0F6412 * value)
	{
		___IcomarPgDQOtvFQAqkQIQWVwlZf_34 = value;
		Il2CppCodeGenWriteBarrier((&___IcomarPgDQOtvFQAqkQIQWVwlZf_34), value);
	}

	inline static int32_t get_offset_of_USvTrWNQsztrTDaJRlzzYsugcyN_35() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___USvTrWNQsztrTDaJRlzzYsugcyN_35)); }
	inline ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62 * get_USvTrWNQsztrTDaJRlzzYsugcyN_35() const { return ___USvTrWNQsztrTDaJRlzzYsugcyN_35; }
	inline ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62 ** get_address_of_USvTrWNQsztrTDaJRlzzYsugcyN_35() { return &___USvTrWNQsztrTDaJRlzzYsugcyN_35; }
	inline void set_USvTrWNQsztrTDaJRlzzYsugcyN_35(ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62 * value)
	{
		___USvTrWNQsztrTDaJRlzzYsugcyN_35 = value;
		Il2CppCodeGenWriteBarrier((&___USvTrWNQsztrTDaJRlzzYsugcyN_35), value);
	}

	inline static int32_t get_offset_of_cZhgkwtQTFgoqgpMoqypewKgXXZA_36() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___cZhgkwtQTFgoqgpMoqypewKgXXZA_36)); }
	inline MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * get_cZhgkwtQTFgoqgpMoqypewKgXXZA_36() const { return ___cZhgkwtQTFgoqgpMoqypewKgXXZA_36; }
	inline MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 ** get_address_of_cZhgkwtQTFgoqgpMoqypewKgXXZA_36() { return &___cZhgkwtQTFgoqgpMoqypewKgXXZA_36; }
	inline void set_cZhgkwtQTFgoqgpMoqypewKgXXZA_36(MappingHelper_t2F1FD9406CF911414A90FCB4852B174257FA2BF6 * value)
	{
		___cZhgkwtQTFgoqgpMoqypewKgXXZA_36 = value;
		Il2CppCodeGenWriteBarrier((&___cZhgkwtQTFgoqgpMoqypewKgXXZA_36), value);
	}

	inline static int32_t get_offset_of_qBplpkUglPNgqwzfqaaAbLOGttB_37() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___qBplpkUglPNgqwzfqaaAbLOGttB_37)); }
	inline TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B * get_qBplpkUglPNgqwzfqaaAbLOGttB_37() const { return ___qBplpkUglPNgqwzfqaaAbLOGttB_37; }
	inline TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B ** get_address_of_qBplpkUglPNgqwzfqaaAbLOGttB_37() { return &___qBplpkUglPNgqwzfqaaAbLOGttB_37; }
	inline void set_qBplpkUglPNgqwzfqaaAbLOGttB_37(TimeHelper_tE60E9315943AF78E350D7DA8EAF5C0E7DF83B48B * value)
	{
		___qBplpkUglPNgqwzfqaaAbLOGttB_37 = value;
		Il2CppCodeGenWriteBarrier((&___qBplpkUglPNgqwzfqaaAbLOGttB_37), value);
	}

	inline static int32_t get_offset_of_ncvdGtGSDIImpbUXOKhmHLFdPjbR_38() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___ncvdGtGSDIImpbUXOKhmHLFdPjbR_38)); }
	inline ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5 * get_ncvdGtGSDIImpbUXOKhmHLFdPjbR_38() const { return ___ncvdGtGSDIImpbUXOKhmHLFdPjbR_38; }
	inline ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5 ** get_address_of_ncvdGtGSDIImpbUXOKhmHLFdPjbR_38() { return &___ncvdGtGSDIImpbUXOKhmHLFdPjbR_38; }
	inline void set_ncvdGtGSDIImpbUXOKhmHLFdPjbR_38(ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5 * value)
	{
		___ncvdGtGSDIImpbUXOKhmHLFdPjbR_38 = value;
		Il2CppCodeGenWriteBarrier((&___ncvdGtGSDIImpbUXOKhmHLFdPjbR_38), value);
	}

	inline static int32_t get_offset_of_gqSFnpTCFawuMMNVGgKKIsYZUXL_39() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___gqSFnpTCFawuMMNVGgKKIsYZUXL_39)); }
	inline EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0 * get_gqSFnpTCFawuMMNVGgKKIsYZUXL_39() const { return ___gqSFnpTCFawuMMNVGgKKIsYZUXL_39; }
	inline EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0 ** get_address_of_gqSFnpTCFawuMMNVGgKKIsYZUXL_39() { return &___gqSFnpTCFawuMMNVGgKKIsYZUXL_39; }
	inline void set_gqSFnpTCFawuMMNVGgKKIsYZUXL_39(EaCqncmrbxTySfotaFWfqDQVLIS_t6F36CD9B448EC26AA5E9F72A2119DDDA6CC8EFE0 * value)
	{
		___gqSFnpTCFawuMMNVGgKKIsYZUXL_39 = value;
		Il2CppCodeGenWriteBarrier((&___gqSFnpTCFawuMMNVGgKKIsYZUXL_39), value);
	}

	inline static int32_t get_offset_of_TojkjqMwwMdhKJGKNsXcfEQqdqk_40() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___TojkjqMwwMdhKJGKNsXcfEQqdqk_40)); }
	inline UserDataStore_tA04E79BDF387346606C6F4379B4FD0935BA176C1 * get_TojkjqMwwMdhKJGKNsXcfEQqdqk_40() const { return ___TojkjqMwwMdhKJGKNsXcfEQqdqk_40; }
	inline UserDataStore_tA04E79BDF387346606C6F4379B4FD0935BA176C1 ** get_address_of_TojkjqMwwMdhKJGKNsXcfEQqdqk_40() { return &___TojkjqMwwMdhKJGKNsXcfEQqdqk_40; }
	inline void set_TojkjqMwwMdhKJGKNsXcfEQqdqk_40(UserDataStore_tA04E79BDF387346606C6F4379B4FD0935BA176C1 * value)
	{
		___TojkjqMwwMdhKJGKNsXcfEQqdqk_40 = value;
		Il2CppCodeGenWriteBarrier((&___TojkjqMwwMdhKJGKNsXcfEQqdqk_40), value);
	}

	inline static int32_t get_offset_of_OdmAAKBtcbRSgXiuxOTrhpIzlofQ_41() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___OdmAAKBtcbRSgXiuxOTrhpIzlofQ_41)); }
	inline RuntimeObject* get_OdmAAKBtcbRSgXiuxOTrhpIzlofQ_41() const { return ___OdmAAKBtcbRSgXiuxOTrhpIzlofQ_41; }
	inline RuntimeObject** get_address_of_OdmAAKBtcbRSgXiuxOTrhpIzlofQ_41() { return &___OdmAAKBtcbRSgXiuxOTrhpIzlofQ_41; }
	inline void set_OdmAAKBtcbRSgXiuxOTrhpIzlofQ_41(RuntimeObject* value)
	{
		___OdmAAKBtcbRSgXiuxOTrhpIzlofQ_41 = value;
		Il2CppCodeGenWriteBarrier((&___OdmAAKBtcbRSgXiuxOTrhpIzlofQ_41), value);
	}

	inline static int32_t get_offset_of_ABhAFxjUUVHIXxvYBKqRrBnPSGY_42() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___ABhAFxjUUVHIXxvYBKqRrBnPSGY_42)); }
	inline MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E * get_ABhAFxjUUVHIXxvYBKqRrBnPSGY_42() const { return ___ABhAFxjUUVHIXxvYBKqRrBnPSGY_42; }
	inline MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E ** get_address_of_ABhAFxjUUVHIXxvYBKqRrBnPSGY_42() { return &___ABhAFxjUUVHIXxvYBKqRrBnPSGY_42; }
	inline void set_ABhAFxjUUVHIXxvYBKqRrBnPSGY_42(MQKnbNqPnvBXaFjURWimREWJePx_tDA208E365DB809C8477A2F047E8CAD06B771192E * value)
	{
		___ABhAFxjUUVHIXxvYBKqRrBnPSGY_42 = value;
		Il2CppCodeGenWriteBarrier((&___ABhAFxjUUVHIXxvYBKqRrBnPSGY_42), value);
	}

	inline static int32_t get_offset_of_wAGfrKtlxRtjFeYStOdjEyhnkFW_43() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___wAGfrKtlxRtjFeYStOdjEyhnkFW_43)); }
	inline SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900 * get_wAGfrKtlxRtjFeYStOdjEyhnkFW_43() const { return ___wAGfrKtlxRtjFeYStOdjEyhnkFW_43; }
	inline SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900 ** get_address_of_wAGfrKtlxRtjFeYStOdjEyhnkFW_43() { return &___wAGfrKtlxRtjFeYStOdjEyhnkFW_43; }
	inline void set_wAGfrKtlxRtjFeYStOdjEyhnkFW_43(SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900 * value)
	{
		___wAGfrKtlxRtjFeYStOdjEyhnkFW_43 = value;
		Il2CppCodeGenWriteBarrier((&___wAGfrKtlxRtjFeYStOdjEyhnkFW_43), value);
	}

	inline static int32_t get_offset_of_XKQeRyjlqgLlnGtmsVoUhRfEeEu_44() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___XKQeRyjlqgLlnGtmsVoUhRfEeEu_44)); }
	inline SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900 * get_XKQeRyjlqgLlnGtmsVoUhRfEeEu_44() const { return ___XKQeRyjlqgLlnGtmsVoUhRfEeEu_44; }
	inline SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900 ** get_address_of_XKQeRyjlqgLlnGtmsVoUhRfEeEu_44() { return &___XKQeRyjlqgLlnGtmsVoUhRfEeEu_44; }
	inline void set_XKQeRyjlqgLlnGtmsVoUhRfEeEu_44(SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900 * value)
	{
		___XKQeRyjlqgLlnGtmsVoUhRfEeEu_44 = value;
		Il2CppCodeGenWriteBarrier((&___XKQeRyjlqgLlnGtmsVoUhRfEeEu_44), value);
	}

	inline static int32_t get_offset_of_HGNegOULkehGQCIplmMVqBQjWvm_45() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___HGNegOULkehGQCIplmMVqBQjWvm_45)); }
	inline SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900 * get_HGNegOULkehGQCIplmMVqBQjWvm_45() const { return ___HGNegOULkehGQCIplmMVqBQjWvm_45; }
	inline SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900 ** get_address_of_HGNegOULkehGQCIplmMVqBQjWvm_45() { return &___HGNegOULkehGQCIplmMVqBQjWvm_45; }
	inline void set_HGNegOULkehGQCIplmMVqBQjWvm_45(SafeAction_1_tE0962C9C4A998738548954CB8FD6C91705F94900 * value)
	{
		___HGNegOULkehGQCIplmMVqBQjWvm_45 = value;
		Il2CppCodeGenWriteBarrier((&___HGNegOULkehGQCIplmMVqBQjWvm_45), value);
	}

	inline static int32_t get_offset_of_VCOpkyvHUubmUcZWHeKYunuBQXa_46() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___VCOpkyvHUubmUcZWHeKYunuBQXa_46)); }
	inline SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * get_VCOpkyvHUubmUcZWHeKYunuBQXa_46() const { return ___VCOpkyvHUubmUcZWHeKYunuBQXa_46; }
	inline SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD ** get_address_of_VCOpkyvHUubmUcZWHeKYunuBQXa_46() { return &___VCOpkyvHUubmUcZWHeKYunuBQXa_46; }
	inline void set_VCOpkyvHUubmUcZWHeKYunuBQXa_46(SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * value)
	{
		___VCOpkyvHUubmUcZWHeKYunuBQXa_46 = value;
		Il2CppCodeGenWriteBarrier((&___VCOpkyvHUubmUcZWHeKYunuBQXa_46), value);
	}

	inline static int32_t get_offset_of_BuGUijCbDBaDRmwDRglZiFsDvpqe_47() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___BuGUijCbDBaDRmwDRglZiFsDvpqe_47)); }
	inline SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * get_BuGUijCbDBaDRmwDRglZiFsDvpqe_47() const { return ___BuGUijCbDBaDRmwDRglZiFsDvpqe_47; }
	inline SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD ** get_address_of_BuGUijCbDBaDRmwDRglZiFsDvpqe_47() { return &___BuGUijCbDBaDRmwDRglZiFsDvpqe_47; }
	inline void set_BuGUijCbDBaDRmwDRglZiFsDvpqe_47(SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * value)
	{
		___BuGUijCbDBaDRmwDRglZiFsDvpqe_47 = value;
		Il2CppCodeGenWriteBarrier((&___BuGUijCbDBaDRmwDRglZiFsDvpqe_47), value);
	}

	inline static int32_t get_offset_of_NkZtgCLxdOallQBmQLJnhwLpvNR_48() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___NkZtgCLxdOallQBmQLJnhwLpvNR_48)); }
	inline SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * get_NkZtgCLxdOallQBmQLJnhwLpvNR_48() const { return ___NkZtgCLxdOallQBmQLJnhwLpvNR_48; }
	inline SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD ** get_address_of_NkZtgCLxdOallQBmQLJnhwLpvNR_48() { return &___NkZtgCLxdOallQBmQLJnhwLpvNR_48; }
	inline void set_NkZtgCLxdOallQBmQLJnhwLpvNR_48(SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * value)
	{
		___NkZtgCLxdOallQBmQLJnhwLpvNR_48 = value;
		Il2CppCodeGenWriteBarrier((&___NkZtgCLxdOallQBmQLJnhwLpvNR_48), value);
	}

	inline static int32_t get_offset_of_LEeKGKpsAldSIdJikAAjfwxSeje_49() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___LEeKGKpsAldSIdJikAAjfwxSeje_49)); }
	inline SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * get_LEeKGKpsAldSIdJikAAjfwxSeje_49() const { return ___LEeKGKpsAldSIdJikAAjfwxSeje_49; }
	inline SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD ** get_address_of_LEeKGKpsAldSIdJikAAjfwxSeje_49() { return &___LEeKGKpsAldSIdJikAAjfwxSeje_49; }
	inline void set_LEeKGKpsAldSIdJikAAjfwxSeje_49(SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * value)
	{
		___LEeKGKpsAldSIdJikAAjfwxSeje_49 = value;
		Il2CppCodeGenWriteBarrier((&___LEeKGKpsAldSIdJikAAjfwxSeje_49), value);
	}

	inline static int32_t get_offset_of_vuOZQDAdWNOZNtLPcsGWbHRvFwF_50() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___vuOZQDAdWNOZNtLPcsGWbHRvFwF_50)); }
	inline SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * get_vuOZQDAdWNOZNtLPcsGWbHRvFwF_50() const { return ___vuOZQDAdWNOZNtLPcsGWbHRvFwF_50; }
	inline SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD ** get_address_of_vuOZQDAdWNOZNtLPcsGWbHRvFwF_50() { return &___vuOZQDAdWNOZNtLPcsGWbHRvFwF_50; }
	inline void set_vuOZQDAdWNOZNtLPcsGWbHRvFwF_50(SafeAction_tF141A9EE03213597DA8CA5527AAE121779DC69AD * value)
	{
		___vuOZQDAdWNOZNtLPcsGWbHRvFwF_50 = value;
		Il2CppCodeGenWriteBarrier((&___vuOZQDAdWNOZNtLPcsGWbHRvFwF_50), value);
	}

	inline static int32_t get_offset_of__ApplicationFocusChangedEvent_51() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ____ApplicationFocusChangedEvent_51)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get__ApplicationFocusChangedEvent_51() const { return ____ApplicationFocusChangedEvent_51; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of__ApplicationFocusChangedEvent_51() { return &____ApplicationFocusChangedEvent_51; }
	inline void set__ApplicationFocusChangedEvent_51(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		____ApplicationFocusChangedEvent_51 = value;
		Il2CppCodeGenWriteBarrier((&____ApplicationFocusChangedEvent_51), value);
	}

	inline static int32_t get_offset_of_SjPvewVCAcAkUrlaNILvKkUJkKyE_52() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___SjPvewVCAcAkUrlaNILvKkUJkKyE_52)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_SjPvewVCAcAkUrlaNILvKkUJkKyE_52() const { return ___SjPvewVCAcAkUrlaNILvKkUJkKyE_52; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_SjPvewVCAcAkUrlaNILvKkUJkKyE_52() { return &___SjPvewVCAcAkUrlaNILvKkUJkKyE_52; }
	inline void set_SjPvewVCAcAkUrlaNILvKkUJkKyE_52(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___SjPvewVCAcAkUrlaNILvKkUJkKyE_52 = value;
		Il2CppCodeGenWriteBarrier((&___SjPvewVCAcAkUrlaNILvKkUJkKyE_52), value);
	}

	inline static int32_t get_offset_of_JrUdKzkIBWnUrmLThTqrbZhGkNnq_53() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___JrUdKzkIBWnUrmLThTqrbZhGkNnq_53)); }
	inline Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899 * get_JrUdKzkIBWnUrmLThTqrbZhGkNnq_53() const { return ___JrUdKzkIBWnUrmLThTqrbZhGkNnq_53; }
	inline Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899 ** get_address_of_JrUdKzkIBWnUrmLThTqrbZhGkNnq_53() { return &___JrUdKzkIBWnUrmLThTqrbZhGkNnq_53; }
	inline void set_JrUdKzkIBWnUrmLThTqrbZhGkNnq_53(Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899 * value)
	{
		___JrUdKzkIBWnUrmLThTqrbZhGkNnq_53 = value;
		Il2CppCodeGenWriteBarrier((&___JrUdKzkIBWnUrmLThTqrbZhGkNnq_53), value);
	}

	inline static int32_t get_offset_of_NYEbfVtXwPGnqWryNGfgyhMndBE_54() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___NYEbfVtXwPGnqWryNGfgyhMndBE_54)); }
	inline Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899 * get_NYEbfVtXwPGnqWryNGfgyhMndBE_54() const { return ___NYEbfVtXwPGnqWryNGfgyhMndBE_54; }
	inline Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899 ** get_address_of_NYEbfVtXwPGnqWryNGfgyhMndBE_54() { return &___NYEbfVtXwPGnqWryNGfgyhMndBE_54; }
	inline void set_NYEbfVtXwPGnqWryNGfgyhMndBE_54(Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899 * value)
	{
		___NYEbfVtXwPGnqWryNGfgyhMndBE_54 = value;
		Il2CppCodeGenWriteBarrier((&___NYEbfVtXwPGnqWryNGfgyhMndBE_54), value);
	}

	inline static int32_t get_offset_of_RKICUSyOXlFZjskVbqFIuVGlJls_55() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___RKICUSyOXlFZjskVbqFIuVGlJls_55)); }
	inline Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899 * get_RKICUSyOXlFZjskVbqFIuVGlJls_55() const { return ___RKICUSyOXlFZjskVbqFIuVGlJls_55; }
	inline Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899 ** get_address_of_RKICUSyOXlFZjskVbqFIuVGlJls_55() { return &___RKICUSyOXlFZjskVbqFIuVGlJls_55; }
	inline void set_RKICUSyOXlFZjskVbqFIuVGlJls_55(Action_1_t9AE7E8278BC7B21B2EF088FEA450424D757A1899 * value)
	{
		___RKICUSyOXlFZjskVbqFIuVGlJls_55 = value;
		Il2CppCodeGenWriteBarrier((&___RKICUSyOXlFZjskVbqFIuVGlJls_55), value);
	}

	inline static int32_t get_offset_of_vasXwzLRByGLnTqYebmSZAOcNcG_56() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___vasXwzLRByGLnTqYebmSZAOcNcG_56)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_vasXwzLRByGLnTqYebmSZAOcNcG_56() const { return ___vasXwzLRByGLnTqYebmSZAOcNcG_56; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_vasXwzLRByGLnTqYebmSZAOcNcG_56() { return &___vasXwzLRByGLnTqYebmSZAOcNcG_56; }
	inline void set_vasXwzLRByGLnTqYebmSZAOcNcG_56(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___vasXwzLRByGLnTqYebmSZAOcNcG_56 = value;
		Il2CppCodeGenWriteBarrier((&___vasXwzLRByGLnTqYebmSZAOcNcG_56), value);
	}

	inline static int32_t get_offset_of_mqfPVmcbmLNZudOfQOcbRCCAgxYf_57() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___mqfPVmcbmLNZudOfQOcbRCCAgxYf_57)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_mqfPVmcbmLNZudOfQOcbRCCAgxYf_57() const { return ___mqfPVmcbmLNZudOfQOcbRCCAgxYf_57; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_mqfPVmcbmLNZudOfQOcbRCCAgxYf_57() { return &___mqfPVmcbmLNZudOfQOcbRCCAgxYf_57; }
	inline void set_mqfPVmcbmLNZudOfQOcbRCCAgxYf_57(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___mqfPVmcbmLNZudOfQOcbRCCAgxYf_57 = value;
		Il2CppCodeGenWriteBarrier((&___mqfPVmcbmLNZudOfQOcbRCCAgxYf_57), value);
	}

	inline static int32_t get_offset_of_PEZTdLWPjWbAMCHYtsIZabliquLe_58() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___PEZTdLWPjWbAMCHYtsIZabliquLe_58)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_PEZTdLWPjWbAMCHYtsIZabliquLe_58() const { return ___PEZTdLWPjWbAMCHYtsIZabliquLe_58; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_PEZTdLWPjWbAMCHYtsIZabliquLe_58() { return &___PEZTdLWPjWbAMCHYtsIZabliquLe_58; }
	inline void set_PEZTdLWPjWbAMCHYtsIZabliquLe_58(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___PEZTdLWPjWbAMCHYtsIZabliquLe_58 = value;
		Il2CppCodeGenWriteBarrier((&___PEZTdLWPjWbAMCHYtsIZabliquLe_58), value);
	}

	inline static int32_t get_offset_of_UzVqkwqMmsnMVrmgTvaiPTZgjzF_59() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___UzVqkwqMmsnMVrmgTvaiPTZgjzF_59)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_UzVqkwqMmsnMVrmgTvaiPTZgjzF_59() const { return ___UzVqkwqMmsnMVrmgTvaiPTZgjzF_59; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_UzVqkwqMmsnMVrmgTvaiPTZgjzF_59() { return &___UzVqkwqMmsnMVrmgTvaiPTZgjzF_59; }
	inline void set_UzVqkwqMmsnMVrmgTvaiPTZgjzF_59(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___UzVqkwqMmsnMVrmgTvaiPTZgjzF_59 = value;
		Il2CppCodeGenWriteBarrier((&___UzVqkwqMmsnMVrmgTvaiPTZgjzF_59), value);
	}

	inline static int32_t get_offset_of_UnZeCgFPYKrcOECMvOoUKxJBUBt_60() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___UnZeCgFPYKrcOECMvOoUKxJBUBt_60)); }
	inline Action_1_t4F62567B66CEDB08BB9CE965F45B0DA1C06F2204 * get_UnZeCgFPYKrcOECMvOoUKxJBUBt_60() const { return ___UnZeCgFPYKrcOECMvOoUKxJBUBt_60; }
	inline Action_1_t4F62567B66CEDB08BB9CE965F45B0DA1C06F2204 ** get_address_of_UnZeCgFPYKrcOECMvOoUKxJBUBt_60() { return &___UnZeCgFPYKrcOECMvOoUKxJBUBt_60; }
	inline void set_UnZeCgFPYKrcOECMvOoUKxJBUBt_60(Action_1_t4F62567B66CEDB08BB9CE965F45B0DA1C06F2204 * value)
	{
		___UnZeCgFPYKrcOECMvOoUKxJBUBt_60 = value;
		Il2CppCodeGenWriteBarrier((&___UnZeCgFPYKrcOECMvOoUKxJBUBt_60), value);
	}

	inline static int32_t get_offset_of_wmHipiZGjflaEWstQdFEaNZigaYl_61() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___wmHipiZGjflaEWstQdFEaNZigaYl_61)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_wmHipiZGjflaEWstQdFEaNZigaYl_61() const { return ___wmHipiZGjflaEWstQdFEaNZigaYl_61; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_wmHipiZGjflaEWstQdFEaNZigaYl_61() { return &___wmHipiZGjflaEWstQdFEaNZigaYl_61; }
	inline void set_wmHipiZGjflaEWstQdFEaNZigaYl_61(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___wmHipiZGjflaEWstQdFEaNZigaYl_61 = value;
		Il2CppCodeGenWriteBarrier((&___wmHipiZGjflaEWstQdFEaNZigaYl_61), value);
	}

	inline static int32_t get_offset_of_VwrVtHVuzenuKikGQAjSkTWmEjp_62() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___VwrVtHVuzenuKikGQAjSkTWmEjp_62)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_VwrVtHVuzenuKikGQAjSkTWmEjp_62() const { return ___VwrVtHVuzenuKikGQAjSkTWmEjp_62; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_VwrVtHVuzenuKikGQAjSkTWmEjp_62() { return &___VwrVtHVuzenuKikGQAjSkTWmEjp_62; }
	inline void set_VwrVtHVuzenuKikGQAjSkTWmEjp_62(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___VwrVtHVuzenuKikGQAjSkTWmEjp_62 = value;
		Il2CppCodeGenWriteBarrier((&___VwrVtHVuzenuKikGQAjSkTWmEjp_62), value);
	}

	inline static int32_t get_offset_of_unscaledDeltaTime_63() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___unscaledDeltaTime_63)); }
	inline float get_unscaledDeltaTime_63() const { return ___unscaledDeltaTime_63; }
	inline float* get_address_of_unscaledDeltaTime_63() { return &___unscaledDeltaTime_63; }
	inline void set_unscaledDeltaTime_63(float value)
	{
		___unscaledDeltaTime_63 = value;
	}

	inline static int32_t get_offset_of_unscaledTime_64() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___unscaledTime_64)); }
	inline float get_unscaledTime_64() const { return ___unscaledTime_64; }
	inline float* get_address_of_unscaledTime_64() { return &___unscaledTime_64; }
	inline void set_unscaledTime_64(float value)
	{
		___unscaledTime_64 = value;
	}

	inline static int32_t get_offset_of_unscaledTimePrev_65() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___unscaledTimePrev_65)); }
	inline float get_unscaledTimePrev_65() const { return ___unscaledTimePrev_65; }
	inline float* get_address_of_unscaledTimePrev_65() { return &___unscaledTimePrev_65; }
	inline void set_unscaledTimePrev_65(float value)
	{
		___unscaledTimePrev_65 = value;
	}

	inline static int32_t get_offset_of_currentFrame_66() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___currentFrame_66)); }
	inline uint32_t get_currentFrame_66() const { return ___currentFrame_66; }
	inline uint32_t* get_address_of_currentFrame_66() { return &___currentFrame_66; }
	inline void set_currentFrame_66(uint32_t value)
	{
		___currentFrame_66 = value;
	}

	inline static int32_t get_offset_of_previousFrame_67() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___previousFrame_67)); }
	inline uint32_t get_previousFrame_67() const { return ___previousFrame_67; }
	inline uint32_t* get_address_of_previousFrame_67() { return &___previousFrame_67; }
	inline void set_previousFrame_67(uint32_t value)
	{
		___previousFrame_67 = value;
	}

	inline static int32_t get_offset_of_absFrame_68() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___absFrame_68)); }
	inline uint32_t get_absFrame_68() const { return ___absFrame_68; }
	inline uint32_t* get_address_of_absFrame_68() { return &___absFrame_68; }
	inline void set_absFrame_68(uint32_t value)
	{
		___absFrame_68 = value;
	}

	inline static int32_t get_offset_of_bIpOcSHJOaNbSfzUQfNHLjsIhGU_69() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___bIpOcSHJOaNbSfzUQfNHLjsIhGU_69)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_bIpOcSHJOaNbSfzUQfNHLjsIhGU_69() const { return ___bIpOcSHJOaNbSfzUQfNHLjsIhGU_69; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_bIpOcSHJOaNbSfzUQfNHLjsIhGU_69() { return &___bIpOcSHJOaNbSfzUQfNHLjsIhGU_69; }
	inline void set_bIpOcSHJOaNbSfzUQfNHLjsIhGU_69(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___bIpOcSHJOaNbSfzUQfNHLjsIhGU_69 = value;
		Il2CppCodeGenWriteBarrier((&___bIpOcSHJOaNbSfzUQfNHLjsIhGU_69), value);
	}

	inline static int32_t get_offset_of_SeKbgRoSSdxvLKKSFGtzsUJVIEq_70() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___SeKbgRoSSdxvLKKSFGtzsUJVIEq_70)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_SeKbgRoSSdxvLKKSFGtzsUJVIEq_70() const { return ___SeKbgRoSSdxvLKKSFGtzsUJVIEq_70; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_SeKbgRoSSdxvLKKSFGtzsUJVIEq_70() { return &___SeKbgRoSSdxvLKKSFGtzsUJVIEq_70; }
	inline void set_SeKbgRoSSdxvLKKSFGtzsUJVIEq_70(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___SeKbgRoSSdxvLKKSFGtzsUJVIEq_70 = value;
		Il2CppCodeGenWriteBarrier((&___SeKbgRoSSdxvLKKSFGtzsUJVIEq_70), value);
	}

	inline static int32_t get_offset_of_jqUTJMOeCBaPwenidONjsgqqmhF_71() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___jqUTJMOeCBaPwenidONjsgqqmhF_71)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_jqUTJMOeCBaPwenidONjsgqqmhF_71() const { return ___jqUTJMOeCBaPwenidONjsgqqmhF_71; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_jqUTJMOeCBaPwenidONjsgqqmhF_71() { return &___jqUTJMOeCBaPwenidONjsgqqmhF_71; }
	inline void set_jqUTJMOeCBaPwenidONjsgqqmhF_71(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___jqUTJMOeCBaPwenidONjsgqqmhF_71 = value;
		Il2CppCodeGenWriteBarrier((&___jqUTJMOeCBaPwenidONjsgqqmhF_71), value);
	}

	inline static int32_t get_offset_of_VxgfIwaEpxChiqoBUxMyAXuOvzah_72() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___VxgfIwaEpxChiqoBUxMyAXuOvzah_72)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_VxgfIwaEpxChiqoBUxMyAXuOvzah_72() const { return ___VxgfIwaEpxChiqoBUxMyAXuOvzah_72; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_VxgfIwaEpxChiqoBUxMyAXuOvzah_72() { return &___VxgfIwaEpxChiqoBUxMyAXuOvzah_72; }
	inline void set_VxgfIwaEpxChiqoBUxMyAXuOvzah_72(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___VxgfIwaEpxChiqoBUxMyAXuOvzah_72 = value;
		Il2CppCodeGenWriteBarrier((&___VxgfIwaEpxChiqoBUxMyAXuOvzah_72), value);
	}

	inline static int32_t get_offset_of_yGfaFeDTgjQCiyfEnFKwcIrTEKK_73() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___yGfaFeDTgjQCiyfEnFKwcIrTEKK_73)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_yGfaFeDTgjQCiyfEnFKwcIrTEKK_73() const { return ___yGfaFeDTgjQCiyfEnFKwcIrTEKK_73; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_yGfaFeDTgjQCiyfEnFKwcIrTEKK_73() { return &___yGfaFeDTgjQCiyfEnFKwcIrTEKK_73; }
	inline void set_yGfaFeDTgjQCiyfEnFKwcIrTEKK_73(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___yGfaFeDTgjQCiyfEnFKwcIrTEKK_73 = value;
		Il2CppCodeGenWriteBarrier((&___yGfaFeDTgjQCiyfEnFKwcIrTEKK_73), value);
	}

	inline static int32_t get_offset_of_bNWtItxSTpScLiOAwzhSFiRFjTf_74() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___bNWtItxSTpScLiOAwzhSFiRFjTf_74)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_bNWtItxSTpScLiOAwzhSFiRFjTf_74() const { return ___bNWtItxSTpScLiOAwzhSFiRFjTf_74; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_bNWtItxSTpScLiOAwzhSFiRFjTf_74() { return &___bNWtItxSTpScLiOAwzhSFiRFjTf_74; }
	inline void set_bNWtItxSTpScLiOAwzhSFiRFjTf_74(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___bNWtItxSTpScLiOAwzhSFiRFjTf_74 = value;
		Il2CppCodeGenWriteBarrier((&___bNWtItxSTpScLiOAwzhSFiRFjTf_74), value);
	}

	inline static int32_t get_offset_of_NmaaMQBbnFbXDfscDfYsWbItnQyf_75() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___NmaaMQBbnFbXDfscDfYsWbItnQyf_75)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_NmaaMQBbnFbXDfscDfYsWbItnQyf_75() const { return ___NmaaMQBbnFbXDfscDfYsWbItnQyf_75; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_NmaaMQBbnFbXDfscDfYsWbItnQyf_75() { return &___NmaaMQBbnFbXDfscDfYsWbItnQyf_75; }
	inline void set_NmaaMQBbnFbXDfscDfYsWbItnQyf_75(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___NmaaMQBbnFbXDfscDfYsWbItnQyf_75 = value;
		Il2CppCodeGenWriteBarrier((&___NmaaMQBbnFbXDfscDfYsWbItnQyf_75), value);
	}

	inline static int32_t get_offset_of_rMSAIWRRORgnlBNyMWCUdKjLpiE_76() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___rMSAIWRRORgnlBNyMWCUdKjLpiE_76)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_rMSAIWRRORgnlBNyMWCUdKjLpiE_76() const { return ___rMSAIWRRORgnlBNyMWCUdKjLpiE_76; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_rMSAIWRRORgnlBNyMWCUdKjLpiE_76() { return &___rMSAIWRRORgnlBNyMWCUdKjLpiE_76; }
	inline void set_rMSAIWRRORgnlBNyMWCUdKjLpiE_76(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___rMSAIWRRORgnlBNyMWCUdKjLpiE_76 = value;
		Il2CppCodeGenWriteBarrier((&___rMSAIWRRORgnlBNyMWCUdKjLpiE_76), value);
	}

	inline static int32_t get_offset_of_oMtJsfQwbTEDGJiLlRXXMThiarq_77() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___oMtJsfQwbTEDGJiLlRXXMThiarq_77)); }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * get_oMtJsfQwbTEDGJiLlRXXMThiarq_77() const { return ___oMtJsfQwbTEDGJiLlRXXMThiarq_77; }
	inline Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC ** get_address_of_oMtJsfQwbTEDGJiLlRXXMThiarq_77() { return &___oMtJsfQwbTEDGJiLlRXXMThiarq_77; }
	inline void set_oMtJsfQwbTEDGJiLlRXXMThiarq_77(Action_1_t18E730906A964925D355310DF8D8719A7B2CB3FC * value)
	{
		___oMtJsfQwbTEDGJiLlRXXMThiarq_77 = value;
		Il2CppCodeGenWriteBarrier((&___oMtJsfQwbTEDGJiLlRXXMThiarq_77), value);
	}

	inline static int32_t get_offset_of_XiZLABGROWZBxAnjRHXsnpGxypM_78() { return static_cast<int32_t>(offsetof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields, ___XiZLABGROWZBxAnjRHXsnpGxypM_78)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_XiZLABGROWZBxAnjRHXsnpGxypM_78() const { return ___XiZLABGROWZBxAnjRHXsnpGxypM_78; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_XiZLABGROWZBxAnjRHXsnpGxypM_78() { return &___XiZLABGROWZBxAnjRHXsnpGxypM_78; }
	inline void set_XiZLABGROWZBxAnjRHXsnpGxypM_78(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___XiZLABGROWZBxAnjRHXsnpGxypM_78 = value;
		Il2CppCodeGenWriteBarrier((&___XiZLABGROWZBxAnjRHXsnpGxypM_78), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REINPUT_T660C845029E93C5E11722938F53BCA3AED33D0BE_H
#ifndef PS4INPUTSOURCE_TB2AB88BEE18488BC309D02A1DBDCC27D25FB6C90_H
#define PS4INPUTSOURCE_TB2AB88BEE18488BC309D02A1DBDCC27D25FB6C90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource
struct  PS4InputSource_tB2AB88BEE18488BC309D02A1DBDCC27D25FB6C90  : public CustomInputSource_t845A0FA665A9B8FFE97AD4828D69AECDF618DE24
{
public:
	// Rewired.Platforms.PS4.PS4InputSource_YUATmdkIYeDTVeeKKfwolrMGUrN Rewired.Platforms.PS4.PS4InputSource::tNBWxJnZzeOxsqvqkCNPNaShizE
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77 * ___tNBWxJnZzeOxsqvqkCNPNaShizE_7;
	// System.Boolean Rewired.Platforms.PS4.PS4InputSource::DNrDrWYCpwQSRfJdevNzWxrTMZC
	bool ___DNrDrWYCpwQSRfJdevNzWxrTMZC_8;
	// System.Boolean Rewired.Platforms.PS4.PS4InputSource::SeCUoinDywZmqZDHRKupOdOaTke
	bool ___SeCUoinDywZmqZDHRKupOdOaTke_9;

public:
	inline static int32_t get_offset_of_tNBWxJnZzeOxsqvqkCNPNaShizE_7() { return static_cast<int32_t>(offsetof(PS4InputSource_tB2AB88BEE18488BC309D02A1DBDCC27D25FB6C90, ___tNBWxJnZzeOxsqvqkCNPNaShizE_7)); }
	inline YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77 * get_tNBWxJnZzeOxsqvqkCNPNaShizE_7() const { return ___tNBWxJnZzeOxsqvqkCNPNaShizE_7; }
	inline YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77 ** get_address_of_tNBWxJnZzeOxsqvqkCNPNaShizE_7() { return &___tNBWxJnZzeOxsqvqkCNPNaShizE_7; }
	inline void set_tNBWxJnZzeOxsqvqkCNPNaShizE_7(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77 * value)
	{
		___tNBWxJnZzeOxsqvqkCNPNaShizE_7 = value;
		Il2CppCodeGenWriteBarrier((&___tNBWxJnZzeOxsqvqkCNPNaShizE_7), value);
	}

	inline static int32_t get_offset_of_DNrDrWYCpwQSRfJdevNzWxrTMZC_8() { return static_cast<int32_t>(offsetof(PS4InputSource_tB2AB88BEE18488BC309D02A1DBDCC27D25FB6C90, ___DNrDrWYCpwQSRfJdevNzWxrTMZC_8)); }
	inline bool get_DNrDrWYCpwQSRfJdevNzWxrTMZC_8() const { return ___DNrDrWYCpwQSRfJdevNzWxrTMZC_8; }
	inline bool* get_address_of_DNrDrWYCpwQSRfJdevNzWxrTMZC_8() { return &___DNrDrWYCpwQSRfJdevNzWxrTMZC_8; }
	inline void set_DNrDrWYCpwQSRfJdevNzWxrTMZC_8(bool value)
	{
		___DNrDrWYCpwQSRfJdevNzWxrTMZC_8 = value;
	}

	inline static int32_t get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_9() { return static_cast<int32_t>(offsetof(PS4InputSource_tB2AB88BEE18488BC309D02A1DBDCC27D25FB6C90, ___SeCUoinDywZmqZDHRKupOdOaTke_9)); }
	inline bool get_SeCUoinDywZmqZDHRKupOdOaTke_9() const { return ___SeCUoinDywZmqZDHRKupOdOaTke_9; }
	inline bool* get_address_of_SeCUoinDywZmqZDHRKupOdOaTke_9() { return &___SeCUoinDywZmqZDHRKupOdOaTke_9; }
	inline void set_SeCUoinDywZmqZDHRKupOdOaTke_9(bool value)
	{
		___SeCUoinDywZmqZDHRKupOdOaTke_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PS4INPUTSOURCE_TB2AB88BEE18488BC309D02A1DBDCC27D25FB6C90_H
#ifndef MHIEEEEHHZDFUUEMISBITPGSLUVZ_T051AC12ACAF43270100320CE3CDB986F36BD48F5_H
#define MHIEEEEHHZDFUUEMISBITPGSLUVZ_T051AC12ACAF43270100320CE3CDB986F36BD48F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_MHIEeEEHHZDfuuemiSbiTpGsluvZ
struct  MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5  : public ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9
{
public:
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_MHIEeEEHHZDfuuemiSbiTpGsluvZ::bjPvxtoovSXrSHlpyegHEODzGVt
	int32_t ___bjPvxtoovSXrSHlpyegHEODzGVt_37;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_MHIEeEEHHZDfuuemiSbiTpGsluvZ::AgefOoehIcsNBZsKZoqMCdHPCFzJ
	int32_t ___AgefOoehIcsNBZsKZoqMCdHPCFzJ_38;
	// UnityEngine.Vector2 Rewired.Platforms.PS4.PS4InputSource_MHIEeEEHHZDfuuemiSbiTpGsluvZ::qYESuZrdeLezSalCTOegbUPejIXf
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___qYESuZrdeLezSalCTOegbUPejIXf_39;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_MHIEeEEHHZDfuuemiSbiTpGsluvZ::NJKRZHwKouDcJqLmrCPOfPtmlHqs
	int32_t ___NJKRZHwKouDcJqLmrCPOfPtmlHqs_40;
	// UnityEngine.Vector2 Rewired.Platforms.PS4.PS4InputSource_MHIEeEEHHZDfuuemiSbiTpGsluvZ::gdYdzjlNkCDsQpofrEknRDzfFBO
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___gdYdzjlNkCDsQpofrEknRDzfFBO_41;
	// Rewired.Platforms.PS4.PS4InputSource_ReRqQZNVnmKJXmrmWwzirxfUKzM_qqVLgrRtXSDSUCHChUgcEBVcwDd Rewired.Platforms.PS4.PS4InputSource_MHIEeEEHHZDfuuemiSbiTpGsluvZ::baEjPyGDurxMkmegrCRHbTWWahsg
	int32_t ___baEjPyGDurxMkmegrCRHbTWWahsg_42;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_MHIEeEEHHZDfuuemiSbiTpGsluvZ::PpZtFsDtZDwcRLTzDAWCrppZRDH
	int32_t ___PpZtFsDtZDwcRLTzDAWCrppZRDH_43;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_MHIEeEEHHZDfuuemiSbiTpGsluvZ::BuTnUeYdAzWywYrICeHcpuPWosW
	int32_t ___BuTnUeYdAzWywYrICeHcpuPWosW_44;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_MHIEeEEHHZDfuuemiSbiTpGsluvZ::ZnLPPnTEwpcMJyOvWQWcbUJDmpZ
	int32_t ___ZnLPPnTEwpcMJyOvWQWcbUJDmpZ_45;
	// System.Int32 Rewired.Platforms.PS4.PS4InputSource_MHIEeEEHHZDfuuemiSbiTpGsluvZ::coEaqqupSzegPEYvCFWdPsbKfFtY
	int32_t ___coEaqqupSzegPEYvCFWdPsbKfFtY_46;
	// System.Single Rewired.Platforms.PS4.PS4InputSource_MHIEeEEHHZDfuuemiSbiTpGsluvZ::gkeqrFdfxYdjiQGlAFUvoHFqeEQf
	float ___gkeqrFdfxYdjiQGlAFUvoHFqeEQf_47;

public:
	inline static int32_t get_offset_of_bjPvxtoovSXrSHlpyegHEODzGVt_37() { return static_cast<int32_t>(offsetof(MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5, ___bjPvxtoovSXrSHlpyegHEODzGVt_37)); }
	inline int32_t get_bjPvxtoovSXrSHlpyegHEODzGVt_37() const { return ___bjPvxtoovSXrSHlpyegHEODzGVt_37; }
	inline int32_t* get_address_of_bjPvxtoovSXrSHlpyegHEODzGVt_37() { return &___bjPvxtoovSXrSHlpyegHEODzGVt_37; }
	inline void set_bjPvxtoovSXrSHlpyegHEODzGVt_37(int32_t value)
	{
		___bjPvxtoovSXrSHlpyegHEODzGVt_37 = value;
	}

	inline static int32_t get_offset_of_AgefOoehIcsNBZsKZoqMCdHPCFzJ_38() { return static_cast<int32_t>(offsetof(MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5, ___AgefOoehIcsNBZsKZoqMCdHPCFzJ_38)); }
	inline int32_t get_AgefOoehIcsNBZsKZoqMCdHPCFzJ_38() const { return ___AgefOoehIcsNBZsKZoqMCdHPCFzJ_38; }
	inline int32_t* get_address_of_AgefOoehIcsNBZsKZoqMCdHPCFzJ_38() { return &___AgefOoehIcsNBZsKZoqMCdHPCFzJ_38; }
	inline void set_AgefOoehIcsNBZsKZoqMCdHPCFzJ_38(int32_t value)
	{
		___AgefOoehIcsNBZsKZoqMCdHPCFzJ_38 = value;
	}

	inline static int32_t get_offset_of_qYESuZrdeLezSalCTOegbUPejIXf_39() { return static_cast<int32_t>(offsetof(MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5, ___qYESuZrdeLezSalCTOegbUPejIXf_39)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_qYESuZrdeLezSalCTOegbUPejIXf_39() const { return ___qYESuZrdeLezSalCTOegbUPejIXf_39; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_qYESuZrdeLezSalCTOegbUPejIXf_39() { return &___qYESuZrdeLezSalCTOegbUPejIXf_39; }
	inline void set_qYESuZrdeLezSalCTOegbUPejIXf_39(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___qYESuZrdeLezSalCTOegbUPejIXf_39 = value;
	}

	inline static int32_t get_offset_of_NJKRZHwKouDcJqLmrCPOfPtmlHqs_40() { return static_cast<int32_t>(offsetof(MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5, ___NJKRZHwKouDcJqLmrCPOfPtmlHqs_40)); }
	inline int32_t get_NJKRZHwKouDcJqLmrCPOfPtmlHqs_40() const { return ___NJKRZHwKouDcJqLmrCPOfPtmlHqs_40; }
	inline int32_t* get_address_of_NJKRZHwKouDcJqLmrCPOfPtmlHqs_40() { return &___NJKRZHwKouDcJqLmrCPOfPtmlHqs_40; }
	inline void set_NJKRZHwKouDcJqLmrCPOfPtmlHqs_40(int32_t value)
	{
		___NJKRZHwKouDcJqLmrCPOfPtmlHqs_40 = value;
	}

	inline static int32_t get_offset_of_gdYdzjlNkCDsQpofrEknRDzfFBO_41() { return static_cast<int32_t>(offsetof(MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5, ___gdYdzjlNkCDsQpofrEknRDzfFBO_41)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_gdYdzjlNkCDsQpofrEknRDzfFBO_41() const { return ___gdYdzjlNkCDsQpofrEknRDzfFBO_41; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_gdYdzjlNkCDsQpofrEknRDzfFBO_41() { return &___gdYdzjlNkCDsQpofrEknRDzfFBO_41; }
	inline void set_gdYdzjlNkCDsQpofrEknRDzfFBO_41(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___gdYdzjlNkCDsQpofrEknRDzfFBO_41 = value;
	}

	inline static int32_t get_offset_of_baEjPyGDurxMkmegrCRHbTWWahsg_42() { return static_cast<int32_t>(offsetof(MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5, ___baEjPyGDurxMkmegrCRHbTWWahsg_42)); }
	inline int32_t get_baEjPyGDurxMkmegrCRHbTWWahsg_42() const { return ___baEjPyGDurxMkmegrCRHbTWWahsg_42; }
	inline int32_t* get_address_of_baEjPyGDurxMkmegrCRHbTWWahsg_42() { return &___baEjPyGDurxMkmegrCRHbTWWahsg_42; }
	inline void set_baEjPyGDurxMkmegrCRHbTWWahsg_42(int32_t value)
	{
		___baEjPyGDurxMkmegrCRHbTWWahsg_42 = value;
	}

	inline static int32_t get_offset_of_PpZtFsDtZDwcRLTzDAWCrppZRDH_43() { return static_cast<int32_t>(offsetof(MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5, ___PpZtFsDtZDwcRLTzDAWCrppZRDH_43)); }
	inline int32_t get_PpZtFsDtZDwcRLTzDAWCrppZRDH_43() const { return ___PpZtFsDtZDwcRLTzDAWCrppZRDH_43; }
	inline int32_t* get_address_of_PpZtFsDtZDwcRLTzDAWCrppZRDH_43() { return &___PpZtFsDtZDwcRLTzDAWCrppZRDH_43; }
	inline void set_PpZtFsDtZDwcRLTzDAWCrppZRDH_43(int32_t value)
	{
		___PpZtFsDtZDwcRLTzDAWCrppZRDH_43 = value;
	}

	inline static int32_t get_offset_of_BuTnUeYdAzWywYrICeHcpuPWosW_44() { return static_cast<int32_t>(offsetof(MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5, ___BuTnUeYdAzWywYrICeHcpuPWosW_44)); }
	inline int32_t get_BuTnUeYdAzWywYrICeHcpuPWosW_44() const { return ___BuTnUeYdAzWywYrICeHcpuPWosW_44; }
	inline int32_t* get_address_of_BuTnUeYdAzWywYrICeHcpuPWosW_44() { return &___BuTnUeYdAzWywYrICeHcpuPWosW_44; }
	inline void set_BuTnUeYdAzWywYrICeHcpuPWosW_44(int32_t value)
	{
		___BuTnUeYdAzWywYrICeHcpuPWosW_44 = value;
	}

	inline static int32_t get_offset_of_ZnLPPnTEwpcMJyOvWQWcbUJDmpZ_45() { return static_cast<int32_t>(offsetof(MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5, ___ZnLPPnTEwpcMJyOvWQWcbUJDmpZ_45)); }
	inline int32_t get_ZnLPPnTEwpcMJyOvWQWcbUJDmpZ_45() const { return ___ZnLPPnTEwpcMJyOvWQWcbUJDmpZ_45; }
	inline int32_t* get_address_of_ZnLPPnTEwpcMJyOvWQWcbUJDmpZ_45() { return &___ZnLPPnTEwpcMJyOvWQWcbUJDmpZ_45; }
	inline void set_ZnLPPnTEwpcMJyOvWQWcbUJDmpZ_45(int32_t value)
	{
		___ZnLPPnTEwpcMJyOvWQWcbUJDmpZ_45 = value;
	}

	inline static int32_t get_offset_of_coEaqqupSzegPEYvCFWdPsbKfFtY_46() { return static_cast<int32_t>(offsetof(MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5, ___coEaqqupSzegPEYvCFWdPsbKfFtY_46)); }
	inline int32_t get_coEaqqupSzegPEYvCFWdPsbKfFtY_46() const { return ___coEaqqupSzegPEYvCFWdPsbKfFtY_46; }
	inline int32_t* get_address_of_coEaqqupSzegPEYvCFWdPsbKfFtY_46() { return &___coEaqqupSzegPEYvCFWdPsbKfFtY_46; }
	inline void set_coEaqqupSzegPEYvCFWdPsbKfFtY_46(int32_t value)
	{
		___coEaqqupSzegPEYvCFWdPsbKfFtY_46 = value;
	}

	inline static int32_t get_offset_of_gkeqrFdfxYdjiQGlAFUvoHFqeEQf_47() { return static_cast<int32_t>(offsetof(MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5, ___gkeqrFdfxYdjiQGlAFUvoHFqeEQf_47)); }
	inline float get_gkeqrFdfxYdjiQGlAFUvoHFqeEQf_47() const { return ___gkeqrFdfxYdjiQGlAFUvoHFqeEQf_47; }
	inline float* get_address_of_gkeqrFdfxYdjiQGlAFUvoHFqeEQf_47() { return &___gkeqrFdfxYdjiQGlAFUvoHFqeEQf_47; }
	inline void set_gkeqrFdfxYdjiQGlAFUvoHFqeEQf_47(float value)
	{
		___gkeqrFdfxYdjiQGlAFUvoHFqeEQf_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MHIEEEEHHZDFUUEMISBITPGSLUVZ_T051AC12ACAF43270100320CE3CDB986F36BD48F5_H
#ifndef BPQXLAQKOBGXPFQQQILPOXJRGOIR_T20D282424AD47831E3D4341BF71E7F4E2A2DB047_H
#define BPQXLAQKOBGXPFQQQILPOXJRGOIR_T20D282424AD47831E3D4341BF71E7F4E2A2DB047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_bPQXlaQKObGXPfqqqiLPOxJrgoiR
struct  bPQXlaQKObGXPfqqqiLPOxJrgoiR_t20D282424AD47831E3D4341BF71E7F4E2A2DB047  : public ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BPQXLAQKOBGXPFQQQILPOXJRGOIR_T20D282424AD47831E3D4341BF71E7F4E2A2DB047_H
#ifndef PRINUJAJURAPZGSZLWSIFSXLYYN_TA5976993757450A471EAA1103DC16F75049C1A47_H
#define PRINUJAJURAPZGSZLWSIFSXLYYN_TA5976993757450A471EAA1103DC16F75049C1A47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn
struct  PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_ConflictCheckingHelper Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ElementAssignmentConflictCheck Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::RXwTkvaioBOXLvHfDHkciCcOgzY
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___RXwTkvaioBOXLvHfDHkciCcOgzY_4;
	// Rewired.ElementAssignmentConflictCheck Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::CdduzXmnfyAqWtnFBTQwIrsXYmO
	ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  ___CdduzXmnfyAqWtnFBTQwIrsXYmO_5;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_9;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::rvxyBFYAmkGHqKzFluKeiruBUZp
	int32_t ___rvxyBFYAmkGHqKzFluKeiruBUZp_10;
	// Rewired.CustomController Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::oNRWjZDkucmsChJTOgREkFFOgDDs
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * ___oNRWjZDkucmsChJTOgREkFFOgDDs_11;
	// Rewired.ElementAssignmentConflictInfo Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::wyQBbpduedskimtfaAIDTYGcbMj
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___wyQBbpduedskimtfaAIDTYGcbMj_12;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.Player_ControllerHelper_ConflictCheckingHelper_PRiNUjaJuRApzgsZLWSifSXlYyn::JrhvhCqJGVNbbEyRvWwwCXHomvh
	RuntimeObject* ___JrhvhCqJGVNbbEyRvWwwCXHomvh_13;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_4() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___RXwTkvaioBOXLvHfDHkciCcOgzY_4)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_RXwTkvaioBOXLvHfDHkciCcOgzY_4() const { return ___RXwTkvaioBOXLvHfDHkciCcOgzY_4; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_RXwTkvaioBOXLvHfDHkciCcOgzY_4() { return &___RXwTkvaioBOXLvHfDHkciCcOgzY_4; }
	inline void set_RXwTkvaioBOXLvHfDHkciCcOgzY_4(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___RXwTkvaioBOXLvHfDHkciCcOgzY_4 = value;
	}

	inline static int32_t get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_5() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___CdduzXmnfyAqWtnFBTQwIrsXYmO_5)); }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  get_CdduzXmnfyAqWtnFBTQwIrsXYmO_5() const { return ___CdduzXmnfyAqWtnFBTQwIrsXYmO_5; }
	inline ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4 * get_address_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_5() { return &___CdduzXmnfyAqWtnFBTQwIrsXYmO_5; }
	inline void set_CdduzXmnfyAqWtnFBTQwIrsXYmO_5(ElementAssignmentConflictCheck_tA6500DB2E0D80B5AFD7B6B52DEF59ECFA4585FE4  value)
	{
		___CdduzXmnfyAqWtnFBTQwIrsXYmO_5 = value;
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_6; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_6 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_7 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_9() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___CVShsOfomYgQbeMNmgtRkoanKAb_9)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_9() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_9; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_9() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_9; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_9(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_9 = value;
	}

	inline static int32_t get_offset_of_rvxyBFYAmkGHqKzFluKeiruBUZp_10() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___rvxyBFYAmkGHqKzFluKeiruBUZp_10)); }
	inline int32_t get_rvxyBFYAmkGHqKzFluKeiruBUZp_10() const { return ___rvxyBFYAmkGHqKzFluKeiruBUZp_10; }
	inline int32_t* get_address_of_rvxyBFYAmkGHqKzFluKeiruBUZp_10() { return &___rvxyBFYAmkGHqKzFluKeiruBUZp_10; }
	inline void set_rvxyBFYAmkGHqKzFluKeiruBUZp_10(int32_t value)
	{
		___rvxyBFYAmkGHqKzFluKeiruBUZp_10 = value;
	}

	inline static int32_t get_offset_of_oNRWjZDkucmsChJTOgREkFFOgDDs_11() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___oNRWjZDkucmsChJTOgREkFFOgDDs_11)); }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * get_oNRWjZDkucmsChJTOgREkFFOgDDs_11() const { return ___oNRWjZDkucmsChJTOgREkFFOgDDs_11; }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A ** get_address_of_oNRWjZDkucmsChJTOgREkFFOgDDs_11() { return &___oNRWjZDkucmsChJTOgREkFFOgDDs_11; }
	inline void set_oNRWjZDkucmsChJTOgREkFFOgDDs_11(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * value)
	{
		___oNRWjZDkucmsChJTOgREkFFOgDDs_11 = value;
		Il2CppCodeGenWriteBarrier((&___oNRWjZDkucmsChJTOgREkFFOgDDs_11), value);
	}

	inline static int32_t get_offset_of_wyQBbpduedskimtfaAIDTYGcbMj_12() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___wyQBbpduedskimtfaAIDTYGcbMj_12)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_wyQBbpduedskimtfaAIDTYGcbMj_12() const { return ___wyQBbpduedskimtfaAIDTYGcbMj_12; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_wyQBbpduedskimtfaAIDTYGcbMj_12() { return &___wyQBbpduedskimtfaAIDTYGcbMj_12; }
	inline void set_wyQBbpduedskimtfaAIDTYGcbMj_12(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___wyQBbpduedskimtfaAIDTYGcbMj_12 = value;
	}

	inline static int32_t get_offset_of_JrhvhCqJGVNbbEyRvWwwCXHomvh_13() { return static_cast<int32_t>(offsetof(PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47, ___JrhvhCqJGVNbbEyRvWwwCXHomvh_13)); }
	inline RuntimeObject* get_JrhvhCqJGVNbbEyRvWwwCXHomvh_13() const { return ___JrhvhCqJGVNbbEyRvWwwCXHomvh_13; }
	inline RuntimeObject** get_address_of_JrhvhCqJGVNbbEyRvWwwCXHomvh_13() { return &___JrhvhCqJGVNbbEyRvWwwCXHomvh_13; }
	inline void set_JrhvhCqJGVNbbEyRvWwwCXHomvh_13(RuntimeObject* value)
	{
		___JrhvhCqJGVNbbEyRvWwwCXHomvh_13 = value;
		Il2CppCodeGenWriteBarrier((&___JrhvhCqJGVNbbEyRvWwwCXHomvh_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRINUJAJURAPZGSZLWSIFSXLYYN_TA5976993757450A471EAA1103DC16F75049C1A47_H
#ifndef GFFEHXUPIXQQJXJMSEVLORWENLC_TBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB_H
#define GFFEHXUPIXQQJXJMSEVLORWENLC_TBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC
struct  gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB  : public RuntimeObject
{
public:
	// Rewired.ElementAssignmentConflictInfo Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::NOArrsNdfKDShNFftfbPqYDzhpq
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_ConflictCheckingHelper Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::DKaFTWTMhFeLQHDExnDMRxZfmROL
	ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::omdPDTkbMQMCXIvHqkpJkHTSFLE
	int32_t ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::eVNodIsBtpdoajQYgHVzEGonurY
	int32_t ___eVNodIsBtpdoajQYgHVzEGonurY_5;
	// Rewired.CustomControllerMap Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::WigXBlhRQEAAKnPZjEeeNdMahyT
	CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * ___WigXBlhRQEAAKnPZjEeeNdMahyT_6;
	// Rewired.CustomControllerMap Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::hqJogBNPbbjuJUDUhZmRhiFNzfx
	CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * ___hqJogBNPbbjuJUDUhZmRhiFNzfx_7;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::OFOCHvgjhTiUuiMSfLxKRWhcDSN
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_8;
	// Rewired.ActionElementMap Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::ISUlQumxWpBMGxJqFBsCyVmpAjn
	ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * ___ISUlQumxWpBMGxJqFBsCyVmpAjn_9;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::TwaNOMlGNtAEaMSLqisoJEBMSTs
	bool ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::EVKdxtFZCFeOvEumfRBcIsqIqNHp
	bool ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::zGyjKmKFfNgNUhYcVkNPQvxOonWZ
	bool ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12;
	// System.Boolean Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::CVShsOfomYgQbeMNmgtRkoanKAb
	bool ___CVShsOfomYgQbeMNmgtRkoanKAb_13;
	// System.Int32 Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::sTmefogCpWKjKroxmlenFSmgOEmM
	int32_t ___sTmefogCpWKjKroxmlenFSmgOEmM_14;
	// Rewired.CustomController Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::iVIqjwgIJSdpAynaPZewlFkjlkm
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * ___iVIqjwgIJSdpAynaPZewlFkjlkm_15;
	// Rewired.ElementAssignmentConflictInfo Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::SuFUCNCfYLzmhEAzNVkEiSskSba
	ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  ___SuFUCNCfYLzmhEAzNVkEiSskSba_16;
	// System.Collections.Generic.IEnumerator`1<Rewired.ElementAssignmentConflictInfo> Rewired.Player_ControllerHelper_ConflictCheckingHelper_gfFEHxupIxqqjXJMSeVLorWenLC::MMaCKxbsSsTsyTnnKtJqCDuwolw
	RuntimeObject* ___MMaCKxbsSsTsyTnnKtJqCDuwolw_17;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(ConflictCheckingHelper_t3B5BA120121109EB2736D5466BA25742F049F1B1 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4)); }
	inline int32_t get_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() const { return ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline int32_t* get_address_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return &___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline void set_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(int32_t value)
	{
		___omdPDTkbMQMCXIvHqkpJkHTSFLE_4 = value;
	}

	inline static int32_t get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___eVNodIsBtpdoajQYgHVzEGonurY_5)); }
	inline int32_t get_eVNodIsBtpdoajQYgHVzEGonurY_5() const { return ___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline int32_t* get_address_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return &___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline void set_eVNodIsBtpdoajQYgHVzEGonurY_5(int32_t value)
	{
		___eVNodIsBtpdoajQYgHVzEGonurY_5 = value;
	}

	inline static int32_t get_offset_of_WigXBlhRQEAAKnPZjEeeNdMahyT_6() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___WigXBlhRQEAAKnPZjEeeNdMahyT_6)); }
	inline CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * get_WigXBlhRQEAAKnPZjEeeNdMahyT_6() const { return ___WigXBlhRQEAAKnPZjEeeNdMahyT_6; }
	inline CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A ** get_address_of_WigXBlhRQEAAKnPZjEeeNdMahyT_6() { return &___WigXBlhRQEAAKnPZjEeeNdMahyT_6; }
	inline void set_WigXBlhRQEAAKnPZjEeeNdMahyT_6(CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * value)
	{
		___WigXBlhRQEAAKnPZjEeeNdMahyT_6 = value;
		Il2CppCodeGenWriteBarrier((&___WigXBlhRQEAAKnPZjEeeNdMahyT_6), value);
	}

	inline static int32_t get_offset_of_hqJogBNPbbjuJUDUhZmRhiFNzfx_7() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___hqJogBNPbbjuJUDUhZmRhiFNzfx_7)); }
	inline CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * get_hqJogBNPbbjuJUDUhZmRhiFNzfx_7() const { return ___hqJogBNPbbjuJUDUhZmRhiFNzfx_7; }
	inline CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A ** get_address_of_hqJogBNPbbjuJUDUhZmRhiFNzfx_7() { return &___hqJogBNPbbjuJUDUhZmRhiFNzfx_7; }
	inline void set_hqJogBNPbbjuJUDUhZmRhiFNzfx_7(CustomControllerMap_t014AB1F2888502EFF0FF5351FADBA1A05716165A * value)
	{
		___hqJogBNPbbjuJUDUhZmRhiFNzfx_7 = value;
		Il2CppCodeGenWriteBarrier((&___hqJogBNPbbjuJUDUhZmRhiFNzfx_7), value);
	}

	inline static int32_t get_offset_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_8() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_8)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_OFOCHvgjhTiUuiMSfLxKRWhcDSN_8() const { return ___OFOCHvgjhTiUuiMSfLxKRWhcDSN_8; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_8() { return &___OFOCHvgjhTiUuiMSfLxKRWhcDSN_8; }
	inline void set_OFOCHvgjhTiUuiMSfLxKRWhcDSN_8(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___OFOCHvgjhTiUuiMSfLxKRWhcDSN_8 = value;
		Il2CppCodeGenWriteBarrier((&___OFOCHvgjhTiUuiMSfLxKRWhcDSN_8), value);
	}

	inline static int32_t get_offset_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_9() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___ISUlQumxWpBMGxJqFBsCyVmpAjn_9)); }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * get_ISUlQumxWpBMGxJqFBsCyVmpAjn_9() const { return ___ISUlQumxWpBMGxJqFBsCyVmpAjn_9; }
	inline ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 ** get_address_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_9() { return &___ISUlQumxWpBMGxJqFBsCyVmpAjn_9; }
	inline void set_ISUlQumxWpBMGxJqFBsCyVmpAjn_9(ActionElementMap_t2BD8E4E67A80A924EAE809AAA6A0DD843A3459C7 * value)
	{
		___ISUlQumxWpBMGxJqFBsCyVmpAjn_9 = value;
		Il2CppCodeGenWriteBarrier((&___ISUlQumxWpBMGxJqFBsCyVmpAjn_9), value);
	}

	inline static int32_t get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10)); }
	inline bool get_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() const { return ___TwaNOMlGNtAEaMSLqisoJEBMSTs_10; }
	inline bool* get_address_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10() { return &___TwaNOMlGNtAEaMSLqisoJEBMSTs_10; }
	inline void set_TwaNOMlGNtAEaMSLqisoJEBMSTs_10(bool value)
	{
		___TwaNOMlGNtAEaMSLqisoJEBMSTs_10 = value;
	}

	inline static int32_t get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11)); }
	inline bool get_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() const { return ___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11; }
	inline bool* get_address_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11() { return &___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11; }
	inline void set_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11(bool value)
	{
		___EVKdxtFZCFeOvEumfRBcIsqIqNHp_11 = value;
	}

	inline static int32_t get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12)); }
	inline bool get_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12() const { return ___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12; }
	inline bool* get_address_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12() { return &___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12; }
	inline void set_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12(bool value)
	{
		___zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12 = value;
	}

	inline static int32_t get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_13() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___CVShsOfomYgQbeMNmgtRkoanKAb_13)); }
	inline bool get_CVShsOfomYgQbeMNmgtRkoanKAb_13() const { return ___CVShsOfomYgQbeMNmgtRkoanKAb_13; }
	inline bool* get_address_of_CVShsOfomYgQbeMNmgtRkoanKAb_13() { return &___CVShsOfomYgQbeMNmgtRkoanKAb_13; }
	inline void set_CVShsOfomYgQbeMNmgtRkoanKAb_13(bool value)
	{
		___CVShsOfomYgQbeMNmgtRkoanKAb_13 = value;
	}

	inline static int32_t get_offset_of_sTmefogCpWKjKroxmlenFSmgOEmM_14() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___sTmefogCpWKjKroxmlenFSmgOEmM_14)); }
	inline int32_t get_sTmefogCpWKjKroxmlenFSmgOEmM_14() const { return ___sTmefogCpWKjKroxmlenFSmgOEmM_14; }
	inline int32_t* get_address_of_sTmefogCpWKjKroxmlenFSmgOEmM_14() { return &___sTmefogCpWKjKroxmlenFSmgOEmM_14; }
	inline void set_sTmefogCpWKjKroxmlenFSmgOEmM_14(int32_t value)
	{
		___sTmefogCpWKjKroxmlenFSmgOEmM_14 = value;
	}

	inline static int32_t get_offset_of_iVIqjwgIJSdpAynaPZewlFkjlkm_15() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___iVIqjwgIJSdpAynaPZewlFkjlkm_15)); }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * get_iVIqjwgIJSdpAynaPZewlFkjlkm_15() const { return ___iVIqjwgIJSdpAynaPZewlFkjlkm_15; }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A ** get_address_of_iVIqjwgIJSdpAynaPZewlFkjlkm_15() { return &___iVIqjwgIJSdpAynaPZewlFkjlkm_15; }
	inline void set_iVIqjwgIJSdpAynaPZewlFkjlkm_15(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * value)
	{
		___iVIqjwgIJSdpAynaPZewlFkjlkm_15 = value;
		Il2CppCodeGenWriteBarrier((&___iVIqjwgIJSdpAynaPZewlFkjlkm_15), value);
	}

	inline static int32_t get_offset_of_SuFUCNCfYLzmhEAzNVkEiSskSba_16() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___SuFUCNCfYLzmhEAzNVkEiSskSba_16)); }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  get_SuFUCNCfYLzmhEAzNVkEiSskSba_16() const { return ___SuFUCNCfYLzmhEAzNVkEiSskSba_16; }
	inline ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D * get_address_of_SuFUCNCfYLzmhEAzNVkEiSskSba_16() { return &___SuFUCNCfYLzmhEAzNVkEiSskSba_16; }
	inline void set_SuFUCNCfYLzmhEAzNVkEiSskSba_16(ElementAssignmentConflictInfo_tF4C8FFD6F770D31AC5113AFEAFE6A344BC3ED33D  value)
	{
		___SuFUCNCfYLzmhEAzNVkEiSskSba_16 = value;
	}

	inline static int32_t get_offset_of_MMaCKxbsSsTsyTnnKtJqCDuwolw_17() { return static_cast<int32_t>(offsetof(gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB, ___MMaCKxbsSsTsyTnnKtJqCDuwolw_17)); }
	inline RuntimeObject* get_MMaCKxbsSsTsyTnnKtJqCDuwolw_17() const { return ___MMaCKxbsSsTsyTnnKtJqCDuwolw_17; }
	inline RuntimeObject** get_address_of_MMaCKxbsSsTsyTnnKtJqCDuwolw_17() { return &___MMaCKxbsSsTsyTnnKtJqCDuwolw_17; }
	inline void set_MMaCKxbsSsTsyTnnKtJqCDuwolw_17(RuntimeObject* value)
	{
		___MMaCKxbsSsTsyTnnKtJqCDuwolw_17 = value;
		Il2CppCodeGenWriteBarrier((&___MMaCKxbsSsTsyTnnKtJqCDuwolw_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GFFEHXUPIXQQJXJMSEVLORWENLC_TBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB_H
#ifndef AKYQKVUIKXSICXJHRKENBMRWQTU_T5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B_H
#define AKYQKVUIKXSICXJHRKENBMRWQTU_T5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_AKyQkVUIkXSicXJHRKEnBmrwqtU
struct  AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_AKyQkVUIkXSicXJHRKEnBmrwqtU::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_AKyQkVUIkXSicXJHRKEnBmrwqtU::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_AKyQkVUIkXSicXJHRKEnBmrwqtU::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_AKyQkVUIkXSicXJHRKEnBmrwqtU::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_AKyQkVUIkXSicXJHRKEnBmrwqtU::hMBbQHSYpxrZxtjFPBFvFGsQNmg
	int32_t ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_AKyQkVUIkXSicXJHRKEnBmrwqtU::piAnfnMqsmhCZfWFjwHhSJYtiKf
	int32_t ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5;
	// Rewired.Joystick Rewired.Player_ControllerHelper_PollingHelper_AKyQkVUIkXSicXJHRKEnBmrwqtU::LXZSgbgtnApwPMGxdeZBLyXVpWq
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___LXZSgbgtnApwPMGxdeZBLyXVpWq_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_AKyQkVUIkXSicXJHRKEnBmrwqtU::cuaaPPKOqGXjshzgYnqbpedMFcDM
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___cuaaPPKOqGXjshzgYnqbpedMFcDM_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_AKyQkVUIkXSicXJHRKEnBmrwqtU::izufmMtxxrCqQERuemqPKAlSNTjf
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___izufmMtxxrCqQERuemqPKAlSNTjf_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_AKyQkVUIkXSicXJHRKEnBmrwqtU::JRBBXCmTHnFgcSOfjDuXshzmCDn
	RuntimeObject* ___JRBBXCmTHnFgcSOfjDuXshzmCDn_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return static_cast<int32_t>(offsetof(AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B, ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4)); }
	inline int32_t get_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() const { return ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline int32_t* get_address_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return &___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline void set_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(int32_t value)
	{
		___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4 = value;
	}

	inline static int32_t get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return static_cast<int32_t>(offsetof(AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B, ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5)); }
	inline int32_t get_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() const { return ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline int32_t* get_address_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return &___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline void set_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(int32_t value)
	{
		___piAnfnMqsmhCZfWFjwHhSJYtiKf_5 = value;
	}

	inline static int32_t get_offset_of_LXZSgbgtnApwPMGxdeZBLyXVpWq_6() { return static_cast<int32_t>(offsetof(AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B, ___LXZSgbgtnApwPMGxdeZBLyXVpWq_6)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_LXZSgbgtnApwPMGxdeZBLyXVpWq_6() const { return ___LXZSgbgtnApwPMGxdeZBLyXVpWq_6; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_LXZSgbgtnApwPMGxdeZBLyXVpWq_6() { return &___LXZSgbgtnApwPMGxdeZBLyXVpWq_6; }
	inline void set_LXZSgbgtnApwPMGxdeZBLyXVpWq_6(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___LXZSgbgtnApwPMGxdeZBLyXVpWq_6 = value;
		Il2CppCodeGenWriteBarrier((&___LXZSgbgtnApwPMGxdeZBLyXVpWq_6), value);
	}

	inline static int32_t get_offset_of_cuaaPPKOqGXjshzgYnqbpedMFcDM_7() { return static_cast<int32_t>(offsetof(AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B, ___cuaaPPKOqGXjshzgYnqbpedMFcDM_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_cuaaPPKOqGXjshzgYnqbpedMFcDM_7() const { return ___cuaaPPKOqGXjshzgYnqbpedMFcDM_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_cuaaPPKOqGXjshzgYnqbpedMFcDM_7() { return &___cuaaPPKOqGXjshzgYnqbpedMFcDM_7; }
	inline void set_cuaaPPKOqGXjshzgYnqbpedMFcDM_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___cuaaPPKOqGXjshzgYnqbpedMFcDM_7 = value;
	}

	inline static int32_t get_offset_of_izufmMtxxrCqQERuemqPKAlSNTjf_8() { return static_cast<int32_t>(offsetof(AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B, ___izufmMtxxrCqQERuemqPKAlSNTjf_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_izufmMtxxrCqQERuemqPKAlSNTjf_8() const { return ___izufmMtxxrCqQERuemqPKAlSNTjf_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_izufmMtxxrCqQERuemqPKAlSNTjf_8() { return &___izufmMtxxrCqQERuemqPKAlSNTjf_8; }
	inline void set_izufmMtxxrCqQERuemqPKAlSNTjf_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___izufmMtxxrCqQERuemqPKAlSNTjf_8 = value;
	}

	inline static int32_t get_offset_of_JRBBXCmTHnFgcSOfjDuXshzmCDn_9() { return static_cast<int32_t>(offsetof(AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B, ___JRBBXCmTHnFgcSOfjDuXshzmCDn_9)); }
	inline RuntimeObject* get_JRBBXCmTHnFgcSOfjDuXshzmCDn_9() const { return ___JRBBXCmTHnFgcSOfjDuXshzmCDn_9; }
	inline RuntimeObject** get_address_of_JRBBXCmTHnFgcSOfjDuXshzmCDn_9() { return &___JRBBXCmTHnFgcSOfjDuXshzmCDn_9; }
	inline void set_JRBBXCmTHnFgcSOfjDuXshzmCDn_9(RuntimeObject* value)
	{
		___JRBBXCmTHnFgcSOfjDuXshzmCDn_9 = value;
		Il2CppCodeGenWriteBarrier((&___JRBBXCmTHnFgcSOfjDuXshzmCDn_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AKYQKVUIKXSICXJHRKENBMRWQTU_T5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B_H
#ifndef HVLDRHIEQVITJEUXEOHLIDCYTGE_T6A0ACFB55C555840C4318716A968D645A4E6B0AA_H
#define HVLDRHIEQVITJEUXEOHLIDCYTGE_T6A0ACFB55C555840C4318716A968D645A4E6B0AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_HvLdRHIeQviTJEuxEohliDcYtge
struct  HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_HvLdRHIeQviTJEuxEohliDcYtge::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_HvLdRHIeQviTJEuxEohliDcYtge::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_HvLdRHIeQviTJEuxEohliDcYtge::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_HvLdRHIeQviTJEuxEohliDcYtge::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Collections.Generic.IList`1<Rewired.Joystick> Rewired.Player_ControllerHelper_PollingHelper_HvLdRHIeQviTJEuxEohliDcYtge::yjlqyyWOozfDvafSPImsgZHdlEC
	RuntimeObject* ___yjlqyyWOozfDvafSPImsgZHdlEC_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_HvLdRHIeQviTJEuxEohliDcYtge::gSpnbjnaCFzSmbyaPfTEsBsEqoD
	int32_t ___gSpnbjnaCFzSmbyaPfTEsBsEqoD_5;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_HvLdRHIeQviTJEuxEohliDcYtge::alECYxDfuAdqiUIgrhwDYaHkDsLa
	int32_t ___alECYxDfuAdqiUIgrhwDYaHkDsLa_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_HvLdRHIeQviTJEuxEohliDcYtge::LkCGWlVEPUdxRhqAwaKDHzIJESI
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___LkCGWlVEPUdxRhqAwaKDHzIJESI_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_HvLdRHIeQviTJEuxEohliDcYtge::ShUoBUkcTxMJNUPwsyJWPpwyGJMA
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___ShUoBUkcTxMJNUPwsyJWPpwyGJMA_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_HvLdRHIeQviTJEuxEohliDcYtge::zsqOmfXuGsEhTBJYNgetctHdxXw
	RuntimeObject* ___zsqOmfXuGsEhTBJYNgetctHdxXw_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_yjlqyyWOozfDvafSPImsgZHdlEC_4() { return static_cast<int32_t>(offsetof(HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA, ___yjlqyyWOozfDvafSPImsgZHdlEC_4)); }
	inline RuntimeObject* get_yjlqyyWOozfDvafSPImsgZHdlEC_4() const { return ___yjlqyyWOozfDvafSPImsgZHdlEC_4; }
	inline RuntimeObject** get_address_of_yjlqyyWOozfDvafSPImsgZHdlEC_4() { return &___yjlqyyWOozfDvafSPImsgZHdlEC_4; }
	inline void set_yjlqyyWOozfDvafSPImsgZHdlEC_4(RuntimeObject* value)
	{
		___yjlqyyWOozfDvafSPImsgZHdlEC_4 = value;
		Il2CppCodeGenWriteBarrier((&___yjlqyyWOozfDvafSPImsgZHdlEC_4), value);
	}

	inline static int32_t get_offset_of_gSpnbjnaCFzSmbyaPfTEsBsEqoD_5() { return static_cast<int32_t>(offsetof(HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA, ___gSpnbjnaCFzSmbyaPfTEsBsEqoD_5)); }
	inline int32_t get_gSpnbjnaCFzSmbyaPfTEsBsEqoD_5() const { return ___gSpnbjnaCFzSmbyaPfTEsBsEqoD_5; }
	inline int32_t* get_address_of_gSpnbjnaCFzSmbyaPfTEsBsEqoD_5() { return &___gSpnbjnaCFzSmbyaPfTEsBsEqoD_5; }
	inline void set_gSpnbjnaCFzSmbyaPfTEsBsEqoD_5(int32_t value)
	{
		___gSpnbjnaCFzSmbyaPfTEsBsEqoD_5 = value;
	}

	inline static int32_t get_offset_of_alECYxDfuAdqiUIgrhwDYaHkDsLa_6() { return static_cast<int32_t>(offsetof(HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA, ___alECYxDfuAdqiUIgrhwDYaHkDsLa_6)); }
	inline int32_t get_alECYxDfuAdqiUIgrhwDYaHkDsLa_6() const { return ___alECYxDfuAdqiUIgrhwDYaHkDsLa_6; }
	inline int32_t* get_address_of_alECYxDfuAdqiUIgrhwDYaHkDsLa_6() { return &___alECYxDfuAdqiUIgrhwDYaHkDsLa_6; }
	inline void set_alECYxDfuAdqiUIgrhwDYaHkDsLa_6(int32_t value)
	{
		___alECYxDfuAdqiUIgrhwDYaHkDsLa_6 = value;
	}

	inline static int32_t get_offset_of_LkCGWlVEPUdxRhqAwaKDHzIJESI_7() { return static_cast<int32_t>(offsetof(HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA, ___LkCGWlVEPUdxRhqAwaKDHzIJESI_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_LkCGWlVEPUdxRhqAwaKDHzIJESI_7() const { return ___LkCGWlVEPUdxRhqAwaKDHzIJESI_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_LkCGWlVEPUdxRhqAwaKDHzIJESI_7() { return &___LkCGWlVEPUdxRhqAwaKDHzIJESI_7; }
	inline void set_LkCGWlVEPUdxRhqAwaKDHzIJESI_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___LkCGWlVEPUdxRhqAwaKDHzIJESI_7 = value;
	}

	inline static int32_t get_offset_of_ShUoBUkcTxMJNUPwsyJWPpwyGJMA_8() { return static_cast<int32_t>(offsetof(HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA, ___ShUoBUkcTxMJNUPwsyJWPpwyGJMA_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_ShUoBUkcTxMJNUPwsyJWPpwyGJMA_8() const { return ___ShUoBUkcTxMJNUPwsyJWPpwyGJMA_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_ShUoBUkcTxMJNUPwsyJWPpwyGJMA_8() { return &___ShUoBUkcTxMJNUPwsyJWPpwyGJMA_8; }
	inline void set_ShUoBUkcTxMJNUPwsyJWPpwyGJMA_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___ShUoBUkcTxMJNUPwsyJWPpwyGJMA_8 = value;
	}

	inline static int32_t get_offset_of_zsqOmfXuGsEhTBJYNgetctHdxXw_9() { return static_cast<int32_t>(offsetof(HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA, ___zsqOmfXuGsEhTBJYNgetctHdxXw_9)); }
	inline RuntimeObject* get_zsqOmfXuGsEhTBJYNgetctHdxXw_9() const { return ___zsqOmfXuGsEhTBJYNgetctHdxXw_9; }
	inline RuntimeObject** get_address_of_zsqOmfXuGsEhTBJYNgetctHdxXw_9() { return &___zsqOmfXuGsEhTBJYNgetctHdxXw_9; }
	inline void set_zsqOmfXuGsEhTBJYNgetctHdxXw_9(RuntimeObject* value)
	{
		___zsqOmfXuGsEhTBJYNgetctHdxXw_9 = value;
		Il2CppCodeGenWriteBarrier((&___zsqOmfXuGsEhTBJYNgetctHdxXw_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HVLDRHIEQVITJEUXEOHLIDCYTGE_T6A0ACFB55C555840C4318716A968D645A4E6B0AA_H
#ifndef MWTYSHGELLMVIAPKKIGYXITMGEO_T2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A_H
#define MWTYSHGELLMVIAPKKIGYXITMGEO_T2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_MWTysHGelLMViAPKkIGyXitmGEO
struct  MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_MWTysHGelLMViAPKkIGyXitmGEO::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_MWTysHGelLMViAPKkIGyXitmGEO::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_MWTysHGelLMViAPKkIGyXitmGEO::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_MWTysHGelLMViAPKkIGyXitmGEO::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Collections.Generic.IList`1<Rewired.CustomController> Rewired.Player_ControllerHelper_PollingHelper_MWTysHGelLMViAPKkIGyXitmGEO::BZcDuWaehMrQqBxjsYKYQcSVhZJX
	RuntimeObject* ___BZcDuWaehMrQqBxjsYKYQcSVhZJX_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_MWTysHGelLMViAPKkIGyXitmGEO::LNVYrrlmellSQmSXzqaXczxYopZ
	int32_t ___LNVYrrlmellSQmSXzqaXczxYopZ_5;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_MWTysHGelLMViAPKkIGyXitmGEO::mzaEkURLsoTtRqjdGqXSeqirFKh
	int32_t ___mzaEkURLsoTtRqjdGqXSeqirFKh_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_MWTysHGelLMViAPKkIGyXitmGEO::aNYOcjFiLfGjadlQfDdEEoMhriRm
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___aNYOcjFiLfGjadlQfDdEEoMhriRm_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_MWTysHGelLMViAPKkIGyXitmGEO::bGTTJRmpDhKUIuVPrIPqROoKdKU
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___bGTTJRmpDhKUIuVPrIPqROoKdKU_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_MWTysHGelLMViAPKkIGyXitmGEO::pDCHARvqClghktOpaTUhurmzDsM
	RuntimeObject* ___pDCHARvqClghktOpaTUhurmzDsM_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_BZcDuWaehMrQqBxjsYKYQcSVhZJX_4() { return static_cast<int32_t>(offsetof(MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A, ___BZcDuWaehMrQqBxjsYKYQcSVhZJX_4)); }
	inline RuntimeObject* get_BZcDuWaehMrQqBxjsYKYQcSVhZJX_4() const { return ___BZcDuWaehMrQqBxjsYKYQcSVhZJX_4; }
	inline RuntimeObject** get_address_of_BZcDuWaehMrQqBxjsYKYQcSVhZJX_4() { return &___BZcDuWaehMrQqBxjsYKYQcSVhZJX_4; }
	inline void set_BZcDuWaehMrQqBxjsYKYQcSVhZJX_4(RuntimeObject* value)
	{
		___BZcDuWaehMrQqBxjsYKYQcSVhZJX_4 = value;
		Il2CppCodeGenWriteBarrier((&___BZcDuWaehMrQqBxjsYKYQcSVhZJX_4), value);
	}

	inline static int32_t get_offset_of_LNVYrrlmellSQmSXzqaXczxYopZ_5() { return static_cast<int32_t>(offsetof(MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A, ___LNVYrrlmellSQmSXzqaXczxYopZ_5)); }
	inline int32_t get_LNVYrrlmellSQmSXzqaXczxYopZ_5() const { return ___LNVYrrlmellSQmSXzqaXczxYopZ_5; }
	inline int32_t* get_address_of_LNVYrrlmellSQmSXzqaXczxYopZ_5() { return &___LNVYrrlmellSQmSXzqaXczxYopZ_5; }
	inline void set_LNVYrrlmellSQmSXzqaXczxYopZ_5(int32_t value)
	{
		___LNVYrrlmellSQmSXzqaXczxYopZ_5 = value;
	}

	inline static int32_t get_offset_of_mzaEkURLsoTtRqjdGqXSeqirFKh_6() { return static_cast<int32_t>(offsetof(MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A, ___mzaEkURLsoTtRqjdGqXSeqirFKh_6)); }
	inline int32_t get_mzaEkURLsoTtRqjdGqXSeqirFKh_6() const { return ___mzaEkURLsoTtRqjdGqXSeqirFKh_6; }
	inline int32_t* get_address_of_mzaEkURLsoTtRqjdGqXSeqirFKh_6() { return &___mzaEkURLsoTtRqjdGqXSeqirFKh_6; }
	inline void set_mzaEkURLsoTtRqjdGqXSeqirFKh_6(int32_t value)
	{
		___mzaEkURLsoTtRqjdGqXSeqirFKh_6 = value;
	}

	inline static int32_t get_offset_of_aNYOcjFiLfGjadlQfDdEEoMhriRm_7() { return static_cast<int32_t>(offsetof(MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A, ___aNYOcjFiLfGjadlQfDdEEoMhriRm_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_aNYOcjFiLfGjadlQfDdEEoMhriRm_7() const { return ___aNYOcjFiLfGjadlQfDdEEoMhriRm_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_aNYOcjFiLfGjadlQfDdEEoMhriRm_7() { return &___aNYOcjFiLfGjadlQfDdEEoMhriRm_7; }
	inline void set_aNYOcjFiLfGjadlQfDdEEoMhriRm_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___aNYOcjFiLfGjadlQfDdEEoMhriRm_7 = value;
	}

	inline static int32_t get_offset_of_bGTTJRmpDhKUIuVPrIPqROoKdKU_8() { return static_cast<int32_t>(offsetof(MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A, ___bGTTJRmpDhKUIuVPrIPqROoKdKU_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_bGTTJRmpDhKUIuVPrIPqROoKdKU_8() const { return ___bGTTJRmpDhKUIuVPrIPqROoKdKU_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_bGTTJRmpDhKUIuVPrIPqROoKdKU_8() { return &___bGTTJRmpDhKUIuVPrIPqROoKdKU_8; }
	inline void set_bGTTJRmpDhKUIuVPrIPqROoKdKU_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___bGTTJRmpDhKUIuVPrIPqROoKdKU_8 = value;
	}

	inline static int32_t get_offset_of_pDCHARvqClghktOpaTUhurmzDsM_9() { return static_cast<int32_t>(offsetof(MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A, ___pDCHARvqClghktOpaTUhurmzDsM_9)); }
	inline RuntimeObject* get_pDCHARvqClghktOpaTUhurmzDsM_9() const { return ___pDCHARvqClghktOpaTUhurmzDsM_9; }
	inline RuntimeObject** get_address_of_pDCHARvqClghktOpaTUhurmzDsM_9() { return &___pDCHARvqClghktOpaTUhurmzDsM_9; }
	inline void set_pDCHARvqClghktOpaTUhurmzDsM_9(RuntimeObject* value)
	{
		___pDCHARvqClghktOpaTUhurmzDsM_9 = value;
		Il2CppCodeGenWriteBarrier((&___pDCHARvqClghktOpaTUhurmzDsM_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MWTYSHGELLMVIAPKKIGYXITMGEO_T2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A_H
#ifndef NACCVOMPIZOBGZKCXYKAFOMHLNE_T022B95CDD88767E9F6406469FC895ED2BD2B7AE6_H
#define NACCVOMPIZOBGZKCXYKAFOMHLNE_T022B95CDD88767E9F6406469FC895ED2BD2B7AE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_NAcCVompizOBGZKcXYKAfoMhlNe
struct  NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_NAcCVompizOBGZKcXYKAfoMhlNe::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_NAcCVompizOBGZKcXYKAfoMhlNe::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_NAcCVompizOBGZKcXYKAfoMhlNe::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_NAcCVompizOBGZKcXYKAfoMhlNe::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Collections.Generic.IList`1<Rewired.Joystick> Rewired.Player_ControllerHelper_PollingHelper_NAcCVompizOBGZKcXYKAfoMhlNe::zUYqedeSKMBBfryhyEpmNaaaVuo
	RuntimeObject* ___zUYqedeSKMBBfryhyEpmNaaaVuo_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_NAcCVompizOBGZKcXYKAfoMhlNe::LaRHWkTAdhzreLHvmkAbWTlUJZk
	int32_t ___LaRHWkTAdhzreLHvmkAbWTlUJZk_5;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_NAcCVompizOBGZKcXYKAfoMhlNe::ViivwZdGtAXPpovrLEdNHfArqOx
	int32_t ___ViivwZdGtAXPpovrLEdNHfArqOx_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_NAcCVompizOBGZKcXYKAfoMhlNe::OERkBDITbWLHsYxuqwldzTjElYa
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___OERkBDITbWLHsYxuqwldzTjElYa_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_NAcCVompizOBGZKcXYKAfoMhlNe::GtuVdHdHWCZSIszFGYtvHAmbNQg
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___GtuVdHdHWCZSIszFGYtvHAmbNQg_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_NAcCVompizOBGZKcXYKAfoMhlNe::UsxcWtJqQRUlMOmLGyXKGxLeumB
	RuntimeObject* ___UsxcWtJqQRUlMOmLGyXKGxLeumB_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_zUYqedeSKMBBfryhyEpmNaaaVuo_4() { return static_cast<int32_t>(offsetof(NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6, ___zUYqedeSKMBBfryhyEpmNaaaVuo_4)); }
	inline RuntimeObject* get_zUYqedeSKMBBfryhyEpmNaaaVuo_4() const { return ___zUYqedeSKMBBfryhyEpmNaaaVuo_4; }
	inline RuntimeObject** get_address_of_zUYqedeSKMBBfryhyEpmNaaaVuo_4() { return &___zUYqedeSKMBBfryhyEpmNaaaVuo_4; }
	inline void set_zUYqedeSKMBBfryhyEpmNaaaVuo_4(RuntimeObject* value)
	{
		___zUYqedeSKMBBfryhyEpmNaaaVuo_4 = value;
		Il2CppCodeGenWriteBarrier((&___zUYqedeSKMBBfryhyEpmNaaaVuo_4), value);
	}

	inline static int32_t get_offset_of_LaRHWkTAdhzreLHvmkAbWTlUJZk_5() { return static_cast<int32_t>(offsetof(NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6, ___LaRHWkTAdhzreLHvmkAbWTlUJZk_5)); }
	inline int32_t get_LaRHWkTAdhzreLHvmkAbWTlUJZk_5() const { return ___LaRHWkTAdhzreLHvmkAbWTlUJZk_5; }
	inline int32_t* get_address_of_LaRHWkTAdhzreLHvmkAbWTlUJZk_5() { return &___LaRHWkTAdhzreLHvmkAbWTlUJZk_5; }
	inline void set_LaRHWkTAdhzreLHvmkAbWTlUJZk_5(int32_t value)
	{
		___LaRHWkTAdhzreLHvmkAbWTlUJZk_5 = value;
	}

	inline static int32_t get_offset_of_ViivwZdGtAXPpovrLEdNHfArqOx_6() { return static_cast<int32_t>(offsetof(NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6, ___ViivwZdGtAXPpovrLEdNHfArqOx_6)); }
	inline int32_t get_ViivwZdGtAXPpovrLEdNHfArqOx_6() const { return ___ViivwZdGtAXPpovrLEdNHfArqOx_6; }
	inline int32_t* get_address_of_ViivwZdGtAXPpovrLEdNHfArqOx_6() { return &___ViivwZdGtAXPpovrLEdNHfArqOx_6; }
	inline void set_ViivwZdGtAXPpovrLEdNHfArqOx_6(int32_t value)
	{
		___ViivwZdGtAXPpovrLEdNHfArqOx_6 = value;
	}

	inline static int32_t get_offset_of_OERkBDITbWLHsYxuqwldzTjElYa_7() { return static_cast<int32_t>(offsetof(NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6, ___OERkBDITbWLHsYxuqwldzTjElYa_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_OERkBDITbWLHsYxuqwldzTjElYa_7() const { return ___OERkBDITbWLHsYxuqwldzTjElYa_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_OERkBDITbWLHsYxuqwldzTjElYa_7() { return &___OERkBDITbWLHsYxuqwldzTjElYa_7; }
	inline void set_OERkBDITbWLHsYxuqwldzTjElYa_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___OERkBDITbWLHsYxuqwldzTjElYa_7 = value;
	}

	inline static int32_t get_offset_of_GtuVdHdHWCZSIszFGYtvHAmbNQg_8() { return static_cast<int32_t>(offsetof(NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6, ___GtuVdHdHWCZSIszFGYtvHAmbNQg_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_GtuVdHdHWCZSIszFGYtvHAmbNQg_8() const { return ___GtuVdHdHWCZSIszFGYtvHAmbNQg_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_GtuVdHdHWCZSIszFGYtvHAmbNQg_8() { return &___GtuVdHdHWCZSIszFGYtvHAmbNQg_8; }
	inline void set_GtuVdHdHWCZSIszFGYtvHAmbNQg_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___GtuVdHdHWCZSIszFGYtvHAmbNQg_8 = value;
	}

	inline static int32_t get_offset_of_UsxcWtJqQRUlMOmLGyXKGxLeumB_9() { return static_cast<int32_t>(offsetof(NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6, ___UsxcWtJqQRUlMOmLGyXKGxLeumB_9)); }
	inline RuntimeObject* get_UsxcWtJqQRUlMOmLGyXKGxLeumB_9() const { return ___UsxcWtJqQRUlMOmLGyXKGxLeumB_9; }
	inline RuntimeObject** get_address_of_UsxcWtJqQRUlMOmLGyXKGxLeumB_9() { return &___UsxcWtJqQRUlMOmLGyXKGxLeumB_9; }
	inline void set_UsxcWtJqQRUlMOmLGyXKGxLeumB_9(RuntimeObject* value)
	{
		___UsxcWtJqQRUlMOmLGyXKGxLeumB_9 = value;
		Il2CppCodeGenWriteBarrier((&___UsxcWtJqQRUlMOmLGyXKGxLeumB_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NACCVOMPIZOBGZKCXYKAFOMHLNE_T022B95CDD88767E9F6406469FC895ED2BD2B7AE6_H
#ifndef NEUOMMDVTUNKCNFOJODSEFBCWVI_T2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2_H
#define NEUOMMDVTUNKCNFOJODSEFBCWVI_T2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_NEuOmMdVTUnKcnfOjoDSefbcWvI
struct  NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_NEuOmMdVTUnKcnfOjoDSefbcWvI::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_NEuOmMdVTUnKcnfOjoDSefbcWvI::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_NEuOmMdVTUnKcnfOjoDSefbcWvI::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_NEuOmMdVTUnKcnfOjoDSefbcWvI::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_NEuOmMdVTUnKcnfOjoDSefbcWvI::omdPDTkbMQMCXIvHqkpJkHTSFLE
	int32_t ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_NEuOmMdVTUnKcnfOjoDSefbcWvI::eVNodIsBtpdoajQYgHVzEGonurY
	int32_t ___eVNodIsBtpdoajQYgHVzEGonurY_5;
	// Rewired.CustomController Rewired.Player_ControllerHelper_PollingHelper_NEuOmMdVTUnKcnfOjoDSefbcWvI::fkLmPSpTTpThPlIehVLEEKpzSSw
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * ___fkLmPSpTTpThPlIehVLEEKpzSSw_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_NEuOmMdVTUnKcnfOjoDSefbcWvI::jkSRyVQrmabuCHjrIhRkPrBsQXa
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___jkSRyVQrmabuCHjrIhRkPrBsQXa_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_NEuOmMdVTUnKcnfOjoDSefbcWvI::GysJxsKYkmwXzIHWouLqCypPCtP
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___GysJxsKYkmwXzIHWouLqCypPCtP_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_NEuOmMdVTUnKcnfOjoDSefbcWvI::oGNjoDKoniJyTKcQVJdgsoUHlPN
	RuntimeObject* ___oGNjoDKoniJyTKcQVJdgsoUHlPN_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return static_cast<int32_t>(offsetof(NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2, ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4)); }
	inline int32_t get_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() const { return ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline int32_t* get_address_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return &___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline void set_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(int32_t value)
	{
		___omdPDTkbMQMCXIvHqkpJkHTSFLE_4 = value;
	}

	inline static int32_t get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return static_cast<int32_t>(offsetof(NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2, ___eVNodIsBtpdoajQYgHVzEGonurY_5)); }
	inline int32_t get_eVNodIsBtpdoajQYgHVzEGonurY_5() const { return ___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline int32_t* get_address_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return &___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline void set_eVNodIsBtpdoajQYgHVzEGonurY_5(int32_t value)
	{
		___eVNodIsBtpdoajQYgHVzEGonurY_5 = value;
	}

	inline static int32_t get_offset_of_fkLmPSpTTpThPlIehVLEEKpzSSw_6() { return static_cast<int32_t>(offsetof(NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2, ___fkLmPSpTTpThPlIehVLEEKpzSSw_6)); }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * get_fkLmPSpTTpThPlIehVLEEKpzSSw_6() const { return ___fkLmPSpTTpThPlIehVLEEKpzSSw_6; }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A ** get_address_of_fkLmPSpTTpThPlIehVLEEKpzSSw_6() { return &___fkLmPSpTTpThPlIehVLEEKpzSSw_6; }
	inline void set_fkLmPSpTTpThPlIehVLEEKpzSSw_6(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * value)
	{
		___fkLmPSpTTpThPlIehVLEEKpzSSw_6 = value;
		Il2CppCodeGenWriteBarrier((&___fkLmPSpTTpThPlIehVLEEKpzSSw_6), value);
	}

	inline static int32_t get_offset_of_jkSRyVQrmabuCHjrIhRkPrBsQXa_7() { return static_cast<int32_t>(offsetof(NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2, ___jkSRyVQrmabuCHjrIhRkPrBsQXa_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_jkSRyVQrmabuCHjrIhRkPrBsQXa_7() const { return ___jkSRyVQrmabuCHjrIhRkPrBsQXa_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_jkSRyVQrmabuCHjrIhRkPrBsQXa_7() { return &___jkSRyVQrmabuCHjrIhRkPrBsQXa_7; }
	inline void set_jkSRyVQrmabuCHjrIhRkPrBsQXa_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___jkSRyVQrmabuCHjrIhRkPrBsQXa_7 = value;
	}

	inline static int32_t get_offset_of_GysJxsKYkmwXzIHWouLqCypPCtP_8() { return static_cast<int32_t>(offsetof(NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2, ___GysJxsKYkmwXzIHWouLqCypPCtP_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_GysJxsKYkmwXzIHWouLqCypPCtP_8() const { return ___GysJxsKYkmwXzIHWouLqCypPCtP_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_GysJxsKYkmwXzIHWouLqCypPCtP_8() { return &___GysJxsKYkmwXzIHWouLqCypPCtP_8; }
	inline void set_GysJxsKYkmwXzIHWouLqCypPCtP_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___GysJxsKYkmwXzIHWouLqCypPCtP_8 = value;
	}

	inline static int32_t get_offset_of_oGNjoDKoniJyTKcQVJdgsoUHlPN_9() { return static_cast<int32_t>(offsetof(NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2, ___oGNjoDKoniJyTKcQVJdgsoUHlPN_9)); }
	inline RuntimeObject* get_oGNjoDKoniJyTKcQVJdgsoUHlPN_9() const { return ___oGNjoDKoniJyTKcQVJdgsoUHlPN_9; }
	inline RuntimeObject** get_address_of_oGNjoDKoniJyTKcQVJdgsoUHlPN_9() { return &___oGNjoDKoniJyTKcQVJdgsoUHlPN_9; }
	inline void set_oGNjoDKoniJyTKcQVJdgsoUHlPN_9(RuntimeObject* value)
	{
		___oGNjoDKoniJyTKcQVJdgsoUHlPN_9 = value;
		Il2CppCodeGenWriteBarrier((&___oGNjoDKoniJyTKcQVJdgsoUHlPN_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEUOMMDVTUNKCNFOJODSEFBCWVI_T2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2_H
#ifndef QYRTGPMFKNDTFDGUSKRWDQOEGYV_T2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63_H
#define QYRTGPMFKNDTFDGUSKRWDQOEGYV_T2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_QYRtgpMFkndtfdgUsKrWDQoEGYV
struct  QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_QYRtgpMFkndtfdgUsKrWDQoEGYV::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_QYRtgpMFkndtfdgUsKrWDQoEGYV::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_QYRtgpMFkndtfdgUsKrWDQoEGYV::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_QYRtgpMFkndtfdgUsKrWDQoEGYV::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Collections.Generic.IList`1<Rewired.CustomController> Rewired.Player_ControllerHelper_PollingHelper_QYRtgpMFkndtfdgUsKrWDQoEGYV::WlJMddRgCkILfsNEyJrynAYlZNd
	RuntimeObject* ___WlJMddRgCkILfsNEyJrynAYlZNd_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_QYRtgpMFkndtfdgUsKrWDQoEGYV::kOrYFTHklSsyuAVOvjAuWGrpNKT
	int32_t ___kOrYFTHklSsyuAVOvjAuWGrpNKT_5;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_QYRtgpMFkndtfdgUsKrWDQoEGYV::PbJflXkfhbqNfPVmadSsbKOIfHNm
	int32_t ___PbJflXkfhbqNfPVmadSsbKOIfHNm_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_QYRtgpMFkndtfdgUsKrWDQoEGYV::dkYgsNIinFjxxCKjCJJgyzptBwzL
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___dkYgsNIinFjxxCKjCJJgyzptBwzL_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_QYRtgpMFkndtfdgUsKrWDQoEGYV::NwJnCKFisXaEmuzxzGrDOmZKnhR
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NwJnCKFisXaEmuzxzGrDOmZKnhR_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_QYRtgpMFkndtfdgUsKrWDQoEGYV::KXwWIrZEnGCaCddXFGeyvTxTMnfZ
	RuntimeObject* ___KXwWIrZEnGCaCddXFGeyvTxTMnfZ_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_WlJMddRgCkILfsNEyJrynAYlZNd_4() { return static_cast<int32_t>(offsetof(QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63, ___WlJMddRgCkILfsNEyJrynAYlZNd_4)); }
	inline RuntimeObject* get_WlJMddRgCkILfsNEyJrynAYlZNd_4() const { return ___WlJMddRgCkILfsNEyJrynAYlZNd_4; }
	inline RuntimeObject** get_address_of_WlJMddRgCkILfsNEyJrynAYlZNd_4() { return &___WlJMddRgCkILfsNEyJrynAYlZNd_4; }
	inline void set_WlJMddRgCkILfsNEyJrynAYlZNd_4(RuntimeObject* value)
	{
		___WlJMddRgCkILfsNEyJrynAYlZNd_4 = value;
		Il2CppCodeGenWriteBarrier((&___WlJMddRgCkILfsNEyJrynAYlZNd_4), value);
	}

	inline static int32_t get_offset_of_kOrYFTHklSsyuAVOvjAuWGrpNKT_5() { return static_cast<int32_t>(offsetof(QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63, ___kOrYFTHklSsyuAVOvjAuWGrpNKT_5)); }
	inline int32_t get_kOrYFTHklSsyuAVOvjAuWGrpNKT_5() const { return ___kOrYFTHklSsyuAVOvjAuWGrpNKT_5; }
	inline int32_t* get_address_of_kOrYFTHklSsyuAVOvjAuWGrpNKT_5() { return &___kOrYFTHklSsyuAVOvjAuWGrpNKT_5; }
	inline void set_kOrYFTHklSsyuAVOvjAuWGrpNKT_5(int32_t value)
	{
		___kOrYFTHklSsyuAVOvjAuWGrpNKT_5 = value;
	}

	inline static int32_t get_offset_of_PbJflXkfhbqNfPVmadSsbKOIfHNm_6() { return static_cast<int32_t>(offsetof(QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63, ___PbJflXkfhbqNfPVmadSsbKOIfHNm_6)); }
	inline int32_t get_PbJflXkfhbqNfPVmadSsbKOIfHNm_6() const { return ___PbJflXkfhbqNfPVmadSsbKOIfHNm_6; }
	inline int32_t* get_address_of_PbJflXkfhbqNfPVmadSsbKOIfHNm_6() { return &___PbJflXkfhbqNfPVmadSsbKOIfHNm_6; }
	inline void set_PbJflXkfhbqNfPVmadSsbKOIfHNm_6(int32_t value)
	{
		___PbJflXkfhbqNfPVmadSsbKOIfHNm_6 = value;
	}

	inline static int32_t get_offset_of_dkYgsNIinFjxxCKjCJJgyzptBwzL_7() { return static_cast<int32_t>(offsetof(QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63, ___dkYgsNIinFjxxCKjCJJgyzptBwzL_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_dkYgsNIinFjxxCKjCJJgyzptBwzL_7() const { return ___dkYgsNIinFjxxCKjCJJgyzptBwzL_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_dkYgsNIinFjxxCKjCJJgyzptBwzL_7() { return &___dkYgsNIinFjxxCKjCJJgyzptBwzL_7; }
	inline void set_dkYgsNIinFjxxCKjCJJgyzptBwzL_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___dkYgsNIinFjxxCKjCJJgyzptBwzL_7 = value;
	}

	inline static int32_t get_offset_of_NwJnCKFisXaEmuzxzGrDOmZKnhR_8() { return static_cast<int32_t>(offsetof(QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63, ___NwJnCKFisXaEmuzxzGrDOmZKnhR_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NwJnCKFisXaEmuzxzGrDOmZKnhR_8() const { return ___NwJnCKFisXaEmuzxzGrDOmZKnhR_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NwJnCKFisXaEmuzxzGrDOmZKnhR_8() { return &___NwJnCKFisXaEmuzxzGrDOmZKnhR_8; }
	inline void set_NwJnCKFisXaEmuzxzGrDOmZKnhR_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NwJnCKFisXaEmuzxzGrDOmZKnhR_8 = value;
	}

	inline static int32_t get_offset_of_KXwWIrZEnGCaCddXFGeyvTxTMnfZ_9() { return static_cast<int32_t>(offsetof(QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63, ___KXwWIrZEnGCaCddXFGeyvTxTMnfZ_9)); }
	inline RuntimeObject* get_KXwWIrZEnGCaCddXFGeyvTxTMnfZ_9() const { return ___KXwWIrZEnGCaCddXFGeyvTxTMnfZ_9; }
	inline RuntimeObject** get_address_of_KXwWIrZEnGCaCddXFGeyvTxTMnfZ_9() { return &___KXwWIrZEnGCaCddXFGeyvTxTMnfZ_9; }
	inline void set_KXwWIrZEnGCaCddXFGeyvTxTMnfZ_9(RuntimeObject* value)
	{
		___KXwWIrZEnGCaCddXFGeyvTxTMnfZ_9 = value;
		Il2CppCodeGenWriteBarrier((&___KXwWIrZEnGCaCddXFGeyvTxTMnfZ_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QYRTGPMFKNDTFDGUSKRWDQOEGYV_T2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63_H
#ifndef WEUMGYPBONHAFQBSJEIYLDKTEES_T7D717A06BD51BF285CF48128B6E80CCA028F133F_H
#define WEUMGYPBONHAFQBSJEIYLDKTEES_T7D717A06BD51BF285CF48128B6E80CCA028F133F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_WeUmGyPBOnhafqbSjEIYlDkteEs
struct  WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_WeUmGyPBOnhafqbSjEIYlDkteEs::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_WeUmGyPBOnhafqbSjEIYlDkteEs::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_WeUmGyPBOnhafqbSjEIYlDkteEs::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_WeUmGyPBOnhafqbSjEIYlDkteEs::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_WeUmGyPBOnhafqbSjEIYlDkteEs::hMBbQHSYpxrZxtjFPBFvFGsQNmg
	int32_t ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_WeUmGyPBOnhafqbSjEIYlDkteEs::piAnfnMqsmhCZfWFjwHhSJYtiKf
	int32_t ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5;
	// Rewired.Joystick Rewired.Player_ControllerHelper_PollingHelper_WeUmGyPBOnhafqbSjEIYlDkteEs::IRQFsXBuxGaQqSPvASJkVExAlvIl
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___IRQFsXBuxGaQqSPvASJkVExAlvIl_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_WeUmGyPBOnhafqbSjEIYlDkteEs::bLwxtYaZuWRqOVNKgwLYPwzVIMU
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___bLwxtYaZuWRqOVNKgwLYPwzVIMU_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_WeUmGyPBOnhafqbSjEIYlDkteEs::IbFgSQoknudTqPStuJGhGvLEjpS
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___IbFgSQoknudTqPStuJGhGvLEjpS_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_WeUmGyPBOnhafqbSjEIYlDkteEs::MxEFTmKyklwklfapaApOEwBseZO
	RuntimeObject* ___MxEFTmKyklwklfapaApOEwBseZO_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return static_cast<int32_t>(offsetof(WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F, ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4)); }
	inline int32_t get_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() const { return ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline int32_t* get_address_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return &___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline void set_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(int32_t value)
	{
		___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4 = value;
	}

	inline static int32_t get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return static_cast<int32_t>(offsetof(WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F, ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5)); }
	inline int32_t get_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() const { return ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline int32_t* get_address_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return &___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline void set_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(int32_t value)
	{
		___piAnfnMqsmhCZfWFjwHhSJYtiKf_5 = value;
	}

	inline static int32_t get_offset_of_IRQFsXBuxGaQqSPvASJkVExAlvIl_6() { return static_cast<int32_t>(offsetof(WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F, ___IRQFsXBuxGaQqSPvASJkVExAlvIl_6)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_IRQFsXBuxGaQqSPvASJkVExAlvIl_6() const { return ___IRQFsXBuxGaQqSPvASJkVExAlvIl_6; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_IRQFsXBuxGaQqSPvASJkVExAlvIl_6() { return &___IRQFsXBuxGaQqSPvASJkVExAlvIl_6; }
	inline void set_IRQFsXBuxGaQqSPvASJkVExAlvIl_6(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___IRQFsXBuxGaQqSPvASJkVExAlvIl_6 = value;
		Il2CppCodeGenWriteBarrier((&___IRQFsXBuxGaQqSPvASJkVExAlvIl_6), value);
	}

	inline static int32_t get_offset_of_bLwxtYaZuWRqOVNKgwLYPwzVIMU_7() { return static_cast<int32_t>(offsetof(WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F, ___bLwxtYaZuWRqOVNKgwLYPwzVIMU_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_bLwxtYaZuWRqOVNKgwLYPwzVIMU_7() const { return ___bLwxtYaZuWRqOVNKgwLYPwzVIMU_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_bLwxtYaZuWRqOVNKgwLYPwzVIMU_7() { return &___bLwxtYaZuWRqOVNKgwLYPwzVIMU_7; }
	inline void set_bLwxtYaZuWRqOVNKgwLYPwzVIMU_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___bLwxtYaZuWRqOVNKgwLYPwzVIMU_7 = value;
	}

	inline static int32_t get_offset_of_IbFgSQoknudTqPStuJGhGvLEjpS_8() { return static_cast<int32_t>(offsetof(WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F, ___IbFgSQoknudTqPStuJGhGvLEjpS_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_IbFgSQoknudTqPStuJGhGvLEjpS_8() const { return ___IbFgSQoknudTqPStuJGhGvLEjpS_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_IbFgSQoknudTqPStuJGhGvLEjpS_8() { return &___IbFgSQoknudTqPStuJGhGvLEjpS_8; }
	inline void set_IbFgSQoknudTqPStuJGhGvLEjpS_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___IbFgSQoknudTqPStuJGhGvLEjpS_8 = value;
	}

	inline static int32_t get_offset_of_MxEFTmKyklwklfapaApOEwBseZO_9() { return static_cast<int32_t>(offsetof(WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F, ___MxEFTmKyklwklfapaApOEwBseZO_9)); }
	inline RuntimeObject* get_MxEFTmKyklwklfapaApOEwBseZO_9() const { return ___MxEFTmKyklwklfapaApOEwBseZO_9; }
	inline RuntimeObject** get_address_of_MxEFTmKyklwklfapaApOEwBseZO_9() { return &___MxEFTmKyklwklfapaApOEwBseZO_9; }
	inline void set_MxEFTmKyklwklfapaApOEwBseZO_9(RuntimeObject* value)
	{
		___MxEFTmKyklwklfapaApOEwBseZO_9 = value;
		Il2CppCodeGenWriteBarrier((&___MxEFTmKyklwklfapaApOEwBseZO_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEUMGYPBONHAFQBSJEIYLDKTEES_T7D717A06BD51BF285CF48128B6E80CCA028F133F_H
#ifndef XFIGJZJPTFZLOLNAJMCJKEETLIKB_TB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D_H
#define XFIGJZJPTFZLOLNAJMCJKEETLIKB_TB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_XFIgJZjPTfZLOlNajMCJkEeTLIKB
struct  XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_XFIgJZjPTfZLOlNajMCJkEeTLIKB::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_XFIgJZjPTfZLOlNajMCJkEeTLIKB::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_XFIgJZjPTfZLOlNajMCJkEeTLIKB::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_XFIgJZjPTfZLOlNajMCJkEeTLIKB::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_XFIgJZjPTfZLOlNajMCJkEeTLIKB::hMBbQHSYpxrZxtjFPBFvFGsQNmg
	int32_t ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_XFIgJZjPTfZLOlNajMCJkEeTLIKB::piAnfnMqsmhCZfWFjwHhSJYtiKf
	int32_t ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5;
	// Rewired.Joystick Rewired.Player_ControllerHelper_PollingHelper_XFIgJZjPTfZLOlNajMCJkEeTLIKB::qbYVUsevpzMZRhlusfJuhWcAGyZ
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___qbYVUsevpzMZRhlusfJuhWcAGyZ_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_XFIgJZjPTfZLOlNajMCJkEeTLIKB::ecefohvCgnETzaWbjgfWJOxaNThk
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___ecefohvCgnETzaWbjgfWJOxaNThk_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_XFIgJZjPTfZLOlNajMCJkEeTLIKB::GTnBQninYpdBXNnnjRlSOtkVmRh
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___GTnBQninYpdBXNnnjRlSOtkVmRh_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_XFIgJZjPTfZLOlNajMCJkEeTLIKB::OsyGaKrcvWcznnvuWTxaBOrYODn
	RuntimeObject* ___OsyGaKrcvWcznnvuWTxaBOrYODn_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return static_cast<int32_t>(offsetof(XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D, ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4)); }
	inline int32_t get_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() const { return ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline int32_t* get_address_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return &___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline void set_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(int32_t value)
	{
		___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4 = value;
	}

	inline static int32_t get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return static_cast<int32_t>(offsetof(XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D, ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5)); }
	inline int32_t get_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() const { return ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline int32_t* get_address_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return &___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline void set_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(int32_t value)
	{
		___piAnfnMqsmhCZfWFjwHhSJYtiKf_5 = value;
	}

	inline static int32_t get_offset_of_qbYVUsevpzMZRhlusfJuhWcAGyZ_6() { return static_cast<int32_t>(offsetof(XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D, ___qbYVUsevpzMZRhlusfJuhWcAGyZ_6)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_qbYVUsevpzMZRhlusfJuhWcAGyZ_6() const { return ___qbYVUsevpzMZRhlusfJuhWcAGyZ_6; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_qbYVUsevpzMZRhlusfJuhWcAGyZ_6() { return &___qbYVUsevpzMZRhlusfJuhWcAGyZ_6; }
	inline void set_qbYVUsevpzMZRhlusfJuhWcAGyZ_6(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___qbYVUsevpzMZRhlusfJuhWcAGyZ_6 = value;
		Il2CppCodeGenWriteBarrier((&___qbYVUsevpzMZRhlusfJuhWcAGyZ_6), value);
	}

	inline static int32_t get_offset_of_ecefohvCgnETzaWbjgfWJOxaNThk_7() { return static_cast<int32_t>(offsetof(XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D, ___ecefohvCgnETzaWbjgfWJOxaNThk_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_ecefohvCgnETzaWbjgfWJOxaNThk_7() const { return ___ecefohvCgnETzaWbjgfWJOxaNThk_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_ecefohvCgnETzaWbjgfWJOxaNThk_7() { return &___ecefohvCgnETzaWbjgfWJOxaNThk_7; }
	inline void set_ecefohvCgnETzaWbjgfWJOxaNThk_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___ecefohvCgnETzaWbjgfWJOxaNThk_7 = value;
	}

	inline static int32_t get_offset_of_GTnBQninYpdBXNnnjRlSOtkVmRh_8() { return static_cast<int32_t>(offsetof(XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D, ___GTnBQninYpdBXNnnjRlSOtkVmRh_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_GTnBQninYpdBXNnnjRlSOtkVmRh_8() const { return ___GTnBQninYpdBXNnnjRlSOtkVmRh_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_GTnBQninYpdBXNnnjRlSOtkVmRh_8() { return &___GTnBQninYpdBXNnnjRlSOtkVmRh_8; }
	inline void set_GTnBQninYpdBXNnnjRlSOtkVmRh_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___GTnBQninYpdBXNnnjRlSOtkVmRh_8 = value;
	}

	inline static int32_t get_offset_of_OsyGaKrcvWcznnvuWTxaBOrYODn_9() { return static_cast<int32_t>(offsetof(XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D, ___OsyGaKrcvWcznnvuWTxaBOrYODn_9)); }
	inline RuntimeObject* get_OsyGaKrcvWcznnvuWTxaBOrYODn_9() const { return ___OsyGaKrcvWcznnvuWTxaBOrYODn_9; }
	inline RuntimeObject** get_address_of_OsyGaKrcvWcznnvuWTxaBOrYODn_9() { return &___OsyGaKrcvWcznnvuWTxaBOrYODn_9; }
	inline void set_OsyGaKrcvWcznnvuWTxaBOrYODn_9(RuntimeObject* value)
	{
		___OsyGaKrcvWcznnvuWTxaBOrYODn_9 = value;
		Il2CppCodeGenWriteBarrier((&___OsyGaKrcvWcznnvuWTxaBOrYODn_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XFIGJZJPTFZLOLNAJMCJKEETLIKB_TB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D_H
#ifndef YNTKVCERSGESKHBVLIANONPHMUR_T4D32988B588735E175C13E6D13280EE0D219BEAF_H
#define YNTKVCERSGESKHBVLIANONPHMUR_T4D32988B588735E175C13E6D13280EE0D219BEAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_YntKvCeRsGEsKHBVLIaNoNphMur
struct  YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_YntKvCeRsGEsKHBVLIaNoNphMur::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_YntKvCeRsGEsKHBVLIaNoNphMur::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_YntKvCeRsGEsKHBVLIaNoNphMur::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_YntKvCeRsGEsKHBVLIaNoNphMur::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Collections.Generic.IList`1<Rewired.CustomController> Rewired.Player_ControllerHelper_PollingHelper_YntKvCeRsGEsKHBVLIaNoNphMur::grsSPenuLaffXjyCYEgfUyNoLhD
	RuntimeObject* ___grsSPenuLaffXjyCYEgfUyNoLhD_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_YntKvCeRsGEsKHBVLIaNoNphMur::iOOfWhiNRrmZjBKVcgnXBpLYORW
	int32_t ___iOOfWhiNRrmZjBKVcgnXBpLYORW_5;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_YntKvCeRsGEsKHBVLIaNoNphMur::RGMofoBpYjOtFuUVxaHLVpQgFEW
	int32_t ___RGMofoBpYjOtFuUVxaHLVpQgFEW_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_YntKvCeRsGEsKHBVLIaNoNphMur::tCsuKotdlEDURGcQQqCsAkoitnKF
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___tCsuKotdlEDURGcQQqCsAkoitnKF_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_YntKvCeRsGEsKHBVLIaNoNphMur::XZiyiPJkBaVCZVvfzWGRyOqWoEl
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___XZiyiPJkBaVCZVvfzWGRyOqWoEl_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_YntKvCeRsGEsKHBVLIaNoNphMur::MqHAUSddShjakUUVqrvjYHlyjwE
	RuntimeObject* ___MqHAUSddShjakUUVqrvjYHlyjwE_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_grsSPenuLaffXjyCYEgfUyNoLhD_4() { return static_cast<int32_t>(offsetof(YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF, ___grsSPenuLaffXjyCYEgfUyNoLhD_4)); }
	inline RuntimeObject* get_grsSPenuLaffXjyCYEgfUyNoLhD_4() const { return ___grsSPenuLaffXjyCYEgfUyNoLhD_4; }
	inline RuntimeObject** get_address_of_grsSPenuLaffXjyCYEgfUyNoLhD_4() { return &___grsSPenuLaffXjyCYEgfUyNoLhD_4; }
	inline void set_grsSPenuLaffXjyCYEgfUyNoLhD_4(RuntimeObject* value)
	{
		___grsSPenuLaffXjyCYEgfUyNoLhD_4 = value;
		Il2CppCodeGenWriteBarrier((&___grsSPenuLaffXjyCYEgfUyNoLhD_4), value);
	}

	inline static int32_t get_offset_of_iOOfWhiNRrmZjBKVcgnXBpLYORW_5() { return static_cast<int32_t>(offsetof(YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF, ___iOOfWhiNRrmZjBKVcgnXBpLYORW_5)); }
	inline int32_t get_iOOfWhiNRrmZjBKVcgnXBpLYORW_5() const { return ___iOOfWhiNRrmZjBKVcgnXBpLYORW_5; }
	inline int32_t* get_address_of_iOOfWhiNRrmZjBKVcgnXBpLYORW_5() { return &___iOOfWhiNRrmZjBKVcgnXBpLYORW_5; }
	inline void set_iOOfWhiNRrmZjBKVcgnXBpLYORW_5(int32_t value)
	{
		___iOOfWhiNRrmZjBKVcgnXBpLYORW_5 = value;
	}

	inline static int32_t get_offset_of_RGMofoBpYjOtFuUVxaHLVpQgFEW_6() { return static_cast<int32_t>(offsetof(YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF, ___RGMofoBpYjOtFuUVxaHLVpQgFEW_6)); }
	inline int32_t get_RGMofoBpYjOtFuUVxaHLVpQgFEW_6() const { return ___RGMofoBpYjOtFuUVxaHLVpQgFEW_6; }
	inline int32_t* get_address_of_RGMofoBpYjOtFuUVxaHLVpQgFEW_6() { return &___RGMofoBpYjOtFuUVxaHLVpQgFEW_6; }
	inline void set_RGMofoBpYjOtFuUVxaHLVpQgFEW_6(int32_t value)
	{
		___RGMofoBpYjOtFuUVxaHLVpQgFEW_6 = value;
	}

	inline static int32_t get_offset_of_tCsuKotdlEDURGcQQqCsAkoitnKF_7() { return static_cast<int32_t>(offsetof(YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF, ___tCsuKotdlEDURGcQQqCsAkoitnKF_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_tCsuKotdlEDURGcQQqCsAkoitnKF_7() const { return ___tCsuKotdlEDURGcQQqCsAkoitnKF_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_tCsuKotdlEDURGcQQqCsAkoitnKF_7() { return &___tCsuKotdlEDURGcQQqCsAkoitnKF_7; }
	inline void set_tCsuKotdlEDURGcQQqCsAkoitnKF_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___tCsuKotdlEDURGcQQqCsAkoitnKF_7 = value;
	}

	inline static int32_t get_offset_of_XZiyiPJkBaVCZVvfzWGRyOqWoEl_8() { return static_cast<int32_t>(offsetof(YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF, ___XZiyiPJkBaVCZVvfzWGRyOqWoEl_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_XZiyiPJkBaVCZVvfzWGRyOqWoEl_8() const { return ___XZiyiPJkBaVCZVvfzWGRyOqWoEl_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_XZiyiPJkBaVCZVvfzWGRyOqWoEl_8() { return &___XZiyiPJkBaVCZVvfzWGRyOqWoEl_8; }
	inline void set_XZiyiPJkBaVCZVvfzWGRyOqWoEl_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___XZiyiPJkBaVCZVvfzWGRyOqWoEl_8 = value;
	}

	inline static int32_t get_offset_of_MqHAUSddShjakUUVqrvjYHlyjwE_9() { return static_cast<int32_t>(offsetof(YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF, ___MqHAUSddShjakUUVqrvjYHlyjwE_9)); }
	inline RuntimeObject* get_MqHAUSddShjakUUVqrvjYHlyjwE_9() const { return ___MqHAUSddShjakUUVqrvjYHlyjwE_9; }
	inline RuntimeObject** get_address_of_MqHAUSddShjakUUVqrvjYHlyjwE_9() { return &___MqHAUSddShjakUUVqrvjYHlyjwE_9; }
	inline void set_MqHAUSddShjakUUVqrvjYHlyjwE_9(RuntimeObject* value)
	{
		___MqHAUSddShjakUUVqrvjYHlyjwE_9 = value;
		Il2CppCodeGenWriteBarrier((&___MqHAUSddShjakUUVqrvjYHlyjwE_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YNTKVCERSGESKHBVLIANONPHMUR_T4D32988B588735E175C13E6D13280EE0D219BEAF_H
#ifndef ZNVVJSYSQWDREULYCFRVXLYWCOSK_T1D9CB290AC48CC781D220DE736A5911CA154295E_H
#define ZNVVJSYSQWDREULYCFRVXLYWCOSK_T1D9CB290AC48CC781D220DE736A5911CA154295E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_ZnVvjSySQWDReUlYCfRvXlYWcosK
struct  ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_ZnVvjSySQWDReUlYCfRvXlYWcosK::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_ZnVvjSySQWDReUlYCfRvXlYWcosK::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_ZnVvjSySQWDReUlYCfRvXlYWcosK::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_ZnVvjSySQWDReUlYCfRvXlYWcosK::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_ZnVvjSySQWDReUlYCfRvXlYWcosK::hMBbQHSYpxrZxtjFPBFvFGsQNmg
	int32_t ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_ZnVvjSySQWDReUlYCfRvXlYWcosK::piAnfnMqsmhCZfWFjwHhSJYtiKf
	int32_t ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5;
	// Rewired.Joystick Rewired.Player_ControllerHelper_PollingHelper_ZnVvjSySQWDReUlYCfRvXlYWcosK::qNKDPmeHjbkHjvhgHFrgmgjZYoXO
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___qNKDPmeHjbkHjvhgHFrgmgjZYoXO_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_ZnVvjSySQWDReUlYCfRvXlYWcosK::XtcStwMHinfKaHYwdgcMinLsiMS
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___XtcStwMHinfKaHYwdgcMinLsiMS_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_ZnVvjSySQWDReUlYCfRvXlYWcosK::CtKvIEUfqriYugaQWIkpoTWwhWZ
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___CtKvIEUfqriYugaQWIkpoTWwhWZ_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_ZnVvjSySQWDReUlYCfRvXlYWcosK::vtnVomtnWrFhLWWltmeRYPgyAgo
	RuntimeObject* ___vtnVomtnWrFhLWWltmeRYPgyAgo_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return static_cast<int32_t>(offsetof(ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E, ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4)); }
	inline int32_t get_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() const { return ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline int32_t* get_address_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return &___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline void set_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(int32_t value)
	{
		___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4 = value;
	}

	inline static int32_t get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return static_cast<int32_t>(offsetof(ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E, ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5)); }
	inline int32_t get_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() const { return ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline int32_t* get_address_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return &___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline void set_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(int32_t value)
	{
		___piAnfnMqsmhCZfWFjwHhSJYtiKf_5 = value;
	}

	inline static int32_t get_offset_of_qNKDPmeHjbkHjvhgHFrgmgjZYoXO_6() { return static_cast<int32_t>(offsetof(ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E, ___qNKDPmeHjbkHjvhgHFrgmgjZYoXO_6)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_qNKDPmeHjbkHjvhgHFrgmgjZYoXO_6() const { return ___qNKDPmeHjbkHjvhgHFrgmgjZYoXO_6; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_qNKDPmeHjbkHjvhgHFrgmgjZYoXO_6() { return &___qNKDPmeHjbkHjvhgHFrgmgjZYoXO_6; }
	inline void set_qNKDPmeHjbkHjvhgHFrgmgjZYoXO_6(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___qNKDPmeHjbkHjvhgHFrgmgjZYoXO_6 = value;
		Il2CppCodeGenWriteBarrier((&___qNKDPmeHjbkHjvhgHFrgmgjZYoXO_6), value);
	}

	inline static int32_t get_offset_of_XtcStwMHinfKaHYwdgcMinLsiMS_7() { return static_cast<int32_t>(offsetof(ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E, ___XtcStwMHinfKaHYwdgcMinLsiMS_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_XtcStwMHinfKaHYwdgcMinLsiMS_7() const { return ___XtcStwMHinfKaHYwdgcMinLsiMS_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_XtcStwMHinfKaHYwdgcMinLsiMS_7() { return &___XtcStwMHinfKaHYwdgcMinLsiMS_7; }
	inline void set_XtcStwMHinfKaHYwdgcMinLsiMS_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___XtcStwMHinfKaHYwdgcMinLsiMS_7 = value;
	}

	inline static int32_t get_offset_of_CtKvIEUfqriYugaQWIkpoTWwhWZ_8() { return static_cast<int32_t>(offsetof(ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E, ___CtKvIEUfqriYugaQWIkpoTWwhWZ_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_CtKvIEUfqriYugaQWIkpoTWwhWZ_8() const { return ___CtKvIEUfqriYugaQWIkpoTWwhWZ_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_CtKvIEUfqriYugaQWIkpoTWwhWZ_8() { return &___CtKvIEUfqriYugaQWIkpoTWwhWZ_8; }
	inline void set_CtKvIEUfqriYugaQWIkpoTWwhWZ_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___CtKvIEUfqriYugaQWIkpoTWwhWZ_8 = value;
	}

	inline static int32_t get_offset_of_vtnVomtnWrFhLWWltmeRYPgyAgo_9() { return static_cast<int32_t>(offsetof(ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E, ___vtnVomtnWrFhLWWltmeRYPgyAgo_9)); }
	inline RuntimeObject* get_vtnVomtnWrFhLWWltmeRYPgyAgo_9() const { return ___vtnVomtnWrFhLWWltmeRYPgyAgo_9; }
	inline RuntimeObject** get_address_of_vtnVomtnWrFhLWWltmeRYPgyAgo_9() { return &___vtnVomtnWrFhLWWltmeRYPgyAgo_9; }
	inline void set_vtnVomtnWrFhLWWltmeRYPgyAgo_9(RuntimeObject* value)
	{
		___vtnVomtnWrFhLWWltmeRYPgyAgo_9 = value;
		Il2CppCodeGenWriteBarrier((&___vtnVomtnWrFhLWWltmeRYPgyAgo_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZNVVJSYSQWDREULYCFRVXLYWCOSK_T1D9CB290AC48CC781D220DE736A5911CA154295E_H
#ifndef CNMSTNPDYOVWZGQDUIKDXEAWVIJ_TECBB1C577121EBE30312891430905BF81556A8EA_H
#define CNMSTNPDYOVWZGQDUIKDXEAWVIJ_TECBB1C577121EBE30312891430905BF81556A8EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_cnmstNPDYOvWZgQduIkdxeawViJ
struct  cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_cnmstNPDYOvWZgQduIkdxeawViJ::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_cnmstNPDYOvWZgQduIkdxeawViJ::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_cnmstNPDYOvWZgQduIkdxeawViJ::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_cnmstNPDYOvWZgQduIkdxeawViJ::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_cnmstNPDYOvWZgQduIkdxeawViJ::omdPDTkbMQMCXIvHqkpJkHTSFLE
	int32_t ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_cnmstNPDYOvWZgQduIkdxeawViJ::eVNodIsBtpdoajQYgHVzEGonurY
	int32_t ___eVNodIsBtpdoajQYgHVzEGonurY_5;
	// Rewired.CustomController Rewired.Player_ControllerHelper_PollingHelper_cnmstNPDYOvWZgQduIkdxeawViJ::EXqvCeIGfyiPfBvrhTrFQEECrIs
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * ___EXqvCeIGfyiPfBvrhTrFQEECrIs_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_cnmstNPDYOvWZgQduIkdxeawViJ::GzsbMdtcKxiiZvcBvfLLhMyuVBA
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___GzsbMdtcKxiiZvcBvfLLhMyuVBA_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_cnmstNPDYOvWZgQduIkdxeawViJ::XtXfuZHZityIduWboLrseFlqMJP
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___XtXfuZHZityIduWboLrseFlqMJP_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_cnmstNPDYOvWZgQduIkdxeawViJ::WKEXKgMKciWWennvQJNaZHBLgLQB
	RuntimeObject* ___WKEXKgMKciWWennvQJNaZHBLgLQB_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return static_cast<int32_t>(offsetof(cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA, ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4)); }
	inline int32_t get_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() const { return ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline int32_t* get_address_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return &___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline void set_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(int32_t value)
	{
		___omdPDTkbMQMCXIvHqkpJkHTSFLE_4 = value;
	}

	inline static int32_t get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return static_cast<int32_t>(offsetof(cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA, ___eVNodIsBtpdoajQYgHVzEGonurY_5)); }
	inline int32_t get_eVNodIsBtpdoajQYgHVzEGonurY_5() const { return ___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline int32_t* get_address_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return &___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline void set_eVNodIsBtpdoajQYgHVzEGonurY_5(int32_t value)
	{
		___eVNodIsBtpdoajQYgHVzEGonurY_5 = value;
	}

	inline static int32_t get_offset_of_EXqvCeIGfyiPfBvrhTrFQEECrIs_6() { return static_cast<int32_t>(offsetof(cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA, ___EXqvCeIGfyiPfBvrhTrFQEECrIs_6)); }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * get_EXqvCeIGfyiPfBvrhTrFQEECrIs_6() const { return ___EXqvCeIGfyiPfBvrhTrFQEECrIs_6; }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A ** get_address_of_EXqvCeIGfyiPfBvrhTrFQEECrIs_6() { return &___EXqvCeIGfyiPfBvrhTrFQEECrIs_6; }
	inline void set_EXqvCeIGfyiPfBvrhTrFQEECrIs_6(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * value)
	{
		___EXqvCeIGfyiPfBvrhTrFQEECrIs_6 = value;
		Il2CppCodeGenWriteBarrier((&___EXqvCeIGfyiPfBvrhTrFQEECrIs_6), value);
	}

	inline static int32_t get_offset_of_GzsbMdtcKxiiZvcBvfLLhMyuVBA_7() { return static_cast<int32_t>(offsetof(cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA, ___GzsbMdtcKxiiZvcBvfLLhMyuVBA_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_GzsbMdtcKxiiZvcBvfLLhMyuVBA_7() const { return ___GzsbMdtcKxiiZvcBvfLLhMyuVBA_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_GzsbMdtcKxiiZvcBvfLLhMyuVBA_7() { return &___GzsbMdtcKxiiZvcBvfLLhMyuVBA_7; }
	inline void set_GzsbMdtcKxiiZvcBvfLLhMyuVBA_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___GzsbMdtcKxiiZvcBvfLLhMyuVBA_7 = value;
	}

	inline static int32_t get_offset_of_XtXfuZHZityIduWboLrseFlqMJP_8() { return static_cast<int32_t>(offsetof(cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA, ___XtXfuZHZityIduWboLrseFlqMJP_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_XtXfuZHZityIduWboLrseFlqMJP_8() const { return ___XtXfuZHZityIduWboLrseFlqMJP_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_XtXfuZHZityIduWboLrseFlqMJP_8() { return &___XtXfuZHZityIduWboLrseFlqMJP_8; }
	inline void set_XtXfuZHZityIduWboLrseFlqMJP_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___XtXfuZHZityIduWboLrseFlqMJP_8 = value;
	}

	inline static int32_t get_offset_of_WKEXKgMKciWWennvQJNaZHBLgLQB_9() { return static_cast<int32_t>(offsetof(cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA, ___WKEXKgMKciWWennvQJNaZHBLgLQB_9)); }
	inline RuntimeObject* get_WKEXKgMKciWWennvQJNaZHBLgLQB_9() const { return ___WKEXKgMKciWWennvQJNaZHBLgLQB_9; }
	inline RuntimeObject** get_address_of_WKEXKgMKciWWennvQJNaZHBLgLQB_9() { return &___WKEXKgMKciWWennvQJNaZHBLgLQB_9; }
	inline void set_WKEXKgMKciWWennvQJNaZHBLgLQB_9(RuntimeObject* value)
	{
		___WKEXKgMKciWWennvQJNaZHBLgLQB_9 = value;
		Il2CppCodeGenWriteBarrier((&___WKEXKgMKciWWennvQJNaZHBLgLQB_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CNMSTNPDYOVWZGQDUIKDXEAWVIJ_TECBB1C577121EBE30312891430905BF81556A8EA_H
#ifndef DINLXBAWBAQPAXDALEFACOCSVBD_T21B810E07D5FF367226AAA6CF387E498F0B1FEC8_H
#define DINLXBAWBAQPAXDALEFACOCSVBD_T21B810E07D5FF367226AAA6CF387E498F0B1FEC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_diNLXbawbaQpAxdalefacoCSVbD
struct  diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_diNLXbawbaQpAxdalefacoCSVbD::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_diNLXbawbaQpAxdalefacoCSVbD::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_diNLXbawbaQpAxdalefacoCSVbD::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_diNLXbawbaQpAxdalefacoCSVbD::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_diNLXbawbaQpAxdalefacoCSVbD::hMBbQHSYpxrZxtjFPBFvFGsQNmg
	int32_t ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_diNLXbawbaQpAxdalefacoCSVbD::piAnfnMqsmhCZfWFjwHhSJYtiKf
	int32_t ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5;
	// Rewired.Joystick Rewired.Player_ControllerHelper_PollingHelper_diNLXbawbaQpAxdalefacoCSVbD::ZgGXCuLyylFmIFXeYRUUrLoUVlX
	Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * ___ZgGXCuLyylFmIFXeYRUUrLoUVlX_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_diNLXbawbaQpAxdalefacoCSVbD::dcoaArEgvzeToFoIlYrLJNcyOYTe
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___dcoaArEgvzeToFoIlYrLJNcyOYTe_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_diNLXbawbaQpAxdalefacoCSVbD::wzHnRYMgTKQeNCGyRXgqqHKnuZe
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___wzHnRYMgTKQeNCGyRXgqqHKnuZe_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_diNLXbawbaQpAxdalefacoCSVbD::cvlShwzUhITmcxheQCGkGNLTJaSf
	RuntimeObject* ___cvlShwzUhITmcxheQCGkGNLTJaSf_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return static_cast<int32_t>(offsetof(diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8, ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4)); }
	inline int32_t get_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() const { return ___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline int32_t* get_address_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4() { return &___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4; }
	inline void set_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(int32_t value)
	{
		___hMBbQHSYpxrZxtjFPBFvFGsQNmg_4 = value;
	}

	inline static int32_t get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return static_cast<int32_t>(offsetof(diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8, ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5)); }
	inline int32_t get_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() const { return ___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline int32_t* get_address_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5() { return &___piAnfnMqsmhCZfWFjwHhSJYtiKf_5; }
	inline void set_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(int32_t value)
	{
		___piAnfnMqsmhCZfWFjwHhSJYtiKf_5 = value;
	}

	inline static int32_t get_offset_of_ZgGXCuLyylFmIFXeYRUUrLoUVlX_6() { return static_cast<int32_t>(offsetof(diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8, ___ZgGXCuLyylFmIFXeYRUUrLoUVlX_6)); }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * get_ZgGXCuLyylFmIFXeYRUUrLoUVlX_6() const { return ___ZgGXCuLyylFmIFXeYRUUrLoUVlX_6; }
	inline Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 ** get_address_of_ZgGXCuLyylFmIFXeYRUUrLoUVlX_6() { return &___ZgGXCuLyylFmIFXeYRUUrLoUVlX_6; }
	inline void set_ZgGXCuLyylFmIFXeYRUUrLoUVlX_6(Joystick_t88A5812A3CF721E58BFB4C06D5D79B25F5A4DD39 * value)
	{
		___ZgGXCuLyylFmIFXeYRUUrLoUVlX_6 = value;
		Il2CppCodeGenWriteBarrier((&___ZgGXCuLyylFmIFXeYRUUrLoUVlX_6), value);
	}

	inline static int32_t get_offset_of_dcoaArEgvzeToFoIlYrLJNcyOYTe_7() { return static_cast<int32_t>(offsetof(diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8, ___dcoaArEgvzeToFoIlYrLJNcyOYTe_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_dcoaArEgvzeToFoIlYrLJNcyOYTe_7() const { return ___dcoaArEgvzeToFoIlYrLJNcyOYTe_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_dcoaArEgvzeToFoIlYrLJNcyOYTe_7() { return &___dcoaArEgvzeToFoIlYrLJNcyOYTe_7; }
	inline void set_dcoaArEgvzeToFoIlYrLJNcyOYTe_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___dcoaArEgvzeToFoIlYrLJNcyOYTe_7 = value;
	}

	inline static int32_t get_offset_of_wzHnRYMgTKQeNCGyRXgqqHKnuZe_8() { return static_cast<int32_t>(offsetof(diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8, ___wzHnRYMgTKQeNCGyRXgqqHKnuZe_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_wzHnRYMgTKQeNCGyRXgqqHKnuZe_8() const { return ___wzHnRYMgTKQeNCGyRXgqqHKnuZe_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_wzHnRYMgTKQeNCGyRXgqqHKnuZe_8() { return &___wzHnRYMgTKQeNCGyRXgqqHKnuZe_8; }
	inline void set_wzHnRYMgTKQeNCGyRXgqqHKnuZe_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___wzHnRYMgTKQeNCGyRXgqqHKnuZe_8 = value;
	}

	inline static int32_t get_offset_of_cvlShwzUhITmcxheQCGkGNLTJaSf_9() { return static_cast<int32_t>(offsetof(diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8, ___cvlShwzUhITmcxheQCGkGNLTJaSf_9)); }
	inline RuntimeObject* get_cvlShwzUhITmcxheQCGkGNLTJaSf_9() const { return ___cvlShwzUhITmcxheQCGkGNLTJaSf_9; }
	inline RuntimeObject** get_address_of_cvlShwzUhITmcxheQCGkGNLTJaSf_9() { return &___cvlShwzUhITmcxheQCGkGNLTJaSf_9; }
	inline void set_cvlShwzUhITmcxheQCGkGNLTJaSf_9(RuntimeObject* value)
	{
		___cvlShwzUhITmcxheQCGkGNLTJaSf_9 = value;
		Il2CppCodeGenWriteBarrier((&___cvlShwzUhITmcxheQCGkGNLTJaSf_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DINLXBAWBAQPAXDALEFACOCSVBD_T21B810E07D5FF367226AAA6CF387E498F0B1FEC8_H
#ifndef GCXXVAUTQXWDNQJUMVEQQQIEYZO_TF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA_H
#define GCXXVAUTQXWDNQJUMVEQQQIEYZO_TF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_gcXXvautqXwDnQJUMveqqqiEyZO
struct  gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_gcXXvautqXwDnQJUMveqqqiEyZO::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_gcXXvautqXwDnQJUMveqqqiEyZO::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_gcXXvautqXwDnQJUMveqqqiEyZO::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_gcXXvautqXwDnQJUMveqqqiEyZO::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Collections.Generic.IList`1<Rewired.Joystick> Rewired.Player_ControllerHelper_PollingHelper_gcXXvautqXwDnQJUMveqqqiEyZO::GCsQTeqGvLRkNFJNssxIvIahsYx
	RuntimeObject* ___GCsQTeqGvLRkNFJNssxIvIahsYx_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_gcXXvautqXwDnQJUMveqqqiEyZO::niXFFKcmIAMcbbBdqTJpvDojTBIn
	int32_t ___niXFFKcmIAMcbbBdqTJpvDojTBIn_5;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_gcXXvautqXwDnQJUMveqqqiEyZO::yJZkGbYqkCXIaEgSZTyuudHYuYh
	int32_t ___yJZkGbYqkCXIaEgSZTyuudHYuYh_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_gcXXvautqXwDnQJUMveqqqiEyZO::tHtKZdDnFlBBxqwiWQtjTIGvvxZ
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___tHtKZdDnFlBBxqwiWQtjTIGvvxZ_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_gcXXvautqXwDnQJUMveqqqiEyZO::UGxLeTcLdFISEciMjdoOAJrqKNwM
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___UGxLeTcLdFISEciMjdoOAJrqKNwM_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_gcXXvautqXwDnQJUMveqqqiEyZO::ZyIvfKZLJrYxlXOsSdTWaGCvGXi
	RuntimeObject* ___ZyIvfKZLJrYxlXOsSdTWaGCvGXi_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_GCsQTeqGvLRkNFJNssxIvIahsYx_4() { return static_cast<int32_t>(offsetof(gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA, ___GCsQTeqGvLRkNFJNssxIvIahsYx_4)); }
	inline RuntimeObject* get_GCsQTeqGvLRkNFJNssxIvIahsYx_4() const { return ___GCsQTeqGvLRkNFJNssxIvIahsYx_4; }
	inline RuntimeObject** get_address_of_GCsQTeqGvLRkNFJNssxIvIahsYx_4() { return &___GCsQTeqGvLRkNFJNssxIvIahsYx_4; }
	inline void set_GCsQTeqGvLRkNFJNssxIvIahsYx_4(RuntimeObject* value)
	{
		___GCsQTeqGvLRkNFJNssxIvIahsYx_4 = value;
		Il2CppCodeGenWriteBarrier((&___GCsQTeqGvLRkNFJNssxIvIahsYx_4), value);
	}

	inline static int32_t get_offset_of_niXFFKcmIAMcbbBdqTJpvDojTBIn_5() { return static_cast<int32_t>(offsetof(gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA, ___niXFFKcmIAMcbbBdqTJpvDojTBIn_5)); }
	inline int32_t get_niXFFKcmIAMcbbBdqTJpvDojTBIn_5() const { return ___niXFFKcmIAMcbbBdqTJpvDojTBIn_5; }
	inline int32_t* get_address_of_niXFFKcmIAMcbbBdqTJpvDojTBIn_5() { return &___niXFFKcmIAMcbbBdqTJpvDojTBIn_5; }
	inline void set_niXFFKcmIAMcbbBdqTJpvDojTBIn_5(int32_t value)
	{
		___niXFFKcmIAMcbbBdqTJpvDojTBIn_5 = value;
	}

	inline static int32_t get_offset_of_yJZkGbYqkCXIaEgSZTyuudHYuYh_6() { return static_cast<int32_t>(offsetof(gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA, ___yJZkGbYqkCXIaEgSZTyuudHYuYh_6)); }
	inline int32_t get_yJZkGbYqkCXIaEgSZTyuudHYuYh_6() const { return ___yJZkGbYqkCXIaEgSZTyuudHYuYh_6; }
	inline int32_t* get_address_of_yJZkGbYqkCXIaEgSZTyuudHYuYh_6() { return &___yJZkGbYqkCXIaEgSZTyuudHYuYh_6; }
	inline void set_yJZkGbYqkCXIaEgSZTyuudHYuYh_6(int32_t value)
	{
		___yJZkGbYqkCXIaEgSZTyuudHYuYh_6 = value;
	}

	inline static int32_t get_offset_of_tHtKZdDnFlBBxqwiWQtjTIGvvxZ_7() { return static_cast<int32_t>(offsetof(gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA, ___tHtKZdDnFlBBxqwiWQtjTIGvvxZ_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_tHtKZdDnFlBBxqwiWQtjTIGvvxZ_7() const { return ___tHtKZdDnFlBBxqwiWQtjTIGvvxZ_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_tHtKZdDnFlBBxqwiWQtjTIGvvxZ_7() { return &___tHtKZdDnFlBBxqwiWQtjTIGvvxZ_7; }
	inline void set_tHtKZdDnFlBBxqwiWQtjTIGvvxZ_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___tHtKZdDnFlBBxqwiWQtjTIGvvxZ_7 = value;
	}

	inline static int32_t get_offset_of_UGxLeTcLdFISEciMjdoOAJrqKNwM_8() { return static_cast<int32_t>(offsetof(gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA, ___UGxLeTcLdFISEciMjdoOAJrqKNwM_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_UGxLeTcLdFISEciMjdoOAJrqKNwM_8() const { return ___UGxLeTcLdFISEciMjdoOAJrqKNwM_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_UGxLeTcLdFISEciMjdoOAJrqKNwM_8() { return &___UGxLeTcLdFISEciMjdoOAJrqKNwM_8; }
	inline void set_UGxLeTcLdFISEciMjdoOAJrqKNwM_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___UGxLeTcLdFISEciMjdoOAJrqKNwM_8 = value;
	}

	inline static int32_t get_offset_of_ZyIvfKZLJrYxlXOsSdTWaGCvGXi_9() { return static_cast<int32_t>(offsetof(gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA, ___ZyIvfKZLJrYxlXOsSdTWaGCvGXi_9)); }
	inline RuntimeObject* get_ZyIvfKZLJrYxlXOsSdTWaGCvGXi_9() const { return ___ZyIvfKZLJrYxlXOsSdTWaGCvGXi_9; }
	inline RuntimeObject** get_address_of_ZyIvfKZLJrYxlXOsSdTWaGCvGXi_9() { return &___ZyIvfKZLJrYxlXOsSdTWaGCvGXi_9; }
	inline void set_ZyIvfKZLJrYxlXOsSdTWaGCvGXi_9(RuntimeObject* value)
	{
		___ZyIvfKZLJrYxlXOsSdTWaGCvGXi_9 = value;
		Il2CppCodeGenWriteBarrier((&___ZyIvfKZLJrYxlXOsSdTWaGCvGXi_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCXXVAUTQXWDNQJUMVEQQQIEYZO_TF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA_H
#ifndef MRUIKTVAMKGRHGVRZAVUJTBIKHX_T92B21FCE298157CB15A6FC7843D181FD8AF666D3_H
#define MRUIKTVAMKGRHGVRZAVUJTBIKHX_T92B21FCE298157CB15A6FC7843D181FD8AF666D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_mruikTvAMkGRHgvrZAVuJtbiKhX
struct  mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_mruikTvAMkGRHgvrZAVuJtbiKhX::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_mruikTvAMkGRHgvrZAVuJtbiKhX::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_mruikTvAMkGRHgvrZAVuJtbiKhX::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_mruikTvAMkGRHgvrZAVuJtbiKhX::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Collections.Generic.IList`1<Rewired.CustomController> Rewired.Player_ControllerHelper_PollingHelper_mruikTvAMkGRHgvrZAVuJtbiKhX::OPdZHGoqixWVplPNpKliwYtMXKS
	RuntimeObject* ___OPdZHGoqixWVplPNpKliwYtMXKS_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_mruikTvAMkGRHgvrZAVuJtbiKhX::lfTiMkclPxkMJOMngGPFpOELFpJh
	int32_t ___lfTiMkclPxkMJOMngGPFpOELFpJh_5;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_mruikTvAMkGRHgvrZAVuJtbiKhX::tlanGeuDkWgBcCNZdDqRcQUTwPog
	int32_t ___tlanGeuDkWgBcCNZdDqRcQUTwPog_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_mruikTvAMkGRHgvrZAVuJtbiKhX::MSPenACddlWMzevXUfCXfrmwbSxP
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___MSPenACddlWMzevXUfCXfrmwbSxP_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_mruikTvAMkGRHgvrZAVuJtbiKhX::WGlNAfsZsHPTsByKPknEettpgeU
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___WGlNAfsZsHPTsByKPknEettpgeU_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_mruikTvAMkGRHgvrZAVuJtbiKhX::vLKoFFARdAKwqxLNDABJctemIOo
	RuntimeObject* ___vLKoFFARdAKwqxLNDABJctemIOo_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_OPdZHGoqixWVplPNpKliwYtMXKS_4() { return static_cast<int32_t>(offsetof(mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3, ___OPdZHGoqixWVplPNpKliwYtMXKS_4)); }
	inline RuntimeObject* get_OPdZHGoqixWVplPNpKliwYtMXKS_4() const { return ___OPdZHGoqixWVplPNpKliwYtMXKS_4; }
	inline RuntimeObject** get_address_of_OPdZHGoqixWVplPNpKliwYtMXKS_4() { return &___OPdZHGoqixWVplPNpKliwYtMXKS_4; }
	inline void set_OPdZHGoqixWVplPNpKliwYtMXKS_4(RuntimeObject* value)
	{
		___OPdZHGoqixWVplPNpKliwYtMXKS_4 = value;
		Il2CppCodeGenWriteBarrier((&___OPdZHGoqixWVplPNpKliwYtMXKS_4), value);
	}

	inline static int32_t get_offset_of_lfTiMkclPxkMJOMngGPFpOELFpJh_5() { return static_cast<int32_t>(offsetof(mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3, ___lfTiMkclPxkMJOMngGPFpOELFpJh_5)); }
	inline int32_t get_lfTiMkclPxkMJOMngGPFpOELFpJh_5() const { return ___lfTiMkclPxkMJOMngGPFpOELFpJh_5; }
	inline int32_t* get_address_of_lfTiMkclPxkMJOMngGPFpOELFpJh_5() { return &___lfTiMkclPxkMJOMngGPFpOELFpJh_5; }
	inline void set_lfTiMkclPxkMJOMngGPFpOELFpJh_5(int32_t value)
	{
		___lfTiMkclPxkMJOMngGPFpOELFpJh_5 = value;
	}

	inline static int32_t get_offset_of_tlanGeuDkWgBcCNZdDqRcQUTwPog_6() { return static_cast<int32_t>(offsetof(mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3, ___tlanGeuDkWgBcCNZdDqRcQUTwPog_6)); }
	inline int32_t get_tlanGeuDkWgBcCNZdDqRcQUTwPog_6() const { return ___tlanGeuDkWgBcCNZdDqRcQUTwPog_6; }
	inline int32_t* get_address_of_tlanGeuDkWgBcCNZdDqRcQUTwPog_6() { return &___tlanGeuDkWgBcCNZdDqRcQUTwPog_6; }
	inline void set_tlanGeuDkWgBcCNZdDqRcQUTwPog_6(int32_t value)
	{
		___tlanGeuDkWgBcCNZdDqRcQUTwPog_6 = value;
	}

	inline static int32_t get_offset_of_MSPenACddlWMzevXUfCXfrmwbSxP_7() { return static_cast<int32_t>(offsetof(mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3, ___MSPenACddlWMzevXUfCXfrmwbSxP_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_MSPenACddlWMzevXUfCXfrmwbSxP_7() const { return ___MSPenACddlWMzevXUfCXfrmwbSxP_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_MSPenACddlWMzevXUfCXfrmwbSxP_7() { return &___MSPenACddlWMzevXUfCXfrmwbSxP_7; }
	inline void set_MSPenACddlWMzevXUfCXfrmwbSxP_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___MSPenACddlWMzevXUfCXfrmwbSxP_7 = value;
	}

	inline static int32_t get_offset_of_WGlNAfsZsHPTsByKPknEettpgeU_8() { return static_cast<int32_t>(offsetof(mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3, ___WGlNAfsZsHPTsByKPknEettpgeU_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_WGlNAfsZsHPTsByKPknEettpgeU_8() const { return ___WGlNAfsZsHPTsByKPknEettpgeU_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_WGlNAfsZsHPTsByKPknEettpgeU_8() { return &___WGlNAfsZsHPTsByKPknEettpgeU_8; }
	inline void set_WGlNAfsZsHPTsByKPknEettpgeU_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___WGlNAfsZsHPTsByKPknEettpgeU_8 = value;
	}

	inline static int32_t get_offset_of_vLKoFFARdAKwqxLNDABJctemIOo_9() { return static_cast<int32_t>(offsetof(mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3, ___vLKoFFARdAKwqxLNDABJctemIOo_9)); }
	inline RuntimeObject* get_vLKoFFARdAKwqxLNDABJctemIOo_9() const { return ___vLKoFFARdAKwqxLNDABJctemIOo_9; }
	inline RuntimeObject** get_address_of_vLKoFFARdAKwqxLNDABJctemIOo_9() { return &___vLKoFFARdAKwqxLNDABJctemIOo_9; }
	inline void set_vLKoFFARdAKwqxLNDABJctemIOo_9(RuntimeObject* value)
	{
		___vLKoFFARdAKwqxLNDABJctemIOo_9 = value;
		Il2CppCodeGenWriteBarrier((&___vLKoFFARdAKwqxLNDABJctemIOo_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRUIKTVAMKGRHGVRZAVUJTBIKHX_T92B21FCE298157CB15A6FC7843D181FD8AF666D3_H
#ifndef NKUYXUHVTIXMOHTJMXTKPUYKMST_T48A2AB1F4FB94922EA0784421A5A5744601CB56E_H
#define NKUYXUHVTIXMOHTJMXTKPUYKMST_T48A2AB1F4FB94922EA0784421A5A5744601CB56E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_nKUYXUHVTIXMohtJmxTkPuyKmsT
struct  nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_nKUYXUHVTIXMohtJmxTkPuyKmsT::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_nKUYXUHVTIXMohtJmxTkPuyKmsT::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_nKUYXUHVTIXMohtJmxTkPuyKmsT::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_nKUYXUHVTIXMohtJmxTkPuyKmsT::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Collections.Generic.IList`1<Rewired.Joystick> Rewired.Player_ControllerHelper_PollingHelper_nKUYXUHVTIXMohtJmxTkPuyKmsT::nBmPCEsHqRIgNvsUMUxEXPycBSk
	RuntimeObject* ___nBmPCEsHqRIgNvsUMUxEXPycBSk_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_nKUYXUHVTIXMohtJmxTkPuyKmsT::HlElcSicCOyzzunUrGfwwCHcstl
	int32_t ___HlElcSicCOyzzunUrGfwwCHcstl_5;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_nKUYXUHVTIXMohtJmxTkPuyKmsT::MKPdMGofjJUFETwfluQvGtCAksT
	int32_t ___MKPdMGofjJUFETwfluQvGtCAksT_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_nKUYXUHVTIXMohtJmxTkPuyKmsT::wTHSjkWuRrrthXhyBDmtiAoxiZsK
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___wTHSjkWuRrrthXhyBDmtiAoxiZsK_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_nKUYXUHVTIXMohtJmxTkPuyKmsT::cVkxKFvIoIqeGCBrgUMnHECzAXG
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___cVkxKFvIoIqeGCBrgUMnHECzAXG_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_nKUYXUHVTIXMohtJmxTkPuyKmsT::JxeurYaFSMEUZxgJoDtsYijwoMO
	RuntimeObject* ___JxeurYaFSMEUZxgJoDtsYijwoMO_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_nBmPCEsHqRIgNvsUMUxEXPycBSk_4() { return static_cast<int32_t>(offsetof(nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E, ___nBmPCEsHqRIgNvsUMUxEXPycBSk_4)); }
	inline RuntimeObject* get_nBmPCEsHqRIgNvsUMUxEXPycBSk_4() const { return ___nBmPCEsHqRIgNvsUMUxEXPycBSk_4; }
	inline RuntimeObject** get_address_of_nBmPCEsHqRIgNvsUMUxEXPycBSk_4() { return &___nBmPCEsHqRIgNvsUMUxEXPycBSk_4; }
	inline void set_nBmPCEsHqRIgNvsUMUxEXPycBSk_4(RuntimeObject* value)
	{
		___nBmPCEsHqRIgNvsUMUxEXPycBSk_4 = value;
		Il2CppCodeGenWriteBarrier((&___nBmPCEsHqRIgNvsUMUxEXPycBSk_4), value);
	}

	inline static int32_t get_offset_of_HlElcSicCOyzzunUrGfwwCHcstl_5() { return static_cast<int32_t>(offsetof(nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E, ___HlElcSicCOyzzunUrGfwwCHcstl_5)); }
	inline int32_t get_HlElcSicCOyzzunUrGfwwCHcstl_5() const { return ___HlElcSicCOyzzunUrGfwwCHcstl_5; }
	inline int32_t* get_address_of_HlElcSicCOyzzunUrGfwwCHcstl_5() { return &___HlElcSicCOyzzunUrGfwwCHcstl_5; }
	inline void set_HlElcSicCOyzzunUrGfwwCHcstl_5(int32_t value)
	{
		___HlElcSicCOyzzunUrGfwwCHcstl_5 = value;
	}

	inline static int32_t get_offset_of_MKPdMGofjJUFETwfluQvGtCAksT_6() { return static_cast<int32_t>(offsetof(nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E, ___MKPdMGofjJUFETwfluQvGtCAksT_6)); }
	inline int32_t get_MKPdMGofjJUFETwfluQvGtCAksT_6() const { return ___MKPdMGofjJUFETwfluQvGtCAksT_6; }
	inline int32_t* get_address_of_MKPdMGofjJUFETwfluQvGtCAksT_6() { return &___MKPdMGofjJUFETwfluQvGtCAksT_6; }
	inline void set_MKPdMGofjJUFETwfluQvGtCAksT_6(int32_t value)
	{
		___MKPdMGofjJUFETwfluQvGtCAksT_6 = value;
	}

	inline static int32_t get_offset_of_wTHSjkWuRrrthXhyBDmtiAoxiZsK_7() { return static_cast<int32_t>(offsetof(nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E, ___wTHSjkWuRrrthXhyBDmtiAoxiZsK_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_wTHSjkWuRrrthXhyBDmtiAoxiZsK_7() const { return ___wTHSjkWuRrrthXhyBDmtiAoxiZsK_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_wTHSjkWuRrrthXhyBDmtiAoxiZsK_7() { return &___wTHSjkWuRrrthXhyBDmtiAoxiZsK_7; }
	inline void set_wTHSjkWuRrrthXhyBDmtiAoxiZsK_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___wTHSjkWuRrrthXhyBDmtiAoxiZsK_7 = value;
	}

	inline static int32_t get_offset_of_cVkxKFvIoIqeGCBrgUMnHECzAXG_8() { return static_cast<int32_t>(offsetof(nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E, ___cVkxKFvIoIqeGCBrgUMnHECzAXG_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_cVkxKFvIoIqeGCBrgUMnHECzAXG_8() const { return ___cVkxKFvIoIqeGCBrgUMnHECzAXG_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_cVkxKFvIoIqeGCBrgUMnHECzAXG_8() { return &___cVkxKFvIoIqeGCBrgUMnHECzAXG_8; }
	inline void set_cVkxKFvIoIqeGCBrgUMnHECzAXG_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___cVkxKFvIoIqeGCBrgUMnHECzAXG_8 = value;
	}

	inline static int32_t get_offset_of_JxeurYaFSMEUZxgJoDtsYijwoMO_9() { return static_cast<int32_t>(offsetof(nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E, ___JxeurYaFSMEUZxgJoDtsYijwoMO_9)); }
	inline RuntimeObject* get_JxeurYaFSMEUZxgJoDtsYijwoMO_9() const { return ___JxeurYaFSMEUZxgJoDtsYijwoMO_9; }
	inline RuntimeObject** get_address_of_JxeurYaFSMEUZxgJoDtsYijwoMO_9() { return &___JxeurYaFSMEUZxgJoDtsYijwoMO_9; }
	inline void set_JxeurYaFSMEUZxgJoDtsYijwoMO_9(RuntimeObject* value)
	{
		___JxeurYaFSMEUZxgJoDtsYijwoMO_9 = value;
		Il2CppCodeGenWriteBarrier((&___JxeurYaFSMEUZxgJoDtsYijwoMO_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NKUYXUHVTIXMOHTJMXTKPUYKMST_T48A2AB1F4FB94922EA0784421A5A5744601CB56E_H
#ifndef PDUHZIZWQLTIRGSJPGVCLVCVKQH_TBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512_H
#define PDUHZIZWQLTIRGSJPGVCLVCVKQH_TBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_pdUhzIzwqLTirgsJpgvCLVcVkqH
struct  pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_pdUhzIzwqLTirgsJpgvCLVcVkqH::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_pdUhzIzwqLTirgsJpgvCLVcVkqH::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_pdUhzIzwqLTirgsJpgvCLVcVkqH::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_pdUhzIzwqLTirgsJpgvCLVcVkqH::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Collections.Generic.IList`1<Rewired.CustomController> Rewired.Player_ControllerHelper_PollingHelper_pdUhzIzwqLTirgsJpgvCLVcVkqH::AzFeQWbdNSyHzOokjjlhrPLiOoy
	RuntimeObject* ___AzFeQWbdNSyHzOokjjlhrPLiOoy_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_pdUhzIzwqLTirgsJpgvCLVcVkqH::qkbWfWnlUBDMvLFHbNZWOlxXNoo
	int32_t ___qkbWfWnlUBDMvLFHbNZWOlxXNoo_5;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_pdUhzIzwqLTirgsJpgvCLVcVkqH::KxEcRhLfTSHONByKtaBOnhSOBkc
	int32_t ___KxEcRhLfTSHONByKtaBOnhSOBkc_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_pdUhzIzwqLTirgsJpgvCLVcVkqH::licCgLfhviTZigQtvIJIffWaosgd
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___licCgLfhviTZigQtvIJIffWaosgd_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_pdUhzIzwqLTirgsJpgvCLVcVkqH::HtrfGgyOdYsirINnQkoSMuavqdB
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___HtrfGgyOdYsirINnQkoSMuavqdB_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_pdUhzIzwqLTirgsJpgvCLVcVkqH::ejjQxPHABCGxuasGvROOvfNVVPNT
	RuntimeObject* ___ejjQxPHABCGxuasGvROOvfNVVPNT_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_AzFeQWbdNSyHzOokjjlhrPLiOoy_4() { return static_cast<int32_t>(offsetof(pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512, ___AzFeQWbdNSyHzOokjjlhrPLiOoy_4)); }
	inline RuntimeObject* get_AzFeQWbdNSyHzOokjjlhrPLiOoy_4() const { return ___AzFeQWbdNSyHzOokjjlhrPLiOoy_4; }
	inline RuntimeObject** get_address_of_AzFeQWbdNSyHzOokjjlhrPLiOoy_4() { return &___AzFeQWbdNSyHzOokjjlhrPLiOoy_4; }
	inline void set_AzFeQWbdNSyHzOokjjlhrPLiOoy_4(RuntimeObject* value)
	{
		___AzFeQWbdNSyHzOokjjlhrPLiOoy_4 = value;
		Il2CppCodeGenWriteBarrier((&___AzFeQWbdNSyHzOokjjlhrPLiOoy_4), value);
	}

	inline static int32_t get_offset_of_qkbWfWnlUBDMvLFHbNZWOlxXNoo_5() { return static_cast<int32_t>(offsetof(pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512, ___qkbWfWnlUBDMvLFHbNZWOlxXNoo_5)); }
	inline int32_t get_qkbWfWnlUBDMvLFHbNZWOlxXNoo_5() const { return ___qkbWfWnlUBDMvLFHbNZWOlxXNoo_5; }
	inline int32_t* get_address_of_qkbWfWnlUBDMvLFHbNZWOlxXNoo_5() { return &___qkbWfWnlUBDMvLFHbNZWOlxXNoo_5; }
	inline void set_qkbWfWnlUBDMvLFHbNZWOlxXNoo_5(int32_t value)
	{
		___qkbWfWnlUBDMvLFHbNZWOlxXNoo_5 = value;
	}

	inline static int32_t get_offset_of_KxEcRhLfTSHONByKtaBOnhSOBkc_6() { return static_cast<int32_t>(offsetof(pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512, ___KxEcRhLfTSHONByKtaBOnhSOBkc_6)); }
	inline int32_t get_KxEcRhLfTSHONByKtaBOnhSOBkc_6() const { return ___KxEcRhLfTSHONByKtaBOnhSOBkc_6; }
	inline int32_t* get_address_of_KxEcRhLfTSHONByKtaBOnhSOBkc_6() { return &___KxEcRhLfTSHONByKtaBOnhSOBkc_6; }
	inline void set_KxEcRhLfTSHONByKtaBOnhSOBkc_6(int32_t value)
	{
		___KxEcRhLfTSHONByKtaBOnhSOBkc_6 = value;
	}

	inline static int32_t get_offset_of_licCgLfhviTZigQtvIJIffWaosgd_7() { return static_cast<int32_t>(offsetof(pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512, ___licCgLfhviTZigQtvIJIffWaosgd_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_licCgLfhviTZigQtvIJIffWaosgd_7() const { return ___licCgLfhviTZigQtvIJIffWaosgd_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_licCgLfhviTZigQtvIJIffWaosgd_7() { return &___licCgLfhviTZigQtvIJIffWaosgd_7; }
	inline void set_licCgLfhviTZigQtvIJIffWaosgd_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___licCgLfhviTZigQtvIJIffWaosgd_7 = value;
	}

	inline static int32_t get_offset_of_HtrfGgyOdYsirINnQkoSMuavqdB_8() { return static_cast<int32_t>(offsetof(pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512, ___HtrfGgyOdYsirINnQkoSMuavqdB_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_HtrfGgyOdYsirINnQkoSMuavqdB_8() const { return ___HtrfGgyOdYsirINnQkoSMuavqdB_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_HtrfGgyOdYsirINnQkoSMuavqdB_8() { return &___HtrfGgyOdYsirINnQkoSMuavqdB_8; }
	inline void set_HtrfGgyOdYsirINnQkoSMuavqdB_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___HtrfGgyOdYsirINnQkoSMuavqdB_8 = value;
	}

	inline static int32_t get_offset_of_ejjQxPHABCGxuasGvROOvfNVVPNT_9() { return static_cast<int32_t>(offsetof(pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512, ___ejjQxPHABCGxuasGvROOvfNVVPNT_9)); }
	inline RuntimeObject* get_ejjQxPHABCGxuasGvROOvfNVVPNT_9() const { return ___ejjQxPHABCGxuasGvROOvfNVVPNT_9; }
	inline RuntimeObject** get_address_of_ejjQxPHABCGxuasGvROOvfNVVPNT_9() { return &___ejjQxPHABCGxuasGvROOvfNVVPNT_9; }
	inline void set_ejjQxPHABCGxuasGvROOvfNVVPNT_9(RuntimeObject* value)
	{
		___ejjQxPHABCGxuasGvROOvfNVVPNT_9 = value;
		Il2CppCodeGenWriteBarrier((&___ejjQxPHABCGxuasGvROOvfNVVPNT_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PDUHZIZWQLTIRGSJPGVCLVCVKQH_TBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512_H
#ifndef TCSKCSIHMVCYQZNCXLMHCAPZBSY_TEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E_H
#define TCSKCSIHMVCYQZNCXLMHCAPZBSY_TEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_tCskCsIHMVcyQZNCxlmhcaPzbSY
struct  tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_tCskCsIHMVcyQZNCxlmhcaPzbSY::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_tCskCsIHMVcyQZNCxlmhcaPzbSY::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_tCskCsIHMVcyQZNCxlmhcaPzbSY::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_tCskCsIHMVcyQZNCxlmhcaPzbSY::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_tCskCsIHMVcyQZNCxlmhcaPzbSY::omdPDTkbMQMCXIvHqkpJkHTSFLE
	int32_t ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_tCskCsIHMVcyQZNCxlmhcaPzbSY::eVNodIsBtpdoajQYgHVzEGonurY
	int32_t ___eVNodIsBtpdoajQYgHVzEGonurY_5;
	// Rewired.CustomController Rewired.Player_ControllerHelper_PollingHelper_tCskCsIHMVcyQZNCxlmhcaPzbSY::KYWLiBSXJxrwyYQybJcFUpwpAAr
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * ___KYWLiBSXJxrwyYQybJcFUpwpAAr_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_tCskCsIHMVcyQZNCxlmhcaPzbSY::VBRuVwCysljKLQBiDWwlpXrFkUy
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___VBRuVwCysljKLQBiDWwlpXrFkUy_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_tCskCsIHMVcyQZNCxlmhcaPzbSY::tNseVKBmKSodAyituDztyLjGwve
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___tNseVKBmKSodAyituDztyLjGwve_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_tCskCsIHMVcyQZNCxlmhcaPzbSY::yOkkFJfJsdRFLNpPIRjTWlHucNG
	RuntimeObject* ___yOkkFJfJsdRFLNpPIRjTWlHucNG_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return static_cast<int32_t>(offsetof(tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E, ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4)); }
	inline int32_t get_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() const { return ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline int32_t* get_address_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return &___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline void set_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(int32_t value)
	{
		___omdPDTkbMQMCXIvHqkpJkHTSFLE_4 = value;
	}

	inline static int32_t get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return static_cast<int32_t>(offsetof(tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E, ___eVNodIsBtpdoajQYgHVzEGonurY_5)); }
	inline int32_t get_eVNodIsBtpdoajQYgHVzEGonurY_5() const { return ___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline int32_t* get_address_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return &___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline void set_eVNodIsBtpdoajQYgHVzEGonurY_5(int32_t value)
	{
		___eVNodIsBtpdoajQYgHVzEGonurY_5 = value;
	}

	inline static int32_t get_offset_of_KYWLiBSXJxrwyYQybJcFUpwpAAr_6() { return static_cast<int32_t>(offsetof(tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E, ___KYWLiBSXJxrwyYQybJcFUpwpAAr_6)); }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * get_KYWLiBSXJxrwyYQybJcFUpwpAAr_6() const { return ___KYWLiBSXJxrwyYQybJcFUpwpAAr_6; }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A ** get_address_of_KYWLiBSXJxrwyYQybJcFUpwpAAr_6() { return &___KYWLiBSXJxrwyYQybJcFUpwpAAr_6; }
	inline void set_KYWLiBSXJxrwyYQybJcFUpwpAAr_6(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * value)
	{
		___KYWLiBSXJxrwyYQybJcFUpwpAAr_6 = value;
		Il2CppCodeGenWriteBarrier((&___KYWLiBSXJxrwyYQybJcFUpwpAAr_6), value);
	}

	inline static int32_t get_offset_of_VBRuVwCysljKLQBiDWwlpXrFkUy_7() { return static_cast<int32_t>(offsetof(tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E, ___VBRuVwCysljKLQBiDWwlpXrFkUy_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_VBRuVwCysljKLQBiDWwlpXrFkUy_7() const { return ___VBRuVwCysljKLQBiDWwlpXrFkUy_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_VBRuVwCysljKLQBiDWwlpXrFkUy_7() { return &___VBRuVwCysljKLQBiDWwlpXrFkUy_7; }
	inline void set_VBRuVwCysljKLQBiDWwlpXrFkUy_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___VBRuVwCysljKLQBiDWwlpXrFkUy_7 = value;
	}

	inline static int32_t get_offset_of_tNseVKBmKSodAyituDztyLjGwve_8() { return static_cast<int32_t>(offsetof(tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E, ___tNseVKBmKSodAyituDztyLjGwve_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_tNseVKBmKSodAyituDztyLjGwve_8() const { return ___tNseVKBmKSodAyituDztyLjGwve_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_tNseVKBmKSodAyituDztyLjGwve_8() { return &___tNseVKBmKSodAyituDztyLjGwve_8; }
	inline void set_tNseVKBmKSodAyituDztyLjGwve_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___tNseVKBmKSodAyituDztyLjGwve_8 = value;
	}

	inline static int32_t get_offset_of_yOkkFJfJsdRFLNpPIRjTWlHucNG_9() { return static_cast<int32_t>(offsetof(tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E, ___yOkkFJfJsdRFLNpPIRjTWlHucNG_9)); }
	inline RuntimeObject* get_yOkkFJfJsdRFLNpPIRjTWlHucNG_9() const { return ___yOkkFJfJsdRFLNpPIRjTWlHucNG_9; }
	inline RuntimeObject** get_address_of_yOkkFJfJsdRFLNpPIRjTWlHucNG_9() { return &___yOkkFJfJsdRFLNpPIRjTWlHucNG_9; }
	inline void set_yOkkFJfJsdRFLNpPIRjTWlHucNG_9(RuntimeObject* value)
	{
		___yOkkFJfJsdRFLNpPIRjTWlHucNG_9 = value;
		Il2CppCodeGenWriteBarrier((&___yOkkFJfJsdRFLNpPIRjTWlHucNG_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TCSKCSIHMVCYQZNCXLMHCAPZBSY_TEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E_H
#ifndef VMARWQELMXIIPZJJBDQSEGYRAAHA_TC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586_H
#define VMARWQELMXIIPZJJBDQSEGYRAAHA_TC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_vMArwqElmXiIpzjjBdQsEgyRaaha
struct  vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_vMArwqElmXiIpzjjBdQsEgyRaaha::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_vMArwqElmXiIpzjjBdQsEgyRaaha::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_vMArwqElmXiIpzjjBdQsEgyRaaha::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_vMArwqElmXiIpzjjBdQsEgyRaaha::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Collections.Generic.IList`1<Rewired.Joystick> Rewired.Player_ControllerHelper_PollingHelper_vMArwqElmXiIpzjjBdQsEgyRaaha::LJHlfEVUxTkHDSgQmduVhmOmhIR
	RuntimeObject* ___LJHlfEVUxTkHDSgQmduVhmOmhIR_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_vMArwqElmXiIpzjjBdQsEgyRaaha::VBfAjlpfIAcskfNhPMYyyTLNttPJ
	int32_t ___VBfAjlpfIAcskfNhPMYyyTLNttPJ_5;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_vMArwqElmXiIpzjjBdQsEgyRaaha::aYoGSMnqNTemdgpuJcbubHCDowZP
	int32_t ___aYoGSMnqNTemdgpuJcbubHCDowZP_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_vMArwqElmXiIpzjjBdQsEgyRaaha::moOSaqGEcYIjbaNbsqBPUMvbCne
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___moOSaqGEcYIjbaNbsqBPUMvbCne_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_vMArwqElmXiIpzjjBdQsEgyRaaha::rJUgbWivcyxaYiJSzSuBsSAmYyX
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___rJUgbWivcyxaYiJSzSuBsSAmYyX_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_vMArwqElmXiIpzjjBdQsEgyRaaha::wtVkGLxYEZhrNggRFLXUKCWwjGD
	RuntimeObject* ___wtVkGLxYEZhrNggRFLXUKCWwjGD_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_LJHlfEVUxTkHDSgQmduVhmOmhIR_4() { return static_cast<int32_t>(offsetof(vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586, ___LJHlfEVUxTkHDSgQmduVhmOmhIR_4)); }
	inline RuntimeObject* get_LJHlfEVUxTkHDSgQmduVhmOmhIR_4() const { return ___LJHlfEVUxTkHDSgQmduVhmOmhIR_4; }
	inline RuntimeObject** get_address_of_LJHlfEVUxTkHDSgQmduVhmOmhIR_4() { return &___LJHlfEVUxTkHDSgQmduVhmOmhIR_4; }
	inline void set_LJHlfEVUxTkHDSgQmduVhmOmhIR_4(RuntimeObject* value)
	{
		___LJHlfEVUxTkHDSgQmduVhmOmhIR_4 = value;
		Il2CppCodeGenWriteBarrier((&___LJHlfEVUxTkHDSgQmduVhmOmhIR_4), value);
	}

	inline static int32_t get_offset_of_VBfAjlpfIAcskfNhPMYyyTLNttPJ_5() { return static_cast<int32_t>(offsetof(vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586, ___VBfAjlpfIAcskfNhPMYyyTLNttPJ_5)); }
	inline int32_t get_VBfAjlpfIAcskfNhPMYyyTLNttPJ_5() const { return ___VBfAjlpfIAcskfNhPMYyyTLNttPJ_5; }
	inline int32_t* get_address_of_VBfAjlpfIAcskfNhPMYyyTLNttPJ_5() { return &___VBfAjlpfIAcskfNhPMYyyTLNttPJ_5; }
	inline void set_VBfAjlpfIAcskfNhPMYyyTLNttPJ_5(int32_t value)
	{
		___VBfAjlpfIAcskfNhPMYyyTLNttPJ_5 = value;
	}

	inline static int32_t get_offset_of_aYoGSMnqNTemdgpuJcbubHCDowZP_6() { return static_cast<int32_t>(offsetof(vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586, ___aYoGSMnqNTemdgpuJcbubHCDowZP_6)); }
	inline int32_t get_aYoGSMnqNTemdgpuJcbubHCDowZP_6() const { return ___aYoGSMnqNTemdgpuJcbubHCDowZP_6; }
	inline int32_t* get_address_of_aYoGSMnqNTemdgpuJcbubHCDowZP_6() { return &___aYoGSMnqNTemdgpuJcbubHCDowZP_6; }
	inline void set_aYoGSMnqNTemdgpuJcbubHCDowZP_6(int32_t value)
	{
		___aYoGSMnqNTemdgpuJcbubHCDowZP_6 = value;
	}

	inline static int32_t get_offset_of_moOSaqGEcYIjbaNbsqBPUMvbCne_7() { return static_cast<int32_t>(offsetof(vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586, ___moOSaqGEcYIjbaNbsqBPUMvbCne_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_moOSaqGEcYIjbaNbsqBPUMvbCne_7() const { return ___moOSaqGEcYIjbaNbsqBPUMvbCne_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_moOSaqGEcYIjbaNbsqBPUMvbCne_7() { return &___moOSaqGEcYIjbaNbsqBPUMvbCne_7; }
	inline void set_moOSaqGEcYIjbaNbsqBPUMvbCne_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___moOSaqGEcYIjbaNbsqBPUMvbCne_7 = value;
	}

	inline static int32_t get_offset_of_rJUgbWivcyxaYiJSzSuBsSAmYyX_8() { return static_cast<int32_t>(offsetof(vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586, ___rJUgbWivcyxaYiJSzSuBsSAmYyX_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_rJUgbWivcyxaYiJSzSuBsSAmYyX_8() const { return ___rJUgbWivcyxaYiJSzSuBsSAmYyX_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_rJUgbWivcyxaYiJSzSuBsSAmYyX_8() { return &___rJUgbWivcyxaYiJSzSuBsSAmYyX_8; }
	inline void set_rJUgbWivcyxaYiJSzSuBsSAmYyX_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___rJUgbWivcyxaYiJSzSuBsSAmYyX_8 = value;
	}

	inline static int32_t get_offset_of_wtVkGLxYEZhrNggRFLXUKCWwjGD_9() { return static_cast<int32_t>(offsetof(vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586, ___wtVkGLxYEZhrNggRFLXUKCWwjGD_9)); }
	inline RuntimeObject* get_wtVkGLxYEZhrNggRFLXUKCWwjGD_9() const { return ___wtVkGLxYEZhrNggRFLXUKCWwjGD_9; }
	inline RuntimeObject** get_address_of_wtVkGLxYEZhrNggRFLXUKCWwjGD_9() { return &___wtVkGLxYEZhrNggRFLXUKCWwjGD_9; }
	inline void set_wtVkGLxYEZhrNggRFLXUKCWwjGD_9(RuntimeObject* value)
	{
		___wtVkGLxYEZhrNggRFLXUKCWwjGD_9 = value;
		Il2CppCodeGenWriteBarrier((&___wtVkGLxYEZhrNggRFLXUKCWwjGD_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VMARWQELMXIIPZJJBDQSEGYRAAHA_TC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586_H
#ifndef XRLYMDJDWWZOITLHDAJJUQGBXAQ_TBF38507916FB399EA565CF03A94A607D5D37FD8D_H
#define XRLYMDJDWWZOITLHDAJJUQGBXAQ_TBF38507916FB399EA565CF03A94A607D5D37FD8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_xrlYMDjdWWzOitLHDajJuQGbxaQ
struct  xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_xrlYMDjdWWzOitLHDajJuQGbxaQ::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_xrlYMDjdWWzOitLHDajJuQGbxaQ::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_xrlYMDjdWWzOitLHDajJuQGbxaQ::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_xrlYMDjdWWzOitLHDajJuQGbxaQ::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_xrlYMDjdWWzOitLHDajJuQGbxaQ::omdPDTkbMQMCXIvHqkpJkHTSFLE
	int32_t ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_xrlYMDjdWWzOitLHDajJuQGbxaQ::eVNodIsBtpdoajQYgHVzEGonurY
	int32_t ___eVNodIsBtpdoajQYgHVzEGonurY_5;
	// Rewired.CustomController Rewired.Player_ControllerHelper_PollingHelper_xrlYMDjdWWzOitLHDajJuQGbxaQ::DbEuFCCKKECbasxrlRTEXgKEmHm
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * ___DbEuFCCKKECbasxrlRTEXgKEmHm_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_xrlYMDjdWWzOitLHDajJuQGbxaQ::bwjpmclZNIwLkRfWzaZHTVHKInxa
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___bwjpmclZNIwLkRfWzaZHTVHKInxa_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_xrlYMDjdWWzOitLHDajJuQGbxaQ::UVRcHhbtljYQdDMOHhPGAvLMDyd
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___UVRcHhbtljYQdDMOHhPGAvLMDyd_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_xrlYMDjdWWzOitLHDajJuQGbxaQ::LWzUkgaucDOPLJJjoJBjOaDZPQI
	RuntimeObject* ___LWzUkgaucDOPLJJjoJBjOaDZPQI_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return static_cast<int32_t>(offsetof(xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D, ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4)); }
	inline int32_t get_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() const { return ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline int32_t* get_address_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return &___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline void set_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(int32_t value)
	{
		___omdPDTkbMQMCXIvHqkpJkHTSFLE_4 = value;
	}

	inline static int32_t get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return static_cast<int32_t>(offsetof(xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D, ___eVNodIsBtpdoajQYgHVzEGonurY_5)); }
	inline int32_t get_eVNodIsBtpdoajQYgHVzEGonurY_5() const { return ___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline int32_t* get_address_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return &___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline void set_eVNodIsBtpdoajQYgHVzEGonurY_5(int32_t value)
	{
		___eVNodIsBtpdoajQYgHVzEGonurY_5 = value;
	}

	inline static int32_t get_offset_of_DbEuFCCKKECbasxrlRTEXgKEmHm_6() { return static_cast<int32_t>(offsetof(xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D, ___DbEuFCCKKECbasxrlRTEXgKEmHm_6)); }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * get_DbEuFCCKKECbasxrlRTEXgKEmHm_6() const { return ___DbEuFCCKKECbasxrlRTEXgKEmHm_6; }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A ** get_address_of_DbEuFCCKKECbasxrlRTEXgKEmHm_6() { return &___DbEuFCCKKECbasxrlRTEXgKEmHm_6; }
	inline void set_DbEuFCCKKECbasxrlRTEXgKEmHm_6(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * value)
	{
		___DbEuFCCKKECbasxrlRTEXgKEmHm_6 = value;
		Il2CppCodeGenWriteBarrier((&___DbEuFCCKKECbasxrlRTEXgKEmHm_6), value);
	}

	inline static int32_t get_offset_of_bwjpmclZNIwLkRfWzaZHTVHKInxa_7() { return static_cast<int32_t>(offsetof(xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D, ___bwjpmclZNIwLkRfWzaZHTVHKInxa_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_bwjpmclZNIwLkRfWzaZHTVHKInxa_7() const { return ___bwjpmclZNIwLkRfWzaZHTVHKInxa_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_bwjpmclZNIwLkRfWzaZHTVHKInxa_7() { return &___bwjpmclZNIwLkRfWzaZHTVHKInxa_7; }
	inline void set_bwjpmclZNIwLkRfWzaZHTVHKInxa_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___bwjpmclZNIwLkRfWzaZHTVHKInxa_7 = value;
	}

	inline static int32_t get_offset_of_UVRcHhbtljYQdDMOHhPGAvLMDyd_8() { return static_cast<int32_t>(offsetof(xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D, ___UVRcHhbtljYQdDMOHhPGAvLMDyd_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_UVRcHhbtljYQdDMOHhPGAvLMDyd_8() const { return ___UVRcHhbtljYQdDMOHhPGAvLMDyd_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_UVRcHhbtljYQdDMOHhPGAvLMDyd_8() { return &___UVRcHhbtljYQdDMOHhPGAvLMDyd_8; }
	inline void set_UVRcHhbtljYQdDMOHhPGAvLMDyd_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___UVRcHhbtljYQdDMOHhPGAvLMDyd_8 = value;
	}

	inline static int32_t get_offset_of_LWzUkgaucDOPLJJjoJBjOaDZPQI_9() { return static_cast<int32_t>(offsetof(xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D, ___LWzUkgaucDOPLJJjoJBjOaDZPQI_9)); }
	inline RuntimeObject* get_LWzUkgaucDOPLJJjoJBjOaDZPQI_9() const { return ___LWzUkgaucDOPLJJjoJBjOaDZPQI_9; }
	inline RuntimeObject** get_address_of_LWzUkgaucDOPLJJjoJBjOaDZPQI_9() { return &___LWzUkgaucDOPLJJjoJBjOaDZPQI_9; }
	inline void set_LWzUkgaucDOPLJJjoJBjOaDZPQI_9(RuntimeObject* value)
	{
		___LWzUkgaucDOPLJJjoJBjOaDZPQI_9 = value;
		Il2CppCodeGenWriteBarrier((&___LWzUkgaucDOPLJJjoJBjOaDZPQI_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRLYMDJDWWZOITLHDAJJUQGBXAQ_TBF38507916FB399EA565CF03A94A607D5D37FD8D_H
#ifndef YZUCGJEHTUUGGQGTMNKYZSAHWWDI_TE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5_H
#define YZUCGJEHTUUGGQGTMNKYZSAHWWDI_TE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Player_ControllerHelper_PollingHelper_yZUcGJEhtUUgGQGtmnkYzSaHWWDi
struct  yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_yZUcGJEhtUUgGQGtmnkYzSaHWWDi::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_yZUcGJEhtUUgGQGtmnkYzSaHWWDi::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_yZUcGJEhtUUgGQGtmnkYzSaHWWDi::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.Player_ControllerHelper_PollingHelper Rewired.Player_ControllerHelper_PollingHelper_yZUcGJEhtUUgGQGtmnkYzSaHWWDi::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_yZUcGJEhtUUgGQGtmnkYzSaHWWDi::omdPDTkbMQMCXIvHqkpJkHTSFLE
	int32_t ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4;
	// System.Int32 Rewired.Player_ControllerHelper_PollingHelper_yZUcGJEhtUUgGQGtmnkYzSaHWWDi::eVNodIsBtpdoajQYgHVzEGonurY
	int32_t ___eVNodIsBtpdoajQYgHVzEGonurY_5;
	// Rewired.CustomController Rewired.Player_ControllerHelper_PollingHelper_yZUcGJEhtUUgGQGtmnkYzSaHWWDi::YeLZFoQfDBSORYCuWDOuawbTVVYO
	CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * ___YeLZFoQfDBSORYCuWDOuawbTVVYO_6;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_yZUcGJEhtUUgGQGtmnkYzSaHWWDi::MsPbEzbRQgWGkpgEHmeVAFLhAzer
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___MsPbEzbRQgWGkpgEHmeVAFLhAzer_7;
	// Rewired.ControllerPollingInfo Rewired.Player_ControllerHelper_PollingHelper_yZUcGJEhtUUgGQGtmnkYzSaHWWDi::nthCnDjDDeUgwqRvjHFEwODUjwJ
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___nthCnDjDDeUgwqRvjHFEwODUjwJ_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.Player_ControllerHelper_PollingHelper_yZUcGJEhtUUgGQGtmnkYzSaHWWDi::flaCxMkdweYiaNkapsZttdRMACw
	RuntimeObject* ___flaCxMkdweYiaNkapsZttdRMACw_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24 * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return static_cast<int32_t>(offsetof(yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5, ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4)); }
	inline int32_t get_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() const { return ___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline int32_t* get_address_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4() { return &___omdPDTkbMQMCXIvHqkpJkHTSFLE_4; }
	inline void set_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(int32_t value)
	{
		___omdPDTkbMQMCXIvHqkpJkHTSFLE_4 = value;
	}

	inline static int32_t get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return static_cast<int32_t>(offsetof(yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5, ___eVNodIsBtpdoajQYgHVzEGonurY_5)); }
	inline int32_t get_eVNodIsBtpdoajQYgHVzEGonurY_5() const { return ___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline int32_t* get_address_of_eVNodIsBtpdoajQYgHVzEGonurY_5() { return &___eVNodIsBtpdoajQYgHVzEGonurY_5; }
	inline void set_eVNodIsBtpdoajQYgHVzEGonurY_5(int32_t value)
	{
		___eVNodIsBtpdoajQYgHVzEGonurY_5 = value;
	}

	inline static int32_t get_offset_of_YeLZFoQfDBSORYCuWDOuawbTVVYO_6() { return static_cast<int32_t>(offsetof(yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5, ___YeLZFoQfDBSORYCuWDOuawbTVVYO_6)); }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * get_YeLZFoQfDBSORYCuWDOuawbTVVYO_6() const { return ___YeLZFoQfDBSORYCuWDOuawbTVVYO_6; }
	inline CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A ** get_address_of_YeLZFoQfDBSORYCuWDOuawbTVVYO_6() { return &___YeLZFoQfDBSORYCuWDOuawbTVVYO_6; }
	inline void set_YeLZFoQfDBSORYCuWDOuawbTVVYO_6(CustomController_tDAEEDDC6632D300249A22FA978C1360DE1EB0C1A * value)
	{
		___YeLZFoQfDBSORYCuWDOuawbTVVYO_6 = value;
		Il2CppCodeGenWriteBarrier((&___YeLZFoQfDBSORYCuWDOuawbTVVYO_6), value);
	}

	inline static int32_t get_offset_of_MsPbEzbRQgWGkpgEHmeVAFLhAzer_7() { return static_cast<int32_t>(offsetof(yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5, ___MsPbEzbRQgWGkpgEHmeVAFLhAzer_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_MsPbEzbRQgWGkpgEHmeVAFLhAzer_7() const { return ___MsPbEzbRQgWGkpgEHmeVAFLhAzer_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_MsPbEzbRQgWGkpgEHmeVAFLhAzer_7() { return &___MsPbEzbRQgWGkpgEHmeVAFLhAzer_7; }
	inline void set_MsPbEzbRQgWGkpgEHmeVAFLhAzer_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___MsPbEzbRQgWGkpgEHmeVAFLhAzer_7 = value;
	}

	inline static int32_t get_offset_of_nthCnDjDDeUgwqRvjHFEwODUjwJ_8() { return static_cast<int32_t>(offsetof(yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5, ___nthCnDjDDeUgwqRvjHFEwODUjwJ_8)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_nthCnDjDDeUgwqRvjHFEwODUjwJ_8() const { return ___nthCnDjDDeUgwqRvjHFEwODUjwJ_8; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_nthCnDjDDeUgwqRvjHFEwODUjwJ_8() { return &___nthCnDjDDeUgwqRvjHFEwODUjwJ_8; }
	inline void set_nthCnDjDDeUgwqRvjHFEwODUjwJ_8(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___nthCnDjDDeUgwqRvjHFEwODUjwJ_8 = value;
	}

	inline static int32_t get_offset_of_flaCxMkdweYiaNkapsZttdRMACw_9() { return static_cast<int32_t>(offsetof(yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5, ___flaCxMkdweYiaNkapsZttdRMACw_9)); }
	inline RuntimeObject* get_flaCxMkdweYiaNkapsZttdRMACw_9() const { return ___flaCxMkdweYiaNkapsZttdRMACw_9; }
	inline RuntimeObject** get_address_of_flaCxMkdweYiaNkapsZttdRMACw_9() { return &___flaCxMkdweYiaNkapsZttdRMACw_9; }
	inline void set_flaCxMkdweYiaNkapsZttdRMACw_9(RuntimeObject* value)
	{
		___flaCxMkdweYiaNkapsZttdRMACw_9 = value;
		Il2CppCodeGenWriteBarrier((&___flaCxMkdweYiaNkapsZttdRMACw_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YZUCGJEHTUUGGQGTMNKYZSAHWWDI_TE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5_H
#ifndef EJYJZJWMFDUHSJVQDPASMSNGCVD_TCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD_H
#define EJYJZJWMFDUHSJVQDPASMSNGCVD_TCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_EJyJzjWMFDUHsJVqdpaSMSnGCVd
struct  EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_EJyJzjWMFDUHsJVqdpaSMSnGCVd::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_EJyJzjWMFDUHsJVqdpaSMSnGCVd::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_EJyJzjWMFDUHsJVqdpaSMSnGCVd::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Collections.Generic.IList`1<Rewired.Joystick> Rewired.ReInput_ControllerHelper_PollingHelper_EJyJzjWMFDUHsJVqdpaSMSnGCVd::SVZrVgRVsLzKoKBEyGIGeAEQgSya
	RuntimeObject* ___SVZrVgRVsLzKoKBEyGIGeAEQgSya_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_EJyJzjWMFDUHsJVqdpaSMSnGCVd::cWJKTKYmtCVmdGGjjjsmZdZSCFF
	int32_t ___cWJKTKYmtCVmdGGjjjsmZdZSCFF_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_EJyJzjWMFDUHsJVqdpaSMSnGCVd::zrIFwcCjOlaXNtcQfMloCLSKmpO
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___zrIFwcCjOlaXNtcQfMloCLSKmpO_5;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_EJyJzjWMFDUHsJVqdpaSMSnGCVd::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_EJyJzjWMFDUHsJVqdpaSMSnGCVd::mDkjXsweizhvYQUYrFNNZmYteSuc
	RuntimeObject* ___mDkjXsweizhvYQUYrFNNZmYteSuc_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_SVZrVgRVsLzKoKBEyGIGeAEQgSya_3() { return static_cast<int32_t>(offsetof(EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD, ___SVZrVgRVsLzKoKBEyGIGeAEQgSya_3)); }
	inline RuntimeObject* get_SVZrVgRVsLzKoKBEyGIGeAEQgSya_3() const { return ___SVZrVgRVsLzKoKBEyGIGeAEQgSya_3; }
	inline RuntimeObject** get_address_of_SVZrVgRVsLzKoKBEyGIGeAEQgSya_3() { return &___SVZrVgRVsLzKoKBEyGIGeAEQgSya_3; }
	inline void set_SVZrVgRVsLzKoKBEyGIGeAEQgSya_3(RuntimeObject* value)
	{
		___SVZrVgRVsLzKoKBEyGIGeAEQgSya_3 = value;
		Il2CppCodeGenWriteBarrier((&___SVZrVgRVsLzKoKBEyGIGeAEQgSya_3), value);
	}

	inline static int32_t get_offset_of_cWJKTKYmtCVmdGGjjjsmZdZSCFF_4() { return static_cast<int32_t>(offsetof(EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD, ___cWJKTKYmtCVmdGGjjjsmZdZSCFF_4)); }
	inline int32_t get_cWJKTKYmtCVmdGGjjjsmZdZSCFF_4() const { return ___cWJKTKYmtCVmdGGjjjsmZdZSCFF_4; }
	inline int32_t* get_address_of_cWJKTKYmtCVmdGGjjjsmZdZSCFF_4() { return &___cWJKTKYmtCVmdGGjjjsmZdZSCFF_4; }
	inline void set_cWJKTKYmtCVmdGGjjjsmZdZSCFF_4(int32_t value)
	{
		___cWJKTKYmtCVmdGGjjjsmZdZSCFF_4 = value;
	}

	inline static int32_t get_offset_of_zrIFwcCjOlaXNtcQfMloCLSKmpO_5() { return static_cast<int32_t>(offsetof(EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD, ___zrIFwcCjOlaXNtcQfMloCLSKmpO_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_zrIFwcCjOlaXNtcQfMloCLSKmpO_5() const { return ___zrIFwcCjOlaXNtcQfMloCLSKmpO_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_zrIFwcCjOlaXNtcQfMloCLSKmpO_5() { return &___zrIFwcCjOlaXNtcQfMloCLSKmpO_5; }
	inline void set_zrIFwcCjOlaXNtcQfMloCLSKmpO_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___zrIFwcCjOlaXNtcQfMloCLSKmpO_5 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return static_cast<int32_t>(offsetof(EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_6 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_6), value);
	}

	inline static int32_t get_offset_of_mDkjXsweizhvYQUYrFNNZmYteSuc_7() { return static_cast<int32_t>(offsetof(EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD, ___mDkjXsweizhvYQUYrFNNZmYteSuc_7)); }
	inline RuntimeObject* get_mDkjXsweizhvYQUYrFNNZmYteSuc_7() const { return ___mDkjXsweizhvYQUYrFNNZmYteSuc_7; }
	inline RuntimeObject** get_address_of_mDkjXsweizhvYQUYrFNNZmYteSuc_7() { return &___mDkjXsweizhvYQUYrFNNZmYteSuc_7; }
	inline void set_mDkjXsweizhvYQUYrFNNZmYteSuc_7(RuntimeObject* value)
	{
		___mDkjXsweizhvYQUYrFNNZmYteSuc_7 = value;
		Il2CppCodeGenWriteBarrier((&___mDkjXsweizhvYQUYrFNNZmYteSuc_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EJYJZJWMFDUHSJVQDPASMSNGCVD_TCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD_H
#ifndef FOHMMGWYKSTOJIIHXYFBSICWAXSA_T15EB7657A8D6116A6D853E97A4B1E3A9616AF37C_H
#define FOHMMGWYKSTOJIIHXYFBSICWAXSA_T15EB7657A8D6116A6D853E97A4B1E3A9616AF37C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa
struct  FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa::PzQGYPrYApVgoXjRjuPiTlFMZyE
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___PzQGYPrYApVgoXjRjuPiTlFMZyE_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa::YTCvkNnmkyGTCGciRjwpLcGOBXnA
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___YTCvkNnmkyGTCGciRjwpLcGOBXnA_5;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa::llDwhCdWKQdCqvhwDksoltjnHsu
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___llDwhCdWKQdCqvhwDksoltjnHsu_6;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa::MSHruCSpYGCtMzaUxvkUhMgZMOj
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___MSHruCSpYGCtMzaUxvkUhMgZMOj_7;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa::FphORXFifLeArZSZKuegTdmnbRaH
	RuntimeObject* ___FphORXFifLeArZSZKuegTdmnbRaH_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa::NqyaCTdoRAFKxaezesiyPtRRgdrL
	RuntimeObject* ___NqyaCTdoRAFKxaezesiyPtRRgdrL_9;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa::zjLLpEfaBZnjZybhxraZuerIOHG
	RuntimeObject* ___zjLLpEfaBZnjZybhxraZuerIOHG_10;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_FOHMMgWyksTOjIiHxyFbSICwaxSa::hkYuNBmNDkjQxhwZKekXGXqQETee
	RuntimeObject* ___hkYuNBmNDkjQxhwZKekXGXqQETee_11;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_PzQGYPrYApVgoXjRjuPiTlFMZyE_4() { return static_cast<int32_t>(offsetof(FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C, ___PzQGYPrYApVgoXjRjuPiTlFMZyE_4)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_PzQGYPrYApVgoXjRjuPiTlFMZyE_4() const { return ___PzQGYPrYApVgoXjRjuPiTlFMZyE_4; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_PzQGYPrYApVgoXjRjuPiTlFMZyE_4() { return &___PzQGYPrYApVgoXjRjuPiTlFMZyE_4; }
	inline void set_PzQGYPrYApVgoXjRjuPiTlFMZyE_4(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___PzQGYPrYApVgoXjRjuPiTlFMZyE_4 = value;
	}

	inline static int32_t get_offset_of_YTCvkNnmkyGTCGciRjwpLcGOBXnA_5() { return static_cast<int32_t>(offsetof(FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C, ___YTCvkNnmkyGTCGciRjwpLcGOBXnA_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_YTCvkNnmkyGTCGciRjwpLcGOBXnA_5() const { return ___YTCvkNnmkyGTCGciRjwpLcGOBXnA_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_YTCvkNnmkyGTCGciRjwpLcGOBXnA_5() { return &___YTCvkNnmkyGTCGciRjwpLcGOBXnA_5; }
	inline void set_YTCvkNnmkyGTCGciRjwpLcGOBXnA_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___YTCvkNnmkyGTCGciRjwpLcGOBXnA_5 = value;
	}

	inline static int32_t get_offset_of_llDwhCdWKQdCqvhwDksoltjnHsu_6() { return static_cast<int32_t>(offsetof(FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C, ___llDwhCdWKQdCqvhwDksoltjnHsu_6)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_llDwhCdWKQdCqvhwDksoltjnHsu_6() const { return ___llDwhCdWKQdCqvhwDksoltjnHsu_6; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_llDwhCdWKQdCqvhwDksoltjnHsu_6() { return &___llDwhCdWKQdCqvhwDksoltjnHsu_6; }
	inline void set_llDwhCdWKQdCqvhwDksoltjnHsu_6(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___llDwhCdWKQdCqvhwDksoltjnHsu_6 = value;
	}

	inline static int32_t get_offset_of_MSHruCSpYGCtMzaUxvkUhMgZMOj_7() { return static_cast<int32_t>(offsetof(FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C, ___MSHruCSpYGCtMzaUxvkUhMgZMOj_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_MSHruCSpYGCtMzaUxvkUhMgZMOj_7() const { return ___MSHruCSpYGCtMzaUxvkUhMgZMOj_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_MSHruCSpYGCtMzaUxvkUhMgZMOj_7() { return &___MSHruCSpYGCtMzaUxvkUhMgZMOj_7; }
	inline void set_MSHruCSpYGCtMzaUxvkUhMgZMOj_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___MSHruCSpYGCtMzaUxvkUhMgZMOj_7 = value;
	}

	inline static int32_t get_offset_of_FphORXFifLeArZSZKuegTdmnbRaH_8() { return static_cast<int32_t>(offsetof(FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C, ___FphORXFifLeArZSZKuegTdmnbRaH_8)); }
	inline RuntimeObject* get_FphORXFifLeArZSZKuegTdmnbRaH_8() const { return ___FphORXFifLeArZSZKuegTdmnbRaH_8; }
	inline RuntimeObject** get_address_of_FphORXFifLeArZSZKuegTdmnbRaH_8() { return &___FphORXFifLeArZSZKuegTdmnbRaH_8; }
	inline void set_FphORXFifLeArZSZKuegTdmnbRaH_8(RuntimeObject* value)
	{
		___FphORXFifLeArZSZKuegTdmnbRaH_8 = value;
		Il2CppCodeGenWriteBarrier((&___FphORXFifLeArZSZKuegTdmnbRaH_8), value);
	}

	inline static int32_t get_offset_of_NqyaCTdoRAFKxaezesiyPtRRgdrL_9() { return static_cast<int32_t>(offsetof(FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C, ___NqyaCTdoRAFKxaezesiyPtRRgdrL_9)); }
	inline RuntimeObject* get_NqyaCTdoRAFKxaezesiyPtRRgdrL_9() const { return ___NqyaCTdoRAFKxaezesiyPtRRgdrL_9; }
	inline RuntimeObject** get_address_of_NqyaCTdoRAFKxaezesiyPtRRgdrL_9() { return &___NqyaCTdoRAFKxaezesiyPtRRgdrL_9; }
	inline void set_NqyaCTdoRAFKxaezesiyPtRRgdrL_9(RuntimeObject* value)
	{
		___NqyaCTdoRAFKxaezesiyPtRRgdrL_9 = value;
		Il2CppCodeGenWriteBarrier((&___NqyaCTdoRAFKxaezesiyPtRRgdrL_9), value);
	}

	inline static int32_t get_offset_of_zjLLpEfaBZnjZybhxraZuerIOHG_10() { return static_cast<int32_t>(offsetof(FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C, ___zjLLpEfaBZnjZybhxraZuerIOHG_10)); }
	inline RuntimeObject* get_zjLLpEfaBZnjZybhxraZuerIOHG_10() const { return ___zjLLpEfaBZnjZybhxraZuerIOHG_10; }
	inline RuntimeObject** get_address_of_zjLLpEfaBZnjZybhxraZuerIOHG_10() { return &___zjLLpEfaBZnjZybhxraZuerIOHG_10; }
	inline void set_zjLLpEfaBZnjZybhxraZuerIOHG_10(RuntimeObject* value)
	{
		___zjLLpEfaBZnjZybhxraZuerIOHG_10 = value;
		Il2CppCodeGenWriteBarrier((&___zjLLpEfaBZnjZybhxraZuerIOHG_10), value);
	}

	inline static int32_t get_offset_of_hkYuNBmNDkjQxhwZKekXGXqQETee_11() { return static_cast<int32_t>(offsetof(FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C, ___hkYuNBmNDkjQxhwZKekXGXqQETee_11)); }
	inline RuntimeObject* get_hkYuNBmNDkjQxhwZKekXGXqQETee_11() const { return ___hkYuNBmNDkjQxhwZKekXGXqQETee_11; }
	inline RuntimeObject** get_address_of_hkYuNBmNDkjQxhwZKekXGXqQETee_11() { return &___hkYuNBmNDkjQxhwZKekXGXqQETee_11; }
	inline void set_hkYuNBmNDkjQxhwZKekXGXqQETee_11(RuntimeObject* value)
	{
		___hkYuNBmNDkjQxhwZKekXGXqQETee_11 = value;
		Il2CppCodeGenWriteBarrier((&___hkYuNBmNDkjQxhwZKekXGXqQETee_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOHMMGWYKSTOJIIHXYFBSICWAXSA_T15EB7657A8D6116A6D853E97A4B1E3A9616AF37C_H
#ifndef HLVBTVUBAXQVMHUFBHWYBWBTRBN_T482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3_H
#define HLVBTVUBAXQVMHUFBHWYBWBTRBN_T482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN
struct  HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN::ZAortNDnjLYCmiBrQFIAbhIaUKW
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___ZAortNDnjLYCmiBrQFIAbhIaUKW_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN::GgtWLeuiabgWtAgQzwEkLwUkXjN
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___GgtWLeuiabgWtAgQzwEkLwUkXjN_5;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN::rWAtWCEOCUwqgRWqpfXjLBoPbhz
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___rWAtWCEOCUwqgRWqpfXjLBoPbhz_6;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN::RPCPVQgmBrYXUBVzyqlkkGxmhDj
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___RPCPVQgmBrYXUBVzyqlkkGxmhDj_7;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN::TyVBFJKcdlQLriQmOXSFSSjNHmAy
	RuntimeObject* ___TyVBFJKcdlQLriQmOXSFSSjNHmAy_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN::wdcjvtQlCdWpBabpzfClHUOIozil
	RuntimeObject* ___wdcjvtQlCdWpBabpzfClHUOIozil_9;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN::PalmDKfwodOEEgPtuWClpZscTYK
	RuntimeObject* ___PalmDKfwodOEEgPtuWClpZscTYK_10;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_HlvbTvUbAxQvmHufBhwyBwBtrbN::ecNYhizHrPzvbIDpVcnLdKhcNIg
	RuntimeObject* ___ecNYhizHrPzvbIDpVcnLdKhcNIg_11;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_ZAortNDnjLYCmiBrQFIAbhIaUKW_4() { return static_cast<int32_t>(offsetof(HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3, ___ZAortNDnjLYCmiBrQFIAbhIaUKW_4)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_ZAortNDnjLYCmiBrQFIAbhIaUKW_4() const { return ___ZAortNDnjLYCmiBrQFIAbhIaUKW_4; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_ZAortNDnjLYCmiBrQFIAbhIaUKW_4() { return &___ZAortNDnjLYCmiBrQFIAbhIaUKW_4; }
	inline void set_ZAortNDnjLYCmiBrQFIAbhIaUKW_4(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___ZAortNDnjLYCmiBrQFIAbhIaUKW_4 = value;
	}

	inline static int32_t get_offset_of_GgtWLeuiabgWtAgQzwEkLwUkXjN_5() { return static_cast<int32_t>(offsetof(HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3, ___GgtWLeuiabgWtAgQzwEkLwUkXjN_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_GgtWLeuiabgWtAgQzwEkLwUkXjN_5() const { return ___GgtWLeuiabgWtAgQzwEkLwUkXjN_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_GgtWLeuiabgWtAgQzwEkLwUkXjN_5() { return &___GgtWLeuiabgWtAgQzwEkLwUkXjN_5; }
	inline void set_GgtWLeuiabgWtAgQzwEkLwUkXjN_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___GgtWLeuiabgWtAgQzwEkLwUkXjN_5 = value;
	}

	inline static int32_t get_offset_of_rWAtWCEOCUwqgRWqpfXjLBoPbhz_6() { return static_cast<int32_t>(offsetof(HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3, ___rWAtWCEOCUwqgRWqpfXjLBoPbhz_6)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_rWAtWCEOCUwqgRWqpfXjLBoPbhz_6() const { return ___rWAtWCEOCUwqgRWqpfXjLBoPbhz_6; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_rWAtWCEOCUwqgRWqpfXjLBoPbhz_6() { return &___rWAtWCEOCUwqgRWqpfXjLBoPbhz_6; }
	inline void set_rWAtWCEOCUwqgRWqpfXjLBoPbhz_6(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___rWAtWCEOCUwqgRWqpfXjLBoPbhz_6 = value;
	}

	inline static int32_t get_offset_of_RPCPVQgmBrYXUBVzyqlkkGxmhDj_7() { return static_cast<int32_t>(offsetof(HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3, ___RPCPVQgmBrYXUBVzyqlkkGxmhDj_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_RPCPVQgmBrYXUBVzyqlkkGxmhDj_7() const { return ___RPCPVQgmBrYXUBVzyqlkkGxmhDj_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_RPCPVQgmBrYXUBVzyqlkkGxmhDj_7() { return &___RPCPVQgmBrYXUBVzyqlkkGxmhDj_7; }
	inline void set_RPCPVQgmBrYXUBVzyqlkkGxmhDj_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___RPCPVQgmBrYXUBVzyqlkkGxmhDj_7 = value;
	}

	inline static int32_t get_offset_of_TyVBFJKcdlQLriQmOXSFSSjNHmAy_8() { return static_cast<int32_t>(offsetof(HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3, ___TyVBFJKcdlQLriQmOXSFSSjNHmAy_8)); }
	inline RuntimeObject* get_TyVBFJKcdlQLriQmOXSFSSjNHmAy_8() const { return ___TyVBFJKcdlQLriQmOXSFSSjNHmAy_8; }
	inline RuntimeObject** get_address_of_TyVBFJKcdlQLriQmOXSFSSjNHmAy_8() { return &___TyVBFJKcdlQLriQmOXSFSSjNHmAy_8; }
	inline void set_TyVBFJKcdlQLriQmOXSFSSjNHmAy_8(RuntimeObject* value)
	{
		___TyVBFJKcdlQLriQmOXSFSSjNHmAy_8 = value;
		Il2CppCodeGenWriteBarrier((&___TyVBFJKcdlQLriQmOXSFSSjNHmAy_8), value);
	}

	inline static int32_t get_offset_of_wdcjvtQlCdWpBabpzfClHUOIozil_9() { return static_cast<int32_t>(offsetof(HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3, ___wdcjvtQlCdWpBabpzfClHUOIozil_9)); }
	inline RuntimeObject* get_wdcjvtQlCdWpBabpzfClHUOIozil_9() const { return ___wdcjvtQlCdWpBabpzfClHUOIozil_9; }
	inline RuntimeObject** get_address_of_wdcjvtQlCdWpBabpzfClHUOIozil_9() { return &___wdcjvtQlCdWpBabpzfClHUOIozil_9; }
	inline void set_wdcjvtQlCdWpBabpzfClHUOIozil_9(RuntimeObject* value)
	{
		___wdcjvtQlCdWpBabpzfClHUOIozil_9 = value;
		Il2CppCodeGenWriteBarrier((&___wdcjvtQlCdWpBabpzfClHUOIozil_9), value);
	}

	inline static int32_t get_offset_of_PalmDKfwodOEEgPtuWClpZscTYK_10() { return static_cast<int32_t>(offsetof(HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3, ___PalmDKfwodOEEgPtuWClpZscTYK_10)); }
	inline RuntimeObject* get_PalmDKfwodOEEgPtuWClpZscTYK_10() const { return ___PalmDKfwodOEEgPtuWClpZscTYK_10; }
	inline RuntimeObject** get_address_of_PalmDKfwodOEEgPtuWClpZscTYK_10() { return &___PalmDKfwodOEEgPtuWClpZscTYK_10; }
	inline void set_PalmDKfwodOEEgPtuWClpZscTYK_10(RuntimeObject* value)
	{
		___PalmDKfwodOEEgPtuWClpZscTYK_10 = value;
		Il2CppCodeGenWriteBarrier((&___PalmDKfwodOEEgPtuWClpZscTYK_10), value);
	}

	inline static int32_t get_offset_of_ecNYhizHrPzvbIDpVcnLdKhcNIg_11() { return static_cast<int32_t>(offsetof(HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3, ___ecNYhizHrPzvbIDpVcnLdKhcNIg_11)); }
	inline RuntimeObject* get_ecNYhizHrPzvbIDpVcnLdKhcNIg_11() const { return ___ecNYhizHrPzvbIDpVcnLdKhcNIg_11; }
	inline RuntimeObject** get_address_of_ecNYhizHrPzvbIDpVcnLdKhcNIg_11() { return &___ecNYhizHrPzvbIDpVcnLdKhcNIg_11; }
	inline void set_ecNYhizHrPzvbIDpVcnLdKhcNIg_11(RuntimeObject* value)
	{
		___ecNYhizHrPzvbIDpVcnLdKhcNIg_11 = value;
		Il2CppCodeGenWriteBarrier((&___ecNYhizHrPzvbIDpVcnLdKhcNIg_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HLVBTVUBAXQVMHUFBHWYBWBTRBN_T482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3_H
#ifndef JBBMHBIFJDJEIKKOUPKVWERHDVE_TAA5A94F75C543CC3A12A465AC3D497423C31C606_H
#define JBBMHBIFJDJEIKKOUPKVWERHDVE_TAA5A94F75C543CC3A12A465AC3D497423C31C606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_JbBmHbiFjdJeiKKOUPKVwERHdVe
struct  JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_JbBmHbiFjdJeiKKOUPKVwERHdVe::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_JbBmHbiFjdJeiKKOUPKVwERHdVe::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_JbBmHbiFjdJeiKKOUPKVwERHdVe::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Collections.Generic.IList`1<Rewired.Joystick> Rewired.ReInput_ControllerHelper_PollingHelper_JbBmHbiFjdJeiKKOUPKVwERHdVe::CNPjPWWHaRddgvyGHodSiqLJICXJ
	RuntimeObject* ___CNPjPWWHaRddgvyGHodSiqLJICXJ_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_JbBmHbiFjdJeiKKOUPKVwERHdVe::tGmxXaYmsCfLvIFUgwbFeArmFED
	int32_t ___tGmxXaYmsCfLvIFUgwbFeArmFED_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_JbBmHbiFjdJeiKKOUPKVwERHdVe::GGoLtFAWxRMAatYGucbJjAFCCUd
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___GGoLtFAWxRMAatYGucbJjAFCCUd_5;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_JbBmHbiFjdJeiKKOUPKVwERHdVe::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_JbBmHbiFjdJeiKKOUPKVwERHdVe::gxABcZAXsZhqMYDOKDZmBrUawSNB
	RuntimeObject* ___gxABcZAXsZhqMYDOKDZmBrUawSNB_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_CNPjPWWHaRddgvyGHodSiqLJICXJ_3() { return static_cast<int32_t>(offsetof(JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606, ___CNPjPWWHaRddgvyGHodSiqLJICXJ_3)); }
	inline RuntimeObject* get_CNPjPWWHaRddgvyGHodSiqLJICXJ_3() const { return ___CNPjPWWHaRddgvyGHodSiqLJICXJ_3; }
	inline RuntimeObject** get_address_of_CNPjPWWHaRddgvyGHodSiqLJICXJ_3() { return &___CNPjPWWHaRddgvyGHodSiqLJICXJ_3; }
	inline void set_CNPjPWWHaRddgvyGHodSiqLJICXJ_3(RuntimeObject* value)
	{
		___CNPjPWWHaRddgvyGHodSiqLJICXJ_3 = value;
		Il2CppCodeGenWriteBarrier((&___CNPjPWWHaRddgvyGHodSiqLJICXJ_3), value);
	}

	inline static int32_t get_offset_of_tGmxXaYmsCfLvIFUgwbFeArmFED_4() { return static_cast<int32_t>(offsetof(JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606, ___tGmxXaYmsCfLvIFUgwbFeArmFED_4)); }
	inline int32_t get_tGmxXaYmsCfLvIFUgwbFeArmFED_4() const { return ___tGmxXaYmsCfLvIFUgwbFeArmFED_4; }
	inline int32_t* get_address_of_tGmxXaYmsCfLvIFUgwbFeArmFED_4() { return &___tGmxXaYmsCfLvIFUgwbFeArmFED_4; }
	inline void set_tGmxXaYmsCfLvIFUgwbFeArmFED_4(int32_t value)
	{
		___tGmxXaYmsCfLvIFUgwbFeArmFED_4 = value;
	}

	inline static int32_t get_offset_of_GGoLtFAWxRMAatYGucbJjAFCCUd_5() { return static_cast<int32_t>(offsetof(JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606, ___GGoLtFAWxRMAatYGucbJjAFCCUd_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_GGoLtFAWxRMAatYGucbJjAFCCUd_5() const { return ___GGoLtFAWxRMAatYGucbJjAFCCUd_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_GGoLtFAWxRMAatYGucbJjAFCCUd_5() { return &___GGoLtFAWxRMAatYGucbJjAFCCUd_5; }
	inline void set_GGoLtFAWxRMAatYGucbJjAFCCUd_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___GGoLtFAWxRMAatYGucbJjAFCCUd_5 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return static_cast<int32_t>(offsetof(JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_6 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_6), value);
	}

	inline static int32_t get_offset_of_gxABcZAXsZhqMYDOKDZmBrUawSNB_7() { return static_cast<int32_t>(offsetof(JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606, ___gxABcZAXsZhqMYDOKDZmBrUawSNB_7)); }
	inline RuntimeObject* get_gxABcZAXsZhqMYDOKDZmBrUawSNB_7() const { return ___gxABcZAXsZhqMYDOKDZmBrUawSNB_7; }
	inline RuntimeObject** get_address_of_gxABcZAXsZhqMYDOKDZmBrUawSNB_7() { return &___gxABcZAXsZhqMYDOKDZmBrUawSNB_7; }
	inline void set_gxABcZAXsZhqMYDOKDZmBrUawSNB_7(RuntimeObject* value)
	{
		___gxABcZAXsZhqMYDOKDZmBrUawSNB_7 = value;
		Il2CppCodeGenWriteBarrier((&___gxABcZAXsZhqMYDOKDZmBrUawSNB_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JBBMHBIFJDJEIKKOUPKVWERHDVE_TAA5A94F75C543CC3A12A465AC3D497423C31C606_H
#ifndef KXVVIPWEHDHXCVVOATZBAGGSAIQ_TDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A_H
#define KXVVIPWEHDHXCVVOATZBAGGSAIQ_TDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq
struct  KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq::lTyvbKnOHHjDDnAgOdgVBsNagGg
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___lTyvbKnOHHjDDnAgOdgVBsNagGg_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq::QsOtdmkbbUcFdBYiSXMdBnRcIse
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___QsOtdmkbbUcFdBYiSXMdBnRcIse_5;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq::agmUdgaQsfJNelkgWXLvTLQjDXJ
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___agmUdgaQsfJNelkgWXLvTLQjDXJ_6;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq::vMPBRfEyrhVVRoXYEggmOmsNgwE
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___vMPBRfEyrhVVRoXYEggmOmsNgwE_7;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq::wIbOHZvKyXtRqMWrkEzIZjzLZhH
	RuntimeObject* ___wIbOHZvKyXtRqMWrkEzIZjzLZhH_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq::JrhvhCqJGVNbbEyRvWwwCXHomvh
	RuntimeObject* ___JrhvhCqJGVNbbEyRvWwwCXHomvh_9;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq::jMljDMzfOoDoFamndTeIjmmBoiX
	RuntimeObject* ___jMljDMzfOoDoFamndTeIjmmBoiX_10;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_KXvVipweHDHxcvvOatzbaGgsaiq::gHOYJROlCWbVZCUTzkskuGcWOOwd
	RuntimeObject* ___gHOYJROlCWbVZCUTzkskuGcWOOwd_11;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_lTyvbKnOHHjDDnAgOdgVBsNagGg_4() { return static_cast<int32_t>(offsetof(KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A, ___lTyvbKnOHHjDDnAgOdgVBsNagGg_4)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_lTyvbKnOHHjDDnAgOdgVBsNagGg_4() const { return ___lTyvbKnOHHjDDnAgOdgVBsNagGg_4; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_lTyvbKnOHHjDDnAgOdgVBsNagGg_4() { return &___lTyvbKnOHHjDDnAgOdgVBsNagGg_4; }
	inline void set_lTyvbKnOHHjDDnAgOdgVBsNagGg_4(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___lTyvbKnOHHjDDnAgOdgVBsNagGg_4 = value;
	}

	inline static int32_t get_offset_of_QsOtdmkbbUcFdBYiSXMdBnRcIse_5() { return static_cast<int32_t>(offsetof(KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A, ___QsOtdmkbbUcFdBYiSXMdBnRcIse_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_QsOtdmkbbUcFdBYiSXMdBnRcIse_5() const { return ___QsOtdmkbbUcFdBYiSXMdBnRcIse_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_QsOtdmkbbUcFdBYiSXMdBnRcIse_5() { return &___QsOtdmkbbUcFdBYiSXMdBnRcIse_5; }
	inline void set_QsOtdmkbbUcFdBYiSXMdBnRcIse_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___QsOtdmkbbUcFdBYiSXMdBnRcIse_5 = value;
	}

	inline static int32_t get_offset_of_agmUdgaQsfJNelkgWXLvTLQjDXJ_6() { return static_cast<int32_t>(offsetof(KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A, ___agmUdgaQsfJNelkgWXLvTLQjDXJ_6)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_agmUdgaQsfJNelkgWXLvTLQjDXJ_6() const { return ___agmUdgaQsfJNelkgWXLvTLQjDXJ_6; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_agmUdgaQsfJNelkgWXLvTLQjDXJ_6() { return &___agmUdgaQsfJNelkgWXLvTLQjDXJ_6; }
	inline void set_agmUdgaQsfJNelkgWXLvTLQjDXJ_6(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___agmUdgaQsfJNelkgWXLvTLQjDXJ_6 = value;
	}

	inline static int32_t get_offset_of_vMPBRfEyrhVVRoXYEggmOmsNgwE_7() { return static_cast<int32_t>(offsetof(KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A, ___vMPBRfEyrhVVRoXYEggmOmsNgwE_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_vMPBRfEyrhVVRoXYEggmOmsNgwE_7() const { return ___vMPBRfEyrhVVRoXYEggmOmsNgwE_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_vMPBRfEyrhVVRoXYEggmOmsNgwE_7() { return &___vMPBRfEyrhVVRoXYEggmOmsNgwE_7; }
	inline void set_vMPBRfEyrhVVRoXYEggmOmsNgwE_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___vMPBRfEyrhVVRoXYEggmOmsNgwE_7 = value;
	}

	inline static int32_t get_offset_of_wIbOHZvKyXtRqMWrkEzIZjzLZhH_8() { return static_cast<int32_t>(offsetof(KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A, ___wIbOHZvKyXtRqMWrkEzIZjzLZhH_8)); }
	inline RuntimeObject* get_wIbOHZvKyXtRqMWrkEzIZjzLZhH_8() const { return ___wIbOHZvKyXtRqMWrkEzIZjzLZhH_8; }
	inline RuntimeObject** get_address_of_wIbOHZvKyXtRqMWrkEzIZjzLZhH_8() { return &___wIbOHZvKyXtRqMWrkEzIZjzLZhH_8; }
	inline void set_wIbOHZvKyXtRqMWrkEzIZjzLZhH_8(RuntimeObject* value)
	{
		___wIbOHZvKyXtRqMWrkEzIZjzLZhH_8 = value;
		Il2CppCodeGenWriteBarrier((&___wIbOHZvKyXtRqMWrkEzIZjzLZhH_8), value);
	}

	inline static int32_t get_offset_of_JrhvhCqJGVNbbEyRvWwwCXHomvh_9() { return static_cast<int32_t>(offsetof(KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A, ___JrhvhCqJGVNbbEyRvWwwCXHomvh_9)); }
	inline RuntimeObject* get_JrhvhCqJGVNbbEyRvWwwCXHomvh_9() const { return ___JrhvhCqJGVNbbEyRvWwwCXHomvh_9; }
	inline RuntimeObject** get_address_of_JrhvhCqJGVNbbEyRvWwwCXHomvh_9() { return &___JrhvhCqJGVNbbEyRvWwwCXHomvh_9; }
	inline void set_JrhvhCqJGVNbbEyRvWwwCXHomvh_9(RuntimeObject* value)
	{
		___JrhvhCqJGVNbbEyRvWwwCXHomvh_9 = value;
		Il2CppCodeGenWriteBarrier((&___JrhvhCqJGVNbbEyRvWwwCXHomvh_9), value);
	}

	inline static int32_t get_offset_of_jMljDMzfOoDoFamndTeIjmmBoiX_10() { return static_cast<int32_t>(offsetof(KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A, ___jMljDMzfOoDoFamndTeIjmmBoiX_10)); }
	inline RuntimeObject* get_jMljDMzfOoDoFamndTeIjmmBoiX_10() const { return ___jMljDMzfOoDoFamndTeIjmmBoiX_10; }
	inline RuntimeObject** get_address_of_jMljDMzfOoDoFamndTeIjmmBoiX_10() { return &___jMljDMzfOoDoFamndTeIjmmBoiX_10; }
	inline void set_jMljDMzfOoDoFamndTeIjmmBoiX_10(RuntimeObject* value)
	{
		___jMljDMzfOoDoFamndTeIjmmBoiX_10 = value;
		Il2CppCodeGenWriteBarrier((&___jMljDMzfOoDoFamndTeIjmmBoiX_10), value);
	}

	inline static int32_t get_offset_of_gHOYJROlCWbVZCUTzkskuGcWOOwd_11() { return static_cast<int32_t>(offsetof(KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A, ___gHOYJROlCWbVZCUTzkskuGcWOOwd_11)); }
	inline RuntimeObject* get_gHOYJROlCWbVZCUTzkskuGcWOOwd_11() const { return ___gHOYJROlCWbVZCUTzkskuGcWOOwd_11; }
	inline RuntimeObject** get_address_of_gHOYJROlCWbVZCUTzkskuGcWOOwd_11() { return &___gHOYJROlCWbVZCUTzkskuGcWOOwd_11; }
	inline void set_gHOYJROlCWbVZCUTzkskuGcWOOwd_11(RuntimeObject* value)
	{
		___gHOYJROlCWbVZCUTzkskuGcWOOwd_11 = value;
		Il2CppCodeGenWriteBarrier((&___gHOYJROlCWbVZCUTzkskuGcWOOwd_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KXVVIPWEHDHXCVVOATZBAGGSAIQ_TDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A_H
#ifndef MHEERQBTZTSURFAEDTQFCTTZWYPT_T47965B9CACBBEF4CDC46C4672367789080F2E4F1_H
#define MHEERQBTZTSURFAEDTQFCTTZWYPT_T47965B9CACBBEF4CDC46C4672367789080F2E4F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_MheerqBtzTSurfAEdtQfcTTzwYpt
struct  MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_MheerqBtzTSurfAEdtQfcTTzwYpt::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_MheerqBtzTSurfAEdtQfcTTzwYpt::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_MheerqBtzTSurfAEdtQfcTTzwYpt::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Collections.Generic.IList`1<Rewired.Joystick> Rewired.ReInput_ControllerHelper_PollingHelper_MheerqBtzTSurfAEdtQfcTTzwYpt::FCZIrNLlLYcBGYMRgfrfhMVkGLkj
	RuntimeObject* ___FCZIrNLlLYcBGYMRgfrfhMVkGLkj_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_MheerqBtzTSurfAEdtQfcTTzwYpt::AWruhtCZVwDWOojDzqRDFmWhfxmd
	int32_t ___AWruhtCZVwDWOojDzqRDFmWhfxmd_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_MheerqBtzTSurfAEdtQfcTTzwYpt::EnOXeeMmrBRllqVcdObtSABvTYP
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___EnOXeeMmrBRllqVcdObtSABvTYP_5;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_MheerqBtzTSurfAEdtQfcTTzwYpt::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_MheerqBtzTSurfAEdtQfcTTzwYpt::nHVPsdWojqlHSeauxUEHrQHhvOD
	RuntimeObject* ___nHVPsdWojqlHSeauxUEHrQHhvOD_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_FCZIrNLlLYcBGYMRgfrfhMVkGLkj_3() { return static_cast<int32_t>(offsetof(MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1, ___FCZIrNLlLYcBGYMRgfrfhMVkGLkj_3)); }
	inline RuntimeObject* get_FCZIrNLlLYcBGYMRgfrfhMVkGLkj_3() const { return ___FCZIrNLlLYcBGYMRgfrfhMVkGLkj_3; }
	inline RuntimeObject** get_address_of_FCZIrNLlLYcBGYMRgfrfhMVkGLkj_3() { return &___FCZIrNLlLYcBGYMRgfrfhMVkGLkj_3; }
	inline void set_FCZIrNLlLYcBGYMRgfrfhMVkGLkj_3(RuntimeObject* value)
	{
		___FCZIrNLlLYcBGYMRgfrfhMVkGLkj_3 = value;
		Il2CppCodeGenWriteBarrier((&___FCZIrNLlLYcBGYMRgfrfhMVkGLkj_3), value);
	}

	inline static int32_t get_offset_of_AWruhtCZVwDWOojDzqRDFmWhfxmd_4() { return static_cast<int32_t>(offsetof(MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1, ___AWruhtCZVwDWOojDzqRDFmWhfxmd_4)); }
	inline int32_t get_AWruhtCZVwDWOojDzqRDFmWhfxmd_4() const { return ___AWruhtCZVwDWOojDzqRDFmWhfxmd_4; }
	inline int32_t* get_address_of_AWruhtCZVwDWOojDzqRDFmWhfxmd_4() { return &___AWruhtCZVwDWOojDzqRDFmWhfxmd_4; }
	inline void set_AWruhtCZVwDWOojDzqRDFmWhfxmd_4(int32_t value)
	{
		___AWruhtCZVwDWOojDzqRDFmWhfxmd_4 = value;
	}

	inline static int32_t get_offset_of_EnOXeeMmrBRllqVcdObtSABvTYP_5() { return static_cast<int32_t>(offsetof(MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1, ___EnOXeeMmrBRllqVcdObtSABvTYP_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_EnOXeeMmrBRllqVcdObtSABvTYP_5() const { return ___EnOXeeMmrBRllqVcdObtSABvTYP_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_EnOXeeMmrBRllqVcdObtSABvTYP_5() { return &___EnOXeeMmrBRllqVcdObtSABvTYP_5; }
	inline void set_EnOXeeMmrBRllqVcdObtSABvTYP_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___EnOXeeMmrBRllqVcdObtSABvTYP_5 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return static_cast<int32_t>(offsetof(MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_6 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_6), value);
	}

	inline static int32_t get_offset_of_nHVPsdWojqlHSeauxUEHrQHhvOD_7() { return static_cast<int32_t>(offsetof(MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1, ___nHVPsdWojqlHSeauxUEHrQHhvOD_7)); }
	inline RuntimeObject* get_nHVPsdWojqlHSeauxUEHrQHhvOD_7() const { return ___nHVPsdWojqlHSeauxUEHrQHhvOD_7; }
	inline RuntimeObject** get_address_of_nHVPsdWojqlHSeauxUEHrQHhvOD_7() { return &___nHVPsdWojqlHSeauxUEHrQHhvOD_7; }
	inline void set_nHVPsdWojqlHSeauxUEHrQHhvOD_7(RuntimeObject* value)
	{
		___nHVPsdWojqlHSeauxUEHrQHhvOD_7 = value;
		Il2CppCodeGenWriteBarrier((&___nHVPsdWojqlHSeauxUEHrQHhvOD_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MHEERQBTZTSURFAEDTQFCTTZWYPT_T47965B9CACBBEF4CDC46C4672367789080F2E4F1_H
#ifndef OZSHKJQDEACRNCCHPGRRNIBNCWYB_T6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3_H
#define OZSHKJQDEACRNCCHPGRRNIBNCWYB_T6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_OZshkjqdEACrnCchpgRRniBNcwyb
struct  OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_OZshkjqdEACrnCchpgRRniBNcwyb::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_OZshkjqdEACrnCchpgRRniBNcwyb::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_OZshkjqdEACrnCchpgRRniBNcwyb::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// System.Collections.Generic.IList`1<Rewired.Joystick> Rewired.ReInput_ControllerHelper_PollingHelper_OZshkjqdEACrnCchpgRRniBNcwyb::FlSdtuhphcTSwGLbunqQbPbehtJe
	RuntimeObject* ___FlSdtuhphcTSwGLbunqQbPbehtJe_3;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_OZshkjqdEACrnCchpgRRniBNcwyb::UYTcgCEVQPWpEYUiRTsrXmgiZC
	int32_t ___UYTcgCEVQPWpEYUiRTsrXmgiZC_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_OZshkjqdEACrnCchpgRRniBNcwyb::jhKSkABuLssiABSYCANtFEpLhHLE
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___jhKSkABuLssiABSYCANtFEpLhHLE_5;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_OZshkjqdEACrnCchpgRRniBNcwyb::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_OZshkjqdEACrnCchpgRRniBNcwyb::aEgFTDWXaaQuVwylBpXzyyBGZtO
	RuntimeObject* ___aEgFTDWXaaQuVwylBpXzyyBGZtO_7;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_FlSdtuhphcTSwGLbunqQbPbehtJe_3() { return static_cast<int32_t>(offsetof(OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3, ___FlSdtuhphcTSwGLbunqQbPbehtJe_3)); }
	inline RuntimeObject* get_FlSdtuhphcTSwGLbunqQbPbehtJe_3() const { return ___FlSdtuhphcTSwGLbunqQbPbehtJe_3; }
	inline RuntimeObject** get_address_of_FlSdtuhphcTSwGLbunqQbPbehtJe_3() { return &___FlSdtuhphcTSwGLbunqQbPbehtJe_3; }
	inline void set_FlSdtuhphcTSwGLbunqQbPbehtJe_3(RuntimeObject* value)
	{
		___FlSdtuhphcTSwGLbunqQbPbehtJe_3 = value;
		Il2CppCodeGenWriteBarrier((&___FlSdtuhphcTSwGLbunqQbPbehtJe_3), value);
	}

	inline static int32_t get_offset_of_UYTcgCEVQPWpEYUiRTsrXmgiZC_4() { return static_cast<int32_t>(offsetof(OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3, ___UYTcgCEVQPWpEYUiRTsrXmgiZC_4)); }
	inline int32_t get_UYTcgCEVQPWpEYUiRTsrXmgiZC_4() const { return ___UYTcgCEVQPWpEYUiRTsrXmgiZC_4; }
	inline int32_t* get_address_of_UYTcgCEVQPWpEYUiRTsrXmgiZC_4() { return &___UYTcgCEVQPWpEYUiRTsrXmgiZC_4; }
	inline void set_UYTcgCEVQPWpEYUiRTsrXmgiZC_4(int32_t value)
	{
		___UYTcgCEVQPWpEYUiRTsrXmgiZC_4 = value;
	}

	inline static int32_t get_offset_of_jhKSkABuLssiABSYCANtFEpLhHLE_5() { return static_cast<int32_t>(offsetof(OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3, ___jhKSkABuLssiABSYCANtFEpLhHLE_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_jhKSkABuLssiABSYCANtFEpLhHLE_5() const { return ___jhKSkABuLssiABSYCANtFEpLhHLE_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_jhKSkABuLssiABSYCANtFEpLhHLE_5() { return &___jhKSkABuLssiABSYCANtFEpLhHLE_5; }
	inline void set_jhKSkABuLssiABSYCANtFEpLhHLE_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___jhKSkABuLssiABSYCANtFEpLhHLE_5 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return static_cast<int32_t>(offsetof(OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_6; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_6 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_6), value);
	}

	inline static int32_t get_offset_of_aEgFTDWXaaQuVwylBpXzyyBGZtO_7() { return static_cast<int32_t>(offsetof(OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3, ___aEgFTDWXaaQuVwylBpXzyyBGZtO_7)); }
	inline RuntimeObject* get_aEgFTDWXaaQuVwylBpXzyyBGZtO_7() const { return ___aEgFTDWXaaQuVwylBpXzyyBGZtO_7; }
	inline RuntimeObject** get_address_of_aEgFTDWXaaQuVwylBpXzyyBGZtO_7() { return &___aEgFTDWXaaQuVwylBpXzyyBGZtO_7; }
	inline void set_aEgFTDWXaaQuVwylBpXzyyBGZtO_7(RuntimeObject* value)
	{
		___aEgFTDWXaaQuVwylBpXzyyBGZtO_7 = value;
		Il2CppCodeGenWriteBarrier((&___aEgFTDWXaaQuVwylBpXzyyBGZtO_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OZSHKJQDEACRNCCHPGRRNIBNCWYB_T6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3_H
#ifndef FZCASPIYNCNGNFNUGILVGPZACYF_T91993F10241249BDBC6DC290E2918A437A9374D7_H
#define FZCASPIYNCNGNFNUGILVGPZACYF_T91993F10241249BDBC6DC290E2918A437A9374D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf
struct  fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf::NmJKXjCDbsOGTRbOEEFMBEOEFUyb
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NmJKXjCDbsOGTRbOEEFMBEOEFUyb_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf::wNPLhUIdVKUmVenimNoFrRATGNpb
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___wNPLhUIdVKUmVenimNoFrRATGNpb_5;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf::zFIsOzpXzzpINimYjVuDQKEOGTTc
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___zFIsOzpXzzpINimYjVuDQKEOGTTc_6;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf::JevOncyqxoOkBpTLicUBwTcTtsT
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___JevOncyqxoOkBpTLicUBwTcTtsT_7;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf::QZQLDIvnrZfxWplZEbMmdPKplWd
	RuntimeObject* ___QZQLDIvnrZfxWplZEbMmdPKplWd_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf::PBuIBnaXXSvgQiBvViWXihECNCtd
	RuntimeObject* ___PBuIBnaXXSvgQiBvViWXihECNCtd_9;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf::vyhJoBzzJIRDJxrbRNNNcNlrGKHh
	RuntimeObject* ___vyhJoBzzJIRDJxrbRNNNcNlrGKHh_10;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_fZCaSpiYNCNgnFNUGiLvGpZACyf::OfxnXpujvLHQkTqseaVDlwbHgRPk
	RuntimeObject* ___OfxnXpujvLHQkTqseaVDlwbHgRPk_11;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_NmJKXjCDbsOGTRbOEEFMBEOEFUyb_4() { return static_cast<int32_t>(offsetof(fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7, ___NmJKXjCDbsOGTRbOEEFMBEOEFUyb_4)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NmJKXjCDbsOGTRbOEEFMBEOEFUyb_4() const { return ___NmJKXjCDbsOGTRbOEEFMBEOEFUyb_4; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NmJKXjCDbsOGTRbOEEFMBEOEFUyb_4() { return &___NmJKXjCDbsOGTRbOEEFMBEOEFUyb_4; }
	inline void set_NmJKXjCDbsOGTRbOEEFMBEOEFUyb_4(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NmJKXjCDbsOGTRbOEEFMBEOEFUyb_4 = value;
	}

	inline static int32_t get_offset_of_wNPLhUIdVKUmVenimNoFrRATGNpb_5() { return static_cast<int32_t>(offsetof(fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7, ___wNPLhUIdVKUmVenimNoFrRATGNpb_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_wNPLhUIdVKUmVenimNoFrRATGNpb_5() const { return ___wNPLhUIdVKUmVenimNoFrRATGNpb_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_wNPLhUIdVKUmVenimNoFrRATGNpb_5() { return &___wNPLhUIdVKUmVenimNoFrRATGNpb_5; }
	inline void set_wNPLhUIdVKUmVenimNoFrRATGNpb_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___wNPLhUIdVKUmVenimNoFrRATGNpb_5 = value;
	}

	inline static int32_t get_offset_of_zFIsOzpXzzpINimYjVuDQKEOGTTc_6() { return static_cast<int32_t>(offsetof(fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7, ___zFIsOzpXzzpINimYjVuDQKEOGTTc_6)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_zFIsOzpXzzpINimYjVuDQKEOGTTc_6() const { return ___zFIsOzpXzzpINimYjVuDQKEOGTTc_6; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_zFIsOzpXzzpINimYjVuDQKEOGTTc_6() { return &___zFIsOzpXzzpINimYjVuDQKEOGTTc_6; }
	inline void set_zFIsOzpXzzpINimYjVuDQKEOGTTc_6(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___zFIsOzpXzzpINimYjVuDQKEOGTTc_6 = value;
	}

	inline static int32_t get_offset_of_JevOncyqxoOkBpTLicUBwTcTtsT_7() { return static_cast<int32_t>(offsetof(fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7, ___JevOncyqxoOkBpTLicUBwTcTtsT_7)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_JevOncyqxoOkBpTLicUBwTcTtsT_7() const { return ___JevOncyqxoOkBpTLicUBwTcTtsT_7; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_JevOncyqxoOkBpTLicUBwTcTtsT_7() { return &___JevOncyqxoOkBpTLicUBwTcTtsT_7; }
	inline void set_JevOncyqxoOkBpTLicUBwTcTtsT_7(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___JevOncyqxoOkBpTLicUBwTcTtsT_7 = value;
	}

	inline static int32_t get_offset_of_QZQLDIvnrZfxWplZEbMmdPKplWd_8() { return static_cast<int32_t>(offsetof(fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7, ___QZQLDIvnrZfxWplZEbMmdPKplWd_8)); }
	inline RuntimeObject* get_QZQLDIvnrZfxWplZEbMmdPKplWd_8() const { return ___QZQLDIvnrZfxWplZEbMmdPKplWd_8; }
	inline RuntimeObject** get_address_of_QZQLDIvnrZfxWplZEbMmdPKplWd_8() { return &___QZQLDIvnrZfxWplZEbMmdPKplWd_8; }
	inline void set_QZQLDIvnrZfxWplZEbMmdPKplWd_8(RuntimeObject* value)
	{
		___QZQLDIvnrZfxWplZEbMmdPKplWd_8 = value;
		Il2CppCodeGenWriteBarrier((&___QZQLDIvnrZfxWplZEbMmdPKplWd_8), value);
	}

	inline static int32_t get_offset_of_PBuIBnaXXSvgQiBvViWXihECNCtd_9() { return static_cast<int32_t>(offsetof(fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7, ___PBuIBnaXXSvgQiBvViWXihECNCtd_9)); }
	inline RuntimeObject* get_PBuIBnaXXSvgQiBvViWXihECNCtd_9() const { return ___PBuIBnaXXSvgQiBvViWXihECNCtd_9; }
	inline RuntimeObject** get_address_of_PBuIBnaXXSvgQiBvViWXihECNCtd_9() { return &___PBuIBnaXXSvgQiBvViWXihECNCtd_9; }
	inline void set_PBuIBnaXXSvgQiBvViWXihECNCtd_9(RuntimeObject* value)
	{
		___PBuIBnaXXSvgQiBvViWXihECNCtd_9 = value;
		Il2CppCodeGenWriteBarrier((&___PBuIBnaXXSvgQiBvViWXihECNCtd_9), value);
	}

	inline static int32_t get_offset_of_vyhJoBzzJIRDJxrbRNNNcNlrGKHh_10() { return static_cast<int32_t>(offsetof(fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7, ___vyhJoBzzJIRDJxrbRNNNcNlrGKHh_10)); }
	inline RuntimeObject* get_vyhJoBzzJIRDJxrbRNNNcNlrGKHh_10() const { return ___vyhJoBzzJIRDJxrbRNNNcNlrGKHh_10; }
	inline RuntimeObject** get_address_of_vyhJoBzzJIRDJxrbRNNNcNlrGKHh_10() { return &___vyhJoBzzJIRDJxrbRNNNcNlrGKHh_10; }
	inline void set_vyhJoBzzJIRDJxrbRNNNcNlrGKHh_10(RuntimeObject* value)
	{
		___vyhJoBzzJIRDJxrbRNNNcNlrGKHh_10 = value;
		Il2CppCodeGenWriteBarrier((&___vyhJoBzzJIRDJxrbRNNNcNlrGKHh_10), value);
	}

	inline static int32_t get_offset_of_OfxnXpujvLHQkTqseaVDlwbHgRPk_11() { return static_cast<int32_t>(offsetof(fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7, ___OfxnXpujvLHQkTqseaVDlwbHgRPk_11)); }
	inline RuntimeObject* get_OfxnXpujvLHQkTqseaVDlwbHgRPk_11() const { return ___OfxnXpujvLHQkTqseaVDlwbHgRPk_11; }
	inline RuntimeObject** get_address_of_OfxnXpujvLHQkTqseaVDlwbHgRPk_11() { return &___OfxnXpujvLHQkTqseaVDlwbHgRPk_11; }
	inline void set_OfxnXpujvLHQkTqseaVDlwbHgRPk_11(RuntimeObject* value)
	{
		___OfxnXpujvLHQkTqseaVDlwbHgRPk_11 = value;
		Il2CppCodeGenWriteBarrier((&___OfxnXpujvLHQkTqseaVDlwbHgRPk_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FZCASPIYNCNGNFNUGILVGPZACYF_T91993F10241249BDBC6DC290E2918A437A9374D7_H
#ifndef IJQERXGRHNIATDDDCTZRWSMJMR_T94736FCF93B9FD4EF65771F60CF00EBD1CF9B409_H
#define IJQERXGRHNIATDDDCTZRWSMJMR_T94736FCF93B9FD4EF65771F60CF00EBD1CF9B409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.ReInput_ControllerHelper_PollingHelper_ijqERXGRHniaTDdDCtzRWsmjMR
struct  ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409  : public RuntimeObject
{
public:
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_ijqERXGRHniaTDdDCtzRWsmjMR::NOArrsNdfKDShNFftfbPqYDzhpq
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NOArrsNdfKDShNFftfbPqYDzhpq_0;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_ijqERXGRHniaTDdDCtzRWsmjMR::HiUOXdDRFcREZCtaKpUyyzdKBGv
	int32_t ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1;
	// System.Int32 Rewired.ReInput_ControllerHelper_PollingHelper_ijqERXGRHniaTDdDCtzRWsmjMR::eWRwTJwLvWfNoJTKpOqPGybGsFGN
	int32_t ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2;
	// Rewired.ReInput_ControllerHelper_PollingHelper Rewired.ReInput_ControllerHelper_PollingHelper_ijqERXGRHniaTDdDCtzRWsmjMR::DKaFTWTMhFeLQHDExnDMRxZfmROL
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_ijqERXGRHniaTDdDCtzRWsmjMR::NpyXwbPIzajNDrTcugaTTmTZExF
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___NpyXwbPIzajNDrTcugaTTmTZExF_4;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_ijqERXGRHniaTDdDCtzRWsmjMR::uUSGzEijikuJhlrFleHVcqNiQgo
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___uUSGzEijikuJhlrFleHVcqNiQgo_5;
	// Rewired.ControllerPollingInfo Rewired.ReInput_ControllerHelper_PollingHelper_ijqERXGRHniaTDdDCtzRWsmjMR::dNbZebdqcSHSsGKBBcUqAqSoFSV
	ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  ___dNbZebdqcSHSsGKBBcUqAqSoFSV_6;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_ijqERXGRHniaTDdDCtzRWsmjMR::mGffXviMGlJeIhFyFdNxAzlXIpkh
	RuntimeObject* ___mGffXviMGlJeIhFyFdNxAzlXIpkh_7;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_ijqERXGRHniaTDdDCtzRWsmjMR::cUwhnomlYYSTFTkvAmfeTPMjhYg
	RuntimeObject* ___cUwhnomlYYSTFTkvAmfeTPMjhYg_8;
	// System.Collections.Generic.IEnumerator`1<Rewired.ControllerPollingInfo> Rewired.ReInput_ControllerHelper_PollingHelper_ijqERXGRHniaTDdDCtzRWsmjMR::dsEtkRNdKgFtWdqxPPmPRZFiOhH
	RuntimeObject* ___dsEtkRNdKgFtWdqxPPmPRZFiOhH_9;

public:
	inline static int32_t get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return static_cast<int32_t>(offsetof(ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409, ___NOArrsNdfKDShNFftfbPqYDzhpq_0)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NOArrsNdfKDShNFftfbPqYDzhpq_0() const { return ___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NOArrsNdfKDShNFftfbPqYDzhpq_0() { return &___NOArrsNdfKDShNFftfbPqYDzhpq_0; }
	inline void set_NOArrsNdfKDShNFftfbPqYDzhpq_0(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NOArrsNdfKDShNFftfbPqYDzhpq_0 = value;
	}

	inline static int32_t get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return static_cast<int32_t>(offsetof(ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409, ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1)); }
	inline int32_t get_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() const { return ___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline int32_t* get_address_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1() { return &___HiUOXdDRFcREZCtaKpUyyzdKBGv_1; }
	inline void set_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(int32_t value)
	{
		___HiUOXdDRFcREZCtaKpUyyzdKBGv_1 = value;
	}

	inline static int32_t get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return static_cast<int32_t>(offsetof(ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409, ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2)); }
	inline int32_t get_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() const { return ___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline int32_t* get_address_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2() { return &___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2; }
	inline void set_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(int32_t value)
	{
		___eWRwTJwLvWfNoJTKpOqPGybGsFGN_2 = value;
	}

	inline static int32_t get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return static_cast<int32_t>(offsetof(ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409, ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3)); }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * get_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() const { return ___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A ** get_address_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3() { return &___DKaFTWTMhFeLQHDExnDMRxZfmROL_3; }
	inline void set_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A * value)
	{
		___DKaFTWTMhFeLQHDExnDMRxZfmROL_3 = value;
		Il2CppCodeGenWriteBarrier((&___DKaFTWTMhFeLQHDExnDMRxZfmROL_3), value);
	}

	inline static int32_t get_offset_of_NpyXwbPIzajNDrTcugaTTmTZExF_4() { return static_cast<int32_t>(offsetof(ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409, ___NpyXwbPIzajNDrTcugaTTmTZExF_4)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_NpyXwbPIzajNDrTcugaTTmTZExF_4() const { return ___NpyXwbPIzajNDrTcugaTTmTZExF_4; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_NpyXwbPIzajNDrTcugaTTmTZExF_4() { return &___NpyXwbPIzajNDrTcugaTTmTZExF_4; }
	inline void set_NpyXwbPIzajNDrTcugaTTmTZExF_4(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___NpyXwbPIzajNDrTcugaTTmTZExF_4 = value;
	}

	inline static int32_t get_offset_of_uUSGzEijikuJhlrFleHVcqNiQgo_5() { return static_cast<int32_t>(offsetof(ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409, ___uUSGzEijikuJhlrFleHVcqNiQgo_5)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_uUSGzEijikuJhlrFleHVcqNiQgo_5() const { return ___uUSGzEijikuJhlrFleHVcqNiQgo_5; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_uUSGzEijikuJhlrFleHVcqNiQgo_5() { return &___uUSGzEijikuJhlrFleHVcqNiQgo_5; }
	inline void set_uUSGzEijikuJhlrFleHVcqNiQgo_5(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___uUSGzEijikuJhlrFleHVcqNiQgo_5 = value;
	}

	inline static int32_t get_offset_of_dNbZebdqcSHSsGKBBcUqAqSoFSV_6() { return static_cast<int32_t>(offsetof(ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409, ___dNbZebdqcSHSsGKBBcUqAqSoFSV_6)); }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  get_dNbZebdqcSHSsGKBBcUqAqSoFSV_6() const { return ___dNbZebdqcSHSsGKBBcUqAqSoFSV_6; }
	inline ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03 * get_address_of_dNbZebdqcSHSsGKBBcUqAqSoFSV_6() { return &___dNbZebdqcSHSsGKBBcUqAqSoFSV_6; }
	inline void set_dNbZebdqcSHSsGKBBcUqAqSoFSV_6(ControllerPollingInfo_t7933CFBCF4884BC44CB6B5947D4058434D658B03  value)
	{
		___dNbZebdqcSHSsGKBBcUqAqSoFSV_6 = value;
	}

	inline static int32_t get_offset_of_mGffXviMGlJeIhFyFdNxAzlXIpkh_7() { return static_cast<int32_t>(offsetof(ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409, ___mGffXviMGlJeIhFyFdNxAzlXIpkh_7)); }
	inline RuntimeObject* get_mGffXviMGlJeIhFyFdNxAzlXIpkh_7() const { return ___mGffXviMGlJeIhFyFdNxAzlXIpkh_7; }
	inline RuntimeObject** get_address_of_mGffXviMGlJeIhFyFdNxAzlXIpkh_7() { return &___mGffXviMGlJeIhFyFdNxAzlXIpkh_7; }
	inline void set_mGffXviMGlJeIhFyFdNxAzlXIpkh_7(RuntimeObject* value)
	{
		___mGffXviMGlJeIhFyFdNxAzlXIpkh_7 = value;
		Il2CppCodeGenWriteBarrier((&___mGffXviMGlJeIhFyFdNxAzlXIpkh_7), value);
	}

	inline static int32_t get_offset_of_cUwhnomlYYSTFTkvAmfeTPMjhYg_8() { return static_cast<int32_t>(offsetof(ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409, ___cUwhnomlYYSTFTkvAmfeTPMjhYg_8)); }
	inline RuntimeObject* get_cUwhnomlYYSTFTkvAmfeTPMjhYg_8() const { return ___cUwhnomlYYSTFTkvAmfeTPMjhYg_8; }
	inline RuntimeObject** get_address_of_cUwhnomlYYSTFTkvAmfeTPMjhYg_8() { return &___cUwhnomlYYSTFTkvAmfeTPMjhYg_8; }
	inline void set_cUwhnomlYYSTFTkvAmfeTPMjhYg_8(RuntimeObject* value)
	{
		___cUwhnomlYYSTFTkvAmfeTPMjhYg_8 = value;
		Il2CppCodeGenWriteBarrier((&___cUwhnomlYYSTFTkvAmfeTPMjhYg_8), value);
	}

	inline static int32_t get_offset_of_dsEtkRNdKgFtWdqxPPmPRZFiOhH_9() { return static_cast<int32_t>(offsetof(ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409, ___dsEtkRNdKgFtWdqxPPmPRZFiOhH_9)); }
	inline RuntimeObject* get_dsEtkRNdKgFtWdqxPPmPRZFiOhH_9() const { return ___dsEtkRNdKgFtWdqxPPmPRZFiOhH_9; }
	inline RuntimeObject** get_address_of_dsEtkRNdKgFtWdqxPPmPRZFiOhH_9() { return &___dsEtkRNdKgFtWdqxPPmPRZFiOhH_9; }
	inline void set_dsEtkRNdKgFtWdqxPPmPRZFiOhH_9(RuntimeObject* value)
	{
		___dsEtkRNdKgFtWdqxPPmPRZFiOhH_9 = value;
		Il2CppCodeGenWriteBarrier((&___dsEtkRNdKgFtWdqxPPmPRZFiOhH_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IJQERXGRHNIATDDDCTZRWSMJMR_T94736FCF93B9FD4EF65771F60CF00EBD1CF9B409_H
#ifndef EOGULEPQGUDCSIJUTWLTIWFLJMQB_T3C73104D2169E81D22AC3F277A8567CFE5C72C87_H
#define EOGULEPQGUDCSIJUTWLTIWFLJMQB_T3C73104D2169E81D22AC3F277A8567CFE5C72C87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_EoGUlEPqgudCsiJuTWLtIwfLJMQB
struct  EoGUlEPqgudCsiJuTWLtIwfLJMQB_t3C73104D2169E81D22AC3F277A8567CFE5C72C87  : public bPQXlaQKObGXPfqqqiLPOxJrgoiR_t20D282424AD47831E3D4341BF71E7F4E2A2DB047
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EOGULEPQGUDCSIJUTWLTIWFLJMQB_T3C73104D2169E81D22AC3F277A8567CFE5C72C87_H
#ifndef IDJFMDZOYAZAMILFUMSPNBKFLFX_TE789ED47A7F7E027BBED3C807535FF2210BF73B8_H
#define IDJFMDZOYAZAMILFUMSPNBKFLFX_TE789ED47A7F7E027BBED3C807535FF2210BF73B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_IDjfMDzOyazAMilfUmspNBkfLfx
struct  IDjfMDzOyazAMilfUmspNBkfLfx_tE789ED47A7F7E027BBED3C807535FF2210BF73B8  : public bPQXlaQKObGXPfqqqiLPOxJrgoiR_t20D282424AD47831E3D4341BF71E7F4E2A2DB047
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDJFMDZOYAZAMILFUMSPNBKFLFX_TE789ED47A7F7E027BBED3C807535FF2210BF73B8_H
#ifndef SVPEVTPBSCFCFVLHLHINGUJAZBFE_TB7103520E69B370373D4C059278E7C22B8F5C20A_H
#define SVPEVTPBSCFCFVLHLHINGUJAZBFE_TB7103520E69B370373D4C059278E7C22B8F5C20A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_SVPEvTpbScfCfvLhlhingujazBfe
struct  SVPEvTpbScfCfvLhlhingujazBfe_tB7103520E69B370373D4C059278E7C22B8F5C20A  : public bPQXlaQKObGXPfqqqiLPOxJrgoiR_t20D282424AD47831E3D4341BF71E7F4E2A2DB047
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVPEVTPBSCFCFVLHLHINGUJAZBFE_TB7103520E69B370373D4C059278E7C22B8F5C20A_H
#ifndef PVMJIIVPYZIXMEGBYPKSBKACLKD_T9DB25F016914F718888DC689A2C04BD3976BF379_H
#define PVMJIIVPYZIXMEGBYPKSBKACLKD_T9DB25F016914F718888DC689A2C04BD3976BF379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rewired.Platforms.PS4.PS4InputSource_pVmjIiVpyzIXmegbypKSBkAcLkD
struct  pVmjIiVpyzIXmegbypKSBkAcLkD_t9DB25F016914F718888DC689A2C04BD3976BF379  : public bPQXlaQKObGXPfqqqiLPOxJrgoiR_t20D282424AD47831E3D4341BF71E7F4E2A2DB047
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PVMJIIVPYZIXMEGBYPKSBKACLKD_T9DB25F016914F718888DC689A2C04BD3976BF379_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4700 = { sizeof (gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4700[18] = 
{
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_WigXBlhRQEAAKnPZjEeeNdMahyT_6(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_hqJogBNPbbjuJUDUhZmRhiFNzfx_7(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_OFOCHvgjhTiUuiMSfLxKRWhcDSN_8(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_ISUlQumxWpBMGxJqFBsCyVmpAjn_9(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_12(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_13(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_sTmefogCpWKjKroxmlenFSmgOEmM_14(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_iVIqjwgIJSdpAynaPZewlFkjlkm_15(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_SuFUCNCfYLzmhEAzNVkEiSskSba_16(),
	gfFEHxupIxqqjXJMSeVLorWenLC_tBEC02C7FDAC4D69C5AC13341268EC8D9E113B8BB::get_offset_of_MMaCKxbsSsTsyTnnKtJqCDuwolw_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4701 = { sizeof (PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4701[14] = 
{
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_RXwTkvaioBOXLvHfDHkciCcOgzY_4(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_CdduzXmnfyAqWtnFBTQwIrsXYmO_5(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_zGyjKmKFfNgNUhYcVkNPQvxOonWZ_8(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_CVShsOfomYgQbeMNmgtRkoanKAb_9(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_rvxyBFYAmkGHqKzFluKeiruBUZp_10(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_oNRWjZDkucmsChJTOgREkFFOgDDs_11(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_wyQBbpduedskimtfaAIDTYGcbMj_12(),
	PRiNUjaJuRApzgsZLWSifSXlYyn_tA5976993757450A471EAA1103DC16F75049C1A47::get_offset_of_JrhvhCqJGVNbbEyRvWwwCXHomvh_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4702 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4702[22] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4703 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4703[24] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4704 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4704[20] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4705 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4706 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4707 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4707[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4708 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4708[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4709 = { sizeof (ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4709[3] = 
{
	ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B::get_offset_of_noBGYKSmBMZluLaZfVKyMJZCFFo_0(),
	ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B::get_offset_of_HwwSIfgySgdOsArnWHjmNWCEpPO_1(),
	ZgoffaakscXwSecdxgDVDCqXYPjo_tC5195B3814C9F96610D57746DD1D2B124700A51B::get_offset_of_erhKEnZojNIWZDQIgCzYuEvMtysj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4710 = { sizeof (GyRicTMqpYlIDAFpGCLnPVhblMN_tD6843F94E9C0E3CF940D28B26610C7B317AB3377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4710[2] = 
{
	GyRicTMqpYlIDAFpGCLnPVhblMN_tD6843F94E9C0E3CF940D28B26610C7B317AB3377::get_offset_of_AHOFIsQodiSzMcHMSUpdlsGdKfH_0(),
	GyRicTMqpYlIDAFpGCLnPVhblMN_tD6843F94E9C0E3CF940D28B26610C7B317AB3377::get_offset_of_DelHerRPxmEUeLMSQbMNIPccOrs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4711 = { sizeof (UuxNpDFxmKnoSYsrSKsxwQtUnoq_tA04956336843A1F38733551F40594502C55158AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4711[3] = 
{
	UuxNpDFxmKnoSYsrSKsxwQtUnoq_tA04956336843A1F38733551F40594502C55158AB::get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_0(),
	UuxNpDFxmKnoSYsrSKsxwQtUnoq_tA04956336843A1F38733551F40594502C55158AB::get_offset_of_IRMhfpXtgdEZHjFGxjGEgBwVQnrW_1(),
	UuxNpDFxmKnoSYsrSKsxwQtUnoq_tA04956336843A1F38733551F40594502C55158AB::get_offset_of_EakTGTTOWHojTaRbVQcQfpoXwjU_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4712 = { sizeof (MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4712[6] = 
{
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92::get_offset_of_ZFfZVnVNyeelQEpFvDpXhcoeRLel_0(),
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92::get_offset_of_DelHerRPxmEUeLMSQbMNIPccOrs_1(),
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92::get_offset_of_vRRcfyGudPCBcdddZEsLFGidKVD_2(),
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92::get_offset_of_LioZUEeJRwaCBYtJXIcadxcIGuff_3(),
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92::get_offset_of_vljqUPZnwJgqfGjUYbLBgPpjcuwp_4(),
	MapHelper_t54C25B42AE16C488282F75EA3EAA33E0869FDC92::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4713 = { sizeof (gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4713[12] = 
{
	gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE::get_offset_of_eNkkokAfmIBWECneBzfnnJQeZTkH_4(),
	gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE::get_offset_of_zJxTuhPSOnNBgdzueKjCOGymjoi_5(),
	gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE::get_offset_of_zNNFbghjzSLPGJYeYOqnJcmxPwWd_6(),
	gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE::get_offset_of_gulWLvYXmWNnBaLeKwoCphoAFmAh_7(),
	gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE::get_offset_of_PMjzCYMDQygIaHIrdVBvizkQSow_8(),
	gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE::get_offset_of_xUwYeaDbkiUXsxkwIbDfcjPUiMK_9(),
	gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE::get_offset_of_EaCFZJPkXOUOhVYjrJRNUuVgfYc_10(),
	gmWkTnwensUkFmJGOmVGiQqkoPo_t00CB891EE1212F0855B98F74E83DC7BF0C73C3EE::get_offset_of_MwvgWQwHHdBetSfMUvcMtaciiKJ_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4714 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4714[20] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4715 = { sizeof (mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4715[12] = 
{
	mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB::get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4(),
	mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB::get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5(),
	mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB::get_offset_of_SSdCMeFQuDZMiegrrjajcgDuZcvG_6(),
	mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB::get_offset_of_aRokAlWvWdWjmNbtalyDWzCQaKE_7(),
	mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB::get_offset_of_tnzQdzfTtfgncibXTcLnqGJmJen_8(),
	mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB::get_offset_of_GIXjFsWXRqPddpiHqJOLeiJNGwPH_9(),
	mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB::get_offset_of_XHHoEvWwsnpoTyNeLTlGDgMedCv_10(),
	mNtFskAmnQiMclAlZmMRgtAUFdB_t3CC67DA26055F1933968D41FBA8BAAD80EA2DFDB::get_offset_of_PJmuMUYjdZfCRrgLweWGDFfeSWeB_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4716 = { sizeof (vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4716[15] = 
{
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_OoFswlQbqBhHqGOCooflwYCYmYWF_4(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_pyCASHftKaJjucJBlmeeWQrdKuRk_5(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_fuTCNQGWJwEIEHYwHqJGmcbHooa_6(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_FJwvShZPOLNXNucTHGOTJlqLDHSc_7(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_oyCUksEjNbKBOfglcfdwgdMkcMC_8(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_lqqScjdzncpCzBEMpvDgkJDdBVY_9(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_BRgpmHvYLixyHASvvTNhHPJbEXH_10(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_SnzkgPJFdCGooiTRIHVkihanwVMK_11(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_CwhJXWuTnfToewlxmgBNbWvHAdZr_12(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_MEciOzoedizohxTqjjeHDfmkvBf_13(),
	vMPFtTiyfRGKaPBXFpzyHpeSQip_tB7325EB63EE089483E90CF238DE25CA7DE83E31E::get_offset_of_OBkynwRWyeiCGHMTWDjEKPyZwygf_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4717 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4717[23] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4718 = { sizeof (TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4718[15] = 
{
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_OoFswlQbqBhHqGOCooflwYCYmYWF_4(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_pyCASHftKaJjucJBlmeeWQrdKuRk_5(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_6(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_7(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_ADxKkzWMUBYYOEXoCnKRlJGONdp_8(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_onuUdLqyZglNFRXvVnmNGVhyvEk_9(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_zXzjGKUSBUPcxIVQpiLUWniHLTD_10(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_ReIdYCkkTUyMvyPmUebDXmqIZIq_11(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_PaFIcIdtgpJEDjPWtuVHsnYMPWHh_12(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_XHPgOPOiyVEVTrtGpFVwhUYYVBLH_13(),
	TOqrfxhzmSQlZbxCrgWIubmBMfY_tA830AB0AC8FEAE08289B6EBE55B9B2E619169332::get_offset_of_WErGCEfmgpirqkWeueqfYlfBfKCt_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4719 = { sizeof (BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4719[19] = 
{
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_vJSqbRZAIniSbEUyqTHpxuFtYNw_8(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_VMDLPkTgOreHqOOGhLoNRYnwvXm_9(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_mLoJhLWnQrndurMnPgZbVzWqubv_10(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_wDDeUqbJHasblCouLgRvDsWeBjxX_11(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_KXjZGRrVrxPNijmBloVSMdblfnD_12(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_jeFKojDJfJOTICGSoOZNdenYJzm_13(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_sGSfdVMWlUIzKizeKzVqIOPYzZL_14(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_KEefkDDHpJgSzZksycmQdqHFpQA_15(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_LzJEikbMnyDvpBsogaRuCRFyooLn_16(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_WXFtcsBJoAEMLFABEPTRqAJvkUUF_17(),
	BgwkajhnNshLWjGQRMxHdSGALByJ_t2E36A229DDFF38120ED3DAAFB6AFB184DBAEBE24::get_offset_of_xKgDcMKGQQaxGajsXNhNAbtUgoI_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4720 = { sizeof (RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4720[19] = 
{
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_NwWdGFWvZmJcWWyNAHbepzNMkQx_8(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_szdBYzMOzBEiNOyTEALnijcajARk_9(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_tqREfqwXDYJGtggikzdGDoPwqhd_10(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_ZzwGnlJTbtYiaCdkjhOVusqwdie_11(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_EQLtfLihFjkPplkttAUbffOWqMa_12(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_qARSefdBrGwWJrjtUkHIBfunGxu_13(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_qlxSeKCbykqCuUffKSWUmhkebKI_14(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_BEzIxNkwBDphsEaOwjhYgOHTEXgL_15(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_tdBXLuvRFzGrgLIVoumcTpHTLsz_16(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_gydWPydMODbPNLvNfFVuhkewRzs_17(),
	RdKLDKRppsreMbamXQKinppcbgIa_t1362DC6AF194550694E6B49F2DFECF6E9B0212D3::get_offset_of_AkiDYCiMmPsWOImxmReybvqfaGPI_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4721 = { sizeof (DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4721[19] = 
{
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_4(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_5(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_6(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_7(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_cVBRKPtFhHRCscACnCxtGAeLzXg_8(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_nMKUaBUMYWExQhyhfnfRFjaEMpBA_9(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_PfgbcgoHhhyIfiegGuBEgOgictI_10(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_lLWEQQnlLAUlHsMyqeTXXvKJhoA_11(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_CMrDgpcbpazkOwYfedaGhXKgNIst_12(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_FpHUITUNeHnEhECpuNZuoskuSNk_13(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_tTcitdrBJjkvLvdUOUxWpgdIiSr_14(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_rjXfKwjSOxyyymWpfVfucprygaKr_15(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_AOEAGlHDRxpFjetZtJjCPtnxxCJg_16(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_xTUbhAOkkpcsTurvYuEJspXWOSX_17(),
	DfAbLscRLRBRVwJBxyozfaFytDh_t3C3FEF00DE6F60BE618C4D96A85A939092DB150E::get_offset_of_KsxNJCeXsgEfZvUrlDppORlBfAe_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4722 = { sizeof (gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4722[14] = 
{
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_6(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_7(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_OoFswlQbqBhHqGOCooflwYCYmYWF_8(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_pyCASHftKaJjucJBlmeeWQrdKuRk_9(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_KupTlJjwsvuCuogzorfWuijVDiT_10(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_xJbmBCZhpIClOCoiaxRsbiCnXHoi_11(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_OQwqNMKQwTkZuhhYgLUhxgJHvLW_12(),
	gthDxuZSmdtryDmUAqOKLePUBeXa_t25444AFCC55788312A716F9211CC03E5E4746AC6::get_offset_of_BCUgAUbtQmCLlvbkbMEtzlSbnPk_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4723 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4723[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4724 = { sizeof (LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4724[16] = 
{
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_6(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_qeXBiJGkJPOsYChNhIanovZIDZIv_10(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_lZjUpbYVsHHZaOjywcFfeUKIvpP_11(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_KNHspcwLKOHnkHqLpGFvwvmwgRFf_12(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_WWpwEwCFOVfFCThcYDWwAVmSEfiP_13(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_tDVTJhFpYkhMiEbIKaIynmfpvOh_14(),
	LrWDwqJEfrfOZGsuhELARHoTgpUS_t5DECA78105C14A54766FA928B4A263AC4C18E981::get_offset_of_lnJghGeJvBsfYDKqLafZBImysnCN_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4725 = { sizeof (MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4725[16] = 
{
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_6(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_kvlmkOReOsuPlGtzVYsuzKQKzot_10(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_BtZoJNsgrjeLvHlTejeztsvEagHQ_11(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_vPSvjioqaGGGZNiUosulfFPocLI_12(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_upHULLWDzLCbgIAwDhjpGPmfzNc_13(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_LCSJwoIOgFUkeQxXqctTiThJJqh_14(),
	MiHkPwytqtUrWzRgUqimqnAUAWT_t328F86AC6593241423296E00584E8CAAD4FE84F2::get_offset_of_ZoGyaSnOEvbuHTDVqGMYZZuaVlp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4726 = { sizeof (BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4726[16] = 
{
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_6(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_7(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_8(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_9(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_URBQFqtSekBfWszqNQJIXMsRucH_10(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_SlDNJpcQAjgKJoILnkZeUDovCno_11(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_ePUoGypcneMKtjguroqjnBOyQBC_12(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_mGiQGeMaNbfPDjeDFiBLphiZpRmA_13(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_WnPgZpymgxIBcygizPMSsmzZolF_14(),
	BqbDbEihtPLyDArkMLiPARASUAvg_tBEB0FF1A60F795CB6A0F6D3B8834210951BCF66E::get_offset_of_AGUbjJCTtqVVCLumaYuWgKSxiyKf_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4727 = { sizeof (LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4727[18] = 
{
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_6(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_7(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_8(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_rOdCrMgoKIoahkqSSMCMtJBpwDO_12(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_sdDvxnFolHwwcrdOGVamLWCrEqo_13(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_AHSMAVBFTUzcGFcKShfsCnpLinS_14(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_GqLpAZjfWeSypLuaMRNuestZemq_15(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_CtDhkLgfHuehRmUChwupgTmIyYcL_16(),
	LZntEMOAFGCFZtKDXkOkEobSvmc_t9993E595F142C71E41AC80B033CB598352BCCDF2::get_offset_of_ACzrLpCTdoWBkZpZWOfZxGsWgQf_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4728 = { sizeof (rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4728[18] = 
{
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_6(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_7(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_8(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_bixDTdhhWGRJCMtLFyBPbvYGXTT_12(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_pSrucIRCjCmJLnkfTWqDplqRPeh_13(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_HBPdylGmJOYFklrOiPYBaUpIOmN_14(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_YeijNFyiBhBFcBBRlXccsNEEnZam_15(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_lGVfuIaotuaJxIknLjgArOnxwKg_16(),
	rvHhChdAfNiPTbGOnCDtdlPPnPZA_t45828CE0019C9DFC0A9BAF674A66B02E58F5202B::get_offset_of_gxzAjugGiCLhjGlKynPliScLgwl_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4729 = { sizeof (ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4729[18] = 
{
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_jeLkzDDlxjdyTfidRDslEjdPyAn_4(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_wneHRsRblNFSFcNjImlYgNBNpoh_5(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_xJPYTeJZICkFpIWTwbMsXQVEALx_6(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_kilUCbTWeCfCdDqQSHFWpvHLrBl_7(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_8(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_MIpezzrjtDrOhbQbHuUvLlLITDa_12(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_DASUOARpziUcAREfuWStVzgjFzp_13(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_bLbTipzKwUeoEarbqhZQDsQXQzyr_14(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_QulqzhoQHxWQqNfCnLabvDfPYQW_15(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_racLOdTkGZjnXYXGPCIWPxcsJFM_16(),
	ZCnVNazJHokdnsnBzhmcsaJVfZj_t11E69DB4461E2D0CC080E2A55FF1E8074CA5E18E::get_offset_of_CIPCeRrmpIUwjJWypjSJRrXojYT_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4730 = { sizeof (ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4730[26] = 
{
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_qJByvTGLorKeLizFyjtftOInyVr_4(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_SanVLbjjVuCJGdzcsHmJAMmULDaN_5(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_YIiyDAYOdHrCPEfxXABEMrhVIIXe_6(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_rpBVoAOjatpRxhDzOjWKkXyzjmH_7(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_pTVJDeVeJlQThAZkFztHqDAWDUa_8(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_jaqfFCICEjyQtCeRrGzrkTCNIcKx_9(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_TwaNOMlGNtAEaMSLqisoJEBMSTs_10(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_EVKdxtFZCFeOvEumfRBcIsqIqNHp_11(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_PdPpJvbafbhutHErebhedpBvgcqA_12(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_SmYnMfHcfpeceinsLzULWLXqcdl_13(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_BhwDcIGwaSCzBcJVoVqHOOQdOIGv_14(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_weuKCoZAmvCvVioJahQuGnpAFJML_15(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_MffrcHpPtnkBaVREoXdvthgNpbj_16(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_RfsvAmBMJrTOuTeemIwOAGGTbHJ_17(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_LAHgYmXkprDMygwUGRhhkYhpjQz_18(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_BAQaFFarWZvZRaVLfPrWBSfJKdqz_19(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_qPKqUoJEYXfxoFlGpDGRDUxHGpGp_20(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_qnyzmLlKIjBWsBItCynTkLPktojQ_21(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_PwphjbqhFYBgNlblbCunFlclFYG_22(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_SsLgDdKsoZjfwHynFGZqtFpuMaQN_23(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_nhzvhDxVQckJidNZgcMYPImmKAB_24(),
	ozYWVrhqByXUVmaxtyLPPdFtgTk_t435B6195479C21B59EA721DD1208873F0878FCE5::get_offset_of_FjaMvlDEramtbltDJipLxhnSGjk_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4731 = { sizeof (PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4731[3] = 
{
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24::get_offset_of_DelHerRPxmEUeLMSQbMNIPccOrs_0(),
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24::get_offset_of_vRRcfyGudPCBcdddZEsLFGidKVD_1(),
	PollingHelper_tE345DB668E84431AA09C97B278D2F36D1716CB24::get_offset_of_MHfTbpbnrYCtsEVBnetgGDrfXxM_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4732 = { sizeof (XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4732[10] = 
{
	XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D::get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(),
	XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D::get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(),
	XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D::get_offset_of_qbYVUsevpzMZRhlusfJuhWcAGyZ_6(),
	XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D::get_offset_of_ecefohvCgnETzaWbjgfWJOxaNThk_7(),
	XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D::get_offset_of_GTnBQninYpdBXNnnjRlSOtkVmRh_8(),
	XFIgJZjPTfZLOlNajMCJkEeTLIKB_tB7C2AC1D748F8D8A211EAD0C6F3BF9D4C9B3730D::get_offset_of_OsyGaKrcvWcznnvuWTxaBOrYODn_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4733 = { sizeof (ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4733[10] = 
{
	ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E::get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(),
	ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E::get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(),
	ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E::get_offset_of_qNKDPmeHjbkHjvhgHFrgmgjZYoXO_6(),
	ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E::get_offset_of_XtcStwMHinfKaHYwdgcMinLsiMS_7(),
	ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E::get_offset_of_CtKvIEUfqriYugaQWIkpoTWwhWZ_8(),
	ZnVvjSySQWDReUlYCfRvXlYWcosK_t1D9CB290AC48CC781D220DE736A5911CA154295E::get_offset_of_vtnVomtnWrFhLWWltmeRYPgyAgo_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4734 = { sizeof (diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4734[10] = 
{
	diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8::get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(),
	diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8::get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(),
	diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8::get_offset_of_ZgGXCuLyylFmIFXeYRUUrLoUVlX_6(),
	diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8::get_offset_of_dcoaArEgvzeToFoIlYrLJNcyOYTe_7(),
	diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8::get_offset_of_wzHnRYMgTKQeNCGyRXgqqHKnuZe_8(),
	diNLXbawbaQpAxdalefacoCSVbD_t21B810E07D5FF367226AAA6CF387E498F0B1FEC8::get_offset_of_cvlShwzUhITmcxheQCGkGNLTJaSf_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4735 = { sizeof (WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4735[10] = 
{
	WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F::get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(),
	WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F::get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(),
	WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F::get_offset_of_IRQFsXBuxGaQqSPvASJkVExAlvIl_6(),
	WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F::get_offset_of_bLwxtYaZuWRqOVNKgwLYPwzVIMU_7(),
	WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F::get_offset_of_IbFgSQoknudTqPStuJGhGvLEjpS_8(),
	WeUmGyPBOnhafqbSjEIYlDkteEs_t7D717A06BD51BF285CF48128B6E80CCA028F133F::get_offset_of_MxEFTmKyklwklfapaApOEwBseZO_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4736 = { sizeof (AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4736[10] = 
{
	AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B::get_offset_of_hMBbQHSYpxrZxtjFPBFvFGsQNmg_4(),
	AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B::get_offset_of_piAnfnMqsmhCZfWFjwHhSJYtiKf_5(),
	AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B::get_offset_of_LXZSgbgtnApwPMGxdeZBLyXVpWq_6(),
	AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B::get_offset_of_cuaaPPKOqGXjshzgYnqbpedMFcDM_7(),
	AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B::get_offset_of_izufmMtxxrCqQERuemqPKAlSNTjf_8(),
	AKyQkVUIkXSicXJHRKEnBmrwqtU_t5EEC4C5C9AA006782CFB8B26B308A46CB260AC5B::get_offset_of_JRBBXCmTHnFgcSOfjDuXshzmCDn_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4737 = { sizeof (HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4737[10] = 
{
	HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA::get_offset_of_yjlqyyWOozfDvafSPImsgZHdlEC_4(),
	HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA::get_offset_of_gSpnbjnaCFzSmbyaPfTEsBsEqoD_5(),
	HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA::get_offset_of_alECYxDfuAdqiUIgrhwDYaHkDsLa_6(),
	HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA::get_offset_of_LkCGWlVEPUdxRhqAwaKDHzIJESI_7(),
	HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA::get_offset_of_ShUoBUkcTxMJNUPwsyJWPpwyGJMA_8(),
	HvLdRHIeQviTJEuxEohliDcYtge_t6A0ACFB55C555840C4318716A968D645A4E6B0AA::get_offset_of_zsqOmfXuGsEhTBJYNgetctHdxXw_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4738 = { sizeof (nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4738[10] = 
{
	nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E::get_offset_of_nBmPCEsHqRIgNvsUMUxEXPycBSk_4(),
	nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E::get_offset_of_HlElcSicCOyzzunUrGfwwCHcstl_5(),
	nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E::get_offset_of_MKPdMGofjJUFETwfluQvGtCAksT_6(),
	nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E::get_offset_of_wTHSjkWuRrrthXhyBDmtiAoxiZsK_7(),
	nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E::get_offset_of_cVkxKFvIoIqeGCBrgUMnHECzAXG_8(),
	nKUYXUHVTIXMohtJmxTkPuyKmsT_t48A2AB1F4FB94922EA0784421A5A5744601CB56E::get_offset_of_JxeurYaFSMEUZxgJoDtsYijwoMO_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4739 = { sizeof (NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4739[10] = 
{
	NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6::get_offset_of_zUYqedeSKMBBfryhyEpmNaaaVuo_4(),
	NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6::get_offset_of_LaRHWkTAdhzreLHvmkAbWTlUJZk_5(),
	NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6::get_offset_of_ViivwZdGtAXPpovrLEdNHfArqOx_6(),
	NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6::get_offset_of_OERkBDITbWLHsYxuqwldzTjElYa_7(),
	NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6::get_offset_of_GtuVdHdHWCZSIszFGYtvHAmbNQg_8(),
	NAcCVompizOBGZKcXYKAfoMhlNe_t022B95CDD88767E9F6406469FC895ED2BD2B7AE6::get_offset_of_UsxcWtJqQRUlMOmLGyXKGxLeumB_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4740 = { sizeof (vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4740[10] = 
{
	vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586::get_offset_of_LJHlfEVUxTkHDSgQmduVhmOmhIR_4(),
	vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586::get_offset_of_VBfAjlpfIAcskfNhPMYyyTLNttPJ_5(),
	vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586::get_offset_of_aYoGSMnqNTemdgpuJcbubHCDowZP_6(),
	vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586::get_offset_of_moOSaqGEcYIjbaNbsqBPUMvbCne_7(),
	vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586::get_offset_of_rJUgbWivcyxaYiJSzSuBsSAmYyX_8(),
	vMArwqElmXiIpzjjBdQsEgyRaaha_tC52AA8D9FDF2F76D18E7C93AF84AB8AB424A3586::get_offset_of_wtVkGLxYEZhrNggRFLXUKCWwjGD_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4741 = { sizeof (gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4741[10] = 
{
	gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA::get_offset_of_GCsQTeqGvLRkNFJNssxIvIahsYx_4(),
	gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA::get_offset_of_niXFFKcmIAMcbbBdqTJpvDojTBIn_5(),
	gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA::get_offset_of_yJZkGbYqkCXIaEgSZTyuudHYuYh_6(),
	gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA::get_offset_of_tHtKZdDnFlBBxqwiWQtjTIGvvxZ_7(),
	gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA::get_offset_of_UGxLeTcLdFISEciMjdoOAJrqKNwM_8(),
	gcXXvautqXwDnQJUMveqqqiEyZO_tF47D2EBB3DB6B1E9FF0B6D0B33F3EBAAE93EEEFA::get_offset_of_ZyIvfKZLJrYxlXOsSdTWaGCvGXi_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4742 = { sizeof (NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4742[10] = 
{
	NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2::get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(),
	NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2::get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5(),
	NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2::get_offset_of_fkLmPSpTTpThPlIehVLEEKpzSSw_6(),
	NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2::get_offset_of_jkSRyVQrmabuCHjrIhRkPrBsQXa_7(),
	NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2::get_offset_of_GysJxsKYkmwXzIHWouLqCypPCtP_8(),
	NEuOmMdVTUnKcnfOjoDSefbcWvI_t2A19456568F6D7239CE9EA8B0FD01DDB3C8FA8E2::get_offset_of_oGNjoDKoniJyTKcQVJdgsoUHlPN_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4743 = { sizeof (yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4743[10] = 
{
	yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5::get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(),
	yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5::get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5(),
	yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5::get_offset_of_YeLZFoQfDBSORYCuWDOuawbTVVYO_6(),
	yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5::get_offset_of_MsPbEzbRQgWGkpgEHmeVAFLhAzer_7(),
	yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5::get_offset_of_nthCnDjDDeUgwqRvjHFEwODUjwJ_8(),
	yZUcGJEhtUUgGQGtmnkYzSaHWWDi_tE35B9D31AA3B92B07BEC7ADFDBA8B6E1D34869B5::get_offset_of_flaCxMkdweYiaNkapsZttdRMACw_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4744 = { sizeof (tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4744[10] = 
{
	tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E::get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(),
	tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E::get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5(),
	tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E::get_offset_of_KYWLiBSXJxrwyYQybJcFUpwpAAr_6(),
	tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E::get_offset_of_VBRuVwCysljKLQBiDWwlpXrFkUy_7(),
	tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E::get_offset_of_tNseVKBmKSodAyituDztyLjGwve_8(),
	tCskCsIHMVcyQZNCxlmhcaPzbSY_tEB8B1D4D0ADCE8AF182AF19FB4E66D553F06C43E::get_offset_of_yOkkFJfJsdRFLNpPIRjTWlHucNG_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4745 = { sizeof (cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4745[10] = 
{
	cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA::get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(),
	cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA::get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5(),
	cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA::get_offset_of_EXqvCeIGfyiPfBvrhTrFQEECrIs_6(),
	cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA::get_offset_of_GzsbMdtcKxiiZvcBvfLLhMyuVBA_7(),
	cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA::get_offset_of_XtXfuZHZityIduWboLrseFlqMJP_8(),
	cnmstNPDYOvWZgQduIkdxeawViJ_tECBB1C577121EBE30312891430905BF81556A8EA::get_offset_of_WKEXKgMKciWWennvQJNaZHBLgLQB_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4746 = { sizeof (xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4746[10] = 
{
	xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D::get_offset_of_omdPDTkbMQMCXIvHqkpJkHTSFLE_4(),
	xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D::get_offset_of_eVNodIsBtpdoajQYgHVzEGonurY_5(),
	xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D::get_offset_of_DbEuFCCKKECbasxrlRTEXgKEmHm_6(),
	xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D::get_offset_of_bwjpmclZNIwLkRfWzaZHTVHKInxa_7(),
	xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D::get_offset_of_UVRcHhbtljYQdDMOHhPGAvLMDyd_8(),
	xrlYMDjdWWzOitLHDajJuQGbxaQ_tBF38507916FB399EA565CF03A94A607D5D37FD8D::get_offset_of_LWzUkgaucDOPLJJjoJBjOaDZPQI_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4747 = { sizeof (YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4747[10] = 
{
	YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF::get_offset_of_grsSPenuLaffXjyCYEgfUyNoLhD_4(),
	YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF::get_offset_of_iOOfWhiNRrmZjBKVcgnXBpLYORW_5(),
	YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF::get_offset_of_RGMofoBpYjOtFuUVxaHLVpQgFEW_6(),
	YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF::get_offset_of_tCsuKotdlEDURGcQQqCsAkoitnKF_7(),
	YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF::get_offset_of_XZiyiPJkBaVCZVvfzWGRyOqWoEl_8(),
	YntKvCeRsGEsKHBVLIaNoNphMur_t4D32988B588735E175C13E6D13280EE0D219BEAF::get_offset_of_MqHAUSddShjakUUVqrvjYHlyjwE_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4748 = { sizeof (pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4748[10] = 
{
	pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512::get_offset_of_AzFeQWbdNSyHzOokjjlhrPLiOoy_4(),
	pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512::get_offset_of_qkbWfWnlUBDMvLFHbNZWOlxXNoo_5(),
	pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512::get_offset_of_KxEcRhLfTSHONByKtaBOnhSOBkc_6(),
	pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512::get_offset_of_licCgLfhviTZigQtvIJIffWaosgd_7(),
	pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512::get_offset_of_HtrfGgyOdYsirINnQkoSMuavqdB_8(),
	pdUhzIzwqLTirgsJpgvCLVcVkqH_tBDD7AF9EABC036FBD4B6D04F57900F4F14FF5512::get_offset_of_ejjQxPHABCGxuasGvROOvfNVVPNT_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4749 = { sizeof (QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4749[10] = 
{
	QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63::get_offset_of_WlJMddRgCkILfsNEyJrynAYlZNd_4(),
	QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63::get_offset_of_kOrYFTHklSsyuAVOvjAuWGrpNKT_5(),
	QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63::get_offset_of_PbJflXkfhbqNfPVmadSsbKOIfHNm_6(),
	QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63::get_offset_of_dkYgsNIinFjxxCKjCJJgyzptBwzL_7(),
	QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63::get_offset_of_NwJnCKFisXaEmuzxzGrDOmZKnhR_8(),
	QYRtgpMFkndtfdgUsKrWDQoEGYV_t2B0D1818E3DE8D987236ACAE2AB51E9C40A5DD63::get_offset_of_KXwWIrZEnGCaCddXFGeyvTxTMnfZ_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4750 = { sizeof (mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4750[10] = 
{
	mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3::get_offset_of_OPdZHGoqixWVplPNpKliwYtMXKS_4(),
	mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3::get_offset_of_lfTiMkclPxkMJOMngGPFpOELFpJh_5(),
	mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3::get_offset_of_tlanGeuDkWgBcCNZdDqRcQUTwPog_6(),
	mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3::get_offset_of_MSPenACddlWMzevXUfCXfrmwbSxP_7(),
	mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3::get_offset_of_WGlNAfsZsHPTsByKPknEettpgeU_8(),
	mruikTvAMkGRHgvrZAVuJtbiKhX_t92B21FCE298157CB15A6FC7843D181FD8AF666D3::get_offset_of_vLKoFFARdAKwqxLNDABJctemIOo_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4751 = { sizeof (MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4751[10] = 
{
	MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A::get_offset_of_BZcDuWaehMrQqBxjsYKYQcSVhZJX_4(),
	MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A::get_offset_of_LNVYrrlmellSQmSXzqaXczxYopZ_5(),
	MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A::get_offset_of_mzaEkURLsoTtRqjdGqXSeqirFKh_6(),
	MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A::get_offset_of_aNYOcjFiLfGjadlQfDdEEoMhriRm_7(),
	MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A::get_offset_of_bGTTJRmpDhKUIuVPrIPqROoKdKU_8(),
	MWTysHGelLMViAPKkIGyXitmGEO_t2C4D3C5B0C758AD927FAEE38B86AB9A2A800D82A::get_offset_of_pDCHARvqClghktOpaTUhurmzDsM_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4752 = { sizeof (HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4752[10] = 
{
	HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9::get_offset_of_tJsKQNEqXVemDDJIXCshTUBAPRyM_4(),
	HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9::get_offset_of_vmYQYvHTiaaVZFFcFKtCffENjjo_5(),
	HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9::get_offset_of_ovZIilmHEFYPccoHmpoAPVWKsJT_6(),
	HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9::get_offset_of_xavlzvxgHIwtBjtgQbjNTqeDVNH_7(),
	HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9::get_offset_of_ktEOrCXXgBpIFwKvshCEnMEzpmG_8(),
	HhKDgjBXKPDLdtTIHEqFdZFOBJDi_tB907C94A3FE3AA1A0EEE7AF60F386775FE2D51A9::get_offset_of_CZPvdsJztOTzkufRzOxSFjclfQC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4753 = { sizeof (CodeHelper_t8618ECFBF040A78FEDA26D398742608E1AA036AB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4754 = { sizeof (LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4754[9] = 
{
	LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3::get_offset_of_status_0(),
	LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3::get_offset_of_primaryUser_1(),
	LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3::get_offset_of_userId_2(),
	LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3::get_offset_of_color_3(),
	LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3::get_offset_of_userName_4(),
	LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3::get_offset_of_padHandle_5(),
	LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3::get_offset_of_move0Handle_6(),
	LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3::get_offset_of_move1Handle_7(),
	LoggedInUser_t2BAC7FCB1ABFA5E27467904F22AEC2DD39C1F2E3::get_offset_of_aimHandle_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4755 = { sizeof (ControllerInformation_t44F7C14FE4C1B05D1A1AF215FD6FA245CDA36D2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4755[2] = 
{
	ControllerInformation_t44F7C14FE4C1B05D1A1AF215FD6FA245CDA36D2C::get_offset_of_padControllerInformation_0(),
	ControllerInformation_t44F7C14FE4C1B05D1A1AF215FD6FA245CDA36D2C::get_offset_of_padDeviceClassExtendedInformation_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4756 = { sizeof (PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC)+ sizeof (RuntimeObject), sizeof(PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC ), 0, 0 };
extern const int32_t g_FieldOffsetTable4756[14] = 
{
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_touchPadInfo_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_stickInfo_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_connectionType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_connectedCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_connected_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_deviceClass_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_reserve0_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_reserve1_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_reserve2_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_reserve3_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_reserve4_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_reserve5_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_reserve6_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadControllerInformation_t65AB8B52C8637880F29C122CA4E0CE678B5A80FC::get_offset_of_reserve7_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4757 = { sizeof (PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253)+ sizeof (RuntimeObject), sizeof(PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4757[5] = 
{
	PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253::get_offset_of_deviceClass_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253::get_offset_of_reserved_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253::get_offset_of_capability_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253::get_offset_of_quantityOfSelectorSwitch_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadDeviceClassExtendedInformation_tA89B04DE21E201757D5FB9CBF3A7DDA35B49A253::get_offset_of_maxPhysicalWheelAngle_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4758 = { sizeof (PadStickInformation_t6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C)+ sizeof (RuntimeObject), sizeof(PadStickInformation_t6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C ), 0, 0 };
extern const int32_t g_FieldOffsetTable4758[2] = 
{
	PadStickInformation_t6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C::get_offset_of_deadZoneLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadStickInformation_t6696679B5EEF9E9CBDD21DE9AF722FBAF71C061C::get_offset_of_deadZoneRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4759 = { sizeof (PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D)+ sizeof (RuntimeObject), sizeof(PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D ), 0, 0 };
extern const int32_t g_FieldOffsetTable4759[3] = 
{
	PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D::get_offset_of_pixelDensity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D::get_offset_of_resolutionX_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PadTouchPadInformation_tBA2B76541DC960B1E2E508DF26FB887491C3B77D::get_offset_of_resolutionY_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4760 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4761 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4762 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4763 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4764 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4765 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4766 = { sizeof (vLEBhncKpOPFOKOXDdCbdgTKhyd_tA01C4EADEDC175C2FAC271C09D7E5245A52EFCBB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4766[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4767 = { sizeof (PS4ControllerExtension_t027116553D089D8E7A625D4826DF5120FFCB07FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4767[1] = 
{
	PS4ControllerExtension_t027116553D089D8E7A625D4826DF5120FFCB07FE::get_offset_of_CdVWyCtwdYqRItEtEoYUUDYOAqxD_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4768 = { sizeof (neBBiiEJZGOxLJguIvQcLTdZvXc_t9E0C1572D8BD03485499BDE0D5E22C70529DA1D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4768[1] = 
{
	neBBiiEJZGOxLJguIvQcLTdZvXc_t9E0C1572D8BD03485499BDE0D5E22C70529DA1D6::get_offset_of_zSCjKsgSKyXaLUKczdjHusCWAAC_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4769 = { sizeof (PS4GamepadExtension_tA4039D658BDA1F08F9AE228646102C7DE9ED9A47), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4770 = { sizeof (PS4GamepadMotorType_t9D22790C80C84BC0A22FFD0682A01D53C7C95BC8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4770[5] = 
{
	PS4GamepadMotorType_t9D22790C80C84BC0A22FFD0682A01D53C7C95BC8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4771 = { sizeof (PS4InputSource_tB2AB88BEE18488BC309D02A1DBDCC27D25FB6C90), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4771[3] = 
{
	PS4InputSource_tB2AB88BEE18488BC309D02A1DBDCC27D25FB6C90::get_offset_of_tNBWxJnZzeOxsqvqkCNPNaShizE_7(),
	PS4InputSource_tB2AB88BEE18488BC309D02A1DBDCC27D25FB6C90::get_offset_of_DNrDrWYCpwQSRfJdevNzWxrTMZC_8(),
	PS4InputSource_tB2AB88BEE18488BC309D02A1DBDCC27D25FB6C90::get_offset_of_SeCUoinDywZmqZDHRKupOdOaTke_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4772 = { sizeof (YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77), -1, sizeof(YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4772[13] = 
{
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77::get_offset_of_QRPeTqgTMycFmacKSbFIvcNcyGo_0(),
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77::get_offset_of_XOVxOqwmCFXMdtGZbdFPWpFTsTl_1(),
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77::get_offset_of_jUYiFkVkWPgXQbEAUrwkKsBeMRaJ_2(),
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77::get_offset_of_BgRnmItikaXPerXhcvSYSxAmonR_3(),
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77::get_offset_of_QbvyhOdowWTAxjeVZEkUyJRHPjq_4(),
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77::get_offset_of_tQPGaqjXdNaJedlKauqSntuhYwuQ_5(),
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77::get_offset_of_PSRvnRfQDraNQOZHkMFWjNnBnsh_6(),
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77::get_offset_of_PVvaGZfIVlrWuEwjgtaskHNFEpFh_7(),
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77::get_offset_of_sNerjSQOBhSIMSAFEuqMDxaQmBK_8(),
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77::get_offset_of_JVhoOrNuXIQFJsRhZCJOqfQeewlB_9(),
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77::get_offset_of_gtrUwLnTdtgLUvCBUTRuYCiGUWX_10(),
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77_StaticFields::get_offset_of_xuKcWqHPyZZLojTegosyfuffNaVL_11(),
	YUATmdkIYeDTVeeKKfwolrMGUrN_t7FF3032418BD332E45EC548B8B7B6B766958AA77_StaticFields::get_offset_of_xgFcMzbcKwfNhKMwwFcokhPKEXIb_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4773 = { sizeof (PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF)+ sizeof (RuntimeObject), sizeof(PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4773[4] = 
{
	PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF::get_offset_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF::get_offset_of_zSkfqYfrOXJXcopeOkTalFrxsHk_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF::get_offset_of_GWNhMohgMSKpOKjKZrAVmDEaFCd_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PYOLdyXwCbgsmWCZaTDfMJgndAl_t1997F390DB0D5007BB63F4BC303BDA66F1F503EF::get_offset_of_pTXqUwcYdENkAAHGFsRzWXKXrZB_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4774 = { sizeof (ZFbPVMKgGgElciFwNeAzftnIMAqR_tAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6)+ sizeof (RuntimeObject), sizeof(ZFbPVMKgGgElciFwNeAzftnIMAqR_tAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4774[3] = 
{
	ZFbPVMKgGgElciFwNeAzftnIMAqR_tAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6::get_offset_of_TSxIVLfBUyELziEzwQrAMRmzGqrD_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ZFbPVMKgGgElciFwNeAzftnIMAqR_tAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6::get_offset_of_zSkfqYfrOXJXcopeOkTalFrxsHk_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ZFbPVMKgGgElciFwNeAzftnIMAqR_tAA504A9D7CB1A0B4C2382B49EA5C68023DA922F6::get_offset_of_pTXqUwcYdENkAAHGFsRzWXKXrZB_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4775 = { sizeof (imtLMjZbMBHFikNPqUtEGzfQavzx_tE4B478119BDA06E0C67ADCBA34F5CEBD76F19215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4775[4] = 
{
	imtLMjZbMBHFikNPqUtEGzfQavzx_tE4B478119BDA06E0C67ADCBA34F5CEBD76F19215::get_offset_of_pTXqUwcYdENkAAHGFsRzWXKXrZB_0(),
	imtLMjZbMBHFikNPqUtEGzfQavzx_tE4B478119BDA06E0C67ADCBA34F5CEBD76F19215::get_offset_of_CvmowhMEGZETWwhyZsLYVgJGqPp_1(),
	imtLMjZbMBHFikNPqUtEGzfQavzx_tE4B478119BDA06E0C67ADCBA34F5CEBD76F19215::get_offset_of_zSkfqYfrOXJXcopeOkTalFrxsHk_2(),
	imtLMjZbMBHFikNPqUtEGzfQavzx_tE4B478119BDA06E0C67ADCBA34F5CEBD76F19215::get_offset_of_GWNhMohgMSKpOKjKZrAVmDEaFCd_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4776 = { sizeof (KlWFQfeHnGkKCcatnYpkzPcivWP_tD45CBA99A77C0D67BB59A8B3E713FA67899715E3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4776[5] = 
{
	KlWFQfeHnGkKCcatnYpkzPcivWP_tD45CBA99A77C0D67BB59A8B3E713FA67899715E3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4777 = { sizeof (ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9), -1, sizeof(ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4777[21] = 
{
	0,
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9_StaticFields::get_offset_of_BaGbTVZuvXfpUGTSlCjpUqLZijs_12(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_FCIziTxDWAnyHATWwZeMEmiXvdc_13(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_bPrhJXsCXGJcROQSTqUrDFGQpAV_14(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_jbyLFeFlKavAMzMlQdBZSCTfOUY_15(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_lmBvgcupLOPeeGLFzFjqdjHceqJi_16(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_iEOKtQxQPtNlJgohLlwhCaQebAA_17(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_DzGpnIrIiBgaZZutTXMWrCjIHVAh_18(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_eqqhvTfqBBgUYHrZujhYBdEEMEV_19(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_DzAgXbAsFcjwinjPLOBYLcFrhIr_20(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_jJcGAOgHrntzwBnkDtkgtVxXoTE_21(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_KuOBoKEXaXnsUmUWfalVpEQPBLC_22(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_NTsMKIHbzzFkBBFPaQqJQeytbvD_23(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_LFifznDPvhfNRBpdXfgbwaFSgGs_24(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_yiXKAHllyDmQlfZQccEGegWUimJ_25(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_egAXiAtTGeshJIBQsJWWhqgtlWu_26(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_EKZEThysBIbRLLOmbBKIxvTvPIe_27(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_LvLzjztTIRIUWhsEUhlEmObmghR_28(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_vPmUStfFLBEocaEdvYkuqnSyeuL_29(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_bjfPXCqUhdrdhnQIIWAFhpWImVr_30(),
	ReRqQZNVnmKJXmrmWwzirxfUKzM_tB8342C505CC902205234CB4CDFE12756108EB1F9::get_offset_of_XjwgwCgXRWvXlTjIVBujMcjgpcZI_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4778 = { sizeof (ControllerType_t4C7A641934DBB047AA97D6BFF4B188AFA39D5A14)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4778[13] = 
{
	ControllerType_t4C7A641934DBB047AA97D6BFF4B188AFA39D5A14::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4779 = { sizeof (qqVLgrRtXSDSUCHChUgcEBVcwDd_t25289B63B5531BB2D243558E594B9D07FEAF1AA4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4779[4] = 
{
	qqVLgrRtXSDSUCHChUgcEBVcwDd_t25289B63B5531BB2D243558E594B9D07FEAF1AA4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4780 = { sizeof (uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4780[5] = 
{
	uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D::get_offset_of_itTafNyeQoucrbhPkPsSqRewAEI_0(),
	uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D::get_offset_of_tOaTpOwzcPDCHdCDIyYaeuASgAM_1(),
	uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D::get_offset_of_ccRBszFJOLqxWvGmvEVjaITEsmlB_2(),
	uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D::get_offset_of_YJKKCodZAAdHLAznZhhKAnRrzmS_3(),
	uHmqJGqYoMpKFcMgEPjnasPBJId_t58930416A761A1E6E5BD3D32524AA74FBBFBB46D::get_offset_of_wRYEGHNQAUJQPkQCpegDxMfPnyF_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4781 = { sizeof (MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4781[16] = 
{
	0,
	0,
	0,
	0,
	0,
	MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5::get_offset_of_bjPvxtoovSXrSHlpyegHEODzGVt_37(),
	MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5::get_offset_of_AgefOoehIcsNBZsKZoqMCdHPCFzJ_38(),
	MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5::get_offset_of_qYESuZrdeLezSalCTOegbUPejIXf_39(),
	MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5::get_offset_of_NJKRZHwKouDcJqLmrCPOfPtmlHqs_40(),
	MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5::get_offset_of_gdYdzjlNkCDsQpofrEknRDzfFBO_41(),
	MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5::get_offset_of_baEjPyGDurxMkmegrCRHbTWWahsg_42(),
	MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5::get_offset_of_PpZtFsDtZDwcRLTzDAWCrppZRDH_43(),
	MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5::get_offset_of_BuTnUeYdAzWywYrICeHcpuPWosW_44(),
	MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5::get_offset_of_ZnLPPnTEwpcMJyOvWQWcbUJDmpZ_45(),
	MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5::get_offset_of_coEaqqupSzegPEYvCFWdPsbKfFtY_46(),
	MHIEeEEHHZDfuuemiSbiTpGsluvZ_t051AC12ACAF43270100320CE3CDB986F36BD48F5::get_offset_of_gkeqrFdfxYdjiQGlAFUvoHFqeEQf_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4782 = { sizeof (bPQXlaQKObGXPfqqqiLPOxJrgoiR_t20D282424AD47831E3D4341BF71E7F4E2A2DB047), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4783 = { sizeof (pVmjIiVpyzIXmegbypKSBkAcLkD_t9DB25F016914F718888DC689A2C04BD3976BF379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4783[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4784 = { sizeof (SVPEvTpbScfCfvLhlhingujazBfe_tB7103520E69B370373D4C059278E7C22B8F5C20A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4784[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4785 = { sizeof (EoGUlEPqgudCsiJuTWLtIwfLJMQB_t3C73104D2169E81D22AC3F277A8567CFE5C72C87), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4785[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4786 = { sizeof (IDjfMDzOyazAMilfUmspNBkfLfx_tE789ED47A7F7E027BBED3C807535FF2210BF73B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4786[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4787 = { sizeof (ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE), -1, sizeof(ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4787[79] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_BmbBIGhcPaQcJtPaDIuasDCjqQNz_7(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_OnpAheqwnSbTCzETUFrLEHROgfTo_8(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_IEqlUZsiLAwwHwjaJiWawGshrHQ_9(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_RVkFnTmTOxSxyomVVDMNYyuaQAg_10(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_fsKUnkudBKaclGoZoLGqLplnpcq_11(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_jkjiciUYDTwUaFkcGqBpczqirBH_12(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_kuvduxCaGZogunQJJYFMqUHXuo_13(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_oGbqxjxXGgvgXNLPUMbdLzEDsmT_14(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_AEpGUwNwMAsWjLqSLSkygEagFsW_15(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_ZfYcJdvFIDCHAofeyUpUZGLeLMy_16(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_efatONDtZGNgmLOHvbVBLBBuace_17(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_ebxMcikMAQQZUToudUFSEUFYfeA_18(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_WLJLSQeGpPKUuKjBWrWTbaFnDry_19(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_yUCTrJSSAVMiycEtyjhErnsZbYwh_20(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_sqyjgtZgshnxlArNKRhZoKPXcspD_21(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_RBQeQNGYzPPywsMagaNKPxDkXre_22(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_QFntQDmTgBlbbKQmkWjoaxmXfDr_23(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_qJWGQSjuXpqXJHLVBEZbgltbRhA_24(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_nFsjgQIYeASozAWVpsmKWYrynGMt_25(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_IxkxAZDFhYdDnAQiwkHxbCHkabao_26(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_kVozOJjgIMPdyuEiTtvdCQDrpVk_27(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_QzLGqBuiJUSnYPDYiBGmAYvPcUAw_28(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of__id_29(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_uYPBIvZJZOvpItwzdZbRubjNHBQ_30(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_EDBzxspLLLJMCVwEjgVqreXxWCw_31(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_FidPeDgESHAdzaeFXcrFbXGDXZpE_32(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_cuHMjPjtOdcyqFzxRQNlFdKcQrI_33(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_IcomarPgDQOtvFQAqkQIQWVwlZf_34(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_USvTrWNQsztrTDaJRlzzYsugcyN_35(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_cZhgkwtQTFgoqgpMoqypewKgXXZA_36(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_qBplpkUglPNgqwzfqaaAbLOGttB_37(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_ncvdGtGSDIImpbUXOKhmHLFdPjbR_38(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_gqSFnpTCFawuMMNVGgKKIsYZUXL_39(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_TojkjqMwwMdhKJGKNsXcfEQqdqk_40(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_OdmAAKBtcbRSgXiuxOTrhpIzlofQ_41(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_ABhAFxjUUVHIXxvYBKqRrBnPSGY_42(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_wAGfrKtlxRtjFeYStOdjEyhnkFW_43(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_XKQeRyjlqgLlnGtmsVoUhRfEeEu_44(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_HGNegOULkehGQCIplmMVqBQjWvm_45(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_VCOpkyvHUubmUcZWHeKYunuBQXa_46(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_BuGUijCbDBaDRmwDRglZiFsDvpqe_47(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_NkZtgCLxdOallQBmQLJnhwLpvNR_48(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_LEeKGKpsAldSIdJikAAjfwxSeje_49(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_vuOZQDAdWNOZNtLPcsGWbHRvFwF_50(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of__ApplicationFocusChangedEvent_51(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_SjPvewVCAcAkUrlaNILvKkUJkKyE_52(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_JrUdKzkIBWnUrmLThTqrbZhGkNnq_53(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_NYEbfVtXwPGnqWryNGfgyhMndBE_54(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_RKICUSyOXlFZjskVbqFIuVGlJls_55(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_vasXwzLRByGLnTqYebmSZAOcNcG_56(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_mqfPVmcbmLNZudOfQOcbRCCAgxYf_57(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_PEZTdLWPjWbAMCHYtsIZabliquLe_58(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_UzVqkwqMmsnMVrmgTvaiPTZgjzF_59(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_UnZeCgFPYKrcOECMvOoUKxJBUBt_60(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_wmHipiZGjflaEWstQdFEaNZigaYl_61(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_VwrVtHVuzenuKikGQAjSkTWmEjp_62(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_unscaledDeltaTime_63(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_unscaledTime_64(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_unscaledTimePrev_65(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_currentFrame_66(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_previousFrame_67(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_absFrame_68(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_bIpOcSHJOaNbSfzUQfNHLjsIhGU_69(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_SeKbgRoSSdxvLKKSFGtzsUJVIEq_70(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_jqUTJMOeCBaPwenidONjsgqqmhF_71(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_VxgfIwaEpxChiqoBUxMyAXuOvzah_72(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_yGfaFeDTgjQCiyfEnFKwcIrTEKK_73(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_bNWtItxSTpScLiOAwzhSFiRFjTf_74(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_NmaaMQBbnFbXDfscDfYsWbItnQyf_75(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_rMSAIWRRORgnlBNyMWCUdKjLpiE_76(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_oMtJsfQwbTEDGJiLlRXXMThiarq_77(),
	ReInput_t660C845029E93C5E11722938F53BCA3AED33D0BE_StaticFields::get_offset_of_XiZLABGROWZBxAnjRHXsnpGxypM_78(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4788 = { sizeof (ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5), -1, sizeof(ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4788[1] = 
{
	ConfigHelper_t31FF7505349528864589F8EEFF5AD862845BE7B5_StaticFields::get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4789 = { sizeof (ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62), -1, sizeof(ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4789[3] = 
{
	ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62_StaticFields::get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(),
	ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62::get_offset_of_polling_1(),
	ControllerHelper_t937589B90E36653120519D111871F0C16D15FF62::get_offset_of_conflictChecking_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4790 = { sizeof (PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A), -1, sizeof(PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4790[1] = 
{
	PollingHelper_t69EDA9E24833EACC807B10BC63FCE58E1090A29A_StaticFields::get_offset_of_FBhGsngGbrZoBDJQYQqNPKqLfYtD_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4791 = { sizeof (fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4791[12] = 
{
	fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7::get_offset_of_NmJKXjCDbsOGTRbOEEFMBEOEFUyb_4(),
	fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7::get_offset_of_wNPLhUIdVKUmVenimNoFrRATGNpb_5(),
	fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7::get_offset_of_zFIsOzpXzzpINimYjVuDQKEOGTTc_6(),
	fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7::get_offset_of_JevOncyqxoOkBpTLicUBwTcTtsT_7(),
	fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7::get_offset_of_QZQLDIvnrZfxWplZEbMmdPKplWd_8(),
	fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7::get_offset_of_PBuIBnaXXSvgQiBvViWXihECNCtd_9(),
	fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7::get_offset_of_vyhJoBzzJIRDJxrbRNNNcNlrGKHh_10(),
	fZCaSpiYNCNgnFNUGiLvGpZACyf_t91993F10241249BDBC6DC290E2918A437A9374D7::get_offset_of_OfxnXpujvLHQkTqseaVDlwbHgRPk_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4792 = { sizeof (HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4792[12] = 
{
	HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3::get_offset_of_ZAortNDnjLYCmiBrQFIAbhIaUKW_4(),
	HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3::get_offset_of_GgtWLeuiabgWtAgQzwEkLwUkXjN_5(),
	HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3::get_offset_of_rWAtWCEOCUwqgRWqpfXjLBoPbhz_6(),
	HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3::get_offset_of_RPCPVQgmBrYXUBVzyqlkkGxmhDj_7(),
	HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3::get_offset_of_TyVBFJKcdlQLriQmOXSFSSjNHmAy_8(),
	HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3::get_offset_of_wdcjvtQlCdWpBabpzfClHUOIozil_9(),
	HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3::get_offset_of_PalmDKfwodOEEgPtuWClpZscTYK_10(),
	HlvbTvUbAxQvmHufBhwyBwBtrbN_t482C2AAF4FEECE7C1A3926DC0394B8E6F8DA14E3::get_offset_of_ecNYhizHrPzvbIDpVcnLdKhcNIg_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4793 = { sizeof (KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4793[12] = 
{
	KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A::get_offset_of_lTyvbKnOHHjDDnAgOdgVBsNagGg_4(),
	KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A::get_offset_of_QsOtdmkbbUcFdBYiSXMdBnRcIse_5(),
	KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A::get_offset_of_agmUdgaQsfJNelkgWXLvTLQjDXJ_6(),
	KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A::get_offset_of_vMPBRfEyrhVVRoXYEggmOmsNgwE_7(),
	KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A::get_offset_of_wIbOHZvKyXtRqMWrkEzIZjzLZhH_8(),
	KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A::get_offset_of_JrhvhCqJGVNbbEyRvWwwCXHomvh_9(),
	KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A::get_offset_of_jMljDMzfOoDoFamndTeIjmmBoiX_10(),
	KXvVipweHDHxcvvOatzbaGgsaiq_tDAAA5B2F8EF4A52DEB8F69CEE355605D6F4E5E1A::get_offset_of_gHOYJROlCWbVZCUTzkskuGcWOOwd_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4794 = { sizeof (FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4794[12] = 
{
	FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C::get_offset_of_PzQGYPrYApVgoXjRjuPiTlFMZyE_4(),
	FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C::get_offset_of_YTCvkNnmkyGTCGciRjwpLcGOBXnA_5(),
	FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C::get_offset_of_llDwhCdWKQdCqvhwDksoltjnHsu_6(),
	FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C::get_offset_of_MSHruCSpYGCtMzaUxvkUhMgZMOj_7(),
	FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C::get_offset_of_FphORXFifLeArZSZKuegTdmnbRaH_8(),
	FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C::get_offset_of_NqyaCTdoRAFKxaezesiyPtRRgdrL_9(),
	FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C::get_offset_of_zjLLpEfaBZnjZybhxraZuerIOHG_10(),
	FOHMMgWyksTOjIiHxyFbSICwaxSa_t15EB7657A8D6116A6D853E97A4B1E3A9616AF37C::get_offset_of_hkYuNBmNDkjQxhwZKekXGXqQETee_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4795 = { sizeof (ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4795[10] = 
{
	ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_3(),
	ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409::get_offset_of_NpyXwbPIzajNDrTcugaTTmTZExF_4(),
	ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409::get_offset_of_uUSGzEijikuJhlrFleHVcqNiQgo_5(),
	ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409::get_offset_of_dNbZebdqcSHSsGKBBcUqAqSoFSV_6(),
	ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409::get_offset_of_mGffXviMGlJeIhFyFdNxAzlXIpkh_7(),
	ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409::get_offset_of_cUwhnomlYYSTFTkvAmfeTPMjhYg_8(),
	ijqERXGRHniaTDdDCtzRWsmjMR_t94736FCF93B9FD4EF65771F60CF00EBD1CF9B409::get_offset_of_dsEtkRNdKgFtWdqxPPmPRZFiOhH_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4796 = { sizeof (OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4796[8] = 
{
	OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3::get_offset_of_FlSdtuhphcTSwGLbunqQbPbehtJe_3(),
	OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3::get_offset_of_UYTcgCEVQPWpEYUiRTsrXmgiZC_4(),
	OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3::get_offset_of_jhKSkABuLssiABSYCANtFEpLhHLE_5(),
	OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(),
	OZshkjqdEACrnCchpgRRniBNcwyb_t6BED346CE91609EE0003762B5D5EF4BBAC7AC0E3::get_offset_of_aEgFTDWXaaQuVwylBpXzyyBGZtO_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4797 = { sizeof (EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4797[8] = 
{
	EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD::get_offset_of_SVZrVgRVsLzKoKBEyGIGeAEQgSya_3(),
	EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD::get_offset_of_cWJKTKYmtCVmdGGjjjsmZdZSCFF_4(),
	EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD::get_offset_of_zrIFwcCjOlaXNtcQfMloCLSKmpO_5(),
	EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(),
	EJyJzjWMFDUHsJVqdpaSMSnGCVd_tCA9DAA9EC7DC7F571EE1F0012304941AFAEBADBD::get_offset_of_mDkjXsweizhvYQUYrFNNZmYteSuc_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4798 = { sizeof (MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4798[8] = 
{
	MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1::get_offset_of_FCZIrNLlLYcBGYMRgfrfhMVkGLkj_3(),
	MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1::get_offset_of_AWruhtCZVwDWOojDzqRDFmWhfxmd_4(),
	MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1::get_offset_of_EnOXeeMmrBRllqVcdObtSABvTYP_5(),
	MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(),
	MheerqBtzTSurfAEdtQfcTTzwYpt_t47965B9CACBBEF4CDC46C4672367789080F2E4F1::get_offset_of_nHVPsdWojqlHSeauxUEHrQHhvOD_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4799 = { sizeof (JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4799[8] = 
{
	JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606::get_offset_of_NOArrsNdfKDShNFftfbPqYDzhpq_0(),
	JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606::get_offset_of_HiUOXdDRFcREZCtaKpUyyzdKBGv_1(),
	JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606::get_offset_of_eWRwTJwLvWfNoJTKpOqPGybGsFGN_2(),
	JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606::get_offset_of_CNPjPWWHaRddgvyGHodSiqLJICXJ_3(),
	JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606::get_offset_of_tGmxXaYmsCfLvIFUgwbFeArmFED_4(),
	JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606::get_offset_of_GGoLtFAWxRMAatYGucbJjAFCCUd_5(),
	JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606::get_offset_of_DKaFTWTMhFeLQHDExnDMRxZfmROL_6(),
	JbBmHbiFjdJeiKKOUPKVwERHdVe_tAA5A94F75C543CC3A12A465AC3D497423C31C606::get_offset_of_gxABcZAXsZhqMYDOKDZmBrUawSNB_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
