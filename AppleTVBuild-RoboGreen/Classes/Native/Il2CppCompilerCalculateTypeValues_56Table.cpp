﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Doozy.Engine.Events.BoolEvent
struct BoolEvent_tC2164AC1844ACBA9870E7D339F6FD57462A40D3B;
// Doozy.Engine.Message/OnMessageHandleDelegate
struct OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D;
// Doozy.Engine.Progress.ProgressEvent
struct ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB;
// Doozy.Engine.Progress.Progressor
struct Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A;
// Doozy.Engine.UI.Animation.UIAnimation
struct UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C;
// Doozy.Engine.UI.Base.UIAction
struct UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7;
// Doozy.Engine.UI.Base.UIContainer
struct UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B;
// Doozy.Engine.UI.Input.InputData
struct InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC;
// Doozy.Engine.UI.UIButton
struct UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527;
// Doozy.Engine.UI.UIButtonBehavior
struct UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB;
// Doozy.Engine.UI.UIButtonEvent
struct UIButtonEvent_t4D2122D7D8CDB28C0231D7429F5D12847FB55BD6;
// Doozy.Engine.UI.UIButtonLoopAnimation
struct UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45;
// Doozy.Engine.UI.UIButton[]
struct UIButtonU5BU5D_t2DA91AEF4743C142AF0D299149E2019C6EF0467F;
// Doozy.Engine.UI.UIDrawer
struct UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A;
// Doozy.Engine.UI.UIDrawerArrow
struct UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3;
// Doozy.Engine.UI.UIDrawerArrow/Holder
struct Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0;
// Doozy.Engine.UI.UIDrawerArrowAnimator
struct UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E;
// Doozy.Engine.UI.UIDrawerBehavior
struct UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843;
// Doozy.Engine.UI.UIDrawerContainer
struct UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C;
// Doozy.Engine.UI.UIDrawerEvent
struct UIDrawerEvent_t511B9CCB479D30EE935B3EA995DBB00D281721F6;
// Doozy.Engine.UI.UIPopup
struct UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228;
// Doozy.Engine.UI.UIPopupBehavior
struct UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57;
// Doozy.Engine.UI.UIPopupContentReferences
struct UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272;
// Doozy.Engine.UI.UIToggle
struct UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273;
// Doozy.Engine.UI.UIToggleBehavior
struct UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB;
// Doozy.Engine.UI.UIView
struct UIView_t65D38836246049951821D4E45B1C81947591EBDF;
// Doozy.Engine.UI.UIViewBehavior
struct UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522;
// Doozy.Engine.UI.UIView[]
struct UIViewU5BU5D_t1BD9626268B6429358BFEB30BFE62444CAC554BD;
// System.Action`2<Doozy.Engine.UI.UIButton,Doozy.Engine.UI.UIButtonBehaviorType>
struct Action_2_tCF266DB9CF64AAECAF08BE47546A164E53C9A5A9;
// System.Action`2<Doozy.Engine.UI.UIDrawer,Doozy.Engine.UI.UIDrawerBehaviorType>
struct Action_2_t01D7645AF504DBFECD9C187C54C4698EC9589290;
// System.Action`2<Doozy.Engine.UI.UIPopup,Doozy.Engine.UI.Animation.AnimationType>
struct Action_2_t8341B9F4DBB00FCCF2A0E62796BDFF83C3CF77E3;
// System.Action`2<Doozy.Engine.UI.UIView,Doozy.Engine.UI.UIViewBehaviorType>
struct Action_2_t1AAA92D44462571FF056D5AFE831F92AA6D8A9D7;
// System.Action`3<Doozy.Engine.UI.UIToggle,Doozy.Engine.UI.UIToggleState,Doozy.Engine.UI.UIToggleBehaviorType>
struct Action_3_t6A0377B8C081E230E32494EB2EDB35E6704E315B;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Delegate>>
struct Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA;
// System.Collections.Generic.List`1<Doozy.Engine.Events.AnimatorEvent>
struct List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5;
// System.Collections.Generic.List`1<Doozy.Engine.UI.UIButton>
struct List_1_t4CB7881968787919895A2E041031FDE0003C3B98;
// System.Collections.Generic.List`1<Doozy.Engine.UI.UICanvas>
struct List_1_t1CD2283A024EC384C8C14392E3D6EEE458CB3E10;
// System.Collections.Generic.List`1<Doozy.Engine.UI.UIDrawer>
struct List_1_tD1156032C8684C7E2CAC4ACFEF99AFB3E9F188A1;
// System.Collections.Generic.List`1<Doozy.Engine.UI.UIPopup>
struct List_1_t0F8A6581E8659C9A921426873F1825C44A02D639;
// System.Collections.Generic.List`1<Doozy.Engine.UI.UIPopupLink>
struct List_1_t2C69E4FC63314547C6DEA51918316C3DA8381618;
// System.Collections.Generic.List`1<Doozy.Engine.UI.UIPopupQueueData>
struct List_1_tBE6FAF4F305CCD5D19B8FFFFEE3E982DB3AF3EE6;
// System.Collections.Generic.List`1<Doozy.Engine.UI.UIToggle>
struct List_1_tD7C2010EE5E409831AF5DF4F17D958536A8323E6;
// System.Collections.Generic.List`1<Doozy.Engine.UI.UIView>
struct List_1_t7A5261DAB640C55051ADE8D918AAF06457CF2378;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Events.UnityAction>
struct List_1_t087DA65C3DC67D8149A4E3F313187F266030002F;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t18801B80DBA94AAA8DF21D6ED1C15CF944BCA6CC;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED;
// System.Func`2<Doozy.Engine.UI.UIButton,System.Boolean>
struct Func_2_t4CF078FA8EE2AA5581BFA52E270B8A7328123AFD;
// System.Func`2<Doozy.Engine.UI.UIPopupLink,System.String>
struct Func_2_t114475A33747B9BCB6E49ADCE6EB27F83DD81893;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MESSAGE_TE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_H
#define MESSAGE_TE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Message
struct  Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E  : public RuntimeObject
{
public:

public:
};

struct Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Delegate>> Doozy.Engine.Message::Handlers
	Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA * ___Handlers_1;
	// Doozy.Engine.Message_OnMessageHandleDelegate Doozy.Engine.Message::OnMessageHandle
	OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D * ___OnMessageHandle_2;

public:
	inline static int32_t get_offset_of_Handlers_1() { return static_cast<int32_t>(offsetof(Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_StaticFields, ___Handlers_1)); }
	inline Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA * get_Handlers_1() const { return ___Handlers_1; }
	inline Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA ** get_address_of_Handlers_1() { return &___Handlers_1; }
	inline void set_Handlers_1(Dictionary_2_t8F66534B8DB751D7F5ED5E86F5915314C9012DDA * value)
	{
		___Handlers_1 = value;
		Il2CppCodeGenWriteBarrier((&___Handlers_1), value);
	}

	inline static int32_t get_offset_of_OnMessageHandle_2() { return static_cast<int32_t>(offsetof(Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_StaticFields, ___OnMessageHandle_2)); }
	inline OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D * get_OnMessageHandle_2() const { return ___OnMessageHandle_2; }
	inline OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D ** get_address_of_OnMessageHandle_2() { return &___OnMessageHandle_2; }
	inline void set_OnMessageHandle_2(OnMessageHandleDelegate_tE979D029A260114F32638C86CA46E6A7D355AA4D * value)
	{
		___OnMessageHandle_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnMessageHandle_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGE_TE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E_H
#ifndef U3CU3EC_TB75A3DC1E24B6E257685C28755193DAF705521BA_H
#define U3CU3EC_TB75A3DC1E24B6E257685C28755193DAF705521BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButton_<>c
struct  U3CU3Ec_tB75A3DC1E24B6E257685C28755193DAF705521BA  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB75A3DC1E24B6E257685C28755193DAF705521BA_StaticFields
{
public:
	// Doozy.Engine.UI.UIButton_<>c Doozy.Engine.UI.UIButton_<>c::<>9
	U3CU3Ec_tB75A3DC1E24B6E257685C28755193DAF705521BA * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB75A3DC1E24B6E257685C28755193DAF705521BA_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB75A3DC1E24B6E257685C28755193DAF705521BA * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB75A3DC1E24B6E257685C28755193DAF705521BA ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB75A3DC1E24B6E257685C28755193DAF705521BA * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB75A3DC1E24B6E257685C28755193DAF705521BA_H
#ifndef U3CDESELECTBUTTONENUMERATORU3ED__100_T31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9_H
#define U3CDESELECTBUTTONENUMERATORU3ED__100_T31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButton_<DeselectButtonEnumerator>d__100
struct  U3CDeselectButtonEnumeratorU3Ed__100_t31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIButton_<DeselectButtonEnumerator>d__100::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIButton_<DeselectButtonEnumerator>d__100::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Doozy.Engine.UI.UIButton_<DeselectButtonEnumerator>d__100::delay
	float ___delay_2;
	// Doozy.Engine.UI.UIButton Doozy.Engine.UI.UIButton_<DeselectButtonEnumerator>d__100::<>4__this
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDeselectButtonEnumeratorU3Ed__100_t31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDeselectButtonEnumeratorU3Ed__100_t31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CDeselectButtonEnumeratorU3Ed__100_t31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDeselectButtonEnumeratorU3Ed__100_t31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9, ___U3CU3E4__this_3)); }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESELECTBUTTONENUMERATORU3ED__100_T31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9_H
#ifndef U3CDISABLEBUTTONBEHAVIORENUMERATORU3ED__103_T199055427FBD89ADC054A444AD42B1B513AEB13B_H
#define U3CDISABLEBUTTONBEHAVIORENUMERATORU3ED__103_T199055427FBD89ADC054A444AD42B1B513AEB13B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButton_<DisableButtonBehaviorEnumerator>d__103
struct  U3CDisableButtonBehaviorEnumeratorU3Ed__103_t199055427FBD89ADC054A444AD42B1B513AEB13B  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIButton_<DisableButtonBehaviorEnumerator>d__103::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIButton_<DisableButtonBehaviorEnumerator>d__103::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.UI.UIButtonBehavior Doozy.Engine.UI.UIButton_<DisableButtonBehaviorEnumerator>d__103::behavior
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * ___behavior_2;
	// Doozy.Engine.UI.UIButton Doozy.Engine.UI.UIButton_<DisableButtonBehaviorEnumerator>d__103::<>4__this
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDisableButtonBehaviorEnumeratorU3Ed__103_t199055427FBD89ADC054A444AD42B1B513AEB13B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDisableButtonBehaviorEnumeratorU3Ed__103_t199055427FBD89ADC054A444AD42B1B513AEB13B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_behavior_2() { return static_cast<int32_t>(offsetof(U3CDisableButtonBehaviorEnumeratorU3Ed__103_t199055427FBD89ADC054A444AD42B1B513AEB13B, ___behavior_2)); }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * get_behavior_2() const { return ___behavior_2; }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB ** get_address_of_behavior_2() { return &___behavior_2; }
	inline void set_behavior_2(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * value)
	{
		___behavior_2 = value;
		Il2CppCodeGenWriteBarrier((&___behavior_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDisableButtonBehaviorEnumeratorU3Ed__103_t199055427FBD89ADC054A444AD42B1B513AEB13B, ___U3CU3E4__this_3)); }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISABLEBUTTONBEHAVIORENUMERATORU3ED__103_T199055427FBD89ADC054A444AD42B1B513AEB13B_H
#ifndef U3CDISABLEBUTTONENUMERATORU3ED__102_T4A1FCECCABAD08BEA951F7C2B639A09F56202420_H
#define U3CDISABLEBUTTONENUMERATORU3ED__102_T4A1FCECCABAD08BEA951F7C2B639A09F56202420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButton_<DisableButtonEnumerator>d__102
struct  U3CDisableButtonEnumeratorU3Ed__102_t4A1FCECCABAD08BEA951F7C2B639A09F56202420  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIButton_<DisableButtonEnumerator>d__102::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIButton_<DisableButtonEnumerator>d__102::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Doozy.Engine.UI.UIButton_<DisableButtonEnumerator>d__102::duration
	float ___duration_2;
	// Doozy.Engine.UI.UIButton Doozy.Engine.UI.UIButton_<DisableButtonEnumerator>d__102::<>4__this
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDisableButtonEnumeratorU3Ed__102_t4A1FCECCABAD08BEA951F7C2B639A09F56202420, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDisableButtonEnumeratorU3Ed__102_t4A1FCECCABAD08BEA951F7C2B639A09F56202420, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(U3CDisableButtonEnumeratorU3Ed__102_t4A1FCECCABAD08BEA951F7C2B639A09F56202420, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDisableButtonEnumeratorU3Ed__102_t4A1FCECCABAD08BEA951F7C2B639A09F56202420, ___U3CU3E4__this_3)); }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISABLEBUTTONENUMERATORU3ED__102_T4A1FCECCABAD08BEA951F7C2B639A09F56202420_H
#ifndef U3CEXECUTEBUTTONBEHAVIORENUMERATORU3ED__101_T2902714DADF27EB596AC709538EA6BBEA4B61A57_H
#define U3CEXECUTEBUTTONBEHAVIORENUMERATORU3ED__101_T2902714DADF27EB596AC709538EA6BBEA4B61A57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButton_<ExecuteButtonBehaviorEnumerator>d__101
struct  U3CExecuteButtonBehaviorEnumeratorU3Ed__101_t2902714DADF27EB596AC709538EA6BBEA4B61A57  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIButton_<ExecuteButtonBehaviorEnumerator>d__101::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIButton_<ExecuteButtonBehaviorEnumerator>d__101::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.UI.UIButtonBehavior Doozy.Engine.UI.UIButton_<ExecuteButtonBehaviorEnumerator>d__101::behavior
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * ___behavior_2;
	// Doozy.Engine.UI.UIButton Doozy.Engine.UI.UIButton_<ExecuteButtonBehaviorEnumerator>d__101::<>4__this
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteButtonBehaviorEnumeratorU3Ed__101_t2902714DADF27EB596AC709538EA6BBEA4B61A57, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteButtonBehaviorEnumeratorU3Ed__101_t2902714DADF27EB596AC709538EA6BBEA4B61A57, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_behavior_2() { return static_cast<int32_t>(offsetof(U3CExecuteButtonBehaviorEnumeratorU3Ed__101_t2902714DADF27EB596AC709538EA6BBEA4B61A57, ___behavior_2)); }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * get_behavior_2() const { return ___behavior_2; }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB ** get_address_of_behavior_2() { return &___behavior_2; }
	inline void set_behavior_2(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * value)
	{
		___behavior_2 = value;
		Il2CppCodeGenWriteBarrier((&___behavior_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CExecuteButtonBehaviorEnumeratorU3Ed__101_t2902714DADF27EB596AC709538EA6BBEA4B61A57, ___U3CU3E4__this_3)); }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEBUTTONBEHAVIORENUMERATORU3ED__101_T2902714DADF27EB596AC709538EA6BBEA4B61A57_H
#ifndef U3CRUNONCLICKENUMERATORU3ED__104_T851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E_H
#define U3CRUNONCLICKENUMERATORU3ED__104_T851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButton_<RunOnClickEnumerator>d__104
struct  U3CRunOnClickEnumeratorU3Ed__104_t851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIButton_<RunOnClickEnumerator>d__104::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIButton_<RunOnClickEnumerator>d__104::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Boolean Doozy.Engine.UI.UIButton_<RunOnClickEnumerator>d__104::debug
	bool ___debug_2;
	// Doozy.Engine.UI.UIButton Doozy.Engine.UI.UIButton_<RunOnClickEnumerator>d__104::<>4__this
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRunOnClickEnumeratorU3Ed__104_t851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRunOnClickEnumeratorU3Ed__104_t851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_debug_2() { return static_cast<int32_t>(offsetof(U3CRunOnClickEnumeratorU3Ed__104_t851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E, ___debug_2)); }
	inline bool get_debug_2() const { return ___debug_2; }
	inline bool* get_address_of_debug_2() { return &___debug_2; }
	inline void set_debug_2(bool value)
	{
		___debug_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CRunOnClickEnumeratorU3Ed__104_t851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E, ___U3CU3E4__this_3)); }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNONCLICKENUMERATORU3ED__104_T851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E_H
#ifndef U3CRUNONLONGCLICKENUMERATORU3ED__105_T1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35_H
#define U3CRUNONLONGCLICKENUMERATORU3ED__105_T1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButton_<RunOnLongClickEnumerator>d__105
struct  U3CRunOnLongClickEnumeratorU3Ed__105_t1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIButton_<RunOnLongClickEnumerator>d__105::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIButton_<RunOnLongClickEnumerator>d__105::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Boolean Doozy.Engine.UI.UIButton_<RunOnLongClickEnumerator>d__105::debug
	bool ___debug_2;
	// Doozy.Engine.UI.UIButton Doozy.Engine.UI.UIButton_<RunOnLongClickEnumerator>d__105::<>4__this
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRunOnLongClickEnumeratorU3Ed__105_t1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRunOnLongClickEnumeratorU3Ed__105_t1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_debug_2() { return static_cast<int32_t>(offsetof(U3CRunOnLongClickEnumeratorU3Ed__105_t1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35, ___debug_2)); }
	inline bool get_debug_2() const { return ___debug_2; }
	inline bool* get_address_of_debug_2() { return &___debug_2; }
	inline void set_debug_2(bool value)
	{
		___debug_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CRunOnLongClickEnumeratorU3Ed__105_t1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35, ___U3CU3E4__this_3)); }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNONLONGCLICKENUMERATORU3ED__105_T1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35_H
#ifndef U3CINVOKECALLBACKSU3ED__63_TBA89A9C8709448600B85DC5223F373BA379DCB49_H
#define U3CINVOKECALLBACKSU3ED__63_TBA89A9C8709448600B85DC5223F373BA379DCB49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButtonBehavior_<InvokeCallbacks>d__63
struct  U3CInvokeCallbacksU3Ed__63_tBA89A9C8709448600B85DC5223F373BA379DCB49  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIButtonBehavior_<InvokeCallbacks>d__63::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIButtonBehavior_<InvokeCallbacks>d__63::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.UI.Animation.UIAnimation Doozy.Engine.UI.UIButtonBehavior_<InvokeCallbacks>d__63::animation
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * ___animation_2;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.UIButtonBehavior_<InvokeCallbacks>d__63::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_3;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.UIButtonBehavior_<InvokeCallbacks>d__63::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInvokeCallbacksU3Ed__63_tBA89A9C8709448600B85DC5223F373BA379DCB49, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInvokeCallbacksU3Ed__63_tBA89A9C8709448600B85DC5223F373BA379DCB49, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_animation_2() { return static_cast<int32_t>(offsetof(U3CInvokeCallbacksU3Ed__63_tBA89A9C8709448600B85DC5223F373BA379DCB49, ___animation_2)); }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * get_animation_2() const { return ___animation_2; }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C ** get_address_of_animation_2() { return &___animation_2; }
	inline void set_animation_2(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * value)
	{
		___animation_2 = value;
		Il2CppCodeGenWriteBarrier((&___animation_2), value);
	}

	inline static int32_t get_offset_of_onStartCallback_3() { return static_cast<int32_t>(offsetof(U3CInvokeCallbacksU3Ed__63_tBA89A9C8709448600B85DC5223F373BA379DCB49, ___onStartCallback_3)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_3() const { return ___onStartCallback_3; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_3() { return &___onStartCallback_3; }
	inline void set_onStartCallback_3(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_3), value);
	}

	inline static int32_t get_offset_of_onCompleteCallback_4() { return static_cast<int32_t>(offsetof(U3CInvokeCallbacksU3Ed__63_tBA89A9C8709448600B85DC5223F373BA379DCB49, ___onCompleteCallback_4)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_4() const { return ___onCompleteCallback_4; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_4() { return &___onCompleteCallback_4; }
	inline void set_onCompleteCallback_4(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVOKECALLBACKSU3ED__63_TBA89A9C8709448600B85DC5223F373BA379DCB49_H
#ifndef U3CU3EC__DISPLAYCLASS23_0_T4C9702962FEB0FE277DD2C647B71A44068DE2AB3_H
#define U3CU3EC__DISPLAYCLASS23_0_T4C9702962FEB0FE277DD2C647B71A44068DE2AB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UICanvas_<>c__DisplayClass23_0
struct  U3CU3Ec__DisplayClass23_0_t4C9702962FEB0FE277DD2C647B71A44068DE2AB3  : public RuntimeObject
{
public:
	// System.String Doozy.Engine.UI.UICanvas_<>c__DisplayClass23_0::canvasName
	String_t* ___canvasName_0;

public:
	inline static int32_t get_offset_of_canvasName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_t4C9702962FEB0FE277DD2C647B71A44068DE2AB3, ___canvasName_0)); }
	inline String_t* get_canvasName_0() const { return ___canvasName_0; }
	inline String_t** get_address_of_canvasName_0() { return &___canvasName_0; }
	inline void set_canvasName_0(String_t* value)
	{
		___canvasName_0 = value;
		Il2CppCodeGenWriteBarrier((&___canvasName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS23_0_T4C9702962FEB0FE277DD2C647B71A44068DE2AB3_H
#ifndef U3CU3EC__DISPLAYCLASS25_0_T9906805B29C8E53DAE16AAAA50C374E875B19C75_H
#define U3CU3EC__DISPLAYCLASS25_0_T9906805B29C8E53DAE16AAAA50C374E875B19C75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UICanvas_<>c__DisplayClass25_0
struct  U3CU3Ec__DisplayClass25_0_t9906805B29C8E53DAE16AAAA50C374E875B19C75  : public RuntimeObject
{
public:
	// System.String Doozy.Engine.UI.UICanvas_<>c__DisplayClass25_0::canvasName
	String_t* ___canvasName_0;

public:
	inline static int32_t get_offset_of_canvasName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_t9906805B29C8E53DAE16AAAA50C374E875B19C75, ___canvasName_0)); }
	inline String_t* get_canvasName_0() const { return ___canvasName_0; }
	inline String_t** get_address_of_canvasName_0() { return &___canvasName_0; }
	inline void set_canvasName_0(String_t* value)
	{
		___canvasName_0 = value;
		Il2CppCodeGenWriteBarrier((&___canvasName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS25_0_T9906805B29C8E53DAE16AAAA50C374E875B19C75_H
#ifndef U3CU3EC_T25A319B6A9D3D41CF9023A159940BCCFE25B4B8D_H
#define U3CU3EC_T25A319B6A9D3D41CF9023A159940BCCFE25B4B8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIDrawer_<>c
struct  U3CU3Ec_t25A319B6A9D3D41CF9023A159940BCCFE25B4B8D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t25A319B6A9D3D41CF9023A159940BCCFE25B4B8D_StaticFields
{
public:
	// Doozy.Engine.UI.UIDrawer_<>c Doozy.Engine.UI.UIDrawer_<>c::<>9
	U3CU3Ec_t25A319B6A9D3D41CF9023A159940BCCFE25B4B8D * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t25A319B6A9D3D41CF9023A159940BCCFE25B4B8D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t25A319B6A9D3D41CF9023A159940BCCFE25B4B8D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t25A319B6A9D3D41CF9023A159940BCCFE25B4B8D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t25A319B6A9D3D41CF9023A159940BCCFE25B4B8D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T25A319B6A9D3D41CF9023A159940BCCFE25B4B8D_H
#ifndef HOLDER_T3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0_H
#define HOLDER_T3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIDrawerArrow_Holder
struct  Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform Doozy.Engine.UI.UIDrawerArrow_Holder::Closed
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___Closed_0;
	// UnityEngine.RectTransform Doozy.Engine.UI.UIDrawerArrow_Holder::Opened
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___Opened_1;
	// UnityEngine.RectTransform Doozy.Engine.UI.UIDrawerArrow_Holder::Root
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___Root_2;

public:
	inline static int32_t get_offset_of_Closed_0() { return static_cast<int32_t>(offsetof(Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0, ___Closed_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_Closed_0() const { return ___Closed_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_Closed_0() { return &___Closed_0; }
	inline void set_Closed_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___Closed_0 = value;
		Il2CppCodeGenWriteBarrier((&___Closed_0), value);
	}

	inline static int32_t get_offset_of_Opened_1() { return static_cast<int32_t>(offsetof(Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0, ___Opened_1)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_Opened_1() const { return ___Opened_1; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_Opened_1() { return &___Opened_1; }
	inline void set_Opened_1(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___Opened_1 = value;
		Il2CppCodeGenWriteBarrier((&___Opened_1), value);
	}

	inline static int32_t get_offset_of_Root_2() { return static_cast<int32_t>(offsetof(Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0, ___Root_2)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_Root_2() const { return ___Root_2; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_Root_2() { return &___Root_2; }
	inline void set_Root_2(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___Root_2 = value;
		Il2CppCodeGenWriteBarrier((&___Root_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLDER_T3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0_H
#ifndef U3CU3EC_T72DAA4CE237962F7E1A2CA768974493280AC9938_H
#define U3CU3EC_T72DAA4CE237962F7E1A2CA768974493280AC9938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopup_<>c
struct  U3CU3Ec_t72DAA4CE237962F7E1A2CA768974493280AC9938  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t72DAA4CE237962F7E1A2CA768974493280AC9938_StaticFields
{
public:
	// Doozy.Engine.UI.UIPopup_<>c Doozy.Engine.UI.UIPopup_<>c::<>9
	U3CU3Ec_t72DAA4CE237962F7E1A2CA768974493280AC9938 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t72DAA4CE237962F7E1A2CA768974493280AC9938_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t72DAA4CE237962F7E1A2CA768974493280AC9938 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t72DAA4CE237962F7E1A2CA768974493280AC9938 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t72DAA4CE237962F7E1A2CA768974493280AC9938 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T72DAA4CE237962F7E1A2CA768974493280AC9938_H
#ifndef U3CEXECUTEHIDEDESELECTBUTTONENUMERATORU3ED__112_TC090EF8248ADF58D35CD2A5E566F5BC3FBFBE0AD_H
#define U3CEXECUTEHIDEDESELECTBUTTONENUMERATORU3ED__112_TC090EF8248ADF58D35CD2A5E566F5BC3FBFBE0AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopup_<ExecuteHideDeselectButtonEnumerator>d__112
struct  U3CExecuteHideDeselectButtonEnumeratorU3Ed__112_tC090EF8248ADF58D35CD2A5E566F5BC3FBFBE0AD  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIPopup_<ExecuteHideDeselectButtonEnumerator>d__112::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIPopup_<ExecuteHideDeselectButtonEnumerator>d__112::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.UI.UIPopup Doozy.Engine.UI.UIPopup_<ExecuteHideDeselectButtonEnumerator>d__112::<>4__this
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteHideDeselectButtonEnumeratorU3Ed__112_tC090EF8248ADF58D35CD2A5E566F5BC3FBFBE0AD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteHideDeselectButtonEnumeratorU3Ed__112_tC090EF8248ADF58D35CD2A5E566F5BC3FBFBE0AD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CExecuteHideDeselectButtonEnumeratorU3Ed__112_tC090EF8248ADF58D35CD2A5E566F5BC3FBFBE0AD, ___U3CU3E4__this_2)); }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEHIDEDESELECTBUTTONENUMERATORU3ED__112_TC090EF8248ADF58D35CD2A5E566F5BC3FBFBE0AD_H
#ifndef U3CEXECUTESHOWSELECTDESELECTBUTTONENUMERATORU3ED__111_TFE3B4EFC8AC290E0C166D2689226E958F8A3B496_H
#define U3CEXECUTESHOWSELECTDESELECTBUTTONENUMERATORU3ED__111_TFE3B4EFC8AC290E0C166D2689226E958F8A3B496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopup_<ExecuteShowSelectDeselectButtonEnumerator>d__111
struct  U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__111_tFE3B4EFC8AC290E0C166D2689226E958F8A3B496  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIPopup_<ExecuteShowSelectDeselectButtonEnumerator>d__111::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIPopup_<ExecuteShowSelectDeselectButtonEnumerator>d__111::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.UI.UIPopup Doozy.Engine.UI.UIPopup_<ExecuteShowSelectDeselectButtonEnumerator>d__111::<>4__this
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__111_tFE3B4EFC8AC290E0C166D2689226E958F8A3B496, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__111_tFE3B4EFC8AC290E0C166D2689226E958F8A3B496, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__111_tFE3B4EFC8AC290E0C166D2689226E958F8A3B496, ___U3CU3E4__this_2)); }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTESHOWSELECTDESELECTBUTTONENUMERATORU3ED__111_TFE3B4EFC8AC290E0C166D2689226E958F8A3B496_H
#ifndef U3CHIDEWITHDELAYENUMERATORU3ED__110_T557B6A11AA67E56EBE7698ADED5EFB93CD747AEF_H
#define U3CHIDEWITHDELAYENUMERATORU3ED__110_T557B6A11AA67E56EBE7698ADED5EFB93CD747AEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopup_<HideWithDelayEnumerator>d__110
struct  U3CHideWithDelayEnumeratorU3Ed__110_t557B6A11AA67E56EBE7698ADED5EFB93CD747AEF  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIPopup_<HideWithDelayEnumerator>d__110::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIPopup_<HideWithDelayEnumerator>d__110::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Doozy.Engine.UI.UIPopup_<HideWithDelayEnumerator>d__110::delay
	float ___delay_2;
	// Doozy.Engine.UI.UIPopup Doozy.Engine.UI.UIPopup_<HideWithDelayEnumerator>d__110::<>4__this
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CHideWithDelayEnumeratorU3Ed__110_t557B6A11AA67E56EBE7698ADED5EFB93CD747AEF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CHideWithDelayEnumeratorU3Ed__110_t557B6A11AA67E56EBE7698ADED5EFB93CD747AEF, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CHideWithDelayEnumeratorU3Ed__110_t557B6A11AA67E56EBE7698ADED5EFB93CD747AEF, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CHideWithDelayEnumeratorU3Ed__110_t557B6A11AA67E56EBE7698ADED5EFB93CD747AEF, ___U3CU3E4__this_3)); }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIDEWITHDELAYENUMERATORU3ED__110_T557B6A11AA67E56EBE7698ADED5EFB93CD747AEF_H
#ifndef U3CTRIGGERSHOWINNEXTFRAMEU3ED__107_T44A529641394EA26320518709F51947546F6A3D6_H
#define U3CTRIGGERSHOWINNEXTFRAMEU3ED__107_T44A529641394EA26320518709F51947546F6A3D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopup_<TriggerShowInNextFrame>d__107
struct  U3CTriggerShowInNextFrameU3Ed__107_t44A529641394EA26320518709F51947546F6A3D6  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIPopup_<TriggerShowInNextFrame>d__107::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIPopup_<TriggerShowInNextFrame>d__107::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Boolean Doozy.Engine.UI.UIPopup_<TriggerShowInNextFrame>d__107::instantAction
	bool ___instantAction_2;
	// Doozy.Engine.UI.UIPopup Doozy.Engine.UI.UIPopup_<TriggerShowInNextFrame>d__107::<>4__this
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTriggerShowInNextFrameU3Ed__107_t44A529641394EA26320518709F51947546F6A3D6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTriggerShowInNextFrameU3Ed__107_t44A529641394EA26320518709F51947546F6A3D6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_instantAction_2() { return static_cast<int32_t>(offsetof(U3CTriggerShowInNextFrameU3Ed__107_t44A529641394EA26320518709F51947546F6A3D6, ___instantAction_2)); }
	inline bool get_instantAction_2() const { return ___instantAction_2; }
	inline bool* get_address_of_instantAction_2() { return &___instantAction_2; }
	inline void set_instantAction_2(bool value)
	{
		___instantAction_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CTriggerShowInNextFrameU3Ed__107_t44A529641394EA26320518709F51947546F6A3D6, ___U3CU3E4__this_3)); }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRIGGERSHOWINNEXTFRAMEU3ED__107_T44A529641394EA26320518709F51947546F6A3D6_H
#ifndef UIPOPUPBEHAVIOR_T5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57_H
#define UIPOPUPBEHAVIOR_T5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopupBehavior
struct  UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.Animation.UIAnimation Doozy.Engine.UI.UIPopupBehavior::Animation
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * ___Animation_2;
	// System.Boolean Doozy.Engine.UI.UIPopupBehavior::LoadSelectedPresetAtRuntime
	bool ___LoadSelectedPresetAtRuntime_3;
	// System.Boolean Doozy.Engine.UI.UIPopupBehavior::InstantAnimation
	bool ___InstantAnimation_4;
	// Doozy.Engine.UI.Base.UIAction Doozy.Engine.UI.UIPopupBehavior::OnFinished
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * ___OnFinished_5;
	// Doozy.Engine.UI.Base.UIAction Doozy.Engine.UI.UIPopupBehavior::OnStart
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * ___OnStart_6;
	// System.String Doozy.Engine.UI.UIPopupBehavior::PresetCategory
	String_t* ___PresetCategory_7;
	// System.String Doozy.Engine.UI.UIPopupBehavior::PresetName
	String_t* ___PresetName_8;
	// System.Single Doozy.Engine.UI.UIPopupBehavior::m_progress
	float ___m_progress_9;

public:
	inline static int32_t get_offset_of_Animation_2() { return static_cast<int32_t>(offsetof(UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57, ___Animation_2)); }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * get_Animation_2() const { return ___Animation_2; }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C ** get_address_of_Animation_2() { return &___Animation_2; }
	inline void set_Animation_2(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * value)
	{
		___Animation_2 = value;
		Il2CppCodeGenWriteBarrier((&___Animation_2), value);
	}

	inline static int32_t get_offset_of_LoadSelectedPresetAtRuntime_3() { return static_cast<int32_t>(offsetof(UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57, ___LoadSelectedPresetAtRuntime_3)); }
	inline bool get_LoadSelectedPresetAtRuntime_3() const { return ___LoadSelectedPresetAtRuntime_3; }
	inline bool* get_address_of_LoadSelectedPresetAtRuntime_3() { return &___LoadSelectedPresetAtRuntime_3; }
	inline void set_LoadSelectedPresetAtRuntime_3(bool value)
	{
		___LoadSelectedPresetAtRuntime_3 = value;
	}

	inline static int32_t get_offset_of_InstantAnimation_4() { return static_cast<int32_t>(offsetof(UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57, ___InstantAnimation_4)); }
	inline bool get_InstantAnimation_4() const { return ___InstantAnimation_4; }
	inline bool* get_address_of_InstantAnimation_4() { return &___InstantAnimation_4; }
	inline void set_InstantAnimation_4(bool value)
	{
		___InstantAnimation_4 = value;
	}

	inline static int32_t get_offset_of_OnFinished_5() { return static_cast<int32_t>(offsetof(UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57, ___OnFinished_5)); }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * get_OnFinished_5() const { return ___OnFinished_5; }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 ** get_address_of_OnFinished_5() { return &___OnFinished_5; }
	inline void set_OnFinished_5(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * value)
	{
		___OnFinished_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinished_5), value);
	}

	inline static int32_t get_offset_of_OnStart_6() { return static_cast<int32_t>(offsetof(UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57, ___OnStart_6)); }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * get_OnStart_6() const { return ___OnStart_6; }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 ** get_address_of_OnStart_6() { return &___OnStart_6; }
	inline void set_OnStart_6(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * value)
	{
		___OnStart_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnStart_6), value);
	}

	inline static int32_t get_offset_of_PresetCategory_7() { return static_cast<int32_t>(offsetof(UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57, ___PresetCategory_7)); }
	inline String_t* get_PresetCategory_7() const { return ___PresetCategory_7; }
	inline String_t** get_address_of_PresetCategory_7() { return &___PresetCategory_7; }
	inline void set_PresetCategory_7(String_t* value)
	{
		___PresetCategory_7 = value;
		Il2CppCodeGenWriteBarrier((&___PresetCategory_7), value);
	}

	inline static int32_t get_offset_of_PresetName_8() { return static_cast<int32_t>(offsetof(UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57, ___PresetName_8)); }
	inline String_t* get_PresetName_8() const { return ___PresetName_8; }
	inline String_t** get_address_of_PresetName_8() { return &___PresetName_8; }
	inline void set_PresetName_8(String_t* value)
	{
		___PresetName_8 = value;
		Il2CppCodeGenWriteBarrier((&___PresetName_8), value);
	}

	inline static int32_t get_offset_of_m_progress_9() { return static_cast<int32_t>(offsetof(UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57, ___m_progress_9)); }
	inline float get_m_progress_9() const { return ___m_progress_9; }
	inline float* get_address_of_m_progress_9() { return &___m_progress_9; }
	inline void set_m_progress_9(float value)
	{
		___m_progress_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOPUPBEHAVIOR_T5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57_H
#ifndef UIPOPUPCONTENTDATA_TB9781C2614518D6288C694A4B0352D258126DA40_H
#define UIPOPUPCONTENTDATA_TB9781C2614518D6288C694A4B0352D258126DA40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopupContentData
struct  UIPopupContentData_tB9781C2614518D6288C694A4B0352D258126DA40  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Events.UnityAction> Doozy.Engine.UI.UIPopupContentData::ButtonCallbacks
	List_1_t087DA65C3DC67D8149A4E3F313187F266030002F * ___ButtonCallbacks_0;
	// System.Collections.Generic.List`1<System.String> Doozy.Engine.UI.UIPopupContentData::ButtonLabels
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___ButtonLabels_1;
	// System.Collections.Generic.List`1<System.String> Doozy.Engine.UI.UIPopupContentData::ButtonNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___ButtonNames_2;
	// System.Collections.Generic.List`1<System.String> Doozy.Engine.UI.UIPopupContentData::Labels
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___Labels_3;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Doozy.Engine.UI.UIPopupContentData::Sprites
	List_1_t18801B80DBA94AAA8DF21D6ED1C15CF944BCA6CC * ___Sprites_4;

public:
	inline static int32_t get_offset_of_ButtonCallbacks_0() { return static_cast<int32_t>(offsetof(UIPopupContentData_tB9781C2614518D6288C694A4B0352D258126DA40, ___ButtonCallbacks_0)); }
	inline List_1_t087DA65C3DC67D8149A4E3F313187F266030002F * get_ButtonCallbacks_0() const { return ___ButtonCallbacks_0; }
	inline List_1_t087DA65C3DC67D8149A4E3F313187F266030002F ** get_address_of_ButtonCallbacks_0() { return &___ButtonCallbacks_0; }
	inline void set_ButtonCallbacks_0(List_1_t087DA65C3DC67D8149A4E3F313187F266030002F * value)
	{
		___ButtonCallbacks_0 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonCallbacks_0), value);
	}

	inline static int32_t get_offset_of_ButtonLabels_1() { return static_cast<int32_t>(offsetof(UIPopupContentData_tB9781C2614518D6288C694A4B0352D258126DA40, ___ButtonLabels_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_ButtonLabels_1() const { return ___ButtonLabels_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_ButtonLabels_1() { return &___ButtonLabels_1; }
	inline void set_ButtonLabels_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___ButtonLabels_1 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonLabels_1), value);
	}

	inline static int32_t get_offset_of_ButtonNames_2() { return static_cast<int32_t>(offsetof(UIPopupContentData_tB9781C2614518D6288C694A4B0352D258126DA40, ___ButtonNames_2)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_ButtonNames_2() const { return ___ButtonNames_2; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_ButtonNames_2() { return &___ButtonNames_2; }
	inline void set_ButtonNames_2(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___ButtonNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonNames_2), value);
	}

	inline static int32_t get_offset_of_Labels_3() { return static_cast<int32_t>(offsetof(UIPopupContentData_tB9781C2614518D6288C694A4B0352D258126DA40, ___Labels_3)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_Labels_3() const { return ___Labels_3; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_Labels_3() { return &___Labels_3; }
	inline void set_Labels_3(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___Labels_3 = value;
		Il2CppCodeGenWriteBarrier((&___Labels_3), value);
	}

	inline static int32_t get_offset_of_Sprites_4() { return static_cast<int32_t>(offsetof(UIPopupContentData_tB9781C2614518D6288C694A4B0352D258126DA40, ___Sprites_4)); }
	inline List_1_t18801B80DBA94AAA8DF21D6ED1C15CF944BCA6CC * get_Sprites_4() const { return ___Sprites_4; }
	inline List_1_t18801B80DBA94AAA8DF21D6ED1C15CF944BCA6CC ** get_address_of_Sprites_4() { return &___Sprites_4; }
	inline void set_Sprites_4(List_1_t18801B80DBA94AAA8DF21D6ED1C15CF944BCA6CC * value)
	{
		___Sprites_4 = value;
		Il2CppCodeGenWriteBarrier((&___Sprites_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOPUPCONTENTDATA_TB9781C2614518D6288C694A4B0352D258126DA40_H
#ifndef UIPOPUPCONTENTREFERENCES_TD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272_H
#define UIPOPUPCONTENTREFERENCES_TD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopupContentReferences
struct  UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Doozy.Engine.UI.UIButton> Doozy.Engine.UI.UIPopupContentReferences::Buttons
	List_1_t4CB7881968787919895A2E041031FDE0003C3B98 * ___Buttons_0;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Doozy.Engine.UI.UIPopupContentReferences::Images
	List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * ___Images_1;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Doozy.Engine.UI.UIPopupContentReferences::Labels
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___Labels_2;

public:
	inline static int32_t get_offset_of_Buttons_0() { return static_cast<int32_t>(offsetof(UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272, ___Buttons_0)); }
	inline List_1_t4CB7881968787919895A2E041031FDE0003C3B98 * get_Buttons_0() const { return ___Buttons_0; }
	inline List_1_t4CB7881968787919895A2E041031FDE0003C3B98 ** get_address_of_Buttons_0() { return &___Buttons_0; }
	inline void set_Buttons_0(List_1_t4CB7881968787919895A2E041031FDE0003C3B98 * value)
	{
		___Buttons_0 = value;
		Il2CppCodeGenWriteBarrier((&___Buttons_0), value);
	}

	inline static int32_t get_offset_of_Images_1() { return static_cast<int32_t>(offsetof(UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272, ___Images_1)); }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * get_Images_1() const { return ___Images_1; }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED ** get_address_of_Images_1() { return &___Images_1; }
	inline void set_Images_1(List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * value)
	{
		___Images_1 = value;
		Il2CppCodeGenWriteBarrier((&___Images_1), value);
	}

	inline static int32_t get_offset_of_Labels_2() { return static_cast<int32_t>(offsetof(UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272, ___Labels_2)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_Labels_2() const { return ___Labels_2; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_Labels_2() { return &___Labels_2; }
	inline void set_Labels_2(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___Labels_2 = value;
		Il2CppCodeGenWriteBarrier((&___Labels_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOPUPCONTENTREFERENCES_TD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272_H
#ifndef U3CU3EC_T8A0C59A22CCA582D1211C88B397689A059E9E56C_H
#define U3CU3EC_T8A0C59A22CCA582D1211C88B397689A059E9E56C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopupDatabase_<>c
struct  U3CU3Ec_t8A0C59A22CCA582D1211C88B397689A059E9E56C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8A0C59A22CCA582D1211C88B397689A059E9E56C_StaticFields
{
public:
	// Doozy.Engine.UI.UIPopupDatabase_<>c Doozy.Engine.UI.UIPopupDatabase_<>c::<>9
	U3CU3Ec_t8A0C59A22CCA582D1211C88B397689A059E9E56C * ___U3CU3E9_0;
	// System.Func`2<Doozy.Engine.UI.UIPopupLink,System.String> Doozy.Engine.UI.UIPopupDatabase_<>c::<>9__21_0
	Func_2_t114475A33747B9BCB6E49ADCE6EB27F83DD81893 * ___U3CU3E9__21_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8A0C59A22CCA582D1211C88B397689A059E9E56C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8A0C59A22CCA582D1211C88B397689A059E9E56C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8A0C59A22CCA582D1211C88B397689A059E9E56C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8A0C59A22CCA582D1211C88B397689A059E9E56C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__21_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8A0C59A22CCA582D1211C88B397689A059E9E56C_StaticFields, ___U3CU3E9__21_0_1)); }
	inline Func_2_t114475A33747B9BCB6E49ADCE6EB27F83DD81893 * get_U3CU3E9__21_0_1() const { return ___U3CU3E9__21_0_1; }
	inline Func_2_t114475A33747B9BCB6E49ADCE6EB27F83DD81893 ** get_address_of_U3CU3E9__21_0_1() { return &___U3CU3E9__21_0_1; }
	inline void set_U3CU3E9__21_0_1(Func_2_t114475A33747B9BCB6E49ADCE6EB27F83DD81893 * value)
	{
		___U3CU3E9__21_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__21_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T8A0C59A22CCA582D1211C88B397689A059E9E56C_H
#ifndef UIPOPUPQUEUEDATA_T7BB621627113FAFAFE1106E59C2781AD81158945_H
#define UIPOPUPQUEUEDATA_T7BB621627113FAFAFE1106E59C2781AD81158945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopupQueueData
struct  UIPopupQueueData_t7BB621627113FAFAFE1106E59C2781AD81158945  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.UIPopup Doozy.Engine.UI.UIPopupQueueData::Popup
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * ___Popup_0;
	// System.String Doozy.Engine.UI.UIPopupQueueData::PopupName
	String_t* ___PopupName_1;
	// System.Boolean Doozy.Engine.UI.UIPopupQueueData::InstantAction
	bool ___InstantAction_2;

public:
	inline static int32_t get_offset_of_Popup_0() { return static_cast<int32_t>(offsetof(UIPopupQueueData_t7BB621627113FAFAFE1106E59C2781AD81158945, ___Popup_0)); }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * get_Popup_0() const { return ___Popup_0; }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 ** get_address_of_Popup_0() { return &___Popup_0; }
	inline void set_Popup_0(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * value)
	{
		___Popup_0 = value;
		Il2CppCodeGenWriteBarrier((&___Popup_0), value);
	}

	inline static int32_t get_offset_of_PopupName_1() { return static_cast<int32_t>(offsetof(UIPopupQueueData_t7BB621627113FAFAFE1106E59C2781AD81158945, ___PopupName_1)); }
	inline String_t* get_PopupName_1() const { return ___PopupName_1; }
	inline String_t** get_address_of_PopupName_1() { return &___PopupName_1; }
	inline void set_PopupName_1(String_t* value)
	{
		___PopupName_1 = value;
		Il2CppCodeGenWriteBarrier((&___PopupName_1), value);
	}

	inline static int32_t get_offset_of_InstantAction_2() { return static_cast<int32_t>(offsetof(UIPopupQueueData_t7BB621627113FAFAFE1106E59C2781AD81158945, ___InstantAction_2)); }
	inline bool get_InstantAction_2() const { return ___InstantAction_2; }
	inline bool* get_address_of_InstantAction_2() { return &___InstantAction_2; }
	inline void set_InstantAction_2(bool value)
	{
		___InstantAction_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOPUPQUEUEDATA_T7BB621627113FAFAFE1106E59C2781AD81158945_H
#ifndef U3CU3EC_T9012F08FC47EB8382D5E4F400955F83D2AB04423_H
#define U3CU3EC_T9012F08FC47EB8382D5E4F400955F83D2AB04423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggle_<>c
struct  U3CU3Ec_t9012F08FC47EB8382D5E4F400955F83D2AB04423  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t9012F08FC47EB8382D5E4F400955F83D2AB04423_StaticFields
{
public:
	// Doozy.Engine.UI.UIToggle_<>c Doozy.Engine.UI.UIToggle_<>c::<>9
	U3CU3Ec_t9012F08FC47EB8382D5E4F400955F83D2AB04423 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9012F08FC47EB8382D5E4F400955F83D2AB04423_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t9012F08FC47EB8382D5E4F400955F83D2AB04423 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t9012F08FC47EB8382D5E4F400955F83D2AB04423 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t9012F08FC47EB8382D5E4F400955F83D2AB04423 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T9012F08FC47EB8382D5E4F400955F83D2AB04423_H
#ifndef U3CDESELECTTOGGLEENUMERATORU3ED__67_TF4419416728077A9CB1B098174095FB66EA2B1FE_H
#define U3CDESELECTTOGGLEENUMERATORU3ED__67_TF4419416728077A9CB1B098174095FB66EA2B1FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggle_<DeselectToggleEnumerator>d__67
struct  U3CDeselectToggleEnumeratorU3Ed__67_tF4419416728077A9CB1B098174095FB66EA2B1FE  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIToggle_<DeselectToggleEnumerator>d__67::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIToggle_<DeselectToggleEnumerator>d__67::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Doozy.Engine.UI.UIToggle_<DeselectToggleEnumerator>d__67::delay
	float ___delay_2;
	// Doozy.Engine.UI.UIToggle Doozy.Engine.UI.UIToggle_<DeselectToggleEnumerator>d__67::<>4__this
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDeselectToggleEnumeratorU3Ed__67_tF4419416728077A9CB1B098174095FB66EA2B1FE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDeselectToggleEnumeratorU3Ed__67_tF4419416728077A9CB1B098174095FB66EA2B1FE, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CDeselectToggleEnumeratorU3Ed__67_tF4419416728077A9CB1B098174095FB66EA2B1FE, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDeselectToggleEnumeratorU3Ed__67_tF4419416728077A9CB1B098174095FB66EA2B1FE, ___U3CU3E4__this_3)); }
	inline UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESELECTTOGGLEENUMERATORU3ED__67_TF4419416728077A9CB1B098174095FB66EA2B1FE_H
#ifndef U3CDISABLETOGGLEBEHAVIORENUMERATORU3ED__70_T83B06053B430B3A4518E32C9FA8DD0797088FC14_H
#define U3CDISABLETOGGLEBEHAVIORENUMERATORU3ED__70_T83B06053B430B3A4518E32C9FA8DD0797088FC14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggle_<DisableToggleBehaviorEnumerator>d__70
struct  U3CDisableToggleBehaviorEnumeratorU3Ed__70_t83B06053B430B3A4518E32C9FA8DD0797088FC14  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIToggle_<DisableToggleBehaviorEnumerator>d__70::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIToggle_<DisableToggleBehaviorEnumerator>d__70::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.UI.UIToggleBehavior Doozy.Engine.UI.UIToggle_<DisableToggleBehaviorEnumerator>d__70::behavior
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * ___behavior_2;
	// Doozy.Engine.UI.UIToggle Doozy.Engine.UI.UIToggle_<DisableToggleBehaviorEnumerator>d__70::<>4__this
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDisableToggleBehaviorEnumeratorU3Ed__70_t83B06053B430B3A4518E32C9FA8DD0797088FC14, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDisableToggleBehaviorEnumeratorU3Ed__70_t83B06053B430B3A4518E32C9FA8DD0797088FC14, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_behavior_2() { return static_cast<int32_t>(offsetof(U3CDisableToggleBehaviorEnumeratorU3Ed__70_t83B06053B430B3A4518E32C9FA8DD0797088FC14, ___behavior_2)); }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * get_behavior_2() const { return ___behavior_2; }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB ** get_address_of_behavior_2() { return &___behavior_2; }
	inline void set_behavior_2(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * value)
	{
		___behavior_2 = value;
		Il2CppCodeGenWriteBarrier((&___behavior_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDisableToggleBehaviorEnumeratorU3Ed__70_t83B06053B430B3A4518E32C9FA8DD0797088FC14, ___U3CU3E4__this_3)); }
	inline UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISABLETOGGLEBEHAVIORENUMERATORU3ED__70_T83B06053B430B3A4518E32C9FA8DD0797088FC14_H
#ifndef U3CDISABLETOGGLEENUMERATORU3ED__69_T462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6_H
#define U3CDISABLETOGGLEENUMERATORU3ED__69_T462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggle_<DisableToggleEnumerator>d__69
struct  U3CDisableToggleEnumeratorU3Ed__69_t462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIToggle_<DisableToggleEnumerator>d__69::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIToggle_<DisableToggleEnumerator>d__69::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Doozy.Engine.UI.UIToggle_<DisableToggleEnumerator>d__69::duration
	float ___duration_2;
	// Doozy.Engine.UI.UIToggle Doozy.Engine.UI.UIToggle_<DisableToggleEnumerator>d__69::<>4__this
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDisableToggleEnumeratorU3Ed__69_t462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDisableToggleEnumeratorU3Ed__69_t462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(U3CDisableToggleEnumeratorU3Ed__69_t462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDisableToggleEnumeratorU3Ed__69_t462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6, ___U3CU3E4__this_3)); }
	inline UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISABLETOGGLEENUMERATORU3ED__69_T462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6_H
#ifndef U3CEXECUTETOGGLEBEHAVIORENUMERATORU3ED__68_TFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268_H
#define U3CEXECUTETOGGLEBEHAVIORENUMERATORU3ED__68_TFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggle_<ExecuteToggleBehaviorEnumerator>d__68
struct  U3CExecuteToggleBehaviorEnumeratorU3Ed__68_tFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIToggle_<ExecuteToggleBehaviorEnumerator>d__68::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIToggle_<ExecuteToggleBehaviorEnumerator>d__68::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.UI.UIToggleBehavior Doozy.Engine.UI.UIToggle_<ExecuteToggleBehaviorEnumerator>d__68::behavior
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * ___behavior_2;
	// Doozy.Engine.UI.UIToggle Doozy.Engine.UI.UIToggle_<ExecuteToggleBehaviorEnumerator>d__68::<>4__this
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteToggleBehaviorEnumeratorU3Ed__68_tFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteToggleBehaviorEnumeratorU3Ed__68_tFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_behavior_2() { return static_cast<int32_t>(offsetof(U3CExecuteToggleBehaviorEnumeratorU3Ed__68_tFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268, ___behavior_2)); }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * get_behavior_2() const { return ___behavior_2; }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB ** get_address_of_behavior_2() { return &___behavior_2; }
	inline void set_behavior_2(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * value)
	{
		___behavior_2 = value;
		Il2CppCodeGenWriteBarrier((&___behavior_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CExecuteToggleBehaviorEnumeratorU3Ed__68_tFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268, ___U3CU3E4__this_3)); }
	inline UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTETOGGLEBEHAVIORENUMERATORU3ED__68_TFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268_H
#ifndef U3CU3EC__DISPLAYCLASS56_0_TB2BE4CEDDE6B05AEF1D6C5D63BB942FF8B58F2F7_H
#define U3CU3EC__DISPLAYCLASS56_0_TB2BE4CEDDE6B05AEF1D6C5D63BB942FF8B58F2F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggleBehavior_<>c__DisplayClass56_0
struct  U3CU3Ec__DisplayClass56_0_tB2BE4CEDDE6B05AEF1D6C5D63BB942FF8B58F2F7  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.UIToggle Doozy.Engine.UI.UIToggleBehavior_<>c__DisplayClass56_0::toggle
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * ___toggle_0;
	// Doozy.Engine.UI.UIToggleBehavior Doozy.Engine.UI.UIToggleBehavior_<>c__DisplayClass56_0::<>4__this
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_toggle_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_0_tB2BE4CEDDE6B05AEF1D6C5D63BB942FF8B58F2F7, ___toggle_0)); }
	inline UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * get_toggle_0() const { return ___toggle_0; }
	inline UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 ** get_address_of_toggle_0() { return &___toggle_0; }
	inline void set_toggle_0(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * value)
	{
		___toggle_0 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_0_tB2BE4CEDDE6B05AEF1D6C5D63BB942FF8B58F2F7, ___U3CU3E4__this_1)); }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS56_0_TB2BE4CEDDE6B05AEF1D6C5D63BB942FF8B58F2F7_H
#ifndef U3CINVOKECALLBACKAFTERDELAYU3ED__62_TD8A3B6A09EB18CA168FD5C126F7CC665158185FD_H
#define U3CINVOKECALLBACKAFTERDELAYU3ED__62_TD8A3B6A09EB18CA168FD5C126F7CC665158185FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggleBehavior_<InvokeCallbackAfterDelay>d__62
struct  U3CInvokeCallbackAfterDelayU3Ed__62_tD8A3B6A09EB18CA168FD5C126F7CC665158185FD  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIToggleBehavior_<InvokeCallbackAfterDelay>d__62::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIToggleBehavior_<InvokeCallbackAfterDelay>d__62::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.UIToggleBehavior_<InvokeCallbackAfterDelay>d__62::callback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___callback_2;
	// System.Single Doozy.Engine.UI.UIToggleBehavior_<InvokeCallbackAfterDelay>d__62::delay
	float ___delay_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInvokeCallbackAfterDelayU3Ed__62_tD8A3B6A09EB18CA168FD5C126F7CC665158185FD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInvokeCallbackAfterDelayU3Ed__62_tD8A3B6A09EB18CA168FD5C126F7CC665158185FD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CInvokeCallbackAfterDelayU3Ed__62_tD8A3B6A09EB18CA168FD5C126F7CC665158185FD, ___callback_2)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_callback_2() const { return ___callback_2; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(U3CInvokeCallbackAfterDelayU3Ed__62_tD8A3B6A09EB18CA168FD5C126F7CC665158185FD, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVOKECALLBACKAFTERDELAYU3ED__62_TD8A3B6A09EB18CA168FD5C126F7CC665158185FD_H
#ifndef U3CINVOKECALLBACKSU3ED__61_T2AA3B7C663BD5E43DE249528D74DDF6499B9D04C_H
#define U3CINVOKECALLBACKSU3ED__61_T2AA3B7C663BD5E43DE249528D74DDF6499B9D04C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggleBehavior_<InvokeCallbacks>d__61
struct  U3CInvokeCallbacksU3Ed__61_t2AA3B7C663BD5E43DE249528D74DDF6499B9D04C  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIToggleBehavior_<InvokeCallbacks>d__61::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIToggleBehavior_<InvokeCallbacks>d__61::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.UI.Animation.UIAnimation Doozy.Engine.UI.UIToggleBehavior_<InvokeCallbacks>d__61::animation
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * ___animation_2;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.UIToggleBehavior_<InvokeCallbacks>d__61::onStartCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onStartCallback_3;
	// UnityEngine.Events.UnityAction Doozy.Engine.UI.UIToggleBehavior_<InvokeCallbacks>d__61::onCompleteCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onCompleteCallback_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInvokeCallbacksU3Ed__61_t2AA3B7C663BD5E43DE249528D74DDF6499B9D04C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInvokeCallbacksU3Ed__61_t2AA3B7C663BD5E43DE249528D74DDF6499B9D04C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_animation_2() { return static_cast<int32_t>(offsetof(U3CInvokeCallbacksU3Ed__61_t2AA3B7C663BD5E43DE249528D74DDF6499B9D04C, ___animation_2)); }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * get_animation_2() const { return ___animation_2; }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C ** get_address_of_animation_2() { return &___animation_2; }
	inline void set_animation_2(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * value)
	{
		___animation_2 = value;
		Il2CppCodeGenWriteBarrier((&___animation_2), value);
	}

	inline static int32_t get_offset_of_onStartCallback_3() { return static_cast<int32_t>(offsetof(U3CInvokeCallbacksU3Ed__61_t2AA3B7C663BD5E43DE249528D74DDF6499B9D04C, ___onStartCallback_3)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onStartCallback_3() const { return ___onStartCallback_3; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onStartCallback_3() { return &___onStartCallback_3; }
	inline void set_onStartCallback_3(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onStartCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___onStartCallback_3), value);
	}

	inline static int32_t get_offset_of_onCompleteCallback_4() { return static_cast<int32_t>(offsetof(U3CInvokeCallbacksU3Ed__61_t2AA3B7C663BD5E43DE249528D74DDF6499B9D04C, ___onCompleteCallback_4)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onCompleteCallback_4() const { return ___onCompleteCallback_4; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onCompleteCallback_4() { return &___onCompleteCallback_4; }
	inline void set_onCompleteCallback_4(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onCompleteCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteCallback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVOKECALLBACKSU3ED__61_T2AA3B7C663BD5E43DE249528D74DDF6499B9D04C_H
#ifndef U3CU3EC_T14BFBF58E1978D2CE47DD45B594E83527C0AEBFB_H
#define U3CU3EC_T14BFBF58E1978D2CE47DD45B594E83527C0AEBFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIView_<>c
struct  U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB_StaticFields
{
public:
	// Doozy.Engine.UI.UIView_<>c Doozy.Engine.UI.UIView_<>c::<>9
	U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB * ___U3CU3E9_0;
	// System.Func`2<Doozy.Engine.UI.UIButton,System.Boolean> Doozy.Engine.UI.UIView_<>c::<>9__93_0
	Func_2_t4CF078FA8EE2AA5581BFA52E270B8A7328123AFD * ___U3CU3E9__93_0_1;
	// System.Func`2<Doozy.Engine.UI.UIButton,System.Boolean> Doozy.Engine.UI.UIView_<>c::<>9__93_1
	Func_2_t4CF078FA8EE2AA5581BFA52E270B8A7328123AFD * ___U3CU3E9__93_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__93_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB_StaticFields, ___U3CU3E9__93_0_1)); }
	inline Func_2_t4CF078FA8EE2AA5581BFA52E270B8A7328123AFD * get_U3CU3E9__93_0_1() const { return ___U3CU3E9__93_0_1; }
	inline Func_2_t4CF078FA8EE2AA5581BFA52E270B8A7328123AFD ** get_address_of_U3CU3E9__93_0_1() { return &___U3CU3E9__93_0_1; }
	inline void set_U3CU3E9__93_0_1(Func_2_t4CF078FA8EE2AA5581BFA52E270B8A7328123AFD * value)
	{
		___U3CU3E9__93_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__93_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__93_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB_StaticFields, ___U3CU3E9__93_1_2)); }
	inline Func_2_t4CF078FA8EE2AA5581BFA52E270B8A7328123AFD * get_U3CU3E9__93_1_2() const { return ___U3CU3E9__93_1_2; }
	inline Func_2_t4CF078FA8EE2AA5581BFA52E270B8A7328123AFD ** get_address_of_U3CU3E9__93_1_2() { return &___U3CU3E9__93_1_2; }
	inline void set_U3CU3E9__93_1_2(Func_2_t4CF078FA8EE2AA5581BFA52E270B8A7328123AFD * value)
	{
		___U3CU3E9__93_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__93_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T14BFBF58E1978D2CE47DD45B594E83527C0AEBFB_H
#ifndef U3CEXECUTEGETORIENTATIONENUMERATORU3ED__101_TFE43CF8EE5417243D1B178B99BAADD1998929041_H
#define U3CEXECUTEGETORIENTATIONENUMERATORU3ED__101_TFE43CF8EE5417243D1B178B99BAADD1998929041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIView_<ExecuteGetOrientationEnumerator>d__101
struct  U3CExecuteGetOrientationEnumeratorU3Ed__101_tFE43CF8EE5417243D1B178B99BAADD1998929041  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIView_<ExecuteGetOrientationEnumerator>d__101::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIView_<ExecuteGetOrientationEnumerator>d__101::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.UI.UIView Doozy.Engine.UI.UIView_<ExecuteGetOrientationEnumerator>d__101::<>4__this
	UIView_t65D38836246049951821D4E45B1C81947591EBDF * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteGetOrientationEnumeratorU3Ed__101_tFE43CF8EE5417243D1B178B99BAADD1998929041, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteGetOrientationEnumeratorU3Ed__101_tFE43CF8EE5417243D1B178B99BAADD1998929041, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CExecuteGetOrientationEnumeratorU3Ed__101_tFE43CF8EE5417243D1B178B99BAADD1998929041, ___U3CU3E4__this_2)); }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UIView_t65D38836246049951821D4E45B1C81947591EBDF * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEGETORIENTATIONENUMERATORU3ED__101_TFE43CF8EE5417243D1B178B99BAADD1998929041_H
#ifndef U3CEXECUTEHIDEDESELECTBUTTONENUMERATORU3ED__100_T12340B89B46953D589254BEC2E7C105912C77CA4_H
#define U3CEXECUTEHIDEDESELECTBUTTONENUMERATORU3ED__100_T12340B89B46953D589254BEC2E7C105912C77CA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIView_<ExecuteHideDeselectButtonEnumerator>d__100
struct  U3CExecuteHideDeselectButtonEnumeratorU3Ed__100_t12340B89B46953D589254BEC2E7C105912C77CA4  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIView_<ExecuteHideDeselectButtonEnumerator>d__100::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIView_<ExecuteHideDeselectButtonEnumerator>d__100::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.UI.UIView Doozy.Engine.UI.UIView_<ExecuteHideDeselectButtonEnumerator>d__100::<>4__this
	UIView_t65D38836246049951821D4E45B1C81947591EBDF * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteHideDeselectButtonEnumeratorU3Ed__100_t12340B89B46953D589254BEC2E7C105912C77CA4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteHideDeselectButtonEnumeratorU3Ed__100_t12340B89B46953D589254BEC2E7C105912C77CA4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CExecuteHideDeselectButtonEnumeratorU3Ed__100_t12340B89B46953D589254BEC2E7C105912C77CA4, ___U3CU3E4__this_2)); }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UIView_t65D38836246049951821D4E45B1C81947591EBDF * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEHIDEDESELECTBUTTONENUMERATORU3ED__100_T12340B89B46953D589254BEC2E7C105912C77CA4_H
#ifndef U3CEXECUTESHOWSELECTDESELECTBUTTONENUMERATORU3ED__99_T150C3AD37AD58726558A7C2AE82DD6B0F56BEDD4_H
#define U3CEXECUTESHOWSELECTDESELECTBUTTONENUMERATORU3ED__99_T150C3AD37AD58726558A7C2AE82DD6B0F56BEDD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIView_<ExecuteShowSelectDeselectButtonEnumerator>d__99
struct  U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__99_t150C3AD37AD58726558A7C2AE82DD6B0F56BEDD4  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIView_<ExecuteShowSelectDeselectButtonEnumerator>d__99::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIView_<ExecuteShowSelectDeselectButtonEnumerator>d__99::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Doozy.Engine.UI.UIView Doozy.Engine.UI.UIView_<ExecuteShowSelectDeselectButtonEnumerator>d__99::<>4__this
	UIView_t65D38836246049951821D4E45B1C81947591EBDF * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__99_t150C3AD37AD58726558A7C2AE82DD6B0F56BEDD4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__99_t150C3AD37AD58726558A7C2AE82DD6B0F56BEDD4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__99_t150C3AD37AD58726558A7C2AE82DD6B0F56BEDD4, ___U3CU3E4__this_2)); }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UIView_t65D38836246049951821D4E45B1C81947591EBDF * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTESHOWSELECTDESELECTBUTTONENUMERATORU3ED__99_T150C3AD37AD58726558A7C2AE82DD6B0F56BEDD4_H
#ifndef U3CHIDEWITHDELAYENUMERATORU3ED__98_TBFDD34504651079027EBC9CE71ACC2BCACC1D091_H
#define U3CHIDEWITHDELAYENUMERATORU3ED__98_TBFDD34504651079027EBC9CE71ACC2BCACC1D091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIView_<HideWithDelayEnumerator>d__98
struct  U3CHideWithDelayEnumeratorU3Ed__98_tBFDD34504651079027EBC9CE71ACC2BCACC1D091  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIView_<HideWithDelayEnumerator>d__98::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIView_<HideWithDelayEnumerator>d__98::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Doozy.Engine.UI.UIView_<HideWithDelayEnumerator>d__98::delay
	float ___delay_2;
	// Doozy.Engine.UI.UIView Doozy.Engine.UI.UIView_<HideWithDelayEnumerator>d__98::<>4__this
	UIView_t65D38836246049951821D4E45B1C81947591EBDF * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CHideWithDelayEnumeratorU3Ed__98_tBFDD34504651079027EBC9CE71ACC2BCACC1D091, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CHideWithDelayEnumeratorU3Ed__98_tBFDD34504651079027EBC9CE71ACC2BCACC1D091, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CHideWithDelayEnumeratorU3Ed__98_tBFDD34504651079027EBC9CE71ACC2BCACC1D091, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CHideWithDelayEnumeratorU3Ed__98_tBFDD34504651079027EBC9CE71ACC2BCACC1D091, ___U3CU3E4__this_3)); }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIView_t65D38836246049951821D4E45B1C81947591EBDF * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIDEWITHDELAYENUMERATORU3ED__98_TBFDD34504651079027EBC9CE71ACC2BCACC1D091_H
#ifndef U3CTRIGGERSHOWINNEXTFRAMEU3ED__95_T2F667FEC4BF3B323240968A9D12C56CE87131EBC_H
#define U3CTRIGGERSHOWINNEXTFRAMEU3ED__95_T2F667FEC4BF3B323240968A9D12C56CE87131EBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIView_<TriggerShowInNextFrame>d__95
struct  U3CTriggerShowInNextFrameU3Ed__95_t2F667FEC4BF3B323240968A9D12C56CE87131EBC  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIView_<TriggerShowInNextFrame>d__95::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIView_<TriggerShowInNextFrame>d__95::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Boolean Doozy.Engine.UI.UIView_<TriggerShowInNextFrame>d__95::instantAction
	bool ___instantAction_2;
	// Doozy.Engine.UI.UIView Doozy.Engine.UI.UIView_<TriggerShowInNextFrame>d__95::<>4__this
	UIView_t65D38836246049951821D4E45B1C81947591EBDF * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTriggerShowInNextFrameU3Ed__95_t2F667FEC4BF3B323240968A9D12C56CE87131EBC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTriggerShowInNextFrameU3Ed__95_t2F667FEC4BF3B323240968A9D12C56CE87131EBC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_instantAction_2() { return static_cast<int32_t>(offsetof(U3CTriggerShowInNextFrameU3Ed__95_t2F667FEC4BF3B323240968A9D12C56CE87131EBC, ___instantAction_2)); }
	inline bool get_instantAction_2() const { return ___instantAction_2; }
	inline bool* get_address_of_instantAction_2() { return &___instantAction_2; }
	inline void set_instantAction_2(bool value)
	{
		___instantAction_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CTriggerShowInNextFrameU3Ed__95_t2F667FEC4BF3B323240968A9D12C56CE87131EBC, ___U3CU3E4__this_3)); }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIView_t65D38836246049951821D4E45B1C81947591EBDF * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRIGGERSHOWINNEXTFRAMEU3ED__95_T2F667FEC4BF3B323240968A9D12C56CE87131EBC_H
#ifndef UIVIEWBEHAVIOR_T735C597F51AF621EC424ED9F4AACD7034F2B8522_H
#define UIVIEWBEHAVIOR_T735C597F51AF621EC424ED9F4AACD7034F2B8522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIViewBehavior
struct  UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.Animation.UIAnimation Doozy.Engine.UI.UIViewBehavior::Animation
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * ___Animation_3;
	// System.Boolean Doozy.Engine.UI.UIViewBehavior::AutoStartLoopAnimation
	bool ___AutoStartLoopAnimation_4;
	// System.Boolean Doozy.Engine.UI.UIViewBehavior::LoadSelectedPresetAtRuntime
	bool ___LoadSelectedPresetAtRuntime_5;
	// System.Boolean Doozy.Engine.UI.UIViewBehavior::InstantAnimation
	bool ___InstantAnimation_6;
	// Doozy.Engine.UI.Base.UIAction Doozy.Engine.UI.UIViewBehavior::OnFinished
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * ___OnFinished_7;
	// Doozy.Engine.UI.Base.UIAction Doozy.Engine.UI.UIViewBehavior::OnStart
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * ___OnStart_8;
	// System.String Doozy.Engine.UI.UIViewBehavior::PresetCategory
	String_t* ___PresetCategory_9;
	// System.String Doozy.Engine.UI.UIViewBehavior::PresetName
	String_t* ___PresetName_10;
	// System.Single Doozy.Engine.UI.UIViewBehavior::m_progress
	float ___m_progress_11;

public:
	inline static int32_t get_offset_of_Animation_3() { return static_cast<int32_t>(offsetof(UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522, ___Animation_3)); }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * get_Animation_3() const { return ___Animation_3; }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C ** get_address_of_Animation_3() { return &___Animation_3; }
	inline void set_Animation_3(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * value)
	{
		___Animation_3 = value;
		Il2CppCodeGenWriteBarrier((&___Animation_3), value);
	}

	inline static int32_t get_offset_of_AutoStartLoopAnimation_4() { return static_cast<int32_t>(offsetof(UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522, ___AutoStartLoopAnimation_4)); }
	inline bool get_AutoStartLoopAnimation_4() const { return ___AutoStartLoopAnimation_4; }
	inline bool* get_address_of_AutoStartLoopAnimation_4() { return &___AutoStartLoopAnimation_4; }
	inline void set_AutoStartLoopAnimation_4(bool value)
	{
		___AutoStartLoopAnimation_4 = value;
	}

	inline static int32_t get_offset_of_LoadSelectedPresetAtRuntime_5() { return static_cast<int32_t>(offsetof(UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522, ___LoadSelectedPresetAtRuntime_5)); }
	inline bool get_LoadSelectedPresetAtRuntime_5() const { return ___LoadSelectedPresetAtRuntime_5; }
	inline bool* get_address_of_LoadSelectedPresetAtRuntime_5() { return &___LoadSelectedPresetAtRuntime_5; }
	inline void set_LoadSelectedPresetAtRuntime_5(bool value)
	{
		___LoadSelectedPresetAtRuntime_5 = value;
	}

	inline static int32_t get_offset_of_InstantAnimation_6() { return static_cast<int32_t>(offsetof(UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522, ___InstantAnimation_6)); }
	inline bool get_InstantAnimation_6() const { return ___InstantAnimation_6; }
	inline bool* get_address_of_InstantAnimation_6() { return &___InstantAnimation_6; }
	inline void set_InstantAnimation_6(bool value)
	{
		___InstantAnimation_6 = value;
	}

	inline static int32_t get_offset_of_OnFinished_7() { return static_cast<int32_t>(offsetof(UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522, ___OnFinished_7)); }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * get_OnFinished_7() const { return ___OnFinished_7; }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 ** get_address_of_OnFinished_7() { return &___OnFinished_7; }
	inline void set_OnFinished_7(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * value)
	{
		___OnFinished_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinished_7), value);
	}

	inline static int32_t get_offset_of_OnStart_8() { return static_cast<int32_t>(offsetof(UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522, ___OnStart_8)); }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * get_OnStart_8() const { return ___OnStart_8; }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 ** get_address_of_OnStart_8() { return &___OnStart_8; }
	inline void set_OnStart_8(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * value)
	{
		___OnStart_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnStart_8), value);
	}

	inline static int32_t get_offset_of_PresetCategory_9() { return static_cast<int32_t>(offsetof(UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522, ___PresetCategory_9)); }
	inline String_t* get_PresetCategory_9() const { return ___PresetCategory_9; }
	inline String_t** get_address_of_PresetCategory_9() { return &___PresetCategory_9; }
	inline void set_PresetCategory_9(String_t* value)
	{
		___PresetCategory_9 = value;
		Il2CppCodeGenWriteBarrier((&___PresetCategory_9), value);
	}

	inline static int32_t get_offset_of_PresetName_10() { return static_cast<int32_t>(offsetof(UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522, ___PresetName_10)); }
	inline String_t* get_PresetName_10() const { return ___PresetName_10; }
	inline String_t** get_address_of_PresetName_10() { return &___PresetName_10; }
	inline void set_PresetName_10(String_t* value)
	{
		___PresetName_10 = value;
		Il2CppCodeGenWriteBarrier((&___PresetName_10), value);
	}

	inline static int32_t get_offset_of_m_progress_11() { return static_cast<int32_t>(offsetof(UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522, ___m_progress_11)); }
	inline float get_m_progress_11() const { return ___m_progress_11; }
	inline float* get_address_of_m_progress_11() { return &___m_progress_11; }
	inline void set_m_progress_11(float value)
	{
		___m_progress_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWBEHAVIOR_T735C597F51AF621EC424ED9F4AACD7034F2B8522_H
#ifndef K_T1BBBD3F72964ADDA67B0FEEECF7D03939235EC48_H
#define K_T1BBBD3F72964ADDA67B0FEEECF7D03939235EC48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.CMYK_K
struct  K_t1BBBD3F72964ADDA67B0FEEECF7D03939235EC48  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // K_T1BBBD3F72964ADDA67B0FEEECF7D03939235EC48_H
#ifndef M_T470B6C8EB456A1AE2FCA220FA5CA47A2F2983764_H
#define M_T470B6C8EB456A1AE2FCA220FA5CA47A2F2983764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.CMYK_M
struct  M_t470B6C8EB456A1AE2FCA220FA5CA47A2F2983764  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // M_T470B6C8EB456A1AE2FCA220FA5CA47A2F2983764_H
#ifndef Y_T811F058E218BD0332457223D58D89F93B033F14E_H
#define Y_T811F058E218BD0332457223D58D89F93B033F14E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.CMYK_Y
struct  Y_t811F058E218BD0332457223D58D89F93B033F14E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // Y_T811F058E218BD0332457223D58D89F93B033F14E_H
#ifndef HSL_T0B3C6ABBDB52C9D9FF28B25B1A6D49B290C4B51F_H
#define HSL_T0B3C6ABBDB52C9D9FF28B25B1A6D49B290C4B51F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.HSL
struct  HSL_t0B3C6ABBDB52C9D9FF28B25B1A6D49B290C4B51F  : public RuntimeObject
{
public:
	// System.Single Doozy.Engine.Utils.ColorModels.HSL::h
	float ___h_0;
	// System.Single Doozy.Engine.Utils.ColorModels.HSL::s
	float ___s_1;
	// System.Single Doozy.Engine.Utils.ColorModels.HSL::l
	float ___l_2;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(HSL_t0B3C6ABBDB52C9D9FF28B25B1A6D49B290C4B51F, ___h_0)); }
	inline float get_h_0() const { return ___h_0; }
	inline float* get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(float value)
	{
		___h_0 = value;
	}

	inline static int32_t get_offset_of_s_1() { return static_cast<int32_t>(offsetof(HSL_t0B3C6ABBDB52C9D9FF28B25B1A6D49B290C4B51F, ___s_1)); }
	inline float get_s_1() const { return ___s_1; }
	inline float* get_address_of_s_1() { return &___s_1; }
	inline void set_s_1(float value)
	{
		___s_1 = value;
	}

	inline static int32_t get_offset_of_l_2() { return static_cast<int32_t>(offsetof(HSL_t0B3C6ABBDB52C9D9FF28B25B1A6D49B290C4B51F, ___l_2)); }
	inline float get_l_2() const { return ___l_2; }
	inline float* get_address_of_l_2() { return &___l_2; }
	inline void set_l_2(float value)
	{
		___l_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSL_T0B3C6ABBDB52C9D9FF28B25B1A6D49B290C4B51F_H
#ifndef H_T5584741E522FCF9D58B1F060256A48C907B9A9B9_H
#define H_T5584741E522FCF9D58B1F060256A48C907B9A9B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.HSL_H
struct  H_t5584741E522FCF9D58B1F060256A48C907B9A9B9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // H_T5584741E522FCF9D58B1F060256A48C907B9A9B9_H
#ifndef L_T753A1F9F315EA5D7218960492E3984BB50EBE371_H
#define L_T753A1F9F315EA5D7218960492E3984BB50EBE371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.HSL_L
struct  L_t753A1F9F315EA5D7218960492E3984BB50EBE371  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L_T753A1F9F315EA5D7218960492E3984BB50EBE371_H
#ifndef S_T681EBA87637B8F525B1905F4BAD6C2C0B32669A6_H
#define S_T681EBA87637B8F525B1905F4BAD6C2C0B32669A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.HSL_S
struct  S_t681EBA87637B8F525B1905F4BAD6C2C0B32669A6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S_T681EBA87637B8F525B1905F4BAD6C2C0B32669A6_H
#ifndef HSV_TD5CDD00DCCE24D4769693D3B853C0051B41272D4_H
#define HSV_TD5CDD00DCCE24D4769693D3B853C0051B41272D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.HSV
struct  HSV_tD5CDD00DCCE24D4769693D3B853C0051B41272D4  : public RuntimeObject
{
public:
	// System.Single Doozy.Engine.Utils.ColorModels.HSV::h
	float ___h_0;
	// System.Single Doozy.Engine.Utils.ColorModels.HSV::s
	float ___s_1;
	// System.Single Doozy.Engine.Utils.ColorModels.HSV::v
	float ___v_2;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(HSV_tD5CDD00DCCE24D4769693D3B853C0051B41272D4, ___h_0)); }
	inline float get_h_0() const { return ___h_0; }
	inline float* get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(float value)
	{
		___h_0 = value;
	}

	inline static int32_t get_offset_of_s_1() { return static_cast<int32_t>(offsetof(HSV_tD5CDD00DCCE24D4769693D3B853C0051B41272D4, ___s_1)); }
	inline float get_s_1() const { return ___s_1; }
	inline float* get_address_of_s_1() { return &___s_1; }
	inline void set_s_1(float value)
	{
		___s_1 = value;
	}

	inline static int32_t get_offset_of_v_2() { return static_cast<int32_t>(offsetof(HSV_tD5CDD00DCCE24D4769693D3B853C0051B41272D4, ___v_2)); }
	inline float get_v_2() const { return ___v_2; }
	inline float* get_address_of_v_2() { return &___v_2; }
	inline void set_v_2(float value)
	{
		___v_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSV_TD5CDD00DCCE24D4769693D3B853C0051B41272D4_H
#ifndef H_T23779FBE8D5D65A8EF821291DD3E3B4EB8A086EE_H
#define H_T23779FBE8D5D65A8EF821291DD3E3B4EB8A086EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.HSV_H
struct  H_t23779FBE8D5D65A8EF821291DD3E3B4EB8A086EE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // H_T23779FBE8D5D65A8EF821291DD3E3B4EB8A086EE_H
#ifndef S_T3AEEB67C04D389B7B58F9F098379DCAB75217776_H
#define S_T3AEEB67C04D389B7B58F9F098379DCAB75217776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.HSV_S
struct  S_t3AEEB67C04D389B7B58F9F098379DCAB75217776  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S_T3AEEB67C04D389B7B58F9F098379DCAB75217776_H
#ifndef V_TA209264E75A9CE99CF8856BC8B0786DB6365E613_H
#define V_TA209264E75A9CE99CF8856BC8B0786DB6365E613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.HSV_V
struct  V_tA209264E75A9CE99CF8856BC8B0786DB6365E613  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // V_TA209264E75A9CE99CF8856BC8B0786DB6365E613_H
#ifndef RGB_T25E15B2C36B00E5F2D7538CEF52679EA7408486D_H
#define RGB_T25E15B2C36B00E5F2D7538CEF52679EA7408486D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.RGB
struct  RGB_t25E15B2C36B00E5F2D7538CEF52679EA7408486D  : public RuntimeObject
{
public:
	// System.Single Doozy.Engine.Utils.ColorModels.RGB::r
	float ___r_0;
	// System.Single Doozy.Engine.Utils.ColorModels.RGB::g
	float ___g_1;
	// System.Single Doozy.Engine.Utils.ColorModels.RGB::b
	float ___b_2;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(RGB_t25E15B2C36B00E5F2D7538CEF52679EA7408486D, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(RGB_t25E15B2C36B00E5F2D7538CEF52679EA7408486D, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(RGB_t25E15B2C36B00E5F2D7538CEF52679EA7408486D, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RGB_T25E15B2C36B00E5F2D7538CEF52679EA7408486D_H
#ifndef B_T9050547E3AD0819EC78B6411B680D0BF40CDBF32_H
#define B_T9050547E3AD0819EC78B6411B680D0BF40CDBF32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.RGB_B
struct  B_t9050547E3AD0819EC78B6411B680D0BF40CDBF32  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // B_T9050547E3AD0819EC78B6411B680D0BF40CDBF32_H
#ifndef G_T69DD6CC9287DC797284FF21BD4BE9D33583821D1_H
#define G_T69DD6CC9287DC797284FF21BD4BE9D33583821D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.RGB_G
struct  G_t69DD6CC9287DC797284FF21BD4BE9D33583821D1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_T69DD6CC9287DC797284FF21BD4BE9D33583821D1_H
#ifndef R_T3B6787C339F9D631EE35A12BB7240E57B895DB8B_H
#define R_T3B6787C339F9D631EE35A12BB7240E57B895DB8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.RGB_R
struct  R_t3B6787C339F9D631EE35A12BB7240E57B895DB8B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // R_T3B6787C339F9D631EE35A12BB7240E57B895DB8B_H
#ifndef XYZ_TD5A59D80D6552D6545DCA9F8D46C07F7AE914D65_H
#define XYZ_TD5A59D80D6552D6545DCA9F8D46C07F7AE914D65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.XYZ
struct  XYZ_tD5A59D80D6552D6545DCA9F8D46C07F7AE914D65  : public RuntimeObject
{
public:
	// System.Single Doozy.Engine.Utils.ColorModels.XYZ::x
	float ___x_0;
	// System.Single Doozy.Engine.Utils.ColorModels.XYZ::y
	float ___y_1;
	// System.Single Doozy.Engine.Utils.ColorModels.XYZ::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(XYZ_tD5A59D80D6552D6545DCA9F8D46C07F7AE914D65, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(XYZ_tD5A59D80D6552D6545DCA9F8D46C07F7AE914D65, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(XYZ_tD5A59D80D6552D6545DCA9F8D46C07F7AE914D65, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XYZ_TD5A59D80D6552D6545DCA9F8D46C07F7AE914D65_H
#ifndef X_T0DE7F3DF404E217F40CEAEEC5688B4C1ED1BDFCD_H
#define X_T0DE7F3DF404E217F40CEAEEC5688B4C1ED1BDFCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.XYZ_X
struct  X_t0DE7F3DF404E217F40CEAEEC5688B4C1ED1BDFCD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X_T0DE7F3DF404E217F40CEAEEC5688B4C1ED1BDFCD_H
#ifndef Y_TC001AC3D799B9E5BECCB966FC77B9349B488BC27_H
#define Y_TC001AC3D799B9E5BECCB966FC77B9349B488BC27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.XYZ_Y
struct  Y_tC001AC3D799B9E5BECCB966FC77B9349B488BC27  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // Y_TC001AC3D799B9E5BECCB966FC77B9349B488BC27_H
#ifndef Z_TF3C318BAA0CAB9A3D587EC2E72593FC33A47458F_H
#define Z_TF3C318BAA0CAB9A3D587EC2E72593FC33A47458F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Utils.ColorModels.XYZ_Z
struct  Z_tF3C318BAA0CAB9A3D587EC2E72593FC33A47458F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // Z_TF3C318BAA0CAB9A3D587EC2E72593FC33A47458F_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef UNITYEVENT_1_TC8DE301C640B5A96A93011573803D218E639ED48_H
#define UNITYEVENT_1_TC8DE301C640B5A96A93011573803D218E639ED48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Doozy.Engine.UI.UIButton>
struct  UnityEvent_1_tC8DE301C640B5A96A93011573803D218E639ED48  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_tC8DE301C640B5A96A93011573803D218E639ED48, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TC8DE301C640B5A96A93011573803D218E639ED48_H
#ifndef UNITYEVENT_1_TEEE895A71C752315C2E1C71229685E86E77E32BE_H
#define UNITYEVENT_1_TEEE895A71C752315C2E1C71229685E86E77E32BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Doozy.Engine.UI.UIDrawer>
struct  UnityEvent_1_tEEE895A71C752315C2E1C71229685E86E77E32BE  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_tEEE895A71C752315C2E1C71229685E86E77E32BE, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TEEE895A71C752315C2E1C71229685E86E77E32BE_H
#ifndef UNITYEVENT_1_T8A1FFF79A8DAE5DB41C1D884E9B294756B624905_H
#define UNITYEVENT_1_T8A1FFF79A8DAE5DB41C1D884E9B294756B624905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Doozy.Engine.UI.UIToggle>
struct  UnityEvent_1_t8A1FFF79A8DAE5DB41C1D884E9B294756B624905  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t8A1FFF79A8DAE5DB41C1D884E9B294756B624905, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T8A1FFF79A8DAE5DB41C1D884E9B294756B624905_H
#ifndef UNITYEVENT_1_T080E4DBB31A1D05BAC21C0C0509158A8333B8BC1_H
#define UNITYEVENT_1_T080E4DBB31A1D05BAC21C0C0509158A8333B8BC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Doozy.Engine.UI.UIView>
struct  UnityEvent_1_t080E4DBB31A1D05BAC21C0C0509158A8333B8BC1  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t080E4DBB31A1D05BAC21C0C0509158A8333B8BC1, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T080E4DBB31A1D05BAC21C0C0509158A8333B8BC1_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef SIMPLESWIPE_T3BEB318CEBE340D43602826A0843D0D23C848473_H
#define SIMPLESWIPE_T3BEB318CEBE340D43602826A0843D0D23C848473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.Touchy.SimpleSwipe
struct  SimpleSwipe_t3BEB318CEBE340D43602826A0843D0D23C848473 
{
public:
	// System.Int32 Doozy.Engine.Touchy.SimpleSwipe::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SimpleSwipe_t3BEB318CEBE340D43602826A0843D0D23C848473, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESWIPE_T3BEB318CEBE340D43602826A0843D0D23C848473_H
#ifndef ANIMATIONTYPE_TD5E6CBA919BA471B2AE3978400DFBFD83760E003_H
#define ANIMATIONTYPE_TD5E6CBA919BA471B2AE3978400DFBFD83760E003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.AnimationType
struct  AnimationType_tD5E6CBA919BA471B2AE3978400DFBFD83760E003 
{
public:
	// System.Int32 Doozy.Engine.UI.Animation.AnimationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimationType_tD5E6CBA919BA471B2AE3978400DFBFD83760E003, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTYPE_TD5E6CBA919BA471B2AE3978400DFBFD83760E003_H
#ifndef BUTTONANIMATIONTYPE_T574DD43CF3A425FF5B14A6F3AF4561B5D5EAB33F_H
#define BUTTONANIMATIONTYPE_T574DD43CF3A425FF5B14A6F3AF4561B5D5EAB33F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.ButtonAnimationType
struct  ButtonAnimationType_t574DD43CF3A425FF5B14A6F3AF4561B5D5EAB33F 
{
public:
	// System.Int32 Doozy.Engine.UI.Animation.ButtonAnimationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonAnimationType_t574DD43CF3A425FF5B14A6F3AF4561B5D5EAB33F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONANIMATIONTYPE_T574DD43CF3A425FF5B14A6F3AF4561B5D5EAB33F_H
#ifndef BUTTONLOOPANIMATIONTYPE_T50E9901AFE81E2202E3A009B903F0F2131705A12_H
#define BUTTONLOOPANIMATIONTYPE_T50E9901AFE81E2202E3A009B903F0F2131705A12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Animation.ButtonLoopAnimationType
struct  ButtonLoopAnimationType_t50E9901AFE81E2202E3A009B903F0F2131705A12 
{
public:
	// System.Int32 Doozy.Engine.UI.Animation.ButtonLoopAnimationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonLoopAnimationType_t50E9901AFE81E2202E3A009B903F0F2131705A12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONLOOPANIMATIONTYPE_T50E9901AFE81E2202E3A009B903F0F2131705A12_H
#ifndef UICONTAINER_T19EF532BD0C41D5271FE710E5665E0A1845A600B_H
#define UICONTAINER_T19EF532BD0C41D5271FE710E5665E0A1845A600B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.UIContainer
struct  UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B  : public RuntimeObject
{
public:
	// UnityEngine.Canvas Doozy.Engine.UI.Base.UIContainer::Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___Canvas_4;
	// UnityEngine.CanvasGroup Doozy.Engine.UI.Base.UIContainer::CanvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___CanvasGroup_5;
	// System.Boolean Doozy.Engine.UI.Base.UIContainer::DisableCanvas
	bool ___DisableCanvas_6;
	// System.Boolean Doozy.Engine.UI.Base.UIContainer::DisableGameObject
	bool ___DisableGameObject_7;
	// System.Boolean Doozy.Engine.UI.Base.UIContainer::DisableGraphicRaycaster
	bool ___DisableGraphicRaycaster_8;
	// System.Boolean Doozy.Engine.UI.Base.UIContainer::Enabled
	bool ___Enabled_9;
	// UnityEngine.UI.GraphicRaycaster Doozy.Engine.UI.Base.UIContainer::GraphicRaycaster
	GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * ___GraphicRaycaster_10;
	// UnityEngine.RectTransform Doozy.Engine.UI.Base.UIContainer::RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___RectTransform_11;
	// System.Single Doozy.Engine.UI.Base.UIContainer::StartAlpha
	float ___StartAlpha_12;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIContainer::StartPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartPosition_13;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIContainer::StartRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartRotation_14;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIContainer::StartScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartScale_15;

public:
	inline static int32_t get_offset_of_Canvas_4() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___Canvas_4)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_Canvas_4() const { return ___Canvas_4; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_Canvas_4() { return &___Canvas_4; }
	inline void set_Canvas_4(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___Canvas_4 = value;
		Il2CppCodeGenWriteBarrier((&___Canvas_4), value);
	}

	inline static int32_t get_offset_of_CanvasGroup_5() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___CanvasGroup_5)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_CanvasGroup_5() const { return ___CanvasGroup_5; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_CanvasGroup_5() { return &___CanvasGroup_5; }
	inline void set_CanvasGroup_5(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___CanvasGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&___CanvasGroup_5), value);
	}

	inline static int32_t get_offset_of_DisableCanvas_6() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___DisableCanvas_6)); }
	inline bool get_DisableCanvas_6() const { return ___DisableCanvas_6; }
	inline bool* get_address_of_DisableCanvas_6() { return &___DisableCanvas_6; }
	inline void set_DisableCanvas_6(bool value)
	{
		___DisableCanvas_6 = value;
	}

	inline static int32_t get_offset_of_DisableGameObject_7() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___DisableGameObject_7)); }
	inline bool get_DisableGameObject_7() const { return ___DisableGameObject_7; }
	inline bool* get_address_of_DisableGameObject_7() { return &___DisableGameObject_7; }
	inline void set_DisableGameObject_7(bool value)
	{
		___DisableGameObject_7 = value;
	}

	inline static int32_t get_offset_of_DisableGraphicRaycaster_8() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___DisableGraphicRaycaster_8)); }
	inline bool get_DisableGraphicRaycaster_8() const { return ___DisableGraphicRaycaster_8; }
	inline bool* get_address_of_DisableGraphicRaycaster_8() { return &___DisableGraphicRaycaster_8; }
	inline void set_DisableGraphicRaycaster_8(bool value)
	{
		___DisableGraphicRaycaster_8 = value;
	}

	inline static int32_t get_offset_of_Enabled_9() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___Enabled_9)); }
	inline bool get_Enabled_9() const { return ___Enabled_9; }
	inline bool* get_address_of_Enabled_9() { return &___Enabled_9; }
	inline void set_Enabled_9(bool value)
	{
		___Enabled_9 = value;
	}

	inline static int32_t get_offset_of_GraphicRaycaster_10() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___GraphicRaycaster_10)); }
	inline GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * get_GraphicRaycaster_10() const { return ___GraphicRaycaster_10; }
	inline GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 ** get_address_of_GraphicRaycaster_10() { return &___GraphicRaycaster_10; }
	inline void set_GraphicRaycaster_10(GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * value)
	{
		___GraphicRaycaster_10 = value;
		Il2CppCodeGenWriteBarrier((&___GraphicRaycaster_10), value);
	}

	inline static int32_t get_offset_of_RectTransform_11() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___RectTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_RectTransform_11() const { return ___RectTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_RectTransform_11() { return &___RectTransform_11; }
	inline void set_RectTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___RectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___RectTransform_11), value);
	}

	inline static int32_t get_offset_of_StartAlpha_12() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___StartAlpha_12)); }
	inline float get_StartAlpha_12() const { return ___StartAlpha_12; }
	inline float* get_address_of_StartAlpha_12() { return &___StartAlpha_12; }
	inline void set_StartAlpha_12(float value)
	{
		___StartAlpha_12 = value;
	}

	inline static int32_t get_offset_of_StartPosition_13() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___StartPosition_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartPosition_13() const { return ___StartPosition_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartPosition_13() { return &___StartPosition_13; }
	inline void set_StartPosition_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartPosition_13 = value;
	}

	inline static int32_t get_offset_of_StartRotation_14() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___StartRotation_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartRotation_14() const { return ___StartRotation_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartRotation_14() { return &___StartRotation_14; }
	inline void set_StartRotation_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartRotation_14 = value;
	}

	inline static int32_t get_offset_of_StartScale_15() { return static_cast<int32_t>(offsetof(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B, ___StartScale_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartScale_15() const { return ___StartScale_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartScale_15() { return &___StartScale_15; }
	inline void set_StartScale_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartScale_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICONTAINER_T19EF532BD0C41D5271FE710E5665E0A1845A600B_H
#ifndef DYNAMICSORTING_TE88845A7C1E8C4071C615A938ABB17C18E7D8230_H
#define DYNAMICSORTING_TE88845A7C1E8C4071C615A938ABB17C18E7D8230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.DynamicSorting
struct  DynamicSorting_tE88845A7C1E8C4071C615A938ABB17C18E7D8230 
{
public:
	// System.Int32 Doozy.Engine.UI.DynamicSorting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DynamicSorting_tE88845A7C1E8C4071C615A938ABB17C18E7D8230, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICSORTING_TE88845A7C1E8C4071C615A938ABB17C18E7D8230_H
#ifndef POPUPDISPLAYON_T889AC013674992F05BFC1482F6E0F119436B3A62_H
#define POPUPDISPLAYON_T889AC013674992F05BFC1482F6E0F119436B3A62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.PopupDisplayOn
struct  PopupDisplayOn_t889AC013674992F05BFC1482F6E0F119436B3A62 
{
public:
	// System.Int32 Doozy.Engine.UI.PopupDisplayOn::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PopupDisplayOn_t889AC013674992F05BFC1482F6E0F119436B3A62, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPDISPLAYON_T889AC013674992F05BFC1482F6E0F119436B3A62_H
#ifndef SINGLECLICKMODE_TDB58245544CF8536DD5907066283135060CAEEB5_H
#define SINGLECLICKMODE_TDB58245544CF8536DD5907066283135060CAEEB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.SingleClickMode
struct  SingleClickMode_tDB58245544CF8536DD5907066283135060CAEEB5 
{
public:
	// System.Int32 Doozy.Engine.UI.SingleClickMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SingleClickMode_tDB58245544CF8536DD5907066283135060CAEEB5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLECLICKMODE_TDB58245544CF8536DD5907066283135060CAEEB5_H
#ifndef TARGETLABEL_T687F455FAE7E9369DC6001A633BEFAEACE1CE1CB_H
#define TARGETLABEL_T687F455FAE7E9369DC6001A633BEFAEACE1CE1CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.TargetLabel
struct  TargetLabel_t687F455FAE7E9369DC6001A633BEFAEACE1CE1CB 
{
public:
	// System.Int32 Doozy.Engine.UI.TargetLabel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TargetLabel_t687F455FAE7E9369DC6001A633BEFAEACE1CE1CB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETLABEL_T687F455FAE7E9369DC6001A633BEFAEACE1CE1CB_H
#ifndef TARGETORIENTATION_TC5F5E22D9BFB8892A068DB6956AE047A4D07AFCC_H
#define TARGETORIENTATION_TC5F5E22D9BFB8892A068DB6956AE047A4D07AFCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.TargetOrientation
struct  TargetOrientation_tC5F5E22D9BFB8892A068DB6956AE047A4D07AFCC 
{
public:
	// System.Int32 Doozy.Engine.UI.TargetOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TargetOrientation_tC5F5E22D9BFB8892A068DB6956AE047A4D07AFCC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETORIENTATION_TC5F5E22D9BFB8892A068DB6956AE047A4D07AFCC_H
#ifndef UIBUTTONBEHAVIORTYPE_TB025C99754BC9980024AFF2E71926A0ACD1A2756_H
#define UIBUTTONBEHAVIORTYPE_TB025C99754BC9980024AFF2E71926A0ACD1A2756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButtonBehaviorType
struct  UIButtonBehaviorType_tB025C99754BC9980024AFF2E71926A0ACD1A2756 
{
public:
	// System.Int32 Doozy.Engine.UI.UIButtonBehaviorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIButtonBehaviorType_tB025C99754BC9980024AFF2E71926A0ACD1A2756, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONBEHAVIORTYPE_TB025C99754BC9980024AFF2E71926A0ACD1A2756_H
#ifndef UIBUTTONEVENT_T4D2122D7D8CDB28C0231D7429F5D12847FB55BD6_H
#define UIBUTTONEVENT_T4D2122D7D8CDB28C0231D7429F5D12847FB55BD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButtonEvent
struct  UIButtonEvent_t4D2122D7D8CDB28C0231D7429F5D12847FB55BD6  : public UnityEvent_1_tC8DE301C640B5A96A93011573803D218E639ED48
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONEVENT_T4D2122D7D8CDB28C0231D7429F5D12847FB55BD6_H
#ifndef UIDRAWERARROW_T752417C85A8252AED8E70B35AAE2197E7011D4A3_H
#define UIDRAWERARROW_T752417C85A8252AED8E70B35AAE2197E7011D4A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIDrawerArrow
struct  UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.UIDrawerArrowAnimator Doozy.Engine.UI.UIDrawerArrow::Animator
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E * ___Animator_5;
	// UnityEngine.Color Doozy.Engine.UI.UIDrawerArrow::ClosedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___ClosedColor_6;
	// UnityEngine.RectTransform Doozy.Engine.UI.UIDrawerArrow::Container
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___Container_7;
	// Doozy.Engine.UI.UIDrawerArrow_Holder Doozy.Engine.UI.UIDrawerArrow::Down
	Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 * ___Down_8;
	// System.Boolean Doozy.Engine.UI.UIDrawerArrow::Enabled
	bool ___Enabled_9;
	// Doozy.Engine.UI.UIDrawerArrow_Holder Doozy.Engine.UI.UIDrawerArrow::Left
	Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 * ___Left_10;
	// UnityEngine.Color Doozy.Engine.UI.UIDrawerArrow::OpenedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___OpenedColor_11;
	// System.Boolean Doozy.Engine.UI.UIDrawerArrow::OverrideColor
	bool ___OverrideColor_12;
	// Doozy.Engine.UI.UIDrawerArrow_Holder Doozy.Engine.UI.UIDrawerArrow::Right
	Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 * ___Right_13;
	// System.Single Doozy.Engine.UI.UIDrawerArrow::Scale
	float ___Scale_14;
	// Doozy.Engine.UI.UIDrawerArrow_Holder Doozy.Engine.UI.UIDrawerArrow::Up
	Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 * ___Up_15;

public:
	inline static int32_t get_offset_of_Animator_5() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3, ___Animator_5)); }
	inline UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E * get_Animator_5() const { return ___Animator_5; }
	inline UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E ** get_address_of_Animator_5() { return &___Animator_5; }
	inline void set_Animator_5(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E * value)
	{
		___Animator_5 = value;
		Il2CppCodeGenWriteBarrier((&___Animator_5), value);
	}

	inline static int32_t get_offset_of_ClosedColor_6() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3, ___ClosedColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_ClosedColor_6() const { return ___ClosedColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_ClosedColor_6() { return &___ClosedColor_6; }
	inline void set_ClosedColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___ClosedColor_6 = value;
	}

	inline static int32_t get_offset_of_Container_7() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3, ___Container_7)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_Container_7() const { return ___Container_7; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_Container_7() { return &___Container_7; }
	inline void set_Container_7(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___Container_7 = value;
		Il2CppCodeGenWriteBarrier((&___Container_7), value);
	}

	inline static int32_t get_offset_of_Down_8() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3, ___Down_8)); }
	inline Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 * get_Down_8() const { return ___Down_8; }
	inline Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 ** get_address_of_Down_8() { return &___Down_8; }
	inline void set_Down_8(Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 * value)
	{
		___Down_8 = value;
		Il2CppCodeGenWriteBarrier((&___Down_8), value);
	}

	inline static int32_t get_offset_of_Enabled_9() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3, ___Enabled_9)); }
	inline bool get_Enabled_9() const { return ___Enabled_9; }
	inline bool* get_address_of_Enabled_9() { return &___Enabled_9; }
	inline void set_Enabled_9(bool value)
	{
		___Enabled_9 = value;
	}

	inline static int32_t get_offset_of_Left_10() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3, ___Left_10)); }
	inline Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 * get_Left_10() const { return ___Left_10; }
	inline Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 ** get_address_of_Left_10() { return &___Left_10; }
	inline void set_Left_10(Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 * value)
	{
		___Left_10 = value;
		Il2CppCodeGenWriteBarrier((&___Left_10), value);
	}

	inline static int32_t get_offset_of_OpenedColor_11() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3, ___OpenedColor_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_OpenedColor_11() const { return ___OpenedColor_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_OpenedColor_11() { return &___OpenedColor_11; }
	inline void set_OpenedColor_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___OpenedColor_11 = value;
	}

	inline static int32_t get_offset_of_OverrideColor_12() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3, ___OverrideColor_12)); }
	inline bool get_OverrideColor_12() const { return ___OverrideColor_12; }
	inline bool* get_address_of_OverrideColor_12() { return &___OverrideColor_12; }
	inline void set_OverrideColor_12(bool value)
	{
		___OverrideColor_12 = value;
	}

	inline static int32_t get_offset_of_Right_13() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3, ___Right_13)); }
	inline Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 * get_Right_13() const { return ___Right_13; }
	inline Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 ** get_address_of_Right_13() { return &___Right_13; }
	inline void set_Right_13(Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 * value)
	{
		___Right_13 = value;
		Il2CppCodeGenWriteBarrier((&___Right_13), value);
	}

	inline static int32_t get_offset_of_Scale_14() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3, ___Scale_14)); }
	inline float get_Scale_14() const { return ___Scale_14; }
	inline float* get_address_of_Scale_14() { return &___Scale_14; }
	inline void set_Scale_14(float value)
	{
		___Scale_14 = value;
	}

	inline static int32_t get_offset_of_Up_15() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3, ___Up_15)); }
	inline Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 * get_Up_15() const { return ___Up_15; }
	inline Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 ** get_address_of_Up_15() { return &___Up_15; }
	inline void set_Up_15(Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0 * value)
	{
		___Up_15 = value;
		Il2CppCodeGenWriteBarrier((&___Up_15), value);
	}
};

struct UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3_StaticFields
{
public:
	// UnityEngine.Color Doozy.Engine.UI.UIDrawerArrow::DefaultOpenedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___DefaultOpenedColor_3;
	// UnityEngine.Color Doozy.Engine.UI.UIDrawerArrow::DefaultClosedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___DefaultClosedColor_4;

public:
	inline static int32_t get_offset_of_DefaultOpenedColor_3() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3_StaticFields, ___DefaultOpenedColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_DefaultOpenedColor_3() const { return ___DefaultOpenedColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_DefaultOpenedColor_3() { return &___DefaultOpenedColor_3; }
	inline void set_DefaultOpenedColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___DefaultOpenedColor_3 = value;
	}

	inline static int32_t get_offset_of_DefaultClosedColor_4() { return static_cast<int32_t>(offsetof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3_StaticFields, ___DefaultClosedColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_DefaultClosedColor_4() const { return ___DefaultClosedColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_DefaultClosedColor_4() { return &___DefaultClosedColor_4; }
	inline void set_DefaultClosedColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___DefaultClosedColor_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWERARROW_T752417C85A8252AED8E70B35AAE2197E7011D4A3_H
#ifndef UIDRAWERBEHAVIORTYPE_T50837F7287BAFBBD867C2803DA5A696515A2DC6F_H
#define UIDRAWERBEHAVIORTYPE_T50837F7287BAFBBD867C2803DA5A696515A2DC6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIDrawerBehaviorType
struct  UIDrawerBehaviorType_t50837F7287BAFBBD867C2803DA5A696515A2DC6F 
{
public:
	// System.Int32 Doozy.Engine.UI.UIDrawerBehaviorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIDrawerBehaviorType_t50837F7287BAFBBD867C2803DA5A696515A2DC6F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWERBEHAVIORTYPE_T50837F7287BAFBBD867C2803DA5A696515A2DC6F_H
#ifndef UIDRAWERCONTAINERSIZE_T179CDF00D47D8D05D0AB7D2074965C38B1FDF5CB_H
#define UIDRAWERCONTAINERSIZE_T179CDF00D47D8D05D0AB7D2074965C38B1FDF5CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIDrawerContainerSize
struct  UIDrawerContainerSize_t179CDF00D47D8D05D0AB7D2074965C38B1FDF5CB 
{
public:
	// System.Int32 Doozy.Engine.UI.UIDrawerContainerSize::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIDrawerContainerSize_t179CDF00D47D8D05D0AB7D2074965C38B1FDF5CB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWERCONTAINERSIZE_T179CDF00D47D8D05D0AB7D2074965C38B1FDF5CB_H
#ifndef UIDRAWEREVENT_T511B9CCB479D30EE935B3EA995DBB00D281721F6_H
#define UIDRAWEREVENT_T511B9CCB479D30EE935B3EA995DBB00D281721F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIDrawerEvent
struct  UIDrawerEvent_t511B9CCB479D30EE935B3EA995DBB00D281721F6  : public UnityEvent_1_tEEE895A71C752315C2E1C71229685E86E77E32BE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWEREVENT_T511B9CCB479D30EE935B3EA995DBB00D281721F6_H
#ifndef UIEFFECTBEHAVIOR_T65B6EA484234ECF2B725C4ED8FAFDCF3F8E81F25_H
#define UIEFFECTBEHAVIOR_T65B6EA484234ECF2B725C4ED8FAFDCF3F8E81F25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIEffectBehavior
struct  UIEffectBehavior_t65B6EA484234ECF2B725C4ED8FAFDCF3F8E81F25 
{
public:
	// System.Int32 Doozy.Engine.UI.UIEffectBehavior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIEffectBehavior_t65B6EA484234ECF2B725C4ED8FAFDCF3F8E81F25, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEFFECTBEHAVIOR_T65B6EA484234ECF2B725C4ED8FAFDCF3F8E81F25_H
#ifndef U3CHIDEENUMERATORU3ED__109_T6F6B6DA4DE92700AF492FD340D870480E9DC59C6_H
#define U3CHIDEENUMERATORU3ED__109_T6F6B6DA4DE92700AF492FD340D870480E9DC59C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109
struct  U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Boolean Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::instantAction
	bool ___instantAction_2;
	// Doozy.Engine.UI.UIPopup Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<>4__this
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * ___U3CU3E4__this_3;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<moveFrom>5__1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CmoveFromU3E5__1_4;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<moveTo>5__2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CmoveToU3E5__2_5;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<rotateFrom>5__3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CrotateFromU3E5__3_6;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<rotateTo>5__4
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CrotateToU3E5__4_7;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<scaleFrom>5__5
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CscaleFromU3E5__5_8;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<scaleTo>5__6
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CscaleToU3E5__6_9;
	// System.Single Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<fadeFrom>5__7
	float ___U3CfadeFromU3E5__7_10;
	// System.Single Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<fadeTo>5__8
	float ___U3CfadeToU3E5__8_11;
	// System.Single Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<startTime>5__9
	float ___U3CstartTimeU3E5__9_12;
	// System.Single Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<totalDuration>5__10
	float ___U3CtotalDurationU3E5__10_13;
	// System.Single Doozy.Engine.UI.UIPopup_<HideEnumerator>d__109::<elapsedTime>5__11
	float ___U3CelapsedTimeU3E5__11_14;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_instantAction_2() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___instantAction_2)); }
	inline bool get_instantAction_2() const { return ___instantAction_2; }
	inline bool* get_address_of_instantAction_2() { return &___instantAction_2; }
	inline void set_instantAction_2(bool value)
	{
		___instantAction_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CU3E4__this_3)); }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CmoveFromU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CmoveFromU3E5__1_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CmoveFromU3E5__1_4() const { return ___U3CmoveFromU3E5__1_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CmoveFromU3E5__1_4() { return &___U3CmoveFromU3E5__1_4; }
	inline void set_U3CmoveFromU3E5__1_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CmoveFromU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CmoveToU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CmoveToU3E5__2_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CmoveToU3E5__2_5() const { return ___U3CmoveToU3E5__2_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CmoveToU3E5__2_5() { return &___U3CmoveToU3E5__2_5; }
	inline void set_U3CmoveToU3E5__2_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CmoveToU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CrotateFromU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CrotateFromU3E5__3_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CrotateFromU3E5__3_6() const { return ___U3CrotateFromU3E5__3_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CrotateFromU3E5__3_6() { return &___U3CrotateFromU3E5__3_6; }
	inline void set_U3CrotateFromU3E5__3_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CrotateFromU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CrotateToU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CrotateToU3E5__4_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CrotateToU3E5__4_7() const { return ___U3CrotateToU3E5__4_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CrotateToU3E5__4_7() { return &___U3CrotateToU3E5__4_7; }
	inline void set_U3CrotateToU3E5__4_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CrotateToU3E5__4_7 = value;
	}

	inline static int32_t get_offset_of_U3CscaleFromU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CscaleFromU3E5__5_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CscaleFromU3E5__5_8() const { return ___U3CscaleFromU3E5__5_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CscaleFromU3E5__5_8() { return &___U3CscaleFromU3E5__5_8; }
	inline void set_U3CscaleFromU3E5__5_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CscaleFromU3E5__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CscaleToU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CscaleToU3E5__6_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CscaleToU3E5__6_9() const { return ___U3CscaleToU3E5__6_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CscaleToU3E5__6_9() { return &___U3CscaleToU3E5__6_9; }
	inline void set_U3CscaleToU3E5__6_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CscaleToU3E5__6_9 = value;
	}

	inline static int32_t get_offset_of_U3CfadeFromU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CfadeFromU3E5__7_10)); }
	inline float get_U3CfadeFromU3E5__7_10() const { return ___U3CfadeFromU3E5__7_10; }
	inline float* get_address_of_U3CfadeFromU3E5__7_10() { return &___U3CfadeFromU3E5__7_10; }
	inline void set_U3CfadeFromU3E5__7_10(float value)
	{
		___U3CfadeFromU3E5__7_10 = value;
	}

	inline static int32_t get_offset_of_U3CfadeToU3E5__8_11() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CfadeToU3E5__8_11)); }
	inline float get_U3CfadeToU3E5__8_11() const { return ___U3CfadeToU3E5__8_11; }
	inline float* get_address_of_U3CfadeToU3E5__8_11() { return &___U3CfadeToU3E5__8_11; }
	inline void set_U3CfadeToU3E5__8_11(float value)
	{
		___U3CfadeToU3E5__8_11 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__9_12() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CstartTimeU3E5__9_12)); }
	inline float get_U3CstartTimeU3E5__9_12() const { return ___U3CstartTimeU3E5__9_12; }
	inline float* get_address_of_U3CstartTimeU3E5__9_12() { return &___U3CstartTimeU3E5__9_12; }
	inline void set_U3CstartTimeU3E5__9_12(float value)
	{
		___U3CstartTimeU3E5__9_12 = value;
	}

	inline static int32_t get_offset_of_U3CtotalDurationU3E5__10_13() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CtotalDurationU3E5__10_13)); }
	inline float get_U3CtotalDurationU3E5__10_13() const { return ___U3CtotalDurationU3E5__10_13; }
	inline float* get_address_of_U3CtotalDurationU3E5__10_13() { return &___U3CtotalDurationU3E5__10_13; }
	inline void set_U3CtotalDurationU3E5__10_13(float value)
	{
		___U3CtotalDurationU3E5__10_13 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__11_14() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6, ___U3CelapsedTimeU3E5__11_14)); }
	inline float get_U3CelapsedTimeU3E5__11_14() const { return ___U3CelapsedTimeU3E5__11_14; }
	inline float* get_address_of_U3CelapsedTimeU3E5__11_14() { return &___U3CelapsedTimeU3E5__11_14; }
	inline void set_U3CelapsedTimeU3E5__11_14(float value)
	{
		___U3CelapsedTimeU3E5__11_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIDEENUMERATORU3ED__109_T6F6B6DA4DE92700AF492FD340D870480E9DC59C6_H
#ifndef U3CSHOWENUMERATORU3ED__108_T230BEBFB042A62D9A283B941A77E258F03F8F7E4_H
#define U3CSHOWENUMERATORU3ED__108_T230BEBFB042A62D9A283B941A77E258F03F8F7E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108
struct  U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Boolean Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::instantAction
	bool ___instantAction_2;
	// Doozy.Engine.UI.UIPopup Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<>4__this
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * ___U3CU3E4__this_3;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<moveFrom>5__1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CmoveFromU3E5__1_4;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<moveTo>5__2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CmoveToU3E5__2_5;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<rotateFrom>5__3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CrotateFromU3E5__3_6;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<rotateTo>5__4
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CrotateToU3E5__4_7;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<scaleFrom>5__5
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CscaleFromU3E5__5_8;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<scaleTo>5__6
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CscaleToU3E5__6_9;
	// System.Single Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<fadeFrom>5__7
	float ___U3CfadeFromU3E5__7_10;
	// System.Single Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<fadeTo>5__8
	float ___U3CfadeToU3E5__8_11;
	// System.Single Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<startTime>5__9
	float ___U3CstartTimeU3E5__9_12;
	// System.Single Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<totalDuration>5__10
	float ___U3CtotalDurationU3E5__10_13;
	// System.Single Doozy.Engine.UI.UIPopup_<ShowEnumerator>d__108::<elapsedTime>5__11
	float ___U3CelapsedTimeU3E5__11_14;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_instantAction_2() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___instantAction_2)); }
	inline bool get_instantAction_2() const { return ___instantAction_2; }
	inline bool* get_address_of_instantAction_2() { return &___instantAction_2; }
	inline void set_instantAction_2(bool value)
	{
		___instantAction_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CU3E4__this_3)); }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CmoveFromU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CmoveFromU3E5__1_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CmoveFromU3E5__1_4() const { return ___U3CmoveFromU3E5__1_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CmoveFromU3E5__1_4() { return &___U3CmoveFromU3E5__1_4; }
	inline void set_U3CmoveFromU3E5__1_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CmoveFromU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CmoveToU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CmoveToU3E5__2_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CmoveToU3E5__2_5() const { return ___U3CmoveToU3E5__2_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CmoveToU3E5__2_5() { return &___U3CmoveToU3E5__2_5; }
	inline void set_U3CmoveToU3E5__2_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CmoveToU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CrotateFromU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CrotateFromU3E5__3_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CrotateFromU3E5__3_6() const { return ___U3CrotateFromU3E5__3_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CrotateFromU3E5__3_6() { return &___U3CrotateFromU3E5__3_6; }
	inline void set_U3CrotateFromU3E5__3_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CrotateFromU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CrotateToU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CrotateToU3E5__4_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CrotateToU3E5__4_7() const { return ___U3CrotateToU3E5__4_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CrotateToU3E5__4_7() { return &___U3CrotateToU3E5__4_7; }
	inline void set_U3CrotateToU3E5__4_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CrotateToU3E5__4_7 = value;
	}

	inline static int32_t get_offset_of_U3CscaleFromU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CscaleFromU3E5__5_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CscaleFromU3E5__5_8() const { return ___U3CscaleFromU3E5__5_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CscaleFromU3E5__5_8() { return &___U3CscaleFromU3E5__5_8; }
	inline void set_U3CscaleFromU3E5__5_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CscaleFromU3E5__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CscaleToU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CscaleToU3E5__6_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CscaleToU3E5__6_9() const { return ___U3CscaleToU3E5__6_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CscaleToU3E5__6_9() { return &___U3CscaleToU3E5__6_9; }
	inline void set_U3CscaleToU3E5__6_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CscaleToU3E5__6_9 = value;
	}

	inline static int32_t get_offset_of_U3CfadeFromU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CfadeFromU3E5__7_10)); }
	inline float get_U3CfadeFromU3E5__7_10() const { return ___U3CfadeFromU3E5__7_10; }
	inline float* get_address_of_U3CfadeFromU3E5__7_10() { return &___U3CfadeFromU3E5__7_10; }
	inline void set_U3CfadeFromU3E5__7_10(float value)
	{
		___U3CfadeFromU3E5__7_10 = value;
	}

	inline static int32_t get_offset_of_U3CfadeToU3E5__8_11() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CfadeToU3E5__8_11)); }
	inline float get_U3CfadeToU3E5__8_11() const { return ___U3CfadeToU3E5__8_11; }
	inline float* get_address_of_U3CfadeToU3E5__8_11() { return &___U3CfadeToU3E5__8_11; }
	inline void set_U3CfadeToU3E5__8_11(float value)
	{
		___U3CfadeToU3E5__8_11 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__9_12() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CstartTimeU3E5__9_12)); }
	inline float get_U3CstartTimeU3E5__9_12() const { return ___U3CstartTimeU3E5__9_12; }
	inline float* get_address_of_U3CstartTimeU3E5__9_12() { return &___U3CstartTimeU3E5__9_12; }
	inline void set_U3CstartTimeU3E5__9_12(float value)
	{
		___U3CstartTimeU3E5__9_12 = value;
	}

	inline static int32_t get_offset_of_U3CtotalDurationU3E5__10_13() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CtotalDurationU3E5__10_13)); }
	inline float get_U3CtotalDurationU3E5__10_13() const { return ___U3CtotalDurationU3E5__10_13; }
	inline float* get_address_of_U3CtotalDurationU3E5__10_13() { return &___U3CtotalDurationU3E5__10_13; }
	inline void set_U3CtotalDurationU3E5__10_13(float value)
	{
		___U3CtotalDurationU3E5__10_13 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__11_14() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4, ___U3CelapsedTimeU3E5__11_14)); }
	inline float get_U3CelapsedTimeU3E5__11_14() const { return ___U3CelapsedTimeU3E5__11_14; }
	inline float* get_address_of_U3CelapsedTimeU3E5__11_14() { return &___U3CelapsedTimeU3E5__11_14; }
	inline void set_U3CelapsedTimeU3E5__11_14(float value)
	{
		___U3CelapsedTimeU3E5__11_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWENUMERATORU3ED__108_T230BEBFB042A62D9A283B941A77E258F03F8F7E4_H
#ifndef UITOGGLEBEHAVIORTYPE_T0AAFDB4A4F415B675CBC7767466D63C5E4D8550A_H
#define UITOGGLEBEHAVIORTYPE_T0AAFDB4A4F415B675CBC7767466D63C5E4D8550A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggleBehaviorType
struct  UIToggleBehaviorType_t0AAFDB4A4F415B675CBC7767466D63C5E4D8550A 
{
public:
	// System.Int32 Doozy.Engine.UI.UIToggleBehaviorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIToggleBehaviorType_t0AAFDB4A4F415B675CBC7767466D63C5E4D8550A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLEBEHAVIORTYPE_T0AAFDB4A4F415B675CBC7767466D63C5E4D8550A_H
#ifndef UITOGGLEEVENT_TBF355DCF93DCBA057A8DD70BE2B783E2D88EEE5C_H
#define UITOGGLEEVENT_TBF355DCF93DCBA057A8DD70BE2B783E2D88EEE5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggleEvent
struct  UIToggleEvent_tBF355DCF93DCBA057A8DD70BE2B783E2D88EEE5C  : public UnityEvent_1_t8A1FFF79A8DAE5DB41C1D884E9B294756B624905
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLEEVENT_TBF355DCF93DCBA057A8DD70BE2B783E2D88EEE5C_H
#ifndef UITOGGLESTATE_T0A73CA396396F36C7DBCC37E27DF02838BE17D8E_H
#define UITOGGLESTATE_T0A73CA396396F36C7DBCC37E27DF02838BE17D8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggleState
struct  UIToggleState_t0A73CA396396F36C7DBCC37E27DF02838BE17D8E 
{
public:
	// System.Int32 Doozy.Engine.UI.UIToggleState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIToggleState_t0A73CA396396F36C7DBCC37E27DF02838BE17D8E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLESTATE_T0A73CA396396F36C7DBCC37E27DF02838BE17D8E_H
#ifndef U3CHIDEENUMERATORU3ED__97_T049B4349E56593055F55AB706DE3A75FAFE075A1_H
#define U3CHIDEENUMERATORU3ED__97_T049B4349E56593055F55AB706DE3A75FAFE075A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIView_<HideEnumerator>d__97
struct  U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Boolean Doozy.Engine.UI.UIView_<HideEnumerator>d__97::instantAction
	bool ___instantAction_2;
	// Doozy.Engine.UI.UIView Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<>4__this
	UIView_t65D38836246049951821D4E45B1C81947591EBDF * ___U3CU3E4__this_3;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<moveFrom>5__1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CmoveFromU3E5__1_4;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<moveTo>5__2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CmoveToU3E5__2_5;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<rotateFrom>5__3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CrotateFromU3E5__3_6;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<rotateTo>5__4
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CrotateToU3E5__4_7;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<scaleFrom>5__5
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CscaleFromU3E5__5_8;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<scaleTo>5__6
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CscaleToU3E5__6_9;
	// System.Single Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<fadeFrom>5__7
	float ___U3CfadeFromU3E5__7_10;
	// System.Single Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<fadeTo>5__8
	float ___U3CfadeToU3E5__8_11;
	// System.Single Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<startTime>5__9
	float ___U3CstartTimeU3E5__9_12;
	// System.Single Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<totalDuration>5__10
	float ___U3CtotalDurationU3E5__10_13;
	// System.Single Doozy.Engine.UI.UIView_<HideEnumerator>d__97::<elapsedTime>5__11
	float ___U3CelapsedTimeU3E5__11_14;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_instantAction_2() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___instantAction_2)); }
	inline bool get_instantAction_2() const { return ___instantAction_2; }
	inline bool* get_address_of_instantAction_2() { return &___instantAction_2; }
	inline void set_instantAction_2(bool value)
	{
		___instantAction_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CU3E4__this_3)); }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIView_t65D38836246049951821D4E45B1C81947591EBDF * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CmoveFromU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CmoveFromU3E5__1_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CmoveFromU3E5__1_4() const { return ___U3CmoveFromU3E5__1_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CmoveFromU3E5__1_4() { return &___U3CmoveFromU3E5__1_4; }
	inline void set_U3CmoveFromU3E5__1_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CmoveFromU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CmoveToU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CmoveToU3E5__2_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CmoveToU3E5__2_5() const { return ___U3CmoveToU3E5__2_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CmoveToU3E5__2_5() { return &___U3CmoveToU3E5__2_5; }
	inline void set_U3CmoveToU3E5__2_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CmoveToU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CrotateFromU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CrotateFromU3E5__3_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CrotateFromU3E5__3_6() const { return ___U3CrotateFromU3E5__3_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CrotateFromU3E5__3_6() { return &___U3CrotateFromU3E5__3_6; }
	inline void set_U3CrotateFromU3E5__3_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CrotateFromU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CrotateToU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CrotateToU3E5__4_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CrotateToU3E5__4_7() const { return ___U3CrotateToU3E5__4_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CrotateToU3E5__4_7() { return &___U3CrotateToU3E5__4_7; }
	inline void set_U3CrotateToU3E5__4_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CrotateToU3E5__4_7 = value;
	}

	inline static int32_t get_offset_of_U3CscaleFromU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CscaleFromU3E5__5_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CscaleFromU3E5__5_8() const { return ___U3CscaleFromU3E5__5_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CscaleFromU3E5__5_8() { return &___U3CscaleFromU3E5__5_8; }
	inline void set_U3CscaleFromU3E5__5_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CscaleFromU3E5__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CscaleToU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CscaleToU3E5__6_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CscaleToU3E5__6_9() const { return ___U3CscaleToU3E5__6_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CscaleToU3E5__6_9() { return &___U3CscaleToU3E5__6_9; }
	inline void set_U3CscaleToU3E5__6_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CscaleToU3E5__6_9 = value;
	}

	inline static int32_t get_offset_of_U3CfadeFromU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CfadeFromU3E5__7_10)); }
	inline float get_U3CfadeFromU3E5__7_10() const { return ___U3CfadeFromU3E5__7_10; }
	inline float* get_address_of_U3CfadeFromU3E5__7_10() { return &___U3CfadeFromU3E5__7_10; }
	inline void set_U3CfadeFromU3E5__7_10(float value)
	{
		___U3CfadeFromU3E5__7_10 = value;
	}

	inline static int32_t get_offset_of_U3CfadeToU3E5__8_11() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CfadeToU3E5__8_11)); }
	inline float get_U3CfadeToU3E5__8_11() const { return ___U3CfadeToU3E5__8_11; }
	inline float* get_address_of_U3CfadeToU3E5__8_11() { return &___U3CfadeToU3E5__8_11; }
	inline void set_U3CfadeToU3E5__8_11(float value)
	{
		___U3CfadeToU3E5__8_11 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__9_12() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CstartTimeU3E5__9_12)); }
	inline float get_U3CstartTimeU3E5__9_12() const { return ___U3CstartTimeU3E5__9_12; }
	inline float* get_address_of_U3CstartTimeU3E5__9_12() { return &___U3CstartTimeU3E5__9_12; }
	inline void set_U3CstartTimeU3E5__9_12(float value)
	{
		___U3CstartTimeU3E5__9_12 = value;
	}

	inline static int32_t get_offset_of_U3CtotalDurationU3E5__10_13() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CtotalDurationU3E5__10_13)); }
	inline float get_U3CtotalDurationU3E5__10_13() const { return ___U3CtotalDurationU3E5__10_13; }
	inline float* get_address_of_U3CtotalDurationU3E5__10_13() { return &___U3CtotalDurationU3E5__10_13; }
	inline void set_U3CtotalDurationU3E5__10_13(float value)
	{
		___U3CtotalDurationU3E5__10_13 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__11_14() { return static_cast<int32_t>(offsetof(U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1, ___U3CelapsedTimeU3E5__11_14)); }
	inline float get_U3CelapsedTimeU3E5__11_14() const { return ___U3CelapsedTimeU3E5__11_14; }
	inline float* get_address_of_U3CelapsedTimeU3E5__11_14() { return &___U3CelapsedTimeU3E5__11_14; }
	inline void set_U3CelapsedTimeU3E5__11_14(float value)
	{
		___U3CelapsedTimeU3E5__11_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIDEENUMERATORU3ED__97_T049B4349E56593055F55AB706DE3A75FAFE075A1_H
#ifndef U3CSHOWENUMERATORU3ED__96_TC69583128583639634BE4ADB32A327ED540F981D_H
#define U3CSHOWENUMERATORU3ED__96_TC69583128583639634BE4ADB32A327ED540F981D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIView_<ShowEnumerator>d__96
struct  U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D  : public RuntimeObject
{
public:
	// System.Int32 Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Boolean Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::instantAction
	bool ___instantAction_2;
	// Doozy.Engine.UI.UIView Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<>4__this
	UIView_t65D38836246049951821D4E45B1C81947591EBDF * ___U3CU3E4__this_3;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<moveFrom>5__1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CmoveFromU3E5__1_4;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<moveTo>5__2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CmoveToU3E5__2_5;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<rotateFrom>5__3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CrotateFromU3E5__3_6;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<rotateTo>5__4
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CrotateToU3E5__4_7;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<scaleFrom>5__5
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CscaleFromU3E5__5_8;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<scaleTo>5__6
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CscaleToU3E5__6_9;
	// System.Single Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<fadeFrom>5__7
	float ___U3CfadeFromU3E5__7_10;
	// System.Single Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<fadeTo>5__8
	float ___U3CfadeToU3E5__8_11;
	// System.Single Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<startTime>5__9
	float ___U3CstartTimeU3E5__9_12;
	// System.Single Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<totalDuration>5__10
	float ___U3CtotalDurationU3E5__10_13;
	// System.Single Doozy.Engine.UI.UIView_<ShowEnumerator>d__96::<elapsedTime>5__11
	float ___U3CelapsedTimeU3E5__11_14;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_instantAction_2() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___instantAction_2)); }
	inline bool get_instantAction_2() const { return ___instantAction_2; }
	inline bool* get_address_of_instantAction_2() { return &___instantAction_2; }
	inline void set_instantAction_2(bool value)
	{
		___instantAction_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CU3E4__this_3)); }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UIView_t65D38836246049951821D4E45B1C81947591EBDF ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UIView_t65D38836246049951821D4E45B1C81947591EBDF * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CmoveFromU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CmoveFromU3E5__1_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CmoveFromU3E5__1_4() const { return ___U3CmoveFromU3E5__1_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CmoveFromU3E5__1_4() { return &___U3CmoveFromU3E5__1_4; }
	inline void set_U3CmoveFromU3E5__1_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CmoveFromU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CmoveToU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CmoveToU3E5__2_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CmoveToU3E5__2_5() const { return ___U3CmoveToU3E5__2_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CmoveToU3E5__2_5() { return &___U3CmoveToU3E5__2_5; }
	inline void set_U3CmoveToU3E5__2_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CmoveToU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CrotateFromU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CrotateFromU3E5__3_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CrotateFromU3E5__3_6() const { return ___U3CrotateFromU3E5__3_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CrotateFromU3E5__3_6() { return &___U3CrotateFromU3E5__3_6; }
	inline void set_U3CrotateFromU3E5__3_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CrotateFromU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CrotateToU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CrotateToU3E5__4_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CrotateToU3E5__4_7() const { return ___U3CrotateToU3E5__4_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CrotateToU3E5__4_7() { return &___U3CrotateToU3E5__4_7; }
	inline void set_U3CrotateToU3E5__4_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CrotateToU3E5__4_7 = value;
	}

	inline static int32_t get_offset_of_U3CscaleFromU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CscaleFromU3E5__5_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CscaleFromU3E5__5_8() const { return ___U3CscaleFromU3E5__5_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CscaleFromU3E5__5_8() { return &___U3CscaleFromU3E5__5_8; }
	inline void set_U3CscaleFromU3E5__5_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CscaleFromU3E5__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CscaleToU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CscaleToU3E5__6_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CscaleToU3E5__6_9() const { return ___U3CscaleToU3E5__6_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CscaleToU3E5__6_9() { return &___U3CscaleToU3E5__6_9; }
	inline void set_U3CscaleToU3E5__6_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CscaleToU3E5__6_9 = value;
	}

	inline static int32_t get_offset_of_U3CfadeFromU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CfadeFromU3E5__7_10)); }
	inline float get_U3CfadeFromU3E5__7_10() const { return ___U3CfadeFromU3E5__7_10; }
	inline float* get_address_of_U3CfadeFromU3E5__7_10() { return &___U3CfadeFromU3E5__7_10; }
	inline void set_U3CfadeFromU3E5__7_10(float value)
	{
		___U3CfadeFromU3E5__7_10 = value;
	}

	inline static int32_t get_offset_of_U3CfadeToU3E5__8_11() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CfadeToU3E5__8_11)); }
	inline float get_U3CfadeToU3E5__8_11() const { return ___U3CfadeToU3E5__8_11; }
	inline float* get_address_of_U3CfadeToU3E5__8_11() { return &___U3CfadeToU3E5__8_11; }
	inline void set_U3CfadeToU3E5__8_11(float value)
	{
		___U3CfadeToU3E5__8_11 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__9_12() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CstartTimeU3E5__9_12)); }
	inline float get_U3CstartTimeU3E5__9_12() const { return ___U3CstartTimeU3E5__9_12; }
	inline float* get_address_of_U3CstartTimeU3E5__9_12() { return &___U3CstartTimeU3E5__9_12; }
	inline void set_U3CstartTimeU3E5__9_12(float value)
	{
		___U3CstartTimeU3E5__9_12 = value;
	}

	inline static int32_t get_offset_of_U3CtotalDurationU3E5__10_13() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CtotalDurationU3E5__10_13)); }
	inline float get_U3CtotalDurationU3E5__10_13() const { return ___U3CtotalDurationU3E5__10_13; }
	inline float* get_address_of_U3CtotalDurationU3E5__10_13() { return &___U3CtotalDurationU3E5__10_13; }
	inline void set_U3CtotalDurationU3E5__10_13(float value)
	{
		___U3CtotalDurationU3E5__10_13 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__11_14() { return static_cast<int32_t>(offsetof(U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D, ___U3CelapsedTimeU3E5__11_14)); }
	inline float get_U3CelapsedTimeU3E5__11_14() const { return ___U3CelapsedTimeU3E5__11_14; }
	inline float* get_address_of_U3CelapsedTimeU3E5__11_14() { return &___U3CelapsedTimeU3E5__11_14; }
	inline void set_U3CelapsedTimeU3E5__11_14(float value)
	{
		___U3CelapsedTimeU3E5__11_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWENUMERATORU3ED__96_TC69583128583639634BE4ADB32A327ED540F981D_H
#ifndef UIVIEWBEHAVIORTYPE_T652DB3CABD15D72ADC5D734651A8CAD49C8B0ECB_H
#define UIVIEWBEHAVIORTYPE_T652DB3CABD15D72ADC5D734651A8CAD49C8B0ECB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIViewBehaviorType
struct  UIViewBehaviorType_t652DB3CABD15D72ADC5D734651A8CAD49C8B0ECB 
{
public:
	// System.Int32 Doozy.Engine.UI.UIViewBehaviorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIViewBehaviorType_t652DB3CABD15D72ADC5D734651A8CAD49C8B0ECB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWBEHAVIORTYPE_T652DB3CABD15D72ADC5D734651A8CAD49C8B0ECB_H
#ifndef UIVIEWEVENT_T5DBF09498B7D574591E73E4A18578AE6CCE35F29_H
#define UIVIEWEVENT_T5DBF09498B7D574591E73E4A18578AE6CCE35F29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIViewEvent
struct  UIViewEvent_t5DBF09498B7D574591E73E4A18578AE6CCE35F29  : public UnityEvent_1_t080E4DBB31A1D05BAC21C0C0509158A8333B8BC1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWEVENT_T5DBF09498B7D574591E73E4A18578AE6CCE35F29_H
#ifndef UIVIEWSTARTBEHAVIOR_T1C1D97FA8BECE7BDEAC82D7BA95BE81C411619B0_H
#define UIVIEWSTARTBEHAVIOR_T1C1D97FA8BECE7BDEAC82D7BA95BE81C411619B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIViewStartBehavior
struct  UIViewStartBehavior_t1C1D97FA8BECE7BDEAC82D7BA95BE81C411619B0 
{
public:
	// System.Int32 Doozy.Engine.UI.UIViewStartBehavior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIViewStartBehavior_t1C1D97FA8BECE7BDEAC82D7BA95BE81C411619B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWSTARTBEHAVIOR_T1C1D97FA8BECE7BDEAC82D7BA95BE81C411619B0_H
#ifndef VISIBILITYSTATE_T57C44223D66727F9AEBFE2E0EDDD2895E58C296D_H
#define VISIBILITYSTATE_T57C44223D66727F9AEBFE2E0EDDD2895E58C296D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.VisibilityState
struct  VisibilityState_t57C44223D66727F9AEBFE2E0EDDD2895E58C296D 
{
public:
	// System.Int32 Doozy.Engine.UI.VisibilityState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VisibilityState_t57C44223D66727F9AEBFE2E0EDDD2895E58C296D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITYSTATE_T57C44223D66727F9AEBFE2E0EDDD2895E58C296D_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef UIBUTTONBEHAVIOR_T08AD16D7696D1DE757449CCA5629CF91DC7989BB_H
#define UIBUTTONBEHAVIOR_T08AD16D7696D1DE757449CCA5629CF91DC7989BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButtonBehavior
struct  UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Doozy.Engine.Events.AnimatorEvent> Doozy.Engine.UI.UIButtonBehavior::Animators
	List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5 * ___Animators_16;
	// Doozy.Engine.UI.Animation.ButtonAnimationType Doozy.Engine.UI.UIButtonBehavior::ButtonAnimationType
	int32_t ___ButtonAnimationType_17;
	// System.Boolean Doozy.Engine.UI.UIButtonBehavior::DeselectButton
	bool ___DeselectButton_18;
	// System.Single Doozy.Engine.UI.UIButtonBehavior::DisableInterval
	float ___DisableInterval_19;
	// System.Boolean Doozy.Engine.UI.UIButtonBehavior::Enabled
	bool ___Enabled_20;
	// System.Boolean Doozy.Engine.UI.UIButtonBehavior::LoadSelectedPresetAtRuntime
	bool ___LoadSelectedPresetAtRuntime_21;
	// Doozy.Engine.UI.Base.UIAction Doozy.Engine.UI.UIButtonBehavior::OnTrigger
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * ___OnTrigger_22;
	// System.String Doozy.Engine.UI.UIButtonBehavior::PresetCategory
	String_t* ___PresetCategory_23;
	// System.String Doozy.Engine.UI.UIButtonBehavior::PresetName
	String_t* ___PresetName_24;
	// Doozy.Engine.UI.Animation.UIAnimation Doozy.Engine.UI.UIButtonBehavior::PunchAnimation
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * ___PunchAnimation_25;
	// System.Boolean Doozy.Engine.UI.UIButtonBehavior::Ready
	bool ___Ready_26;
	// System.Boolean Doozy.Engine.UI.UIButtonBehavior::SelectButton
	bool ___SelectButton_27;
	// Doozy.Engine.UI.Animation.UIAnimation Doozy.Engine.UI.UIButtonBehavior::StateAnimation
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * ___StateAnimation_28;
	// System.Boolean Doozy.Engine.UI.UIButtonBehavior::TriggerEventsAfterAnimation
	bool ___TriggerEventsAfterAnimation_29;
	// Doozy.Engine.UI.UIButtonBehaviorType Doozy.Engine.UI.UIButtonBehavior::m_behaviorType
	int32_t ___m_behaviorType_30;

public:
	inline static int32_t get_offset_of_Animators_16() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___Animators_16)); }
	inline List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5 * get_Animators_16() const { return ___Animators_16; }
	inline List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5 ** get_address_of_Animators_16() { return &___Animators_16; }
	inline void set_Animators_16(List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5 * value)
	{
		___Animators_16 = value;
		Il2CppCodeGenWriteBarrier((&___Animators_16), value);
	}

	inline static int32_t get_offset_of_ButtonAnimationType_17() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___ButtonAnimationType_17)); }
	inline int32_t get_ButtonAnimationType_17() const { return ___ButtonAnimationType_17; }
	inline int32_t* get_address_of_ButtonAnimationType_17() { return &___ButtonAnimationType_17; }
	inline void set_ButtonAnimationType_17(int32_t value)
	{
		___ButtonAnimationType_17 = value;
	}

	inline static int32_t get_offset_of_DeselectButton_18() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___DeselectButton_18)); }
	inline bool get_DeselectButton_18() const { return ___DeselectButton_18; }
	inline bool* get_address_of_DeselectButton_18() { return &___DeselectButton_18; }
	inline void set_DeselectButton_18(bool value)
	{
		___DeselectButton_18 = value;
	}

	inline static int32_t get_offset_of_DisableInterval_19() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___DisableInterval_19)); }
	inline float get_DisableInterval_19() const { return ___DisableInterval_19; }
	inline float* get_address_of_DisableInterval_19() { return &___DisableInterval_19; }
	inline void set_DisableInterval_19(float value)
	{
		___DisableInterval_19 = value;
	}

	inline static int32_t get_offset_of_Enabled_20() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___Enabled_20)); }
	inline bool get_Enabled_20() const { return ___Enabled_20; }
	inline bool* get_address_of_Enabled_20() { return &___Enabled_20; }
	inline void set_Enabled_20(bool value)
	{
		___Enabled_20 = value;
	}

	inline static int32_t get_offset_of_LoadSelectedPresetAtRuntime_21() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___LoadSelectedPresetAtRuntime_21)); }
	inline bool get_LoadSelectedPresetAtRuntime_21() const { return ___LoadSelectedPresetAtRuntime_21; }
	inline bool* get_address_of_LoadSelectedPresetAtRuntime_21() { return &___LoadSelectedPresetAtRuntime_21; }
	inline void set_LoadSelectedPresetAtRuntime_21(bool value)
	{
		___LoadSelectedPresetAtRuntime_21 = value;
	}

	inline static int32_t get_offset_of_OnTrigger_22() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___OnTrigger_22)); }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * get_OnTrigger_22() const { return ___OnTrigger_22; }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 ** get_address_of_OnTrigger_22() { return &___OnTrigger_22; }
	inline void set_OnTrigger_22(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * value)
	{
		___OnTrigger_22 = value;
		Il2CppCodeGenWriteBarrier((&___OnTrigger_22), value);
	}

	inline static int32_t get_offset_of_PresetCategory_23() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___PresetCategory_23)); }
	inline String_t* get_PresetCategory_23() const { return ___PresetCategory_23; }
	inline String_t** get_address_of_PresetCategory_23() { return &___PresetCategory_23; }
	inline void set_PresetCategory_23(String_t* value)
	{
		___PresetCategory_23 = value;
		Il2CppCodeGenWriteBarrier((&___PresetCategory_23), value);
	}

	inline static int32_t get_offset_of_PresetName_24() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___PresetName_24)); }
	inline String_t* get_PresetName_24() const { return ___PresetName_24; }
	inline String_t** get_address_of_PresetName_24() { return &___PresetName_24; }
	inline void set_PresetName_24(String_t* value)
	{
		___PresetName_24 = value;
		Il2CppCodeGenWriteBarrier((&___PresetName_24), value);
	}

	inline static int32_t get_offset_of_PunchAnimation_25() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___PunchAnimation_25)); }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * get_PunchAnimation_25() const { return ___PunchAnimation_25; }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C ** get_address_of_PunchAnimation_25() { return &___PunchAnimation_25; }
	inline void set_PunchAnimation_25(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * value)
	{
		___PunchAnimation_25 = value;
		Il2CppCodeGenWriteBarrier((&___PunchAnimation_25), value);
	}

	inline static int32_t get_offset_of_Ready_26() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___Ready_26)); }
	inline bool get_Ready_26() const { return ___Ready_26; }
	inline bool* get_address_of_Ready_26() { return &___Ready_26; }
	inline void set_Ready_26(bool value)
	{
		___Ready_26 = value;
	}

	inline static int32_t get_offset_of_SelectButton_27() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___SelectButton_27)); }
	inline bool get_SelectButton_27() const { return ___SelectButton_27; }
	inline bool* get_address_of_SelectButton_27() { return &___SelectButton_27; }
	inline void set_SelectButton_27(bool value)
	{
		___SelectButton_27 = value;
	}

	inline static int32_t get_offset_of_StateAnimation_28() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___StateAnimation_28)); }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * get_StateAnimation_28() const { return ___StateAnimation_28; }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C ** get_address_of_StateAnimation_28() { return &___StateAnimation_28; }
	inline void set_StateAnimation_28(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * value)
	{
		___StateAnimation_28 = value;
		Il2CppCodeGenWriteBarrier((&___StateAnimation_28), value);
	}

	inline static int32_t get_offset_of_TriggerEventsAfterAnimation_29() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___TriggerEventsAfterAnimation_29)); }
	inline bool get_TriggerEventsAfterAnimation_29() const { return ___TriggerEventsAfterAnimation_29; }
	inline bool* get_address_of_TriggerEventsAfterAnimation_29() { return &___TriggerEventsAfterAnimation_29; }
	inline void set_TriggerEventsAfterAnimation_29(bool value)
	{
		___TriggerEventsAfterAnimation_29 = value;
	}

	inline static int32_t get_offset_of_m_behaviorType_30() { return static_cast<int32_t>(offsetof(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB, ___m_behaviorType_30)); }
	inline int32_t get_m_behaviorType_30() const { return ___m_behaviorType_30; }
	inline int32_t* get_address_of_m_behaviorType_30() { return &___m_behaviorType_30; }
	inline void set_m_behaviorType_30(int32_t value)
	{
		___m_behaviorType_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONBEHAVIOR_T08AD16D7696D1DE757449CCA5629CF91DC7989BB_H
#ifndef UIBUTTONLOOPANIMATION_T1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45_H
#define UIBUTTONLOOPANIMATION_T1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButtonLoopAnimation
struct  UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.Animation.UIAnimation Doozy.Engine.UI.UIButtonLoopAnimation::Animation
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * ___Animation_0;
	// System.Boolean Doozy.Engine.UI.UIButtonLoopAnimation::Enabled
	bool ___Enabled_1;
	// System.Boolean Doozy.Engine.UI.UIButtonLoopAnimation::IsPlaying
	bool ___IsPlaying_2;
	// Doozy.Engine.UI.Animation.ButtonLoopAnimationType Doozy.Engine.UI.UIButtonLoopAnimation::LoopAnimationType
	int32_t ___LoopAnimationType_3;
	// System.Boolean Doozy.Engine.UI.UIButtonLoopAnimation::LoadSelectedPresetAtRuntime
	bool ___LoadSelectedPresetAtRuntime_4;
	// System.String Doozy.Engine.UI.UIButtonLoopAnimation::PresetCategory
	String_t* ___PresetCategory_5;
	// System.String Doozy.Engine.UI.UIButtonLoopAnimation::PresetName
	String_t* ___PresetName_6;

public:
	inline static int32_t get_offset_of_Animation_0() { return static_cast<int32_t>(offsetof(UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45, ___Animation_0)); }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * get_Animation_0() const { return ___Animation_0; }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C ** get_address_of_Animation_0() { return &___Animation_0; }
	inline void set_Animation_0(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * value)
	{
		___Animation_0 = value;
		Il2CppCodeGenWriteBarrier((&___Animation_0), value);
	}

	inline static int32_t get_offset_of_Enabled_1() { return static_cast<int32_t>(offsetof(UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45, ___Enabled_1)); }
	inline bool get_Enabled_1() const { return ___Enabled_1; }
	inline bool* get_address_of_Enabled_1() { return &___Enabled_1; }
	inline void set_Enabled_1(bool value)
	{
		___Enabled_1 = value;
	}

	inline static int32_t get_offset_of_IsPlaying_2() { return static_cast<int32_t>(offsetof(UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45, ___IsPlaying_2)); }
	inline bool get_IsPlaying_2() const { return ___IsPlaying_2; }
	inline bool* get_address_of_IsPlaying_2() { return &___IsPlaying_2; }
	inline void set_IsPlaying_2(bool value)
	{
		___IsPlaying_2 = value;
	}

	inline static int32_t get_offset_of_LoopAnimationType_3() { return static_cast<int32_t>(offsetof(UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45, ___LoopAnimationType_3)); }
	inline int32_t get_LoopAnimationType_3() const { return ___LoopAnimationType_3; }
	inline int32_t* get_address_of_LoopAnimationType_3() { return &___LoopAnimationType_3; }
	inline void set_LoopAnimationType_3(int32_t value)
	{
		___LoopAnimationType_3 = value;
	}

	inline static int32_t get_offset_of_LoadSelectedPresetAtRuntime_4() { return static_cast<int32_t>(offsetof(UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45, ___LoadSelectedPresetAtRuntime_4)); }
	inline bool get_LoadSelectedPresetAtRuntime_4() const { return ___LoadSelectedPresetAtRuntime_4; }
	inline bool* get_address_of_LoadSelectedPresetAtRuntime_4() { return &___LoadSelectedPresetAtRuntime_4; }
	inline void set_LoadSelectedPresetAtRuntime_4(bool value)
	{
		___LoadSelectedPresetAtRuntime_4 = value;
	}

	inline static int32_t get_offset_of_PresetCategory_5() { return static_cast<int32_t>(offsetof(UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45, ___PresetCategory_5)); }
	inline String_t* get_PresetCategory_5() const { return ___PresetCategory_5; }
	inline String_t** get_address_of_PresetCategory_5() { return &___PresetCategory_5; }
	inline void set_PresetCategory_5(String_t* value)
	{
		___PresetCategory_5 = value;
		Il2CppCodeGenWriteBarrier((&___PresetCategory_5), value);
	}

	inline static int32_t get_offset_of_PresetName_6() { return static_cast<int32_t>(offsetof(UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45, ___PresetName_6)); }
	inline String_t* get_PresetName_6() const { return ___PresetName_6; }
	inline String_t** get_address_of_PresetName_6() { return &___PresetName_6; }
	inline void set_PresetName_6(String_t* value)
	{
		___PresetName_6 = value;
		Il2CppCodeGenWriteBarrier((&___PresetName_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONLOOPANIMATION_T1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45_H
#ifndef UIBUTTONMESSAGE_T61D6B99B7450B7A9F457D4ED5568802137189B2A_H
#define UIBUTTONMESSAGE_T61D6B99B7450B7A9F457D4ED5568802137189B2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButtonMessage
struct  UIButtonMessage_t61D6B99B7450B7A9F457D4ED5568802137189B2A  : public Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E
{
public:
	// Doozy.Engine.UI.UIButton Doozy.Engine.UI.UIButtonMessage::Button
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * ___Button_3;
	// System.String Doozy.Engine.UI.UIButtonMessage::ButtonName
	String_t* ___ButtonName_4;
	// Doozy.Engine.UI.UIButtonBehaviorType Doozy.Engine.UI.UIButtonMessage::Type
	int32_t ___Type_5;

public:
	inline static int32_t get_offset_of_Button_3() { return static_cast<int32_t>(offsetof(UIButtonMessage_t61D6B99B7450B7A9F457D4ED5568802137189B2A, ___Button_3)); }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * get_Button_3() const { return ___Button_3; }
	inline UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 ** get_address_of_Button_3() { return &___Button_3; }
	inline void set_Button_3(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527 * value)
	{
		___Button_3 = value;
		Il2CppCodeGenWriteBarrier((&___Button_3), value);
	}

	inline static int32_t get_offset_of_ButtonName_4() { return static_cast<int32_t>(offsetof(UIButtonMessage_t61D6B99B7450B7A9F457D4ED5568802137189B2A, ___ButtonName_4)); }
	inline String_t* get_ButtonName_4() const { return ___ButtonName_4; }
	inline String_t** get_address_of_ButtonName_4() { return &___ButtonName_4; }
	inline void set_ButtonName_4(String_t* value)
	{
		___ButtonName_4 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonName_4), value);
	}

	inline static int32_t get_offset_of_Type_5() { return static_cast<int32_t>(offsetof(UIButtonMessage_t61D6B99B7450B7A9F457D4ED5568802137189B2A, ___Type_5)); }
	inline int32_t get_Type_5() const { return ___Type_5; }
	inline int32_t* get_address_of_Type_5() { return &___Type_5; }
	inline void set_Type_5(int32_t value)
	{
		___Type_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONMESSAGE_T61D6B99B7450B7A9F457D4ED5568802137189B2A_H
#ifndef UIDRAWERBEHAVIOR_T904272C6FDCA7A411594306A88157DB626250843_H
#define UIDRAWERBEHAVIOR_T904272C6FDCA7A411594306A88157DB626250843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIDrawerBehavior
struct  UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843  : public RuntimeObject
{
public:
	// Doozy.Engine.UI.UIDrawerBehaviorType Doozy.Engine.UI.UIDrawerBehavior::DrawerBehaviorType
	int32_t ___DrawerBehaviorType_0;
	// Doozy.Engine.UI.Base.UIAction Doozy.Engine.UI.UIDrawerBehavior::OnFinished
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * ___OnFinished_1;
	// Doozy.Engine.UI.Base.UIAction Doozy.Engine.UI.UIDrawerBehavior::OnStart
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * ___OnStart_2;

public:
	inline static int32_t get_offset_of_DrawerBehaviorType_0() { return static_cast<int32_t>(offsetof(UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843, ___DrawerBehaviorType_0)); }
	inline int32_t get_DrawerBehaviorType_0() const { return ___DrawerBehaviorType_0; }
	inline int32_t* get_address_of_DrawerBehaviorType_0() { return &___DrawerBehaviorType_0; }
	inline void set_DrawerBehaviorType_0(int32_t value)
	{
		___DrawerBehaviorType_0 = value;
	}

	inline static int32_t get_offset_of_OnFinished_1() { return static_cast<int32_t>(offsetof(UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843, ___OnFinished_1)); }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * get_OnFinished_1() const { return ___OnFinished_1; }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 ** get_address_of_OnFinished_1() { return &___OnFinished_1; }
	inline void set_OnFinished_1(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * value)
	{
		___OnFinished_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinished_1), value);
	}

	inline static int32_t get_offset_of_OnStart_2() { return static_cast<int32_t>(offsetof(UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843, ___OnStart_2)); }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * get_OnStart_2() const { return ___OnStart_2; }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 ** get_address_of_OnStart_2() { return &___OnStart_2; }
	inline void set_OnStart_2(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * value)
	{
		___OnStart_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnStart_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWERBEHAVIOR_T904272C6FDCA7A411594306A88157DB626250843_H
#ifndef UIDRAWERCONTAINER_T6875EA47AF7184E1D7387EC138A4BFFD1E79484C_H
#define UIDRAWERCONTAINER_T6875EA47AF7184E1D7387EC138A4BFFD1E79484C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIDrawerContainer
struct  UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C  : public UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B
{
public:
	// UnityEngine.Vector2 Doozy.Engine.UI.UIDrawerContainer::CalculatedSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___CalculatedSize_21;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIDrawerContainer::ClosedPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___ClosedPosition_22;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIDrawerContainer::CurrentPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___CurrentPosition_23;
	// System.Boolean Doozy.Engine.UI.UIDrawerContainer::FadeOut
	bool ___FadeOut_24;
	// System.Single Doozy.Engine.UI.UIDrawerContainer::FixedSize
	float ___FixedSize_25;
	// System.Single Doozy.Engine.UI.UIDrawerContainer::MinimumSize
	float ___MinimumSize_26;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIDrawerContainer::OpenedPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___OpenedPosition_27;
	// System.Single Doozy.Engine.UI.UIDrawerContainer::PercentageOfScreen
	float ___PercentageOfScreen_28;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIDrawerContainer::PreviousPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___PreviousPosition_29;
	// Doozy.Engine.UI.UIDrawerContainerSize Doozy.Engine.UI.UIDrawerContainer::Size
	int32_t ___Size_30;

public:
	inline static int32_t get_offset_of_CalculatedSize_21() { return static_cast<int32_t>(offsetof(UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C, ___CalculatedSize_21)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_CalculatedSize_21() const { return ___CalculatedSize_21; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_CalculatedSize_21() { return &___CalculatedSize_21; }
	inline void set_CalculatedSize_21(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___CalculatedSize_21 = value;
	}

	inline static int32_t get_offset_of_ClosedPosition_22() { return static_cast<int32_t>(offsetof(UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C, ___ClosedPosition_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_ClosedPosition_22() const { return ___ClosedPosition_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_ClosedPosition_22() { return &___ClosedPosition_22; }
	inline void set_ClosedPosition_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___ClosedPosition_22 = value;
	}

	inline static int32_t get_offset_of_CurrentPosition_23() { return static_cast<int32_t>(offsetof(UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C, ___CurrentPosition_23)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_CurrentPosition_23() const { return ___CurrentPosition_23; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_CurrentPosition_23() { return &___CurrentPosition_23; }
	inline void set_CurrentPosition_23(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___CurrentPosition_23 = value;
	}

	inline static int32_t get_offset_of_FadeOut_24() { return static_cast<int32_t>(offsetof(UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C, ___FadeOut_24)); }
	inline bool get_FadeOut_24() const { return ___FadeOut_24; }
	inline bool* get_address_of_FadeOut_24() { return &___FadeOut_24; }
	inline void set_FadeOut_24(bool value)
	{
		___FadeOut_24 = value;
	}

	inline static int32_t get_offset_of_FixedSize_25() { return static_cast<int32_t>(offsetof(UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C, ___FixedSize_25)); }
	inline float get_FixedSize_25() const { return ___FixedSize_25; }
	inline float* get_address_of_FixedSize_25() { return &___FixedSize_25; }
	inline void set_FixedSize_25(float value)
	{
		___FixedSize_25 = value;
	}

	inline static int32_t get_offset_of_MinimumSize_26() { return static_cast<int32_t>(offsetof(UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C, ___MinimumSize_26)); }
	inline float get_MinimumSize_26() const { return ___MinimumSize_26; }
	inline float* get_address_of_MinimumSize_26() { return &___MinimumSize_26; }
	inline void set_MinimumSize_26(float value)
	{
		___MinimumSize_26 = value;
	}

	inline static int32_t get_offset_of_OpenedPosition_27() { return static_cast<int32_t>(offsetof(UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C, ___OpenedPosition_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_OpenedPosition_27() const { return ___OpenedPosition_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_OpenedPosition_27() { return &___OpenedPosition_27; }
	inline void set_OpenedPosition_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___OpenedPosition_27 = value;
	}

	inline static int32_t get_offset_of_PercentageOfScreen_28() { return static_cast<int32_t>(offsetof(UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C, ___PercentageOfScreen_28)); }
	inline float get_PercentageOfScreen_28() const { return ___PercentageOfScreen_28; }
	inline float* get_address_of_PercentageOfScreen_28() { return &___PercentageOfScreen_28; }
	inline void set_PercentageOfScreen_28(float value)
	{
		___PercentageOfScreen_28 = value;
	}

	inline static int32_t get_offset_of_PreviousPosition_29() { return static_cast<int32_t>(offsetof(UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C, ___PreviousPosition_29)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_PreviousPosition_29() const { return ___PreviousPosition_29; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_PreviousPosition_29() { return &___PreviousPosition_29; }
	inline void set_PreviousPosition_29(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___PreviousPosition_29 = value;
	}

	inline static int32_t get_offset_of_Size_30() { return static_cast<int32_t>(offsetof(UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C, ___Size_30)); }
	inline int32_t get_Size_30() const { return ___Size_30; }
	inline int32_t* get_address_of_Size_30() { return &___Size_30; }
	inline void set_Size_30(int32_t value)
	{
		___Size_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWERCONTAINER_T6875EA47AF7184E1D7387EC138A4BFFD1E79484C_H
#ifndef UIDRAWERMESSAGE_TB7B5BD2CABB78B5CBC2F1B5E7AA4B8A58B3A85A2_H
#define UIDRAWERMESSAGE_TB7B5BD2CABB78B5CBC2F1B5E7AA4B8A58B3A85A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIDrawerMessage
struct  UIDrawerMessage_tB7B5BD2CABB78B5CBC2F1B5E7AA4B8A58B3A85A2  : public Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E
{
public:
	// Doozy.Engine.UI.UIDrawer Doozy.Engine.UI.UIDrawerMessage::Drawer
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A * ___Drawer_3;
	// Doozy.Engine.UI.UIDrawerBehaviorType Doozy.Engine.UI.UIDrawerMessage::Type
	int32_t ___Type_4;

public:
	inline static int32_t get_offset_of_Drawer_3() { return static_cast<int32_t>(offsetof(UIDrawerMessage_tB7B5BD2CABB78B5CBC2F1B5E7AA4B8A58B3A85A2, ___Drawer_3)); }
	inline UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A * get_Drawer_3() const { return ___Drawer_3; }
	inline UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A ** get_address_of_Drawer_3() { return &___Drawer_3; }
	inline void set_Drawer_3(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A * value)
	{
		___Drawer_3 = value;
		Il2CppCodeGenWriteBarrier((&___Drawer_3), value);
	}

	inline static int32_t get_offset_of_Type_4() { return static_cast<int32_t>(offsetof(UIDrawerMessage_tB7B5BD2CABB78B5CBC2F1B5E7AA4B8A58B3A85A2, ___Type_4)); }
	inline int32_t get_Type_4() const { return ___Type_4; }
	inline int32_t* get_address_of_Type_4() { return &___Type_4; }
	inline void set_Type_4(int32_t value)
	{
		___Type_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWERMESSAGE_TB7B5BD2CABB78B5CBC2F1B5E7AA4B8A58B3A85A2_H
#ifndef UIPOPUPMESSAGE_T4CDAE4BEA172DE80A59680BCC384ABFD6F05C896_H
#define UIPOPUPMESSAGE_T4CDAE4BEA172DE80A59680BCC384ABFD6F05C896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopupMessage
struct  UIPopupMessage_t4CDAE4BEA172DE80A59680BCC384ABFD6F05C896  : public Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E
{
public:
	// Doozy.Engine.UI.UIPopup Doozy.Engine.UI.UIPopupMessage::Popup
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * ___Popup_3;
	// Doozy.Engine.UI.Animation.AnimationType Doozy.Engine.UI.UIPopupMessage::AnimationType
	int32_t ___AnimationType_4;

public:
	inline static int32_t get_offset_of_Popup_3() { return static_cast<int32_t>(offsetof(UIPopupMessage_t4CDAE4BEA172DE80A59680BCC384ABFD6F05C896, ___Popup_3)); }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * get_Popup_3() const { return ___Popup_3; }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 ** get_address_of_Popup_3() { return &___Popup_3; }
	inline void set_Popup_3(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * value)
	{
		___Popup_3 = value;
		Il2CppCodeGenWriteBarrier((&___Popup_3), value);
	}

	inline static int32_t get_offset_of_AnimationType_4() { return static_cast<int32_t>(offsetof(UIPopupMessage_t4CDAE4BEA172DE80A59680BCC384ABFD6F05C896, ___AnimationType_4)); }
	inline int32_t get_AnimationType_4() const { return ___AnimationType_4; }
	inline int32_t* get_address_of_AnimationType_4() { return &___AnimationType_4; }
	inline void set_AnimationType_4(int32_t value)
	{
		___AnimationType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOPUPMESSAGE_T4CDAE4BEA172DE80A59680BCC384ABFD6F05C896_H
#ifndef UITOGGLEBEHAVIOR_TA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB_H
#define UITOGGLEBEHAVIOR_TA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggleBehavior
struct  UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Doozy.Engine.Events.AnimatorEvent> Doozy.Engine.UI.UIToggleBehavior::Animators
	List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5 * ___Animators_12;
	// Doozy.Engine.UI.Animation.ButtonAnimationType Doozy.Engine.UI.UIToggleBehavior::ButtonAnimationType
	int32_t ___ButtonAnimationType_13;
	// System.Boolean Doozy.Engine.UI.UIToggleBehavior::DeselectButton
	bool ___DeselectButton_14;
	// System.Single Doozy.Engine.UI.UIToggleBehavior::DisableInterval
	float ___DisableInterval_15;
	// System.Boolean Doozy.Engine.UI.UIToggleBehavior::Enabled
	bool ___Enabled_16;
	// System.Boolean Doozy.Engine.UI.UIToggleBehavior::LoadSelectedPresetAtRuntime
	bool ___LoadSelectedPresetAtRuntime_17;
	// Doozy.Engine.UI.Base.UIAction Doozy.Engine.UI.UIToggleBehavior::OnToggleOff
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * ___OnToggleOff_18;
	// Doozy.Engine.UI.Base.UIAction Doozy.Engine.UI.UIToggleBehavior::OnToggleOn
	UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * ___OnToggleOn_19;
	// System.String Doozy.Engine.UI.UIToggleBehavior::PresetCategory
	String_t* ___PresetCategory_20;
	// System.String Doozy.Engine.UI.UIToggleBehavior::PresetName
	String_t* ___PresetName_21;
	// Doozy.Engine.UI.Animation.UIAnimation Doozy.Engine.UI.UIToggleBehavior::PunchAnimation
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * ___PunchAnimation_22;
	// System.Boolean Doozy.Engine.UI.UIToggleBehavior::Ready
	bool ___Ready_23;
	// System.Boolean Doozy.Engine.UI.UIToggleBehavior::SelectButton
	bool ___SelectButton_24;
	// Doozy.Engine.UI.Animation.UIAnimation Doozy.Engine.UI.UIToggleBehavior::StateAnimation
	UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * ___StateAnimation_25;
	// System.Boolean Doozy.Engine.UI.UIToggleBehavior::TriggerEventsAfterAnimation
	bool ___TriggerEventsAfterAnimation_26;
	// Doozy.Engine.UI.UIToggleBehaviorType Doozy.Engine.UI.UIToggleBehavior::m_behaviorType
	int32_t ___m_behaviorType_27;

public:
	inline static int32_t get_offset_of_Animators_12() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___Animators_12)); }
	inline List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5 * get_Animators_12() const { return ___Animators_12; }
	inline List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5 ** get_address_of_Animators_12() { return &___Animators_12; }
	inline void set_Animators_12(List_1_t03F586A29FEF63A4DF4A05FAE14694EE15BF39D5 * value)
	{
		___Animators_12 = value;
		Il2CppCodeGenWriteBarrier((&___Animators_12), value);
	}

	inline static int32_t get_offset_of_ButtonAnimationType_13() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___ButtonAnimationType_13)); }
	inline int32_t get_ButtonAnimationType_13() const { return ___ButtonAnimationType_13; }
	inline int32_t* get_address_of_ButtonAnimationType_13() { return &___ButtonAnimationType_13; }
	inline void set_ButtonAnimationType_13(int32_t value)
	{
		___ButtonAnimationType_13 = value;
	}

	inline static int32_t get_offset_of_DeselectButton_14() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___DeselectButton_14)); }
	inline bool get_DeselectButton_14() const { return ___DeselectButton_14; }
	inline bool* get_address_of_DeselectButton_14() { return &___DeselectButton_14; }
	inline void set_DeselectButton_14(bool value)
	{
		___DeselectButton_14 = value;
	}

	inline static int32_t get_offset_of_DisableInterval_15() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___DisableInterval_15)); }
	inline float get_DisableInterval_15() const { return ___DisableInterval_15; }
	inline float* get_address_of_DisableInterval_15() { return &___DisableInterval_15; }
	inline void set_DisableInterval_15(float value)
	{
		___DisableInterval_15 = value;
	}

	inline static int32_t get_offset_of_Enabled_16() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___Enabled_16)); }
	inline bool get_Enabled_16() const { return ___Enabled_16; }
	inline bool* get_address_of_Enabled_16() { return &___Enabled_16; }
	inline void set_Enabled_16(bool value)
	{
		___Enabled_16 = value;
	}

	inline static int32_t get_offset_of_LoadSelectedPresetAtRuntime_17() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___LoadSelectedPresetAtRuntime_17)); }
	inline bool get_LoadSelectedPresetAtRuntime_17() const { return ___LoadSelectedPresetAtRuntime_17; }
	inline bool* get_address_of_LoadSelectedPresetAtRuntime_17() { return &___LoadSelectedPresetAtRuntime_17; }
	inline void set_LoadSelectedPresetAtRuntime_17(bool value)
	{
		___LoadSelectedPresetAtRuntime_17 = value;
	}

	inline static int32_t get_offset_of_OnToggleOff_18() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___OnToggleOff_18)); }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * get_OnToggleOff_18() const { return ___OnToggleOff_18; }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 ** get_address_of_OnToggleOff_18() { return &___OnToggleOff_18; }
	inline void set_OnToggleOff_18(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * value)
	{
		___OnToggleOff_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnToggleOff_18), value);
	}

	inline static int32_t get_offset_of_OnToggleOn_19() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___OnToggleOn_19)); }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * get_OnToggleOn_19() const { return ___OnToggleOn_19; }
	inline UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 ** get_address_of_OnToggleOn_19() { return &___OnToggleOn_19; }
	inline void set_OnToggleOn_19(UIAction_t7E575EC5B7B0E812AF25B2EF5B745B10155537A7 * value)
	{
		___OnToggleOn_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnToggleOn_19), value);
	}

	inline static int32_t get_offset_of_PresetCategory_20() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___PresetCategory_20)); }
	inline String_t* get_PresetCategory_20() const { return ___PresetCategory_20; }
	inline String_t** get_address_of_PresetCategory_20() { return &___PresetCategory_20; }
	inline void set_PresetCategory_20(String_t* value)
	{
		___PresetCategory_20 = value;
		Il2CppCodeGenWriteBarrier((&___PresetCategory_20), value);
	}

	inline static int32_t get_offset_of_PresetName_21() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___PresetName_21)); }
	inline String_t* get_PresetName_21() const { return ___PresetName_21; }
	inline String_t** get_address_of_PresetName_21() { return &___PresetName_21; }
	inline void set_PresetName_21(String_t* value)
	{
		___PresetName_21 = value;
		Il2CppCodeGenWriteBarrier((&___PresetName_21), value);
	}

	inline static int32_t get_offset_of_PunchAnimation_22() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___PunchAnimation_22)); }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * get_PunchAnimation_22() const { return ___PunchAnimation_22; }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C ** get_address_of_PunchAnimation_22() { return &___PunchAnimation_22; }
	inline void set_PunchAnimation_22(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * value)
	{
		___PunchAnimation_22 = value;
		Il2CppCodeGenWriteBarrier((&___PunchAnimation_22), value);
	}

	inline static int32_t get_offset_of_Ready_23() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___Ready_23)); }
	inline bool get_Ready_23() const { return ___Ready_23; }
	inline bool* get_address_of_Ready_23() { return &___Ready_23; }
	inline void set_Ready_23(bool value)
	{
		___Ready_23 = value;
	}

	inline static int32_t get_offset_of_SelectButton_24() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___SelectButton_24)); }
	inline bool get_SelectButton_24() const { return ___SelectButton_24; }
	inline bool* get_address_of_SelectButton_24() { return &___SelectButton_24; }
	inline void set_SelectButton_24(bool value)
	{
		___SelectButton_24 = value;
	}

	inline static int32_t get_offset_of_StateAnimation_25() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___StateAnimation_25)); }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * get_StateAnimation_25() const { return ___StateAnimation_25; }
	inline UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C ** get_address_of_StateAnimation_25() { return &___StateAnimation_25; }
	inline void set_StateAnimation_25(UIAnimation_t5594956F11A5F167AC1C8C083516B3BE07CF374C * value)
	{
		___StateAnimation_25 = value;
		Il2CppCodeGenWriteBarrier((&___StateAnimation_25), value);
	}

	inline static int32_t get_offset_of_TriggerEventsAfterAnimation_26() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___TriggerEventsAfterAnimation_26)); }
	inline bool get_TriggerEventsAfterAnimation_26() const { return ___TriggerEventsAfterAnimation_26; }
	inline bool* get_address_of_TriggerEventsAfterAnimation_26() { return &___TriggerEventsAfterAnimation_26; }
	inline void set_TriggerEventsAfterAnimation_26(bool value)
	{
		___TriggerEventsAfterAnimation_26 = value;
	}

	inline static int32_t get_offset_of_m_behaviorType_27() { return static_cast<int32_t>(offsetof(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB, ___m_behaviorType_27)); }
	inline int32_t get_m_behaviorType_27() const { return ___m_behaviorType_27; }
	inline int32_t* get_address_of_m_behaviorType_27() { return &___m_behaviorType_27; }
	inline void set_m_behaviorType_27(int32_t value)
	{
		___m_behaviorType_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLEBEHAVIOR_TA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB_H
#ifndef UITOGGLEMESSAGE_T707A3D7907C271DF472A0D1BB244424E2ACE5728_H
#define UITOGGLEMESSAGE_T707A3D7907C271DF472A0D1BB244424E2ACE5728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggleMessage
struct  UIToggleMessage_t707A3D7907C271DF472A0D1BB244424E2ACE5728  : public Message_tE1EC555B9024A0C7EBD7EBC602E9AE1BE1E21E0E
{
public:
	// Doozy.Engine.UI.UIToggle Doozy.Engine.UI.UIToggleMessage::Toggle
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * ___Toggle_3;
	// Doozy.Engine.UI.UIToggleState Doozy.Engine.UI.UIToggleMessage::ToggleState
	int32_t ___ToggleState_4;
	// Doozy.Engine.UI.UIToggleBehaviorType Doozy.Engine.UI.UIToggleMessage::Type
	int32_t ___Type_5;

public:
	inline static int32_t get_offset_of_Toggle_3() { return static_cast<int32_t>(offsetof(UIToggleMessage_t707A3D7907C271DF472A0D1BB244424E2ACE5728, ___Toggle_3)); }
	inline UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * get_Toggle_3() const { return ___Toggle_3; }
	inline UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 ** get_address_of_Toggle_3() { return &___Toggle_3; }
	inline void set_Toggle_3(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273 * value)
	{
		___Toggle_3 = value;
		Il2CppCodeGenWriteBarrier((&___Toggle_3), value);
	}

	inline static int32_t get_offset_of_ToggleState_4() { return static_cast<int32_t>(offsetof(UIToggleMessage_t707A3D7907C271DF472A0D1BB244424E2ACE5728, ___ToggleState_4)); }
	inline int32_t get_ToggleState_4() const { return ___ToggleState_4; }
	inline int32_t* get_address_of_ToggleState_4() { return &___ToggleState_4; }
	inline void set_ToggleState_4(int32_t value)
	{
		___ToggleState_4 = value;
	}

	inline static int32_t get_offset_of_Type_5() { return static_cast<int32_t>(offsetof(UIToggleMessage_t707A3D7907C271DF472A0D1BB244424E2ACE5728, ___Type_5)); }
	inline int32_t get_Type_5() const { return ___Type_5; }
	inline int32_t* get_address_of_Type_5() { return &___Type_5; }
	inline void set_Type_5(int32_t value)
	{
		___Type_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLEMESSAGE_T707A3D7907C271DF472A0D1BB244424E2ACE5728_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef UIPOPUPDATABASE_T2DD34A70147D17B8FB3EE2E8F0C95131852AD382_H
#define UIPOPUPDATABASE_T2DD34A70147D17B8FB3EE2E8F0C95131852AD382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopupDatabase
struct  UIPopupDatabase_t2DD34A70147D17B8FB3EE2E8F0C95131852AD382  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<System.String> Doozy.Engine.UI.UIPopupDatabase::PopupNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___PopupNames_4;
	// System.Collections.Generic.List`1<Doozy.Engine.UI.UIPopupLink> Doozy.Engine.UI.UIPopupDatabase::Popups
	List_1_t2C69E4FC63314547C6DEA51918316C3DA8381618 * ___Popups_5;

public:
	inline static int32_t get_offset_of_PopupNames_4() { return static_cast<int32_t>(offsetof(UIPopupDatabase_t2DD34A70147D17B8FB3EE2E8F0C95131852AD382, ___PopupNames_4)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_PopupNames_4() const { return ___PopupNames_4; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_PopupNames_4() { return &___PopupNames_4; }
	inline void set_PopupNames_4(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___PopupNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___PopupNames_4), value);
	}

	inline static int32_t get_offset_of_Popups_5() { return static_cast<int32_t>(offsetof(UIPopupDatabase_t2DD34A70147D17B8FB3EE2E8F0C95131852AD382, ___Popups_5)); }
	inline List_1_t2C69E4FC63314547C6DEA51918316C3DA8381618 * get_Popups_5() const { return ___Popups_5; }
	inline List_1_t2C69E4FC63314547C6DEA51918316C3DA8381618 ** get_address_of_Popups_5() { return &___Popups_5; }
	inline void set_Popups_5(List_1_t2C69E4FC63314547C6DEA51918316C3DA8381618 * value)
	{
		___Popups_5 = value;
		Il2CppCodeGenWriteBarrier((&___Popups_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOPUPDATABASE_T2DD34A70147D17B8FB3EE2E8F0C95131852AD382_H
#ifndef UIPOPUPLINK_TFEA0CF345A8EEDF5E9A3CAE95DFE0069A97C78BC_H
#define UIPOPUPLINK_TFEA0CF345A8EEDF5E9A3CAE95DFE0069A97C78BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopupLink
struct  UIPopupLink_tFEA0CF345A8EEDF5E9A3CAE95DFE0069A97C78BC  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String Doozy.Engine.UI.UIPopupLink::PopupName
	String_t* ___PopupName_4;
	// UnityEngine.GameObject Doozy.Engine.UI.UIPopupLink::Prefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Prefab_5;

public:
	inline static int32_t get_offset_of_PopupName_4() { return static_cast<int32_t>(offsetof(UIPopupLink_tFEA0CF345A8EEDF5E9A3CAE95DFE0069A97C78BC, ___PopupName_4)); }
	inline String_t* get_PopupName_4() const { return ___PopupName_4; }
	inline String_t** get_address_of_PopupName_4() { return &___PopupName_4; }
	inline void set_PopupName_4(String_t* value)
	{
		___PopupName_4 = value;
		Il2CppCodeGenWriteBarrier((&___PopupName_4), value);
	}

	inline static int32_t get_offset_of_Prefab_5() { return static_cast<int32_t>(offsetof(UIPopupLink_tFEA0CF345A8EEDF5E9A3CAE95DFE0069A97C78BC, ___Prefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Prefab_5() const { return ___Prefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Prefab_5() { return &___Prefab_5; }
	inline void set_Prefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Prefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOPUPLINK_TFEA0CF345A8EEDF5E9A3CAE95DFE0069A97C78BC_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef UICOMPONENTBASE_1_T1949D0532AD1F86A71653A26F39CB15528B706C1_H
#define UICOMPONENTBASE_1_T1949D0532AD1F86A71653A26F39CB15528B706C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.UIComponentBase`1<Doozy.Engine.UI.UIButton>
struct  UIComponentBase_1_t1949D0532AD1F86A71653A26F39CB15528B706C1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.UI.Base.UIComponentBase`1::DebugMode
	bool ___DebugMode_7;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartPosition_8;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartRotation_9;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartScale_10;
	// System.Single Doozy.Engine.UI.Base.UIComponentBase`1::StartAlpha
	float ___StartAlpha_11;
	// UnityEngine.RectTransform Doozy.Engine.UI.Base.UIComponentBase`1::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_12;

public:
	inline static int32_t get_offset_of_DebugMode_7() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t1949D0532AD1F86A71653A26F39CB15528B706C1, ___DebugMode_7)); }
	inline bool get_DebugMode_7() const { return ___DebugMode_7; }
	inline bool* get_address_of_DebugMode_7() { return &___DebugMode_7; }
	inline void set_DebugMode_7(bool value)
	{
		___DebugMode_7 = value;
	}

	inline static int32_t get_offset_of_StartPosition_8() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t1949D0532AD1F86A71653A26F39CB15528B706C1, ___StartPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartPosition_8() const { return ___StartPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartPosition_8() { return &___StartPosition_8; }
	inline void set_StartPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartPosition_8 = value;
	}

	inline static int32_t get_offset_of_StartRotation_9() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t1949D0532AD1F86A71653A26F39CB15528B706C1, ___StartRotation_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartRotation_9() const { return ___StartRotation_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartRotation_9() { return &___StartRotation_9; }
	inline void set_StartRotation_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartRotation_9 = value;
	}

	inline static int32_t get_offset_of_StartScale_10() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t1949D0532AD1F86A71653A26F39CB15528B706C1, ___StartScale_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartScale_10() const { return ___StartScale_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartScale_10() { return &___StartScale_10; }
	inline void set_StartScale_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartScale_10 = value;
	}

	inline static int32_t get_offset_of_StartAlpha_11() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t1949D0532AD1F86A71653A26F39CB15528B706C1, ___StartAlpha_11)); }
	inline float get_StartAlpha_11() const { return ___StartAlpha_11; }
	inline float* get_address_of_StartAlpha_11() { return &___StartAlpha_11; }
	inline void set_StartAlpha_11(float value)
	{
		___StartAlpha_11 = value;
	}

	inline static int32_t get_offset_of_m_rectTransform_12() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t1949D0532AD1F86A71653A26F39CB15528B706C1, ___m_rectTransform_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_12() const { return ___m_rectTransform_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_12() { return &___m_rectTransform_12; }
	inline void set_m_rectTransform_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_12), value);
	}
};

struct UIComponentBase_1_t1949D0532AD1F86A71653A26F39CB15528B706C1_StaticFields
{
public:
	// System.Collections.Generic.List`1<T> Doozy.Engine.UI.Base.UIComponentBase`1::Database
	List_1_t4CB7881968787919895A2E041031FDE0003C3B98 * ___Database_4;
	// System.Int32 Doozy.Engine.UI.Base.UIComponentBase`1::s_uiInteractionsDisableLevel
	int32_t ___s_uiInteractionsDisableLevel_5;
	// UnityEngine.EventSystems.EventSystem Doozy.Engine.UI.Base.UIComponentBase`1::s_unityEventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___s_unityEventSystem_6;

public:
	inline static int32_t get_offset_of_Database_4() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t1949D0532AD1F86A71653A26F39CB15528B706C1_StaticFields, ___Database_4)); }
	inline List_1_t4CB7881968787919895A2E041031FDE0003C3B98 * get_Database_4() const { return ___Database_4; }
	inline List_1_t4CB7881968787919895A2E041031FDE0003C3B98 ** get_address_of_Database_4() { return &___Database_4; }
	inline void set_Database_4(List_1_t4CB7881968787919895A2E041031FDE0003C3B98 * value)
	{
		___Database_4 = value;
		Il2CppCodeGenWriteBarrier((&___Database_4), value);
	}

	inline static int32_t get_offset_of_s_uiInteractionsDisableLevel_5() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t1949D0532AD1F86A71653A26F39CB15528B706C1_StaticFields, ___s_uiInteractionsDisableLevel_5)); }
	inline int32_t get_s_uiInteractionsDisableLevel_5() const { return ___s_uiInteractionsDisableLevel_5; }
	inline int32_t* get_address_of_s_uiInteractionsDisableLevel_5() { return &___s_uiInteractionsDisableLevel_5; }
	inline void set_s_uiInteractionsDisableLevel_5(int32_t value)
	{
		___s_uiInteractionsDisableLevel_5 = value;
	}

	inline static int32_t get_offset_of_s_unityEventSystem_6() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t1949D0532AD1F86A71653A26F39CB15528B706C1_StaticFields, ___s_unityEventSystem_6)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_s_unityEventSystem_6() const { return ___s_unityEventSystem_6; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_s_unityEventSystem_6() { return &___s_unityEventSystem_6; }
	inline void set_s_unityEventSystem_6(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___s_unityEventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_unityEventSystem_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICOMPONENTBASE_1_T1949D0532AD1F86A71653A26F39CB15528B706C1_H
#ifndef UICOMPONENTBASE_1_T41D90A74F073BFCFEAAFCC68E302571B5C2B3B44_H
#define UICOMPONENTBASE_1_T41D90A74F073BFCFEAAFCC68E302571B5C2B3B44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.UIComponentBase`1<Doozy.Engine.UI.UICanvas>
struct  UIComponentBase_1_t41D90A74F073BFCFEAAFCC68E302571B5C2B3B44  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.UI.Base.UIComponentBase`1::DebugMode
	bool ___DebugMode_7;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartPosition_8;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartRotation_9;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartScale_10;
	// System.Single Doozy.Engine.UI.Base.UIComponentBase`1::StartAlpha
	float ___StartAlpha_11;
	// UnityEngine.RectTransform Doozy.Engine.UI.Base.UIComponentBase`1::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_12;

public:
	inline static int32_t get_offset_of_DebugMode_7() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t41D90A74F073BFCFEAAFCC68E302571B5C2B3B44, ___DebugMode_7)); }
	inline bool get_DebugMode_7() const { return ___DebugMode_7; }
	inline bool* get_address_of_DebugMode_7() { return &___DebugMode_7; }
	inline void set_DebugMode_7(bool value)
	{
		___DebugMode_7 = value;
	}

	inline static int32_t get_offset_of_StartPosition_8() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t41D90A74F073BFCFEAAFCC68E302571B5C2B3B44, ___StartPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartPosition_8() const { return ___StartPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartPosition_8() { return &___StartPosition_8; }
	inline void set_StartPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartPosition_8 = value;
	}

	inline static int32_t get_offset_of_StartRotation_9() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t41D90A74F073BFCFEAAFCC68E302571B5C2B3B44, ___StartRotation_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartRotation_9() const { return ___StartRotation_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartRotation_9() { return &___StartRotation_9; }
	inline void set_StartRotation_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartRotation_9 = value;
	}

	inline static int32_t get_offset_of_StartScale_10() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t41D90A74F073BFCFEAAFCC68E302571B5C2B3B44, ___StartScale_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartScale_10() const { return ___StartScale_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartScale_10() { return &___StartScale_10; }
	inline void set_StartScale_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartScale_10 = value;
	}

	inline static int32_t get_offset_of_StartAlpha_11() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t41D90A74F073BFCFEAAFCC68E302571B5C2B3B44, ___StartAlpha_11)); }
	inline float get_StartAlpha_11() const { return ___StartAlpha_11; }
	inline float* get_address_of_StartAlpha_11() { return &___StartAlpha_11; }
	inline void set_StartAlpha_11(float value)
	{
		___StartAlpha_11 = value;
	}

	inline static int32_t get_offset_of_m_rectTransform_12() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t41D90A74F073BFCFEAAFCC68E302571B5C2B3B44, ___m_rectTransform_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_12() const { return ___m_rectTransform_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_12() { return &___m_rectTransform_12; }
	inline void set_m_rectTransform_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_12), value);
	}
};

struct UIComponentBase_1_t41D90A74F073BFCFEAAFCC68E302571B5C2B3B44_StaticFields
{
public:
	// System.Collections.Generic.List`1<T> Doozy.Engine.UI.Base.UIComponentBase`1::Database
	List_1_t1CD2283A024EC384C8C14392E3D6EEE458CB3E10 * ___Database_4;
	// System.Int32 Doozy.Engine.UI.Base.UIComponentBase`1::s_uiInteractionsDisableLevel
	int32_t ___s_uiInteractionsDisableLevel_5;
	// UnityEngine.EventSystems.EventSystem Doozy.Engine.UI.Base.UIComponentBase`1::s_unityEventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___s_unityEventSystem_6;

public:
	inline static int32_t get_offset_of_Database_4() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t41D90A74F073BFCFEAAFCC68E302571B5C2B3B44_StaticFields, ___Database_4)); }
	inline List_1_t1CD2283A024EC384C8C14392E3D6EEE458CB3E10 * get_Database_4() const { return ___Database_4; }
	inline List_1_t1CD2283A024EC384C8C14392E3D6EEE458CB3E10 ** get_address_of_Database_4() { return &___Database_4; }
	inline void set_Database_4(List_1_t1CD2283A024EC384C8C14392E3D6EEE458CB3E10 * value)
	{
		___Database_4 = value;
		Il2CppCodeGenWriteBarrier((&___Database_4), value);
	}

	inline static int32_t get_offset_of_s_uiInteractionsDisableLevel_5() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t41D90A74F073BFCFEAAFCC68E302571B5C2B3B44_StaticFields, ___s_uiInteractionsDisableLevel_5)); }
	inline int32_t get_s_uiInteractionsDisableLevel_5() const { return ___s_uiInteractionsDisableLevel_5; }
	inline int32_t* get_address_of_s_uiInteractionsDisableLevel_5() { return &___s_uiInteractionsDisableLevel_5; }
	inline void set_s_uiInteractionsDisableLevel_5(int32_t value)
	{
		___s_uiInteractionsDisableLevel_5 = value;
	}

	inline static int32_t get_offset_of_s_unityEventSystem_6() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t41D90A74F073BFCFEAAFCC68E302571B5C2B3B44_StaticFields, ___s_unityEventSystem_6)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_s_unityEventSystem_6() const { return ___s_unityEventSystem_6; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_s_unityEventSystem_6() { return &___s_unityEventSystem_6; }
	inline void set_s_unityEventSystem_6(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___s_unityEventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_unityEventSystem_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICOMPONENTBASE_1_T41D90A74F073BFCFEAAFCC68E302571B5C2B3B44_H
#ifndef UICOMPONENTBASE_1_TEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF_H
#define UICOMPONENTBASE_1_TEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.UIComponentBase`1<Doozy.Engine.UI.UIDrawer>
struct  UIComponentBase_1_tEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.UI.Base.UIComponentBase`1::DebugMode
	bool ___DebugMode_7;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartPosition_8;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartRotation_9;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartScale_10;
	// System.Single Doozy.Engine.UI.Base.UIComponentBase`1::StartAlpha
	float ___StartAlpha_11;
	// UnityEngine.RectTransform Doozy.Engine.UI.Base.UIComponentBase`1::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_12;

public:
	inline static int32_t get_offset_of_DebugMode_7() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF, ___DebugMode_7)); }
	inline bool get_DebugMode_7() const { return ___DebugMode_7; }
	inline bool* get_address_of_DebugMode_7() { return &___DebugMode_7; }
	inline void set_DebugMode_7(bool value)
	{
		___DebugMode_7 = value;
	}

	inline static int32_t get_offset_of_StartPosition_8() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF, ___StartPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartPosition_8() const { return ___StartPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartPosition_8() { return &___StartPosition_8; }
	inline void set_StartPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartPosition_8 = value;
	}

	inline static int32_t get_offset_of_StartRotation_9() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF, ___StartRotation_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartRotation_9() const { return ___StartRotation_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartRotation_9() { return &___StartRotation_9; }
	inline void set_StartRotation_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartRotation_9 = value;
	}

	inline static int32_t get_offset_of_StartScale_10() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF, ___StartScale_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartScale_10() const { return ___StartScale_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartScale_10() { return &___StartScale_10; }
	inline void set_StartScale_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartScale_10 = value;
	}

	inline static int32_t get_offset_of_StartAlpha_11() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF, ___StartAlpha_11)); }
	inline float get_StartAlpha_11() const { return ___StartAlpha_11; }
	inline float* get_address_of_StartAlpha_11() { return &___StartAlpha_11; }
	inline void set_StartAlpha_11(float value)
	{
		___StartAlpha_11 = value;
	}

	inline static int32_t get_offset_of_m_rectTransform_12() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF, ___m_rectTransform_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_12() const { return ___m_rectTransform_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_12() { return &___m_rectTransform_12; }
	inline void set_m_rectTransform_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_12), value);
	}
};

struct UIComponentBase_1_tEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF_StaticFields
{
public:
	// System.Collections.Generic.List`1<T> Doozy.Engine.UI.Base.UIComponentBase`1::Database
	List_1_tD1156032C8684C7E2CAC4ACFEF99AFB3E9F188A1 * ___Database_4;
	// System.Int32 Doozy.Engine.UI.Base.UIComponentBase`1::s_uiInteractionsDisableLevel
	int32_t ___s_uiInteractionsDisableLevel_5;
	// UnityEngine.EventSystems.EventSystem Doozy.Engine.UI.Base.UIComponentBase`1::s_unityEventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___s_unityEventSystem_6;

public:
	inline static int32_t get_offset_of_Database_4() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF_StaticFields, ___Database_4)); }
	inline List_1_tD1156032C8684C7E2CAC4ACFEF99AFB3E9F188A1 * get_Database_4() const { return ___Database_4; }
	inline List_1_tD1156032C8684C7E2CAC4ACFEF99AFB3E9F188A1 ** get_address_of_Database_4() { return &___Database_4; }
	inline void set_Database_4(List_1_tD1156032C8684C7E2CAC4ACFEF99AFB3E9F188A1 * value)
	{
		___Database_4 = value;
		Il2CppCodeGenWriteBarrier((&___Database_4), value);
	}

	inline static int32_t get_offset_of_s_uiInteractionsDisableLevel_5() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF_StaticFields, ___s_uiInteractionsDisableLevel_5)); }
	inline int32_t get_s_uiInteractionsDisableLevel_5() const { return ___s_uiInteractionsDisableLevel_5; }
	inline int32_t* get_address_of_s_uiInteractionsDisableLevel_5() { return &___s_uiInteractionsDisableLevel_5; }
	inline void set_s_uiInteractionsDisableLevel_5(int32_t value)
	{
		___s_uiInteractionsDisableLevel_5 = value;
	}

	inline static int32_t get_offset_of_s_unityEventSystem_6() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF_StaticFields, ___s_unityEventSystem_6)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_s_unityEventSystem_6() const { return ___s_unityEventSystem_6; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_s_unityEventSystem_6() { return &___s_unityEventSystem_6; }
	inline void set_s_unityEventSystem_6(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___s_unityEventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_unityEventSystem_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICOMPONENTBASE_1_TEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF_H
#ifndef UICOMPONENTBASE_1_TD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B_H
#define UICOMPONENTBASE_1_TD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.UIComponentBase`1<Doozy.Engine.UI.UIPopup>
struct  UIComponentBase_1_tD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.UI.Base.UIComponentBase`1::DebugMode
	bool ___DebugMode_7;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartPosition_8;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartRotation_9;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartScale_10;
	// System.Single Doozy.Engine.UI.Base.UIComponentBase`1::StartAlpha
	float ___StartAlpha_11;
	// UnityEngine.RectTransform Doozy.Engine.UI.Base.UIComponentBase`1::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_12;

public:
	inline static int32_t get_offset_of_DebugMode_7() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B, ___DebugMode_7)); }
	inline bool get_DebugMode_7() const { return ___DebugMode_7; }
	inline bool* get_address_of_DebugMode_7() { return &___DebugMode_7; }
	inline void set_DebugMode_7(bool value)
	{
		___DebugMode_7 = value;
	}

	inline static int32_t get_offset_of_StartPosition_8() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B, ___StartPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartPosition_8() const { return ___StartPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartPosition_8() { return &___StartPosition_8; }
	inline void set_StartPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartPosition_8 = value;
	}

	inline static int32_t get_offset_of_StartRotation_9() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B, ___StartRotation_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartRotation_9() const { return ___StartRotation_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartRotation_9() { return &___StartRotation_9; }
	inline void set_StartRotation_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartRotation_9 = value;
	}

	inline static int32_t get_offset_of_StartScale_10() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B, ___StartScale_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartScale_10() const { return ___StartScale_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartScale_10() { return &___StartScale_10; }
	inline void set_StartScale_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartScale_10 = value;
	}

	inline static int32_t get_offset_of_StartAlpha_11() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B, ___StartAlpha_11)); }
	inline float get_StartAlpha_11() const { return ___StartAlpha_11; }
	inline float* get_address_of_StartAlpha_11() { return &___StartAlpha_11; }
	inline void set_StartAlpha_11(float value)
	{
		___StartAlpha_11 = value;
	}

	inline static int32_t get_offset_of_m_rectTransform_12() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B, ___m_rectTransform_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_12() const { return ___m_rectTransform_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_12() { return &___m_rectTransform_12; }
	inline void set_m_rectTransform_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_12), value);
	}
};

struct UIComponentBase_1_tD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B_StaticFields
{
public:
	// System.Collections.Generic.List`1<T> Doozy.Engine.UI.Base.UIComponentBase`1::Database
	List_1_t0F8A6581E8659C9A921426873F1825C44A02D639 * ___Database_4;
	// System.Int32 Doozy.Engine.UI.Base.UIComponentBase`1::s_uiInteractionsDisableLevel
	int32_t ___s_uiInteractionsDisableLevel_5;
	// UnityEngine.EventSystems.EventSystem Doozy.Engine.UI.Base.UIComponentBase`1::s_unityEventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___s_unityEventSystem_6;

public:
	inline static int32_t get_offset_of_Database_4() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B_StaticFields, ___Database_4)); }
	inline List_1_t0F8A6581E8659C9A921426873F1825C44A02D639 * get_Database_4() const { return ___Database_4; }
	inline List_1_t0F8A6581E8659C9A921426873F1825C44A02D639 ** get_address_of_Database_4() { return &___Database_4; }
	inline void set_Database_4(List_1_t0F8A6581E8659C9A921426873F1825C44A02D639 * value)
	{
		___Database_4 = value;
		Il2CppCodeGenWriteBarrier((&___Database_4), value);
	}

	inline static int32_t get_offset_of_s_uiInteractionsDisableLevel_5() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B_StaticFields, ___s_uiInteractionsDisableLevel_5)); }
	inline int32_t get_s_uiInteractionsDisableLevel_5() const { return ___s_uiInteractionsDisableLevel_5; }
	inline int32_t* get_address_of_s_uiInteractionsDisableLevel_5() { return &___s_uiInteractionsDisableLevel_5; }
	inline void set_s_uiInteractionsDisableLevel_5(int32_t value)
	{
		___s_uiInteractionsDisableLevel_5 = value;
	}

	inline static int32_t get_offset_of_s_unityEventSystem_6() { return static_cast<int32_t>(offsetof(UIComponentBase_1_tD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B_StaticFields, ___s_unityEventSystem_6)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_s_unityEventSystem_6() const { return ___s_unityEventSystem_6; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_s_unityEventSystem_6() { return &___s_unityEventSystem_6; }
	inline void set_s_unityEventSystem_6(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___s_unityEventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_unityEventSystem_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICOMPONENTBASE_1_TD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B_H
#ifndef UICOMPONENTBASE_1_T208748696434ABD229DF180B295D28F60187FF7C_H
#define UICOMPONENTBASE_1_T208748696434ABD229DF180B295D28F60187FF7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.UIComponentBase`1<Doozy.Engine.UI.UIToggle>
struct  UIComponentBase_1_t208748696434ABD229DF180B295D28F60187FF7C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.UI.Base.UIComponentBase`1::DebugMode
	bool ___DebugMode_7;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartPosition_8;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartRotation_9;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartScale_10;
	// System.Single Doozy.Engine.UI.Base.UIComponentBase`1::StartAlpha
	float ___StartAlpha_11;
	// UnityEngine.RectTransform Doozy.Engine.UI.Base.UIComponentBase`1::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_12;

public:
	inline static int32_t get_offset_of_DebugMode_7() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t208748696434ABD229DF180B295D28F60187FF7C, ___DebugMode_7)); }
	inline bool get_DebugMode_7() const { return ___DebugMode_7; }
	inline bool* get_address_of_DebugMode_7() { return &___DebugMode_7; }
	inline void set_DebugMode_7(bool value)
	{
		___DebugMode_7 = value;
	}

	inline static int32_t get_offset_of_StartPosition_8() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t208748696434ABD229DF180B295D28F60187FF7C, ___StartPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartPosition_8() const { return ___StartPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartPosition_8() { return &___StartPosition_8; }
	inline void set_StartPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartPosition_8 = value;
	}

	inline static int32_t get_offset_of_StartRotation_9() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t208748696434ABD229DF180B295D28F60187FF7C, ___StartRotation_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartRotation_9() const { return ___StartRotation_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartRotation_9() { return &___StartRotation_9; }
	inline void set_StartRotation_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartRotation_9 = value;
	}

	inline static int32_t get_offset_of_StartScale_10() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t208748696434ABD229DF180B295D28F60187FF7C, ___StartScale_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartScale_10() const { return ___StartScale_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartScale_10() { return &___StartScale_10; }
	inline void set_StartScale_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartScale_10 = value;
	}

	inline static int32_t get_offset_of_StartAlpha_11() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t208748696434ABD229DF180B295D28F60187FF7C, ___StartAlpha_11)); }
	inline float get_StartAlpha_11() const { return ___StartAlpha_11; }
	inline float* get_address_of_StartAlpha_11() { return &___StartAlpha_11; }
	inline void set_StartAlpha_11(float value)
	{
		___StartAlpha_11 = value;
	}

	inline static int32_t get_offset_of_m_rectTransform_12() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t208748696434ABD229DF180B295D28F60187FF7C, ___m_rectTransform_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_12() const { return ___m_rectTransform_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_12() { return &___m_rectTransform_12; }
	inline void set_m_rectTransform_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_12), value);
	}
};

struct UIComponentBase_1_t208748696434ABD229DF180B295D28F60187FF7C_StaticFields
{
public:
	// System.Collections.Generic.List`1<T> Doozy.Engine.UI.Base.UIComponentBase`1::Database
	List_1_tD7C2010EE5E409831AF5DF4F17D958536A8323E6 * ___Database_4;
	// System.Int32 Doozy.Engine.UI.Base.UIComponentBase`1::s_uiInteractionsDisableLevel
	int32_t ___s_uiInteractionsDisableLevel_5;
	// UnityEngine.EventSystems.EventSystem Doozy.Engine.UI.Base.UIComponentBase`1::s_unityEventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___s_unityEventSystem_6;

public:
	inline static int32_t get_offset_of_Database_4() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t208748696434ABD229DF180B295D28F60187FF7C_StaticFields, ___Database_4)); }
	inline List_1_tD7C2010EE5E409831AF5DF4F17D958536A8323E6 * get_Database_4() const { return ___Database_4; }
	inline List_1_tD7C2010EE5E409831AF5DF4F17D958536A8323E6 ** get_address_of_Database_4() { return &___Database_4; }
	inline void set_Database_4(List_1_tD7C2010EE5E409831AF5DF4F17D958536A8323E6 * value)
	{
		___Database_4 = value;
		Il2CppCodeGenWriteBarrier((&___Database_4), value);
	}

	inline static int32_t get_offset_of_s_uiInteractionsDisableLevel_5() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t208748696434ABD229DF180B295D28F60187FF7C_StaticFields, ___s_uiInteractionsDisableLevel_5)); }
	inline int32_t get_s_uiInteractionsDisableLevel_5() const { return ___s_uiInteractionsDisableLevel_5; }
	inline int32_t* get_address_of_s_uiInteractionsDisableLevel_5() { return &___s_uiInteractionsDisableLevel_5; }
	inline void set_s_uiInteractionsDisableLevel_5(int32_t value)
	{
		___s_uiInteractionsDisableLevel_5 = value;
	}

	inline static int32_t get_offset_of_s_unityEventSystem_6() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t208748696434ABD229DF180B295D28F60187FF7C_StaticFields, ___s_unityEventSystem_6)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_s_unityEventSystem_6() const { return ___s_unityEventSystem_6; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_s_unityEventSystem_6() { return &___s_unityEventSystem_6; }
	inline void set_s_unityEventSystem_6(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___s_unityEventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_unityEventSystem_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICOMPONENTBASE_1_T208748696434ABD229DF180B295D28F60187FF7C_H
#ifndef UICOMPONENTBASE_1_T64C56FBF7E4AFAA3D397341303F4A8DDD33F4843_H
#define UICOMPONENTBASE_1_T64C56FBF7E4AFAA3D397341303F4A8DDD33F4843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.Base.UIComponentBase`1<Doozy.Engine.UI.UIView>
struct  UIComponentBase_1_t64C56FBF7E4AFAA3D397341303F4A8DDD33F4843  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.UI.Base.UIComponentBase`1::DebugMode
	bool ___DebugMode_7;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartPosition_8;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartRotation_9;
	// UnityEngine.Vector3 Doozy.Engine.UI.Base.UIComponentBase`1::StartScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartScale_10;
	// System.Single Doozy.Engine.UI.Base.UIComponentBase`1::StartAlpha
	float ___StartAlpha_11;
	// UnityEngine.RectTransform Doozy.Engine.UI.Base.UIComponentBase`1::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_12;

public:
	inline static int32_t get_offset_of_DebugMode_7() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t64C56FBF7E4AFAA3D397341303F4A8DDD33F4843, ___DebugMode_7)); }
	inline bool get_DebugMode_7() const { return ___DebugMode_7; }
	inline bool* get_address_of_DebugMode_7() { return &___DebugMode_7; }
	inline void set_DebugMode_7(bool value)
	{
		___DebugMode_7 = value;
	}

	inline static int32_t get_offset_of_StartPosition_8() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t64C56FBF7E4AFAA3D397341303F4A8DDD33F4843, ___StartPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartPosition_8() const { return ___StartPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartPosition_8() { return &___StartPosition_8; }
	inline void set_StartPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartPosition_8 = value;
	}

	inline static int32_t get_offset_of_StartRotation_9() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t64C56FBF7E4AFAA3D397341303F4A8DDD33F4843, ___StartRotation_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartRotation_9() const { return ___StartRotation_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartRotation_9() { return &___StartRotation_9; }
	inline void set_StartRotation_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartRotation_9 = value;
	}

	inline static int32_t get_offset_of_StartScale_10() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t64C56FBF7E4AFAA3D397341303F4A8DDD33F4843, ___StartScale_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartScale_10() const { return ___StartScale_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartScale_10() { return &___StartScale_10; }
	inline void set_StartScale_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartScale_10 = value;
	}

	inline static int32_t get_offset_of_StartAlpha_11() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t64C56FBF7E4AFAA3D397341303F4A8DDD33F4843, ___StartAlpha_11)); }
	inline float get_StartAlpha_11() const { return ___StartAlpha_11; }
	inline float* get_address_of_StartAlpha_11() { return &___StartAlpha_11; }
	inline void set_StartAlpha_11(float value)
	{
		___StartAlpha_11 = value;
	}

	inline static int32_t get_offset_of_m_rectTransform_12() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t64C56FBF7E4AFAA3D397341303F4A8DDD33F4843, ___m_rectTransform_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_12() const { return ___m_rectTransform_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_12() { return &___m_rectTransform_12; }
	inline void set_m_rectTransform_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_12), value);
	}
};

struct UIComponentBase_1_t64C56FBF7E4AFAA3D397341303F4A8DDD33F4843_StaticFields
{
public:
	// System.Collections.Generic.List`1<T> Doozy.Engine.UI.Base.UIComponentBase`1::Database
	List_1_t7A5261DAB640C55051ADE8D918AAF06457CF2378 * ___Database_4;
	// System.Int32 Doozy.Engine.UI.Base.UIComponentBase`1::s_uiInteractionsDisableLevel
	int32_t ___s_uiInteractionsDisableLevel_5;
	// UnityEngine.EventSystems.EventSystem Doozy.Engine.UI.Base.UIComponentBase`1::s_unityEventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___s_unityEventSystem_6;

public:
	inline static int32_t get_offset_of_Database_4() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t64C56FBF7E4AFAA3D397341303F4A8DDD33F4843_StaticFields, ___Database_4)); }
	inline List_1_t7A5261DAB640C55051ADE8D918AAF06457CF2378 * get_Database_4() const { return ___Database_4; }
	inline List_1_t7A5261DAB640C55051ADE8D918AAF06457CF2378 ** get_address_of_Database_4() { return &___Database_4; }
	inline void set_Database_4(List_1_t7A5261DAB640C55051ADE8D918AAF06457CF2378 * value)
	{
		___Database_4 = value;
		Il2CppCodeGenWriteBarrier((&___Database_4), value);
	}

	inline static int32_t get_offset_of_s_uiInteractionsDisableLevel_5() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t64C56FBF7E4AFAA3D397341303F4A8DDD33F4843_StaticFields, ___s_uiInteractionsDisableLevel_5)); }
	inline int32_t get_s_uiInteractionsDisableLevel_5() const { return ___s_uiInteractionsDisableLevel_5; }
	inline int32_t* get_address_of_s_uiInteractionsDisableLevel_5() { return &___s_uiInteractionsDisableLevel_5; }
	inline void set_s_uiInteractionsDisableLevel_5(int32_t value)
	{
		___s_uiInteractionsDisableLevel_5 = value;
	}

	inline static int32_t get_offset_of_s_unityEventSystem_6() { return static_cast<int32_t>(offsetof(UIComponentBase_1_t64C56FBF7E4AFAA3D397341303F4A8DDD33F4843_StaticFields, ___s_unityEventSystem_6)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_s_unityEventSystem_6() const { return ___s_unityEventSystem_6; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_s_unityEventSystem_6() { return &___s_unityEventSystem_6; }
	inline void set_s_unityEventSystem_6(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___s_unityEventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_unityEventSystem_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICOMPONENTBASE_1_T64C56FBF7E4AFAA3D397341303F4A8DDD33F4843_H
#ifndef UIBUTTONLISTENER_T9B4F79D2098A92A608A52A396C408B25C969DF52_H
#define UIBUTTONLISTENER_T9B4F79D2098A92A608A52A396C408B25C969DF52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButtonListener
struct  UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Doozy.Engine.UI.UIButtonListener::ButtonCategory
	String_t* ___ButtonCategory_4;
	// System.String Doozy.Engine.UI.UIButtonListener::ButtonName
	String_t* ___ButtonName_5;
	// System.Boolean Doozy.Engine.UI.UIButtonListener::DebugMode
	bool ___DebugMode_6;
	// Doozy.Engine.UI.UIButtonEvent Doozy.Engine.UI.UIButtonListener::Event
	UIButtonEvent_t4D2122D7D8CDB28C0231D7429F5D12847FB55BD6 * ___Event_7;
	// System.Boolean Doozy.Engine.UI.UIButtonListener::ListenForAllUIButtons
	bool ___ListenForAllUIButtons_8;
	// Doozy.Engine.UI.UIButtonBehaviorType Doozy.Engine.UI.UIButtonListener::TriggerAction
	int32_t ___TriggerAction_9;

public:
	inline static int32_t get_offset_of_ButtonCategory_4() { return static_cast<int32_t>(offsetof(UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52, ___ButtonCategory_4)); }
	inline String_t* get_ButtonCategory_4() const { return ___ButtonCategory_4; }
	inline String_t** get_address_of_ButtonCategory_4() { return &___ButtonCategory_4; }
	inline void set_ButtonCategory_4(String_t* value)
	{
		___ButtonCategory_4 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonCategory_4), value);
	}

	inline static int32_t get_offset_of_ButtonName_5() { return static_cast<int32_t>(offsetof(UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52, ___ButtonName_5)); }
	inline String_t* get_ButtonName_5() const { return ___ButtonName_5; }
	inline String_t** get_address_of_ButtonName_5() { return &___ButtonName_5; }
	inline void set_ButtonName_5(String_t* value)
	{
		___ButtonName_5 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonName_5), value);
	}

	inline static int32_t get_offset_of_DebugMode_6() { return static_cast<int32_t>(offsetof(UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52, ___DebugMode_6)); }
	inline bool get_DebugMode_6() const { return ___DebugMode_6; }
	inline bool* get_address_of_DebugMode_6() { return &___DebugMode_6; }
	inline void set_DebugMode_6(bool value)
	{
		___DebugMode_6 = value;
	}

	inline static int32_t get_offset_of_Event_7() { return static_cast<int32_t>(offsetof(UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52, ___Event_7)); }
	inline UIButtonEvent_t4D2122D7D8CDB28C0231D7429F5D12847FB55BD6 * get_Event_7() const { return ___Event_7; }
	inline UIButtonEvent_t4D2122D7D8CDB28C0231D7429F5D12847FB55BD6 ** get_address_of_Event_7() { return &___Event_7; }
	inline void set_Event_7(UIButtonEvent_t4D2122D7D8CDB28C0231D7429F5D12847FB55BD6 * value)
	{
		___Event_7 = value;
		Il2CppCodeGenWriteBarrier((&___Event_7), value);
	}

	inline static int32_t get_offset_of_ListenForAllUIButtons_8() { return static_cast<int32_t>(offsetof(UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52, ___ListenForAllUIButtons_8)); }
	inline bool get_ListenForAllUIButtons_8() const { return ___ListenForAllUIButtons_8; }
	inline bool* get_address_of_ListenForAllUIButtons_8() { return &___ListenForAllUIButtons_8; }
	inline void set_ListenForAllUIButtons_8(bool value)
	{
		___ListenForAllUIButtons_8 = value;
	}

	inline static int32_t get_offset_of_TriggerAction_9() { return static_cast<int32_t>(offsetof(UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52, ___TriggerAction_9)); }
	inline int32_t get_TriggerAction_9() const { return ___TriggerAction_9; }
	inline int32_t* get_address_of_TriggerAction_9() { return &___TriggerAction_9; }
	inline void set_TriggerAction_9(int32_t value)
	{
		___TriggerAction_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONLISTENER_T9B4F79D2098A92A608A52A396C408B25C969DF52_H
#ifndef UIDRAWERARROWANIMATOR_T6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E_H
#define UIDRAWERARROWANIMATOR_T6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIDrawerArrowAnimator
struct  UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Doozy.Engine.UI.UIDrawer Doozy.Engine.UI.UIDrawerArrowAnimator::<Drawer>k__BackingField
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A * ___U3CDrawerU3Ek__BackingField_7;
	// System.Single Doozy.Engine.UI.UIDrawerArrowAnimator::<Width>k__BackingField
	float ___U3CWidthU3Ek__BackingField_8;
	// System.Single Doozy.Engine.UI.UIDrawerArrowAnimator::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_9;
	// UnityEngine.RectTransform Doozy.Engine.UI.UIDrawerArrowAnimator::Rotator
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___Rotator_10;
	// UnityEngine.RectTransform Doozy.Engine.UI.UIDrawerArrowAnimator::LeftBar
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___LeftBar_11;
	// UnityEngine.RectTransform Doozy.Engine.UI.UIDrawerArrowAnimator::RightBar
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___RightBar_12;
	// UnityEngine.RectTransform Doozy.Engine.UI.UIDrawerArrowAnimator::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_13;
	// UnityEngine.UI.Image Doozy.Engine.UI.UIDrawerArrowAnimator::m_leftBarImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_leftBarImage_14;
	// UnityEngine.UI.Image Doozy.Engine.UI.UIDrawerArrowAnimator::m_rightBarImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_rightBarImage_15;
	// System.Single Doozy.Engine.UI.UIDrawerArrowAnimator::m_velocity
	float ___m_velocity_16;
	// UnityEngine.Vector3[] Doozy.Engine.UI.UIDrawerArrowAnimator::m_rotatorCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_rotatorCorners_17;
	// UnityEngine.Vector3[] Doozy.Engine.UI.UIDrawerArrowAnimator::m_drawerCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_drawerCorners_18;
	// System.Single Doozy.Engine.UI.UIDrawerArrowAnimator::m_rotatorDisableThreshold
	float ___m_rotatorDisableThreshold_19;
	// UnityEngine.Vector3[] Doozy.Engine.UI.UIDrawerArrowAnimator::m_tempCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_tempCorners_20;
	// UnityEngine.Rect Doozy.Engine.UI.UIDrawerArrowAnimator::m_rotatorRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___m_rotatorRect_21;

public:
	inline static int32_t get_offset_of_U3CDrawerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___U3CDrawerU3Ek__BackingField_7)); }
	inline UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A * get_U3CDrawerU3Ek__BackingField_7() const { return ___U3CDrawerU3Ek__BackingField_7; }
	inline UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A ** get_address_of_U3CDrawerU3Ek__BackingField_7() { return &___U3CDrawerU3Ek__BackingField_7; }
	inline void set_U3CDrawerU3Ek__BackingField_7(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A * value)
	{
		___U3CDrawerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDrawerU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___U3CWidthU3Ek__BackingField_8)); }
	inline float get_U3CWidthU3Ek__BackingField_8() const { return ___U3CWidthU3Ek__BackingField_8; }
	inline float* get_address_of_U3CWidthU3Ek__BackingField_8() { return &___U3CWidthU3Ek__BackingField_8; }
	inline void set_U3CWidthU3Ek__BackingField_8(float value)
	{
		___U3CWidthU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___U3CHeightU3Ek__BackingField_9)); }
	inline float get_U3CHeightU3Ek__BackingField_9() const { return ___U3CHeightU3Ek__BackingField_9; }
	inline float* get_address_of_U3CHeightU3Ek__BackingField_9() { return &___U3CHeightU3Ek__BackingField_9; }
	inline void set_U3CHeightU3Ek__BackingField_9(float value)
	{
		___U3CHeightU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_Rotator_10() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___Rotator_10)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_Rotator_10() const { return ___Rotator_10; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_Rotator_10() { return &___Rotator_10; }
	inline void set_Rotator_10(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___Rotator_10 = value;
		Il2CppCodeGenWriteBarrier((&___Rotator_10), value);
	}

	inline static int32_t get_offset_of_LeftBar_11() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___LeftBar_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_LeftBar_11() const { return ___LeftBar_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_LeftBar_11() { return &___LeftBar_11; }
	inline void set_LeftBar_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___LeftBar_11 = value;
		Il2CppCodeGenWriteBarrier((&___LeftBar_11), value);
	}

	inline static int32_t get_offset_of_RightBar_12() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___RightBar_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_RightBar_12() const { return ___RightBar_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_RightBar_12() { return &___RightBar_12; }
	inline void set_RightBar_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___RightBar_12 = value;
		Il2CppCodeGenWriteBarrier((&___RightBar_12), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_13() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___m_rectTransform_13)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_13() const { return ___m_rectTransform_13; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_13() { return &___m_rectTransform_13; }
	inline void set_m_rectTransform_13(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_13), value);
	}

	inline static int32_t get_offset_of_m_leftBarImage_14() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___m_leftBarImage_14)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_leftBarImage_14() const { return ___m_leftBarImage_14; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_leftBarImage_14() { return &___m_leftBarImage_14; }
	inline void set_m_leftBarImage_14(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_leftBarImage_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_leftBarImage_14), value);
	}

	inline static int32_t get_offset_of_m_rightBarImage_15() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___m_rightBarImage_15)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_rightBarImage_15() const { return ___m_rightBarImage_15; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_rightBarImage_15() { return &___m_rightBarImage_15; }
	inline void set_m_rightBarImage_15(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_rightBarImage_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_rightBarImage_15), value);
	}

	inline static int32_t get_offset_of_m_velocity_16() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___m_velocity_16)); }
	inline float get_m_velocity_16() const { return ___m_velocity_16; }
	inline float* get_address_of_m_velocity_16() { return &___m_velocity_16; }
	inline void set_m_velocity_16(float value)
	{
		___m_velocity_16 = value;
	}

	inline static int32_t get_offset_of_m_rotatorCorners_17() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___m_rotatorCorners_17)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_rotatorCorners_17() const { return ___m_rotatorCorners_17; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_rotatorCorners_17() { return &___m_rotatorCorners_17; }
	inline void set_m_rotatorCorners_17(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_rotatorCorners_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_rotatorCorners_17), value);
	}

	inline static int32_t get_offset_of_m_drawerCorners_18() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___m_drawerCorners_18)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_drawerCorners_18() const { return ___m_drawerCorners_18; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_drawerCorners_18() { return &___m_drawerCorners_18; }
	inline void set_m_drawerCorners_18(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_drawerCorners_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_drawerCorners_18), value);
	}

	inline static int32_t get_offset_of_m_rotatorDisableThreshold_19() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___m_rotatorDisableThreshold_19)); }
	inline float get_m_rotatorDisableThreshold_19() const { return ___m_rotatorDisableThreshold_19; }
	inline float* get_address_of_m_rotatorDisableThreshold_19() { return &___m_rotatorDisableThreshold_19; }
	inline void set_m_rotatorDisableThreshold_19(float value)
	{
		___m_rotatorDisableThreshold_19 = value;
	}

	inline static int32_t get_offset_of_m_tempCorners_20() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___m_tempCorners_20)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_tempCorners_20() const { return ___m_tempCorners_20; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_tempCorners_20() { return &___m_tempCorners_20; }
	inline void set_m_tempCorners_20(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_tempCorners_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_tempCorners_20), value);
	}

	inline static int32_t get_offset_of_m_rotatorRect_21() { return static_cast<int32_t>(offsetof(UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E, ___m_rotatorRect_21)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_m_rotatorRect_21() const { return ___m_rotatorRect_21; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_m_rotatorRect_21() { return &___m_rotatorRect_21; }
	inline void set_m_rotatorRect_21(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___m_rotatorRect_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWERARROWANIMATOR_T6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E_H
#ifndef UIDRAWERLISTENER_T068FA313F449D3B14A5B81D2EFD270F3C812310A_H
#define UIDRAWERLISTENER_T068FA313F449D3B14A5B81D2EFD270F3C812310A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIDrawerListener
struct  UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Doozy.Engine.UI.UIDrawerListener::DebugMode
	bool ___DebugMode_4;
	// System.String Doozy.Engine.UI.UIDrawerListener::DrawerName
	String_t* ___DrawerName_5;
	// System.Boolean Doozy.Engine.UI.UIDrawerListener::CustomDrawerName
	bool ___CustomDrawerName_6;
	// Doozy.Engine.UI.UIDrawerEvent Doozy.Engine.UI.UIDrawerListener::Event
	UIDrawerEvent_t511B9CCB479D30EE935B3EA995DBB00D281721F6 * ___Event_7;
	// System.Boolean Doozy.Engine.UI.UIDrawerListener::ListenForAllUIDrawers
	bool ___ListenForAllUIDrawers_8;
	// Doozy.Engine.UI.UIDrawerBehaviorType Doozy.Engine.UI.UIDrawerListener::TriggerAction
	int32_t ___TriggerAction_9;

public:
	inline static int32_t get_offset_of_DebugMode_4() { return static_cast<int32_t>(offsetof(UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A, ___DebugMode_4)); }
	inline bool get_DebugMode_4() const { return ___DebugMode_4; }
	inline bool* get_address_of_DebugMode_4() { return &___DebugMode_4; }
	inline void set_DebugMode_4(bool value)
	{
		___DebugMode_4 = value;
	}

	inline static int32_t get_offset_of_DrawerName_5() { return static_cast<int32_t>(offsetof(UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A, ___DrawerName_5)); }
	inline String_t* get_DrawerName_5() const { return ___DrawerName_5; }
	inline String_t** get_address_of_DrawerName_5() { return &___DrawerName_5; }
	inline void set_DrawerName_5(String_t* value)
	{
		___DrawerName_5 = value;
		Il2CppCodeGenWriteBarrier((&___DrawerName_5), value);
	}

	inline static int32_t get_offset_of_CustomDrawerName_6() { return static_cast<int32_t>(offsetof(UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A, ___CustomDrawerName_6)); }
	inline bool get_CustomDrawerName_6() const { return ___CustomDrawerName_6; }
	inline bool* get_address_of_CustomDrawerName_6() { return &___CustomDrawerName_6; }
	inline void set_CustomDrawerName_6(bool value)
	{
		___CustomDrawerName_6 = value;
	}

	inline static int32_t get_offset_of_Event_7() { return static_cast<int32_t>(offsetof(UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A, ___Event_7)); }
	inline UIDrawerEvent_t511B9CCB479D30EE935B3EA995DBB00D281721F6 * get_Event_7() const { return ___Event_7; }
	inline UIDrawerEvent_t511B9CCB479D30EE935B3EA995DBB00D281721F6 ** get_address_of_Event_7() { return &___Event_7; }
	inline void set_Event_7(UIDrawerEvent_t511B9CCB479D30EE935B3EA995DBB00D281721F6 * value)
	{
		___Event_7 = value;
		Il2CppCodeGenWriteBarrier((&___Event_7), value);
	}

	inline static int32_t get_offset_of_ListenForAllUIDrawers_8() { return static_cast<int32_t>(offsetof(UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A, ___ListenForAllUIDrawers_8)); }
	inline bool get_ListenForAllUIDrawers_8() const { return ___ListenForAllUIDrawers_8; }
	inline bool* get_address_of_ListenForAllUIDrawers_8() { return &___ListenForAllUIDrawers_8; }
	inline void set_ListenForAllUIDrawers_8(bool value)
	{
		___ListenForAllUIDrawers_8 = value;
	}

	inline static int32_t get_offset_of_TriggerAction_9() { return static_cast<int32_t>(offsetof(UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A, ___TriggerAction_9)); }
	inline int32_t get_TriggerAction_9() const { return ___TriggerAction_9; }
	inline int32_t* get_address_of_TriggerAction_9() { return &___TriggerAction_9; }
	inline void set_TriggerAction_9(int32_t value)
	{
		___TriggerAction_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWERLISTENER_T068FA313F449D3B14A5B81D2EFD270F3C812310A_H
#ifndef UIPOPUPMANAGER_TFD00A6918320B8C6193C0F5BE38E305703751156_H
#define UIPOPUPMANAGER_TFD00A6918320B8C6193C0F5BE38E305703751156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopupManager
struct  UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156_StaticFields
{
public:
	// Doozy.Engine.UI.UIPopupManager Doozy.Engine.UI.UIPopupManager::s_instance
	UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156 * ___s_instance_4;
	// Doozy.Engine.UI.UIPopup Doozy.Engine.UI.UIPopupManager::CurrentVisibleQueuePopup
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * ___CurrentVisibleQueuePopup_5;
	// System.Collections.Generic.List`1<Doozy.Engine.UI.UIPopupQueueData> Doozy.Engine.UI.UIPopupManager::PopupQueue
	List_1_tBE6FAF4F305CCD5D19B8FFFFEE3E982DB3AF3EE6 * ___PopupQueue_6;
	// System.Boolean Doozy.Engine.UI.UIPopupManager::<ApplicationIsQuitting>k__BackingField
	bool ___U3CApplicationIsQuittingU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_s_instance_4() { return static_cast<int32_t>(offsetof(UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156_StaticFields, ___s_instance_4)); }
	inline UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156 * get_s_instance_4() const { return ___s_instance_4; }
	inline UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156 ** get_address_of_s_instance_4() { return &___s_instance_4; }
	inline void set_s_instance_4(UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156 * value)
	{
		___s_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_4), value);
	}

	inline static int32_t get_offset_of_CurrentVisibleQueuePopup_5() { return static_cast<int32_t>(offsetof(UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156_StaticFields, ___CurrentVisibleQueuePopup_5)); }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * get_CurrentVisibleQueuePopup_5() const { return ___CurrentVisibleQueuePopup_5; }
	inline UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 ** get_address_of_CurrentVisibleQueuePopup_5() { return &___CurrentVisibleQueuePopup_5; }
	inline void set_CurrentVisibleQueuePopup_5(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228 * value)
	{
		___CurrentVisibleQueuePopup_5 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentVisibleQueuePopup_5), value);
	}

	inline static int32_t get_offset_of_PopupQueue_6() { return static_cast<int32_t>(offsetof(UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156_StaticFields, ___PopupQueue_6)); }
	inline List_1_tBE6FAF4F305CCD5D19B8FFFFEE3E982DB3AF3EE6 * get_PopupQueue_6() const { return ___PopupQueue_6; }
	inline List_1_tBE6FAF4F305CCD5D19B8FFFFEE3E982DB3AF3EE6 ** get_address_of_PopupQueue_6() { return &___PopupQueue_6; }
	inline void set_PopupQueue_6(List_1_tBE6FAF4F305CCD5D19B8FFFFEE3E982DB3AF3EE6 * value)
	{
		___PopupQueue_6 = value;
		Il2CppCodeGenWriteBarrier((&___PopupQueue_6), value);
	}

	inline static int32_t get_offset_of_U3CApplicationIsQuittingU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156_StaticFields, ___U3CApplicationIsQuittingU3Ek__BackingField_7)); }
	inline bool get_U3CApplicationIsQuittingU3Ek__BackingField_7() const { return ___U3CApplicationIsQuittingU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CApplicationIsQuittingU3Ek__BackingField_7() { return &___U3CApplicationIsQuittingU3Ek__BackingField_7; }
	inline void set_U3CApplicationIsQuittingU3Ek__BackingField_7(bool value)
	{
		___U3CApplicationIsQuittingU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOPUPMANAGER_TFD00A6918320B8C6193C0F5BE38E305703751156_H
#ifndef UIBUTTON_TE09DCDE2C5CC3714E7EAC47596EB415B340DC527_H
#define UIBUTTON_TE09DCDE2C5CC3714E7EAC47596EB415B340DC527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIButton
struct  UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527  : public UIComponentBase_1_t1949D0532AD1F86A71653A26F39CB15528B706C1
{
public:
	// System.Boolean Doozy.Engine.UI.UIButton::AllowMultipleClicks
	bool ___AllowMultipleClicks_14;
	// System.String Doozy.Engine.UI.UIButton::ButtonCategory
	String_t* ___ButtonCategory_15;
	// System.String Doozy.Engine.UI.UIButton::ButtonName
	String_t* ___ButtonName_16;
	// Doozy.Engine.UI.SingleClickMode Doozy.Engine.UI.UIButton::ClickMode
	int32_t ___ClickMode_17;
	// System.Boolean Doozy.Engine.UI.UIButton::DeselectButtonAfterClick
	bool ___DeselectButtonAfterClick_18;
	// System.Single Doozy.Engine.UI.UIButton::DisableButtonBetweenClicksInterval
	float ___DisableButtonBetweenClicksInterval_19;
	// System.Single Doozy.Engine.UI.UIButton::DoubleClickRegisterInterval
	float ___DoubleClickRegisterInterval_20;
	// Doozy.Engine.UI.Input.InputData Doozy.Engine.UI.UIButton::InputData
	InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC * ___InputData_21;
	// System.Single Doozy.Engine.UI.UIButton::LongClickRegisterInterval
	float ___LongClickRegisterInterval_22;
	// Doozy.Engine.UI.UIButtonBehavior Doozy.Engine.UI.UIButton::OnPointerEnter
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * ___OnPointerEnter_23;
	// Doozy.Engine.UI.UIButtonBehavior Doozy.Engine.UI.UIButton::OnPointerExit
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * ___OnPointerExit_24;
	// Doozy.Engine.UI.UIButtonBehavior Doozy.Engine.UI.UIButton::OnPointerDown
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * ___OnPointerDown_25;
	// Doozy.Engine.UI.UIButtonBehavior Doozy.Engine.UI.UIButton::OnPointerUp
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * ___OnPointerUp_26;
	// Doozy.Engine.UI.UIButtonBehavior Doozy.Engine.UI.UIButton::OnClick
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * ___OnClick_27;
	// Doozy.Engine.UI.UIButtonBehavior Doozy.Engine.UI.UIButton::OnDoubleClick
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * ___OnDoubleClick_28;
	// Doozy.Engine.UI.UIButtonBehavior Doozy.Engine.UI.UIButton::OnLongClick
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * ___OnLongClick_29;
	// Doozy.Engine.UI.UIButtonBehavior Doozy.Engine.UI.UIButton::OnSelected
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * ___OnSelected_30;
	// Doozy.Engine.UI.UIButtonBehavior Doozy.Engine.UI.UIButton::OnDeselected
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * ___OnDeselected_31;
	// Doozy.Engine.UI.UIButtonLoopAnimation Doozy.Engine.UI.UIButton::NormalLoopAnimation
	UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45 * ___NormalLoopAnimation_32;
	// Doozy.Engine.UI.UIButtonLoopAnimation Doozy.Engine.UI.UIButton::SelectedLoopAnimation
	UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45 * ___SelectedLoopAnimation_33;
	// Doozy.Engine.UI.TargetLabel Doozy.Engine.UI.UIButton::TargetLabel
	int32_t ___TargetLabel_34;
	// UnityEngine.UI.Text Doozy.Engine.UI.UIButton::TextLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___TextLabel_35;
	// TMPro.TextMeshProUGUI Doozy.Engine.UI.UIButton::TextMeshProLabel
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___TextMeshProLabel_36;
	// UnityEngine.UI.Button Doozy.Engine.UI.UIButton::m_button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_button_37;
	// UnityEngine.CanvasGroup Doozy.Engine.UI.UIButton::m_canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___m_canvasGroup_38;
	// System.Boolean Doozy.Engine.UI.UIButton::m_clickedOnce
	bool ___m_clickedOnce_39;
	// UnityEngine.Coroutine Doozy.Engine.UI.UIButton::m_disableButtonCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_disableButtonCoroutine_40;
	// System.Single Doozy.Engine.UI.UIButton::m_doubleClickTimeoutCounter
	float ___m_doubleClickTimeoutCounter_41;
	// System.Boolean Doozy.Engine.UI.UIButton::m_executedLongClick
	bool ___m_executedLongClick_42;
	// UnityEngine.Coroutine Doozy.Engine.UI.UIButton::m_longClickRegisterCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_longClickRegisterCoroutine_43;
	// System.Single Doozy.Engine.UI.UIButton::m_longClickTimeoutCounter
	float ___m_longClickTimeoutCounter_44;
	// System.Boolean Doozy.Engine.UI.UIButton::m_updateStartValuesRequired
	bool ___m_updateStartValuesRequired_45;

public:
	inline static int32_t get_offset_of_AllowMultipleClicks_14() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___AllowMultipleClicks_14)); }
	inline bool get_AllowMultipleClicks_14() const { return ___AllowMultipleClicks_14; }
	inline bool* get_address_of_AllowMultipleClicks_14() { return &___AllowMultipleClicks_14; }
	inline void set_AllowMultipleClicks_14(bool value)
	{
		___AllowMultipleClicks_14 = value;
	}

	inline static int32_t get_offset_of_ButtonCategory_15() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___ButtonCategory_15)); }
	inline String_t* get_ButtonCategory_15() const { return ___ButtonCategory_15; }
	inline String_t** get_address_of_ButtonCategory_15() { return &___ButtonCategory_15; }
	inline void set_ButtonCategory_15(String_t* value)
	{
		___ButtonCategory_15 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonCategory_15), value);
	}

	inline static int32_t get_offset_of_ButtonName_16() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___ButtonName_16)); }
	inline String_t* get_ButtonName_16() const { return ___ButtonName_16; }
	inline String_t** get_address_of_ButtonName_16() { return &___ButtonName_16; }
	inline void set_ButtonName_16(String_t* value)
	{
		___ButtonName_16 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonName_16), value);
	}

	inline static int32_t get_offset_of_ClickMode_17() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___ClickMode_17)); }
	inline int32_t get_ClickMode_17() const { return ___ClickMode_17; }
	inline int32_t* get_address_of_ClickMode_17() { return &___ClickMode_17; }
	inline void set_ClickMode_17(int32_t value)
	{
		___ClickMode_17 = value;
	}

	inline static int32_t get_offset_of_DeselectButtonAfterClick_18() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___DeselectButtonAfterClick_18)); }
	inline bool get_DeselectButtonAfterClick_18() const { return ___DeselectButtonAfterClick_18; }
	inline bool* get_address_of_DeselectButtonAfterClick_18() { return &___DeselectButtonAfterClick_18; }
	inline void set_DeselectButtonAfterClick_18(bool value)
	{
		___DeselectButtonAfterClick_18 = value;
	}

	inline static int32_t get_offset_of_DisableButtonBetweenClicksInterval_19() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___DisableButtonBetweenClicksInterval_19)); }
	inline float get_DisableButtonBetweenClicksInterval_19() const { return ___DisableButtonBetweenClicksInterval_19; }
	inline float* get_address_of_DisableButtonBetweenClicksInterval_19() { return &___DisableButtonBetweenClicksInterval_19; }
	inline void set_DisableButtonBetweenClicksInterval_19(float value)
	{
		___DisableButtonBetweenClicksInterval_19 = value;
	}

	inline static int32_t get_offset_of_DoubleClickRegisterInterval_20() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___DoubleClickRegisterInterval_20)); }
	inline float get_DoubleClickRegisterInterval_20() const { return ___DoubleClickRegisterInterval_20; }
	inline float* get_address_of_DoubleClickRegisterInterval_20() { return &___DoubleClickRegisterInterval_20; }
	inline void set_DoubleClickRegisterInterval_20(float value)
	{
		___DoubleClickRegisterInterval_20 = value;
	}

	inline static int32_t get_offset_of_InputData_21() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___InputData_21)); }
	inline InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC * get_InputData_21() const { return ___InputData_21; }
	inline InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC ** get_address_of_InputData_21() { return &___InputData_21; }
	inline void set_InputData_21(InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC * value)
	{
		___InputData_21 = value;
		Il2CppCodeGenWriteBarrier((&___InputData_21), value);
	}

	inline static int32_t get_offset_of_LongClickRegisterInterval_22() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___LongClickRegisterInterval_22)); }
	inline float get_LongClickRegisterInterval_22() const { return ___LongClickRegisterInterval_22; }
	inline float* get_address_of_LongClickRegisterInterval_22() { return &___LongClickRegisterInterval_22; }
	inline void set_LongClickRegisterInterval_22(float value)
	{
		___LongClickRegisterInterval_22 = value;
	}

	inline static int32_t get_offset_of_OnPointerEnter_23() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___OnPointerEnter_23)); }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * get_OnPointerEnter_23() const { return ___OnPointerEnter_23; }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB ** get_address_of_OnPointerEnter_23() { return &___OnPointerEnter_23; }
	inline void set_OnPointerEnter_23(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * value)
	{
		___OnPointerEnter_23 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerEnter_23), value);
	}

	inline static int32_t get_offset_of_OnPointerExit_24() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___OnPointerExit_24)); }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * get_OnPointerExit_24() const { return ___OnPointerExit_24; }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB ** get_address_of_OnPointerExit_24() { return &___OnPointerExit_24; }
	inline void set_OnPointerExit_24(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * value)
	{
		___OnPointerExit_24 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerExit_24), value);
	}

	inline static int32_t get_offset_of_OnPointerDown_25() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___OnPointerDown_25)); }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * get_OnPointerDown_25() const { return ___OnPointerDown_25; }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB ** get_address_of_OnPointerDown_25() { return &___OnPointerDown_25; }
	inline void set_OnPointerDown_25(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * value)
	{
		___OnPointerDown_25 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerDown_25), value);
	}

	inline static int32_t get_offset_of_OnPointerUp_26() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___OnPointerUp_26)); }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * get_OnPointerUp_26() const { return ___OnPointerUp_26; }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB ** get_address_of_OnPointerUp_26() { return &___OnPointerUp_26; }
	inline void set_OnPointerUp_26(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * value)
	{
		___OnPointerUp_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerUp_26), value);
	}

	inline static int32_t get_offset_of_OnClick_27() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___OnClick_27)); }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * get_OnClick_27() const { return ___OnClick_27; }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB ** get_address_of_OnClick_27() { return &___OnClick_27; }
	inline void set_OnClick_27(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * value)
	{
		___OnClick_27 = value;
		Il2CppCodeGenWriteBarrier((&___OnClick_27), value);
	}

	inline static int32_t get_offset_of_OnDoubleClick_28() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___OnDoubleClick_28)); }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * get_OnDoubleClick_28() const { return ___OnDoubleClick_28; }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB ** get_address_of_OnDoubleClick_28() { return &___OnDoubleClick_28; }
	inline void set_OnDoubleClick_28(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * value)
	{
		___OnDoubleClick_28 = value;
		Il2CppCodeGenWriteBarrier((&___OnDoubleClick_28), value);
	}

	inline static int32_t get_offset_of_OnLongClick_29() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___OnLongClick_29)); }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * get_OnLongClick_29() const { return ___OnLongClick_29; }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB ** get_address_of_OnLongClick_29() { return &___OnLongClick_29; }
	inline void set_OnLongClick_29(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * value)
	{
		___OnLongClick_29 = value;
		Il2CppCodeGenWriteBarrier((&___OnLongClick_29), value);
	}

	inline static int32_t get_offset_of_OnSelected_30() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___OnSelected_30)); }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * get_OnSelected_30() const { return ___OnSelected_30; }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB ** get_address_of_OnSelected_30() { return &___OnSelected_30; }
	inline void set_OnSelected_30(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * value)
	{
		___OnSelected_30 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelected_30), value);
	}

	inline static int32_t get_offset_of_OnDeselected_31() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___OnDeselected_31)); }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * get_OnDeselected_31() const { return ___OnDeselected_31; }
	inline UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB ** get_address_of_OnDeselected_31() { return &___OnDeselected_31; }
	inline void set_OnDeselected_31(UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB * value)
	{
		___OnDeselected_31 = value;
		Il2CppCodeGenWriteBarrier((&___OnDeselected_31), value);
	}

	inline static int32_t get_offset_of_NormalLoopAnimation_32() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___NormalLoopAnimation_32)); }
	inline UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45 * get_NormalLoopAnimation_32() const { return ___NormalLoopAnimation_32; }
	inline UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45 ** get_address_of_NormalLoopAnimation_32() { return &___NormalLoopAnimation_32; }
	inline void set_NormalLoopAnimation_32(UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45 * value)
	{
		___NormalLoopAnimation_32 = value;
		Il2CppCodeGenWriteBarrier((&___NormalLoopAnimation_32), value);
	}

	inline static int32_t get_offset_of_SelectedLoopAnimation_33() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___SelectedLoopAnimation_33)); }
	inline UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45 * get_SelectedLoopAnimation_33() const { return ___SelectedLoopAnimation_33; }
	inline UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45 ** get_address_of_SelectedLoopAnimation_33() { return &___SelectedLoopAnimation_33; }
	inline void set_SelectedLoopAnimation_33(UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45 * value)
	{
		___SelectedLoopAnimation_33 = value;
		Il2CppCodeGenWriteBarrier((&___SelectedLoopAnimation_33), value);
	}

	inline static int32_t get_offset_of_TargetLabel_34() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___TargetLabel_34)); }
	inline int32_t get_TargetLabel_34() const { return ___TargetLabel_34; }
	inline int32_t* get_address_of_TargetLabel_34() { return &___TargetLabel_34; }
	inline void set_TargetLabel_34(int32_t value)
	{
		___TargetLabel_34 = value;
	}

	inline static int32_t get_offset_of_TextLabel_35() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___TextLabel_35)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_TextLabel_35() const { return ___TextLabel_35; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_TextLabel_35() { return &___TextLabel_35; }
	inline void set_TextLabel_35(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___TextLabel_35 = value;
		Il2CppCodeGenWriteBarrier((&___TextLabel_35), value);
	}

	inline static int32_t get_offset_of_TextMeshProLabel_36() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___TextMeshProLabel_36)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_TextMeshProLabel_36() const { return ___TextMeshProLabel_36; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_TextMeshProLabel_36() { return &___TextMeshProLabel_36; }
	inline void set_TextMeshProLabel_36(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___TextMeshProLabel_36 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshProLabel_36), value);
	}

	inline static int32_t get_offset_of_m_button_37() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___m_button_37)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_button_37() const { return ___m_button_37; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_button_37() { return &___m_button_37; }
	inline void set_m_button_37(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_button_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_button_37), value);
	}

	inline static int32_t get_offset_of_m_canvasGroup_38() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___m_canvasGroup_38)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_m_canvasGroup_38() const { return ___m_canvasGroup_38; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_m_canvasGroup_38() { return &___m_canvasGroup_38; }
	inline void set_m_canvasGroup_38(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___m_canvasGroup_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasGroup_38), value);
	}

	inline static int32_t get_offset_of_m_clickedOnce_39() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___m_clickedOnce_39)); }
	inline bool get_m_clickedOnce_39() const { return ___m_clickedOnce_39; }
	inline bool* get_address_of_m_clickedOnce_39() { return &___m_clickedOnce_39; }
	inline void set_m_clickedOnce_39(bool value)
	{
		___m_clickedOnce_39 = value;
	}

	inline static int32_t get_offset_of_m_disableButtonCoroutine_40() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___m_disableButtonCoroutine_40)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_disableButtonCoroutine_40() const { return ___m_disableButtonCoroutine_40; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_disableButtonCoroutine_40() { return &___m_disableButtonCoroutine_40; }
	inline void set_m_disableButtonCoroutine_40(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_disableButtonCoroutine_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_disableButtonCoroutine_40), value);
	}

	inline static int32_t get_offset_of_m_doubleClickTimeoutCounter_41() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___m_doubleClickTimeoutCounter_41)); }
	inline float get_m_doubleClickTimeoutCounter_41() const { return ___m_doubleClickTimeoutCounter_41; }
	inline float* get_address_of_m_doubleClickTimeoutCounter_41() { return &___m_doubleClickTimeoutCounter_41; }
	inline void set_m_doubleClickTimeoutCounter_41(float value)
	{
		___m_doubleClickTimeoutCounter_41 = value;
	}

	inline static int32_t get_offset_of_m_executedLongClick_42() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___m_executedLongClick_42)); }
	inline bool get_m_executedLongClick_42() const { return ___m_executedLongClick_42; }
	inline bool* get_address_of_m_executedLongClick_42() { return &___m_executedLongClick_42; }
	inline void set_m_executedLongClick_42(bool value)
	{
		___m_executedLongClick_42 = value;
	}

	inline static int32_t get_offset_of_m_longClickRegisterCoroutine_43() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___m_longClickRegisterCoroutine_43)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_longClickRegisterCoroutine_43() const { return ___m_longClickRegisterCoroutine_43; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_longClickRegisterCoroutine_43() { return &___m_longClickRegisterCoroutine_43; }
	inline void set_m_longClickRegisterCoroutine_43(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_longClickRegisterCoroutine_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_longClickRegisterCoroutine_43), value);
	}

	inline static int32_t get_offset_of_m_longClickTimeoutCounter_44() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___m_longClickTimeoutCounter_44)); }
	inline float get_m_longClickTimeoutCounter_44() const { return ___m_longClickTimeoutCounter_44; }
	inline float* get_address_of_m_longClickTimeoutCounter_44() { return &___m_longClickTimeoutCounter_44; }
	inline void set_m_longClickTimeoutCounter_44(float value)
	{
		___m_longClickTimeoutCounter_44 = value;
	}

	inline static int32_t get_offset_of_m_updateStartValuesRequired_45() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527, ___m_updateStartValuesRequired_45)); }
	inline bool get_m_updateStartValuesRequired_45() const { return ___m_updateStartValuesRequired_45; }
	inline bool* get_address_of_m_updateStartValuesRequired_45() { return &___m_updateStartValuesRequired_45; }
	inline void set_m_updateStartValuesRequired_45(bool value)
	{
		___m_updateStartValuesRequired_45 = value;
	}
};

struct UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527_StaticFields
{
public:
	// System.Action`2<Doozy.Engine.UI.UIButton,Doozy.Engine.UI.UIButtonBehaviorType> Doozy.Engine.UI.UIButton::OnUIButtonAction
	Action_2_tCF266DB9CF64AAECAF08BE47546A164E53C9A5A9 * ___OnUIButtonAction_13;

public:
	inline static int32_t get_offset_of_OnUIButtonAction_13() { return static_cast<int32_t>(offsetof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527_StaticFields, ___OnUIButtonAction_13)); }
	inline Action_2_tCF266DB9CF64AAECAF08BE47546A164E53C9A5A9 * get_OnUIButtonAction_13() const { return ___OnUIButtonAction_13; }
	inline Action_2_tCF266DB9CF64AAECAF08BE47546A164E53C9A5A9 ** get_address_of_OnUIButtonAction_13() { return &___OnUIButtonAction_13; }
	inline void set_OnUIButtonAction_13(Action_2_tCF266DB9CF64AAECAF08BE47546A164E53C9A5A9 * value)
	{
		___OnUIButtonAction_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnUIButtonAction_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTON_TE09DCDE2C5CC3714E7EAC47596EB415B340DC527_H
#ifndef UICANVAS_T1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37_H
#define UICANVAS_T1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UICanvas
struct  UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37  : public UIComponentBase_1_t41D90A74F073BFCFEAAFCC68E302571B5C2B3B44
{
public:
	// System.String Doozy.Engine.UI.UICanvas::CanvasName
	String_t* ___CanvasName_14;
	// System.Boolean Doozy.Engine.UI.UICanvas::CustomCanvasName
	bool ___CustomCanvasName_15;
	// System.Boolean Doozy.Engine.UI.UICanvas::DontDestroyCanvasOnLoad
	bool ___DontDestroyCanvasOnLoad_16;
	// UnityEngine.Canvas Doozy.Engine.UI.UICanvas::m_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_canvas_17;

public:
	inline static int32_t get_offset_of_CanvasName_14() { return static_cast<int32_t>(offsetof(UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37, ___CanvasName_14)); }
	inline String_t* get_CanvasName_14() const { return ___CanvasName_14; }
	inline String_t** get_address_of_CanvasName_14() { return &___CanvasName_14; }
	inline void set_CanvasName_14(String_t* value)
	{
		___CanvasName_14 = value;
		Il2CppCodeGenWriteBarrier((&___CanvasName_14), value);
	}

	inline static int32_t get_offset_of_CustomCanvasName_15() { return static_cast<int32_t>(offsetof(UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37, ___CustomCanvasName_15)); }
	inline bool get_CustomCanvasName_15() const { return ___CustomCanvasName_15; }
	inline bool* get_address_of_CustomCanvasName_15() { return &___CustomCanvasName_15; }
	inline void set_CustomCanvasName_15(bool value)
	{
		___CustomCanvasName_15 = value;
	}

	inline static int32_t get_offset_of_DontDestroyCanvasOnLoad_16() { return static_cast<int32_t>(offsetof(UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37, ___DontDestroyCanvasOnLoad_16)); }
	inline bool get_DontDestroyCanvasOnLoad_16() const { return ___DontDestroyCanvasOnLoad_16; }
	inline bool* get_address_of_DontDestroyCanvasOnLoad_16() { return &___DontDestroyCanvasOnLoad_16; }
	inline void set_DontDestroyCanvasOnLoad_16(bool value)
	{
		___DontDestroyCanvasOnLoad_16 = value;
	}

	inline static int32_t get_offset_of_m_canvas_17() { return static_cast<int32_t>(offsetof(UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37, ___m_canvas_17)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_canvas_17() const { return ___m_canvas_17; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_canvas_17() { return &___m_canvas_17; }
	inline void set_m_canvas_17(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_canvas_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_17), value);
	}
};

struct UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37_StaticFields
{
public:
	// Doozy.Engine.UI.UICanvas Doozy.Engine.UI.UICanvas::<MasterCanvas>k__BackingField
	UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37 * ___U3CMasterCanvasU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CMasterCanvasU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37_StaticFields, ___U3CMasterCanvasU3Ek__BackingField_13)); }
	inline UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37 * get_U3CMasterCanvasU3Ek__BackingField_13() const { return ___U3CMasterCanvasU3Ek__BackingField_13; }
	inline UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37 ** get_address_of_U3CMasterCanvasU3Ek__BackingField_13() { return &___U3CMasterCanvasU3Ek__BackingField_13; }
	inline void set_U3CMasterCanvasU3Ek__BackingField_13(UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37 * value)
	{
		___U3CMasterCanvasU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMasterCanvasU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICANVAS_T1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37_H
#ifndef UIDRAWER_TE812989F3C01F90F70B60FA2EF8D250C07A3427A_H
#define UIDRAWER_TE812989F3C01F90F70B60FA2EF8D250C07A3427A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIDrawer
struct  UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A  : public UIComponentBase_1_tEB9510B6EE4A41FE9DCC5F4264D4EEC247634ABF
{
public:
	// System.Boolean Doozy.Engine.UI.UIDrawer::<IsDragged>k__BackingField
	bool ___U3CIsDraggedU3Ek__BackingField_19;
	// Doozy.Engine.UI.UIDrawerArrow Doozy.Engine.UI.UIDrawer::Arrow
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3 * ___Arrow_20;
	// Doozy.Engine.UI.UIDrawerBehavior Doozy.Engine.UI.UIDrawer::CloseBehavior
	UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843 * ___CloseBehavior_21;
	// Doozy.Engine.Touchy.SimpleSwipe Doozy.Engine.UI.UIDrawer::CloseDirection
	int32_t ___CloseDirection_22;
	// System.Single Doozy.Engine.UI.UIDrawer::CloseSpeed
	float ___CloseSpeed_23;
	// Doozy.Engine.UI.UIDrawerContainer Doozy.Engine.UI.UIDrawer::Container
	UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C * ___Container_24;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIDrawer::CustomStartAnchoredPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___CustomStartAnchoredPosition_25;
	// System.Boolean Doozy.Engine.UI.UIDrawer::CustomDrawerName
	bool ___CustomDrawerName_26;
	// System.String Doozy.Engine.UI.UIDrawer::DrawerName
	String_t* ___DrawerName_27;
	// System.Boolean Doozy.Engine.UI.UIDrawer::DetectGestures
	bool ___DetectGestures_28;
	// Doozy.Engine.UI.UIDrawerBehavior Doozy.Engine.UI.UIDrawer::DragBehavior
	UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843 * ___DragBehavior_29;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.UI.UIDrawer::OnProgressChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnProgressChanged_30;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.UI.UIDrawer::OnInverseProgressChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnInverseProgressChanged_31;
	// Doozy.Engine.UI.UIDrawerBehavior Doozy.Engine.UI.UIDrawer::OpenBehavior
	UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843 * ___OpenBehavior_32;
	// System.Single Doozy.Engine.UI.UIDrawer::OpenSpeed
	float ___OpenSpeed_33;
	// Doozy.Engine.UI.Base.UIContainer Doozy.Engine.UI.UIDrawer::Overlay
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B * ___Overlay_34;
	// Doozy.Engine.Progress.Progressor Doozy.Engine.UI.UIDrawer::Progressor
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * ___Progressor_35;
	// System.Boolean Doozy.Engine.UI.UIDrawer::UseCustomStartAnchoredPosition
	bool ___UseCustomStartAnchoredPosition_36;
	// UnityEngine.Canvas Doozy.Engine.UI.UIDrawer::m_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_canvas_37;
	// Doozy.Engine.UI.VisibilityState Doozy.Engine.UI.UIDrawer::m_visibility
	int32_t ___m_visibility_38;
	// System.Single Doozy.Engine.UI.UIDrawer::m_visibilityProgress
	float ___m_visibilityProgress_39;
	// UnityEngine.Vector2 Doozy.Engine.UI.UIDrawer::m_scaledCanvas
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_scaledCanvas_40;
	// System.Boolean Doozy.Engine.UI.UIDrawer::m_availableForDrag
	bool ___m_availableForDrag_41;
	// UnityEngine.Vector2 Doozy.Engine.UI.UIDrawer::m_dragStartPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_dragStartPosition_42;

public:
	inline static int32_t get_offset_of_U3CIsDraggedU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___U3CIsDraggedU3Ek__BackingField_19)); }
	inline bool get_U3CIsDraggedU3Ek__BackingField_19() const { return ___U3CIsDraggedU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CIsDraggedU3Ek__BackingField_19() { return &___U3CIsDraggedU3Ek__BackingField_19; }
	inline void set_U3CIsDraggedU3Ek__BackingField_19(bool value)
	{
		___U3CIsDraggedU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_Arrow_20() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___Arrow_20)); }
	inline UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3 * get_Arrow_20() const { return ___Arrow_20; }
	inline UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3 ** get_address_of_Arrow_20() { return &___Arrow_20; }
	inline void set_Arrow_20(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3 * value)
	{
		___Arrow_20 = value;
		Il2CppCodeGenWriteBarrier((&___Arrow_20), value);
	}

	inline static int32_t get_offset_of_CloseBehavior_21() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___CloseBehavior_21)); }
	inline UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843 * get_CloseBehavior_21() const { return ___CloseBehavior_21; }
	inline UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843 ** get_address_of_CloseBehavior_21() { return &___CloseBehavior_21; }
	inline void set_CloseBehavior_21(UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843 * value)
	{
		___CloseBehavior_21 = value;
		Il2CppCodeGenWriteBarrier((&___CloseBehavior_21), value);
	}

	inline static int32_t get_offset_of_CloseDirection_22() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___CloseDirection_22)); }
	inline int32_t get_CloseDirection_22() const { return ___CloseDirection_22; }
	inline int32_t* get_address_of_CloseDirection_22() { return &___CloseDirection_22; }
	inline void set_CloseDirection_22(int32_t value)
	{
		___CloseDirection_22 = value;
	}

	inline static int32_t get_offset_of_CloseSpeed_23() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___CloseSpeed_23)); }
	inline float get_CloseSpeed_23() const { return ___CloseSpeed_23; }
	inline float* get_address_of_CloseSpeed_23() { return &___CloseSpeed_23; }
	inline void set_CloseSpeed_23(float value)
	{
		___CloseSpeed_23 = value;
	}

	inline static int32_t get_offset_of_Container_24() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___Container_24)); }
	inline UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C * get_Container_24() const { return ___Container_24; }
	inline UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C ** get_address_of_Container_24() { return &___Container_24; }
	inline void set_Container_24(UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C * value)
	{
		___Container_24 = value;
		Il2CppCodeGenWriteBarrier((&___Container_24), value);
	}

	inline static int32_t get_offset_of_CustomStartAnchoredPosition_25() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___CustomStartAnchoredPosition_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_CustomStartAnchoredPosition_25() const { return ___CustomStartAnchoredPosition_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_CustomStartAnchoredPosition_25() { return &___CustomStartAnchoredPosition_25; }
	inline void set_CustomStartAnchoredPosition_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___CustomStartAnchoredPosition_25 = value;
	}

	inline static int32_t get_offset_of_CustomDrawerName_26() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___CustomDrawerName_26)); }
	inline bool get_CustomDrawerName_26() const { return ___CustomDrawerName_26; }
	inline bool* get_address_of_CustomDrawerName_26() { return &___CustomDrawerName_26; }
	inline void set_CustomDrawerName_26(bool value)
	{
		___CustomDrawerName_26 = value;
	}

	inline static int32_t get_offset_of_DrawerName_27() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___DrawerName_27)); }
	inline String_t* get_DrawerName_27() const { return ___DrawerName_27; }
	inline String_t** get_address_of_DrawerName_27() { return &___DrawerName_27; }
	inline void set_DrawerName_27(String_t* value)
	{
		___DrawerName_27 = value;
		Il2CppCodeGenWriteBarrier((&___DrawerName_27), value);
	}

	inline static int32_t get_offset_of_DetectGestures_28() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___DetectGestures_28)); }
	inline bool get_DetectGestures_28() const { return ___DetectGestures_28; }
	inline bool* get_address_of_DetectGestures_28() { return &___DetectGestures_28; }
	inline void set_DetectGestures_28(bool value)
	{
		___DetectGestures_28 = value;
	}

	inline static int32_t get_offset_of_DragBehavior_29() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___DragBehavior_29)); }
	inline UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843 * get_DragBehavior_29() const { return ___DragBehavior_29; }
	inline UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843 ** get_address_of_DragBehavior_29() { return &___DragBehavior_29; }
	inline void set_DragBehavior_29(UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843 * value)
	{
		___DragBehavior_29 = value;
		Il2CppCodeGenWriteBarrier((&___DragBehavior_29), value);
	}

	inline static int32_t get_offset_of_OnProgressChanged_30() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___OnProgressChanged_30)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnProgressChanged_30() const { return ___OnProgressChanged_30; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnProgressChanged_30() { return &___OnProgressChanged_30; }
	inline void set_OnProgressChanged_30(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnProgressChanged_30 = value;
		Il2CppCodeGenWriteBarrier((&___OnProgressChanged_30), value);
	}

	inline static int32_t get_offset_of_OnInverseProgressChanged_31() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___OnInverseProgressChanged_31)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnInverseProgressChanged_31() const { return ___OnInverseProgressChanged_31; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnInverseProgressChanged_31() { return &___OnInverseProgressChanged_31; }
	inline void set_OnInverseProgressChanged_31(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnInverseProgressChanged_31 = value;
		Il2CppCodeGenWriteBarrier((&___OnInverseProgressChanged_31), value);
	}

	inline static int32_t get_offset_of_OpenBehavior_32() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___OpenBehavior_32)); }
	inline UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843 * get_OpenBehavior_32() const { return ___OpenBehavior_32; }
	inline UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843 ** get_address_of_OpenBehavior_32() { return &___OpenBehavior_32; }
	inline void set_OpenBehavior_32(UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843 * value)
	{
		___OpenBehavior_32 = value;
		Il2CppCodeGenWriteBarrier((&___OpenBehavior_32), value);
	}

	inline static int32_t get_offset_of_OpenSpeed_33() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___OpenSpeed_33)); }
	inline float get_OpenSpeed_33() const { return ___OpenSpeed_33; }
	inline float* get_address_of_OpenSpeed_33() { return &___OpenSpeed_33; }
	inline void set_OpenSpeed_33(float value)
	{
		___OpenSpeed_33 = value;
	}

	inline static int32_t get_offset_of_Overlay_34() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___Overlay_34)); }
	inline UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B * get_Overlay_34() const { return ___Overlay_34; }
	inline UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B ** get_address_of_Overlay_34() { return &___Overlay_34; }
	inline void set_Overlay_34(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B * value)
	{
		___Overlay_34 = value;
		Il2CppCodeGenWriteBarrier((&___Overlay_34), value);
	}

	inline static int32_t get_offset_of_Progressor_35() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___Progressor_35)); }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * get_Progressor_35() const { return ___Progressor_35; }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A ** get_address_of_Progressor_35() { return &___Progressor_35; }
	inline void set_Progressor_35(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * value)
	{
		___Progressor_35 = value;
		Il2CppCodeGenWriteBarrier((&___Progressor_35), value);
	}

	inline static int32_t get_offset_of_UseCustomStartAnchoredPosition_36() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___UseCustomStartAnchoredPosition_36)); }
	inline bool get_UseCustomStartAnchoredPosition_36() const { return ___UseCustomStartAnchoredPosition_36; }
	inline bool* get_address_of_UseCustomStartAnchoredPosition_36() { return &___UseCustomStartAnchoredPosition_36; }
	inline void set_UseCustomStartAnchoredPosition_36(bool value)
	{
		___UseCustomStartAnchoredPosition_36 = value;
	}

	inline static int32_t get_offset_of_m_canvas_37() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___m_canvas_37)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_canvas_37() const { return ___m_canvas_37; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_canvas_37() { return &___m_canvas_37; }
	inline void set_m_canvas_37(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_canvas_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_37), value);
	}

	inline static int32_t get_offset_of_m_visibility_38() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___m_visibility_38)); }
	inline int32_t get_m_visibility_38() const { return ___m_visibility_38; }
	inline int32_t* get_address_of_m_visibility_38() { return &___m_visibility_38; }
	inline void set_m_visibility_38(int32_t value)
	{
		___m_visibility_38 = value;
	}

	inline static int32_t get_offset_of_m_visibilityProgress_39() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___m_visibilityProgress_39)); }
	inline float get_m_visibilityProgress_39() const { return ___m_visibilityProgress_39; }
	inline float* get_address_of_m_visibilityProgress_39() { return &___m_visibilityProgress_39; }
	inline void set_m_visibilityProgress_39(float value)
	{
		___m_visibilityProgress_39 = value;
	}

	inline static int32_t get_offset_of_m_scaledCanvas_40() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___m_scaledCanvas_40)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_scaledCanvas_40() const { return ___m_scaledCanvas_40; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_scaledCanvas_40() { return &___m_scaledCanvas_40; }
	inline void set_m_scaledCanvas_40(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_scaledCanvas_40 = value;
	}

	inline static int32_t get_offset_of_m_availableForDrag_41() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___m_availableForDrag_41)); }
	inline bool get_m_availableForDrag_41() const { return ___m_availableForDrag_41; }
	inline bool* get_address_of_m_availableForDrag_41() { return &___m_availableForDrag_41; }
	inline void set_m_availableForDrag_41(bool value)
	{
		___m_availableForDrag_41 = value;
	}

	inline static int32_t get_offset_of_m_dragStartPosition_42() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A, ___m_dragStartPosition_42)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_dragStartPosition_42() const { return ___m_dragStartPosition_42; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_dragStartPosition_42() { return &___m_dragStartPosition_42; }
	inline void set_m_dragStartPosition_42(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_dragStartPosition_42 = value;
	}
};

struct UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A_StaticFields
{
public:
	// Doozy.Engine.UI.UIDrawer Doozy.Engine.UI.UIDrawer::<DraggedDrawer>k__BackingField
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A * ___U3CDraggedDrawerU3Ek__BackingField_16;
	// Doozy.Engine.UI.UIDrawer Doozy.Engine.UI.UIDrawer::<OpenedDrawer>k__BackingField
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A * ___U3COpenedDrawerU3Ek__BackingField_17;
	// System.Action`2<Doozy.Engine.UI.UIDrawer,Doozy.Engine.UI.UIDrawerBehaviorType> Doozy.Engine.UI.UIDrawer::OnUIDrawerBehavior
	Action_2_t01D7645AF504DBFECD9C187C54C4698EC9589290 * ___OnUIDrawerBehavior_18;

public:
	inline static int32_t get_offset_of_U3CDraggedDrawerU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A_StaticFields, ___U3CDraggedDrawerU3Ek__BackingField_16)); }
	inline UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A * get_U3CDraggedDrawerU3Ek__BackingField_16() const { return ___U3CDraggedDrawerU3Ek__BackingField_16; }
	inline UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A ** get_address_of_U3CDraggedDrawerU3Ek__BackingField_16() { return &___U3CDraggedDrawerU3Ek__BackingField_16; }
	inline void set_U3CDraggedDrawerU3Ek__BackingField_16(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A * value)
	{
		___U3CDraggedDrawerU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDraggedDrawerU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3COpenedDrawerU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A_StaticFields, ___U3COpenedDrawerU3Ek__BackingField_17)); }
	inline UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A * get_U3COpenedDrawerU3Ek__BackingField_17() const { return ___U3COpenedDrawerU3Ek__BackingField_17; }
	inline UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A ** get_address_of_U3COpenedDrawerU3Ek__BackingField_17() { return &___U3COpenedDrawerU3Ek__BackingField_17; }
	inline void set_U3COpenedDrawerU3Ek__BackingField_17(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A * value)
	{
		___U3COpenedDrawerU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3COpenedDrawerU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_OnUIDrawerBehavior_18() { return static_cast<int32_t>(offsetof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A_StaticFields, ___OnUIDrawerBehavior_18)); }
	inline Action_2_t01D7645AF504DBFECD9C187C54C4698EC9589290 * get_OnUIDrawerBehavior_18() const { return ___OnUIDrawerBehavior_18; }
	inline Action_2_t01D7645AF504DBFECD9C187C54C4698EC9589290 ** get_address_of_OnUIDrawerBehavior_18() { return &___OnUIDrawerBehavior_18; }
	inline void set_OnUIDrawerBehavior_18(Action_2_t01D7645AF504DBFECD9C187C54C4698EC9589290 * value)
	{
		___OnUIDrawerBehavior_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnUIDrawerBehavior_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDRAWER_TE812989F3C01F90F70B60FA2EF8D250C07A3427A_H
#ifndef UIPOPUP_TDDEB8981C29D9A18EE7BD971926654FF0BE76228_H
#define UIPOPUP_TDDEB8981C29D9A18EE7BD971926654FF0BE76228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIPopup
struct  UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228  : public UIComponentBase_1_tD0D8962B578CCDFC1CA0967558F1D9DDBC99DC5B
{
public:
	// System.String Doozy.Engine.UI.UIPopup::<PopupName>k__BackingField
	String_t* ___U3CPopupNameU3Ek__BackingField_17;
	// System.Boolean Doozy.Engine.UI.UIPopup::AddToPopupQueue
	bool ___AddToPopupQueue_18;
	// System.Boolean Doozy.Engine.UI.UIPopup::AutoHideAfterShow
	bool ___AutoHideAfterShow_19;
	// System.Single Doozy.Engine.UI.UIPopup::AutoHideAfterShowDelay
	float ___AutoHideAfterShowDelay_20;
	// System.Boolean Doozy.Engine.UI.UIPopup::AutoSelectButtonAfterShow
	bool ___AutoSelectButtonAfterShow_21;
	// System.String Doozy.Engine.UI.UIPopup::CanvasName
	String_t* ___CanvasName_22;
	// Doozy.Engine.UI.Base.UIContainer Doozy.Engine.UI.UIPopup::Container
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B * ___Container_23;
	// System.Boolean Doozy.Engine.UI.UIPopup::CustomCanvasName
	bool ___CustomCanvasName_24;
	// Doozy.Engine.UI.UIPopupContentReferences Doozy.Engine.UI.UIPopup::Data
	UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272 * ___Data_25;
	// System.Boolean Doozy.Engine.UI.UIPopup::DestroyAfterHide
	bool ___DestroyAfterHide_26;
	// Doozy.Engine.UI.PopupDisplayOn Doozy.Engine.UI.UIPopup::DisplayTarget
	int32_t ___DisplayTarget_27;
	// Doozy.Engine.UI.UIPopupBehavior Doozy.Engine.UI.UIPopup::HideBehavior
	UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57 * ___HideBehavior_28;
	// System.Boolean Doozy.Engine.UI.UIPopup::HideOnBackButton
	bool ___HideOnBackButton_29;
	// System.Boolean Doozy.Engine.UI.UIPopup::HideOnClickAnywhere
	bool ___HideOnClickAnywhere_30;
	// System.Boolean Doozy.Engine.UI.UIPopup::HideOnClickContainer
	bool ___HideOnClickContainer_31;
	// System.Boolean Doozy.Engine.UI.UIPopup::HideOnClickOverlay
	bool ___HideOnClickOverlay_32;
	// Doozy.Engine.Progress.Progressor Doozy.Engine.UI.UIPopup::HideProgressor
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * ___HideProgressor_33;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.UI.UIPopup::OnInverseVisibilityChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnInverseVisibilityChanged_34;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.UI.UIPopup::OnVisibilityChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnVisibilityChanged_35;
	// Doozy.Engine.UI.Base.UIContainer Doozy.Engine.UI.UIPopup::Overlay
	UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B * ___Overlay_36;
	// UnityEngine.GameObject Doozy.Engine.UI.UIPopup::SelectedButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___SelectedButton_37;
	// Doozy.Engine.UI.UIPopupBehavior Doozy.Engine.UI.UIPopup::ShowBehavior
	UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57 * ___ShowBehavior_38;
	// Doozy.Engine.Progress.Progressor Doozy.Engine.UI.UIPopup::ShowProgressor
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * ___ShowProgressor_39;
	// System.Boolean Doozy.Engine.UI.UIPopup::UpdateHideProgressorOnShow
	bool ___UpdateHideProgressorOnShow_40;
	// System.Boolean Doozy.Engine.UI.UIPopup::UpdateShowProgressorOnHide
	bool ___UpdateShowProgressorOnHide_41;
	// System.Boolean Doozy.Engine.UI.UIPopup::UseOverlay
	bool ___UseOverlay_42;
	// UnityEngine.Canvas Doozy.Engine.UI.UIPopup::m_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_canvas_43;
	// UnityEngine.UI.GraphicRaycaster Doozy.Engine.UI.UIPopup::m_graphicRaycaster
	GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * ___m_graphicRaycaster_44;
	// UnityEngine.GameObject Doozy.Engine.UI.UIPopup::m_previousSelectedButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_previousSelectedButton_45;
	// System.Single Doozy.Engine.UI.UIPopup::m_visibilityProgress
	float ___m_visibilityProgress_46;
	// Doozy.Engine.UI.VisibilityState Doozy.Engine.UI.UIPopup::m_visibilityState
	int32_t ___m_visibilityState_47;
	// System.Boolean Doozy.Engine.UI.UIPopup::m_addedToQueue
	bool ___m_addedToQueue_48;
	// UnityEngine.Coroutine Doozy.Engine.UI.UIPopup::m_showCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_showCoroutine_49;
	// UnityEngine.Coroutine Doozy.Engine.UI.UIPopup::m_hideCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_hideCoroutine_50;
	// UnityEngine.Coroutine Doozy.Engine.UI.UIPopup::m_autoHideCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_autoHideCoroutine_51;
	// UnityEngine.Coroutine Doozy.Engine.UI.UIPopup::m_disableButtonClickCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_disableButtonClickCoroutine_52;
	// Doozy.Engine.UI.UIButton[] Doozy.Engine.UI.UIPopup::m_childUIButtons
	UIButtonU5BU5D_t2DA91AEF4743C142AF0D299149E2019C6EF0467F* ___m_childUIButtons_53;
	// System.Boolean Doozy.Engine.UI.UIPopup::m_initialized
	bool ___m_initialized_54;

public:
	inline static int32_t get_offset_of_U3CPopupNameU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___U3CPopupNameU3Ek__BackingField_17)); }
	inline String_t* get_U3CPopupNameU3Ek__BackingField_17() const { return ___U3CPopupNameU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CPopupNameU3Ek__BackingField_17() { return &___U3CPopupNameU3Ek__BackingField_17; }
	inline void set_U3CPopupNameU3Ek__BackingField_17(String_t* value)
	{
		___U3CPopupNameU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPopupNameU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_AddToPopupQueue_18() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___AddToPopupQueue_18)); }
	inline bool get_AddToPopupQueue_18() const { return ___AddToPopupQueue_18; }
	inline bool* get_address_of_AddToPopupQueue_18() { return &___AddToPopupQueue_18; }
	inline void set_AddToPopupQueue_18(bool value)
	{
		___AddToPopupQueue_18 = value;
	}

	inline static int32_t get_offset_of_AutoHideAfterShow_19() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___AutoHideAfterShow_19)); }
	inline bool get_AutoHideAfterShow_19() const { return ___AutoHideAfterShow_19; }
	inline bool* get_address_of_AutoHideAfterShow_19() { return &___AutoHideAfterShow_19; }
	inline void set_AutoHideAfterShow_19(bool value)
	{
		___AutoHideAfterShow_19 = value;
	}

	inline static int32_t get_offset_of_AutoHideAfterShowDelay_20() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___AutoHideAfterShowDelay_20)); }
	inline float get_AutoHideAfterShowDelay_20() const { return ___AutoHideAfterShowDelay_20; }
	inline float* get_address_of_AutoHideAfterShowDelay_20() { return &___AutoHideAfterShowDelay_20; }
	inline void set_AutoHideAfterShowDelay_20(float value)
	{
		___AutoHideAfterShowDelay_20 = value;
	}

	inline static int32_t get_offset_of_AutoSelectButtonAfterShow_21() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___AutoSelectButtonAfterShow_21)); }
	inline bool get_AutoSelectButtonAfterShow_21() const { return ___AutoSelectButtonAfterShow_21; }
	inline bool* get_address_of_AutoSelectButtonAfterShow_21() { return &___AutoSelectButtonAfterShow_21; }
	inline void set_AutoSelectButtonAfterShow_21(bool value)
	{
		___AutoSelectButtonAfterShow_21 = value;
	}

	inline static int32_t get_offset_of_CanvasName_22() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___CanvasName_22)); }
	inline String_t* get_CanvasName_22() const { return ___CanvasName_22; }
	inline String_t** get_address_of_CanvasName_22() { return &___CanvasName_22; }
	inline void set_CanvasName_22(String_t* value)
	{
		___CanvasName_22 = value;
		Il2CppCodeGenWriteBarrier((&___CanvasName_22), value);
	}

	inline static int32_t get_offset_of_Container_23() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___Container_23)); }
	inline UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B * get_Container_23() const { return ___Container_23; }
	inline UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B ** get_address_of_Container_23() { return &___Container_23; }
	inline void set_Container_23(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B * value)
	{
		___Container_23 = value;
		Il2CppCodeGenWriteBarrier((&___Container_23), value);
	}

	inline static int32_t get_offset_of_CustomCanvasName_24() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___CustomCanvasName_24)); }
	inline bool get_CustomCanvasName_24() const { return ___CustomCanvasName_24; }
	inline bool* get_address_of_CustomCanvasName_24() { return &___CustomCanvasName_24; }
	inline void set_CustomCanvasName_24(bool value)
	{
		___CustomCanvasName_24 = value;
	}

	inline static int32_t get_offset_of_Data_25() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___Data_25)); }
	inline UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272 * get_Data_25() const { return ___Data_25; }
	inline UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272 ** get_address_of_Data_25() { return &___Data_25; }
	inline void set_Data_25(UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272 * value)
	{
		___Data_25 = value;
		Il2CppCodeGenWriteBarrier((&___Data_25), value);
	}

	inline static int32_t get_offset_of_DestroyAfterHide_26() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___DestroyAfterHide_26)); }
	inline bool get_DestroyAfterHide_26() const { return ___DestroyAfterHide_26; }
	inline bool* get_address_of_DestroyAfterHide_26() { return &___DestroyAfterHide_26; }
	inline void set_DestroyAfterHide_26(bool value)
	{
		___DestroyAfterHide_26 = value;
	}

	inline static int32_t get_offset_of_DisplayTarget_27() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___DisplayTarget_27)); }
	inline int32_t get_DisplayTarget_27() const { return ___DisplayTarget_27; }
	inline int32_t* get_address_of_DisplayTarget_27() { return &___DisplayTarget_27; }
	inline void set_DisplayTarget_27(int32_t value)
	{
		___DisplayTarget_27 = value;
	}

	inline static int32_t get_offset_of_HideBehavior_28() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___HideBehavior_28)); }
	inline UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57 * get_HideBehavior_28() const { return ___HideBehavior_28; }
	inline UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57 ** get_address_of_HideBehavior_28() { return &___HideBehavior_28; }
	inline void set_HideBehavior_28(UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57 * value)
	{
		___HideBehavior_28 = value;
		Il2CppCodeGenWriteBarrier((&___HideBehavior_28), value);
	}

	inline static int32_t get_offset_of_HideOnBackButton_29() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___HideOnBackButton_29)); }
	inline bool get_HideOnBackButton_29() const { return ___HideOnBackButton_29; }
	inline bool* get_address_of_HideOnBackButton_29() { return &___HideOnBackButton_29; }
	inline void set_HideOnBackButton_29(bool value)
	{
		___HideOnBackButton_29 = value;
	}

	inline static int32_t get_offset_of_HideOnClickAnywhere_30() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___HideOnClickAnywhere_30)); }
	inline bool get_HideOnClickAnywhere_30() const { return ___HideOnClickAnywhere_30; }
	inline bool* get_address_of_HideOnClickAnywhere_30() { return &___HideOnClickAnywhere_30; }
	inline void set_HideOnClickAnywhere_30(bool value)
	{
		___HideOnClickAnywhere_30 = value;
	}

	inline static int32_t get_offset_of_HideOnClickContainer_31() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___HideOnClickContainer_31)); }
	inline bool get_HideOnClickContainer_31() const { return ___HideOnClickContainer_31; }
	inline bool* get_address_of_HideOnClickContainer_31() { return &___HideOnClickContainer_31; }
	inline void set_HideOnClickContainer_31(bool value)
	{
		___HideOnClickContainer_31 = value;
	}

	inline static int32_t get_offset_of_HideOnClickOverlay_32() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___HideOnClickOverlay_32)); }
	inline bool get_HideOnClickOverlay_32() const { return ___HideOnClickOverlay_32; }
	inline bool* get_address_of_HideOnClickOverlay_32() { return &___HideOnClickOverlay_32; }
	inline void set_HideOnClickOverlay_32(bool value)
	{
		___HideOnClickOverlay_32 = value;
	}

	inline static int32_t get_offset_of_HideProgressor_33() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___HideProgressor_33)); }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * get_HideProgressor_33() const { return ___HideProgressor_33; }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A ** get_address_of_HideProgressor_33() { return &___HideProgressor_33; }
	inline void set_HideProgressor_33(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * value)
	{
		___HideProgressor_33 = value;
		Il2CppCodeGenWriteBarrier((&___HideProgressor_33), value);
	}

	inline static int32_t get_offset_of_OnInverseVisibilityChanged_34() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___OnInverseVisibilityChanged_34)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnInverseVisibilityChanged_34() const { return ___OnInverseVisibilityChanged_34; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnInverseVisibilityChanged_34() { return &___OnInverseVisibilityChanged_34; }
	inline void set_OnInverseVisibilityChanged_34(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnInverseVisibilityChanged_34 = value;
		Il2CppCodeGenWriteBarrier((&___OnInverseVisibilityChanged_34), value);
	}

	inline static int32_t get_offset_of_OnVisibilityChanged_35() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___OnVisibilityChanged_35)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnVisibilityChanged_35() const { return ___OnVisibilityChanged_35; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnVisibilityChanged_35() { return &___OnVisibilityChanged_35; }
	inline void set_OnVisibilityChanged_35(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnVisibilityChanged_35 = value;
		Il2CppCodeGenWriteBarrier((&___OnVisibilityChanged_35), value);
	}

	inline static int32_t get_offset_of_Overlay_36() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___Overlay_36)); }
	inline UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B * get_Overlay_36() const { return ___Overlay_36; }
	inline UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B ** get_address_of_Overlay_36() { return &___Overlay_36; }
	inline void set_Overlay_36(UIContainer_t19EF532BD0C41D5271FE710E5665E0A1845A600B * value)
	{
		___Overlay_36 = value;
		Il2CppCodeGenWriteBarrier((&___Overlay_36), value);
	}

	inline static int32_t get_offset_of_SelectedButton_37() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___SelectedButton_37)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_SelectedButton_37() const { return ___SelectedButton_37; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_SelectedButton_37() { return &___SelectedButton_37; }
	inline void set_SelectedButton_37(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___SelectedButton_37 = value;
		Il2CppCodeGenWriteBarrier((&___SelectedButton_37), value);
	}

	inline static int32_t get_offset_of_ShowBehavior_38() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___ShowBehavior_38)); }
	inline UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57 * get_ShowBehavior_38() const { return ___ShowBehavior_38; }
	inline UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57 ** get_address_of_ShowBehavior_38() { return &___ShowBehavior_38; }
	inline void set_ShowBehavior_38(UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57 * value)
	{
		___ShowBehavior_38 = value;
		Il2CppCodeGenWriteBarrier((&___ShowBehavior_38), value);
	}

	inline static int32_t get_offset_of_ShowProgressor_39() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___ShowProgressor_39)); }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * get_ShowProgressor_39() const { return ___ShowProgressor_39; }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A ** get_address_of_ShowProgressor_39() { return &___ShowProgressor_39; }
	inline void set_ShowProgressor_39(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * value)
	{
		___ShowProgressor_39 = value;
		Il2CppCodeGenWriteBarrier((&___ShowProgressor_39), value);
	}

	inline static int32_t get_offset_of_UpdateHideProgressorOnShow_40() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___UpdateHideProgressorOnShow_40)); }
	inline bool get_UpdateHideProgressorOnShow_40() const { return ___UpdateHideProgressorOnShow_40; }
	inline bool* get_address_of_UpdateHideProgressorOnShow_40() { return &___UpdateHideProgressorOnShow_40; }
	inline void set_UpdateHideProgressorOnShow_40(bool value)
	{
		___UpdateHideProgressorOnShow_40 = value;
	}

	inline static int32_t get_offset_of_UpdateShowProgressorOnHide_41() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___UpdateShowProgressorOnHide_41)); }
	inline bool get_UpdateShowProgressorOnHide_41() const { return ___UpdateShowProgressorOnHide_41; }
	inline bool* get_address_of_UpdateShowProgressorOnHide_41() { return &___UpdateShowProgressorOnHide_41; }
	inline void set_UpdateShowProgressorOnHide_41(bool value)
	{
		___UpdateShowProgressorOnHide_41 = value;
	}

	inline static int32_t get_offset_of_UseOverlay_42() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___UseOverlay_42)); }
	inline bool get_UseOverlay_42() const { return ___UseOverlay_42; }
	inline bool* get_address_of_UseOverlay_42() { return &___UseOverlay_42; }
	inline void set_UseOverlay_42(bool value)
	{
		___UseOverlay_42 = value;
	}

	inline static int32_t get_offset_of_m_canvas_43() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___m_canvas_43)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_canvas_43() const { return ___m_canvas_43; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_canvas_43() { return &___m_canvas_43; }
	inline void set_m_canvas_43(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_canvas_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_43), value);
	}

	inline static int32_t get_offset_of_m_graphicRaycaster_44() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___m_graphicRaycaster_44)); }
	inline GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * get_m_graphicRaycaster_44() const { return ___m_graphicRaycaster_44; }
	inline GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 ** get_address_of_m_graphicRaycaster_44() { return &___m_graphicRaycaster_44; }
	inline void set_m_graphicRaycaster_44(GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * value)
	{
		___m_graphicRaycaster_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphicRaycaster_44), value);
	}

	inline static int32_t get_offset_of_m_previousSelectedButton_45() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___m_previousSelectedButton_45)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_previousSelectedButton_45() const { return ___m_previousSelectedButton_45; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_previousSelectedButton_45() { return &___m_previousSelectedButton_45; }
	inline void set_m_previousSelectedButton_45(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_previousSelectedButton_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_previousSelectedButton_45), value);
	}

	inline static int32_t get_offset_of_m_visibilityProgress_46() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___m_visibilityProgress_46)); }
	inline float get_m_visibilityProgress_46() const { return ___m_visibilityProgress_46; }
	inline float* get_address_of_m_visibilityProgress_46() { return &___m_visibilityProgress_46; }
	inline void set_m_visibilityProgress_46(float value)
	{
		___m_visibilityProgress_46 = value;
	}

	inline static int32_t get_offset_of_m_visibilityState_47() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___m_visibilityState_47)); }
	inline int32_t get_m_visibilityState_47() const { return ___m_visibilityState_47; }
	inline int32_t* get_address_of_m_visibilityState_47() { return &___m_visibilityState_47; }
	inline void set_m_visibilityState_47(int32_t value)
	{
		___m_visibilityState_47 = value;
	}

	inline static int32_t get_offset_of_m_addedToQueue_48() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___m_addedToQueue_48)); }
	inline bool get_m_addedToQueue_48() const { return ___m_addedToQueue_48; }
	inline bool* get_address_of_m_addedToQueue_48() { return &___m_addedToQueue_48; }
	inline void set_m_addedToQueue_48(bool value)
	{
		___m_addedToQueue_48 = value;
	}

	inline static int32_t get_offset_of_m_showCoroutine_49() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___m_showCoroutine_49)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_showCoroutine_49() const { return ___m_showCoroutine_49; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_showCoroutine_49() { return &___m_showCoroutine_49; }
	inline void set_m_showCoroutine_49(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_showCoroutine_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_showCoroutine_49), value);
	}

	inline static int32_t get_offset_of_m_hideCoroutine_50() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___m_hideCoroutine_50)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_hideCoroutine_50() const { return ___m_hideCoroutine_50; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_hideCoroutine_50() { return &___m_hideCoroutine_50; }
	inline void set_m_hideCoroutine_50(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_hideCoroutine_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_hideCoroutine_50), value);
	}

	inline static int32_t get_offset_of_m_autoHideCoroutine_51() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___m_autoHideCoroutine_51)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_autoHideCoroutine_51() const { return ___m_autoHideCoroutine_51; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_autoHideCoroutine_51() { return &___m_autoHideCoroutine_51; }
	inline void set_m_autoHideCoroutine_51(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_autoHideCoroutine_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_autoHideCoroutine_51), value);
	}

	inline static int32_t get_offset_of_m_disableButtonClickCoroutine_52() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___m_disableButtonClickCoroutine_52)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_disableButtonClickCoroutine_52() const { return ___m_disableButtonClickCoroutine_52; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_disableButtonClickCoroutine_52() { return &___m_disableButtonClickCoroutine_52; }
	inline void set_m_disableButtonClickCoroutine_52(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_disableButtonClickCoroutine_52 = value;
		Il2CppCodeGenWriteBarrier((&___m_disableButtonClickCoroutine_52), value);
	}

	inline static int32_t get_offset_of_m_childUIButtons_53() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___m_childUIButtons_53)); }
	inline UIButtonU5BU5D_t2DA91AEF4743C142AF0D299149E2019C6EF0467F* get_m_childUIButtons_53() const { return ___m_childUIButtons_53; }
	inline UIButtonU5BU5D_t2DA91AEF4743C142AF0D299149E2019C6EF0467F** get_address_of_m_childUIButtons_53() { return &___m_childUIButtons_53; }
	inline void set_m_childUIButtons_53(UIButtonU5BU5D_t2DA91AEF4743C142AF0D299149E2019C6EF0467F* value)
	{
		___m_childUIButtons_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_childUIButtons_53), value);
	}

	inline static int32_t get_offset_of_m_initialized_54() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228, ___m_initialized_54)); }
	inline bool get_m_initialized_54() const { return ___m_initialized_54; }
	inline bool* get_address_of_m_initialized_54() { return &___m_initialized_54; }
	inline void set_m_initialized_54(bool value)
	{
		___m_initialized_54 = value;
	}
};

struct UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228_StaticFields
{
public:
	// System.Action`2<Doozy.Engine.UI.UIPopup,Doozy.Engine.UI.Animation.AnimationType> Doozy.Engine.UI.UIPopup::OnUIPopupAction
	Action_2_t8341B9F4DBB00FCCF2A0E62796BDFF83C3CF77E3 * ___OnUIPopupAction_15;
	// System.Collections.Generic.List`1<Doozy.Engine.UI.UIPopup> Doozy.Engine.UI.UIPopup::VisiblePopups
	List_1_t0F8A6581E8659C9A921426873F1825C44A02D639 * ___VisiblePopups_16;

public:
	inline static int32_t get_offset_of_OnUIPopupAction_15() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228_StaticFields, ___OnUIPopupAction_15)); }
	inline Action_2_t8341B9F4DBB00FCCF2A0E62796BDFF83C3CF77E3 * get_OnUIPopupAction_15() const { return ___OnUIPopupAction_15; }
	inline Action_2_t8341B9F4DBB00FCCF2A0E62796BDFF83C3CF77E3 ** get_address_of_OnUIPopupAction_15() { return &___OnUIPopupAction_15; }
	inline void set_OnUIPopupAction_15(Action_2_t8341B9F4DBB00FCCF2A0E62796BDFF83C3CF77E3 * value)
	{
		___OnUIPopupAction_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnUIPopupAction_15), value);
	}

	inline static int32_t get_offset_of_VisiblePopups_16() { return static_cast<int32_t>(offsetof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228_StaticFields, ___VisiblePopups_16)); }
	inline List_1_t0F8A6581E8659C9A921426873F1825C44A02D639 * get_VisiblePopups_16() const { return ___VisiblePopups_16; }
	inline List_1_t0F8A6581E8659C9A921426873F1825C44A02D639 ** get_address_of_VisiblePopups_16() { return &___VisiblePopups_16; }
	inline void set_VisiblePopups_16(List_1_t0F8A6581E8659C9A921426873F1825C44A02D639 * value)
	{
		___VisiblePopups_16 = value;
		Il2CppCodeGenWriteBarrier((&___VisiblePopups_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPOPUP_TDDEB8981C29D9A18EE7BD971926654FF0BE76228_H
#ifndef UITOGGLE_T273F3954806AC38A83534BC27D54B2839CF31273_H
#define UITOGGLE_T273F3954806AC38A83534BC27D54B2839CF31273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIToggle
struct  UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273  : public UIComponentBase_1_t208748696434ABD229DF180B295D28F60187FF7C
{
public:
	// System.Boolean Doozy.Engine.UI.UIToggle::AllowMultipleClicks
	bool ___AllowMultipleClicks_14;
	// System.Single Doozy.Engine.UI.UIToggle::DisableButtonBetweenClicksInterval
	float ___DisableButtonBetweenClicksInterval_15;
	// System.Boolean Doozy.Engine.UI.UIToggle::DeselectButtonAfterClick
	bool ___DeselectButtonAfterClick_16;
	// Doozy.Engine.UI.Input.InputData Doozy.Engine.UI.UIToggle::InputData
	InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC * ___InputData_17;
	// Doozy.Engine.UI.UIToggleBehavior Doozy.Engine.UI.UIToggle::OnPointerEnter
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * ___OnPointerEnter_18;
	// Doozy.Engine.UI.UIToggleBehavior Doozy.Engine.UI.UIToggle::OnPointerExit
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * ___OnPointerExit_19;
	// Doozy.Engine.UI.UIToggleBehavior Doozy.Engine.UI.UIToggle::OnClick
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * ___OnClick_20;
	// Doozy.Engine.UI.UIToggleBehavior Doozy.Engine.UI.UIToggle::OnSelected
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * ___OnSelected_21;
	// Doozy.Engine.UI.UIToggleBehavior Doozy.Engine.UI.UIToggle::OnDeselected
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * ___OnDeselected_22;
	// Doozy.Engine.Events.BoolEvent Doozy.Engine.UI.UIToggle::OnValueChanged
	BoolEvent_tC2164AC1844ACBA9870E7D339F6FD57462A40D3B * ___OnValueChanged_23;
	// Doozy.Engine.UI.TargetLabel Doozy.Engine.UI.UIToggle::TargetLabel
	int32_t ___TargetLabel_24;
	// UnityEngine.UI.Text Doozy.Engine.UI.UIToggle::TextLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___TextLabel_25;
	// TMPro.TextMeshProUGUI Doozy.Engine.UI.UIToggle::TextMeshProLabel
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___TextMeshProLabel_26;
	// Doozy.Engine.Progress.Progressor Doozy.Engine.UI.UIToggle::ToggleProgressor
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * ___ToggleProgressor_27;
	// UnityEngine.CanvasGroup Doozy.Engine.UI.UIToggle::m_canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___m_canvasGroup_28;
	// UnityEngine.Coroutine Doozy.Engine.UI.UIToggle::m_disableButtonCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_disableButtonCoroutine_29;
	// System.Boolean Doozy.Engine.UI.UIToggle::m_previousValue
	bool ___m_previousValue_30;
	// UnityEngine.UI.Toggle Doozy.Engine.UI.UIToggle::m_toggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___m_toggle_31;
	// System.Boolean Doozy.Engine.UI.UIToggle::m_updateStartValuesRequired
	bool ___m_updateStartValuesRequired_32;

public:
	inline static int32_t get_offset_of_AllowMultipleClicks_14() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___AllowMultipleClicks_14)); }
	inline bool get_AllowMultipleClicks_14() const { return ___AllowMultipleClicks_14; }
	inline bool* get_address_of_AllowMultipleClicks_14() { return &___AllowMultipleClicks_14; }
	inline void set_AllowMultipleClicks_14(bool value)
	{
		___AllowMultipleClicks_14 = value;
	}

	inline static int32_t get_offset_of_DisableButtonBetweenClicksInterval_15() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___DisableButtonBetweenClicksInterval_15)); }
	inline float get_DisableButtonBetweenClicksInterval_15() const { return ___DisableButtonBetweenClicksInterval_15; }
	inline float* get_address_of_DisableButtonBetweenClicksInterval_15() { return &___DisableButtonBetweenClicksInterval_15; }
	inline void set_DisableButtonBetweenClicksInterval_15(float value)
	{
		___DisableButtonBetweenClicksInterval_15 = value;
	}

	inline static int32_t get_offset_of_DeselectButtonAfterClick_16() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___DeselectButtonAfterClick_16)); }
	inline bool get_DeselectButtonAfterClick_16() const { return ___DeselectButtonAfterClick_16; }
	inline bool* get_address_of_DeselectButtonAfterClick_16() { return &___DeselectButtonAfterClick_16; }
	inline void set_DeselectButtonAfterClick_16(bool value)
	{
		___DeselectButtonAfterClick_16 = value;
	}

	inline static int32_t get_offset_of_InputData_17() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___InputData_17)); }
	inline InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC * get_InputData_17() const { return ___InputData_17; }
	inline InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC ** get_address_of_InputData_17() { return &___InputData_17; }
	inline void set_InputData_17(InputData_tCD7F71966DD3D5EDFA52AFA6652AB02C0084F4CC * value)
	{
		___InputData_17 = value;
		Il2CppCodeGenWriteBarrier((&___InputData_17), value);
	}

	inline static int32_t get_offset_of_OnPointerEnter_18() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___OnPointerEnter_18)); }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * get_OnPointerEnter_18() const { return ___OnPointerEnter_18; }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB ** get_address_of_OnPointerEnter_18() { return &___OnPointerEnter_18; }
	inline void set_OnPointerEnter_18(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * value)
	{
		___OnPointerEnter_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerEnter_18), value);
	}

	inline static int32_t get_offset_of_OnPointerExit_19() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___OnPointerExit_19)); }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * get_OnPointerExit_19() const { return ___OnPointerExit_19; }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB ** get_address_of_OnPointerExit_19() { return &___OnPointerExit_19; }
	inline void set_OnPointerExit_19(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * value)
	{
		___OnPointerExit_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerExit_19), value);
	}

	inline static int32_t get_offset_of_OnClick_20() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___OnClick_20)); }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * get_OnClick_20() const { return ___OnClick_20; }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB ** get_address_of_OnClick_20() { return &___OnClick_20; }
	inline void set_OnClick_20(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * value)
	{
		___OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((&___OnClick_20), value);
	}

	inline static int32_t get_offset_of_OnSelected_21() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___OnSelected_21)); }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * get_OnSelected_21() const { return ___OnSelected_21; }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB ** get_address_of_OnSelected_21() { return &___OnSelected_21; }
	inline void set_OnSelected_21(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * value)
	{
		___OnSelected_21 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelected_21), value);
	}

	inline static int32_t get_offset_of_OnDeselected_22() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___OnDeselected_22)); }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * get_OnDeselected_22() const { return ___OnDeselected_22; }
	inline UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB ** get_address_of_OnDeselected_22() { return &___OnDeselected_22; }
	inline void set_OnDeselected_22(UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB * value)
	{
		___OnDeselected_22 = value;
		Il2CppCodeGenWriteBarrier((&___OnDeselected_22), value);
	}

	inline static int32_t get_offset_of_OnValueChanged_23() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___OnValueChanged_23)); }
	inline BoolEvent_tC2164AC1844ACBA9870E7D339F6FD57462A40D3B * get_OnValueChanged_23() const { return ___OnValueChanged_23; }
	inline BoolEvent_tC2164AC1844ACBA9870E7D339F6FD57462A40D3B ** get_address_of_OnValueChanged_23() { return &___OnValueChanged_23; }
	inline void set_OnValueChanged_23(BoolEvent_tC2164AC1844ACBA9870E7D339F6FD57462A40D3B * value)
	{
		___OnValueChanged_23 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_23), value);
	}

	inline static int32_t get_offset_of_TargetLabel_24() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___TargetLabel_24)); }
	inline int32_t get_TargetLabel_24() const { return ___TargetLabel_24; }
	inline int32_t* get_address_of_TargetLabel_24() { return &___TargetLabel_24; }
	inline void set_TargetLabel_24(int32_t value)
	{
		___TargetLabel_24 = value;
	}

	inline static int32_t get_offset_of_TextLabel_25() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___TextLabel_25)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_TextLabel_25() const { return ___TextLabel_25; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_TextLabel_25() { return &___TextLabel_25; }
	inline void set_TextLabel_25(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___TextLabel_25 = value;
		Il2CppCodeGenWriteBarrier((&___TextLabel_25), value);
	}

	inline static int32_t get_offset_of_TextMeshProLabel_26() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___TextMeshProLabel_26)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_TextMeshProLabel_26() const { return ___TextMeshProLabel_26; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_TextMeshProLabel_26() { return &___TextMeshProLabel_26; }
	inline void set_TextMeshProLabel_26(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___TextMeshProLabel_26 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshProLabel_26), value);
	}

	inline static int32_t get_offset_of_ToggleProgressor_27() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___ToggleProgressor_27)); }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * get_ToggleProgressor_27() const { return ___ToggleProgressor_27; }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A ** get_address_of_ToggleProgressor_27() { return &___ToggleProgressor_27; }
	inline void set_ToggleProgressor_27(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * value)
	{
		___ToggleProgressor_27 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleProgressor_27), value);
	}

	inline static int32_t get_offset_of_m_canvasGroup_28() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___m_canvasGroup_28)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_m_canvasGroup_28() const { return ___m_canvasGroup_28; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_m_canvasGroup_28() { return &___m_canvasGroup_28; }
	inline void set_m_canvasGroup_28(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___m_canvasGroup_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasGroup_28), value);
	}

	inline static int32_t get_offset_of_m_disableButtonCoroutine_29() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___m_disableButtonCoroutine_29)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_disableButtonCoroutine_29() const { return ___m_disableButtonCoroutine_29; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_disableButtonCoroutine_29() { return &___m_disableButtonCoroutine_29; }
	inline void set_m_disableButtonCoroutine_29(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_disableButtonCoroutine_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_disableButtonCoroutine_29), value);
	}

	inline static int32_t get_offset_of_m_previousValue_30() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___m_previousValue_30)); }
	inline bool get_m_previousValue_30() const { return ___m_previousValue_30; }
	inline bool* get_address_of_m_previousValue_30() { return &___m_previousValue_30; }
	inline void set_m_previousValue_30(bool value)
	{
		___m_previousValue_30 = value;
	}

	inline static int32_t get_offset_of_m_toggle_31() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___m_toggle_31)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_m_toggle_31() const { return ___m_toggle_31; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_m_toggle_31() { return &___m_toggle_31; }
	inline void set_m_toggle_31(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___m_toggle_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggle_31), value);
	}

	inline static int32_t get_offset_of_m_updateStartValuesRequired_32() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273, ___m_updateStartValuesRequired_32)); }
	inline bool get_m_updateStartValuesRequired_32() const { return ___m_updateStartValuesRequired_32; }
	inline bool* get_address_of_m_updateStartValuesRequired_32() { return &___m_updateStartValuesRequired_32; }
	inline void set_m_updateStartValuesRequired_32(bool value)
	{
		___m_updateStartValuesRequired_32 = value;
	}
};

struct UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273_StaticFields
{
public:
	// System.Action`3<Doozy.Engine.UI.UIToggle,Doozy.Engine.UI.UIToggleState,Doozy.Engine.UI.UIToggleBehaviorType> Doozy.Engine.UI.UIToggle::OnUIToggleAction
	Action_3_t6A0377B8C081E230E32494EB2EDB35E6704E315B * ___OnUIToggleAction_13;

public:
	inline static int32_t get_offset_of_OnUIToggleAction_13() { return static_cast<int32_t>(offsetof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273_StaticFields, ___OnUIToggleAction_13)); }
	inline Action_3_t6A0377B8C081E230E32494EB2EDB35E6704E315B * get_OnUIToggleAction_13() const { return ___OnUIToggleAction_13; }
	inline Action_3_t6A0377B8C081E230E32494EB2EDB35E6704E315B ** get_address_of_OnUIToggleAction_13() { return &___OnUIToggleAction_13; }
	inline void set_OnUIToggleAction_13(Action_3_t6A0377B8C081E230E32494EB2EDB35E6704E315B * value)
	{
		___OnUIToggleAction_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnUIToggleAction_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLE_T273F3954806AC38A83534BC27D54B2839CF31273_H
#ifndef UIVIEW_T65D38836246049951821D4E45B1C81947591EBDF_H
#define UIVIEW_T65D38836246049951821D4E45B1C81947591EBDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doozy.Engine.UI.UIView
struct  UIView_t65D38836246049951821D4E45B1C81947591EBDF  : public UIComponentBase_1_t64C56FBF7E4AFAA3D397341303F4A8DDD33F4843
{
public:
	// System.Boolean Doozy.Engine.UI.UIView::AutoHideAfterShow
	bool ___AutoHideAfterShow_15;
	// System.Single Doozy.Engine.UI.UIView::AutoHideAfterShowDelay
	float ___AutoHideAfterShowDelay_16;
	// System.Boolean Doozy.Engine.UI.UIView::AutoSelectButtonAfterShow
	bool ___AutoSelectButtonAfterShow_17;
	// Doozy.Engine.UI.UIViewStartBehavior Doozy.Engine.UI.UIView::BehaviorAtStart
	int32_t ___BehaviorAtStart_18;
	// UnityEngine.Vector3 Doozy.Engine.UI.UIView::CustomStartAnchoredPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___CustomStartAnchoredPosition_19;
	// System.Boolean Doozy.Engine.UI.UIView::DeselectAnyButtonSelectedOnHide
	bool ___DeselectAnyButtonSelectedOnHide_20;
	// System.Boolean Doozy.Engine.UI.UIView::DeselectAnyButtonSelectedOnShow
	bool ___DeselectAnyButtonSelectedOnShow_21;
	// System.Boolean Doozy.Engine.UI.UIView::DisableCanvasWhenHidden
	bool ___DisableCanvasWhenHidden_22;
	// System.Boolean Doozy.Engine.UI.UIView::DisableGameObjectWhenHidden
	bool ___DisableGameObjectWhenHidden_23;
	// System.Boolean Doozy.Engine.UI.UIView::DisableGraphicRaycasterWhenHidden
	bool ___DisableGraphicRaycasterWhenHidden_24;
	// Doozy.Engine.UI.UIViewBehavior Doozy.Engine.UI.UIView::HideBehavior
	UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522 * ___HideBehavior_25;
	// Doozy.Engine.Progress.Progressor Doozy.Engine.UI.UIView::HideProgressor
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * ___HideProgressor_26;
	// Doozy.Engine.UI.UIViewBehavior Doozy.Engine.UI.UIView::LoopBehavior
	UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522 * ___LoopBehavior_27;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.UI.UIView::OnInverseVisibilityChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnInverseVisibilityChanged_28;
	// Doozy.Engine.Progress.ProgressEvent Doozy.Engine.UI.UIView::OnVisibilityChanged
	ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * ___OnVisibilityChanged_29;
	// UnityEngine.GameObject Doozy.Engine.UI.UIView::SelectedButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___SelectedButton_30;
	// Doozy.Engine.UI.UIViewBehavior Doozy.Engine.UI.UIView::ShowBehavior
	UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522 * ___ShowBehavior_31;
	// Doozy.Engine.Progress.Progressor Doozy.Engine.UI.UIView::ShowProgressor
	Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * ___ShowProgressor_32;
	// Doozy.Engine.UI.TargetOrientation Doozy.Engine.UI.UIView::TargetOrientation
	int32_t ___TargetOrientation_33;
	// System.Boolean Doozy.Engine.UI.UIView::UpdateHideProgressorOnShow
	bool ___UpdateHideProgressorOnShow_34;
	// System.Boolean Doozy.Engine.UI.UIView::UpdateShowProgressorOnHide
	bool ___UpdateShowProgressorOnHide_35;
	// System.Boolean Doozy.Engine.UI.UIView::UseCustomStartAnchoredPosition
	bool ___UseCustomStartAnchoredPosition_36;
	// System.String Doozy.Engine.UI.UIView::ViewCategory
	String_t* ___ViewCategory_37;
	// System.String Doozy.Engine.UI.UIView::ViewName
	String_t* ___ViewName_38;
	// UnityEngine.Canvas Doozy.Engine.UI.UIView::m_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_canvas_39;
	// UnityEngine.UI.GraphicRaycaster Doozy.Engine.UI.UIView::m_graphicRaycaster
	GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * ___m_graphicRaycaster_40;
	// UnityEngine.CanvasGroup Doozy.Engine.UI.UIView::m_canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___m_canvasGroup_41;
	// System.Single Doozy.Engine.UI.UIView::m_visibilityProgress
	float ___m_visibilityProgress_42;
	// Doozy.Engine.UI.VisibilityState Doozy.Engine.UI.UIView::m_visibility
	int32_t ___m_visibility_43;
	// UnityEngine.Coroutine Doozy.Engine.UI.UIView::m_showCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_showCoroutine_44;
	// UnityEngine.Coroutine Doozy.Engine.UI.UIView::m_hideCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_hideCoroutine_45;
	// UnityEngine.Coroutine Doozy.Engine.UI.UIView::m_autoHideCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_autoHideCoroutine_46;
	// UnityEngine.Coroutine Doozy.Engine.UI.UIView::m_disableButtonClickCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_disableButtonClickCoroutine_47;
	// Doozy.Engine.UI.UIButton[] Doozy.Engine.UI.UIView::m_childUIButtons
	UIButtonU5BU5D_t2DA91AEF4743C142AF0D299149E2019C6EF0467F* ___m_childUIButtons_48;
	// Doozy.Engine.UI.UIView[] Doozy.Engine.UI.UIView::m_childUIViews
	UIViewU5BU5D_t1BD9626268B6429358BFEB30BFE62444CAC554BD* ___m_childUIViews_49;
	// System.Boolean Doozy.Engine.UI.UIView::m_initialized
	bool ___m_initialized_50;

public:
	inline static int32_t get_offset_of_AutoHideAfterShow_15() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___AutoHideAfterShow_15)); }
	inline bool get_AutoHideAfterShow_15() const { return ___AutoHideAfterShow_15; }
	inline bool* get_address_of_AutoHideAfterShow_15() { return &___AutoHideAfterShow_15; }
	inline void set_AutoHideAfterShow_15(bool value)
	{
		___AutoHideAfterShow_15 = value;
	}

	inline static int32_t get_offset_of_AutoHideAfterShowDelay_16() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___AutoHideAfterShowDelay_16)); }
	inline float get_AutoHideAfterShowDelay_16() const { return ___AutoHideAfterShowDelay_16; }
	inline float* get_address_of_AutoHideAfterShowDelay_16() { return &___AutoHideAfterShowDelay_16; }
	inline void set_AutoHideAfterShowDelay_16(float value)
	{
		___AutoHideAfterShowDelay_16 = value;
	}

	inline static int32_t get_offset_of_AutoSelectButtonAfterShow_17() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___AutoSelectButtonAfterShow_17)); }
	inline bool get_AutoSelectButtonAfterShow_17() const { return ___AutoSelectButtonAfterShow_17; }
	inline bool* get_address_of_AutoSelectButtonAfterShow_17() { return &___AutoSelectButtonAfterShow_17; }
	inline void set_AutoSelectButtonAfterShow_17(bool value)
	{
		___AutoSelectButtonAfterShow_17 = value;
	}

	inline static int32_t get_offset_of_BehaviorAtStart_18() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___BehaviorAtStart_18)); }
	inline int32_t get_BehaviorAtStart_18() const { return ___BehaviorAtStart_18; }
	inline int32_t* get_address_of_BehaviorAtStart_18() { return &___BehaviorAtStart_18; }
	inline void set_BehaviorAtStart_18(int32_t value)
	{
		___BehaviorAtStart_18 = value;
	}

	inline static int32_t get_offset_of_CustomStartAnchoredPosition_19() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___CustomStartAnchoredPosition_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_CustomStartAnchoredPosition_19() const { return ___CustomStartAnchoredPosition_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_CustomStartAnchoredPosition_19() { return &___CustomStartAnchoredPosition_19; }
	inline void set_CustomStartAnchoredPosition_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___CustomStartAnchoredPosition_19 = value;
	}

	inline static int32_t get_offset_of_DeselectAnyButtonSelectedOnHide_20() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___DeselectAnyButtonSelectedOnHide_20)); }
	inline bool get_DeselectAnyButtonSelectedOnHide_20() const { return ___DeselectAnyButtonSelectedOnHide_20; }
	inline bool* get_address_of_DeselectAnyButtonSelectedOnHide_20() { return &___DeselectAnyButtonSelectedOnHide_20; }
	inline void set_DeselectAnyButtonSelectedOnHide_20(bool value)
	{
		___DeselectAnyButtonSelectedOnHide_20 = value;
	}

	inline static int32_t get_offset_of_DeselectAnyButtonSelectedOnShow_21() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___DeselectAnyButtonSelectedOnShow_21)); }
	inline bool get_DeselectAnyButtonSelectedOnShow_21() const { return ___DeselectAnyButtonSelectedOnShow_21; }
	inline bool* get_address_of_DeselectAnyButtonSelectedOnShow_21() { return &___DeselectAnyButtonSelectedOnShow_21; }
	inline void set_DeselectAnyButtonSelectedOnShow_21(bool value)
	{
		___DeselectAnyButtonSelectedOnShow_21 = value;
	}

	inline static int32_t get_offset_of_DisableCanvasWhenHidden_22() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___DisableCanvasWhenHidden_22)); }
	inline bool get_DisableCanvasWhenHidden_22() const { return ___DisableCanvasWhenHidden_22; }
	inline bool* get_address_of_DisableCanvasWhenHidden_22() { return &___DisableCanvasWhenHidden_22; }
	inline void set_DisableCanvasWhenHidden_22(bool value)
	{
		___DisableCanvasWhenHidden_22 = value;
	}

	inline static int32_t get_offset_of_DisableGameObjectWhenHidden_23() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___DisableGameObjectWhenHidden_23)); }
	inline bool get_DisableGameObjectWhenHidden_23() const { return ___DisableGameObjectWhenHidden_23; }
	inline bool* get_address_of_DisableGameObjectWhenHidden_23() { return &___DisableGameObjectWhenHidden_23; }
	inline void set_DisableGameObjectWhenHidden_23(bool value)
	{
		___DisableGameObjectWhenHidden_23 = value;
	}

	inline static int32_t get_offset_of_DisableGraphicRaycasterWhenHidden_24() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___DisableGraphicRaycasterWhenHidden_24)); }
	inline bool get_DisableGraphicRaycasterWhenHidden_24() const { return ___DisableGraphicRaycasterWhenHidden_24; }
	inline bool* get_address_of_DisableGraphicRaycasterWhenHidden_24() { return &___DisableGraphicRaycasterWhenHidden_24; }
	inline void set_DisableGraphicRaycasterWhenHidden_24(bool value)
	{
		___DisableGraphicRaycasterWhenHidden_24 = value;
	}

	inline static int32_t get_offset_of_HideBehavior_25() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___HideBehavior_25)); }
	inline UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522 * get_HideBehavior_25() const { return ___HideBehavior_25; }
	inline UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522 ** get_address_of_HideBehavior_25() { return &___HideBehavior_25; }
	inline void set_HideBehavior_25(UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522 * value)
	{
		___HideBehavior_25 = value;
		Il2CppCodeGenWriteBarrier((&___HideBehavior_25), value);
	}

	inline static int32_t get_offset_of_HideProgressor_26() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___HideProgressor_26)); }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * get_HideProgressor_26() const { return ___HideProgressor_26; }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A ** get_address_of_HideProgressor_26() { return &___HideProgressor_26; }
	inline void set_HideProgressor_26(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * value)
	{
		___HideProgressor_26 = value;
		Il2CppCodeGenWriteBarrier((&___HideProgressor_26), value);
	}

	inline static int32_t get_offset_of_LoopBehavior_27() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___LoopBehavior_27)); }
	inline UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522 * get_LoopBehavior_27() const { return ___LoopBehavior_27; }
	inline UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522 ** get_address_of_LoopBehavior_27() { return &___LoopBehavior_27; }
	inline void set_LoopBehavior_27(UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522 * value)
	{
		___LoopBehavior_27 = value;
		Il2CppCodeGenWriteBarrier((&___LoopBehavior_27), value);
	}

	inline static int32_t get_offset_of_OnInverseVisibilityChanged_28() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___OnInverseVisibilityChanged_28)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnInverseVisibilityChanged_28() const { return ___OnInverseVisibilityChanged_28; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnInverseVisibilityChanged_28() { return &___OnInverseVisibilityChanged_28; }
	inline void set_OnInverseVisibilityChanged_28(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnInverseVisibilityChanged_28 = value;
		Il2CppCodeGenWriteBarrier((&___OnInverseVisibilityChanged_28), value);
	}

	inline static int32_t get_offset_of_OnVisibilityChanged_29() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___OnVisibilityChanged_29)); }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * get_OnVisibilityChanged_29() const { return ___OnVisibilityChanged_29; }
	inline ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB ** get_address_of_OnVisibilityChanged_29() { return &___OnVisibilityChanged_29; }
	inline void set_OnVisibilityChanged_29(ProgressEvent_t029EF1ED75264735DFB03CFABD83B7D07DD8C3BB * value)
	{
		___OnVisibilityChanged_29 = value;
		Il2CppCodeGenWriteBarrier((&___OnVisibilityChanged_29), value);
	}

	inline static int32_t get_offset_of_SelectedButton_30() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___SelectedButton_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_SelectedButton_30() const { return ___SelectedButton_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_SelectedButton_30() { return &___SelectedButton_30; }
	inline void set_SelectedButton_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___SelectedButton_30 = value;
		Il2CppCodeGenWriteBarrier((&___SelectedButton_30), value);
	}

	inline static int32_t get_offset_of_ShowBehavior_31() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___ShowBehavior_31)); }
	inline UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522 * get_ShowBehavior_31() const { return ___ShowBehavior_31; }
	inline UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522 ** get_address_of_ShowBehavior_31() { return &___ShowBehavior_31; }
	inline void set_ShowBehavior_31(UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522 * value)
	{
		___ShowBehavior_31 = value;
		Il2CppCodeGenWriteBarrier((&___ShowBehavior_31), value);
	}

	inline static int32_t get_offset_of_ShowProgressor_32() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___ShowProgressor_32)); }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * get_ShowProgressor_32() const { return ___ShowProgressor_32; }
	inline Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A ** get_address_of_ShowProgressor_32() { return &___ShowProgressor_32; }
	inline void set_ShowProgressor_32(Progressor_t235D7E32E01D56976A39145AFDDBF6483EA8498A * value)
	{
		___ShowProgressor_32 = value;
		Il2CppCodeGenWriteBarrier((&___ShowProgressor_32), value);
	}

	inline static int32_t get_offset_of_TargetOrientation_33() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___TargetOrientation_33)); }
	inline int32_t get_TargetOrientation_33() const { return ___TargetOrientation_33; }
	inline int32_t* get_address_of_TargetOrientation_33() { return &___TargetOrientation_33; }
	inline void set_TargetOrientation_33(int32_t value)
	{
		___TargetOrientation_33 = value;
	}

	inline static int32_t get_offset_of_UpdateHideProgressorOnShow_34() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___UpdateHideProgressorOnShow_34)); }
	inline bool get_UpdateHideProgressorOnShow_34() const { return ___UpdateHideProgressorOnShow_34; }
	inline bool* get_address_of_UpdateHideProgressorOnShow_34() { return &___UpdateHideProgressorOnShow_34; }
	inline void set_UpdateHideProgressorOnShow_34(bool value)
	{
		___UpdateHideProgressorOnShow_34 = value;
	}

	inline static int32_t get_offset_of_UpdateShowProgressorOnHide_35() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___UpdateShowProgressorOnHide_35)); }
	inline bool get_UpdateShowProgressorOnHide_35() const { return ___UpdateShowProgressorOnHide_35; }
	inline bool* get_address_of_UpdateShowProgressorOnHide_35() { return &___UpdateShowProgressorOnHide_35; }
	inline void set_UpdateShowProgressorOnHide_35(bool value)
	{
		___UpdateShowProgressorOnHide_35 = value;
	}

	inline static int32_t get_offset_of_UseCustomStartAnchoredPosition_36() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___UseCustomStartAnchoredPosition_36)); }
	inline bool get_UseCustomStartAnchoredPosition_36() const { return ___UseCustomStartAnchoredPosition_36; }
	inline bool* get_address_of_UseCustomStartAnchoredPosition_36() { return &___UseCustomStartAnchoredPosition_36; }
	inline void set_UseCustomStartAnchoredPosition_36(bool value)
	{
		___UseCustomStartAnchoredPosition_36 = value;
	}

	inline static int32_t get_offset_of_ViewCategory_37() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___ViewCategory_37)); }
	inline String_t* get_ViewCategory_37() const { return ___ViewCategory_37; }
	inline String_t** get_address_of_ViewCategory_37() { return &___ViewCategory_37; }
	inline void set_ViewCategory_37(String_t* value)
	{
		___ViewCategory_37 = value;
		Il2CppCodeGenWriteBarrier((&___ViewCategory_37), value);
	}

	inline static int32_t get_offset_of_ViewName_38() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___ViewName_38)); }
	inline String_t* get_ViewName_38() const { return ___ViewName_38; }
	inline String_t** get_address_of_ViewName_38() { return &___ViewName_38; }
	inline void set_ViewName_38(String_t* value)
	{
		___ViewName_38 = value;
		Il2CppCodeGenWriteBarrier((&___ViewName_38), value);
	}

	inline static int32_t get_offset_of_m_canvas_39() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___m_canvas_39)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_canvas_39() const { return ___m_canvas_39; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_canvas_39() { return &___m_canvas_39; }
	inline void set_m_canvas_39(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_canvas_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_39), value);
	}

	inline static int32_t get_offset_of_m_graphicRaycaster_40() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___m_graphicRaycaster_40)); }
	inline GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * get_m_graphicRaycaster_40() const { return ___m_graphicRaycaster_40; }
	inline GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 ** get_address_of_m_graphicRaycaster_40() { return &___m_graphicRaycaster_40; }
	inline void set_m_graphicRaycaster_40(GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * value)
	{
		___m_graphicRaycaster_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_graphicRaycaster_40), value);
	}

	inline static int32_t get_offset_of_m_canvasGroup_41() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___m_canvasGroup_41)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_m_canvasGroup_41() const { return ___m_canvasGroup_41; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_m_canvasGroup_41() { return &___m_canvasGroup_41; }
	inline void set_m_canvasGroup_41(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___m_canvasGroup_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasGroup_41), value);
	}

	inline static int32_t get_offset_of_m_visibilityProgress_42() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___m_visibilityProgress_42)); }
	inline float get_m_visibilityProgress_42() const { return ___m_visibilityProgress_42; }
	inline float* get_address_of_m_visibilityProgress_42() { return &___m_visibilityProgress_42; }
	inline void set_m_visibilityProgress_42(float value)
	{
		___m_visibilityProgress_42 = value;
	}

	inline static int32_t get_offset_of_m_visibility_43() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___m_visibility_43)); }
	inline int32_t get_m_visibility_43() const { return ___m_visibility_43; }
	inline int32_t* get_address_of_m_visibility_43() { return &___m_visibility_43; }
	inline void set_m_visibility_43(int32_t value)
	{
		___m_visibility_43 = value;
	}

	inline static int32_t get_offset_of_m_showCoroutine_44() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___m_showCoroutine_44)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_showCoroutine_44() const { return ___m_showCoroutine_44; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_showCoroutine_44() { return &___m_showCoroutine_44; }
	inline void set_m_showCoroutine_44(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_showCoroutine_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_showCoroutine_44), value);
	}

	inline static int32_t get_offset_of_m_hideCoroutine_45() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___m_hideCoroutine_45)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_hideCoroutine_45() const { return ___m_hideCoroutine_45; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_hideCoroutine_45() { return &___m_hideCoroutine_45; }
	inline void set_m_hideCoroutine_45(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_hideCoroutine_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_hideCoroutine_45), value);
	}

	inline static int32_t get_offset_of_m_autoHideCoroutine_46() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___m_autoHideCoroutine_46)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_autoHideCoroutine_46() const { return ___m_autoHideCoroutine_46; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_autoHideCoroutine_46() { return &___m_autoHideCoroutine_46; }
	inline void set_m_autoHideCoroutine_46(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_autoHideCoroutine_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_autoHideCoroutine_46), value);
	}

	inline static int32_t get_offset_of_m_disableButtonClickCoroutine_47() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___m_disableButtonClickCoroutine_47)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_disableButtonClickCoroutine_47() const { return ___m_disableButtonClickCoroutine_47; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_disableButtonClickCoroutine_47() { return &___m_disableButtonClickCoroutine_47; }
	inline void set_m_disableButtonClickCoroutine_47(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_disableButtonClickCoroutine_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_disableButtonClickCoroutine_47), value);
	}

	inline static int32_t get_offset_of_m_childUIButtons_48() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___m_childUIButtons_48)); }
	inline UIButtonU5BU5D_t2DA91AEF4743C142AF0D299149E2019C6EF0467F* get_m_childUIButtons_48() const { return ___m_childUIButtons_48; }
	inline UIButtonU5BU5D_t2DA91AEF4743C142AF0D299149E2019C6EF0467F** get_address_of_m_childUIButtons_48() { return &___m_childUIButtons_48; }
	inline void set_m_childUIButtons_48(UIButtonU5BU5D_t2DA91AEF4743C142AF0D299149E2019C6EF0467F* value)
	{
		___m_childUIButtons_48 = value;
		Il2CppCodeGenWriteBarrier((&___m_childUIButtons_48), value);
	}

	inline static int32_t get_offset_of_m_childUIViews_49() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___m_childUIViews_49)); }
	inline UIViewU5BU5D_t1BD9626268B6429358BFEB30BFE62444CAC554BD* get_m_childUIViews_49() const { return ___m_childUIViews_49; }
	inline UIViewU5BU5D_t1BD9626268B6429358BFEB30BFE62444CAC554BD** get_address_of_m_childUIViews_49() { return &___m_childUIViews_49; }
	inline void set_m_childUIViews_49(UIViewU5BU5D_t1BD9626268B6429358BFEB30BFE62444CAC554BD* value)
	{
		___m_childUIViews_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_childUIViews_49), value);
	}

	inline static int32_t get_offset_of_m_initialized_50() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF, ___m_initialized_50)); }
	inline bool get_m_initialized_50() const { return ___m_initialized_50; }
	inline bool* get_address_of_m_initialized_50() { return &___m_initialized_50; }
	inline void set_m_initialized_50(bool value)
	{
		___m_initialized_50 = value;
	}
};

struct UIView_t65D38836246049951821D4E45B1C81947591EBDF_StaticFields
{
public:
	// System.Action`2<Doozy.Engine.UI.UIView,Doozy.Engine.UI.UIViewBehaviorType> Doozy.Engine.UI.UIView::OnUIViewAction
	Action_2_t1AAA92D44462571FF056D5AFE831F92AA6D8A9D7 * ___OnUIViewAction_13;
	// System.Collections.Generic.List`1<Doozy.Engine.UI.UIView> Doozy.Engine.UI.UIView::VisibleViews
	List_1_t7A5261DAB640C55051ADE8D918AAF06457CF2378 * ___VisibleViews_14;

public:
	inline static int32_t get_offset_of_OnUIViewAction_13() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF_StaticFields, ___OnUIViewAction_13)); }
	inline Action_2_t1AAA92D44462571FF056D5AFE831F92AA6D8A9D7 * get_OnUIViewAction_13() const { return ___OnUIViewAction_13; }
	inline Action_2_t1AAA92D44462571FF056D5AFE831F92AA6D8A9D7 ** get_address_of_OnUIViewAction_13() { return &___OnUIViewAction_13; }
	inline void set_OnUIViewAction_13(Action_2_t1AAA92D44462571FF056D5AFE831F92AA6D8A9D7 * value)
	{
		___OnUIViewAction_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnUIViewAction_13), value);
	}

	inline static int32_t get_offset_of_VisibleViews_14() { return static_cast<int32_t>(offsetof(UIView_t65D38836246049951821D4E45B1C81947591EBDF_StaticFields, ___VisibleViews_14)); }
	inline List_1_t7A5261DAB640C55051ADE8D918AAF06457CF2378 * get_VisibleViews_14() const { return ___VisibleViews_14; }
	inline List_1_t7A5261DAB640C55051ADE8D918AAF06457CF2378 ** get_address_of_VisibleViews_14() { return &___VisibleViews_14; }
	inline void set_VisibleViews_14(List_1_t7A5261DAB640C55051ADE8D918AAF06457CF2378 * value)
	{
		___VisibleViews_14 = value;
		Il2CppCodeGenWriteBarrier((&___VisibleViews_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEW_T65D38836246049951821D4E45B1C81947591EBDF_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5600 = { sizeof (M_t470B6C8EB456A1AE2FCA220FA5CA47A2F2983764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5600[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5601 = { sizeof (Y_t811F058E218BD0332457223D58D89F93B033F14E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5601[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5602 = { sizeof (K_t1BBBD3F72964ADDA67B0FEEECF7D03939235EC48), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5602[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5603 = { sizeof (HSL_t0B3C6ABBDB52C9D9FF28B25B1A6D49B290C4B51F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5603[3] = 
{
	HSL_t0B3C6ABBDB52C9D9FF28B25B1A6D49B290C4B51F::get_offset_of_h_0(),
	HSL_t0B3C6ABBDB52C9D9FF28B25B1A6D49B290C4B51F::get_offset_of_s_1(),
	HSL_t0B3C6ABBDB52C9D9FF28B25B1A6D49B290C4B51F::get_offset_of_l_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5604 = { sizeof (H_t5584741E522FCF9D58B1F060256A48C907B9A9B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5604[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5605 = { sizeof (S_t681EBA87637B8F525B1905F4BAD6C2C0B32669A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5605[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5606 = { sizeof (L_t753A1F9F315EA5D7218960492E3984BB50EBE371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5606[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5607 = { sizeof (HSV_tD5CDD00DCCE24D4769693D3B853C0051B41272D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5607[3] = 
{
	HSV_tD5CDD00DCCE24D4769693D3B853C0051B41272D4::get_offset_of_h_0(),
	HSV_tD5CDD00DCCE24D4769693D3B853C0051B41272D4::get_offset_of_s_1(),
	HSV_tD5CDD00DCCE24D4769693D3B853C0051B41272D4::get_offset_of_v_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5608 = { sizeof (H_t23779FBE8D5D65A8EF821291DD3E3B4EB8A086EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5608[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5609 = { sizeof (S_t3AEEB67C04D389B7B58F9F098379DCAB75217776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5609[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5610 = { sizeof (V_tA209264E75A9CE99CF8856BC8B0786DB6365E613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5610[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5611 = { sizeof (RGB_t25E15B2C36B00E5F2D7538CEF52679EA7408486D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5611[3] = 
{
	RGB_t25E15B2C36B00E5F2D7538CEF52679EA7408486D::get_offset_of_r_0(),
	RGB_t25E15B2C36B00E5F2D7538CEF52679EA7408486D::get_offset_of_g_1(),
	RGB_t25E15B2C36B00E5F2D7538CEF52679EA7408486D::get_offset_of_b_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5612 = { sizeof (R_t3B6787C339F9D631EE35A12BB7240E57B895DB8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5612[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5613 = { sizeof (G_t69DD6CC9287DC797284FF21BD4BE9D33583821D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5613[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5614 = { sizeof (B_t9050547E3AD0819EC78B6411B680D0BF40CDBF32), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5614[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5615 = { sizeof (XYZ_tD5A59D80D6552D6545DCA9F8D46C07F7AE914D65), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5615[3] = 
{
	XYZ_tD5A59D80D6552D6545DCA9F8D46C07F7AE914D65::get_offset_of_x_0(),
	XYZ_tD5A59D80D6552D6545DCA9F8D46C07F7AE914D65::get_offset_of_y_1(),
	XYZ_tD5A59D80D6552D6545DCA9F8D46C07F7AE914D65::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5616 = { sizeof (X_t0DE7F3DF404E217F40CEAEEC5688B4C1ED1BDFCD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5616[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5617 = { sizeof (Y_tC001AC3D799B9E5BECCB966FC77B9349B488BC27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5617[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5618 = { sizeof (Z_tF3C318BAA0CAB9A3D587EC2E72593FC33A47458F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5618[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5619 = { sizeof (DynamicSorting_tE88845A7C1E8C4071C615A938ABB17C18E7D8230)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5619[5] = 
{
	DynamicSorting_tE88845A7C1E8C4071C615A938ABB17C18E7D8230::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5620 = { sizeof (PopupDisplayOn_t889AC013674992F05BFC1482F6E0F119436B3A62)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5620[3] = 
{
	PopupDisplayOn_t889AC013674992F05BFC1482F6E0F119436B3A62::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5621 = { sizeof (TargetLabel_t687F455FAE7E9369DC6001A633BEFAEACE1CE1CB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5621[4] = 
{
	TargetLabel_t687F455FAE7E9369DC6001A633BEFAEACE1CE1CB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5622 = { sizeof (TargetOrientation_tC5F5E22D9BFB8892A068DB6956AE047A4D07AFCC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5622[4] = 
{
	TargetOrientation_tC5F5E22D9BFB8892A068DB6956AE047A4D07AFCC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5623 = { sizeof (UIEffectBehavior_t65B6EA484234ECF2B725C4ED8FAFDCF3F8E81F25)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5623[3] = 
{
	UIEffectBehavior_t65B6EA484234ECF2B725C4ED8FAFDCF3F8E81F25::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5624 = { sizeof (VisibilityState_t57C44223D66727F9AEBFE2E0EDDD2895E58C296D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5624[5] = 
{
	VisibilityState_t57C44223D66727F9AEBFE2E0EDDD2895E58C296D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5625 = { sizeof (SingleClickMode_tDB58245544CF8536DD5907066283135060CAEEB5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5625[3] = 
{
	SingleClickMode_tDB58245544CF8536DD5907066283135060CAEEB5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5626 = { sizeof (UIButtonBehaviorType_tB025C99754BC9980024AFF2E71926A0ACD1A2756)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5626[10] = 
{
	UIButtonBehaviorType_tB025C99754BC9980024AFF2E71926A0ACD1A2756::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5627 = { sizeof (UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527), -1, sizeof(UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5627[33] = 
{
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527_StaticFields::get_offset_of_OnUIButtonAction_13(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_AllowMultipleClicks_14(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_ButtonCategory_15(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_ButtonName_16(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_ClickMode_17(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_DeselectButtonAfterClick_18(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_DisableButtonBetweenClicksInterval_19(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_DoubleClickRegisterInterval_20(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_InputData_21(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_LongClickRegisterInterval_22(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_OnPointerEnter_23(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_OnPointerExit_24(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_OnPointerDown_25(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_OnPointerUp_26(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_OnClick_27(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_OnDoubleClick_28(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_OnLongClick_29(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_OnSelected_30(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_OnDeselected_31(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_NormalLoopAnimation_32(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_SelectedLoopAnimation_33(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_TargetLabel_34(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_TextLabel_35(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_TextMeshProLabel_36(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_m_button_37(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_m_canvasGroup_38(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_m_clickedOnce_39(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_m_disableButtonCoroutine_40(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_m_doubleClickTimeoutCounter_41(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_m_executedLongClick_42(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_m_longClickRegisterCoroutine_43(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_m_longClickTimeoutCounter_44(),
	UIButton_tE09DCDE2C5CC3714E7EAC47596EB415B340DC527::get_offset_of_m_updateStartValuesRequired_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5628 = { sizeof (U3CDeselectButtonEnumeratorU3Ed__100_t31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5628[4] = 
{
	U3CDeselectButtonEnumeratorU3Ed__100_t31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9::get_offset_of_U3CU3E1__state_0(),
	U3CDeselectButtonEnumeratorU3Ed__100_t31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9::get_offset_of_U3CU3E2__current_1(),
	U3CDeselectButtonEnumeratorU3Ed__100_t31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9::get_offset_of_delay_2(),
	U3CDeselectButtonEnumeratorU3Ed__100_t31BD0D6B5E469A8ADD4C8D17A2D75C7CD913B0A9::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5629 = { sizeof (U3CExecuteButtonBehaviorEnumeratorU3Ed__101_t2902714DADF27EB596AC709538EA6BBEA4B61A57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5629[4] = 
{
	U3CExecuteButtonBehaviorEnumeratorU3Ed__101_t2902714DADF27EB596AC709538EA6BBEA4B61A57::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteButtonBehaviorEnumeratorU3Ed__101_t2902714DADF27EB596AC709538EA6BBEA4B61A57::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteButtonBehaviorEnumeratorU3Ed__101_t2902714DADF27EB596AC709538EA6BBEA4B61A57::get_offset_of_behavior_2(),
	U3CExecuteButtonBehaviorEnumeratorU3Ed__101_t2902714DADF27EB596AC709538EA6BBEA4B61A57::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5630 = { sizeof (U3CDisableButtonEnumeratorU3Ed__102_t4A1FCECCABAD08BEA951F7C2B639A09F56202420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5630[4] = 
{
	U3CDisableButtonEnumeratorU3Ed__102_t4A1FCECCABAD08BEA951F7C2B639A09F56202420::get_offset_of_U3CU3E1__state_0(),
	U3CDisableButtonEnumeratorU3Ed__102_t4A1FCECCABAD08BEA951F7C2B639A09F56202420::get_offset_of_U3CU3E2__current_1(),
	U3CDisableButtonEnumeratorU3Ed__102_t4A1FCECCABAD08BEA951F7C2B639A09F56202420::get_offset_of_duration_2(),
	U3CDisableButtonEnumeratorU3Ed__102_t4A1FCECCABAD08BEA951F7C2B639A09F56202420::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5631 = { sizeof (U3CDisableButtonBehaviorEnumeratorU3Ed__103_t199055427FBD89ADC054A444AD42B1B513AEB13B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5631[4] = 
{
	U3CDisableButtonBehaviorEnumeratorU3Ed__103_t199055427FBD89ADC054A444AD42B1B513AEB13B::get_offset_of_U3CU3E1__state_0(),
	U3CDisableButtonBehaviorEnumeratorU3Ed__103_t199055427FBD89ADC054A444AD42B1B513AEB13B::get_offset_of_U3CU3E2__current_1(),
	U3CDisableButtonBehaviorEnumeratorU3Ed__103_t199055427FBD89ADC054A444AD42B1B513AEB13B::get_offset_of_behavior_2(),
	U3CDisableButtonBehaviorEnumeratorU3Ed__103_t199055427FBD89ADC054A444AD42B1B513AEB13B::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5632 = { sizeof (U3CRunOnClickEnumeratorU3Ed__104_t851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5632[4] = 
{
	U3CRunOnClickEnumeratorU3Ed__104_t851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E::get_offset_of_U3CU3E1__state_0(),
	U3CRunOnClickEnumeratorU3Ed__104_t851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E::get_offset_of_U3CU3E2__current_1(),
	U3CRunOnClickEnumeratorU3Ed__104_t851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E::get_offset_of_debug_2(),
	U3CRunOnClickEnumeratorU3Ed__104_t851DD6DCC03567B1D4EFC2BFAE4CC8D40E349F4E::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5633 = { sizeof (U3CRunOnLongClickEnumeratorU3Ed__105_t1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5633[4] = 
{
	U3CRunOnLongClickEnumeratorU3Ed__105_t1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35::get_offset_of_U3CU3E1__state_0(),
	U3CRunOnLongClickEnumeratorU3Ed__105_t1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35::get_offset_of_U3CU3E2__current_1(),
	U3CRunOnLongClickEnumeratorU3Ed__105_t1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35::get_offset_of_debug_2(),
	U3CRunOnLongClickEnumeratorU3Ed__105_t1FCB5C4A2DC51867FC954F8A30CAFF7EE3BC7E35::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5634 = { sizeof (U3CU3Ec_tB75A3DC1E24B6E257685C28755193DAF705521BA), -1, sizeof(U3CU3Ec_tB75A3DC1E24B6E257685C28755193DAF705521BA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5634[1] = 
{
	U3CU3Ec_tB75A3DC1E24B6E257685C28755193DAF705521BA_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5635 = { sizeof (UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5635[31] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_Animators_16(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_ButtonAnimationType_17(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_DeselectButton_18(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_DisableInterval_19(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_Enabled_20(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_LoadSelectedPresetAtRuntime_21(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_OnTrigger_22(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_PresetCategory_23(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_PresetName_24(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_PunchAnimation_25(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_Ready_26(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_SelectButton_27(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_StateAnimation_28(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_TriggerEventsAfterAnimation_29(),
	UIButtonBehavior_t08AD16D7696D1DE757449CCA5629CF91DC7989BB::get_offset_of_m_behaviorType_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5636 = { sizeof (U3CInvokeCallbacksU3Ed__63_tBA89A9C8709448600B85DC5223F373BA379DCB49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5636[5] = 
{
	U3CInvokeCallbacksU3Ed__63_tBA89A9C8709448600B85DC5223F373BA379DCB49::get_offset_of_U3CU3E1__state_0(),
	U3CInvokeCallbacksU3Ed__63_tBA89A9C8709448600B85DC5223F373BA379DCB49::get_offset_of_U3CU3E2__current_1(),
	U3CInvokeCallbacksU3Ed__63_tBA89A9C8709448600B85DC5223F373BA379DCB49::get_offset_of_animation_2(),
	U3CInvokeCallbacksU3Ed__63_tBA89A9C8709448600B85DC5223F373BA379DCB49::get_offset_of_onStartCallback_3(),
	U3CInvokeCallbacksU3Ed__63_tBA89A9C8709448600B85DC5223F373BA379DCB49::get_offset_of_onCompleteCallback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5637 = { sizeof (UIButtonEvent_t4D2122D7D8CDB28C0231D7429F5D12847FB55BD6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5638 = { sizeof (UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5638[6] = 
{
	UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52::get_offset_of_ButtonCategory_4(),
	UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52::get_offset_of_ButtonName_5(),
	UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52::get_offset_of_DebugMode_6(),
	UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52::get_offset_of_Event_7(),
	UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52::get_offset_of_ListenForAllUIButtons_8(),
	UIButtonListener_t9B4F79D2098A92A608A52A396C408B25C969DF52::get_offset_of_TriggerAction_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5639 = { sizeof (UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5639[7] = 
{
	UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45::get_offset_of_Animation_0(),
	UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45::get_offset_of_Enabled_1(),
	UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45::get_offset_of_IsPlaying_2(),
	UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45::get_offset_of_LoopAnimationType_3(),
	UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45::get_offset_of_LoadSelectedPresetAtRuntime_4(),
	UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45::get_offset_of_PresetCategory_5(),
	UIButtonLoopAnimation_t1650AF2BAEF3EC75DA2B5ECCE37E17B43D6B1C45::get_offset_of_PresetName_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5640 = { sizeof (UIButtonMessage_t61D6B99B7450B7A9F457D4ED5568802137189B2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5640[3] = 
{
	UIButtonMessage_t61D6B99B7450B7A9F457D4ED5568802137189B2A::get_offset_of_Button_3(),
	UIButtonMessage_t61D6B99B7450B7A9F457D4ED5568802137189B2A::get_offset_of_ButtonName_4(),
	UIButtonMessage_t61D6B99B7450B7A9F457D4ED5568802137189B2A::get_offset_of_Type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5641 = { sizeof (UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37), -1, sizeof(UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5641[5] = 
{
	UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37_StaticFields::get_offset_of_U3CMasterCanvasU3Ek__BackingField_13(),
	UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37::get_offset_of_CanvasName_14(),
	UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37::get_offset_of_CustomCanvasName_15(),
	UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37::get_offset_of_DontDestroyCanvasOnLoad_16(),
	UICanvas_t1EF5C75FAD3B7FA2460FEB344C1ECE8D136DDF37::get_offset_of_m_canvas_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5642 = { sizeof (U3CU3Ec__DisplayClass23_0_t4C9702962FEB0FE277DD2C647B71A44068DE2AB3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5642[1] = 
{
	U3CU3Ec__DisplayClass23_0_t4C9702962FEB0FE277DD2C647B71A44068DE2AB3::get_offset_of_canvasName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5643 = { sizeof (U3CU3Ec__DisplayClass25_0_t9906805B29C8E53DAE16AAAA50C374E875B19C75), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5643[1] = 
{
	U3CU3Ec__DisplayClass25_0_t9906805B29C8E53DAE16AAAA50C374E875B19C75::get_offset_of_canvasName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5644 = { sizeof (UIDrawerBehaviorType_t50837F7287BAFBBD867C2803DA5A696515A2DC6F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5644[4] = 
{
	UIDrawerBehaviorType_t50837F7287BAFBBD867C2803DA5A696515A2DC6F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5645 = { sizeof (UIDrawerContainerSize_t179CDF00D47D8D05D0AB7D2074965C38B1FDF5CB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5645[4] = 
{
	UIDrawerContainerSize_t179CDF00D47D8D05D0AB7D2074965C38B1FDF5CB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5646 = { sizeof (UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A), -1, sizeof(UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5646[37] = 
{
	0,
	0,
	0,
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A_StaticFields::get_offset_of_U3CDraggedDrawerU3Ek__BackingField_16(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A_StaticFields::get_offset_of_U3COpenedDrawerU3Ek__BackingField_17(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A_StaticFields::get_offset_of_OnUIDrawerBehavior_18(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_U3CIsDraggedU3Ek__BackingField_19(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_Arrow_20(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_CloseBehavior_21(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_CloseDirection_22(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_CloseSpeed_23(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_Container_24(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_CustomStartAnchoredPosition_25(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_CustomDrawerName_26(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_DrawerName_27(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_DetectGestures_28(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_DragBehavior_29(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_OnProgressChanged_30(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_OnInverseProgressChanged_31(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_OpenBehavior_32(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_OpenSpeed_33(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_Overlay_34(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_Progressor_35(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_UseCustomStartAnchoredPosition_36(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_m_canvas_37(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_m_visibility_38(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_m_visibilityProgress_39(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_m_scaledCanvas_40(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_m_availableForDrag_41(),
	UIDrawer_tE812989F3C01F90F70B60FA2EF8D250C07A3427A::get_offset_of_m_dragStartPosition_42(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5647 = { sizeof (U3CU3Ec_t25A319B6A9D3D41CF9023A159940BCCFE25B4B8D), -1, sizeof(U3CU3Ec_t25A319B6A9D3D41CF9023A159940BCCFE25B4B8D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5647[1] = 
{
	U3CU3Ec_t25A319B6A9D3D41CF9023A159940BCCFE25B4B8D_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5648 = { sizeof (UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3), -1, sizeof(UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5648[16] = 
{
	0,
	0,
	0,
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3_StaticFields::get_offset_of_DefaultOpenedColor_3(),
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3_StaticFields::get_offset_of_DefaultClosedColor_4(),
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3::get_offset_of_Animator_5(),
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3::get_offset_of_ClosedColor_6(),
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3::get_offset_of_Container_7(),
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3::get_offset_of_Down_8(),
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3::get_offset_of_Enabled_9(),
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3::get_offset_of_Left_10(),
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3::get_offset_of_OpenedColor_11(),
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3::get_offset_of_OverrideColor_12(),
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3::get_offset_of_Right_13(),
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3::get_offset_of_Scale_14(),
	UIDrawerArrow_t752417C85A8252AED8E70B35AAE2197E7011D4A3::get_offset_of_Up_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5649 = { sizeof (Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5649[3] = 
{
	Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0::get_offset_of_Closed_0(),
	Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0::get_offset_of_Opened_1(),
	Holder_t3C12D4A9F70C9EA75964CB7EFADC8A7629EA20C0::get_offset_of_Root_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5650 = { sizeof (UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5650[18] = 
{
	0,
	0,
	0,
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_U3CDrawerU3Ek__BackingField_7(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_U3CWidthU3Ek__BackingField_8(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_U3CHeightU3Ek__BackingField_9(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_Rotator_10(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_LeftBar_11(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_RightBar_12(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_m_rectTransform_13(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_m_leftBarImage_14(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_m_rightBarImage_15(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_m_velocity_16(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_m_rotatorCorners_17(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_m_drawerCorners_18(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_m_rotatorDisableThreshold_19(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_m_tempCorners_20(),
	UIDrawerArrowAnimator_t6CCB105AF52EBAFA8CE9CE9BE801602FA9A4596E::get_offset_of_m_rotatorRect_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5651 = { sizeof (UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5651[3] = 
{
	UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843::get_offset_of_DrawerBehaviorType_0(),
	UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843::get_offset_of_OnFinished_1(),
	UIDrawerBehavior_t904272C6FDCA7A411594306A88157DB626250843::get_offset_of_OnStart_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5652 = { sizeof (UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5652[15] = 
{
	0,
	0,
	0,
	0,
	0,
	UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C::get_offset_of_CalculatedSize_21(),
	UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C::get_offset_of_ClosedPosition_22(),
	UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C::get_offset_of_CurrentPosition_23(),
	UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C::get_offset_of_FadeOut_24(),
	UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C::get_offset_of_FixedSize_25(),
	UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C::get_offset_of_MinimumSize_26(),
	UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C::get_offset_of_OpenedPosition_27(),
	UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C::get_offset_of_PercentageOfScreen_28(),
	UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C::get_offset_of_PreviousPosition_29(),
	UIDrawerContainer_t6875EA47AF7184E1D7387EC138A4BFFD1E79484C::get_offset_of_Size_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5653 = { sizeof (UIDrawerEvent_t511B9CCB479D30EE935B3EA995DBB00D281721F6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5654 = { sizeof (UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5654[6] = 
{
	UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A::get_offset_of_DebugMode_4(),
	UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A::get_offset_of_DrawerName_5(),
	UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A::get_offset_of_CustomDrawerName_6(),
	UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A::get_offset_of_Event_7(),
	UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A::get_offset_of_ListenForAllUIDrawers_8(),
	UIDrawerListener_t068FA313F449D3B14A5B81D2EFD270F3C812310A::get_offset_of_TriggerAction_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5655 = { sizeof (UIDrawerMessage_tB7B5BD2CABB78B5CBC2F1B5E7AA4B8A58B3A85A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5655[2] = 
{
	UIDrawerMessage_tB7B5BD2CABB78B5CBC2F1B5E7AA4B8A58B3A85A2::get_offset_of_Drawer_3(),
	UIDrawerMessage_tB7B5BD2CABB78B5CBC2F1B5E7AA4B8A58B3A85A2::get_offset_of_Type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5656 = { sizeof (UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228), -1, sizeof(UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5656[42] = 
{
	0,
	0,
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228_StaticFields::get_offset_of_OnUIPopupAction_15(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228_StaticFields::get_offset_of_VisiblePopups_16(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_U3CPopupNameU3Ek__BackingField_17(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_AddToPopupQueue_18(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_AutoHideAfterShow_19(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_AutoHideAfterShowDelay_20(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_AutoSelectButtonAfterShow_21(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_CanvasName_22(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_Container_23(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_CustomCanvasName_24(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_Data_25(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_DestroyAfterHide_26(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_DisplayTarget_27(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_HideBehavior_28(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_HideOnBackButton_29(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_HideOnClickAnywhere_30(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_HideOnClickContainer_31(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_HideOnClickOverlay_32(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_HideProgressor_33(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_OnInverseVisibilityChanged_34(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_OnVisibilityChanged_35(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_Overlay_36(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_SelectedButton_37(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_ShowBehavior_38(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_ShowProgressor_39(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_UpdateHideProgressorOnShow_40(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_UpdateShowProgressorOnHide_41(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_UseOverlay_42(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_m_canvas_43(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_m_graphicRaycaster_44(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_m_previousSelectedButton_45(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_m_visibilityProgress_46(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_m_visibilityState_47(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_m_addedToQueue_48(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_m_showCoroutine_49(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_m_hideCoroutine_50(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_m_autoHideCoroutine_51(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_m_disableButtonClickCoroutine_52(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_m_childUIButtons_53(),
	UIPopup_tDDEB8981C29D9A18EE7BD971926654FF0BE76228::get_offset_of_m_initialized_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5657 = { sizeof (U3CTriggerShowInNextFrameU3Ed__107_t44A529641394EA26320518709F51947546F6A3D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5657[4] = 
{
	U3CTriggerShowInNextFrameU3Ed__107_t44A529641394EA26320518709F51947546F6A3D6::get_offset_of_U3CU3E1__state_0(),
	U3CTriggerShowInNextFrameU3Ed__107_t44A529641394EA26320518709F51947546F6A3D6::get_offset_of_U3CU3E2__current_1(),
	U3CTriggerShowInNextFrameU3Ed__107_t44A529641394EA26320518709F51947546F6A3D6::get_offset_of_instantAction_2(),
	U3CTriggerShowInNextFrameU3Ed__107_t44A529641394EA26320518709F51947546F6A3D6::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5658 = { sizeof (U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5658[15] = 
{
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CU3E1__state_0(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CU3E2__current_1(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_instantAction_2(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CU3E4__this_3(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CmoveFromU3E5__1_4(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CmoveToU3E5__2_5(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CrotateFromU3E5__3_6(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CrotateToU3E5__4_7(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CscaleFromU3E5__5_8(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CscaleToU3E5__6_9(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CfadeFromU3E5__7_10(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CfadeToU3E5__8_11(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CstartTimeU3E5__9_12(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CtotalDurationU3E5__10_13(),
	U3CShowEnumeratorU3Ed__108_t230BEBFB042A62D9A283B941A77E258F03F8F7E4::get_offset_of_U3CelapsedTimeU3E5__11_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5659 = { sizeof (U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5659[15] = 
{
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CU3E1__state_0(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CU3E2__current_1(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_instantAction_2(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CU3E4__this_3(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CmoveFromU3E5__1_4(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CmoveToU3E5__2_5(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CrotateFromU3E5__3_6(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CrotateToU3E5__4_7(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CscaleFromU3E5__5_8(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CscaleToU3E5__6_9(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CfadeFromU3E5__7_10(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CfadeToU3E5__8_11(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CstartTimeU3E5__9_12(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CtotalDurationU3E5__10_13(),
	U3CHideEnumeratorU3Ed__109_t6F6B6DA4DE92700AF492FD340D870480E9DC59C6::get_offset_of_U3CelapsedTimeU3E5__11_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5660 = { sizeof (U3CHideWithDelayEnumeratorU3Ed__110_t557B6A11AA67E56EBE7698ADED5EFB93CD747AEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5660[4] = 
{
	U3CHideWithDelayEnumeratorU3Ed__110_t557B6A11AA67E56EBE7698ADED5EFB93CD747AEF::get_offset_of_U3CU3E1__state_0(),
	U3CHideWithDelayEnumeratorU3Ed__110_t557B6A11AA67E56EBE7698ADED5EFB93CD747AEF::get_offset_of_U3CU3E2__current_1(),
	U3CHideWithDelayEnumeratorU3Ed__110_t557B6A11AA67E56EBE7698ADED5EFB93CD747AEF::get_offset_of_delay_2(),
	U3CHideWithDelayEnumeratorU3Ed__110_t557B6A11AA67E56EBE7698ADED5EFB93CD747AEF::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5661 = { sizeof (U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__111_tFE3B4EFC8AC290E0C166D2689226E958F8A3B496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5661[3] = 
{
	U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__111_tFE3B4EFC8AC290E0C166D2689226E958F8A3B496::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__111_tFE3B4EFC8AC290E0C166D2689226E958F8A3B496::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__111_tFE3B4EFC8AC290E0C166D2689226E958F8A3B496::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5662 = { sizeof (U3CExecuteHideDeselectButtonEnumeratorU3Ed__112_tC090EF8248ADF58D35CD2A5E566F5BC3FBFBE0AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5662[3] = 
{
	U3CExecuteHideDeselectButtonEnumeratorU3Ed__112_tC090EF8248ADF58D35CD2A5E566F5BC3FBFBE0AD::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteHideDeselectButtonEnumeratorU3Ed__112_tC090EF8248ADF58D35CD2A5E566F5BC3FBFBE0AD::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteHideDeselectButtonEnumeratorU3Ed__112_tC090EF8248ADF58D35CD2A5E566F5BC3FBFBE0AD::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5663 = { sizeof (U3CU3Ec_t72DAA4CE237962F7E1A2CA768974493280AC9938), -1, sizeof(U3CU3Ec_t72DAA4CE237962F7E1A2CA768974493280AC9938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5663[1] = 
{
	U3CU3Ec_t72DAA4CE237962F7E1A2CA768974493280AC9938_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5664 = { sizeof (UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5664[10] = 
{
	0,
	0,
	UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57::get_offset_of_Animation_2(),
	UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57::get_offset_of_LoadSelectedPresetAtRuntime_3(),
	UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57::get_offset_of_InstantAnimation_4(),
	UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57::get_offset_of_OnFinished_5(),
	UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57::get_offset_of_OnStart_6(),
	UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57::get_offset_of_PresetCategory_7(),
	UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57::get_offset_of_PresetName_8(),
	UIPopupBehavior_t5662D7BF47237D1E0787EAE8A320ADDAD5E5CA57::get_offset_of_m_progress_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5665 = { sizeof (UIPopupContentData_tB9781C2614518D6288C694A4B0352D258126DA40), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5665[5] = 
{
	UIPopupContentData_tB9781C2614518D6288C694A4B0352D258126DA40::get_offset_of_ButtonCallbacks_0(),
	UIPopupContentData_tB9781C2614518D6288C694A4B0352D258126DA40::get_offset_of_ButtonLabels_1(),
	UIPopupContentData_tB9781C2614518D6288C694A4B0352D258126DA40::get_offset_of_ButtonNames_2(),
	UIPopupContentData_tB9781C2614518D6288C694A4B0352D258126DA40::get_offset_of_Labels_3(),
	UIPopupContentData_tB9781C2614518D6288C694A4B0352D258126DA40::get_offset_of_Sprites_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5666 = { sizeof (UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5666[3] = 
{
	UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272::get_offset_of_Buttons_0(),
	UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272::get_offset_of_Images_1(),
	UIPopupContentReferences_tD4A8E0DEE7DFCC2DBD5600E72DD685BF6EBE2272::get_offset_of_Labels_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5667 = { sizeof (UIPopupDatabase_t2DD34A70147D17B8FB3EE2E8F0C95131852AD382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5667[2] = 
{
	UIPopupDatabase_t2DD34A70147D17B8FB3EE2E8F0C95131852AD382::get_offset_of_PopupNames_4(),
	UIPopupDatabase_t2DD34A70147D17B8FB3EE2E8F0C95131852AD382::get_offset_of_Popups_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5668 = { sizeof (U3CU3Ec_t8A0C59A22CCA582D1211C88B397689A059E9E56C), -1, sizeof(U3CU3Ec_t8A0C59A22CCA582D1211C88B397689A059E9E56C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5668[2] = 
{
	U3CU3Ec_t8A0C59A22CCA582D1211C88B397689A059E9E56C_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8A0C59A22CCA582D1211C88B397689A059E9E56C_StaticFields::get_offset_of_U3CU3E9__21_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5669 = { sizeof (UIPopupLink_tFEA0CF345A8EEDF5E9A3CAE95DFE0069A97C78BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5669[2] = 
{
	UIPopupLink_tFEA0CF345A8EEDF5E9A3CAE95DFE0069A97C78BC::get_offset_of_PopupName_4(),
	UIPopupLink_tFEA0CF345A8EEDF5E9A3CAE95DFE0069A97C78BC::get_offset_of_Prefab_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5670 = { sizeof (UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156), -1, sizeof(UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5670[4] = 
{
	UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156_StaticFields::get_offset_of_s_instance_4(),
	UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156_StaticFields::get_offset_of_CurrentVisibleQueuePopup_5(),
	UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156_StaticFields::get_offset_of_PopupQueue_6(),
	UIPopupManager_tFD00A6918320B8C6193C0F5BE38E305703751156_StaticFields::get_offset_of_U3CApplicationIsQuittingU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5671 = { sizeof (UIPopupMessage_t4CDAE4BEA172DE80A59680BCC384ABFD6F05C896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5671[2] = 
{
	UIPopupMessage_t4CDAE4BEA172DE80A59680BCC384ABFD6F05C896::get_offset_of_Popup_3(),
	UIPopupMessage_t4CDAE4BEA172DE80A59680BCC384ABFD6F05C896::get_offset_of_AnimationType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5672 = { sizeof (UIPopupQueueData_t7BB621627113FAFAFE1106E59C2781AD81158945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5672[3] = 
{
	UIPopupQueueData_t7BB621627113FAFAFE1106E59C2781AD81158945::get_offset_of_Popup_0(),
	UIPopupQueueData_t7BB621627113FAFAFE1106E59C2781AD81158945::get_offset_of_PopupName_1(),
	UIPopupQueueData_t7BB621627113FAFAFE1106E59C2781AD81158945::get_offset_of_InstantAction_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5673 = { sizeof (UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273), -1, sizeof(UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5673[20] = 
{
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273_StaticFields::get_offset_of_OnUIToggleAction_13(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_AllowMultipleClicks_14(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_DisableButtonBetweenClicksInterval_15(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_DeselectButtonAfterClick_16(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_InputData_17(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_OnPointerEnter_18(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_OnPointerExit_19(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_OnClick_20(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_OnSelected_21(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_OnDeselected_22(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_OnValueChanged_23(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_TargetLabel_24(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_TextLabel_25(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_TextMeshProLabel_26(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_ToggleProgressor_27(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_m_canvasGroup_28(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_m_disableButtonCoroutine_29(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_m_previousValue_30(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_m_toggle_31(),
	UIToggle_t273F3954806AC38A83534BC27D54B2839CF31273::get_offset_of_m_updateStartValuesRequired_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5674 = { sizeof (U3CDeselectToggleEnumeratorU3Ed__67_tF4419416728077A9CB1B098174095FB66EA2B1FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5674[4] = 
{
	U3CDeselectToggleEnumeratorU3Ed__67_tF4419416728077A9CB1B098174095FB66EA2B1FE::get_offset_of_U3CU3E1__state_0(),
	U3CDeselectToggleEnumeratorU3Ed__67_tF4419416728077A9CB1B098174095FB66EA2B1FE::get_offset_of_U3CU3E2__current_1(),
	U3CDeselectToggleEnumeratorU3Ed__67_tF4419416728077A9CB1B098174095FB66EA2B1FE::get_offset_of_delay_2(),
	U3CDeselectToggleEnumeratorU3Ed__67_tF4419416728077A9CB1B098174095FB66EA2B1FE::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5675 = { sizeof (U3CExecuteToggleBehaviorEnumeratorU3Ed__68_tFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5675[4] = 
{
	U3CExecuteToggleBehaviorEnumeratorU3Ed__68_tFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteToggleBehaviorEnumeratorU3Ed__68_tFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteToggleBehaviorEnumeratorU3Ed__68_tFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268::get_offset_of_behavior_2(),
	U3CExecuteToggleBehaviorEnumeratorU3Ed__68_tFDC04559E4DC8186D21C1FFACF8C8DA0BEBB2268::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5676 = { sizeof (U3CDisableToggleEnumeratorU3Ed__69_t462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5676[4] = 
{
	U3CDisableToggleEnumeratorU3Ed__69_t462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6::get_offset_of_U3CU3E1__state_0(),
	U3CDisableToggleEnumeratorU3Ed__69_t462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6::get_offset_of_U3CU3E2__current_1(),
	U3CDisableToggleEnumeratorU3Ed__69_t462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6::get_offset_of_duration_2(),
	U3CDisableToggleEnumeratorU3Ed__69_t462B4BF337FD1F606A1F9BA3DCD1DDD843312DE6::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5677 = { sizeof (U3CDisableToggleBehaviorEnumeratorU3Ed__70_t83B06053B430B3A4518E32C9FA8DD0797088FC14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5677[4] = 
{
	U3CDisableToggleBehaviorEnumeratorU3Ed__70_t83B06053B430B3A4518E32C9FA8DD0797088FC14::get_offset_of_U3CU3E1__state_0(),
	U3CDisableToggleBehaviorEnumeratorU3Ed__70_t83B06053B430B3A4518E32C9FA8DD0797088FC14::get_offset_of_U3CU3E2__current_1(),
	U3CDisableToggleBehaviorEnumeratorU3Ed__70_t83B06053B430B3A4518E32C9FA8DD0797088FC14::get_offset_of_behavior_2(),
	U3CDisableToggleBehaviorEnumeratorU3Ed__70_t83B06053B430B3A4518E32C9FA8DD0797088FC14::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5678 = { sizeof (U3CU3Ec_t9012F08FC47EB8382D5E4F400955F83D2AB04423), -1, sizeof(U3CU3Ec_t9012F08FC47EB8382D5E4F400955F83D2AB04423_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5678[1] = 
{
	U3CU3Ec_t9012F08FC47EB8382D5E4F400955F83D2AB04423_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5679 = { sizeof (UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5679[28] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_Animators_12(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_ButtonAnimationType_13(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_DeselectButton_14(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_DisableInterval_15(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_Enabled_16(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_LoadSelectedPresetAtRuntime_17(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_OnToggleOff_18(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_OnToggleOn_19(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_PresetCategory_20(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_PresetName_21(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_PunchAnimation_22(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_Ready_23(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_SelectButton_24(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_StateAnimation_25(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_TriggerEventsAfterAnimation_26(),
	UIToggleBehavior_tA4A2D478F55BD633ED901A82FA5B8B11BE07A1AB::get_offset_of_m_behaviorType_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5680 = { sizeof (U3CU3Ec__DisplayClass56_0_tB2BE4CEDDE6B05AEF1D6C5D63BB942FF8B58F2F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5680[2] = 
{
	U3CU3Ec__DisplayClass56_0_tB2BE4CEDDE6B05AEF1D6C5D63BB942FF8B58F2F7::get_offset_of_toggle_0(),
	U3CU3Ec__DisplayClass56_0_tB2BE4CEDDE6B05AEF1D6C5D63BB942FF8B58F2F7::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5681 = { sizeof (U3CInvokeCallbacksU3Ed__61_t2AA3B7C663BD5E43DE249528D74DDF6499B9D04C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5681[5] = 
{
	U3CInvokeCallbacksU3Ed__61_t2AA3B7C663BD5E43DE249528D74DDF6499B9D04C::get_offset_of_U3CU3E1__state_0(),
	U3CInvokeCallbacksU3Ed__61_t2AA3B7C663BD5E43DE249528D74DDF6499B9D04C::get_offset_of_U3CU3E2__current_1(),
	U3CInvokeCallbacksU3Ed__61_t2AA3B7C663BD5E43DE249528D74DDF6499B9D04C::get_offset_of_animation_2(),
	U3CInvokeCallbacksU3Ed__61_t2AA3B7C663BD5E43DE249528D74DDF6499B9D04C::get_offset_of_onStartCallback_3(),
	U3CInvokeCallbacksU3Ed__61_t2AA3B7C663BD5E43DE249528D74DDF6499B9D04C::get_offset_of_onCompleteCallback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5682 = { sizeof (U3CInvokeCallbackAfterDelayU3Ed__62_tD8A3B6A09EB18CA168FD5C126F7CC665158185FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5682[4] = 
{
	U3CInvokeCallbackAfterDelayU3Ed__62_tD8A3B6A09EB18CA168FD5C126F7CC665158185FD::get_offset_of_U3CU3E1__state_0(),
	U3CInvokeCallbackAfterDelayU3Ed__62_tD8A3B6A09EB18CA168FD5C126F7CC665158185FD::get_offset_of_U3CU3E2__current_1(),
	U3CInvokeCallbackAfterDelayU3Ed__62_tD8A3B6A09EB18CA168FD5C126F7CC665158185FD::get_offset_of_callback_2(),
	U3CInvokeCallbackAfterDelayU3Ed__62_tD8A3B6A09EB18CA168FD5C126F7CC665158185FD::get_offset_of_delay_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5683 = { sizeof (UIToggleBehaviorType_t0AAFDB4A4F415B675CBC7767466D63C5E4D8550A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5683[6] = 
{
	UIToggleBehaviorType_t0AAFDB4A4F415B675CBC7767466D63C5E4D8550A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5684 = { sizeof (UIToggleEvent_tBF355DCF93DCBA057A8DD70BE2B783E2D88EEE5C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5685 = { sizeof (UIToggleMessage_t707A3D7907C271DF472A0D1BB244424E2ACE5728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5685[3] = 
{
	UIToggleMessage_t707A3D7907C271DF472A0D1BB244424E2ACE5728::get_offset_of_Toggle_3(),
	UIToggleMessage_t707A3D7907C271DF472A0D1BB244424E2ACE5728::get_offset_of_ToggleState_4(),
	UIToggleMessage_t707A3D7907C271DF472A0D1BB244424E2ACE5728::get_offset_of_Type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5686 = { sizeof (UIToggleState_t0A73CA396396F36C7DBCC37E27DF02838BE17D8E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5686[3] = 
{
	UIToggleState_t0A73CA396396F36C7DBCC37E27DF02838BE17D8E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5687 = { sizeof (UIViewBehaviorType_t652DB3CABD15D72ADC5D734651A8CAD49C8B0ECB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5687[5] = 
{
	UIViewBehaviorType_t652DB3CABD15D72ADC5D734651A8CAD49C8B0ECB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5688 = { sizeof (UIViewStartBehavior_t1C1D97FA8BECE7BDEAC82D7BA95BE81C411619B0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5688[4] = 
{
	UIViewStartBehavior_t1C1D97FA8BECE7BDEAC82D7BA95BE81C411619B0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5689 = { sizeof (UIView_t65D38836246049951821D4E45B1C81947591EBDF), -1, sizeof(UIView_t65D38836246049951821D4E45B1C81947591EBDF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5689[38] = 
{
	UIView_t65D38836246049951821D4E45B1C81947591EBDF_StaticFields::get_offset_of_OnUIViewAction_13(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF_StaticFields::get_offset_of_VisibleViews_14(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_AutoHideAfterShow_15(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_AutoHideAfterShowDelay_16(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_AutoSelectButtonAfterShow_17(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_BehaviorAtStart_18(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_CustomStartAnchoredPosition_19(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_DeselectAnyButtonSelectedOnHide_20(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_DeselectAnyButtonSelectedOnShow_21(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_DisableCanvasWhenHidden_22(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_DisableGameObjectWhenHidden_23(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_DisableGraphicRaycasterWhenHidden_24(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_HideBehavior_25(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_HideProgressor_26(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_LoopBehavior_27(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_OnInverseVisibilityChanged_28(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_OnVisibilityChanged_29(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_SelectedButton_30(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_ShowBehavior_31(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_ShowProgressor_32(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_TargetOrientation_33(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_UpdateHideProgressorOnShow_34(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_UpdateShowProgressorOnHide_35(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_UseCustomStartAnchoredPosition_36(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_ViewCategory_37(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_ViewName_38(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_m_canvas_39(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_m_graphicRaycaster_40(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_m_canvasGroup_41(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_m_visibilityProgress_42(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_m_visibility_43(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_m_showCoroutine_44(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_m_hideCoroutine_45(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_m_autoHideCoroutine_46(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_m_disableButtonClickCoroutine_47(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_m_childUIButtons_48(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_m_childUIViews_49(),
	UIView_t65D38836246049951821D4E45B1C81947591EBDF::get_offset_of_m_initialized_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5690 = { sizeof (U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB), -1, sizeof(U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5690[3] = 
{
	U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB_StaticFields::get_offset_of_U3CU3E9__93_0_1(),
	U3CU3Ec_t14BFBF58E1978D2CE47DD45B594E83527C0AEBFB_StaticFields::get_offset_of_U3CU3E9__93_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5691 = { sizeof (U3CTriggerShowInNextFrameU3Ed__95_t2F667FEC4BF3B323240968A9D12C56CE87131EBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5691[4] = 
{
	U3CTriggerShowInNextFrameU3Ed__95_t2F667FEC4BF3B323240968A9D12C56CE87131EBC::get_offset_of_U3CU3E1__state_0(),
	U3CTriggerShowInNextFrameU3Ed__95_t2F667FEC4BF3B323240968A9D12C56CE87131EBC::get_offset_of_U3CU3E2__current_1(),
	U3CTriggerShowInNextFrameU3Ed__95_t2F667FEC4BF3B323240968A9D12C56CE87131EBC::get_offset_of_instantAction_2(),
	U3CTriggerShowInNextFrameU3Ed__95_t2F667FEC4BF3B323240968A9D12C56CE87131EBC::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5692 = { sizeof (U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5692[15] = 
{
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CU3E1__state_0(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CU3E2__current_1(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_instantAction_2(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CU3E4__this_3(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CmoveFromU3E5__1_4(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CmoveToU3E5__2_5(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CrotateFromU3E5__3_6(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CrotateToU3E5__4_7(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CscaleFromU3E5__5_8(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CscaleToU3E5__6_9(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CfadeFromU3E5__7_10(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CfadeToU3E5__8_11(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CstartTimeU3E5__9_12(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CtotalDurationU3E5__10_13(),
	U3CShowEnumeratorU3Ed__96_tC69583128583639634BE4ADB32A327ED540F981D::get_offset_of_U3CelapsedTimeU3E5__11_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5693 = { sizeof (U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5693[15] = 
{
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CU3E1__state_0(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CU3E2__current_1(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_instantAction_2(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CU3E4__this_3(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CmoveFromU3E5__1_4(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CmoveToU3E5__2_5(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CrotateFromU3E5__3_6(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CrotateToU3E5__4_7(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CscaleFromU3E5__5_8(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CscaleToU3E5__6_9(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CfadeFromU3E5__7_10(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CfadeToU3E5__8_11(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CstartTimeU3E5__9_12(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CtotalDurationU3E5__10_13(),
	U3CHideEnumeratorU3Ed__97_t049B4349E56593055F55AB706DE3A75FAFE075A1::get_offset_of_U3CelapsedTimeU3E5__11_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5694 = { sizeof (U3CHideWithDelayEnumeratorU3Ed__98_tBFDD34504651079027EBC9CE71ACC2BCACC1D091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5694[4] = 
{
	U3CHideWithDelayEnumeratorU3Ed__98_tBFDD34504651079027EBC9CE71ACC2BCACC1D091::get_offset_of_U3CU3E1__state_0(),
	U3CHideWithDelayEnumeratorU3Ed__98_tBFDD34504651079027EBC9CE71ACC2BCACC1D091::get_offset_of_U3CU3E2__current_1(),
	U3CHideWithDelayEnumeratorU3Ed__98_tBFDD34504651079027EBC9CE71ACC2BCACC1D091::get_offset_of_delay_2(),
	U3CHideWithDelayEnumeratorU3Ed__98_tBFDD34504651079027EBC9CE71ACC2BCACC1D091::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5695 = { sizeof (U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__99_t150C3AD37AD58726558A7C2AE82DD6B0F56BEDD4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5695[3] = 
{
	U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__99_t150C3AD37AD58726558A7C2AE82DD6B0F56BEDD4::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__99_t150C3AD37AD58726558A7C2AE82DD6B0F56BEDD4::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteShowSelectDeselectButtonEnumeratorU3Ed__99_t150C3AD37AD58726558A7C2AE82DD6B0F56BEDD4::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5696 = { sizeof (U3CExecuteHideDeselectButtonEnumeratorU3Ed__100_t12340B89B46953D589254BEC2E7C105912C77CA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5696[3] = 
{
	U3CExecuteHideDeselectButtonEnumeratorU3Ed__100_t12340B89B46953D589254BEC2E7C105912C77CA4::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteHideDeselectButtonEnumeratorU3Ed__100_t12340B89B46953D589254BEC2E7C105912C77CA4::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteHideDeselectButtonEnumeratorU3Ed__100_t12340B89B46953D589254BEC2E7C105912C77CA4::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5697 = { sizeof (U3CExecuteGetOrientationEnumeratorU3Ed__101_tFE43CF8EE5417243D1B178B99BAADD1998929041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5697[3] = 
{
	U3CExecuteGetOrientationEnumeratorU3Ed__101_tFE43CF8EE5417243D1B178B99BAADD1998929041::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteGetOrientationEnumeratorU3Ed__101_tFE43CF8EE5417243D1B178B99BAADD1998929041::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteGetOrientationEnumeratorU3Ed__101_tFE43CF8EE5417243D1B178B99BAADD1998929041::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5698 = { sizeof (UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5698[12] = 
{
	0,
	0,
	0,
	UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522::get_offset_of_Animation_3(),
	UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522::get_offset_of_AutoStartLoopAnimation_4(),
	UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522::get_offset_of_LoadSelectedPresetAtRuntime_5(),
	UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522::get_offset_of_InstantAnimation_6(),
	UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522::get_offset_of_OnFinished_7(),
	UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522::get_offset_of_OnStart_8(),
	UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522::get_offset_of_PresetCategory_9(),
	UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522::get_offset_of_PresetName_10(),
	UIViewBehavior_t735C597F51AF621EC424ED9F4AACD7034F2B8522::get_offset_of_m_progress_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5699 = { sizeof (UIViewEvent_t5DBF09498B7D574591E73E4A18578AE6CCE35F29), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
